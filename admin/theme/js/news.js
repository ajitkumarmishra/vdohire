$().ready(function () {
    $("#newsForm").validate({
        rules: {
            news_title: "required",
            news_cat: "required",
        },
        messages: {
            news_title: "Please Enter News title",
        }
    });
})

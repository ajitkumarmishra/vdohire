function ContactForms(ln) {
	var formId = $("#INPUT_FORM");
	var $validator = formId.validate({
		rules : {
			first_name : "required",
			//lastname : "required",
			email : {
				required : true,
				email : true
			},
			phone : "required"
		},
    lang: ln,
		submitHandler : function() {
			$('#validations').removeClass('successcolor');
			$.post(base_url+"pages/sendmail", formId.serialize(),  function(response) { 				 
         var obj = JSON.parse(response);
         $('.validations').html(obj.msg);
         document.getElementById("INPUT_FORM").reset();
			});
		
			$('#validations').addClass('successcolor');

		}
	});
}

initialzePages = function(ln){
	ContactForms(ln);
}
$(document).on("click", ".del_Listing", function () {
    var delID = $(this).attr('name');
    var deleteUrl = $("#P_deleteurl").val();
    bootbox.confirm("Are you sure to delete this ?", function (result) {
        if (result == true) {
            $.ajax({
                type: 'POST',
                url: deleteUrl,
                data: {delID: delID},
                success: function (res) {
                    var obj = JSON.parse(res);
                    if (obj['status'] == true) {
                        location.reload();
                    } else {
                        alert("Bad request");
                    }
                },
            });
        }
    });
});

$().ready(function () {
    $("#productForm").validate({
        rules: {
            store_id: "required",
            barcode: "required",
            material_desc: "required",
            material_mrp: "required",
            material_sp: "required",
            material_pp: "required",
            material_class1: "required",
            material_class2: "required",
            material_class3: "required",
            material_tax_id: "required",
            manufacturer: "required",
            brand: "required",
            material_pack_unit1: "required",
            material_pack_unit2: "required",
            material_pack_conversion: "required",
            material_strength: "required",
            unit_of_strength: "required",
            material_measure: "required",
            material_uom: "required",
            material_code: "required",
            is_prescription_relevant: "required",
            is_batch_relevant: "required",
            material_drug_category: "required",
            material_schedule: "required"
        },
        messages: {
            barcode: "Please enter Barcode",
            material_desc: "Please enter Material Description",
            material_MRP: "Please enter MRP",
        }
    });
})
$.validator.setDefaults({
    submitHandler: function () {
        alert("submitted!");
    }
});

function checkall(objForm){
	len = objForm.elements.length;
	var i=0;
	for( i=0 ; i<len ; i++) {
		if (objForm.elements[i].type=='checkbox') {
			objForm.elements[i].checked=objForm.check_all.checked;
		}
	}
}

function import_csv(){
    $('input[type=file]').click();
}
function submit_form(){
    $("#import_btn").click();
}
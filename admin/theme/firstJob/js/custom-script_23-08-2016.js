$(function () {

    $("#datepicker-13, #datepicker-14").datepicker({
        changeYear: true
    });
    $('#datepick1').on('click', function () {
        $("#datepicker-13").datepicker("show");
    });
    $('#datepick2').on('click', function () {
        $("#datepicker-14").datepicker("show");
    });
    $('#cityDropDown').on('change', function () {
        var cityVal = $(this).val();
        if (cityVal != 0) {
            var url = siteUrl + 'users/getState';
            $.post(url, {city: cityVal, accessToken: token}, function (data) {
                if (data) {
                    $('#stateDropDown').html(data.trim());
                }
            });
        }
    });
    function readImageURL(input) {
        var arr = [];
        arr = input.files[0].name.split('.');
        var ext = ['jpg', 'png', 'jpeg'];
        if ($.inArray(arr[1].toString(), ext) == -1)
        {
            alert('Please upload jpg or png!');
            return false;
        }
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#uploadImageName').attr('src', e.target.result);
                $('#coprorateImage').val(e.target.result);
                $('#uploadImageName').show();
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#uploadCompanyLogo").click(function () {
        readImageURL(document.getElementById("companylogo"));
        //var url = '<?php echo base_url(); ?>';
        var file = document.getElementById("companylogo").files[0]; //fetch file
        var formData = new FormData();
        formData.append('companylogo', file); //append file to formData object
        $.ajax({
            url: siteUrl + "users/uploadLogo",
            type: "POST",
            data: formData,
            processData: false, //prevent jQuery from converting your FormData into a string
            contentType: false, //jQuery does not add a Content-Type header for you
            success: function (msg) {
                $('#coprorateLogoName').val(msg.trim());
            }
        });
    });
    //  alert($("#sorting").length);
    if ($("#sorting").length) {
        $("#sorting").tablesorter();
    }
    if ($("#sorting1").length) {
        $("#sorting1").tablesorter();
    }
    if ($("#sorting2").length) {
        $("#sorting2").tablesorter();
    }
    $('.chk_bx').on('click', function () {
        if ($(this).prop("checked") == true) {
            $('.inviteCheck').attr('checked', true);
        } else {
            $('.inviteCheck').attr('checked', false);
        }
    });
    $('.post-job').on('click', function () {
        var postJob = $(this);
        var jobId = $(this).attr('id');
        var target = $(this).attr('target');
        if (target == 1) {
            var posted = 1;
        } else {
            var posted = 1;
        }
        var url = siteUrl + 'jobs/postJob';
        $.post(url, {jobId: jobId, accessToken: token, posted: posted}, function (data) {
            if (data) {
                postJob.text(data.trim());
            }
        });
    });
    $('.status-job').on('click', function () {
        var statusJob = $(this);
        var jobId = $(this).attr('id');
        var target = $(this).attr('target');
        if (target == 1) {
            var status = 2;
        } else {
            var status = 1;
        }
        var url = siteUrl + 'jobs/statusJob';
        $.post(url, {jobId: jobId, accessToken: token, status: status}, function (data) {
            if (data) {
                statusJob.text(data.trim());
            }
        });
    });
    $('.replciate-job').on('click', function () {
        var url = siteUrl + 'jobs/replicate';
        var jobId = $(this).attr('id');
        $.post(url, {jobId: jobId, accessToken: token}, function (data) {
            // alert(data);
            if (data) {
                // statusJob.text(data.trim());
                bootbox.alert('Replicated successfully!', function () {
                    location.reload();
                });
                //location.reload();

            }
        });
    });
    $('body').on('keydown', '#titleOfJob', function (e) {
        if (e.keyCode === 9) {
            $('#cityState').parent().find('input').focus();
        }
    });
    $('body').on('keydown', '#noticePeriod', function (e) {
        if (e.keyCode === 9) {
            $('#qualification').parent().find('input').focus();
        }
    });

    $('body').on('keydown', '#jd', function (e) {
        if (e.keyCode === 9) {
            $('#assessmentAttach').parent().find('input').focus();
        }
    });

    $('body').on('keydown', '#assessmentAttach', function (e) {
        if (e.keyCode === 9) {
            $('#interviewAttach').parent().find('input').focus();
        }
    });

    $('.candidate-search').on('click', function () {
        var $i = 0;
        $('.search-critera').each(function (index) {
            if ($(this).val()) {
                $i++;
            }
        });
        if ($i == 0) {
            bootbox.alert('Please select at least one search criterea.', function () {
//location.reload();
            });
        } else {
            $('#searchForm').submit();
        }
    });
    $(document).on('change', '.question-file', function () {

        var type = $('#type').val();
        var msg = '';
        var fileArr = this.files[0].name.split('.');
        var ext = fileArr[fileArr.length - 1];

        // Video extension.
        var videoExt = JSON.parse(videoExtension);
        var videoExtArr = [];
        for (var i = 0; i < videoExt.length; i++) {
            videoExtArr.push(videoExt[i]);
        }

        // Audio extension.
        var audioExt = JSON.parse(audioExtension);
        var audioExtArr = [];
        for (var i = 0; i < audioExt.length; i++) {
            audioExtArr.push(audioExt[i]);
        }

        if ($('#type').val() == '') {
            msg = 'Please select interview type.';
        } else if ($.inArray(ext, videoExtArr) == -1 && type == 1) {
            msg = 'Following extensions are allowed for video type.<br/>' + videoExtArr.toString();
        } else if ($.inArray(ext, audioExtArr) == -1 && type == 2) {
            msg = 'Following extensions are allowed for audio type.<br/>' + audioExtArr.toString();
        }
        if (msg != '') {
            //delete-set
            bootbox.alert(msg, function () {
            });
            return false;
        }
        $(this).parent().parent().find('.file-name').val(this.files[0].name);
        $(this).parent().parent().find('#questionFileUpload').val('');
    });

    $(document).on('change', '.question-file1', function () {

        var type = $('#type_1').val();
        var msg = '';
        var fileArr = this.files[0].name.split('.');
        var ext = fileArr[fileArr.length - 1];

        // Video extension.
        var videoExt = JSON.parse(videoExtension);
        var videoExtArr = [];
        for (var i = 0; i < videoExt.length; i++) {
            videoExtArr.push(videoExt[i]);
        }

        // Audio extension.
        var audioExt = JSON.parse(audioExtension);
        var audioExtArr = [];
        for (var i = 0; i < audioExt.length; i++) {
            audioExtArr.push(audioExt[i]);
        }

        if ($('#type_1').val() == '') {
            msg = 'Please select interview type.';
        } else if ($.inArray(ext, videoExtArr) == -1 && type == 1) {
            msg = 'Following extensions are allowed for video type.<br/>' + videoExtArr.toString();
        } else if ($.inArray(ext, audioExtArr) == -1 && type == 2) {
            msg = 'Following extensions are allowed for audio type.<br/>' + audioExtArr.toString();
        }
        if (msg != '') {
            //delete-set
            bootbox.alert(msg, function () {
            });
            return false;
        }
        $(this).parent().parent().find('.file-name1').val(this.files[0].name);
        $(this).parent().parent().find('#questionFileUpload1').val('');
    });

    $(document).on('change', '.question-file2', function () {

        var type = $('#type_2').val();
        var msg = '';
        var fileArr = this.files[0].name.split('.');
        var ext = fileArr[fileArr.length - 1];

        // Video extension.
        var videoExt = JSON.parse(videoExtension);
        var videoExtArr = [];
        for (var i = 0; i < videoExt.length; i++) {
            videoExtArr.push(videoExt[i]);
        }

        // Audio extension.
        var audioExt = JSON.parse(audioExtension);
        var audioExtArr = [];
        for (var i = 0; i < audioExt.length; i++) {
            audioExtArr.push(audioExt[i]);
        }

        if ($('#type_2').val() == '') {
            msg = 'Please select interview type.';
        } else if ($.inArray(ext, videoExtArr) == -1 && type == 1) {
            msg = 'Following extensions are allowed for video type.<br/>' + videoExtArr.toString();
        } else if ($.inArray(ext, audioExtArr) == -1 && type == 2) {
            msg = 'Following extensions are allowed for audio type.<br/>' + audioExtArr.toString();
        }
        if (msg != '') {
            //delete-set
            bootbox.alert(msg, function () {
            });
            return false;
        }
        
        $(this).parent().parent().find('.file-name2').val(this.files[0].name);
        $(this).parent().parent().find('#questionFileUpload2').val('');
    });


    $(document).on('change', '#type', function () {
        if ($(this).val() == 3) {
            $('.file-section').hide();
        } else {
            $('.file-section').show();
        }
    });
    $(document).on('change', '#type_1', function () {
        if ($(this).val() == 3) {
            $('.file-section1').hide();
        } else {
            $('.file-section1').show();
        }
    });
    $(document).on('change', '#type_2', function () {
        if ($(this).val() == 3) {
            $('.file-section2').hide();
        } else {
            $('.file-section2').show();
        }
    });

    $(document).on('click', '.add-more-question-new', function () {
        //alert('dddddd');
        var i = parseInt($('#questionList .form-group').length);
        $(this).remove();
        $.post(siteUrl + 'getQuestionRow', {index: i, accessToken: token}, function (data) {
            // alert(data);
            $('#questionList').append(data);
            $(".chosen-select").chosen();
            if ($('#type').val() == 3) {
                $('.file-section').hide();
            }

        });
    });
    $(document).on('click', '.select-interview-question', function () {
        var qtype = $(this).parents('#selectQuestionType').find('select').val();
        $("#selectQuestionType .close").click();
        
        var selectedVal = "";
        var selected = $("input[type='radio'][name='environment']:checked");
        if (selected.length > 0) {
            selectedVal = selected.val();
        }
        
        if( selectedVal != "" && selectedVal != 0 ){
            if( selectedVal == 1 ){
                var questype = $('#type').val();
            } else if( selectedVal == 2 ){
                var questype = 1;
            }
        } else {
            var questype = $('#type').val();
        }
        //alert(qtype);
        if( questype == "" || questype == 0 ){
            bootbox.alert('Sorry, please select question type first', function () {
            });
            //alert("sdfsdfsdfsdf");
            return false;
        } else {
            var i = parseInt($('#questionList .form-group').length);
            // alert(i)
            //$('#addMore' + i).remove();
            $.post(siteUrl + 'getQuestionRow', {index: i, accessToken: token, qtype: qtype, questype:questype}, function (data) {
                //alert(data);
                $('#questionList').append(data);
                $(".chosen-select").chosen();
                if ($('#type').val() == 3) {
                    $('.file-section').hide();
                }
                $('.del-question1').on('click', function () {
                    $(this).parents('.form-group').remove();
                });

            });
        }
    });
    
    $('.del-question1').on('click', function () {
        $(this).parents('.form-group').remove();
    });
                
    /* first select question */
    $(document).on('click', '.select-interview-question1', function () {
        var qtype = $(this).parents('#selectQuestionType1').find('select').val();
        $("#selectQuestionType1 .close").click();
        var questype = 2;
        var i = parseInt($('#questionList1 .form-group').length);
        // alert(i)
        //$('#addMore1' + i).remove();
        $.post(siteUrl + 'getQuestionFirstRow', {index: i, accessToken: token, qtype: qtype, questype:questype}, function (data) {
            //alert(data);
            $('#questionList1').append(data);
            $(".chosen-select1").chosen();
            if ($('#type_1').val() == 3) {
                $('.file-section1').hide();
            }
            
            $('.del-question2').on('click', function () {
                $(this).parents('.form-group').remove();
            });

        });
    });
    
    $('.del-question2').on('click', function () {
        $(this).parents('.form-group').remove();
    });
            
    /* second select question */
    $(document).on('click', '.select-interview-question2', function () {
        var qtype = $(this).parents('#selectQuestionType2').find('select').val();
        $("#selectQuestionType2 .close").click();
        var questype = 3;
        var i = parseInt($('#questionList2 .form-group').length);
        // alert(i)
        //$('#addMore2' + i).remove();
        $.post(siteUrl + 'getQuestionSecondRow', {index: i, accessToken: token, qtype: qtype, questype:questype}, function (data) {
            //alert(data);
            $('#questionList2').append(data);
            $(".chosen-select2").chosen();
            if ($('#type_2').val() == 3) {
                $('.file-section2').hide();
            }
            
            $('.del-question3').on('click', function () {
                $(this).parents('.form-group').remove();
            });
        });
    });
    
    $('.del-question3').on('click', function () {
        $(this).parents('.form-group').remove();
    });
            
    $(document).on('click', '.add-more-my-question', function () {
        var i = parseInt($('#questionList .form-group').length);
        //$(this).remove();
        $.post(siteUrl + 'getMyQuestionRow', {index: i, accessToken: token}, function (data) {
            // alert(data);
            $('#questionList').append(data);
            $(".chosen-select").chosen();
            if ($('#type').val() == 3) {
                $('.file-section').hide();
            }
            
            $('.del-addquestion').on('click', function () {
                $(this).parents('.form-group').remove();
            });

        });
    });
    
    $('.del-addquestion').on('click', function () {
        $(this).parents('.form-group').remove();
    });
    $(document).on('click', '.add-more-assessment-question', function () {
        var i = parseInt($('#questionList .form-group').length);
        //$(this).remove();
        $.post(siteUrl + 'getAssessmentQuestionRow', {index: i, accessToken: token}, function (data) {
            // alert(data);
            $('#questionList').append(data);
            /*$(".chosen-select").chosen();
             if ($('#type').val() == 3) {
             $('.file-section').hide();
             }*/
            $('.del-assessment').on('click', function () {
                $(this).parents('.form-group').remove();
            });

        });
    });
    $('.del-assessment').on('click', function () {
        $(this).parents('.form-group').remove();
    });
    
    $('.notdelete-set').on('click', function () {
        
        var interviewId = $(this).attr('id');
        bootbox.alert('Sorry, you cant delete becasue some related records are available.', function () {
        });
        return false;

    });
    
    $('.notdelete-job').on('click', function () {
        
        var jobId = $(this).attr('id');
        bootbox.alert('Sorry, you cant delete becasue some related records are available.', function () {
        });
        return false;

    });
    
    
    $('.delete-set').on('click', function () {
        
        var interviewId = $(this).attr('id');
        bootbox.confirm('Are you sure you want to delete?', function (result) {

            if (result) {
                $.post(siteUrl + 'interviews/delete', {id: interviewId, accessToken: token}, function (data) {

                    //bootbox.alert(data);
                    location.reload();
                });
            }
        });

    });

    $('.delete-assessment-set').on('click', function () {
        var assessmentId = $(this).attr('id');
        bootbox.confirm('Are you sure you want to delete?', function (result) {
            if (result) {
                $.post(siteUrl + 'assessments/delete', {id: assessmentId, accessToken: token}, function (data) {
                    //alert(data);
                    location.reload();
                });
            }
        });

    });

    $('.quesdelete-set').on('click', function () {
        
        var questionId = $(this).attr('id');
        
        bootbox.confirm('Are you sure you want to delete?', function (result) {

            if (result) {
                $.post(siteUrl + 'interviews/questiondelete', {questionId: questionId, accessToken: token}, function (data) {

                    //bootbox.alert(data);
                    location.reload();
                });
            }
        });

    });
    
    
    $('.copydata-set').on('click', function () {
        
        var questionId = $(this).attr('data-questionid');
        var questionType = $(this).attr('data-id');
        
        bootbox.confirm('Are you sure you want to copy?', function (result) {

            if (result) {
                $.post(siteUrl + 'interviews/fjquestioncopy', {questionId: questionId, accessToken: token}, function (data) {

                    //bootbox.alert(data);
                    location.reload();
                });
            }
        });

    });
    
    
    $(document).on('click', '.add-question-to-interviewset', function () {
        var type = $(this).data('id');
        var questionId = $(this).data('questionid');
        //alert(questionId);
        $.post(siteUrl + 'getInterviewSetByType', {type: type, accessToken: token, questionId: questionId}, function (data) {
            //alert(data);
            $('#interviewSets').html(data);
            $(".chosen-select").chosen();
            /*if($('#type').val() == 3) {
             $('.file-section').hide();
             }*/

        });
    });

    $(document).on('click', '.add-question-to-assessmentset', function () {
        var type = $(this).data('id');
        var questionId = $(this).data('questionid');
        //alert(questionId);
        $.post(siteUrl + 'getAssessmentSetByType', {type: type, accessToken: token, questionId: questionId}, function (data) {
            //alert(data);
            $('#assessmentSets').html(data);
            $(".chosen-select").chosen();
            /*if($('#type').val() == 3) {
             $('.file-section').hide();
             }*/

        });
    });

    $(document).on('click', '.add-interview-question', function () {
        var interviewVal = $('#interviewSetsDropDown').val();
        var questionId = $('.question-id').val();
        //alert(questionId);
        if (!interviewVal) {
            $('#interviewQuestionAlert').html('Please select interview set.');
            $('#interviewQuestionAlert').removeClass('hidden');
        } else {
            $.post(siteUrl + 'checkInterview', {accessToken: token, questionId: questionId, interview: interviewVal}, function (data) {
                //alert(data.trim());
                if (data.trim() == 1) {
                    $('#interviewQuestionAlert').html('This question is already exist in selected interview set(s).');
                    $('#interviewQuestionAlert').removeClass('hidden');
                    return false;
                } else {
                    $('#interviewQuestionForm').submit();
                }
            });

        }
    });

    $(document).on('click', '.add-assessment-question', function () {
        var assessmentVal = $('#assessmentSetsDropDown').val();
        var questionId = $('.question-id').val();
        //alert(questionId);
        //alert(assessmentVal);
        if (!assessmentVal) {
            $('#interviewQuestionAlert').html('Please select assessment set.');
            $('#interviewQuestionAlert').removeClass('hidden');
        } else {
            $.post(siteUrl + 'checkAssessment', {accessToken: token, questionId: questionId, assessment: assessmentVal}, function (data) {
                //alert(data.trim());
                if (data.trim() == 1) {
                    $('#interviewQuestionAlert').html('This question is already exist in selected interview set(s).');
                    $('#interviewQuestionAlert').removeClass('hidden');
                    return false;
                } else {
                    $('#interviewQuestionForm').submit();
                }
            });

        }
    });


    $(document).on('change', '.my-question-file', function () {

        var type = $(this).parents('.form-group').find('#type').val();
        //alert(type);
        var msg = '';
        var fileArr = this.files[0].name.split('.');
        var ext = fileArr[fileArr.length - 1];

        // Video extension.
        var videoExt = JSON.parse(videoExtension);
        var videoExtArr = [];
        for (var i = 0; i < videoExt.length; i++) {
            videoExtArr.push(videoExt[i]);
        }

        // Audio extension.
        var audioExt = JSON.parse(audioExtension);
        var audioExtArr = [];
        for (var i = 0; i < audioExt.length; i++) {
            audioExtArr.push(audioExt[i]);
        }

        if (type == '') {
            msg = 'Please select interview type.';
        } else if ($.inArray(ext, videoExtArr) == -1 && type == 1) {
            msg = 'Following extensions are allowed for video type.<br/>' + videoExtArr.toString();
        } else if ($.inArray(ext, audioExtArr) == -1 && type == 2) {
            msg = 'Following extensions are allowed for audio type.<br/>' + audioExtArr.toString();
        }
        if (msg != '') {
            //delete-set
            bootbox.alert(msg, function () {
            });
            return false;
        }
        $(this).parent().parent().find('.file-name').val(this.files[0].name);
        $(this).parent().parent().find('#questionFileUpload').val('');
    });
    $('#myQuestions').on('click', function () {
        $('.add-new-set').attr('href', siteUrl + 'interviews/addQuestion');
        $('.add-new-set-text').text('Add Question');
        $('.service_content').hide();
        $('#myPaging').show();
    });
    $('#questionBank').on('click', function () {
        $('.service_content').hide();
        $('#fjPaging').show();
    });
    $('#InterviewSet').on('click', function () {
        $('.add-new-set').attr('href', siteUrl + 'interviews/add');
        $('.add-new-set-text').text('Add New Interview Set');
        $('.service_content').hide();
        $('#setPaging').show();
    });
    $('.del-question').on('click', function () {
        $(this).parents('.form-group').remove();
    });
    
    $('.answer-check').on('click', function () {
        //$('.answer-check').removeAttr('checked');
        //$('this').attr('checked', true);
    });
    $('#myAssessmentQuestions').on('click', function () {
        $('.add-new-set').show();
        $('.add-new-set').attr('href', siteUrl + 'questionbank/add');
        $('.add-new-set-text').text('Add Question Bank');
        $('.service_content').hide();
        $('#myPaging').show();
    });
    $('#AssessmentSet').on('click', function () {
        $('.add-new-set').show();
        $('.add-new-set').attr('href', siteUrl + 'assessments/add');
        $('.add-new-set-text').text('Add New Assessment Set');
        $('.service_content').hide();
        $('#setPaging').show();
    });
    $('#fjQuestionBank').on('click', function () {
        $('.add-new-set').show();
        $('.add-new-set').attr('href', siteUrl + 'questionbank/add/question/');
        $('.add-new-set-text').text('Add Question');
        $('.service_content').hide();
        $('#fjPaging').show();
    });
    $(document).on('click', '.delete-question-bank', function () {
        var questionbankid = $(this).data('questionbankid');
        //alert(questionbankid);
        bootbox.confirm('Are you sure you want to delete?', function (result) {
            if (result) {
                $.post(siteUrl + 'questionbank/delete', {accessToken: token, questionbankid: questionbankid}, function (data) {
                    // alert(data);
                    //location.reload();
                    window.location.href = 'mylist/1';
                });
            }
        });
    });

    $(document).on('click', '.delete-question-assessment', function () {
        var questionid = $(this).data('questionid');
        //alert(questionbankid);
        bootbox.confirm('Are you sure you want to delete?', function (result) {
            if (result) {
                $.post(siteUrl + 'assessments/question/delete', {accessToken: token, questionid: questionid}, function (data) {
                    // alert(data);
                    location.reload();
                });
            }
        });
    });

    $(".upload-file").click(function () {
        var jobId = $(this).attr('id');
        var file = document.getElementById("inviteUsers").files[0];//fetch file
        var inviteFrom = $('input[type="checkbox"][name="inviteFrom\\[\\]"]:checked').map(function() { 
                            return this.value; 
                        }).get();//fetch file
        //console.log(file['name']);
        //alert(file['name']);
        //alert(inviteFrom);
        
        if (!file) {
            $('#inviteAlert').html('<p>Please select File.</p>');
            $('#inviteAlert').removeClass('hidden');
            return false;
        }
        if ( inviteFrom == "" ) {
            $('#inviteAlert').html('<p>Please select atleast one either Email OR SMS</p>');
            $('#inviteAlert').removeClass('hidden');
            return false;
        }
        var formData = new FormData();
        formData.append('inviteUsers', file);
        formData.append('inviteFrom', inviteFrom);
        $.ajax({
            url: siteUrl + 'users/invite/' + jobId,
            data: formData,
            async: false,
            contentType: false,
            processData: false,
            cache: false,
            type: 'POST',
            success: function (data)
            {
                //alert(data);
                if (data.trim().toString() == 'invalid') {
                    $('#inviteAlert').html('<p>File must have CSV and with three columns in order (Name, Email, Phone).</p>');
                    $('#inviteAlert').removeClass('hidden');
                    return false;
                }
                $('.close').trigger('click');
                bootbox.alert('Users invited successfully!', function () {
                    location.reload();
                });
            },
        });
        return false;
    });

    $(document).on('click', '.invite-users', function () {
        var jobId = $(this).attr('id');
        $('.upload-file').attr('id', jobId);
    });

    $(document).on('click', '.invite-app-users', function () {
        var appUsers = [];
        $.each($(".inviteCheck:checked"), function () {
            appUsers.push($(this).val());
        });
        //alert(appUsers);
        if (appUsers == '') {
            $('#inviteAppUser').hide();
            $('#inviteAppUser .close').trigger('click');
            bootbox.alert('Please select at least one user!');
            return false;
        } else {
            $('#inviteAppUser').show();
        }
        // alert(appUsers);
        $('.userListInvite').val(appUsers);
    });
    $(document).on('click', '.save-invite-app-users', function () {
        
        if (!$('#myJobs').val()) {
            $('#inviteAlert').html('<p> Please select at least one job.</p>');
            $('#inviteAlert').removeClass('hidden');
            return false;
        }
        
        var inviteFrom = $('input[type="checkbox"][name="inviteFrom\\[\\]"]:checked').map(function() { 
                            return this.value; 
                        }).get();//fetch file
                        
        if ( inviteFrom == "" ) {
            $('#inviteAlert').html('<p>Please select atleast one either Email OR SMS</p>');
            $('#inviteAlert').removeClass('hidden');
            return false;
        }
        
        
        $('#inviteAppUser').submit();
    });



    $(document).on('click', '.add-more-audition-question', function () {
        var i = parseInt($('#questionList .form-group').length);
        //$(this).remove();
        $.post(siteUrl + 'getAuditionQuestionRow', {index: i, accessToken: token}, function (data) {
            //alert(data);
            $('#questionList').append(data);
            $(".chosen-select").chosen();
            if ($('#type').val() == 3) {
                $('.file-section').hide();
            }
            $('.del-auditionRow').on('click', function () {
                $(this).parents('.form-group').remove();
            });

        });
    });
    
    
    $('.del-auditionRow').on('click', function () {
        $(this).parents('.form-group').remove();
    });
    
    $('.delete-audition-set').on('click', function () {
        var auditionId = $(this).attr('id');
        bootbox.confirm('Are you sure you want to delete?', function (result) {

            if (result) {
                $.post(siteUrl + 'audition/delete', {id: auditionId, accessToken: token}, function (data) {
                    bootbox.alert(data);
                    location.reload();
                });
            }
        });

    });
    $(document).on('click', '.add-question-to-auditionset', function () {
        var questionId = $(this).data('questionid');
        //alert(questionId);
        $.post(siteUrl + 'getAuditionSet', {accessToken: token, questionId: questionId}, function (data) {
            //alert(data);
            $('#auditionSets').html(data);
            $(".chosen-select").chosen();
            /*if($('#type').val() == 3) {
             $('.file-section').hide();
             }*/

        });
    });
    $(document).on('click', '.add-audition-question', function () {
        var auditionVal = $('#auditionSetDropDown').val();
        var questionId = $('.question-id').val();
        if (!auditionVal) {
            $('#auditionQuestionAlert').html('Please select audition set.');
            $('#auditionQuestionAlert').removeClass('hidden');
        } else {
            $.post(siteUrl + 'checkAudition', {accessToken: token, questionId: questionId, audition: auditionVal}, function (data) {
                //alert(data);
                if (data.trim() == 1) {
                    $('#auditionQuestionAlert').html('This question is already exist in selected audition set(s).');
                    $('#auditionQuestionAlert').removeClass('hidden');
                    return false;
                } else {
                    $('#auditionQuestionForm').submit();
                }
            });

        }
    });
    $('#myAuditionQuestions').on('click', function () {
        $('.add-new-set').attr('href', siteUrl + 'audition/question');
        $('.add-new-set').text('Add Audition Question');
    });
    $('#AuditionSet').on('click', function () {
        $('.add-new-set').attr('href', siteUrl + 'audition/addAudition');
        $('.add-new-set').text('Add New Audition Set');
    });
});

//Functions List
function checkedForPractice(event) {
    
    var checkAssessmentIdValue = event.value; //assessment id
    if ($(event).is(':checked')) {
        var checkedStatus = 1;
        var alertMessage = "Assessment successfully added in practice set";
    } else {
        var checkedStatus = 0;
        var alertMessage = "Assessment successfully removed from practice set";
    }
    //alert(checkAssessmentIdValue);
    var formData = new FormData();
    formData.append('checkAssessmentIdValue', checkAssessmentIdValue); //append assess id to formData object
    formData.append('checkedStatus', checkedStatus); //append id checked status to formData object
    
    $.ajax({
        url: siteUrl + "assessments/setAssessmentAsPractice",
        type: "POST",
        data: formData,
        processData: false, //prevent jQuery from converting your FormData into a string
        contentType: false, //jQuery does not add a Content-Type header for you
        success: function (msg) {
            //alert(msg);
            if( msg.trim() == "Done" ){
                bootbox.alert(alertMessage, function () {
                });
                return false;
            } else if( msg == "NotDone" ) {
                bootbox.alert("Sorry, we are facing some technical issue with server", function () {
                });
                return false;
            } else if( msg == "Blank" ) {
                bootbox.alert("Please select assessment to add in practice set", function () {
                });
                return false;
            }
        }
    });
}

//Functions List
function jwVideo(videoUrl, elementId) {
    if (elementId) {
        jwplayer(elementId).setup({
            file: siteUrl + videoUrl,
            width: "100%",
            height: "100%",
            stretching: "fill",
            flashplayer: siteUrl + "theme/player/player.swf"
        });
    }
}
function jwVideoInList(videoUrl, elementId) {
    if (elementId) {
        jwplayer(elementId).setup({
            file: siteUrl + videoUrl,
            width: "70%",
            height: "10%",
            stretching: "fill",
            flashplayer: siteUrl + "theme/player/player.swf"
        });
    }
}
function jwVideoPlaylist(videoUrl, elementId) {
    jwplayer(elementId).setup({
        'flashplayer': siteUrl + "theme/player/player.swf",
        'controlbar': 'bottom',
        'volume': '60',
        'width': '480',
        'height': '380',
        'playlist': [{
                'file': siteUrl + videoUrl,
                'image': siteUrl + 'theme/firstJob/image/arrow_head.png',
                'title': 'Big Buck Bunny',
                'description': 'Shot Film'
            },
            {
                'file': siteUrl + videoUrl,
                'image': siteUrl + 'theme/firstJob/image/arrow_head.png',
                'title': 'Big Buck Bunny12',
                'description': 'Shot Film12'
            }],
        'playlist.position': "bottom",
        'playlist.size': '90',
        'plugins': 'hd',
        'primary': 'flash'
    });

}
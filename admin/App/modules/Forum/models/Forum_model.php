<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Forum Model
 * Description : Used to handle all CRUD operations
 * @author Synergy
 * @createddate : Nov 2, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 17, 2017
 */

class Forum_model extends CI_Model {

    /**
     * Use to initialize variable to store tables
     */
    var $table = "nc_forum";
    var $comment_table = "nc_comments";

    /**
     * Responsable for auto load the the session library
     * @return void
     */
    function __construct() {
        parent::__construct();
        $this->load->library('session');
    }

    /**
     * Description : Get limited list of data 
     * Author : Synergy
     * @param $showPerpage, $offset
     * @return array of data
     */
    function read($showPerpage, $offset) {
        $this->db->limit($showPerpage, $offset);
        $this->db->from($this->table);
        $query = $this->db->get();
        return $query->result();
    }

    /**
     * Description : Use to delete specific forum
     * Author : Synergy
     * @param $id
     * @return boolean
     */
    function deleteforum($id) {
        $this->db->where('forum_id', $id);
        $query = $this->db->delete($this->table);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Description : Use to details of a forum
     * Author : Synergy
     * @param $id
     * @return array of data
     */
    function getForum($id) {
        $this->db->where('forum_id', $id);
        $query = $this->db->get($this->table);
        return $query->row();
    }

    /**
     * Description : Use to comments on a specific forum
     * Author : Synergy
     * @param $id
     * @return array of data
     */
    function getForumComments($id) {
        $this->db->where('com.forum_id', $id);
        $this->db->select('com.*,us.user_nicename,IF((`us`.`user_pic`=""),null,CONCAT("' . base_url('uploads') . '/users/profile/",us.user_pic)) As user_pic', FALSE);
        $this->db->from($this->comment_table . ' as com');
        $this->db->join('users' . " us", "com.user_id = us.id", 'left');
        $query = $this->db->get($this->comment_table);
        return $query->result();
    }

    /**
     * Description : Use to count number of forum
     * Author : Synergy
     * @param $id
     * @return int
     */
    function count_all() {
        $query = $this->db->get($this->table);
        return $query->num_rows();
    }
}
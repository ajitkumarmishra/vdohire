<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Forum Controller
 * Description : Used to handle all Forum related data
 * @author Synergy
 * @createddate : Nov 2, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 15, 2017
 */

class Forum extends MY_Controller {

    /**
     * Responsable for auto load the the forum_model
     * Responsable for auto load session and form_validation library
     * @return void
     */
    function __construct() {
        parent::__construct();
        $this->load->model('forum_model');
        $this->load->library('session');
        $this->load->library('form_validation');
    }

    /**
     * Description : Use to list all all the Forum
     * Author : Synergy
     * @param null
     * @return Render data into view
     */
    function list_all() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('signin'));
        }
        $includeJs = array('theme/js/custom.js', 'theme/js/bootbox.min.js');
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $config = array();
        $config["base_url"] = base_url() . "forum/list_all";
        $config['total_rows'] = $this->forum_model->count_all();
        $config['per_page'] = (site_limit()) ? site_limit() : DEFAULT_LIMIT_PER_PAGE;
        $config['full_tag_open'] = '<div class="pagination pagination-small pagination-right"><ul class="pagination">';
        $config['full_tag_close'] = '</ul></div>';
        $config['first_link'] = true;
        $config['last_link'] = true;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["forum"] = $this->forum_model->read($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        $data['meta_title'] = "Forums";
        $data['meta_keyword'] = "Forums";
        $data['meta_description'] = "Forums";
        $data['main_content'] = 'forums';
        $this->load->view('page', $data);
    }

    /**
     * Description : Use to get details of forum
     * Author : Synergy
     * @param int $id
     * @return Render data into view
     */
    function details($id) {
        $data['forum'] = $this->forum_model->getForum($id);
        $data['comments'] = $this->forum_model->getForumComments($id);
        $data['main_content'] = 'details';
        $this->load->view('page', $data);
    }

    /**
     * Description : Use to get details of forum
     * Author : Synergy
     * @param int $id
     * @return Render data into view
     */
    function delete() {
        $id = $this->input->post('delID');
        if (isset($id) and $id != '') {
            $check = $this->forum_model->deleteforum($id);
            if ($check) {
                $data['status'] = true;
                $data['message'] = "Deleted Successfully";
                echo json_encode($data);
            } else {
                $data['status'] = false;
                $data['message'] = "Something Went wrong";
                echo json_encode($data);
            }
        } else {
            $data['status'] = false;
            $data['message'] = "Something Went wrong";
            echo json_encode($data);
        }
    }

    /**
     * Description : Use to get details of account or update account
     * Author : Synergy
     * @param int $id, $_POST
     * @return Render data into view
     */
    function account($id = null) {
        $this->_member_area();
        if ($id != null) {
            $userID = $id;
        } else {
            $userID = $this->session->userdata('userid');
        }

        if ($_POST) {
            $userdata = new stdClass();
            $userdata->user_nicename = $this->input->post('nickname');
            $userdata->user_name = $this->input->post('user_name');
            $userdata->user_mobile = $this->input->post('user_mobile');
            $userdata->user_dob = $this->input->post('user_dob');
            $userdata->user_email = $this->input->post('email');
            $userdata->user_pass = md5($this->input->post('password'));
            $insert = $this->user_model->update($userID, $userdata);
            if ($insert) {
                $data['message'] = "Updated succesfully";
                $data['user'] = $this->user_model->user_by_id($userID);
                $data['main_content'] = 'account';
                $this->load->view('page', $data);
            }
            return;
        }

        $data['user'] = $this->user_model->user_by_id($userID);
        $data['main_content'] = 'account';
        $this->load->view('page', $data);
    }
}
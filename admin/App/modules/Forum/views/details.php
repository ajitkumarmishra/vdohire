<h1><?php echo $forum->forum_title; ?></h1>
<p class="lead">
    by <a href="#"><?php echo getForumUser($forum->user_id); ?></a>
</p>
<hr>
<p><span class="glyphicon glyphicon-time"></span> Posted on <?php
    $datetime = new DateTime($forum->forum_creation_date);
    echo $datetime->format('l, F j, Y - h:i A');
    ?></p>
<hr>
<p class="lead"><?php echo $forum->forum_desc; ?></p>

<hr>

<?php foreach ($comments as $list) { ?>
    <div class="media">
        <a class="pull-left" href="#">
            <?php if($list->user_pic!=null){ ?>
            <img class="media-object comment-pic" src="<?php echo $list->user_pic; ?>" alt="">
            <?php } else { ?>
            <img class="media-object comment-pic" src="http://placehold.it/64x64" alt="">
            <?php } ?>
        </a>
        <div class="media-body">
            <h4 class="media-heading">
                <small><?php
                    $datetime = new DateTime($list->comment_creation_date);
                    echo $datetime->format('l, F j, Y - h:i A');
                    ?></small>
            </h4>
            <?php echo $list->comment_title; ?>
            <?php echo $list->comment_des; ?>
        </div>
    </div>
<?php } ?>
<?php if ($this->session->flashdata('message')) { ?>
    <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Success!</strong> <?php echo $this->session->flashdata('message'); ?>
    </div>
<?php } ?>
<?php if ($this->session->flashdata('error')) { ?>
    <div class="alert alert-danger">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Danger!</strong> <?php echo $this->session->flashdata('error'); ?>
    </div>
<?php } ?>
<div class="row">
    <div class="col-sm-12 col-md-12">
        <h1>Forums</h1>
        <hr/>
        <form method="post" name="form1" id="form1" action="">
            <table class="table">
                <thead>
                    <tr>
                        <th><input name="check_all" type="checkbox" id="check_all" value="1" onclick="checkall(this.form)" /></th>
                        <th>User id</th>
                        <th>Title</th>
                        <th>Date</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php //print_r($forum); die; ?>
                    <?php
                    foreach ($forum as $forums):
                        //print_r($forums); 
                        ?>
                        <tr class="<?php echo ($forums->forum_status == '1' ? 'success' : 'warning'); ?>">
                            <td>
                                <input name="arr_ids[]" type="checkbox" id="arr_ids[]" value="<?php echo $forums->forum_id; ?>" />
                            </td>
                            <td><?php echo getForumUser($forums->user_id); ?></td>
                            <td><?php echo $forums->forum_title; ?></td>
                            <td><?php echo $forums->forum_creation_date; ?></td>
                            <td><?php echo ($forums->forum_status == '1' ? 'Active' : 'Inactive'); ?></td>
                            <td>
                                <a href="javascript:void(0);" class="btn btn-mini btn-xs del_Listing" name="<?php echo $forums->forum_id; ?>"><span class="glyphicon glyphicon-remove "></span></a>
                                <a href="<?php echo base_url('forum/details/' . $forums->forum_id); ?>" class="btn btn-mini btn-xs" name="<?php echo $forums->forum_id; ?>"><span class="glyphicon glyphicon-eye-open "></span></a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="left" style="padding:2px">
                        <!--<input type="submit" name="Activate" value="Activate" class="btn"/>
                        <input type="submit" name="Deactivate" value="Deactivate" class="btn" />-->
                        <input type="submit" name="Delete" value="Delete" class="btn" />

                    </td>
                </tr>
            </table>
            <input type="hidden" id="P_deleteurl" value="<?php echo base_url('forum/delete'); ?>" />
            <?php echo $links; ?>
        </form>
    </div>
</div>

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Jobuserinvite Model
 * Description : Handle all the CRUD operation for Jobuserinvite
 * @author Synergy
 * @createddate : Nov 16, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 22, 2017
 */

class Jobuserinvite_model extends CI_Model {

    /**
     * Declaring variables
     */
    var $table = "fj_jobInvitationForUser";

    /**
     * Responsable for inheriting parent constructor
     * Responsable for auto load the the session and pagination library
     * @return void
     */
    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('pagination');
    }

    /**
     * countForInvitedJobs function
     * Discription : Use to get number of invitations on a specific job
     * @author Synergy
     * @param int $job_id.
     * @return array the count of invitation on specific job
     */
    function countForInvitedJobs($job_id) {
        
        try {
            
            $query = $this->db->select("count(id) as invitecount")
                                ->from($this->table)
                                ->where('jobId =', $job_id)
                                ->get();
            $result = $query->row_array();
           
            if (!$result) {
                throw new Exception('Unable to fetch jobs data.');
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    
    /**
     * countForInvitedPendingJobs function
     * Discription : Use to get number of pending invitations to users on specific job.
     * @author Synergy
     * @param int $job_id.
     * @return array the count of pending invitation on specific job
     */
    function countForInvitedPendingJobs($job_id) {
        
        try {
            
            $query = $this->db->select("count(id) as pendingcount")
                                ->from($this->table)
                                ->where('jobId =', $job_id)
                                ->where('invitationStatus =', 0)
                                ->get();
            $result = $query->row_array();
           
            if (!$result) {
                throw new Exception('Unable to fetch jobs data.');
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
}
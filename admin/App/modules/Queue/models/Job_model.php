<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Job Model
 * Description : Handle all the CRUD operation for Job
 * @author Synergy
 * @createddate : Nov 22, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 22, 2017
 */
class Job_model extends CI_Model {

    /**
     * Declaring variables
     */
    var $table = "jobs";

    /**
     * Responsable for inheriting parent constructor
     * Responsable for auto load the the session and pagination library
     * @return void
     */
    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('pagination');
    }

    /**
     * GetJob function
     * Description : Gets a specific job detail.
     * @author Synergy
     * @param int $id.
     * @return array the jobs data.
     */
    function getJob($id) {
        try {
            $this->db->select('*');
            $this->db->from('fj_jobs');
            $this->db->where('id', $id);
            $query = $this->db->get();
            $result = $query->row_array();
            if (count($result) == 0) {
                throw new Exception('Unable to fetch job data.');
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * listJobs function
     * Description : Use to Get lsit of jobs.
     * @author Synergy
     * @param int $limit, int $offset, string $serachColumn, string $searchText, int $createdBy, array $usersJobListL1, array $usersJobListL2, array $usersJobListL1L2, int $userRole
     * @return array of jobs data.
     */
    function listJobs($limit, $offset, $serachColumn = NULL, $searchText = NULL, $createdBy = NULL, $usersJobListL1, $usersJobListL2, $usersJobListL1L2, $userRole) {
        try {
            //echo 'listJobs';exit;
            $currentDate = date('Y-m-d h:i:s');
            $sql = "select j.id, j.jobId, j.fjCode, j.title, j.openTill, j.assessment, j.interview from fj_jobs as j";
            
            if( $usersJobListL1 != "" && $usersJobListL2 != "" )
            {   
                $sql .= " where j.id in (".$usersJobListL1.") or j.id in (".$usersJobListL2.")"; 
                
            } else if( $usersJobListL1 != "" && $usersJobListL2 == "" ) {
                $sql .= " where j.id in (".$usersJobListL1.")";
            } else if( $usersJobListL1 == "" && $usersJobListL2 != "" ) {
                $sql .= " where j.id in (".$usersJobListL2.")";
            } else if( $usersJobListL1L2 != "" && $userRole == 2 ) {
                $sql .= " where j.id in (".$usersJobListL1L2.")";
            } else {
                $sql .= " where j.id in ('')";
            }
            $sql .= " and j.openTill >= CURDATE()";
			$sql .= " order by j.title asc ";
            if(isset($offset)) {
                $sql .= " limit " .$offset. ", " .$limit."";
            }
			
            $query = $this->db->query($sql);
            $result = $query->result();

            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
	
	function listJobsNew($limit, $offset, $serachColumn = NULL, $searchText = NULL, $createdBy = NULL, $usersJobListL1, $usersJobListL2, $usersJobListL1L2, $userRole) {
        try {
            $currentDate = date('Y-m-d h:i:s');
            $sql = "select j.id, j.jobId, j.fjCode, j.title, j.openTill, j.assessment, j.interview from fj_jobs as j";
            
            if( $usersJobListL1 != "" && $usersJobListL2 != "" )
            {   
                $sql .= " where ( j.id in (".$usersJobListL1.") or j.id in (".$usersJobListL2.") )"; 
                
            } else if( $usersJobListL1 != "" && $usersJobListL2 == "" ) {
                $sql .= " where j.id in (".$usersJobListL1.")";
            } else if( $usersJobListL1 == "" && $usersJobListL2 != "" ) {
                $sql .= " where j.id in (".$usersJobListL2.")";
            } else {
                $sql .= " where j.id in ('')";
            }
            //$sql .= " and j.openTill >= CURDATE()";
			$sql .= " and j.status in ('1','5') ";
			$sql .= " order by j.title asc ";
            
			
            $query = $this->db->query($sql);
            $result = $query->result();
			
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
	
	function listJobsPast($limit, $offset, $serachColumn = NULL, $searchText = NULL, $createdBy = NULL, $usersJobListL1, $usersJobListL2, $usersJobListL1L2, $userRole, $allUserIdsStr) {
        try {
            
            $sql = "select j.id, j.jobId, j.fjCode, j.title, j.openTill, j.assessment, j.interview from fj_jobs as j";
            if($userRole == 2 and $allUserIdsStr != ""){
				$sql .= " where j.createdBy in (".$allUserIdsStr.") "; 
			}else if( $usersJobListL1 != "" && $usersJobListL2 != "" )
            {   
                $sql .= " where j.id in (".$usersJobListL1.") or j.id in (".$usersJobListL2.")"; 
                
            } else if( $usersJobListL1 != "" && $usersJobListL2 == "" ) {
                $sql .= " where j.id in (".$usersJobListL1.")";
            } else if( $usersJobListL1 == "" && $usersJobListL2 != "" ) {
                $sql .= " where j.id in (".$usersJobListL2.")";
            } else if( $usersJobListL1L2 != "" && $userRole == 2 ) {
                $sql .= " where j.id in (".$usersJobListL1L2.")";
            } else {
                $sql .= " where j.id in ('')";
            }
            $sql .= " and j.status = '6' ";
			$sql .= " order by j.title asc ";
            
			
            $query = $this->db->query($sql);
            $result = $query->result();

            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * userAssessmentDetail function
     * Description : Use to get details of assessment on specific job given by specific user
     * @author Synergy
     * @param int $userId, int $jobId
     * @return array assessment data.
     */
    function userAssessmentDetail($userId, $jobId) {
        try {
            $sql = "
                    SELECT
                        FINAL.*,
                        (SELECT MC.option FROM fj_multipleChoiceAnswer MC WHERE MC.id=FINAL.userAnswer) AS userOption
                    FROM
                        (
                        SELECT
                            RES.*,
                            (SELECT UA.answer FROM fj_userAnswers UA WHERE UA.userId='$userId' AND UA.questionId=RES.questionId AND UA.jobId='$jobId') AS userAnswer
                        FROM
                            (
                            SELECT
                                J.assessment        AS assessmentId,
                                Q.id                AS questionId,
                                Q.title,
                                Q.type              AS questionType,
                                Q.IsMultipleChoice  AS IsQuestionMultiple,
                                Q.answer            AS questionAnswerId,
                                MCA.option
                            FROM
                                fj_jobs                 J
                            JOIN
                                fj_assessmentQuestions  AQ,
                                fj_question             Q,
                                fj_multipleChoiceAnswer MCA
                            WHERE
                                J.assessment=AQ.assessmentId    AND
                                AQ.status='1'                   AND
                                AQ.questionId=Q.id              AND
                                Q.answer=MCA.id                 AND
                                J.id='$jobId'
                            GROUP BY
                                Q.id
                            )   RES
                        )   FINAL
                    
                    ";
					
            $query  = $this->db->query($sql);
            $result = $query->result();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
	
	function userAssessmentDetail_v2($currentUser, $currentJob){
		
		$this->db->select('*');
		$this->db->from('fj_userJobAssessmentQuestions');
		$this->db->where('jobId', $currentJob);
		$this->db->where('userId', $currentUser);
		$query = $this->db->get();
		$result = $query->result_array();
		
		return $result;
		
	}
	
	function assessmentOptionText($optionId){
		$this->db->select('*');
		$this->db->from('fj_multipleChoiceAnswer');
		$this->db->where('id', $optionId);
		$query = $this->db->get();
		$result = $query->result_array();
		
		return $result;
	}
}
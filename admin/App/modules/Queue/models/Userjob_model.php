<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Userjob Model
 * Description : Handle all the CRUD operation for Userjob
 * @author Synergy
 * @createddate : Nov 12, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 22, 2017
 */

class Userjob_model extends CI_Model {

    /**
     * Declaring variables
     */
    var $table = "fj_userJob";

    /**
     * Responsable for inheriting parent constructor
     * Responsable for auto load the the session and pagination library
     * @return void
     */
    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('pagination');
    }

    /**
     * countForUserJobs function
     * Discription : Use to get number of times a user applied on a job
     * @author Synergy
     * @param int $job_id
     * @param int $applied_status
     * @return array the count of application
     */
    function countForUserJobs($job_id, $applied_status) {
        
        try {
            $date = date("Y-m-d H:m:s", strtotime('-24 hours', time()));
            $query = $this->db->select("count(id) as appliedCount")
                                ->from($this->table)
                                ->where('jobId =', $job_id);
            
            if( $applied_status == 1 ){
                $query->where('status =', 1);
				$query->where('createdAt <',$date);
            } else if( $applied_status == 2 ){
                $query->where('(status=2)');
            } else if( $applied_status == 3 ){
                $query->where('(status=3)');
            }else if( $applied_status == 4 ){
                $query->where('(status=4)');
            } else if( $applied_status == 5 ){
                $query->where('(status=5)');
            } else {
                $query->where('status =', 0);
            }
            
            $selectqry = $query->get();
            $result = $selectqry->row_array();
			
            if (!$result) {
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
	
	function countForUserJobsForL2($job_id, $applied_status) {
        
        try {
            $date = date("Y-m-d H:m:s", strtotime('-24 hours', time()));
            $query = $this->db->select("count(id) as appliedCount")
                                ->from($this->table)
                                ->where('jobId =', $job_id);
            
            if( $applied_status == 1 ){
                $query->where('status =', 1);
				$query->where('createdAt <',$date);
            } else if( $applied_status == 2 ){
                $query->where('(status=2)');
				$query->where('createdAt <',$date);
            } else if( $applied_status == 3 ){
                $query->where('(status=3)');
            }else if( $applied_status == 4 ){
                $query->where('(status=4)');
            } else if( $applied_status == 5 ){
                $query->where('(status=5)');
            } else {
                $query->where('status =', 0);
            }
            
            $selectqry = $query->get();
            $result = $selectqry->row_array();
			
            if (!$result) {
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
	
	function countForUserJobsNew($job_id, $applied_status) {
        
        try {
            $date = date("Y-m-d H:m:s", strtotime('-24 hours', time()));
            $query = $this->db->select("count(id) as appliedCount")
                                ->from($this->table)
                                ->where('jobId =', $job_id);
            
            if( $applied_status == 1 ){
                $query->where('status =', 1);
            } else if( $applied_status == 2 ){
                $query->where('(status=2)');
            } else if( $applied_status == 3 ){
                $query->where('(status=3)');
            } else if( $applied_status == 5 ){
                $query->where('(status = 5)');
            } else if( $applied_status == 4 ){
                $query->where('(status=4)');
            } else {
                $query->where('status =', 0);
            }
            $query->where('createdAt >=',$date);
            $selectqry = $query->get();
            $result = $selectqry->row_array();
           
            if (!$result) {
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    
    /**
     * listForUserJobs function
     * Discription : Use to get users who have applied on specific job
     * @author Synergy
     * @param int $job_id.
     * @return array of users
     */
    function listForUserJobs($job_id) {
        
        try {
            
            $query = $this->db->select("UJ.*, U.fullname, U.imageSource, U.id as userid, U.resume_path")
                                ->from($this->table.' as UJ')
                                ->join('fj_users as U', 'U.id = UJ.userId', 'left')
                                ->where('UJ.jobId =', $job_id);
            echo $this->db->last_query();
            $selectqry = $query->get();
            $result = $selectqry->result_array();
                       
            if (!$result) {
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
	
	/**
     * listForUserJobs function
     * Discription : Use to get users who have applied on specific job
     * @author Synergy
     * @param int $job_id.
     * @return array of users
     */
	 
    function listForUserJobsNew($job_id,$sorting,$filterValues,$filterBy,$role) {
        
        try {
            if($sorting == 1){
				$sortBy = "UJ.createdAt ASC";
			}elseif($sorting == 2){
				$sortBy = "UJ.createdAt DESC";
			}elseif($sorting == 3){
				$sortBy = "U.fullname ASC";
			}elseif($sorting == 4){
				$sortBy = "U.fullname DESC";
			}else{
				$sortBy = "UJ.createdAt DESC";
			}
			/*if(isset($filterValues) && $filterValues != ""){
				$statusFilter = "UJ.status in (".$filterValues.")";
			}else{
				$statusFilter = "UJ.status in (1,2,4)";
			}*/
			if($role == 4){
				$userData = $this->session->userdata['logged_in'];
				$userLoggeinId = $userData->id;
				$userRole = $userData->role;
				$statusFilter = "UJ.status in (1,2,3)";
			}else if($role == 5){
				$userData = $this->session->userdata['logged_in'];
				$userLoggeinId = $userData->id;
				$userRole = $userData->role;
				$statusFilter = "UJ.status in (2,4,5)";
			}else{
				$statusFilter = "UJ.status in (1,2,3,4,5)";
			}
			
			
			$filterQuery = "";
			if($filterBy > 0){
					if($filterBy == 5 || $filterBy == 7){
						$filterQuery = " UJ.jobResume != '' ";
					}
					if($filterBy == 10){
						$location = $_GET['locationPrefered'];
						$filterQuery = " U.preferredLocation like '%".$location."%' ";
					}
					
			}
			
			
			if($filterQuery != ""){
				$query = $this->db->select("UJ.*, U.fullname, U.imageSource, U.email, U.id as userid,U.image,U.preferredLocation, U.resume_path")
                                ->from($this->table.' as UJ')
                                ->join('fj_users as U', 'U.id = UJ.userId', 'left')
                                ->where('UJ.jobId =', $job_id)
                                ->where($statusFilter)
                                ->where($filterQuery)
								->order_by($sortBy);
            }else{
				$query = $this->db->select("UJ.*, U.fullname, U.imageSource, U.email, U.id as userid,U.image,U.preferredLocation, U.resume_path")
                                ->from($this->table.' as UJ')
                                ->join('fj_users as U', 'U.id = UJ.userId', 'left')
                                ->where('UJ.jobId =', $job_id)
                                ->where($statusFilter)
								->order_by($sortBy);
			}
			
			
            $selectqry = $query->get();
            $result = $selectqry->result_array();
                       
            if (!$result) {
                return false;
            }
			
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * listForSubUserJobs function
     * Discription : Use to get users who have applied on specific job at specific jobStatus
     * @author Synergy
     * @param int $job_id
     * @param int $jobStatus
     * @return array the count of user jobs, short listed, rejected and pending
     */
    function listForSubUserJobs($job_id, $jobStatus) {
        
        try {
            
            $query = $this->db->select("UJ.*, U.fullname, U.imageSource, U.id as userPid, U.resume_path")
                                ->from($this->table.' as UJ')
                                ->join('fj_users as U', 'U.id = UJ.userId', 'left')
                                ->where('UJ.jobId =', $job_id)
                                ->where('UJ.status =', $jobStatus);
            
            $selectqry = $query->get();
            $result = $selectqry->result_array();
                       
            if (!$result) {
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    
    /**
     * detailsForUserJobs function
     * Discription : Use to get details of specific interivew given by specific user
     * @author Synergy
     * @param int $interview_id.
     * @param int $user_id.
     * @return array of interview data with user data
     */
    function detailsForUserJobs($interview_id, $user_id) {
        
        try {
            
            $query = $this->db->select("UJ.*,U.email, U.imageSource, U.fullname,U.image,U.pinCode,U.linkedInLink, U.id as userid,U.resume_path, J.title, J.description")
                                ->from($this->table.' as UJ')
                                ->join('fj_users as U', 'U.id = UJ.userId', 'left')
                                ->join('fj_jobs as J', 'J.id = UJ.jobId', 'left')
                                ->where('UJ.id =', $interview_id)
                                ->where('UJ.userId =', $user_id);
            
            $selectqry = $query->get();
            $result = $selectqry->row_array();
                       
            if (!$result) {
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * updateJobInterview function
     * Discription : Use to update a specific interview
     * @author Synergy
     * @param array $data.
     * @param int $hdninterviewid.
     * @return boolean.
     */
    function updateJobInterview($data, $hdninterviewid) {
        $userData = $this->session->userdata['logged_in'];
		
        try {
			if(isset($_POST['hdenjobid']) && $_POST['hdenjobid'] != ""){
				$query = $this->db->select("*")
                                ->from('fj_adminComments as ac')
                                ->where('ac.createdBy =', $userData->id)
                                ->where('ac.userJobId =', $_POST['hdninterviewid']);
				
				$selectqry = $query->get();
				$result = $selectqry->row_array();
				
				
				
				if(empty($result)) {
					$content['rating']    = $_POST['adminRating'];
					$content['firstName'] = $userData->fullname;
					$content['userJobId'] = $_POST['hdninterviewid'];
					$content['emailId']   = $userData->email;
					$content['comment']   = $_POST['interview_comment'];
					$content['createdAt'] = date("Y-m-d H:i:s");
					$content['createdBy'] = $userData->id;
					$resp = $this->db->insert('fj_adminComments', $content);
				} else {
					$content['rating']    = $_POST['adminRating'];
					$content['firstName'] = $userData->fullname;
					$content['comment']   = $_POST['interview_comment'];
					
					$this->db->where('createdBy', $userData->id);
					$this->db->where('userJobId', $_POST['hdenjobid']);
					$resp = $this->db->update('fj_adminComments', $content);
					
				}
			}
			
            $this->db->where('id', $hdninterviewid);
            $resp = $this->db->update('fj_userJob', $data);
			
            if (!$resp) {
                throw new Exception('Unable to update job data.');
                return false;
            }
            return $hdninterviewid;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    
    /**
     * countForUserJobs function
     * Discription : Use to get all shortlisted interview by Level1 user
     * @author Synergy
     * @param int $id.
     * @return array of shortlsited interviews
     */
    function allDoneJobsByL1($usersJobL2) {
        
        try {
            
            $query = $this->db->select("group_concat(DISTINCT UJ.jobId separator ',') as jobIdL2Str")
                                ->from($this->table.' as UJ');
            
            if( $usersJobL2 != "" ){
                $query->where("UJ.jobId in (".$usersJobL2.")");
            } else {
                $query->where("UJ.jobId in ('')");
            }
            
            $selectqry = $query->get();
            $result = $selectqry->row_array();
                       
            if (!$result) {
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    
    /**
     * countAllAppliedUserJobs function
     * Discription : Use to get all application on a specific job
     * @author Synergy
     * @param int $job_id.
     * @return array of application count
     */
    function countAllAppliedUserJobs($job_id) {
        
        try {
            
            $query = $this->db->select("count(id) as appliedCount")
                                ->from($this->table)
                                ->where('jobId =', $job_id)
                                ->get();
            $result = $query->row_array();

            if (!$result) {
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    
    /**
     * countAllAppliedUserMergedVideosJobs function
     * Discription : Use to get number of merged application video on specific job  
     * @author Synergy
     * @param int $job_id.
     * @return array of merged video count
     */
    function countAllAppliedUserMergedVideosJobs($job_id) {
        
        try {
            
            $query = $this->db->select("count(id) as appliedCountMergedVideo")
                                ->from($this->table)
                                ->where('jobId =', $job_id)
                                ->where('mergedVideo !=', '')
                                ->get();
            
            $result = $query->row_array();
           
            if (!$result) {
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * countAllAppliedUserJobs function
     * Discription : Use to get all application for sub user jobs
     * @author Synergy
     * @param int $job_id.
     * @param int $interview_tatus.
     * @return array of sub user jobs
     */
    function countAllAppliedForSubUserJobs($job_id, $interview_tatus) {
        
        try {
            
            $query = $this->db->select("count(id) as appliedCount")
                                ->from($this->table)
                                ->where('jobId =', $job_id)
                                ->where('status =', $interview_tatus)
                                ->get();
            
            $result = $query->row_array();
           
            if (!$result) {
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * getUserJobDetail function
     * Discription : Use to get a specific applied job details 
     * @author Synergy
     * @param int $userJobId.
     * @return array applied job details
     */
    function getUserJobDetail($userJobId) {
        
        try {
            
            $query = $this->db->select("UJ.*")
                                ->from($this->table.' as UJ')
                                ->where('UJ.id =', $userJobId);
            
            $selectqry = $query->get();
            $result = $selectqry->row_array();
                       
            if (!$result) {
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * interviewComment function
     * Discription : Use to get comments on a specific interview
     * @author Synergy
     * @param int $userJobId.
     * @return array of comments
     */
    function interviewComment($userJobId) {        
        try {
            $sql    = "
                    SELECT 
                        UJC.* 
                    FROM 
                        fj_userJobComments 	UJC
                    LEFT JOIN
                        fj_userJobShare		UJS
                    ON
                        UJC.userJobShareId=UJS.id
                    WHERE
                        UJS.userJobId=".$userJobId."
                    ";
            $query  = $this->db->query($sql);
            $result = $query->result_array();
                      
            if (!$result) {
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
	
	function adminComment($userJobId) {        
        try {
            $sql    = "
                    SELECT 
                        AC.* 
                    FROM 
                        fj_adminComments 	AC
                    
                    WHERE
                        AC.userJobId=".$userJobId."
                    ";
            $query  = $this->db->query($sql);
            $result = $query->result_array();
                      
            if (!$result) {
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
	
	function adminUserComment($userJobId,$adminId) {        
        try {
            $sql    = "
                    SELECT 
                        AC.* 
                    FROM 
                        fj_adminComments 	AC
                    
                    WHERE
                        AC.userJobId=".$userJobId." and AC.createdBy = 
                    ".$adminId;
            $query  = $this->db->query($sql);
            $result = $query->result_array();
                      
            if (!$result) {
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
	
	
	function getCityStatePincode($pincode) {        
        try {
            $sql    = "SELECT city, state from fj_pinCode WHERE pinCode= '".$pincode."'";
            $query  = $this->db->query($sql);
            $result = $query->result();
                      
            if (!$result) {
                return false;
            }
            return $result[0];
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
	
	function checkInvitationUserJob($jobId,$email){
		try {
            $sql    = "SELECT * from  fj_jobInvitationForUser WHERE email= '".$email."' and jobId = ".$jobId;
            $query  = $this->db->query($sql);
            $result = $query->result();
                      
            if (!$result) {
                return false;
            }
            return $result[0];
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
	}
	
	function getUserJobPanel($jobId, $userId){
		$query = $this->db->select("*")
                                ->from('fj_jobPanel as UJ')
                                ->where('UJ.jobId =', $jobId)
                                ->where('UJ.userId = '.$userId);
		 $selectqry = $query->get();
            $result = $selectqry->result_array();
			
			return  $result;
								
		
	}
}
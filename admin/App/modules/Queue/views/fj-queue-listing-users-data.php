<?php
$ci =&get_instance();
                    //job user invitation model
$ci->load->model('jobuserinvite_model');
$ci->load->helper('fj');
$currentJob = 0;
$currentUser = 0;
if(isset($_GET['JobId']) && $_GET['JobId'] != ""  && $_GET['JobId'] != 0 ){
	$currentJob = $_GET['JobId'];
	$currentJob = decryptURLparam($currentJob, URLparamKey);
}

if(isset($_GET['userId']) && $_GET['userId'] != "" && $_GET['userId'] != 0){
	$currentUser = $_GET['userId'];
	$currentUser = decryptURLparam($currentUser, URLparamKey);
}

$Class= $_POST['class'];
$classArray = explode(" ",$Class);
//echo "<pre>";print_r($classArray);die;
foreach($classArray as $className){
	if (strpos($className, 'userlisting_') !== false) {
		$currentClass = $className;
		$ContentArray = explode("_",$className);
		$currentUser = $ContentArray[count($ContentArray)-1];
		$currentJob = $ContentArray[count($ContentArray)-2];
		$UjId = $ContentArray[count($ContentArray)-3];
		break;
	}
}
$readOnlyAll = "";
$DIsableAll = "";
$DIsableColor = "";
if(isset($_POST['past']) && $_POST['past'] = 1){
	$DIsableAll = ' disabled ';
	$readOnlyAll = ' disabled ';
	$DIsableColor = ' background:#e8e8e8; ' ;
}
$UserJobRole = $ci->userjob_model->getUserJobPanel($currentJob,$AdminLoggedInuserId);
$getJobDetail = $ci->job_model->getJob($currentJob);
$currentJobName = $getJobDetail['title'];

if($UserJobRole) {
	if($UserJobRole[0]['level'] == 1){
		$userJobRoleFor = 4;
		$ci->db->where('id', $UjId)->update('fj_userJob', array('viewed' => 1, 'L1ViewedDate' => date('Y-m-d H:i:s')));
	}else if($UserJobRole[0]['level'] == 2){
		$userJobRoleFor = 5;
		$ci->db->where('id', $UjId)->update('fj_userJob', array('viewed' => 1, 'L2ViewedDate' => date('Y-m-d H:i:s')));
	}else{
		//$ci->db->where('id', $UjId)->update('fj_userJob', array('viewed' => 1));
		//$userJobRoleFor = 2;
	}
} else {
	//$userJobRoleFor = 2;
}

$detailsForUserJob = $ci->userjob_model->detailsForUserJobs($UjId, $currentUser);


?>

			<div class="col-md-12" style="padding-left:0px;padding-right:0px;    border-bottom: 1px solid lightgray;">
				<div class="col-md-3" style="padding-left:0px;padding-right:0px;">
					<div style="background:#fff;color:#000;padding-left:12px;padding-top:6px;height:136px;padding-right: 12px;    padding-bottom: 6px;">
						<div class="pull-left  col-md-2" style="padding-left: 17px;">
							<?php if($currentUser > 0){  ?>
								<?php if($detailsForUserJob['image'] == ""){ ?>
									<img src="http://35.154.53.72/admin/theme/firstJob/image/user_image.png" style="width: 46px;" class="img-circle" >
								<?php } else { ?>
									<?php if($detailsForUserJob['imageSource'] == 0) { ?>
										<img src="<?php echo 'https://'.$_SERVER["HTTP_HOST"] ?>/admin/uploads/userImages/<?php echo $detailsForUserJob['image']  ?>"  style="width: 46px;"  class="img-circle">
									<?php } else { ?>
										<img src="<?php echo 'https://'.$_SERVER["HTTP_HOST"] ?>/jobseeker/uploads/userImages/<?php echo $detailsForUserJob['image']  ?>"  style="width: 46px;"  class="img-circle">
									<?php } ?>
								<?php } ?>
							<?php } else { ?>
								<div  style="width: 46px;background:#0094ba;    height: 46px;    margin-top: 6px;"  class="img-circle"></div>
							<?php } ?>
						</div>
						<div class="pull-left col-md-10" style="padding-top:0px; padding-left: 30px;">
							<div style="float:left;padding-top:2px;    font-size: 18px;font-weight:bold;">
								<?php 
									if($detailsForUserJob['fullname'] != ""){
										$nameArray = explode(" ",$detailsForUserJob['fullname']);
										$i = 0;
										//echo "<pre>";print_r($nameArray);die;
										foreach($nameArray as $array){
											if($i == 0){
												echo $array."<br>";
												$i++;
											}else{
												echo " ".$array;
											}
										}
									}else{
										echo "N/A";
									}

								//echo ($detailsForUserJob['fullname'] == "")?"N/A":$detailsForUserJob['fullname']
								?>
							</div>
							<?php
							if($detailsForUserJob['linkedInLink'] != ""){ ?>
								<div style="float:right"><a href="<?php echo $detailsForUserJob['linkedInLink']; ?>" target="_blank"><i class="fa fa-linkedin-square" aria-hidden="true" style="    color: #0077b5;font-size: 16px;"></i></a></div>
							<?php }
							?>
							
							<div style="clear:both"></div>
							<div style="font-size:13px;    height: 26px;">
								<?php
									$PinCode = $ci->userjob_model->getCityStatePincode($detailsForUserJob['pinCode']);
									if(!empty($PinCode)){
										echo ucwords(strtolower($PinCode->city)).", ".ucwords(strtolower($PinCode->state));
									}
								?>
							</div>
						</div>
						<div style="clear:both"></div>
						
						<div style="color:black;padding-top:10px;padding-bottom:10px;">
							<center><div class=" pull-left" style="float:left">
								<?php 
								
								if($currentUser > 0){  ?>
								<?php if($detailsForUserJob['jobResume'] != ""){ ?>
									<a data-toggle="modal" data-target="#UserResume"  onclick="getResumeUsers('<?php echo $detailsForUserJob['jobResume'] ?>')"   class="btn" style="width:100%;background:#054F72;color:#fff;padding:5px;"><i class="fa fa-file" aria-hidden="true"></i>&nbsp;&nbsp;Resume</a>
								<?php }else if(trim($detailsForUserJob['resume_path']) != ""){ ?>
									<a data-toggle="modal" data-target="#UserResume"  onclick="getResumeUsers('<?php echo $detailsForUserJob['resume_path'] ?>')"   class="btn" style="width:100%;background:#054F72;color:#fff;padding:5px;"><i class="fa fa-file" aria-hidden="true"></i>&nbsp;&nbsp;Resume</a>
								<?php }else{ ?>
									<button  class="btn" style="width:100%;background: #e8e8e8;color:#fff;padding:5px;" disabled><i class="fa fa-file" aria-hidden="true"></i>&nbsp;&nbsp;Resumes</button>
								<?php } ?>
								<?php } ?>
							</div>
							<div class="pull-right"  style="float:right">
							<?php if($currentUser > 0){  ?>
								<?php
								//video Resume list
								$statictoken = 'ecbcd7eaee29848978134beeecdfbc7c';
								$base_url = base_url().'Fjapi';
								$jsonVideo = array("token" => $statictoken, "methodname" => 'auditionHistoryForWeb', 'userId' => $currentUser);
								
								$data_string_video = json_encode($jsonVideo);                                                                                   
																																					 
								$ch_visitor = curl_init($base_url);                                                                      
								curl_setopt($ch_visitor, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
								curl_setopt($ch_visitor, CURLOPT_POSTFIELDS, $data_string_video);                                                                  
								curl_setopt($ch_visitor, CURLOPT_RETURNTRANSFER, true);
								curl_setopt($ch_visitor, CURLOPT_SSL_VERIFYPEER, false);
								curl_setopt($ch_visitor, CURLOPT_SSL_VERIFYHOST, 0);                     
								curl_setopt($ch_visitor, CURLOPT_HTTPHEADER, array(                                                                          
									'Content-Type: application/json',                                                                                
									'Content-Length: ' . strlen($data_string_video))                                                                       
								);                                                                                                                   
								$curlVideoResult = curl_exec($ch_visitor);
								
								$VideoallResult = json_decode($curlVideoResult);
								$VideoresumeList = $VideoallResult->data;
									if(count($VideoresumeList)>0){
										
								?>
								
									<a  href="javascript://"  onclick="getResumeUsers('<?php echo $detailsForUserJob['userId'] ?>', '<?php  echo $detailsForUserJob['jobId'] ?>')"   data-toggle="modal" data-target="#videoAudition" class="btn" style="width:107px;color: #fff;    background: #054F72;padding:5px;"><i class="fa fa-video-camera" aria-hidden="true"></i>&nbsp;&nbsp;Video Resume</a>
									<?php }else{  ?>
									<button class="btn" style="width:107px;color: #fff;    background: #e8e8e8;padding:5px;" disabled><i class="fa fa-video-camera" aria-hidden="true"></i>&nbsp;&nbsp;Video Resume</button>
									<?php } ?>
								<?php } ?>
							</div>
							<div style="clear:both"></div>
							</center>
						</div>
					</div>
				</div>
				<?php
				$AdminUserComments   = $this->userjob_model->adminUserComment($UjId, $userData->id);
				$AdminUserComment = $AdminUserComments[0];
				
				?>
				<div class="col-md-9" style="padding-left:0px;padding-right:0px;">
					<form name="submitReviewForm" id="submitReviewForm" method="post" >
						<div style="    border-left: 1px solid black;background:#fff;color:#000;padding-left:12px;   padding-bottom: 6px;height: 136px;padding-right:12px;    padding-top: 6px;">
							<div class="col-md-3 pull-left" style="">		
							<div class="col-md-12 rating" style="    padding-left: 0px;">
							<?php 
							$disbaledStatus = "";
							$readonly = "";
							if($AdminUserComment['comment'] != ""){ 
									$disbaledStatus = "";
									$readonly = "";
							}
							if($userJobRoleFor == 4){
								if($detailsForUserJob['status'] == 2 || $detailsForUserJob['status'] == 3 ){
									$disbaledStatus = " disabled ";
									$readonly = " readonly ";
								}else{
									$detailsForUserJob['status'] = "1";
								}
							}else if($userJobRoleFor == 5){
								if($detailsForUserJob['status'] == 4 || $detailsForUserJob['status'] == 5 ){
									$disbaledStatus = " disabled ";
									$readonly = " readonly ";
								}else{
									$detailsForUserJob['status'] = "1";
								}
							}else{
								$detailsForUserJob = array();
								$disbaledStatus = "  ";
								$readonly = "  ";
								
							}
							
							?>
								<div style="">Rating<span style="color:red">*</span></div>
								<fieldset class="rating">
									<input onclick="initiate()"  type="radio" id="star5" name="adminRating" value="5"   <?php echo ($AdminUserComment['rating'] == 5)?"checked ".$disbaledStatus:"".$disbaledStatus ?>  <?php echo $DIsableAll ?>/>
									<label class = "full" for="star5" title="Excellent"></label>
									<input onclick="initiate()"  type="radio" id="star4half" name="adminRating" value="4.5"   <?php echo ($AdminUserComment['rating'] == 4.5)?"checked ".$disbaledStatus:"".$disbaledStatus ?>  <?php echo $DIsableAll ?>/>
									<label class="half" for="star4half" title="Very Good"></label>
									<input onclick="initiate()"  type="radio" id="star4" name="adminRating" value="4"   <?php echo ($AdminUserComment['rating'] == 4)?"checked ".$disbaledStatus:"".$disbaledStatus ?> <?php echo $DIsableAll ?>/>
									<label class = "full" for="star4" title="Very Good"></label>
									<input onclick="initiate()"  type="radio" id="star3half" name="adminRating" value="3.5"   <?php echo ($AdminUserComment['rating'] == 3.5)?"checked ".$disbaledStatus:"".$disbaledStatus ?> <?php echo $DIsableAll ?>/>
									<label class="half" for="star3half" title="Good"></label>
									<input onclick="initiate()"  type="radio" id="star3" name="adminRating" value="3"   <?php echo ($AdminUserComment['rating'] == 3)?"checked ".$disbaledStatus:"".$disbaledStatus ?> <?php echo $DIsableAll ?>/>
									<label class = "full" for="star3" title="Good"></label>
									<input onclick="initiate()"  type="radio" id="star2half" name="adminRating" value="2.5"   <?php echo ($AdminUserComment['rating'] == 2.5)?"checked ".$disbaledStatus:"".$disbaledStatus ?> <?php echo $DIsableAll ?>/>
									<label class="half" for="star2half" title="Fair"></label>
									<input onclick="initiate()"  type="radio" id="star2" name="adminRating" value="2"  <?php echo ($AdminUserComment['rating'] == 2)?"checked ".$disbaledStatus:"".$disbaledStatus ?>  <?php echo $DIsableAll ?>/>
									<label class = "full" for="star2" title="Fair"></label>
									<input onclick="initiate()"  type="radio" id="star1half" name="adminRating" value="1.5"   <?php echo ($AdminUserComment['rating'] == 1.5)?"checked ".$disbaledStatus:"".$disbaledStatus ?> <?php echo $DIsableAll ?> />
									<label class="half" for="star1half" title="Bad"></label>
									<input onclick="initiate()"  type="radio" id="star1" name="adminRating" value="1"   <?php echo ($AdminUserComment['rating'] == 1)?"checked ".$disbaledStatus:"".$disbaledStatus ?> <?php echo $DIsableAll ?>/>
									<label class = "full" for="star1" title="Bad"></label>
									<input onclick="initiate()"  type="radio" id="starhalf" name="adminRating" value="0.5"  <?php echo ($AdminUserComment['rating'] == 0.5)?"checked ".$disbaledStatus:"".$disbaledStatus ?> <?php echo $DIsableAll ?> />
									<label class="half" for="starhalf" title="Bad">
									</label>
								</fieldset>
							</div>
							<div class="col-md-12" style="padding-right:0px;padding-left:0px;    padding-top: 16px;">
								<?php if($currentUser > 0){
									
									
									
								?>
								<div style="padding-bottom: 4px;">My Recommendation<span style="color:red">*</span></div>
								<div>
										<input type="hidden" name="hdenjobid" value="<?php echo $currentJob  ?>">
										<input type="hidden" name="accessToken" value="f1r$tj0b098">
										<input type="hidden" name="hdnusrid" id="hdnusrid" value="<?php echo $currentUser ?>">
										<input type="hidden" name="hdninterviewid" id="hdninterviewid" value="<?php echo $UjId ?>">
								<?php 
								if($userJobRoleFor == 5){?>
									<select  onchange="initiate()"  class="form-control" name="interview_status" id="interview_status_dropdown" style="border-color: #B5B5B5;color: black !important;" <?php echo $disbaledStatus ?>>
										<option value="2"  <?php echo ($detailsForUserJob['status'] == 2)?"selected":"" ?> >Undecided</option>
										<option value="4"  <?php echo ($detailsForUserJob['status'] == 4)?"selected":"" ?> >Shortlisted</option>
										<option value="5"  <?php echo ($detailsForUserJob['status'] == 5)?"selected":"" ?>>Rejected</option>
									</select>
								<?php }else{

								?>
									<select  onchange="initiate()" class="form-control" name="interview_status" id="interview_status_dropdown" style="border-color: #B5B5B5;color: black !important;" <?php echo $disbaledStatus ?> <?php echo $DIsableAll ?>>
										<option value="1"  <?php echo ($detailsForUserJob['status'] == 1)?"selected":"" ?> >Undecided</option>
										<option value="2"  <?php echo ($detailsForUserJob['status'] == 2)?"selected":"" ?> >Shortlisted</option>
										<option value="3"  <?php echo ($detailsForUserJob['status'] == 3)?"selected":"" ?>>Rejected</option>
									</select>
								<?php } ?>
								</div>
							
								<?php } ?>
							</div>
						</div>
						<div class="col-md-6 pull-left"  style="">
							<div>Comment<span style="color:red">*</span></div>
							<textarea col="10" row="10" placeholder="Comments" name="interview_comment" onchange="initiate()" id="interview_comment" style="background:#fff;color:#000;width:100%;height:104px;border-radius:5px;" <?php echo $readonly ?> <?php echo $readOnlyAll ?>><?php echo $AdminUserComment['comment'] ?></textarea>
						</div>
						
						<div class="col-md-3 pull-right" style="    padding-top: 2%;">
						<?php if($currentUser > 0){  ?>
							
							<div style="">
								<?php if($detailsForUserJob['status'] == 2 || $detailsForUserJob['status'] == 3 || $detailsForUserJob['status'] == 5 || $detailsForUserJob['status'] == 4){ ?>
									<input type="button"  onclick="submitReview('<?php echo $currentClass; ?>');"  name="submit" value="Submit" class="btn" style=" color:white;background:#054F72;   width: 100%;padding: 15px; float: right;<?php echo $DIsableColor ?>"  <?php echo $disbaledStatus ?>>
								<?php }else{ ?>
									<input type="button"  onclick="submitReview('<?php echo $currentClass; ?>');"  name="submit" value="Submit" class="btn" style=" color:white;background:#054F72;   width: 100%;padding: 15px; float: right;">
								<?php } ?>
							</div>
							<?php } ?>
						</div>
						<div style="clear:both"></div>
					</div>
					</form>
				
				</div>
					
				</div>
				<div style="clear:both"></div>
				<?php if($currentUser > 0){ ?>
					<?php
					
					$jobApplicationDetail   = $this->job_model->userAssessmentDetail_v2($currentUser, $currentJob);
					
					?>
				<?php }else{
					$jobApplicationDetail = null;
					
				 }
					
					
				 ?>
				<div class="row" style="padding-left:0px;padding-right:0px;    background: white;    padding-top: 8px;">
					<ul class="nav nav-tabs applicants-tabs">
						<li id="panellevelformid"  class="active"  style="color:black;font-size: 16px;" ><a data-toggle="tab" href="#recruitersjobcontent" aria-expanded="false"  style="color:black;">
							<?php
							if($detailsForUserJob['source'] == 0){
								echo '<i style="font-size:18px;" class="fa fa-mobile" aria-hidden="true" title="Mobile User"></i>&nbsp;&nbsp;';
							}else{
								echo '<i style="font-size:18px;"  class="fa fa-desktop" aria-hidden="true"  title="Website User"></i>&nbsp;&nbsp;';
							}
							?>
							<b>Video Interview</b></a>
						</li>
						<?php if(count($jobApplicationDetail) > 0){ ?>
						<li  id="basicformid" style="color:black;font-size: 16px;">
							<a id="basicjobcontentId" data-toggle="tab" href="#basicjobcontent" aria-expanded="true"  style="color:black;">
								<div title="Assessment score of applicant" class="pull-left" id="assessmentRating"  ><b><i style="font-size:18px;"  class="fa fa-book" aria-hidden="true"></i>&nbsp;&nbsp;Assessment Details</b></div>
								<!--<div  class="pull-left"  style="">0</div>-->
								<div style="clear:both;"></div>
							</a>
						</li>
						<?php } ?>
						<li style="color:black;font-size: 16px;" id="interviewformid" class="">
							<a data-toggle="tab" href="#interviewjobcontent" aria-expanded="false"  style="color:black;">
								<div   title="Average Interview Feedback Score by Reviewers" class="pull-left" id="totalFeedback"><i style="font-size:18px;"  class="fa fa-houzz"></i>&nbsp;&nbsp;<b>Interview Feedback</b></div>
									<!--<div  class="pull-left" style="" ></div>-->
								<div style="clear:both"></div>
							</a>
						</li>
						
						
					</ul>
				</div>
				
				<div style="clear:both"></div>
				<div class="col-md-12" style="padding-left: 0px;    padding-right: 0px;">
					<div class="tab-content style-4" style="border-top:1px solid grey;background:white;height: 300px;overflow-y: scroll;">
						<div id="basicjobcontent" class="tab-pane fade">
								<div style="color:white;">
									
									
									<?php if(count($jobApplicationDetail)>0){ ?>
										<div style="background:#fff;color:black;display:block;" class="assessmentDetails">
									<?php }else{ ?>
										<div style="background:#fff;color:black;display:block;" class="assessmentDetails">
									<?php }?>
									
									
										<?php 
										$totalWrongAns = 0;
										$totalCorrectAns = 0; 
										if(count($jobApplicationDetail)>0){
											foreach($jobApplicationDetail as $assessment){
												$optionText   = $this->job_model->assessmentOptionText($assessment['userAnswer']);
											?>
											<div style="padding-top: 11px;padding-bottom: 11px;">
												<div class="col-md-10 question pull-left">Question:&nbsp;&nbsp;<?php echo $assessment['title'] ?></div>
												<div class="col-md-2 question pull-left">
													<?php if($assessment['userAnswer'] == $assessment['correctAnswer']){ 
														$totalCorrectAns++;
													?>
														<i class="fa fa-check-circle"  title="Applicant gave right answer" style="font-size: 23px;" aria-hidden="true"></i>
														<i class="fa fa-times-circle" title="Applicant gave right answer" style="font-size: 23px;color:lightgrey;" aria-hidden="true"></i>
														
													<?php }else{
														$totalWrongAns++;
														?>
														<i class="fa fa-check-circle" title="Applicant gave wrong answer" style="font-size: 23px;color:lightgrey;" aria-hidden="true"></i>
														<i class="fa fa-times-circle" title="Applicant gave wrong answer" style="font-size: 23px;" aria-hidden="true"></i>
													<?php } ?>
												</div>
												<div style="clear:both"></div>
												<div class="col-md-12 answer">
													<span>Answer:&nbsp;&nbsp;&nbsp;&nbsp;</span><?php echo $optionText[0]['option'] ?>
												</div>
												
												<div style="clear:both"></div>
											</div>
											<?php }?>
												<input type="hidden" name="assessmentCorrect" id="assessmentCorrect" value="<?php echo $totalCorrectAns ?>">
												<input type="hidden" name="assessmentWrong" id="assessmentWrong" value="<?php echo $totalWrongAns ?>">
												<input type="hidden" name="Totalassessment" id="Totalassessment" value="<?php echo count($jobApplicationDetail) ?>">
										<?php }else{						?>
										<div style="padding-top: 11px;padding-bottom: 11px;height:100px;">
											<div class="col-md-10 question pull-left">No Assessments Available.</div>
											<div class="col-md-2 question pull-left"></div>
											<div style="clear:both"></div>
											<div class="col-md-12 answer">
												<span></span>
											</div>
												<input type="hidden" name="assessmentCorrect" id="assessmentCorrect" value="<?php echo $totalCorrectAns ?>">
												<input type="hidden" name="assessmentWrong" id="assessmentWrong" value="<?php echo $totalWrongAns ?>">
												<input type="hidden" name="Totalassessment" id="Totalassessment" value="<?php echo count($jobApplicationDetail) ?>">

											<div style="clear:both"></div>
										</div>
										<?php } ?>
										
									</div>
								</div>
						</div>
						
						
							<div id="recruitersjobcontent" class="tab-pane fade active in">
								<?php
				
								$statictoken = 'ecbcd7eaee29848978134beeecdfbc7c';
								$base_url = base_url().'Fjapi';
								
								$json = array("token" => $statictoken, "methodname" => 'getUserJobQuestionsAnswers', 'jobId' => $currentJob, 'userId' => $currentUser);                                                                    
								$data_string = json_encode($json);                
								
								$ch = curl_init($base_url);                                                                      
								curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
								curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
								curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
								curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
								curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
									'Content-Type: application/json',                                                                                
									'Content-Length: ' . strlen($data_string))                                                                       
								);                                                                                                                  
																																					 
								$result = curl_exec($ch);
								curl_close ($ch);
								$allResult = json_decode($result);
								$questionsListDetails = $allResult->questionsList;
								
								?>
							<div style="background:#054F72;color:#000;">
								
								<div style="clear:both;"></div>
								<div style="background:#fff">
								<center style="    padding: 10px;"><b>Questions</b></center>
										<?php if($detailsForUserJob['mergedVideo'] != "" ){ ?>
												<div class="col-md-11 pull-left" style=" padding:10px;">
												<?php
												for($i = 1;$i<=count($questionsListDetails);$i++){
													if($i == 1){
														$QuestionIDCss = "background:#0091BA;color:white;";
													}else{
														$QuestionIDCss = "background:#ccc;color:black;";
													}
												?>
														<div title="<?php echo $questionsListDetails[0]->question  ?>" class="pull-left" style="margin-right: 10px;"><button class="btn questionIdList" id="questionIdList_<?php echo $i; ?>" onclick="changeInterviewQuestionNew(<?php echo $i?>)" style="<?php echo $QuestionIDCss; ?>   padding: 2px;padding-left: 10px;padding-right: 10px;border-radius: 0px;"><?php echo $i ?></button></div>
												<?php }
												?>
													<div style="clear:both;"></div>
												</div>
												<div class="col-md-1 pull-left" style="font-size:16px;    padding-right: 27px;">
													
													<a href="javascript:"  style="color:white;background:#054F72;   padding: 5px;padding-left: 10px;padding-right: 10px;border-radius: 0px;float:right;" data-toggle="modal" data-target="#shareInterview" class="btn share-user-interview" 
																	data-options='{"jobId":"<?php echo $currentJob; ?>", "userId":"<?php echo $currentUser; ?>", "userJob":"<?php echo $UjId; ?>", "appUserName":"<?php echo $detailsForUserJob['fullname'];?>", "interviewVideo":"<?php echo $detailsForUserJob['mergedVideo']; ?>","appUserJob":"<?php echo $currentJobName ?>"}'
																>Share</a>
													
												</div>
												<div style="clear:both;"></div>
												<?php 
												$counter = 1;
												foreach($questionsListDetails as $details){
													if($counter == 1){
														$questionCss = "display:block;";
													}else{
														$questionCss = "display:none;";
													}
													$counter++;
												?>
													<div>
														<div class="cold-md-12">
															<center>
																
																<div style="color:black;background:#ccc;width:50%; <?php echo $questionCss ?>   padding: 5px;"><?php echo $details->question ?></div>
															</center>
														</div>
														<div style="clear:both"></div>
													</div>
													
												<?php } ?> 
												
												
												<div style="padding:10px;"><center><b>Applicant's Response</b></center></div>
												<div style="clear:both;"></div>
												<div>
													<center>
														<video style="width: 634px;height: 295px;" src="https://s3.amazonaws.com/videoMerge/<?php echo $detailsForUserJob['mergedVideo']; ?>" controls></video>
													</center>
												</div>
										<?php }else{  ?>
										<div style="clear:both;"></div>
										<div class="col-md-11 pull-left" style=" padding:10px;">
											<?php
											for($i = 1;$i<=count($questionsListDetails);$i++){
												if($i == 1){
													$QuestionIDCss = "background:#0091BA;color:white;";
												}else{
													$QuestionIDCss = "background:#ccc;color:black;";
												}
											?>
													<div title="<?php echo $questionsListDetails[0]->question  ?>" class="pull-left" style="margin-right: 10px;"><button class="btn questionIdList" id="questionIdList_<?php echo $i; ?>" onclick="changeInterviewQuestionNew(<?php echo $i?>)" style="<?php echo $QuestionIDCss; ?>   padding: 2px;padding-left: 10px;padding-right: 10px;border-radius: 0px;"><?php echo $i ?></button></div>
											<?php }
											?>
											<div style="clear:both;"></div>
										</div>
										<div class="col-md-1 pull-left" style="font-size:16px;    padding-right: 27px;">
											
											<a href="javascript:"  style="color:white;background:#054F72;   padding: 5px;padding-left: 10px;padding-right: 10px;border-radius: 0px;float:right;" data-toggle="modal" data-target="#shareInterview" class="btn share-user-interview" 
															data-options='{"jobId":"<?php echo $currentJob; ?>", "userId":"<?php echo $currentUser; ?>", "userJob":"<?php echo $UjId; ?>", "appUserName":"<?php echo $detailsForUserJob['fullname'];?>", "interviewVideo":"<?php echo $detailsForUserJob['mergedVideo']; ?>","appUserJob":"<?php echo $currentJobName ?>"}'
														>Share</a>
											
										</div>
										<div style="clear:both;"></div>
										<input type="hidden" name="inputVideoLock" id="inputVideoLock" value="0">
										<select id="questionDropdown" style="display:none;color:black;background:#ccc;width:50%;    padding: 5px;">
										<?php
										$QuestionCounter = 1;
										foreach($questionsListDetails as $details){?>
											<option value="<?php echo $QuestionCounter++ ?>"><?php echo $details->question ?></option>
										<?php } ?>
										</select>
										<?php
										$counter = 1;
										foreach($questionsListDetails as $details){
											if($counter == 1){
												$questionCss = "display:block;";
											}else{
												$questionCss = "display:none;";
											}
											
										?>
										<div id="QuestionCounter_<?php echo $counter; ?>" class="QuestionList" style="<?php echo $questionCss ?> ">
											<div>
												<div class="cold-md-12">
													<center>
														
														<div style="color:black;background:#ccc;width:50%;    padding: 5px;"><?php echo $details->question ?></div>
														
													</center>
												</div>
												<div style="clear:both"></div>
											</div>
											<div style="padding:10px;"><center><b>Applicant's Response</b></center></div>
											<div>
												<center>
													<video class="auditionClassNew" id="auditionClassNew<?php echo $counter; ?>"  style="    width: 634px;height: 295px;" src="https://s3.amazonaws.com/fjinterviewanswers/<?php echo $details->answer ?>" controls></video>
												</center>
											</div>
										</div>
										<?php $counter++; } ?>
										<script>
										$(".auditionClassNew").on('pause',function(e){
											if($("#inputVideoLock").val() == 0){
												var lastValue = $('#questionDropdown option:last-child').val();
												var currentQuestn = $("#questionDropdown").val();
												if(lastValue != currentQuestn){
													var currentId = e.target.id;
													$("#QuestionCounter_"+currentQuestn).css("display","none");
													$("#questionIdList_"+currentQuestn).css("background","#ccc");
													$("#questionIdList_"+currentQuestn).css("color","#000");
													
													$('#questionDropdown option:selected').next().attr('selected', 'selected');
													var NextQuestn = $("#questionDropdown").val();
													
													$("#questionIdList_"+NextQuestn).css("background","#0091BA");
													$("#questionIdList_"+NextQuestn).css("color","#fff");
													
													$("#QuestionCounter_"+NextQuestn).css("display","block");
													console.log("playing");
													$("#auditionClassNew"+NextQuestn).trigger("play");
													
												}else{
													return false;
												}
											}

										})
										</script>
										<?php } ?>
									</div>
									
								</div>
							
							</div>
							<div id="interviewjobcontent" class="tab-pane fade">
								<?php  if($currentUser > 0){ ?>
									<?php 
										$feedbacks = $ci->userjob_model->interviewComment($UjId);
										$adminfeedbacks = $ci->userjob_model->adminComment($UjId);
										if(!empty($adminfeedbacks) && !empty($feedbacks)){
											$feedbacksAll = array_merge($feedbacks,$adminfeedbacks);	
										}else if(!empty($feedbacks)>0){
											$feedbacksAll = $feedbacks;	
										}else if(!empty($adminfeedbacks) > 0){
											$feedbacksAll = $adminfeedbacks;	
										}else{
											$feedbacksAll = array();
										}
										
										
										$noFeedback = 1;
											
									}else{
									$feedbacks = null;
									$noFeedback = 0;
									} ?>
								<div style="color:white;">
									<?php 
									$totalFeedBack = 0;
									$totalCountFeedback = 0;
									$count = 0;
									if(count($feedbacksAll)>0){   ?>
										<div style="background:#fff;color:black;" class="feedbackDetails">
											
										<?php 
										
										foreach($feedbacksAll  as $feedback){
											$totalFeedBack 		= $totalFeedBack + $feedback['rating'];
											$totalCountFeedback = $count++;
										?>
											<div style="padding-top: 11px;padding-bottom: 11px;    border-bottom: 1px solid black;">
												<div class="col-md-12 answer">
													<p>"<?php echo $feedback['comment']  ?>"</p>
												</div>
												
												<div style="clear:both"></div>
												<div class="col-md-12 rating">
													<fieldset class="rating">
														<input type="radio" id="star5" name="rating<?php echo $feedback['id'] ?>" value="5"   <?php echo ($feedback['rating'] == 5)?"checked":"" ?>  /><label class = "full" for="star5" title="Awesome - 5 stars"></label>
														<input type="radio" id="star4half" name="rating<?php echo $feedback['id'] ?>" value="4.5" <?php echo ($feedback['rating'] == 4.5)?"checked":"" ?> /><label class="half" for="star4half" title="Pretty good - 4.5 stars"></label>
														<input type="radio" id="star4" name="rating<?php echo $feedback['id'] ?>" value="4"  <?php echo ($feedback['rating'] == 4)?"checked":"" ?>  /><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
														<input type="radio" id="star3half" name="rating<?php echo $feedback['id'] ?>" value="3.5"  <?php echo ($feedback['rating'] == 3.5)?"checked":"" ?> /><label class="half" for="star3half" title="Meh - 3.5 stars"></label>
														<input type="radio" id="star3" name="rating<?php echo $feedback['id'] ?>" value="3"  <?php echo ($feedback['rating'] == 3)?"checked":"" ?>  /><label class = "full" for="star3" title="Meh - 3 stars"></label>
														<input type="radio" id="star2half" name="rating<?php echo $feedback['id'] ?>" value="2.5"  <?php echo ($feedback['rating'] == 2.5)?"checked":"" ?>  /><label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>
														<input type="radio" id="star2" name="rating<?php echo $feedback['id'] ?>" value="2"  <?php echo ($feedback['rating'] == 2)?"checked":"" ?>  /><label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
														<input type="radio" id="star1half" name="rating<?php echo $feedback['id'] ?>" value="1.5"  <?php echo ($feedback['rating'] == 1.5)?"checked":"" ?>  /><label class="half" for="star1half" title="Meh - 1.5 stars"></label>
														<input type="radio" id="star1" name="rating<?php echo $feedback['id'] ?>" value="1"  <?php echo ($feedback['rating'] == 1)?"checked":"" ?>  /><label class = "full" for="star1" title="Sucks big time - 1 star"></label>
														<input type="radio" id="starhalf" name="rating<?php echo $feedback['id'] ?>" value="0.5" /><label class="half" for="starhalf" title="Sucks big time - 0.5 stars"></label>
													</fieldset>
												</div>
												<div class="col-md-12 question">
												<?php
												if(isset($feedback['createdBy']) && $feedback['createdBy'] != ""){ ?>
												<?php }else{ ?>
													<div  class="pull-left" style="background:#27acff;padding:3px;">External</div>
												<?php }
												?>
												<?php echo '<div  class="pull-left">&nbsp;&nbsp;'.$feedback['firstName'].' '.$feedback['lastName'].' ('.$feedback['emailId'].') </div>'; ?>
												
												</div>
												<div class="col-md-12 answer">
													<span style="font-size: 12px;font-style: italic;"><?php echo date('d M, Y', strtotime($feedback['createdAt']));  ?></span>
												</div>
												<div style="clear:both"></div>
											</div>
											<?php } ?>
											<input type="hidden" name="totalFeedBackCal" id="totalFeedBackCal" value="<?php echo $totalFeedBack ?>">
											<input type="hidden" name="totalFeedBackCountCal" id="totalFeedBackCountCal" value="<?php echo $count ?>">
											</div>
											
										<?php }else{ 
												
										?>
											<div style="background:#fff;color:black; display:block;" class="feedbackDetails">
												<div style="height:100px;padding-top: 11px;padding-bottom: 11px;">
													
													<div style="clear:both;"></div>
													<center>No Feedback Available</center>
													<input type="hidden" name="totalFeedBackCal" id="totalFeedBackCal" value="<?php echo $totalFeedBack ?>">
													<input type="hidden" name="totalFeedBackCountCal" id="totalFeedBackCountCal" value="<?php echo $totalCountFeedback ?>">
												</div>
											</div>
												<?php 
							} ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /#page-content-wrapper -->


<!-- PopUp for view share's profile  -->
<div id="videoAudition" class="modal fade" role="dialog">
    <div class="modal-dialog" style="margin-top:7%;">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="    border-bottom: 1px solid #000;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4><?php echo $detailsForUserJob['fullname']." Video Resume"  ?></h4>
            </div>
            <div class="modal-body" >                                      
                <?php
				
				if(count($VideoresumeList) > 0) { ?>
           
					<div id="page-content-wrapper">
						<div class="container-fluid whitebg">
							<div class="row">
							
								<div class="col-md-12 padd_tp_bt_20">
								<?php if($VideoresumeList[0]->auditionFiles == "" ){ ?>
									<div class="col-md-12 text-center pull-right">
										<?php
										$url = 'https://s3.amazonaws.com/fjauditionsmergevideo/';
										if($VideoresumeList[0]->audition_file != ""){ ?>
											
											<div id="auditio">
												<video  class="auditionClass"  id="auditionVideo_<?php echo $count; ?>" style="width:400px;" src='<?php echo $url.$VideoresumeList[0]->audition_file ?>' controls>
											</div>
										<?php }else{
										?>
											<div id="auditio">
												Sorry No Video Resume Available.
											</div>
										<?php } ?>
											
									
									</div>
								<?php }else{ ?>
								<div class="col-md-12 text-center pull-right">
									Questions: <select class="selectpicker" id="questionDropdown" onchange="showAuditionForQuestion()" style="color:black;    margin-top: 20px;margin-bottom: 27px;width: 263px;">
													<option value="0">Tell us something about yourself?</option>
													<option value="1">Tell us about your strengths?</option>
													<option value="2">Tell us about your weakness?</option>
													<option value="3">Tell us about any of your project/work that you are proud of?</option>
												</select>
								</div>
								<div class="col-md-12 text-center pull-right">
										<?php
										
										$auditionsVideos = $VideoresumeList[0];
										$auditionArray = array_filter(explode(",", $auditionsVideos->auditionFiles));
										
										if(!empty($auditionArray)){
											for($count = 0;$count<=count($auditionArray);$count++){?>
												<?php
												$url = 'https://s3.amazonaws.com/fjauditions/';
												if($count == 0){ ?>
													
													<div id="audition_<?php echo $count ?>" style="display:block;">
														<video class="auditionClass" id="auditionVideo_<?php echo $count; ?>" style="width:400px;" src='<?php echo $url.trim($auditionArray[$count]) ?>' controls>
													</div>
												<?php }else{
												?>
													<div id="audition_<?php echo $count ?>" style="display:none">
														<video class="auditionClass"  id="auditionVideo_<?php echo $count; ?>" style="width:400px;"  src='<?php echo $url.trim($auditionArray[$count]) ?>' controls>
													</div>
												<?php } ?>
											<?php }
										
										}
										?>
										
									
									</div>
								<?php } ?>
								</div>
							</div>
						</div>
					</div>


            <!-- /.row -->

			<?php }else{
				echo "<div style='clear:both'></div><center style='    margin-top: 11%;' id='auditionNotAvaialble'>Sorry No Video Resume Available.</center>";
				
			} ?>
			</div>
        </div>
    </div>
</div>






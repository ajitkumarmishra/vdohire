<?php
$ci =&get_instance();
                    //job user invitation model
$ci->load->model('jobuserinvite_model');
$ci->load->helper('fj');
$currentJob = 0;
$currentUser = 0;
if(isset($_GET['JobId']) && $_GET['JobId'] != "" ){
	$currentJob = $_GET['JobId'];
	$currentJob = decryptURLparam($currentJob, URLparamKey);
}

if(isset($_GET['userId']) && $_GET['userId'] != "" && $_GET['userId'] != 0){
	$currentUser = $_GET['userId'];
	$currentUser = decryptURLparam($currentUser, URLparamKey);
}


?>
<style>
	@import url(//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css);
	.rating { 
	border: none;
	float: left;
	}

	.rating > input { display: none; } 
	.rating > label:before { 
	margin: 2px;
	font-size: 15px;
	font-family: FontAwesome;
	display: inline-block;
	content: "\f005";
	}

	.rating > .half:before { 
	content: "\f089";
	position: absolute;
	}

	.rating > label { 
	color: #ddd; 
	float: right; 
	}

	/***** CSS Magic to Highlight Stars on Hover *****/

	.rating > input:checked ~ label, /* show gold star when clicked */
	.rating:not(:checked) > label:hover, /* hover current star */
	.rating:not(:checked) > label:hover ~ label { color: #FFD700;  } /* hover previous stars in list */

	.rating > input:checked + label:hover, /* hover current star when changing rating */
	.rating > input:checked ~ label:hover,
	.rating > label:hover ~ input:checked ~ label, /* lighten current selection */
	.rating > input:checked ~ label:hover ~ label { color: #FFED85;  }
	
	
	
.style-4::-webkit-scrollbar-track
{
	-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
	background-color: #F5F5F5;
}

.style-4::-webkit-scrollbar
{
	width: 10px;
	background-color: #F5F5F5;
}

.style-4::-webkit-scrollbar-thumb
{
	background-color: #000000;
	border: 2px solid #555555;
}

#myBtn {
  display: block;
  position: fixed;
  top: 58px;
  right: 42%;
  z-index: 99;
  border: none;
  outline: none;
  background-color: #054F72;
  color: white;
  cursor: pointer;
  padding:7px;
  border-radius: 10px;
}

#myBtnDown {
  display: block;
  position: fixed;
  bottom: 58px;
  right: 42%;
  z-index: 99;
  border: none;
  outline: none;
  background-color: #054F72;
  color: white;
  cursor: pointer;
  padding:7px;
  border-radius: 10px;
}
.nav-tabs>li{
	    margin-bottom: -3px;
}

input[type=checkbox].css-checkbox {
    position: absolute;
    overflow: hidden;
    clip: rect(0 0 0 0);
    height: 1px;
    width: 1px;
    margin: -1px;
    padding: 0;
    border: 0;
}
input[type=checkbox].css-checkbox.sme:checked + label.css-label.sme {
    background-position: 0 -16px;
}
input[type=checkbox].css-checkbox.sme + label.css-label.sme {
    padding-left: 18px;
    height: 16px;
    display: inline-block;
    line-height: 16px;
    background-repeat: no-repeat;
    background-position: 0 0;
    font-size: 15px;
    vertical-align: middle;
    cursor: pointer;
}
.depressed {
    background-image: url(http://35.154.53.72/admin/theme/images/depressed.png);
}
</style>


<div id="page-content-wrapper" style="margin-top: 52px;background:#eeeeee;">
    
<div style="display:none;">
	<button  onclick="movetoNext()" id="myBtn" title="Go to top"><i class="fa fa-arrow-up" aria-hidden="true"></i></button>
</div>
<div style="display:none;">
	<button  onclick="movetoPrevious()" id="myBtnDown" title="Go to top"><i class="fa fa-arrow-down" aria-hidden="true"></i></button>
</div>


    <div class="container-fluid" style="padding-left:10px;padding-right:10px;">
        <div class="row">
            <div class="col-md-3" style="padding-left: 0px;padding-right: 0px;">
				<div style="background:#FFF;padding-top:7px;    box-shadow: 0.2px 0.2px 1px 1px;    margin-top: 10px;padding-bottom: 7px;    height: 136px;">
					<center>
					<?php 
					
							$i = 0;
							$j = 0;
							$totalcount = 0;
							$newContent = array();
							if(!isset($_GET['JobId']) && empty($_GET['JobId']) && $currentJob == 0){
								foreach($content as $jobs){
										//echo "<pre>";print_r($jobs);die;
										$UserJobRole = $ci->userjob_model->getUserJobPanel($jobs->id,$AdminLoggedInuserId);
										
										if($UserJobRole) {
											if($UserJobRole[0]['level'] == 1){
												$userJobRoleFor = 4;
											}else if($UserJobRole[0]['level'] == 2){
												$userJobRoleFor = 5;
											}else{
												$userJobRoleFor = 2;
											}
										} else {
											$userJobRoleFor = 2;
										}
										
										$userjobpendingcountres     = $ci->userjob_model->countForUserJobs($jobs->id, 1);
										$userjobpendingcount        = $userjobpendingcountres['appliedCount'];
										
										$userjobNewcountres     	= $ci->userjob_model->countForUserJobsNew($jobs->id, 1);   
										$userjobNewCount    		= $userjobNewcountres['appliedCount'];   
														  
										$userjobshortlistedcountres = $ci->userjob_model->countForUserJobs($jobs->id, 2);   
										$userjobshortlistedcount    = $userjobshortlistedcountres['appliedCount'];                    
										
										$userjobrejectedcountres    = $ci->userjob_model->countForUserJobs($jobs->id, 3);   
										$userjobrejectedcount       = $userjobrejectedcountres['appliedCount'];
										
										$totalstatuscount           = $userjobNewCount+$userjobpendingcount+$userjobshortlistedcount+$userjobrejectedcount;
										$DefaultJob = $userjobNewCount+$userjobpendingcount;
										
										if($DefaultJob > $totalcount){
											$currentJob = $jobs->id;
											$currentJobName = $jobs->title;
											$totalcount = $DefaultJob;
										}
										if(isset($_GET['hide']) && $_GET['hide'] == 1 &&  $totalstatuscount > 0 ){
											$newContent[$j]['id'] = $jobs->id;
											$newContent[$j]['jobId'] = $jobs->jobId;
											$newContent[$j]['fjCode'] = $jobs->fjCode;
											$newContent[$j]['title'] = $jobs->title;
											$newContent[$j]['openTill'] = $jobs->openTill;
											$newContent[$j]['assessment'] = $jobs->assessment;
											$newContent[$j]['interview'] = $jobs->interview;
											$newContent[$j]['applicationsReceived'] = $totalstatuscount;
											$newContent[$j]['newPending'] = $userjobNewCount+$userjobpendingcount;
											$j++;
										}else if(isset($_GET['hide']) && $_GET['hide'] == 0 ){
											$newContent[$j]['id'] = $jobs->id;
											$newContent[$j]['jobId'] = $jobs->jobId;
											$newContent[$j]['fjCode'] = $jobs->fjCode;
											$newContent[$j]['title'] = $jobs->title;
											$newContent[$j]['openTill'] = $jobs->openTill;
											$newContent[$j]['assessment'] = $jobs->assessment;
											$newContent[$j]['interview'] = $jobs->interview;
											$newContent[$j]['applicationsReceived'] = $totalstatuscount;
											$newContent[$j]['newPending'] = $userjobNewCount+$userjobpendingcount;
											$j++;
										}else if(!isset($_GET['hide']) && empty($_GET['hide']) &&  $totalstatuscount > 0 ){
											$newContent[$j]['id'] = $jobs->id;
											$newContent[$j]['jobId'] = $jobs->jobId;
											$newContent[$j]['fjCode'] = $jobs->fjCode;
											$newContent[$j]['title'] = $jobs->title;
											$newContent[$j]['openTill'] = $jobs->openTill;
											$newContent[$j]['assessment'] = $jobs->assessment;
											$newContent[$j]['interview'] = $jobs->interview;
											$newContent[$j]['applicationsReceived'] = $totalstatuscount;
											$newContent[$j]['newPending'] = $userjobNewCount+$userjobpendingcount;
											$j++;
										}
									
									
									$i++;
								}
							}else if(isset($_GET['JobId']) && $_GET['JobId'] != "" && $currentJob >= 0){
								foreach($content as $jobs){
										//echo "<pre>";print_r($jobs);die;
										$userjobpendingcountres     = $ci->userjob_model->countForUserJobs($jobs->id, 1);
										$userjobpendingcount        = $userjobpendingcountres['appliedCount'];
										
										$userjobNewcountres     	= $ci->userjob_model->countForUserJobsNew($jobs->id, 1);   
										$userjobNewCount    		= $userjobNewcountres['appliedCount'];   
														  
										$userjobshortlistedcountres = $ci->userjob_model->countForUserJobs($jobs->id, 2);   
										$userjobshortlistedcount    = $userjobshortlistedcountres['appliedCount'];                    
										
										$userjobrejectedcountres    = $ci->userjob_model->countForUserJobs($jobs->id, 3);   
										$userjobrejectedcount       = $userjobrejectedcountres['appliedCount'];
										
										$totalstatuscount           = $userjobNewCount+$userjobpendingcount+$userjobshortlistedcount+$userjobrejectedcount;
										
										
										
										if(isset($_GET['hide']) && $_GET['hide'] == 1 &&  $totalstatuscount > 0 ){
											$newContent[$j]['id'] = $jobs->id;
											$newContent[$j]['jobId'] = $jobs->jobId;
											$newContent[$j]['fjCode'] = $jobs->fjCode;
											$newContent[$j]['title'] = $jobs->title;
											$newContent[$j]['openTill'] = $jobs->openTill;
											$newContent[$j]['assessment'] = $jobs->assessment;
											$newContent[$j]['interview'] = $jobs->interview;
											$newContent[$j]['applicationsReceived'] = $totalstatuscount;
											$newContent[$j]['newPending'] = $userjobNewCount+$userjobpendingcount;
											$j++;
										}else if(isset($_GET['hide']) && $_GET['hide'] == 0 ){
											$newContent[$j]['id'] = $jobs->id;
											$newContent[$j]['jobId'] = $jobs->jobId;
											$newContent[$j]['fjCode'] = $jobs->fjCode;
											$newContent[$j]['title'] = $jobs->title;
											$newContent[$j]['openTill'] = $jobs->openTill;
											$newContent[$j]['assessment'] = $jobs->assessment;
											$newContent[$j]['interview'] = $jobs->interview;
											$newContent[$j]['applicationsReceived'] = $totalstatuscount;
											$newContent[$j]['newPending'] = $userjobNewCount+$userjobpendingcount;
											$j++;
										}else if(!isset($_GET['hide']) && $_GET['hide'] == "" &&  $totalstatuscount > 0 ){
											$newContent[$j]['id'] = $jobs->id;
											$newContent[$j]['jobId'] = $jobs->jobId;
											$newContent[$j]['fjCode'] = $jobs->fjCode;
											$newContent[$j]['title'] = $jobs->title;
											$newContent[$j]['openTill'] = $jobs->openTill;
											$newContent[$j]['assessment'] = $jobs->assessment;
											$newContent[$j]['interview'] = $jobs->interview;
											$newContent[$j]['applicationsReceived'] = $totalstatuscount;
											$newContent[$j]['newPending'] = $userjobNewCount+$userjobpendingcount;
											$j++;
										}
									
									
									$i++;
								}
							}
							
							?>
					<div class="col-md-12 pull-left" style="padding-left:0px;    padding-left: 7px;padding-right: 7px;">
						<div style="padding-bottom: 12px;color:black;font-size: 18px;">
							<div>
						
								<div class="dropdown" style="width: 95%;">Evaluating Applications for &nbsp;Past Jobs&nbsp;&nbsp;
								<i class="fa fa-caret-down" aria-hidden="true" style="cursor:pointer;"  class="dropdown-toggle" type="button" data-toggle="dropdown"></i>
		
								&nbsp;
									<ul class="dropdown-menu" style="left:48%">
									  <li><a href="<?php echo base_url()."queue/list" ?>">Active Jobs</a></li>
									  <li><a href="<?php echo base_url()."queue/pastList"; ?>">Past Jobs</a></li>
									</ul>
								</div>
							</div>
						</div>
						<div>
							
							<select title='' name="JobList" class="form-control" onchange="changeJobId()" id="JobListdropdown" style="border-radius: 0px;border-color: #B5B5B5;width:78%;;color:black !important;    height: 37px;font-size: 18px;    font-weight: bold;">
								<?php 
								
								$i = 0;
								foreach($newContent as $jobs){
									if(isset($_GET['JobId']) && $_GET['JobId'] > 0 && $currentJob == $jobs->id){
									$currentJob = $jobs['id'];
										$currentJobName = $jobs['title'];
									}else if(!isset($_GET['JobId']) && $_GET['JobId'] == "" && $currentJob > 0){
										
									}
									$i++;
								?>
								<option title="This job has <?php echo $jobs['applicationsReceived']  ?> Received Applications and <?php echo $jobs['newPending'] ?> Unprocessed (New & Pending) Applications" value="<?php  echo encryptURLparam($jobs['id'], URLparamKey); ?>" <?php echo ($currentJob == $jobs['id'])?"selected ":"";  ?>><?php echo $jobs['title']  ?> (<?php echo $jobs['applicationsReceived']."/".$jobs['newPending']  ?>)</option>
								<?php } ?>
							</select>
							
						</div>
						<div style="color:#000;    padding-top: 10px;">
							<input type="checkbox" name="hideJobs" id="hideJobs" onclick="hideJobs('<?php echo isset($_GET['JobId']) ? $_GET['JobId'] : '' ; ?>')" <?php if(isset($_GET['hide']) && $_GET['hide']== 1){ echo "checked"; } else if(!isset($_GET['hide']) && empty($_GET['hide']) ){ echo "checked"; } ?>>&nbsp;&nbsp;Hide jobs with no applications
						</div>
					</div>
		
	

					<div style="clear:both"></div>
					</center>
				
				</div>
				<div style="background:#fff;     box-shadow: 0.2px 0.2px 1px 1px;   margin-top: 10px;  padding-top: 10px;    padding-bottom: 10px;   padding-left: 0px;padding-right: 0px;height:126px;">
					<div class="col-md-12" style="    padding-left: 0px;padding-right: 0px;">
						<div class="col-md-12">
							<center>
							<div style="font-size:16px;">Applications</div>
							</center>
							
						</div>
						<?php
						// function to get count of pending user's job
						$userjobpendingcountres     = $ci->userjob_model->countForUserJobs($currentJob, 1);
						$userjobpendingcount        = $userjobpendingcountres['appliedCount'];  
						
						$userjobNewcountres     	= $ci->userjob_model->countForUserJobsNew($currentJob, 1);   
						$userjobNewCount    		= $userjobNewcountres['appliedCount'];   
										  
						// function to get count of shortlisted user's job
						$userjobshortlistedcountres = $ci->userjob_model->countForUserJobs($currentJob, 2);   
						$userjobshortlistedcount    = $userjobshortlistedcountres['appliedCount'];                    
						// function to get count of rejected user's job
						$userjobrejectedcountres    = $ci->userjob_model->countForUserJobs($currentJob, 3);   
						$userjobrejectedcount       = $userjobrejectedcountres['appliedCount'];                    
						$totalstatuscount           = $userjobNewCount+$userjobpendingcount+$userjobshortlistedcount+$userjobrejectedcount;
						$filterget = "";
						$filterValues = "";
						$filterArray = array(1,2,4,3,5,6);
						$showListingFilter = array(0,1,2,4);
						if(isset($_GET['filter']) && $_GET['filter'] != ""){
							$filterget = $_GET['filter'];
							$filterArray = array_unique(array_filter(explode(",",$filterget)));
							$filterValues = implode(",",$filterArray);
						}else{
							$filterValues = implode(",",$filterArray);
						}
						
						?>
		<div class="col-md-3 pull-left" style="width:14%;     margin-top: 0px;">
		</div>
		<div class="col-md-6 pull-left" style="width:60%;padding-top:10px;padding-bottom:5px;">
			<div  onclick="filterResultbycheck('allreceivedId')"   title="Applications Received for <?php echo !empty($currentJobName) ? $currentJobName : ''; ?>" style="background:rgb(220, 148, 41);color:white;  padding-left: 0px;cursor:pointer;padding-right: 0px;    font-size: 18px;">
				<div style=" padding-left: 0px;">
				<div style="">
					<div class="col-md-11 pull-left" style="    width: 86%;font-size: 21px;padding-left: 4px;padding-right:0px;">
					<center style="    padding-left: 19px;"><?php echo $totalstatuscount ?></center>
					</div>
					<div class="pull-right" style="    font-size: 21px;padding-left: 0px;    padding-right: 0px;">
						<input id="allreceivedId" class="css-checkbox sme" style="position: absolute;margin-top: 0px;"  type="checkbox" name="filtervalue" onclick="filterResultbycheck('allreceivedId')" value="6" <?php if(in_array("6", $showListingFilter)){ echo "checked"; } ?>>
							<label for="checkbox_n02" name="checkbox_n02_lbl" class="css-label sme depressed" style="
"></label>
					</div>
					<div style="clear:both"></div>
				</div>
				<div style="clear:both"></div>
				</div>
			</div>
			<div class="col-md-12" style="padding-left: 0px;padding-right: 0px;padding-top: 5px;">
		
				<table style="width:100%;">
						<tr>
						<td   onclick="filterResultbycheck('newId')"  title="New Applications Received for <?php echo !empty($currentJobName) ? $currentJobName : '' ; ?>" style="background:#27acff;color:white;width:25%;cursor:pointer;    padding-left: 0px;padding-right: 0px;    font-size: 18px;">
							<div style=" padding-left: 0px;">
							<div style="padding-right: 0px;">
								<div class="pull-left" style="    font-size: 21px;padding-left: 4px;"><?php echo $userjobNewCount ?></div>
								<div class="pull-right" style="    font-size: 21px;">
									<input id="newId" class="css-checkbox sme"  style="position: absolute;margin-top: 0px;"  type="checkbox" name="filtervalue" onclick="filterResultbycheck('newId')" value="0" <?php if(in_array("0", $showListingFilter)){ echo "checked"; } ?>>
									<label for="checkbox_n02" name="checkbox_n02_lbl" class="css-label sme depressed" style="
"></label>
								</div>
								<div style="clear:both"></div>
							</div>
							<div style="clear:both"></div>
							</div>
						</td>
						<td  onclick="filterResultbycheck('pendingId')"   title="Applications Pending for <?php echo !empty($currentJobName) ? $currentJobName : '' ; ?>"  style="background:#b981cc;color:white;width:25%;cursor:pointer;    padding-left: 0px;padding-right: 0px;   font-size: 18px;">
							<div style=" padding-left: 0px;">
							<div style="padding-right: 0px;">
								<div class="pull-left" style="  font-size: 21px;padding-left: 4px;"><?php echo $userjobpendingcount ?></div>
								<div class="pull-right" style="    font-size: 21px;">
									<input id="pendingId" class="css-checkbox sme"   style="position: absolute;margin-top: 0px;"  type="checkbox" name="filtervalue" onclick="filterResultbycheck('pendingId')" value="1"  <?php if(in_array("1", $showListingFilter)){ echo "checked"; } ?>>
									<label for="checkbox_n02" name="checkbox_n02_lbl" class="css-label sme depressed" style="
"></label>
								</div>
								<div style="clear:both"></div>
								
							</div>
							<div style="clear:both"></div>
							</div>
							
						</td>
						<td  onclick="filterResultbycheck('shortlistedId')"  title="Applications Shortlisted for <?php echo !empty($currentJobName) ? $currentJobName : '' ; ?>"   style="background:#5CC36A;color:white;width:25%;cursor:pointer;    padding-left: 0px;padding-right: 0px;        font-size: 18px;">
							<div style=" padding-left: 0px;">
							<div style="padding-right: 0px;">
								<div class="pull-left" style=" font-size: 21px;padding-left: 4px;"><?php echo $userjobshortlistedcount ?></div>
								<div class="pull-right" style="    font-size: 21px;">
									<input id="shortlistedId" class="css-checkbox sme"  style="position: absolute;margin-top: 0px;"  type="checkbox" name="filtervalue" onclick="filterResultbycheck('shortlistedId')" value="2"  <?php if(in_array("2", $showListingFilter)){ echo "checked"; } ?>>
									<label for="checkbox_n02" name="checkbox_n02_lbl" class="css-label sme depressed" style="
"></label>
								</div>
								<div style="clear:both"></div>
								
							</div>
							<div style="clear:both"></div>
							</div>
							
						</td>
				
						<td  onclick="filterResultbycheck('rejectedId')"  title="Applications Rejected for <?php echo !empty($currentJobName) ? $currentJobName : '' ; ?>"   style="background:#EE6E3D;color:white; width:25%;cursor:pointer;   padding-left: 0px;padding-right: 0px;    font-size: 18px;">
							<div style=" padding-left: 0px;">
							<div style="padding-right: 0px;">
								<div class="pull-left" style=" font-size: 21px;padding-left: 4px;"><?php echo $userjobrejectedcount ?></div>
								<div class="pull-right" style="    font-size: 21px;">
									<input id="rejectedId" class="css-checkbox sme" style="position: absolute;margin-top: 0px;" type="checkbox" name="filtervalue" onclick="filterResultbycheck('rejectedId')" value="3"  <?php if(in_array("3", $showListingFilter)){ echo "checked"; } ?>>
									<label for="checkbox_n02" name="checkbox_n02_lbl" class="css-label sme depressed" style="
"></label>
								</div>
								<div style="clear:both"></div>
								
							</div>
							<div style="clear:both"></div>
							</div>
							
						</td>
					</tr>
				</table>
				<div style="clear:both"></div>
			</div>
		</div>
		<div class="col-md-3 pull-left" style="    margin-top: 0px;padding-left: 0px;padding-right: 0px;">
			<div class="pull-left"  style="color:white;width:50%;margin-top: 13px;">
					<a href="javascript://" style="color:#000;"   name="Reset" onclick="unselectAll()" value="Reset" ><u>Reset</u></a>
			</div>
			<div class="pull-left" style="color:white;width:50%;margin-top: 13px;">
					<a href="javascript://" onclick="showFilter()" style="color:#000;"   ><u style="    float: left;padding-right: 5px;">Filter</u><i class="fa fa-arrow-down" aria-hidden="true"></i><div style="clear:both"></div></a>
	
			</div>
			<div style="clear:both"></div>
			
			
		</div>
		<div style="clear:both"></div>
		<div class="col-md-12"  style="padding-left: 15px;    padding-right: 0px;">
			<div style="width:20%;float:left;">
			<div style="    font-size: 11px;float: left;width: 13%;height: 12px;background: rgb(220, 148, 41);"></div>
			<div style="    font-size: 11px;float: left;width: 10%;padding-left: -5px;font-weight: 100;padding-right: 15px;margin-right: 4px;">&nbsp;&nbsp;All
			</div><div style="clear:both"></div>
			</div>
			
			<div  style="width:20%;float:left;">
			<div style="    font-size: 11px;float: left;width: 13%;height: 12px;background: #27acff;"></div>
			<div style="    font-size: 11px;float: left;width: 10%;padding-left: -5px;font-weight: 100;padding-right: 15px;margin-right: 4px;">&nbsp;&nbsp;New
			</div><div style="clear:both"></div>
			</div>
			<div  style="width:20%;float:left;">
			<div style="    font-size: 11px;float: left;width: 13%;height: 12px;background:#b981cc;"></div>
			<div style="    font-size: 11px;float: left;width: 10%;padding-left: -5px;font-weight: 100;padding-right: 15px;margin-right: 4px;">&nbsp;&nbsp;Pending
			</div><div style="clear:both"></div>
			</div>
			
			<div  style="width:20%;float:left;">
			<div style="    font-size: 11px;float: left;width: 13%;height: 12px;background:#5CC36A;"></div>
			<div style="    font-size: 11px;float: left;width: 14%;padding-left: -5px;font-weight: 100;padding-right: 15px;margin-right: 4px;">&nbsp;&nbsp;Shortlisted
			</div><div style="clear:both"></div>
			</div>
			
			<div  style="width:20%;float:left;">
			<div style="    font-size: 11px;float: left;width: 13%;height: 12px;background:#EE6E3D;"></div>
			<div style="    font-size: 11px;float: left;width: 14%;padding-left: -5px;font-weight: 100;padding-right: 15px;">&nbsp;&nbsp;Rejected
			</div><div style="clear:both"></div>
			</div>
				
				<div style="clear:both"></div>
		</div>
		<div class="col-md-12 filterDiv" id="filterPopup" style="    box-shadow: 2px 2px 2px 2px;z-index:1;display:none;    position: absolute;padding-left: 0px;width: 100%;background: white;padding: 10px;">
		
		
		
		<div class="col-md-6 pull-left">
			<div class="col-md-12">
				<h3>Filter</h3>
			</div>
			<div class="radio">
			  <label><input type="radio" name="filterBy"  id="filterBy" value="5" <?php echo (isset($_GET['filterBy']) && ($_GET['filterBy'] == 5))?"checked":"" ?>>With Resume</label>
			</div>
			<div class="radio">
			  <label><input type="radio" name="filterBy" id="filterBy" value="6" <?php echo (isset($_GET['filterBy']) && ($_GET['filterBy'] == 6)) ? "checked":"" ?>>With Video Resume</label>
			</div>
			<div class="radio">
			  <label><input type="radio" name="filterBy" id="filterBy" value="7" <?php echo (isset($_GET['filterBy']) && ($_GET['filterBy'] == 7)) ? "checked":"" ?>>With Resume & Video Resume</label>
			</div>
			<div class="radio">
			  <label><input type="radio" onclick="interviewFeedbackAbove()" name="filterBy" id="filterBy" value="8" <?php echo (isset($_GET['filterBy']) && ($_GET['filterBy'] == 8)) ? "checked":"" ?>>Interview Feedback Score above </label>
			  <?php
			  $interviewValue = " display:none; ";
			  if($_GET['interviewValue'] != ""){
				  $interviewValue = " display:block; ";
			  }
			  ?>
			  
			  <div  id="interviewFeedbackAbove" style="<?php echo $interviewValue ?>"><input type="text" name="interviewFeedbackAbove" id="interviewValue" class="form-control" Placeholder="Enter minimum value required" value="<?php echo isset($_GET['interviewValue']) ? $_GET['interviewValue'] : '' ; ?>"></div>
			</div>
			
			<div class="radio">
			  <label><input type="radio" onclick="assessmentFeedbackAbove()" name="filterBy" id="filterBy" value="9" <?php echo ((isset($_GET['filterBy']) && $_GET['filterBy'] == 9)) ? "checked":"" ?>>Assessment Score above</label>
			  <?php
			  $assessmentValue = " display:none; ";
			  if(isset($_GET['assessmentValue']) && !empty($_GET['assessmentValue'])){
				  $assessmentValue = " display:block; ";
			  }
			  ?>
			  
			  <div  id="assessmentFeedbackAbove" style="<?php echo $assessmentValue; ?>"><input type="text" name="assessmentFeedbackAbove" id="assessmentValue" class="form-control" Placeholder="Enter minimum value required" value="<?php echo isset($_GET['assessmentValue']) ? $_GET['assessmentValue'] : '' ; ?>" ></div>
			</div>
			<div class="radio">
			  <label><input type="radio" onclick="locationPrefred()" name="filterBy" id="filterBy" value="10" <?php echo (isset($_GET['filterBy']) && ($_GET['filterBy'] == 10)) ? "checked":"" ?>>Location</label>
			  <?php
			  $locationPrefered = " display:none; ";
			  if(isset($_GET['locationPrefered']) && !empty($_GET['locationPrefered'])) {
				  $locationPrefered = " display:block; ";
			  }
			  ?>
			  <div  id="locationPrefred" style="<?php echo $locationPrefered ?>"><input type="text" name="locationPrefered" id="locationPrefered" class="form-control" Placeholder="Enter minimum value required" value="<?php echo isset($_GET['locationPrefered']) ? $_GET['locationPrefered'] : '' ; ?>"></div>
			  
			</div>
			
		</div>
		<div class="col-md-6 pull-right">
			<div class="col-md-12">
				<h3>Sort By</h3>
			</div>
			<div class="radio">
			  <label><input type="radio" name="sortBy" id="sortBy"  value="1" <?php echo (isset($_GET['sort']) && ($_GET['sort'] == 1)) ? "checked":"" ?>>Sort by Date Asc</label>
			</div>
			<div class="radio">
			  <label><input type="radio" name="sortBy" id="sortBy"  value="2" <?php echo (isset($_GET['sort']) && ($_GET['sort'] == 2)) ? "checked":"" ?>>Sort by Date Desc</label>
			</div>
			<div class="radio">
			  <label><input type="radio" name="sortBy" id="sortBy" value="3" <?php echo (isset($_GET['sort']) && ($_GET['sort'] == 3)) ? "checked":"" ?>>Sort by Name Asc</label>
			</div>
			<div class="radio">
			  <label><input type="radio" name="sortBy" id="sortBy" value="4" <?php echo (isset($_GET['sort']) && ($_GET['sort'] == 4)) ? "checked":"" ?>>Sort by Name Desc</label>
			</div>
			
			
		</div>
		<div style="clear:both"></div>
		<div class="col-md-12" style="padding: 10px;border-top: 1px solid grey;">
			<div class="col-md-6 pull-left">
				<button style="background: #054f72 none repeat scroll 0 0;color: white;" onclick="changeSortByOrder('<?php echo isset($_GET['JobId']) ? $_GET['JobId'] : ''; ?>');" class="btn">Apply</button>
			</div>
			<div class="col-md-6 pull-left">
				<button style="background: #054f72 none repeat scroll 0 0;color: white;"  onclick="resetFilterPopup()" class="btn">Reset</button>
			</div>
			<div style="clear:both"></div>
		</div>
	</div>
    </div>
	
					
	<div style="clear:both"></div>
				
				</div>
				<div style="    margin-top: 10px;    box-shadow: 0.2px 0.2px 1px 1px;">
					
					<?php
					$j = 0;
					if(isset($_GET['sort']) && $_GET['sort'] > 0){
						$sorting = $_GET['sort']; 
					}else{
						$sorting = 0; 
					}
					if(isset($_GET['filterBy']) && $_GET['filterBy'] > 0){
						$filterBy = $_GET['filterBy']; 
					}else{
						$filterBy = 0; 
					}
					
					//foreach($content as $item){
						$CurrentUserJobRole = $ci->userjob_model->getUserJobPanel($currentJob,$AdminLoggedInuserId);
						if(empty($CurrentUserJobRole)){
							$currentAdminRole = 2;
						}else{
							$currentAdminRole = 3;
						}
						$userlistofjob = $ci->userjob_model->listForUserJobsNew($currentJob,$sorting,$filterValues,$filterBy,$currentAdminRole);  
						//echo "<pre>";print_r($filterBy);die;
							//echo $currentJob."--".$item->id;
						$displyCss = "display:block;";
							
						$j++;
						$ApplicantsjsonArray = array()
							//$encryptedJobId = encryptURLparam($item->id, URLparamKey);
						?>
						<ul class="userList_All style-4" id="userList_<?php echo $currentJob ?>" style="<?php echo $displyCss; ?> background:#fff;  padding-left: 0px;    height: 300px;overflow-y: scroll;">
							<?php
							if(!empty($userlistofjob)){
							$i = 0;
							$filterCurrentUser = 0;
							
							foreach($userlistofjob as $user){
								
								
									if(isset($_GET['userId']) && $_GET['userId'] != "" && $currentUser == $user['userId']){
										$currentUser = $user['userId'];
										$UjId = $user['id'];
										$mergedVideo = $user['mergedVideo'];
										$Resume = ($user['jobResume'] != "")?$user['jobResume']:$user['resume_path'];
										$liCss = "background:none;color:black;font-weight:bold;";
										$headerBacground = "background:#054F72;color:white;";
										
									}else if($i == 0 && !isset($_GET['userId'])){
										$currentUser = $user['userId'];
										$UjId = $user['id'];
										$mergedVideo = $user['mergedVideo'];
										$Resume = ($user['jobResume'] != "")?$user['jobResume']:$user['resume_path'];
										$liCss = "background:none;color:black;font-weight:bold;";
										$headerBacground = "background:#054F72;color:white;";
										
									}else{
										$liCss = "background:none;color:black;";
										$headerBacground = "background:#999;color:white;";
										
									}
								
								
								$i++;
								
								
								//check video Resume<?php
								//video Resume list
								$statictoken = 'ecbcd7eaee29848978134beeecdfbc7c';
								$base_url = base_url().'Fjapi';
								
								$jsonVideo = array("token" => $statictoken, "methodname" => 'auditionHistoryForWeb', 'userId' => $user['userId']);
								
								$data_string_video = json_encode($jsonVideo);                                                                                 
																																					 
								$ch_visitor = curl_init($base_url);                                                                      
								curl_setopt($ch_visitor, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
								curl_setopt($ch_visitor, CURLOPT_POSTFIELDS, $data_string_video);                                                                  
								curl_setopt($ch_visitor, CURLOPT_RETURNTRANSFER, true);
								curl_setopt($ch_visitor, CURLOPT_SSL_VERIFYPEER, false);
								curl_setopt ($ch_visitor, CURLOPT_SSL_VERIFYHOST, 0);								
								curl_setopt($ch_visitor, CURLOPT_HTTPHEADER, array(                                                                          
									'Content-Type: application/json',                                                                                
									'Content-Length: ' . strlen($data_string_video))                                                                       
								);                                                                                                                   
								$curlVideoResult = curl_exec($ch_visitor);
								
								$VideoallResult = json_decode($curlVideoResult);
								$VideoresumeList = $VideoallResult->data;
								$filterCss = "";
								
								
								
								$correctAssessmentAns = 0;
								$totalAssessmentResult = 0;
								$jobApplicationDetailForFilter   = $this->job_model->userAssessmentDetail($user['userId'], $currentJob);
								foreach($jobApplicationDetailForFilter as $assessmentResult){
									if($assessmentResult->userAnswer == $assessmentResult->questionAnswerId){
										$correctAssessmentAns++;
									}
									$totalAssessmentResult++;
								}
								$assessmentScore = ($correctAssessmentAns/$totalAssessmentResult)*100;
								
								
								
								$feedbacks = $ci->userjob_model->interviewComment($user['id']);
								$adminfeedbacks = $ci->userjob_model->adminComment($user['id']);
								if(!empty($feedbacks) && !empty($adminfeedbacks)){
									$feedbacksAll = array_merge($feedbacks,$adminfeedbacks);
								}else if(!empty($adminfeedbacks)){
									$feedbacksAll = $adminfeedbacks;
								}else if(!empty($feedbacks)){
									$feedbacksAll = $feedbacks;
								}else{
									$feedbacksAll = array();
								}
								
								
								$totalFeedBack = 0;
								$totalCountFeedback = 0;
								foreach($feedbacksAll as $userFeedback){
									$totalFeedBack 		= $totalFeedBack + $userFeedback['rating'];
									$totalCountFeedback++;
								}
								if($totalCountFeedback == 0){
									$totalCountFeedback = 1;
								}
								$userFeedbackCal = round($totalFeedBack/$totalCountFeedback, 0);
								
								$liClass = 'filter';
								$checkResume = ($user['jobResume'] != "")?$user['jobResume']:$user['resume_path'];
								if($checkResume != ""){
									
									$liClass .= '1';
								}else{
									$liClass .= '0';
								}
								
								if(count($VideoresumeList) > 0){
									$liClass .= '1';
								}else{
									$liClass .= '0';
								}
								
								if(count($VideoresumeList) > 0 && $checkResume != ""){
									$liClass .= '1';
								}else{
									$liClass .= '0';
								}
								//$checkResume = ($user['jobResume'] != "")?$user['jobResume']:$user['resume_path'];
								$date = date("Y-m-d H:m:s", strtotime('-24 hours', time()));
								if($user['status'] == 1){
									if($date > $user['createdAt']){
											$status = "1";
									}else{
											$status = "0";
									}
								}else{
									$status = $user['status'];
								}
								
								$ApplicantsjsonArray[] = array(
													'id'  => 'userlisting_'.$user['id']."_".$currentJob."_".$user['userId'],
													'userId' => $user['userId'],
													'name' => $user['fullname'],
													'jobId' => $currentJob,
													'UjId' => $user['id'],
													'resume' => $checkResume,
													'videoResume' => $VideoresumeList,
													'status' => $status,
													'location' => $user['preferredLocation'],
													'assessmentScore' => $assessmentScore,
													'feedbackScore' => $userFeedbackCal,
											
								);
								if(in_array($user['status'],$showListingFilter)){
									$displayLi = " display:block; ";
								}else{
									$displayLi = " display:none; ";
								}
								$statictoken = 'ecbcd7eaee29848978134beeecdfbc7c';
								$base_url = base_url().'Fjapi';
								
								$json = array("token" => $statictoken, "methodname" => 'getMessageListForWeb', 'UserjobId' => $user['id'], 'userId' => $_SESSION['logged_in']->id, 'UserFromId' => $user['userId']);  
									
								/*$json = array("token" => $statictoken, "methodname" => 'getMessageListForWeb', 'UserjobId' => 137, 'userId' => 190, 'UserFromId' => 186); */                                                                   
								$data_string = json_encode($json);                
								
								$ch = curl_init($base_url);                                                                      
								curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
								curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
								curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
								curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
								curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
								curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
									'Content-Type: application/json',                                                                                
									'Content-Length: ' . strlen($data_string))                                                                       
								);                                                                                                                  
																																					 
								$result = curl_exec($ch);
								curl_close ($ch);
								$allResult = json_decode($result);
								$messagesReceived = $allResult->data;
								
								$userIdEncrypted = encryptURLparam($user['userId'], URLparamKey);
								
								
							?>
							<li class="userlisting active<?php echo $userFeedbackCal. " ".$liClass  ?> userlisting_<?php echo $user['id']."_".$currentJob."_".$user['userId'] ?>" style="list-style:none;<?php echo $filterCss.$liCss.$displayLi; ?>padding-bottom: 5px;    box-shadow: 2px 2px 2px 0px gray;">
									<div class="col-md-12 headerListing" style="padding-left:0px;padding-right:0px;    padding: 10px;padding-left: 0px;<?php echo $headerBacground ?>">
										<div  class="col-md-9 pull-left" style="padding-left:10px;padding-right:0px;">
											<div style="    font-size: 18px;"><?php echo ($user['fullname'] != "")?$user['fullname']:"N/A"; ?></div>
										</div>
										<div class="col-md-3" style="padding-left:0px;padding-right:0px;padding: 3px;">
											<div>
												<div  style=" width: 30%;cursor:pointer; float:left;">
													<div>
													<center>
														<i  style="font-size: 15px;color:white;"  title="Click here to send message to applicant." onclick="sendMessage('<?php echo $user['fullname'] ?>',<?php echo $user['userId'] ?>,'<?php echo $currentJobName ?>',<?php echo $user['id'] ?>)"  data-toggle="modal" data-target="#SendMessage" class="fa fa-envelope" aria-hidden="true"></i>
													</center>
													</div>
												</div>
											
												<?php if(count($messagesReceived) > 0){ ?>
													<div style="  width: 30%;cursor:pointer;float:right;">
														<div>
														<center>
															<i onclick="GetMessage('<?php echo $user['fullname'] ?>',<?php echo $user['userId'] ?>,'<?php echo $currentJobName ?>',<?php echo $user['id'] ?>)" title="Click here to view applicant's response."  data-toggle="modal" data-target="#GetMessage" class="fa fa-inbox" style="font-size: 15px;color:white;" aria-hidden="true"></i>
															<span class="noti_mail" style="display: block;background:#ffd700;font-size: 11px;margin-left: 11px;    margin-top: -27px;color: rgb(5, 79, 114);font-weight: bold;"><?php echo count($messagesReceived) ?></span>
														</center>
													</div>
													</div>
												<?php }else{ ?>
													<div  style="width: 30%; cursor:pointer;float:right;">
														<div>
													<center>
														<i title="No Message received" class="fa fa-inbox" style="cursor: not-allowed;font-size: 15px;color:white;" aria-hidden="true"></i>
														</center>
													</div>
													</div>
													
												<?php } ?>
												<div style="clear:both"></div>
											</div>
										</div>
									</div>
									<div style="clear:both"></div>
									<div class="col-md-9 pull-left" style="padding-top: 6px;padding-left: 6px;">
										<div onclick="changeUserNew('<?php echo 'userlisting_'.$user['id']."_".$currentJob."_".$user['userId']; ?>');" style="cursor:pointer;" >
											<div class="col-md-3 pull-left" style="    padding-left: 0px;    padding-right: 0px;">
												<?php if($user['image'] == ""){ ?>
													<div><img src="http://35.154.53.72/admin/theme/firstJob/image/user_image.png" style="width: 46px;" class="img-circle" ></div>
												<?php }else{ ?>
												<div><img src="http://35.154.53.72/admin/theme/firstJob/image/<?php echo $user['image'] ?>"  style="width: 46px;"  class="img-circle"></div>
												<?php } ?>
												<?php
												
												$checkInvitaton = $ci->userjob_model->checkInvitationUserJob($user['jobId'],$user['email']);  
												if(!empty($checkInvitaton)){
												?>
												<div style="font-weight:100;font-size:12px;    padding-left: 7px;">
													<?php echo "Invited" ?>
												</div>
												<?php } ?>
											</div>
											<div class="col-md-9"  style="padding-left:0px;padding-right:0px;">
												<div>
												<fieldset class="rating">
													<input type="radio" id="star51<?php echo $user['userId'] ?>" name="<?php echo $user['userId'] ?>" value="5"   <?php echo ($userFeedbackCal == 5)?"checked":"" ?>  disabled />
													<label class = "full" for="star51<?php echo $user['userId'] ?>" title="Awesome - 5 stars"></label>
													<input type="radio" id="star4half1<?php echo $user['userId'] ?>" name="<?php echo $user['userId'] ?>" value="4.5" <?php echo ($userFeedbackCal == 4.5)?"checked":"" ?>  disabled  />
													<label class="half" for="star4half1<?php echo $user['userId'] ?>" title="Pretty good - 4.5 stars"></label>
													<input type="radio" id="star41<?php echo $user['userId'] ?>" name="<?php echo $user['userId'] ?>" value="4"  <?php echo ($userFeedbackCal == 4)?"checked":"" ?>  disabled   />
													<label class = "full" for="star41<?php echo $user['userId'] ?>" title="Pretty good - 4 stars"></label>
													<input type="radio" id="star3half1<?php echo $user['userId'] ?>" name="<?php echo $user['userId'] ?>" value="3.5"  <?php echo ($userFeedbackCal == 3.5)?"checked":"" ?>  disabled  />
													<label class="half" for="star3half1<?php echo $user['userId'] ?>" title="Meh - 3.5 stars"></label>
													<input type="radio" id="star31<?php echo $user['userId'] ?>" name="<?php echo $user['userId'] ?>" value="3"  <?php echo ($userFeedbackCal == 3)?"checked":"" ?>  disabled   />
													<label class = "full" for="star31<?php echo $user['userId'] ?>" title="Meh - 3 stars"></label>
													<input type="radio" id="star2half1<?php echo $user['userId'] ?>" name="<?php echo $user['userId'] ?>" value="2.5"  <?php echo ($userFeedbackCal == 2.5)?"checked":"" ?>   disabled  />
													<label class="half" for="star2half1<?php echo $user['userId'] ?>" title="Kinda bad - 2.5 stars"></label>
													<input type="radio" id="star21<?php echo $user['userId'] ?>" name="<?php echo $user['userId'] ?>" value="2"  <?php echo ($userFeedbackCal == 2)?"checked":"" ?>   disabled  />
													<label class = "full" for="star21<?php echo $user['userId'] ?>" title="Kinda bad - 2 stars"></label>
													<input type="radio" id="star1half1<?php echo $user['userId'] ?>" name="<?php echo $user['userId'] ?>" value="1.5"  <?php echo ($userFeedbackCal == 1.5)?"checked":"" ?>  disabled   />
													<label class="half" for="star1half1<?php echo $user['userId'] ?>" title="Meh - 1.5 stars"></label>
													<input type="radio" id="star11<?php echo $user['userId'] ?>" name="<?php echo $user['userId'] ?>" value="1"  <?php echo ($userFeedbackCal == 1)?"checked":"" ?>  disabled   />
													<label class = "full" for="star11<?php echo $user['userId'] ?>" title="Sucks big time - 1 star"></label>
													<input type="radio" id="starhalf1<?php echo $user['userId'] ?>" name="<?php echo $user['userId'] ?>" value="0.5" <?php echo ($userFeedbackCal == 0.5)?"checked":"" ?>  disabled  />
													<label class="half" for="starhalf1<?php echo $user['userId'] ?>" title="Sucks big time - 0.5 stars"></label>
												</fieldset>
												</div>
												<div style="clear:both"></div>
												<div style="font-weight:100;font-size:13px;"><?php echo date('d M, Y H:i', strtotime($user['createdAt'])); ?></div>
											</div>
											<div style="clear:both"></div>
										</div>
									</div>
									<div class="col-md-3 pull-left" style="    padding-right: 0px;padding-left:0px;">
										<div style="padding-right:15px;padding-top: 5px;">
												<?php
											if($user['status'] == 2 || $user['status'] == 4){
											?>
											
												<div>
													<div style="font-size: 13px;">
														<center><div style="background:#5CC36A;padding-left:10px;padding-right:10px;padding-top:2px;padding-bottom:2px;color:white;">
														Shortlisted</div></center>
													</div>
												</div>
												
											<?php }else if($user['status'] == 3 || $user['status'] == 5){?>
												
												<div>
													<div style="font-size: 13px;">
														<center><div style="background:#EE6E3D;padding-left:10px;padding-right:10px;padding-top:2px;padding-bottom:2px;color:white;">Rejected</div></center>
													</div>
												</div>
												
											<?php }else if($user['status'] == 1){
												if($date > $user['createdAt']){
													$statusText = "Pending";
													$backgroundColor = "#b981cc";
													
												}else{
													$statusText = "New";
													$backgroundColor = "#27acff";
												}
												?>
												<div>
													<div style="font-size: 13px;">
														<center><div style="background:<?php echo $backgroundColor ?>;padding-left:10px;padding-right:10px;padding-top:2px;padding-bottom:2px;color:white;">
														<?php echo $statusText ?>
														</div>
														</center>
													</div>
												</div>
												
											<?php } ?>
										
											
										</div>
										
										
											</div>
									
								<div style="clear:both"></div>
							</li>
								<?php  } 
							}else{
								$currentUser = 0;
								$UjId = 0;
								$mergedVideo = "";
								$Resume = "";?>
								<li><div style="padding-top: 25%;font-size: 18px;font-weight: bold;"> <Center>No applicants</center></div></li>
							<?php }
							?>
						</ul>
						
					
						
				</div>
			
            </div>
			<?php
			
			$detailsForUserJob = $ci->userjob_model->detailsForUserJobs($UjId, $currentUser);
			
			?>
			<div class="col-md-9" id="userData" style="width:74%;margin-top:10px;background:white;    box-shadow: 0.2px 0.2px 1px 1px; padding-right: 0px;padding-left:0px;margin-left:16px;">
				<?php if($currentUser == 0){?>
					<div style='font-size:18px;    padding-top: 25%;font-weight: bold;'><center>No applicant selected</center></div>
					<script>
					$( document ).ready(function() {
						var documentHeight = $(document).height();
						$("#userData").css("height",documentHeight-80);
						
						
					})
					</script>
					
				<?php }else{  ?>
			
			<div class="col-md-12" style="padding-left:0px;padding-right:0px;    border-bottom: 1px solid lightgray;">
				<div class="col-md-3" style="padding-left:0px;padding-right:0px;">
					<div style="background:#fff;color:#000;padding-left:12px;padding-top:6px;height:136px;padding-right: 12px;padding-bottom:6px;">
						<div class="pull-left col-md-2" style="padding-left: 17px;">
							<?php if($currentUser > 0){  ?>
							<?php if($detailsForUserJob['image'] == ""){ ?>
							<img src="http://35.154.53.72/admin/theme/firstJob/image/user_image.png" style="width: 46px;" class="img-circle" >
							<?php }else{ ?>
							<img src="<?php echo base_url() ?>/theme/firstJob/image/<?php echo $detailsForUserJob['image']  ?>"  style="width: 46px;"  class="img-circle">
								<?php } ?>
							<?php }else{ ?>
								<div  style="width: 46px;background:#0094ba;    height: 46px;    margin-top: 6px;"  class="img-circle"></div>
							<?php } ?>
						</div>
						<div class="pull-left col-md-10" style="padding-left: 30px;padding-top:0px;">
							<div style="float:left;padding-top:2px;    font-size: 18px;font-weight:bold;"><?php 
							if($detailsForUserJob['fullname'] != ""){
								$nameArray = explode(" ",$detailsForUserJob['fullname']);
								$i = 0;
								//echo "<pre>";print_r($nameArray);die;
								foreach($nameArray as $array){
									if($i == 0){
										echo $array."<br>";
										$i++;
									}else{
										echo " ".$array;
									}
								}
							}else{
								echo "N/A";
							}
							
							?>
							
							</div>
							<?php
							if($detailsForUserJob['linkedInLink'] != ""){ ?>
								<div style="float:right"><a href="<?php echo $detailsForUserJob['linkedInLink']; ?>" target="_blank"><i class="fa fa-linkedin-square" aria-hidden="true" style="    color: #0077b5;font-size: 16px;"></i></a></div>
							<?php }
							?>
							
							<div style="clear:both"></div>
							<div style="font-size:13px; height: 26px;">
								<?php
									$PinCode = $ci->userjob_model->getCityStatePincode($detailsForUserJob['pinCode']);
									if(!empty($PinCode)){
										echo ucwords(strtolower($PinCode->city)).", ".ucwords(strtolower($PinCode->state));
									}
								?>
							</div>
						</div>
						<div style="clear:both"></div>
						
						<div style="color:black;padding-top:14px;">
							<center><div class="pull-left" style="float:left">
								<?php if($currentUser > 0){  ?>
								<?php if($detailsForUserJob['jobResume'] != ""){ ?>
								
									<a data-toggle="modal" data-target="#UserResume" onclick="getResumeUsers('<?php echo $detailsForUserJob['jobResume'] ?>')"  class="btn" style="width:100%;background:#054F72;color:#fff;padding:5px;"><i class="fa fa-file" aria-hidden="true"></i>&nbsp;&nbsp;Resume</a>
									
								
								<?php }else if($detailsForUserJob['resume_path'] != ""){ ?>
									<a data-toggle="modal" data-target="#UserResume" onclick="getResumeUsers('<?php echo $detailsForUserJob['resume_path'] ?>')"  class="btn" style="width:100%;background:#054F72;color:#fff;padding:5px;"><i class="fa fa-file" aria-hidden="true"></i>&nbsp;&nbsp;Resume</a>
								<?php }else{ ?>
									<button  class="btn" style="width:100%;background: #e8e8e8;color:#fff;padding:5px;" disabled><i class="fa fa-file" aria-hidden="true"></i>&nbsp;&nbsp;Resume</button>
								<?php  } ?>
							<?php } ?>
							</div>
							<div class="pull-right"  style="float:right">
							<?php if($currentUser > 0){  ?>
								<?php
								//video Resume list
								$statictoken = 'ecbcd7eaee29848978134beeecdfbc7c';
								$base_url = base_url().'Fjapi';
								$jsonVideo = array("token" => $statictoken, "methodname" => 'auditionHistoryForWeb', 'userId' => $currentUser);
								
								$data_string_video = json_encode($jsonVideo);                                                                                   
																																					 
								$ch_visitor = curl_init($base_url);                                                                      
								curl_setopt($ch_visitor, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
								curl_setopt($ch_visitor, CURLOPT_POSTFIELDS, $data_string_video);                                                                  
								curl_setopt($ch_visitor, CURLOPT_RETURNTRANSFER, true);
								curl_setopt($ch_visitor, CURLOPT_SSL_VERIFYPEER, false);
								curl_setopt ($ch_visitor, CURLOPT_SSL_VERIFYHOST, 0);
								curl_setopt($ch_visitor, CURLOPT_HTTPHEADER, array(                                                                          
									'Content-Type: application/json',                                                                                
									'Content-Length: ' . strlen($data_string_video))                                                                       
								);                                                                                                                   
								$curlVideoResult = curl_exec($ch_visitor);
								$VideoallResult = json_decode($curlVideoResult);
								$VideoresumeList = $VideoallResult->data;
									if(count($VideoresumeList)>0){
								?>
								
									<a  href="javascript://"  data-toggle="modal" data-target="#videoAudition" class="btn" style="width:107px;color: #fff;   background: #054F72;padding:5px;"><i class="fa fa-video-camera" aria-hidden="true"></i>&nbsp;&nbsp;Video Resume</a>
									<?php }else{  ?>
									<button class="btn" style="width:107px;color: #fff;    background: #e8e8e8;padding:5px;" disabled><i class="fa fa-video-camera" aria-hidden="true"></i>&nbsp;&nbsp;Video Resume</button>
									<?php } ?>
								<?php } ?>
							</div>
							<div style="clear:both"></div>
							</center>
						</div>
					</div>
				</div>
				<?php
				$AdminUserComments   = $this->userjob_model->adminUserComment($UjId, $userData->id);
				$AdminUserComment = $AdminUserComments[0];
				
				?>
				<div class="col-md-9" style="padding-left:0px;padding-right:0px;">
					<form name="submitReviewForm" id="submitReviewForm" method="post" >
						<div style="border-left: 1px solid lightgray;background:#fff;color:#000;padding-left:12px;height: 136px;padding-right:12px;    padding-top: 6px;  padding-bottom: 6px;">
							<div class="col-md-3 pull-left" style="">		
							<div class="col-md-12 rating" style="    padding-left: 0px;">
							<?php 
							$disbaledStatus = "";
							$readonly = "";
							//if($AdminUserComment['comment'] != ""){ 
									$disbaledStatus = " disabled";
									$readonly = " readonly ";
							//}
							?>
								<div style="">Rating<span style="color:red">*</span></div>
								<fieldset class="rating">
									<input onclick="initiate()" type="radio" id="star5" name="adminRating" value="5"   <?php echo ($AdminUserComment['rating'] == 5)?"checked ".$disbaledStatus:"".$disbaledStatus ?>  />
									<label class = "full" for="star5" title="Awesome - 5 stars"></label>
									<input onclick="initiate()"  type="radio" id="star4half" name="adminRating" value="4.5"   <?php echo ($AdminUserComment['rating'] == 4.5)?"checked ".$disbaledStatus:"".$disbaledStatus ?>  />
									<label class="half" for="star4half" title="Pretty good - 4.5 stars"></label>
									<input onclick="initiate()"  type="radio" id="star4" name="adminRating" value="4"   <?php echo ($AdminUserComment['rating'] == 4)?"checked ".$disbaledStatus:"".$disbaledStatus ?> />
									<label class = "full" for="star4" title="Pretty good - 4 stars"></label>
									<input onclick="initiate()"  type="radio" id="star3half" name="adminRating" value="3.5"   <?php echo ($AdminUserComment['rating'] == 3.5)?"checked ".$disbaledStatus:"".$disbaledStatus ?> />
									<label class="half" for="star3half" title="Meh - 3.5 stars"></label>
									<input onclick="initiate()"  type="radio" id="star3" name="adminRating" value="3"   <?php echo ($AdminUserComment['rating'] == 3)?"checked ".$disbaledStatus:"".$disbaledStatus ?> />
									<label class = "full" for="star3" title="Meh - 3 stars"></label>
									<input onclick="initiate()"  type="radio" id="star2half" name="adminRating" value="2.5"   <?php echo ($AdminUserComment['rating'] == 2.5)?"checked ".$disbaledStatus:"".$disbaledStatus ?> />
									<label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>
									<input onclick="initiate()"  type="radio" id="star2" name="adminRating" value="2"  <?php echo ($AdminUserComment['rating'] == 2)?"checked ".$disbaledStatus:"".$disbaledStatus ?>  />
									<label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
									<input onclick="initiate()"  type="radio" id="star1half" name="adminRating" value="1.5"   <?php echo ($AdminUserComment['rating'] == 1.5)?"checked ".$disbaledStatus:"".$disbaledStatus ?>  />
									<label class="half" for="star1half" title="Meh - 1.5 stars"></label>
									<input onclick="initiate()"  type="radio" id="star1" name="adminRating" value="1"   <?php echo ($AdminUserComment['rating'] == 1)?"checked ".$disbaledStatus:"".$disbaledStatus ?> />
									<label class = "full" for="star1" title="Sucks big time - 1 star"></label>
									<input onclick="initiate()"  type="radio" id="starhalf" name="adminRating" value="0.5"  <?php echo ($AdminUserComment['rating'] == 0.5)?"checked ".$disbaledStatus:"".$disbaledStatus ?>  /><label class="half" for="starhalf" title="Sucks big time - 0.5 stars">
									</label>
								</fieldset>
							</div>
							<div class="col-md-12" style="padding-right:0px;padding-left:0px;    padding-top: 16px;">
								<?php if($currentUser > 0){
									
									
									
								?>
								<div style="padding-bottom: 4px;">My Recommendation<span style="color:red">*</span></div>
								<div>
										<input type="hidden" name="hdenjobid" value="<?php echo $UjId  ?>">
										<input type="hidden" name="accessToken" value="f1r$tj0b098">
										<input type="hidden" name="hdnusrid" id="hdnusrid" value="<?php echo $currentUser ?>">
										<input type="hidden" name="hdninterviewid" id="hdninterviewid" value="<?php echo $UjId ?>">
									<?php 
									$disableDropDown = "";
									//if($detailsForUserJob['status'] == 2 || $detailsForUserJob['status'] == 3 || $detailsForUserJob['status'] == 5 || $detailsForUserJob['status'] == 4){
										$disableDropDown = "disabled";
									//} 
									?>
									<select  onchange="initiate()"  class="form-control" name="interview_status" id="interview_status_dropdown" style="border-color: #B5B5B5;color: black !important;" <?php echo $disableDropDown ?>>
										<option value="1"  <?php echo ($detailsForUserJob['status'] == 1)?"selected":"" ?> >Undecided</option>
										<option value="2"  <?php echo ($detailsForUserJob['status'] == 2)?"selected":"" ?> >Shortlisted</option>
										<option value="3"  <?php echo ($detailsForUserJob['status'] == 3)?"selected":"" ?>>Rejected</option>
									</select>
								</div>
							
								<?php } ?>
							</div>
						</div>
						
						<div class="col-md-6 pull-left" style="">
							<div>Comment<span style="color:red">*</span></div>
							<div>
								<textarea col="10" row="10" placeholder="Comments" name="interview_comment" onchange="initiate()" id="interview_comment" style="background:#fff;color:#000;width:100%;height:104px;border-radius:5px;" <?php echo $readonly ?>><?php echo $AdminUserComment['comment'] ?></textarea>
							</div>
						</div>
						
						<div class="col-md-3 pull-left" style="    padding-top: 2%;">
						<?php if($currentUser > 0){  ?>
							
							<div style="">
								<?php if($detailsForUserJob['status'] == 2 || $detailsForUserJob['status'] == 3 || $detailsForUserJob['status'] == 5 || $detailsForUserJob['status'] == 4){ ?>
									<input type="button" onclick="submitReview();" name="submit" value="Submit" class="btn" style=" color:#fff;background:#e8e8e8;   width: 100%;
    padding: 15px;    float: right;" disabled >
								<?php }else{ ?>
									<input type="button" onclick="submitReview();" name="submit" value="Submit" class="btn" style=" color:#fff;background:#e8e8e8;   width: 100%;
    padding: 15px;    float: right;" disabled>
								<?php } ?>
							</div>
							<?php } ?>
						</div>
						<div style="clear:both"></div>
					</div>
					</form>
				
				</div>
				</div>
				<div style="clear:both"></div>
				<?php if($currentUser > 0){ ?>
					<?php
					
					$jobApplicationDetail   = $this->job_model->userAssessmentDetail($currentUser, $currentJob);
					
					?>
				<?php }else{
					$jobApplicationDetail = null;
					
				 }
					
					
				 ?>
				<div class="row" style="padding-left:0px;padding-right:0px;    background: white;    padding-top: 8px;">
					<ul class="nav nav-tabs applicants-tabs" style="border-bottom:none;">
						<li id="panellevelformid"  class="active" style="cursor:pointer;color:black;font-size: 16px;" ><a data-toggle="tab" href="#recruitersjobcontent" aria-expanded="false" style="color:black;">
							<?php
							if($detailsForUserJob['source'] == 0){
								echo '<i style="font-size:18px;"  class="fa fa-mobile" aria-hidden="true" title="This video interview came from mobile"></i>&nbsp;&nbsp;';
							}else{
								echo '<i style="font-size:18px;"  class="fa fa-desktop" aria-hidden="true"  title="This video interview came from website"></i>&nbsp;&nbsp;';
							}
							?>
						<b>Video Interview</b></a></li>
						<?php if(count($jobApplicationDetail) > 0){ ?>
						<li id="basicformid" style="cursor:pointer;color:black;font-size: 16px;"><a id="basicjobcontentId" data-toggle="tab" href="#basicjobcontent" aria-expanded="true"  style="color:black;">
						<div class="pull-left" ><i style="font-size:18px;"  class="fa fa-book" aria-hidden="true"></i>&nbsp;&nbsp;<b>Assessment Details</b></div>
						<div  title="Assessment score of applicant"  class="pull-left"  id="assessmentRating" style="margin-top:0px;margin-bottom:0px;padding: 1px;margin-left:10px;font-weight:bold;">0</div>
						<div style="clear:both;"></div>
						</a></li>
						<?php } ?>
						<li style="color:black;font-size: 16px;" id="interviewformid" class="cursor:pointer;">
							<a data-toggle="tab" href="#interviewjobcontent" aria-expanded="false"  style="color:black;">
								<div class="pull-left"><i style="font-size:18px;"  class="fa fa-houzz"></i>&nbsp;&nbsp;<b>Interview Feedback</b></div>
								<div  title="Average Interview Feedback Score by Reviewers"  class="pull-left" style="font-weight:bold;padding-left: 11px;font-size:14px;" id="totalFeedback"></div>
								<div style="clear:both"></div>
							</a>
						</li>
						
						
					</ul>
				</div>
				<div class="tab-content style-4" style="border-top:1px solid grey;background:white;  height: 300px;overflow-y: scroll;">
						<div id="basicjobcontent" class="tab-pane fade">
								<div style="color:white;">
									<?php if(count($jobApplicationDetail)>0){ ?>
										<div style="background:#fff;color:black;display:block;" class="assessmentDetails">
									<?php }else{ ?>
										<div style="background:#fff;color:black;display:block;" class="assessmentDetails">
									<?php }?>
									
										<?php 
										$totalWrongAns = 0;
										$totalCorrectAns = 0; 
										if(count($jobApplicationDetail)>0){
											foreach($jobApplicationDetail as $assessment){
												//echo "<pre>";print_r($assessment);
											?>
											<div style="padding-top: 11px;padding-bottom: 11px;">
												<div class="col-md-10 question pull-left">Questions: <?php echo $assessment->title ?></div>
												<div class="col-md-2 question pull-left">
													<?php if($assessment->userAnswer == $assessment->questionAnswerId){ 
														$totalCorrectAns++;
													?>
														<i class="fa fa-check-circle" title="Applicant gave right answer" style="font-size: 23px;" aria-hidden="true"></i>
														<i class="fa fa-times-circle" title="Applicant gave right answer" style="font-size: 23px;color:lightgrey;" aria-hidden="true"></i>
														
													<?php }else{
														$totalWrongAns++;
														?>
														<i class="fa fa-check-circle" title="Applicant gave wrong answer" style="font-size: 23px;color:lightgrey;" aria-hidden="true"></i>
														<i class="fa fa-times-circle" title="Applicant gave wrong answer" style="font-size: 23px;" aria-hidden="true"></i>
													<?php } ?>
												</div>
												<div style="clear:both"></div>
												<div class="col-md-12 answer">
													<span>Answers:</span><?php echo $assessment->userOption ?>
												</div>
												
												<div style="clear:both"></div>
											</div>
											<?php }?>
												<input type="hidden" name="assessmentCorrect" id="assessmentCorrect" value="<?php echo $totalCorrectAns ?>">
												<input type="hidden" name="assessmentWrong" id="assessmentWrong" value="<?php echo $totalWrongAns ?>">
												<input type="hidden" name="Totalassessment" id="Totalassessment" value="<?php echo count($jobApplicationDetail) ?>">
										<?php }else{						?>
										<div style="padding-top: 11px;padding-bottom: 11px;height:100px;">
											<div class="col-md-10 question pull-left">No Assessments Available.</div>
											<div class="col-md-2 question pull-left"></div>
											<div style="clear:both"></div>
											<div class="col-md-12 answer">
												<span></span>
											</div>
												<input type="hidden" name="assessmentCorrect" id="assessmentCorrect" value="<?php echo $totalCorrectAns ?>">
												<input type="hidden" name="assessmentWrong" id="assessmentWrong" value="<?php echo $totalWrongAns ?>">
												<input type="hidden" name="Totalassessment" id="Totalassessment" value="<?php echo count($jobApplicationDetail) ?>">

											<div style="clear:both"></div>
										</div>
										<?php } ?>
										
									</div>
								</div>
						</div>
						
						
							<div id="recruitersjobcontent" class="tab-pane fade active in">
								<?php
				
								$statictoken = 'ecbcd7eaee29848978134beeecdfbc7c';
								$base_url = base_url().'Fjapi';
								
								$json = array("token" => $statictoken, "methodname" => 'getUserJobQuestionsAnswers', 'jobId' => $currentJob, 'userId' => $currentUser);                                                                    
								$data_string = json_encode($json);                
								
								$ch = curl_init($base_url);                                                                      
								curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
								curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
								curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
								curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
								curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
								curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
									'Content-Type: application/json',                                                                                
									'Content-Length: ' . strlen($data_string))                                                                       
								);                                                                                                                  
																																					 
								$result = curl_exec($ch);
								curl_close ($ch);
								$allResult = json_decode($result);
								$questionsListDetails = $allResult->questionsList;
								
								
								?>
							<div style="background:#054F72;color:#000;">
								
								<div style="clear:both;"></div>
								<div style="background:#fff">
									<center style="    padding: 10px;"><b>Questions</b></center>
										<?php if($detailsForUserJob['mergedVideo'] != "" ){ ?>
												<div class="col-md-11 pull-left" style=" padding:10px;">
												<?php
												for($i = 1;$i<=count($questionsListDetails);$i++){
													if($i == 1){
														$QuestionIDCss = "background:#0091BA;color:white;";
													}else{
														$QuestionIDCss = "background:#ccc;color:black;";
													}
												?>
														<div title="<?php echo $questionsListDetails[0]->question  ?>" class="pull-left" style="margin-right: 10px;"><button class="btn questionIdList" id="questionIdList_<?php echo $i; ?>" onclick="changeInterviewQuestion(<?php echo $i?>)" style="<?php echo $QuestionIDCss; ?>   padding: 2px;padding-left: 10px;padding-right: 10px;border-radius: 0px;"><?php echo $i ?></button></div>
												<?php }
												?>
													<div style="clear:both;"></div>
												</div>
												<div class="col-md-1 pull-left" style="font-size:16px;    padding-right: 27px;">
													
													<a href="javascript:"  style="color:white;background:#054F72;   padding: 5px;padding-left: 10px;padding-right: 10px;border-radius: 0px;float:right;" data-toggle="modal" data-target="#shareInterview" class="btn share-user-interview" 
																	data-options='{"jobId":"<?php echo $currentJob; ?>", "userId":"<?php echo $currentUser; ?>", "userJob":"<?php echo $UjId; ?>", "appUserName":"<?php echo $detailsForUserJob['fullname'];?>", "interviewVideo":"<?php echo $detailsForUserJob['mergedVideo']; ?>"}'
																>Share</a>
													
												</div>
												<div style="clear:both;"></div>
												
												
												<?php 
												$counter = 1;
												foreach($questionsListDetails as $details){
													if($counter == 1){
														$questionCss = "display:block;";
													}else{
														$questionCss = "display:none;";
													}
													$counter++;
												?>
													<div>
														<div class="cold-md-12">
															<center>
																
																<div style="color:black;background:#ccc;width:50%;<?php echo $questionCss ?>    padding: 5px;"><?php echo $details->question ?></div>
															</center>
														</div>
														<div style="clear:both"></div>
													</div>
													
												<?php } ?> 
												
												
												<div style="padding:10px;"><center><b>Applicant's Response</b></center></div>
												<div style="clear:both;"></div>
												<div>
													<center>
														<video style="width: 634px;height: 295px;" src="https://s3.amazonaws.com/videoMerge/<?php echo $detailsForUserJob['mergedVideo']; ?>" controls></video>
													</center>
												</div>
										<?php }else{  ?>
										
											
											
											<div style="clear:both;"></div>
										
										<div style="col-md-11  pull-left"  style=" padding:10px;">
											<?php
											for($i = 1;$i<=count($questionsListDetails);$i++){
												if($i == 1){
													$QuestionIDCss = "background:#0091BA;color:white;";
												}else{
													$QuestionIDCss = "background:#ccc;color:black;";
												}
											?>
													<div title="<?php echo $questionsListDetails[0]->question  ?>" class="pull-left" style="margin-right: 10px;"><button class="btn questionIdList" id="questionIdList_<?php echo $i; ?>" onclick="changeInterviewQuestion(<?php echo $i?>)" style="<?php echo $QuestionIDCss; ?>   padding: 2px;padding-left: 10px;padding-right: 10px;border-radius: 0px;"><?php echo $i ?></button></div>
											<?php }
											?>
											
										</div>
										<div class="col-md-1 pull-right" style="font-size:16px;    padding-right: 27px;    margin-top: 12px;">
											
												<a href="javascript:"  style="color:white;background:#054F72;   padding: 5px;padding-left: 10px;padding-right: 10px;border-radius: 0px;    float: right;" data-toggle="modal" data-target="#shareInterview" class="btn share-user-interview" 
															data-options='{"jobId":"<?php echo $currentJob; ?>", "userId":"<?php echo $currentUser; ?>", "userJob":"<?php echo $UjId; ?>", "appUserName":"<?php echo $detailsForUserJob['fullname'];?>", "interviewVideo":"<?php echo $detailsForUserJob['mergedVideo']; ?>"}'
														>Share</a>
											
										</div>
										<div style="clear:both"></div>
										<?php
										$counter = 1;
										foreach($questionsListDetails as $details){
											if($counter == 1){
												$questionCss = "display:block;";
											}else{
												$questionCss = "display:none;";
											}
											
										?>
										<div id="QuestionCounter_<?php echo $counter; ?>" class="QuestionList" style="<?php echo $questionCss ?> ">
											<div>
												<div class="cold-md-12">
													<center>
														<div style="color:black;background:#ccc;width:50%;    padding: 5px;"><?php echo $details->question ?></div>
													</center>
												</div>
												<div style="clear:both"></div>
											</div>
											<div  style="padding:10px;"><center><b>Applicant's Response</b></center></div>
											<div>
												<center>
													<video class="auditionClassNew" id="auditionClassNew<?php echo $counter; ?>" style="    width: 634px;height: 295px;" src="https://s3.amazonaws.com/fjinterviewanswers/<?php echo $details->answer ?>" controls></video>
												</center>
											</div>
										</div>
										<?php $counter++; } ?>
										<script>
										$(".auditionClassNew").on('pause',function(e){
											var lastValue = $('#questionDropdown option:last-child').val();
											var currentQuestn = $("#questionDropdown").val();
											if(lastValue != currentQuestn){
												var currentId = e.target.id;
												$("#QuestionCounter_"+currentQuestn).css("display","none");
												$("#questionIdList_"+currentQuestn).css("background","#ccc");
												$("#questionIdList_"+currentQuestn).css("color","#000");
												
												$('#questionDropdown option:selected').next().attr('selected', 'selected');
												var NextQuestn = $("#questionDropdown").val();
												$("#questionIdList_"+NextQuestn).css("background","#0091BA");
												$("#questionIdList_"+NextQuestn).css("color","#fff");
												
												$("#QuestionCounter_"+NextQuestn).css("display","block");
												$("#AnswerCounter_"+NextQuestn).trigger("play");
											}else{
												return false;
											}

										})
										</script>
										
										<?php } ?>
									</div>
									
								</div>
							
							</div>
							<div id="interviewjobcontent" class="tab-pane fade" style="height: 300px;overflow-y: scroll;">
								<?php  if($currentUser > 0){ ?>
									<?php 
										$feedbacks = $ci->userjob_model->interviewComment($UjId);
										$adminfeedbacks = $ci->userjob_model->adminComment($UjId);
										if(!empty($adminfeedbacks) && !empty($feedbacks)){
											$feedbacksAll = array_merge($feedbacks,$adminfeedbacks);	
										}else if(!empty($feedbacks)>0){
											$feedbacksAll = $feedbacks;	
										}else if(!empty($adminfeedbacks) > 0){
											$feedbacksAll = $adminfeedbacks;	
										}else{
											$feedbacksAll = array();
										}
										
										
										$noFeedback = 1;
											
									}else{
									$feedbacks = null;
									$noFeedback = 0;
									} ?>
								<div style="color:white;">
									<?php 
									$totalFeedBack = 0;
									$totalCountFeedback = 0;
									$count = 0;
									if(count($feedbacksAll)>0){   ?>
										<div style="background:#fff;color:black;" class="feedbackDetails">
											
										<?php 
										
										foreach($feedbacksAll  as $feedback){
											$totalFeedBack 		= $totalFeedBack + $feedback['rating'];
											$totalCountFeedback = $count++;
										?>
											<div style="padding-top: 11px;padding-bottom: 11px;    border-bottom: 1px solid black;">
												<div class="col-md-12 answer">
													<p>"<?php echo $feedback['comment']  ?>"</p>
												</div>
												
												<div style="clear:both"></div>
												<div class="col-md-12 rating">
													<fieldset class="rating">
														<input type="radio" id="star5" name="rating" value="5"   <?php echo ($feedback['rating'] == 5)?"checked":"" ?>  /><label class = "full" for="star5" title="Awesome - 5 stars"></label>
														<input type="radio" id="star4half" name="rating1" value="4.5" <?php echo ($feedback['rating'] == 4.5)?"checked":"" ?> /><label class="half" for="star4half" title="Pretty good - 4.5 stars"></label>
														<input type="radio" id="star4" name="rating1" value="4"  <?php echo ($feedback['rating'] == 4)?"checked":"" ?>  /><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
														<input type="radio" id="star3half" name="rating1" value="3.5"  <?php echo ($feedback['rating'] == 3.5)?"checked":"" ?> /><label class="half" for="star3half" title="Meh - 3.5 stars"></label>
														<input type="radio" id="star3" name="rating1" value="3"  <?php echo ($feedback['rating'] == 3)?"checked":"" ?>  /><label class = "full" for="star3" title="Meh - 3 stars"></label>
														<input type="radio" id="star2half" name="rating1" value="2.5"  <?php echo ($feedback['rating'] == 2.5)?"checked":"" ?>  /><label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>
														<input type="radio" id="star2" name="rating" value="2"  <?php echo ($feedback['rating'] == 2)?"checked":"" ?>  /><label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
														<input type="radio" id="star1half" name="rating1" value="1.5"  <?php echo ($feedback['rating'] == 1.5)?"checked":"" ?>  /><label class="half" for="star1half" title="Meh - 1.5 stars"></label>
														<input type="radio" id="star1" name="rating1" value="1"  <?php echo ($feedback['rating'] == 1)?"checked":"" ?>  /><label class = "full" for="star1" title="Sucks big time - 1 star"></label>
														<input type="radio" id="starhalf" name="rating1" value="0.5" /><label class="half" for="starhalf" title="Sucks big time - 0.5 stars"></label>
													</fieldset>
												</div>
												<div class="col-md-12 question">
												<?php
												if(isset($feedback['createdBy']) && $feedback['createdBy'] != ""){ ?>
												<?php }else{ ?>
													<div  class="pull-left" style="background:#27acff;padding:3px;">External</div>
												<?php }
												?>
												<?php echo '<div  class="pull-left">&nbsp;&nbsp;'.$feedback['firstName'].' '.$feedback['lastName'].' ('.$feedback['emailId'].') </div>'; ?>
												
												</div>
												
												<div class="col-md-12 answer">
													<span style="font-size: 12px;font-style: italic;"><?php echo date('d M, Y', strtotime($feedback['createdAt']));  ?></span>
												</div>
												<div style="clear:both"></div>
											</div>
											<?php } ?>
											<input type="hidden" name="totalFeedBackCal" id="totalFeedBackCal" value="<?php echo $totalFeedBack ?>">
											<input type="hidden" name="totalFeedBackCountCal" id="totalFeedBackCountCal" value="<?php echo $count ?>">
											</div>
											
										<?php }else{ 
												
										?>
											<div style="background:#fff;color:black; display:block;" class="feedbackDetails">
												<div style="height:100px;padding-top: 11px;padding-bottom: 11px;">
													
													<div style="clear:both;"></div>
													<center>No Feedback Available</center>
													<input type="hidden" name="totalFeedBackCal" id="totalFeedBackCal" value="<?php echo $totalFeedBack ?>">
													<input type="hidden" name="totalFeedBackCountCal" id="totalFeedBackCountCal" value="<?php echo $totalCountFeedback ?>">
												</div>
											</div>
												<?php 
												} ?>
									
								</div>
						</div>
						</div>
						</div>
			<div style="clear:both"></div>
				<?php } ?>
		</div>	 
    </div>
</div>
<!-- /#page-content-wrapper -->

<!-- PopUp for view share's profile  -->
<div id="videoAudition" class="modal fade" role="dialog">
    <div class="modal-dialog" style="margin-top:7%;">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="    border-bottom: 1px solid #000;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4><?php echo $detailsForUserJob['fullname']." Video Resume"  ?></h4>
            </div>
            <div class="modal-body" >                                      
                <?php
				
				if(count($VideoresumeList) > 0) { ?>
           
					<div id="page-content-wrapper">
						<div class="container-fluid whitebg">
							<div class="row">
							
								<div class="col-md-12 padd_tp_bt_20">
								<?php if($VideoresumeList[0]->auditionFiles == "" ){ ?>
									<div class="col-md-12 text-center pull-right">
										<?php
										$url = 'https://s3.amazonaws.com/fjauditionsmergevideo/';
										if($VideoresumeList[0]->audition_file != ""){ ?>
											
											<div id="auditio">
												<video  class="auditionClass"  id="auditionVideo_<?php echo $count; ?>" style="width:400px;" src='<?php echo $url.$VideoresumeList[0]->audition_file ?>' controls>
											</div>
										<?php }else{
										?>
											<div id="auditio">
												Sorry No Video Resume Available.
											</div>
										<?php } ?>
											
									
									</div>
								<?php }else{ ?>
								<div class="col-md-12 text-center pull-right">
									Questions: <select class="selectpicker" id="questionDropdown" onchange="showAuditionForQuestion()" style="color:black;    margin-top: 20px;margin-bottom: 27px;width: 263px;">
													<option value="0">Tell us something about yourself?</option>
													<option value="1">Tell us about your strengths?</option>
													<option value="2">Tell us about your weakness?</option>
													<option value="3">Tell us about any of your project/work that you are proud of?</option>
												</select>
								</div>
								<div class="col-md-12 text-center pull-right">
										<?php
										
										$auditionsVideos = $VideoresumeList[0];
										$auditionArray = array_filter(explode(",", $auditionsVideos->auditionFiles));
										
										if(!empty($auditionArray)){
											for($count = 0;$count<=count($auditionArray);$count++){?>
												<?php
												$url = 'https://s3.amazonaws.com/fjauditions/';
												if($count == 0){ ?>
													
													<div id="audition_<?php echo $count ?>" style="display:block;">
														<video class="auditionClass" id="auditionVideo_<?php echo $count; ?>" style="width:400px;" src='<?php echo $url.trim($auditionArray[$count]) ?>' controls>
													</div>
												<?php }else{
												?>
													<div id="audition_<?php echo $count ?>" style="display:none">
														<video class="auditionClass"  id="auditionVideo_<?php echo $count; ?>" style="width:400px;"  src='<?php echo $url.trim($auditionArray[$count]) ?>' controls>
													</div>
												<?php } ?>
											<?php }
										
										}
										?>
										
									
									</div>
								<?php } ?>
								</div>
							</div>
						</div>
					</div>


            <!-- /.row -->

			<?php }else{
				echo "<div style='clear:both'></div><center style='    margin-top: 11%;' id='auditionNotAvaialble'>Sorry No Video Resume Available.</center>";
				
			} ?>
			</div>
        </div>
    </div>
</div>


<!-- PopUp for view share's profile  -->
<div id="shareInterview" class="modal fade" role="dialog">
    <div class="modal-dialog" style="margin-top: 15%;">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Share Applicant Interview Video</h4>
            </div>
            <div class="modal-body" id="share">                                      
                <form method="post" id="shareInterviewForm">
                <p id="errMessage"></p>
                <table class="table" border="0" bordercolor="#CCCCCC" style="width:100%;float:left;">
                    <thead class="tbl_head">
                        <tr>
                            <th colspan="3">Add Email to Share Applicant's Video Interview</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><label for="inputEmail" class="control-label" style="font-weight:500">Email Id</label></td>
                            <td><input type="text" class="form-control receiverEmail"  id="receiverEmail1" placeholder="enter a valid email id"></td>
                        </tr>
						<tr>
                            <td><label for="inputEmail" class="control-label" style="font-weight:500">Email Id</label></td>
                            <td><input type="text" class="form-control receiverEmail"  id="receiverEmail2" placeholder="enter a valid email id"></td>
                        </tr>
                        <tr>
                            <td><label for="inputEmail" class="control-label" style="font-weight:500">Email Id</label></td>
                            <td><input type="text" class="form-control receiverEmail"  id="receiverEmail3" placeholder="enter a valid email id"></td>
                        </tr>
                        <tr>
                            <td><label for="inputEmail" class="control-label" style="font-weight:500">Email Id</label></td>
                            <td><input type="text" class="form-control receiverEmail"  id="receiverEmail4" placeholder="enter a valid email id"></td>
                        </tr>
                        <tr>
                            <td><label for="inputEmail" class="control-label" style="font-weight:500">Email Id</label></td>
                            <td><input type="text" class="form-control receiverEmail"  id="receiverEmail5" placeholder="enter a valid email id"></td>
                        </tr>
                        <tr>
							<td></td>
                            <td colspan="3">
								<input type="hidden" name="userJob" id="userJob" value="<?php echo $UjId ?>">
                                <span id="formButton">
									<button type="button" class="Save_frmw" id="sendInterview" style="background:#054F72;">Share Interview</button>
								</span>
                            </td>
                        </tr>
                    </tbody>
                </table>
				
				<div style="clear:both"></div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="UserResume" class="modal fade" role="dialog">
    <div class="modal-dialog" style="    width: 800px;height: 900px;">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="border-bottom:1px solid black;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><?php echo $detailsForUserJob['fullname'] ?> Doc Resume</h4>
            </div>
            <div class="modal-body" id="share">
				
					<div style="padding: 10px;">
							<button onclick="ResumeDownload();" style="background:#0094BA;color:white;" class="btn">Download&nbsp;&nbsp;<i class="fa fa-download" aria-hidden="true"></i></button>
							<input type="hidden" name="resumeFilename" id="resumeFilename" value=""> 
					</div>
                <iframe id="iframeSource" style="    height: 800px;width: 100%;" src=""></iframe>
				
            </div>
        </div>
    </div>
</div>

<div id="SendMessage" class="modal fade" role="dialog">
    <div class="modal-dialog" style="margin-top: 15%;">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="messageHeader"></h4>
            </div>
            <div class="modal-body" >
				
					<h5 id="messageSubject"></h5>
					<input type="hidden" name="SendMessageTo" id="SendMessageTo" value="">
					<input type="hidden" name="userJobIdMessage" id="userJobIdMessage" value="">
					<input type="hidden" name="SendMessageSubject" id="SendMessageSubject" value="">
					<textarea id="messageText" style="width:100%;height:100px;"></textarea>
				
            </div>
			<div class="modal-body" >
				<button class="btn pull-right" onclick="postmessage()" >SEND</button>
				<div style="clear:both"></div>
			</div>
        </div>
    </div>
</div>

<div id="GetMessage" class="modal fade" role="dialog">
    <div class="modal-dialog" style="margin-top: 15%;">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="border-bottom:1px solid black;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="GetMessageHeader"></h4>
            </div>
            <div class="modal-body" id="getMessageContent">
				
					
				
            </div>
			<div class="modal-body" >
				<div style="clear:both"></div>
			</div>
        </div>
    </div>
</div>

<?php
echo "<script>";
$jsonApplicatants = json_encode($ApplicantsjsonArray);
echo "var applicants = ".$jsonApplicatants; 
echo "</script>";
?>

<script>



function postmessage(){
	var methodname = 'sendMessageForWeb';
	var toId = $("#SendMessageTo").val();
	var messageText = $("#messageText").val();
	var messageSubject = $("#SendMessageSubject").val();
	var UserjobId = 	$("#userJobIdMessage").val();
	var parentMessageId = "0";
	var tokenUser = "<?php echo $_SESSION['logged_in']->id ?>";
	var base_URL = "<?php echo base_url() ?>";
	var js_obj = {token: 'ecbcd7eaee29848978134beeecdfbc7c',methodname: methodname,userId:tokenUser,toId:toId, messageText:messageText, messageSubject:messageSubject, parentMessageId:parentMessageId, UserjobId:UserjobId};
    var encoded = JSON.stringify( js_obj );
    var data= encoded;
    $.ajax({
          url : base_URL+"Fjapi",
          data: data,
          type: "POST",
          success: function(response){
                item = $.parseJSON(response);
                if(item.code == "200"){
                    alert("Message Sent successfully.");
                }else{
                    alert(item.message);
                }
            }
    });
}
$(document).ready(function(){
    $.fn.serializeObject = function()
    {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
    var feedbackCount 	 = $("#totalFeedBackCountCal").val();
    var totalFeedBackCal = $("#totalFeedBackCal").val();
	if(feedbackCount == 0){
		$("#totalFeedback").html("( N/A )");
	}else{
		var cal = totalFeedBackCal/feedbackCount;
		$("#totalFeedback").text("( Average Feedback Score : "+cal.toFixed(0)+" )");
	}
	
	var assessmentCorrect 	= $("#assessmentCorrect").val();
    var assessmentWrong 	= $("#assessmentWrong").val();
    var Totalassessment 	= $("#Totalassessment").val();
	if(Totalassessment == 0){
		$("#assessmentRating").text("( Assessment Score : 0 )");
	}else{
		var assCal = assessmentCorrect/Totalassessment*100;
		$("#assessmentRating").text("( Assessment Score : "+assCal.toFixed(0)+"% )");
	}
	
	$(".auditionClass").on('pause',function(){
		var previousValue = $('#questionDropdown').val();
		$('#questionDropdown option:selected').next().attr('selected', 'selected');
		
		playVideo(previousValue,$('#questionDropdown').val());
		return false;
	});
	
	

	
	var JobSelected = $("#JobListdropdown").val();
	
	var element = $("#JobListdropdown option:selected").attr("title");
	
	$('#JobListdropdown').attr("title",element);
	

});

function playVideo(previousValue,id){
	if(previousValue == id){
		return false;
	}
	$("#audition_"+previousValue).css("display","none");
	$("#audition_"+id).css("display","block");
	
	$("#auditionVideo_"+id).trigger('play');
}

function showAuditionForQuestion(){
		
		var q1 = $("#questionDropdown").val();
		$("#audition_0").css("display","none");
		$("#audition_1").css("display","none");
		$("#audition_2").css("display","none");
		$("#audition_3").css("display","none");
		$("#audition_4").css("display","none");
		$("#audition_"+q1).css("display","block");
		
		$("#auditionVideo_0").trigger('pause');
		$("#auditionVideo_1").trigger('pause');
		$("#auditionVideo_2").trigger('pause');
		$("#auditionVideo_3").trigger('pause');
		$("#auditionVideo_4").trigger('pause');
		$("#auditionVideo_"+q1).trigger('play');
		return false;

	}	
	
function sendMessage(name,id,jobName,UserjobId){
	$("#messageHeader").text("Send Message to "+name);
	$("#SendMessageTo").val(id);
	$("#SendMessageSubject").val(jobName);
	$("#messageSubject").text("Subject: "+jobName);
	$("#userJobIdMessage").val(UserjobId);
}

function toggleAssessment(){
	$(".assessmentDetails").toggle("slow");
	$(".assessment-minus").toggleClass("fa-minus fa-plus");
}

function toggleFeedback(){
	$(".feedbackDetails").toggle("slow");
	$(".feedback-minus").toggleClass("fa-minus fa-plus");
}

function changeInterviewQuestion(id){
	$(".QuestionList").each(function(){
		$(this).css("display","none");
	})
	$(".questionIdList").each(function(){
		$(this).css("background","white");
		$(this).css("color","black");
	})
	$("#questionIdList_"+id).css("background","#0091BA");
	$("#questionIdList_"+id).css("color","white");
	$("#QuestionCounter_"+id).css("display","block");
	$("#AnswerCounter_"+id).trigger("play");
	
}

function changeUser(UserId,jobId){
	var filter = "<?php echo isset($_GET['filter'])?$_GET['filter']:0 ?>";
	var filterBy = "<?php echo isset($_GET['filterBy'])?$_GET['filterBy']:0 ?>";
	
	if(jobId != "" && jobId != 0){
		if(filter == 0){
			if(filterBy != 0){
				window.location = "<?php echo base_URL() ?>queue/pastList?JobId="+jobId+"&userId="+UserId+"&filterBy="+filterBy;
			}else{
				window.location = "<?php echo base_URL() ?>queue/pastList?JobId="+jobId+"&userId="+UserId;
			}
			
		}else{
			if(filterBy != 0){
				window.location = "<?php echo base_URL() ?>queue/pastList?JobId="+jobId+"&userId="+UserId+"&filterBy="+filterBy+"&filter="+filter;
			}else{
				window.location = "<?php echo base_URL() ?>queue/pastList?JobId="+jobId+"&userId="+UserId+"&filter="+filter;
			}
		}
	}else{
		if(filter == 0){
			if(filterBy != 0){
				window.location = "<?php echo base_URL() ?>queue/pastList?userId="+UserId+"&filterBy="+filterBy;
			}else{
				window.location = "<?php echo base_URL() ?>queue/pastList?userId="+UserId;
			}
		}else{
			if(filterBy != 0){
				window.location = "<?php echo base_URL() ?>queue/pastList?userId="+UserId+"&filterBy="+filterBy+"&filter="+filter;
			}else{
				window.location = "<?php echo base_URL() ?>queue/pastList?userId="+UserId+"&filter="+filter;
			}
		}
	}

}

$(document).on('click', '.share-user-interview', function () {
        //("#errMessage").html('');
        $("#formButton").html('<button type="button" class="btn Save_frmw" id="sendInterview" style="background:#054F72;color:white;">Share Interview</button>');
        document.getElementById("shareInterviewForm").reset();
        var userJob         = $(this).data('options').userJob;
        var jobId         = $(this).data('options').jobId;
        var userId         = $(this).data('options').userId;
        var interviewVideo  = $(this).data('options').interviewVideo;
        var accessToken     = '<?php echo $this->config->item('accessToken'); ?>';
        var appUserName     = $(this).data('options').appUserName;
        
        $("#errMessage").html('<h3 style="margin-top: 0px; margin-bottom: 0px;"><center>'+appUserName+'</center></h3>'); 
        $("#userVideo").html('');        
        
        formData    = {jobId:jobId, userId:userId,userJob:userJob, accessToken:accessToken, interviewVideo:interviewVideo};
        $.ajax({
            url : siteUrl + "jobs/userVideo",
            type: "POST",
            data : formData,
            cache: false,
            success: function(result)
            {
                $("#userVideo").html(result);
            }
        });
    });
	
	
    $(document).on('click', '#sendInterview', function(e) {
        // Prevent form submission
        e.preventDefault();
        
        $("#sendInterview").text('please wait ...');
        var email1      = $("#receiverEmail1").val();
        var email2      = $("#receiverEmail2").val();
        var email3      = $("#receiverEmail3").val();
        var email4      = $("#receiverEmail4").val();
        var email5      = $("#receiverEmail5").val();
        var videoName   = $("#videoName").val();
        var userJob     = $("#userJob").val();
        var accessToken = '<?php echo $this->config->item('accessToken'); ?>';
        
        var formData    = new FormData();
        formData.append('email1', email1);
        formData.append('email2', email2);
        formData.append('email3', email3);
        formData.append('email4', email4);
        formData.append('email5', email5);
        formData.append('videoName', videoName);
        formData.append('userJob', userJob);
        formData.append('accessToken', accessToken);
        
        if(email1=='' && email2=='' && email3=='' && email4=='' && email5=='') {
            alert('Please fill at least one email id');
			$("#sendInterview").text('Share Interview');
			return false;
        }
        else {
			
            
			
        }
		
        $.ajax({
            url: siteUrl + 'jobs/sendInterview/',
            data: formData,
            async: false,
            contentType: false,
            processData: false,
            cache: false,
            type: 'POST',
            success: function (data) {
				$("#formButton").html('Email Successfully send');              
				setTimeout(function(){
					$(".close").trigger("click");
				}, 1000);
            },
        });
        return false;
    });
function applyFilter(){
	var filterValue = $('input[name=filterBy]:checked').val();
	if(filterValue == 5){
		$("#withResume").css("display","block");
	}
}

function changeUserNew(classListName){
	if(valueChanged == 1){
		var condfirmvalue = confirm("Decision for the applicant has been changed. Do you want to save?");
		if(condfirmvalue){
			submitReview();
			valueChanged = 0;
			return false;
		}else{
			valueChanged = 0;
		}
	}
	$(".headerListing").each(function (){
						
		$(this).css("background","#999");
		$(this).css("color","#fff");
		$(this).css("font-weight","100");
	});
	
	$.ajax({
		  url : "<?php echo base_url() ?>queue/listQueueUsers",
		  data: "class="+classListName+"&past=1&accessToken=f1r$tj0b098",
		  type: "POST",
		  success: function(response){
				$("#userData").html(response);
				var feedbackCount 	 = $("#totalFeedBackCountCal").val();
				var totalFeedBackCal = $("#totalFeedBackCal").val();
				if(feedbackCount == 0){
					$("#totalFeedback").html("( N/A )");
				}else{
					var cal = totalFeedBackCal/feedbackCount;
					$("#totalFeedback").text("(Average Feedback Score : "+cal.toFixed(0)+" )");
				}
				var assessmentCorrect 	= $("#assessmentCorrect").val();
				var assessmentWrong 	= $("#assessmentWrong").val();
				var Totalassessment 	= $("#Totalassessment").val();
				if(Totalassessment == 0){
					$("#assessmentRating").html("( Assessment Score : 0% )");
				}else{
					var assCal = assessmentCorrect/Totalassessment*100;
					$("#assessmentRating").text("( Assessment Score : "+assCal.toFixed(0)+"% )");
				}
				$("."+classListName+" .headerListing").css("background","#054F72");
				$("."+classListName+" .headerListing").css("color","#fff");
				$("."+classListName+" .headerListing").css("font-weight","bold");
				manageHeight();
				
		  }
	});

}
function manageHeight(){
	var documentHeight = $(document).height();
	$(".userList_All").css("height",documentHeight-362);
	$(".tab-content").css("height",documentHeight-265);
}
function sortbyAsc(){ 
	applicants.sort(function (a, b) {
		return a.UjId>(b.UjId);
	});
}

function sortbyDsc(){ 
	applicants.sort(function (a, b) {
		return a.UjId<(b.UjId);
	});
}
function sortbyNameAsc(){ 
	applicants.sort(function (a, b) {
		return a.name>(b.name);
	});
}
function sortbyNameDsc(){ 
	applicants.sort(function (a, b) {
		return a.name<(b.name);
	});
}

function changeSortByOrder(JobId){
	if(valueChanged == 1){
		var condfirmvalue = confirm("Decision for the applicant has been changed. Do you want to save?");
		if(condfirmvalue){
			submitReview();
			valueChanged = 0;
			return false;
		}else{
			valueChanged = 0;
		}
	}
	showFilter();
	//var filterValue = $("#filterBy").val();
	var filterValue = $('input[name=filterBy]:checked').val();
	var sortBy = $('input[name=sortBy]:checked').val();
	if(filterValue == undefined && sortBy == undefined){
		
		filterResultby();
		
	}else{
		var userId = "";
		
		var interviewValue = $("#interviewValue").val();
		var assessmentValue = $("#assessmentValue").val();
		var locationPrefered = $("#locationPrefered").val().toLowerCase();
		//alert(data.length);
		var currentuser = 0;
		var userArray = [];
		//for sorting
		if(sortBy == 1){
			sortbyAsc();
		}
		if(sortBy == 2){
			sortbyDsc();
		}
		
		if(sortBy == 3){
			sortbyNameAsc();
		}
		
		if(sortBy == 4){
			sortbyNameDsc();
		}
		//console.log(applicants);
		data = JSON.parse(JSON.stringify(applicants));
		
		//jsonNew =  [{"filter": }];
		//console.log();
		$.ajax({
			  url : "<?php echo base_url() ?>queue/listingUser",
			  data: JSON.stringify(applicants),
			  type: "POST",
			  success: function(response){
				  
					$(".userList_All").html(response);
					
			 
					var filter = [];
					$("input:checkbox[name=filtervalue]:checked").each(function(){
						if($(this).val() == 6){
							filter = ['0','1','2','3','4','5'];
						}else if($(this).val() == 2){
							filter.push('2');
							filter.push('4');
						}else if($(this).val() == 3){
							filter.push('3');
							filter.push('5');
						}else if($(this).val() == 1){
							filter.push('1');
						}else if($(this).val() == 0){
							filter.push('0');
						}
						
						
					});
					
					
					if(filterValue != undefined){
						for(var i = 0; i<data.length; i++){
							var currentStatus = data[i].status;
							if(filterValue == 5){
								if(data[i].resume != null && $.inArray(currentStatus, filter) > -1){
									if(userArray.length <= 0){
										currentuser = data[i].id;
									}
									userArray.push(data[i].id);
								}
							}
							if(filterValue == 6){
								if(data[i].videoResume!= null && data[i].videoResume.length > 0 && $.inArray(currentStatus, filter) > -1){
									if(userArray.length <= 0){
										currentuser = data[i].id;
									}
									
									userArray.push(data[i].id);
								}
							}
							
							if(filterValue == 7){
								if(data[i].resume != null && data[i].videoResume!= null && data[i].videoResume.length > 0 && $.inArray(currentStatus, filter) > -1){
									if(userArray.length <= 0){
										currentuser = data[i].id;
									}
									userArray.push(data[i].id);
								}
							}
							
							if(filterValue == 8){
								if(data[i].feedbackScore != null && data[i].feedbackScore >= interviewValue && $.inArray(currentStatus, filter) > -1){
									if(userArray.length <= interviewValue){
										currentuser = data[i].id;
									}
									userArray.push(data[i].id);
								}
							}
							
							if(filterValue == 9){
								if(data[i].assessmentScore != null && data[i].assessmentScore >= assessmentValue && $.inArray(currentStatus, filter) > -1){
									if(userArray.length <= interviewValue){
										currentuser = data[i].id;
									}
									userArray.push(data[i].id);
								}
							}
							
							if(filterValue == 10){
								if(data[i].location != null){
									var userLocatn = data[i].location;
									userLocatn = userLocatn.toLowerCase();
									if(data[i].location != null && userLocatn.indexOf(locationPrefered) !== -1 && $.inArray(currentStatus, filter) > -1){
										if(userArray.length <= interviewValue){
											currentuser = data[i].id;
										}
										userArray.push(data[i].id);
									}
								}
								
							}
							
						}
					}else{
						for(var i = 0; i<data.length; i++){
							var currentStatus = data[i].status;
							
							if($.inArray(currentStatus, filter) > -1){
								if(userArray.length <= 0){
									currentuser = data[i].id;
								}
								userArray.push(data[i].id);
							}
							
							
							if($.inArray(currentStatus, filter) > -1){
								if(userArray.length <= 0){
									currentuser = data[i].id;
								}
								
								userArray.push(data[i].id);
							}
							
							
							
							if($.inArray(currentStatus, filter) > -1){
								if(userArray.length <= 0){
									currentuser = data[i].id;
								}
								userArray.push(data[i].id);
							}
							
							
							
							if($.inArray(currentStatus, filter) > -1){
								if(userArray.length <= interviewValue){
									currentuser = data[i].id;
								}
								userArray.push(data[i].id);
							}
							
							
							
							if($.inArray(currentStatus, filter) > -1){
								if(userArray.length <= interviewValue){
									currentuser = data[i].id;
								}
								userArray.push(data[i].id);
							}
							
							
							
							
							
							if($.inArray(currentStatus, filter) > -1){
								if(userArray.length <= interviewValue){
									currentuser = data[i].id;
								}
								userArray.push(data[i].id);
							}
							
								
							
							
						}
					}
					
					$(".userlisting").each(function (){
						
						$(this).css("display","none");
					});
					//console.log(userArray);
					for(var j = 0; j<userArray.length;j++){
						
						$("."+userArray[j]).css("display","block");
					}
					
					
					NoAppliants = 1;
					$(".userlisting").each(function (){
						if($(this).is(':visible')){
							NoAppliants = 0;
							var classList = $(this).attr('class').split(/\s+/);
							var classListName = "";
							$.each(classList, function(index, item) {
								classListName = classListName+" "+item;
							});
							$.ajax({
								  url : "<?php echo base_url() ?>queue/listQueueUsers",
								  data: "class="+classListName+"&past=1&accessToken=f1r$tj0b098",
								  type: "POST",
								  success: function(response){
										$("#userData").html(response);
										//$("."+classListName).css("background","#999");
										//$("."+classListName).css("color","#fff");
										
										var classListArray = classListName.split(" ");
										for(var i =0; i<classListArray.length;i++ ){
											
											if(classListArray[i].indexOf('userlisting_') != -1){
												$("."+classListArray[i]).css("background","#999");
												$("."+classListArray[i]).css("color","#fff");
												$("."+classListArray[i]).css("font-weight","bold");
												
											}
										}
								  }
							});
							manageHeight();
							
							return false;
						}
						
					});
					if(NoAppliants == 1){
						var documentHeight = $(document).height();
						$("#userData").css("height",documentHeight-80);
						$("#userData").css("background","white");
						$("#userData").html("<div style='font-size:18px;    padding-top: 25%;font-weight: bold;'><center>No applicant selected</center></div>");
						$(".userList_All").html("<li><div style=' font-size:18px;   padding-top: 25%;font-weight: bold;'><center>No applicants</center></div></li>");
						//alert("No Applicants");
					}
				}
		});
		manageHeight();
		return false;
		
		if(sortBy == undefined){
			sortBy = 0;
		}
		if(JobId != ""){
			if(filterValue == 10){
				if($("#locationPrefered").val() == ""){
					alert("Please enter location");
				}else{
					var locationPrefered = $("#locationPrefered").val();
					window.location = "<?php echo base_URL() ?>queue/pastList?userId="+userId+"&JobId="+JobId+"&sort="+sortBy+"&filterBy="+filterValue+"&locationPrefered="+locationPrefered;
				}
			}else if(filterValue == 9){
				if($("#assessmentValue").val() == ""){
					alert("Please enter location");
				}else{
					var assessmentValue = $("#assessmentValue").val();
					window.location = "<?php echo base_URL() ?>queue/pastList?userId="+userId+"&JobId="+JobId+"&sort="+sortBy+"&filterBy="+filterValue+"&assessmentValue="+locationPrefered;
				}
			}else{
				window.location = "<?php echo base_URL() ?>queue/pastList?userId="+userId+"&JobId="+JobId+"&sort="+sortBy+"&filterBy="+filterValue;
			}
		}else if(JobId == ""){
			if(filterValue == 10){
				if($("#locationPrefered").val() == ""){
					alert("Please enter location");
				}else{
					var locationPrefered = $("#locationPrefered").val();
					window.location = "<?php echo base_URL() ?>queue/pastList?userId="+userId+"&sort="+sortBy+"&filterBy="+filterValue+"&locationPrefered="+locationPrefered;
				}
			}else if(filterValue == 9){
				if($("#assessmentValue").val() == ""){
					alert("Please enter Minimum Value per cent");
				}else{
					var assessmentValue = $("#assessmentValue").val();
					window.location = "<?php echo base_URL() ?>queue/pastList?userId="+userId+"&sort="+sortBy+"&filterBy="+filterValue+"&assessmentValue="+assessmentValue;
				}
			}else if(filterValue == 8){
				if($("#interviewValue").val() == ""){
					alert("Please enter Minimum Interview Feedback value");
				}else{
					var interviewValue = $("#interviewValue").val();
					window.location = "<?php echo base_URL() ?>queue/pastList?userId="+userId+"&sort="+sortBy+"&filterBy="+filterValue+"&interviewValue="+interviewValue;
				}
			}else{
				window.location = "<?php echo base_URL() ?>queue/pastList?userId="+userId+"&sort="+sortBy+"&filterBy="+filterValue;
			}
		}
	}
}
function checkfilterSelect(value){
	if(value >= 8){
			 $("#filterpopup").modal();
			 $("#JobListFilter").val(value);
			 if(value == 8){
				 $("#InterviewFeedback").css("display","block");
				 $("#AssessmentScore").css("display","none");
				 $("#AssessmentScore").css("display","none");
			 }else if(value == 9){
				 
				 $("#AssessmentScore").css("display","block");
				 $("#InterviewFeedback").css("display","none");
				 $("#Location").css("display","none");
			 }else if(value == 10){
				 $("#Location").css("display","block");
				 $("#InterviewFeedback").css("display","none");
				 $("#InterviewFeedback").css("display","none");
			 }
			 
	}else{
		var query = "<?php echo $_SERVER['QUERY_STRING'] ?>";
		window.location = "<?php echo base_URL() ?>queue/pastList?"+query+"&sort="+value;
	}
	
}
function filterResultby(){
	
	var filter = [];
	$("input:checkbox[name=filtervalue]:checked").each(function(){
		if($(this).val() == 6){
			filter = ['0','1','2','3','4','5'];
		}else if($(this).val() == 2){
			filter.push('2');
			filter.push('4');
		}else if($(this).val() == 3){
			filter.push('3');
			filter.push('5');
		}else if($(this).val() == 1){
			filter.push('1');
		}else if($(this).val() == 0){
			filter.push('0');
		}
		
		
	});
	
	data = JSON.parse(JSON.stringify(applicants));
	console.log(filter);
	var currentuser = 0;
	var userArray = [];
	for(var i = 0; i<data.length; i++){
		if($("#allreceivedId").prop('checked')){
			if(userArray.length <= 0){
				currentuser = data[i].id;
			}
			userArray.push(data[i].id);
			
		}
		var currentStatus = data[i].status;
		console.log(currentStatus);
		if($.inArray(currentStatus, filter) > -1){
			if(userArray.length <= 0){
				currentuser = data[i].id;
			}
			
			userArray.push(data[i].id);
		}
		
	}
	$(".userlisting").each(function (){
		$(this).css("display","none");
	});
	NoAppliants = 1;
		for(var j = 0; j<userArray.length;j++){
				NoAppliants = 0;
				$("."+userArray[j]).css("display","block");
				$("."+userArray[j]).css("display","block");
				$("."+userArray[j]+" .headerListing").css("background","#999");
				if(j == 0){
					$.ajax({
						  url : "<?php echo base_url() ?>queue/listQueueUsers",
						  data: "class="+userArray[0]+"&past=1&accessToken=f1r$tj0b098",
						  type: "POST",
						  success: function(response){
								$("#userData").html(response);
								$("."+userArray[0]+" .headerListing").css("background","#054F72");
								$("."+userArray[0]).css("color","#fff");
								$("."+userArray[0]).css("font-weight","bold");
								updateScore();
						  }
					});
				}
				manageHeight();
				
		}
	if(NoAppliants == 1){
		var documentHeight = $(document).height();
		$("#userData").css("height",documentHeight-80);
		$("#userData").css("background","white");
		$("#userData").html("<div style='font-size:18px;    padding-top: 25%;font-weight: bold;'><center>No applicant selected</center></div>");
		$(".userList_All").html("<li><div style=' font-size:18px;   padding-top: 25%;font-weight: bold;'><center>No applicants</center></div></li>");
		//alert("No Applicants");
	}
	
	
}

function updateScore(){
	var feedbackCount 	 = $("#totalFeedBackCountCal").val();
    var totalFeedBackCal = $("#totalFeedBackCal").val();
	if(feedbackCount == 0){
		$("#totalFeedback").html("( N/A )");
	}else{
		var cal = totalFeedBackCal/feedbackCount;
		$("#totalFeedback").text("( Average Feedback Score : "+cal.toFixed(0)+" )");
	}
	
	var assessmentCorrect 	= $("#assessmentCorrect").val();
    var assessmentWrong 	= $("#assessmentWrong").val();
    var Totalassessment 	= $("#Totalassessment").val();
	if(Totalassessment == 0){
		$("#assessmentRating").text("( Assessment Score : 0 )");
	}else{
		var assCal = assessmentCorrect/Totalassessment*100;
		$("#assessmentRating").text("( Assessment Score : "+assCal.toFixed(0)+"% )");
	}
	
	
	
}

function interviewFeedbackAbove(){
	
	$("#interviewFeedbackAbove").css("display","block");
	$("#assessmentFeedbackAbove").css("display","none");
	$("#locationPrefred").css("display","none");
}


function unselectAll(){
	$("input:checkbox[name=filtervalue]:checked").each(function(){
		$(this).removeAttr("checked");
		
	});
}

function GetMessage(name,id,jobName,UserjobId){
	$("#GetMessageHeader").text("Messages From "+name);
	var methodname = 'getMessageListForWeb';
	var tokenUser = "<?php echo $_SESSION['logged_in']->id ?>";
	var base_URL = "<?php echo base_url() ?>";
	var js_obj = {token: 'ecbcd7eaee29848978134beeecdfbc7c',methodname: methodname,userId:tokenUser,UserjobId:UserjobId,UserFromId:id};
    var encoded = JSON.stringify( js_obj );
    var data= encoded;
    $.ajax({
          url : base_URL+"Fjapi",
          data: data,
          type: "POST",
          success: function(response){
                item = $.parseJSON(response);
                if(item.code == "200"){
                    var setId = item.data[0].setID;
					
					var message = "";
					for(var i = 0; i < item.data.length; i++)
					{	
						
						message = message+"<div style='padding-bottom:10px;padding-top:10px;border-bottom: 1px solid grey;'><div class='pull-left'>"+item.data[i].message+"</div><div class='pull-right'>"+item.data[i].time+"</div><div style='clear:both'></div></div>";
						
					   //console.log("Type: " + item.data[i].setID + " Name: " + item.data[i].setID + " Account: " + item.data[i].setID);
							/**/
					}
					$("#getMessageContent").html(message);
                }else{
					$("#getMessageContent").text(item.message);
                    
                }
            }
    });
}

function ResumeDownload(){
	var link = $("#resumeFilename").val();
	 var url = "https://s3.amazonaws.com/resumefirstjob/"+link;	
	 window.location = url;
}

function hideJobs(jobId){
	if($('#hideJobs').is(":checked")){
		if(jobId != ""){
			window.location = "<?php echo base_URL() ?>queue/pastList?hide=1";
		}else{
			window.location = "<?php echo base_URL() ?>queue/pastList?hide=1";
		}
	}else{
		if(jobId != ""){
			window.location = "<?php echo base_URL() ?>queue/pastList?hide=0";
		}else{
			window.location = "<?php echo base_URL() ?>queue/pastList?hide=0";
		}
	}
}

function assessmentFeedbackAbove(){
	
	$("#assessmentFeedbackAbove").css("display","block");
	$("#locationPrefred").css("display","none");
	$("#interviewFeedbackAbove").css("display","none");
}

function locationPrefred(){
	$("#locationPrefred").css("display","block");
	$("#assessmentFeedbackAbove").css("display","none");
	$("#interviewFeedbackAbove").css("display","none");
}

function showFilter(){
	
	if($("#filterPopup").hasClass("open")){
		$("#filterPopup").css("display","none");
		$("#filterPopup").removeClass( "open" );
	}else{
		
		$("#filterPopup").toggle("slide", { direction: "right" }, 1000);
		$("#filterPopup").addClass("open");
	}
	
}

function resetFilterPopup(){
	$('input[name=filterBy]').removeAttr('checked');
	$('input[name=sortBy]').removeAttr('checked');
	$("#locationPrefred").css("display","none");
	$("#assessmentFeedbackAbove").css("display","none");
	$("#interviewFeedbackAbove").css("display","none");
	$("#interviewValue").val("");
	$("#assessmentValue").val("");
	$("#locationPrefered").val("");
	
}

function filterResultbycheck(id){
	if(valueChanged == 1){
		var condfirmvalue = confirm("Decision for the applicant has been changed. Do you want to save?");
		if(condfirmvalue){
			submitReview();
			valueChanged = 0;
			return false;
		}else{
			valueChanged = 0;
		}
	}
	if($('#'+ id).is(":checked")){
		if(id == 'allreceivedId'){
			
		}else{
			$("#allreceivedId").attr('checked',false);
			$('#'+id).attr('checked', false);
		}
	}else{
		if(id == 'allreceivedId'){
				
			$('#newId').prop('checked', true);
			$('#pendingId').prop('checked', true);
			$('#shortlistedId').prop('checked', true);
			$('#rejectedId').prop('checked', true);
		}
		$('#'+id).prop('checked', true);
	}
	filterResultby();
	resetFilterPopup();
	return false;
	
}


</script>


<script>
var valueChanged = 0;
function initiate(){
	valueChanged = 1;
}
$('#interview_comment').on('change',function(){
	alert("sdfsdf");
   valueChanged = 1;
});

$(document).mouseup(function(e) 
{
    var container = $("#filterPopup");

    // if the target of the click isn't the container nor a descendant of the container
    if (!container.is(e.target) && container.has(e.target).length === 0) 
    {
        container.hide();
    }
});
function submitReview(){
	$("body").css("cursor", "progress");
	var FormData = $("#submitReviewForm").serialize();
	$.ajax({
		  url : "<?php echo base_url() ?>queue/interviewreview",
		  data: FormData,
		  type: "POST",
		  success: function(response){
			alert("Interview Review Submitted successfully");
			$("body").css("cursor", "default");
		  }
	});
}

function getResumeUsers(Resume){
	
	$("#iframeSource").attr("src","https://docs.google.com/gview?url=https://s3.amazonaws.com/resumefirstjob/"+Resume+"&embedded=true");
	$("#resumeFilename").val(Resume);
}
$(document).ready(function(){
	var documentHeight = $(document).height();
	$(".userList_All").css("height",documentHeight-362);
	$(".tab-content").css("height",documentHeight-265);
	$("#interviewjobcontent").css("height",documentHeight-265);
	//alert();
	
	
})


function movetoNext(){
	$(".userlisting").each(function (){
		if($(this).is(':visible')){
			$(this).css("background","none");
			$(this).css("color","#000");
		}
	});
	if($(".currentList")[0]){
		var nextLi = $(".currentList").next("li").attr('class');
		if($("."+nextLi).is(':visible')){
			alert(" visible");
		}else{
			
		}
		var nextLiArray = nextLi.split(/\s+/);
		var thisLiCurrent = "";
		$.each(nextLiArray, function(index,name){
			if(name.indexOf('userlisting_') != -1){
				thisLiCurrent = name; 
			}
		})
	}else{
		alert("not exist");
		$(".userlisting").each(function (){
			if($(this).is(':visible')){
				var classList = $(this).attr('class').split(/\s+/);
				var classListName = "";
					$.each(classList, function(index, item) {
						classListName = classListName+" "+item;
					});
				var eachClass = classListName.split(" ");
				var FirstClass = eachClass[1];
				var nextLi = $("."+FirstClass).next("li").attr('class');
				var nextLiArray = nextLi.split(/\s+/);
				var thisLiCurrent = "";
				$.each(nextLiArray, function(index,name){
					if(name.indexOf('userlisting_') != -1){
						thisLiCurrent = name; 
					}
				})
				
				$("."+thisLiCurrent).addClass('currentList');
				$("."+thisLiCurrent).css('background',"#999");
				$("."+thisLiCurrent).css('color',"#000");
				
				
				$.ajax({
					  url : "<?php echo base_url() ?>queue/listQueueUsers",
					  data: "class="+nextLi+"&past=1&accessToken=f1r$tj0b098",
					  type: "POST",
					  success: function(response){
							$("#userData").html(response);
							
					  }
				});
				
				return false;
			}
			
		});
	}
	
}



</script>

<script>
function changeJobId(){
	id = $("#JobListdropdown").val();
	var hide = "<?php echo isset($_GET['hide'])?$_GET['hide']:0; ?>";
	
	window.location = "<?php echo base_URL() ?>queue/pastList?JobId="+id+"&hide="+hide;
}
</script>



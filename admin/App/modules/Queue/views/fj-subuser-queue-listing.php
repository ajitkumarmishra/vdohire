<?php 
//echo '<pre>';print_r($content);die(); ?>
<!-- Page Content -->




<script type="text/javascript">
    function flashObject(url, id, width, height, version, bg, flashvars, params, att)
    {
        var pr = '';
        var attpr = '';
        var fv = '';
        var nofv = 0;
        for(i in params)
        {
            pr += '<param name="'+i+'" value="'+params[i]+'" />';
            attpr += i+'="'+params[i]+'" ';
            if(i.match(/flashvars/ig))
            {
                nofv = 1;
            }
        }
        if(nofv==0)
        {
            fv = '<param name="flashvars" value="';
            for(i in flashvars)
            {
                fv += i+'='+escape(flashvars[i])+'&';
            }
            fv += '" />';
        }
        htmlcode = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"  codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=5,0,0,0" width="'+width+'" height="'+height+'">'
    +'  <param name="movie" value="'+url+'" />'+pr+fv
    +'  <embed src="'+url+'" width="'+width+'" height="'+height+'" '+attpr+'type="application/x-shockwave-flash" pluginspage="http://www.adobe.com/go/getflashplayer"></embed>'
    +'</object>';
        document.getElementById(id).innerHTML=htmlcode;
    }
</script>



<div id="page-content-wrapper">
    <?php if($this->session->flashdata('flashmessageerror')){  ?>
        <div class="alert alert-success">
            <?php echo $this->session->flashdata('flashmessagesuccess'); ?>
        </div>
    <?php } ?>
    
    <?php if($this->session->flashdata('flashmessageerror')){  ?>
        <div class="alert alert-danger">
            <?php echo $this->session->flashdata('flashmessageerror'); ?>
        </div>
    <?php } ?>
    
    <?php if (isset($message)): ?>
        <div class="alert alert-success">
            <?php echo $message; ?>
        </div>
    <?php endif; ?>
    
    <?php if (isset($error)): ?>
        <div class="alert alert-danger">
            <?php foreach ($error as $item): ?>
                <?php echo $item; ?>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
        
    <div class="container-fluid whitebg">
        <div class="row">
            <div class="col-md-12 padd_tp_bt_20">
                <h2><img src="<?php echo base_url(); ?>/theme/firstJob/image/arrow_head.png" class="prev-page">Queue Management</h2>
            </div>
            <?php
            $i = 0;             
            if( !empty($content) ) {
                foreach ($content as $item){                    
                    $invitecount                = 0;
                    $pendingcount               = 0;                    
                    $userjobpendingcount        = 0;
                    $userjobshortlistedcount    = 0;
                    $userjobrejectedcount       = 0;                     
                    // ci instance
                    $ci =&get_instance();
                    //job user invitation model
                    $ci->load->model('jobuserinvite_model');                    
                    // function to get count of invitation job for user
                    $jobinvitecount             = $ci->jobuserinvite_model->countForInvitedJobs($item->id); 
                    $invitecount                = $jobinvitecount['invitecount'];
                    $jobpendingcount            = $ci->jobuserinvite_model->countForInvitedPendingJobs($item->id);   
                    $pendingcount               = $jobpendingcount['pendingcount'];
                    $joblevelres                = $ci->jobpanel_model->getUserJobLevel($item->id, $userLoggeinId);   
                    $userjoblevel               = $joblevelres['level'];                    
                    // ci instance
                    $ci->load->model('userjob_model');                    
                    if( $userjoblevel == 1 ){
                        // function to get count of pending user's job
                        $userjobappliedcountres = $ci->userjob_model->countAllAppliedForSubUserJobs($item->id, 1);   
                        $userjobappliedcount    = $userjobappliedcountres['appliedCount'];
                    } else {
                        // function to get count of pending user's job
                        $userjobappliedcountres1    = $ci->userjob_model->countAllAppliedForSubUserJobs($item->id, 2);   
                        $userjobappliedcount        = $userjobappliedcountres1['appliedCount'];
                    }
                    
                    // function to get count of pending user's job
                    $userjobpendingcountres     = $ci->userjob_model->countForUserJobs($item->id, 1);   
                    $userjobpendingcount        = $userjobpendingcountres['appliedCount'];
                    
                    // function to get count of shortlisted user's job
                    $userjobshortlistedcountres = $ci->userjob_model->countForUserJobs($item->id, 4);   
                    $userjobshortlistedcount    = $userjobshortlistedcountres['appliedCount'];
                    
                    // function to get count of rejected user's job
                    $userjobrejectedcountres    = $ci->userjob_model->countForUserJobs($item->id, 3);   
                    $userjobrejectedcount       = $userjobrejectedcountres['appliedCount'];
                    
                    $totalstatuscount           = $userjobappliedcount+$userjobshortlistedcount;                    
                ?>            
                <div class="col-md-12">
                    <div class="over_fl">
                        <table class="table padd_tp_bt_10 blue_bg pad_bt_1 tab_rsp">
                            <tr>
                                <td class="col-md-3" style="width:35% !important;">
                                    <p class="txt_white">Job Title</p>
                                    <p  class="heading_que"><?php echo $item->title; ?></p>
                                </td>
                                <td class="col-md-1" style="width:10% !important;">
                                    <p class="txt_white">My Role</p>
                                    <p class="heading_que">
                                        <?php
                                        if( $userjoblevel == 1 ):
                                            echo "L1"; 
                                        elseif( $userjoblevel == 2 ):
                                            echo "L2"; 
                                        endif;
                                        ?>
                                    </p>
                                </td>
                                <td class="col-md-1" style="width:15% !important;">
                                    <p class="txt_white">Pending Applications</p>
                                    <p class="heading_que"><?=$userjobappliedcount?></p>
                                </td>
                                <td class="col-md-2" style="width:15% !important;">
                                    <p class="txt_white">Job End Date</p>
                                    <p class="heading_que2"><?php echo date("j M, Y", strtotime($item->openTill)); ?></p>
                                </td>                                
                                <td class="col-md-1 pad_25px" style="width:5% !important;">
                                    <?php if($totalstatuscount>0) { ?>
                                    <a href="#demo_<?=$i?>" data-toggle="collapse">
                                        <img src="<?php echo base_url(); ?>/theme/firstJob/image/plus_big.png" class="pull-right" id="img_toggle"></a>
                                    <?php } ?>
                                </td>
                            </tr>
                        </table>
                        
                        <?php
                        // function to get all user for particular job
                        if( $userjoblevel == 1 ){
                            $userlistofjob = $ci->userjob_model->listForSubUserJobs($item->id, 1);   
                        } else {
                            $userlistofjob = $ci->userjob_model->listForSubUserJobs($item->id, 2);   
                        }
                        
                        if( !empty($userlistofjob) ){   ?>
                        <div class="collapse no_padding" id="demo_<?=$i?>">
                            <table class="table tab_rsp">
                                <thead class="tbl_head">
                                    <tr>
                                        <!--<th><input type="checkbox" class="chk_bx"></th>-->
                                        <th style="width:40%;">Name</th>
                                        <th style="width:45%;">View Interview / Assessment / Resume</th>                                        
                                        <th style="width:15%;">Take Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $j=0;
                                    foreach( $userlistofjob as $userlistofjobval )
                                    {
                                        $j++;   ?>
                                    <tr class="green_l">
                                        <!--<td><input type="checkbox"></td>-->
                                        <td><?=($userlistofjobval['fullname']!=""?$userlistofjobval['fullname']:"")?></td>
                                        <td>
                                            <?php 
                                            //if($userlistofjobval['mergedVideo']!='') { ?>
                                            View                         
                                            <a
                                                href="javascript:" 
                                                data-toggle="modal" 
                                                data-target="#shareInterview" 
                                                class="share-user-interview" 
                                                data-options='{"jobId":"<?php echo $userlistofjobval['jobId']; ?>", "userId":"<?php echo $userlistofjobval['userId']; ?>", "userJob":"<?php echo $userlistofjobval['id']; ?>", "appUserName":"<?php echo $userlistofjobval['fullname'];?>", "interviewVideo":"<?php echo $userlistofjobval['mergedVideo']; ?>"}'
                                            >Interview</a>

                                            <?php
                                            /*}
                                            else { 
                                                $userlistofjobval['createdAt'];
                                                $futureTime = date('Y-m-d H:i:s', strtotime("+5 min", strtotime($userlistofjobval['createdAt'])));
                                                if(strtotime(date('Y-m-d H:i:s'))>strtotime($futureTime)){ ?>
                                                    <a   href='javascript:void(0);'
                                                            data-options='{"userId":"<?php echo $userlistofjobval['userId']; ?>", "jobId":"<?php echo $userlistofjobval['jobId']; ?>"}'
                                                            id='generateVideo<?php echo $j; ?>' 
                                                            class='generateVideo'>Generate Video</a>
                                                    <?php
                                                }
                                            }    */                        
                                            ?>
                                            /
                                            <a
                                            href="javascript:" 
                                            data-toggle="modal" 
                                            data-target="#viewUserProfileDetails" 
                                            class="view-user-profile-details" 
                                            data-options='{"userId":"<?php echo $userlistofjobval['userId']; ?>", "jobId":"<?php echo $userlistofjobval['jobId']; ?>", "interviewVideo":"<?php echo $userlistofjobval['mergedVideo']; ?>"}'
                                            >Assessment</a>

                                            <?php if($userlistofjobval['jobResume']) { ?>
                                                <a target="_blank" href="https://s3.amazonaws.com/resumefirstjob/<?php echo $userlistofjobval['jobResume']; ?>"> / Resume</a>
                                            <?php } elseif($userlistofjobval['resume_path']) { ?>
                                                <a target="_blank" href="https://s3.amazonaws.com/resumefirstjob/<?php echo $userlistofjobval['resume_path']; ?>"> / Resume</a>
                                            <?php } else { ?>
                                            <?php echo ' / Resume'; } ?>
                                        </td>
                                        
                                        
                                        
                                        <?php
                                        if( $userLevel == 3 ){ ?>
                                            <td><a href="<?php echo base_url(); ?>queue/viewinterview/inteviewid/<?=$userlistofjobval['id']?>/userid/<?=$userlistofjobval['userPid']?>">Evaluate Interview</a></td>
                                            <?php 
                                        }
                                        elseif( $userlistofjobval['status'] == 1 && $userLevel != 3 ){
                                            $assignJobIds = explode(',',$usersJobListL1);
                                            if(in_array($userlistofjobval['jobId'], $assignJobIds)){ ?>
                                            <td><a href="<?php echo base_url(); ?>queue/viewinterview/inteviewid/<?=$userlistofjobval['id']?>/userid/<?=$userlistofjobval['userPid']?>">Evaluate Interview</a></td>
                                            <?php 
                                            }                                        
                                        } 
                                        elseif( $userlistofjobval['status'] == 2 && $userLevel != 3 ){ 
                                            $assignJobIds = explode(',',$usersJobListL2);
                                            if(in_array($userlistofjobval['jobId'], $assignJobIds)){ ?>
                                            <td><a href="<?php echo base_url(); ?>queue/viewinterview/inteviewid/<?=$userlistofjobval['id']?>/userid/<?=$userlistofjobval['userPid']?>">Evaluate Interview</a></td>
                                            <?php 
                                            } 
                                        }
                                        else { ?>
                                            <td> - </td>
                                        <?php } ?>                                        
                                    </tr>
                                    <?php
                                    }
                                    ?>
                                </tbody>
                            </table>  
                        </div>
                        <?php
                        }
                        ?>
                    </div>
                </div>  
                <?php
                $i++;
                }
            } else {
                echo "<div class=\"\">Sorry, there is not any job for you to review</div>";
            }
            ?>
            <div class="service_content ">

                <?php if ($totalCount > $itemsPerPage): ?>
                    <ul class="service_content_last_list">
                        <?php if ($page > 1): ?>
                            <p class="pagi_img"><a href="<?php echo base_url(); ?>queue/list/<?php echo $page - 1; ?>" ><img src="<?php echo base_url(); ?>/theme/firstJob/image/pagination_img2.png"></a></p>    
                        <?php endif; ?>
                        <input type="hidden" name="page_no" value="1" id="page_no"> 
                        <?php for ($i = 1; $i <= $count; $i++): ?>
                            <li><a href="<?php echo base_url(); ?>queue/list/<?php echo $i; ?>"  <?php if ($i == $page): ?>style="color: orange;"<?php endif; ?>><?php echo $i; ?></a></li>
                        <?php endfor; ?>
                        <?php if ($page < $count): ?>
                            <p class="pagi_img"><a href="<?php echo base_url(); ?>queue/list/<?php echo $page + 1; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/pagination_img1.png"></a></p>            
                        <?php endif; ?>
                    </ul>
                <?php endif; ?>
            </div>
        </div>    
    </div>
</div>












<style>
    .service_content ul.service_content_last_list li{ display:inline-block; font-size:20px; width:38px; height:38px; line-height:38px; border:0px; }
    .service_content ul.service_content_last_list li a{ color:#333333; padding:0px; text-align:center; display:block; border:0px;  }
    .service_content ul.service_content_last_list li a:hover{ color:#fff; border-radius:50px; -moz-border-radius:50px; -webkit-border-radius:50px; background:#0072dc }
    .response_img img{ max-width: 100%; }
    .service_content .service_content_last_list{ text-align:center; padding:10px 0 70px; }
    ul.service_content_last_list  li.pagination-last-list{padding:0 20px 0 0px; cursor:pointer; text-indent:-9999px; margin-left:5px;  }
    .pagi_img{display: -webkit-inline-box;}
    .pagi_img a img{display: -webkit-inline-box;}
    .service_content ul.service_content_last_list li{margin:0px!important;}
    h1{font-size:32px!important;}
    .acu_bt a {
        text-decoration: none;
        color: #fff;
    }
    .sel_opt {
        font-size:15px;
        width:33% !important;
        padding-left: 9px !important;
    }
    #sorting td a {color: #303030!important; }
    #sorting td a:hover {color: #777!important; }
    
    .heading_que2 {
        color: #FFF;
        font-size: 16px;
    }
</style>


<script src="<?php echo base_url(); ?>theme/firstJob/js/chosen.jquery.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>theme/firstJob/js/prism.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
    $(document).on('click', '.view-user-profile-details', function () {
        var userId          = $(this).data('options').userId;
        var jobId           = $(this).data('options').jobId;
        var interviewVideo  = $(this).data('options').interviewVideo;
        var accessToken     = '<?php echo $this->config->item('accessToken'); ?>';
        $("#assessmentDetail").html('');
          
        
        formData    = {userId:userId, jobId:jobId, accessToken:accessToken, interviewVideo:interviewVideo};
        $.ajax({
            url : siteUrl + "queue/userAssessmentDetail",
            type: "POST",
            data : formData,
            cache: false,
            success: function(result)
            {
                var arrstr = result.split("####");
                //alert(arrstr[1]);
                $("#assessmentDetail").html(arrstr[0]);
                $("#percentageBlock").html("( "+arrstr[1]+"% )");
            }
        });
    });
    
    
    
    $(".generateVideo").click(function () {        
        var userId      = $(this).data('options').userId;
        var jobId       = $(this).data('options').jobId;
        var recordId    = $(this).attr('id');   
        
        var formData    = new FormData();
        formData.append('jobId', jobId);
        formData.append('userId', userId);        
        formData.append('accessToken', '<?php echo $this->config->item('accessToken'); ?>');
        
        $(this).parent().html('Merging Video, Please Wait...'); 
        $('.view-user-profile-details').prop('disabled', true);
        
        $.ajax({
            url: siteUrl + 'jobs/generateVideo/',
            data: formData,
            async: false,
            contentType: false,
            processData: false,
            cache: false,
            type: 'POST',
            success: function (data) {
                if(data) {
                    setTimeout(function(){
                        location.reload();
                    }, 5000);
                }
            },
        });
        return false;
    });
    
    
    
    var config = {
        '.chosen-select': {},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
    
    
    
    $(function () {
        $("#datepicker-19").datepicker();
    });

    
    
    $(function(){
        var videoUrl = "uploads/jd/<?php echo $job['jd'] ?>";
        jwVideo(videoUrl, 'jdvideo');
    });
</script>









<style>
.modal-content {
    -webkit-box-shadow: 0 5px 15px rgba(0,0,0,.5);
    box-shadow: 0 5px 15px rgba(0,0,0,.5);
}

.modal-content {
    position: relative;
    background-color: #fff;
    -webkit-background-clip: padding-box;
    background-clip: padding-box;
    border: 1px solid #999;
    border: 1px solid rgba(0,0,0,.2);
    border-radius: 6px;
    outline: 0;
    -webkit-box-shadow: 0 3px 9px rgba(0,0,0,.5);
    box-shadow: 0 3px 9px rgba(0,0,0,.5);
    width: 920px;
    margin-left:-23% !important;
}
</style>


<!-- PopUp for view user's profile  -->
<div id="viewUserProfileDetails" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">User Assessment Details</h4>
            </div>
            <div class="modal-body" id="assessmentDetail">                                      
                <table class="table">
                    <thead class="tbl_head">
                        <tr>
                            <th>Assessment Details</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).on('click', '.view-user-profile-details', function () {
        var userId          = $(this).data('options').userId;
        var jobId           = $(this).data('options').jobId;
        var interviewVideo  = $(this).data('options').interviewVideo;
        var accessToken     = '<?php echo $this->config->item('accessToken'); ?>';
        $("#assessmentDetail").html('');
          
        
        formData    = {userId:userId, jobId:jobId, accessToken:accessToken, interviewVideo:interviewVideo};
        $.ajax({
            url : siteUrl + "jobs/userAssessmentDetail",
            type: "POST",
            data : formData,
            cache: false,
            success: function(result)
            {
                $("#assessmentDetail").html(result);
            }
        });
    });
</script>





<!-- PopUp for view share's profile  -->
<div id="shareInterview" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Share User Interview Video</h4>
            </div>
            <div class="modal-body" id="share">                                      
                <form method="post" id="shareInterviewForm">
                <p id="errMessage"></p>
                <table class="table" border="0" bordercolor="#CCCCCC">
                    <thead class="tbl_head">
                        <tr>
                            <th colspan="3">Add Email Id to Share</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td valign="middle"><label for="inputEmail" class="control-label" style="font-weight:500">Email Id</label></td>
                            <td><input type="text" class="form-control receiverEmail"  id="receiverEmail1" placeholder="enter a valid email id"></td>
                            <td rowspan="5" align="right" width="40%">
                                <span id="userVideo"></span>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="inputEmail" class="control-label" style="font-weight:500">Email Id</label></td>
                            <td><input type="text" class="form-control receiverEmail"  id="receiverEmail2" placeholder="enter a valid email id"></td>
                        </tr>
                        <tr>
                            <td><label for="inputEmail" class="control-label" style="font-weight:500">Email Id</label></td>
                            <td><input type="text" class="form-control receiverEmail"  id="receiverEmail3" placeholder="enter a valid email id"></td>
                        </tr>
                        <tr>
                            <td><label for="inputEmail" class="control-label" style="font-weight:500">Email Id</label></td>
                            <td><input type="text" class="form-control receiverEmail"  id="receiverEmail4" placeholder="enter a valid email id"></td>
                        </tr>
                        <tr>
                            <td><label for="inputEmail" class="control-label" style="font-weight:500">Email Id</label></td>
                            <td><input type="text" class="form-control receiverEmail"  id="receiverEmail5" placeholder="enter a valid email id"></td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <span id="formButton"><button type="button" class="Save_frm" id="sendInterview">Share Interview</button></span>
                            </td>
                        </tr>
                    </tbody>
                </table>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).on('click', '.share-user-interview', function () {
        //("#errMessage").html('');
        $("#formButton").html('<button type="button" class="Save_frm" id="sendInterview">Share Interview</button>');
        document.getElementById("shareInterviewForm").reset();
        var userJob         = $(this).data('options').userJob;
        var jobId         = $(this).data('options').jobId;
        var userId         = $(this).data('options').userId;
        var interviewVideo  = $(this).data('options').interviewVideo;
        var accessToken     = '<?php echo $this->config->item('accessToken'); ?>';
        var appUserName     = $(this).data('options').appUserName;
        
        $("#errMessage").html('<h3 style="margin-top: 0px; margin-bottom: 0px;"><center>'+appUserName+'</center></h3>'); 
        $("#userVideo").html('');        
        
        formData    = {jobId:jobId, userId:userId, userJob:userJob, accessToken:accessToken, interviewVideo:interviewVideo};
        $.ajax({
            url : siteUrl + "jobs/userVideo",
            type: "POST",
            data : formData,
            cache: false,
            success: function(result)
            {
                $("#userVideo").html(result);
            }
        });
    });
    
    
    $(document).on('click', '#sendInterview', function(e) {
        // Prevent form submission
        e.preventDefault();
        
        $("#formButton").html('please wait ...');
        var email1      = $("#receiverEmail1").val();
        var email2      = $("#receiverEmail2").val();
        var email3      = $("#receiverEmail3").val();
        var email4      = $("#receiverEmail4").val();
        var email5      = $("#receiverEmail5").val();
        var videoName   = $("#videoName").val();
        var userJob     = $("#userJob").val();
        var accessToken = '<?php echo $this->config->item('accessToken'); ?>';
        
        var formData    = new FormData();
        formData.append('email1', email1);
        formData.append('email2', email2);
        formData.append('email3', email3);
        formData.append('email4', email4);
        formData.append('email5', email5);
        formData.append('videoName', videoName);
        formData.append('userJob', userJob);
        formData.append('accessToken', accessToken);
        
        if(email1=='' && email2=='' && email3=='' && email4=='' && email5=='') {
            $("#formButton").html('Please fill at least 1 email id');
        }
        else {
            $("#formButton").html('Email Successfully send');               
            setTimeout(function(){
                $(".close").trigger("click");
            }, 1000);
        }

        $.ajax({
            url: siteUrl + 'jobs/sendInterview/',
            data: formData,
            async: false,
            contentType: false,
            processData: false,
            cache: false,
            type: 'POST',
            success: function (data) {
            },
        });
        return false;
    });
</script>
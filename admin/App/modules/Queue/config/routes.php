<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['queue/viewinterview/inteviewid'] = "Queue/viewinterviewQueue";
$route['queue/viewinterview/inteviewid/(:any)'] = "Queue/viewinterviewQueue/$1";
$route['queue/viewinterview/inteviewid/(:any)/userid/(:any)'] = "Queue/viewinterviewQueue/$1/userid/$2";
$route['queue/list/(:any)'] = "Queue/listQueue/$1";
$route['queue/list'] = "Queue/listQueue";
$route['queue/pastList'] = "Queue/listQueuePast";
$route['queue/listingUser'] = "Queue/listingUser";
$route['queue/listQueueUsers'] = "Queue/listQueueUsers";
$route['queue/interviewreview'] = "Queue/viewInterviewReviewPostQueue";

$route['queue/viewinterviewdetail/inteviewid'] = "Queue/viewinterviewdetailQueue";
$route['queue/viewinterviewdetail/inteviewid/(:any)'] = "Queue/viewinterviewdetailQueue/$1";
$route['queue/viewinterviewdetail/inteviewid/(:any)/userid/(:any)'] = "Queue/viewinterviewdetailQueue/$1/userid/$2";
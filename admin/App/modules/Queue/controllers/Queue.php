<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Queue Controller
 * Description : Handle all the functions related to Queue
 * @author Synergy
 * @createddate : Nov 22, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 22, 2017
 */
class Queue extends MY_Controller {

    /**
     * Responsable for inheriting parent constructor
     * Responsable for auto load the the job_model, userjob_model, jobpanel_model
     * Responsable for auto load the form_validation, session, email library
     * Responsable for auto load fj, url helper
     * Check Uri Segment to display specific page role wise
     * @return void
     */
    function __construct() {
        parent::__construct();
		
        $this->load->model('job_model');
        $this->load->model('userjob_model');
        $this->load->model('jobpanel_model');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('email');
        $this->load->helper('fj');
        $this->load->helper('url');
		
        $token = $this->config->item('accessToken');
        /*if (isset($_POST) && $_POST != NULL) {
            if ($token != $_POST['accessToken']) {
                echo 'You do not have pemission.';
                exit;
            }
        }*/

        /**********Check any type of fraund activity*******/
        if ($this->session->userdata('logged_in')) {
            $userRealData   = $this->session->userdata['logged_in'];
            $uriStringSegment = $this->uri->uri_string();
            
            if($userRealData->role == 1) {
                $portal = 'SuperAdminPortal';
            }
            elseif ($userRealData->role == 2 || $userRealData->role == 4 || $userRealData->role == 5) {
                $portal = 'CorporatePortal';
            }
            else{
                $portal = '';
            }

            if($uriStringSegment == 'queue/list') {
                if(!checkUserAccessForWebPage($userRealData->id, $userRealData->role, $portal, 'queue/list')) {
                    $this->session->sess_destroy();
                    $insertFaultData['userId'] = $userRealData->id;
                    $insertFaultData['comment'] = 'This user try to access this this page';
                    $insertFaultData['pagePath'] = 'queue/list';
                    $this->db->insert('fj_user_access_log', $insertFaultData);
                    redirect(base_url('users/login'));
                }
            }
        } 
        /****************End of handle fraud activity****************/
    }

    /**
     * Description : List all the queues i.e assigned jobs to a specific corporate user
     * Author : Synergy
     * @param int $page, array $_POST
     * @return render queue data into view
     */
	 
	function listingUser(){
		$data['breadcrumb'] = 'ajax';
		$data['main_content'] = 'fj-queue-listing-users';
		
		$this->load->view('fj-mainpage-recuiter', $data); //die();
			
		die;
	}
	
	
    function listQueuePast($page = null) {
        $this->load->helper('fj');
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        //Jobs Listing Start
        $token = $this->config->item('accessToken');
        $userData = $this->session->userdata['logged_in'];
        
        $userLoggeinId = $userData->id;
        $userRole = $userData->role;
        
        $data['page'] = $page;
        $resp['error'] = 0;
        $serachColumn = $this->input->post('serachColumn', true);
        $searchText = $this->input->post('serachText', true);
        $token = $this->config->item('accessToken');
        $fromEmail = $this->config->item('fromEmail');
        $itemsPerPage = $this->config->item('itemsPerPage');
        $data['itemsPerPage'] = $itemsPerPage;
        $createdBy = NULL;
        $usersJobListL1L2 = "";
 
        if (!$page) {
            $page = 0;
        } else {
            $page = $page - 1;
        }
        $offset = $page * $itemsPerPage;
        try {            
            
            $allUserStrList = getAllCorporateAndSubuserIds($userLoggeinId, $userRole);
            
            if( $userRole == 2 ){
                $allUserIdsStr = $allUserStrList;
                //L1 & L2 level job list
                //$usersJobL1L2LevelListRes = $this->jobpanel_model->getAllUserJobPenalForUser($allUserIdsStr,'');
                
				
                $usersJobListL1L2 = "";
                $usersJobListL1 = "";
                $usersJobListL2 = "";
                $usersJobDoneListL2 = "";
                $userLevel = 3;
            } else {
                //L1 level job list
                $usersJobL1LevelListRes = $this->jobpanel_model->getUserJobPenalForUser($userLoggeinId, 1);

                if( $usersJobL1LevelListRes['jobIdStr'] != "" ){
                    $usersJobListL1 = $usersJobL1LevelListRes['jobIdStr'];
                    $userLevel      = 1;
                } else {
                    $usersJobListL1 = "";
                }

                //L2 level job list
                $usersJobL2LevelListRes = $this->jobpanel_model->getUserJobPenalForUser($userLoggeinId, 2);

                if( $usersJobL2LevelListRes['jobIdStr'] != "" ){
                    $usersJobListL2     = $usersJobL2LevelListRes['jobIdStr'];                       
                    $allJobDoneByL2Res  = $this->userjob_model->allDoneJobsByL1($usersJobListL2);
                    $usersJobDoneListL2 = $allJobDoneByL2Res['jobIdL2Str'];                    
                    $userLevel          = 2;
                } else {
                    $usersJobListL2     = "";
                    $usersJobDoneListL2 = "";
                }
            }
			if($userRole == 2){
				$result = $this->job_model->listJobsPast($itemsPerPage, $offset, $serachColumn, $searchText, $createdBy, $usersJobListL1, $usersJobDoneListL2, $usersJobListL1L2, $userRole,$allUserIdsStr);
			}else if( $userRole == 4 || $userRole == 5 ) {
				$result             = $this->job_model->listJobsPast($itemsPerPage, $offset, $serachColumn, $searchText, $createdBy, $usersJobListL1, $usersJobDoneListL2, $usersJobListL1L2, $userRole);  
					
			}else{
				$result             = $this->job_model->listJobs($itemsPerPage, $offset, $serachColumn, $searchText, $createdBy, $usersJobListL1, $usersJobDoneListL2, $usersJobListL1L2, $userRole);            
			}
			
            $completeResult     = $this->job_model->listJobs(NULL, NULL, NULL, NULL, $createdBy, $usersJobListL1, $usersJobDoneListL2, $usersJobListL1L2, $userRole);
            $count              = ceil(count($completeResult) / $itemsPerPage);
            $data['count']      = $count;
            $data['totalCount'] = count($completeResult);
			
            $data['content']    = $result;
            
            $data['userLoggeinId']  = $userLoggeinId;
			$data['AdminLoggedInuserId']  = $userLoggeinId;
            $data['userLevel']      = $userLevel;
            $data['searchText']     = $searchText;
            $data['serachColumn']   = $serachColumn;
            $data['token']          = $token;
            if( $userRole == 2  || $userRole == 4   || $userRole == 5) {
				
                $data['main_content'] = 'fj-queue-listing-past';
            } else {
                $data['main_content'] = 'fj-subuser-queue-listing';
            }
			$data['breadcrumb'] = 'applicants';
            $data['usersJobListL1']  = $usersJobListL1;
            $data['usersJobListL2']  = $usersJobListL2; 
            $data['userData']  = $userData; 
			
			//added code for the permission
			$userId = getUserId();
			$userDet = getUserById($userId);
			$userRoleForMenu = $userDet['role'];

			if( $userRoleForMenu == 1 ) {
				$urlDashborad = 'superadmin/dashboard';
			}
			else if( $userRoleForMenu == 2 || $userRoleForMenu == 4 || $userRoleForMenu == 5 ) {
				$urlDashborad = 'corporate/dashboard'; 
			}

			//USER'S PERMISSION
			$userPermissionStr = "";
			$userPermissionArr = array();

			$userPermissionRes = dashboardUserPermissions($userId);

			if( $userPermissionRes ){
				$userPermissionStr = $userPermissionRes->permissionIds;
				$userPermissionArr = explode(",", $userPermissionStr);
			}
			//ALL PERMISSION FOR ADMIN
			$adminPermission = array(6,9,10,11,12,8);
			$corporatePermission = array(1,2,3,4,5,7,8);
			//SIDE MENU FOR LISTING
			$sideBarMenu = getMenuForSideBar();

			$currentFullBaseUrl = base_url(uri_string());
			$currentBaseUrl = str_replace(base_url(), "", $currentFullBaseUrl);
			$data['userPermissionArr'] = $userPermissionArr;
			$data['userRoleForMenu'] = $userRoleForMenu;
			//till here added code for the permission
			
			
			if($userRole == 2   || $userRole == 4   || $userRole == 5){
				
				$this->load->view('fj-mainpage-recuiter', $data); //die();
			}else{
				$this->load->view('fj-mainpage', $data); //die();
			}
            
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
	
    function listQueue($page = null) {
        $this->load->helper('fj');
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        //Jobs Listing Start
        $token = $this->config->item('accessToken');
        $userData = $this->session->userdata['logged_in'];
        
        $userLoggeinId = $userData->id;
        $userRole = $userData->role;
        
        $data['page'] = $page;
        $resp['error'] = 0;
        $serachColumn = $this->input->post('serachColumn', true);
        $searchText = $this->input->post('serachText', true);
        $token = $this->config->item('accessToken');
        $fromEmail = $this->config->item('fromEmail');
        $itemsPerPage = $this->config->item('itemsPerPage');
        $data['itemsPerPage'] = $itemsPerPage;
        $createdBy = NULL;
        $usersJobListL1L2 = "";
 
        if (!$page) {
            $page = 0;
        } else {
            $page = $page - 1;
        }
        $offset = $page * $itemsPerPage;
        try {            
            
            //$allUserStrList = getAllCorporateAndSubuserIds($userLoggeinId, $userRole);
            
            if( $userRole == 2){
                //L1 level job list
                $usersJobL1LevelListRes = $this->jobpanel_model->getUserJobPenalForUser($userLoggeinId);

                if( $usersJobL1LevelListRes['jobIdStr'] != "" ){
                    $usersJobListL1 = $usersJobL1LevelListRes['jobIdStr'];
                    $userLevel      = 1;
                } else {
                    $usersJobListL1 = "";
                }
				$usersJobDoneListL2 = "";
				$usersJobListL2 = "";
                //L2 level job list
                /*$usersJobL2LevelListRes = $this->jobpanel_model->getUserJobPenalForUser($userLoggeinId, 2);

                if( $usersJobL2LevelListRes['jobIdStr'] != "" ){
                    $usersJobListL2     = $usersJobL2LevelListRes['jobIdStr'];                       
                    $allJobDoneByL2Res  = $this->userjob_model->allDoneJobsByL1($usersJobListL2);
                    $usersJobDoneListL2 = $allJobDoneByL2Res['jobIdL2Str'];                    
                    $userLevel          = 2;
                } else {
                    $usersJobListL2     = "";
                    $usersJobDoneListL2 = "";
                }*/
            } else {
                //L1 level job list
                $usersJobL1LevelListRes = $this->jobpanel_model->getUserJobPenalForUser($userLoggeinId, 1);

                if( $usersJobL1LevelListRes['jobIdStr'] != "" ){
                    $usersJobListL1 = $usersJobL1LevelListRes['jobIdStr'];
                    $userLevel      = 1;
                } else {
                    $usersJobListL1 = "";
                }

                //L2 level job list
                $usersJobL2LevelListRes = $this->jobpanel_model->getUserJobPenalForUser($userLoggeinId, 2);

                if( $usersJobL2LevelListRes['jobIdStr'] != "" ){
                    $usersJobListL2     = $usersJobL2LevelListRes['jobIdStr'];                       
                    $allJobDoneByL2Res  = $this->userjob_model->allDoneJobsByL1($usersJobListL2);
                    $usersJobDoneListL2 = $allJobDoneByL2Res['jobIdL2Str'];                    
                    $userLevel          = 2;
                } else {
                    $usersJobListL2     = "";
                    $usersJobDoneListL2 = "";
                }
            }
			
			if( $userRole == 2  || $userRole == 4 || $userRole == 5) {
				$result = $this->job_model->listJobsNew($itemsPerPage, $offset, $serachColumn, $searchText, $createdBy, $usersJobListL1, $usersJobDoneListL2, $usersJobListL1L2, $userRole);
				
			} else {
				$result = $this->job_model->listJobs($itemsPerPage, $offset, $serachColumn, $searchText, $createdBy, $usersJobListL1, $usersJobDoneListL2, $usersJobListL1L2, $userRole);            
			}
			
            $completeResult = $this->job_model->listJobs(NULL, NULL, NULL, NULL, $createdBy, $usersJobListL1, $usersJobDoneListL2, $usersJobListL1L2, $userRole);
            $count              = ceil(count($completeResult) / $itemsPerPage);
            $data['count']      = $count;
            $data['totalCount'] = count($completeResult);
            $data['content']    = $result;
            
            $data['userLoggeinId']  = $userLoggeinId;
            $data['AdminLoggedInuserId']  = $userLoggeinId;
            $data['userLevel']      = $userLevel;
            $data['searchText']     = $searchText;
            $data['serachColumn']   = $serachColumn;
            $data['token']          = $token;
			
			//added code for the permission
			$userId = getUserId();
			$userDet = getUserById($userId);
			$userRoleForMenu = $userDet['role'];

			if( $userRoleForMenu == 1 ) {
				$urlDashborad = 'superadmin/dashboard';
			}
			else if( $userRoleForMenu == 2 || $userRoleForMenu == 4 || $userRoleForMenu == 5 ) {
				$urlDashborad = 'corporate/dashboard'; 
			}

			//USER'S PERMISSION
			$userPermissionStr = "";
			$userPermissionArr = array();

			$userPermissionRes = dashboardUserPermissions($userId);

			if( $userPermissionRes ){
				$userPermissionStr = $userPermissionRes->permissionIds;
				$userPermissionArr = explode(",", $userPermissionStr);
			}
			//ALL PERMISSION FOR ADMIN
			$adminPermission = array(6,9,10,11,12,8);
			$corporatePermission = array(1,2,3,4,5,7,8);
			//SIDE MENU FOR LISTING
			$sideBarMenu = getMenuForSideBar();

			$currentFullBaseUrl = base_url(uri_string());
			$currentBaseUrl = str_replace(base_url(), "", $currentFullBaseUrl);
			$data['userPermissionArr'] = $userPermissionArr;
			$data['userRoleForMenu'] = $userRoleForMenu;
			//till here added code for the permission
			
            if( $userRole == 2  || $userRole == 4) {
				
                $data['main_content'] = 'fj-queue-listing';
            } else {
                $data['main_content'] = 'fj-subuser-queue-listing';
            }
			$data['breadcrumb'] = 'applicants';
            $data['usersJobListL1']  = $usersJobListL1;
            $data['usersJobListL2']  = $usersJobListL2; 
            $data['userData']  = $userData; 
			if($userRole == 2 || $userRole == 4  || $userRole == 5){
				
				$this->load->view('fj-mainpage-recuiter', $data); //die();
			}else{
				$this->load->view('fj-mainpage', $data); //die();
			}
            
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
	
	function listQueueUsers($page = null) {
        $this->load->helper('fj');
		
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        //Jobs Listing Start
        $token = $this->config->item('accessToken');
        $userData = $this->session->userdata['logged_in'];
        
        $userLoggeinId = $userData->id;
        $userRole = $userData->role;
        
        $data['page'] = $page;
        $resp['error'] = 0;
        $serachColumn = $this->input->post('serachColumn', true);
        $searchText = $this->input->post('serachText', true);
        $token = $this->config->item('accessToken');
        $fromEmail = $this->config->item('fromEmail');
        $itemsPerPage = $this->config->item('itemsPerPage');
        $data['itemsPerPage'] = $itemsPerPage;
        $createdBy = NULL;
        $usersJobListL1L2 = "";
	
        if (!$page) {
            $page = 0;
        } else {
            $page = $page - 1;
        }
        $offset = $page * $itemsPerPage;
        try {            
            
            $allUserStrList = getAllCorporateAndSubuserIds($userLoggeinId, $userRole);
            
            if( $userRole == 2  || $userRole == 4){
                $allUserIdsStr = $allUserStrList;
                //L1 & L2 level job list
                $usersJobL1L2LevelListRes = $this->jobpanel_model->getAllUserJobPenalForUser($allUserIdsStr, $levelsss=null);
                
                if( $usersJobL1L2LevelListRes['jobIdStr'] != "" ){
                    $usersJobListL1L2 = $usersJobL1L2LevelListRes['jobIdStr'];
                    $usersJobListL1 = "";
                    $usersJobListL2     = "";
                    $usersJobDoneListL2 = "";
                } else {
                    $usersJobListL1L2 = "";
                    $usersJobListL1 = "";
                    $usersJobListL2     = "";
                    $usersJobDoneListL2 = "";
                } 
                
                $userLevel = 3;
            } else {
                //L1 level job list
                $usersJobL1LevelListRes = $this->jobpanel_model->getUserJobPenalForUser($userLoggeinId, 1);

                if( $usersJobL1LevelListRes['jobIdStr'] != "" ){
                    $usersJobListL1 = $usersJobL1LevelListRes['jobIdStr'];
                    $userLevel      = 1;
                } else {
                    $usersJobListL1 = "";
                }

                //L2 level job list
                $usersJobL2LevelListRes = $this->jobpanel_model->getUserJobPenalForUser($userLoggeinId, 2);

                if( $usersJobL2LevelListRes['jobIdStr'] != "" ){
                    $usersJobListL2     = $usersJobL2LevelListRes['jobIdStr'];                       
                    $allJobDoneByL2Res  = $this->userjob_model->allDoneJobsByL1($usersJobListL2);
                    $usersJobDoneListL2 = $allJobDoneByL2Res['jobIdL2Str'];                    
                    $userLevel          = 2;
                } else {
                    $usersJobListL2     = "";
                    $usersJobDoneListL2 = "";
                }
            }
			if( $userRole == 2  || $userRole == 4) {
            $result             = $this->job_model->listJobsNew($itemsPerPage, $offset, $serachColumn, $searchText, $createdBy, $usersJobListL1, $usersJobDoneListL2, $usersJobListL1L2, $userRole);            
			}else{
				$result             = $this->job_model->listJobs($itemsPerPage, $offset, $serachColumn, $searchText, $createdBy, $usersJobListL1, $usersJobDoneListL2, $usersJobListL1L2, $userRole);            
			}
			
            $completeResult     = $this->job_model->listJobs(NULL, NULL, NULL, NULL, $createdBy, $usersJobListL1, $usersJobDoneListL2, $usersJobListL1L2, $userRole);
            $count              = ceil(count($completeResult) / $itemsPerPage);
            $data['count']      = $count;
            $data['totalCount'] = count($completeResult);
            $data['content']    = $result;
            
            $data['userLoggeinId']  = $userLoggeinId;
			$data['AdminLoggedInuserId']  = $userLoggeinId;
            $data['userLevel']      = $userLevel;
            $data['searchText']     = $searchText;
            $data['serachColumn']   = $serachColumn;
            $data['token']          = $token;
            if( $userRole == 2 ||  $userRole == 4 ||  $userRole == 5 ) {
				
                $data['main_content'] = 'fj-queue-listing-users-data';
            } else {
                $data['main_content'] = 'fj-subuser-queue-listing';
            }
			$data['breadcrumb'] = 'ajax';
            $data['usersJobListL1']  = $usersJobListL1;
            $data['usersJobListL2']  = $usersJobListL2; 
            $data['userData']  = $userData; 
			if($userRole == 2  || $userRole == 4){
				
				$this->load->view('fj-mainpage-recuiter', $data); //die();
			}else{
				$this->load->view('fj-mainpage', $data); //die();
			}
            
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
	
	
	
    /**
     * Description : List all the interviews in a specific jobs to assigned specific corporate user
     * Author : Synergy
     * @param int $page
     * @return render interviews data into view
     */
    function viewinterviewQueue() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }

        $token              = $this->config->item('accessToken');
        $userData           = $this->session->userdata['logged_in'];        
        $userTypeRole       = $userData->role;
        $userByFullName     = $userData->fullname;
        $createdForId       = $userData->createdBy;
        $createdEmailId     = $userData->email;
        $createdByUserId    = $userData->id;
        
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            $userId = 1;
        }
        
        $rightanswer        = 0;
        $wronganswer        = 0;
        $totalquestioncount = 0;
        $totalpercentage    = 0;
        $userjobdetails     = array();
        $resp['error']      = 0;
        
        $interviewId        = $this->uri->segment(4);
        $userIntrvId        = $this->uri->segment(6);
        
        try {    
            $userjobdetails = $this->userjob_model->detailsForUserJobs($interviewId, $userIntrvId);             
            $jobId          = $userjobdetails['jobId'];
            
            if( $userTypeRole == 2 ) {                    
                $userJobDataRes = $this->userjob_model->getUserJobDetail($interviewId);
                if( $userJobDataRes ){
                    $userJobStatus = $userJobDataRes['status'];
                    if( $userJobStatus == 1 ){                        
                        $userLevelValue = 1;                        
                    } else if( $userJobDataRes != 1 ){                        
                        $userLevelValue = 2;                                         
                    }
                }
            }
            else {
                $jobleveldetails    = $this->jobpanel_model->getUserJobLevel($jobId, $userId);
                $userLevelValue     = $jobleveldetails['level'];
            }
            
			$statictoken = 'ecbcd7eaee29848978134beeecdfbc7c';
			$base_url = base_url().'Fjapi';
			
			$json = array("token" => $statictoken, "methodname" => 'getUserJobQuestionsAnswers', 'jobId' => $jobId, 'userId' => $userIntrvId);                                                                    
			$data_string = json_encode($json);                
			
			$ch = curl_init($base_url);                                                                      
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
			curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
				'Content-Type: application/json',                                                                                
				'Content-Length: ' . strlen($data_string))                                                                       
			);                                                                                                                  
																																 
			$result = curl_exec($ch);
			curl_close ($ch);
			$allResult = json_decode($result);
			$questionsListDetails = $allResult->questionsList;
			$data['questionsListDetails'] = $questionsListDetails;
			
			
            $jobApplicationDetail   = $this->job_model->userAssessmentDetail($userjobdetails['userId'], $jobId);
            
            if( $jobApplicationDetail ){
                foreach($jobApplicationDetail as $assessmentDetail) {                    
                    $totalquestioncount = $totalquestioncount+1;
                    if( $assessmentDetail->userOption == $assessmentDetail->option ){
                        $rightanswer = $rightanswer+1;
                    } else {
                        $wronganswer = $wronganswer+1;
                    }
                }
            }
            $jobDetail  = $this->job_model->getJob($userjobdetails['jobId']);
            
            $totalpercentage                        = number_format((( $rightanswer / $totalquestioncount ) * 100), 2, '.', '');
            $data['interview_content']              = $userjobdetails;   
            $data['totalpercentage']                = $totalpercentage;  
            $data['interview_data']['interviewid']  = $interviewId;
            $data['interview_data']['userid']       = $userIntrvId;
            $data['job_id']                         = $jobId;
            $data['mergedVideo']                    = $userjobdetails['mergedVideo'];
            $data['user_role_value']                = $userLevelValue;
            $data['jobApplicationDetail']           = $jobApplicationDetail;
            $data['token']                          = $token;            
            $data['job_code']                       = $jobDetail['fjCode'];
            $data['user_id']                        = $userjobdetails['userId'];
            $data['interviewComment']               = $this->userjob_model->interviewComment($userjobdetails['id']);
			
            if($userjobdetails['mergedVideo'] != ""){
				$data['main_content'] = 'fj-queue-viewinterview-for-flv';
			}else if($questionsListDetails[0]->questionFile == "" || $questionsListDetails[0]->questionFile == "0"){
				
				$data['main_content'] = 'fj-queue-viewinterview-for-text';
			}else{ 
				$data['main_content'] = 'fj-queue-viewinterview';
			}
            $this->load->view('fj-mainpage', $data); //die();
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Description : Use to view an interview given by specific user on specific job and write review(comment), shortlist or reject candidate based upon his/her interview
     * Author : Synergy
     * @param int or array $_POST
     * @return render interviews data into view
     */
    function viewInterviewReviewPostQueue() {
        
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token = $this->config->item('accessToken');
        $userData = $this->session->userdata['logged_in'];
        
        $userTypeRole = $userData->role;
        $userByFullName = ($userData->fullname==''?$userData->company:$userData->fullname);
        $createdForId = $userData->createdBy;
        $createdEmailId = $userData->email;
        $createdByUserId = $userData->id;
        
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            $userId = 1;
        }
        
        if (isset($_POST) && $_POST != NULL) {            
            $hdenjobid      = $this->input->post('hdenjobid', true);            
            $jobData        = $this->job_model->getJob($hdenjobid);            
            $hdnusrid       = $this->input->post('hdnusrid', true);            
            $hdninterviewid = $this->input->post('hdninterviewid', true);
            $resps = array();
            if ($token != $this->input->post('accessToken', true)) {
                $resps['error'] = 'You do not have pemission.';
            }
            
            $this->form_validation->set_rules('interview_comment', 'interview_comment', 'trim|required');
            $this->form_validation->set_rules('interview_status', 'interview_status', 'trim|required');
            
            if ($this->form_validation->run() == false) {
                $resps['error'] = validation_errors();
            }
                        
            if (!isset($resps['error']) && empty($resps['error'])) {             
                $user       = getUserById($hdnusrid);                
                $jobComment = $this->input->post('interview_comment', true);                                
                $status     = $this->input->post('interview_status', true);     
                $data['jobComment'] = $jobComment;
                $data['status']     = $status;  
                
                if( $userTypeRole == 2 ) {               
                    $userJobDataRes = $this->userjob_model->getUserJobDetail($hdninterviewid);
                    if( $userJobDataRes ){
                        $userJobStatus = $userJobDataRes['status'];                        
                        if( $userJobStatus == 1 ){
                            $userLevelValue = 1;
                            $data['shorlistedByL1UserDate']         = date('Y-m-d'); 
                            $data['shorlistedByCorporateForL1User'] = $userId;
                            $jobuserfromleveldetails                = $this->jobpanel_model->getUserFromJobLevel($hdenjobid, 1);                            
                            if( $jobuserfromleveldetails ){
                                $data['shorlistedByL1User']         = $jobuserfromleveldetails['userId'];
                            }                    
                        } 
                        else if( $userJobDataRes != 1 ){
                            $userLevelValue = 2; 
                            $data['shorlistedByL2UserDate']         = date('Y-m-d');
                            $data['shorlistedByCorporateForL2User'] = $userId;                            
                            $jobuserfromleveldetails                = $this->jobpanel_model->getUserFromJobLevel($hdenjobid, 2);                            
                            if( $jobuserfromleveldetails ){
                                $data['shorlistedByL2User']         = $jobuserfromleveldetails['userId'];
                            }                    
                        }
                    }                    
                } 
                else {
                    $jobleveldetails    = $this->jobpanel_model->getUserJobLevel($hdenjobid, $userId);                    
                    $userLevelValue     = $jobleveldetails['level'];
					
                    if( $userLevelValue == 1 ){
                        $data['shorlistedByL1UserDate'] = date('Y-m-d');
                        $data['shorlistedByL1User']     = $userId;
                    } else if( $userLevelValue == 2 ){                        
                        $data['shorlistedByL2UserDate'] = date('Y-m-d');
                        $data['shorlistedByL2User'] = $userId;
                    }   
                }                
                $data['updatedAt'] = date("Y-m-d H:i:s");
                try {
					$data['rating'] = isset($_POST['adminRating'])?$_POST['adminRating']:0;
                    $resp = $this->userjob_model->updateJobInterview($data, $hdninterviewid);                    
                    if ($resp) {                       
                        if( $userLevelValue == 1 ){
                            $joblevelcheck = $this->jobpanel_model->checkJobLevel($hdenjobid, 2);                        
                            if( $joblevelcheck ) {
                                $fromEmail = $this->config->item('fromEmail');                                
                                if( $status == 2 ) {
                                    $subjectEmail = "User shortlisted by L1 user";                                    
                                    /* notification message for corporate admin */
                                    $notificationMsg = ucwords($user['fullname'])." is shortlisted by L1 User ".ucwords($userByFullName). " for the Job ". $jobData['title']." for 1st level";
                                    insertNotificationForTopBarInAdmin($createdForId, $notificationMsg, "Job Shortlist", $createdByUserId);

                                    // new line
                                    $notiData['userId']                 = $user['id'];
                                    $notiData['notificationType']       = 'JobApplicationStatus';
                                    $notiData['notificationTitle']      = 'Interview Status';
                                    $notiData['notificationMessage']    = $notificationMsg;
                                    $notiData['flag']                   = '1';
                                    $this->db->insert('fj_userNotificationList', $notiData);
            
                                    $notificationData       = array("notificationType"=>"JobApplicationStatus");                                    
                                    $params['device_token'] = array($user['deviceId']);
                                    $params['title']        = 'Interview Status';
                                    $params['message']      = cleanString($notificationMsg);
                                    $params['data']         = json_encode($notificationData);
                                    //send_gcm_notify($params);
                                    $this->userId = $hdnusrid;
                                    $this->type = 'shortlisted';
                                    $this->jobCode = $jobData['fjCode'];
                                    processApiSuccess($this);
                                    // new line                   
                                }
                                else if( $status == 3 ) {
                                    $subjectEmail = "User rejected by L1 user";                                    
                                    /* notification message for corporate admin */
                                    $notificationMsg = ucwords($user['fullname'])." is rejected by ".ucwords($userByFullName). " for the Job ". $jobData['title']." for 1st level";
                                    insertNotificationForTopBarInAdmin($createdForId, $notificationMsg, "Job Rejected", $createdByUserId);

                                    // new line
                                    $notiData['userId']                 = $user['id'];
                                    $notiData['notificationType']       = 'JobApplicationStatus';
                                    $notiData['notificationTitle']      = 'Interview Status';
                                    $notiData['notificationMessage']    = $notificationMsg;
                                    $notiData['flag']                   = '1';
                                    $this->db->insert('fj_userNotificationList', $notiData);
                                    
                                    $notificationData       = array("notificationType"=>"JobApplicationStatus");                                    
                                    $params['device_token'] = array($user['deviceId']);
                                    $params['title']        = 'Interview Status';
                                    $params['message']      = cleanString($notificationMsg);
                                    $params['data']         = json_encode($notificationData);
                                    //send_gcm_notify($params);
                                    $this->userId = $hdnusrid;
                                    $this->type = 'rejected';
                                    $this->jobCode = $jobData['fjCode'];
                                    processApiSuccess($this);
                                    // new line
                                } else {
                                    $subjectEmail = "Candidate is put on hold by L1 user";                                    
                                    /* notification message for corporate admin */
                                    $notificationMsg = ucwords($user['fullname'])." is put on hold by ".ucwords($userByFullName). " for the Job ". $jobData['title']." for 1st level";
                                    insertNotificationForTopBarInAdmin($createdForId, $notificationMsg, "On Hold", $createdByUserId);

                                    // new line
                                    $notiData['userId']                 = $user['id'];
                                    $notiData['notificationType']       = 'JobApplicationStatus';
                                    $notiData['notificationTitle']      = 'Interview Status';
                                    $notiData['notificationMessage']    = $notificationMsg;
                                    $notiData['flag']                   = '1';
                                    $this->db->insert('fj_userNotificationList', $notiData);
                                    
                                    $notificationData       = array("notificationType"=>"JobApplicationStatus");                                    
                                    $params['device_token'] = array($user['deviceId']);
                                    $params['title']        = 'Interview Status';
                                    $params['message']      = cleanString($notificationMsg);
                                    $params['data']         = json_encode($notificationData);
                                    //send_gcm_notify($params);
                                    $this->userId = $hdnusrid;
                                    $this->type = 'onhold';
                                    $this->jobCode = $jobData['fjCode'];
                                    processApiSuccess($this);
                                }

                                $this->email->from($fromEmail, 'VDOHire - User Action');
                                $this->email->subject($subjectEmail);
                                $this->email->set_mailtype('html');                                
                                foreach ($joblevelcheck as $item) {
                                    if( $status == 2 ) {
                                        /* notification message for L2 user */
                                        $notificationMsg = ucwords($user['fullname'])." is shortlisted by L1 User ".ucwords($userByFullName). " for the Job ". $jobData['title']." for 1st level";
                                        insertNotificationForTopBarInAdmin($item['userid'], $notificationMsg, "Job Shortlist", $createdByUserId);

                                        // new line
                                        $notiData['userId']                 = $user['id'];
                                        $notiData['notificationType']       = 'JobApplicationStatus';
                                        $notiData['notificationTitle']      = 'Interview Status';
                                        $notiData['notificationMessage']    = $notificationMsg;
                                        $notiData['flag']                   = '1';
                                        $this->db->insert('fj_userNotificationList', $notiData);
                                    
                                        $notificationData       = array("notificationType"=>"JobApplicationStatus");                                    
                                        $params['device_token'] = array($user['deviceId']);
                                        $params['title']        = 'Interview Status';
                                        $params['message']      = cleanString($notificationMsg);
                                        $params['data']         = json_encode($notificationData);
                                        //send_gcm_notify($params);
                                        /*$this->userId = $hdnusrid;
                                        $this->type = 'shortlisted';
                                        $this->jobCode = $jobData['fjCode'];
                                        processApiSuccess($this);*/
                                        // new line
                                    } 
                                    else if( $status == 3 ) {
                                        /* notification message for L2 user */
                                        $notificationMsg = ucwords($user['fullname'])." is rejected by L1 User ".ucwords($userByFullName). " for the Job ". $jobData['title']." for 1st level";
                                        //insertNotificationForTopBarInAdmin($item['userid'], $notificationMsg, "Job Rejected", $createdByUserId);

                                        // new line
                                        $notiData['userId']                 = $user['id'];
                                        $notiData['notificationType']       = 'JobApplicationStatus';
                                        $notiData['notificationTitle']      = 'Interview Status';
                                        $notiData['notificationMessage']    = $notificationMsg;
                                        $notiData['flag']                   = '1';
                                        $this->db->insert('fj_userNotificationList', $notiData);
                                        
                                        $notificationData       = array("notificationType"=>"JobApplicationStatus");                                    
                                        $params['device_token'] = array($user['deviceId']);
                                        $params['title']        = 'Interview Status';
                                        $params['message']      = cleanString($notificationMsg);
                                        $params['data']         = json_encode($notificationData);
                                        //send_gcm_notify($params);
                                        /*$this->userId = $hdnusrid;
                                        $this->type = 'rejected';
                                        $this->jobCode = $jobData['fjCode'];
                                        processApiSuccess($this);*/
                                    } else {
                                        /* notification message for L2 user */
                                        $notificationMsg = ucwords($user['fullname'])." is put on hold by L1 User ".ucwords($userByFullName). " for the Job ". $jobData['title']." for 1st level";
                                        //insertNotificationForTopBarInAdmin($item['userid'], $notificationMsg, "Job Rejected", $createdByUserId);

                                        // new line
                                        $notiData['userId']                 = $user['id'];
                                        $notiData['notificationType']       = 'JobApplicationStatus';
                                        $notiData['notificationTitle']      = 'Interview Status';
                                        $notiData['notificationMessage']    = $notificationMsg;
                                        $notiData['flag']                   = '1';
                                        $this->db->insert('fj_userNotificationList', $notiData);
                                        
                                        $notificationData       = array("notificationType"=>"JobApplicationStatus");                                    
                                        $params['device_token'] = array($user['deviceId']);
                                        $params['title']        = 'Interview Status';
                                        $params['message']      = cleanString($notificationMsg);
                                        $params['data']         = json_encode($notificationData);
                                        //send_gcm_notify($params);
                                        /*$this->userId = $hdnusrid;
                                        $this->type = 'onhold';
                                        $this->jobCode = $jobData['fjCode'];
                                        processApiSuccess($this);*/
                                    }

                                    $temp['username']                       = $item['fullname'];
                                    $temp['jobtitle']                       = $jobData['title'];
                                    $temp['interviewGivenByUsername']       = $user['fullname'];
                                    $temp['interviewSubmitedByUsername']    = $userByFullName;                                    
                                    if( $status == 2 ) {
                                        $temp['jobstatus'] = "Shortlisted";
                                    } else if( $status == 3 ) {
                                        $temp['jobstatus'] = "Rejected";
                                    } else {
                                        $temp['jobstatus'] = "Put On Hold";
                                    }

                                    if($status == 2) {
                                        $toemail    = $item['email'];
                                        $body       = $this->load->view('emails/selectforJobTemp.php', $temp, TRUE);
                                        $this->email->to($toemail);
                                        $this->email->message($body);
                                        $this->email->send();
                                    }
                                }
                            }
                            else {                                
                                $userCreatedByDetails   = getUserById($createdForId);
                                $fromEmail              = $this->config->item('fromEmail');                                
                                if( $status == 2 ) {
                                    $subjectEmail       = "User shortlisted by L1 user";                                    
                                    /* notification message for corporate admin */
                                    $notificationMsg    = ucwords($user['fullname'])." is shortlisted by L1 User ".ucwords($userByFullName). " for the Job ". $jobData['title']." for 1st level";
                                    insertNotificationForTopBarInAdmin($createdForId, $notificationMsg, "Job Shortlist", $createdByUserId);

                                    // new line
                                    $notiData['userId']                 = $user['id'];
                                    $notiData['notificationType']       = 'JobApplicationStatus';
                                    $notiData['notificationTitle']      = 'Interview Status';
                                    $notiData['notificationMessage']    = $notificationMsg;
                                    $notiData['flag']                   = '1';
                                    $this->db->insert('fj_userNotificationList', $notiData);
                                    
                                    $notificationData       = array("notificationType"=>"JobApplicationStatus");                                    
                                    $params['device_token'] = array($user['deviceId']);
                                    $params['title']        = 'Interview Status';
                                    $params['message']      = cleanString($notificationMsg);
                                    $params['data']         = json_encode($notificationData);
                                    //send_gcm_notify($params);
                                    $this->userId = $hdnusrid;
                                    $this->type = 'shortlisted';
                                    $this->jobCode = $jobData['fjCode'];
                                    processApiSuccess($this);
                                    // new line
                                }
                                else if( $status == 3 ) {
                                    $subjectEmail       = "User rejected by L1 user";                                    
                                    /* notification message for corporate admin */
                                    $notificationMsg    = ucwords($user['fullname'])." is rejected by L1 User ".ucwords($userByFullName). " for the Job ". $jobData['title']." for 1st level";
                                    insertNotificationForTopBarInAdmin($createdForId, $notificationMsg, "Job Rejected", $createdByUserId);

                                    // new line
                                    $notiData['userId']                 = $user['id'];
                                    $notiData['notificationType']       = 'JobApplicationStatus';
                                    $notiData['notificationTitle']      = 'Interview Status';
                                    $notiData['notificationMessage']    = $notificationMsg;
                                    $notiData['flag']                   = '1';
                                    $this->db->insert('fj_userNotificationList', $notiData);
                                    
                                    $notificationData       = array("notificationType"=>"JobApplicationStatus");                                    
                                    $params['device_token'] = array($user['deviceId']);
                                    $params['title']        = 'Interview Status';
                                    $params['message']      = cleanString($notificationMsg);
                                    $params['data']         = json_encode($notificationData);
                                    //send_gcm_notify($params);
                                    $this->userId = $hdnusrid;
                                    $this->type = 'rejected';
                                    $this->jobCode = $jobData['fjCode'];
                                    processApiSuccess($this);
                                } else {
                                    $subjectEmail       = "Candidate is put on hold by L1 user";                                    
                                    /* notification message for corporate admin */
                                    $notificationMsg    = ucwords($user['fullname'])." is put on hold by L1 User ".ucwords($userByFullName). " for the Job ". $jobData['title']." for 1st level";
                                    insertNotificationForTopBarInAdmin($createdForId, $notificationMsg, "Job Rejected", $createdByUserId);

                                    // new line
                                    $notiData['userId']                 = $user['id'];
                                    $notiData['notificationType']       = 'JobApplicationStatus';
                                    $notiData['notificationTitle']      = 'Interview Status';
                                    $notiData['notificationMessage']    = $notificationMsg;
                                    $notiData['flag']                   = '1';
                                    $this->db->insert('fj_userNotificationList', $notiData);
                                    
                                    $notificationData       = array("notificationType"=>"JobApplicationStatus");                                    
                                    $params['device_token'] = array($user['deviceId']);
                                    $params['title']        = 'Interview Status';
                                    $params['message']      = cleanString($notificationMsg);
                                    $params['data']         = json_encode($notificationData);
                                    //send_gcm_notify($params);
                                    $this->userId = $hdnusrid;
                                    $this->type = 'onhold';
                                    $this->jobCode = $jobData['fjCode'];
                                    processApiSuccess($this);
                                }
                                $this->email->from($fromEmail, 'VDOHire - User Action');
                                $this->email->subject($subjectEmail);
                                $this->email->set_mailtype('html');
                                
                                $temp['username']                       = $userCreatedByDetails['username'];
                                $temp['jobtitle']                       = $jobData['title'];
                                $temp['interviewGivenByUsername']       = $user['fullname'];
                                $temp['interviewSubmitedByUsername']    = $userByFullName;

                                if( $status == 2 ) {
                                    $temp['jobstatus'] = "Shortlisted";
                                } else if( $status == 3 ) {
                                    $temp['jobstatus'] = "Rejected";
                                } else {
                                    $temp['jobstatus'] = "Put on Hold";
                                }
                                
                                $toemail    = $userCreatedByDetails['email'];
                                $body       = $this->load->view('emails/selectforJobTemp.php', $temp, TRUE);
                                $this->email->to($toemail);
                                $this->email->message($body);
                                $this->email->send();                                
                            }                            
                        }
                        elseif( $userLevelValue == 2 ){                    
                            $userCreatedByDetails   = getUserById($createdForId);
                            $fromEmail              = $this->config->item('fromEmail');
                            if( $status == 2 ) {
                                $subjectEmail       = "User shortlisted by L2 user";
                                /* notification message for corporate admin */
                                $notificationMsg    = ucwords($user['fullname'])." is shortlisted by L2 User ".ucwords($userByFullName). " for the Job ". $jobData['title']." for 2nd level";
                                insertNotificationForTopBarInAdmin($createdForId, $notificationMsg, "Job Shortlist", $createdByUserId);

                                // new line
                                $notiData['userId']                 = $user['id'];
                                $notiData['notificationType']       = 'JobApplicationStatus';
                                $notiData['notificationTitle']      = 'Interview Status';
                                $notiData['notificationMessage']    = $notificationMsg;
                                $notiData['flag']                   = '1';
                                $this->db->insert('fj_userNotificationList', $notiData);
                                
                                $notificationData       = array("notificationType"=>"JobApplicationStatus");                                    
                                $params['device_token'] = array($user['deviceId']);
                                $params['title']        = 'Interview Status';
                                $params['message']      = cleanString($notificationMsg);
                                $params['data']         = json_encode($notificationData);
                                //send_gcm_notify($params);
                                $this->userId = $hdnusrid;
                                $this->type = 'shortlisted';
                                $this->jobCode = $jobData['fjCode'];
                                processApiSuccess($this);

                            } else if( $status == 3 ) {
                                $subjectEmail = "User rejected by L2 user";

                                /* notification message for corporate admin */
                                $notificationMsg = ucwords($user['fullname'])." is rejected by L2 User ".ucwords($userByFullName). " for the Job ". $jobData['title']." for 2nd level";
                                insertNotificationForTopBarInAdmin($createdForId, $notificationMsg, "Job Rejected", $createdByUserId);

                                // new line
                                $notiData['userId']                 = $user['id'];
                                $notiData['notificationType']       = 'JobApplicationStatus';
                                $notiData['notificationTitle']      = 'Interview Status';
                                $notiData['notificationMessage']    = $notificationMsg;
                                $notiData['flag']                   = '1';
                                $this->db->insert('fj_userNotificationList', $notiData);
                                
                                $notificationData       = array("notificationType"=>"JobApplicationStatus");                                    
                                $params['device_token'] = array($user['deviceId']);
                                $params['title']        = 'Interview Status';
                                $params['message']      = cleanString($notificationMsg);
                                $params['data']         = json_encode($notificationData);
                                //send_gcm_notify($params);
                                $this->userId = $hdnusrid;
                                $this->type = 'rejected';
                                $this->jobCode = $jobData['fjCode'];
                                processApiSuccess($this);
                            }

                            $this->email->from($fromEmail, 'VDOHire - User Action');
                            $this->email->subject($subjectEmail);
                            $this->email->set_mailtype('html');

                            $temp['username'] = $userCreatedByDetails['username'];
                            $temp['jobtitle'] = $jobData['title'];


                            $temp['interviewGivenByUsername'] = $user['fullname'];
                            $temp['interviewSubmitedByUsername'] = $userByFullName;

                            if( $status == 2 ) {
                                $temp['jobstatus'] = "Shortlisted";
                            } else if( $status == 3 ) {
                                $temp['jobstatus'] = "Rejected";
                            } else {
                                $temp['jobstatus'] = "Put On Hold";
                            }

                            $toemail = $userCreatedByDetails['email'];
                            //$toemail = "jinandra.gupta@gmail.com";

                            $this->email->to($toemail);
                            $body = $this->load->view('emails/selectforJobTemp.php', $temp, TRUE);
                            $this->email->message($body);
                            $this->email->send();     
                                
                            if( $status == 2 ) {
                                /* notification message for corporate admin */
                                $notificationMsg = ucwords($user['fullname'])." is shortlisted by L2 User ".ucwords($userByFullName). " for the Job ". $jobData['title']." for 2nd level";
                                insertNotificationForTopBarInAdmin($createdForId, $notificationMsg, "Job Shortlist", $createdByUserId);

                                // new line
                                $notiData['userId']                 = $user['id'];
                                $notiData['notificationType']       = 'JobApplicationStatus';
                                $notiData['notificationTitle']      = 'Interview Status';
                                $notiData['notificationMessage']    = $notificationMsg;
                                $notiData['flag']                   = '1';
                                $this->db->insert('fj_userNotificationList', $notiData);
                                
                                $notificationData       = array("notificationType"=>"JobApplicationStatus");                                    
                                $params['device_token'] = array($user['deviceId']);
                                $params['title']        = 'Interview Status';
                                $params['message']      = cleanString($notificationMsg);
                                $params['data']         = json_encode($notificationData);
                                //send_gcm_notify($params);
                                /*$this->userId = $hdnusrid;
                                $this->type = 'shortlisted';
                                $this->jobCode = $jobData['fjCode'];
                                processApiSuccess($this);*/
                            } else if( $status == 3 ) {
                                /* notification message for corporate admin */
                                $notificationMsg = ucwords($user['fullname'])." is rejected by L2 User ".ucwords($userByFullName). " for the Job ". $jobData['title']." for 2nd level";
                                insertNotificationForTopBarInAdmin($createdForId, $notificationMsg, "Job Rejected", $createdByUserId);

                                // new line
                                $notiData['userId']                 = $user['id'];
                                $notiData['notificationType']       = 'JobApplicationStatus';
                                $notiData['notificationTitle']      = 'Interview Status';
                                $notiData['notificationMessage']    = $notificationMsg;
                                $notiData['flag']                   = '1';
                                $this->db->insert('fj_userNotificationList', $notiData);
                                
                                $notificationData       = array("notificationType"=>"JobApplicationStatus");                                    
                                $params['device_token'] = array($user['deviceId']);
                                $params['title']        = 'Interview Status';
                                $params['message']      = cleanString($notificationMsg);
                                $params['data']         = json_encode($notificationData);
                                //send_gcm_notify($params);
                                /*$this->userId = $hdnusrid;
                                $this->type = 'rejected';
                                $this->jobCode = $jobData['fjCode'];
                                processApiSuccess($this);*/
                            }
                            
                        }
                        
                        $data['message'] = 'Interview review successfully updated!';
                        $this->session->set_flashdata('flashmessagesuccess', 'Interview review successfully updated!');
                        redirect('queue/list/1');
                    } else {
                        $data['error'] = 'Sorry, interview review not updated!';
                        $this->session->set_flashdata('flashmessageerror', 'Sorry, interview review not updated!');
                        redirect('queue/list/1');
                    }
                    
                } catch (Exception $e) {
                    $data['main_content'] = 'fj-queue-viewinterview';
                    $data['error'] = $e->getMessage();
                    $data['token'] = $token;
                    redirect('queue/viewinterview/inteviewid/'.$hdninterviewid.'/userid/'.$hdnusrid);
                }                
            }      
            else {
                $data['main_content'] = 'fj-queue-viewinterview';
                
                $data['error'] = $resp;
                $data['token'] = $token;
                if (isset($resps['error']) && !empty($resps['error'])) {
                    $this->session->set_flashdata('flashmessageerror', $resps['error']);
                } else {
                    $this->session->set_flashdata('flashmessageerror', '');
                }
                
                redirect('queue/viewinterview/inteviewid/'.$hdninterviewid.'/userid/'.$hdnusrid);
            }
        }
    }

    /**
     * Description : Use to view an interview details on a specific job given by specific candidate
     * Author : Synergy
     * @param int $interviewId, int $userIntrvId
     * @return render interviews data into view
     */
    function viewinterviewdetailQueue() {

        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }

        $token = $this->config->item('accessToken');
        $userData = $this->session->userdata['logged_in'];
        
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            $userId = 1;
        }

        $rightanswer = 0;
        $wronganswer = 0;
        $totalquestioncount = 0;
        $totalpercentage = 0;
        $userjobdetails = array();
        $resp['error'] = 0;
        
        $interviewId = $this->uri->segment(4);
        $userIntrvId = $this->uri->segment(6);

        try {
            $userjobdetails = $this->userjob_model->detailsForUserJobs($interviewId, $userIntrvId); 
            $jobId = $userjobdetails['jobId'];
            $jobleveldetails = $this->jobpanel_model->getUserJobLevel($jobId, $userId); 
            $jobApplicationDetail = $this->job_model->userAssessmentDetail($userjobdetails['userId'], $jobId);
            
            if( $jobApplicationDetail ){
                foreach($jobApplicationDetail as $assessmentDetail) {
                    
                    $totalquestioncount = $totalquestioncount+1;
                    if( $assessmentDetail->userOption == $assessmentDetail->option ){
                        $rightanswer = $rightanswer+1;
                    } else {
                        $wronganswer = $wronganswer+1;
                    }
                }
            }
            
            $totalpercentage = number_format((( $rightanswer / $totalquestioncount ) * 100), 2, '.', '');
            
            $data['interview_content'] = $userjobdetails;   
            $data['totalpercentage'] = $totalpercentage;  
            $data['interview_data']['interviewid'] = $interviewId;
            $data['interview_data']['userid'] = $userIntrvId;
            $data['job_id'] = $jobId;
            $data['mergedVideo'] = $userjobdetails['mergedVideo'];
            $data['user_role_value'] = $jobleveldetails['level'];
            $data['jobApplicationDetail'] = $jobApplicationDetail;
            $data['token'] = $token;
            $data['main_content'] = 'fj-queue-viewinterview-detail';
            $this->load->view('fj-mainpage', $data); //die();
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Description : Use to view an assessment details on a specific job given by specific candidate
     * Author : Synergy
     * @param array $_POST
     * @return html entities of assessment data
     */
    function userAssessmentDetail() {
        $userId = trim($this->input->post('userId', true));
        $jobId  = trim($this->input->post('jobId', true));
        $rightanswer = 0;
        $wronganswer = 0;
        $totalquestioncount = 0;
        $totalpercentage = 0;
        $interviewVideo = trim($this->input->post('interviewVideo', true));
        $jobApplicationDetail           = $this->job_model->userAssessmentDetail($userId, $jobId);
        $data['jobApplicationDetail']   = $jobApplicationDetail;
        $details = '';
        if(count($data)>0){
            if( $jobApplicationDetail ){
                foreach($jobApplicationDetail as $assessmentDetail) {
                    
                    $totalquestioncount = $totalquestioncount+1;
                    if( $assessmentDetail->userOption == $assessmentDetail->option ){
                        $rightanswer = $rightanswer+1;
                    } else {
                        $wronganswer = $wronganswer+1;
                    }
                }
            }
            
            $totalpercentage = number_format((( $rightanswer / $totalquestioncount ) * 100), 2, '.', '');
                    
            $details .= "<table class='table'>
                            <thead class='tbl_head'>
                                <tr>
                                    <th>Assessment Question</th>
                                    <th>User Answer</th>
                                    <th>Correct Answer</th>
                                </tr>
                            </thead>                            
                            <tbody>";
            if( $jobApplicationDetail ){
                foreach($jobApplicationDetail as $assessmentDetail) {
                    $details .= "
                            <tr>
                                <td>".$assessmentDetail->title."</td>
                                <td>".($assessmentDetail->userOption!=""?$assessmentDetail->userOption:"Not Attempted")."</td>
                                <td>".$assessmentDetail->option."</td>
                            </tr>
                    ";
                }
            } else {
                $details .= "
                        <tr>
                            <td colspan=\"3\">Sorry, Records not available.</td>
                        </tr>
                ";
            }
            
            $details .= "
                    </tbody>
                </table>";
        }
        echo $details."####".$totalpercentage;
    }
}
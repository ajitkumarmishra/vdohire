<?php if ($this->session->flashdata('message')) { ?>
    <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Success!</strong> <?php echo $this->session->flashdata('message'); ?>
    </div>
<?php } ?>
<?php if ($this->session->flashdata('error')) { ?>
    <div class="alert alert-danger">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Danger!</strong> <?php echo $this->session->flashdata('error'); ?>
    </div>
<?php } ?>
<div class="row">
    <div class="col-sm-12 col-md-12">
       
        <form method="post" name="form1" id="form1" action="">
            <table class="table">
                <thead>
                    <tr>
                        <th><input name="check_all" type="checkbox" id="check_all" value="1" onclick="checkall(this.form)" /></th>
                        <th>Forum id</th>
						<th>User id</th>
						<th>Title</th>
                        <th>Description</th>
                        <th>Date</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                </thead>
                <tbod>
				<?php  //print_r($comment); die; ?>
                    <?php foreach ($comment as $comments):
                    //print_r($comments); 
					?>
                        <tr class="<?php echo ($comments->comment_status == '1' ? 'success' : 'warning'); ?>">
                            <td>
                                <input name="arr_ids[]" type="checkbox" id="arr_ids[]" value="<?php echo $comments->comment_id; ?>" />
                            </td>
							 <td><?php echo $comments->forum_id; ?></td>
							  <td><?php echo $comments->user_id; ?></td>
                            <td><?php echo $comments->comment_title; ?></td>
                            <td><?php echo $comments->comment_des; ?></td>
                            <td><?php echo $comments->comment_creation_date; ?></td>
                            <td><?php echo ($comments->comment_status == '1' ? 'Active' : 'Inactive'); ?></td>
                            <!--<td><a href="<?php //echo base_url('forum/account/id') . '/' . $comments->forum_id; ?>"><span class="glyphicon glyphicon-pencil pull-right"></a></td>-->
                         <td><a href="javascript:void(0);" class="btn btn-mini btn-xs del_Listing" name="<?php echo $comments->comment_id; ?>"><span class="glyphicon glyphicon-remove "></a></td>
						</tr>
                    <?php endforeach; ?>
                </tbod>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="left" style="padding:2px">
                        <!--<input type="submit" name="Activate" value="Activate" class="btn"/>
                        <input type="submit" name="Deactivate" value="Deactivate" class="btn" />-->
						<input type="submit" name="Delete" value="Delete" class="btn" />
                    </td>
                </tr>
            </table>
			 <input type="hidden" id="P_deleteurl" value="<?php echo base_url('comment/delete'); ?>" />
			   <?php //echo $links; ?>
        </form>
    </div>
</div>

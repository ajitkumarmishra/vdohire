<div class="row">
    <div class="col-sm-6 col-md-6 offset3">
        <h1>Forgot Password</h1>

        <?php if (@$error): ?>
            <div class="alert">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <?php echo $error; ?>
            </div>
        <?php endif; ?>
        <?php if ($this->session->flashdata('message')) { ?>
            <div class="alert alert-success">
                <strong>Success!</strong> <?php echo $this->session->flashdata('message'); ?>
            </div>
        <?php } ?>

        <div class="well">
            <form class="form-horizontal" method="post" action="">
                <div class="control-group">
                    <label class="control-label" for="inputEmail">Email</label>
                    <div class="controls">
                        <input type="text" id="inputEmail" placeholder="Email" name="user_email" value="<?php echo set_value('user_email');?>">
                    </div>
                </div>

                <div class="control-group">
                    <div class="controls">
                        <button type="submit" class="btn">Send</button>
                    </div>
                </div>
            </form>
        </div>

    </div>
</div>

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Comment Model
 * Description : Handle all the CRUD operation for Analytics
 * @author Synergy
 * @createddate : Nov 19, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 16, 2017
 */
class Comment_model extends CI_Model {

    /**
     * Defining static variable to store tables name
     */
    var $table = "nc_comments";

    /**
     * Responsable for auto load the the session library
     * @return void
     */
    function __construct() {
        parent::__construct();
        $this->load->library('session');
    }

    /**
     * Description : Return total number of Job Posted and Number of Open Job Posts(Jobs Status fj-report-1)
     * Author : Synergy
     * @param $date1, $date2
     * @return int(Active jobs count) 
     */
    function read() {
        $query = $this->db->get($this->table);
        return $query->result();
    }
	
    /**
     * Description : Return total number of Job Posted and Number of Open Job Posts(Jobs Status fj-report-1)
     * Author : Synergy
     * @param $date1, $date2
     * @return int(Active jobs count) 
     */
	function deletecomment($id) {
        $this->db->where('comment_id', $id);
        $query = $this->db->delete($this->table);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }
}
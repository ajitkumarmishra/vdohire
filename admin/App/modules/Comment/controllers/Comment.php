<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Comment Controller
 * Description : Used to handle all analytics related data
 * @author Synergy
 * @createddate : Nov 2, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 15, 2017
 */

class Comment extends MY_Controller {

     /**
     * Responsable for auto load the the comment_model
     * Responsable for auto load form_validation library
     * Check Uri Segment to display specific page role wise
     * @return void
     */
    function __construct() {
        parent::__construct();
        $this->load->model('comment_model');
        $this->load->library('form_validation');
    }

    /**
     * Description : Use to display all comments
     * Author : Synergy
     * @param none
     * @return array of data 
     */
    function index() {
        $includeJs = array('theme/js/custom.js', 'theme/js/bootbox.min.js');
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data["comment"] = $this->comment_model->read();
        $data['main_content'] = 'comments';
        $this->load->view('page', $data);
    }

    /**
     * Description : Use to delete comment
     * Author : Synergy
     * @param array $_POST
     * @return json data
     */
    function delete() {
        $id = $this->input->post('delID');
        if (isset($id) and $id != '') {
            $check = $this->comment_model->deletecomment($id);
            if ($check) {
                $data['status'] = true;
                $data['message'] = "Deleted Successfully";
                echo json_encode($data);
            } else {
                $data['status'] = false;
                $data['message'] = "Something Went wrong";
                echo json_encode($data);
            }
        } else {
            $data['status'] = false;
            $data['message'] = "Something Went wrong";
            echo json_encode($data);
        }
    }

    /**
     * Description : Use to udpate account
     * Author : Synergy
     * @param array $_POST
     * @return data
     */
    function account($id = null) {
        $this->_member_area();
        if ($id != null) {
            $userID = $id;
        } else {
            $userID = $this->session->userdata('userid');
        }

        if ($_POST) {
            $userdata = new stdClass();
            $userdata->user_nicename = $this->input->post('nickname');
            $userdata->user_name = $this->input->post('user_name');
            $userdata->user_mobile = $this->input->post('user_mobile');
            $userdata->user_dob = $this->input->post('user_dob');
            $userdata->user_email = $this->input->post('email');
            $userdata->user_pass = md5($this->input->post('password'));
            $insert = $this->user_model->update($userID, $userdata);
            if ($insert) {
                $data['message'] = "Updated succesfully";
                $data['user'] = $this->user_model->user_by_id($userID);
                $data['main_content'] = 'account';
                $this->load->view('page', $data);
            }
            return;
        }

        $data['user'] = $this->user_model->user_by_id($userID);
        $data['main_content'] = 'account';
        $this->load->view('page', $data);
    }
}
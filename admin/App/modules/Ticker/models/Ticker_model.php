<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Ticker Model
 * Description : Handle all the CRUD operation for Ticker
 * @author Synergy
 * @createddate : Nov 21, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 20, 2017
 */

class Ticker_model extends CI_Model {

    /**
     * Declaring static variables
     */
    var $table = "fj_ticker";

    /**
     * Responsable for inheriting parent constructor
     * Responsable for auto load the session and pagination library
     * @return void
     */
    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('pagination');
    }

    /**
     * Description : Use to list all the tickers
     * Author : Synergy
     * @param int $limit, int $offset, int $serachColumn, int $searchText, int $createdBy
     * @return array of ticker data
     */
    function listTicker($limit, $offset, $serachColumn = NULL, $searchText = NULL, $createdBy = NULL) {
        try {
            $currentDate = date('Y-m-d h:i:s');
            $sql = "select T.*, U.fullname from fj_ticker as T left join fj_users as U on U.id=T.tickerCreatedBy";
            //$sql .= " GROUP BY T.tickerToken";
            $sql .= " order by T.createdAt desc";
            if(isset($offset)) {
                $sql .= " limit " .$offset. ", " .$limit."";
            }
            
            $query = $this->db->query($sql);
            $result = $query->result();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Description : Use to add new ticker
     * Author : Synergy
     * @param array $_POST
     * @return int $tickerId or boolean false
     */
    function addTicker($data) {
        $resp = $this->db->insert($this->table, $data);
        $tickerId = $this->db->insert_id();
        if ($resp) {
            return $tickerId;
        } else {
            return false;
        }
    }
    
    /**
     * updateTicker function
     * Updates a specific ticker.
     * @param array $data.
     * @param string $tickerToken.
     * @return boolean.
     */
    function updateTicker($data, $tickerToken) {
        try {
            $this->db->where('tickerToken', $tickerToken);
            $resp = $this->db->update($this->table, $data);
            if (!$resp) {
                throw new Exception('Unable to update ticker data.');
                return false;
            }
            return $tickerToken;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * GetTicker function
     * Gets a specific ticker detail.
     * @param stirng $tickerToken.
     * @return array the ticker data.
     */
    function getTicker($tickerToken) {
        try {
            $this->db->select('*');
            $this->db->from($this->table);
            $this->db->where('tickerToken', $tickerToken);
            $query = $this->db->get();
            $result = $query->row_array();
            
            if (count($result) == 0) {
                throw new Exception('Unable to fetch ticker data.');
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * deleteAllOldTicker function
     * delete token with ticker token_name(token)
     * @param stirng $tickerToken.
     * @return array the ticker data.
     */
    function deleteAllOldTicker($tickerToken) {       
        $sql    = "DELETE FROM fj_ticker WHERE tickerToken='$tickerToken'";            
        $query  = $this->db->query($sql);
        $result = $query->result();
    }
    
    
    /**
     * fileUpload function
     * Uploads a file to directory
     * @param array $files
     * @param string $uploads_dir
     * @param string $imagename
     * @return boolean
     */
    public function fileUpload($files, $uploads_dir, $imagename) {
        try {
            $tmp_name = $files["tickerImage"]["tmp_name"];
            if (move_uploaded_file($tmp_name, $uploads_dir . "/" . $imagename)) {
                return $imagename;
            } else {
                //throw new Exception('Could not upload file');
            }
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    /**
     * checkGuidExist function
     * Discription : Use to check if guid is exist or not
     * Author : Synergy
     * @param int $guid
     * @return boolean
     */
    function checkGuidExist($guid) {
        $this->db->where('guid', $guid);
        $query = $this->db->get('fj_ticker');
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
}
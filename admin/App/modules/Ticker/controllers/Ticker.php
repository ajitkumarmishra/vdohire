<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Ticker Controller
 * Description : Used to handle all Ticker related data
 * @author Synergy
 * @createddate : Nov 3, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 22, 2017
 */

class Ticker extends MY_Controller {

    /**
     * Responsable for auto load the ticker_model
     * Responsable for auto load the form_validation, session, email
     * Responsable for auto load fj,url helper
     * Check Uri Segment to display specific page role wise
     * @return void
     */
    function __construct() {
        parent::__construct();
        $this->load->model('ticker_model');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('email');
        $this->load->helper('fj');
        $this->load->helper('url');
        $token = $this->config->item('accessToken');
        if (isset($_POST) && $_POST != NULL) {
            if ($token != $_POST['accessToken']) {
                echo 'You do not have pemission.';
                exit;
            }
        }

        /**********Check any type of fraund activity*******/
        if ($this->session->userdata('logged_in')) {
            $userRealData   = $this->session->userdata['logged_in'];
            $uriStringSegment = $this->uri->uri_string();
            $arraySegments = explode('/', $uriStringSegment);
            array_pop($arraySegments);
            $newString = implode('/', $arraySegments);
            //echo $newString;exit;
            if($userRealData->role == 1) {
                $portal = 'SuperAdminPortal';
            }
            else{
                $portal = '';
            }

            if($uriStringSegment == 'ticker/list') {
                if(!checkUserAccessForWebPage($userRealData->id, $userRealData->role, $portal, 'ticker/list')) {
                    $this->session->sess_destroy();
                    $insertFaultData['userId'] = $userRealData->id;
                    $insertFaultData['comment'] = 'This user try to access this this page';
                    $insertFaultData['pagePath'] = 'ticker/list';
                    $this->db->insert('fj_user_access_log', $insertFaultData);
                    redirect(base_url('users/login'));
                }
            }

            if($uriStringSegment == 'ticker/add') {
                if(!checkUserAccessForWebPage($userRealData->id, $userRealData->role, $portal, 'ticker/add')) {
                    $this->session->sess_destroy();
                    $insertFaultData['userId'] = $userRealData->id;
                    $insertFaultData['comment'] = 'This user try to access this this page';
                    $insertFaultData['pagePath'] = 'ticker/add';
                    $this->db->insert('fj_user_access_log', $insertFaultData);
                    redirect(base_url('users/login'));
                }
            }

            if($newString == 'ticker/view') {
                if(!checkUserAccessForWebPage($userRealData->id, $userRealData->role, $portal, 'ticker/view')) {
                    $this->session->sess_destroy();
                    $insertFaultData['userId'] = $userRealData->id;
                    $insertFaultData['comment'] = 'This user try to access this this page';
                    $insertFaultData['pagePath'] = 'ticker/view';
                    $this->db->insert('fj_user_access_log', $insertFaultData);
                    redirect(base_url('users/login'));
                }
            }

            if($newString == 'ticker/edit') {
                if(!checkUserAccessForWebPage($userRealData->id, $userRealData->role, $portal, 'ticker/edit')) {
                    $this->session->sess_destroy();
                    $insertFaultData['userId'] = $userRealData->id;
                    $insertFaultData['comment'] = 'This user try to access this this page';
                    $insertFaultData['pagePath'] = 'ticker/edit';
                    $this->db->insert('fj_user_access_log', $insertFaultData);
                    redirect(base_url('users/login'));
                }
            }
        }
        /****************End of handle fraud activity****************/
    }

    /**
     * Description : List all the tickers
     * Author : Synergy
     * @param int $page
     * @return render data into view
     */
    function listTicker($page) {
        
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }

        $token = $this->config->item('accessToken');
        $userData = $this->session->userdata['logged_in'];
        
        $userLoggeinId = $userData->id;
        
        $data['page'] = $page;
        $resp['error'] = 0;
        $serachColumn = $this->input->post('serachColumn', true);
        $searchText = $this->input->post('serachText', true);
        $token = $this->config->item('accessToken');
        $fromEmail = $this->config->item('fromEmail');
        $itemsPerPage = $this->config->item('itemsPerPage');
        $data['itemsPerPage'] = $itemsPerPage;
        $createdBy = NULL;

        if (!$page) {
            $page = 0;
        } else {
            $page = $page - 1;
        }
        $offset = $page * $itemsPerPage;
        try {
            
            $result = $this->ticker_model->listTicker($itemsPerPage, $offset, $serachColumn, $searchText, $createdBy);

            $completeResult = $this->ticker_model->listTicker(NULL, NULL, NULL, NULL, $createdBy);
            $count = ceil(count($completeResult) / $itemsPerPage);
            $data['count'] = $count;
            $data['totalCount'] = count($completeResult);
            $data['content'] = $result;
            $data['main_content'] = 'fj-ticker-listing';
            $data['searchText'] = $searchText;
            $data['serachColumn'] = $serachColumn;
            $data['page'] = $page;
            $data['token'] = $token;
            
            $this->load->view('fj-mainpage', $data);
            
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Description : Use to add new ticker
     * Author : Synergy
     * @param null or array $_POST
     * @return render data into view
     */
    function addTicker() {
        
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }

        $token = $this->config->item('accessToken');
        $userData = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            $userId = 1;
        }
        $tickerImage = "";
        $tickerThumbImage = "";
        
        if (isset($_POST) && $_POST != NULL) {
           
            $resp['error'] = '';
            
            $this->form_validation->set_rules('tickerStartDate', 'Ticker Start Date', 'trim|required');
            $this->form_validation->set_rules('tickerEndDate', 'Ticker End Date', 'trim|required');
            $this->form_validation->set_rules('tickerTitle', 'Ticker Title', 'trim|required');
            $this->form_validation->set_rules('tickerPriority', 'Ticker Priority', 'trim|required');
            $this->form_validation->set_rules('tickerDesc', 'Ticker Description', 'trim|required');        
            
            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }
            
            $tickerStartDate = strtotime($_POST['tickerStartDate']);
            $tickerEndDate = strtotime($_POST['tickerEndDate']);

            if ($tickerEndDate < $tickerStartDate) {
                $resp['error'] .= '<p>End date should be greater than Start date</p>';
            }           
            
            if (!$resp['error']) {
                
                $data['tickerCreatedBy'] = $userId;
                $data['tickerTitle'] = $this->input->post('tickerTitle', true);
                $data['tickerDesc'] = $this->input->post('tickerDesc', true);
                
                $tickerStartDate = date("Y-m-d", strtotime($this->input->post('tickerStartDate', true)));
                $tickerStartTime = $this->input->post('tickerStartTime', true);
                
                $tickerEndDate = date("Y-m-d", strtotime($this->input->post('tickerEndDate', true)));
                $tickerEndTime = $this->input->post('tickerEndTime', true);
                
                $data['tickerStartDate'] = $tickerStartDate." ".$tickerStartTime.":00";
                $data['tickerEndDate'] = $tickerEndDate." ".$tickerEndTime.":00";
                
                $tickerPriority = $this->input->post('tickerPriority', true);
                
                $data['tickerPriority'] = $this->input->post('tickerPriority', true);
                
                $data['tickerToken'] = time();
                
                $data['createdAt'] = date('Y-m-d h:i:s');
                $data['updatedAt'] = date('Y-m-d h:i:s');
                $data['tickerCreatedBy'] = $userId;
                
                try {
                    if (isset($_FILES['tickerImage']) && $_FILES['tickerImage']['name'] != "") {
                        
                        $tickerImage = $this->fileUpload($_FILES);
                        if (!$tickerImage) {
                            $resp['message'] = 'There is a server problem occured to upload image. Please make sure image type is png or jpg!';
                        }
                    }
                    $data['tickerImage'] = $tickerImage;
                    
                    if( $tickerImage != "" ){
                        $tickerThumbImage = $this->tickerThumbImage('uploads/ticker', $tickerImage);
                        $data['tickerThumbImage'] = $tickerThumbImage;                        
                    } else {
                        $data['tickerThumbImage'] = $tickerThumbImage;
                    }
                    
                    $tickerCopies = 0;
                    
                    if( $tickerPriority != "" ){
                        
                        if( $tickerPriority == 1 ) {
                            $tickerCopies = 5;
                        } else if( $tickerPriority == 2 ) {
                            $tickerCopies = 3;
                        } else if( $tickerPriority == 3 ) {
                            $tickerCopies = 1;
                        }
                        
                        for( $j=0; $j < $tickerCopies; $j++){
                            $resp = $this->ticker_model->addTicker($data);
                        }
                    } else {
                        $resp = $this->ticker_model->addTicker($data);
                    }
                    if ($resp) {
                        $this->session->set_flashdata('flashmessagesuccess', 'Ticker successfully created!');
                        $data['token'] = $token;
                        $data['subuser'] = getSubUsers($userId);
                        redirect('ticker/add');
                    }
                } catch (Exception $e) {
                    $resp['error'] = $e->getMessage();
                }
            } else {
                $data['main_content'] = 'fj-create-ticker';
                $data['error'] = $resp;
                $data['ticker'] = $this->input->post();
                $data['files'] = $_FILES;
                $data['token'] = $token;
                $data['subuser'] = getSubUsers($userId);
                $this->load->view('fj-mainpage', $data);
            }
        } else {
            $data['subuser'] = getSubUsers($userId);
            $data['main_content'] = 'fj-create-ticker';
            $data['token'] = $token;
            $this->load->view('fj-mainpage', $data);
        }
    }
    
    /**
     * Description : Use to update a specific ticker
     * Author : Synergy
     * @param int tickerTokenId, array $_POST
     * @return render data into view
     */
    function editTicker($tickerTokenId) {
        
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $ticker = $this->ticker_model->getTicker($tickerTokenId);
        $token = $this->config->item('accessToken');
        $userData = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            $userId = 1;
        }
        $info['main_content'] = 'fj-edit-ticker';
        
        $tickerStartDateVal = explode(" ", $ticker['tickerStartDate']);
        $tickerEndDateVal = explode(" ", $ticker['tickerEndDate']);
        
        $ticker['tickerStartDate'] = date("m/d/Y", strtotime($tickerStartDateVal[0]));
        $ticker['tickerStartTime'] = $tickerStartDateVal[1];
        
        $ticker['tickerEndDate'] = date("m/d/Y", strtotime($tickerEndDateVal[0]));
        $ticker['tickerEndTime'] = $tickerEndDateVal[1];
        
        $tickerImage = $ticker['tickerImage'];
        $tickerThumbImage = $ticker['tickerThumbImage'];
        $tickerTitle = $ticker['tickerTitle'];
        
        $info['ticker'] = $ticker;
        $info['token'] = $token;
        $info['subuser'] = getSubUsers($userId);
        
        $j = 0;
        if (isset($_POST) && $_POST != NULL) {
           
            $resp['error'] = '';
            
            $this->form_validation->set_rules('tickerStartDate', 'Ticker Start Date', 'trim|required');
            $this->form_validation->set_rules('tickerEndDate', 'Ticker End Date', 'trim|required');
            $this->form_validation->set_rules('tickerTitle', 'Ticker Title', 'trim|required');
            $this->form_validation->set_rules('tickerPriority', 'Ticker Priority', 'trim|required');
            $this->form_validation->set_rules('tickerDesc', 'Ticker Description', 'trim|required');          
            
            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }
            
            $tickerStartDate = strtotime($_POST['tickerStartDate']);
            $tickerEndDate = strtotime($_POST['tickerEndDate']);

            if ($tickerEndDate < $tickerStartDate) {
                $resp['error'] .= '<p>End date should be greater than Start date</p>';
            } 
            
            if (!$resp['error']) {
                
                $data['tickerCreatedBy'] = $userId;
                $data['tickerTitle'] = $this->input->post('tickerTitle', true);
                $data['tickerDesc'] = $this->input->post('tickerDesc', true);
                
                $tickerStartDate = date("Y-m-d", strtotime($this->input->post('tickerStartDate', true)));
                $tickerStartTime = $this->input->post('tickerStartTime', true);
                
                $tickerEndDate = date("Y-m-d", strtotime($this->input->post('tickerEndDate', true)));
                $tickerEndTime = $this->input->post('tickerEndTime', true);
                
                $data['tickerStartDate'] = $tickerStartDate." ".$tickerStartTime.":00";
                $data['tickerEndDate'] = $tickerEndDate." ".$tickerEndTime.":00";
                        
                $tickerPriority = $this->input->post('tickerPriority', true);
                
                $data['tickerPriority'] = $this->input->post('tickerPriority', true);
                
                $data['createdAt'] = date('Y-m-d h:i:s');
                $data['updatedAt'] = date('Y-m-d h:i:s');
                $data['tickerCreatedBy'] = $userId;
                
                try {                    
                    
                    if (isset($_FILES['tickerImage']) && $_FILES['tickerImage']['name'] != "") {
                        
                        $tickerImage = $this->fileUpload($_FILES);
                        if (!$tickerImage) {
                            $resp['message'] = 'There is a server problem occured to upload image. Please make sure image type is png or jpg!';
                        }
                        
                        $data['tickerImage'] = $tickerImage;
                        
                        if( $tickerImage != "" ){
                            $tickerThumbImage = $this->tickerThumbImage('uploads/ticker', $tickerImage);
                            $data['tickerThumbImage'] = $tickerThumbImage;                        
                        } else {
                            $data['tickerThumbImage'] = $tickerThumbImage;
                        }
                    
                    } else {
                        $data['tickerImage'] = $tickerImage;
                        $data['tickerThumbImage'] = $tickerThumbImage;
                    }
                    
                    $tickerCopies = 0;
                    
                    if( $tickerPriority != "" ){
                        
                        if( $tickerPriority == 1 ) {
                            $tickerCopies = 5;
                        } else if( $tickerPriority == 2 ) {
                            $tickerCopies = 3;
                        } else if( $tickerPriority == 3 ) {
                            $tickerCopies = 1;
                        }
                        
                        for( $j=0; $j < $tickerCopies; $j++){
                            $resp = $this->ticker_model->updateTicker($data, $tickerTokenId);
                        }
                    } else {
                        $resp = $this->ticker_model->updateTicker($data, $tickerTokenId);
                    }
                    
                    if ($resp) {                        
                        $this->session->set_flashdata('flashmessagesuccess', 'Ticker successfully updated!');
                        $data['token'] = $token;
                        $data['subuser'] = getSubUsers($userId);                        
                        redirect('ticker/edit/'.$tickerTokenId);
                    }
                } catch (Exception $e) {
                    $resp['error'] = $e->getMessage();
                }
            } else {
                $data['main_content'] = 'fj-edit-ticker';
                $data['error'] = $resp;
                $data['ticker'] = $this->input->post();
                $data['files'] = $_FILES;
                $data['token'] = $token;
                $data['subuser'] = getSubUsers($userId);
                $this->load->view('fj-mainpage', $data);
            }
        } else {
            $this->load->view('fj-mainpage', $info);
        }
    }
    
    /**
     * Description : Use to delete a specific ticker
     * Author : Synergy
     * @param array $_POST
     * @return string of success message | error message or render data into view
     */
    function deleteTicker() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token = $this->config->item('accessToken');
        $userData = $this->session->userdata['logged_in'];
        
        $resp['error'] = 0;
        $tickerId = $this->input->post('tickerId', true);
       
        $pageNo = $this->input->post('pageNo', true);
        $pageNo = $pageNo+1;
        
        if (!$tickerId) {
            echo $error = 'Undefined ticker!';
            exit;
        }

        if (!$resp['error']) {
            $data['status'] = 3;
            
            $ticker = $this->ticker_model->getTicker($tickerId);
            $tickerToken = $ticker['tickerToken'];
            
            // delete all old ticker for new one
            $deleteAllOldTickerRes = $this->ticker_model->deleteAllOldTicker($tickerToken);
            
            if ($deleteAllOldTickerRes) {
                $this->session->set_flashdata('flashmessagesuccess', 'Ticker successfully deleted!');
                $data['token'] = $token;
                $data['subuser'] = getSubUsers($userId);
                redirect('ticker/list/'.$pageNo);
            } else {
                $this->session->set_flashdata('flashmessageerror', 'Sorry, Ticker not deleted!');
                $data['token'] = $token;
                $data['subuser'] = getSubUsers($userId);
                redirect('ticker/list/'.$pageNo);
            }
        }
    }
    
    /**
     * Description : Use to view a specific ticker
     * Author : Synergy
     * @param int $tickerTokenId
     * @return render ticker data into view
     */
    function viewTicker($tickerTokenId) {
        
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $ticker = $this->ticker_model->getTicker($tickerTokenId);
        $token = $this->config->item('accessToken');
        $userData = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            $userId = 1;
        }
        $info['main_content'] = 'fj-view-ticker';
        
        $tickerStartDateVal = explode(" ", $ticker['tickerStartDate']);
        $tickerEndDateVal = explode(" ", $ticker['tickerEndDate']);
        
        $ticker['tickerStartDate'] = date("m/d/Y", strtotime($tickerStartDateVal[0]));
        $ticker['tickerStartTime'] = $tickerStartDateVal[1];
        
        $ticker['tickerEndDate'] = date("m/d/Y", strtotime($tickerEndDateVal[0]));
        $ticker['tickerEndTime'] = $tickerEndDateVal[1];
        
        $info['ticker'] = $ticker;
        $info['token'] = $token;
        $this->load->view('fj-mainpage', $info);
    }
    
    /**
     * FileUpload function
     * Uploads a file.
     * @param array $files.
     * @return string the uploaded file or boolean false.
     */
    function fileUpload($files) {
        $typeArr = explode('/', $files['tickerImage']['type']);
        $imgName = rand() . date('ymdhis') . '_ticker.' . $typeArr[1];
        
        $data['tickerImage'] = $imgName;
        $tickerImageType = $this->config->item('userImageType');
        
        if (in_array($typeArr[1], $tickerImageType)) {
            $uploadResp = $this->ticker_model->fileUpload($files, 'uploads/ticker', $imgName);
            return $uploadResp;
        } else {
            return false;
        }
    }
    
    /**
     * tickerThumbImage function
     * Uploads a file.
     * @param string $updir, string $img
     * @return string the uploaded file or boolean false.
     */
    function tickerThumbImage($updir, $img)
    {
        $thumbnail_width = 134;
        $thumbnail_height = 189;
        $tickerThumbImage = "";
        $thumb_beforeword = "thumb";
        
        $arr_image_details = getimagesize("$updir" . '/' . "$img"); // pass id to thumb name
        $original_width = $arr_image_details[0];
        $original_height = $arr_image_details[1];
        if ($original_width > $original_height) {
            $new_width = $thumbnail_width;
            $new_height = intval($original_height * $new_width / $original_width);
        } else {
            $new_height = $thumbnail_height;
            $new_width = intval($original_width * $new_height / $original_height);
        }
        $dest_x = intval(($thumbnail_width - $new_width) / 2);
        $dest_y = intval(($thumbnail_height - $new_height) / 2);
        if ($arr_image_details[2] == 1) {
            $imgt = "ImageGIF";
            $imgcreatefrom = "ImageCreateFromGIF";
        }
        if ($arr_image_details[2] == 2) {
            $imgt = "ImageJPEG";
            $imgcreatefrom = "ImageCreateFromJPEG";
        }
        if ($arr_image_details[2] == 3) {
            $imgt = "ImagePNG";
            $imgcreatefrom = "ImageCreateFromPNG";
        }
        if ($imgt) {
            
            $typeArr = explode('.', $img);
            $imgNewThumb = $typeArr[0] . '_thumb.' . $typeArr[1];
        
            $old_image = $imgcreatefrom("$updir" . '/' . "$img");
            $new_image = imagecreatetruecolor($thumbnail_width, $thumbnail_height);
            imagecopyresized($new_image, $old_image, $dest_x, $dest_y, 0, 0, $new_width, $new_height, $original_width, $original_height);
            $tickerThumbImageRes = $imgt($new_image, "$updir" . '/' . "$thumb_beforeword" . '/' . "$imgNewThumb");
            if( $tickerThumbImageRes ) {
                $tickerThumbImage = $imgNewThumb;
            }
        }
        
        return $tickerThumbImage;
    }

    /**
     * importRssFeed function
     * Discription : Importing news feed from RSS feed
     * Author : Synergy
     * @param none
     * @return boolean
     */
    public function importRssFeed() {

        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }

        $userData = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            $userId = 1;
        }

        try {
            $url="http://economictimes.indiatimes.com/jobs/rssfeeds/107115.cms";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_URL, $url);    // get the url contents

            $data = curl_exec($ch); // execute curl request
            curl_close($ch);

            $xml = simplexml_load_string($data);
            $all_object_data = $xml->channel->item;

            foreach ($all_object_data as $object_data) {
                if(!$this->ticker_model->checkGuidExist($object_data->guid)) {
                    $insert_data['tickerCreatedBy'] = $userId;
                    $insert_data['tickerTitle'] = $object_data->title;
                    $insert_data['tickerDesc'] = $object_data->description;

                    //Extracting src attribute of image
                    $str = $all_object_data->description;
                    $dom = new DomDocument();
                    $dom->loadHTML($str);
                    $output = array();
                    foreach ($dom->getElementsByTagName('img') as $item) {
                       $output[] = $item->getAttribute('src');
                    }
                    //End

                    //echo $output[0];exit;
                    $insert_data['tickerThumbImage'] = $output[0];
                    $insert_data['link'] = $object_data->link;
                    $insert_data['guid'] = $object_data->guid;
                    $insert_data['tickerPriority'] = 1;
                    $insert_data['tickerToken'] = time();
                    $insert_data['createdAt'] = date('Y-m-d h:i:s');
                    $insert_data['updatedAt'] = date('Y-m-d h:i:s');
                    $insert_data['tickerCreatedBy'] = $userId;
                    $this->ticker_model->addTicker($insert_data);
                }
            }

            redirect('ticker/list');
        } catch (Exception $e) {
            redirect('ticker/list');
        }
    }
}
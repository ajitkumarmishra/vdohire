<?php //echo '<pre>';print_r($content);die();   ?>
<!-- Page Content -->
<div id="page-content-wrapper">
    <?php if($this->session->flashdata('flashmessagesuccess')){ 
            echo "<div class=\"alert alert-success\">".$this->session->flashdata('flashmessagesuccess')."</div>"; 
          } 
    ?>
    <?php if($this->session->flashdata('flashmessageerror')){ 
            echo "<div class=\"alert alert-danger\">".$this->session->flashdata('flashmessageerror')."</div>"; 
          } 
    ?>
    <?php if (isset($message)): ?>
        <div class="alert alert-success">
            <?php echo $message; ?>
        </div>
    <?php endif; ?>
    <?php if (isset($error)): ?>
        <div class="alert alert-danger">
            <?php foreach ($error as $item): ?>
                <?php echo $item; ?>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
    
    <div class="container-fluid whitebg">
        <div class="row">
            <div class="col-md-6"><h2><img src="<?php echo base_url(); ?>/theme/firstJob/image/arrow_head.png" class="prev-page">Ticker Management</h2></div>
            <div class="col-md-6">	
                <div class="ed_job_bt pull-right">
                    <a href="<?php echo base_url(); ?>ticker/add"><button type="button">Add New</button></a>
                </div>
                <div class="ed_job_bt pull-right">
                    <a href="<?php echo base_url(); ?>ticker/importRssFeed"><button type="button">Import News Feed</button></a>
                </div>
            </div>
            <div class="col-md-12 padd_tp_bt_20">
                
                <?php
                $i = 0; 

                if( !empty($content) )
                {
                    foreach ($content as $item){
                ?>        
                <div class="media">
                    <div class="media-left media-middle">
                        <a href="<?php echo base_url(); ?>ticker/view/<?php echo $item->tickerId; ?>">
                            <?php
                            if ( $item->tickerImage != "" ) {
                                echo "<img style=\"width: 100px; height: 100px;\" class=\"media-object\" src=\"".base_url()."/uploads/ticker/".$item->tickerImage."\" alt=\"\">";
                            } else {
                                echo "<img class=\"media-object\" style=\"width: 100px; height: 100px;\" src=\"".base_url()."theme/firstJob/image/logo_01.png\" alt=\"\">";
                            }
                            ?>                            
                        </a>
                    </div>
                    <div class="media-body">
                        <div class="col-md-5">
                            <h5 class="media-heading"><?php echo $item->tickerTitle; ?></h5>
                            <p><a href="<?php echo base_url(); ?>ticker/view/<?php echo $item->tickerToken; ?>">read detail...</a></p>
                        </div>
                        <div class="col-md-2">
                            <p class="black_color">Priority : <?php echo ($item->tickerPriority!=""?$item->tickerPriority:"0"); ?></p>
                        </div>
                        <div class="col-md-3">
                            <p class="blue_color">By : <?php echo ($item->fullname!=""?$item->fullname:"FirstJob"); ?><br>At : <?php echo date('d, M-Y', strtotime($item->createdAt)); ?></p>
                        </div>
                        <div class="col-md-2">
                            <div><a href="<?php echo base_url(); ?>ticker/edit/<?php echo $item->tickerToken; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/edit.png"></a>
                            <a href="<?php echo base_url(); ?>ticker/view/<?php echo $item->tickerToken; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/eye.png"></a>
                            <a href="javascript:" class="delete-ticker" id="<?php echo $item->tickerToken; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/del.png"></a></div>
                        </div>
                    </div>
                </div>
                <?php
                    }
                } else {
                ?>
                <div class="media">Ticker data not available</div>
                <?php } ?>
                <div class="service_content ">

                    <?php if ($totalCount > $itemsPerPage): ?>
                        <ul class="service_content_last_list">
                            <?php if ($page > 1): ?>
                                <p class="pagi_img"><a href="<?php echo base_url(); ?>ticker/list/<?php echo $page - 1; ?>" ><img src="<?php echo base_url(); ?>/theme/firstJob/image/pagination_img2.png"></a></p>    
                            <?php endif; ?>
                            <input type="hidden" name="page_no" value="1" id="page_no"> 
                            <?php for ($i = 1; $i <= $count; $i++): ?>
                                <li><a href="<?php echo base_url(); ?>ticker/list/<?php echo $i; ?>"  <?php if ($i == $page): ?>style="color: orange;"<?php endif; ?>><?php echo $i; ?></a></li>
                            <?php endfor; ?>
                            <?php if ($page < $count): ?>
                                <p class="pagi_img"><a href="<?php echo base_url(); ?>ticker/list/<?php echo $page + 1; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/pagination_img1.png"></a></p>            
                            <?php endif; ?>
                        </ul>
                    <?php endif; ?>
                </div>
            </div>
            
        </div>
    </div>
</div>
<!-- /#page-content-wrapper -->


<style>
    .service_content ul.service_content_last_list li{ display:inline-block; font-size:20px; width:38px; height:38px; line-height:38px; border:0px; }
    .service_content ul.service_content_last_list li a{ color:#333333; padding:0px; text-align:center; display:block; border:0px;  }
    .service_content ul.service_content_last_list li a:hover{ color:#fff; border-radius:50px; -moz-border-radius:50px; -webkit-border-radius:50px; background:#0072dc }
    .response_img img{ max-width: 100%; }
    .service_content .service_content_last_list{ text-align:center; padding:10px 0 70px; }
    ul.service_content_last_list  li.pagination-last-list{padding:0 20px 0 0px; cursor:pointer; text-indent:-9999px; margin-left:5px;  }
    .pagi_img{display: -webkit-inline-box;}
    .pagi_img a img{display: -webkit-inline-box;}
    .service_content ul.service_content_last_list li{margin:0px!important;}
    h1{font-size:32px!important;}
    .acu_bt a {
        text-decoration: none;
        color: #fff;
    }
    .sel_opt {
        font-size:15px;
        width:33% !important;
        padding-left: 9px !important;
    }
    #sorting td a {color: #303030!important; }
    #sorting td a:hover {color: #777!important; }

</style>

<script>
    $(function () {
        var url = '<?php echo base_url(); ?>';
        window.siteUrl = url;
        window.token = '<?php echo $token; ?>';
    });
    
    $('.delete-ticker').on('click', function () {
        var tickerId = $(this).attr('id');
        bootbox.confirm('Are you sure you want to delete?', function (result) {

            if (result) {
                var formData = new FormData();
                formData.append('tickerId', tickerId); 
                formData.append('pageNo', <?=$page?>);
                formData.append('accessToken', token);
                $.ajax({
                    url: siteUrl + "ticker/delete",
                    type: "POST",
                    data: formData,
                    processData: false, //prevent jQuery from converting your FormData into a string
                    contentType: false, //jQuery does not add a Content-Type header for you
                    success: function (result) {
                        location.reload();
                    },
                    error: function (result) {
                        location.reload();
                    }
                });
//                $.post(siteUrl + 'ticker/delete', {tickerId: tickerId, pageNo:<?=$page?>, accessToken: token}, function (data) {                    
//                    //alert(data);
//                    //bootbox.alert(data);
//                    location.reload();
//                });
//                    //location.reload();
            }
        });

    });
</script>


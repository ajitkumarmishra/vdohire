<?php
if (isset($ticker) && $ticker != NULL) {
    //echo '<pre>';print_r($ticker);  
}
//echo '<pre>'; print_r($subuser);
?>

<!-- Page Content -->
<div id="page-content-wrapper">
    <div class="container-fluid whitebg">
        <div class="row">
            <div class="col-md-12 padd_tp_bt_20">
                <div class="col-md-12"><h2><img src="<?php echo base_url(); ?>/theme/firstJob/image/arrow_head.png" class="prev-page">Ticker Details</h2></div>
                <form class="form-horizontal form_save" action="" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="inputEmail" class="control-label col-xs-2">Ticker Period</label>
                        <div class="col-xs-10" style="float: left">
                            <div style="float: left"> 
                                <div style="float: left">
                                    <div style="float: left; font-size: 16px; font-weight: 700">From&nbsp;&nbsp;</div>
                                    <div style="float: left"><?php if (isset($ticker['tickerStartDate'])): echo $ticker['tickerStartDate'];endif;?></div>
                                </div>
                                <div style="float: left; padding-left: 5px">
                                    <?php if (isset($ticker['tickerStartTime'])): echo substr($ticker['tickerStartTime'], 0, 5);endif;?>
                                </div>
                            </div>
                            <div style="float: left"> 
                                <div style="float: left">
                                    <div style="float: left; padding-left: 30px; font-size: 16px; font-weight: 700">To</div>
                                    <div style="float: left;  padding-left: 10px;"><?php if (isset($ticker['tickerEndDate'])): echo $ticker['tickerEndDate'];endif;?></div>
                                </div>
                                <div style="float: left; padding-left: 5px">
                                    <?php if (isset($ticker['tickerEndTime'])): echo substr($ticker['tickerEndTime'], 0, 5);endif;?>
                                </div>
                            </div>  
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="tickerTitle" class="control-label col-xs-2">Title</label>
                        <div class="col-xs-10">
                            <?php if (isset($ticker['tickerTitle'])): echo $ticker['tickerTitle'];endif;?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="tickerPriority" class="control-label col-xs-2">Ticker Priority</label>
                        <!--div class="col-xs-9">
                            <input type="text" class="form-control" placeholder="Title of Job*">
                        </div -->
                        <div class="col-xs-1">
                            <?=((isset($ticker['tickerPriority']) && $ticker['tickerPriority'] ==1)?"1":"")?>
                            <?=((isset($ticker['tickerPriority']) && $ticker['tickerPriority'] ==2)?"2":"")?>
                            <?=((isset($ticker['tickerPriority']) && $ticker['tickerPriority'] ==3)?"3":"")?>
                        </div>
                    </div>
                    <div class="form-group">
                        
                        <label for="tickerImage" class="control-label col-xs-2">Ticker Thumb</label>
                        <!--div class="col-xs-9">
                            <input type="text" class="form-control" placeholder="Title of Job*">
                        </div -->
                        <div class="col-xs-4">
                            <?php
                            if ( $ticker['tickerImage'] != "" ) {
                                echo "<img style=\"width: 200px;\" class=\"media-object\" src=\"".base_url()."/uploads/ticker/".$ticker['tickerImage']."\" alt=\"\">";
                            }
                            else {
                                echo "No Thumbnail Uploaded";
                            }
                            ?>
                        </div>
                    </div> 
                    <div class="clearfix">
                        <div class="form-group">
                            <label for="tickerTitle" class="control-label col-xs-2">Description</label>
                            <div class="col-xs-10">
                                <?php if (isset($ticker['tickerDesc'])): echo $ticker['tickerDesc'];endif;?>
                            </div>
                        </div>
                        <div class="clearfix">
                            <div class="col-xs-offset-2 col-xs-2 no_padding">
                                <input type="hidden" name="accessToken" value="<?php echo $token; ?>"/>
                                <button type="button" onclick="history.go(-1);" class="Save_frm">Back</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /#page-content-wrapper -->
    </div>
</div>

<script src="<?php echo base_url(); ?>theme/firstJob/js/chosen.jquery.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>theme/firstJob/js/prism.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
    var config = {
        '.chosen-select': {},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }

    $(function () {
        $("#datepicker-20").datepicker();
        $("#datepicker-21").datepicker();
    });
    
    
    function compareDate()
    {
        var startDate = $("#trickerStartDate").val();
        var endDate = $("#trickerEndDate").val();
        if(startDate>endDate)
        {
            alert("Your start date must be earlier than your end date.");
            return false;
        }
    }
    

</script>



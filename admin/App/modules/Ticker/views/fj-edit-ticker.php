<?php
if (isset($ticker) && $ticker != NULL) {
    //echo '<pre>';print_r($ticker);  
}
//echo '<pre>'; print_r($subuser);
?>
<script type="text/javascript">
$(document).ready( function(){
    $('#tickerStartTime').mTimePicker().mTimePicker( 'setTime', '<?php echo substr($ticker['tickerStartTime'],0,5); ?>' );
    $('#tickerEndTime').mTimePicker().mTimePicker( 'setTime', '<?php echo substr($ticker['tickerEndTime'],0,5); ?>' );
    //$('button#set').on('click', function(event) { $('#my_time').mTimePicker( 'setTime', '15:40' ); });
    //$('button#get').on('click', function(event) { alert( 'On timer is ' + $('#my_time').mTimePicker('getTime') ); });
});
</script>
<!-- Page Content -->
<div id="page-content-wrapper">
    <?php if($this->session->flashdata('flashmessagesuccess')){ 
            echo "<div class=\"alert alert-success\">".$this->session->flashdata('flashmessagesuccess')."</div>"; 
          } 
    ?>
    <?php if($this->session->flashdata('flashmessageerror')){ 
            echo "<div class=\"alert alert-danger\">".$this->session->flashdata('flashmessageerror')."</div>"; 
          } 
    ?>
    <?php if (isset($message)): ?>
        <div class="alert alert-success">
            <?php echo $message; ?>
        </div>
    <?php endif; ?>
    <?php if (isset($error)): ?>
        <div class="alert alert-danger">
            <?php foreach ($error as $item): ?>
                <?php echo $item; ?>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
    <div class="container-fluid whitebg">
        <div class="row">
            <div class="col-md-12 padd_tp_bt_20">
                <h2><img src="<?php echo base_url(); ?>/theme/firstJob/image/arrow_head.png" class="prev-page">Edit Ticker</h2>
                <form class="form-horizontal form_save" action="" method="post" enctype="multipart/form-data" id="tickerSet">
                    <div class="form-group">
                        <label for="inputEmail" class="control-label col-xs-2 required-field">Ticker Period</label>
                        <div class="col-xs-10" style="float: left">
                            <div style="float: left"> 
                                <div style="float: left">
                                    <div style="float: left">From&nbsp;&nbsp;</div>
                                    <div style="float: left">
                                        <input type="text" readonly="" data-validation="required" class="form-control tickerStartDate" style="width: 150px" placeholder="From Date" name="tickerStartDate" id="datepicker-20" value="<?php if (isset($ticker['tickerStartDate'])): echo $ticker['tickerStartDate'];endif;?>"></div>
                                </div>
                                <div style="float: left; padding-left: 20px">
                                    <input type="text" readonly="" class="form-control tickerStartTime" style="width: 80px" placeholder="From Date" name="tickerStartTime" id="tickerStartTime" value="<?php if (isset($ticker['tickerStartTime'])): echo $ticker['tickerStartTime'];endif;?>" data-validation="required">
                                </div>
                            </div>
                            <div style="float: left"> 
                                <div style="float: left">
                                    <div style="float: left; padding-left: 30px;">To&nbsp;&nbsp;</div>
                                    <div style="float: left;  padding-left: 10px;">
                                        <input type="text" readonly="" class="form-control tickerEndDate" style="width: 150px" placeholder="End Date" name="tickerEndDate" id="datepicker-21" value="<?php if (isset($ticker['tickerEndDate'])): echo $ticker['tickerEndDate'];endif;?>" data-validation="required"></div>
                                </div>
                                <div style="float: left; padding-left: 20px">
                                    <input type="text" readonly="" class="form-control tickerEndTime" style="width: 80px" placeholder="From Date" name="tickerEndTime" id="tickerEndTime" value="<?php if (isset($ticker['tickerEndTime'])): echo $ticker['tickerEndTime'];endif;?>" data-validation="required">
                                </div>
                            </div>  
                        </div>
                    </div>
                    
                                <div class="form-group">
                                    <label for="inputPassword" class="control-label col-xs-2"></label>
                                    <div class="col-xs-10">                                        
                                        <input type="text" data-validation="lessgreaterExp" style="height: 0px; width: 0px; visibility: hidden; " /> 
                                    </div>
                                </div>
                                
                    
                    <div class="form-group">
                        <label for="tickerTitle" class="control-label col-xs-2 required-field">Title <span style="font-size:10px;">(Limit 100 char)</span></label>
                        <div class="col-xs-10">
                            <input type="text" class="form-control" data-validation="required" placeholder="Title" name="tickerTitle" id="tickerTitle" value="<?php if (isset($ticker['tickerTitle'])): echo $ticker['tickerTitle'];endif;?>" maxlength="100">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="tickerPriority" class="control-label col-xs-2 required-field">Ticker Priority</label>
                        <!--div class="col-xs-9">
                            <input type="text" class="form-control" placeholder="Title of Job*">
                        </div -->
                        <div class="col-xs-10">
                            <select name="tickerPriority" id="tickerPriority" data-validation="required">
                                <option value="">--Please Select Priority--</option>
                                <option value="1" <?=((isset($ticker['tickerPriority']) && $ticker['tickerPriority'] ==1)?"selected":"")?>>1</option>
                                <option value="2" <?=((isset($ticker['tickerPriority']) && $ticker['tickerPriority'] ==2)?"selected":"")?>>2</option>
                                <option value="3" <?=((isset($ticker['tickerPriority']) && $ticker['tickerPriority'] ==3)?"selected":"")?>>3</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        
                        <label for="tickerImage" class="control-label col-xs-2">Ticker Thumb</label>
                        <!--div class="col-xs-9">
                            <input type="text" class="form-control" placeholder="Title of Job*">
                        </div -->
                        <div class="col-xs-1">
                            <?php
                            if ( $ticker['tickerImage'] != "" ) {
                                echo "<img style=\"width: 200px;\" class=\"media-object\" src=\"".base_url()."/uploads/ticker/".$ticker['tickerImage']."\" alt=\"\">";
                            }
                            ?>
                            <input type="file" name="tickerImage" id="tickerImage" value="" />
                        </div>
                    </div> 
                    <div class="clearfix">
                        <div class="form-group">
                            <label for="tickerDescription" class="control-label col-xs-2 required-field">Description</label>
                            <div class="col-xs-10">
                                <script type="text/javascript" src="//cdn.tinymce.com/4/tinymce.min.js"></script>
                                <script>
                                    tinymce.init({
                                        selector: 'textarea',
                                        height: 300,
                                        plugins: [
                                          'advlist autolink lists link image charmap print preview',
                                          'searchreplace visualblocks code fullscreen',
                                          'insertdatetime media table contextmenu paste code'
                                        ],
                                        toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
                                        content_css: [
                                          '//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
                                          '//www.tinymce.com/css/codepen.min.css'
                                        ]
                                    });
                                </script>
                                <textarea name="tickerDesc" class="tickerDescription" id="tickerDesc"><?php if (isset($ticker['tickerDesc'])): echo $ticker['tickerDesc'];endif;?></textarea>
                                <input type="text" data-validation="tickerDescription" style="height: 0px; width: 0px; visibility: hidden; " />
                            </div>
                        </div>
                    </div>
                    
                    <div class="clearfix"></div>
                    <div class="col-xs-offset-2 col-xs-2 no_padding">
                        <input type="hidden" name="accessToken" value="<?php echo $token; ?>"/>
                        <button type="submit" class="Save_frm">Update</button>
                    </div>
                </form>
            </div>
        </div>
        <!-- /#page-content-wrapper -->
    </div>
</div>

<script src="<?php echo base_url(); ?>theme/firstJob/js/chosen.jquery.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>theme/firstJob/js/prism.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
    var config = {
        '.chosen-select': {},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }

    $(function () {
        $("#datepicker-20").datepicker();
        $("#datepicker-21").datepicker();
    });
    
    
    function compareDate()
    {
        var startDate = $("#trickerStartDate").val();
        var endDate = $("#trickerEndDate").val();
        if(startDate>endDate)
        {
            alert("Your start date must be earlier than your end date.");
            return false;
        }
    }
    

</script>




<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script>

// Add validator For Interview Assessment
$.formUtils.addValidator({
    name : 'lessgreaterExp',
    validatorFunction : function(value, $el, config, language, $form) {
        if($('.tickerStartDate').val()!='' && $('.tickerEndDate').val()!=''){                
            if( (new Date($('.tickerStartDate').val()).getTime() > new Date($('.tickerEndDate').val()).getTime())) {
                return false;
            }
            else {
                return true;
            }
        }
        //return parseInt(value, 10) % 2 === 0;
    },
    errorMessage : 'End date should be greater than Start date'
});

  
// Add validator For Interview Assessment
$.formUtils.addValidator({
    name : 'tickerDescription',
    validatorFunction : function(value, $el, config, language, $form) {
        if(tinymce.get('tickerDesc').getContent()!='') {
            return true;
        }
        else {
            return false;
        }
        //return parseInt(value, 10) % 2 === 0;
    },
    errorMessage : 'This is a required field'
});

$.validate({
    form: '#tickerSet',
    modules: 'file',
    onError: function ($form) {
        //alert($('.setquestion').val());
        //alert($('.setInterviewType').html());
        return false;
    },
    onSuccess: function ($form) {
        return true;
    },
});
</script>



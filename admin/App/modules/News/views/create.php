<script src="<?php echo base_url('theme/plugins/ckeditor/ckeditor.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('theme/plugins/ckfinder/ckfinder.js'); ?>"></script>
<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="row title_head">
            <div class="col-sm-12 col-md-12">
                <hr/>
                <h4 class="pull-left"><a href="javascript:void(0);">Add News</a></h4>
                <div class="clear"></div>
                <hr/>
            </div>
        </div>
        <?php if (@$error): ?>
            <div class="alert">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <?php echo $error; ?>
            </div>
        <?php endif; ?>
        <form method="post" id="newsForm" action="">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="form-group">
                        <label for="news_title">News Title</label>
                        <div class="controls">
                            <input class="form-control" type="text" id="news_title" value="<?php echo set_value('news_title'); ?>" name="news_title">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <div class="col-sm-12 col-md-12">
                        <textarea id="editor1" name="news_description" class="form-control" rows="20"></textarea>
                    </div>
                </div>
                <script>
                    var editor = CKEDITOR.replace('editor1');
                    editor.config.extraAllowedContent = 'div(*)';
                    CKEDITOR.config.allowedContent = true;
                    CKEDITOR.disableAutoInline = true;
                    CKFinder.setupCKEditor(editor, '<?php echo base_url() ?>theme/plugins/ckfinder/');
                </script>
            </div>

            <div class="row">
                <div class="form-group">
                    <label for="news_cat">Category</label>
                    <div class="controls">
                        <select class="form-control" name="news_cat" id="news_cat">
                            <option value="">Select</option>
                            <option value="skills">Skills</option>
                            <option value="communication">Communication</option>
                            <option value="body_language">Body Language</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    <label for="news_meta_title">Meta Title</label>
                    <div class="controls">
                        <input type="text" id="news_meta_title" name="news_meta_title" class="form-control" value="" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <label for="news_meta_keywords">Meta Keywords</label>
                    <div class="controls">
                        <textarea id="news_meta_keywords" name="news_meta_keywords" class="form-control" rows="2" ></textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <label for="news_meta_description">Meta Description</label>
                    <div class="controls">
                        <textarea id="news_meta_description" name="news_meta_description" class="form-control" rows="2" ></textarea>
                    </div>
                </div>
            </div>
            <div class="form-actions no-margin-bottom" style="text-align:center;">
                <button type="submit" class="btn btn-primary button-submit">Submit</button>
            </div>
        </form>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="row title_head">
            <div class="col-sm-12 col-md-12">
                <hr/>
                <h4 class="pull-left"><a href="javascript:void(0);">Manage News</a></h4>
                <a href="<?php echo base_url('news/createNews'); ?>" class="btn btn-primary pull-right">Add New</a>
                <div class="clear"></div>
                <hr/>
            </div>
        </div>
        <form method="post" name="form1" id="form1" action="">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th><input name="check_all" type="checkbox" id="check_all" value="1" onclick="checkall(this.form)" /></th>
                        <th>Title</th>
                        <th>Category</th>
                        <th>Status</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (!empty($lists)) {
                        foreach ($lists as $list) {
                            ?>
                            <tr class="<?php echo ($list->is_active == '1' ? 'success' : 'warning'); ?>">
                                <td>
                                    <input name="arr_ids[]" type="checkbox" id="arr_ids[]" value="<?php echo $list->news_id; ?>" />
                                </td>
                                <td><?php echo $list->news_title; ?></td>
                                <td><?php echo ucfirst($list->news_cat); ?></td>
                                <td><?php echo ($list->is_active == '1' ? 'Active' : 'Inactive'); ?></td>
                                <td><a href="<?php echo base_url('news/editNews') . '/' . $list->news_id; ?>"><span class="glyphicon glyphicon-pencil pull-right"></a></td>
                                <td><a href="javascript:void(0);" class="btn btn-mini btn-xs del_Listing" name="<?php echo $list->news_id; ?>"><span class="glyphicon glyphicon-remove "></a></td>
                            </tr>
                            <?php
                        }
                    } else {
                        ?>
                        <tr>
                            <td colspan="6" align="center">
                                No result Found
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="left" style="padding:2px">
                        <input type="submit" name="Activate" value="Activate" class="btn"/>
                        <input type="submit" name="Deactivate" value="Deactivate" class="btn" />
                        <input type="submit" name="Delete" value="Delete" class="btn" />
                    </td>
                </tr>
            </table>
            <input type="hidden" id="P_orderby" value="news_id" />
            <input type="hidden" id="P_orderin" value="news_id" />
            <input type="hidden" id="P_page" value="1" />
            <input type="hidden" id="P_reqesturl" value="<?php echo base_url('news'); ?>" />
            <input type="hidden" id="P_deleteurl" value="<?php echo base_url('news/delete'); ?>" />
            <input type="hidden" id="P_responseDiv" value="responce_container" />
            <?php echo $links; ?>
        </form>
    </div>
</div>

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : News Model
 * Description : Handle all the CRUD operation for News
 * @author Synergy
 * @createddate : Nov 21, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 21, 2017
 */

class News_model extends CI_Model {

    /**
     * Declaring variables
     */
    var $news_table = "nc_news";

    /**
     * Responsable for inheriting parent constructor
     * @return void
     */
    function __construct() {
        parent::__construct();
    }

    /**
     * Description : Use to get list of news
     * Author : Synergy
     * @param int $showPerpage, int $offset
     * @return array of news data
     */
    function getList($showPerpage, $offset) {
        $conditions = array(
            'is_deleted' => '0'
        );
        $this->db->where($conditions);
        $this->db->limit($showPerpage, $offset);
        $this->db->from($this->news_table);
        $result = $this->db->get();
        return $result->result();
    }

    /**
     * Description : Use to update a particular news
     * Author : Synergy
     * @param array $_POST
     * @return boolean
     */
    function updateStatus() {
        if ($this->input->post('Delete') != null) {
            $arr_ids = $this->input->post('arr_ids');
            if (!empty($arr_ids)) {
                $this->db->where_in('news_id', $arr_ids);
                $this->db->delete($this->news_table);
            }
        } else {
            if ($this->input->post('Deactivate') != null) {
                $status = "0";
            } else if ($this->input->post('Activate') != null) {
                $status = "1";
            }
            $data = array(
                'is_active' => $status
            );
            $arr_ids = $this->input->post('arr_ids');
            if (!empty($arr_ids)) {
                $this->db->where_in('news_id', $arr_ids);
                $this->db->update($this->news_table, $data);
            }
        }
    }

    /**
     * Description : Use to count all the news
     * Author : Synergy
     * @param none
     * @return int
     */
    function count_all() {
        $conditions = array(
            'is_deleted' => '0'
        );
        $this->db->where($conditions);
        $query = $this->db->get($this->news_table);
        return $query->num_rows();
    }

    /**
     * Description : Use to delete a particular news
     * Author : Synergy
     * @param int $id
     * @return boolean
     */
    function deleteNews($id) {
        $this->db->where('news_id', $id);
        $query = $this->db->delete($this->news_table);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Description : Use to create a new news
     * Author : Synergy
     * @param array $_POST
     * @return boolean
     */
    public function createNews() {
        $data = $this->input->post();
        $this->db->set('news_creation_date', 'NOW()', false);
        $this->db->set('news_modification_date', 'NOW()', false);
        $query = $this->db->insert($this->news_table, $data);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Description : Use to update a specified news
     * Author : Synergy
     * @param array $_POST
     * @return boolean
     */
    function updateNews($id) {
        $data = $this->input->post();
        $this->db->where('news_id', $id);
        $this->db->set('news_modification_date', 'NOW()', false);
        $query = $this->db->update($this->news_table, $data);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Description : Use to get details of a news
     * Author : Synergy
     * @param int $id
     * @return array of news data
     */
    function getNews($id) {
        $this->db->where('news_id', $id);
        $this->db->from($this->news_table);
        $result = $this->db->get();
        return $result->row();
    }
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : News controller
 * Description : Used to handle all News related data
 * @author Synergy
 * @createddate : Nov 21, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 20, 2017
 */
class News extends MY_Controller {

    /**
     * Responsable for inheriting parent constructor
     * Responsable for auto load the the news_model
     * Responsable for auto load session, pagination
     * @return void
     */
    function __construct() {
        parent::__construct();
        $this->load->model('news_model', 'news');
        $this->load->library("session");
        $this->load->library('pagination');
    }

    /**
     * Description : List all the news
     * Author : Synergy
     * @param none
     * @return render data into view or redirect on news list page
     */
    function list_all() {
        check_auth();
        if(!$this->session->userdata('logged_in')) {
            redirect(base_url('signin'));
        }
        if ($this->input->post() != null) {
            $this->news->updateStatus();
        }
        $config = array();
        $config["base_url"] = base_url() . "news/list_all";
        $config['total_rows'] = $this->news->count_all();
        $config['per_page'] = (site_limit()) ? site_limit() : DEFAULT_LIMIT_PER_PAGE;
        $config['full_tag_open'] = '<div class="pagination pagination-small pagination-right"><ul class="pagination">';
        $config['full_tag_close'] = '</ul></div>';
        $config['first_link'] = true;
        $config['last_link'] = true;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["lists"] = $this->news->getList($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        $data['main_content'] = 'list';
        $data['meta_title'] = "News List";
        $data['meta_keyword'] = "This is an meta keyword";
        $data['meta_description'] = "This is an meta description";
        $includeJs = array('theme/js/news.js', 'theme/js/custom_listing.js', 'theme/js/bootbox.min.js');
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $this->load->view('page', $data);
    }

    /**
     * Description : Use to delete a particular news
     * Author : Synergy
     * @param array $_POST
     * @return json data
     */
    function delete() {
        $id = $this->input->post('delID');
        if (isset($id) and $id != '') {
            $check = $this->news->deleteNews($id);
            if ($check) {
                $data['status'] = true;
                $data['message'] = "Deleted Successfully";
                echo json_encode($data);
            } else {
                $data['status'] = false;
                $data['message'] = "Something Went wrong";
                echo json_encode($data);
            }
        } else {
            $data['status'] = false;
            $data['message'] = "Something Went wrong";
            echo json_encode($data);
        }
    }

    /**
     * Description : Use to create a new news
     * Author : Synergy
     * @param array $_POST
     * @return render data into view or redirect on news list page
     */
    public function createNews() {
        if(!$this->session->userdata('logged_in')) {
            redirect(base_url('signin'));
        }
        $includeJs = array('theme/js/news.js',
            'theme/js/bootbox.min.js',
            'theme/plugins/jquery-validation/lib/jquery.js',
            'theme/plugins/jquery-validation/dist/jquery.validate.js'
        );
        $includeCss = array('theme/plugins/jquery-validation/demo/css/screen.css');
        $data['includes_for_layout_css'] = add_includes('css', $includeCss);
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data['main_content'] = 'create';
        $this->load->library('form_validation');
        $this->form_validation->set_rules('news_title', 'Title', 'trim|required');
        if ($this->form_validation->run() == false) {
            $data['error'] = validation_errors();
            $this->load->view('page', $data);
        } else {
            $this->news->createNews();
            redirect(base_url('news'));
        }
    }

    /**
     * Description : Use to update a particular news
     * Author : Synergy
     * @param array $_POST
     * @return render data into view or redirect on news list page
     */
    public function editNews($id) {
        if(!$this->session->userdata('logged_in')) {
            redirect(base_url('signin'));
        }
        $includeJs = array('theme/js/news.js',
            'theme/js/bootbox.min.js',
            'theme/plugins/jquery-validation/lib/jquery.js',
            'theme/plugins/jquery-validation/dist/jquery.validate.js'
        );
        $includeCss = array('theme/plugins/jquery-validation/demo/css/screen.css');
        $data['includes_for_layout_css'] = add_includes('css', $includeCss);
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data['main_content'] = 'edit';
        $data['news'] = $this->news->getNews($id);
        $this->load->library('form_validation');
        $this->form_validation->set_rules('news_title', 'Title', 'trim|required');
        if ($this->form_validation->run() == false) {
            $data['error'] = validation_errors();
            $this->load->view('page', $data);
        } else {
            $this->news->updateNews($id);
            redirect(base_url('news'));
        }
    }

}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Notification Model
 * Description : Handle all the CRUD operation for Notification
 * @author Synergy
 * @createddate : Nov 21, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 21, 2017
 */

class Notification_model extends CI_Model {

    /**
     * Declaring variables
     */
    var $table = "fj_notification";

    /**
     * Responsable for auto load the the session and pagination library
     * @return void
     */
    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('pagination');
    }

    /**
     * Function Name : listNotification function
     * Discription : Use to get all notifications for logged in user.
     * @author Synergy
     * @param int $limit, int $offset, string $serachColumn, string $searchText, int $createdBy, int $userLoggeinId
     * @return array of notification data.
     */
    function listNotification($limit, $offset, $serachColumn = NULL, $searchText = NULL, $createdBy = NULL, $userLoggeinId) {
        try {
            $currentDate = date('Y-m-d h:i:s');
            $sql = "select N.* from fj_notification as N where userId='".$userLoggeinId."' ORDER BY createdAt DESC";
                        
            if(isset($offset)) {
                $sql .= " limit " .$offset. ", " .$limit."";
            }

            $query = $this->db->query($sql);
            $result = $query->result();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Function Name : countUnreadNotification function
     * Discription : Use to get all notification
     * @author Synergy
     * @param int $userId
     * @return array of notification data.
     */
    function countUnreadNotification($userId) {
        try {
            
            $query = $this->db->select("count(notifyId) as unreadcount")
                                ->from($this->table)
                                ->where('userId =', $messageId)
                                ->where('messageReadStatus = 0 or messageReadStatus = 1')
                                ->get();
            
            $result = $query->row_array();

            if (!$result) {
                throw new Exception('Unable to fetch unread message data.');
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Function Name : countUnreadTopBarNotification function
     * Discription : Use to get all unread notifications
     * @author Synergy
     * @param int $userId
     * @return array of notification data.
     */
    function countUnreadTopBarNotification($userId) {
        
        try {
            
            $query = $this->db->select("count(notifyId) as unreadcount")
                                ->from($this->table)
                                ->where('userId =', $userId)
                                ->where('messageReadStatus = 0')
                                ->get();
            
            $result = $query->row_array();
           
            if (!$result) {
                throw new Exception('Unable to fetch unread message data.');
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Function Name : updateNotificationForTopBarRead function
     * Discription : Use to update notification
     * @author Synergy
     * @param array $messagedata, int $userId
     * @return int or string or boolean
     */
    function updateNotificationForTopBarRead($messagedata, $userId) {
        
        try {
            $this->db->where('userId', $userId);
            
            if( $messagedata['messageReadStatus'] == 1 ){
                $this->db->where('messageReadStatus', 0);
            }
            
            $resp = $this->db->update($this->table, $messagedata);
            
            if (!$resp) {
                throw new Exception('Unable to update notification data.');
                return false;
            }
            return $id;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * updateNotificationForPageRead function
     * Use to update top notifications of a specific user
     * @param array $messagedata, int $userId
     * @return boolean.
     */
    function updateNotificationForPageRead($messagedata, $userId) {
        try {
            $this->db->where('userId', $userId);
            $resp = $this->db->update($this->table, $messagedata);
            if (!$resp) {
                throw new Exception('Unable to update notification data.');
                return false;
            }
            return $userId;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * latestNotificationForTopBar function
     * use to get top 3 notifications
     * @param int $userId.
     * @return array of notification
     */
    function latestNotificationForTopBar($userId) {
        
        try {
            
            $query = $this->db->select("*")
                                ->from($this->table)
                                ->where('userId =', $userId)
                                ->where('messageReadStatus', 0)
                                ->limit(0, 3)
                                ->get();
            
            $result = $query->result();

            if (!$result) {
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * deleteMessageThread function
     * Creates a to delete message thread
     * Author : Synergy
     * @params int $messageId
     * @return boolean
     */
    function deleteNotification($notificationId) {
        try {
            $this->db->where('notifyId', $notificationId);
            $this->db->delete($this->table);
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
}
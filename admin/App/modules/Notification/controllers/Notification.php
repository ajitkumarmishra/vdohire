<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Analytics Controller
 * Description : Used to handle all analytics related data
 * @author Synergy
 * @createddate : Nov 2, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 15, 2017
 */
class Notification extends MY_Controller {

    /**
     * Responsable for inheriting parent constructor
     * Responsable for auto loading notification_model
     * Responsable for auto load the form_validation, email and session library
     * Responsable for auto load the helper fj and url
     * Responsable for initializing global variable
     * @return void
     */
    function __construct() {
        parent::__construct();
        $this->load->model('notification_model');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('email');
        $this->load->helper('fj');
        $this->load->helper('url');
        $token = $this->config->item('accessToken');
        if (isset($_POST) && $_POST != NULL) {
            if ($token != $_POST['accessToken']) {
                echo 'You do not have pemission.';
                exit;
            }
        }
    }

    /**
     * listNotification function
     * Discription : Use to get list of notifications
     * @param int $page, array $_POST
     * @author Synergy
     * @return render data into view
     */
    function listNotification($page) {
        
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        //Jobs Listing Start
        $token = $this->config->item('accessToken');
        $userData = $this->session->userdata['logged_in'];
        
        $userLoggeinId = $userData->id;
        $userRole = $userData->role;

        $messagedata = array();
        $data['page'] = $page;
        $resp['error'] = 0;
        $serachColumn = $this->input->post('serachColumn', true);
        $searchText = $this->input->post('serachText', true);
        $token = $this->config->item('accessToken');
        $fromEmail = $this->config->item('fromEmail');
        $itemsPerPage = $this->config->item('itemsPerPage');
        $data['itemsPerPage'] = $itemsPerPage;
        $createdBy = NULL;
        if (!$page) {
            $page = 0;
        } else {
            $page = $page - 1;
        }
        
        $offset = $page * $itemsPerPage;
        try {
            
            /*
             * Update message status to 1 if admin click on top message icon
             */
            
            $messagedata['messageReadStatus'] = 2;
            $messagereadRes = $this->notification_model->updateNotificationForPageRead($messagedata, $userLoggeinId);
            
            $result = $this->notification_model->listNotification($itemsPerPage, $offset, $serachColumn, $searchText, $createdBy, $userLoggeinId);
            
            $completeResult = $this->notification_model->listNotification(NULL, NULL, NULL, NULL, $createdBy, $userLoggeinId);
            $count = ceil(count($completeResult) / $itemsPerPage);
            $data['count'] = $count;
            $data['totalCount'] = count($completeResult);
            $data['content'] = $result;

            if($userRole == 1)
                $data['main_content'] = 'fj-notification-listing';
            else
                $data['main_content'] = 'fj-recruiter-notification-listing';

            $data['searchText'] = $searchText;
            $data['serachColumn'] = $serachColumn;
            $data['userLoggeinId'] = $userLoggeinId;
            $data['token'] = $token;
            
            if($userRole == 1)
                $this->load->view('fj-mainpage', $data);
            else
                $this->load->view('fj-mainpage-recuiter', $data);

        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * listNotification function
     * Discription : Use to delete the notification
     * @param int $page, array $_POST
     * @author Synergy
     * @return render data into view
     */
    function delete() {
        $notificationId = $this->input->post('notificationsId', true);
        $this->notification_model->deleteNotification($notificationId);
        echo '1';exit;
    }
}
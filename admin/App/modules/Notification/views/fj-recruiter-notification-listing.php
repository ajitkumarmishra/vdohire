<!-- Page Content -->
<div id="page-content-wrapper" style="margin-top: 52px;">
    <div class="container-fluid whitebg">
        <div class="row">
            <div class="col-md-10" style="margin-top: 20px; margin-bottom: 10px;"><h4 style="border-bottom: 23px; font-size: 20px;"> Notifications </h4></div>
            
            <div class="clearfix"></div>
            <div class="col-md-12 rsp_tbl">
                <table class="table">
                    <thead class="tbl_head">
                        <tr>
                            <?php if(count($content) > 0) { ?>
                            <th><input type="checkbox" class="" id="ckbCheckAll"></th>
                            <?php } ?>
                            <th>Notification Message</th>
                            <th>Time</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        <?php
                        $i = 0; 

                        if( !empty($content) )
                        {
                            foreach ($content as $item){
                                
                        ?>    
                        <tr>
                            <td><input type="checkbox" class="checkBoxClass" value="<?php echo $item->notifyId; ?>" name="notifyIds[]" id="notifyIds"></td>
                            <td><?php echo ucwords($item->notifyMessage); ?></td>
                            <td><?php echo date('d-m-Y h:i:s A', strtotime($item->createdAt)); ?></td>
                        </tr>
                        <?php
                            }
                        } else { ?>
                        <tr><td>Notification data not available</td></tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div> 
            <?php if( !empty($content) ) { ?>
            <div class="col-md-12">  
                <div class="ed_job_bt">
                    <input type="hidden" name="hdnnotificationids" id="hdnnotificationids" value="" />
                    <button type="button" class="delete-notifications pull-left">Delete</button>
                </div>
            </div>
            <?php } ?>
            <div class="service_content ">

                <?php if ($totalCount > $itemsPerPage): ?>
                    <ul class="service_content_last_list">
                        <?php if ($page > 1): ?>
                            <p class="pagi_img"><a href="<?php echo base_url(); ?>notification/list/<?php echo $page - 1; ?>" ><img src="<?php echo base_url(); ?>/theme/firstJob/image/pagination_img2.png"></a></p>    
                        <?php endif; ?>
                        <input type="hidden" name="page_no" value="1" id="page_no"> 
                        <?php for ($i = 1; $i <= $count; $i++): ?>
                            <li><a href="<?php echo base_url(); ?>notification/list/<?php echo $i; ?>"  <?php if ($i == $page): ?>style="color: orange;"<?php endif; ?>><?php echo $i; ?></a></li>
                        <?php endfor; ?>
                        <?php if ($page < $count): ?>
                            <p class="pagi_img"><a href="<?php echo base_url(); ?>notification/list/<?php echo $page + 1; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/pagination_img1.png"></a></p>            
                        <?php endif; ?>
                    </ul>
                <?php endif; ?>
            </div>
              
        </div>
    </div>
</div>
<!-- /#page-content-wrapper -->

<script>
    
    $("#ckbCheckAll").click(function () {
        $(".checkBoxClass").prop('checked', $(this).prop('checked'));
        
        var output = $.map($(':checkbox[name=notifyIds\\[\\]]:checked'), function(n, i){
                return n.value;
          }).join(',');
          
        if( output ){
            $('#hdnnotificationids').val(output);
        } else {
            $('#hdnnotificationids').val('');
        }

    });
    
    $(".checkBoxClass").click(function () {
        
        var output = $.map($(':checkbox[name=notifyIds\\[\\]]:checked'), function(n, i){
                return n.value;
          }).join(',');
          
        if( output ){
            $('#hdnnotificationids').val(output);
        } else {
            $('#hdnnotificationids').val('');
        }

    });
    
    $(function () {
        var url = '<?php echo base_url(); ?>';
        window.siteUrl = url;
        window.token = '<?php echo $token; ?>';
    });
    
    //DELETE MESSAGE FROM SINGLE OR MULTIPLE SELECTION ON CHECKBOX
    $('.delete-notifications').on('click', function () {
        var notificationsId = $('#hdnnotificationids').val();
        
        if(notificationsId != '') {
            bootbox.confirm('Are you sure you want to delete?', function (result) {

                if (result) {
                    //alert(notificationsId);
                    $.post(siteUrl + 'notification/delete', {notificationsId: notificationsId, pageNo:<?=$page?>, accessToken: token}, function (data) {

                        //bootbox.alert(data);
                        location.reload();
                    });
                }
            });
        } else {
            bootbox.alert('Please check at least one notification message to delete!');
        }

    });
    
    $('.close-notification').on('click', function () {
        var notificationCloseData = $(this).attr('id');
        var notificationCloseDataRes = notificationCloseData.split('/');
        notificationId = notificationCloseDataRes[0];
        notificationStatus = notificationCloseDataRes[1];
        
        if( notificationStatus == 0 ){
            var messateStatusStr = "Close";
        } else if( notificationStatus == 1 ){
            var messateStatusStr = "Open";
        }
        //alert(notificationId);
        bootbox.confirm('Are you sure you want to '+messateStatusStr+'?', function (result) {

            if (result) {
                //alert(notificationId);
                $.post(siteUrl + 'notification/closenotification', {notificationId: notificationId, notificationStatus:notificationStatus, pageNo:<?=$page?>, accessToken: token}, function (data) {

                    //bootbox.alert(data);
                    location.reload();
                });
            }
        });

    });
</script>

<style>
/* Define how each icon button should look like */
.button {
  color: white;
  display: inline-block; /* Inline elements with width and height. TL;DR they make the icon buttons stack from left-to-right instead of top-to-bottom */
  position: relative; /* All 'absolute'ly positioned elements are relative to this one */
  padding: 2px 5px; /* Add some padding so it looks nice */
}


    /* Make the badge float in the top right corner of the button */
.button__badge {
  background-color: #51a6b9;
  border-radius: 2px;
  color: white;
 
  padding: 1px 3px;
  font-size: 10px;
  
  position: absolute; /* Position the badge within the relatively positioned button */
  top: 0;
  right: 0;
}
    </style>
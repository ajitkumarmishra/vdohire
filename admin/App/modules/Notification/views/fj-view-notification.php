<?php
if (isset($message) && $message != NULL) {
    //echo '<pre>';print_r($message);  
}
//echo '<pre>'; print_r($subuser);
?>

<!-- Page Content -->
<div id="page-content-wrapper">
    <?php if($this->session->flashdata('flashmessagesuccess')){ 
            echo "<div class=\"alert alert-success\">".$this->session->flashdata('flashmessagesuccess')."</div>"; 
          } 
    ?>
    <?php if($this->session->flashdata('flashmessageerror')){ 
            echo "<div class=\"alert alert-danger\">".$this->session->flashdata('flashmessageerror')."</div>"; 
          } 
    ?>
    <?php if (isset($message)): ?>
        <div class="alert alert-success">
            <?php echo $message; ?>
        </div>
    <?php endif; ?>
    <?php if (isset($error)): ?>
        <div class="alert alert-danger">
            <?php foreach ($error as $item): ?>
                <?php echo $item; ?>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
    <div class="container-fluid whitebg">
        <div class="row">
            <div class="col-md-12 padd_tp_bt_20">
                <h2>View Message</h2>
                <table cellpadding="5" cellspacing="5" border="0">
                    
                <?php
                $i = 1; 

                if( !empty($messageListRes) )
                {
                    foreach ($messageListRes as $item)
                    {
                        
                        if( $item['senderId'] == $userLoggedinId ){
                            $colorcode = "#f9a727";
                        } else {
                            $colorcode = "#51a6b9";
                        }
                        echo "<tr>"
                            . "<td><span style=\"color:".$colorcode."; font-size:16px; font-weight:700\">".$item['senderfullname']."</span></td>"
                            . "</tr>";
                        echo "<tr>"
                        . "<td style=\"padding: 10px; border-style: solid; border-color: #0000ff;\">"
                            . "<table cellpading=\"5\" cellspacing=\"0\" border=\"0\" style=\"width:900px\">"
                                . "<td>".$item['messageText']."</td>"
                                . "<td width=\"20px\">&nbsp;</td>";
                                if( $item['senderId'] != $userLoggedinId ){
                                    echo "<td valign=\"bottom\" align=\"right\"><a href=\"".base_url()."message/replymessage/messageid/".$item['messageId']."/touserid/".$item['messageToId']."\">Reply</a></td>";
                                }
                    echo "</table>"
                        ."</td>"
                        . "</tr>";
                        
                        echo "<tr><td>&nbsp;</td></tr>";
                        
                        $i++;
                    }
                }
                ?>                        
                </table>
                <div class="clearfix">
                    <div class="col-xs-offset-2 col-xs-2 no_padding">
                        <input type="hidden" name="accessToken" value="<?php echo $token; ?>"/>
                        <button type="button" onclick="history.go(-1);" class="Save_frm">Back</button>
                    </div>
                </div>
        </div>
        <!-- /#page-content-wrapper -->
    </div>
</div>

<script src="<?php echo base_url(); ?>theme/firstJob/js/chosen.jquery.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>theme/firstJob/js/prism.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
    var config = {
        '.chosen-select': {},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>



<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Tips Model
 * Description : Handle all the CRUD operation for Tips
 * @author Synergy
 * @createddate : Nov 29, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 22, 2017
 */

class Tips_model extends CI_Model {

    /**
     * Declaring static variables
     */
    var $tips_table = "nc_tips";

    /**
     * Responsable for inheriting parent constructor
     * @return void
     */
    function __construct() {
        parent::__construct();
    }

    /**
     * Description : Use to list of tips
     * Author : Synergy
     * @param int $showPerpage, int $offset
     * @return array of tips data
     */
    function getList($showPerpage, $offset) {
        $conditions = array(
            'is_deleted' => '0'
        );
        $this->db->where($conditions);
        $this->db->limit($showPerpage, $offset);
        $this->db->from($this->tips_table);
        $result = $this->db->get();
        return $result->result();
    }

    /**
     * Description : Use to update a specific tips
     * Author : Synergy
     * @param array $_POST
     * @return boolean
     */
    function updateStatus() {
        if ($this->input->post('Delete') != null) {
            $arr_ids = $this->input->post('arr_ids');
            if (!empty($arr_ids)) {
                $this->db->where_in('tips_id', $arr_ids);
                $this->db->delete($this->tips_table);
            }
        } else {
            if ($this->input->post('Deactivate') != null) {
                $status = "0";
            } else if ($this->input->post('Activate') != null) {
                $status = "1";
            }
            $data = array(
                'is_active' => $status
            );
            $arr_ids = $this->input->post('arr_ids');
            if (!empty($arr_ids)) {
                $this->db->where_in('tips_id', $arr_ids);
                $this->db->update($this->tips_table, $data);
            }
        }
    }

    /**
     * Description : Use to count all the tips
     * Author : Synergy
     * @param none
     * @return int of count of tips
     */
    function count_all() {
        $conditions = array(
            'is_deleted' => '0'
        );
        $this->db->where($conditions);
        $query = $this->db->get($this->tips_table);
        return $query->num_rows();
    }

    /**
     * Description : Use to delete a tips
     * Author : Synergy
     * @param int $id
     * @return boolean
     */
    function deleteTips($id) {
        $this->db->where('tips_id', $id);
        $query = $this->db->delete($this->tips_table);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Description : Use to create new tips
     * Author : Synergy
     * @param array $_POST
     * @return boolean
     */
    public function createTips() {
        $data = $this->input->post();
        $this->db->set('tips_creation_date','NOW()',false);
        $this->db->set('tips_modification_date','NOW()',false);
        $query = $this->db->insert($this->tips_table, $data);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Description : Use to update an existing tips
     * Author : Synergy
     * @param int $id
     * @return boolean
     */
    function updatePage($id) {
        $data = $this->input->post();
        $this->db->where('tips_id', $id);
        $this->db->set('tips_modification_date','NOW()',false);
        $query = $this->db->update($this->tips_table, $data);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Description : Use to get details of a specific tips
     * Author : Synergy
     * @param int $id
     * @return array of tips
     */
    function getTips($id) {
        $this->db->where('tips_id', $id);
        $this->db->from($this->tips_table);
        $result = $this->db->get();
        return $result->row();
    }
}
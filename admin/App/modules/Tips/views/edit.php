<script src="<?php echo base_url('theme/plugins/ckeditor/ckeditor.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('theme/plugins/ckfinder/ckfinder.js'); ?>"></script>
<div class="row">
    <div class="col-sm-12 col-md-12">
        <h2>Edit Tips</h2>
        <?php if (@$error): ?>
            <div class="alert">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <?php echo $error; ?>
            </div>
        <?php endif; ?>
        <form method="post" id="cmsForm" action="">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="form-group">
                        <label for="tips_title">Tips Title</label>
                        <div class="controls">
                            <input class="form-control" type="text" id="tips_title" value="<?php echo $tips->tips_title; ?>" name="tips_title">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <div class="col-sm-12 col-md-12">
                        <textarea id="editor1" name="tips_description" class="form-control" rows="20"><?php echo $tips->tips_description; ?></textarea>
                    </div>
                </div>
                <script>
                    var editor = CKEDITOR.replace('editor1');
                    editor.config.extraAllowedContent = 'div(*)';
                    CKEDITOR.config.allowedContent = true;
                    CKEDITOR.disableAutoInline = true;
                    CKFinder.setupCKEditor(editor, '<?php echo base_url() ?>theme/plugins/ckfinder/');
                </script>
            </div>
            <div class="row">
                <div class="form-group">
                    <label for="nc_cat">Category</label>
                    <div class="controls">
                        <select class="form-control" name="tips_cat" id="tips_cat">
                            <option value="">Select</option>
                            <option value="skills" <?php echo $tips->tips_cat == 'skills' ? 'selected="selected"' : ''; ?>>Skills</option>
                            <option value="communication" <?php echo $tips->tips_cat == 'communication' ? 'selected="selected"' : ''; ?>>Communication</option>
                            <option value="body_language" <?php echo $tips->tips_cat == 'body_language' ? 'selected="selected"' : ''; ?>>Body Language</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <label for="tips_meta_title">Meta Title</label>
                    <div class="controls">
                        <input type="text" id="tips_meta_title" name="tips_meta_title" class="form-control" value="<?php echo $tips->tips_meta_title; ?>" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <label for="tips_meta_keywords">Meta Keywords</label>
                    <div class="controls">
                        <textarea id="tips_meta_keywords" name="tips_meta_keywords" class="form-control" rows="2" ><?php echo $tips->tips_meta_keywords; ?></textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <label for="tips_meta_description">Meta Description</label>
                    <div class="controls">
                        <textarea id="tips_meta_description" name="tips_meta_description" class="form-control" rows="2" ><?php echo $tips->tips_meta_description; ?></textarea>
                    </div>
                </div>
            </div>
             <div class="row">
                <div class="form-group">
                    <label for="is_active">Status</label>
                    <div class="controls">
                        <select class="form-control" name="is_active" id="is_active">
                            <option value="">Select</option>
                            <option value="1" <?php echo $tips->is_active == '1' ? 'selected="selected"' : ''; ?>>Active</option>
                            <option value="0" <?php echo $tips->is_active == '0' ? 'selected="selected"' : ''; ?>>Inactive</option>
                        </select> 
                    </div>
                </div>
            </div>
            <div class="form-actions no-margin-bottom" style="text-align:center;">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
</div>

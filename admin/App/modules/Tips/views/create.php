<script src="<?php echo base_url('theme/plugins/ckeditor/ckeditor.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('theme/plugins/ckfinder/ckfinder.js'); ?>"></script>
<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="row title_head">
            <div class="col-sm-12 col-md-12">
                <hr/>
                <h4 class="pull-left"><a href="javascript:void(0);">Add Tips</a></h4>
                <div class="clear"></div>
                <hr/>
            </div>
        </div>
        <?php if (@$error): ?>
            <div class="alert">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <?php echo $error; ?>
            </div>
        <?php endif; ?>
        <form method="post" id="tipsForm" action="">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="form-group">
                        <label for="tips_title">Tips Title</label>
                        <div class="controls">
                            <input class="form-control" type="text" id="tips_title" value="<?php echo set_value('tips_title'); ?>" name="tips_title">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <div class="col-sm-12 col-md-12">
                        <textarea id="editor1" name="tips_description" class="form-control" rows="20"></textarea>
                    </div>
                </div>
                <script>
                    var editor = CKEDITOR.replace('editor1');
                    editor.config.extraAllowedContent = 'div(*)';
                    CKEDITOR.config.allowedContent = true;
                    CKEDITOR.disableAutoInline = true;
                    CKFinder.setupCKEditor(editor, '<?php echo base_url() ?>theme/plugins/ckfinder/');
                </script>
            </div>

            <div class="row">
                <div class="form-group">
                    <label for="tips_cat">Category</label>
                    <div class="controls">
                        <select class="form-control" name="tips_cat" id="tips_cat">
                            <option value="">Select</option>
                            <option value="skills">Skills</option>
                            <option value="communication">Communication</option>
                            <option value="body_language">Body Language</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group">
                    <label for="tips_meta_title">Meta Title</label>
                    <div class="controls">
                        <input type="text" id="tips_meta_title" name="tips_meta_title" class="form-control" value="" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <label for="tips_meta_keywords">Meta Keywords</label>
                    <div class="controls">
                        <textarea id="meta_keywords" name="tips_meta_keywords" class="form-control" rows="2" ></textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <label for="tips_meta_description">Meta Description</label>
                    <div class="controls">
                        <textarea id="tips_meta_description" name="tips_meta_description" class="form-control" rows="2" ></textarea>
                    </div>
                </div>
            </div>
            <div class="form-actions no-margin-bottom" style="text-align:center;">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
</div>

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Home Controller
 * Description : Used to handle home screen
 * @author Synergy
 * @createddate : Jun 2, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 17, 2017
 */

class Home extends MY_Controller{

	/**
     * Description : Use to display index page
     * Author : Synergy
     * @param none
     * @return render data into view
     */
	function index(){		
		$data['main_content'] = 'index';
		$this->load->view('full_width_banner', $data);
	}
}
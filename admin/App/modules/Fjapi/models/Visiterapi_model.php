<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Visiterapi Model
 * Description : Handle CRUD operation for Visitor
 * @author Synergy
 * @createddate : Jan 6, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 17, 2017
 */

class Visiterapi_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->model('Coreapi_model', 'coreModel');
    }
    
    /**
     * Description : Insert device id when user visit the app first time
     * Author : Synergy
     * @param array $params(deviceUniqueId)
     * @return array of data
     */
    public function visitor($params) {
        $deviceUniqueId     = $this->coreModel->cleanString($params['deviceUniqueId']);
        if($deviceUniqueId!='') {
            $query          = $this->db->query("SELECT visitorId FROM fj_visitor WHERE DATE(createdAt)=CURDATE() AND deviceUniqueId = '$deviceUniqueId' ");
            $total_records  = $query->num_rows();
            if($total_records=='0') {
                coreapi_model::$data['deviceUniqueId']       = $deviceUniqueId;
                $insert_id = $this->InsertVisitorInfo(coreapi_model::$data);                
                if($insert_id) {
                    $this->coreModel->codeMessage('200', $this->lang->line('success'));
                }
                else {
                    $this->coreModel->codeMessage('500', $this->lang->line('something_wrong'));
                }
            }
            else {
                $this->coreModel->codeMessage('200', $this->lang->line('success'));
            }
        }
        else {
            $this->coreModel->codeMessage('500', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;        
    }
    
    /**
     * Description : Insert device id when user visit the app first time
     * Author : Synergy
     * @param array $data
     * @return int inserted id
     */
    public function InsertVisitorInfo($data) {
        $this->db->set('createdAt', 'NOW()', FALSE);
        $this->db->insert('fj_visitor', $data);
        return $this->db->insert_id();
    }
}
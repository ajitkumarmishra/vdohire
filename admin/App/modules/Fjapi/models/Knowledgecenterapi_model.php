<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : knowledgecenterapi Model
 * Description : Handle all the CRUD operation for Knowledge Center
 * @author Synergy
 * @createddate : Aug 25, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 17, 2017
 */
class knowledgecenterapi_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->model('Coreapi_model', 'coreModel');
    }
    
    /**
     * Description : Use to all the category of knowledge center
     * Author : Synergy
     * @param array $params
     * @return array of data 
     */
    function getKnowledgeCenterCategory($params) {
        $knowledgeResult = $this->coreModel->queryResultArray("SELECT id, categoryName FROM fj_knowledgeCenterCategory WHERE status='1' ORDER BY createdAt DESC");
        if(count($knowledgeResult)>0) {
            foreach($knowledgeResult as $knowledgeRow){
                $rowData['id']          = $knowledgeRow['id'];
                $rowData['category']    = substr($knowledgeRow['categoryName'], 0, 24);
                coreapi_model::$data[]  = $rowData;
            }
            $this->coreModel->codeMessage('200', $this->lang->line('success'));
            coreapi_model::$returnArray['data']    = coreapi_model::$data; 
        }
        else {
            $this->coreModel->codeMessage('500', $this->lang->line('no_knowledge_category_found'));
        }
        return coreapi_model::$returnArray;
    }

    /**
     * Description : Use to all the category of knowledge center
     * Author : Synergy
     * @param array $params(categoryId)
     * @return array of data 
     */
    function getKnowledgeCenterTopics($params) {
        $categoryID = $this->coreModel->cleanString($params['categoryId']);
        if($categoryID!='') {
            $knowledgeResult = $this->coreModel->queryResultArray("
                                                SELECT 	C.id, C.categoryName, T.id as topicId, T.topic, T.description
                                                FROM 	fj_knowledgeCenterCategory	C
                                                JOIN 	fj_knowledgeCenterTopics	T
                                                WHERE 	C.id=T.categoryId	AND
                                                                C.status='1'		AND
                                                                T.status='1'		AND
                                                                C.id='$categoryID'
                                                ORDER BY T.topic ASC");
            if(count($knowledgeResult)>0) {
                if(count($knowledgeResult)==1) {
                    $rowData['topic']       = $knowledgeResult[0]['topic'];
                    $rowData['description'] = (string)base_url() . '/page/getKnowledgeCenter/category/'.encryptURLparam($knowledgeResult[0]['id'], URLparamKey);
                    coreapi_model::$data[]  = $rowData;
                    $this->coreModel->codeMessage('200', $this->lang->line('success'));
                    coreapi_model::$returnArray['data']         = coreapi_model::$data;
                    coreapi_model::$returnArray['contentType']  = 'contentDetail';
                }
                else {
                    foreach($knowledgeResult as $knowledgeRow){
                        if($knowledgeRow['topic']!='') {
                            $rowData['id']          = $knowledgeRow['topicId'];
                            $rowData['topic']       = $knowledgeRow['topic'];
                            coreapi_model::$data[]  = $rowData;
                        }
                    }
                    $this->coreModel->codeMessage('200', $this->lang->line('success'));
                    coreapi_model::$returnArray['data']         = coreapi_model::$data; 
                    coreapi_model::$returnArray['contentType']  = 'contentTopics';
                }
            }
            else {
                $this->coreModel->codeMessage('500', $this->lang->line('no_knowledge_topic_found'));
            }
        }
        else {
            $this->coreModel->codeMessage('500', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;        
    }

    /**
     * Description : Use to details of of a knowledge center(topic details)
     * Author : Synergy
     * @param array $params(topicId)
     * @return array of data 
     */
    function getKnowledgeCenterDetail($params) {
        $topicId = $this->coreModel->cleanString($params['topicId']);
        if($topicId!='') {
            // Knowledge Center Topic Details
            $knowledgeRow = $this->coreModel->queryRowArray("SELECT id, topic, description FROM fj_knowledgeCenterTopics WHERE status='1' AND id='$topicId' LIMIT 0,1");
            if(count($knowledgeRow)>0) {
                $rowData['topic']       = $knowledgeRow['topic'];
                $rowData['description'] = (string)base_url() . '/page/getKnowledgeCenter/topic/'.encryptURLparam($knowledgeRow['id'], URLparamKey);
                coreapi_model::$data[]  = $rowData;
                $this->coreModel->codeMessage('200', $this->lang->line('success'));
                coreapi_model::$returnArray['data']    = coreapi_model::$data; 
            }
            else {
                $this->coreModel->codeMessage('500', $this->lang->line('no_knowledge_topic_found'));
            }
        }
        else {
            $this->coreModel->codeMessage('500', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;        
    }
}
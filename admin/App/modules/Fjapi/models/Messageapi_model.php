<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Messageapi Model
 * Description : Handle all the CRUD operation for Message API
 * @author Synergy
 * @createddate : Oct 2, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 16, 2017
 */
/*
#############################################################
    -------------------- Api Listing --------------------
    =========USER MESSAGE API RELATED=========
        01. messageList
        02. messageDetail
        03. messageReply
#############################################################
*/
class messageapi_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->model('Coreapi_model', 'coreModel');
    }

    /**
     * Description : Return All Messages For a Particular App User
     * Author : Synergy
     * @param array $params(userId)
     * @return array of data 
     */
    public function messageList($params) {
        $userId     = $this->coreModel->cleanString($params['userId']);
        $id         = $this->coreModel->getJwtValue($userId);
        if($userId!='') {
            $userRow    = $this->coreModel->userExists($id);
            if(count($userRow)>0) {
                $query  =   "
                            SELECT 
                                M.*, U.fullname, C.company
                            FROM
                                fj_message  M
                            LEFT JOIN 
                                fj_users    U
                            ON
                                M.userTwoId = U.id
                            LEFT JOIN
                                fj_users    C
                            ON
                                M.userOneId = C.id
                            WHERE
                                U.status    = '1'  AND
                                U.role      = '3'  AND
                                U.id = '$id'
                            ";                            
                $messageResult = $this->coreModel->queryResultArray($query);
                
                if(count($messageResult)>0){
                    foreach($messageResult as $message) {
                        $rowData['id']          = (string)$message['messageId'];
                        $rowData['to']          = (string)$message['fullname'];
                        $rowData['from']        = (string)$message['company'];
                        $rowData['subject']     = (string)$message['messageSubject'];
                        $rowData['body']        = (string)$message['messageText'];
                        $rowData['time']        = (string)$message['createdAt'];
                        $rowData['openStatus']  = (string)($message['messageStatus']=='1'?'Open':'Closed');
                        $setQuestions[]         = $rowData;
                    }
                    $this->coreModel->codeMessage('200', $this->lang->line('success'));                            
                    coreapi_model::$returnArray['data']     = $setQuestions;
                }
                else {
                    $this->coreModel->codeMessage('501', $this->lang->line('no_message_found'));
                }
            }
            else {
                $this->coreModel->codeMessage('502', $this->lang->line('invalid_user'));
            }
        }
        else {
            $this->coreModel->codeMessage('202', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }
    
    /**
     * Description : Use to Get Message Details for a specific message and specific user
     * Author : Synergy
     * @param array $params(userId, messageId)
     * @return array of data 
     */
    public function messageDetail($params) {
        $messageId  = $this->coreModel->cleanString($params['messageId']);
        $userId     = $this->coreModel->cleanString($params['userId']);
        $id         = $this->coreModel->getJwtValue($userId);
        if($userId!='' && $messageId!='') {
            $userRow    = $this->coreModel->userExists($id);
            if(count($userRow)>0) {
                $query  =   "
                            SELECT 
                                MR.*,
                                M.messageStatus,
                                M.messageSubject,
                                M.messageText AS initialMessageBody,
                                IF(U.fullname='' || U.fullname=NULL,U.company,U.fullname)   AS replyBy,
                                IF(C.fullname='' || C.fullname=NULL,C.company,C.fullname)   AS messageBy
                            FROM
                                fj_message_reply    MR
                            LEFT JOIN 
                                fj_message          M
                            ON
                                MR.messageId = M.messageId
                            LEFT JOIN 
                                fj_users            U
                            ON
                                MR.replyById = U.id
                            LEFT JOIN
                                fj_users            C
                            ON
                                MR.messageToId = C.id
                            WHERE
                                MR.messageId = '$messageId'
                            "; 
                $messageResult = $this->coreModel->queryResultArray($query);
                
                if(count($messageResult)>0){
                    foreach($messageResult as $message) {
                        $rowData['id']          = (string)$message['messageId'];
                        $rowData['replyBy']     = (string)$message['replyBy'];
                        $rowData['messageBy']   = (string)$message['messageBy'];
                        $rowData['subject']     = (string)$message['messageSubject'];
                        $rowData['body']        = (string)$message['messageText'];
                        $rowData['time']        = (string)$message['createdAt'];
                        $rowData['openStatus']  = (string)($message['messageStatus']=='1'?'Open':'Closed');
                        $setQuestions[]         = $rowData;
                    }
                    $this->coreModel->codeMessage('200', $this->lang->line('success'));                            
                    coreapi_model::$returnArray['data']     = $setQuestions;
                }
                else {
                    $this->coreModel->codeMessage('501', $this->lang->line('no_message_detail_found'));
                }
            }
            else {
                $this->coreModel->codeMessage('502', $this->lang->line('invalid_user'));
            }
        }
        else {
            $this->coreModel->codeMessage('202', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }
    
    /**
     * Description : Use to reply on a message by specific user
     * Author : Synergy
     * @param array $params(userId, messageId, messageText)
     * @return array of data 
     */
    public function messageReply($params) {
        $messageId  = $this->coreModel->cleanString($params['messageId']);
        $messageText= $this->coreModel->cleanString($params['messageText']);
        $userId     = $this->coreModel->cleanString($params['userId']);
        $id         = $this->coreModel->getJwtValue($userId);
        if($userId!='' && $messageId!='' && $messageText!='') {
            $userRow    = $this->coreModel->userExists($id);
            if(count($userRow)>0) {
                $messageRow = $this->coreModel->queryRowArray("SELECT * FROM fj_message WHERE messageId='$messageId' LIMIT 0,1");
                if (count($messageRow) > 0) {
                    if($messageRow['messageStatus']=='1') {
                        // Data Array
                        $insertData = array(
                                            'messageId'         => $messageId,
                                            'replyById'         => $id,
                                            'messageToId'       => $messageRow['userOneId'],
                                            'messageText'       => $messageText,
                                            'messageReadStatus' => '2',
                                            'createdAt'         => date('Y-m-d H:i:s')
                                        );

                        $query = $this->db->insert('fj_message_reply', $insertData);
                        if($this->db->insert_id()) {
                            $dataArray['userId']    = $userId;
                            $dataArray['messageId'] = $messageId;
                            $this->messageDetail($dataArray);
                        } else {
                            $this->coreModel->codeMessage('500', $this->lang->line('something_wrong'));
                        }
                    }
                    else {
                        $this->coreModel->codeMessage('500', $this->lang->line('message_closed'));
                    }
                }
                else {
                    $this->coreModel->codeMessage('501', $this->lang->line('no_message_found'));
                }
            }
            else {
                $this->coreModel->codeMessage('502', $this->lang->line('invalid_user'));
            }
        }
        else {
            $this->coreModel->codeMessage('202', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }

    /**
     * Author : Synergy
     * Function Name : getMessageList function
     * Description : Get all the message for a particular user.
     * @params array(userId).
     * @return message list.
     * 
     */
    public function getMessageListForWeb($params) {
        $userId     = $this->coreModel->cleanString($params['userId']);
        $UserjobId     = $this->coreModel->cleanString($params['UserjobId']);
        $UserFromId     = $this->coreModel->cleanString($params['UserFromId']);
        $id         = $userId;
        if($userId!='') {
            $userRow    = $this->coreModel->userExists($id);
            if(count($userRow)>0) {
                $query  =   "
                            SELECT 
                                M.*, U.fullname, C.company AS senderName, C.companyLogo as corporateLogo
                            FROM
                                fj_message  M
                            LEFT JOIN 
                                fj_users    U
                            ON
                                M.userTwoId = U.id
                            LEFT JOIN
                                fj_users    C
                            ON
                                M.userOneId = C.id
                            WHERE
                                U.status    = '1'  AND
                                M.messageStatus      = 1  AND
                                M.userTwoId = '$id' AND M.UserjobId = '$UserjobId' AND M.userOneId='$UserFromId' ORDER BY M.createdAt DESC
                            ";
                            

                $messageResult = $this->coreModel->queryResultArray($query);
                //print_r($messageResult);exit;
                
                if(count($messageResult)>0){
                    $messageLists = array();
                    foreach($messageResult as $message) {
                        $rowData['messageId']          = (string)$message['messageId'];
                        $rowData['parentMessageId']          = (string)$message['parentMessageId'];
                        $rowData['messageSubject']          = (string)$message['messageSubject'];
                        $rowData['receiverName']          = (string)$message['fullname'];
                        $rowData['senderId']     = (string)$message['userOneId'];
                        $rowData['senderName']     = (string)$message['senderName'];

                        if($message['corporateLogo'])
                            $rowData['thumbnail']     = base_url().'uploads/corporateLogo/'.$message['corporateLogo'];
                        else
                            $rowData['thumbnail']     = '';
                        $rowData['message']        = (string)$message['messageText'];
                        $rowData['time']        = date('M d, Y h:i A', strtotime($message['createdAt']));
                        $rowData['openStatus']  = (string)($message['messageStatus']=='1'?'Open':'Closed');
                        $messageLists[]         = $rowData;
                    }
                    $this->coreModel->codeMessage('200', $this->lang->line('success'));                            
                    coreapi_model::$returnArray['data']     = $messageLists;
                }
                else {
                    $this->coreModel->codeMessage('501', $this->lang->line('no_message_found'));
                    coreapi_model::$returnArray['data'] = array();
                }
            }
            else {
                $this->coreModel->codeMessage('502', $this->lang->line('invalid_user'));
                coreapi_model::$returnArray['data'] = array();
            }
        }
        else {
            $this->coreModel->codeMessage('202', $this->lang->line('missing_required_fields'));
            coreapi_model::$returnArray['data'] = array();
        }
        return coreapi_model::$returnArray;
    }
    
    /**
     * Author : Synergy
     * Function Name : getMessageList function
     * Description : Get all the message for a particular user.
     * @params array(userId).
     * @return message list.
     * 
     */
    public function getMessageList($params) {
        $userId     = $this->coreModel->cleanString($params['userId']);
        $id         = $this->coreModel->getJwtValue($userId);
        if($userId!='') {
            $userRow    = $this->coreModel->userExists($id);
            if(count($userRow)>0) {
                $query  =   "
                            SELECT 
                                M.*, U.fullname, C.company AS senderName, C.companyLogo as corporateLogo
                            FROM
                                fj_message  M
                            LEFT JOIN 
                                fj_users    U
                            ON
                                M.userTwoId = U.id
                            LEFT JOIN
                                fj_users    C
                            ON
                                M.userOneId = C.id
                            WHERE
                                U.status    = '1'  AND
                                U.role      = '3'  AND
                                M.messageStatus      = 1  AND
                                M.userTwoId = '$id' ORDER BY M.createdAt DESC
                            ";

                $messageResult = $this->coreModel->queryResultArray($query);
                //print_r($messageResult);exit;
                
                if(count($messageResult)>0){
                    $messageLists = array();
                    foreach($messageResult as $message) {
                        $rowData['messageId']          = (string)$message['messageId'];
                        $rowData['parentMessageId']          = (string)$message['parentMessageId'];
                        $rowData['messageSubject']          = (string)$message['messageSubject'];
                        $rowData['receiverName']          = (string)$message['fullname'];
                        $rowData['senderId']     = (string)$message['userOneId'];
                        $rowData['senderName']     = (string)$message['senderName'];

                        if($message['corporateLogo'])
                            $rowData['thumbnail']     = base_url().'uploads/corporateLogo/'.$message['corporateLogo'];
                        else
                            $rowData['thumbnail']     = '';
                        $rowData['message']        = (string)$message['messageText'];
                        $rowData['time']        = date('M d, Y h:i A', strtotime($message['createdAt']));
                        $rowData['openStatus']  = (string)($message['messageStatus']=='1'?'Open':'Closed');
                        $messageLists[]         = $rowData;
                    }
                    $this->coreModel->codeMessage('200', $this->lang->line('success'));                            
                    coreapi_model::$returnArray['data']     = $messageLists;
                }
                else {
                    $this->coreModel->codeMessage('501', $this->lang->line('no_message_found'));
                }
            }
            else {
                $this->coreModel->codeMessage('502', $this->lang->line('invalid_user'));
            }
        }
        else {
            $this->coreModel->codeMessage('202', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }

    /**
     * Author : Synergy
     * Function Name : getMessageList function
     * Description : Get all the message for a particular user.
     * @params array(userId).
     * @return message list.
     * 
     */
    public function getSentMessageList($params) {
        $userId     = $this->coreModel->cleanString($params['userId']);
        $id         = $this->coreModel->getJwtValue($userId);
        if($userId!='') {
            $userRow    = $this->coreModel->userExists($id);
            if(count($userRow)>0) {
                $query  =   "
                            SELECT 
                                M.*, U.fullname, C.company AS receiverName, C.companyLogo as corporateLogo
                            FROM
                                fj_message  M
                            LEFT JOIN 
                                fj_users    U
                            ON
                                M.userOneId = U.id
                            LEFT JOIN
                                fj_users    C
                            ON
                                M.userTwoId = C.id
                            WHERE
                                U.status    = '1'  AND
                                U.role      = '3'  AND
                                M.messageStatus      = 1  AND
                                M.userOneId = '$id' ORDER BY M.createdAt DESC
                            ";                            
                $messageResult = $this->coreModel->queryResultArray($query);
                
                if(count($messageResult)>0){
                    $messageLists = array();
                    foreach($messageResult as $message) {
                        $rowData['messageId']          = (string)$message['messageId'];
                        $rowData['parentMessageId']          = (string)$message['parentMessageId'];
                        $rowData['messageSubject']          = (string)$message['messageSubject'];
                        $rowData['senderName']          = (string)$message['fullname'];
                        $rowData['senderId']     = (string)$message['userOneId'];
                        $rowData['receiverName']     = (string)$message['receiverName'];

                        if($message['corporateLogo'])
                            $rowData['thumbnail']     = base_url().'uploads/corporateLogo/'.$message['corporateLogo'];
                        else
                            $rowData['thumbnail']     = '';

                        $rowData['message']        = (string)$message['messageText'];
                        $rowData['time']        = date('M d, Y h:i A', strtotime($message['createdAt']));
                        $rowData['openStatus']  = (string)($message['messageStatus']=='1'?'Open':'Closed');
                        $messageLists[]         = $rowData;
                    }
                    $this->coreModel->codeMessage('200', $this->lang->line('success'));                            
                    coreapi_model::$returnArray['data']     = $messageLists;
                }
                else {
                    $this->coreModel->codeMessage('501', $this->lang->line('no_message_found'));
                }
            }
            else {
                $this->coreModel->codeMessage('502', $this->lang->line('invalid_user'));
            }
        }
        else {
            $this->coreModel->codeMessage('202', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }

    /**
     * Author : Synergy
     * Function Name : sendMessage function
     * Description : send message to specific user.
     * @params array(userId).
     * @return message list.
     * 
     */
    public function sendMessage($params) {
        $this->userId = $params['toId'];
        $userId     = $this->coreModel->cleanString($params['userId']);
        $toId     = $this->coreModel->cleanString($params['toId']);
        $messageText = $this->coreModel->cleanString($params['messageText']);
        $messageSubject = $this->coreModel->cleanString($params['messageSubject']);
        $parentMessageId = $this->coreModel->cleanString($params['parentMessageId']);
        $id         = $this->coreModel->getJwtValue($userId);

        if($userId!='' && $messageText!='' && $toId!='') {
            $userRow    = $this->coreModel->userExists($id);
            if(count($userRow)>0) {

                $insertData = array(
                                    'userOneId'         => $id,
                                    'parentMessageId'   => $parentMessageId,
                                    'userTwoId'         => $toId,
                                    'messageSubject'       => $messageSubject,
                                    'messageText'       => $messageText,
                                    'createdAt'         => date('Y-m-d H:i:s'),
                                    'updatedAt'         => date('Y-m-d H:i:s')
                                );

                $query = $this->db->insert('fj_message', $insertData);
                if($this->db->insert_id()) {
                    $dataArray['userId']    = $userId;
                    $dataArray['messageId'] = $this->db->insert_id();
                    $messageData[]         = $dataArray;

                    processApiSuccess($this);
                    $this->coreModel->codeMessage('200', $this->lang->line('success'));
                    coreapi_model::$returnArray['data'] = $messageData;
                } else {
                    $this->coreModel->codeMessage('500', $this->lang->line('something_wrong'));
                }
            }
            else {
                $this->coreModel->codeMessage('502', $this->lang->line('invalid_user'));
            }
        }
        else {
            $this->coreModel->codeMessage('202', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }

    public function sendMessageForWeb($params) {
        $this->userId = $params['toId'];
        $userId     = coreapi_model::cleanString($params['userId']);
        $toId     = coreapi_model::cleanString($params['toId']);
        $UserjobId     = coreapi_model::cleanString($params['UserjobId']);
        $messageText = coreapi_model::cleanString($params['messageText']);
        $messageSubject = coreapi_model::cleanString($params['messageSubject']);
        $parentMessageId = coreapi_model::cleanString($params['parentMessageId']);
        $id         = $userId;

        if($userId!='' && $messageText!='' && $toId!='') {
            $userRow    = coreapi_model::userExists($id);
            if(count($userRow)>0) {

                $insertData = array(
                                    'userOneId'         => $id,
                                    'parentMessageId'   => $parentMessageId,
                                    'userTwoId'         => $toId,
                                    'messageSubject'       => $messageSubject,
                                    'messageText'       => $messageText,
                                    'UserjobId'       => $UserjobId,
                                    'createdAt'         => date('Y-m-d H:i:s'),
                                    'updatedAt'         => date('Y-m-d H:i:s')
                                );

                $query = $this->db->insert('fj_message', $insertData);
                if($this->db->insert_id()) {
                    $dataArray['userId']    = $userId;
                    $dataArray['messageId'] = $this->db->insert_id();
                    $messageData[]         = $dataArray;

                    processApiSuccess($this);
                    coreapi_model::codeMessage('200', $this->lang->line('success'));
                    coreapi_model::$returnArray['data'] = $messageData;
                } else {
                    coreapi_model::codeMessage('500', $this->lang->line('something_wrong'));
                }
            }
            else {
                coreapi_model::codeMessage('502', $this->lang->line('invalid_user'));
            }
        }
        else {
            coreapi_model::codeMessage('202', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }

    /**
     * Author : Synergy
     * Function Name : getMessageDetails function
     * Description : get details of message.
     * @params array(userId, messageId).
     * @return message list.
     * 
     */
    public function getMessageDetails($params) {
        $messageId  = $this->coreModel->cleanString($params['messageId']);
        $userId     = $this->coreModel->cleanString($params['userId']);
        $id         = $this->coreModel->getJwtValue($userId);
        if($userId!='' && $messageId!='') {
            $userRow    = $this->coreModel->userExists($id);
            if(count($userRow)>0) {
                $query  =   "
                            SELECT 
                                M.*, U.fullname, C.fullname AS senderName
                            FROM
                                fj_message  M
                            LEFT JOIN 
                                fj_users    U
                            ON
                                M.userTwoId = U.id
                            LEFT JOIN
                                fj_users    C
                            ON
                                M.userOneId = C.id
                            WHERE
                                U.status    = '1'  AND
                                U.role      = '3'  AND
                                M.messageStatus      = 1  AND
                                M.userTwoId = '$id'
                            ";                            
                $messageResult = $this->coreModel->queryRowArray($query);
                
                if(count($messageResult)>0){
                    $rowData['messageId']          = (string)$messageResult['messageId'];
                    $rowData['parentMessageId']          = (string)$messageResult['parentMessageId'];
                    $rowData['messageSubject']          = (string)$message['messageSubject'];
                    $rowData['receiverName']          = (string)$messageResult['fullname'];
                    $rowData['senderId']     = (string)$messageResult['userOneId'];
                    $rowData['senderName']     = (string)$messageResult['senderName'];
                    $rowData['message']        = (string)$messageResult['messageText'];
                    $rowData['time']        = date('M d, Y h:i A', strtotime($message['createdAt']));
                    $rowData['openStatus']  = (string)($messageResult['messageStatus']=='1'?'Open':'Closed');

                    $this->coreModel->codeMessage('200', $this->lang->line('success'));                            
                    coreapi_model::$returnArray['data']     = $rowData;
                }
                else {
                    $this->coreModel->codeMessage('501', $this->lang->line('no_message_detail_found'));
                }
            }
            else {
                $this->coreModel->codeMessage('502', $this->lang->line('invalid_user'));
            }
        }
        else {
            $this->coreModel->codeMessage('202', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }

    /**
     * Author : Synergy
     * Function Name : sendMessage function
     * Description : send message to specific user.
     * @params array(userId).
     * @return message list.
     * 
     */
    public function deleteMessage($params) {
        $messageId  = $this->coreModel->cleanString($params['messageId']);
        $userId     = $this->coreModel->cleanString($params['userId']);
        $id         = $this->coreModel->getJwtValue($userId);

        if($userId!='' && $messageId!='') {
            $userRow    = $this->coreModel->userExists($id);
            if(count($userRow)>0) {

                $updateData = array(
                                    'messageStatus'       => 0,
                                    'updatedAt'         => date('Y-m-d H:i:s')
                                );

                $this->db->where('messageId', $messageId)->update('fj_message', $updateData);

                if($this->db->affected_rows() > 0) {
                    $this->coreModel->codeMessage('200', $this->lang->line('success'));
                } else {
                    $this->coreModel->codeMessage('500', $this->lang->line('something_wrong'));
                }
            }
            else {
                $this->coreModel->codeMessage('502', $this->lang->line('invalid_user'));
            }
        }
        else {
            $this->coreModel->codeMessage('202', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }
}
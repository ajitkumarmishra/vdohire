<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Practiseassessmentapi Model
 * Description : Handle all the CRUD operation for practiseassessmentapi
 * @author Synergy
 * @createddate : Aug 30, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 16, 2017
 */
class practiseassessmentapi_model extends CI_Model {
    
    function __construct() {
        parent::__construct();
        $this->load->model('Coreapi_model', 'coreModel');
    }
    
    /**
     * Description : Use to get All the practice assessment sets
     * Author : Synergy
     * @param array $params(userId)
     * @return array of data 
     */
    function practiceAssessmentSet($params) {        
        $userId     = $this->coreModel->cleanString($params['userId']);
        $id         = $this->coreModel->getJwtValue($userId);
        if($userId!='') {
            $userRow    = $this->coreModel->userExists($id);
            if(count($userRow)>0) {
                $query  = " SELECT 
                                id,
                                name,
                                duration
                            FROM
                                fj_assessments
                            WHERE
                                practiceSetStatus='1'	AND
                                status='1'";
                $result = $this->coreModel->queryResultArray($query);
                if(count($result)>0){
                    foreach($result as $assessmentSet) {
                        $rowData['set_id']      = (string)$assessmentSet['id'];
                        $rowData['set_name']    = (string)$assessmentSet['name'];
                        $rowData['set_duration']= (string)$assessmentSet['duration'];
                        $setQuestions[]   = $rowData;
                    }
                    $this->coreModel->codeMessage('200', $this->lang->line('success'));    
                    coreapi_model::$returnArray['data']     = $setQuestions;
                }
                else {
                    $this->coreModel->codeMessage('500', $this->lang->line('assessment_not_found'));
                }
            }
            else {
                $this->coreModel->codeMessage('500', $this->lang->line('invalid_user'));
            }
        }
        else {
            $this->coreModel->codeMessage('202', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }
    
    /**
     * Description : Use to get all details of a particular practice assessment set
     * Author : Synergy
     * @param array $params(userId, set_id)
     * @return array of data 
     */
    function practiceAssessmentSetDetail($params) {        
        $userId         = $this->coreModel->cleanString($params['userId']);
        $id             = $this->coreModel->getJwtValue($userId);
        $setId          = $this->coreModel->cleanString($params['set_id']);
        
        if($setId!='' && $userId!='') {
            $userRow    = $this->coreModel->userExists($id);
            if(count($userRow)>0) {                
                $resultAss  = $this->coreModel->queryRowArray("
                                                            SELECT 
                                                                *
                                                            FROM
                                                                fj_assessments
                                                            WHERE
                                                                id='$setId'
                                                            ");
                $totalDuration  = $resultAss['duration'];                
                $result  = $this->coreModel->queryResultArray("
                                                            SELECT 
                                                                *
                                                            FROM
                                                                fj_assessmentQuestions
                                                            WHERE
                                                                assessmentId='$setId'
                                                            ");
                $totalQuestion      = count($result);
                $eachQusDuration    = (int)($totalQuestion!=0?($totalDuration/$totalQuestion):0);
                
                if(count($result)>0) {
                    foreach($result as $resultSet) {
                        $qusResult  = $this->coreModel->queryResultArray("
                                                                    SELECT                                                                                     
                                                                        *
                                                                    FROM
                                                                        fj_question
                                                                    WHERE
                                                                        id='$resultSet[questionId]'
                                                                    ");
                        foreach($qusResult as $question) {                                        
                            $folderName                     = (($question['type']=='1' || $question['type']=='2')?($question['type']=='1'?'video':'audio'):'');
                            $rowData['questionId']          = (string)$question['id'];
                            $rowData['question_industry']   = '1';
                            $rowData['question_function']   = '1';
                            $rowData['question_type']       = (string)($question['type']=='1'?'video':($question['type']=='2'?'audio':'text'));                                        
                            $rowData['experience']          = '0';
                            $rowData['question_title']      = (string)$question['title'];
                            $rowData['question_file']       = (($question['type']=='1' || $question['type']=='2')?base_url() . '/uploads/questions/'.$folderName.'/' . $question['file']:'');
                            $rowData['question_duration']   = (string)$eachQusDuration;
                            $qusAnswer  = $this->coreModel->queryRowArray("
                                                                        SELECT                                                                                     
                                                                            *
                                                                        FROM
                                                                            fj_multipleChoiceAnswer
                                                                        WHERE
                                                                            id ='$question[answer]'
                                                                        ");
                            $rowData['question_ans']            = (string)$qusAnswer['option'];
                            $rowData['question_ans_duration']   = '0';
                            $qusChoiceResult  = $this->coreModel->queryResultArray("
                                                                        SELECT                                                                                     
                                                                            *
                                                                        FROM
                                                                            fj_multipleChoiceAnswer
                                                                        WHERE
                                                                            questionId ='$resultSet[questionId]'
                                                                        ");
                            $i=0;
                            foreach($qusChoiceResult as $questionChoice) {
                                $i++;
                                if($i==1) {
                                    $rowData['question_ans_one']    = (string)$questionChoice['option'];
                                }
                                if($i==2) {
                                    $rowData['question_ans_two']    = (string)$questionChoice['option'];
                                }
                                if($i==3) {
                                    $rowData['question_ans_three']    = (string)$questionChoice['option'];
                                }
                                if($i==4) {
                                    $rowData['question_ans_four']    = (string)$questionChoice['option'];
                                }
                            }                            
                        }
                        $rowData['question_creation_date']      = '1';
                        $rowData['question_modification_date']  = '1';
                        $rowData['question_status']             = '1';
                        $setQuestions[]   = $rowData;
                    }                        
                    $this->coreModel->codeMessage('200', $this->lang->line('success'));    
                    coreapi_model::$returnArray['data']     = $setQuestions;                    
                }
                else {
                    $this->coreModel->codeMessage('202', $this->lang->line('missing_required_fields'));
                }
            }
            else {
                $this->coreModel->codeMessage('202', $this->lang->line('missing_required_fields'));
            }
        }
        else {
            $this->coreModel->codeMessage('202', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }
}
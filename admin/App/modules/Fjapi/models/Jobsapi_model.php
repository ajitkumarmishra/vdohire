<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Jobsapi Model
 * Description : Handle all the CRUD operation for Jobsapi
 * @author Synergy
 * @createddate : Aug 25, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 16, 2017
 */

/*
#############################################################
    -------------------- Api Listing --------------------
    =========USER JOB RELATED=========
        01. userExists
        02. jobExists
        03. savedJobExists
        04. jobDetailQuery
        05. jobLocationQuery
        06. jobQualificationQuery   
        07. jobInterviewQuestionQuery
        08. jobAssessmentQuestionQuery
        09. jobSearchLocationQuery
        10. allLocationIdQuery
        11. allJobIdQuery 
        12. jobLocations 
        13. jobQualification 
        14. jobDaysLeft     
        15. verifyJob
        16. searchJobs
        17.     getJobLocation
        18. getJobDetail
        19. saveJob
        20.     removeSavedJob
        21.     getSavedJob
        22. applyJob
        23. getAppliedJob
#############################################################
*/
class jobsapi_model extends CI_Model { 
    
    function __construct() {
        parent::__construct();
        $this->load->model('Coreapi_model', 'coreModel');
    }
    
    //-------------------------------------------------------------------------//
    //------------------ Queries Used In JobsAPI Model Starts -----------------//
    //-------------------------------------------------------------------------//    
    
    /**
     * Description : Use to check user saved job exists or not
     * Author : Synergy
     * @param int $userId, int $jobId
     * @return array of data 
     */
    protected function savedJobExists($userId, $jobId) {
        $savedJobRow = $this->coreModel->queryRowArray("SELECT id FROM fj_userSavedJobs WHERE userId='$userId' AND jobId='$jobId' ORDER BY addedDate DESC LIMIT 0,1");
        return $savedJobRow;
    }
    
    /**
     * Description : Use to Get All Details Of A Job
     * Author : Synergy
     * @param int $fjJobId
     * @return array of data 
     */
    /*protected function jobDetailQuery($fjJobId){
        // find job created by
        // find role of created by
        // if role is 1 or 2 then ok
        // else if role is 4 then find the parent of user having role 4
        $jobExists = $this->coreModel->queryRowArray("SELECT createdBy FROM fj_jobs WHERE fjCode='$fjJobId' and status='1'");
        if(count($jobExists)>0) {
            $userExists = $this->coreModel->queryRowArray("SELECT id, role, createdBy FROM fj_users WHERE id='$jobExists[createdBy]' and status='1'");
            if(count($userExists)>0) {
                if($userExists['role']=='1' || $userExists['role']=='2') {
                    $jobRow = $this->coreModel->queryRowArray("
                                                            SELECT 
                                                                J.*, 
                                                                I.id            AS jobIndustryId, 
                                                                I.name          AS jobIndustryName,
                                                                U.company       AS company,
                                                                U.companyLogo   AS companyLogo,
                                                                U.aboutCompany  AS aboutCompany
                                                            FROM 
                                                                fj_jobs J 
                                                            JOIN 
                                                                nc_industry     I,
                                                                fj_users        U 
                                                            WHERE 
                                                                J.industryId=I.id   AND 
                                                                J.createdBy=U.id    AND 
                                                                BINARY J.fjCode=BINARY '$fjJobId' AND 
                                                                J.status='1'    AND
                                                                DATE(J.openTill)>=DATE(NOW())
                                                            ORDER BY
                                                                J.id DESC
                                                        ");
                }
                else {
                    $parentExists = $this->coreModel->queryRowArray("SELECT id, role, createdBy FROM fj_users WHERE id='$userExists[createdBy]' and status='1'");
                    if(count($parentExists)>0) {
                        if($parentExists['role']=='1' || $parentExists['role']=='2') {                            
                            $jobRow = $this->coreModel->queryRowArray("
                                                                    SELECT 
                                                                        J.*, 
                                                                        I.id            AS jobIndustryId, 
                                                                        I.name          AS jobIndustryName,
                                                                        U.company       AS company,
                                                                        U.companyLogo   AS companyLogo,
                                                                        U.aboutCompany  AS aboutCompany
                                                                    FROM 
                                                                        fj_jobs J 
                                                                    JOIN 
                                                                        nc_industry     I,
                                                                        fj_users        U 
                                                                    WHERE 
                                                                        J.industryId=I.id                   AND 
                                                                        U.id='$userExists[createdBy]'       AND
                                                                        BINARY J.fjCode=BINARY '$fjJobId'   AND 
                                                                        J.status='1'    AND
                                                                        DATE(J.openTill)>=DATE(NOW())
                                                                    ORDER BY
                                                                        J.id DESC
                                                                        
                                                                ");
                        }
                        else {
                            $jobRow;
                        }
                    }
                }
            }
        }
        return $jobRow;
    }*/

    protected function jobDetailQuery($fjJobId){
        // find job created by
        // find role of created by
        // if role is 1 or 2 then ok
        // else if role is 4 then find the parent of user having role 4
        $jobExists = $this->coreModel->queryRowArray("SELECT createdBy FROM fj_jobs WHERE fjCode='$fjJobId' and status='1'");
        if(count($jobExists)>0) {
            $userExists = $this->coreModel->queryRowArray("SELECT id, role, createdBy FROM fj_users WHERE id='$jobExists[createdBy]' and status='1'");
            if(count($userExists)>0) {
                if($userExists['role']=='1' || $userExists['role']=='2') {
                    $jobRow = $this->coreModel->queryRowArray("
                                                            SELECT 
                                                                J.*, 
                                                                I.id            AS jobIndustryId, 
                                                                I.name          AS jobIndustryName,
                                                                U.company       AS company,
                                                                U.companyLogo   AS companyLogo,
                                                                U.aboutCompany  AS aboutCompany
                                                            FROM 
                                                                fj_jobs J 
                                                            JOIN 
                                                                nc_industry     I,
                                                                fj_users        U 
                                                            WHERE 
                                                                J.industryId=I.id   AND 
                                                                J.createdBy=U.id    AND 
                                                                BINARY J.fjCode=BINARY '$fjJobId' AND 
                                                                J.status='1'  
                                                            ORDER BY
                                                                J.id DESC
                                                        ");
                }
                else {
                    $parentExists = $this->coreModel->queryRowArray("SELECT id, role, createdBy FROM fj_users WHERE id='$userExists[createdBy]' and status='1'");
                    if(count($parentExists)>0) {
                        if($parentExists['role']=='1' || $parentExists['role']=='2') {                            
                            $jobRow = $this->coreModel->queryRowArray("
                                                                    SELECT 
                                                                        J.*, 
                                                                        I.id            AS jobIndustryId, 
                                                                        I.name          AS jobIndustryName,
                                                                        U.company       AS company,
                                                                        U.companyLogo   AS companyLogo,
                                                                        U.aboutCompany  AS aboutCompany
                                                                    FROM 
                                                                        fj_jobs J 
                                                                    JOIN 
                                                                        nc_industry     I,
                                                                        fj_users        U 
                                                                    WHERE 
                                                                        J.industryId=I.id                   AND 
                                                                        U.id='$userExists[createdBy]'       AND
                                                                        BINARY J.fjCode=BINARY '$fjJobId'   AND 
                                                                        J.status='1' 
                                                                    ORDER BY
                                                                        J.id DESC
                                                                        
                                                                ");
                        }
                        else {
                            $jobRow;
                        }
                    }
                }
            }
        }
        return $jobRow;
    }
    
    /**
     * Description : Use to Get All Location For A Job
     * Author : Synergy
     * @param int $jobRowID
     * @return array of data 
     */
    protected function jobLocationQuery($jobRowID){ 
        $jobLocationResult = $this->coreModel->queryResultArray("
                                                            SELECT 
                                                                    JL.*, 
                                                                    CS.city, 
                                                                    CS.state, 
                                                                    CS.country 
                                                            FROM 
                                                                    fj_jobLocations JL 
                                                            JOIN 
                                                                    fj_cityState CS 
                                                            WHERE 
                                                                    JL.location=CS.id       AND 
                                                                    JL.jobId='$jobRowID'    AND
                                                                    status='1'
                                                        ");
        return $jobLocationResult;
    }

    /**
     * Description : Use to Get All Courses For A Job
     * Author : Synergy
     * @param int $jobRowID
     * @return array of data 
     */ 
    protected function jobQualificationQuery($jobRowID){ 
        $jobQualificationResult = $this->coreModel->queryResultArray("
                                                                SELECT 
                                                                        Q.*, 
                                                                        C.name AS qualification 
                                                                FROM 
                                                                        fj_jobQualifications Q 
                                                                JOIN 
                                                                        fj_courses C 
                                                                WHERE 
                                                                        Q.qualificationId=C.id  AND 
                                                                        Q.jobId='$jobRowID'
                                                            ");
        return $jobQualificationResult;
    }
    
    /**
     * Description : Use to Get All Interview Questions For A Job
     * Author : Synergy
     * @param int $interviewID
     * @return array of data 
     */ 
    protected function jobInterviewQuestionQuery($interviewID){
        $interviewQuestionsResult = $this->coreModel->queryResultArray("
                                                                SELECT
                                                                    IQ.id, Q.duration, Q.type
                                                                FROM
                                                                    fj_interviewQuestions IQ
                                                                JOIN
                                                                    fj_question Q
                                                                WHERE
                                                                    IQ.interviewId='$interviewID'   AND
                                                                    IQ.questionId=Q.id              AND
                                                                    IQ.status='1'                   AND
                                                                    Q.status='1'
                                                                ");
        return $interviewQuestionsResult;
    }
    
    /**
     * Description : Use to  Get All Assessment Questions For A Job
     * Author : Synergy
     * @param int $assessmentID
     * @return array of data 
     */
    protected function jobAssessmentQuestionQuery($assessmentID){
        $assessmentQuestionsResult = $this->coreModel->queryResultArray("
                                                                SELECT
                                                                    AQ.id, A.duration
                                                                FROM
                                                                    fj_assessmentQuestions AQ
                                                                JOIN
                                                                    fj_question     Q,
                                                                    fj_assessments  A
                                                                WHERE
                                                                    AQ.assessmentId='$assessmentID' AND
                                                                    AQ.questionId=Q.id	AND
                                                                    AQ.status='1'	AND
                                                                    Q.status='1'
                                                                GROUP BY AQ.id
                                                                ");
        return $assessmentQuestionsResult;
    }
    
    /**
     * Description : Use to Get All Location For Search Filter
     * Author : Synergy
     * @param string $locationName
     * @return array of data 
     */
    protected function jobSearchLocationQuery($locationName){
        $locationRow    = $this->coreModel->queryResultArray(" SELECT city FROM `fj_cityState` WHERE city LIKE '$locationName%'
                                                            UNION
                                                            SELECT state FROM `fj_cityState` WHERE state LIKE '$locationName%'
                                                            UNION
                                                            SELECT country FROM `fj_cityState` WHERE country LIKE '$locationName%' ");
        return $locationRow;
    }
    
    /**
     * Description : Use to Get All Location For A Search Result
     * Author : Synergy
     * @param string $locationValue
     * @return array of data 
     */
    protected function allLocationIdQuery($locationValue){
        $locationData   = $this->coreModel->queryRowArray("
                                                    SELECT GROUP_CONCAT(id) AS locationID
                                                    FROM
                                                        (
                                                        SELECT id FROM `fj_cityState` WHERE city LIKE '$locationValue%'
                                                        UNION
                                                        SELECT id FROM `fj_cityState` WHERE state LIKE '$locationValue%'
                                                        UNION
                                                        SELECT id FROM `fj_cityState` WHERE country LIKE '$locationValue%'
                                                        ) as cityStateID
                                                ");
        return $locationData;
    }
   
    /**
     * Description : Use to Get All Job Ids which are layed in $locationIDs
     * Author : Synergy
     * @param string $locationIDs
     * @return array of data 
     */
    protected function allJobIdQuery($locationIDs){
        $jobData   = $this->coreModel->queryRowArray("
                                                    SELECT GROUP_CONCAT( DISTINCT jobId ) AS jobID
                                                    FROM fj_jobLocations 
                                                    WHERE location IN ($locationIDs)
                                                ");
        return $jobData;
    }
    
    /**
     * Description : Use to get all location for a particular job
     * Author : Synergy
     * @param int $jobId
     * @return array of data 
     */ 
    protected function jobLocations($jobId){        
        $jobLocation = array();
        $jobLocationRow = $this->jobLocationQuery($jobId);
        if(count($jobLocationRow)>0) {
            foreach($jobLocationRow as $locationData) {
                $location['city']       = (string)$locationData['city'];
                $location['state']      = (string)$locationData['state'];
                $location['country']    = (string)$locationData['country'];
                $jobLocation[]  = $location;
            }
        }
        return $jobLocation;
    }
    
    /**
     * Description : Use to get all courses for a particular job
     * Author : Synergy
     * @param int $jobId
     * @return array of data 
     */ 
    protected function jobQualification($jobId){ 
        $qualification = array();
        $jobQualificationResult     = $this->jobQualificationQuery($jobId);
        if(count($jobQualificationResult)>0){
            foreach($jobQualificationResult as $jobQualification){
                $qualificationData['qualificationName']   = (string)$jobQualification['qualification'];
                $qualification[]    = $qualificationData;
            }
        }
        return $qualification;
    }
    
    /**
     * Description : Use to get days left for after passing date in argument
     * Author : Synergy
     * @param date jobOpenTill
     * @return string of days
     */
    protected function jobDaysLeft($jobOpenTill){
        $now        = date('Y-m-d', strtotime('now'));// or your date as well
        $your_date  = date('Y-m-d', strtotime($jobOpenTill));
        $datediff   = strtotime($your_date)-strtotime($now);
        $daysLeft   = floor($datediff/(60*60*24));
        return $daysLeft;
    }

    //-------------------------------------------------------------------------//
    //------------------- Queries Used In JobsAPI Model Ends ------------------//
    //-------------------------------------------------------------------------//
    
    

    //-------------------------------------------------------------------------//
    //----------------- Search Job Based On Referral Code ---------------------//
    //-------------------------------------------------------------------------//
    ############################################################################
    /*
     *  @Author : Wildnet
     *  @Params : array(user id, referrence job code)
     *  @Short Description : Search Job Based on Referral Job Code
     *  @return : array with code. 
     */
    //------------------- Referral Job Code - Search Starts -------------------//
    public function verifyJob($params) {
        $fjJobId        = $this->coreModel->cleanString($params['referrenceJobId']);
        $searchBy       = $this->coreModel->cleanString($params['searchBy']);
        $deviceUniqueId = $this->coreModel->cleanString($params['deviceUniqueId']);
        if($fjJobId!='' && $searchBy!='' && $deviceUniqueId!='') {
            // Job Information
            $jobRow     = $this->jobDetailQuery($fjJobId);
            if(count($jobRow)>0) {
                                
                $query          = $this->db->query("SELECT visitorId FROM fj_visitor WHERE page='$searchBy' AND DATE(createdAt)=CURDATE() AND deviceUniqueId = '$deviceUniqueId' ");
                $total_records  = $query->num_rows();

                if($total_records=='0') {
                    coreapi_model::$data['page']            = $searchBy;
                    coreapi_model::$data['deviceUniqueId']  = $deviceUniqueId;
                    $this->db->set('createdAt', 'NOW()', FALSE);
                    $this->db->insert('fj_visitor', coreapi_model::$data);
                }
            
                
                $trackData['deviceUniqueId']  = $deviceUniqueId;
                $trackData['pageVisitBy']     = $searchBy;
                $trackData['jobId']           = $jobRow['id'];
                $trackData['createdAt']       = date('Y-m-d H:i:s');
                $this->db->insert('fj_jobTracker', $trackData);
                
                
                // Interview Question List
                if(($jobRow['interview']!='') && ($jobRow['interview']!='0')){
                    $interviewQuestionsResult   = $this->jobInterviewQuestionQuery($jobRow['interview']);
                    $numberOfInterviewQuestion  = (count($interviewQuestionsResult)>0?count($interviewQuestionsResult):'0');
                    if(count($interviewQuestionsResult)) {
                        $totalDuration = 0;
                        foreach($interviewQuestionsResult as $durationValue) {
                            $totalDuration = $totalDuration+$durationValue['duration'];
                        }
                    }
                    $durationOfInterviewQuestion    = (count($interviewQuestionsResult)>0?$totalDuration:'0');
                    $typeOfInterviewQuestion        = (count($interviewQuestionsResult)>0?$interviewQuestionsResult[0]['type']:'0');
                }
                // Assessment Question List
                if(($jobRow['assessment']!='') && ($jobRow['assessment']!='0')){
                    $assessmentQuestionsResult      = $this->jobAssessmentQuestionQuery($jobRow['assessment']);
                    $numberOfAssessmentQuestion     = (count($assessmentQuestionsResult)>0?count($assessmentQuestionsResult):'0');

                    $randomQuestions = $this->db->query('SELECT SUM(noOfQuestions) noOfRandomQuestions FROM fj_assessmentRandomQuestionnaire WHERE assessmentId ='.$jobRow['assessment'])->row();
                    $numberOfAssessmentQuestion = $numberOfAssessmentQuestion + $randomQuestions->noOfRandomQuestions;
                    
                    $durationOfAssessmentQuestion   = (count($assessmentQuestionsResult)>0?$assessmentQuestionsResult[0]['duration']:'0');                    
                    $typeOfAssessmentQuestion       = (count($assessmentQuestionsResult)>0?$assessmentQuestionsResult[0]['type']:'0');
                }  
                
                $assessmentDurationResult = $this->coreModel->queryRowArray("
                                                                            SELECT
                                                                                A.id, A.duration
                                                                            FROM
                                                                                fj_assessments A
                                                                            WHERE
                                                                                A.id='$jobRow[assessment]'
                                                                            ");
                
                // Job Details
                $rowData['jobCode']         = (string)$jobRow['fjCode'];
                $rowData['title']           = (string)$jobRow['title'];
                $rowData['description']     = (string)$jobRow['description'];
                $rowData['vaccancies']      = (string)$jobRow['noOfVacancies'];
                $rowData['minAge']          = (string)$jobRow['ageFrom'];
                $rowData['maxAge']          = (string)$jobRow['ageTo'];
                $rowData['minSalary']       = (string)$this->coreModel->customNumberFormat($jobRow['salaryFrom']);
                $rowData['maxSalary']       = (string)$this->coreModel->customNumberFormat($jobRow['salaryTo']);
                $rowData['noticePeriod']    = (string)$jobRow['noticePeriod'];               
                $rowData['qualification']   = $this->jobQualification($jobRow['id']);                
                $rowData['experienceFrom']  = (string)$jobRow['expFrom'];
                $rowData['experienceTo']    = (string)$jobRow['expTo'];
                $rowData['openTill']        = (string)$jobRow['openTill'];                    
                $rowData['jdVideo']         = ($jobRow['jd']!='' && $jobRow['jd']!='0'?base_url() . '/uploads/jd/' . $jobRow['jd']:'');
                $rowData['jdThumbnail']     = ($jobRow['jdThumbnail']!=''?base_url() . '/uploads/jdThumbnail/' . $jobRow['jdThumbnail']:'');
                $rowData['industryId']      = (string)$jobRow['jobIndustryId'];
                $rowData['industryName']    = (string)$jobRow['jobIndustryName'];
                $rowData['companyName']     = (string)$jobRow['company'];
                $rowData['aboutCompany']    = (string)$jobRow['aboutCompany'];
                $rowData['companyLogo']     = ($jobRow['companyLogo']!=''?base_url() . '/uploads/corporateLogo/' . $jobRow['companyLogo']:'');
                $rowData['location']        = $this->jobLocations($jobRow['id']);
                $rowData['interviewQuestion']   = (string)(($jobRow['interview']!='' && $jobRow['interview']!='0')?$numberOfInterviewQuestion:'');
                $rowData['interviewDuration']   = (string)(($jobRow['interview']!='' && $jobRow['interview']!='0')?$durationOfInterviewQuestion:'');
                $rowData['interviewType']       = (string)(($jobRow['interview']!='' && $jobRow['interview']!='0')?($typeOfInterviewQuestion=='1'?'Video':($typeOfInterviewQuestion=='2'?'Audio':'Text')):'');
                $rowData['assessmentQuestion']  = (string)(($jobRow['assessment']!='' && $jobRow['assessment']!='0')?$numberOfAssessmentQuestion:'');
                $rowData['assessmentDuration']  = (string)(($jobRow['assessment']!='' && $jobRow['assessment']!='0')?$assessmentDurationResult['duration']:'');
                $rowData['assessmentType']      = (string)(($jobRow['assessment']!='' && $jobRow['assessment']!='0')?($typeOfAssessmentQuestion=='1'?'Video':($typeOfAssessmentQuestion=='2'?'Audio':'Text')):'');
                
                coreapi_model::$data[] = $rowData;
                $this->coreModel->codeMessage('200', $this->lang->line('success'));
                coreapi_model::$returnArray['data']    = coreapi_model::$data;
            }
            else {
                $this->coreModel->codeMessage('500', $this->lang->line('invalid_job_code'));
            }
        }
        else {
            $this->coreModel->codeMessage('300', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }
    //------------------- Referral Job Code - Search Ends ---------------------//
    ############################################################################
    //-------------------------------------------------------------------------//
    //----------------- Search Job Based On Referral Code ---------------------//
    //-------------------------------------------------------------------------//
    
    
    
    
    
    
    //-------------------------------------------------------------------------//
    //------------------------ Search Job Locations ---------------------------//
    //-------------------------------------------------------------------------//
    ############################################################################
    /*
     *  @Author : Wildnet
     *  @Params : desired location
     *  @Short Description : Search Jobs Location Based On User Filters
     *  @return : array with code. 
     */
    //-------------------- Search Locations For Job Starts --------------------//
    public function getJobLocation($params) {
        $locationName   = $this->coreModel->cleanString($params['locationName']);        
        if($locationName!='' ) {
            $locationRow    = $this->jobSearchLocationQuery($locationName);
            if(count($locationRow)>0){
                $locations = array();
                foreach($locationRow as $rowData){
                    $locationData['name']   = (string)$rowData['city'];
                    coreapi_model::$data[]  = $locationData;
                }
                $this->coreModel->codeMessage('200', $this->lang->line('success'));
                coreapi_model::$returnArray['data']    = coreapi_model::$data;
            }
            else {
                $this->coreModel->codeMessage('300', $this->lang->line('no_location_found'));
            }
        }
        else {
            $this->coreModel->codeMessage('300', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }
    //-------------------- Search Locations For Job Starts --------------------//
    ############################################################################
    //-------------------------------------------------------------------------//
    //------------------------ Search Job Locations ---------------------------//
    //-------------------------------------------------------------------------//
    
    
    
    
    
    
    //-------------------------------------------------------------------------//
    //---------------------------- Search Jobs --------------------------------//
    //-------------------------------------------------------------------------//
    ############################################################################
    /*
     *  @Author : Wildnet
     *  @Params : desired location, qualification, experience and certain keywords.
     *  @Short Description : Search Jobs Based On User Filters
     *  @return : array with code. 
     */
    //------------------------- General Job Search Starts ---------------------//
    /*public function searchJobs($params) {
        $keyword        = $this->coreModel->cleanString($params['keyword']);
        $locationName   = $this->coreModel->cleanString($params['locationName']);
        $yearsFrom      = $this->coreModel->cleanString($params['yearsFrom']);
        $yearsTo        = $this->coreModel->cleanString($params['yearsTo']);
        $salaryFrom     = $this->coreModel->cleanString($params['salaryFrom']);
        $salaryTo       = $this->coreModel->cleanString($params['salaryTo']);
        $qualification  = $this->coreModel->cleanString($params['qualification']);
        
        $startPoint     = ($this->coreModel->cleanString($params['startPoint'])!=''?$this->coreModel->cleanString($params['startPoint']):'0');
        $limitJobs      = ($this->coreModel->cleanString($params['jobsLimit'])!=''?$this->coreModel->cleanString($params['jobsLimit']):'5000');
        
        
        // All Location IDs This Search Criteria
        $concatQuery    = $this->db->query(" SET SESSION group_concat_max_len = 1000000 ");        
        $locationIDs    = '';
        $locations      = explode('|',$this->coreModel->cleanString($locationName));
        if(count($locations)>0){
            foreach($locations as $locationValue){
                $locationData   = $this->allLocationIdQuery($locationValue);
                $locationIDs    = ($locationIDs==''?$locationData['locationID']:$locationIDs.','.$locationData['locationID']);
            }
        }

        $jobData   = $this->allJobIdQuery($locationIDs);
        $jobIDs    = $jobData['jobID'];

        if(count($jobIDs)>0) {
            $query  = " 
                    SELECT
                        RES.*
                    FROM
                        (
                        SELECT
                            FT.*,
                            (SELECT email FROM fj_users U WHERE U.id=FT.createdBy) AS userName,
                            (SELECT role FROM fj_users U WHERE U.id=FT.createdBy)  AS userRole,
                            (
                                CASE
                                WHEN (SELECT role FROM fj_users U WHERE U.id=FT.createdBy)='1'
                                    THEN (SELECT company FROM fj_users WHERE id=FT.createdBy)
                                WHEN (SELECT role FROM fj_users U WHERE U.id=FT.createdBy)='2'
                                    THEN (SELECT company FROM fj_users WHERE id=FT.createdBy)
                                WHEN (SELECT role FROM fj_users U WHERE U.id=FT.createdBy)='4'
                                    THEN (SELECT company FROM fj_users WHERE id=(SELECT createdBy FROM fj_users WHERE id=FT.createdBy))
                                END
                            ) AS company,
                            (
                                CASE
                                WHEN (SELECT role FROM fj_users U WHERE U.id=FT.createdBy)='2'
                                    THEN (SELECT companyLogo FROM fj_users WHERE id=FT.createdBy)
                                WHEN (SELECT role FROM fj_users U WHERE U.id=FT.createdBy)='4'
                                    THEN (SELECT companyLogo FROM fj_users WHERE id=(SELECT createdBy FROM fj_users WHERE id=FT.createdBy))
                                END
                            ) AS companyLogo,
                            (
                                CASE
                                WHEN (SELECT role FROM fj_users U WHERE U.id=FT.createdBy)='2'
                                    THEN (SELECT aboutCompany FROM fj_users WHERE id=FT.createdBy)
                                WHEN (SELECT role FROM fj_users U WHERE U.id=FT.createdBy)='4'
                                    THEN (SELECT aboutCompany FROM fj_users WHERE id=(SELECT createdBy FROM fj_users WHERE id=FT.createdBy))
                                END
                            ) AS aboutCompany
                        FROM
                            (
                                SELECT * 
                                FROM (
                                    SELECT 
                                        J.*,
                                        I.id            AS jobIndustryId, 
                                        I.name          AS jobIndustryName,
                                        GROUP_CONCAT(Q.qualificationId) AS qualificationId
                                    FROM 
                                        fj_jobs J 
                                    JOIN 
                                        nc_industry             I,
                                        fj_jobQualifications    Q
                                    WHERE
                                        Q.jobId         = J.id          AND
                                        J.industryId    = I.id          AND 
                                        J.status        = '1'           AND
                                        J.posted        = '1'           AND
                                        J.id IN ($jobIDs)               AND
                                        DATE(J.openTill)>=DATE(NOW())
                                    GROUP BY 
                                        J.id
                                ) jobSearchTable 
                                WHERE
                                    id IS NOT NULL
                            ) FT
                        ) RES
                    WHERE
                    ";
            $query .= (" 1=1 ");
            $query .= ($keyword!=''?" AND ( title LIKE '%".$keyword."%' ":"");
            $query .= ($keyword!=''?" OR description LIKE '%".$keyword."%' ":"");
            $query .= ($keyword!=''?" OR company LIKE '%$keyword%' )":"");
            $query .= (($salaryFrom!='' AND $salaryTo!='')?"    AND ( (salaryFrom>=".$salaryTo." OR salaryTo>=".$salaryTo." OR (salaryFrom BETWEEN $salaryFrom AND $salaryTo) OR (salaryTo BETWEEN $salaryFrom AND $salaryTo)) AND (salaryFrom<=$salaryTo AND salaryTo<=$salaryTo) )  ":"");
            $query .= (($yearsFrom!='' AND $yearsTo!='')?"  AND (   (expFrom>=".$yearsTo." OR expTo>=".$yearsTo." OR (expFrom BETWEEN $yearsFrom AND $yearsTo) OR (expTo BETWEEN $yearsFrom AND $yearsTo)) AND (expFrom<=$yearsTo AND expTo<=$yearsTo) )  ":"");
        
            if($qualification!='') {
                $query .=   " AND
                            (   qualificationId LIKE'%,$qualification,%'	OR
                                qualificationId LIKE'$qualification,%'	OR
                                qualificationId LIKE'%,$qualification'	OR
                                qualificationId LIKE'$qualification'
                            )";
            }
        
            $query .= " ORDER BY RES.id DESC";
        
            $searchData         = $this->coreModel->queryResultArray($query);
            
            if(count($searchData)>0){
                $lastRowId = count($searchData);
                foreach($searchData as $jobRow){
                    // Job Data
                    $rowData['jobCode']         = (string)$jobRow['fjCode'];
                    $rowData['title']           = (string)$jobRow['title'];
                    $rowData['description']     = (string)$jobRow['description'];
                    $rowData['vaccancies']      = (string)$jobRow['noOfVacancies'];
                    $rowData['minAge']          = (string)$jobRow['ageFrom'];
                    $rowData['maxAge']          = (string)$jobRow['ageTo'];
                    $rowData['minSalary']       = (string)$this->coreModel->customNumberFormat($jobRow['salaryFrom']);
                    $rowData['maxSalary']       = (string)$this->coreModel->customNumberFormat($jobRow['salaryTo']);
                    $rowData['noticePeriod']    = (string)$jobRow['noticePeriod'];                   
                    $rowData['qualification']   = $this->jobQualification($jobRow['id']);
                    $rowData['experienceFrom']  = (string)$jobRow['expFrom'];
                    $rowData['experienceTo']    = (string)$jobRow['expTo'];
                    $rowData['openTill']        = (string)$jobRow['openTill'];
                    $rowData['industryId']      = (string)$jobRow['jobIndustryId'];
                    $rowData['industryName']    = (string)$jobRow['jobIndustryName'];
                    $rowData['companyName']     = (string)$jobRow['company'];
                    $rowData['aboutCompany']    = (string)$jobRow['aboutCompany'];
                    $rowData['companyLogo']     = ($jobRow['companyLogo']!=''?base_url() . '/uploads/corporateLogo/' . $jobRow['companyLogo']:'');
                    $rowData['daysLeft']        = (string)$this->jobDaysLeft($jobRow['openTill']);
                    $rowData['location']        = $this->jobLocations($jobRow['id']);;
                    coreapi_model::$data[]      = $rowData;
                }
                $this->coreModel->codeMessage('200', $this->lang->line('success'));
                coreapi_model::$returnArray['data']             = coreapi_model::$data;
                unset($params['token']);
                unset($params['methodname']);                
                coreapi_model::$returnArray['endPoint']         = (int)$lastRowId+(int)$startPoint;
                coreapi_model::$returnArray['searchCriteria']   = $params;
            }
            else {
                $this->coreModel->codeMessage('300', $this->lang->line('no_job_found'));
            } 
        }
        else {
            $this->coreModel->codeMessage('300', $this->lang->line('no_job_found'));
        }        
        return coreapi_model::$returnArray;
    }*/

    public function searchJobs($params) {
        $keyword        = $this->coreModel->cleanString($params['keyword']);
        $locationName   = $this->coreModel->cleanString($params['locationName']);
        $yearsFrom      = $this->coreModel->cleanString($params['yearsFrom']);
        $yearsTo        = $this->coreModel->cleanString($params['yearsTo']);
        $salaryFrom     = $this->coreModel->cleanString($params['salaryFrom']);
        $salaryTo       = $this->coreModel->cleanString($params['salaryTo']);
        $qualification  = $this->coreModel->cleanString($params['qualification']);
        
        $startPoint     = ($this->coreModel->cleanString($params['startPoint'])!=''?$this->coreModel->cleanString($params['startPoint']):'0');
        $limitJobs      = ($this->coreModel->cleanString($params['jobsLimit'])!=''?$this->coreModel->cleanString($params['jobsLimit']):'5000');
        
        
        // All Location IDs This Search Criteria
        $concatQuery    = $this->db->query(" SET SESSION group_concat_max_len = 1000000 ");        
        $locationIDs    = '';
        $jobIDs         = 0;

        if($locationName) {
            $locations = explode('|',$this->coreModel->cleanString($locationName));
        } else {
            $locations = array();
        }

        if(count($locations)>0){
            foreach($locations as $locationValue){
                $locationData   = $this->allLocationIdQuery($locationValue);
                $locationIDs    = ($locationIDs==''?$locationData['locationID']:$locationIDs.','.$locationData['locationID']);
            }
            $jobData   = $this->allJobIdQuery($locationIDs);
            $jobIDs    = $jobData['jobID'];
        }

        if($jobIDs > 0) {
            $inWhere = " AND J.id IN (".$jobIDs.")";
        } else {
            $inWhere = " AND 1=1";
        }

        $query  = " 
                SELECT
                    RES.*
                FROM
                    (
                    SELECT
                        FT.*,
                        (SELECT email FROM fj_users U WHERE U.id=FT.createdBy) AS userName,
                        (SELECT role FROM fj_users U WHERE U.id=FT.createdBy)  AS userRole,
                        (
                            CASE
                            WHEN (SELECT role FROM fj_users U WHERE U.id=FT.createdBy)='1'
                                THEN (SELECT company FROM fj_users WHERE id=FT.createdBy)
                            WHEN (SELECT role FROM fj_users U WHERE U.id=FT.createdBy)='2'
                                THEN (SELECT company FROM fj_users WHERE id=FT.createdBy)
                            WHEN (SELECT role FROM fj_users U WHERE U.id=FT.createdBy)='4'
                                THEN (SELECT company FROM fj_users WHERE id=(SELECT createdBy FROM fj_users WHERE id=FT.createdBy))
                            END
                        ) AS company,
                        (
                            CASE
                            WHEN (SELECT role FROM fj_users U WHERE U.id=FT.createdBy)='2'
                                THEN (SELECT companyLogo FROM fj_users WHERE id=FT.createdBy)
                            WHEN (SELECT role FROM fj_users U WHERE U.id=FT.createdBy)='4'
                                THEN (SELECT companyLogo FROM fj_users WHERE id=(SELECT createdBy FROM fj_users WHERE id=FT.createdBy))
                            END
                        ) AS companyLogo,
                        (
                            CASE
                            WHEN (SELECT role FROM fj_users U WHERE U.id=FT.createdBy)='2'
                                THEN (SELECT aboutCompany FROM fj_users WHERE id=FT.createdBy)
                            WHEN (SELECT role FROM fj_users U WHERE U.id=FT.createdBy)='4'
                                THEN (SELECT aboutCompany FROM fj_users WHERE id=(SELECT createdBy FROM fj_users WHERE id=FT.createdBy))
                            END
                        ) AS aboutCompany
                    FROM
                        (
                            SELECT * 
                            FROM (

                                SELECT 
                                    J.*,
                                    I.id            AS jobIndustryId, 
                                    I.name          AS jobIndustryName,
                                    GROUP_CONCAT(Q.qualificationId) AS qualificationId
                                FROM 
                                    fj_jobs J 
                                JOIN 
                                    nc_industry             I ON I.id = J.industryId 
                                LEFT JOIN 
                                    fj_jobQualifications    Q ON Q.jobId = J.id 
                                WHERE
                                    J.status        = '1'           AND
                                    (DATE(J.openTill)>=DATE(NOW()) OR J.openTill = '0000-00-00 00:00:00') AND 
                                    J.posted        = '1'           $inWhere
                                GROUP BY 
                                    J.id
                            ) jobSearchTable 
                            WHERE
                                id IS NOT NULL
                        ) FT
                    ) RES
                WHERE
                ";

        $query .= (" 1=1 ");

        if($keyword) {
            $query .= ($keyword!=''?" AND ( title LIKE '%".$keyword."%' ":"");
            $query .= ($keyword!=''?" OR description LIKE '%".$keyword."%' ":"");
            $query .= ($keyword!=''?" OR company LIKE '%$keyword%' )":"");
        }

        if($salaryFrom && $salaryTo) {
            $query .= (($salaryFrom!='' AND $salaryTo!='')?"    AND ( (salaryFrom>=".$salaryTo." OR salaryTo>=".$salaryTo." OR (salaryFrom BETWEEN $salaryFrom AND $salaryTo) OR (salaryTo BETWEEN $salaryFrom AND $salaryTo)) AND (salaryFrom<=$salaryTo AND salaryTo<=$salaryTo) )  ":"");
        }

        if($yearsFrom && $yearsTo) {
            $query .= (($yearsFrom!='' AND $yearsTo!='')?"  AND (   (expFrom>=".$yearsTo." OR expTo>=".$yearsTo." OR (expFrom BETWEEN $yearsFrom AND $yearsTo) OR (expTo BETWEEN $yearsFrom AND $yearsTo)) AND (expFrom<=$yearsTo AND expTo<=$yearsTo) )  ":"");
        }
    
        if($qualification!='') {
            $query .=   " AND
                        (   qualificationId LIKE'%,$qualification,%'    OR
                            qualificationId LIKE'$qualification,%'  OR
                            qualificationId LIKE'%,$qualification'  OR
                            qualificationId LIKE'$qualification'
                        )";
        }
    
        $query .= " ORDER BY RES.id DESC";
        
        $searchData         = $this->coreModel->queryResultArray($query);
        
        if(count($searchData)>0){
            $lastRowId = count($searchData);
            foreach($searchData as $jobRow){
                // Job Data
                $rowData['jobCode']         = (string)$jobRow['fjCode'];
                $rowData['title']           = (string)$jobRow['title'];
                $rowData['description']     = (string)$jobRow['description'];
                $rowData['vaccancies']      = (string)$jobRow['noOfVacancies'];
                $rowData['minAge']          = (string)$jobRow['ageFrom'];
                $rowData['maxAge']          = (string)$jobRow['ageTo'];
                $rowData['minSalary']       = (string)$this->coreModel->customNumberFormat($jobRow['salaryFrom']);
                $rowData['maxSalary']       = (string)$this->coreModel->customNumberFormat($jobRow['salaryTo']);
                $rowData['noticePeriod']    = (string)$jobRow['noticePeriod'];                   
                $rowData['qualification']   = $this->jobQualification($jobRow['id']);
                $rowData['experienceFrom']  = (string)$jobRow['expFrom'];
                $rowData['experienceTo']    = (string)$jobRow['expTo'];
                $rowData['openTill']        = (string)$jobRow['openTill'];
                $rowData['industryId']      = (string)$jobRow['jobIndustryId'];
                $rowData['industryName']    = (string)$jobRow['jobIndustryName'];
                $rowData['companyName']     = (string)$jobRow['company'];
                $rowData['aboutCompany']    = (string)$jobRow['aboutCompany'];
                $rowData['companyLogo']     = ($jobRow['companyLogo']!=''?base_url() . '/uploads/corporateLogo/' . $jobRow['companyLogo']:'');

                if($jobRow['openTill'] == '0000-00-00 00:00:00') {
                    $daysLefts = '1';
                } else {
                    $daysLefts = (string)$this->jobDaysLeft($jobRow['openTill']);
                }
                $rowData['daysLeft']        = $daysLefts;
                $rowData['location']        = $this->jobLocations($jobRow['id']);;
                coreapi_model::$data[]      = $rowData;
            }
            $this->coreModel->codeMessage('200', $this->lang->line('success'));
            coreapi_model::$returnArray['data']             = coreapi_model::$data;
            unset($params['token']);
            unset($params['methodname']);                
            coreapi_model::$returnArray['endPoint']         = (int)$lastRowId+(int)$startPoint;
            coreapi_model::$returnArray['searchCriteria']   = $params;
        }
        else {
            $this->coreModel->codeMessage('300', $this->lang->line('no_job_found'));
        }        
        return coreapi_model::$returnArray;
    }

    //------------------------- General Job Search Ends -----------------------//
    ############################################################################
    //-------------------------------------------------------------------------//
    //---------------------------- Search Jobs --------------------------------//
    //-------------------------------------------------------------------------//
    
    


    
    //-------------------------------------------------------------------------//
    //----------------------------- User's Job --------------------------------//
    //-------------------------------------------------------------------------//
    ############################################################################
    /*
     *  @Author : Wildnet
     *  @Params : array(job code, user id)
     *  @Short Description : Save Job Details
     *  @return : array with code. 
     */
    //---------------------- Save Job To User Account Starts ------------------//
    public function saveJob($params) {
        $userId         = $this->coreModel->cleanString($params['userId']);
        $id             = $this->coreModel->getJwtValue($userId);
        $fjJobId        = $this->coreModel->cleanString($params['jobCode']);
        if($fjJobId!='' && $userId!='') {
            $userRow    = $this->coreModel->userExists($id);
            if(count($userRow)>0){
                $jobRow = $this->coreModel->jobExists($fjJobId);
                if(count($jobRow)>0){
                    $savedJobRow = $this->savedJobExists($userRow['id'], $jobRow['id']);
                    if(count($savedJobRow)>0) { 
                        $whereData  = array(
                                            'userId'=> (int)$userRow['id'], 
                                            'jobId' => (int)$jobRow['id']
                                            );
                        $updateData = array('status'=>'1');
                        $updateJob  = $this->db->where($whereData)->update('fj_userSavedJobs', $updateData);                        
                        if($updateJob){
                            coreapi_model::$returnArray['code']    = '200';
                            coreapi_model::$returnArray['message'] = $this->lang->line('success');
                        }
                        else {                        
                            coreapi_model::$returnArray['code']    = '500';
                            coreapi_model::$returnArray['message'] = $this->lang->line('something_wrong');
                        }
                    }
                    else {
                        // Save Job Information
                        $jobData    = array(
                                            'userId'    => (int)$userRow['id'], 
                                            'jobId'     => (int)$jobRow['id'], 
                                            'status'    => '1');
                        $insertJob  = $this->db->insert('fj_userSavedJobs', $jobData);
                        if($insertJob){
                            $this->coreModel->codeMessage('200', $this->lang->line('success'));
                        }
                        else {                        
                            $this->coreModel->codeMessage('500', $this->lang->line('something_wrong'));
                        }
                    }
                }
                else {
                    $this->coreModel->codeMessage('500', $this->lang->line('invalid_job_code'));
                }
            }
            else {
                $this->coreModel->codeMessage('500', $this->lang->line('invalid_user'));
            }
        }
        else {
            $this->coreModel->codeMessage('300', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }
    //--------------------- Save Job To User Account Ends ---------------------//
    ############################################################################
    

    /*
     *  @Author : Wildnet
     *  @Params : array(job code, user id)
     *  @Short Description : Remove Saved Job Details
     *  @return : array with code.
     */
    //----------------- Remove Save Job To User Account Starts ----------------//
    public function removeSavedJob($params) {
        $userId         = $this->coreModel->cleanString($params['userId']);
        $id             = $this->coreModel->getJwtValue($userId);
        $fjJobId        = $this->coreModel->cleanString($params['jobCode']);
        if($fjJobId!='' && $userId!='') {
            $userRow    = $this->coreModel->userExists($id);
            if(count($userRow)>0){
                $jobRow = $this->coreModel->jobExists($fjJobId);
                if(count($jobRow)>0){
                    $savedJobRow = $this->savedJobExists($userRow['id'], $jobRow['id']);
                    if(count($savedJobRow)>0) {                        
                        $updateData     = array('status'=>'0');
                        $whereData      = array(
                                                'userId'    => (int)$userRow['id'],
                                                'jobId'     => (int)$jobRow['id'],
                                                'status'    => '1'
                                            );
                        $queryExecute   = $this->db->where($whereData)->update('fj_userSavedJobs',$updateData);
                        $this->coreModel->codeMessage('200', $this->lang->line('success'));
                    }
                    else {
                        $this->coreModel->codeMessage('500', $this->lang->line('no_saved_job_found'));
                    }
                }
                else {
                    $this->coreModel->codeMessage('500', $this->lang->line('invalid_job_code'));
                }
            }
            else {
                $this->coreModel->codeMessage('500', $this->lang->line('invalid_user'));
            }
        }
        else {
            $this->coreModel->codeMessage('300', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }
    //----------------- Remove Save Job To User Account Ends ------------------//
    ############################################################################
    
    /*
     *  @Author : Wildnet
     *  @Params : array(user id)
     *  @Short Description : Send All Saved Job Details
     *  @return : array with code. 
     */
    //--------------- Send All Saved Job To User Account Starts ---------------//
    public function getSavedJob($params) {
        $userId         = $this->coreModel->cleanString($params['userId']);
        $id             = $this->coreModel->getJwtValue($userId);
        if($userId!='') {
            $userRow    = $this->coreModel->userExists($id);
            if(count($userRow)>0){
                $checkResult = $this->coreModel->queryRowArray("SELECT GROUP_CONCAT(jobId) AS id FROM fj_userSavedJobs WHERE userId='".$userRow['id']."' AND status='1'");                
                if($checkResult['id']!='') {
                    $jobIds = $checkResult['id'];
                    $query = "  SELECT
                                    FT.*,
                                    (SELECT email FROM fj_users U WHERE U.id=FT.createdBy) AS userName,
                                    (SELECT role FROM fj_users U WHERE U.id=FT.createdBy)  AS userRole,
                                    (
                                        CASE
                                        WHEN (SELECT role FROM fj_users U WHERE U.id=FT.createdBy)='2'
                                            THEN (SELECT company FROM fj_users WHERE id=FT.createdBy)
                                        WHEN (SELECT role FROM fj_users U WHERE U.id=FT.createdBy)='4'
                                            THEN (SELECT company FROM fj_users WHERE id=(SELECT createdBy FROM fj_users WHERE id=FT.createdBy))
                                        END
                                    ) AS company,
                                    (
                                        CASE
                                        WHEN (SELECT role FROM fj_users U WHERE U.id=FT.createdBy)='2'
                                            THEN (SELECT companyLogo FROM fj_users WHERE id=FT.createdBy)
                                        WHEN (SELECT role FROM fj_users U WHERE U.id=FT.createdBy)='4'
                                            THEN (SELECT companyLogo FROM fj_users WHERE id=(SELECT createdBy FROM fj_users WHERE id=FT.createdBy))
                                        END
                                    ) AS companyLogo,
                                    (
                                        CASE
                                        WHEN (SELECT role FROM fj_users U WHERE U.id=FT.createdBy)='2'
                                            THEN (SELECT aboutCompany FROM fj_users WHERE id=FT.createdBy)
                                        WHEN (SELECT role FROM fj_users U WHERE U.id=FT.createdBy)='4'
                                            THEN (SELECT aboutCompany FROM fj_users WHERE id=(SELECT createdBy FROM fj_users WHERE id=FT.createdBy))
                                        END
                                    ) AS aboutCompany
                                FROM
                                    (
                                        SELECT * 
                                        FROM (
                                            SELECT 
                                                J.*,
                                                I.id            AS jobIndustryId, 
                                                I.name          AS jobIndustryName,
                                                GROUP_CONCAT(Q.qualificationId) AS qualificationId
                                            FROM 
                                                fj_jobs J 
                                            JOIN 
                                                nc_industry             I ON I.id = J.industryId 
                                            LEFT JOIN 
                                                fj_jobQualifications    Q ON Q.jobId = J.id 
                                            WHERE
                                                J.status = '1' AND 
                                                (DATE(J.openTill)>=DATE(NOW()) OR J.openTill = '0000-00-00 00:00:00') AND  
                                                J.id IN ($jobIds)  
                                            GROUP BY 
                                                J.id
                                        ) jobSearchTable 
                                        WHERE
                                            id IS NOT NULL
                                    ) FT
                                ORDER BY
                                    FT.id ASC";

                    $searchData = $this->coreModel->queryResultArray($query);
                    if(count($searchData)>0) {                        
                        foreach($searchData as $jobRow){
                            // Job Data
                            $rowData['jobCode']         = (string)$jobRow['fjCode'];
                            $rowData['title']           = (string)$jobRow['title'];
                            $rowData['description']     = (string)$jobRow['description'];
                            $rowData['vaccancies']      = (string)$jobRow['noOfVacancies'];
                            $rowData['minAge']          = (string)$jobRow['ageFrom'];
                            $rowData['maxAge']          = (string)$jobRow['ageTo'];
                            $rowData['minSalary']       = (string)$this->coreModel->customNumberFormat($jobRow['salaryFrom']);
                            $rowData['maxSalary']       = (string)$this->coreModel->customNumberFormat($jobRow['salaryTo']);
                            $rowData['noticePeriod']    = (string)$jobRow['noticePeriod'];                   
                            $rowData['qualification']   = $this->jobQualification($jobRow['id']);
                            $rowData['experienceFrom']  = (string)$jobRow['expFrom'];
                            $rowData['experienceTo']    = (string)$jobRow['expTo'];

                            if(($jobRow['openTill'] != '0000-00-00 00:00:00') && ($jobRow['openTill'] != '')) {
                                $rowData['openTill'] = (string)$jobRow['openTill'];
                            }
                            else {
                                $stop_date = date('Y-m-d H:i:s');
                                $rowData['openTill'] = date('Y-m-d H:i:s', strtotime($stop_date . ' +1 day'));
                            }

                            $rowData['industryId']      = (string)$jobRow['jobIndustryId'];
                            $rowData['industryName']    = (string)$jobRow['jobIndustryName'];
                            $rowData['companyName']     = (string)$jobRow['company'];
                            $rowData['aboutCompany']    = (string)$jobRow['aboutCompany'];
                            $rowData['companyLogo']     = ($jobRow['companyLogo']!=''?base_url() . '/uploads/corporateLogo/' . $jobRow['companyLogo']:'');
                            

                            if(($jobRow['openTill'] != '0000-00-00 00:00:00') && ($jobRow['openTill'] != '')) {
                                $rowData['daysLeft'] = (string)$this->jobDaysLeft($jobRow['openTill']);
                            }
                            else {
                                $rowData['daysLeft'] = 1;
                            }

                            $rowData['location']        = $this->jobLocations($jobRow['id']);
                            coreapi_model::$data[]      = $rowData;
                        }
                        coreapi_model::codeMessage('200', $this->lang->line('success'));
                        coreapi_model::$returnArray['data']             = coreapi_model::$data;
                    }
                    else {
                        $this->coreModel->codeMessage('500', $this->lang->line('expire_saved_job'));
                    }
                }
                else {
                    $this->coreModel->codeMessage('500', $this->lang->line('no_saved_job_found'));
                }
            }
            else {
                $this->coreModel->codeMessage('500', $this->lang->line('invalid_user'));
            }
        }
        else {
            $this->coreModel->codeMessage('300', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }
    //--------------- Send All Saved Job To User Account Ends -----------------//
    ############################################################################    
    //-------------------------------------------------------------------------//
    //----------------------------- Save Job ----------------------------------//
    //-------------------------------------------------------------------------//
    
    
     
    /*
     *  @Author : Wildnet
     *  @Params : array(user id)
     *  @Short Description : Send All User's Applied Job Details
     *  @return : array with code. 
     */
    //------------------ Send All User Applied Jobs Starts --------------------//
    public function getAppliedJob($params) {
        $userId         = $this->coreModel->cleanString($params['userId']);
        $id             = $this->coreModel->getJwtValue($userId);
        if($userId!='') {
            $userRow    = $this->coreModel->userExists($id);
            if(count($userRow)>0){
                $checkResult = $this->coreModel->queryRowArray("SELECT GROUP_CONCAT(jobId) AS id, GROUP_CONCAT(createdAt) AS appliedAt, GROUP_CONCAT(status) AS appliedStatus FROM fj_userJob WHERE userId='".$userRow['id']."'");                

                if($checkResult['id']!='') {
                    $jobIds = $checkResult['id'];
                    
                    $query = "  SELECT
                                    FT.*,
                                    (SELECT email FROM fj_users U WHERE U.id=FT.createdBy) AS userName,
                                    (SELECT role FROM fj_users U WHERE U.id=FT.createdBy)  AS userRole,
                                    (
                                        CASE
                                        WHEN (SELECT role FROM fj_users U WHERE U.id=FT.createdBy)='2'
                                            THEN (SELECT company FROM fj_users WHERE id=FT.createdBy)
                                        WHEN (SELECT role FROM fj_users U WHERE U.id=FT.createdBy)='4'
                                            THEN (SELECT company FROM fj_users WHERE id=(SELECT createdBy FROM fj_users WHERE id=FT.createdBy))
                                        END
                                    ) AS company,
                                    (
                                        CASE
                                        WHEN (SELECT role FROM fj_users U WHERE U.id=FT.createdBy)='2'
                                            THEN (SELECT companyLogo FROM fj_users WHERE id=FT.createdBy)
                                        WHEN (SELECT role FROM fj_users U WHERE U.id=FT.createdBy)='4'
                                            THEN (SELECT companyLogo FROM fj_users WHERE id=(SELECT createdBy FROM fj_users WHERE id=FT.createdBy))
                                        END
                                    ) AS companyLogo,
                                    (
                                        CASE
                                        WHEN (SELECT role FROM fj_users U WHERE U.id=FT.createdBy)='2'
                                            THEN (SELECT aboutCompany FROM fj_users WHERE id=FT.createdBy)
                                        WHEN (SELECT role FROM fj_users U WHERE U.id=FT.createdBy)='4'
                                            THEN (SELECT aboutCompany FROM fj_users WHERE id=(SELECT createdBy FROM fj_users WHERE id=FT.createdBy))
                                        END
                                    ) AS aboutCompany
                                FROM
                                    (
                                        SELECT * 
                                        FROM (
                                            SELECT 
                                                J.*,
                                                I.id            AS jobIndustryId, 
                                                I.name          AS jobIndustryName,
                                                GROUP_CONCAT(Q.qualificationId) AS qualificationId
                                            FROM 
                                                fj_jobs J 
                                            JOIN  
                                                nc_industry             I ON I.id = J.industryId  
                                            LEFT JOIN 
                                                fj_jobQualifications    Q ON Q.jobId = J.id  
                                            WHERE 
                                                J.status        IN ('1')    AND
                                                J.id            IN ($jobIds)    AND  
                                                (DATE(J.openTill)>=DATE(NOW()) OR J.openTill = '0000-00-00 00:00:00') 
                                            GROUP BY 
                                                J.id 
                                        ) jobSearchTable 
                                        WHERE
                                            id IS NOT NULL
                                    ) FT
                                ORDER BY
                                    FT.id ASC";

                    $searchData = $this->coreModel->queryResultArray($query);

                    if(count($searchData)>0) {                        
                        foreach($searchData as $jobRow){
                            $jobAppliedIDs      = explode(',', $checkResult['id']);
                            $jobAppliedDates    = explode(',', $checkResult['appliedAt']);
                            $jobAppliedStatus   = explode(',', $checkResult['appliedStatus']);
                            $jobPanelRow        = $this->coreModel->queryRowArray("SELECT level FROM fj_jobPanel WHERE jobId='".$jobRow['id']."'");
                            if(in_array($jobRow['id'], $jobAppliedIDs)){
                                $key                    = array_search ($jobRow['id'], $jobAppliedIDs);
                                $rowData['appliedAt']   = (string)$jobAppliedDates[$key];
                                $appliedStatus          = (string)$jobAppliedStatus[$key];
                                if($jobPanelRow['level']=='1') {
                                    $appliedStatusValue     = ($appliedStatus=='3' || $appliedStatus=='5'?'Rejected':($appliedStatus==1?'Pending':'Shortlisted'));
                                }
                                if($jobPanelRow['level']=='2') {
                                    $appliedStatusValue     = ($appliedStatus=='3' || $appliedStatus=='5'?'Rejected':($appliedStatus==1 || $appliedStatus==2?'Pending':'Shortlisted'));
                                }
                                $rowData['appliedStatus']   = (string)$appliedStatusValue;
                                $rowData['applieddaysAgo']  = (string)abs($this->jobDaysLeft($jobAppliedDates[$key]));
                            }                            
                            $rowData['jobCode']         = (string)$jobRow['fjCode'];
                            $rowData['title']           = (string)$jobRow['title'];                  
                            $rowData['qualification']   = $this->jobQualification($jobRow['id']);
                            $rowData['companyName']     = (string)$jobRow['company'];
                            $rowData['companyLogo']     = ($jobRow['companyLogo']!=''?base_url() . '/uploads/corporateLogo/' . $jobRow['companyLogo']:'');
                            $rowData['location']        = $this->jobLocations($jobRow['id']);
                            coreapi_model::$data[]      = $rowData;
                        }
                        $this->coreModel->codeMessage('200', $this->lang->line('success'));
                        coreapi_model::$returnArray['data'] = coreapi_model::$data;
                    }
                    else {
                        $this->coreModel->codeMessage('500', $this->lang->line('no_job_found'));
                    }
                }
                else {
                    $this->coreModel->codeMessage('500', $this->lang->line('no_job_found'));
                }
            }
            else {
                $this->coreModel->codeMessage('500', $this->lang->line('invalid_user'));
            }
        }
        else {
            $this->coreModel->codeMessage('300', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }

    /*
     *  @Author : Synergy
     *  @Params : userId
     *  @Short Description : Use to get job fair list
     *  @return : array with code. 
     */
    public function getJobFairList($params) {
        $userId = $this->coreModel->cleanString($params['userId']);
        $id = $this->coreModel->getJwtValue($userId);
        if ($userId != '') {
            $row = $this->coreModel->queryRowArray("SELECT id FROM fj_users WHERE id='$id' AND status='1'");
            if (count($row) > 0) {
                // Employment Information
                $jobFairList = array();
                $jobFairResults = $this->coreModel->queryResultArray("SELECT * FROM fj_events WHERE endDate >= NOW() AND eventTypeId = 1");

                foreach ($jobFairResults as $row) {
                    $rowFairData['id'] = (string) $row['id'];
                    $rowFairData['eventName'] = (string) $row['eventName'];
                    $rowFairData['description'] = (string) $row['description'];
                    $rowFairData['startDate'] = (string) date('d M Y', strtotime($row['startDate']));
                    $rowFairData['endDate'] = (string) date('d M Y', strtotime($row['endDate']));
                    $jobFairList[] = $rowFairData;
                }
                $this->coreModel->codeMessage('200', $this->lang->line('success'));
                coreapi_model::$returnArray['jobFairList'] = $jobFairList;
            } else {
                $this->coreModel->codeMessage('500', $this->lang->line('no_record_found'));
            }
        } else {
            $this->coreModel->codeMessage('300', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }

    /*
     *  @Author : Synergy
     *  @Params : userId
     *  @Short Description : Use to get job fair list
     *  @return : array with code. 
     */
    public function getFairJobsList($params) {
        $userId = $this->coreModel->cleanString($params['userId']);
        $eventId = $this->coreModel->cleanString($params['eventId']);
        $id = $this->coreModel->getJwtValue($userId);
        if ($userId != '') {
            $row = $this->coreModel->queryRowArray("SELECT id FROM fj_users WHERE id='$id' AND status='1'");
            if (count($row) > 0) {
                // Employment Information
                $jobFairList = array();

                $query  = " 
                        SELECT
                            RES.*
                        FROM
                            (
                            SELECT
                                FT.*,
                                (SELECT email FROM fj_users U WHERE U.id=FT.createdBy) AS userName,
                                (SELECT role FROM fj_users U WHERE U.id=FT.createdBy)  AS userRole,
                                (
                                    CASE
                                    WHEN (SELECT role FROM fj_users U WHERE U.id=FT.createdBy)='1'
                                        THEN (SELECT company FROM fj_users WHERE id=FT.createdBy)
                                    WHEN (SELECT role FROM fj_users U WHERE U.id=FT.createdBy)='2'
                                        THEN (SELECT company FROM fj_users WHERE id=FT.createdBy)
                                    WHEN (SELECT role FROM fj_users U WHERE U.id=FT.createdBy)='4'
                                        THEN (SELECT company FROM fj_users WHERE id=(SELECT createdBy FROM fj_users WHERE id=FT.createdBy))
                                    END
                                ) AS company,
                                (
                                    CASE
                                    WHEN (SELECT role FROM fj_users U WHERE U.id=FT.createdBy)='2'
                                        THEN (SELECT companyLogo FROM fj_users WHERE id=FT.createdBy)
                                    WHEN (SELECT role FROM fj_users U WHERE U.id=FT.createdBy)='4'
                                        THEN (SELECT companyLogo FROM fj_users WHERE id=(SELECT createdBy FROM fj_users WHERE id=FT.createdBy))
                                    END
                                ) AS companyLogo,
                                (
                                    CASE
                                    WHEN (SELECT role FROM fj_users U WHERE U.id=FT.createdBy)='2'
                                        THEN (SELECT aboutCompany FROM fj_users WHERE id=FT.createdBy)
                                    WHEN (SELECT role FROM fj_users U WHERE U.id=FT.createdBy)='4'
                                        THEN (SELECT aboutCompany FROM fj_users WHERE id=(SELECT createdBy FROM fj_users WHERE id=FT.createdBy))
                                    END
                                ) AS aboutCompany
                            FROM
                                (
                                    SELECT * 
                                    FROM (

                                        SELECT 
                                            J.*,
                                            I.id            AS jobIndustryId, 
                                            I.name          AS jobIndustryName,
                                            GROUP_CONCAT(Q.qualificationId) AS qualificationId
                                        FROM 
                                            fj_jobs J 
                                        JOIN 
                                            nc_industry             I ON I.id = J.industryId 
                                        JOIN
                                            fj_events               fe ON fe.id = J.eventId 
                                        LEFT JOIN 
                                            fj_jobQualifications    Q ON Q.jobId = J.id 
                                        WHERE
                                            J.status        = '1'           AND
                                            (DATE(J.openTill)>=DATE(NOW()) OR J.openTill = '0000-00-00 00:00:00') AND 
                                            fe.eventTypeId = 1              AND 
                                            fe.endDate >= NOW()             AND 
                                            fe.id = $eventId   
                                        GROUP BY 
                                            J.id
                                    ) jobSearchTable 
                                    WHERE
                                        id IS NOT NULL
                                ) FT
                            ) RES
                        ";
                $fairJobsResults = $this->coreModel->queryResultArray($query);

                foreach ($fairJobsResults as $jobRow) {
                    $rowData['jobCode']         = (string)$jobRow['fjCode'];
                    $rowData['title']           = (string)$jobRow['title'];
                    $rowData['description']     = (string)$jobRow['description'];
                    $rowData['vaccancies']      = (string)$jobRow['noOfVacancies'];
                    $rowData['minAge']          = (string)$jobRow['ageFrom'];
                    $rowData['maxAge']          = (string)$jobRow['ageTo'];
                    $rowData['minSalary']       = (string)$this->coreModel->customNumberFormat($jobRow['salaryFrom']);
                    $rowData['maxSalary']       = (string)$this->coreModel->customNumberFormat($jobRow['salaryTo']);
                    $rowData['noticePeriod']    = (string)$jobRow['noticePeriod'];                   
                    $rowData['qualification']   = $this->jobQualification($jobRow['id']);
                    $rowData['experienceFrom']  = (string)$jobRow['expFrom'];
                    $rowData['experienceTo']    = (string)$jobRow['expTo'];
                    $rowData['openTill']        = (string)$jobRow['openTill'];
                    $rowData['industryId']      = (string)$jobRow['jobIndustryId'];
                    $rowData['industryName']    = (string)$jobRow['jobIndustryName'];
                    $rowData['companyName']     = (string)$jobRow['company'];
                    $rowData['aboutCompany']    = (string)$jobRow['aboutCompany'];
                    $rowData['companyLogo']     = ($jobRow['companyLogo']!=''?base_url() . '/uploads/corporateLogo/' . $jobRow['companyLogo']:'');
                    $rowData['daysLeft']        = (string)$this->jobDaysLeft($jobRow['openTill']);
                    $rowData['location']        = $this->jobLocations($jobRow['id']);;
                    coreapi_model::$data[]      = $rowData;
                }
                $this->coreModel->codeMessage('200', $this->lang->line('success'));
                coreapi_model::$returnArray['data']             = coreapi_model::$data;
            } else {
                $this->coreModel->codeMessage('500', $this->lang->line('no_record_found'));
            }
        } else {
            $this->coreModel->codeMessage('300', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }

    /*
     *  @Author : Synergy
     *  @Params : userId
     *  @Short Description : Use to get job fair list
     *  @return : array with code. 
     */
    public function getLastAppReleaseInfo($params) {
        $appResult = $this->coreModel->queryRowArray("SELECT * FROM fj_app_releases ORDER BY releaseDate DESC LIMIT 0, 1");
        if (count($appResult) > 0) {
            $this->coreModel->codeMessage('200', $this->lang->line('success'));
            coreapi_model::$returnArray['data'] = $appResult;
        } else {
            $this->coreModel->codeMessage('500', $this->lang->line('no_record_found'));
        }
        return coreapi_model::$returnArray;
    }

    /*
     *  @Author : Synergy
     *  @Params : userId
     *  @Short Description : Use to get job fair list
     *  @return : array with code. 
     */
    public function getJobsFilterCriteria($params) {
        $allFilters = array();
        //Salary Filter
        $salaryCriteriaList = array();
        $salaryCriteriaArray = array('0to3' => '0-3 Lakhs', '3to6' => '3-6 Lakhs', '6to10' => '6-10 Lakhs', '10to15' => '10-15 Lakhs', '15to25' => '15-25 Lakhs', '25to50' => '25-50 Lakhs', '50to75' => '50-75 Lakhs', '75to100' => '75-100 Lakhs', '101' => '100+ Lakhs');
        foreach ($salaryCriteriaArray as $salkey => $salValue) {
            $salRow['id'] = (string)$salkey;
            $salRow['name'] = $salValue;
            $salaryCriteriaList[] = $salRow;
        }

        //Freshness Filter
        $freshnessCriteriaList = array();
        $freshnessCriteriaArray = array('1' => '1 Day Old', '3' => '< 3 Day Old', '7' => '< 7 Day Old', '15' => '< 15 Day Old', '30' => '< 30 Day Old');
        foreach ($freshnessCriteriaArray as $freshkey => $freshValue) {
            $freshRow['id'] = (string)$freshkey;
            $freshRow['name'] = $freshValue;
            $freshnessCriteriaList[] = $freshRow;
        }

        //Location Filter
        $searchLocations = $this->coreModel->queryResultArray(" SELECT city FROM `fj_cityState` 
                                                            UNION
                                                            SELECT state FROM `fj_cityState`");
        $allLocations = array();
        foreach ($searchLocations as $location) {
            $allLocations[] = $location['city'];
        }

        //Get Courses
        $result = $this->coreModel->queryResultArray("SELECT DISTINCT(name), id FROM fj_courses ORDER BY name ASC");
        $allEducation = array();
        foreach ($result as $row) {
            $allEducationRow['id'] = (string) $row['id'];
            $allEducationRow['name'] = (string) $row['name'];
            $allEducation[] = $allEducationRow;
        }
            

        $allFilters['salaryFilter'] = $salaryCriteriaList;
        $allFilters['freshnessFilter'] = $freshnessCriteriaList;
        $allFilters['locationFilter'] = $allLocations;
        $allFilters['educationFilter'] = $allEducation;
        $this->coreModel->codeMessage('200', $this->lang->line('success'));
        coreapi_model::$data[]      = $allFilters;
        coreapi_model::$returnArray['data'] = coreapi_model::$data;
        return coreapi_model::$returnArray;
    }

    /*
     *  @Author : Synergy
     *  @Params : array(user id)
     *  @Short Description : Send All User's Applied Job Details
     *  @return : array with code. 
     */
    public function getRecentJobSearchList($params) {
        $userId         = $this->coreModel->cleanString($params['userId']);
        $id             = $this->coreModel->getJwtValue($userId);

        if($userId!='') {
            $userRow    = $this->coreModel->userExists($id);
            if(count($userRow)>0){
                $recentSearchJobs = $this->coreModel->queryResultArray("SELECT * FROM fj_searchJobsLog WHERE userId = '$id' AND status=1 ORDER BY createdAt DESC LIMIT 0, 5");
                //print_r($recentSearchJobs);exit;
                if(count($recentSearchJobs)>0) {                       
                    $this->coreModel->codeMessage('200', $this->lang->line('success'));
                    coreapi_model::$returnArray['data'] = $recentSearchJobs;
                }
                else {
                    $this->coreModel->codeMessage('200', $this->lang->line('success'));
                    coreapi_model::$returnArray['data'] = array();
                }
            }
            else {
                $this->coreModel->codeMessage('500', $this->lang->line('invalid_user'));
            }
        }
        else {
            $this->coreModel->codeMessage('300', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }

    /*
     *  @Author : Synergy
     *  @Params : array($params)
     *  @Short Description : search jobs version 2 API
     *  @return : array with code. 
     */
    public function searchJobs_v2($params) {
        //error_reporting(E_ALL);
        //ini_set('display_errors', 1);
        $searchKeywordArray = array();

        $keyword        = $this->coreModel->cleanString($params['searchKeyword']);
        $locationName   = $this->coreModel->cleanString($params['locationName']);
        $yearsFrom      = $this->coreModel->cleanString($params['yearsFrom']);
        $salaryFrom     = $this->coreModel->cleanString($params['expectedSalary']);
        $qualification  = $this->coreModel->cleanString($params['qualification']);
        $salaryRangeFilter  = $this->coreModel->cleanString($params['salaryRangeFilter']);
        $freshnessFilter  = $this->coreModel->cleanString($params['freshnessFilter']);
        $userId = $this->coreModel->cleanString($params['userId']);

        if($userId) {
            $id = $this->coreModel->getJwtValue($userId);
            $searchKeywordArray['userId'] = $id;
        }


        
        $startPoint     = ($this->coreModel->cleanString($params['startPoint'])!=''?$this->coreModel->cleanString($params['startPoint']):'0');
        $limitJobs      = ($this->coreModel->cleanString($params['jobsLimit'])!=''?$this->coreModel->cleanString($params['jobsLimit']):'5000');
        
        // All Location IDs This Search Criteria
        $concatQuery    = $this->db->query(" SET SESSION group_concat_max_len = 1000000 ");        
        $locationIDs    = '';
        $jobIDs         = 0;

        if($locationName) {
            $searchKeywordArray['locations'] = $locationName;
            $locations = explode(',',$this->coreModel->cleanString($locationName));
        } else {
            $locations = array();
        }

        if(count($locations)>0){
            foreach($locations as $locationValue){
                $locationData   = $this->allLocationIdQuery($locationValue);
                $locationIDs    = ($locationIDs==''?$locationData['locationID']:$locationIDs.','.$locationData['locationID']);
            }

            $locationIDs = rtrim($locationIDs, ',');
            $jobData   = $this->allJobIdQuery($locationIDs);
            $jobIDs    = $jobData['jobID'];
        }

        if($jobIDs > 0) {
            $inWhere = " AND J.id IN (".$jobIDs.")";
        } else {
            $inWhere = " AND 1=1";
        }

        $query  = " 
                SELECT
                    RES.*
                FROM
                    (
                    SELECT
                        FT.*,
                        (SELECT email FROM fj_users U WHERE U.id=FT.createdBy) AS userName,
                        (SELECT role FROM fj_users U WHERE U.id=FT.createdBy)  AS userRole,
                        (
                            CASE
                            WHEN (SELECT role FROM fj_users U WHERE U.id=FT.createdBy)='1'
                                THEN (SELECT company FROM fj_users WHERE id=FT.createdBy)
                            WHEN (SELECT role FROM fj_users U WHERE U.id=FT.createdBy)='2'
                                THEN (SELECT company FROM fj_users WHERE id=FT.createdBy)
                            WHEN (SELECT role FROM fj_users U WHERE U.id=FT.createdBy)='4'
                                THEN (SELECT company FROM fj_users WHERE id=(SELECT createdBy FROM fj_users WHERE id=FT.createdBy))
                            END
                        ) AS company,
                        (
                            CASE
                            WHEN (SELECT role FROM fj_users U WHERE U.id=FT.createdBy)='2'
                                THEN (SELECT companyLogo FROM fj_users WHERE id=FT.createdBy)
                            WHEN (SELECT role FROM fj_users U WHERE U.id=FT.createdBy)='4'
                                THEN (SELECT companyLogo FROM fj_users WHERE id=(SELECT createdBy FROM fj_users WHERE id=FT.createdBy))
                            END
                        ) AS companyLogo,
                        (
                            CASE
                            WHEN (SELECT role FROM fj_users U WHERE U.id=FT.createdBy)='2'
                                THEN (SELECT aboutCompany FROM fj_users WHERE id=FT.createdBy)
                            WHEN (SELECT role FROM fj_users U WHERE U.id=FT.createdBy)='4'
                                THEN (SELECT aboutCompany FROM fj_users WHERE id=(SELECT createdBy FROM fj_users WHERE id=FT.createdBy))
                            END
                        ) AS aboutCompany
                    FROM
                        (
                            SELECT * 
                            FROM (

                                SELECT 
                                    J.*,
                                    datediff(CURRENT_DATE(), J.createdAt) noofdays, 
                                    I.id            AS jobIndustryId, 
                                    I.name          AS jobIndustryName,
                                    GROUP_CONCAT(Q.qualificationId) AS qualificationId
                                FROM 
                                    fj_jobs J 
                                JOIN 
                                    nc_industry             I ON I.id = J.industryId 
                                LEFT JOIN 
                                    fj_jobQualifications    Q ON Q.jobId = J.id 
                                WHERE
                                    J.status        = '1'           AND
                                    (DATE(J.openTill)>=DATE(NOW()) OR J.openTill = '0000-00-00 00:00:00') AND 
                                    J.posted        = '1'           $inWhere
                                GROUP BY 
                                    J.id

                            ) jobSearchTable 
                            WHERE
                                id IS NOT NULL
                        ) FT
                    ) RES
                WHERE
                ";

        $query .= (" 1=1 ");

        if($keyword) {
            $query .= ($keyword!=''?" AND ( title LIKE '%".$keyword."%' ":"");
            $query .= ($keyword!=''?" OR description LIKE '%".$keyword."%' ":"");
            $query .= ($keyword!=''?" OR company LIKE '%$keyword%' )":"");
            $searchKeywordArray['searchKeyword'] = $keyword;
        }

        if($salaryFrom) {
            $query .= " AND ($salaryFrom BETWEEN salaryFrom AND salaryTo )";
        }

        if($salaryRangeFilter) {
            $salaryRangeArray = explode(',', $salaryRangeFilter);

            $salArray = array();
            foreach($salaryRangeArray as $salData) {
                if($salData == '0to3') {
                    $salArray[] = 0;
                    $salArray[] = 300000;
                } elseif ($salData == '3to6') {
                    $salArray[] = 300000;
                    $salArray[] = 600000;
                } elseif ($salData == '6to10') {
                    $salArray[] = 600000;
                    $salArray[] = 1000000;
                } elseif ($salData == '10to15') {
                    $salArray[] = 1000000;
                    $salArray[] = 1500000;
                } elseif ($salData == '15to25') {
                    $salArray[] = 1500000;
                    $salArray[] = 2500000;
                } elseif ($salData == '25to50') {
                    $salArray[] = 2500000;
                    $salArray[] = 5000000;
                } elseif ($salData == '50to75') {
                    $salArray[] = 5000000;
                    $salArray[] = 7500000;
                } elseif ($salData == '75to100') {
                    $salArray[] = 10000000;
                    $salArray[] = 1000000000;
                }
            }

            $minSalary = min($salArray);
            $maxSalary = max($salArray);

            $query .= (" AND ( ( (salaryFrom BETWEEN $minSalary AND $maxSalary) OR (salaryTo BETWEEN $minSalary AND $maxSalary)) )  ");

            $searchKeywordArray['salaryRanges'] = $salaryRangeFilter;
        }

        if($yearsFrom) {
            $query .= " AND ($yearsFrom BETWEEN expFrom AND expTo) ";
            $searchKeywordArray['experience'] = $yearsFrom;
        }

        if($qualification!='') {
            $query .=   " AND
                        (   qualificationId LIKE'%,$qualification,%'    OR
                            qualificationId LIKE'$qualification,%'  OR
                            qualificationId LIKE'%,$qualification'  OR
                            qualificationId LIKE'$qualification'
                        )";
        }
        
        if($freshnessFilter) {
            $freshnessRangeArray = explode(',', $freshnessFilter);
            $maxDay = max($freshnessRangeArray);
            $minDay = 0;
            $query .= " HAVING (noofdays BETWEEN $minDay AND $maxDay) ";
        }

        $query .= " ORDER BY RES.id ";
        $countQuery = $query;
    
        $searchData         = $this->coreModel->queryResultArray($query);
 
        $logInserted = $this->db->select('*')->from('fj_searchJobsLog')->where($searchKeywordArray)->get()->num_rows();
        //echo $this->db->last_query();exit;
        if($logInserted <= 0) {
            $this->db->insert('fj_searchJobsLog', $searchKeywordArray);
        }

        if(count($searchData)>0){
            $lastRowId = count($searchData);
            $countQuery .= " LIMIT $startPoint, $limitJobs ";
            $countSearchData         = $this->coreModel->queryResultArray($countQuery);
            //print_r($countSearchData);exit;
            foreach($countSearchData as $jobRow){
                // Job Data
                $rowData['jobCode']         = (string)$jobRow['fjCode'];
                $rowData['title']           = (string)$jobRow['title'];
                $rowData['description']     = (string)$jobRow['description'];
                $rowData['vaccancies']      = (string)$jobRow['noOfVacancies'];
                $rowData['minAge']          = (string)$jobRow['ageFrom'];
                $rowData['maxAge']          = (string)$jobRow['ageTo'];
                $rowData['minSalary']       = (string)$this->coreModel->customNumberFormat($jobRow['salaryFrom']);
                $rowData['maxSalary']       = (string)$this->coreModel->customNumberFormat($jobRow['salaryTo']);
                $rowData['minimumSalary']   = (string)$jobRow['salaryFrom'];
                $rowData['maximumSalary']   = (string)$jobRow['salaryTo'];
                $rowData['noticePeriod']    = (string)$jobRow['noticePeriod'];                   
                $rowData['qualification']   = $this->jobQualification($jobRow['id']);
                $rowData['experienceFrom']  = (string)$jobRow['expFrom'];
                $rowData['experienceTo']    = (string)$jobRow['expTo'];
                
                if(($jobRow['openTill'] != '0000-00-00 00:00:00') && ($jobRow['openTill'] != '')) {
                    $rowData['openTill'] = (string)$jobRow['openTill'];
                }
                else {
                    $stop_date = date('Y-m-d H:i:s');
                    $rowData['openTill'] = date('Y-m-d H:i:s', strtotime($stop_date . ' +1 day'));
                }
                            
                $rowData['industryId']      = (string)$jobRow['jobIndustryId'];
                $rowData['industryName']    = (string)$jobRow['jobIndustryName'];
                $rowData['companyName']     = (string)$jobRow['company'];
                $rowData['aboutCompany']    = (string)$jobRow['aboutCompany'];
                $rowData['createdAt']     = $this->jobGetAgo($jobRow['createdAt']);
                $rowData['companyLogo']     = ($jobRow['companyLogo']!=''?base_url() . '/uploads/corporateLogo/' . $jobRow['companyLogo']:'');


                if(($jobRow['openTill'] != '0000-00-00 00:00:00') && ($jobRow['openTill'] != ''))
                    $rowData['daysLeft'] = (string)$this->jobDaysLeft($jobRow['openTill']);
                else
                    $rowData['daysLeft'] = 1;

                $rowData['location']        = $this->jobLocations($jobRow['id']);;
                coreapi_model::$data[]      = $rowData;
            }

            $this->coreModel->codeMessage('200', $this->lang->line('success'));
            coreapi_model::$returnArray['data']             = coreapi_model::$data;
            unset($params['token']);
            unset($params['methodname']);                
            coreapi_model::$returnArray['endPoint']         = (int)$lastRowId;
            coreapi_model::$returnArray['searchCriteria']   = $params;
        }
        else {
            $this->coreModel->codeMessage('300', $this->lang->line('no_job_found'));
        } 
        return coreapi_model::$returnArray;
    }

    function jobGetAgo($datetime, $full = false) {
        $now = new DateTime;
        $ago = new DateTime($datetime);


        $year1 = date('Y', strtotime($datetime));
        $year2 = date('Y', time());

        $month1 = date('m', strtotime($datetime));
        $month2 = date('m', time());

        $totalMonth = (($year2 - $year1) * 12) + ($month2 - $month1);

        if($totalMonth > 2) {
            return date('d M Y', strtotime($datetime));
        } else {

            $diff = $now->diff($ago);

            $diff->w = floor($diff->d / 7);
            $diff->d -= $diff->w * 7;

            $string = array(
                'y' => 'year',
                'm' => 'month',
                'w' => 'week',
                'd' => 'day',
                'h' => 'hour',
                'i' => 'minute',
                's' => 'second',
            );
            foreach ($string as $k => &$v) {
                if ($diff->$k) {
                    $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
                } else {
                    unset($string[$k]);
                }
            }

            if (!$full) $string = array_slice($string, 0, 1);
            return $string ? implode(', ', $string) . ' ago' : 'just now';
        }
    }
}
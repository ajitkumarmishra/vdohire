<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Practiseapi Model
 * Description : Handle all the CRUD operation for Practiseapi
 * @author Synergy
 * @createddate : Dec 1, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 17, 2017
 */
class practiseapi_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->model('Coreapi_model', 'coreModel');
    }
    
    /**
     * Description : Use to Get All Industries
     * Author : Synergy
     * @param array $params
     * @return array of data 
     */
    function getIndustryList($params){
        $industryResult     = $this->coreModel->queryResultArray("SELECT id, name FROM nc_industry WHERE indus_id='0' AND is_active='1' ORDER BY name ASC ");
        if(count($industryResult)>0){
            foreach($industryResult as $industryRow) {
                // Forum Details
                $rowData['id']          = (string)$industryRow['id'];
                $rowData['industry']    = (string)ucwords($industryRow['name']);
                coreapi_model::$data[]  = $rowData;
            }
            $this->coreModel->codeMessage('200', $this->lang->line('success'));
            coreapi_model::$returnArray['data']    = coreapi_model::$data;        
        }
        else {
            $this->coreModel->codeMessage('500', $this->lang->line('no_industry_found'));
        }
        return coreapi_model::$returnArray;
    }
    
    /**
     * Description : Use to Get All the functions of a particular industry
     * Author : Synergy
     * @param array $params(industryId)
     * @return array of data 
     */
    function getFunctionList($params){
        $industryId     = $this->coreModel->cleanString($params['industryId']);
        $functionResult = $this->coreModel->queryResultArray("SELECT id, name FROM nc_industry WHERE indus_id='$industryId' AND is_active='1' ORDER BY name ASC ");
        if(count($functionResult)>0){
            foreach($functionResult as $functionRow) {
                // Forum Details
                $rowData['id']          = (string)$functionRow['id'];
                $rowData['function']    = (string)ucwords($functionRow['name']);
                coreapi_model::$data[]  = $rowData;
            }
            $this->coreModel->codeMessage('200', $this->lang->line('success'));
            coreapi_model::$returnArray['data']    = coreapi_model::$data;        
        }
        else {
            $this->coreModel->codeMessage('500', $this->lang->line('no_function_found'));
        }
        return coreapi_model::$returnArray;
    }

    /**
     * Description : Use to Get All the practice sets of a spcific function and industry
     * Author : Synergy
     * @param array $params(industryId, functionId)
     * @return array of data 
     */
    function getPractiseSetFunctions($params){
        $industryId         = $this->coreModel->cleanString($params['industryId']);
        $functionId         = $this->coreModel->cleanString($params['functionId']);        
        $query      = "SELECT DISTINCT(F.name), I.functionId, IND.id AS indusId, IND.name As industryName
                        FROM fj_interviews I
                        JOIN nc_industry IND, nc_industry F
                        WHERE
                            I.industryId=IND.id AND 
                            I.functionId=F.id   AND
                            I.status='1'        AND
                            I.industryId = '$industryId'";
        if($functionId!='') {
            $query     .= " AND I.functionId = '$functionId' ";
        }
        
        $practiseSetResult  = $this->coreModel->queryResultArray($query);
        if(count($practiseSetResult)>0){
            foreach($practiseSetResult as $practiseSetRow) {
                // Function Name
                $rowData['id']          = (string)$practiseSetRow['functionId'];
                $rowData['name']        = (string)ucwords($practiseSetRow['name']);
                coreapi_model::$data[]  = $rowData;
            }
            $this->coreModel->codeMessage('200', $this->lang->line('success'));
            coreapi_model::$returnArray['data']    = coreapi_model::$data;              
        }
        else {
            $this->coreModel->codeMessage('500', $this->lang->line('no_practise_set_found'));
        }
        return coreapi_model::$returnArray;
    }

    /**
     * Description : Use to Get All the practice sets with its details of a spcific function and industry
     * Author : Synergy
     * @param array $params(industryId, functionId)
     * @return array of data 
     */
    function getPractiseSets($params){
        $industryId         = $this->coreModel->cleanString($params['industryId']);
        $functionId         = $this->coreModel->cleanString($params['functionId']);        
        $query      = 
                    "   SELECT I.name, I.id, I.practiceInterviewType, IND.id AS indusId, IND.name As industryName, I.functionId, F.name AS functionName
                        FROM fj_interviews I
                        JOIN nc_industry IND, nc_industry F
                        WHERE
                            I.industryId = IND.id           AND
                            I.functionId = F.id             AND
                            I.status='1'                    AND
                            I.industryId = '$industryId'    AND 
                            I.functionId = '$functionId'
                    ";
        $practiseSetResult  = $this->coreModel->queryResultArray($query);
        if(count($practiseSetResult)>0){
            foreach($practiseSetResult as $practiseSetRow) {
                // Function Name
                $rowData['setId']   = (string)$practiseSetRow['id'];
                $rowData['setName'] = (string)ucwords($practiseSetRow['name']);
                $rowData['video']   = 'No';
                $rowData['audio']   = 'No';
                $rowData['text']    = 'No';
                $practiseType       = explode(',',$practiseSetRow['practiceInterviewType']);
                foreach($practiseType as $practiseRow){
                    if($practiseRow=='1'){
                        $rowData['video']   = 'Yes';
                    }
                    else if($practiseRow=='2'){
                        $rowData['audio']   = 'Yes';
                    }
                    else if($practiseRow=='3'){
                        $rowData['text']    = 'Yes';
                    }
                    else {
                        
                    }
                }
                coreapi_model::$data[]  = $rowData;
            }
            $this->coreModel->codeMessage('200', $this->lang->line('success'));
            coreapi_model::$returnArray['data']    = coreapi_model::$data;              
        }
        else {
            $this->coreModel->codeMessage('500', $this->lang->line('no_practise_set_found'));
        }
        return coreapi_model::$returnArray;
    }

    /**
     * Description : Use to download all the questions of particular practice set and particular set type. It will create a zip file and move on to server
     * Author : Synergy
     * @param array $params(setId, setType)
     * @return array of data 
     */
    function downloadPractiseSet($params){
        $practiseSetId      = $this->coreModel->cleanString($params['setId']);
        $practiseSetType    = $this->coreModel->cleanString($params['setType']);
        if($practiseSetId!='' && $practiseSetType!='') {
            $setType            = (string)(($this->coreModel->cleanString($params['setType'])=='video' || $this->coreModel->cleanString($params['setType'])=='audio')?($this->coreModel->cleanString($params['setType'])=='video'?'1':'2'):'3');
            $practiseSetResult  = $this->coreModel->queryResultArray("
                                                                SELECT 
                                                                    I.*,
                                                                    Q.duration,
                                                                    Q.title,                                                                    
                                                                    Q.file,
                                                                    IQ.practiceInterviewType AS questionType
                                                                FROM 
                                                                    fj_interviews   I
                                                                JOIN
                                                                    fj_interviewQuestions   IQ,
                                                                    fj_question             Q
                                                                WHERE
                                                                    I.id=IQ.interviewId     AND
                                                                    IQ.questionId=Q.id      AND
                                                                    I.status='1'            AND
                                                                    IQ.status='1'           AND
                                                                    Q.status='1'            AND
                                                                    I.environment='2'       AND
                                                                    IQ.interviewId='$practiseSetId'   AND
                                                                    IQ.practiceInterviewType='$setType'
                                                                ORDER BY I.id
                                                                ");
            if(count($practiseSetResult)>0) {                
                $tmp_file   = rand(1111,9999).time().rand(1111,9999).".zip";
                $zip        = new ZipArchive();
                $zip->open($tmp_file, ZipArchive::CREATE);
                $i=0;
                foreach($practiseSetResult as $practiseSetRow) {
                    $i++;
                    // Forum Details
                    $rowData['setName']         = (string)$practiseSetRow['name'];       
                    $rowData['type']            = (string)(($practiseSetRow['questionType']=='1' || $practiseSetRow['questionType']=='2')?($practiseSetRow['questionType']=='1'?'video':'audio'):'text');
                    $rowData['questionid']      = (int)$practiseSetRow['id']+rand(111111,999999);            
                    $rowData['questionText']    = (string)ucwords($practiseSetRow['title']);
                    $rowData['questionFile']    = (string)($practiseSetRow['file']!='' && $practiseSetRow['file']!='0'?$practiseSetRow['file']:$i);               
                    $rowData['questionDuration']= (string)ucwords($practiseSetRow['duration']); 
                    $rowData['videoType']       = (string)($practiseSetRow['questionType']=='1'?'video':'');
                    $rowData['audioType']       = (string)($practiseSetRow['questionType']=='2'?'audio':'');
                    $rowData['textType']        = (string)($practiseSetRow['questionType']=='3'?'text':'');
                    $rowData['zipFileName']     = (string)$tmp_file; 
                    coreapi_model::$data[]  = $rowData;
                    
                    if($practiseSetRow['file']!='' && $setType=='1') {
                        $download_file = file_get_contents(base_url() . '/uploads/questions/video/' . $practiseSetRow['file']);
                        $zip->addFromString($practiseSetRow['file'],$download_file);
                    }
                    else if($practiseSetRow['file']!='' && $setType=='2') {
                        $download_file = file_get_contents(base_url() . '/uploads/questions/audio/' . $practiseSetRow['file']);
                        $zip->addFromString($practiseSetRow['file'],$download_file);
                    }
                    else { }
                }
                $zip->close();                
                $this->coreModel->codeMessage('200', $this->lang->line('success'));
                coreapi_model::$returnArray['data']         = coreapi_model::$data;
                coreapi_model::$returnArray['downloadURL']  = base_url().'/'.$tmp_file;
            }
            else {
                $this->coreModel->codeMessage('500', $this->lang->line('no_practise_set_found'));
            }
        }
        else {
            $this->coreModel->codeMessage('500', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }
    
    /**
     * Description : It will just unlik the zip file
     * Author : Synergy
     * @param array $params(filename)
     * @return array of data 
     */
    function downloadReport($params){
        $fileName   = $this->coreModel->cleanString($params['filename']);        
        if($fileName!='') {
            if(file_exists($fileName)) {
                $filePath = base_url().'/'.$fileName;
                if(pathinfo($filePath, PATHINFO_EXTENSION)==='zip') {
                    unlink($fileName);
                    $this->coreModel->codeMessage('200', $this->lang->line('success'));
                }
                else {
                    $this->coreModel->codeMessage('500', $this->lang->line('invalid_attempt'));
                }
            }
            else {
                $this->coreModel->codeMessage('500', $this->lang->line('invalid_attempt'));
            }
        }        
        else {
            $this->coreModel->codeMessage('500', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;        
    }

    /**
     * Description : Use to get all the practice sets of a particular function and particular industry
     * Author : Synergy
     * @param array $params(industryId, functionId)
     * @return array of data 
     */
    function getPractiseSet($params){
        $industryId = $this->coreModel->cleanString($params['industryId']);
        $functionId = $this->coreModel->cleanString($params['functionId']);        
        $query      = "   SELECT DISTINCT(F.name) AS functionName, I.functionId
                        FROM fj_interviews I
                        JOIN nc_industry IND, nc_industry F
                        WHERE
                            I.industryId=IND.id AND 
                            I.functionId=F.id   AND
                            I.status='1'        AND
                            I.industryId = '$industryId'";
                    
        if($functionId!='') {
            $query     .= " AND I.functionId = '$functionId' ";
        }
        $functionResult     = $this->coreModel->queryResultArray($query);
        $functionListData   = array();
        if(count($functionResult)>0){
            foreach($functionResult as $functionRow) {
                $functionList = array();
                // function name
                $functionList['functionName'] = $functionRow['functionName'];
                $query2      = 
                            "   SELECT F.name AS functionName, I.functionId, IND.id AS indusId, IND.name As industryName, I.practiceInterviewType, I.id, I.name AS setName
                                FROM fj_interviews I
                                JOIN nc_industry IND, nc_industry F
                                WHERE
                                    I.industryId=IND.id             AND
                                    I.functionId=F.id               AND
                                    I.status='1'                    AND
                                    I.industryId = '$industryId'    AND
                                    I.functionId = '$functionRow[functionId]'";
                $functionSetResult  = $this->coreModel->queryResultArray($query2);
                if(count($functionSetResult)>0){
                    $functionList['setInfo']    = array();
                    $setList                    = array();
                    foreach($functionSetResult as $functionSetRow) {
                        // set name corresponding to function name
                        $setListData['setId']        = $functionSetRow['id'];
                        $setListData['setName']      = $functionSetRow['setName'];
                        $setListData['video']        = 'No';
                        $setListData['audio']        = 'No';
                        $setListData['text']         = 'No';
                        $practiseType = explode(',',$functionSetRow['practiceInterviewType']);
                        foreach($practiseType as $practiseTypeRow){
                            if($practiseTypeRow==1){
                                $setListData['video']='Yes';
                            }
                            else if($practiseTypeRow==2){
                                $setListData['audio']='Yes';
                            }
                            else if($practiseTypeRow==3){
                                $setListData['text']='Yes';
                            }
                            else{ }
                        }
                        $setList[] = $setListData;
                    } 
                    $functionList['setInfo'] = $setList;
                }
                $functionListData[] = $functionList;
            }
            coreapi_model::$data[]  = $functionListData;
            $this->coreModel->codeMessage('200', $this->lang->line('success'));
            coreapi_model::$returnArray['data']    = $functionListData;              
        }
        else {
            $this->coreModel->codeMessage('500', $this->lang->line('no_practise_set_found'));
        }
        return coreapi_model::$returnArray;
    }

    /**
     * Description : Use to get all the industries who have practice sets
     * Author : Synergy
     * @param array $params
     * @return array of data 
     */
    function getPractiseIndustryList($params){
        $industryResult     = $this->coreModel->queryResultArray("SELECT industryId, industryName FROM fj_industry WHERE industryType='1' AND isActive='1' ORDER BY industryName ASC ");
        if(count($industryResult)>0){
            foreach($industryResult as $industryRow) {
                // Forum Details
                $rowData['id']          = (string)$industryRow['industryId'];
                $rowData['industry']    = (string)ucwords($industryRow['industryName']);
                coreapi_model::$data[]  = $rowData;
            }
            $this->coreModel->codeMessage('200', $this->lang->line('success'));
            coreapi_model::$returnArray['data']    = coreapi_model::$data;        
        }
        else {
            $this->coreModel->codeMessage('500', $this->lang->line('no_industry_found'));
        }
        return coreapi_model::$returnArray;
    }
    
    /**
     * Description : Use to get all the industries functions
     * Author : Synergy
     * @param array $params
     * @return array of data 
     */
    function getPractiseFunctionList($params){
        $functionResult = $this->coreModel->queryResultArray("SELECT industryId, industryName FROM fj_industry WHERE industryType='2' AND isActive='1' ORDER BY industryName ASC ");
        if(count($functionResult)>0){
            foreach($functionResult as $functionRow) {
                // Forum Details
                $rowData['id']          = (string)$functionRow['industryId'];
                $rowData['function']    = (string)ucwords($functionRow['industryName']);
                coreapi_model::$data[]  = $rowData;
            }
            $this->coreModel->codeMessage('200', $this->lang->line('success'));
            coreapi_model::$returnArray['data']    = coreapi_model::$data;        
        }
        else {
            $this->coreModel->codeMessage('500', $this->lang->line('no_function_found'));
        }
        return coreapi_model::$returnArray;
    }
    
    /**
     * Description : Use to get all the practice sets of particular industry and particular function
     * Author : Synergy
     * @param array $params(industryId, functionId)
     * @return array of data 
     */
    function getPractiseSetList($params){
        $industryId = $this->coreModel->cleanString($params['industryId']);
        $functionId = $this->coreModel->cleanString($params['functionId']);
        if($industryId!='0' || $industryId!='') {
            $selectFields   =  "DISTINCT(IND.industryName) AS functionName,
                                I.industryId AS functionId ";
        }
        else {
            $selectFields   =  "DISTINCT(IND.industryName) AS functionName,
                                I.functionId ";            
        }
        $query      = 
                    "   SELECT 
                            ".$selectFields."
                        FROM fj_interviews I
                        JOIN fj_industry IND
                        WHERE 
                            (CASE WHEN I.industryId<>0 THEN I.industryId=IND.industryId ELSE 1=1 END) AND
                            (CASE WHEN I.functionId<>0 THEN I.functionId=IND.industryId ELSE 1=1 END) AND
                            I.status='1'                    AND
                            I.industryId = '$industryId'    AND
                            I.functionId = '$functionId' ";
        $functionResult     = $this->coreModel->queryResultArray($query);
        $functionListData   = array();
        if(count($functionResult)>0){
            foreach($functionResult as $functionRow) {
                $functionList = array();
                // function name
                $functionList['functionName'] = $functionRow['functionName'];
                $query2      = 
                            "   SELECT 
                                    I.id, 
                                    I.name, 
                                    I.practiceInterviewType, 
                                    I.industryId, 
                                    (CASE WHEN I.industryId<>0 THEN IND.industryName ELSE '' END) AS industryName,
                                    I.functionId, 
                                    (CASE WHEN I.functionId<>0 THEN IND.industryName ELSE '' END) AS functionName,
                                    I.environment
                                FROM fj_interviews I
                                JOIN fj_industry IND
                                WHERE 
                                    (CASE WHEN I.industryId<>0 THEN I.industryId=IND.industryId ELSE 1=1 END) AND
                                    (CASE WHEN I.functionId<>0 THEN I.functionId=IND.industryId ELSE 1=1 END) AND
                                    I.industryId = '$industryId' AND
                                    I.functionId = '$functionId' AND
                                    I.status='1'                
                        ";
                $functionSetResult  = $this->coreModel->queryResultArray($query2);
                if(count($functionSetResult)>0){
                    $functionList['setInfo']    = array();
                    $setList                    = array();
                    foreach($functionSetResult as $functionSetRow) {
                        // set name corresponding to function name
                        $setListData['setId']        = $functionSetRow['id'];
                        $setListData['setName']      = $functionSetRow['name'];
                        $setListData['video']        = 'No';
                        $setListData['audio']        = 'No';
                        $setListData['text']         = 'No';
                        $practiseType = explode(',',$functionSetRow['practiceInterviewType']);
                        foreach($practiseType as $practiseTypeRow){
                            if($practiseTypeRow==1){
                                $setListData['video']='Yes';
                            }
                            else if($practiseTypeRow==2){
                                $setListData['audio']='Yes';
                            }
                            else if($practiseTypeRow==3){
                                $setListData['text']='Yes';
                            }
                            else{ }
                        }
                        $setList[] = $setListData;
                    } 
                    $functionList['setInfo'] = $setList;
                }
                $functionListData[] = $functionList;
            }
            coreapi_model::$data[]  = $functionListData;
            $this->coreModel->codeMessage('200', $this->lang->line('success'));
            coreapi_model::$returnArray['data']    = $functionListData;              
        }
        else {
            $this->coreModel->codeMessage('500', $this->lang->line('no_practise_set_found'));
        }
        return coreapi_model::$returnArray;
    }
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Analytics Controller
 * Description : Used to handle all analytics related data
 * @author Synergy
 * @createddate : Nov 2, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 15, 2017
 */
class Cms extends MY_Controller {

    /**
     * Responsable for auto load the cms_model
     * Responsable for auto load the session and paginatin library
     * Responsable for auto load fj helper
     * @return void
     */
    function __construct() {
        parent::__construct();
        $this->load->model('cms_model');
        $this->load->library("session");
        $this->load->library('pagination');
    }

    /**
     * Description : This is for cms index page
     * Author : Synergy
     * @param array $_POST
     * @return void
     */
    function index() {
        check_auth();
        if ($this->input->post() != null) {
            $this->cms_model->updateStatus();
        }
        $config = array();
        $config["base_url"] = base_url() . "vendor/list_all";
        $config['total_rows'] = $this->cms_model->count_all();
        $config['per_page'] = (site_limit()) ? site_limit() : DEFAULT_LIMIT_PER_PAGE;
        $config['full_tag_open'] = '<div class="pagination pagination-small pagination-right"><ul class="pagination">';
        $config['full_tag_close'] = '</ul></div>';
        $config['first_link'] = true;
        $config['last_link'] = true;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["lists"] = $this->cms_model->getList($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        $data['main_content'] = 'cms';
        $data['meta_title'] = "This is an meta title";
        $data['meta_keyword'] = "This is an meta keyword";
        $data['meta_description'] = "This is an meta description";
        $includeJs = array('theme/js/custom_listing.js', 'theme/js/bootbox.min.js');
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $this->load->view('page', $data);
    }

    /**
     * Description : This is setting page
     * Author : Synergy
     * @param array $_POST
     * @return void
     */
    function setting() {
        if ($this->input->post('section'))
            $data['update'] = $this->cms_model->saveBlocks();

        $data['content'] = $this->cms_model->getStaicBlocks();
        $data['main_content'] = 'setting';
        $this->load->view('page', $data);
    }

    /**
     * Description : Used to display Blocks
     * Author : Synergy
     * @param array $_POST
     * @return void
     */
    public function customBlocks() {
        if ($this->input->post('section')) {
            $data['update'] = $this->cms_model->saveBlocks();
        }
        $data['content'] = $this->cms_model->getStaicBlocks();
        $data['main_content'] = 'setting';
        $this->load->view('page', $data);
    }

    /**
     * Description : Used to get title of Site
     * Author : Synergy
     * @param none
     * @return string site title
     */
    public function getSitetitle() {
        $info = $this->cms_model->getStaicBlocks();
        return $info['contacts']->content;
    }

    /**
     * Description : Used to delete the page
     * Author : Synergy
     * @param array $_POST
     * @return json data
     */
    function delete() {
        $id = $this->input->post('delID');
        if (isset($id) and $id != '') {
            $check = $this->cms_model->deletePage($id);
            if ($check) {
                $data['status'] = true;
                $data['message'] = "Deleted Successfully";
                echo json_encode($data);
            } else {
                $data['status'] = false;
                $data['message'] = "Something Went wrong";
                echo json_encode($data);
            }
        } else {
            $data['status'] = false;
            $data['message'] = "Something Went wrong";
            echo json_encode($data);
        }
    }

    /**
     * Description : Used to create a new page
     * Author : Synergy
     * @param array $_POST
     * @return void
     */
    public function createPage() {
        $includeJs = array('theme/js/cms.js',
            'theme/js/bootbox.min.js',
            'theme/plugins/jquery-validation/lib/jquery.js',
            'theme/plugins/jquery-validation/dist/jquery.validate.js'
        );
        $includeCss = array('theme/plugins/jquery-validation/demo/css/screen.css');
        $data['includes_for_layout_css'] = add_includes('css', $includeCss);
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data['allParent'] = $this->cms_model->getAllParentPage();
        $data['main_content'] = 'cms_create';
        $this->load->library('form_validation');
        $this->form_validation->set_rules('title', 'Title', 'trim|required');
        if ($this->form_validation->run() == false) {
            $data['error'] = validation_errors();
            $this->load->view('page', $data);
        } else {
            $this->cms_model->createPage();
            redirect('cms');
        }
    }

    /**
     * Description : Used to edit a page
     * Author : Synergy
     * @param int $id, array $_POST
     * @return void
     */
    public function editPage($id) {
        $includeJs = array('theme/js/cms.js',
            'theme/js/bootbox.min.js',
            'theme/plugins/jquery-validation/lib/jquery.js',
            'theme/plugins/jquery-validation/dist/jquery.validate.js'
        );
        $includeCss = array('theme/plugins/jquery-validation/demo/css/screen.css');
        $data['includes_for_layout_css'] = add_includes('css', $includeCss);
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data['main_content'] = 'cms_edit';
        $data['cms'] = $this->cms_model->getPage($id);
        $data['allParent'] = $this->cms_model->getAllParentPage();
        $this->load->library('form_validation');
        $this->form_validation->set_rules('title', 'Title', 'trim|required');
        if ($this->form_validation->run() == false) {
            $data['error'] = validation_errors();
            $this->load->view('page', $data);
        } else {
            $this->cms_model->updatePage($id);
            redirect('cms');
        }
    }

    /**
     * Description : Used to display page in full width
     * Author : Synergy
     * @param none
     * @return void
     */
    function page($slug = null) {
        $alias = $this->uri->segment(1);
        $pageAlias = str_replace('.html', '', $alias);
        $data['result'] = $this->cms_model->getContent($pageAlias);
        $data['meta_title'] = $data['result']->meta_title;
        $data['meta_description'] = $data['result']->meta_description;
        $data['meta_keyword'] = $data['result']->meta_keywords;
        $data['main_content'] = 'pages';
        $this->load->view('full_width_banner', $data);
    }

    /**
     * Description : Used to display contact us page in full width
     * Author : Synergy
     * @param array $_POST
     * @return void
     */
    function contact_us() {
        $includeJs = array(
            'theme/js/contact.js',
            'theme/js/bootbox.min.js',
            'theme/plugins/jquery-validation/lib/jquery.js',
            'theme/plugins/jquery-validation/dist/jquery.validate.js'
        );
        $includeCss = array('theme/plugins/jquery-validation/demo/css/screen.css', 'theme/plugins/contact-form/bootstrap/css/contact.css', 'theme/plugins/contact-form/bootstrap/css/bootstrap.min.css', 'theme/plugins/contact-form/font-awesome/css/font-awesome.min.css');
        $data['includes_for_layout_css'] = add_includes('css', $includeCss);
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data['meta_description'] = 'Contact Us';
        $data['meta_keyword'] = 'Contact Us';
        $data['main_content'] = "contact";
        $this->load->library('form_validation');
        $this->form_validation->set_rules('first_name', 'First Name', 'trim|required');
        if ($this->form_validation->run() == false) {
            $data['error'] = validation_errors();
        } else {
            $message='<table>'
                    .'<tr><td>Name:</td><td>'.$this->input->post('first_name').'-'.$this->input->post('last_name').'</td>'
                    .'<tr><td>Phone:</td><td>'.$this->input->post('phone').'</td>'
                    .'<tr><td>Email:</td><td>'.$this->input->post('email').'</td>'
                    .'<tr><td>Message:</td><td>'.$this->input->post('message').'</td>'
                    . '</table>';
            $this->load->library('email');
            $this->email->from($this->input->post('email'), $this->input->post('first_name'));
            $this->email->to(ADMIN_EMAIL);
            //$this->email->cc('another@another-example.com');
            $this->email->subject('99 Retail Street Contact Form');
            $this->email->message($message);
            $this->email->send();

            $this->session->set_flashdata('message', 'Thank You for the message.We will contact you soon!');
        }
        $data['meta_title'] = 'Contact Us';
        $this->load->view('full_width_banner', $data);
    }
    
    /**
     * Description : Used to display all rules
     * Author : Synergy
     * @param array $_POST
     * @return array $data
     */
    public function rules() {
        $data["lists"] = $this->cms_model->getRules();
        $data['main_content'] = 'rules';
        $data['meta_title'] = "Guide & Rules";
        $data['meta_keyword'] = "This is an meta keyword";
        $data['meta_description'] = "This is an meta description";
        $includeJs = array('theme/js/custom_listing.js', 'theme/js/bootbox.min.js');
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $this->load->view('page', $data);
    }
    
    /**
     * Description : Used to edit rule
     * Author : Synergy
     * @param array $id, array $_POST
     * @return array $data
     */
    public function editRule($id) {
        $includeJs = array(
            'theme/js/bootbox.min.js',
            'theme/plugins/jquery-validation/lib/jquery.js',
            'theme/plugins/jquery-validation/dist/jquery.validate.js'
        );
        $includeCss = array('theme/plugins/jquery-validation/demo/css/screen.css');
        $data['includes_for_layout_css'] = add_includes('css', $includeCss);
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data['main_content'] = 'rules_edit';
        $data['rule'] = $this->cms_model->getRule($id);
        $data['allParent'] = $this->cms_model->getAllParentPage();
        $this->load->library('form_validation');
        $this->form_validation->set_rules('title', 'Title', 'trim|required');
        if ($this->form_validation->run() == false) {
            $data['error'] = validation_errors();
            $this->load->view('page', $data);
        } else {
            $this->cms_model->updateRule($id);
            redirect('cms/rules');
        }
    }
}

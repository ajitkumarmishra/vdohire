<script src="<?php echo base_url('theme/plugins/ckeditor/ckeditor.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('theme/plugins/ckfinder/ckfinder.js'); ?>"></script>
<div class="row">
    <div class="col-sm-12 col-md-12">
        <h2>Edit Rule</h2>
        <?php if (@$error): ?>
            <div class="alert">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <?php echo $error; ?>
            </div>
        <?php endif; ?>
        <form method="post" id="cmsForm" action="">
            <div class="row">
                <div class="col-sm-6 col-md-6">
                    <div class="form-group">
                        <label for="title">File Type</label>
                        <div class="controls">
                            <input class="form-control" type="text" id="title" value="<?php echo $rule->rule_file_type; ?>" name="title" readonly>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6">
                    <div class="form-group">
                        <label for="alias">Rule Type</label>
                        <div class="controls">
                            <input class="form-control" type="text" id="alias" value="<?php echo $rule->rule_type; ?>" name="alias" readonly>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <div class="col-sm-12 col-md-12">
                        <textarea id="editor1" name="description" class="form-control" rows="20"><?php echo $rule->description; ?></textarea>
                    </div>
                </div>
                <script>
                    var editor = CKEDITOR.replace('editor1');
                    editor.config.extraAllowedContent = 'div(*)';
                    CKEDITOR.config.allowedContent = true;
                    CKEDITOR.disableAutoInline = true;
                    CKFinder.setupCKEditor(editor, '<?php echo base_url() ?>theme/plugins/ckfinder/');
                </script>
            </div>
            
            <div class="form-actions no-margin-bottom" style="text-align:center;">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
</div>

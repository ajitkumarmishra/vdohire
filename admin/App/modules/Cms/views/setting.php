<div class="row">
    <div class="col-sm-12 col-md-12">
        <h2>CMS Setting</h2>
        <?php $aBlock = $this->input->get('S') ? $this->input->get('S') : 'contacts'; ?>

        <div class="panel-heading">
            <?php
            $Tabs = array(
                'contacts' => 'Contacts',
                'google_analytic_code' => 'Google Analytic Code',
                'logo' => 'Site Logo'
            );
            ?>
            <ul class="nav nav-pills">		
                <?php foreach ($Tabs as $t => $abs): ?>				    
                    <li class="<?php echo $aBlock == $t ? 'active' : ''; ?>">
                        <a data-toggle="tab" href="#<?php echo $t; ?>"><?php echo $abs; ?></a>
                    </li>												
                <?php endforeach; ?>
            </ul>
        </div>
        <div class="panel-body">
            <div class="alert alert-success alert-dismissable" id="successMsg" style="display:<?php echo @$update == true ? 'block' : 'none'; ?>;">
                Successfully Updated . .<button aria-hidden="true" data-dismiss="alert" class="close" type="button">�</button>                               
            </div>
            <div class="tab-content">

                <?php $contacts = @unserialize($content['contacts']->content); ?>
                <?php
                $contArray = array(
                    'c_telephone' => 'Customer Service Telephone',
                    'c_email' => 'Customer Service E-mail',
                    'c_fax' => 'Fax',
                    'c_address' => 'Address',
                    'c_facebook' => 'Facebook',
                    'c_title' => 'Site Title',
                    'c_limit' => 'Limit Per Page'
                );
                ?>
                <div class="tab-pane fade<?php echo $aBlock == 'contacts' ? ' in active' : ''; ?>" id="contacts">
                    <form action="<?php echo base_url('cms/customBlocks?S=contacts'); ?>" method="post" role="form">
                        <div class="col-sm-12 col-md-12">
                            <?php foreach ($contArray as $name => $title): ?>
                                <div class="form-group">
								<label for="<?php echo $name; ?>"><?php echo $title; ?></label>
								<input type="test" class="form-control" id="<?php echo $name; ?>" name="<?php echo $name; ?>" value="<?php echo @$contacts[$name]; ?>">
                                </div>
                                <div class="clearfix"></div>  
                            <?php endforeach; ?>
                        </div>
                        <div class="clear" style="clear:both;"></div>
                        <div class="col-sm-12 col-md-12" style="text-align:center;">
                            <input type="submit" class="btn btn-primary btn-grad submit P_finish" value="Save" style="display: inline-block;">
                        </div>
                        <input type="hidden" name="section" value="contacts" />
                    </form>
                </div>

                <?php
                $simpleTabs = array(
                    'google_analytic_code' => 'Google Analytic Code',
                );
                ?>
                <?php foreach ($simpleTabs as $tabkey => $tabs): ?>
                    <div class="tab-pane fade<?php echo $aBlock == $tabkey ? ' in active' : ''; ?>" id="<?php echo $tabkey; ?>">
                        <form action="<?php echo base_url('cms/customBlocks?S=' . $tabkey); ?>" method="post">
                            <?php $cntnt = @unserialize($content[$tabkey]->content); ?>
                            <div class="col-sm-12 col-md-12">
                                <div class="form-group">
                                    <label>Title</label>
                                    <input type="text" class="form-control" name="title" value="<?php echo @$cntnt['title']; ?>" />
                                </div>
                            </div>

                            <div class="col-sm-12 col-md-12">
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea rows="10" class="form-control" name="description" ><?php echo @$cntnt['description']; ?></textarea>
                                </div>
                            </div>

                            <div class="clear" style="clear:both;"></div>
                            <div class="col-sm-12 col-md-12" style="text-align:center;">
                                <input type="submit" class="btn btn-primary btn-grad submit P_finish" value="Save" style="display: inline-block;">
                            </div>
                            <input type="hidden" name="section" value="<?php echo $tabkey; ?>" />						            
                        </form>
                    </div>
                <?php endforeach; ?>

                <div class="tab-pane fade<?php echo $aBlock == 'logo' ? ' in active' : ''; ?>" id="logo">
                    <form class="form-horizontal" action="<?php echo base_url('cms/customBlocks?S=logo'); ?>" method="post" enctype="multipart/form-data">
                        <?php if ($content['logo']->content && file_exists(IMAGESPATH . 'logo/' . $content['logo']->content)) { ?>
                            <div class="form-group">
                                <label class="control-label">Current Logo</label>
                                <div class="col-sm-12 col-md-12">
                                    <img src="<?php echo base_url('uploads/logo/' . $content['logo']->content); ?>" width="140" />
                                    <input type="checkbox" id="deleteLogo" name="deleteLogo" value="1" />Delete
                                </div>
                            </div>
                        <?php } ?>
                        <div class="col-sm-12 col-md-12">
                            <div class="form-group">
                                <label class="control-label">Site Logo</label>
                                <div class="col-sm-12 col-md-12">
                                    <input type="file" class="form-control" name="logo" />
                                </div>
                            </div>
                        </div>
                        <div class="clear" style="clear:both;"></div>
                        <div class="col-sm-12 col-md-12" style="text-align:center;">
                            <input type="submit" class="btn btn-primary btn-grad submit P_finish" value="Save" style="display: inline-block;">
                        </div>
                        <input type="hidden" name="section" value="logo" />
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

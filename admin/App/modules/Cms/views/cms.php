<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="row">
            <div class="col-sm-9 col-md-9">
                <h2>Pages List</h2>
            </div>
            <div class="col-sm-3 col-md-3">
                <a href="<?php echo base_url('cms/createPage');?>" class="btn btn-primary pull-right">Add New</a>
            </div>
        </div>
        <form method="post" name="form1" id="form1" action="">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th><input name="check_all" type="checkbox" id="check_all" value="1" onclick="checkall(this.form)" /></th>
                        <th>Title</th>
                        <th>Alias</th>
                        <th>Page Heading</th>
                        <th>Status</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($lists as $list) { ?>
                        <tr>
                            <td>
                                <input name="arr_ids[]" type="checkbox" id="arr_ids[]" value="<?php echo $list->id; ?>" />
                            </td>
                            <td><?php echo $list->title; ?></td>
                            <td><?php echo $list->alias; ?></td>
                            <td><?php echo $list->page_heading; ?></td>
                            <td><?php echo ($list->is_active == '1' ? 'Active' : 'Inactive'); ?></td>
                            <td><a href="<?php echo base_url('cms/editPage') . '/' . $list->id; ?>"><span class="glyphicon glyphicon-pencil pull-right"></a></td>
                            <td><a href="javascript:void(0);" class="btn btn-mini btn-xs del_Listing" name="<?php echo $list->id; ?>"><span class="glyphicon glyphicon-remove "></a></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="left" style="padding:2px">
                        <input type="submit" name="Activate" value="Activate" class="btn"/>
                        <input type="submit" name="Deactivate" value="Deactivate" class="btn" />
                        <input type="submit" name="Delete" value="Delete" class="btn" />
                    </td>
                </tr>
            </table>
            <input type="hidden" id="P_orderby" value="id" />
            <input type="hidden" id="P_orderin" value="desc" />
            <input type="hidden" id="P_page" value="1" />
            <input type="hidden" id="P_reqesturl" value="<?php echo base_url('cms'); ?>" />
            <input type="hidden" id="P_deleteurl" value="<?php echo base_url('cms/delete'); ?>" />
            <input type="hidden" id="P_responseDiv" value="responce_container" />
            <?php echo $links; ?>
        </form>
    </div>
</div>

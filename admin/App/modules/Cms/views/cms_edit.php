<script src="<?php echo base_url('theme/plugins/ckeditor/ckeditor.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('theme/plugins/ckfinder/ckfinder.js'); ?>"></script>
<div class="row">
    <div class="col-sm-12 col-md-12">
        <h2>Edit Page</h2>
        <?php if (@$error): ?>
            <div class="alert">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <?php echo $error; ?>
            </div>
        <?php endif; ?>
        <form method="post" id="cmsForm" action="">
            <div class="row">
                <div class="col-sm-6 col-md-6">
                    <div class="form-group">
                        <label for="title">Page Title</label>
                        <div class="controls">
                            <input class="form-control" type="text" id="title" value="<?php echo $cms->title; ?>" name="title">
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6">
                    <div class="form-group">
                        <label for="alias">Alias</label>
                        <div class="controls">
                            <input class="form-control" type="text" id="alias" value="<?php echo $cms->alias; ?>" name="alias">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <div class="col-sm-12 col-md-12">
                        <textarea id="editor1" name="description" class="form-control" rows="20"><?php echo $cms->description; ?></textarea>
                    </div>
                </div>
                <script>
                    var editor = CKEDITOR.replace('editor1');
                    editor.config.extraAllowedContent = 'div(*)';
                    CKEDITOR.config.allowedContent = true;
                    CKEDITOR.disableAutoInline = true;
                    CKFinder.setupCKEditor(editor, '<?php echo base_url() ?>theme/plugins/ckfinder/');
                </script>
            </div>
            <div class="row">
                <div class="col-sm-6 col-md-6">
                    <div class="form-group">
                        <label for="page_heading">Page Heading</label>
                        <div class="controls">
                            <input class="form-control" type="text" id="page_heading" value="<?php echo $cms->page_heading; ?>" name="page_heading">
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6">
                    <div class="form-group">
                        <label for="page_order">Page Order</label>
                        <div class="controls">
                            <input class="form-control" type="text" id="page_order" value="<?php echo $cms->page_order; ?>" name="page_order">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <label for="meta_title">Parent ID</label>
                    <div class="controls">
                        <select class="form-control" name="parent_id" id="parent_id">
                            <option value="0">Parent Page</option>
                            <?php foreach ($allParent as $page) { ?>
                                <option value="<?php echo $page->id; ?>" <?php echo $cms->parent_id == $page->id ? 'selected="selected"' : ''; ?>><?php echo $page->title; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <label for="include_in">Include In</label>
                    <div class="controls">
                        <select class="form-control" name="include_in" id="include_in">
                            <option value="">Select</option>
                            <option value="header" <?php echo $cms->include_in == 'header' ? 'selected="selected"' : ''; ?>>Header</option>
                            <option value="footer" <?php echo $cms->include_in == 'footer' ? 'selected="selected"' : ''; ?>>Footer</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <label for="is_active">Status</label>
                    <div class="controls">
                        <select class="form-control" name="is_active" id="is_active">
                            <option value="">Select</option>
                            <option value="1" <?php echo $cms->is_active == '1' ? 'selected="selected"' : ''; ?>>Active</option>
                            <option value="0" <?php echo $cms->is_active == '0' ? 'selected="selected"' : ''; ?>>Inactive</option>
                        </select> 
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <label for="meta_title">Page Title</label>
                    <div class="controls">
                        <input type="text" id="meta_title" name="meta_title" class="form-control" value="<?php echo $cms->meta_title; ?>" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <label for="meta_keywords">Meta Keywords</label>
                    <div class="controls">
                        <textarea id="meta_keywords" name="meta_keywords" class="form-control" rows="2" ><?php echo $cms->meta_keywords; ?></textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <label for="meta_description">Meta Description</label>
                    <div class="controls">
                        <textarea id="meta_description" name="meta_description" class="form-control" rows="2" ><?php echo $cms->meta_description; ?></textarea>
                    </div>
                </div>
            </div>
            <div class="form-actions no-margin-bottom" style="text-align:center;">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
</div>

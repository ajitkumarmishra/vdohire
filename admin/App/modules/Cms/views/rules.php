<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="row">
            <div class="col-sm-9 col-md-9">
                <h2>Guide & Rules</h2>
            </div>
            <div class="col-sm-3 col-md-3">
                
            </div>
        </div>
        <form method="post" name="form1" id="form1" action="">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>File Type</th>
                        <th>Rule Type</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($lists as $list) { ?>
                        <tr>
                            <td><?php echo $list->rule_file_type; ?></td>
                            <td><?php echo $list->rule_type; ?></td>
                            <td><?php echo ($list->status == '1' ? 'Active' : 'Inactive'); ?></td>
                            <td><a href="<?php echo base_url('cms/editRule') . '/' . $list->rule_id; ?>"><span class="glyphicon glyphicon-pencil pull-right"></a></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <input type="hidden" id="P_orderby" value="id" />
            <input type="hidden" id="P_orderin" value="desc" />
            <input type="hidden" id="P_page" value="1" />
            <input type="hidden" id="P_responseDiv" value="responce_container" />
        </form>
    </div>
</div>

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : CMS Model
 * Description : Handle all the CRUD operation for CMS
 * @author Synergy
 * @createddate : Nov 10, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 16, 2017
 */
class Cms_model extends CI_Model {

    /**
     * Defining static variables
     */
    public $table_cms = 'cms';
    public $table_static_block = 'static_blocks';
    var $pages_table = "pages";
    var $rule_table = "nc_rules";

    /**
     * Responsable for inherit the parent constructor
     * @return void
     */
    function __construct() {
        parent::__construct();
    }

    /**
     * Description : Used to save the blocks
     * Author : Synergy
     * @param array $_GET, array $_POST
     * @return boolean
     */
    public function saveBlocks() {
        $action = $this->input->get('S');
        $alias = $this->input->post('section');

        $data['content'] = "";
        switch ($action) {
            case 'f-cms':
                $pids = $this->input->post('flinks');
                if (!empty($pids))
                    $data['content'] = serialize(explode(',', $pids));
                break;
            case 'logo':
                $this->deletelogo();
                $logo_info = $this->uploadLogo();
                if ($logo_info['file_name']) {
                    $data['content'] = $logo_info['file_name'];
                }
                break;

            default:
                $data['content'] = serialize($this->input->post());
                break;
        }
        $this->db->where('alias', $alias);

        $return = $this->db->update($this->table_cms, $data);
        return $return;
    }

    /**
     * Description : Use to get list of blocks
     * Author : Synergy
     * @param $alias
     * @return array of blocks
     */
    public function getStaicBlocks($alias = "") {
        if ($alias != "")
            $this->db->where('alias', $alias);

        $query = $this->db->get($this->table_cms);
        $qqq = $query->result();
        $ret = array();
        foreach ($qqq as $row) {
            $ret[$row->alias] = $row;
        }
        return $ret;
    }

    /**
     * Description : Use to upload file
     * Author : Synergy
     * @param $alias
     * @return array of data
     */
    function uploadLogo() {
        $config = array();
        $config['upload_path'] = IMAGESPATH . 'logo/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['file_name'] = md5(uniqid(rand(), true));
        $this->load->helper(array('form', 'url'));
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if ($this->upload->do_upload('logo')) {
            $info = $this->upload->data();
        } else {
            $info = array('file_name' => '');
        }
        return $info;
    }

    /**
     * Description : Use to delete logo
     * Author : Synergy
     * @param $_POST
     * @return boolean
     */
    function deletelogo() {
        if ($this->input->post('deleteLogo')) {
            $logo = $this->getStaicBlocks();
            $file_name = $logo['logo']->content;
            unlink(IMAGESPATH . 'logo/' . $file_name);
        }
    }

    /**
     * Description : Use to get list of pages
     * Author : Synergy
     * @param $_POST
     * @return array list of pages
     */
    function getList($showPerpage, $offset) {
        $conditions = array(
            'is_deleted' => '0'
        );
        $this->db->where($conditions);
        $this->db->limit($showPerpage, $offset);
        $this->db->from($this->pages_table);
        $result = $this->db->get();
        return $result->result();
    }

    /**
     * Description : Use to update status of page
     * Author : Synergy
     * @param $_POST
     * @return boolean
     */
    function updateStatus() {
        if ($this->input->post('Delete') != null) {
            $data = array(
                'is_deleted' => '1'
            );
        } else {
            if ($this->input->post('Deactivate') != null) {
                $status = "0";
            } else if ($this->input->post('Activate') != null) {
                $status = "1";
            }
            $data = array(
                'is_active' => $status
            );
        }
        $arr_ids = $this->input->post('arr_ids');
        if (!empty($arr_ids)) {
            $this->db->where_in('id', $arr_ids);
            $this->db->update($this->pages_table, $data);
        }
    }

    /**
     * Description : Use to count the number of pagers
     * Author : Synergy
     * @param none
     * @return int number of pages
     */
    function count_all() {
        $conditions = array(
            'is_deleted' => '0'
        );
        $this->db->where($conditions);
        $query = $this->db->get($this->pages_table);
        return $query->num_rows();
    }

    /**
     * Description : Use to delete a page
     * Author : Synergy
     * @param int $id
     * @return boolean
     */
    function deletePage($id) {
        $data = array(
            'is_deleted' => '1',
        );
        $this->db->where('id', $id);
        $query = $this->db->update($this->pages_table, $data);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Description : Use to create a page
     * Author : Synergy
     * @param array $_POST
     * @return boolean
     */
    public function createPage() {
        $data = $this->input->post();
        $query = $this->db->insert($this->pages_table, $data);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Description : Use to update a page
     * Author : Synergy
     * @param int $id, array $_POST
     * @return boolean
     */
    function updatePage($id) {
        $data = $this->input->post();
        $this->db->where('id', $id);
        $query = $this->db->update($this->pages_table, $data);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Description : Use to get details of a page
     * Author : Synergy
     * @param int $id
     * @return array of data
     */
    function getPage($id) {
        $this->db->where('id', $id);
        $this->db->from($this->pages_table);
        $result = $this->db->get();
        return $result->row();
    }

    /**
     * Description : Use to get all parent pages
     * Author : Synergy
     * @param int $exclue
     * @return array of data
     */
    function getAllParentPage($exclue = 0) {
        if ($exclue != 0)
            $this->db->where('id !=', $exclue);
        $this->db->where('parent_id', '0');
        $this->db->order_by("id", "desc");
        $query = $this->db->get($this->pages_table);
        return $query->result();
    }

    /**
     * Description : Use to get content of a page
     * Author : Synergy
     * @param int $alias
     * @return array of data
     */
    function getContent($alias) {
        $this->db->where('alias', $alias);
        $query = $this->db->get($this->pages_table);
        return $query->row();
    }

    /**
     * Description : Use to get all the rules
     * Author : Synergy
     * @param none
     * @return array of data
     */
    public function getRules() {
        $query = $this->db->get($this->rule_table);
        return $query->result();
    }

    /**
     * Description : Use to get detials of a rule
     * Author : Synergy
     * @param int $id
     * @return array of data
     */
    function getRule($id) {
        $this->db->where('rule_id', $id);
        $this->db->from($this->rule_table);
        $result = $this->db->get();
        return $result->row();
    }

    /**
     * Description : Use to update rule
     * Author : Synergy
     * @param int $id
     * @return boolean
     */
    function updateRule($id) {
        $data = array(
            'description' => $this->input->post('description')
        );
        $this->db->where('rule_id', $id);
        $query = $this->db->update($this->rule_table, $data);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }
}
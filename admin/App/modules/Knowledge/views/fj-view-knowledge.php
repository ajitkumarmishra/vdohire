<div id="page-content-wrapper">
    <div class="container-fluid whitebg">
        <div class="row">
            <div class="col-md-9"><h4><img src="<?php echo base_url(); ?>/theme/firstJob/image/arrow_head.png" class="prev-page">Knowledge Center Category Detail</h4></div>
            
            <div class="clearfix"></div>

            <div class="col-md-12 rsp_tbl">
                <div class="tab-content">
                    <div id="home" class="tab-pane fade active in">
                        <?php
                        if((count($topics)>0) && (count($topics)!=1) && ($this->uri->segment(2)!='topicview')) { ?>
                        <table class="table" id="sorting">
                            <thead class="tbl_head">
                                <tr>
                                    <th><span><?php echo $topics[0]['categoryName']; ?> -> Topics</span></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 0;
                                foreach ($topics as $item):
                                    ?>
                                    <tr <?php if ($i % 2 != 0) echo 'class="tbl_bg"'; ?>>
                                        <td><a href="<?php echo base_url(); ?>knowledge/topicview/<?php echo $item['id']; ?>"><?php echo $item['topic']; ?></a></td>
                                        <td align='right'> 
                                            <a href="javascript:" 
                                               data-toggle="modal" 
                                               data-target="#editKnowledgeCenterTopic" 
                                               class="edit-knowledge-center-topic" 
                                               data-options='{"topicId":"<?php echo $item['id']; ?>", "topic":"<?php echo $item['topic']; ?>"}'><img src="<?php echo base_url(); ?>/theme/firstJob/image/edit.png"></a>
                                            &emsp;
                                            <a href="javascript:" class="delete-topics" id="<?php echo $item['id']; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/del.png"></a>
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                endforeach;
                                ?>
                            </tbody>
                        </table>
                        <?php
                        }
                        else { ?>                        
                        <form class="form-horizontal form_save" method="post" action="<?php echo base_url(); ?>knowledge/editdescription" enctype="multipart/form-data" id="editKnowledgeCenterDescription">
                            <table class="table" id="sorting">
                                <thead class="tbl_head">
                                    <tr>
                                        <th><span>Description</span></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Category - <i>"<?php echo $topics[0]['categoryName'];?>"</i>&emsp;&emsp;&emsp;Topic - <i>"<?php echo $topics[0]['topic'];?>"</i></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <script type="text/javascript" src="//cdn.tinymce.com/4/tinymce.min.js"></script>
                                            <script>
                                                tinymce.init({
                                                    selector: 'textarea',
                                                    height: 300,
                                                    plugins: [
                                                      'advlist autolink lists link image charmap print preview',
                                                      'searchreplace visualblocks code fullscreen',
                                                      'insertdatetime media table contextmenu paste code'
                                                    ],
                                                    toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
                                                    content_css: [
                                                      '//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
                                                      '//www.tinymce.com/css/codepen.min.css'
                                                    ]
                                                });
                                            </script>
                                            <textarea name="description" class="knowledgeDescription" id="description"><?php echo $topics[0]['description']; ?></textarea>
                                            <input type="text" data-validation="knowledgeDescription" style="height: 0px; width: 0px; visibility: hidden; " />
                                            <input type="hidden" class="form-control" value="<?php echo $topics[0]['id']; ?>" id="topicId" name="topicId">
                                        </td>                                    
                                    </tr>
                                    <tr>
                                        <td align='right'><button type="button" class="btn btn-primary edit-knowledge-description">Update</button></td>
                                    </tr>
                                </tbody>
                            </table>
                        </form>
                        <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
            <!--            
            <div class="col-md-12">  
                <ul class="pagination pull-right">
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                </ul>
            </div>  
            -->

            <div class="service_content " id="setPaging">
                <?php if ($totalCount > $itemsPerPage): ?>
                <ul class="service_content_last_list">
                    <?php if ($page > 1): ?>
                        <p class="pagi_img"><a href="<?php echo base_url(); ?>knowledge/list/<?php echo $page - 1; ?>" ><img src="<?php echo base_url(); ?>/theme/firstJob/image/pagination_img2.png"></a></p>    
                    <?php endif; ?>
                    <input type="hidden" name="page_no" value="1" id="page_no"> 
                    <?php for ($i = 1; $i <= $count; $i++): ?>
                        <li><a href="<?php echo base_url(); ?>knowledge/list/<?php echo $i; ?>"  <?php if ($i == $page): ?>style="color: orange;"<?php endif; ?>><?php echo $i; ?></a></li>
                    <?php endfor; ?>
                    <?php if ($page < $count): ?>
                        <p class="pagi_img"><a href="<?php echo base_url(); ?>knowledge/list/<?php echo $page + 1; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/pagination_img1.png"></a></p>            
                    <?php endif; ?>
                </ul>
                <?php endif; ?>
            </div>

        </div>
    </div>
</div>






<!-- PopUp for add question to interview set  -->
<div id="editKnowledgeCenterTopic" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <form class="form-horizontal form_save" action="<?php echo base_url(); ?>knowledge/edittopic" method="post" id="knowledgeCenterTopicForm" >
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Knowledge Center Topic</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger hidden" id="knowledgeCenterTopicAlert"></div>
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-3">Edit Topic Title</label>
                        <div class="col-xs-9" id="knowledgeCenterTopic">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary edit-knowledge-topic">Update</button>
                </div>
            </div>
        </form>
    </div>
</div>



<script>
    $(document).on('click', '.edit-knowledge-center-topic', function () {
        var topic    = $(this).data('options').topic;
        var topicId  = $(this).data('options').topicId; 
        $('#knowledgeCenterTopicAlert').addClass('hidden');
        //$.post(siteUrl + 'knowledge/getKnowledgeCenterTopic', {topic: topic, accessToken: token, topicId: topicId}, function (data) {
            var data = '<input type="text"  class="form-control" value="'+topic+'" id="topic" name="topic"><input type="hidden" class="form-control" value="'+topicId+'" id="topicId" name="topicId">';
            $('#knowledgeCenterTopic').html(data);
        //});
    });

    $(document).on('click', '.edit-knowledge-topic', function () {
        var topic    = $('#topic').val();
        var topicId  = $('#topicId').val();
        var url         = '<?php echo base_url(); ?>';
        if (!topic && !topicId) {
            $('#knowledgeCenterTopicAlert').html('Please Enter Topic.');
            $('#knowledgeCenterTopicAlert').removeClass('hidden');
        } else {
            $.post(url + 'knowledge/checktopic', {accessToken: token, topic: topic, topicId: topicId}, function (data) {
                //alert(data.trim());
                if (data.trim() == 1) {
                    $('#knowledgeCenterTopicAlert').html('This topic is already exist.');
                    $('#knowledgeCenterTopicAlert').removeClass('hidden');
                    return false;
                } else {
                    $('#knowledgeCenterTopicForm').submit();
                }
            });

        }
    });
    
    
    

    $(document).on('click', '.edit-knowledge-description', function () {
        var description = $('#description').val();
        var url         = '<?php echo base_url(); ?>';
        $('#editKnowledgeCenterDescription').submit();
    });
    
    
    
    
    $(function () {
        var settingTableRowColor = function (tableName) {
            $('#' + tableName + ' tbody tr').removeAttr('class');
            $('#' + tableName + ' tbody tr:odd').attr('class', 'tbl_bg');
        }
        settingTableRowColor('sorting');
        $('.sort-header').on('click', function () {
            settingTableRowColor('sorting');
        });
        $('.delete-topics').on('click', function () {
            var url         = '<?php echo base_url(); ?>';
            var topicId     = $(this).attr('id');
            var accessToken = $('#accessToken').val();
            bootbox.confirm('Are you sure you want to delete?', function (result) {
                //alert(result);
                if (result) {
                    $.post(url + 'knowledge/deletetopic', {id: topicId, accessToken: accessToken}, function (data) {
                        //bootbox.alert(data);
                        location.reload();
                    });
                }
            });

        });
    });
</script>




<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script>
// Add validator For Interview Assessment
$.formUtils.addValidator({
    name : 'knowledgeDescription',
    validatorFunction : function(value, $el, config, language, $form) {
        if(tinymce.get('description').getContent()!='') {
            return true;
        }
        else {
            return false;
        }
        //return parseInt(value, 10) % 2 === 0;
    },
    errorMessage : 'This is a required field'
});

$.validate({
    form: '#editKnowledgeCenterDescription',
    modules: 'file',
    onError: function ($form) {
        //alert($('.setquestion').val());
        //alert($('.setInterviewType').html());
        return false;
    },
    onSuccess: function ($form) {
        return true;
    },
});
</script>


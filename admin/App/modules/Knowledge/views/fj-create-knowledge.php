<?php
if (isset($interview) && $interview != NULL) {
    //echo '<pre>';print_r($interview);  
}
?>
<div id="page-content-wrapper">
    <?php if (isset($message)): ?>
    <div class="alert alert-success">
        <?php echo $message; ?>
    </div>
    <?php endif; ?>
    <?php if (isset($error)): ?>
    <div class="alert alert-danger">
        <?php foreach ($error as $item): ?>
            <?php echo $item; ?>
        <?php endforeach; ?>
    </div>
    <?php endif; ?>
    
    <div class="container-fluid whitebg">
        <div class="row">
            <div class="col-md-12 padd_tp_bt_20">
                <h4><img src="<?php echo base_url(); ?>/theme/firstJob/image/arrow_head.png" class="prev-page"> Add Knowledge Center</h4>
                <form class="form-horizontal form_save" method="post" action="" enctype="multipart/form-data" id="knowledgeCenter">
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-2 required-field">Category <span style="font-size:10px;">(Limit 30 char)</span></label>
                        <div class="col-xs-10  ques-title">
                            <select data-placeholder="Type Knowledge Center Category" style="width: 350px; display: none;" class="chosen-select sel_sty_padd kCategory" tabindex="-1" name="categoryName">
                                <option value=""></option>
                                <?php foreach ($fjCategory as $item): ?>
                                    <option><?php echo $item['categoryName']; ?></option>
                                <?php endforeach; ?>
                            </select>
                            <input type="text" data-validation="knowledgeCategory" style="height: 0px; width: 0px; visibility: hidden; " />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-2">Topic</label>
                        <div class="col-xs-10  ques-title">
                            <select data-placeholder="Type Knowledge Center Topic" style="width: 350px; display: none;" class="chosen-select sel_sty_padd" tabindex="-1" name="categoryTopic">
                                <option value=""></option>
                                <?php foreach ($fjTopics as $item): ?>
                                    <option><?php echo $item['topic']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-2 required-field">Description</label>
                        <div class="col-xs-10">
                            <script type="text/javascript" src="//cdn.tinymce.com/4/tinymce.min.js"></script>
                            <script>
                                tinymce.init({
                                    selector: 'textarea',
                                    height: 300,
                                    plugins: [
                                      'advlist autolink lists link image charmap print preview',
                                      'searchreplace visualblocks code fullscreen',
                                      'insertdatetime media table contextmenu paste code'
                                    ],
                                    toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
                                    content_css: [
                                      '//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
                                      '//www.tinymce.com/css/codepen.min.css'
                                    ]
                                });
                            </script>
                            <textarea name="description" id="description" class="knowledgeDescription"><?php echo $topics[0]['description']; ?></textarea>                            
                            <input type="text" data-validation="knowledgeDescription" style="height: 0px; width: 0px; visibility: hidden; " />
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="col-xs-offset-2 col-xs-10 no_padding">
                        <input type="hidden" name="accessToken" value="<?php echo $token; ?>"/>
                        <button type="submit" class="Save_frm pull-right save-interview" value="testing">SAVE</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>


<script src="<?php echo base_url(); ?>theme/firstJob/js/chosen.jquery.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>theme/firstJob/js/prism.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
    var config = {
        '.chosen-select':           {},
        '.chosen-select-deselect':  {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results':{no_results_text: ''},
        '.chosen-select-width':     {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }

    $(function () {
        $("#datepicker-19").datepicker();
    });
    $(document).on('blur', '.chosen-search input', function (evt) {
        //alert($('.no-results').length);
        if ($('.no-results').length == 1) {
            //alert($('.chosen-choices .search-field input').val());
            //$('.chosen-select');
            $(this).parents('.ques-title').find('.chosen-select').append('<option>' + $(evt.target).val() + '</option>');
            $(this).parents('.ques-title').find('.chosen-select').val($(evt.target).val());
            $(this).parents('.ques-title').find('.chosen-select').trigger('chosen:updated');

        }

    });
</script>

<style>
    .no-results {
        display: none!important;
    }
    .chosen-single {
        background :none!important;
        border-radius:0px !important;
        height: 34px!important;
    }
</style>




<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script>
// Add validator For Interview Assessment
$.formUtils.addValidator({
    name : 'knowledgeCategory',
    validatorFunction : function(value, $el, config, language, $form) {
        if($('.kCategory').val()!='' && $('.kCategory').val().length<=30) {
            return true;
        }
        else {
            return false;
        }
        //return parseInt(value, 10) % 2 === 0;
    },
    errorMessage : 'This is a required field (limit 30 characters) '
});


// Add validator For Interview Assessment
$.formUtils.addValidator({
    name : 'knowledgeDescription',
    validatorFunction : function(value, $el, config, language, $form) {
        if(tinymce.get('description').getContent()!='') {
            return true;
        }
        else {
            return false;
        }
        //return parseInt(value, 10) % 2 === 0;
    },
    errorMessage : 'This is a required field'
});

$.validate({
    form: '#knowledgeCenter',
    modules: 'file',
    onError: function ($form) {
        //alert($('.setquestion').val());
        //alert($('.setInterviewType').html());
        return false;
    },
    onSuccess: function ($form) {
        return true;
    },
});
</script>


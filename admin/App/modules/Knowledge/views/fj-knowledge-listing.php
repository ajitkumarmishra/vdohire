<div id="page-content-wrapper">
    <div class="container-fluid whitebg">
        <div class="row">
            <div class="col-md-9"><h2><img src="<?php echo base_url(); ?>/theme/firstJob/image/arrow_head.png" class="prev-page">Manage Knowledge Center</h2></div>
            <div class="col-md-3 ed_job_bt"><a href="<?php echo base_url(); ?>knowledge/add" class="add-new-set"><button type="button">Add New Knowledge Center</button></a></div>

            <div class="clearfix"></div>

            <div class="col-md-12 rsp_tbl">
                <div class="tab-content">
                    <div id="home" class="tab-pane fade active in">
                        <table class="table" id="sorting">
                            <thead class="tbl_head">
                                <tr>
                                    <!--                          	
                                    <th><input type="checkbox" class="chk_bx"></th>
                                    -->
                                    <th><span>Category Title</span></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 0;
                                foreach ($contentKnowledgeCenter as $item):
                                    ?>
                                    <tr <?php if ($i % 2 != 0) echo 'class="tbl_bg"'; ?>>
                                        <!--
                                        <td><input type="checkbox"></td>
                                        -->
                                        <td><a href="<?php echo base_url(); ?>knowledge/view/<?php echo $item->id; ?>"><?php echo $item->categoryName; ?></a></td>
                                        <td align='right'>
                                            <a href="javascript:" 
                                               data-toggle="modal" 
                                               data-target="#editKnowledgeCenterCategory" 
                                               class="edit-knowledge-center-category" 
                                               data-options='{"categoryId":"<?php echo $item->id; ?>", "category":"<?php echo $item->categoryName; ?>"}'><img src="<?php echo base_url(); ?>/theme/firstJob/image/edit.png"></a>
                                            &emsp;
                                            <a href="javascript:" class="delete-category" id="<?php echo $item->id; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/del.png"></a>                                           
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                endforeach;
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div> 

            <!--            
            <div class="col-md-12">  
                <ul class="pagination pull-right">
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                </ul>
            </div>  
            -->

            <div class="service_content " id="setPaging">
                <?php if ($totalCount > $itemsPerPage): ?>
                <ul class="service_content_last_list">
                    <?php if ($page > 1): ?>
                        <p class="pagi_img"><a href="<?php echo base_url(); ?>knowledge/list/<?php echo $page - 1; ?>" ><img src="<?php echo base_url(); ?>/theme/firstJob/image/pagination_img2.png"></a></p>    
                    <?php endif; ?>
                    <input type="hidden" name="page_no" value="1" id="page_no"> 
                    <?php for ($i = 1; $i <= $count; $i++): ?>
                        <li><a href="<?php echo base_url(); ?>knowledge/list/<?php echo $i; ?>"  <?php if ($i == $page): ?>style="color: orange;"<?php endif; ?>><?php echo $i; ?></a></li>
                    <?php endfor; ?>
                    <?php if ($page < $count): ?>
                        <p class="pagi_img"><a href="<?php echo base_url(); ?>knowledge/list/<?php echo $page + 1; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/pagination_img1.png"></a></p>            
                    <?php endif; ?>
                </ul>
                <?php endif; ?>
            </div>
            
        </div>
    </div>
</div>



<!-- PopUp for add question to interview set  -->
<div id="editKnowledgeCenterCategory" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <form class="form-horizontal form_save" action="<?php echo base_url(); ?>knowledge/editcategory" method="post" id="knowledgeCenterCategoryForm" >
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Knowledge Center Category</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger hidden" id="knowledgeCenterCategoryAlert"></div>
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-3">Edit Category Title</label>
                        <div class="col-xs-9" id="knowledgeCenterCategory">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary edit-knowledge-category">Update</button>
                </div>
            </div>
        </form>
    </div>
</div>



<script>
    $(document).on('click', '.edit-knowledge-center-category', function () {
        var category    = $(this).data('options').category;
        var categoryId  = $(this).data('options').categoryId;        
        $('#knowledgeCenterCategoryAlert').addClass('hidden');
        //$.post(siteUrl + 'knowledge/getKnowledgeCenterCategory', {category: category, accessToken: token, categoryId: categoryId}, function (data) {
            var data = '<input type="text"  class="form-control" value="'+category+'" id="category" name="category" maxlength="30"><input type="hidden" class="form-control" value="'+categoryId+'" id="categoryId" name="categoryId">';
            $('#knowledgeCenterCategory').html(data);
        //});
    });

    $(document).on('click', '.edit-knowledge-category', function () {
        var category    = $('#category').val();
        var categoryId  = $('#categoryId').val();
        var url         = '<?php echo base_url(); ?>';
        if (!category && !categoryId) {
            $('#knowledgeCenterCategoryAlert').html('Please Enter Category.');
            $('#knowledgeCenterCategoryAlert').removeClass('hidden');
        } else {
            $.post(url + 'knowledge/checkcategory', {accessToken: token, category: category, categoryId: categoryId}, function (data) {
                //alert(data.trim());
                if (data.trim() == 1) {
                    $('#knowledgeCenterCategoryAlert').html('This category is already exist.');
                    $('#knowledgeCenterCategoryAlert').removeClass('hidden');
                    return false;
                } else {
                    $('#knowledgeCenterCategoryForm').submit();
                }
            });

        }
    });
    
    
    
    $(function () {
        var settingTableRowColor = function (tableName) {
            $('#' + tableName + ' tbody tr').removeAttr('class');
            $('#' + tableName + ' tbody tr:odd').attr('class', 'tbl_bg');
        }
        settingTableRowColor('sorting');
        $('.sort-header').on('click', function () {
            settingTableRowColor('sorting');
        });
        $('.delete-category').on('click', function () {
            var url         = '<?php echo base_url(); ?>';
            var categoryId  = $(this).attr('id');
            var accessToken = $('#accessToken').val();
            bootbox.confirm('Are you sure you want to delete?', function (result) {
                //alert(result);
                if (result) {
                    $.post(url + 'knowledge/deletecategory', {id: categoryId, accessToken: accessToken}, function (data) {
                        //bootbox.alert(data);
                        location.reload();
                    });
                }
            });

        });
    });
</script>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Knowledge Controller
 * Description : Used to handle all Knowledge center related data
 * @author Synergy
 * @createddate : Nov 3, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 21, 2017
 */
class Knowledge extends MY_Controller {

    /**
     * Responsable for inheriting parent constructor
     * Responsable for auto load the knowledge_model
     * Responsable for auto load the email, form_validation and session library
     * RResponsable for auto load fj helper
     * Check Uri Segment to display specific page role wise
     * @return void
     */
    function __construct() {
        parent::__construct();
        $this->load->model('knowledge_model');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('email');
        $this->load->helper('fj');
        $token = $this->config->item('accessToken');
        if (isset($_POST) && $_POST != NULL) {
            if ($token != $_POST['accessToken']) {
                //  echo 'You do not have pemission.';
                //exit;
            }
        }

        /**********Check any type of fraund activity*******/
        if ($this->session->userdata('logged_in')) {
            $userRealData   = $this->session->userdata['logged_in'];
            $uriStringSegment = $this->uri->uri_string();
            
            if($userRealData->role == 1) {
                $portal = 'SuperAdminPortal';
            }
            else{
                $portal = '';
            }

            if($uriStringSegment == 'knowledge/list') {
                if(!checkUserAccessForWebPage($userRealData->id, $userRealData->role, $portal, 'knowledge/list')) {
                    $this->session->sess_destroy();
                    $insertFaultData['userId'] = $userRealData->id;
                    $insertFaultData['comment'] = 'This user try to access this this page';
                    $insertFaultData['pagePath'] = 'knowledge/list';
                    $this->db->insert('fj_user_access_log', $insertFaultData);
                    redirect(base_url('users/login'));
                }
            }
        } 
        /****************End of handle fraud activity****************/
    }

    /**
     * Description : List all the knowledge center
     * Author : Synergy
     * @param int $page, array $_POST
     * @return render data into view
     */
    function listKnowledgeCenter($page) {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        //Interviews Listing Start
        $userData               = $this->session->userdata['logged_in'];
        $data['page']           = $page;
        $resp['error']          = 0;
        $serachColumn           = $this->input->post('serachColumn', true);
        $searchText             = $this->input->post('serachText', true);
        $token                  = $this->config->item('accessToken');
        $fromEmail              = $this->config->item('fromEmail');
        $itemsPerPage           = $this->config->item('itemsPerPage');
        $data['itemsPerPage']   = $itemsPerPage;
       // $createdBy = NULL;
        $createdBy = $userData->id;
        if (!$page) {
            $page = 0;
        } else {
            $page = $page - 1;
        }
        $offset = $page * $itemsPerPage;
        try {
            $result = $this->knowledge_model->listKnowledgeCenter($itemsPerPage, $offset, $serachColumn, $searchText, $createdBy);

            $completeResult                 = $this->knowledge_model->listKnowledgeCenter(NULL, NULL, NULL, NULL, $createdBy);
            $count                          = ceil(count($completeResult) / $itemsPerPage);
            $data['count']                  = $count;
            $data['totalCount']             = count($completeResult);
            $data['contentKnowledgeCenter'] = $result;
            $data['main_content']           = 'fj-knowledge-listing';
            $data['searchText']             = $searchText;
            $data['serachColumn']           = $serachColumn;
            $data['token']                  = $token;

            $this->load->view('fj-mainpage', $data);
            if (!$result) {
                //throw new Exception('Could not get list of users!');
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Description : List all the topics by specific category
     * Author : Synergy
     * @param int $id
     * @return render data into view
     */
    function getKnowledgeTopic($id) {
        $token                  = $this->config->item('accessToken');
        $data['token']          = $token;
        $data['main_content']   = 'fj-view-knowledge';
        $data['category']       = $knowledgeCenter;
        $data['topics']         = $this->knowledge_model->getTopicsByCategory($id);
        $this->load->view('fj-mainpage', $data);
    }

    /**
     * Description : Use to get detials of a specific topic
     * Author : Synergy
     * @param int $id
     * @return render data into view
     */
    function getKnowledgeTopicDetail($id) {
        $token                  = $this->config->item('accessToken');
        $data['token']          = $token;
        $data['main_content']   = 'fj-view-knowledge';
        $data['category']       = $knowledgeCenter;
        $data['topics']         = $this->knowledge_model->getKnowledgeTopicDetail($id);
        $this->load->view('fj-mainpage', $data);
    }

    /**
     * Description : Use to add new knowledge center or topic
     * Author : Synergy
     * @param null or array $_POST
     * @return render data into view
     */
    function addKnowledgeCenter() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }

        $token      = $this->config->item('accessToken');
        $userData   = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        }

        $info['main_content']   = 'fj-create-knowledge';
        $info['token']          = $token;        
        $info['fjCategory']     = getFjCategory();
        $info['fjTopics']       = getFjTopics();
        
        if (isset($_POST) && $_POST != NULL) {
            $this->form_validation->set_rules('categoryName',   'Category Name',    'trim|required');
            $this->form_validation->set_rules('categoryTopic',  'Description',      'trim'); 
            $this->form_validation->set_rules('description',    'Description',      'trim|required');            
            $categoryName   = $this->input->post('categoryName',    true);
            $description    = $this->input->post('description',     true);
            $categoryTopic  = $this->input->post('categoryTopic');
            if ($this->form_validation->run() == false) {
                $resp['error']  = validation_errors();
            }
            if (!$resp['error']) {
                $data['categoryName']   = $categoryName;
                $data['createdAt']      = date('Y-m-d h:i:s');
                $data['createdBy']      = $userId;

                try {
                    $sql    = "select * from fj_knowledgeCenterCategory where categoryName = '" . $categoryName . "' AND status='1'";
                    $query  = $this->db->query($sql);
                    if ($query->num_rows()) {
                        $result = $query->result_array();
                        $resp = $result[0]['id'];
                    }
                    else {
                        $resp = $this->knowledge_model->addKnowledgeCenterCategory($data);
                    }
                    
                    if ($resp) {
                        $item['topic']      = $categoryTopic;
                        $item['createdAt']  = date('Y-m-d h:i:s');
                        $item['createdBy']  = $userId;
                        $item['categoryId'] = $resp;
                        $item['description']= $description;
                        $this->knowledge_model->addKnowledgeCenterTopics($item);                        
                        $info['message'] = 'Successfully created!';
                        redirect(base_url('knowledge/list'));
                    }
                } catch (Exception $e) {
                    $resp['error'] = $e->getMessage();
                }
            } else {
                $info['error']      = $resp;
                $info['interview']  = $this->input->post();
                $this->load->view('fj-mainpage', $info);
            }
        } 
        else {
            $this->load->view('fj-mainpage', $info);
        }
    }

    /**
     * Description : Use to delete a specific category
     * Author : Synergy
     * @param array $_POST
     * @return string
     */
    function deleteKnowledgeCenterCategory() {
        $resp['error']  = 0;
        $token          = $this->config->item('accessToken');
        $categoryId     = $this->input->post('id', true);
        if (!$categoryId) {
            echo $error = 'Undefined user!';
            exit;
        }
        if ($error == 0) {
            $data['status'] = 3;
            $resp = $this->knowledge_model->updateKnowledgeCenterCategory($data, $categoryId, 'topic');
            if ($resp) {
                echo "Deleted successfully";
            }
        }
    }

    /**
     * Description : Use to delete a specific topic or knowledge center
     * Author : Synergy
     * @param array $_POST
     * @return string
     */
    function deleteKnowledgeCenterTopics() {
        $resp['error']  = 0;
        $token          = $this->config->item('accessToken');
        $topicId        = $this->input->post('id', true);
        if (!$topicId) {
            echo $error = 'Undefined user!';
            exit;
        }
        if ($error == 0) {
            $data['status'] = 3;
            $resp = $this->knowledge_model->updateKnowledgeCenterTopic($data, $topicId);
            if ($resp) {
                echo "Deleted successfully";
            }
        }
    }

    /**
     * Description : Use to edit a specific category
     * Author : Synergy
     * @param array $_POST
     * @return string
     */
    function editKnowledgeCenterCategory() {
        $resp['error']  = 0;
        $token          = $this->config->item('accessToken');
        $category       = $this->input->post('category', true);
        $categoryId     = $this->input->post('categoryId', true);
        if ($category=='' || $categoryId=='') {
            echo $error = 'Undefined Category!';
            exit;
        }
        if ($error == 0) {
            $data['categoryName'] = $category;
            $resp = $this->knowledge_model->updateKnowledgeCenterCategory($data, $categoryId);
            if ($resp) {
                redirect($_SERVER['HTTP_REFERER']);
            }
        }
    }

    /**
     * Description : Use to check a category if it exists or not
     * Author : Synergy
     * @param array $_POST
     * @return int
     */
    function checkKnowledgeCenterCategory() {
        $resp['error']  = 0;
        $token          = $this->config->item('accessToken');
        $category       = $this->input->post('category', true);
        $categoryId     = $this->input->post('categoryId', true);
        $check = 0;
        if ($this->knowledge_model->checkKnowledgeCenterCategory($category, $categoryId)) {
            $check = 1;
        }
        if ($category=='' || $categoryId=='') {
            $check = 1;
        }
        echo $check;
        exit;
    }

    /**
     * Description : Use to edit a specific topic
     * Author : Synergy
     * @param array $_POST
     * @return string or redirect or http referer
     */
    function editKnowledgeCenterTopic() {
        $resp['error']  = 0;
        $token          = $this->config->item('accessToken');
        $topic       = $this->input->post('topic', true);
        $topicId     = $this->input->post('topicId', true);
        if ($topic=='' || $topicId=='') {
            echo $error = 'Undefined Category Topic!';
            exit;
        }
        if ($error == 0) {
            $data['topic'] = $topic;
            $resp = $this->knowledge_model->updateKnowledgeCenterTopic($data, $topicId);
            if ($resp) {
                redirect($_SERVER['HTTP_REFERER']);
            }
        }
    }

    /**
     * Description : Use to check if a topic is exists or not
     * Author : Synergy
     * @param array $_POST
     * @return int
     */
    function checkKnowledgeCenterTopic() {
        $resp['error']  = 0;
        $token          = $this->config->item('accessToken');
        $topic          = $this->input->post('topic', true);
        $topicId        = $this->input->post('topicId', true);
        $check = 0;
        if ($topic=='' || $topicId=='') {
            $check = 1;
        }
        echo $check;
        exit;
    }

    /**
     * Description : Use to update discription of specific topic
     * Author : Synergy
     * @param array $_POST
     * @return string or redirect or http referer
     */
    function editKnowledgeCenterDescription() {
        $resp['error']  = 0;
        $token          = $this->config->item('accessToken');
        $description    = $this->input->post('description', true);
        $topicId        = $this->input->post('topicId', true);
        if ($topicId=='') {
            echo $error = 'Undefined Category Topic!';
            exit;
        }
        if ($error == 0) {
            $data['description'] = $description;
            $resp = $this->knowledge_model->updateKnowledgeCenterTopic($data, $topicId);
            if ($resp) {
                redirect($_SERVER['HTTP_REFERER']);
            }
        }
    }
}
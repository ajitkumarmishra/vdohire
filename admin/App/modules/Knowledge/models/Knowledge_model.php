<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Knowledge Model
 * Description : Hanldle all the CRUD operations for Knowledge model
 * @author Synergy
 * @createddate : Nov 23, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 21, 2017
 */

class Knowledge_model extends CI_Model {
    
    /**
     * Declaring static variables
     */
    static $tableCategory  = "fj_knowledgeCenterCategory";
    static $tableTopics    = "fj_knowledgeCenterTopics";
    
    /**
     * Responsable for inheriting parent constructor
     * Responsable for auto load the session and pagination library
     * @return void
     */
    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('pagination');
    }
    
    /**
     * Function Name : listKnowledgeCenter function
     * Description : Use to get list of knowledge or topic
     * @param int $limit, int $offset
     * @return array of Knowledge Center data.
     */
    function listKnowledgeCenter($limit, $offset, $serachColumn = NULL, $searchText = NULL, $createdBy = NULL) {
        
        try {
            $currentDate    = date('Y-m-d h:i:s');
            $sql            = "SELECT * FROM ".Knowledge_model::$tableCategory." WHERE status<>'3' ORDER BY categoryName";

            if (isset($offset)) {
                $sql .= " limit " . $offset . ", " . $limit . "";
            }
            $query  = $this->db->query($sql);
            $result = $query->result();
            if (!$result) {
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Function Name : getKnowledgeTopic function
     * Description : Use to get a specific Knowledge or topic Center detail.
     * @param int $id.
     * @return array of Knowledge Center data.
     */
    function getKnowledgeTopic($id) {
        try {            
            $this->db->select('*');
            $this->db->from(Knowledge_model::$tableCategory);
            $this->db->where('id', $id);
            $query = $this->db->get();
            $result = $query->row_array();
            if (count($result) == 0) {
                //throw new Exception('Unable to fetch interview data.');
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Function Name : getTopicsByCategory function
     * Description : Use to get all the topics or knowledge of a particular category
     * @param int $id.
     * @return array of topic data.
     */
    function getTopicsByCategory($id) {
        try {
            $sql    = "SELECT KCC.categoryName, KCT.id, KCT.topic, KCT.description, KCT.status FROM ".Knowledge_model::$tableCategory." KCC "
                    . "LEFT JOIN ".Knowledge_model::$tableTopics." KCT "
                    . "ON KCC.id=KCT.categoryId "
                    . "WHERE KCT.categoryId = '" . $id . "' AND KCT.status<>'3' AND KCC.status<>'3'";
            $query  = $this->db->query($sql);
            $result = $query->result_array();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    
    /**
     * Function Name : getKnowledgeTopicDetail function
     * Description : Use to get details of a particular topic
     * @param int $id.
     * @return array the knowledge or topic data.
     */
    function getKnowledgeTopicDetail($id) {
        try {
            $sql    = "SELECT KCC.categoryName, KCT.id, KCT.topic, KCT.description, KCT.status FROM ".Knowledge_model::$tableTopics." KCT "
                    . "LEFT JOIN ".Knowledge_model::$tableCategory." KCC "
                    . "ON KCC.id=KCT.categoryId "
                    . "WHERE KCT.id = '" . $id . "' AND KCT.status<>'3' AND KCC.status<>'3'";
            $query  = $this->db->query($sql);
            $result = $query->result_array();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    
    /**
     * Function Name : addKnowledgeCenterCategory function
     * Description : Use to add category for knowledge center or topic
     * @param int $id.
     * @return int or boolean
     */
    function addKnowledgeCenterCategory($id) {
        $resp = $this->db->insert(Knowledge_model::$tableCategory, $id);
        $categoryId = $this->db->insert_id();
        if ($resp) {
            return $categoryId;
        } else {
            return false;
        }
    }
    
    
    /**
     * Function Name : addKnowledgeCenterTopics function
     * Description : Use to add new knowledge center or topic
     * @params array $data
     * @return int or boolean
     */
    function addKnowledgeCenterTopics($data) {
        $resp = $this->db->insert(Knowledge_model::$tableTopics, $data);
        $topicId = $this->db->insert_id();
        if ($resp) {
            return $topicId;
        } else {
            return false;
        }
    }

    /**
     * Function Name : updateKnowledgeCenterCategory function
     * Description : Use to update category for knowledge center or topic
     * @param array $data, int $id, string $topic
     * @return boolean or string
     */
    function updateKnowledgeCenterCategory($data, $id, $topic='') {
        try {
            $this->db->where('id', $id);
            $resp = $this->db->update(Knowledge_model::$tableCategory, $data);
            if (!$resp) {
                throw new Exception('Unable to update data.');
                return false;
            }
            if($topic=='topic'){
                $this->db->where('categoryId', $id);
                $resptopic = $this->db->update(Knowledge_model::$tableTopics, $data);
                if (!$resptopic) {
                    throw new Exception('Unable to update data.');
                    return false;
                }
            }
            return $id;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Function Name : updateKnowledgeCenterTopic function
     * Description : Use to update specific topic or knowledge center
     * @param array $data, int $id
     * @return boolean or int or string
     */
    function updateKnowledgeCenterTopic($data, $id) {
        try {
            $this->db->where('id', $id);
            $resp = $this->db->update(Knowledge_model::$tableTopics, $data);
            if (!$resp) {
                throw new Exception('Unable to update user data.');
                return false;
            }
            return $id;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Function Name : checkKnowledgeCenterCategory function
     * Description : Use to check if a category exists or not
     * @param string category, int $categoryId
     * @return array of data
     */
    function checkKnowledgeCenterCategory($category, $categoryId) {
        $CI = &get_instance();
        $sql = "select * from ".Knowledge_model::$tableCategory." where categoryName = '" . $category . "' AND id<>'" . $categoryId . "'";
        $query = $CI->db->query($sql);
        return $query->num_rows();
    }
}
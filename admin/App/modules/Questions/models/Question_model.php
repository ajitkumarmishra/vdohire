<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Question Model
 * Description : Handle all the CRUD operation for Questions
 * @author Synergy
 * @createddate : Nov 22, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 21, 2017
 */

class Question_model extends CI_Model {

    /**
     * Declaring variables
     */
    var $table = "nc_questions";
    var $asses_table = "nc_assesments";
    var $indus_table = "nc_industry";
    var $inter_table = "nc_interview";
    var $sample_table = "nc_sample_interview";

    /**
     * Responsable for inheriting parent constructor
     * Responsable for auto load the session library
     * @return void
     */
    function __construct() {
        parent::__construct();
        $this->load->library('session');
    }

    /**
     * Description : Use to get all the assessments
     * Author : Synergy
     * @param none
     * @return array or assessments data
     */
    function read() {
        $query = $this->db->get($this->asses_table);
        return $query->result();
    }

    /**
     * Description : Use to get limited assessments
     * Author : Synergy
     * @param int $showPerpage, int $offset
     * @return array or assessments data
     */
    function getList($showPerpage, $offset) {
        $this->db->limit($showPerpage, $offset);
        $this->db->from($this->asses_table);
        $result = $this->db->get();
        return $result->result();
    }

    /**
     * Description : Use to get number of assessments
     * Author : Synergy
     * @param none
     * @return int no of assessments
     */
    function count_all() {
        $query = $this->db->get($this->asses_table);
        return $query->num_rows();
    }

    /**
     * Description : Use to all assessments in which a question exists
     * Author : Synergy
     * @param int $id
     * @return int no of assessments
     */
    public function getQuestion($id) {
        $this->db->where('question_id', $id);
        $query = $this->db->get($this->asses_table);
        return $query->row();
    }

    /**
     * Description : Use to create question
     * Author : Synergy
     * @param array $_POST
     * @return boolean
     */
    public function create() {
        $file = $this->upload_question();
        $this->load->library('getid3/getid3');
        $quest_ans = $this->input->post('question_ans');
        $data = array(
            'question_industry' => $this->input->post('question_industry'),
            'question_function' => $this->input->post('question_function'),
            'question_type' => $this->input->post('question_type'),
            'experience' => $this->input->post('experience'),
            'question_title' => $this->input->post('question_title'),
            'question_file' => $file['file_name'],
            'question_ans_duration' => $this->input->post('question_ans_duration'),
            'question_ans' => $this->input->post('question_ans'),
            'question_ans_one' => $this->input->post('question_ans_one'),
            'question_ans_two' => $this->input->post('question_ans_two'),
            'question_ans_three' => $this->input->post('question_ans_three'),
            'question_ans_four' => $this->input->post('question_ans_four'),
            'question_status' => '1',
        );
        $check_answer = $data['question_ans'];
        if ($check_answer != 'on' && $check_answer != '') {
            if ($file['file_name'] != '') {
                $path = IMAGESPATH . 'questions/assessments/' . $file['file_name'];
                $dataArray = $this->getid3->analyze($path);
                $value = $dataArray['playtime_string'];
                sscanf($value, "%d:%d", $minutes, $seconds);
                $ms = $seconds * 1000 + $minutes * 60 * 1000;
                $data['question_duration'] = $ms;
            } $this->db->set('question_creation_date', 'NOW()', false);
            $this->db->set('question_modification_date', 'NOW()', false);
            $query = $this->db->insert($this->asses_table, $data);
            if ($query) {
                return true;
            }
        }
    }

    /**
     * Description : Use to update a specific question
     * Author : Synergy
     * @param int $id, array $_POST
     * @return boolean
     */
    public function update($id) {
        $file = $this->upload_question();
        $this->load->library('getid3/getid3');
        $data = array(
            'question_industry' => $this->input->post('question_industry'),
            'question_function' => $this->input->post('question_function'),
            'question_type' => $this->input->post('question_type'),
            'experience' => $this->input->post('experience'),
            'question_title' => $this->input->post('question_title'),
            'question_ans_duration' => $this->input->post('question_ans_duration'),
            'question_ans' => $this->input->post('question_ans'),
            'question_ans_one' => $this->input->post('question_ans_one'),
            'question_ans_two' => $this->input->post('question_ans_two'),
            'question_ans_three' => $this->input->post('question_ans_three'),
            'question_ans_four' => $this->input->post('question_ans_four'),
            'question_status' => '1',
        );
        if ($file['file_name'] != '') {
            $data['question_file'] = $file['file_name'];
            $path = IMAGESPATH . 'questions/assessments/' . $file['file_name'];
            $dataArray = $this->getid3->analyze($path);
            $value = $dataArray['playtime_string'];
            sscanf($value, "%d:%d", $minutes, $seconds);
            $ms = $seconds * 1000 + $minutes * 60 * 1000;
            $data['question_duration'] = $ms;
        }
        $this->db->where('question_id', $id);
        $query = $this->db->update($this->asses_table, $data);
        if ($query) {
            return true;
        }
    }

    /**
     * Description : Use to upload question file
     * Author : Synergy
     * @param array $_POST
     * @return array of file uploaded data
     */
    public function upload_question() {
        $config = array();
        ini_set('upload_max_filesize', '200M');
        ini_set('post_max_size', '200M');
        ini_set('max_input_time', 3000);
        ini_set('max_execution_time', 3000);
        $config['upload_path'] = IMAGESPATH . 'questions/assessments/';
        $config['allowed_types'] = '*'; //'mp3|wav';
        $config['file_name'] = md5(uniqid(rand(), true));
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if ($this->upload->do_upload('question_file')) {
            $info = $this->upload->data();
            return $info;
        }
    }

    /**
     * Description : Use to delete an existing question file
     * Author : Synergy
     * @param array $file, int $id
     * @return boolean
     */
    public function deleteexistingfile($file, $id) {
        unlink(IMAGESPATH . 'questions/' . $file);
        $data['question_file'] = '';
        $this->db->where('question_id', $id);
        $this->db->update($this->asses_table, $data);
    }

    /**
     * Description : Use to update status active or deactive of questions or delete questions
     * Author : Synergy
     * @param array $_POST
     * @return boolean
     */
    function updateStatus() {
        $arr_ids = $this->input->post('arr_ids');
        if ($this->input->post('Delete') != null) {
            if (!empty($arr_ids)) {
                $this->db->where_in('question_id', $arr_ids);
                $this->db->delete($this->asses_table);
            }
        } else {
            if ($this->input->post('Deactivate') != null) {
                $status = "0";
            } else if ($this->input->post('Activate') != null) {
                $status = "1";
            }
            $data = array(
                'question_status' => $status
            );
            if (!empty($arr_ids)) {
                $this->db->where_in('question_id', $arr_ids);
                $this->db->update($this->asses_table, $data);
            }
        }
    }

    /**
     * Description : Use to count all interview
     * Author : Synergy
     * @param null
     * @return int
     */
    function interview_count_all() {
        $query = $this->db->get($this->table);
        return $query->num_rows();
    }

    /**
     * Description : Use to get all limited interview list
     * Author : Synergy
     * @param int $showPerpage, int $offset
     * @return array or data
     */
    function getInterviewList($showPerpage, $offset) {
        $this->db->limit($showPerpage, $offset);
        $this->db->from($this->table);
        $result = $this->db->get();
        return $result->result();
    }

    /**
     * Description : Use to update status active or deactive of questions or delete questions
     * Author : Synergy
     * @param array $_POST
     * @return boolean
     */
    function interviewUpdateStatus() {
        $arr_ids = $this->input->post('arr_ids');
        if ($this->input->post('Delete') != null) {
            if (!empty($arr_ids)) {
                $this->db->where_in('question_id', $arr_ids);
                $this->db->delete($this->table);
            }
        } else {
            if ($this->input->post('Deactivate') != null) {
                $status = "0";
            } else if ($this->input->post('Activate') != null) {
                $status = "1";
            }
            $data = array(
                'question_status' => $status
            );
            if (!empty($arr_ids)) {
                $this->db->where_in('question_id', $arr_ids);
                $this->db->update($this->table, $data);
            }
        }
    }

    /**
     * Description : Use to create interivew
     * Author : Synergy
     * @param array $_POST
     * @return boolean
     */
    public function interview_create() {
        $file = $this->interview_upload_question();
        $this->load->library('getid3/getid3');
        $data = array(
            'question_industry' => $this->input->post('question_industry'),
            'question_function' => $this->input->post('question_function'),
            'question_type' => $this->input->post('question_type'),
            'experience' => $this->input->post('experience'),
            'question_title' => $this->input->post('question_title'),
            'question_file' => $file['file_name'],
            'question_duration' => $dataArray['duration'],
            'question_ans' => $this->input->post('question_ans'),
            'question_ans_duration' => $this->input->post('question_ans_duration'),
            'question_status' => '1',
        );
        
        if ($file['file_name'] != '') {
            $path = IMAGESPATH . 'questions/interview/' . $file['file_name'];
            $dataArray = $this->getid3->analyze($path);
            $value = $dataArray['playtime_string'];
            sscanf($value, "%d:%d", $minutes, $seconds);
            $ms = $seconds * 1000 + $minutes * 60 * 1000;
            $data['question_duration'] = $ms;
        }
        $this->db->set('question_creation_date', 'NOW()', false);
        $this->db->set('question_modification_date', 'NOW()', false);
        $query = $this->db->insert($this->table, $data);
        if ($query) {
            return true;
        }
    }

    /**
     * Description : Use to update an specified interivew
     * Author : Synergy
     * @param int $id, array $_POST
     * @return boolean
     */
    public function interview_update($id) {
        $file = $this->interview_upload_question();
        $this->load->library('getid3/getid3');
        $data = array(
            'question_industry' => $this->input->post('question_industry'),
            'question_function' => $this->input->post('question_function'),
            'question_type' => $this->input->post('question_type'),
            'experience' => $this->input->post('experience'),
            'question_title' => $this->input->post('question_title'),
            'question_ans' => $this->input->post('question_ans'),
            'question_ans_duration' => $this->input->post('question_ans_duration'),
            'question_status' => '1',
        );
        if ($file['file_name'] != '') {
            $data['question_file'] = $file['file_name'];
            $path = IMAGESPATH . 'questions/interview/' . $file['file_name'];
            $dataArray = $this->getid3->analyze($path);
            $value = $dataArray['playtime_string'];
            sscanf($value, "%d:%d", $minutes, $seconds);
            $ms = $seconds * 1000 + $minutes * 60 * 1000;
            $data['question_duration'] = $ms;
        }
        $this->db->where('question_id', $id);
        $query = $this->db->update($this->table, $data);
        if ($query) {
            return true;
        }
    }

    /**
     * Description : Use to upload interview questions file
     * Author : Synergy
     * @param int $id, array $_POST
     * @return array of file
     */
    public function interview_upload_question() {
        $config = array();
        ini_set('upload_max_filesize', '200M');
        ini_set('post_max_size', '200M');
        ini_set('max_input_time', 3000);
        ini_set('max_execution_time', 3000);
        $config['upload_path'] = IMAGESPATH . 'questions/interview/';
        $config['allowed_types'] = '*'; //'mp3|wav';
        $config['file_name'] = md5(uniqid(rand(), true));
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if ($this->upload->do_upload('question_file')) {
            $info = $this->upload->data();
            return $info;
        }
    }

    /**
     * Description : Use to get details of an specific interview question
     * Author : Synergy
     * @param int $id
     * @return array of data
     */
    public function getinterviewQuestion($id) {
        $this->db->where('question_id', $id);
        $query = $this->db->get($this->table);
        return $query->row();
    }

    /**
     * Description : Use to delete an existing interview questions file
     * Author : Synergy
     * @param int $id
     * @return boolean
     */
    public function deleteexistinginterviewfile($file, $id) {
        unlink(IMAGESPATH . 'questions/interview/' . $file);
        $data['question_file'] = '';
        $this->db->where('question_id', $id);
        $this->db->update($this->table, $data);
    }

    /**
     * Description : Use to get all list of industries
     * Author : Synergy
     * @param none
     * @return array of data
     */
    function getIndustryList() {
        $conditions = array(
            'is_active' => '1',
            'indus_id' => '0'
        );
        $this->db->where($conditions);
        $this->db->from($this->indus_table);
        $result = $this->db->get();
        return $result->result();
    }

    /**
     * Description : Use to get all list of functions
     * Author : Synergy
     * @param int $id
     * @return array of data
     */
    function getFuncList($id) {
        $this->db->where('indus_id', $id);
        $query = $this->db->get($this->indus_table);
        return $query->result();
    }

    /**
     * Description : Use to get all list of interviews
     * Author : Synergy
     * @param none
     * @return array of data
     */
    function cronJob() {
        $query = $this->db->get($this->inter_table);
        return $query->result();
    }

    /**
     * Description : Use to update status of interviews or delete interviews
     * Author : Synergy
     * @param array $_POST
     * @return boolean
     */
    function sampleinterviewUpdateStatus() {
        $arr_ids = $this->input->post('arr_ids');
        if ($this->input->post('Delete') != null) {
            if (!empty($arr_ids)) {
                $this->db->where_in('question_id', $arr_ids);
                $this->db->delete($this->sample_table);
            }
        } else {
            if ($this->input->post('Deactivate') != null) {
                $status = "0";
            } else if ($this->input->post('Activate') != null) {
                $status = "1";
            }
            $data = array(
                'question_status' => $status
            );
            if (!empty($arr_ids)) {
                $this->db->where_in('question_id', $arr_ids);
                $this->db->update($this->sample_table, $data);
            }
        }
    }

    /**
     * Description : Use to count all sample interviews
     * Author : Synergy
     * @param none
     * @return int of number of interviews
     */
    function sample_interview_count_all() {
        $query = $this->db->get($this->sample_table);
        return $query->num_rows();
    }

    /**
     * Description : Use to get limited sample interview list
     * Author : Synergy
     * @param int $showPerpage, int $offset
     * @return array of interviews
     */
    function getSampleInterviewList($showPerpage, $offset) {
        $this->db->limit($showPerpage, $offset);
        $this->db->from($this->sample_table);
        $result = $this->db->get();
        return $result->result();
    }

    /**
     * Description : Use to create sample interview
     * Author : Synergy
     * @param array $_POST
     * @return boolean
     */
    public function sample_interview_create() {
        $file = $this->sample_interview_upload_question();
        $data = array(
            'question_type' => $this->input->post('question_type'),
            'question_title' => $this->input->post('question_title'),
            'question_file' => $file['file_name'],
            'question_status' => '1',
        );
        if ($file['file_name'] != '') {
            $path = IMAGESPATH . 'questions/sample/' . $file['file_name'];
        }
        $this->db->set('question_creation_date', 'NOW()', false);
        $this->db->set('question_modification_date', 'NOW()', false);
        $query = $this->db->insert($this->sample_table, $data);
        if ($query) {
            return true;
        }
    }

    /**
     * Description : Use to upload sample interview file
     * Author : Synergy
     * @param array $_POST
     * @return array of file data
     */
    public function sample_interview_upload_question() {
        $config = array();
        ini_set('upload_max_filesize', '200M');
        ini_set('post_max_size', '200M');
        ini_set('max_input_time', 3000);
        ini_set('max_execution_time', 3000);
        $config['upload_path'] = IMAGESPATH . 'questions/sample/';
        $config['allowed_types'] = '*'; //'mp3|wav';
        $config['file_name'] = md5(uniqid(rand(), true));
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if ($this->upload->do_upload('question_file')) {
            $info = $this->upload->data();
            return $info;
        }
    }

    /**
     * Description : Use to get details of an sample interview details
     * Author : Synergy
     * @param int $id
     * @return array of data
     */
    public function getSampleQuestion($id) {
        $this->db->where('question_id', $id);
        $query = $this->db->get($this->sample_table);
        return $query->row();
    }

    /**
     * Description : Use to delete an existing sample file
     * Author : Synergy
     * @param string $file, int $id
     * @return boolean
     */
    public function deleteexistingsamplefile($file, $id) {
        unlink(IMAGESPATH . 'questions/sample/' . $file);
        $data['question_file'] = '';
        $this->db->where('question_id', $id);
        $this->db->update($this->sample_table, $data);
    }

    /**
     * Description : Use to update a specific sample question
     * Author : Synergy
     * @param int $id, array $_POST
     * @return boolean
     */
    public function sample_update($id) {
        $file = $this->sample_interview_upload_question();
        $data = array(
            'question_type' => $this->input->post('question_type'),
            'question_title' => $this->input->post('question_title'),
            'question_status' => '1',
        );
        if ($file['file_name'] != '') {
            $data['question_file'] = $file['file_name'];
        }
        $this->db->where('question_id', $id);
        $query = $this->db->update($this->sample_table, $data);
        if ($query) {
            return true;
        }
    }
}
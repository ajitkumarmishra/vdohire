<div class="row">
    <div class="col-sm-12 col-md-12">
        <?php if (@$error): ?>
            <div class="alert">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <?php echo $error; ?>
            </div>
        <?php endif; ?>
        <div class="well">
            <h3>Edit Sample</h3>
            <form class="form-horizontal" method="post" action="" id="sampleFormEdit" enctype="multipart/form-data">
                
                <div class="control-group">
                    <label class="control-label" for="question_type">Question Type</label>
                    <div class="controls">
                        <select class="form-control" name="question_type" id="question_type">
                            <option value="">Select</option>
                            <option value="Audio" <?php echo $ques->question_type == 'Audio' ? 'selected="selected"' : ''; ?>>Audio</option>
                            <option value="Video" <?php echo $ques->question_type == 'Video' ? 'selected="selected"' : ''; ?>>Video</option>
                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="question_title">Question Title:</label>
                    <div class="controls">
                        <textarea class="form-control" rows="2" id="question_title" name="question_title"><?php echo $ques->question_title; ?></textarea>
                    </div>
                </div>
                <?php if ($ques->question_type == 'Audio') { ?>
                    <?php if ($ques->question_file != '' && $ques->question_file != null) : ?>
                        <div class="control-group">
                            <label class="control-label" for="question_file">Sample File Listen</label>
                            <div class="controls">
                                <audio src="<?php echo base_url() . 'uploads/questions/sample/' . $ques->question_file; ?>" controls></audio>
                                <input type="checkbox" id="deleteSamplefile" name="deleteSamplefile" value="1" />Delete
                            </div>
                        </div>
                    <?php endif; ?>
                <?php } else { ?>
                    <?php if ($ques->question_file != '' && $ques->question_file != null) : ?>
                        <div class="control-group">
                            <label class="control-label" for="question_file">Sample File</label>
                            <div class="controls">
                                
                                <video width="400" controls>
                                    <source src="<?php echo base_url() . 'uploads/questions/sample/' . $ques->question_file; ?>" type="video/mp4">
                                    <source src="mov_bbb.ogg" type="video/ogg">
                                    Your browser does not support HTML5 video.
                                </video>

                                <input type="checkbox" id="deleteSamplefile" name="deleteSamplefile" value="1" />Delete
                            </div>
                        </div>

                    <?php endif; ?>
                <?php } ?>


                <div class="control-group">
                    <label class="control-label" for="question_file">Sample File</label>
                    <div class="controls">
                        <input type="file" name="question_file" id="question_file">
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <button type="submit" class="form-control btn btn-primary button-submit">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

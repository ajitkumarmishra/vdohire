<div class="row">
    <div class="col-sm-12 col-md-12">
        <?php if (@$error): ?>
            <div class="alert">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <?php echo $error; ?>
            </div>
        <?php endif; ?>
        <div class="well">
            <h3>Create New Question</h3>
            <form class="form-horizontal" method="post" action="" id="quesFormEdit" enctype="multipart/form-data">
                <div class="control-group">
                    <label class="control-label" for="question_industry">Industry</label>
                    <div class="controls">
                        <select class="form-control" name="question_industry" id="question_industry">
                            <option value="">Select</option>
                            <?php foreach ($indus as $indu) { ?>
                                <option value="<?php echo $indu->id; ?>" <?php echo $indu->id == $ques->question_industry ? 'selected="selected"' : ''; ?>><?php echo $indu->name; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="question_function">Function</label>
                    <div class="controls">
                        <?php $func = functions_list($ques->question_industry); ?>
                        <select class="form-control" name="question_function" id="question_function">
                            <option value="">Select</option>
                            <?php foreach ($func as $func) { ?>
                                <option value="<?php echo $func->id; ?>" <?php echo $func->id == $ques->question_function ? 'selected="selected"' : ''; ?>><?php echo $func->name; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="question_type">Question Type</label>
                    <div class="controls">
                        <select class="form-control" name="question_type" id="question_type">
                            <option value="">Select</option>
                            <option value="Audio" <?php echo $ques->question_type == 'Audio' ? 'selected="selected"' : ''; ?>>Audio</option>
                            <option value="Video" <?php echo $ques->question_type == 'Video' ? 'selected="selected"' : ''; ?>>Video</option>
                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="experience">Experience</label>
                    <div class="controls">
                        <select class="form-control" name="experience" id="experience">
                            <option value="">Select</option>
                            <option value="0" <?php echo $ques->experience == '0' ? 'selected="selected"' : ''; ?>>Fresher</option>
                            <?php /*<option value="1" <?php echo $ques->experience == '1' ? 'selected="selected"' : ''; ?>>1 year</option>
                            <option value="2" <?php echo $ques->experience == '2' ? 'selected="selected"' : ''; ?>>2 year</option>*/?>
                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="question_title">Question Title:</label>
                    <div class="controls">
                        <textarea class="form-control" rows="2" id="question_title" name="question_title"><?php echo $ques->question_title; ?></textarea>
                    </div>
                </div>

                <?php if ($ques->question_type == 'Audio') { ?>
                    <?php if ($ques->question_file != '' && $ques->question_file != null) : ?>
                        <div class="control-group">
                            <label class="control-label" for="question_file">Question File Listen</label>
                            <div class="controls">
                                <audio src="<?php echo base_url() . 'uploads/questions/assessments/' . $ques->question_file; ?>" controls></audio>
                                <input type="checkbox" id="deleteInterviewfile" name="deleteQuesfile" value="1" />Delete
                            </div>
                        </div>
                    <?php endif; ?>
                <?php } else { ?>
                    <?php if ($ques->question_file != '' && $ques->question_file != null) : ?>
                        <div class="control-group">
                            <label class="control-label" for="question_file">Question File</label>
                            <div class="controls">

                                <video width="400" controls>
                                    <source src="<?php echo base_url() . 'uploads/questions/assessments/' . $ques->question_file; ?>" type="video/mp4">
                                    <source src="mov_bbb.ogg" type="video/ogg">
                                    Your browser does not support HTML5 video.
                                </video>

                                <input type="checkbox" id="deleteInterviewfile" name="deleteQuesfile" value="1" />Delete
                            </div>
                        </div>

                    <?php endif; ?>
                <?php } ?>


                <div class="control-group">
                    <label class="control-label" for="question_file">Question Audio</label>
                    <div class="controls">
                        <input type="file" name="question_file" id="question_file">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="question_ans_duration">Answer Duration</label>
                    <div class="controls">
                        <input type="text" class="form-control" id="question_ans_duration" value="<?php echo $ques->question_ans_duration; ?>" name="question_ans_duration">
                    </div>
                </div>
                <!--<div class="control-group">
                    <label class="control-label" for="question_ans">Question Answer</label>
                    <div class="controls">
                        <input type="text" class="form-control" id="question_ans" value="<?php echo $ques->question_ans; ?>" name="question_ans">
                    </div>
                </div>-->
                <?php
                $answer = $ques->question_ans;
                $answer1 = $ques->question_ans_one;
                $answer2 = $ques->question_ans_two;
                $answer3 = $ques->question_ans_three;
                $answer4 = $ques->question_ans_four;
                ?>
                <div class="control-group">
                    <label class="control-label" for="question_ans_one">Question Answer One</label>
                    <div class="controls">
                        <input type="text" class="form-control" id="question_ans_one" value="<?php echo $ques->question_ans_one; ?>" name="question_ans_one">
                        <?php
                        if ($answer == $answer1) {
                            ?>
                            <input type="radio" checked name="question_ans"  id="get_answer1">
                        <?php } else { ?>
                            <input type="radio" name="question_ans"  id="get_answer1" value="<?php echo $ques->question_ans_one; ?>">
                        <?php } ?>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="question_ans_two">Question Answer Two</label>
                    <div class="controls">
                        <input type="text" class="form-control" id="question_ans_two" value="<?php echo $ques->question_ans_two; ?>" name="question_ans_two">
                        <?php
                        if ($answer == $answer2) {
                            ?>
                            <input type="radio" checked name="question_ans"  id="get_answer2" value="<?php echo $ques->question_ans_two; ?>">
                        <?php } else { ?>
                            <input type="radio" name="question_ans"  id="get_answer2" value="<?php echo $ques->question_ans_two; ?>">
                        <?php } ?>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="question_ans">Question Answer Three</label>
                    <div class="controls">
                        <input type="text" class="form-control" id="question_ans_three" value="<?php echo $ques->question_ans_three; ?>" name="question_ans_three">
<?php
if ($answer == $answer3) {
    ?>
                            <input type="radio" checked name="question_ans"  id="get_answer3" value="<?php echo $ques->question_ans_three; ?>">
                        <?php
                        } else {
                            ?>
                            <input type="radio" name="question_ans"  id="get_answer3" value="<?php echo $ques->question_ans_three; ?>">
                        <?php }
                        ?>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="question_ans_four">Question Answer Four</label>
                    <div class="controls">
                        <input type="text" class="form-control" id="question_ans_four" value="<?php echo $ques->question_ans_four; ?>" name="question_ans_four">
<?php
if ($answer == $answer4) {
    ?>
                            <input type="radio" checked name="question_ans"  id="get_answer4" value="<?php echo $ques->question_ans_four; ?>">
                        <?php
                        } else {
                            ?>
                            <input type="radio"  name="question_ans"  id="get_answer4" value="<?php echo $ques->question_ans_four; ?>">
                        <?php } ?>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <button type="submit" class="form-control btn btn-primary button-submit">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <?php if (@$error): ?>
            <div class="alert">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <?php echo $error; ?>
            </div>
        <?php endif; ?>
		 <?php if (@$error_file): ?>            <div class="alert">                <button type="button" class="close" data-dismiss="alert">×</button>                <span style="color:#ee0000; font-size:15px;"><?php echo $error_file; ?></span>            </div>        <?php endif; ?>
		
        <div class="well">
            <h3>Create New Question</h3>
            <form class="form-horizontal" method="post" action="" id="quesFormCreate" enctype="multipart/form-data">
                <div class="control-group">
                    <label class="control-label" for="question_industry">Industry</label>
                    <div class="controls">
                        <select class="form-control" name="question_industry" id="question_industry">
                            <option value="">Select</option>
                            <?php foreach ($indus as $indu) { ?>
                                <option value="<?php echo $indu->id; ?>"><?php echo $indu->name; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="question_function">Function</label>
                    <div class="controls">
                        <select class="form-control" name="question_function" id="question_function">
                            <option value="">Select Function</option>
                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="question_type">Question Type</label>
                    <div class="controls">
                        <select class="form-control" name="question_type" id="question_type">
                            <option value="">Select</option>
                            <option value="Audio">Audio</option>
                            <option value="Video">Video</option>
                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="experience">Experience</label>
                    <div class="controls">
                        <select class="form-control" name="experience" id="experience">
                            <option value="">Select</option>
                            <option value="0">Fresher</option>
                            <?php /*<option value="1">1 year</option>
                            <option value="2">2 year</option>*/?>
                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="question_title">Question Title:</label>
                    <div class="controls">
                        <textarea class="form-control" rows="2" id="question_title" name="question_title"><?php echo set_value('question_title'); ?></textarea>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="question_file">Question Audio</label>
                    <div class="controls">
                        <input type="file" name="question_file" id="question_file">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="question_ans_duration">Answer Duration</label>
                    <div class="controls">
                        <input type="text" class="form-control" id="question_ans_duration" value="<?php echo set_value('question_ans_duration'); ?>" name="question_ans_duration">
                    </div>
                </div>
               <!--- <div class="control-group">
                    <label class="control-label" for="question_ans">Question Answer</label>
                    <div class="controls">
                        <input type="text" class="form-control" id="question_ans" value="<?php //echo set_value('question_ans'); ?>" name="question_ans">
                    </div>
                </div>-->
                <div class="control-group">
                    <label class="control-label" for="question_ans_one">Question Answer One</label>
                    <div class="controls">
                        <input type="text" class="form-control" id="question_ans_one" ng-model="name" value="<?php echo set_value('question_ans_one'); ?>" name="question_ans_one">
                        <input type="radio" name="question_ans"  id="get_answer1">	
				   </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="question_ans_two">Question Answer Two</label>
                    <div class="controls">
                        <input type="text" class="form-control" id="question_ans_two" value="<?php echo set_value('question_ans_two'); ?>" name="question_ans_two">
                    <input type="radio" name="question_ans" id="get_answer2">
					
					</div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="question_ans">Question Answer Three</label>
                    <div class="controls">
                        <input type="text" class="form-control" id="question_ans_three" value="<?php echo set_value('question_ans_three'); ?>" name="question_ans_three">
                   <input type="radio" name="question_ans"  id="get_answer3">
				 
				   
					</div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="question_ans_four">Question Answer Four</label>
                    <div class="controls">
                        <input type="text" class="form-control" id="question_ans_four" value="<?php echo set_value('question_ans_four'); ?>" name="question_ans_four">
                    <input type="radio" name="question_ans"  id="get_answer4">
					
					</div>
					
                </div>				
                <div class="control-group">
                    <div class="controls">
                        <button type="submit" class="form-control btn btn-primary button-submit">Submit</button>
                    </div>
                </div><div class="clear"></div>
            </form>
        </div>
    </div>
</div>

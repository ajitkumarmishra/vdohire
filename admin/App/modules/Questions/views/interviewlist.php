<?php if ($this->session->flashdata('message')) { ?>
    <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Success!</strong> <?php echo $this->session->flashdata('message'); ?>
    </div>
<?php } ?>
<?php if ($this->session->flashdata('error')) { ?>
    <div class="alert alert-danger">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Danger!</strong> <?php echo $this->session->flashdata('error'); ?>
    </div>
<?php } ?>
<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <h3> Interview Questions</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <a href="<?php echo base_url('interview/add'); ?>" class="btn btn-primary pull-right">Add New Question</a>
            </div>
        </div>
        <form method="post" name="form1" id="form1" action="">
            <table class="table">
                <thead>
                    <tr>
                        <th><input name="check_all" type="checkbox" id="check_all" value="1" onclick="checkall(this.form)" /></th>
                        <th>Title</th>
                        <th>Industry</th>
                        <th>Question Type</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                </thead>
                <tbod>
                    <?php
                    $indus_list = industry_list();
                    foreach ($questions as $list):
                        ?>
                        <tr class="<?php echo ($list->question_status == '1' ? 'success' : 'warning'); ?>">
                            <td>
                                <input name="arr_ids[]" type="checkbox" id="arr_ids[]" value="<?php echo $list->question_id; ?>" />
                            </td>
                            <td><?php echo $list->question_title; ?></td>
                            <td><?php echo (array_key_exists($list->question_industry, $indus_list)) ? $indus_list[$list->question_industry] : ''; ?></td>
                            <td><?php echo $list->question_type; ?></td>
                            <td><?php echo ($list->question_status == '1' ? 'Active' : 'Inactive'); ?></td>
                            <td><a href="<?php echo base_url('interview/edit') . '/' . $list->question_id; ?>"><span class="glyphicon glyphicon-pencil pull-right"></a></td>
                        </tr>
                    <?php endforeach; ?>
                </tbod>
            </table>
            <div class="col-sm-12 col-md-12">
                <div class="col-sm-4 col-md-4">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td style="padding:2px">
                                <input type="submit" name="Activate" value="Activate" class="btn"/>
                                <input type="submit" name="Deactivate" value="Deactivate" class="btn" />
                                <input type="submit" name="Delete" value="Delete" class="btn" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="col-sm-8 col-md-8">
                    <div class="pull-right"><?php echo $links; ?></div>
                </div>   

            </div>


        </form>
    </div>
</div>

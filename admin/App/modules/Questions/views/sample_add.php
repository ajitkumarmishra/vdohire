<div class="row">
    <div class="col-sm-12 col-md-12">
        <?php if (@$error): ?>
            <div class="alert">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <?php echo $error; ?>
            </div>
        <?php endif; ?>
        <div class="well">
            <h3>Create New Sample Question</h3>
            <form class="form-horizontal" method="post" action="" id="sampleFormCreate" enctype="multipart/form-data">
                
                <div class="control-group">
                    <label class="control-label" for="question_type">Question Type</label>
                    <div class="controls">
                        <select class="form-control" name="question_type" id="question_type">
                            <option value="">Select</option>
                            <option value="Audio" <?php echo set_select('question_type', 'Audio', TRUE); ?>>Audio</option>
                            <option value="Video" <?php echo set_select('question_type', 'Video', TRUE); ?>>Video</option>
                        </select>
                    </div>
                </div>
               
                <div class="control-group">
                    <label class="control-label" for="question_title">Question Title:</label>
                    <div class="controls">
                        <textarea class="form-control" rows="2" id="question_title" name="question_title"><?php echo set_value('question_title'); ?></textarea>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="question_file">Question File</label>
                    <div class="controls">
                        <input type="file" name="question_file" id="question_file">
                    </div>
                </div>

                <div class="control-group">
                    <div class="controls">
                        <button type="submit" class="form-control btn btn-primary button-submit">Submit</button>
                    </div>
                </div>
                <div class="clear"></div>
            </form>
        </div>
    </div>
</div>

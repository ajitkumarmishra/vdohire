<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Questions Controller
 * Description : Used to handle all Questions related data
 * @author Synergy
 * @createddate : Nov 3, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 21, 2017
 */

class Questions extends MY_Controller {

    /**
     * Responsable for inheriting parent constructor
     * Responsable for auto load the question_model
     * Responsable for auto load the form_validation, pagination and session library
     * @return void
     */
    function __construct() {
        parent::__construct();
        $this->load->model('question_model', 'ques');
        $this->load->library('form_validation');
        $this->load->library('pagination');
        $this->load->library('session');
    }

    /**
     * Description : index page for Questions controller
     * Author : Synergy
     * @param none
     * @return void
     */
    function index() {
        if(!$this->session->userdata('logged_in')) {
            redirect(base_url('signin'));
        }
        echo "Index Page For Questions";
    }

    /**
     * Description : List all the Questions
     * Author : Synergy
     * @param none
     * @return render data into view
     */
    function list_all() {
        if(!$this->session->userdata('logged_in')) {
            redirect(base_url('signin'));
        }
        if ($this->input->post() != null) {
            $this->ques->updateStatus();
        }
        $config = array();
        $config["base_url"] = base_url() . "questions/list_all";
        $config['total_rows'] = $this->ques->count_all();
        $config['per_page'] = (site_limit()) ? site_limit() : DEFAULT_LIMIT_PER_PAGE;
        $config['full_tag_open'] = '<div class="pagination pagination-small pagination-right"><ul class="pagination">';
        $config['full_tag_close'] = '</ul></div>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["questions"] = $this->ques->getList($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        $data['main_content'] = 'list';
        $data['meta_title'] = "This is an meta title";
        $data['meta_keyword'] = "This is an meta keyword";
        $data['meta_description'] = "This is an meta description";
        $includeJs = array('theme/js/custom.js', 'theme/js/bootbox.min.js');
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data['main_content'] = 'questions';
        $this->load->view('page', $data);
    }

    /**
     * Description : Use to create new question
     * Author : Synergy
     * @param $_POST
     * @return redirect to questions page after success
     */
    function create() {
        if(!$this->session->userdata('logged_in')) {
            redirect(base_url('signin'));
        }
        $includeJs = array('theme/js/question.js',
            'theme/js/bootbox.min.js',
            'theme/js/angular.min.js',
            'theme/plugins/jquery-validation/lib/jquery.js',
            'theme/plugins/jquery-validation/dist/jquery.validate.js'
        );
        $includeCss = array('theme/plugins/jquery-validation/demo/css/screen.css');
        $data['includes_for_layout_css'] = add_includes('css', $includeCss);
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data['indus'] = $this->ques->getIndustryList();
        $this->form_validation->set_rules('question_industry', 'Industry', 'trim|required');

        if ($this->form_validation->run() == false) {
            $data['error'] = validation_errors();
            $data['main_content'] = 'create';
        } else {
            $ques_insert = $this->ques->create();
            if (!empty($ques_insert)) {
                redirect(base_url('questions'));
            } else {
                $data['error'] = validation_errors();
                $data['error_file'] = 'Please Click Radio button in Correct Answer';
                $data['main_content'] = 'create';
            }
        }
        $this->load->view('page', $data);
    }

    /**
     * Description : Use to update a specific question
     * Author : Synergy
     * @param int $id, array $_POST
     * @return render data into view and update if $_POST
     */
    function edit($id) {
        if(!$this->session->userdata('logged_in')) {
            redirect(base_url('signin'));
        }
        $includeJs = array('theme/js/question.js',
            'theme/js/bootbox.min.js',
            'theme/plugins/jquery-validation/lib/jquery.js',
            'theme/plugins/jquery-validation/dist/jquery.validate.js'
        );
        $includeCss = array('theme/plugins/jquery-validation/demo/css/screen.css');
        $data['includes_for_layout_css'] = add_includes('css', $includeCss);
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data['ques'] = $this->ques->getQuestion($id);
        if ($this->input->post('deleteQuesfile')) {
            $this->ques->deleteexistingfile($data['ques']->question_file, $id);
        }
        $del_ques = $this->input->post('deleteQuesfile');
        unset($del_ques);
        $data['indus'] = $this->ques->getIndustryList();
        $this->form_validation->set_rules('question_industry', 'Industry', 'trim|required');
        if ($this->form_validation->run() == false) {
            $data['error'] = validation_errors();
            $data['main_content'] = 'edit';
        } else {
            $ques_insert = $this->ques->update($id);
            redirect(base_url('questions'));
        }
        $this->load->view('page', $data);
    }

    /**
     * Description : Use to list all the interview
     * Author : Synergy
     * @param none
     * @return render data into view
     */
    function interview() {
        if(!$this->session->userdata('logged_in')) {
            redirect(base_url('signin'));
        }
        if ($this->input->post() != null) {
            $this->ques->interviewUpdateStatus();
        }
        $config = array();
        $config["base_url"] = base_url() . "questions/interview";
        $config['total_rows'] = $this->ques->interview_count_all();
        $config['per_page'] = (site_limit()) ? site_limit() : DEFAULT_LIMIT_PER_PAGE;
        $config['full_tag_open'] = '<div class="pagination pagination-small pagination-right"><ul class="pagination">';
        $config['full_tag_close'] = '</ul></div>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["questions"] = $this->ques->getInterviewList($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        $data['meta_title'] = "This is an meta title";
        $data['meta_keyword'] = "This is an meta keyword";
        $data['meta_description'] = "This is an meta description";
        $includeJs = array('theme/js/custom.js', 'theme/js/bootbox.min.js');
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data['main_content'] = 'interviewlist';
        $this->load->view('page', $data);
    }

    /**
     * Description : Use to add new interview
     * Author : Synergy
     * @param array $_POST
     * @return render data into view or insert interview data if isset($_POST)
     */
    function interview_add() {
        if(!$this->session->userdata('logged_in')) {
            redirect(base_url('signin'));
        }
        $includeJs = array('theme/js/question.js',
            'theme/js/bootbox.min.js',
            'theme/plugins/jquery-validation/lib/jquery.js',
            'theme/plugins/jquery-validation/dist/jquery.validate.js'
        );
        $includeCss = array('theme/plugins/jquery-validation/demo/css/screen.css');
        $data['includes_for_layout_css'] = add_includes('css', $includeCss);
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data['indus'] = $this->ques->getIndustryList();
        $this->form_validation->set_rules('question_industry', 'Industry', 'trim|required');
        if ($this->form_validation->run() == false) {
            $data['error'] = validation_errors();
            $data['main_content'] = 'interview_add';
        } else {
            $ques_insert = $this->ques->interview_create();
            redirect(base_url('interview'));
        }
        $this->load->view('page', $data);
    }

    /**
     * Description : Use to update an existing interview
     * Author : Synergy
     * @param int $id, array $_POST
     * @return render data into view or update interview data if isset($_POST)
     */
    function interview_edit($id) {
        if(!$this->session->userdata('logged_in')) {
            redirect(base_url('signin'));
        }
        $includeJs = array('theme/js/question.js',
            'theme/js/bootbox.min.js',
            'theme/plugins/jquery-validation/lib/jquery.js',
            'theme/plugins/jquery-validation/dist/jquery.validate.js'
        );
        $includeCss = array('theme/plugins/jquery-validation/demo/css/screen.css');
        $data['includes_for_layout_css'] = add_includes('css', $includeCss);
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data['ques'] = $this->ques->getinterviewQuestion($id);
        $data['indus'] = $this->ques->getIndustryList();
        if ($this->input->post('deleteInterviewfile')) {
            $this->ques->deleteexistinginterviewfile($data['ques']->question_file, $id);
        }
        $this->form_validation->set_rules('question_industry', 'Industry', 'trim|required');
        if ($this->form_validation->run() == false) {
            $data['error'] = validation_errors();
            $data['main_content'] = 'interview_edit';
        } else {
            $ques_insert = $this->ques->interview_update($id);
            redirect(base_url('interview'));
        }
        $this->load->view('page', $data);
    }

    /**
     * Description : Use to get functions for a particular industry
     * Author : Synergy
     * @param array $_POST
     * @return json encoded data
     */
    function getIndusFunctions() {
        $indus_id = $this->input->post('industry');
        $list = $this->ques->getFuncList($indus_id);
        echo json_encode($list);
    }

    /**
     * Description : Use to run chron job for interview
     * Author : Synergy
     * @param none
     * @return string
     */
    function interview_cron() {
        $this->ques->cronJob();
        $datetime1 = date_create('2015-12-10 10:15:42');
        $datetime2 = date_create('2015-12-10 11:15:42');
        $interval = date_diff($datetime1, $datetime2);
        echo $interval->format('%H');
        die;
    }

    /**
     * Description : Use to get all sample interview
     * Author : Synergy
     * @param none
     * @return render data into view
     */
    function sample_interview() {
        if(!$this->session->userdata('logged_in')) {
            redirect(base_url('signin'));
        }
        if ($this->input->post() != null) {
            $this->ques->sampleinterviewUpdateStatus();
        }
        $config = array();
        $config["base_url"] = base_url() . "questions/sample_interview";
        $config['total_rows'] = $this->ques->sample_interview_count_all();
        $config['per_page'] = (site_limit()) ? site_limit() : DEFAULT_LIMIT_PER_PAGE;
        $config['full_tag_open'] = '<div class="pagination pagination-small pagination-right"><ul class="pagination">';
        $config['full_tag_close'] = '</ul></div>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["questions"] = $this->ques->getSampleInterviewList($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        $data['meta_title'] = "Sample Interviews";
        $data['meta_keyword'] = "This is an meta keyword";
        $data['meta_description'] = "This is an meta description";
        $includeJs = array('theme/js/custom.js', 'theme/js/bootbox.min.js');
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data['main_content'] = 'samplelist';
        $this->load->view('page', $data);
    }

    /**
     * Description : Use to add sample interview
     * Author : Synergy
     * @param array $_POST
     * @return render data into view or insert sample interview data if isset($_POST)
     */
    function sample_interview_add() {
        if(!$this->session->userdata('logged_in')) {
            redirect(base_url('signin'));
        }
        $includeJs = array('theme/js/question.js',
            'theme/js/bootbox.min.js',
            'theme/plugins/jquery-validation/lib/jquery.js',
            'theme/plugins/jquery-validation/dist/jquery.validate.js'
        );
        $includeCss = array('theme/plugins/jquery-validation/demo/css/screen.css');
        $data['includes_for_layout_css'] = add_includes('css', $includeCss);
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $this->form_validation->set_rules('question_title', 'Title', 'trim|required');
        if ($this->form_validation->run() == false) {
            $data['error'] = validation_errors();
            $data['main_content'] = 'sample_add';
        } else {
            $ques_insert = $this->ques->sample_interview_create();
            redirect(base_url('sample'));
        }
        $this->load->view('page', $data);
    }

    /**
     * Description : Use to update a particular sample interview
     * Author : Synergy
     * @param int $id, array $_POST
     * @return render data into view or update sample interview data if isset($_POST)
     */
    function sample_interview_edit($id) {
        if(!$this->session->userdata('logged_in')) {
            redirect(base_url('signin'));
        }
        $includeJs = array('theme/js/question.js',
            'theme/js/bootbox.min.js',
            'theme/plugins/jquery-validation/lib/jquery.js',
            'theme/plugins/jquery-validation/dist/jquery.validate.js'
        );
        $includeCss = array('theme/plugins/jquery-validation/demo/css/screen.css');
        $data['includes_for_layout_css'] = add_includes('css', $includeCss);
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data['ques'] = $this->ques->getSampleQuestion($id);
        if ($this->input->post('deleteSamplefile')) {
            $this->ques->deleteexistingsamplefile($data['ques']->question_file, $id);
        }
        $this->form_validation->set_rules('question_title', 'Title', 'trim|required');
        if ($this->form_validation->run() == false) {
            $data['error'] = validation_errors();
            $data['main_content'] = 'sample_edit';
        } else {
            $ques_insert = $this->ques->sample_update($id);
            redirect(base_url('sample'));
        }
        $this->load->view('page', $data);
    }
}
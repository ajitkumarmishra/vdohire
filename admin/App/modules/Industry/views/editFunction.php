<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="row title_head">
            <div class="col-sm-12 col-md-12">
                <hr/>
                <h4 class="pull-left"><a href="javascript:void(0);">Edit Function</a></h4>
                <div class="clear"></div>
                <hr/>
            </div>
        </div>
        <?php if (@$error): ?>
            <div class="alert">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <?php echo $error; ?>
            </div>
        <?php endif; ?>
        <form method="post" id="indusFunForm" action="">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="form-group">
                        <label for="indus_id">Select Industry</label>
                        <div class="controls">
                            <select class="form-control" name="indus_id" id="indus_id">
                                <option value="">Select</option>
                                <?php foreach ($indus as $indu) { ?>
                                    <option value="<?php echo $indu->id; ?>" <?php echo ($indu->id == $func->indus_id) ? 'selected="selected"' : ''; ?>><?php echo $indu->name; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <div class="controls">
                            <input class="form-control" type="text" id="name" value="<?php echo $func->name; ?>" name="name">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions no-margin-bottom" style="text-align:center;">
                <button type="submit" class="btn btn-primary button-submit">Submit</button>
            </div>
        </form>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="row title_head">
            <div class="col-sm-12 col-md-12">
                <hr/>
                <h4 class="pull-left"><a href="javascript:void(0);">Manage Industry</a></h4>
                <a href="<?php echo base_url('industry/createIndustry'); ?>" class="btn btn-primary pull-right">Add New Industry</a>&nbsp;&nbsp;&nbsp;
                <a href="<?php echo base_url('industry/createFunction'); ?>" class="btn btn-primary pull-right">Add New Function</a>
                <div class="clear"></div>
                <hr/>
            </div>
        </div>
        <form method="post" name="form1" id="form1" action="">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th><input name="check_all" type="checkbox" id="check_all" value="1" onclick="checkall(this.form)" /></th>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Status</th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (!empty($lists)) {
                        foreach ($lists as $list) {
                            ?>
                            <tr class="<?php echo ($list->is_active == '1' ? 'success' : 'warning'); ?>">
                                <td>
                                    <input name="arr_ids[]" type="checkbox" id="arr_ids[]" value="<?php echo $list->id; ?>" />
                                </td>
                                <td><?php echo $list->id; ?></td>
                                <td><?php echo ucfirst($list->name); ?></td>
                                <td><?php echo ($list->is_active == '1' ? 'Active' : 'Inactive'); ?></td>
                                <td><a href="<?php echo base_url('industry/functions') . '/' . $list->id; ?>">Functions</a></td>
                                <td><a href="<?php echo base_url('industry/editIndustry') . '/' . $list->id; ?>"><span class="glyphicon glyphicon-pencil pull-right"></a></td>
                                <td><a href="javascript:void(0);" class="btn btn-mini btn-xs del_Listing" name="<?php echo $list->id; ?>"><span class="glyphicon glyphicon-remove "></a></td>
                            </tr>
                            <?php
                        }
                    } else {
                        ?>
                        <tr>
                            <td colspan="6" align="center">
                                No result Found
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="left" style="padding:2px">
                        <input type="submit" name="Activate" value="Activate" class="btn"/>
                        <input type="submit" name="Deactivate" value="Deactivate" class="btn" />
                        <input type="submit" name="Delete" value="Delete" class="btn" />
                    </td>
                </tr>
            </table>
            <input type="hidden" id="P_orderby" value="id" />
            <input type="hidden" id="P_orderin" value="id" />
            <input type="hidden" id="P_page" value="1" />
            <input type="hidden" id="P_reqesturl" value="<?php echo base_url('industry'); ?>" />
            <input type="hidden" id="P_deleteurl" value="<?php echo base_url('industry/delete'); ?>" />
            <input type="hidden" id="P_responseDiv" value="responce_container" />
            <?php echo $links; ?>
        </form>
    </div>
</div>

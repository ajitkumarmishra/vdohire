<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Industry Model
 * Description : Handle all the CRUD operation for Industry
 * @author Synergy
 * @createddate : Nov 12, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 17, 2017
 */

class Industry_model extends CI_Model {

    /**
     * Initialize variales
     */
    var $indus_table = "nc_industry";

    /**
     * Responsable for inherit the parent constructor
     * @return void
     */
    function __construct() {
        parent::__construct();
    }

    /**
     * Description : Use to get all the list of Industry
     * Author : Synergy
     * @param $showPerpage, $offset
     * @return array of data
     */
    function getList($showPerpage, $offset) {
        $conditions=array(
            'indus_id'=>'0'
        );
        $this->db->where($conditions);
        $this->db->limit($showPerpage, $offset);
        $this->db->from($this->indus_table);
        $result = $this->db->get();
        return $result->result();
    }

    /**
     * Description : Use to update the status of industry
     * Author : Synergy
     * @param array $_POST
     * @return void
     */
    function updateStatus() {
        if ($this->input->post('Delete') != null) {
            $arr_ids = $this->input->post('arr_ids');
            if (!empty($arr_ids)) {
                $this->db->where_in('id', $arr_ids);
                $this->db->delete($this->indus_table);
            }
        } else {
            if ($this->input->post('Deactivate') != null) {
                $status = "0";
            } else if ($this->input->post('Activate') != null) {
                $status = "1";
            }
            $data = array(
                'is_active' => $status
            );
            $arr_ids = $this->input->post('arr_ids');
            if (!empty($arr_ids)) {
                $this->db->where_in('id', $arr_ids);
                $this->db->update($this->indus_table, $data);
            }
        }
    }

    /**
     * Description : Use to count all the industries
     * Author : Synergy
     * @param none
     * @return array of data
     */
    function count_all() {
        $query = $this->db->get($this->indus_table);
        return $query->num_rows();
    }

    /**
     * Description : Use to delete an industry
     * Author : Synergy
     * @param int $id
     * @return boolean
     */
    function deleteIndustry($id) {
        $this->db->where('id', $id);
        $query = $this->db->delete($this->indus_table);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Description : Use to create an industry
     * Author : Synergy
     * @param array $_POST
     * @return boolean
     */
    public function createIndustry() {
        $data = array(
            'indus_id' => '0',
            'name' => $this->input->post('name'),
            'is_active' => '1'
        );
        $query = $this->db->insert($this->indus_table, $data);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Description : Use to update specific Industry
     * Author : Synergy
     * @param int $id
     * @return boolean
     */
    function updateIndustry($id) {
        $data = array(
            'name' => $this->input->post('name'),
            'is_active' => $this->input->post('is_active'),
        );
        $this->db->where('id', $id);
        $query = $this->db->update($this->indus_table, $data);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Description : Use to get detail of a specific industry
     * Author : Synergy
     * @param int $id
     * @return array of data
     */
    function getIndustry($id) {
        $this->db->where('id', $id);
        $this->db->from($this->indus_table);
        $result = $this->db->get();
        return $result->row();
    }
    
    /**
     * Description : Use to get all the list of Industry
     * Author : Synergy
     * @param none
     * @return array of data
     */
    function getIndustryList(){
        $conditions=array(
            'is_active'=>'1',
            'indus_id'=>'0'
        );
        $this->db->where($conditions);
        $this->db->from($this->indus_table);
        $result = $this->db->get();
        return $result->result();
    }
    
    /**
     * Description : Use to create function in an industry
     * Author : Synergy
     * @param array $_POST
     * @return boolean
     */
    function createFunction(){
        $data = array(
            'indus_id' => $this->input->post('indus_id'),
            'name' => $this->input->post('name'),
            'is_active' => '1'
        );
        $query = $this->db->insert($this->indus_table, $data);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * Description : Use to updaet the function status
     * Author : Synergy
     * @param array $_POST
     * @return void
     */
    function updateFunctionStatus() {
        if ($this->input->post('Delete') != null) {
            $arr_ids = $this->input->post('arr_ids');
            if (!empty($arr_ids)) {
                $this->db->where_in('id', $arr_ids);
                $this->db->delete($this->indus_table);
            }
        } else {
            if ($this->input->post('Deactivate') != null) {
                $status = "0";
            } else if ($this->input->post('Activate') != null) {
                $status = "1";
            }
            $data = array(
                'is_active' => $status
            );
            $arr_ids = $this->input->post('arr_ids');
            if (!empty($arr_ids)) {
                $this->db->where_in('id', $arr_ids);
                $this->db->update($this->indus_table, $data);
            }
        }
    }
    
    /**
     * Description : Use to count all the functions of a specific industry
     * Author : Synergy
     * @param int $id
     * @return array of data
     */
    function count_all_functions($id) {
        $conditions=array(
            'indus_id'=>$id
        );
        $this->db->where($conditions);
        $query = $this->db->get($this->indus_table);
        return $query->num_rows();
    }
    
    /**
     * Description : Use to get limited list of function of a specific indystry
     * Author : Synergy
     * @param int $id, int $showPerpage, int $offset
     * @return array of data
     */
    function getFunctionList($id,$showPerpage, $offset) {
        $conditions=array(
            'indus_id'=>$id
        );
        $this->db->where($conditions);
        $this->db->limit($showPerpage, $offset);
        $this->db->from($this->indus_table);
        $result = $this->db->get();
        return $result->result();
    }
    
    /**
     * Description : Use to get function details
     * Author : Synergy
     * @param int id
     * @return array of data
     */
    function getFunction($id) {
        $this->db->where('id', $id);
        $this->db->from($this->indus_table);
        $result = $this->db->get();
        return $result->row();
    }
    /**
     * Description : Use to update a specific function
     * Author : Synergy
     * @param int $id
     * @return boolean
     */
    function updateFunction($id) {
        $data = array(
            'indus_id' => $this->input->post('indus_id'),
            'name' => $this->input->post('name')
        );
        $this->db->where('id', $id);
        $query = $this->db->update($this->indus_table, $data);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }
}
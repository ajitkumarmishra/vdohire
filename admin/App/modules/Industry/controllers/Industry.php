<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Industry Controller
 * Description : Used to handle all Industry related data
 * @author Synergy
 * @createddate : July 2, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 17, 2017
 */
class Industry extends MY_Controller {

    /**
     * Responsable for auto load the the industry_model
     * Responsable for auto load the the session and pagination library
     * @return void
     */
    function __construct() {
        parent::__construct();
        $this->load->model('industry_model', 'indus');
        $this->load->library("session");
        $this->load->library('pagination');
    }

    /**
     * Description : List all the Industry
     * Author : Synergy
     * @param none
     * @return array of data 
     */
    function index() {
        check_auth();
        if(!$this->session->userdata('logged_in')) {
            redirect(base_url('signin'));
        }
        if ($this->input->post() != null) {
            $this->indus->updateStatus();
        }
        $config = array();
        $config["base_url"] = base_url() . "industry/index";
        $config['total_rows'] = $this->indus->count_all();
        $config['per_page'] = (site_limit()) ? site_limit() : DEFAULT_LIMIT_PER_PAGE;
        $config['full_tag_open'] = '<div class="pagination pagination-small pagination-right"><ul class="pagination">';
        $config['full_tag_close'] = '</ul></div>';
        $config['first_link'] = true;
        $config['last_link'] = true;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["lists"] = $this->indus->getList($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        $data['main_content'] = 'list';
        $data['meta_title'] = "Industry List";
        $data['meta_keyword'] = "This is an meta keyword";
        $data['meta_description'] = "This is an meta description";
        $includeJs = array('theme/js/industry.js', 'theme/js/custom_listing.js', 'theme/js/bootbox.min.js');
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $this->load->view('page', $data);
    }

    /**
     * Description : Use to delete an Industry
     * Author : Synergy
     * @param array $_POST
     * @return json encoded data 
     */
    function delete() {
        $id = $this->input->post('delID');
        if (isset($id) and $id != '') {
            $check = $this->indus->deleteIndustry($id);
            if ($check) {
                $data['status'] = true;
                $data['message'] = "Deleted Successfully";
                echo json_encode($data);
            } else {
                $data['status'] = false;
                $data['message'] = "Something Went wrong";
                echo json_encode($data);
            }
        } else {
            $data['status'] = false;
            $data['message'] = "Something Went wrong";
            echo json_encode($data);
        }
    }

    /**
     * Description : Use to create a new Industry
     * Author : Synergy
     * @param array $_POST
     * @return render data into view or redirect to main page 
     */
    public function createIndustry() {
        if(!$this->session->userdata('logged_in')) {
            redirect(base_url('signin'));
        }
        $includeJs = array('theme/js/industry.js',
            'theme/js/bootbox.min.js',
            'theme/plugins/jquery-validation/lib/jquery.js',
            'theme/plugins/jquery-validation/dist/jquery.validate.js'
        );
        $includeCss = array('theme/plugins/jquery-validation/demo/css/screen.css');
        $data['includes_for_layout_css'] = add_includes('css', $includeCss);
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data['main_content'] = 'create';
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Title', 'trim|required');
        if ($this->form_validation->run() == false) {
            $data['error'] = validation_errors();
            $this->load->view('page', $data);
        } else {
            $this->indus->createIndustry();
            redirect(base_url('industry'));
        }
    }

    /**
     * Description : Use to edit an Industry
     * Author : Synergy
     * @param int $id, array $_POST
     * @return render data into view or redirect to industry list page
     */
    public function editIndustry($id) {
        if(!$this->session->userdata('logged_in')) {
            redirect(base_url('signin'));
        }
        $includeJs = array('theme/js/industry.js',
            'theme/js/bootbox.min.js',
            'theme/plugins/jquery-validation/lib/jquery.js',
            'theme/plugins/jquery-validation/dist/jquery.validate.js'
        );
        $includeCss = array('theme/plugins/jquery-validation/demo/css/screen.css');
        $data['includes_for_layout_css'] = add_includes('css', $includeCss);
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data['main_content'] = 'edit';
        $data['indus'] = $this->indus->getIndustry($id);
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Title', 'trim|required');
        if ($this->form_validation->run() == false) {
            $data['error'] = validation_errors();
            $this->load->view('page', $data);
        } else {
            $this->indus->updateIndustry($id);
            redirect(base_url('industry'));
        }
    }

    /**
     * Description : Use to create a function in industry
     * Author : Synergy
     * @param array $_POST
     * @return render data into view or redirect to industry list page
     */
    public function createFunction() {
        if(!$this->session->userdata('logged_in')) {
            redirect(base_url('signin'));
        }
        $includeJs = array('theme/js/industry.js',
            'theme/js/bootbox.min.js',
            'theme/plugins/jquery-validation/lib/jquery.js',
            'theme/plugins/jquery-validation/dist/jquery.validate.js'
        );
        $includeCss = array('theme/plugins/jquery-validation/demo/css/screen.css');
        $data['includes_for_layout_css'] = add_includes('css', $includeCss);
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data['main_content'] = 'createFunction';
        $data['indus'] = $this->indus->getIndustryList();
        $this->load->library('form_validation');
        $this->form_validation->set_rules('indus_id', 'Select Industry', 'trim|required');
        if ($this->form_validation->run() == false) {
            $data['error'] = validation_errors();
            $this->load->view('page', $data);
        } else {
            $this->indus->createFunction();
            redirect(base_url('industry'));
        }
    }

    /**
     * Description : Use to list all the functions of a specific Industry
     * Author : Synergy
     * @param int $id
     * @return render data into function view
     */
    function functions($id) {
        if(!$this->session->userdata('logged_in')) {
            redirect(base_url('signin'));
        }
        check_auth();
        if ($this->input->post() != null) {
            $this->indus->updateFunctionStatus();
        }
        $config = array();
        $config["base_url"] = base_url() . "industry/functions/" . $id;
        $config['total_rows'] = $this->indus->count_all_functions($id);
        $config['per_page'] = (site_limit()) ? site_limit() : DEFAULT_LIMIT_PER_PAGE;
        $config['full_tag_open'] = '<div class="pagination pagination-small pagination-right"><ul class="pagination">';
        $config['full_tag_close'] = '</ul></div>';
        $config['first_link'] = true;
        $config['last_link'] = true;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $data["lists"] = $this->indus->getFunctionList($id, $config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        $data['main_content'] = 'Functionslist';
        $data['meta_title'] = "Industry Function List";
        $data['meta_keyword'] = "This is an meta keyword";
        $data['meta_description'] = "This is an meta description";
        $includeJs = array('theme/js/industry.js', 'theme/js/custom_listing.js', 'theme/js/bootbox.min.js');
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $this->load->view('page', $data);
    }
    
    /**
     * Description : Use to edit the specified function of a specific Industry
     * Author : Synergy
     * @param int $ind_id, int $id
     * @return render data into view or redirect to industry list page
     */
    public function editFunction($ind_id,$id) {
        $includeJs = array('theme/js/industry.js',
            'theme/js/bootbox.min.js',
            'theme/plugins/jquery-validation/lib/jquery.js',
            'theme/plugins/jquery-validation/dist/jquery.validate.js'
        );
        $includeCss = array('theme/plugins/jquery-validation/demo/css/screen.css');
        $data['includes_for_layout_css'] = add_includes('css', $includeCss);
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data['indus'] = $this->indus->getIndustryList();
        $data['func'] = $this->indus->getFunction($id);
        $data['main_content'] = 'editFunction';
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Title', 'trim|required');
        if ($this->form_validation->run() == false) {
            $data['error'] = validation_errors();
            $this->load->view('page', $data);
        } else {
            $this->indus->updateFunction($id);
            redirect(base_url('industry'));
        }
    }
}

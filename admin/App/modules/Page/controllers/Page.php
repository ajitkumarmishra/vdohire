<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Page Controller
 * Description : Used to handle all Page related data
 * @author Synergy
 * @createddate : Nov 2, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 21, 2017
 */

class Page extends MY_Controller {
    
    protected $token;
    
    /**
     * Responsable for auto load the the page_model,userdata_model
     * Responsable for auto load the the fjapi_message message library
     * Responsable for auto load fj, jwt and url helper
     * Responsable for auto load the the email, session and form_validation library
     * @return void
     */
    function __construct() {
        parent::__construct();
        $this->load->model('page_model');
        $this->load->model('userdata_model');
        // Load API Message
        $this->lang->load('fjapi_message',  'english');
        $this->load->helper('fj');
        $this->load->helper('jwt');   
        $this->load->helper('url');
        $this->load->library('email');
        $this->load->library('session');    
        $this->load->library('form_validation');
        $token = $this->config->item('accessToken');
        if (isset($_POST) && $_POST != NULL) {
            if ($token != $_POST['accessToken']) {
                //  echo 'You do not have pemission.';
                //exit;
            }
        }
        // Set Token Value
        $this->token    = 'ecbcd7eaee29848978134beeecdfbc7c';
    }
    
    /**
     * Description : Use to get client ip
     * Author : Synergy
     * @param none
     * @return string of ip address
     */
    function get_client_ip() {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
           $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

    /**
     * Description : Use to get client server
     * Author : Synergy
     * @param none
     * @return string of client ip server
     */
    function get_client_ip_server() {
        $ipaddress = '';
        if ($_SERVER['HTTP_CLIENT_IP'])
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if($_SERVER['HTTP_X_FORWARDED_FOR'])
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if($_SERVER['HTTP_X_FORWARDED'])
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if($_SERVER['HTTP_FORWARDED_FOR'])
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if($_SERVER['HTTP_FORWARDED'])
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if($_SERVER['REMOTE_ADDR'])
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

    /**
     * Description : Use to get audition details
     * Author : Synergy
     * @param int $id
     * @return render data into view
     */
    function getAudition($id) {
        $ipAddress              = $this->get_client_ip();
        $userBrowsingPlatform   = $_SERVER['HTTP_USER_AGENT'];
        $id                     = decryptURLparam($id, URLparamKey);
        
        $sql    = "select * from fj_userAuditionLike where ipAddress = '" . $ipAddress . "' AND auditionId='" . $id . "'";
        $query  = $this->db->query($sql);
        $result = $query->row_array();
        if ($result>0) {
            $data['auditionStatus'] = 'True';
        }        
        $data['audition']       = $this->page_model->getAudition($id);
        $this->load->view('fj-view-audition', $data);
    }

    /**
     * Description : Use to like or unlike the audition from a browser by a user
     * Author : Synergy
     * @param int $id
     * @return redirect on getAudition page
     */
    function likeAudition($id) {
        if (isset($_POST) && $_POST != NULL) {
            $likeStatus         = $this->input->post('likeStatus', true);
            $auditionId         = $this->input->post('audition', true);
            $decryptAuditionId  = decryptURLparam($auditionId, URLparamKey);
            if($decryptAuditionId!='' && $decryptAuditionId!=0){
                try {
                    $ipAddress              = $this->get_client_ip();
                    $userBrowsingPlatform   = $_SERVER['HTTP_USER_AGENT'];
                    $sql    = "select * from fj_userAuditionLike where ipAddress = '" . $ipAddress . "' AND auditionId='" . $decryptAuditionId . "'";
                    $query  = $this->db->query($sql);
                    $result = $query->row_array();
                    if ($result>0) {
                        if($likeStatus=='Dislike') {
                            // delete
                            $this->db->where('id', $result['id']);
                            $this->db->delete('fj_userAuditionLike');
                            redirect(base_url('page/getAudition/'.$auditionId));
                        }
                        else {
                            redirect(base_url('page/getAudition/'.$auditionId));
                        }
                    }
                    else {
                        // insert                        
                        $item['auditionId']             = $decryptAuditionId;
                        $item['ipAddress']              = $ipAddress;
                        $item['userBrowsingPlatform']   = $userBrowsingPlatform;
                        $this->page_model->addLikeAudtionData($item);
                        redirect(base_url('page/getAudition/'.$auditionId));
                    }
                } catch (Exception $e) {
                    redirect(base_url('page/getAudition/'.$auditionId));
                }
            }
        }
    }

    /**
     * Description : Use to get profile of a user
     * Author : Synergy
     * @param array $params
     * @return render data into view
     */
    function getProfile($params) {
        $data['userData']       = $this->page_model->getprofileInformation($params);      
        $this->load->view('fj-view-profile', $data);
    }    
    
    /**
     * Description : Use to get tickers
     * Author : Synergy
     * @param array $params
     * @return render data into view
     */
    function getTicker($params) {
        $data['tickerData']       = $this->page_model->getTickerInformation($params);
        $this->load->view('fj-view-ticker', $data);
    }  
    
    /**
     * Description : Use to get knowledge center or topics data 
     * Author : Synergy
     * @param array $params, array $params2
     * @return render data into view
     */
    function getKnowledgeCenter($params1,$params2) {
        $data['knowledgeData']       = $this->page_model->getKnowledgeCenterInformation($params1,$params2);
        $this->load->view('fj-view-knowledge', $data);
    }
    
    /**
     * Description : Use to display help page
     * Author : Synergy
     * @param array $params
     * @return void
     */
    function getHelp($params) {     
        $this->load->view('fj-help');
    } 
    
    /**
     * Description : Use to display sample interview page
     * Author : Synergy
     * @param array $params
     * @return void
     */
    function sampleInterview($params) {     
        $this->load->view('fj-sampleInterview');
    }

    /**
     * Description : Use to display user interview page where any user to which interview is shared can see
     * Author : Synergy
     * @param int $id
     * @return render data into view
     */
    function userJobInterview($id) {
		 
        if($id!='') {
			//$userJobId = encryptURLparam($id, URLparamKey); echo $userJobId;die;
            $userJobId  = decryptURLparam($id, URLparamKey);
            $sql    = "
                        SELECT	U.fullname,UJ.createdAt as interviewDate,UJ.jobId,UJ.userId as jobSeekerId
                        FROM 	fj_userJob      UJ 
                        LEFT JOIN	fj_users	U
                        ON		UJ.userId = U.id
                        WHERE       UJ.id = '" . $userJobId . "'
                    ";
            $query  = $this->db->query($sql);
            $result = $query->row_array();
			
            if ($result>0) {
				
                if($userJobId > 1) {
					//$RequestSentBy = $test = getUserById($result['createdBy']);
					$jobCode = getJobDetail($result['jobId']);
                    $interview['flag']              = 'TRUE';
                    //$interview['userJobShare']      = encryptURLparam($result['id'], URLparamKey);
                    //$interview['videoLink']         = $result['videoLink'];
                    $interview['fullname']          = $result['fullname'];
                    $interview['emailId']           = encryptURLparam($result['email'], URLparamKey);
                    $interview['interviewDate']     = $result['interviewDate'];
                    $interview['Job']           	=  $jobCode['title'];
					
					//$interview['RequestSentBy']     = $RequestSentBy['fullname'];
					
					//getAllInterviewAnswerDetails
					$statictoken = 'ecbcd7eaee29848978134beeecdfbc7c';
					$base_url = base_url().'Fjapi';
					
					$json = array("token" => $statictoken, "methodname" => 'getUserJobQuestionsAnswers', 'jobId' => $result['jobId'], 'userId' => $result['jobSeekerId']);                                                                    
					$data_string = json_encode($json);                
					
					$ch = curl_init($base_url);                                                                      
					curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
					curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
					curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
					curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
						'Content-Type: application/json',                                                                                
						'Content-Length: ' . strlen($data_string))                                                                       
					);                                                                                                                  
																																		 
					$result = curl_exec($ch);
					
					curl_close ($ch);
					$allResult = json_decode($result);
					$questionsListDetails = $allResult->questionsList;
					
					$interview['questionsListDetails']     = $questionsListDetails;
					
					
                    $this->load->view('fj-view-job-interview', compact(interview));
                }
                else {                    
                    $interview['emailId']           = encryptURLparam($result['emailId'], URLparamKey);
                    $interview['videoLink']         = $result['videoLink'];
                    $interview['userJobShare']      = encryptURLparam($result['id'], URLparamKey);
                    $interview['fullname']          = $result['fullname'];
                    $interview['flag']              = 'FALSE';
                    $this->load->view('fj-view-interview', compact($interview));                
                }
            }
            else {
                $interview['flag']               = 'FALSE';
                $this->load->view('fj-view-interview', compact($interview));

            }
        }
        else {
            redirect(base_url());
        }
    }
	
	
	function getCSVEventInterviews() {
        
			//$userJobId = encryptURLparam($id, URLparamKey); echo $userJobId;die;
            
            $sql    = "SELECT uj.id,uj.userId,j.fjCode,j.sourceCompanyName,j.title,j.createdBy FROM `fj_userJob` uj left join fj_jobs j on uj.jobId = j.id WHERE j.eventId='1' order by uj.jobId
                    ";
            $query  = $this->db->query($sql);
            $results = $query->result();
            if ($results>0) {
				$j = 0;
				foreach($results as $result){
					if($result->sourceCompanyName == ""){
						$creater = getUserById($result->createdBy);
						$companyName = $creater['company'];
					}else{
						$companyName = $result->sourceCompanyName;
					}
					$userDetailArray = getUserById($result->userId);
					//$jobCode = getJobDetail();
					$interview[$j]['userName'] 	= $userDetailArray['fullname'];
					$interview[$j]['link']  	= "https://firstjob.co.in/admin/page/userJobInterview/".encryptURLparam($result->id, URLparamKey);
					$interview[$j]['Job']       =  $result->title;
					$interview[$j]['CompanyName']       =  $companyName;
					$interview[$j]['fjCode']       =  $result->fjCode;
					$j++;
				}
				//echo "<pre>";print_r($interview);die;
				$content = "";
				foreach($interview as $data){
					$content .= $data['userName'].",".$data['Job'].",".$data['CompanyName'].",".$data['link'].",".$data['fjCode']."<br>";
				}
				echo $content;die;
					
            }
            else {
                $interview['flag']               = 'FALSE';
                $this->load->view('fj-view-interview', compact($interview));

            }
        
    }
	
	
	function userInterview($id=NULL) {
		
        if($id!='') {
            $video  = decryptURLparam($id, URLparamKey);
            $sql    = "
                        SELECT	U.fullname,UJ.createdAt as interviewDate,UJ.jobId,UJ.userId as jobSeekerId, UJS.*
                        FROM 	fj_userJobShare	UJS
                        LEFT JOIN   fj_userJob      UJ
                        ON		UJS.userJobId = UJ.id
                        LEFT JOIN	fj_users	U
                        ON		UJ.userId = U.id
                        WHERE       UJS.id = '" . $video . "'
                    ";
					
            $query  = $this->db->query($sql);
            $result = $query->row_array();
			
            if ($result>0) {
				
                if($result['videoLinkStatus'] == '1') {
					$RequestSentBy = $test = getUserById($result['createdBy']);
					$jobCode = getJobDetail($result['jobId']);
                    $interview['flag']              = 'TRUE';
                    $interview['userJobShare']      = encryptURLparam($result['id'], URLparamKey);
                    $interview['videoLink']         = $result['videoLink'];
                    $interview['fullname']          = $result['fullname'];
                    $interview['emailId']           = encryptURLparam($result['emailId'], URLparamKey);
                    $interview['emailIdDecrypted']  = $result['emailId'];
                    $interview['interviewDate']     = $result['interviewDate'];
                    $interview['Job']           	=  $jobCode['title'];
					
					$interview['RequestSentBy']     = $RequestSentBy['fullname'];
					
					//getAllInterviewAnswerDetails
					$statictoken = 'ecbcd7eaee29848978134beeecdfbc7c';
					$base_url = base_url().'Fjapi';
					
					$json = array("token" => $statictoken, "methodname" => 'getUserJobQuestionsAnswers', 'jobId' => $result['jobId'], 'userId' => $result['jobSeekerId']);                                                                    
					$data_string = json_encode($json);                
					
					$ch = curl_init($base_url);                                                                      
					curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
					curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
					curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
					curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
						'Content-Type: application/json',                                                                                
						'Content-Length: ' . strlen($data_string))                                                                       
					);                                                                                                                  
																																		 
					$result = curl_exec($ch);
					
					curl_close ($ch);
					$allResult = json_decode($result);
					$questionsListDetails = $allResult->questionsList;
					
					$interview['questionsListDetails']     = $questionsListDetails;
					
					
                    $this->load->view('fj-view-interview', compact('interview'));
                }
                else {                    
                    $interview['emailId']           = encryptURLparam($result['emailId'], URLparamKey);
                    $interview['videoLink']         = $result['videoLink'];
                    $interview['videoLinkStatus']         = $result['videoLinkStatus'];
                    $interview['userJobShare']      = encryptURLparam($result['id'], URLparamKey);
                    $interview['fullname']          = $result['fullname'];
                    $interview['flag']              = 'FALSE';
                    $interview['success']              = 'Your interview feedback has been submitted successfully';
					
                    $this->load->view('fj-view-interview', $interview);                
                }
            }
            else {
                $interview['flag'] = 'FALSE';
                $this->load->view('fj-view-interview', compact($interview));

            }
        }
        else {
            redirect(base_url());
        }
    }

    function muserInterview($id=NULL) {
        
        if($id!='') {
            $video  = decryptURLparam($id, URLparamKey);
            $sql    = "
                        SELECT  U.fullname,UJ.createdAt as interviewDate,UJ.jobId,UJ.userId as jobSeekerId, UJS.*
                        FROM    fj_userJobShare UJS
                        LEFT JOIN   fj_userJob      UJ
                        ON      UJS.userJobId = UJ.id
                        LEFT JOIN   fj_users    U
                        ON      UJ.userId = U.id
                        WHERE       UJS.id = '" . $video . "'
                    ";
                    
            $query  = $this->db->query($sql);
            $result = $query->row_array();
            
            if ($result>0) {
                
                if($result['videoLinkStatus'] == '1') {
                    $RequestSentBy = $test = getUserById($result['createdBy']);
                    $jobCode = getJobDetail($result['jobId']);
                    $interview['flag']              = 'TRUE';
                    $interview['userJobShare']      = encryptURLparam($result['id'], URLparamKey);
                    $interview['videoLink']         = $result['videoLink'];
                    $interview['fullname']          = $result['fullname'];
                    $interview['emailId']           = encryptURLparam($result['emailId'], URLparamKey);
                    $interview['emailIdDecrypted']  = $result['emailId'];
                    $interview['interviewDate']     = $result['interviewDate'];
                    $interview['Job']               =  $jobCode['title'];
                    
                    $interview['RequestSentBy']     = $RequestSentBy['fullname'];
                    
                    //getAllInterviewAnswerDetails
                    $statictoken = 'ecbcd7eaee29848978134beeecdfbc7c';
                    $base_url = base_url().'Fjapi';
                    
                    $json = array("token" => $statictoken, "methodname" => 'getUserJobQuestionsAnswers', 'jobId' => $result['jobId'], 'userId' => $result['jobSeekerId']);                                                                    
                    $data_string = json_encode($json);                
                    
                    $ch = curl_init($base_url);                                                                      
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
                    curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
                        'Content-Type: application/json',                                                                                
                        'Content-Length: ' . strlen($data_string))                                                                       
                    );                                                                                                                  
                                                                                                                                         
                    $result = curl_exec($ch);
                    
                    curl_close ($ch);
                    $allResult = json_decode($result);
                    $questionsListDetails = $allResult->questionsList;
                    
                    $interview['questionsListDetails']     = $questionsListDetails;
                    
                    
                    $this->load->view('fj-mobile-view-interview', compact('interview'));
                }
                else {                    
                    $interview['emailId']           = encryptURLparam($result['emailId'], URLparamKey);
                    $interview['videoLink']         = $result['videoLink'];
                    $interview['videoLinkStatus']         = $result['videoLinkStatus'];
                    $interview['userJobShare']      = encryptURLparam($result['id'], URLparamKey);
                    $interview['fullname']          = $result['fullname'];
                    $interview['flag']              = 'FALSE';
                    $interview['success']              = 'Your interview feedback has been submitted successfully';
                    
                    $this->load->view('fj-mobile-view-interview', $interview);                
                }
            }
            else {
                $interview['flag'] = 'FALSE';
                $this->load->view('fj-mobile-view-interview', compact($interview));

            }
        }
        else {
            redirect(base_url());
        }
    }
	
	function videoResume(){
		$userId = $this->input->get('user');
		
		$result 		= $this->page_model->listAuditions($userId);
		
        //echo $totalCount;exit;
        
		$data['contentAuditionSet']     = $result;
		$data['totalAuditions'] 		= $totalCount;
		
		$this->load->view('fj_index', $data);
	}

    /**
     * Description : Use to comment on an interview by a user to which interview is shared
     * Author : Synergy
     * @param array $_POST
     * @return string of error message or redirect to current page
     */
    function interviewComment () {
        //print_r($_POST);exit;
        if (isset($_POST) && $_POST != NULL) {
            $fullname       = $this->input->post('fullname', true);
            $lastname       = $this->input->post('lastname', true);
            $emailid        = $this->input->post('emailid', true);            
            $email          = $this->input->post('eFlag', true);
            $rating         = $this->input->post('rating', true);
            $comment        = $this->input->post('comment', true);            
            $userJobShare   = $this->input->post('userJobShare', true);           
            $eFlag          = $this->input->post('eFlag', true); 
            
            $this->form_validation->set_rules('fullname', 'Full Name', 'trim|required|min_length[2]|max_length[25]');
            //$this->form_validation->set_rules('lastname', 'Last Name', 'trim|required|min_length[2]|max_length[25]');
            $this->form_validation->set_rules('emailid', 'Email Id', 'trim|required|valid_email');
            //$this->form_validation->set_rules('rating', 'Rating', 'trim|required');
            $this->form_validation->set_rules('comment', 'Comment', 'trim|required|min_length[4]|max_length[250]');
            //$this->form_validation->set_rules('userJobShareId', 'Invalid Data', 'trim|required');
            //$this->form_validation->set_rules('eFlag', 'Invalid Data', 'trim|required');
            
            $resp['interviewVideo'] =  $userJobShare;
                    
            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }
            
            if(encryptURLparam($emailid, URLparamKey) != $email) {
                $resp['error'] = "You are not allowed to provide feedback on this interview.";
            }
            
            if($userJobShare=='' || $userJobShare==NULL || $eFlag=='' || $eFlag==NULL) {
                $resp['error'] = "You are not allowed to provide feedback on this interview.";
            }
            
            if (!$resp['error']) {
                $userJobShareId = decryptURLparam($userJobShare, URLparamKey);
                $sql            = "select * from fj_userJobShare where id = '" . $userJobShareId . "'";
                $query          = $this->db->query($sql);
                $result         = $query->row_array();
                //echo "<pre>"; print_r($result); exit;
                if ($result>0) {
                    //if($emailid == $result['emailId']) {
                        if($result['videoLinkStatus']==1) {
                            $data['userJobShareId'] = $userJobShareId;
                            $data['firstName']      = $fullname;
                            $data['lastName']       = $lastname;
                            $data['emailId']        = $emailid;
                            $data['rating']         = $rating;
                            $data['comment']        = $comment;        
                            $resp['error']          = "Your feedback on this interview is successfully submitted.";
                            $interview['error']     = $resp;
                            $assessmentId = $this->page_model->addInterviewComment($data);
                            $this->session->set_userdata('interviewError', $resp);
                            redirect($_SERVER['HTTP_REFERER']);
                        }
                        else {
                            $resp['error']                  = "Your feedback on this interview is already submitted.";                        
                            $interview['flag']              = 'TRUE';
                            $interview['userJobShare']      = $userJobShare;
                            $interview['videoLink']         = $result['videoLink'];
                            $interview['emailId']           = encryptURLparam($result['emailId'], URLparamKey);                        
                            $interview['error']             = $resp;
                            $this->session->set_userdata('interviewError', $resp);
                            redirect($_SERVER['HTTP_REFERER']);
                        }
                    //}
                    /*else {
                        $resp['error']      = "You are not allowed to provide feedback on this interview.";
                        $interview['error'] = $resp;
                        $this->session->set_userdata('interviewError', $resp);
                        redirect($_SERVER['HTTP_REFERER']);                        
                    }*/
                }
                else {
                    $resp['error']      = "You are not allowed to provide feedback on this interview.";
                    $interview['error'] = $resp;
                    $this->session->set_userdata('interviewError', $resp);
                    redirect($_SERVER['HTTP_REFERER']);
                }                
            }
            else {                     
                $interview['error']             = $resp;
                $this->session->set_userdata('interviewError', $resp);
                redirect($_SERVER['HTTP_REFERER']);
            }
        }
    }

    function sendEmailToUsers() {
        $from = "falconadmin@firstjob.co.in";
        $fromname = "Gaurav";
        $to = "ajitchandour@gmail.com"; //Recipients list (semicolon separated)
        $api_key = "d9a069ff3aaa57f17c65093e489f9bc2";
        $subject = "Test Mail";
        $content = "This is my MAil Content";

        $data = array();
        $data['subject']= rawurlencode($subject);                                                                       
        $data['fromname']= rawurlencode($fromname);                                                             
        $data['api_key'] = $api_key;
        $data['from'] = $from;
        $data['content']= rawurlencode($content);
        $data['recipients']= $to;
        $apiresult = sendEmailFalconideApi(@$api_type,@$action,$data);
        print_r($apiresult);exit;
    }
}
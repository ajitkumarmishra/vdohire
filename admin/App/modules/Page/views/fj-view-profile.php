<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Firstjob User Profile</title>
        <script src="<?php echo base_url(); ?>theme/firstJob/js/jquery.js"></script>
        <link href="<?php echo base_url(); ?>theme/firstJob/css/public_page_style.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url(); ?>theme/firstJob/css/bootstrap-theme.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url(); ?>theme/firstJob/css/bootstrap.css" rel="stylesheet" type="text/css">            
        
        <style>
            video::-internal-media-controls-download-button {
                display:none;
            }

            video::-webkit-media-controls-enclosure {
                overflow:hidden;
            }

            video::-webkit-media-controls-panel {
                width: calc(100% + 30px); /* Adjust as needed */
            }
        </style>
    </head>
    <body>
        
    <?php
    if ($userData) 
    {
        if($userData['code']=='200') {?>
        <div class="container-fluid font_size12">
            <div class="container">
            <div class="row">

             <p>
                <table width="100%">
                    <tr>
                        <th colspan="3" class="font_size20"><h2><?php echo $userData['basicInfo'][0]['fullname']; ?></h2></th>
                    </tr>
                    <tr>
                        <th colspan="3" class="font_size20" style="padding:2%">
                            <?php
                            if($userData['otherInfo'][0]['image']!='') { ?>
                                <img src="<?php echo $userData['otherInfo'][0]['image']; ?>.PNG" width="100%" height="400">
                            <?php
                            } 
                            else { ?>
                                <img src="<?php echo base_url(); ?>uploads/userImages/defualt.PNG" width="100%" height="400">
                            <?php
                            }
                            ?>
                        </th>
                    </tr>
                    <tr>
                        <td colspan="3" class="font_size20" style="padding:2%">                            
                            <?php
                            if($userData['auditionInfo']!='') { ?>
                                <center>
                                <?php
                                if($userData['auditionFileNames']!='') {
                                    $auditionFiles = explode(',', $userData['auditionFileNames']);
                                    echo '<video id="videoarea" preload="auto" tabindex="0" controls="" src="'.S3BUCKETPATH.'/'.S3AUDITION.'/'.trim($auditionFiles[0]).'"></video>'; ?>
                                    <ul id="playlist">
                                        <?php
                                        $m=0;
                                        foreach($auditionFiles as $val) {
                                            $m++;
                                            if($m==1)
                                                echo '<li class="active"><a href="'.S3BUCKETPATH.'/'.S3AUDITION.'/'.trim($val).'">Audition Part '.$m.'</a></li>';
                                            else
                                                echo '<li><a href="'.S3BUCKETPATH.'/'.S3AUDITION.'/'.trim($val).'">Audition Part '.$m.'</a></li>';
                                        }
                                        ?>
                                    </ul>
                                    <?php
                                }
                                else {
                                    echo '<video id="videoarea" preload="auto" tabindex="0" controls="" src="'.S3BUCKETPATH.'/'.S3MERGEAUDITION.'/'.str_replace('flv', 'mp4', $auditionFiles['audition_file']).'"></video>'; ?>
                                    <ul id="playlist">
                                        <?php
                                        echo '<li class="active"><a href="'.S3BUCKETPATH.'/'.S3MERGEAUDITION.'/'.str_replace('flv', 'mp4', $auditionFiles['audition_file']).'">Audition Part 1</a></li>';
                                        ?>
                                    </ul>
                                    <?php
                                }
                                ?>
                                    
                <script>                      
                    document.addEventListener('contextmenu', event => event.preventDefault());
                    init();
                    function init(){
                        var current     = 0;
                        var audio       = $('#videoarea');
                        var playlist    = $('#playlist');
                        var tracks      = playlist.find('li a');
                        var len         = tracks.length;
                        audio[0].volume = .10;
                        //audio[0].play();
                        playlist.find('a').click(function(e){
                            e.preventDefault();
                            link    = $(this);
                            current = link.parent().index();
                            run(link, audio[0]);
                        });
                        audio[0].addEventListener('ended',function(e){
                            current++;
                            if(current == len){
                                current = 0;
                                link    = playlist.find('a')[0];
                            }else{
                                link    = playlist.find('a')[current];    
                            }
                            if(current!=0)
                                run($(link),audio[0]);
                        });
                    }
                    function run(link, player){
                            player.src  = link.attr('href');
                            par         = link.parent();
                            par.addClass('active').siblings().removeClass('active');
                            player.load();
                            player.play();
                    }
                </script>
                
                <style>
                #playlist li{
                    cursor:pointer;
                    padding:1px;
                    list-style: none;
                }
                #videoarea {
                    width:100%;
                    height:400px;   
                }                
                #playlist,audio{background:#666;width:50%;padding:5px;}
                .active a{color:#4CAF50;text-decoration:none;}
                #playlist li a{color:#eeeedd;background:#333;padding:5px;display:inline-block; font-size: 12px; text-decoration:none;}
                #playlist li a:hover{color:#eeeedd;text-decoration:none;}
                #playlist li a:visited{color:#eeeedd;text-decoration:none;}
                </style>
                                </center>
                                
                                <p><center>Audition Likes - <?php echo $userData['auditionLikeCount'][0]['count'];?></center></p>
                                
                            <?php
                            } 
                            else {
                                echo "<p><center>No Audition Found</center></p>";
                            }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <th colspan="3" class="font_size20">Basic Information</th>
                    </tr>
                    <tr>
                        <th>Name</th>
                        <th> &emsp; </th>
                        <td><?php echo $userData['basicInfo'][0]['fullname']; ?></td>
                    </tr>
                    <tr>
                        <th>Email</th>
                        <th> &emsp; </th>
                        <td><?php echo $userData['basicInfo'][0]['email']; ?></td>
                    </tr>
                    <tr>
                        <th>Mobile</th>
                        <th> &emsp; </th>
                        <td><?php echo $userData['basicInfo'][0]['mobile']; ?></td>
                    </tr>
                    <tr>
                        <th>YOB</th>
                        <th> &emsp; </th>
                        <td><?php echo ($userData['basicInfo'][0]['dob']!='0000-00-00' || $userData['basicInfo'][0]['dob']!=''?date('Y',strtotime($userData['basicInfo'][0]['dob'])):''); ?></td>
                    </tr>
                    <tr>
                        <th>Gender</th>
                        <th> &emsp; </th>
                        <td><?php echo ($userData['basicInfo'][0]['gender']=='1'?'Male':'Female'); ?></td>
                    </tr>
                    <tr>
                        <th>Pincode</th>
                        <th> &emsp; </th>
                        <td><?php echo $userData['basicInfo'][0]['pincode']; ?></td>
                    </tr>
                    <tr>
                        <th colspan="3" class="font_size20">&emsp;</th>
                    </tr>
                    <tr>
                        <th colspan="3" class="font_size20">Social Information</th>
                    </tr>
                    <tr>
                        <th>Facebook</th>
                        <th> &emsp; </th>
                        <td><?php echo $userData['socialInfo'][0]['facebookLink']; ?></td>
                    </tr>
                    <tr>
                        <th>LinkedIn</th>
                        <th> &emsp; </th>
                        <td><?php echo $userData['socialInfo'][0]['linkedInLink']; ?></td>
                    </tr>
                    <tr>
                        <th>Twitter</th>
                        <th> &emsp; </th>
                        <td><?php echo $userData['socialInfo'][0]['twitterLink']; ?></td>
                    </tr>
                    <tr>
                        <th>Google Plus</th>
                        <th> &emsp; </th>
                        <td><?php echo $userData['socialInfo'][0]['googlePlusLink']; ?></td>
                    </tr>
                    <tr>
                        <th colspan="3" class="font_size20">&emsp;</th>
                    </tr>
                    <tr>
                        <th colspan="3" class="font_size20">Educational Information</th>
                    </tr>
                    <?php
                    foreach($userData['educationalInfo'] as $educationalData) { ?>
                    <tr>
                        <th>University</th>
                        <th> &emsp; </th>
                        <td><?php echo $educationalData['universityName']; ?></td>
                    </tr>
                    <tr>
                        <th>Institute</th>
                        <th> &emsp; </th>
                        <td><?php echo $educationalData['institute']; ?></td>
                    </tr>
                    <tr>
                        <th>Course</th>
                        <th> &emsp; </th>
                        <td><?php echo $educationalData['courseName']; ?></td>
                    </tr>
                    <tr>
                        <th>Completion Year</th>
                        <th> &emsp; </th>
                        <td><?php echo $educationalData['completionYear']; ?></td>
                    </tr>
                    <tr>
                        <th>Percent</th>
                        <th> &emsp; </th>
                        <td><?php echo $educationalData['percent']; ?></td>
                    </tr>
                    <tr>
                        <th colspan="3">&emsp;</th>
                    </tr>
                    <?php
                    } ?>
                    <tr>
                        <th colspan="3" class="font_size20">&emsp;</th>
                    </tr>
                    <tr>
                        <th colspan="3" class="font_size20">Other Information</th>
                    </tr>
                    <tr>
                        <th>Preferred Location</th>
                        <th> &emsp; </th>
                        <td><?php 
                            foreach($userData['otherInfo'][0]['preferredLocation'] as $location){
                                echo $location['name']."<br>";
                            }
                            ?></td>
                    </tr>
                    <tr>
                        <th>Expected CTC</th>
                        <th> &emsp; </th>
                        <td><?php echo $userData['otherInfo'][0]['expectedCtcFrom']; ?> - <?php echo $userData['otherInfo'][0]['expectedCtcTo']; ?> Lakhs</td>
                    </tr>
                    <tr>
                        <th>Work Experience</th>
                        <th> &emsp; </th>
                        <td><?php echo $userData['otherInfo'][0]['workExperience']; ?> Years</td>
                    </tr>
                    <tr>
                        <th>Aadhar Card</th>
                        <th> &emsp; </th>
                        <td><?php echo $userData['otherInfo'][0]['adharCard']; ?></td>
                    </tr>
                    <tr>
                        <th>Languages</th>
                        <th> &emsp; </th>
                        <td><?php echo $userData['otherInfo'][0]['languages']; ?></td>
                    </tr>
                </table>
             </p>

            </div>               
        </div>  
        </div>

        <script type="text/javascript" src="<?php echo base_url(); ?>theme/firstJob/js/jquery-1.12.4.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>theme/firstJob/js/bootstrap.js"></script>
    <?php
        }
    }
    ?>
    </body>
</html>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Firstjob User Audition</title>
        <script src="<?php echo base_url(); ?>theme/firstJob/js/jquery.js"></script>
        <link href="<?php echo base_url(); ?>theme/firstJob/css/public_page_style.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url(); ?>theme/firstJob/css/bootstrap-theme.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url(); ?>theme/firstJob/css/bootstrap.css" rel="stylesheet" type="text/css">
    </head>
    <body>
    <script>         
    document.addEventListener('contextmenu', event => event.preventDefault());
    $(document).on('click', '.checkbox', function () {
        if($('.checkbox').is(":checked")) {
            $('#likeStatus').val('Like');
        }
        else {
            $('#likeStatus').val('Dislike');
        }
        $('#auditionLikeForm').submit();
    });
    </script>
    <style>
        video::-internal-media-controls-download-button {
            display:none;
        }

        video::-webkit-media-controls-enclosure {
            overflow:hidden;
        }

        video::-webkit-media-controls-panel {
            width: calc(100% + 30px); /* Adjust as needed */
        }
    </style>
    <?php
    //echo encryptURLparam('561', URLparamKey);
    //print_r($audition);
    if ($audition) {
        ?>
        <div class="container-fluid header_bg">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <img src="<?php echo base_url(); ?>theme/firstJob/image/logo.png" class="img-responsive center-block logo_padd"  />
                    </div>
                </div>    

                <div class="clearfix"></div>
                <div class="col-md-2"></div>
                
                <div class="col-md-8 video_padd">
                    
                    <center>
                    <?php
                    if($audition['auditionFiles']!='') {
                        $auditionFiles = explode(',', $audition['auditionFiles']);
                        echo '<video id="videoarea" preload="auto" tabindex="0" controls="" src="'.S3BUCKETPATH.'/'.S3AUDITION.'/'.trim($auditionFiles[0]).'"></video>'; ?>
                        <ul id="playlist">
                            <?php
                            $m=0;
                            foreach($auditionFiles as $val) {
                                $m++;
                                if($m==1)
                                    echo '<li class="active"><a href="'.S3BUCKETPATH.'/'.S3AUDITION.'/'.trim($val).'">Audition Part '.$m.'</a></li>';
                                else
                                    echo '<li><a href="'.S3BUCKETPATH.'/'.S3AUDITION.'/'.trim($val).'">Audition Part '.$m.'</a></li>';
                            }
                            ?>
                        </ul>
                        <?php
                    }
                    else {
                        echo '<video id="videoarea" preload="auto" tabindex="0" controls="" src="'.S3BUCKETPATH.'/'.S3MERGEAUDITION.'/'.str_replace('flv', 'mp4', $auditionFiles['audition_file']).'"></video>'; ?>
                        <ul id="playlist">
                            <?php
                            echo '<li class="active"><a href="'.S3BUCKETPATH.'/'.S3MERGEAUDITION.'/'.str_replace('flv', 'mp4', $auditionFiles['audition_file']).'">Audition Part 1</a></li>';
                            ?>
                        </ul>
                        <?php
                    }
                    ?>
                    </center>
                </div>
                
                <script>                    
                    init();
                    function init(){
                        var current = 0;
                        var audio = $('#videoarea');
                        var playlist = $('#playlist');
                        var tracks = playlist.find('li a');
                        var len = tracks.length;
                        audio[0].volume = .10;
                        //audio[0].play();
                        playlist.find('a').click(function(e){
                            e.preventDefault();
                            link = $(this);
                            current = link.parent().index();
                            run(link, audio[0]);
                        });
                        audio[0].addEventListener('ended',function(e){
                            current++;
                            if(current == len){
                                current = 0;
                                link = playlist.find('a')[0];
                            }else{
                                link = playlist.find('a')[current];    
                            }
                            if(current!=0)
                                run($(link),audio[0]);
                        });
                    }
                    function run(link, player){
                            player.src = link.attr('href');
                            par = link.parent();
                            par.addClass('active').siblings().removeClass('active');
                            player.load();
                            player.play();
                    }
                </script>
                
                <style>
                #playlist li{
                    cursor:pointer;
                    padding:1px;
                    list-style: none;
                }
                #videoarea {
                    width:100%;
                    height:400px;   
                } 
                #playlist,audio{background:#666;width:50%;padding:5px;}
                .active a{color:#4CAF50;text-decoration:none;}
                #playlist li a{color:#eeeedd;background:#333;padding:5px;display:inline-block; font-size: 12px; text-decoration:none;}
                #playlist li a:hover{color:#eeeedd;text-decoration:none;}
                #playlist li a:visited{color:#eeeedd;text-decoration:none;}
                </style>
                
                <div class="col-md-2"></div>
                <div class="clearfix"></div>       
                
                <div class="chk_bx center-block">
                    <form action="<?php echo base_url(); ?>page/like" name="auditionLikeForm" id="auditionLikeForm" method="post">
                        <input class="checkbox" type="checkbox" name="thing" id="thing" value="" <?php if($auditionStatus=='True') echo "checked='checked' disabled";?> >
                        <label for="thing"></label>                        
                        <input class="audition" type="hidden" value="<?php echo encryptURLparam($audition['id'], URLparamKey);?>" name="audition" id="audition" >
                        <input type="hidden" name="likeStatus" id="likeStatus" >
                    </form>
                </div>
                
                <p class="text-center txt_white font_size20"><?php if($auditionStatus=='True') echo "You Liked This Video"; else echo "Click to Like Audition"; ?></p>
                
                <div class="text-center padding_btm">
                    <p class="text-center txt_white font_size20"><a href="<?php echo PLAYSTORE; ?>" target="_blank">Download</a> Firstjob Android App </p>
                    <div></div>  
                </div>
                
                <script type="text/javascript" src="<?php echo base_url(); ?>theme/firstJob/js/jquery-1.12.4.js"></script>
                <script type="text/javascript" src="<?php echo base_url(); ?>theme/firstJob/js/bootstrap.js"></script>
            </div>
        </div>        
        <?php
    }
    ?>
    </body>
</html>

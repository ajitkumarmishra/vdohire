<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>VDOHire User Interview</title>
        <script src="<?php echo base_url(); ?>theme/firstJob/js/jquery.js"></script>
        <link href="<?php echo base_url(); ?>theme/firstJob/css/public_page_style.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url(); ?>theme/firstJob/css/bootstrap-theme.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url(); ?>theme/firstJob/css/bootstrap.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>/theme/css/firstJob/global.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>/theme/css/firstJob/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>/theme/css/firstJob/responsive.css">
        <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700' rel='stylesheet' type='text/css'>
        <script src="<?php echo base_url(); ?>/theme/js/firstJob/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>/theme/js/firstJob/bootstrap.js"></script>
        <script src="<?php echo base_url(); ?>/theme/js/firstJob/bootbox.min.js"></script>
        <style>
            @import url(//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css);
            .rating { 
            border: none;
            float: left;
            }

            .rating > input { display: none; } 
            .rating > label:before { 
            margin: 5px;
            font-size: 1.25em;
            font-family: FontAwesome;
            display: inline-block;
            content: "\f005";
            }

            .rating > .half:before { 
            content: "\f089";
            position: absolute;
            }

            .rating > label { 
            color: #ddd; 
            float: right; 
            }

            /***** CSS Magic to Highlight Stars on Hover *****/

            .rating > input:checked ~ label, /* show gold star when clicked */
            .rating:not(:checked) > label:hover, /* hover current star */
            .rating:not(:checked) > label:hover ~ label { color: #FFD700;  } /* hover previous stars in list */

            .rating > input:checked + label:hover, /* hover current star when changing rating */
            .rating > input:checked ~ label:hover,
            .rating > label:hover ~ input:checked ~ label, /* lighten current selection */
            .rating > input:checked ~ label:hover ~ label { color: #FFED85;  } 
        </style>
    </head>
    <body>
        <div class="container-fluid header_bg" style="height:960px;">
            <div class="container">
                
                <div class="row">
                    <div class="col-md-18">
                        <img src="<?php echo base_url(); ?>theme/firstJob/image/logo.png" class="img-responsive center-block logo_padd"  />
                    </div>
                </div>    

                <div class="clearfix"></div>
				
                <div class="col-md-12" style="width: 100%;">
				<center>
					<div  class="col-md-4" style="color:#FFF">User - <?php echo $interview['fullname']; ?>
					<input type="hidden" name="RequestedUser" id="RequestedUser" value="<?php echo $interview['emailIdDecrypted'] ?>"></div>
					<div  class="col-md-4" style="color:#FFF">Job - <?php echo $interview['Job']; ?></div>
					<div  class="col-md-4" style="color:#FFF">Interview Date - <?php $date = explode(" ",$interview['interviewDate']); echo isset($interview['interviewDate'])?date('F d, Y', strtotime($date[0])):""; ?></div>
					<!-- <div  class="col-md-3" style="color:#FFF">Forwarded By - <?php echo $interview['RequestSentBy']; ?></div>-->
					
				</center>
				
				</div>
				<div class="col-md-12">
					<center style="color:green;margin-top: 21px;">
						<?php echo isset($success)?$success:"" ?>
					</center>
				</div>
                <div class="col-md-12" style="margin-top: 50px;width: 100%;">
				
                <div class="col-md-6" style="width: 50%;color:white;">
                    <?php
					if($interview['videoLink'] != "" && $interview['videoLink'] != 0){
					?>
                    <script type="text/javascript">
                        function flashObject(url, id, width, height, version, bg, flashvars, params, att)
                        {
                            var pr = '';
                            var attpr = '';
                            var fv = '';
                            var nofv = 0;
                            for(i in params)
                            {
                                pr += '<param name="'+i+'" value="'+params[i]+'" />';
                                attpr += i+'="'+params[i]+'" ';
                                if(i.match(/flashvars/ig))
                                {
                                    nofv = 1;
                                }
                            }
                            if(nofv==0)
                            {
                                fv = '<param name="flashvars" value="';
                                for(i in flashvars)
                                {
                                    fv += i+'='+escape(flashvars[i])+'&';
                                }
                                fv += '" />';
                            }
                            htmlcode = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"  codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=5,0,0,0" width="'+width+'" height="'+height+'">'
                        +'  <param name="movie" value="'+url+'" />'+pr+fv
                        +'  <embed src="'+url+'" width="'+width+'" height="'+height+'" '+attpr+'type="application/x-shockwave-flash" pluginspage="http://www.adobe.com/go/getflashplayer"></embed>'
                        +'</object>';
                            document.getElementById(id).innerHTML=htmlcode;
                        }
                    </script>
                    <?php
                    if($this->session->userdata('interviewError')['interviewVideo']==$interview['userJobShare']) {
                    ?>
                    <span style="color: rgb(248, 146, 103); font-size: 20px;"><?php echo $this->session->userdata('interviewError')['error']; ?></span>
                    <?php
                    }
                    ?>
                    
                    
                    <div id="player_32931" style="display:inline-block;">
                        <a href="https://get.adobe.com/flashplayer/">You need to install the Flash plugin</a>
                    </div>
                    <script type="text/javascript">
                        var flashvars_32931 = {};
                        var params_32931 = {
                            quality: "high",
                            wmode: "transparent",
                            bgcolor: "#ffffff",
                            allowScriptAccess: "always",
                            allowFullScreen: "true",
                            flashvars: "fichier=https://s3.amazonaws.com/videoMerge/<?php echo $interview['videoLink']; ?>.flv&apercu=https://firstjob.co.in/admin/uploads/bg_black.png"
                        };
                        var attributes_32931 = {};
                        flashObject("https://firstjob.co.in/admin/uploads/v1_27.swf", "player_32931", "500", "350", "8", false, flashvars_32931, params_32931, attributes_32931);
                    </script>
                
                <?php
				}else{
					
					$questionsListDetails = $interview['questionsListDetails'];
					if($questionsListDetails[0]->questionFile == "" || $questionsListDetails[0]->questionFile == "0"){
									$content = "";
									$content .= "<div id='videoContent' style='    float: left;width: 80%;'>
												<div id='form-group'>";
									$content .= "<input type='hidden' name='totalQuestion' value='".count($questionsListDetails)."' id='totalQuestion'>
									<div style='    font-weight: bold;'>Question</div>";
									$counterForInput = 0;
									
									$content .= "<div style='margin-top: 10px;'>
									<label id='textQuiestionFlow' style='    width: 100%;padding: 10px;    border: 1px solid white ;' title='Question Text'></label>
									<select class='form-control' name='questionList' onchange='getVideoAnswers(this.value)' id='questionListDropdownMenu' title='Question Text' style='display:none;' disabled>";
									$i = 0;
									foreach($questionsListDetails as $details){
											if($i == 0){
												$AnswerFirst = $details->answerId;
											}
											$i++;
													$content .= "<option value='".$details->answerId."'>".$details->question."</option>";
												
									}
									$content .= "</select></div>";
									
									$content .= "<ul id='playlist' style='display:none;'>";
									$counter = 0;
									foreach($questionsListDetails as $details){
											if($counter == 0){
												$firstVideo = $details->answer;
											}
											$content .= "<li id='listofPlaylist_".$details->answerId."' class='playlistli'><a id='".$details->answerId."' href='https://s3.amazonaws.com/fjinterviewanswers/".$details->answer."' ></a></li>";
											$counter++;
												
									}
									$content .= "</ul>";
									
									foreach($questionsListDetails as $details){
											if($counter == 0){
												$firstVideo = $details->answer;
											}
											$content .= "<input type='hidden' name='videoAnswers' id='answerHideen".$details->answerId."' value='".$details->answer."' >";
											$counter++;
												
									}
									
									$content .=	"</div><div style='    font-weight: bold;    margin-top: 30px;'>Candidate's Response</div>
												<div id='answerContent' style='    padding: 11px;border: 1px solid white;margin-top: 17px;' >
												<video id='videoShowAnswerbox' width='400' height='200' src='https://s3.amazonaws.com/fjinterviewanswers/".$firstVideo."' type='video/mp4' controls>
												</video>
												</div>
												</div>";
									
									$content .= '<div style="    width: 20%;float:left;    margin-top: 5%;">';
									for($j =1; $j<=count($questionsListDetails);$j++){
										if($j == 1){
											$background = '#00A2E8';
										}else{
											$background = 'none';
										}
										$AnswerId = $questionsListDetails[$counterForInput]->answerId; 
											$AnswerQuestionToolttip = $questionsListDetails[$counterForInput]->question; 
											$content .= "<input type='button' style='margin-left: 7px;background:".$background.";color: white;    float: left;    padding: 5px;  ' class='questionlistbutton' onclick='getVideoAnswers(".$AnswerId.")' id='qustionsCounterDropDown_".$AnswerId."' name='".$j."'  value='".$j."'  title='".$AnswerQuestionToolttip."'>";
										$counterForInput++;
												
									}
									$content .= "</div>";
									
									$content .= "<script>$(document).ready(function() {
													init();
													
													
													
													
												});</script>";
									echo $content;
								}else{
									$content = "";
									$content .= "<div id='videoContent' style=' float:left;   width: 80%;'><div id='form-group'>";
									$content .= "<input type='hidden' name='totalQuestion' value='".count($questionsListDetails)."' id='totalQuestion'>
									<div style='    font-weight: bold;'>Question</div>";
									/*
									$counterForInput = 0;
									for($j =1; $j<=count($questionsListDetails);$j++){
										$AnswerQuestionToolttip = $questionsListDetails[$counterForInput]->question;
											$counterForInput++;		
											$content .= "<input type='button' style='margin-right: 7px;' class='questionlistbutton' onclick='showQuestionCounter(".$j.")' id='qustionsCounterDropDown_".$j."' name='".$j."'  value='".$j."' title='".$AnswerQuestionToolttip."'>";
												
									}
									
									
									
									$content .= "<div style='    margin-top: 10px;'><input type='checkbox' name='ignoreQuestion' id='ignoreQuestion' value='0' checked>&nbsp;Skip Question Videos</div>";
									*/
									$counter = 1;
									foreach($questionsListDetails as $details){
											if($counter == 1){
												$firstVideo = $details->answer;
											}
											$content .= '<div id="textArea_'.$counter.'" class="textArea" style="display:none;"><div id="QuestionText" style="overflow: auto;    height: 48px;padding: 10px;font-size: 18px;" title="Question Text">'.$details->question.'</div><div id="questionFiles" style="margin-top:10px;padding: 11px;border: 1px solid white;"  ><video id="myQuestion_'.$counter.'" width="400" height="200" src="https://firstjob.co.in/admin/uploads/questions/video/'.$details->questionFile.'" controls></video></div>
											
											<div style="    font-weight: bold;    margin-top: 30px;">Candidate\'s Response</div><div id="videoAnswers"  style="padding: 11px;border: 1px solid white;margin-top: 24px;" ><video id="myAnswer_'.$counter.'" width="400" height="200" src="https://s3.amazonaws.com/fjinterviewanswers/'.$details->answer.'" controls></video>
											</div></div>';
											$counter++;
												
									}
									$content .= "</div></div>";
									$content .= '<div style="    width: 20%;float:left;    margin-top: 11%;">';
									$content .= '<div style="    margin-top: 10px;"><input type="checkbox" name="ignoreQuestion" id="ignoreQuestion" value="0" checked>&nbsp;<span style="font-size: 11px;">Skip Question Videos</span></div>';
									
									$counterForInput = 0;
									for($j =1; $j<=count($questionsListDetails);$j++){
										$AnswerQuestionToolttip = $questionsListDetails[$counterForInput]->question;
											$counterForInput++;		
											$content .= "<input type='button' style='margin-right: 7px;    background: none;    margin: 4px;color: white;    float: left;    padding: 5px;' class='questionlistbutton' onclick='showQuestionCounter(".$j.")' id='qustionsCounterDropDown_".$j."' name='".$j."'  value='".$j."' title='".$AnswerQuestionToolttip."'>";
												
									}
									$content .= '</div>';

									
									
									$content .= "<script>$(document).ready(function() {
													$('#textArea_1').css('display','block');
													$('#qustionsCounterDropDown_1').css('background','#00A2E8');
													video = document.getElementById('myQuestion_1');
													Answervideo = document.getElementById('myAnswer_1');
													
													if($('#ignoreQuestion').prop('checked') == true){
														//$(Answervideo).trigger('play');
													}else{
														//$(video).trigger('play');
													}
													
													video.onended = function(e) {
														$('#myAnswer_1').trigger('play');
													};
													Answervideo.onended = function(e) {
														showNextAuto(1);
													};
												});</script>";
									echo $content;
								}
				}

                ?>
				</div>
                <div class="col-md-6" style="width: 50%;">
                        <span class="text-center txt_white font_size10" style="color:#810000;" ><?php if(isset($interview['error']) && !empty($interview['error'])) echo $interview['error']['error']; ?></span>
                        <form method="post" id="shareInterviewReceivingForm"  action="<?php echo base_url();?>/page/interviewComment">                        
                            <table class="table" border="0" bordercolor="#CCCCCC" style="width:400px;">
                                <thead class="tbl_head">
                                    <tr>
                                        <th colspan="2" class="text-center txt_white font_size20">Share Your Feedback</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td valign="middle"><label for="inputEmail" class="control-label txt_white required" style="font-weight:500">First Name</label><span style="color:red; padding-left: 5px;">*</span></td>
                                        <td><input type="text" class="form-control receiverEmail required" id="fullname" name="fullname" placeholder="Enter your First name" maxlength="25" required="required"></td>
                                    </tr>
                                    <tr>
                                        <td><label for="inputEmail" class="control-label txt_white" style="font-weight:500">Last Name</label></td>
                                        <td><input type="text" class="form-control receiverEmail required" id="lastname" name="lastname" placeholder="Enter your last name" maxlength="25"></td>
                                    </tr>
                                    <tr>
                                        <td><label for="inputEmail" class="control-label txt_white" style="font-weight:500">Confirm Your Email Id</label><span style="color:red; padding-left: 5px;">*</span></td>
                                        <td>
                                            <input type="email" value="<?php echo $interview['emailIdDecrypted'] ?>" class="form-control receiverEmail required"  id="emailid" name="emailid" placeholder="Enter a valid email id" maxlength="50" required="required" readonly>
                                            <input type="hidden" class="required" id="eFlag" name="eFlag" value="<?php echo $interview['emailId']; ?>">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label for="inputEmail" class="control-label txt_white" style="font-weight:500">Rating</label><span style="color:red; padding-left: 5px;">*</span></td>
                                        <td>
											<div>
												<fieldset class="rating">
													<input type="radio" id="star5" name="rating" value="5" /><label class = "full" for="star5" title="Excellent"></label>
													<input type="radio" id="star4half" name="rating" value="4.5" /><label class="half" for="star4half" title="Very Good"></label>
													<input type="radio" id="star4" name="rating" value="4" /><label class = "full" for="star4" title="Very Good"></label>
													<input type="radio" id="star3half" name="rating" value="3.5" /><label class="half" for="star3half" title="Good"></label>
													<input type="radio" id="star3" name="rating" value="3" /><label class = "full" for="star3" title="Good"></label>
													<input type="radio" id="star2half" name="rating" value="2.5" /><label class="half" for="star2half" title="Fair"></label>
													<input type="radio" id="star2" name="rating" value="2" /><label class = "full" for="star2" title="Fair"></label>
													<input type="radio" id="star1half" name="rating" value="1.5" /><label class="half" for="star1half" title="Bad"></label>
													<input type="radio" id="star1" name="rating" value="1" /><label class = "full" for="star1" title="Bad"></label>
													<input type="radio" id="starhalf" name="rating" value="0.5" /><label class="half" for="starhalf" title="Bad"></label>
												</fieldset>
											</div>
											<div style="clear:both"></div>
											<div id="errorRating" style="color:red;display:none;font-size:12px;">
												Rating Required
											</div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label for="inputEmail" class="control-label txt_white" style="font-weight:500">Comment</label><span style="color:red; padding-left: 5px;">*</span></td>
                                        <td>
                                            <textarea type="text" class="form-control receiverEmail required"  id="comment" name="comment" placeholder="Enter your comment here" maxlength="200" required="required"></textarea>                                            
                                            <input type="hidden" class="required" id="userJobShare" name="userJobShare" value="<?php echo $interview['userJobShare']; ?>">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="right">
                                            <span id="formButton"><button type="submit" class="Save_frm" onclick="submitFeedback()" id="sendInterview">&emsp;Submit&emsp;</button></span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </form>
                </div>   
<div class="clearfix"></div>   	
</div>			
                <?php
                //}
                ?>

                <script type="text/javascript" src="<?php echo base_url(); ?>theme/firstJob/js/jquery-1.12.4.js"></script>
                <script type="text/javascript" src="<?php echo base_url(); ?>theme/firstJob/js/bootstrap.js"></script>
				<script>
	$(document).ready(function() {
       var height = $(window).height();
	   $(".header_bg").css("height",height);
	});
	$('#shareInterviewReceivingForm').submit(function() {
	  var radioValue = $('input[name=rating]:checked').val();
		if(radioValue > 0){
			$("#shareInterviewReceivingForm").submit();
		}else{
			$("#errorRating").css("display","block");
			return false;
		}
	});
	
	function init(){
		var Answer = $("#questionListDropdownMenu").val();
		$('#questionListDropdownMenu').val(Answer);
		
		var text = $("#questionListDropdownMenu option[value='"+Answer+"']").text();
		$("#textQuiestionFlow").text(text);
		var SingleVideoAnswer = document.getElementById('videoShowAnswerbox');
		SingleVideoAnswer.onended = function(e) {
			var Answer = $("#questionListDropdownMenu").val();
			$('#questionListDropdownMenu option:selected').next().attr('selected', 'selected');
			var NextAnswer = $("#questionListDropdownMenu").val();
			if(NextAnswer == Answer){
			}else{
				getVideoAnswers(NextAnswer);
			}
		};
		
	}
	
	
	
	function getVideoAnswers(valueSelected){
		var Answer = $("#answerHideen"+valueSelected).val();
		//alert(Answer);
		
		$("#videoShowAnswerbox").attr('src',"https://s3.amazonaws.com/fjinterviewanswers/"+Answer);
		$("#videoShowAnswerbox").trigger('play');
		
		$( ".questionlistbutton" ).each(function() {
			$(this).css("background","none");
		});
		$('#qustionsCounterDropDown_'+valueSelected).css("background", "#00A2E8");
		$('#questionListDropdownMenu').val(valueSelected);
		var text = $("#questionListDropdownMenu option[value='"+valueSelected+"']").text();
		$("#textQuiestionFlow").text(text);
		var SingleVideoAnswer = document.getElementById('videoShowAnswerbox');
		SingleVideoAnswer.onended = function(e) {
			var Answer = $("#questionListDropdownMenu").val();
			$('#questionListDropdownMenu option:selected').next().attr('selected', 'selected');
			var NextAnswer = $("#questionListDropdownMenu").val();
			if(NextAnswer == Answer){
			}else{
				getVideoAnswers(NextAnswer);
			}
		};
		//$("#listofPlaylist_"+valueSelected).addClass('active');
		
	}
	
	
	function showPrevious(counter){
		var counterNew = counter-1;
		var nextToken = counterNew-1;
		var totalQuestion = $("#totalQuestion").val();
		if(counterNew < 1){
			$("#butttonprevious"+counter).css("color","grey");	
		}else{
			if(nextToken < 1){
				$("#butttonprevious"+counterNew).css("color","grey");	
			}
			$("#textArea_"+counter).css("display","none");
			$("#textArea_"+counterNew).css("display","block");
			$( ".questionlistbutton" ).each(function() {
				$(this).css("background","none");
			});
			$('#qustionsCounterDropDown_'+counterNew).css("background", "#00A2E8");
		}
	}
	var video = "";
	video.onended = function(e) {
		alert("video ended");
	};
	
	
	function showNext(counter){
		
		var counterNew = counter+1;
		var nextToken = counterNew+1;
		var totalQuestion = $("#totalQuestion").val();
		if(counterNew > totalQuestion){
			$("#butttonnext"+counter).css("color","grey");	
		}else{
			
			if(nextToken == totalQuestion){
				$("#butttonnext"+counterNew).css("color","grey");	
			}
			$("#textArea_"+counter).css("display","none");
			$("#textArea_"+counterNew).css("display","block");
			$( ".questionlistbutton" ).each(function() {
				$(this).css("background","none");
			});
			video = document.getElementById('myQuestion_'+counterNew);
			$('#qustionsCounterDropDown_'+counterNew).css("background", "#00A2E8");
		}
		
	}
	
	function showNextAuto(counter){
		
		var counterNew = counter+1;
		var nextToken = counterNew+1;
		var totalQuestion = $("#totalQuestion").val();
		if(counterNew > totalQuestion){
			$("#butttonnext"+counter).css("color","grey");	
		}else{
			
			if(nextToken == totalQuestion){
				$("#butttonnext"+counterNew).css("color","grey");	
			}
			$("#textArea_"+counter).css("display","none");
			$("#textArea_"+counterNew).css("display","block");
			$( ".questionlistbutton" ).each(function() {
				$(this).css("background","none");
			});
			video = document.getElementById('myQuestion_'+counterNew);
			Answervideo = document.getElementById('myAnswer_'+counterNew);
			
			if($("#ignoreQuestion").prop('checked') == true){
				$(Answervideo).trigger('play');
			}else{
				$(video).trigger('play');
			}
			
			
			
			video.onended = function(e) {
				$(Answervideo).trigger('play');
			};
			Answervideo.onended = function(e) {
				showNextAuto(counterNew);
			};
			$('#qustionsCounterDropDown_'+counterNew).css("background", "#00A2E8");
		}
		
	}
	
	function showQuestionCounter(counter){
		
		$( ".textArea" ).each(function() {
			$(this).css("display","none");
		});
		$( ".questionlistbutton" ).each(function() {
			$(this).css("background","none");
		});
		
		$( "video" ).each(function() {
			
			$(this).trigger('pause');
		});
		
		$("#textArea_"+counter).css("display","block");
		$("#qustionsCounterDropDown_"+counter).css("background", "#00A2E8");
		
		video = document.getElementById('myQuestion_'+counter);
		Answervideo = document.getElementById('myAnswer_'+counter);
		
		if($("#ignoreQuestion").prop('checked') == true){
			$(Answervideo).trigger('play');
		}else{
			$(video).trigger('play');
		}
		
		
		
		video.onended = function(e) {
			$(Answervideo).trigger('play');
		};
		Answervideo.onended = function(e) {
			//showNextAuto(counter);
		};
		
		
	}
				</script>
            </div>
        </div>  
    </body>
</html>

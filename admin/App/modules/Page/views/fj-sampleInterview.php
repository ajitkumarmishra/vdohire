<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Firstjob Help</title>
        <script src="<?php echo base_url(); ?>theme/firstJob/js/jquery.js"></script>
        <script language="javascript" type="text/javascript">
        $('.iframe-full-height').on('load', function(){
            this.style.height=this.contentDocument.body.scrollHeight +'px';
        });
        </script>
        <link href="<?php echo base_url(); ?>theme/firstJob/css/public_page_style.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url(); ?>theme/firstJob/css/bootstrap-theme.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url(); ?>theme/firstJob/css/bootstrap.css" rel="stylesheet" type="text/css">
        <style>
            label {
                display: inline-block;
                max-width: 100%;
                margin-bottom: 5px;
                font-size: 10px;
                font-weight: normal;
            }
        </style>
    </head>
    <body>
        <center>
        <p style="margin: 5% 0%;">
            <h4>Interview Tips</h4>
            <iframe src="https://www.youtube.com/embed/rQwanxQmFnc?modestbranding=1&autoplay=0&controls=0&rel=0&title=" frameborder="0" allowFullScreen></iframe>
            </br>
            <label>Credits - Video uploaded on YouTube by NTDTraining Videos</label>
        </p>        
        <p style="margin-bottom: 5%;">
            <h4>Job interview tips</h4>
            <iframe src="https://www.youtube.com/embed/KjR9WLDZeDA?modestbranding=1&autoplay=0&controls=0&rel=0&title=" frameborder="0" allowFullScreen></iframe>
            </br>
            <label>Credits - Video uploaded on YouTube by Mittan Gupta</label>
        </p>
        <p style="margin-bottom: 5%;">
            <h4>Impress Your Fresher Job Interviewer</h4>
            <iframe src="https://www.youtube.com/embed/-7a9inDMw90?modestbranding=1&autoplay=0&controls=0&rel=0&title=" frameborder="0" allowFullScreen></iframe>
            </br>
            <label>Credits - Video uploaded on YouTube by Neha Sharma</label>
        </p>
        <p style="margin-bottom: 5%;">
            <h4>How to introduce yourself in a job interview</h4>
            <iframe src="https://www.youtube.com/embed/t2u5mkc5UEk?modestbranding=1&autoplay=0&controls=0&rel=0&title=" frameborder="0" allowFullScreen></iframe>
            </br>
            <label>Credits - Video uploaded on YouTube by Ankit Kalonia</label>
        </p>
        <p style="margin-bottom: 5%;">
            <h4>How To Face Interviews For Freshers</h4>
            <iframe  src="https://www.youtube.com/embed/vbGfFBPkpR8?modestbranding=1&autoplay=0&controls=0&rel=0&title=" frameborder="0" allowFullScreen></iframe>
            </br>
            <label>Credits - Video uploaded on YouTube by MoviesDabba</label>
        </p>
        </center>
    </body>
</html>

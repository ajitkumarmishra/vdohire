<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>VDOHire Help</title>
        <script src="<?php echo base_url(); ?>theme/firstJob/js/jquery.js"></script>
        <script language="javascript" type="text/javascript">
            $('.iframe-full-height').on('load', function(){
                this.style.height=this.contentDocument.body.scrollHeight +'px';
            });
        </script>
    </head>
    <body>        
        <iframe frameborder="0" style="overflow: hidden; height: 100%; width: 100%; position: absolute;" height="100%" width="100%" src="https://docs.google.com/viewer?url=https://vdohire.com/admin/uploads/docs/Help_Document_for_VDOHire_App_V2.0.pdf&embedded=true"></iframe>
    </body>
</html>

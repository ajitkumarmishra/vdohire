<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Firstjob User Interview</title>
        <script src="<?php echo base_url(); ?>theme/firstJob/js/jquery.js"></script>
        <link href="<?php echo base_url(); ?>theme/firstJob/css/public_page_style.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url(); ?>theme/firstJob/css/bootstrap-theme.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url(); ?>theme/firstJob/css/bootstrap.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>/theme/css/firstJob/global.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>/theme/css/firstJob/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>/theme/css/firstJob/responsive.css">
        <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700' rel='stylesheet' type='text/css'>
        <script src="<?php echo base_url(); ?>/theme/js/firstJob/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>/theme/js/firstJob/bootstrap.js"></script>
        <script src="<?php echo base_url(); ?>/theme/js/firstJob/bootbox.min.js"></script>
        <style>
            @import url(//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css);
            .rating { 
            border: none;
            float: left;
            }

            .rating > input { display: none; } 
            .rating > label:before { 
            margin: 5px;
            font-size: 1.25em;
            font-family: FontAwesome;
            display: inline-block;
            content: "\f005";
            }

            .rating > .half:before { 
            content: "\f089";
            position: absolute;
            }

            .rating > label { 
            color: #ddd; 
            float: right; 
            }

            /***** CSS Magic to Highlight Stars on Hover *****/

            .rating > input:checked ~ label, /* show gold star when clicked */
            .rating:not(:checked) > label:hover, /* hover current star */
            .rating:not(:checked) > label:hover ~ label { color: #FFD700;  } /* hover previous stars in list */

            .rating > input:checked + label:hover, /* hover current star when changing rating */
            .rating > input:checked ~ label:hover,
            .rating > label:hover ~ input:checked ~ label, /* lighten current selection */
            .rating > input:checked ~ label:hover ~ label { color: #FFED85;  } 
        </style>
    </head>
	  <body>
	  <div class="container-fluid" style="background:#4b9bb5;color:white;height:900px;">
	<div style="margin-left: 1%;    width: 960px;
    margin: 0 auto;">
	<center><h2>Video Resume</h2></center>
<?php

?><script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


<div class="container" style="width:100%">
	<div class="row">
	<div class="col-md-6 pull-left">
      <div class="table-responsive">
        <table class="table table-hover" >
          <thead>
		  <?php //echo "<pre>";print_r($contentAuditionSet);die;
			foreach($contentAuditionSet as $audition){ ?>
            <tr>
              <td>Name</td><td><?php echo $audition->fullname; ?></td>
			</tr>
            <tr>  <td>Email</td><td><?php echo $audition->email; ?></td></tr>
             <tr><td>Phone No</td><td><?php echo $audition->mobile; ?></td></tr>
              
			<?php }
			
			?>
          </thead>
          
        </table>   
      </div>
      </div>
      <div class="col-md-6 text-center pull-right">
		Questions: <select class="selectpicker" id="questionDropdown" onchange="showAuditionForQuestion()" style="color:black;    margin-top: 20px;margin-bottom: 27px;width: 263px;">
			<option value="0">Tell us something about yourself?</option>
			<option value="1">Tell us about your strengths?</option>
			<option value="2">Tell us about your weakness?</option>
			<option value="3">Tell us about any of your project/work that you are proud of?</option>
		</select>
		<div class="col-md-12 text-center">
		<?php
		$auditionArray = explode(",",$contentAuditionSet[0]->auditionFiles);
		if(!empty($auditionArray)){
			for($count = 0;$count<=count($auditionArray);$count++){?>
				<?php
				if($count == 0){ ?>
					
					<div id="audition_<?php echo $count ?>" style="display:block;">
						<video id="auditionVideo_<?php echo $count; ?>" style="width:400px;" src='https://s3.amazonaws.com/fjauditions/<?php echo trim($auditionArray[$count]) ?>' controls>
					</div>
				<?php }else{
				?>
					<div id="audition_<?php echo $count ?>" style="display:none">
						<video id="auditionVideo_<?php echo $count; ?>" style="width:400px;"  src='https://s3.amazonaws.com/fjauditions/<?php echo trim($auditionArray[$count]) ?>' controls>
					</div>
				<?php } ?>
			<?php }
		
		}
		?>
	  </div>
	  </div>
	  
	</div>
	<div class="col-md-12 text-center" style="margin-top: 40px;
">
	*Change questions from the dropdown to view the answer of that question.
	</div>
<div class="modal fade" id="messageModal">
    <div class="modal-dialog">
        <div class="modal-content">
		<form id="messageReplyBox">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <div class="modal-title" style="float:left;"><b>Duration:</b></div><div id="Auditionduration" class="pull-left"></div>
				<div style="clear:both"></div>
            </div>
            <div class="modal-body" style="    padding-top: 0px;">
			
                
				<center><div id="auditionVideoPlayer" style="width: 500px;"></div></center>
            </div>
            <div class="modal-footer">
            </div>      
			</form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="imageModal">
    <div class="modal-dialog" style="width:80%;">
        <div class="modal-content">
		<form id="messageReplyBox">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <div class="modal-title" style="float:left;"><b>Duration:</b></div><div id="Auditionduration" class="pull-left"></div>
				<div style="clear:both"></div>
            </div>
            <div class="modal-body" id="auditionImages" style="    padding-top: 0px;">
			
                
				
            </div>
            <div class="modal-footer">
            </div>      
			</form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
</div>
</div>
</div>
</body>
</html>
<script>
function showAudition(id){
	//$("#iconLoader").css("display","block");
	$.ajax({
          url : "<?php echo base_url() ?>interviews/getAuditionVideo",
          data: 'id='+id,
          type: "POST",
          success: function(response){
				$("#auditionVideoPlayer").html(response);
          }
    });
	
	//$("#iconLoader").css("display","none");
}


function showTextfile(textFile){
	$("#iconLoader").css("display","block");
	$.ajax({
          url : "<?php echo base_url() ?>audition/getAuditionImages",
          data: 'file='+textFile,
          type: "POST",
          success: function(response){
                $("#auditionImages").html(response);
				$("#iconLoader").css("display","none");
            }
    });
}

function submitHandrating(id,userId){
	$("#iconLoader").css("display","block");
	var HumanRatingForVideo = $("#HumanRatingForVideo").val();
	$.ajax({
          url : "<?php echo base_url() ?>audition/saveAuditionHandrating",
          data: 'id='+id+"&HumanRatingForVideo="+HumanRatingForVideo+"&userId="+userId,
          type: "POST",
          success: function(response){
                $("#auditionImages").html(response);
				$("#iconLoader").css("display","none");
            }
    });
}

function showNextFiles(page){
	var q1 = $("#questionDropdown").val();
	$("#pageNumber").val(page);
	$.ajax({
          url : "<?php echo base_url() ?>audition/getNextAuditions",
          data: 'page='+page+'&question='+q1,
          type: "POST",
          success: function(response){
                $("#myTable").html(response);
				$("#iconLoader").css("display","none");
            }
    });
}

function showAuditionForQuestion(){
	$("#iconLoader").css("display","block");
	var q1 = $("#questionDropdown").val();
	var page = $("#pageNumber").val();
	$("#audition_0").css("display","none");
	$("#audition_1").css("display","none");
	$("#audition_2").css("display","none");
	$("#audition_3").css("display","none");
	$("#audition_"+q1).css("display","block");
	
	$("#auditionVideo_0").trigger('pause');
	$("#auditionVideo_1").trigger('pause');
	$("#auditionVideo_2").trigger('pause');
	$("#auditionVideo_3").trigger('pause');
	$("#auditionVideo_"+q1).trigger('play');

}
$( document ).ready(function() {
	$("#auditionVideo_0").trigger('play');
	$('#messageModal').on('hide.bs.modal', function (e) {
		$("#auditionVideoPlayer").attr("src","");
	})
});
</script>
<?php

/*

 */

class Page_model extends CI_Model {
    
    static $tableUserAudition       = "fj_userAudition";
    static $tableUserAuditionLike   = "fj_userAuditionLike";
    
    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('pagination');
    }
    
    function getAudition($id) {
        try {            
            $this->db->select('*');
            $this->db->from(Page_model::$tableUserAudition);
            $this->db->where('id', $id);
            $query = $this->db->get();
            $result = $query->row_array();
            if (count($result) == 0) {
                //throw new Exception('Unable to fetch interview data.');
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
	function listAuditions($userId) {
        try {
            $currentDate = date('Y-m-d h:i:s');
            $sql    = "SELECT u.fullname, u.email, u.mobile, a.* FROM `fj_userAudition` a left join fj_users u on a.userId=u.id  WHERE a.auditionFiles like '%.mp4%' and u.id=".$userId;   
				
            $queryResult = $this->db->query($sql);
            $result = $queryResult->result();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    function addLikeAudtionData($data) {
        $resp = $this->db->insert(Page_model::$tableUserAuditionLike, $data);
        $topicId = $this->db->insert_id();
        if ($resp) {
            return $topicId;
        } else {
            return false;
        }
    }
    
    
    
    
    
    public function getprofileInformation($params) {        
        $userId         = $this->userdata_model->cleanString($params);
        $id             = $this->userdata_model->getJwtValue($userId);
               
        if($id!='') {            
            $row        = $this->userdata_model->queryRowArray("SELECT 
                                                            id,fullname,email,mobile,dob,pinCode,gender,
                                                            preferredLocation,expectedCtcFrom, expectedCtcTo, workExperience,adharCard,languages,image,
                                                            facebookLink,linkedInLink,twitterLink,googlePlusLink
                                                      FROM fj_users WHERE id='$id' AND status='1'");
            //print_r($row); exit;
            if(count($row)>0) {
                // Basic Information
                $basicInfo  = array();
                $rowDataBasicInfo['fullname']   = $row['fullname'];
                $rowDataBasicInfo['email']      = $row['email'];
                $rowDataBasicInfo['mobile']     = $row['mobile'];
                $rowDataBasicInfo['dob']        = $row['dob'];
                $rowDataBasicInfo['pincode']    = $row['pinCode'];
                $rowDataBasicInfo['gender']     = $row['gender'];
                $basicInfo[]                    = $rowDataBasicInfo;
                // Other Information
                $locationList   = array();
                $getAllPreferredLocation    = $this->userdata_model->queryResultArray("SELECT locationName FROM fj_userPreferLocations WHERE userId='$id' AND status='1'");
                if(count($getAllPreferredLocation)>0) {
                    foreach($getAllPreferredLocation as $locationVal){
                        $tempArray['name']  = trim($locationVal['locationName']);
                        $locationList[]     = $tempArray;
                    }
                }
                $otherInfo  = array();
                $rowDataOtherInfo['preferredLocation']  = $locationList;
                $rowDataOtherInfo['expectedCtcFrom']    = $row['expectedCtcFrom'];
                $rowDataOtherInfo['expectedCtcTo']      = $row['expectedCtcTo'];
                $rowDataOtherInfo['workExperience']     = $row['workExperience'];
                $rowDataOtherInfo['adharCard']          = $row['adharCard'];
                $rowDataOtherInfo['languages']          = $row['languages'];
                $rowDataOtherInfo['image']              = ($row['image']!=''?base_url() . '/uploads/userImages/' . $row['image']:'');
                $otherInfo[]                            = $rowDataOtherInfo;
                // Social Information
                $socialInfo = array();
                $rowDataSocialInfo['facebookLink']      = $row['facebookLink'];
                $rowDataSocialInfo['linkedInLink']      = $row['linkedInLink'];
                $rowDataSocialInfo['twitterLink']       = $row['twitterLink'];
                $rowDataSocialInfo['googlePlusLink']    = $row['googlePlusLink'];
                $socialInfo[]                           = $rowDataSocialInfo;
                // Educational Information
                $educationalInfo = array();
                $qualificationResults   = $this->userdata_model->queryResultArray("SELECT Q . * , C.id as courseId, C.name as courseName, U.name as adminUniversityName FROM fj_userQualification Q JOIN fj_university U, fj_courses C WHERE Q.universityId = U.id AND Q.courseId=C.id AND Q.userId='$id' AND Q.status='1'");
                foreach($qualificationResults as $row) {
                    $rowDataEducationalInfo['id']               = $row['id'];
                    $rowDataEducationalInfo['courseId']         = $row['courseId'];
                    $rowDataEducationalInfo['courseName']       = $row['courseName'];
                    $rowDataEducationalInfo['universityId']     = $row['universityId'];
                    $rowDataEducationalInfo['universityId']     = $row['universityId'];
                    $rowDataEducationalInfo['universityName']   = ($row['universityId']=='287'?$row['universityName']:$row['adminUniversityName']);
                    $rowDataEducationalInfo['institute']        = $row['institute'];
                    $rowDataEducationalInfo['completionYear']   = $row['completionYear'];
                    $rowDataEducationalInfo['percent']          = $row['percent'];
                    $educationalInfo[]                          = $rowDataEducationalInfo;
                }                
                // Audition Information
                $auditionInfo   = array();
                $auditionRow    = $this->userdata_model->queryRowArray("SELECT * FROM fj_userAudition WHERE userId='$id' AND status='1'");
                $auditionInfo   = (count($auditionRow)>0?$auditionRow['audition_file']:'');
                
                $auditionMergeFile   = (count($auditionRow)>0?$auditionRow['audition_file']:'');
                $auditionFileNames   = (count($auditionRow)>0?$auditionRow['auditionFiles']:'');
                
                // Audition Like Information
                if(count($auditionRow)>0) {
                    $auditionLikeInfo   = array();
                    $sql   = "SELECT count(auditionLikeId) AS count FROM fj_userAuditionLike WHERE auditionId='$auditionRow[id]' ";
                    $query  = $this->db->query($sql);
                    $resultAud = $query->result_array();
                    $auditionLikeCount  = $resultAud;
                }
                else {
                    $auditionLikeCount  = 0;
                }
                
                //print_r($resultAud); 
                
                //--------------- Response Data ---------------//
                $this->userdata_model->codeMessage('200', $this->lang->line('success'));                
                userdata_model::$returnArray['basicInfo']           = $basicInfo;
                userdata_model::$returnArray['otherInfo']           = $otherInfo;
                userdata_model::$returnArray['socialInfo']          = $socialInfo;
                userdata_model::$returnArray['educationalInfo']     = $educationalInfo; 
                userdata_model::$returnArray['auditionInfo']        = $auditionInfo;   
                
                userdata_model::$returnArray['auditionMergeFile']   = $auditionMergeFile;                   
                userdata_model::$returnArray['auditionFileNames']   = $auditionFileNames;   
                
                userdata_model::$returnArray['auditionLikeCount']   = $auditionLikeCount;          
                //print_r(userdata_model::$returnArray); exit;                
            }
            else {
                $this->userdata_model->codeMessage('500', $this->lang->line('no_record_found'));
            }
        }
        else {
            $this->userdata_model->codeMessage('300', $this->lang->line('missing_required_fields'));
        }
        return userdata_model::$returnArray;
    }
    
    public function getTickerInformation($params) {
        if($params!='') {
            $tokenId    = decryptURLparam($params, URLparamKey);
            $row        = $this->userdata_model->queryRowArray("SELECT * FROM fj_ticker WHERE tickerId='$tokenId' Limit 0,1");
            //print_r($row); exit;
            if(count($row)>0) {                 
                $this->userdata_model->codeMessage('200', $this->lang->line('success')); 
                userdata_model::$returnArray['tickerDescription']   = $row['tickerDesc'];
            }
            else {
                $this->userdata_model->codeMessage('500', $this->lang->line('no_record_found'));
            }
        }
        else {
            $this->userdata_model->codeMessage('300', $this->lang->line('missing_required_fields'));
        }
        return userdata_model::$returnArray;
    }
    
    public function getKnowledgeCenterInformation($params1, $params2) {
        if($params1!='' && $params2!='') {
            $topicId    = decryptURLparam($params2, URLparamKey);
            //echo $topicId;
            if($params1=='category')
            $row        = $this->userdata_model->queryRowArray("SELECT * FROM fj_knowledgeCenterTopics WHERE status='1' AND categoryId='$topicId' Limit 0,1");
            if($params1=='topic')
            $row        = $this->userdata_model->queryRowArray("SELECT * FROM fj_knowledgeCenterTopics WHERE status='1' AND id='$topicId' Limit 0,1");
            //print_r($row); exit;
            if(count($row)>0) {                 
                $this->userdata_model->codeMessage('200', $this->lang->line('success')); 
                userdata_model::$returnArray['knowledgeDescription']   = $row['description'];
            }
            else {
                $this->userdata_model->codeMessage('500', $this->lang->line('no_record_found'));
            }
        }
        else {
            $this->userdata_model->codeMessage('300', $this->lang->line('missing_required_fields'));
        }
        return userdata_model::$returnArray;
    }
    
    
    
    function addInterviewComment($data) {        
        //print_r($data); exit;
        $resp       = $this->db->insert('fj_userJobComments', $data);        
        $insertId   = $this->db->insert_id();
        if ($resp) {
            $whereData  = array('id'=> $data['userJobShareId']);
            $updateData = array('videoLinkStatus' => "2");
            $updateJob  = $this->db->where($whereData)->update('fj_userJobShare', $updateData);
            //echo $this->db->last_query(); exit;
            return $insertId;
        } else {
            return false;
        }
    }
}
?>



<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : userdata Model
 * Description : Handle all the CRUD operation for userdata
 * @author Synergy
 * @createddate : Nov 2, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 21, 2017
 */
class userdata_model extends CI_Model {

    /**
     * Declaring global Variable
     */
    public static $returnArray;
    public static $data;
    
    /**
     * Responsable for inheriting parent constructor
     * Responsable for auto loading database
     * Responsable for auto load the the email and session library
     * Responsable for initializing global variable
     * @return void
     */
    function __construct() {
        parent::__construct();        
        $this->load->database();
        $this->load->library('email');
        $this->load->library('session');
        userdata_model::$returnArray  = array();
        userdata_model::$data         = array();
    }
    
    /**
     * Description : Filter User Input Data
     * Author : Synergy
     * @param string $string
     * @return htmlEntites
     */
    function cleanString($string) {
        $string         = trim($string);
        $detagged       = strip_tags($string);
        $stripped       = stripslashes($detagged);
        $htmlEntites    = htmlentities($stripped, ENT_QUOTES, 'UTF-8');
        return $htmlEntites;
    }

    /**
     * Description : Convert User ID to JWT Token
     * Author : Synergy
     * @param string $string
     * @return string
     */
    function getJwtToken($userId) {
        $token          = array();
        $token['id']    = $userId;
        $jwtKey         = $this->config->item('jwtKey');
        $secretKey      = base64_decode($jwtKey);
        $signatureAlgo  = $this->config->item('signatureAlgorithm');
        return JWT::encode($token, $secretKey, $signatureAlgo);
    }
    
    /**
     * Description : Convert User ID to JWT Token
     * Author : Synergy
     * @param string $string
     * @return string jwt value
     */
    function getJwtValue($userToken) {
        
        $jwtKey         = $this->config->item('jwtKey');
        $secretKey      = base64_decode($jwtKey);
        $signatureAlgo  = $this->config->item('signatureAlgorithm');
        try {
            $token = JWT::decode($userToken, $secretKey, $signatureAlgo);
        } catch (Exception $ex) {
            coreapi_model::codeMessage('500', $e->getMessage());
        }
        return $token->id;
    }
    
    /**
     * Description : Generate Response Message with HTTP Code
     * Author : Synergy
     * @param int $code, string $message
     * @return array
     */
    function codeMessage($code, $message){
        userdata_model::$returnArray['code']    = $code;
        userdata_model::$returnArray['message'] = $message;
        return userdata_model::$returnArray;
    }
    
    /**
     * Description : Fetch All Record From DB
     * Author : Synergy
     * @param string $query
     * @return array with data
     */
    function queryResultArray($query){
        try {
            $query  = $this->db->query($query);
            try{
                $result = $query->result_array();
                return $result;
            } catch (Exception $e) {
                    userdata_model::codeMessage('500', $e->getMessage());
            }
        } catch (Exception $e) {
            userdata_model::codeMessage('500', $e->getMessage());
        }
    }
    
    /**
     * Description : Fetch A Single Row From DB
     * Author : Synergy
     * @param string $query
     * @return array with data
     */
    function queryRowArray($query){
        try {
            $query  = $this->db->query($query);
            try{
                $result = $query->row_array();
                return $result;
            } catch (Exception $e) {
                    userdata_model::codeMessage('500', $e->getMessage());
            }
        } catch (Exception $e) {
            userdata_model::codeMessage('500', $e->getMessage());
        }
    }
    
    /**
     * Description : Check User Exists Or Not
     * Author : Synergy
     * @param int $id
     * @return array of data
     */
    function userExists($id) {
        $userRow    = userdata_model::queryRowArray("SELECT id FROM fj_users WHERE id='$id' AND status='1' LIMIT 0,1");
        return $userRow;
    }
	
	
}

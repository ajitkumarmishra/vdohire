<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Industry Model
 * Description : Handle all the CRUD operation for Industry
 * @author Synergy
 * @createddate : Nov 10, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 20, 2017
 */

class Industry_model extends CI_Model {

    /**
     * Initializing variables
     */
    var $indus_table = "fj_industry";

    /**
     * Responsable for inherit the parent constructor
     * @return void
     */
    function __construct() {
        parent::__construct();
    }

    /**
     * Description : Use to update the status of industry
     * Author : Synergy
     * @param array $_POST
     * @return void
     */
    function updateStatus() {
        if ($this->input->post('Delete') != null) {
            $arr_ids = $this->input->post('arr_ids');
            if (!empty($arr_ids)) {
                $this->db->where_in('id', $arr_ids);
                $this->db->delete($this->indus_table);
            }
        } else {
            if ($this->input->post('Deactivate') != null) {
                $status = "0";
            } else if ($this->input->post('Activate') != null) {
                $status = "1";
            }
            $data = array(
                'is_active' => $status
            );
            $arr_ids = $this->input->post('arr_ids');
            if (!empty($arr_ids)) {
                $this->db->where_in('id', $arr_ids);
                $this->db->update($this->indus_table, $data);
            }
        }
    }

    /**
     * Description : Use to count all the industries
     * Author : Synergy
     * @param array $_POST
     * @return int industry count
     */
    function count_all() {
        $query = $this->db->get($this->indus_table);
        return $query->num_rows();
    }

    /**
     * Description : Use to delete a particular industry
     * Author : Synergy
     * @param int $id
     * @return boolean
     */
    function deleteIndustry($id) {
        $this->db->where('id', $id);
        $query = $this->db->delete($this->indus_table);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Description : Use to create a indystry
     * Author : Synergy
     * @param array $dataIndustry(industryName, industryType, createdAt, updatedAt)
     * @return boolean
     */
    public function createIndustry($dataIndustry) {
        $data = array(
            'industryName' => $dataIndustry['industryName'],
            'industryType' => $dataIndustry['industryType'],
            'createdAt' => $dataIndustry['createdAt'],
            'updatedAt' => $dataIndustry['updatedAt'],
            'isActive' => '1'
        );
        $query = $this->db->insert($this->indus_table, $data);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Description : Use to update a particular industry
     * Author : Synergy
     * @param array $_POST
     * @return boolean
     */
    function updateIndustry($id) {
        $data = array(
            'name' => $this->input->post('name'),
            'is_active' => $this->input->post('is_active'),
        );
        $this->db->where('id', $id);
        $query = $this->db->update($this->indus_table, $data);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Description : Use to get details of a particular industry
     * Author : Synergy
     * @param int $id
     * @return array of data
     */
    function getIndustry($id) {
        $this->db->where('id', $id);
        $this->db->from($this->indus_table);
        $result = $this->db->get();
        return $result->row();
    }
    
    /**
     * Description : Use to get industry details by name
     * Author : Synergy
     * @param string $industryName
     * @return array of data
     */
    function getIndustryByName($industryName) {
        $this->db->where('industryName', $industryName);
        $this->db->where('industryType', 1);
        $this->db->from($this->indus_table);
        $result = $this->db->get();
        return $result->row();
    }
    
    /**
     * Description : Use to list of industry based upon the industry type
     * Author : Synergy
     * @param array $_POST
     * @return array of data
     */
    function getIndustryList($industryType){
        $conditions=array(
            'isActive'=>'1',
            'industryType'=> $industryType
        );
        $this->db->where($conditions);
        $this->db->from($this->indus_table);
        $result = $this->db->get();
        return $result->result();
    }
    
    /**
     * Description : Use to create function in specified industry
     * Author : Synergy
     * @param array $_POST
     * @return boolean
     */
    function createFunction(){
        $data = array(
            'indus_id' => $this->input->post('indus_id'),
            'name' => $this->input->post('name'),
            'is_active' => '1'
        );
        $query = $this->db->insert($this->indus_table, $data);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * Description : Use to update status of a function in specified industry
     * Author : Synergy
     * @param array $_POST
     * @return void
     */
    function updateFunctionStatus() {
        if ($this->input->post('Delete') != null) {
            $arr_ids = $this->input->post('arr_ids');
            if (!empty($arr_ids)) {
                $this->db->where_in('id', $arr_ids);
                $this->db->delete($this->indus_table);
            }
        } else {
            if ($this->input->post('Deactivate') != null) {
                $status = "0";
            } else if ($this->input->post('Activate') != null) {
                $status = "1";
            }
            $data = array(
                'is_active' => $status
            );
            $arr_ids = $this->input->post('arr_ids');
            if (!empty($arr_ids)) {
                $this->db->where_in('id', $arr_ids);
                $this->db->update($this->indus_table, $data);
            }
        }
    }
    
    /**
     * Description : Use to count all the functions in a specified indystry
     * Author : Synergy
     * @param int $id
     * @return array of data
     */
    function count_all_functions($id) {
        $conditions=array(
            'indus_id'=>$id
        );
        $this->db->where($conditions);
        $query = $this->db->get($this->indus_table);
        return $query->num_rows();
    }
    
    /**
     * Description : Use to get limited list of functions in a specified industry
     * Author : Synergy
     * @param int $id, int $showPerpage, int $offset
     * @return array of data
     */
    function getFunctionList($id,$showPerpage, $offset) {
        $conditions=array(
            'indus_id'=>$id
        );
        $this->db->where($conditions);
        $this->db->limit($showPerpage, $offset);
        $this->db->from($this->indus_table);
        $result = $this->db->get();
        return $result->result();
    }
    
    /**
     * Description : Use to get details of a particular function
     * Author : Synergy
     * @param array $_POST
     * @return int industry count
     */
    function getFunction($id) {
        $this->db->where('id', $id);
        $this->db->from($this->indus_table);
        $result = $this->db->get();
        return $result->row();
    }
    
    /**
     * Description : Use to get details of function by function name
     * Author : Synergy
     * @param string $functionName
     * @return array of data
     */
    function getFunctionByName($functionName) {
        $this->db->where('industryName', $functionName);
        $this->db->where('industryType', 2);
        $this->db->from($this->indus_table);
        $result = $this->db->get();
        return $result->row();
    }
    
    /**
     * Description : Use to update the specified function
     * Author : Synergy
     * @param array $_POST
     * @return boolean
     */
    function updateFunction($id) {
        $data = array(
            'indus_id' => $this->input->post('indus_id'),
            'name' => $this->input->post('name')
        );
        $this->db->where('id', $id);
        $query = $this->db->update($this->indus_table, $data);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * Description : Use to get list of functions in a specified industry
     * Author : Synergy
     * @param int $id
     * @return array of data
     */
    function getListOfFunction($id) {
        $this->db->where('indus_id', $id);
        $this->db->from($this->indus_table);
        $result = $this->db->get();
        return $result->result();
    }
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Interview Model
 * Description : Handle all the CRUD operation for Interviews
 * @author Synergy
 * @createddate : Nov 11, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 20, 2017
 */

class Interview_model extends CI_Model {

    /**
     * Initializing variables
     */
    var $table = "interviews";

    /**
     * Responsable for inherit the parent constructor
     * Responsable for auto load the session and pagination library
     * @return void
     */
    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('pagination');
    }

    /**
     * Function Name : addInterview function
     * Discription : Use to creates a new interview set
     * Author : Synergy
     * @params array $data
     * @return boolean
     */
    function addInterview($data) {
        $resp = $this->db->insert('fj_interviews', $data);
        $interviewId = $this->db->insert_id();
        if ($resp) {
            return $interviewId;
        } else {
            return false;
        }
    }

    /**
     * Function Name : AddQuestion function
     * Discription : Creates a question
     * Author : Synergy
     * @params array $data
     * @params int $interview
     * @return boolean
     */
    function addQuestion($data) {
        $resp = $this->db->insert('fj_question', $data);
        $questionId = $this->db->insert_id();
        if ($resp) {
            return $questionId;
        } else {
            return false;
        }
    }
    
    /**
     * Function Name : AddInterviewQuestion function
     * Discription : Use to add questions in interview set
     * Author : Synergy
     * @params array $data
     * @params int $interview
     * @return boolean
     */
    function addInterviewQuestion($data) {

        $resp = $this->db->insert('fj_interviewQuestions', $data);
        $questionId = $this->db->insert_id();
        if ($resp) {
            return $questionId;
        } else {
            return false;
        }
    }

    /**
     * Function Name : fileUpload function
     * Discription : Uploads a file to directory
     * Author : Synergy
     * @param array $files
     * @param string $uploads_dir
     * @param string $imagename
     * @return boolean
     */
    public function fileUpload($files, $uploads_dir, $fileName) {
        try {
            $fileType = pathinfo($fileName, PATHINFO_EXTENSION);
            $tmp_name = $files["tmp_name"];
            if (move_uploaded_file($tmp_name, $uploads_dir . "/" . $fileName)) {
                /*if($fileType=='mp4') {
                    shell_exec("ffmpeg -i ".$uploads_dir."/".$fileName." -vcodec libx264 -crf 24 ".$uploads_dir."/q".$fileName." -y ");
                    unlink($uploads_dir."/".$fileName);
                    $fileName = "q".$fileName;
                }*/
                return $fileName;
            } else {
                //throw new Exception('Could not upload file');
                return $fileName;
            }
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    /**
     * Function Name : UpdateInterview function
     * Discription : Updates a specific interview.
     * Author : Synergy
     * @param array $data.
     * @param int $id.
     * @return boolean.
     */
    function updateInterview($data, $id) {
        try {
            $this->db->where('id', $id);
            $resp = $this->db->update('fj_interviews', $data);
            if (!$resp) {
                throw new Exception('Unable to update interview data.');
                return false;
            }
            return $id;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Function Name : updateQuestion function
     * Discription : Updates details of a specific question.
     * Author : Synergy
     * @param array $data.
     * @param int $id.
     * @return boolean.
     */
    function updateQuestion($data, $id) {
        try {
            $this->db->where('id', $id);
            $resp = $this->db->update('fj_question', $data);
            if (!$resp) {
                throw new Exception('Unable to update question.');
                return false;
            }
            return $id;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Function Name : GetInterview function
     * Discription : Gets a specific interview detail.
     * Author : Synergy
     * @param int $id.
     * @return array the interviews data.
     */
    function getInterview($id) {
        try {
            
            $this->db->select('*');
            $this->db->from('fj_interviews');
            $this->db->where('id', $id);
            $query = $this->db->get();
            $result = $query->row_array();
            if (count($result) == 0) {
                //throw new Exception('Unable to fetch interview data.');
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Function Name : GetQuestion function
     * Discription : Gets a specific question detail.
     * Author : Synergy
     * @param int $id.
     * @return array the question data.
     */
    function getQuestion($id) {
        try {
            
            $this->db->select('*');
            $this->db->from('fj_question');
            $this->db->where('id', $id);
            $query = $this->db->get();
            $result = $query->row_array();
            if (count($result) == 0) {
                //throw new Exception('Unable to fetch interview data.');
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Function Name : getQuestionOnType function
     * Discription : Gets List of questions by specific question type.
     * Author : Synergy
     * @param int $quesType.
     * @return array the question data.
     */
    function getQuestionOnType($quesType) {
        try {
            $userData   = $this->session->userdata['logged_in'];
            $userRole   = $userData->role;
            $userId     = $userData->id;
            
            if($userRole!=1) {                
                $sql        = " SELECT * FROM fj_users WHERE role='1' ";
                $query      = $this->db->query($sql);
                $result     = $query->result_array();
                $createdBy  = '';
                foreach ($result AS $user) {
                    $corporateId    = getCorporateUser($user['id']);
                    $subUserIds     = getCorporateSubUser($corporateId, $user['role']);
                    $subUserIds     = ($subUserIds!=''?','.$subUserIds:'');
                    $createdBy      .= " '".$corporateId."'".$subUserIds.",";
                }
                
                $corporateId    = getCorporateUser($userId);
                $subUserIds     = getCorporateSubUser($corporateId, $userRole);
                $subUserIds     = ($subUserIds!=''?','.$subUserIds:'');
                $createdBy      .= " '".$corporateId."'".$subUserIds."";
                
                $createdBy      = array_filter(explode(',',  str_replace("'", '', $createdBy)));
            }
            else {
                $sql        = " SELECT * FROM fj_users WHERE role='1' ";
                $query      = $this->db->query($sql);
                $result     = $query->result_array();
                $createdBy  = '';
                foreach ($result AS $user) {
                    $corporateId    = getCorporateUser($user['id']);
                    $subUserIds     = getCorporateSubUser($corporateId, $user['role']);
                    $subUserIds     = ($subUserIds!=''?','.$subUserIds:'');
                    $createdBy      .= " '".$corporateId."'".$subUserIds.",";
                }
                $createdBy      = array_filter(explode(',',  str_replace("'", '', $createdBy)));
            }
            
            $this->db->select('*');
            $this->db->from('fj_question');
            $this->db->where('type', $quesType);
            $this->db->where('status =', 1);
            $this->db->where_in('createdBy ', $createdBy);
            $this->db->order_by('title asc');
            $query = $this->db->get();
            $result = $query->result();

            if (count($result) == 0) {
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Function Name: listInterviews function
     * Discription : Use to Get List of interviews.
     * Author : Synergy
     * @param int $limit, int $offset, string $serachColumn, string $searchText, int $createdBy
     * @return array the interviews data.
     */
    function listInterviews($limit, $offset, $serachColumn = NULL, $searchText = NULL, $createdBy = NULL) {
        try {
            $currentDate = date('Y-m-d h:i:s');
            $userTypeQuery  = "SELECT * FROM fj_users WHERE id='$createdBy' LIMIT 0,1";
            $userQuery      = $this->db->query($userTypeQuery);
            $userResult     = $userQuery->row();

            if ($userResult) {
                if($userResult->role!='1') {
                    $sql = "select * from fj_interviews where status != 3";
                    if (isset($createdBy) && $createdBy != NULL) {
                        $sql .= " AND createdBy IN (" . $createdBy . ")";
                    }
                    $sql .= " order by type asc, createdAt desc";
                    if (isset($offset)) {
                        $sql .= " limit " . $offset . ", " . $limit . "";
                    }

                    $query = $this->db->query($sql);
                    $result = $query->result();
                    if (!$result) {
                        return false;
                    }
                }
                else{
                    $sql = "SELECT 
                                * 
                            FROM 
                                fj_interviews 
                            WHERE 
                                status != 3 AND 
                                createdBy   IN (
                                                SELECT
                                                    id
                                                FROM
                                                    fj_users
                                                WHERE
                                                    role='1'
                                                )
                            ORDER BY
                                type ASC,
                                createdAt DESC ";
                    if (isset($offset)) {
                        $sql .= " LIMIT " . $offset . ", " . $limit . "";
                    }

                    $query = $this->db->query($sql);
                    $result = $query->result();
                    if (!$result) {
                        return false;
                    }                    
                }
            }  
            else {
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Function Name : listFjQuestions function
     * Discription : Use to get list of interview questions crteated by superuser.
     * Author : Synergy
     * @param int $limit, int $offset, string $serachColumn, string $searchText, int $createdBy
     * @return array of interviews questions data.
     */
    function listFjQuestions($limit, $offset, $serachColumn = NULL, $searchText = NULL, $createdBy = NULL) {
        try {
            $currentDate    = date('Y-m-d h:i:s');
            $userTypeQuery  = "SELECT * FROM fj_users WHERE id='$createdBy' LIMIT 0,1";
            $userQuery      = $this->db->query($userTypeQuery);
            $userResult     = $userQuery->row();
            if ($userResult) {
                if($userResult->role!='1') {

                    $sql = "SELECT 
                                * 
                            FROM
                                fj_question
                            WHERE
                                createdBy IN (
                                        SELECT
                                            id
                                        FROM
                                            fj_users
                                        WHERE
                                            role='1'
                                        )       AND 
                                status != 3   AND
                                IsMultipleChoice = '0'
                            ORDER BY
                                type asc, 
                                createdAt desc";
                    if (isset($offset)) {
                        $sql .= " limit " . $offset . ", " . $limit . "";
                    }
                    $query = $this->db->query($sql);
                    $result = $query->result();
                }
                else {
                    $sql = "SELECT 
                                * 
                            FROM
                                fj_question
                            WHERE
                                createdBy IN (
                                        SELECT
                                            id
                                        FROM
                                            fj_users
                                        WHERE
                                            role='1'
                                        )       AND 
                                status != 3   AND
                                IsMultipleChoice = '0'
                            ORDER BY
                                type asc, 
                                createdAt desc";
                    if (isset($offset)) {
                        $sql .= " limit " . $offset . ", " . $limit . "";
                    }

                    $query = $this->db->query($sql);
                    $result = $query->result();
                }
            }
            else {
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * listFjQuestions function
     * Discription : Use to get list of interview questions created by loggedin corporate user.
     * Author : Synergy
     * @param int $limit, int $offset, string $serachColumn, string $searchText, int $createdBy
     * @return array of interview questions data.
     */
    function listMyQuestions($limit, $offset, $serachColumn = NULL, $searchText = NULL, $createdBy = NULL) {
        try {
            $currentDate = date('Y-m-d h:i:s');
            $userTypeQuery  = "SELECT * FROM fj_users WHERE id='$createdBy' LIMIT 0,1";
            $userQuery      = $this->db->query($userTypeQuery);
            $userResult     = $userQuery->row();
            if ($userResult) {
                if($userResult->role!='1') {
                    $sql = "select * from fj_question where createdBy='" . $createdBy . "' AND status != 3";
                    if (isset($offset)) {
                        $sql .= " limit " . $offset . ", " . $limit . "";
                    }
                    $query = $this->db->query($sql);
                    $result = $query->result();
                }
                else {
                    $sql =  "SELECT
                                * 
                            FROM
                                fj_question
                            WHERE 
                                status != 3 AND 
                                createdBy   IN (
                                                SELECT
                                                    id
                                                FROM
                                                    fj_users
                                                WHERE
                                                    role='1'
                                                )   AND
                                IsMultipleChoice = '0'
                            ORDER BY
                                type ASC ";
                    if (isset($offset)) {
                        $sql .= " limit " . $offset . ", " . $limit . "";
                    }
                    $query = $this->db->query($sql);
                    $result = $query->result();                    
                }
            } 
            else {
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Function Name : getQuestionsByInterview function
     * Discription : Use to Get all interview questions of a particular interview set.
     * Author : Synergy
     * @param int $id.
     * @return array of interview questions data.
     */
    function getQuestionsByInterview($id) {
        try {
            $sql = "select q.* from fj_interviewQuestions iq "
                    . "left join fj_question q on q.id=iq.questionId "
                    . "where iq.interviewId = '" . $id . "' and q.status!=3";
            $query = $this->db->query($sql);
            $result = $query->result_array();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Function Name : getQuestionsByInterview function
     * Discription : Use to Get all interview questions of a particular interview set.
     * Author : Synergy
     * @param int $id.
     * @return array of interview questions data.
     */
    function deleteQuestionsByInterviewSet($id) {
        try {
            $this->db->where('interviewId', $id);
            $this->db->delete('fj_interviewQuestions');
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Function Name : deleteQuestionsById function
     * Discription : Use to delete a question
     * Author : Synergy
     * @param int $id.
     * @return boolean
     */
    function deleteQuestionsById($id) {
        try {
            $this->db->where('id', $id);
            $this->db->delete('fj_question');
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
	
	/**
     * Function Name: listInterviews function
     * Discription : Use to Get List of interviews.
     * Author : Synergy
     * @param int $limit, int $offset, string $serachColumn, string $searchText, int $createdBy
     * @return array the interviews data.
     */
    function getUserQuestionAnswers($limit, $offset, $serachColumn = NULL, $searchText = NULL, $createdBy = NULL) {
        try {
            
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
	
	
	function listAuditions($userId) {
        try {
            $currentDate = date('Y-m-d h:i:s');
            $sql    = "SELECT u.fullname, u.email, u.mobile, a.* FROM `fj_userAudition` a left join fj_users u on a.userId=u.id  WHERE a.auditionFiles like '%.mp4%' and u.id=".$userId;   
					
            $queryResult = $this->db->query($sql);
            $result = $queryResult->result();
			
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
	
	function CountAuditions() {
        try {
            $currentDate = date('Y-m-d h:i:s');
            $sql    = "SELECT u.fullname, u.email, u.mobile, a.* FROM `fj_userAudition` a left join fj_users u on a.userId=u.id";            

            $queryResult = $this->db->query($sql);
            $result = $queryResult->result();
			
			
            return count($result);
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
	
	
	function getAuditions($id){
		$sql    = "SELECT * FROM `fj_userAudition` WHERE id=".$id;   
					
		$queryResult = $this->db->query($sql);
		$result = $queryResult->result();
		return $result;
	}

    function getAlreadyCreatedInterviewSet($userId, $createdById, $interviewSetName){
        $sql    = "SELECT * FROM fj_interviews WHERE name='$interviewSetName' AND status='1' AND (createdBy = '$userId' OR createdBy = '$createdById')";   
                    
        $queryResult = $this->db->query($sql);
        $result = $queryResult->result();
        return $result;
    }
}
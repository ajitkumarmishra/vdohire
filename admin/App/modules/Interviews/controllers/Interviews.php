<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Interviews Controller
 * Description : Used to handle all Interviews related data
 * @author Synergy
 * @createddate : Nov 2, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 17, 2017
 */

class Interviews extends MY_Controller {

    /**
     * Responsable for auto load the the analytics_model,industry_model,question_model
     * Responsable for auto load the the form_validation,session,email library
     * Responsable for auto load fj helper
     * Check Uri Segment to display specific page role wise
     * @return void
     */
    function __construct() {
        parent::__construct();
        $this->load->model('interview_model');
        $this->load->model('industry_model');
        $this->load->model('question_model');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('email');
        $this->load->helper('fj');
        //$this->load->helper('jwt');
        $token = $this->config->item('accessToken');
        

        /**********Check any type of fraund activity*******/
        if ($this->session->userdata('logged_in')) {
            $userRealData   = $this->session->userdata['logged_in'];
            $uriStringSegment = $this->uri->uri_string();

            $arraySegments = explode('/', $uriStringSegment);
            array_pop($arraySegments);
            $newString = implode('/', $arraySegments);
            
            if($userRealData->role == 1) {
                $portal = 'SuperAdminPortal';
            }
            elseif ($userRealData->role == 2 || $userRealData->role == 4 || $userRealData->role == 5) {
                $portal = 'CorporatePortal';
            }
            else{
                $portal = '';
            }

            if($uriStringSegment == 'interviews/list') {
                if(!checkUserAccessForWebPage($userRealData->id, $userRealData->role, $portal, 'interviews/list')) {
                    $this->session->sess_destroy();
                    $insertFaultData['userId'] = $userRealData->id;
                    $insertFaultData['comment'] = 'This user try to access this this page';
                    $insertFaultData['pagePath'] = 'interviews/list';
                    $this->db->insert('fj_user_access_log', $insertFaultData);
                    redirect(base_url('users/login'));
                }
            }

            if($uriStringSegment == 'interviews/add') {
                if(!checkUserAccessForWebPage($userRealData->id, $userRealData->role, $portal, 'interviews/add')) {
                    $this->session->sess_destroy();
                    $insertFaultData['userId'] = $userRealData->id;
                    $insertFaultData['comment'] = 'This user try to access this this page';
                    $insertFaultData['pagePath'] = 'interviews/add';
                    $this->db->insert('fj_user_access_log', $insertFaultData);
                    redirect(base_url('users/login'));
                }
            }

            if($uriStringSegment == 'interviews/addQuestion') {
                if(!checkUserAccessForWebPage($userRealData->id, $userRealData->role, $portal, 'interviews/addQuestion')) {
                    $this->session->sess_destroy();
                    $insertFaultData['userId'] = $userRealData->id;
                    $insertFaultData['comment'] = 'This user try to access this this page';
                    $insertFaultData['pagePath'] = 'interviews/addQuestion';
                    $this->db->insert('fj_user_access_log', $insertFaultData);
                    redirect(base_url('users/login'));
                }
            }

            if($newString == 'interviews/edit') {
                if(!checkUserAccessForWebPage($userRealData->id, $userRealData->role, $portal, 'interviews/edit')) {
                    $this->session->sess_destroy();
                    $insertFaultData['userId'] = $userRealData->id;
                    $insertFaultData['comment'] = 'This user try to access this this page';
                    $insertFaultData['pagePath'] = 'interviews/edit';
                    $this->db->insert('fj_user_access_log', $insertFaultData);
                    redirect(base_url('users/login'));
                }
            }

            if($newString == 'interviews/editQuestion') {
                if(!checkUserAccessForWebPage($userRealData->id, $userRealData->role, $portal, 'interviews/edit')) {
                    $this->session->sess_destroy();
                    $insertFaultData['userId'] = $userRealData->id;
                    $insertFaultData['comment'] = 'This user try to access this this page';
                    $insertFaultData['pagePath'] = 'interviews/edit';
                    $this->db->insert('fj_user_access_log', $insertFaultData);
                    redirect(base_url('users/login'));
                }
            }
        }
        /****************End of handle fraud activity****************/
    }

    /**
     * Description : Use to add interview set by corporate or superadmin
     * Author : Synergy
     * @param array $_POST
     * @return render data to view
     */
    function addInterview() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token      = $this->config->item('accessToken');
        $userData   = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        }

        $practiceInterviewType  = "";
        $type_1                 = "";
        $type_2                 = "";        
        $info['main_content']   = 'fj-create-interview';
        $info['token']          = $token;
        $info['fjQuestion']     = getFjQuestions();
        $allIndustryList        = $this->industry_model->getIndustryList(1);
        $allFunctionList        = $this->industry_model->getIndustryList(2);        
        $info['fjIndustry']     = $allIndustryList;
        $info['fjFunction']     = $allFunctionList;
                
        if (isset($_POST) && $_POST != NULL) {
            $this->form_validation->set_rules('name', 'Set Name', 'trim|required');
            $this->form_validation->set_rules('type', 'Type', 'trim|required');            
            $environment        = $this->input->post('environment', true);
            $input_file_0       = $this->input->post('input_file_0', true);
            $input_file_1_0     = $this->input->post('input_file_1_0', true);            
            $questions          = $this->input->post('question', true);            
            $data['type']       = $this->input->post('type', true);            
            $tempdata['type0']  = $this->input->post('type', true);
            
            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }
            
            if( $environment == 2 ){
                $industryIdData     = $this->input->post('industryId', true);
                $functionIdData     = $this->input->post('functionId', true);
                $errorStatus        = false;
                if( $industryIdData == "" && $functionIdData == "" ){
                    $errorStatus    = false;
                    $resp['error'] .= '<p>Atleast one should be selected either Industry or Function.</p>';
                } else if( $industryIdData != "" && $functionIdData != "" ){
                    $errorStatus    = false;
                    $resp['error'] .= '<p>Only one should be selected either Industry or Function.</p>';
                } else if( $industryIdData != "" && $functionIdData == "" ){
                    $errorStatus    = true;
                } else if( $industryIdData == "" && $functionIdData != "" ){
                    $errorStatus    = true;
                } else {
                    $errorStatus    = false;
                    $resp['error'] .= '<p>Atleast one should be selected either Industry or Function.</p>';
                }
                
                if( $errorStatus == true ){
                    if( $industryIdData != "" && $functionIdData == "" ){
                        if( !is_numeric($industryIdData) ){                            
                            $industryNameExists = $this->industry_model->getIndustryByName($industryIdData);                            
                            if( $industryNameExists ){
                                $industryId = $industryNameExists['industryId'];
                            } else {
                                $dataIndustry['industryName']   = $industryIdData;
                                $dataIndustry['industryType']   = 1;
                                $dataIndustry['createdAt']      = date('Y-m-d h:i:s');
                                $dataIndustry['updatedAt']      = date('Y-m-d h:i:s');
                                $industryId = $this->industry_model->createIndustry($dataIndustry);
                            }
                        } else {
                                $industryId = $industryIdData;
                        }
                    } 
                    else if( $industryIdData == "" && $functionIdData != "" ){
                        if( !is_numeric($functionIdData) ){                            
                            $functionNameExists = $this->industry_model->getIndustryByName($functionIdData);                            
                            if( $functionNameExists ){
                                $functionId = $functionNameExists['industryId'];
                            } else {
                                $dataIndustry['industryName']   = $functionIdData;
                                $dataIndustry['industryType']   = 2;
                                $dataIndustry['createdAt']      = date('Y-m-d h:i:s');
                                $dataIndustry['updatedAt']      = date('Y-m-d h:i:s');
                                $functionId = $this->industry_model->createIndustry($dataIndustry);
                            }
                        } else {
                                $functionId = $functionIdData;
                        }
                    }
                    
                    $questions1         = $this->input->post('question1', true);
                    $type_1             = $this->input->post('type_1', true);
                    $tempdata['type1']  = $this->input->post('type_1', true);
                    $questions2         = $this->input->post('question2', true);
                    $type_2             = $this->input->post('type_2', true);
                    $tempdata['type2']  = $this->input->post('type_2', true);

                    if ( count($questions) > 0 && $questions[0]['title'] != "" ) {
                        $practiceInterviewType .= "1,";
                    }
                    if ( count($questions1) > 0 && $questions1[0]['title'] != "" ) {
                        $practiceInterviewType .= "2,";
                    }
                    if ( count($questions2) > 0 && $questions2[0]['title'] != "" ) {
                        $practiceInterviewType .= "3";
                    }                

                    if( $practiceInterviewType != "" ){
                        $practiceInterviewTypeArr       = explode(",", $practiceInterviewType);
                        $practiceInterviewTypeArr       = array_filter($practiceInterviewTypeArr);
                        $practiceInterviewTypeStr       = implode(",", $practiceInterviewTypeArr);
                        $data['practiceInterviewType']  = $practiceInterviewTypeStr;
                    } else {
                        $practiceInterviewTypeStr       = "";
                        $data['practiceInterviewType']  = "";
                    }
                    $data['type'] = "0";
                }
            }
            else {
                $data['practiceInterviewType'] = "";
            }            
            
            
            if( $environment != 2 ){
                if ($questions[0]['title'] == '' && $data['type'] == 3) {
                    $resp['error'] .= '<p>Title field of first question is required.</p>';
                }
                if ($questions[0]['duration'] == '') {
                    $resp['error'] .= '<p>Answer Duration field of first question is required.</p>';
                }
                if ($_FILES['file_0']['name'] == '' && $input_file_0 == "" && $tempdata['type0'] != 3 && $tempdata['type0'] != "") {
                    $resp['error'] .= '<p>Please upload file of first question.</p>';
                }
            }
            
            if (!$resp['error']) {                
                if( $environment == 2 ){                    
                    $data['environment']    = $this->input->post('environment', true);
                    if( $industryId != "" ) {
                        $data['industryId'] = $industryId;
                    } else {
                        $data['industryId'] = 0;
                    }                    
                    if( $functionId != "" ) {
                        $data['functionId'] = $functionId;
                    } else {
                        $data['functionId'] = 0;
                    }
                }
                
                $data['name']       = $this->input->post('name', true);
                $data['createdAt']  = date('Y-m-d h:i:s');
                $data['updatedAt']  = date('Y-m-d h:i:s');
                $data['createdBy']  = $userId;
                $data['updatedBy']  = 0;
                
                try {
                    $resInterviewId = $this->interview_model->addInterview($data);                    
                    if ($resInterviewId) {                        

                        if( $environment == 2 ){
                            if (count($questions) > 0 && $questions[0]['title'] != "") {
                                $f = 0;
                                foreach ($questions as $item) {
                                    if( is_numeric($item['title']) ){
                                        //echo $input_file_0; echo "dfsd";
                                        $input_file_value   = $this->input->post('input_file_'.$f, true);
                                        $questionDetailRes  = $this->question_model->getQuestion($item['title']);
                                        $questionId         = $questionDetailRes['id'];
                                        $questionFile       = $questionDetailRes['file'];
                                        $questionDuration   = $questionDetailRes['duration'];
                                        if( $input_file_value != $questionFile && isset($_FILES['file_' . $f]) ){
                                            if (isset($_FILES['file_' . $f]) && $_FILES['file_' . $f] != NULL) {
                                                $item['file'] = $this->fileUpload($_FILES['file_' . $f], $tempdata['type0']);
                                            }
                                            $item['title']      = $questionDetailRes['title'];
                                            $item['duration']   = $item['duration'];
                                            $item['createdAt']  = date('Y-m-d h:i:s');
                                            $item['updatedAt']  = date('Y-m-d h:i:s');
                                            $item['createdBy']  = $userId;
                                            $item['type']       = $this->input->post('type', true);
                                            $questionId         = $this->interview_model->addQuestion($item, $resInterviewId);                                            
                                        } 
                                        elseif( $item['duration'] != $questionDuration ){                                            
                                            if( $input_file_value != $questionFile ){
                                                if (isset($_FILES['file_' . $f]) && $_FILES['file_' . $f] != NULL) {
                                                    $item['file'] = $this->fileUpload($_FILES['file_' . $f], $tempdata['type0']);
                                                }
                                            } else {
                                                $item['file'] = $questionFile;
                                            }                                            
                                            $item['title']      = $questionDetailRes['title'];
                                            $item['duration']   = $item['duration'];
                                            $item['createdAt']  = date('Y-m-d h:i:s');
                                            $item['updatedAt']  = date('Y-m-d h:i:s');
                                            $item['createdBy']  = $userId;
                                            $item['type']       = $this->input->post('type', true);
                                            $questionId         = $this->interview_model->addQuestion($item, $resInterviewId);
                                        } else {
                                            $questionId         = $questionId;
                                        }     
                                        
                                    } else {
                                        if (isset($_FILES['file_' . $f]) && $_FILES['file_' . $f] != NULL) {
                                            $item['file']   = $this->fileUpload($_FILES['file_' . $f], $tempdata['type0']);
                                        }
                                        $item['createdAt']  = date('Y-m-d h:i:s');
                                        $item['updatedAt']  = date('Y-m-d h:i:s');
                                        $item['createdBy']  = $userId;
                                        $item['type']       = $this->input->post('type', true);
                                        $questionId         = $this->interview_model->addQuestion($item, $resInterviewId);
                                    }                                    
                                    
                                    $interviewQuestion['interviewId']           = $resInterviewId;
                                    $interviewQuestion['practiceInterviewType'] = $this->input->post('type', true);
                                    $interviewQuestion['questionId']            = $questionId;
                                    $interviewQuestion['createdAt']             = date('Y-m-d h:i:s');
                                    $interviewQuestion['updatedAt']             = date('Y-m-d h:i:s');
                                    $this->interview_model->addInterviewQuestion($interviewQuestion);
                                    $f++;
                                }
                            }
                        
                            if (count($questions1) > 0 && $questions1[0]['title'] != "") {
                                $f = 0;                                
                                foreach ($questions1 as $item) {                                    
                                    if( is_numeric($item['title']) ){
                                        $input_file_1_value = $this->input->post('input_file_1_'.$f, true);                                        
                                        $questionDetailRes  = $this->question_model->getQuestion($item['title']);                                        
                                        $questionId         = $questionDetailRes['id'];
                                        $questionFile       = $questionDetailRes['file'];
                                        $questionDuration   = $questionDetailRes['duration'];                                        
                                        if( $input_file_1_value != $questionFile && isset($_FILES['file_1_' . $f]) ){                                            
                                            if (isset($_FILES['file_1_' . $f]) && $_FILES['file_1_' . $f] != NULL) {
                                                $item['file'] = $this->fileUpload($_FILES['file_1_' . $f], $tempdata['type1']);
                                            }                                            
                                            $item['title']      = $questionDetailRes['title'];
                                            $item['duration']   = $item['duration'];
                                            $item['createdAt']  = date('Y-m-d h:i:s');
                                            $item['updatedAt']  = date('Y-m-d h:i:s');
                                            $item['createdBy']  = $userId;
                                            $item['type']       = $this->input->post('type_1', true);
                                            $questionId         = $this->interview_model->addQuestion($item, $resInterviewId);                                            
                                        } 
                                        elseif( $item['duration'] != $questionDuration ){                                            
                                            if( $input_file_1_value != $questionFile ){
                                                if (isset($_FILES['file_1_' . $f]) && $_FILES['file_1_' . $f] != NULL) {
                                                    $item['file'] = $this->fileUpload($_FILES['file_1_' . $f], $tempdata['type1']);
                                                }
                                            } else {
                                                $item['file'] = $questionFile;
                                            }                                            
                                            $item['title']      = $questionDetailRes['title'];
                                            $item['duration']   = $item['duration'];
                                            $item['createdAt']  = date('Y-m-d h:i:s');
                                            $item['updatedAt']  = date('Y-m-d h:i:s');
                                            $item['createdBy']  = $userId;
                                            $item['type']       = $this->input->post('type_1', true);
                                            $questionId         = $this->interview_model->addQuestion($item, $resInterviewId);
                                        } else {
                                            $questionId         = $questionId;
                                        }     
                                        
                                    } else {
                                        if (isset($_FILES['file_1_' . $f]) && $_FILES['file_1_' . $f] != NULL) {
                                            $item['file']   = $this->fileUpload($_FILES['file_1_' . $f], $tempdata['type1']);
                                        }
                                        $item['createdAt']  = date('Y-m-d h:i:s');
                                        $item['updatedAt']  = date('Y-m-d h:i:s');
                                        $item['createdBy']  = $userId;
                                        $item['type']       = $this->input->post('type_1', true);
                                        $questionId         = $this->interview_model->addQuestion($item, $resInterviewId);
                                    }                            
                                    
                                    $interviewQuestion['interviewId']           = $resInterviewId;
                                    $interviewQuestion['practiceInterviewType'] = $this->input->post('type_1', true);
                                    $interviewQuestion['questionId']            = $questionId;
                                    $interviewQuestion['createdAt']             = date('Y-m-d h:i:s');
                                    $interviewQuestion['updatedAt']             = date('Y-m-d h:i:s');
                                    $this->interview_model->addInterviewQuestion($interviewQuestion);
                                    $f++;
                                }
                            }
                            
                            if (count($questions2) > 0 && $questions2[0]['title'] != "") {
                                $f = 0;                                
                                foreach ($questions2 as $item) {                                    
                                    if( is_numeric($item['title']) ){
                                      
                                        $questionDetailRes  = $this->question_model->getQuestion($item['title']);                                        
                                        $questionId         = $questionDetailRes['id'];
                                        $questionFile       = $questionDetailRes['file'];
                                        $questionDuration   = $questionDetailRes['duration'];                                        
                                        if( $item['duration'] != $questionDuration ){                                            
                                            $item['file']       = "";
                                            $item['title']      = $questionDetailRes['title'];
                                            $item['duration']   = $item['duration'];
                                            $item['createdAt']  = date('Y-m-d h:i:s');
                                            $item['updatedAt']  = date('Y-m-d h:i:s');
                                            $item['createdBy']  = $userId;
                                            $item['type']       = $this->input->post('type_2', true);
                                            $questionId         = $this->interview_model->addQuestion($item, $resInterviewId);
                                        } else {
                                            $questionId         = $questionId;
                                        }
                                    } else {
                                        $item['file']       = "";
                                        $item['createdAt']  = date('Y-m-d h:i:s');
                                        $item['updatedAt']  = date('Y-m-d h:i:s');
                                        $item['createdBy']  = $userId;
                                        $item['type']       = $this->input->post('type_2', true);
                                        $questionId         = $this->interview_model->addQuestion($item, $resInterviewId);
                                    }                            
                                    
                                    $interviewQuestion['interviewId']           = $resInterviewId;
                                    $interviewQuestion['practiceInterviewType'] = $this->input->post('type_2', true);
                                    $interviewQuestion['questionId']            = $questionId;
                                    $interviewQuestion['createdAt']             = date('Y-m-d h:i:s');
                                    $interviewQuestion['updatedAt']             = date('Y-m-d h:i:s');
                                    $this->interview_model->addInterviewQuestion($interviewQuestion);
                                    $f++;
                                }
                            }
                        } 
                        else {
                            if (count($questions) > 0 && $questions[0]['title'] != "") {
                                $f = 0;
                                foreach ($questions as $item) {
                                    if( is_numeric($item['title']) ){

                                        $input_file_value   = $this->input->post('input_file_'.$f, true);
                                        $questionDetailRes  = $this->question_model->getQuestion($item['title']);
                                        $questionId         = $questionDetailRes['id'];
                                        $questionFile       = $questionDetailRes['file'];
                                        $questionDuration   = $questionDetailRes['duration'];

                                        if( $data['type']!='3' && $input_file_value != $questionFile && isset($_FILES['file_' . $f]) ){
                                            if (isset($_FILES['file_' . $f]) && $_FILES['file_' . $f] != NULL) {
                                                $item['file'] = $this->fileUpload($_FILES['file_' . $f], $data['type']);
                                            }
                                            $item['title']      = $questionDetailRes['title'];
                                            $item['duration']   = $item['duration'];
                                            $item['createdAt']  = date('Y-m-d h:i:s');
                                            $item['updatedAt']  = date('Y-m-d h:i:s');
                                            $item['createdBy']  = $userId;
                                            $item['type']       = $this->input->post('type', true);
                                            $questionId         = $this->interview_model->addQuestion($item, $resInterviewId);
                                        }
                                        elseif( $item['duration'] != $questionDuration ){                                            
                                            if( $input_file_value != $questionFile ){
                                                if (isset($_FILES['file_' . $f]) && $_FILES['file_' . $f] != NULL) {
                                                    $item['file'] = $this->fileUpload($_FILES['file_' . $f], $data['type']);
                                                }
                                            } else {
                                                $item['file'] = $questionFile;
                                            }                                            
                                            $item['title']      = $questionDetailRes['title'];
                                            $item['duration']   = $item['duration'];
                                            $item['createdAt']  = date('Y-m-d h:i:s');
                                            $item['updatedAt']  = date('Y-m-d h:i:s');
                                            $item['createdBy']  = $userId;
                                            $item['type']       = $this->input->post('type', true);
                                            $questionId         = $this->interview_model->addQuestion($item, $resInterviewId);
                                        } 
                                        else {
                                            $questionId         = $questionId;
                                        }                                        
                                    } 
                                    else {
                                        if (isset($_FILES['file_' . $f]) && $_FILES['file_' . $f] != NULL) {
                                            $item['file']   = $this->fileUpload($_FILES['file_' . $f], $data['type']);
                                        }
                                        $item['createdAt']  = date('Y-m-d h:i:s');
                                        $item['updatedAt']  = date('Y-m-d h:i:s');
                                        $item['createdBy']  = $userId;
                                        $item['type']       = $this->input->post('type', true);
                                        $questionId         = $this->interview_model->addQuestion($item, $resInterviewId);
                                    }

                                    $interviewQuestion['interviewId']   = $resInterviewId;
                                    $interviewQuestion['questionId']    = $questionId;
                                    $interviewQuestion['createdAt']     = date('Y-m-d h:i:s');
                                    $interviewQuestion['updatedAt']     = date('Y-m-d h:i:s');
                                    $this->interview_model->addInterviewQuestion($interviewQuestion);
                                    $f++;
                                }
                            }
                        }
                        
                        $info['message'] = 'Successfully created!';
                        redirect(base_url('interviews/view/'.$resInterviewId));
                        $this->load->view('fj-mainpage', $info);
                    }
                } catch (Exception $e) {
                    $resp['error'] = $e->getMessage();
                }
            } 
            else {
                $info['error']      = $resp;
                $info['interview']  = $this->input->post();                
                $environment        = $this->input->post('environment', true);
                if( $environment == 2 ) {
                    $industryId             = $this->input->post('industryId', true);
                    $functionId             = $this->input->post('functionId', true);
                    $info['industryData']   = $industryId;
                    $info['functionData']   = $functionId;
                } else {
                    $info['industryData']   = "";
                    $info['functionData']   = "";
                }
                $this->load->view('fj-mainpage', $info);
            }
        } else {
            $this->load->view('fj-mainpage', $info);
        }
    }
    
    /**
     * Description : Use to add questions to interview set
     * Author : Synergy
     * @param array $_POST
     * @return render to interview list or error message
     */
    function addQuestionToInterviewSet() {
        $questionId = $this->input->post('questionId', true);
        $interview = $this->input->post('interview', true);
        try {
            foreach ($interview as $item) {
                $interviewQuestion['interviewId'] = $item;
                $interviewQuestion['questionId'] = $questionId;
                $interviewQuestion['createdAt'] = date('Y-m-d h:i:s');
                $interviewQuestion['updatedAt'] = date('Y-m-d h:i:s');
                $this->interview_model->addInterviewQuestion($interviewQuestion);
                redirect(base_url('interviews/list/1'));
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
     * Description : Use to add questions to interview set
     * Author : Synergy
     * @param array $_POST
     * @return render to interview list or error message
     */
    function addQuestionToInterviewSetRecruiter() {
        $questionId = $this->input->post('questionId', true);
        $interview = $this->input->post('interview', true);
        try {
            foreach ($interview as $item) {
                $interviewQuestion['interviewId'] = $item;
                $interviewQuestion['questionId'] = $questionId;
                $interviewQuestion['createdAt'] = date('Y-m-d h:i:s');
                $interviewQuestion['updatedAt'] = date('Y-m-d h:i:s');
                $this->interview_model->addInterviewQuestion($interviewQuestion);
                //redirect(base_url('interviews/list/1'));
                echo true;
            }
        } catch (Exception $e) {
            echo false;
        }
    }

    /**
     * Description : Use to check an question which is exists or not in specific interview set
     * Author : Synergy
     * @param array $_POST
     * @return boolean
     */
    function checkInterview() {
        $questionId = $this->input->post('questionId', true);
        $interview = $this->input->post('interview', true);
        $check = 0;
        foreach ($interview as $item) {
            if (checkQuestionExistInInterview($questionId, $item)) {
                $check = 1;
            }
        }
        echo $check;
        exit;
    }

    /**
     * Description : Use to get interview set by type
     * Author : Synergy
     * @param array $_POST
     * @return html string
     */
    function getInterviewSetByType() {
        $type = $this->input->post('type', true);
        $userData = $this->session->userdata['logged_in'];
        $userId = $userData->id;
        $questionId = $this->input->post('questionId', true); //die;
        $results = getInterviewSetByType($type, $userId);
        $str = '<select data-placeholder="Interview Sets" style="width: 350px; display: none;" multiple="" class="chosen-select required" tabindex="-1" name="interview[]" id="interviewSetsDropDown">';

        foreach ($results as $item) {
            $str .= '<option value="' . $item['id'] . '">' . $item['name'] . '</option>';
        }
        $str .= '</select>';
        $str .= '<input type="hidden" name ="questionId" class= "question-id" value="' . $questionId . '" />';
        echo $str;
        exit;
    }

    /**
     * Description : Use to playable string
     * Author : Synergy
     * @param array $_POST
     * @return html string
     */
    function getPlayableFile() {
        $filetype   = $this->input->post('filetype', true);
        $fileUrl    = $this->input->post('fileUrl', true);
        $userData   = $this->session->userdata['logged_in'];
        $userId     = $userData->id;

        if($filetype==1){
        $fileData   .='<video controls  preload="auto" width="550" height="400">
                        <source src="'.$fileUrl.'" type="video/mp4">
                        </video>';
        }
        if($filetype==2){
        $fileData   .='<audio controls  preload="auto" width="550" height="400">
                        <source src="'.$fileUrl.'" type="audio/mpeg">
                        </audio>';
        }
        echo $fileData;
        exit;
    }

    /**
     * FileUpload function
     * Uploads a file.
     * @param array $files.
     * @return string the uploaded file or boolean false.
     */
    function fileUpload($files, $type) {
        // will get transfer in helper.
        $typeArr = explode('/', $files['type']);
        $fileName = rand() . date('ymdhis') . '_question.' . $typeArr[1];
        $data['image'] = $fileName;
        if ($type == 1)
            $type = 'video';
        else
            $type = 'audio';
        $questionType = $this->config->item('questionType');
        if (in_array($typeArr[1], $questionType)) {
            $uploadResp = $this->interview_model->fileUpload($files, 'uploads/questions/' . $type, 'q'.$fileName);
            $fileType = pathinfo($fileName, PATHINFO_EXTENSION);
            //if($fileType=='mp4') {
                //shell_exec("ffmpeg -i ".$uploads_dir."/".$fileName." -vcodec libx264 -crf 24 ".$uploads_dir."/q".$fileName." -y ");
                //unlink($uploads_dir."/".$fileName);
                //$fileName = "q".$fileName;
                //exit;
            //}
            return $uploadResp;
        } else {
            return false;
        }
    }

    /**
     * Use to update a specific interveiw set
     * Author : Synergy
     * @param int $id, array $_POST
     * @return string the success message | error message.
     */
    function editInterview($id) {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token = $this->config->item('accessToken');
        $userData = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            $userId = 1;
        }
        $interview = $this->interview_model->getInterview($id);
        $info['main_content'] = 'fj-edit-interview';
        $info['token'] = $token;
        $info['fjQuestion'] = getFjQuestions($interview['type']);
        $info['fjVideoQuestion'] = getFjQuestions(1);
        $info['fjAudioQuestion'] = getFjQuestions(2);
        $info['fjTextQuestion'] = getFjQuestions(3);
        $info['interview'] = $interview;
        $info['questions'] = getQuestionsByInterview($id);
        
        $industryList = $this->industry_model->getIndustryList(1);
        $info['industryList'] = $industryList;
        $functionList = $this->industry_model->getIndustryList(2);
        $info['functionList'] = $functionList; 
        
        if (isset($_POST) && $_POST != NULL) {
            $this->form_validation->set_rules('name', 'Set Name', 'trim|required');
            $this->form_validation->set_rules('type', 'Type', 'trim|required');
            
            $environment = $this->input->post('environment', true);
            $input_file_0 = $this->input->post('input_file_0', true);
            $input_file_1_0 = $this->input->post('input_file_1_0', true);
            
            $questions = $this->input->post('question', true);
            $data['type'] = $this->input->post('type', true);
            
            $tempdata['type0'] = $this->input->post('type', true);
            
            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }
            
            if( $environment == 2 ){
                
                $industryIdData = $this->input->post('industryId', true);
                $functionIdData = $this->input->post('functionId', true);
                $errorStatus = false;
                if( $industryIdData == "" && $functionIdData == "" ){
                    $errorStatus = false;
                    $resp['error'] .= '<p>Atleast one should be selected either Industry or Function.</p>';
                } else if( $industryIdData != "" && $functionIdData != "" ){
                    $errorStatus = false;
                    $resp['error'] .= '<p>Only one should be selected either Industry or Function.</p>';
                } else if( $industryIdData != "" && $functionIdData == "" ){
                    $errorStatus = true;
                } else if( $industryIdData == "" && $functionIdData != "" ){
                    $errorStatus = true;
                } else {
                    $errorStatus = false;
                    $resp['error'] .= '<p>Atleast one should be selected either Industry or Function.</p>';
                }
                
                if( $errorStatus == true ){
                    
                    if( $industryIdData != "" && $functionIdData == "" ){
                        if( !is_numeric($industryIdData) ){
                            $dataIndustry['industryName'] = $industryIdData;
                            $dataIndustry['industryType'] = 1;
                            $dataIndustry['createdAt'] = date('Y-m-d h:i:s');
                            $dataIndustry['updatedAt'] = date('Y-m-d h:i:s');
                            $industryId = $this->industry_model->createIndustry($dataIndustry);
                        } else {
                            $industryId = $industryIdData;
                        }
                    } else if( $industryIdData == "" && $functionIdData != "" ){
                        if( !is_numeric($functionIdData) ){
                            $dataIndustry['industryName'] = $functionIdData;
                            $dataIndustry['industryType'] = 2;
                            $dataIndustry['createdAt'] = date('Y-m-d h:i:s');
                            $dataIndustry['updatedAt'] = date('Y-m-d h:i:s');
                            $functionId = $this->industry_model->createIndustry($dataIndustry);
                        } else {
                            $functionId = $functionIdData;
                        }
                    }
                    
                    $questions1 = $this->input->post('question1', true);
                    $type_1 = $this->input->post('type_1', true);
                    $tempdata['type1'] = $this->input->post('type_1', true);

                    $questions2 = $this->input->post('question2', true);
                    $type_2 = $this->input->post('type_2', true);
                    $tempdata['type2'] = $this->input->post('type_2', true);

                    if ( count($questions) > 0 && $questions[0]['title'] != "" ) {
                        $practiceInterviewType .= "1,";
                    }

                    if ( count($questions1) > 0 && $questions1[0]['title'] != "" ) {
                        $practiceInterviewType .= "2,";
                    }

                    if ( count($questions2) > 0 && $questions2[0]['title'] != "" ) {
                        $practiceInterviewType .= "3";
                    }                

                    if( $practiceInterviewType != "" ){
                        $practiceInterviewTypeArr = explode(",", $practiceInterviewType);
                        $practiceInterviewTypeArr = array_filter($practiceInterviewTypeArr);
                        $practiceInterviewTypeStr = implode(",", $practiceInterviewTypeArr);

                        $data['practiceInterviewType'] = $practiceInterviewTypeStr;
                    } else {
                        $practiceInterviewTypeStr = "";
                        $data['practiceInterviewType'] = "";
                    }

                    $data['type'] = "0";
                }
                
            } else {
                $data['practiceInterviewType'] = "";
            }
            
            if(count($questions)==0) {
                $resp['error'] .= '<p>Atleast one question is required in the set.</p>';
            }
    
            if (!$resp['error']) {
                
                if( $environment == 2 ){
                    $data['environment'] = $this->input->post('environment', true);
                    
                    if( $industryId != "" ) {
                        $data['industryId'] = $industryId;
                    } else {
                        $data['industryId'] = 0;
                    }
                    
                    if( $functionId != "" ) {
                        $data['functionId'] = $functionId;
                    } else {
                        $data['functionId'] = 0;
                    }
                    
                } else {
                    $data['environment'] = $this->input->post('environment', true);
                    $data['industryId'] = 0;
                    $data['functionId'] = 0;
                }
                
                $data['name'] = $this->input->post('name', true);
                $data['createdAt'] = date('Y-m-d h:i:s');
                $data['updatedAt'] = date('Y-m-d h:i:s');
                $data['createdBy'] = $userId;
                try {
                    $resInterviewId = $this->interview_model->updateInterview($data, $id);
                    if ($resInterviewId) {
                       
                        $interviewQuestions = $this->interview_model->getQuestionsByInterview($id);
                        
                        
                        //Delete Questions Start
                        foreach ($interviewQuestions as $interviewQuestionItem) {
                            //$this->interview_model->deleteQuestionsById($interviewQuestionItem['id']);
                        }
                        $this->interview_model->deleteQuestionsByInterviewSet($id);
                        //Delete Questions End
                        
                        if( $environment == 2 ){
                           
                            if (count($questions) > 0 && $questions[0]['title'] != "") {
                                $f = 0;
                                
                                foreach ($questions as $item) {
                                    
                                    if( is_numeric($item['title']) ){

                                        $input_file_value = $this->input->post('input_file_'.$f, true);
                                        
                                        $questionDetailRes = $this->question_model->getQuestion($item['title']);
                                        
                                        $questionId = $questionDetailRes['id'];
                                        $questionFile = $questionDetailRes['file'];
                                        $questionDuration = $questionDetailRes['duration'];
                                        
                                        if( $input_file_value != $questionFile && isset($_FILES['file_' . $f]) ){
                                            
                                            if (isset($_FILES['file_' . $f]) && $_FILES['file_' . $f] != NULL) {
                                                $item['file'] = $this->fileUpload($_FILES['file_' . $f], $tempdata['type0']);
                                            } else {
                                                $item['file'] = $input_file_value;
                                            }
                                            
                                            $item['title'] = $questionDetailRes['title'];
                                            $item['duration'] = $item['duration'];
                                            $item['createdAt'] = date('Y-m-d h:i:s');
                                            $item['updatedAt'] = date('Y-m-d h:i:s');
                                            $item['createdBy'] = $userId;
                                            $item['type'] = $this->input->post('type', true);
                                            $questionId = $this->interview_model->addQuestion($item, $resInterviewId);
                                            
                                        } 
                                        elseif( $item['duration'] != $questionDuration ){
                                            
                                            if( $input_file_value != $questionFile ){
                                                if (isset($_FILES['file_' . $f]) && $_FILES['file_' . $f] != NULL) {
                                                    $item['file'] = $this->fileUpload($_FILES['file_' . $f], $tempdata['type0']);
                                                } else {
                                                    $item['file'] = $input_file_value;
                                                }
                                            } else {
                                                $item['file'] = $input_file_value;
                                            }
                                            
                                            $item['title'] = $questionDetailRes['title'];
                                            $item['duration'] = $item['duration'];
                                            $item['createdAt'] = date('Y-m-d h:i:s');
                                            $item['updatedAt'] = date('Y-m-d h:i:s');
                                            $item['createdBy'] = $userId;
                                            $item['type'] = $this->input->post('type', true);
                                            $questionId = $this->interview_model->addQuestion($item, $resInterviewId);
                                        } else {
                                            $questionId = $questionId;
                                        }     
                                        
                                    } else {
                                        if (isset($_FILES['file_' . $f]) && $_FILES['file_' . $f] != NULL) {
                                            $item['file'] = $this->fileUpload($_FILES['file_' . $f], $tempdata['type0']);
                                        }
                                        $item['createdAt'] = date('Y-m-d h:i:s');
                                        $item['updatedAt'] = date('Y-m-d h:i:s');
                                        $item['createdBy'] = $userId;
                                        $item['type'] = $this->input->post('type', true);
                                        $questionId = $this->interview_model->addQuestion($item, $resInterviewId);
                                    }
                            
                                    
                                    $interviewQuestion['interviewId'] = $resInterviewId;
                                    $interviewQuestion['practiceInterviewType'] = $this->input->post('type', true);
                                    $interviewQuestion['questionId'] = $questionId;
                                    $interviewQuestion['createdAt'] = date('Y-m-d h:i:s');
                                    $interviewQuestion['updatedAt'] = date('Y-m-d h:i:s');
                                    $this->interview_model->addInterviewQuestion($interviewQuestion);
                                    $f++;
                                }
                            }
                        
                            if (count($questions1) > 0 && $questions1[0]['title'] != "") {
                                $f = 0;
                                foreach ($questions1 as $item) {
                                    
                                    if( is_numeric($item['title']) ){
                                        //echo $input_file_0;
                                        $input_file_1_value = $this->input->post('input_file_1_'.$f, true);
                                        
                                        $questionDetailRes = $this->question_model->getQuestion($item['title']);
                                        
                                        $questionId = $questionDetailRes['id'];
                                        $questionFile = $questionDetailRes['file'];
                                        $questionDuration = $questionDetailRes['duration'];
                                        
                                        if( $input_file_1_value != $questionFile && isset($_FILES['file_1_' . $f]) ){
                                            
                                            if (isset($_FILES['file_1_' . $f]) && $_FILES['file_1_' . $f] != NULL) {
                                                $item['file'] = $this->fileUpload($_FILES['file_1_' . $f], $tempdata['type1']);
                                            } else {
                                                $item['file'] = $input_file_1_value;
                                            }
                                            
                                            $item['title'] = $questionDetailRes['title'];
                                            $item['duration'] = $item['duration'];
                                            $item['createdAt'] = date('Y-m-d h:i:s');
                                            $item['updatedAt'] = date('Y-m-d h:i:s');
                                            $item['createdBy'] = $userId;
                                            $item['type'] = $this->input->post('type_1', true);
                                            $questionId = $this->interview_model->addQuestion($item, $resInterviewId);
                                            
                                        } 
                                        elseif( $item['duration'] != $questionDuration ){
                                            
                                            if( $input_file_1_value != $questionFile ){
                                                if (isset($_FILES['file_1_' . $f]) && $_FILES['file_1_' . $f] != NULL) {
                                                    $item['file'] = $this->fileUpload($_FILES['file_1_' . $f], $tempdata['type1']);
                                                } else {
                                                    $item['file'] = $input_file_1_value;
                                                }
                                            } else {
                                                $item['file'] = $questionFile;
                                            }
                                            
                                            $item['title'] = $questionDetailRes['title'];
                                            $item['duration'] = $item['duration'];
                                            $item['createdAt'] = date('Y-m-d h:i:s');
                                            $item['updatedAt'] = date('Y-m-d h:i:s');
                                            $item['createdBy'] = $userId;
                                            $item['type'] = $this->input->post('type_1', true);
                                            $questionId = $this->interview_model->addQuestion($item, $resInterviewId);
                                        } else {
                                            $questionId = $questionId;
                                        }     
                                        
                                    } else {
                                        if (isset($_FILES['file_1_' . $f]) && $_FILES['file_1_' . $f] != NULL) {
                                            $item['file'] = $this->fileUpload($_FILES['file_1_' . $f], $tempdata['type1']);
                                        }
                                        $item['createdAt'] = date('Y-m-d h:i:s');
                                        $item['updatedAt'] = date('Y-m-d h:i:s');
                                        $item['createdBy'] = $userId;
                                        $item['type'] = $this->input->post('type_1', true);
                                        $questionId = $this->interview_model->addQuestion($item, $resInterviewId);
                                    }
                            
                                    
                                    $interviewQuestion['interviewId'] = $resInterviewId;
                                    $interviewQuestion['practiceInterviewType'] = $this->input->post('type_1', true);
                                    $interviewQuestion['questionId'] = $questionId;
                                    $interviewQuestion['createdAt'] = date('Y-m-d h:i:s');
                                    $interviewQuestion['updatedAt'] = date('Y-m-d h:i:s');
                                    $this->interview_model->addInterviewQuestion($interviewQuestion);
                                    $f++;
                                }
                            }
                            
                            if (count($questions2) > 0 && $questions2[0]['title'] != "") {
                                $f = 0;
                                foreach ($questions2 as $item) {
                                    
                                    if( is_numeric($item['title']) ){
                                        //echo $input_file_0;
                                        
                                        $questionDetailRes = $this->question_model->getQuestion($item['title']);
                                        
                                        $questionId = $questionDetailRes['id'];
                                        $questionFile = $questionDetailRes['file'];
                                        $questionDuration = $questionDetailRes['duration'];
                                        
                                        if( $item['duration'] != $questionDuration ){
                                            
                                            $item['file'] = "";
                                            $item['title'] = $questionDetailRes['title'];
                                            $item['duration'] = $item['duration'];
                                            $item['createdAt'] = date('Y-m-d h:i:s');
                                            $item['updatedAt'] = date('Y-m-d h:i:s');
                                            $item['createdBy'] = $userId;
                                            $item['type'] = $this->input->post('type_2', true);
                                            $questionId = $this->interview_model->addQuestion($item, $resInterviewId);
                                        } else {
                                            $questionId = $questionId;
                                        }     
                                        
                                    } else {
                                        $item['file'] = "";
                                        $item['createdAt'] = date('Y-m-d h:i:s');
                                        $item['updatedAt'] = date('Y-m-d h:i:s');
                                        $item['createdBy'] = $userId;
                                        $item['type'] = $this->input->post('type_2', true);
                                        $questionId = $this->interview_model->addQuestion($item, $resInterviewId);
                                    }
                            
                                    
                                    $interviewQuestion['interviewId'] = $resInterviewId;
                                    $interviewQuestion['practiceInterviewType'] = $this->input->post('type_2', true);
                                    $interviewQuestion['questionId'] = $questionId;
                                    $interviewQuestion['createdAt'] = date('Y-m-d h:i:s');
                                    $interviewQuestion['updatedAt'] = date('Y-m-d h:i:s');
                                    $this->interview_model->addInterviewQuestion($interviewQuestion);
                                    $f++;
                                }
                            }
                        } else {
                            if (count($questions) > 0 && $questions[0]['title'] != "") {
                                $f = 0;
                                
                                foreach ($questions as $item) {
                                    
                                    if( is_numeric($item['title']) ){
                                        //echo $input_file_0;
                                        $input_file_value = $this->input->post('input_file_'.$f, true);
                                        
                                        $questionDetailRes = $this->question_model->getQuestion($item['title']);
                                        
                                        $questionId = $questionDetailRes['id'];
                                        $questionFile = $questionDetailRes['file'];
                                        $questionDuration = $questionDetailRes['duration'];
                                        
                                        if( $input_file_value != $questionFile && isset($_FILES['file_' . $f]) ){
                                            
                                            if (isset($_FILES['file_' . $f]) && $_FILES['file_' . $f] != NULL) {
                                                $item['file'] = $this->fileUpload($_FILES['file_' . $f], $data['type']);
                                            } else {
                                                $item['file'] = $input_file_value;
                                            }
                                            
                                            $item['title'] = $questionDetailRes['title'];
                                            $item['duration'] = $item['duration'];
                                            $item['createdAt'] = date('Y-m-d h:i:s');
                                            $item['updatedAt'] = date('Y-m-d h:i:s');
                                            $item['createdBy'] = $userId;
                                            $item['type'] = $this->input->post('type', true);
                                            $questionId = $this->interview_model->addQuestion($item, $resInterviewId);
                                            
                                        } 
                                        elseif( $item['duration'] != $questionDuration ){
                                            
                                            if( $input_file_value != $questionFile ){
                                                if (isset($_FILES['file_' . $f]) && $_FILES['file_' . $f] != NULL) {
                                                    $item['file'] = $this->fileUpload($_FILES['file_' . $f], $data['type']);
                                                } else {
                                                    $item['file'] = $input_file_value;
                                                }
                                            } else {
                                                $item['file'] = $questionFile;
                                            }
                                            
                                            $item['title'] = $questionDetailRes['title'];
                                            $item['duration'] = $item['duration'];
                                            $item['createdAt'] = date('Y-m-d h:i:s');
                                            $item['updatedAt'] = date('Y-m-d h:i:s');
                                            $item['createdBy'] = $userId;
                                            $item['type'] = $this->input->post('type', true);
                                            $questionId = $this->interview_model->addQuestion($item, $resInterviewId);
                                        } else {
                                            $questionId = $questionId;
                                        }     
                                        
                                    } else {
                                        if (isset($_FILES['file_' . $f]) && $_FILES['file_' . $f] != NULL) {
                                            $item['file'] = $this->fileUpload($_FILES['file_' . $f], $data['type']);
                                        }
                                        $item['createdAt'] = date('Y-m-d h:i:s');
                                        $item['updatedAt'] = date('Y-m-d h:i:s');
                                        $item['createdBy'] = $userId;
                                        $item['type'] = $this->input->post('type', true);
                                        $questionId = $this->interview_model->addQuestion($item, $resInterviewId);
                                    }
                            
                                    
                                    $interviewQuestion['interviewId'] = $resInterviewId;
                                    //$interviewQuestion['practiceInterviewType'] = "";
                                    $interviewQuestion['questionId'] = $questionId;
                                    $interviewQuestion['createdAt'] = date('Y-m-d h:i:s');
                                    $interviewQuestion['updatedAt'] = date('Y-m-d h:i:s');
                                    $this->interview_model->addInterviewQuestion($interviewQuestion);
                                    $f++;
                                }
                            }
                        }
                        
                        
                        $info['message'] = 'Successfully created!';
                        redirect(base_url('interviews/list/1'));
                        $this->load->view('fj-mainpage', $info);
                    }
                } catch (Exception $e) {
                    $resp['error'] = $e->getMessage();
                }
            } else {
                $info['error'] = $resp;
                $info['interview'] = $this->input->post();
                $this->load->view('fj-mainpage', $info);
            }
        } else {
            $this->load->view('fj-mainpage', $info);
        }
    }

    /**
     * Discription : Use to delete an interview set
     * Author : Synergy
     * @param array $_POST
     * @return string the success message | error message.
     */
    function deleteInterview() {
        $error = 0;
        $token = $this->config->item('accessToken');
        $fromEmail = $this->config->item('fromEmail');
        $interviewId = $this->input->post('id', true);
        if (!$interviewId) {
            echo $error = 'Undefined interview!';
            exit;
        }
        if (!$error) {
            $data['status'] = 3;
            $resp = $this->interview_model->updateInterview($data, $interviewId);
            if ($resp) {
                echo "Deleted successfully";
            }
        }
    }

    /**
     * Discription : Use to Checks interview in live interview set.
     * Author : Synergy
     * @param array $_POST
     * @return boolean
     */
    function checkInterviewInLiveJob($interviewId) {
        if (checkInterviewInLiveJob($interviewId)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Discription : Use to get interview details
     * Author : Synergy
     * @param int $id
     * @return void
     */
    function getInterview($id) {
        $interview = $this->interview_model->getInterview($id);
        $data['questions'] = $this->interview_model->getQuestionsByInterview($id);
        $token = $this->config->item('accessToken');
        $data['main_content'] = 'fj-view-interview';
        $data['interview'] = $interview;
        $data['token'] = $token;
        $this->load->view('fj-mainpage', $data);
    }

    /**
     * Discription : Use to list interview sets
     * Author : Synergy
     * @param int $page
     * @return render data in view
     */
    function listInterviews($page) {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        //Interviews Listing Start
        $urlSeg = $this->uri->segment(2);
        
        $userData = $this->session->userdata['logged_in'];
        $userRole = $userData->role;
        $userLoggedinUserId = $userData->id;
        
        $data['page'] = $page;
        $resp['error'] = 0;
        $serachColumn = $this->input->post('serachColumn', true);
        $searchText = $this->input->post('serachText', true);
        $token = $this->config->item('accessToken');
        $fromEmail = $this->config->item('fromEmail');
        $itemsPerPage = $this->config->item('itemsPerPage');
        $data['itemsPerPage'] = $itemsPerPage;
       // $createdBy = NULL;
        $createdBy = $userData->id;
        if (!$page) {
            $page = 0;
        } else {
            $page = $page - 1;
        }
        $offset = $page * $itemsPerPage;
        try {
            
            if( $urlSeg == "fjlist" ){
                $offsetFj = $page * $itemsPerPage;
                $offset = 0;
            } else {
                $offsetFj = 0;
            }
            
            
            if( $urlSeg == "mylist" ){
                $offsetMy = $page * $itemsPerPage;
                $offset = 0;
            } else {
                $offsetMy = 0;
            }
            
            
            $createdByStr = getAllCorporateAndSubuserIds($userLoggedinUserId, $userRole);
            
            
            $result = $this->interview_model->listInterviews($itemsPerPage, $offset, $serachColumn, $searchText, $createdByStr, $userRole);
            $i = 0;
            //echo "<pre>";
            
            foreach ($result as $item) {
                $questionByInterview = $this->interview_model->getQuestionsByInterview($item->id);
                $result[$i]->questionCount = count($questionByInterview);
                $i++;
            }
            $completeResult = $this->interview_model->listInterviews(NULL, NULL, NULL, NULL, $createdByStr);
            $count = ceil(count($completeResult) / $itemsPerPage);

            $data['count'] = $count;
            $data['totalCount'] = count($completeResult);
            $data['contentInterviewSet'] = $result;
            $data['main_content'] = 'fj-interview-listing';
            $data['searchText'] = $searchText;
            $data['serachColumn'] = $serachColumn;
            $data['token'] = $token;

            //List Fj Questions
            $resultFjQuestions          = $this->interview_model->listFjQuestions($itemsPerPage, $offsetFj, $serachColumn, $searchText, $createdBy);
            $completeResultFjQuestions  = $this->interview_model->listFjQuestions(NULL, NULL, NULL, NULL, $createdBy);
            $countFj                    = ceil(count($completeResultFjQuestions) / $itemsPerPage);
            $data['countFj']            = $countFj;
            $data['totalCountFj']       = count($completeResultFjQuestions);
            $data['contentFjQuestions'] = $resultFjQuestions;

            $resultMyQuestions          = getMyQuestionsPagination($itemsPerPage, $offsetMy, $serachColumn, $searchText, $createdByStr);
            $completeResultMyQuestions  = getMyQuestionsPagination(NULL, NULL, NULL, NULL, $createdByStr);
            $countMy                    = ceil(count($completeResultMyQuestions) / $itemsPerPage);            
            $data['countMy']            = $countMy;
            $data['totalCountMy']       = count($completeResultMyQuestions);
            $data['contentMyQuestions'] = $resultMyQuestions;
            
            //List Fj Questions

            $this->load->view('fj-mainpage', $data);
            if (!$result) {
                //throw new Exception('Could not get list of users!');
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
        //Interviews Listing End
    }

    /**
     * Discription : Use to get list all the questions which are created by logged in user
     * Author : Synergy
     * @param array $_POST
     * @return html entities with list of questions
     */
    function getMyQuestionRow() {
        $index = $this->input->post('index', true);
        $fjQuestions = getFjQuestions();
        $fjQuestion = '';
        foreach ($fjQuestions as $item) {
            $fjQuestion .= '<option>' . $item['title'] . '</option>';
        }
        $str = getMyQuestionRow($fjQuestion, $index);
        echo $str;
        exit;
    }

    /**
     * Discription : Use to get list all the questions which are created by logged in user or super admin user
     * Author : Synergy
     * @param array $_POST
     * @return html entities with list of questions
     */
    function getQuestionRow() {
        $userData = $this->session->userdata['logged_in'];
        $userId = $userData->id;
        $index = $this->input->post('index', true);
        $indexIncr = $index+1;
        $qType = $this->input->post('qtype', true);
        $quesType = $this->input->post('questype', true);
        
        if($qType == 1) {
            $labelText = 'VH Question Bank';
            $fjQuestions = getFjQuestions($quesType);
        } else {
            $labelText = 'My Question Bank';
            $fjQuestions = getMyQuestionsNew($userId, $quesType);
        }
        
        $fjQuestion = '';
        foreach ($fjQuestions as $item) {
            $fjQuestion .= '<option value="'.$item['id'].'">' . $item['title'] . '</option>';
        }
        $str = '<div class="form-group hasclass" style="margin-left: 0px !important; margin-right: 0px !important;">
                            <label for="inputPassword" class="control-label col-xs-2">'.$labelText.'</label>
                            <div class="col-xs-4 ques-title questionOnTypeBlock" id="questions1'.$indexIncr.'">
                                <label for="inputPassword" class="control-label frm_in_padd">Question Title</label>
                                <select data-placeholder="Type a question" style="width: 350px; display: none;" class="chosen-select sel_sty_padd interviewQuestion" tabindex="-1" name="question[' . $index . '][title]">
                                    <option value=""></option>' . $fjQuestion . '
                                </select>
                                <input type="text" data-validation="interviewQuestion" style="height: 0px; width: 0px; visibility: hidden; ">
                            </div>

                            <div class="col-xs-3 file-section">
                                <div class="col-md-12">
                                    <label for="inputPassword" class="control-label frm_in_padd" style="font-size: 12px;">Upload File(mp3, mp4)</label>
                                </div>
                                <div class="col-xs-8">
                                    <input type="text" class="form-control file-name interviewFileNames" name="input_file_' . $index . '" id="filetextboxforimage1'.$indexIncr.'"  readonly="readonly">
                                </div>
                                <div class="col-xs-4 no_padding browse-section" id="filebrowseforimage1'.$indexIncr.'">
                                    <button type="button" class="btn_upload">BROWSE</button>
                                    <input type="file" name="file_' . $index . '" class="question-file interviewFile">
                                    <input type="text" data-validation="interviewFile" style="height: 0px; width: 0px; visibility: hidden; ">
                                </div>
                                <div class="question-video-create-page">
                                    <div id="questionVideo1'.$indexIncr.'" class="question-video questionvideoshowblock"></div>
                                </div>
                            </div>

                            <div class="col-xs-3">
                                <label for="inputPassword" class="control-label frm_in_padd col-xs-12">Answer Duration <span style="font-size:10px;">(Limit 600 Sec)</span></label><div class="clearfix"></div>
                                <div class="col-md-6 no_padding">
                                    <input type="number" class="form-control questiondurationtogetid interviewDuration" placeholder="" name="question[' . $index . '][duration]" id="durations1'.$indexIncr.'" maxlength="3" min="1">
                                    <input type="text" data-validation="interviewDuration" style="height: 0px; width: 0px; visibility: hidden; ">
                                </div>
                                <div class="col-md-4">
                                    <p class="padd_txtmin">Sec</p>
                            </div>
                            <div class="col-md-2" style="padding: 0px !important;">                                       
                                <button type="button" class="del-question1 pull-right" value="testing" style="float: left !important; width: auto;">x</button>
                            </div><div class="clearfix"></div>'
                . '</div>'
                . '</div>';
        echo $str;
        exit;
    }
    
    /**
     * Discription : Use to list all the questions which are created by super admin user
     * Author : Synergy
     * @param array $_POST
     * @return html entities with list of questions
     */
    function getQuestionFirstRow() {
        $userData = $this->session->userdata['logged_in'];
        $userId = $userData->id;
        $index = $this->input->post('index', true);
        $indexIncr = $index+1;
        $qType = $this->input->post('qType', true);
        $quesType = $this->input->post('questype', true);
        
        if($qType == 1) {
        $fjQuestions = getFjQuestions($quesType);
        } else {
        $fjQuestions = getMyQuestionsNew($userId, $quesType);
        }
        
        $fjQuestion = '';
        foreach ($fjQuestions as $item) {
            $fjQuestion .= '<option value="'.$item['id'].'">' . $item['title'] . '</option>';
        }
        $str = '<div class="form-group">
                            <label for="inputPassword" class="control-label col-xs-2"></label>
                            <div class="col-xs-4 ques-title1 questionOnTypeBlock1">
                                <label for="inputPassword" class="control-label frm_in_padd">Question Title</label>
                                <select data-placeholder="Type a question" style="width: 350px; display: none;" class="chosen-select1 sel_sty_padd interviewAudioQuestion" tabindex="-1" name="question1[' . $index . '][title]">
                                    <option value=""></option>' . $fjQuestion . '
                                </select>
                            </div>

                            <div class="col-xs-3 file-section1">
                                <div class="col-md-12">
                                    <label for="inputPassword" class="control-label frm_in_padd" style="font-size: 12px;">Upload File(mp3, mp4)</label>
                                </div>
                                <div class="col-xs-8">
                                    <input type="text" class="form-control file-name1 interviewQuestionFileName" name="input_file_1_' . $index . '" id="filetextboxforimage2'.$indexIncr.'">
                                </div>
                                <div class="col-xs-4 no_padding browse-section" id="filetextboxforimage2'.$indexIncr.'">
                                    <button type="button" class="btn_upload">BROWSE</button>
                                    <input type="file" name="file_1_' . $index . '" class="question-file1 interviewQuestionFile">
                                </div>
                                <div class="question-video-create-page">
                                    <div id="questionVideo2'.$indexIncr.'" class="question-video questionvideoshowblock1"></div>
                                </div>
                            </div>

                            <div class="col-xs-3">
                                <label for="inputPassword" class="control-label frm_in_padd col-xs-12">Answer Duration <span style="font-size:10px;">(Limit 600 Sec)</span></label>
                                <div class="col-md-6 no_padding">
                                    <input type="text" class="form-control questiondurationtogetid1 interviewAudioDuration" placeholder="" name="question1[' . $index . '][duration]" id="durations2'.$indexIncr.'" maxlength="3">
                                </div>
                                <div class="col-md-4">
                                    <p class="padd_txtmin">Sec</p>
                            </div>
                            <div class="col-md-2">                                       
                                <button type="button" class="del-question2 pull-right" value="testing">x</button>
                            </div>'
                . '</div>'
                . '</div>';
        echo $str;
        exit;
    }
    
    /**
     * Discription : Use to list all the questions which are created by super admin user
     * Author : Synergy
     * @param array $_POST
     * @return html entities with list of questions
     */
    function getQuestionSecondRow() {
        $userData = $this->session->userdata['logged_in'];
        $userId = $userData->id;
        $index = $this->input->post('index', true);
        $indexIncr = $index+1;
        $qType = $this->input->post('qType', true);
        $quesType = $this->input->post('questype', true);
        
        if($qType == 1) {
        $fjQuestions = getFjQuestions($quesType);
        } else {
        $fjQuestions = getMyQuestionsNew($userId, $quesType);
        }
        
        $fjQuestion = '';
        foreach ($fjQuestions as $item) {
            $fjQuestion .= '<option value="'.$item['id'].'">' . $item['title'] . '</option>';
        }
        $str = '<div class="form-group">
                            <label for="inputPassword" class="control-label col-xs-2"></label>
                            <div class="col-xs-4 ques-title2 questionOnTypeBlock2">
                                <label for="inputPassword" class="control-label frm_in_padd">Question Title</label>
                                <select data-placeholder="Type a question" style="width: 350px; display: none;" class="chosen-select2 sel_sty_padd interviewTextQuestion" tabindex="-1" name="question2[' . $index . '][title]">
                                    <option></option>' . $fjQuestion . '
                                </select>
                            </div>

                            <!-- div class="col-xs-3 file-section2">
                                <div class="col-md-12">
                                    <label for="inputPassword" class="control-label frm_in_padd" style="font-size: 12px;">Upload File(mp3, mp4)</label>
                                </div>
                                <div class="col-xs-8">
                                    <input type="text" class="form-control file-name2">
                                </div>
                                <div class="col-xs-4 no_padding browse-section">
                                    <button type="button" class="btn_upload">BROWSE</button>
                                    <input type="file" name="file_2_' . $index . '" class="question-file2">
                                </div>
                            </div -->

                            <div class="col-xs-3">
                                <label for="inputPassword" class="control-label frm_in_padd col-xs-12">Answer Duration <span style="font-size:10px;">(Limit 600 Sec)</span></label>
                                <div class="col-md-6 no_padding">
                                    <input type="text" class="form-control questiondurationtogetid2 interviewTextDuration" placeholder="" name="question2[' . $index . '][duration]" id="durations3'.$indexIncr.'" maxlength="3">
                                </div>
                                <div class="col-md-4">
                                    <p class="padd_txtmin">Sec </p>
                            </div>
                            <div class="col-md-2">                                       
                                <button type="button" class="del-question3 pull-right" value="testing">x</button>
                            </div>'
                . '</div>'
                . '</div>';
        echo $str;
        exit;
    }
    
    /**
     * Discription : Use to add question in interview sets
     * Author : Synergy
     * @param array $_POST
     * @return render data into view
     */
    function addQuestion() {
        if (!$this->session->userdata('logged_in')) {
            //redirect(base_url('users/login'));
        }
        $token = $this->config->item('accessToken');
        $userData = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            //redirect(base_url('users/login'));
            $userId = 1;
        }
        $info['main_content'] = 'fj-create-question';
        $info['token'] = $token;
        $info['fjQuestion'] = getFjQuestions();
        if (isset($_POST) && $_POST != NULL) {
            $questions = $this->input->post('question', true);
            $info['questions'] = $questions;
            $data['type'] = $this->input->post('type', true);
            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }
            if ($questions[0]['title'] == '' && $questions[0]['type'] == 3) {
                $resp['error'] .= '<p>Title field of first question is required.</p>';
            }
            if ($questions[0]['duration'] == '') {
                $resp['error'] .= '<p>Answer Duration field of first question is required.</p>';
            }
            if ($_FILES['file_0']['name'] == '' && $questions[0]['type'] != 3) {
                $resp['error'] .= '<p>Please upload file of first question.</p>';
            }
            if (!$resp['error']) {
                $data['name'] = $this->input->post('name', true);
                $data['createdAt'] = date('Y-m-d h:i:s');
                $data['updatedAt'] = date('Y-m-d h:i:s');
                $data['createdBy'] = $userId;
                try {
                    $f = 0;
                    if (count($questions) > 0) {
                        foreach ($questions as $item) {
                            //echo 
                            $questionType = $item['type'];
                            
                            if (isset($_FILES['file_' . $f]) && $_FILES['file_' . $f] != NULL) {
                                $item['file'] = $this->fileUpload($_FILES['file_' . $f], $questionType);
                            }
                            $item['createdAt'] = date('Y-m-d h:i:s');
                            $item['updatedAt'] = date('Y-m-d h:i:s');
                            $item['createdBy'] = $userId;
                            
                            if( $item['title'] != "" ){
                                $questionId = $this->interview_model->addQuestion($item);
                            }
                            $f++;
                        }
                    }
                    redirect(base_url('interviews/mylist/1'));
                } catch (Exception $e) {
                    $resp['error'] = $e->getMessage();
                }
            } else {
                $info['error'] = $resp;
                $info['interview'] = $this->input->post();
                $this->load->view('fj-mainpage', $info);
            }
        } else {
            $this->load->view('fj-mainpage', $info);
        }
    }

    /**
     * Discription : Use to add question in interview sets
     * Author : Synergy
     * @param array $_POST
     * @return render data into view
     */
    function addQuestionRecruiter() {
        //print '<pre>';print_r($_FILES);
        //print '<pre>';print_r($_POST);exit;
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token = $this->config->item('accessToken');
        $userData = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            $userId = 1;
            redirect(base_url('users/login'));
        }

        $allQuestins = array();
        if (isset($_POST) && $_POST != NULL) {
            $questions = $this->input->post('question', true);
            try {
                $f = 0;
                if (count($questions) > 0) {
                    foreach ($questions as $item) {
                        //echo 
                        $questionType = $item['type'];
                        
                        if (isset($_FILES['file_' . $f]['name']) && !empty($_FILES['file_' . $f]['name'])) {
                            $item['file'] = $this->fileUpload($_FILES['file_' . $f], $questionType);
                            //echo $item['file'];exit;
                        }

                        $item['type'] = $questionType;
                        $item['duration'] = $item['duration'];
                        $item['status'] = '1';
                        $item['answer'] = '';
                        $item['createdAt'] = date('Y-m-d h:i:s');
                        $item['updatedAt'] = date('Y-m-d h:i:s');
                        $item['createdBy'] = $userId;
                        $item['title'] = $item['title'];

                        $questionId = $this->interview_model->addQuestion($item);
                        $item['questionId'] = $questionId;

                        $allQuestins[] = $item;

                        $f++;
                    }

                    $returnData = array('questions' => $allQuestins, 'errorMessage' => '');
                    echo json_encode($returnData);exit;
                }
                $returnData = array('questions' => $allQuestins, 'errorMessage' => 'Please add Question!');
                echo json_encode($returnData);exit;
            } catch (Exception $e) {
                $returnData = array('questions' => $allQuestins, 'errorMessage' => 'Please add Question!');
                echo json_encode($returnData);exit;
            }
        } else {
            $returnData = array('questions' => $allQuestins, 'errorMessage' => 'Unauthorised access!');
            echo json_encode($returnData);exit;
        }
    }

    /**
     * Discription : Use to update question in an interview set 
     * Author : Synergy
     * @param int $id, array $_POST
     * @return render data into view
     */
    function editQuestion($id) {
        if (!$this->session->userdata('logged_in')) {
            //redirect(base_url('users/login'));
        }
        $token = $this->config->item('accessToken');
        $userData = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            //redirect(base_url('users/login'));
            $userId = 1;
        }
        $info['main_content'] = 'fj-edit-question';
        $info['token'] = $token;
        $info['fjQuestion'] = getFjQuestions();
        $info['questions'] = $this->interview_model->getQuestion($id);
        if (isset($_POST) && $_POST != NULL) {
            $questions = $this->input->post('question', true);
            $info['questions'] = $questions;
            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }
            if ($questions['title'] == '' && $questions['type'] == 3) {
                $resp['error'] .= '<p>Title field of  question is required.</p>';
            }
            if ($questions['duration'] == '') {
                $resp['error'] .= '<p>Answer Duration field of question is required.</p>';
            }
            if (!$resp['error']) {
                $data['createdAt'] = date('Y-m-d h:i:s');
                $data['updatedAt'] = date('Y-m-d h:i:s');
                $data['createdBy'] = $userId;
                $data['duration'] = $questions['duration'];
                $data['title'] = $questions['title'];
                if(isset($questions['type']) && $questions['type'] != NULL) {
                    $data['type'] = $questions['type'];
                }
                try {
                    if (isset($_FILES['file_0']['name']) && $_FILES['file_0']['name'] != NULL) {
                        $data['file'] = $this->fileUpload($_FILES['file_0'], $data['type']);
                    }
                    $questionId = $this->interview_model->updateQuestion($data, $id);

                    redirect(base_url('interviews/mylist/1'));
                } catch (Exception $e) {
                    $resp['error'] = $e->getMessage();
                }
            } else {
                $info['error'] = $resp;
                $info['interview'] = $this->input->post();
                $this->load->view('fj-mainpage', $info);
            }
        } else {
            $this->load->view('fj-mainpage', $info);
        }
    }

    /**
     * Discription : Use to update question in an interview set 
     * Author : Synergy
     * @param int $id, array $_POST
     * @return render data into view
     */
    function getQuestionDetails() {
        $questionId = $this->input->post('questionId', true);
        $question = $this->interview_model->getQuestion($questionId);
        $return  = json_encode(array('question' => $question));
        echo $return;exit;
    }

    /**
     * Discription : Use to update question in an interview set 
     * Author : Synergy
     * @param int $id, array $_POST
     * @return render data into view
     */
    function editUpdateQuestion() {
       
        $token = $this->config->item('accessToken');
        $userData = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            $userId = 1;
        }

        
        if (isset($_POST) && $_POST != NULL) {
            $questionsType = $this->input->post('editQuestionType', true);
            $questionsTitle = $this->input->post('editQuestionTitle', true);
            $questionsDuratin = $this->input->post('editQuestionDuration', true);
            $id = $this->input->post('questionId', true);

            
            $data['createdAt'] = date('Y-m-d h:i:s');
            $data['updatedAt'] = date('Y-m-d h:i:s');
            $data['createdBy'] = $userId;
            $data['duration'] = $questionsDuratin;
            $data['title'] = $questionsTitle;
            if(isset($questionsType) && $questionsType != NULL) {
                $data['type'] = $questionsType;
            }
            try {
                if (isset($_FILES['file_0']['name']) && !empty($_FILES['file_0']['name'])) {
                    $data['file'] = $this->fileUpload($_FILES['file_0'], $data['type']);
                }
                $this->interview_model->updateQuestion($data, $id);

                $returnData = json_encode(array('title' => $questionsTitle, 'duration' => $questionsDuratin, 'questionId' => $id, 'type' => $questionsType));
                echo $returnData;exit;
            } catch (Exception $e) {
                $returnData = json_encode(array('title' => $questionsTitle, 'duration' => $questionsDuratin, 'questionId' => $id, 'type' => $questionsType));
                echo $returnData;exit;
            }
        } else {
            $returnData = json_encode(array('title' => '', 'duration' => ''));
                echo $returnData;exit;
        }
    }
    
    
    /**
     * Discription : Use to update question in an interview set 
     * Author : Synergy
     * @param array $_POST
     * @return render data into view
     */
    function getIndustryFunction() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token = $this->config->item('accessToken');
        $userData = $this->session->userdata['logged_in'];
       
        $resp['error'] = 0;
        $indestryId = $this->input->post('industryId', true);
       
        if (!$indestryId) {
            $error = "NotDone";
            echo $error;
            exit();
        }
        if (!$resp['error']) {
            $allIndustryFunctionRes = $this->industry_model->getListOfFunction($indestryId);
            
            if( $allIndustryFunctionRes ){
                $stringquery = "<select class=\"selectpicker\" name=\"functionId\" id=\"functionId\"><option value=\"\">Select Function</option>";
                foreach ($allIndustryFunctionRes as $item){
                    $stringquery .= "<option value=\"".$item->id."\">".$item->name."</option>";
                }
                $stringquery .="</select>";
            } else {
                $stringquery = "<select class=\"selectpicker\" name=\"functionId\" id=\"functionId\"><option value=\"\">Select Function</option>";
                $stringquery .="</select>";
            }
            
            echo $stringquery;
            exit();
        }
    }

    /**
     * Discription : Use to get all questions of a specific question type
     * Author : Synergy
     * @param array $_POST
     * @return return all questions in html select list 
     */
    function getQuestionOnTypeFunction() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token = $this->config->item('accessToken');
        $userData = $this->session->userdata['logged_in'];
       
        $resp['error'] = 0;
        $questionTypeId = $this->input->post('questionTypeId', true);
       
        if (!$questionTypeId) {
            $error = "NotDone";
            echo $error;
            exit();
        }
        if (!$resp['error']) {
            
            $allQuestionOnTypeRes = $this->interview_model->getQuestionOnType($questionTypeId);
            
            if( $allQuestionOnTypeRes ){
                
                $stringquery = "<label for=\"inputPassword\" class=\"control-label frm_in_padd\">Question Title</label>";
                $stringquery .= "<select data-placeholder=\"Type a question\" style=\"width: 350px; display: none;\" class=\"chosen-select sel_sty_padd interviewQuestion\" tabindex=\"-1\" name=\"question[0][title]\"><option value=\"\"></option>";
                foreach ($allQuestionOnTypeRes as $item):
                        $stringquery .= "<option value=\"".$item->id."\">".$item->title."</option>";
                endforeach;
                $stringquery .="</select>";
                $stringquery .='<input type="text" data-validation="interviewQuestion" style="height: 0px; width: 0px; visibility: hidden; " />';
            } else {
                $stringquery = "<label for=\"inputPassword\" class=\"control-label frm_in_padd\">Question Title</label>";
                $stringquery .= "<select data-placeholder=\"Type a question\" style=\"width: 350px; display: none;\" class=\"chosen-select sel_sty_padd\" tabindex=\"-1\" name=\"question[0][title]\"><option value=\"\"></option>";
                $stringquery .="</select>";
            }
            
            echo $stringquery;
            exit();
        }
    }
    
    /**
     * Discription : Use to get all questions of a specific question type
     * Author : Synergy
     * @param array $_POST
     * @return return all questions in html select list 
     */
    function getQuestionOnTypeFirstFunction() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token = $this->config->item('accessToken');
        $userData = $this->session->userdata['logged_in'];
       
        $resp['error'] = 0;
        $questionTypeId = $this->input->post('questionTypeId', true);
       
        if (!$questionTypeId) {
            $error = "NotDone";
            echo $error;
            exit();
        }
        if (!$resp['error']) {
            
            $allQuestionOnTypeRes = $this->interview_model->getQuestionOnType($questionTypeId);
            
            if( $allQuestionOnTypeRes ){
                
                $stringquery = "<label for=\"inputPassword\" class=\"control-label frm_in_padd\">Question Title</label>";
                $stringquery .= "<select data-placeholder=\"Type a question\" style=\"width: 350px; display: none;\" class=\"chosen-select1 sel_sty_padd interviewAudioQuestion\" tabindex=\"-1\" name=\"question1[0][title]\"><option value=\"\"></option>";
                foreach ($allQuestionOnTypeRes as $item):
                        $stringquery .= "<option value=\"".$item->id."\">".$item->title."</option>";
                endforeach;
                $stringquery .="</select>";
                $stringquery .='<input type="text" data-validation="interviewAudioQuestion" style="height: 0px; width: 0px; visibility: hidden; " />';
            } else {
                $stringquery = "<label for=\"inputPassword\" class=\"control-label frm_in_padd\">Question Title</label><select data-placeholder=\"Type a question\" style=\"width: 350px; display: none;\" class=\"chosen-select1 sel_sty_padd\" tabindex=\"-1\" name=\"question1[0][title]\"><option value=\"\"></option>";
                $stringquery .="</select>";
            }
            
            echo $stringquery;
            exit();
        }
    }
    
    /**
     * Discription : Use to get all questions of a specific question type
     * Author : Synergy
     * @param array $_POST
     * @return return all questions in html select list 
     */
    function getQuestionOnTypeSecondFunction() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token = $this->config->item('accessToken');
        $userData = $this->session->userdata['logged_in'];
       
        $resp['error'] = 0;
        $questionTypeId = $this->input->post('questionTypeId', true);
       
        if (!$questionTypeId) {
            $error = "NotDone";
            echo $error;
            exit();
        }
        if (!$resp['error']) {
            
            $allQuestionOnTypeRes = $this->interview_model->getQuestionOnType($questionTypeId);
            
            if( $allQuestionOnTypeRes ){
                
                $stringquery = "<label for=\"inputPassword\" class=\"control-label frm_in_padd\">Question Title</label>";
                $stringquery .= "<select data-placeholder=\"Type a question\" style=\"width: 350px; display: none;\" class=\"chosen-select2 sel_sty_padd interviewTextQuestion\" tabindex=\"-1\" name=\"question2[0][title]\"><option value=\"\"></option>";
                foreach ($allQuestionOnTypeRes as $item):
                        $stringquery .= "<option value=\"".$item->id."\">".$item->title."</option>";
                endforeach;
                $stringquery .="</select>";
                $stringquery .='<input type="text" data-validation="interviewTextQuestion" style="height: 0px; width: 0px; visibility: hidden; " />';
            } else {
                $stringquery = "<label for=\"inputPassword\" class=\"control-label frm_in_padd\">Question Title</label><select data-placeholder=\"Type a question\" style=\"width: 350px; display: none;\" class=\"chosen-select2 sel_sty_padd\" tabindex=\"-1\" name=\"question2[0][title]\"><option value=\"\"></option>";
                $stringquery .="</select>";
            }
            
            echo $stringquery;
            exit();
        }
    }
    
    /**
     * Discription : Use to get details of a question
     * Author : Synergy
     * @param array $_POST
     * @return return string
     */
    function getQuestionDetailsByQuestionId() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token = $this->config->item('accessToken');
        $userData = $this->session->userdata['logged_in'];
       
        $resp['error'] = 0;
        $questionValueId = $this->input->post('questionValueId', true);
        
        if (!$questionValueId) {
            $error = "NotDone";
            echo $error;
            exit();
        }
        if (!$resp['error']) {
            
            $questionDetailsRes = getQuestionsById($questionValueId);
            
            if( $questionDetailsRes ){
                if( $questionDetailsRes['file'] != "" && $questionDetailsRes['file'] != null && $questionDetailsRes['file'] != '0' ){
                    $questionDetailsStr = $questionDetailsRes['file'];
                } else {
                    $questionDetailsStr = "NotVideo";
                }
                
                if( $questionDetailsRes['type'] != "" && $questionDetailsRes['type'] != null ){
                    $questionDetailsStr .= "####".$questionDetailsRes['type'];
                } else {
                    $questionDetailsStr .= "####NotType";
                }
                
                if( $questionDetailsRes['duration'] != "" && $questionDetailsRes['duration'] != null ){
                    $questionDetailsStr .= "####".$questionDetailsRes['duration'];
                } else {
                    $questionDetailsStr .= "####NotDuration";
                }
                
                echo $questionDetailsStr;
                exit();            
            } else {
                echo "NotDone";
                exit();     
            }            
        }
    }

    /**
     * Discription : Use to delete a specific question
     * Author : Synergy
     * @param array $_POST
     * @return return string
     */
    function deleteQuestion() {
        $error = 0;
        $token = $this->config->item('accessToken');
        $fromEmail = $this->config->item('fromEmail');
        $questionId = $this->input->post('questionId', true);
        if (!$questionId) {
            echo $error = 'Undefined question!';
            exit;
        }
        if (!$error) {
            $data['status'] = 3;
            $resp = $this->question_model->updateQuestion($data, $questionId);
            if ($resp) {
                echo "Deleted successfully";
            }
        }
    }
    
    /**
     * Discription : Use to copy the existing question
     * Author : Synergy
     * @param array $_POST
     * @return return string
     */
    function CopyfjQuestion() {
        $error = 0;
        $token = $this->config->item('accessToken');
        $fromEmail = $this->config->item('fromEmail');
        
        $userData = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            redirect(base_url('users/login'));
        }
        
        $questionId = $this->input->post('questionId', true);
        if (!$questionId) {
            echo $error = 'Undefined question!';
            exit;
        }
        if (!$error) {
            
            $questionDetailsRes = getQuestionsById($questionId);
            
            if( $questionDetailsRes ){
                $datavalue['title'] = $questionDetailsRes['title'];
                $datavalue['description'] = $questionDetailsRes['description'];
                $datavalue['file'] = $questionDetailsRes['file'];
                $datavalue['type'] = $questionDetailsRes['type'];
                $datavalue['duration'] = $questionDetailsRes['duration'];
                $datavalue['IsMultipleChoice'] = $questionDetailsRes['IsMultipleChoice'];
                $datavalue['answer'] = $questionDetailsRes['answer'];
                $datavalue['status'] = $questionDetailsRes['status'];
                $datavalue['createdBy'] = $userId;
                $datavalue['createdAt'] = date('Y-m-d h:i:s');
                $datavalue['updatedAt'] = date('Y-m-d h:i:s');

                $resp = $this->question_model->copyQuestion($datavalue);
                
            }
            echo json_encode(array('questionDetails' => $questionDetailsRes, 'questionId' => $resp));
        }
    }
	
	function videoResume(){
		$userId = $this->input->get('user');
		
		$userData       = $this->session->userdata['logged_in'];
		$result 		= $this->interview_model->listAuditions($userId);
		$totalCount 	= $this->interview_model->CountAuditions();
        //echo $totalCount;exit;
        //print_r($result);exit;
		$data['contentAuditionSet']     = $result;
		$data['totalAuditions'] 		= $totalCount;
        $data['main_content']           = 'fj_index';
		
		$this->load->view('fj-mainpage', $data);
	}
	
	function getAuditionVideo(){
		$id 			= $this->input->post('id');
		$result 		= $this->interview_model->getAuditions($id);
		
		$content = "";
		foreach($result as $res){
			if($res->auditionFiles != ""){
				$auditionVideo = explode(",",$res->auditionFiles);
				foreach($auditionVideo as $video){
					$content .= "<video src='https://s3.amazonaws.com/fjauditions/".$video."'>";
				}
			}
		}
		echo $content;die;
	}

    /**
     * Description : Use to add interview set by corporate or superadmin
     * Author : Synergy
     * @param array $_POST
     * @return render data to view
     */
    function addInterviewRecruiter() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }

        $token      = $this->config->item('accessToken');
        $userData   = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
            $createdById = $userData->createdBy;
        }

        $practiceInterviewType  = "";
        $type_1                 = "";
        $type_2                 = "";        

        $info['token']          = $token;
        $info['fjQuestion']     = getFjQuestions();
        $allIndustryList        = $this->industry_model->getIndustryList(1);
        $allFunctionList        = $this->industry_model->getIndustryList(2);        
        $info['fjIndustry']     = $allIndustryList;
        $info['fjFunction']     = $allFunctionList;
        $noOfQuestions = 0;

        if (isset($_POST) && $_POST != NULL) {           
            $environment        = $this->input->post('environment', true);
            $input_file_0       = $this->input->post('input_file_1', true);
            $input_file_1_0     = $this->input->post('input_file_1_0', true);            
            $questions          = $this->input->post('question', true);            
            $data['type']       = $this->input->post('type', true);            
            $tempdata['type0']  = $this->input->post('type', true);
    
            $data['name']       = $this->input->post('name', true);
            $data['createdAt']  = date('Y-m-d h:i:s');
            $data['updatedAt']  = date('Y-m-d h:i:s');
            $data['createdBy']  = $userId;
            $data['updatedBy']  = $userId;
                
            try {
                $interviewSetExist = $this->interview_model->getAlreadyCreatedInterviewSet($userId, $createdById, $data['name']);
                if(count($interviewSetExist) < 1) {
                    $resInterviewId = $this->interview_model->addInterview($data);                    
                    if ($resInterviewId) {                        
                        if (count($questions) > 0) {
                            $f = 1;
                            foreach ($questions as $item) {
                                if( is_numeric($item['title']) ){
                                    $noOfQuestions = $noOfQuestions + 1;
                                    $input_file_value   = $this->input->post('input_file_'.$f, true);
                                    $questionDetailRes  = $this->question_model->getQuestion($item['title']);
                                    $questionId         = $questionDetailRes['id'];
                                    $questionFile       = $questionDetailRes['file'];
                                    $questionDuration   = $questionDetailRes['duration'];

                                    if( $data['type']!='3' && $input_file_value != $questionFile && isset($_FILES['file_' . $f]) ){
                                        if (isset($_FILES['file_' . $f]['name']) && !empty($_FILES['file_' . $f])) {
                                            $item['file'] = $this->fileUpload($_FILES['file_' . $f], $data['type']);
                                        }
                                        $item['title']      = $questionDetailRes['title'];
                                        $item['duration']   = $item['duration'];
                                        $item['createdAt']  = date('Y-m-d h:i:s');
                                        $item['updatedAt']  = date('Y-m-d h:i:s');
                                        $item['createdBy']  = $userId;
                                        $item['type']       = $this->input->post('type', true);
                                        $questionId         = $this->interview_model->addQuestion($item, $resInterviewId);
                                    }
                                    elseif( $item['duration'] != $questionDuration ){                                           
                                        if( $input_file_value != $questionFile ){
                                            if (isset($_FILES['file_' . $f]['name']) && !empty($_FILES['file_' . $f]['name'])) {
                                                $item['file'] = $this->fileUpload($_FILES['file_' . $f], $data['type']);
                                            }
                                        } else {
                                            $item['file'] = $questionFile;
                                        }                                            
                                        $item['title']      = $questionDetailRes['title'];
                                        $item['duration']   = $item['duration'];
                                        $item['createdAt']  = date('Y-m-d h:i:s');
                                        $item['updatedAt']  = date('Y-m-d h:i:s');
                                        $item['createdBy']  = $userId;
                                        $item['type']       = $this->input->post('type', true);
                                        $questionId         = $this->interview_model->addQuestion($item, $resInterviewId);
                                    } 
                                    else {
                                        $questionId         = $questionId;
                                    }                                        
                                } 
                                else {
                                    $noOfQuestions = $noOfQuestions + 1; 
                                    if (isset($_FILES['file_' . $f]['name']) && !empty($_FILES['file_' . $f]['name'])) {
                                        $item['file']   = $this->fileUpload($_FILES['file_' . $f], $data['type']);
                                    }
                                    $item['createdAt']  = date('Y-m-d h:i:s');
                                    $item['updatedAt']  = date('Y-m-d h:i:s');
                                    $item['createdBy']  = $userId;
                                    $item['type']       = $this->input->post('type', true);
                                    $questionId         = $this->interview_model->addQuestion($item, $resInterviewId);
                                }

                                $interviewQuestion['interviewId']   = $resInterviewId;
                                $interviewQuestion['questionId']    = $questionId;
                                $interviewQuestion['createdAt']     = date('Y-m-d h:i:s');
                                $interviewQuestion['updatedAt']     = date('Y-m-d h:i:s');
                                $interviewQuestion['status']     = '1';
                                $this->interview_model->addInterviewQuestion($interviewQuestion);
                                $f++;
                            }
                        }
                        $returnData = array('interviewSetId' => $resInterviewId, 'noOfQuestions' => $noOfQuestions, 'errorMessage' => '');
                        echo json_encode($returnData);exit;
                    }
                } else {
                    $returnData = array('interviewSetId' => '', 'noOfQuestions' => '', 'errorMessage' => 'There is already a set with same name. Please use another one.');
                    echo json_encode($returnData);exit;
                }
            } catch (Exception $e) {
                $returnData = array('interviewSetId' => '', 'noOfQuestions' => $noOfQuestions, 'errorMessage' => 'Something Went Wrong! Please try after some time.');
                echo json_encode($returnData);exit;
            }
        } else {
            $returnData = array('interviewSetId' => '', 'noOfQuestions' => $noOfQuestions, 'errorMessage' => 'Unauthorised access');
            echo json_encode($returnData);exit;
        }
    }

    /**
     * Use to update a specific interveiw set
     * Author : Synergy
     * @param int $id, array $_POST
     * @return string the success message | error message.
     */
    function getRecruiterInterviewSet($id = null) {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }

        $id = $this->input->post('interviewSetId', true);
        
        $token = $this->config->item('accessToken');
        $userData = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            $userId = 1;
        }

        $interview = $this->interview_model->getInterview($id);
        //print_r($interview);exit;
        $questions = getQuestionsByInterview($id);
        $fjQuestion = getFjQuestions($interview['type']);
        $fjVideoQuestion = getFjQuestions(1);
        $fjTextQuestion = getFjQuestions(3);

        $returnData = array('interview' => $interview, 'questions' => $questions, 'fjQuestion' => $fjQuestion, 'fjVideoQuestion' => $fjVideoQuestion, 'fjTextQuestion' => $fjTextQuestion, 'intereviewSetEditId' => $id);
        echo json_encode($returnData);exit;
    }

    function editInterviewSetRecruiter() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }

        $token = $this->config->item('accessToken');
        $userData = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            $userId = 1;
        }

        $totalQuestions = 0;
        if (isset($_POST) && $_POST != NULL) {
            $id = $this->input->post('intereviewSetEditId', true);
            $interview = $this->interview_model->getInterview($id);

            $environment = $this->input->post('environment', true);
            $input_file_0 = $this->input->post('input_file_0', true);
            $input_file_1_0 = $this->input->post('input_file_1_0', true);
            
            $questions = $this->input->post('question', true);
            $data['type'] = $this->input->post('intereviewSetEditType', true);
            
            $tempdata['type0'] = $this->input->post('intereviewSetEditType', true);
            
            $data['environment'] = $this->input->post('environment', true);
            $data['industryId'] = 0;
            $data['functionId'] = 0;
            $data['name'] = $this->input->post('name', true);
            $data['createdAt'] = date('Y-m-d h:i:s');
            $data['updatedAt'] = date('Y-m-d h:i:s');
            $data['createdBy'] = $userId;
            try {
                $resInterviewId = $this->interview_model->updateInterview($data, $id);
                if ($resInterviewId) {
                   
                    $interviewQuestions = $this->interview_model->getQuestionsByInterview($id);
                    
                    
                    //Delete Questions Start
                    $this->interview_model->deleteQuestionsByInterviewSet($id);
                    //Delete Questions End
                    
                    if (count($questions) > 0) {
                        $f = 0;
                        
                        foreach ($questions as $item) {
                            $totalQuestions = $totalQuestions + 1;
                            if( is_numeric($item['title']) ){
                                //echo $input_file_0;
                                $input_file_value = $this->input->post('input_file_'.$f, true);
                                
                                $questionDetailRes = $this->question_model->getQuestion($item['title']);
                                
                                $questionId = $questionDetailRes['id'];
                                $questionFile = $questionDetailRes['file'];
                                $questionDuration = $questionDetailRes['duration'];
                                
                                if( $input_file_value != $questionFile && isset($_FILES['file_' . $f]) ){
                                    
                                    if (isset($_FILES['file_' . $f]['name']) && !empty($_FILES['file_' . $f]['name'])) {
                                        $item['file'] = $this->fileUpload($_FILES['file_' . $f], $data['type']);
                                    } else {
                                        $item['file'] = $input_file_value;
                                    }
                                    
                                    $item['title'] = $questionDetailRes['title'];
                                    $item['duration'] = $item['duration'];
                                    $item['createdAt'] = date('Y-m-d h:i:s');
                                    $item['updatedAt'] = date('Y-m-d h:i:s');
                                    $item['createdBy'] = $userId;
                                    $item['type'] = $this->input->post('intereviewSetEditType', true);
                                    $questionId = $this->interview_model->addQuestion($item, $resInterviewId);
                                    
                                } 
                                elseif( $item['duration'] != $questionDuration ){
                                    
                                    if( $input_file_value != $questionFile ){
                                        if (isset($_FILES['file_' . $f]['name']) && !empty($_FILES['file_' . $f]['name'])) {
                                            $item['file'] = $this->fileUpload($_FILES['file_' . $f], $data['type']);
                                        } else {
                                            $item['file'] = $input_file_value;
                                        }
                                    } else {
                                        $item['file'] = $questionFile;
                                    }
                                    
                                    $item['title'] = $questionDetailRes['title'];
                                    $item['duration'] = $item['duration'];
                                    $item['createdAt'] = date('Y-m-d h:i:s');
                                    $item['updatedAt'] = date('Y-m-d h:i:s');
                                    $item['createdBy'] = $userId;
                                    $item['type'] = $this->input->post('intereviewSetEditType', true);
                                    $questionId = $this->interview_model->addQuestion($item, $resInterviewId);
                                } else {
                                    $questionId = $questionId;
                                }     
                                
                            } else {
                                if (isset($_FILES['file_' . $f]['name']) && !empty($_FILES['file_' . $f]['name'])) {
                                    $item['file'] = $this->fileUpload($_FILES['file_' . $f], $data['type']);
                                }
                                $item['createdAt'] = date('Y-m-d h:i:s');
                                $item['updatedAt'] = date('Y-m-d h:i:s');
                                $item['createdBy'] = $userId;
                                $item['type'] = $this->input->post('intereviewSetEditType', true);
                                $questionId = $this->interview_model->addQuestion($item, $resInterviewId);
                            }
                    
                            
                            $interviewQuestion['interviewId'] = $resInterviewId;
                            //$interviewQuestion['practiceInterviewType'] = "";
                            $interviewQuestion['questionId'] = $questionId;
                            $interviewQuestion['createdAt'] = date('Y-m-d h:i:s');
                            $interviewQuestion['updatedAt'] = date('Y-m-d h:i:s');
                            $this->interview_model->addInterviewQuestion($interviewQuestion);
                            $f++;
                        }
                    }

                    if(count($questions) > 0 ) {
                        $returnData = array('interviewSetId' => $id, 'noOfQuestions' => $totalQuestions, 'errorMessage' => 'Your interview set is updated successfully.');
                    } else {
                        $returnData = array('interviewSetId' => $id, 'noOfQuestions' => $totalQuestions, 'errorMessage' => 'Your interview set is updated successfully. There is no question in interview set.');
                    }
                    echo json_encode($returnData);exit;
                }
            } catch (Exception $e) {
                $returnData = array('interviewSetId' => $id, 'noOfQuestions' => $totalQuestions, 'errorMessage' => 'Something went wrong.');
                echo json_encode($returnData);exit;
            }
        } else {
            $returnData = array('interviewSetId' => $id, 'noOfQuestions' => $totalQuestions, 'errorMessage' => '');
            echo json_encode($returnData);exit;
        }
    }
}
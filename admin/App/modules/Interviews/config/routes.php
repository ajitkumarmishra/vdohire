<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['interviews/add'] = "Interviews/addInterview";
$route['interviews/edit/(:any)'] = "Interviews/editInterview/$1";
$route['getQuestionRow'] = "Interviews/getQuestionRow";
$route['getQuestionFirstRow'] = "Interviews/getQuestionFirstRow";
$route['getQuestionSecondRow'] = "Interviews/getQuestionSecondRow";
$route['interviews/view/(:any)'] = "Interviews/getInterview/$1";
$route['interviews/delete'] = "Interviews/deleteInterview";
$route['interviews/addToInterview'] = "Interviews/addQuestionToInterviewSet";
$route['getInterviewSetByType'] = "Interviews/getInterviewSetByType";
$route['checkInterview'] = "Interviews/checkInterview";
$route['interviews/addQuestion'] = "Interviews/addQuestion";
$route['getMyQuestionRow'] = "Interviews/getMyQuestionRow";
$route['interviews/editQuestion/(:any)'] = "Interviews/editQuestion/$1";
$route['getIndustryFunction'] = "Interviews/getIndustryFunction";
$route['getQuestionOnTypeFunction'] = "Interviews/getQuestionOnTypeFunction";
$route['getQuestionOnTypeFirstFunction'] = "Interviews/getQuestionOnTypeFirstFunction";
$route['getQuestionOnTypeSecondFunction'] = "Interviews/getQuestionOnTypeSecondFunction";

$route['getQuestionDetailsByQuestionId'] = "Interviews/getQuestionDetailsByQuestionId";


$route['interviews/list'] = "Interviews/listInterviews";
$route['interviews/list/(:any)'] = "Interviews/listInterviews/$1";

$route['interviews/fjlist'] = "Interviews/listInterviews";
$route['interviews/fjlist/(:any)'] = "Interviews/listInterviews/$1";

$route['interviews/mylist'] = "Interviews/listInterviews";
$route['interviews/mylist/(:any)'] = "Interviews/listInterviews/$1";

$route['interviews/questiondelete'] = "Interviews/deleteQuestion";
$route['interviews/fjquestioncopy'] = "Interviews/CopyfjQuestion";
$route['interviews/videoResume/(:any)'] = "Interviews/videoResume/$1";
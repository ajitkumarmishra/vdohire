<div style="margin-left: 1%;">
<?php

?><script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<select class="selectpicker" id="questionDropdown" onchange="showAuditionForQuestion()" style="    margin-top: 20px;margin-bottom: 27px;width: 263px;">
	<option value="1">Tell us something about yourself?</option>
    <option value="2">Tell us about your strengths?</option>
    <option value="3">Tell us about your weakness?</option>
	<option value="4">Tell us about any of your project/work that you are proud of?</option>
</select>

<div class="container" style="width:100%">
	<div class="row">
      <div class="table-responsive">
        <table class="table table-hover">
          <thead>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Email</th>
              <th>Audition Link</th>
              
            </tr>
          </thead>
          <tbody id="myTable">
			<?php
			$i = 1;
			foreach($contentAuditionSet as $audition){ ?>
				<tr>
				  <td><?php echo $i; ?></td>
				  <td><?php echo $audition->fullname; ?></td>
				  <td><?php echo $audition->email; ?></td>
				  <td><a href="javascript://"  data-toggle="modal" data-target="#messageModal" onclick="showAudition('<?php echo trim($audition->id); ?>')" >Click to View</a></td>
				  
				  
				</tr>
			<?php $i++; }
			
			?>
            
          </tbody>
        </table>   
      </div>
      <div class="col-md-12 text-center">
      <ul class="pagination pagination-lg pager" id="myPager">
		<?php 
		$counter = 1;
		for($j = 0; $j<$totalAuditions/100; $j ++){ ?>
			<li><a href="javascript://" onclick="showNextFiles(<?php echo $j; ?>)" class="page_link"><?php echo $counter; ?></a></li>
		<?php $counter ++ ; } ?>
		<input type="hidden" name="pageNumber" id="pageNumber" value="0">
	  </ul>
      </div>
	</div>
<div class="modal fade" id="messageModal">
    <div class="modal-dialog">
        <div class="modal-content">
		<form id="messageReplyBox">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <div class="modal-title" style="float:left;"><b>Duration:</b></div><div id="Auditionduration" class="pull-left"></div>
				<div style="clear:both"></div>
            </div>
            <div class="modal-body" style="    padding-top: 0px;">
			
                
				<center><div id="auditionVideoPlayer" style="width: 500px;"></div></center>
            </div>
            <div class="modal-footer">
            </div>      
			</form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="imageModal">
    <div class="modal-dialog" style="width:80%;">
        <div class="modal-content">
		<form id="messageReplyBox">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <div class="modal-title" style="float:left;"><b>Duration:</b></div><div id="Auditionduration" class="pull-left"></div>
				<div style="clear:both"></div>
            </div>
            <div class="modal-body" id="auditionImages" style="    padding-top: 0px;">
			
                
				
            </div>
            <div class="modal-footer">
            </div>      
			</form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div id="iconLoader" style="    position: absolute;
    top: 30%;
    left: 40%;display:none;"><img src="https://firstjob.co.in/images/loading.gif"></div>
</div>
</div>
<script>
function showAudition(id){
	//$("#iconLoader").css("display","block");
	$.ajax({
          url : "<?php echo base_url() ?>interviews/getAuditionVideo",
          data: 'id='+id,
          type: "POST",
          success: function(response){
				$("#auditionVideoPlayer").html(response);
          }
    });
	
	//$("#iconLoader").css("display","none");
}


function showTextfile(textFile){
	$("#iconLoader").css("display","block");
	$.ajax({
          url : "<?php echo base_url() ?>audition/getAuditionImages",
          data: 'file='+textFile,
          type: "POST",
          success: function(response){
                $("#auditionImages").html(response);
				$("#iconLoader").css("display","none");
            }
    });
}

function submitHandrating(id,userId){
	$("#iconLoader").css("display","block");
	var HumanRatingForVideo = $("#HumanRatingForVideo").val();
	$.ajax({
          url : "<?php echo base_url() ?>audition/saveAuditionHandrating",
          data: 'id='+id+"&HumanRatingForVideo="+HumanRatingForVideo+"&userId="+userId,
          type: "POST",
          success: function(response){
                $("#auditionImages").html(response);
				$("#iconLoader").css("display","none");
            }
    });
}

function showNextFiles(page){
	$("#iconLoader").css("display","block");
	var q1 = $("#questionDropdown").val();
	$("#pageNumber").val(page);
	$.ajax({
          url : "<?php echo base_url() ?>audition/getNextAuditions",
          data: 'page='+page+'&question='+q1,
          type: "POST",
          success: function(response){
                $("#myTable").html(response);
				$("#iconLoader").css("display","none");
            }
    });
}

function showAuditionForQuestion(){
	$("#iconLoader").css("display","block");
	var q1 = $("#questionDropdown").val();
	var page = $("#pageNumber").val();
	$.ajax({
          url : "<?php echo base_url() ?>audition/getNextAuditions",
          data: 'page='+page+'&question='+q1,
          type: "POST",
          success: function(response){
                $("#myTable").html(response);
				$("#iconLoader").css("display","none");
            }
    });
}
$( document ).ready(function() {
	$('#messageModal').on('hide.bs.modal', function (e) {
		$("#auditionVideoPlayer").attr("src","");
	})
});
</script>
<style>
.percent {
    font-size: 25px;
}
.para_txtxt {
    font-size: 25px;
}
.padd_add {
    padding-bottom: 0px;
    padding-top: 0px;
}
.purple {
    background: #852c9a;
    padding-top: 15px;
    padding-bottom: 10px;
    margin-bottom: 20px;
}
.green {
    background: #28b779;
    padding-top: 15px;
    padding-bottom: 10px;
    margin-bottom: 20px;
}
.gray {
    background: #777f8a;
    padding-top: 15px;
    padding-bottom: 10px;
    margin-bottom: 20px;
}
.red {
    background: #b75028;
    height: 165px;
    margin-bottom: 20px;
    padding-bottom: 10px;
    padding-top: 10px;
}
.txt_white {
    color: #fff;
}
</style>

<?php
//echo '<pre>';
//print_r($searchCandidate['searchCandidate']);
?>

<div id="page-content-wrapper">

    <div class="container-fluid no_padding dasboard_padd">
        <div class="row">

            <div class="col-md-3">
                <div class="purple">
                    <a href="<?php echo base_url(); ?>jobs/list/" title="Manage Jobs">
                    <div class="graph">
                        <span class="chart" data-percent="25">
                            <span class="percent"><?php echo ($totalJobs?$totalJobs['totalJobs']:0); ?>
                                <p style="line-height:20px;">Total Jobs</p>
                            </span>          
                            <canvas height="110" width="110"></canvas></span>
                    </div> 
                    <div class="text_ss">
                        <div class="padd_add">                           
                            <p class="text-right para_txtxt"><?php echo ($openJobs?$openJobs['openJobs']:0); ?></p>
                            <p class="text-right">Active Jobs</p>      
                            <p class="text-right para_txtxt"><?php echo ($lastWeekJobs?$lastWeekJobs['lastWeekJobs']:0); ?></p>
                            <p class="text-right">Last Week Jobs</p>               
                            <p class="text-right para_txtxt"><?php echo ($lastMonthJobs?$lastMonthJobs['lastMonthJobs']:0); ?></p>
                            <p class="text-right">Last Month Jobs</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            
            <div class="col-md-3">
                <div class="gray">
                    <a href="<?php echo base_url(); ?>corporate/dashboard">
                    <div class="graph">
                            <?php
                            /*
                            echo "<pr>";
                            print_r($highestPendingJob); 
                            echo "</pr>";
                            exit;
                             * 
                             */
                            ?>
                        <div class="padd_add txt_white" style="width: 195px;">  
                            <p class="text-right txt_white" style="font-size: 18px;">User Job Application</p>
                            <p class="text-left txt_white" style="margin: 0px; padding-left: 10px;">Job With Highest Pending<br>Application</p>
                            <p class="text-left" style="margin:0px; padding-left: 10px;">Job With Least Application<br>Received</p>
                            <p class="text-left" style="margin:0px; padding-left: 10px;">Oldest Active Job</p> 
                        </div>
                    </div> 
                    <div class="text_ss">
                        <div class="padd_add txt_white">   
                            <p class="text-right txt_white" style="font-size: 18px;">&emsp;</p>                        
                            <p class="text-right" style="margin:0 0 0px;"><?php echo ($highestPendingJob?$highestPendingJob['TotalPendingJobs']:0); ?> Job<br>&emsp;</p>
                            <p class="text-right" style="margin:0 0 0px;"><?php echo ($longestOpenJob?$longestOpenJob['LongestJobCount']:0); ?> Job<br>&emsp;</p>
                            <p class="text-right" style="margin:0 0 0px;"><?php echo ($longestOpenJob?$longestOpenJob['LongestJobCount']:0); ?> Job</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            
            <div class="col-md-3">
                <div class="green">                    
                    <a href="<?php echo base_url(); ?>queue/list/" title="Queue Management">
                    <div class="graph">
                        <span class="chart" data-percent="50">
                            <span class="percent"><?php echo ($totalApplication?$totalApplication['totalApplication']:0); ?>
                                <p style="line-height:20px;">Applications<br>Received</p>
                            </span>          
                            <canvas height="110" width="110"></canvas>
                        </span>
                    </div> 
                    <div class="text_ss">
                        <div class="padd_add">                           
                            <p class="text-right para_txtxt"><?php echo ($pendingApplication?$pendingApplication['pendingApplication']:0); ?></p>
                            <p class="text-right">Evaluation Pending</p>            
                            <p class="text-right para_txtxt"><?php echo ($shortlistedApplication?$shortlistedApplication['shortlistedApplication']:0); ?></p>
                            <p class="text-right">Shortlisted</p>         
                            <p class="text-right para_txtxt"><?php echo ($rejectedApplication?$rejectedApplication['rejectedApplication']:0); ?></p>
                            <p class="text-right">Rejected</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
          
            <div class="col-md-3">
                <div class="red">
                    <a href="<?php echo base_url(); ?>users/list/" title="Manage Users">
                    <div class="graph">
                        <div class="padd_add txt_white" style="width: 155px;">  
                            <p class="text-right txt_white" style="font-size: 18px;">User Permission</p>
                            <p class="text-left" style="margin:0px; padding-left: 10px;">For Manage Jobs</p>
                            <p class="text-left" style="margin:0px; padding-left: 10px;">For Manage Interviews</p> 
                            <p class="text-left" style="margin:0px; padding-left: 10px;">For Manage Assessments</p>
                            <p class="text-left" style="margin:0px; padding-left: 10px;">For Queue Management</p>
                            <p class="text-left" style="margin:0px; padding-left: 10px;">For Search Candidates</p>
                        </div>
                    </div> 
                    <div class="text_ss">
                        <div class="padd_add txt_white">   
                            <p class="text-right txt_white" style="font-size: 18px;">&emsp;</p>                        
                            <p class="text-right" style="margin:0 0 0px;"><?php echo ($manageJobs?$manageJobs['manageJobs']:0); ?> User</p>
                            <p class="text-right" style="margin:0 0 0px;"><?php echo ($manageInterview?$manageInterview['manageInterview']:0); ?> User</p>     
                            <p class="text-right" style="margin:0 0 0px;"><?php echo ($manageAssessment?$manageAssessment['manageAssessment']:0); ?> User</p>
                            <p class="text-right" style="margin:0 0 0px;"><?php echo ($queueManagement?$queueManagement['queueManagement']:0); ?> User</p>
                            <p class="text-right" style="margin:0 0 0px;"><?php echo ($searchCandidate?$searchCandidate['searchCandidate']:0); ?> User</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>

        </div>
    </div>
    <?php
    $userLoggedInForId      = getUserId(); 
    $userDetNew             = getUserById($userLoggedInForId);
    $userRoleForNewMenu     = $userDetNew['role'];    
    $userPermissionNewArr   = array();
    $divShowCondition       = false;
    $userPermissionNewRes   = dashboardUserPermissions($userLoggedInForId);
    if( $userPermissionNewRes ){
        $userPermissionNewStr   = $userPermissionNewRes->permissionIds;
        $userPermissionNewArr   = explode(",", $userPermissionNewStr);
    }
    
    if($userPermissionNewArr)
    {
        if( $userRoleForNewMenu == 4 || $userRoleForNewMenu == 5 ){
            if( !in_array(4, $userPermissionNewArr) ){
                $divShowCondition = true;
            }
        }
    }
    ?>
    
    <?php if( $divShowCondition == false ) { ?>
    <div class="container-fluid whitebg">
        <div class="row">
            <div class="col-md-12"><h1>All Jobs</h1></div>
            <div class="clearfix"></div>
            <div class="col-md-12 rsp_tbl">
                <table class="table" id="sorting">
                    <thead class="tbl_head">
                        <tr>
                            <th><span>Title</span></th>
                            <th style="text-align:center" align="center"><span>Invited</span></th>
                            <th style="text-align:center" align="center"><span>Responded</span></th>
                            <th style="text-align:center" align="center"><sup>Pending</sup> L1 | <sup>Pending</sup> L2 | Shorlist</th>
                            <th><span>End Date</span></th>
                            <th><span>FJ Code</span></th>
                            <th><span>Post</span></th>
                            <th><span>Invite</span></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 0;
                        if( $content ):
                            foreach ($content as $item):
                                $invitecount        = 0;
                               
                                // ci instance
                                $ci = &get_instance();
                                
                                //job user invitation model
                                $ci->load->model('jobuserinvite_model');                                
                                // function to get count of invitation job for user
                                $jobinvitecount             = $ci->jobuserinvite_model->countForInvitedJobs($item->id);
                                $invitecount                = $jobinvitecount['invitecount'];
                                
                                //job user userjob model
                                $ci->load->model('userjob_model');
                                // function to get count of pending user's job
                                $totalPending       = $ci->userjob_model->getJobApplicationLevel($item->id, 'totalPending');
                                $pendingAtL1        = $ci->userjob_model->getJobApplicationLevel($item->id, 'pendingAtL1');
                                $pendingAtL2        = $ci->userjob_model->getJobApplicationLevel($item->id, 'pendingAtL2');
                                
                                $totalShorlisted    = $ci->userjob_model->getJobApplicationLevel($item->id, 'totalShorlisted');
                                $shorlistedAtL1     = $ci->userjob_model->getJobApplicationLevel($item->id, 'shorlistedAtL1');
                                $shorlistedAtL2     = $ci->userjob_model->getJobApplicationLevel($item->id, 'shorlistedAtL2');
                                
                                $totalRejected      = $ci->userjob_model->getJobApplicationLevel($item->id, 'totalRejected');
                                $rejectedAtL1       = $ci->userjob_model->getJobApplicationLevel($item->id, 'rejectedAtL1');
                                $rejectedAtL2       = $ci->userjob_model->getJobApplicationLevel($item->id, 'rejectedAtL2');
                                ?>
                                <tr>
                                    <td><a href="<?php echo base_url(); ?>jobs/view/<?php echo $item->id; ?>" ><?php echo $item->title; ?></a></td>
                                    <td align="center"><?php echo $invitecount; ?></td>
                                    <td align="center"><?php echo ($totalShorlisted['records']?$totalShorlisted['records']:0)+($totalRejected['records']?$totalRejected['records']:0); ?></td>
                                    <td align="center"><?php echo ($pendingAtL1['records']?$pendingAtL1['records']:0); ?> | <?php echo ($pendingAtL2['records']?$pendingAtL2['records']:0); ?> | <?php echo ($totalShorlisted['records']?$totalShorlisted['records']:0);?></p></td>
                                    <td><?php echo date('d/m/y',strtotime($item->openTill)); ?></td>
                                    <td><?php echo $item->fjCode; ?></td>
                                    <td><?php if ($item->posted == 2): ?>
                                            <a href="javascript:" class="post-job" id="<?php echo $item->id; ?>" target="<?php echo $item->posted; ?>">
                                                Post
                                            </a>
                                        <?php else: ?>
                                            Posted
                                        <?php endif; ?>
                                    </td>
                                    <td><a href="javascript:" class="invite-users" data-toggle="modal" data-target="#inviteUsersForJob" id="<?php echo $item->id; ?>">Invite</a></td>
                                    
                                </tr>
                                <?php
                                $i++;
                            endforeach;
                        else:
                            echo "<tr><td colspan=\"2\">Jobs not available.</td></tr>";
                        endif;
                        ?>

                    </tbody>
                </table>
            </div>
            <?php echo $this->pagination->create_links(); ?>
            <div class="service_content ">
                <?php if ($totalCount > $itemsPerPage): ?>
                   
                <?php endif; ?>
            </div>


        </div>
    </div>
    <?php } ?>
</div>

<!-- PopUp for invite users for a job  -->
<div id="inviteUsersForJob" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <form class="form-horizontal form_save" action="<?php echo base_url(); ?>assessments/addToAssessment" method="post" id="interviewQuestionForm" name="interviewQuestionForm" >
            <div class="modal-content">

                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Invite Users</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger hidden" id="inviteAlert">

                    </div>
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-3">Inivitation By</label>
                        <div class="col-xs-6">
                            <input type="checkbox" name="inviteFrom[]" id="inviteFrom_1" value="1" checked="" />&nbsp;Email&nbsp;&nbsp;
                            <input type="checkbox" name="inviteFrom[]" id="inviteFrom_2" value="2" />&nbsp;SMS
                        </div>
                        
                    </div>
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-3">Upload File</label>
                        <div class="col-xs-6">
                            <input type="file" name="inviteUsers" id="inviteUsers" />
                        </div>
                        <div class="col-xs-3">
                            <button type="button" class="btn btn-primary upload-file">Invite</button>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-3"></label>
                        <div class="col-xs-4 aligncenter">
                            <?php
                            $fileCsv = base_url() . 'uploads/inviteUserFiles/inviteUsers' . $userId . '.csv';
                            $fileSamleCsv = base_url() . 'uploads/inviteUserFiles/inviteSampleFile.csv';
                            if ($fileEist == 1):
                                ?>
                                <a href="<?php echo $fileCsv; ?>"> Download File</a>
                            <?php else: ?>
                                <a href="<?php echo $fileSamleCsv; ?>"> Download Sample File</a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">


                    <!--                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
                </div>
            </div>
        </form>

    </div>
</div>


<!--<script src="js/jquery-1.12.4.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery-2.0.3.min.js"></script>-->
<script src="<?php echo base_url(); ?>theme/firstJob/js/jquery.easypiechart.min.js"></script>
<!--<script src="js/script.js"></script>-->
<script>
    $(function () {
        $(function () {
            //create instance
            $('.chart').easyPieChart({
                animate: 2000,
                scaleColor: 'rgba(0, 0, 0, 0)',
                trackColor: 'rgba(255, 255, 255, 0.2)',
                barColor: '#fff'
            });

        });
    });

    $(function () {
        $(function () {
            //create instance
            $('.chart-2').easyPieChart({
                animate: 2000,
                scaleColor: 'rgba(0, 0, 0, 0)'
            });

        });
    });
</script>
<style>
    .text_ss {
        float:right!important;
        padding-right:0px;
    }
    canvas {
        position: absolute;
        left: 6%;
    }
    body{
        font-family:  RobotoCondensed !important;
        background: #f1f1f1 !important;

    }

</style>

<script>
    $(function () {
        var url = '<?php echo base_url(); ?>';
        window.siteUrl = url;
        window.token = '<?php echo $token; ?>';
    });

    $('.delete-job').on('click', function () {
        var userId = $(this).attr('id');
        bootbox.confirm('Are you sure you want to delete?', function (result) {

            if (result) {
                $.post(siteUrl + 'jobs/delete', {id: userId, accessToken: token}, function (data) {

                    //bootbox.alert(data);
                    location.reload();
                });
            }
        });

    });
</script>

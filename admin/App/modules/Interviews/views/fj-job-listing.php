<?php //echo '<pre>';print_r($content);?>
<div id="page-content-wrapper">
    <div class="container-fluid whitebg">
        <div class="row">
            <div class="col-md-3"><h1><img src="<?php echo base_url(); ?>/theme/firstJob/image/arrow_head.png" class="prev-page">All Jobs</h1></div>
            <div class="col-md-4"><button type="button" class="acu_bt pull-right"><a href="<?php echo base_url(); ?>jobs/add">Add Job</a></button></div>
            <form action="<?php echo base_url(); ?>jobs/list/1" method="post">
                <div class="col-md-5">
                    <select class="sel_opt" name ="serachColumn">
                        <option value="title" <?php if($serachColumn == 'title') echo "selected";?>>Search By Title</option>
                        <option value="city" <?php if($serachColumn == 'city') echo "selected";?>>Search By Location</option>
                        <option value="experience" <?php if($serachColumn == 'experience') echo "selected";?>>Search By Experience</option>
                    </select>
                    <?php if($searchText):?>
                        <div class="cross-search">X</div>
                    <?php endif;?>
                    <input type="text" class="search_box" name ="serachText" placeholder="" value ="<?php echo $searchText; ?>">
                    <input type="hidden" id= "accessToken" name ="accessToken" value="<?php echo $token; ?>" >
                    <button type="submit" class="srch_bt"><img src="<?php echo base_url(); ?>/theme/firstJob/image/search.png"></button>

                </div>
            </form>

            <div class="clearfix"></div>

            <div class="col-md-12 rsp_tbl">

                <table class="table sortable" id="sorting">
                    <thead class="tbl_head">
                        <tr>
                            <!--<th><input type="checkbox" class="chk_bx"></th>-->
                            <th><span>Title</span></th>
                            <th class="th-location"><span>Location</span></th>
                            <th><span>Qualifications</span></th>
                            <th><span>Experience</span></th>
                            <th><span>End Date</span></th>
                            <th><span>FJ Code</span></th>
                            <th><span>Vacancy</span></th>
                            <th><span>Post</span></th>
                            <th><span>Invite</span></th>
                            <th><span>Status</span></th>
                            <th><span>Action</span></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 0;
                        foreach ($content as $item):
                            ?>
                                <tr>
    <!-- <td><input type="checkbox"></td>-->
                                <td><?php echo $item->title; ?></td>
                                <td><?php echo $item->city; ?></td>
                                <td><?php echo $item->name; ?></td>
                                <td><?php echo $item->experience; ?> Years</td>
                                <td><?php echo date('Y-m-d',strtotime($item->openTill)); ?></td>
                                <td><?php echo $item->fjCode; ?></td>
                                <td><?php echo $item->noOfVacancies; ?></td>
                                <td><?php  if($item->posted == 2): ?>
                                    <a href="javascript:" class="post-job" id="<?php echo $item->id;?>" target="<?php echo $item->posted;?>">
                                    Post
                                    </a>
                                    
                                <?php else:?>
                                    <a href="javascript:" class="post-job" id="<?php echo $item->id;?>" target="<?php echo $item->posted;?>">
                                    Posted
                                    </a>
                                    <?php endif;?>
                                </td>
                                <td><?php echo "Invite"; ?></td>
                                <td><?php 
                                if(date('Y-m-d', strtotime($item->openTill)) >= date('Y-m-d')) : ?>
<!--                                    <a href="javascript:" class="status-job" id="<?php echo $item->id;?>" target="<?php echo $item->status;?>">
                                    Active
                                    </a>-->Active
                                <?php else:?>
<!--                                    <a href="javascript:" class="status-job" id="<?php echo $item->id;?>" target="<?php echo $item->status;?>">
                                    Inactive
                                    </a>-->Inactive
                                   <?php endif;?>
                                </td>
                                <td>
                                    <a href="<?php echo base_url(); ?>jobs/edit/<?php echo $item->id; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/edit.png" class="padd_rgt"></a>
                                    <a href="javascript:" class="delete-job" id="<?php echo $item->id; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/del.png"></a>
                                </td>
                            </tr>
                            <?php
                            $i++;
                        endforeach;
                        ?>

                    </tbody>
                </table>
            </div>
            <div class="service_content ">

                <?php if ($totalCount > $itemsPerPage): ?>
                    <ul class="service_content_last_list">
                        <?php if ($page > 1): ?>
                            <p class="pagi_img"><a href="<?php echo base_url(); ?>jobs/list/<?php echo $page - 1; ?>" ><img src="<?php echo base_url(); ?>/theme/firstJob/image/pagination_img2.png"></a></p>    
                        <?php endif; ?>
                        <input type="hidden" name="page_no" value="1" id="page_no"> 
                        <?php for ($i = 1; $i <= $count; $i++): ?>
                            <li><a href="<?php echo base_url(); ?>jobs/list/<?php echo $i; ?>"  <?php if ($i == $page): ?>style="color: orange;"<?php endif; ?>><?php echo $i; ?></a></li>
                        <?php endfor; ?>
                        <?php if ($page < $count): ?>
                            <p class="pagi_img"><a href="<?php echo base_url(); ?>jobs/list/<?php echo $page + 1; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/pagination_img1.png"></a></p>            
                        <?php endif; ?>
                    </ul>
                <?php endif; ?>
            </div>

        </div>
    </div>
</div>

<style>
    .service_content ul.service_content_last_list li{ display:inline-block; font-size:20px; width:38px; height:38px; line-height:38px; border:0px; }
    .service_content ul.service_content_last_list li a{ color:#333333; padding:0px; text-align:center; display:block; border:0px;  }
    .service_content ul.service_content_last_list li a:hover{ color:#fff; border-radius:50px; -moz-border-radius:50px; -webkit-border-radius:50px; background:#0072dc }
    .response_img img{ max-width: 100%; }
    .service_content .service_content_last_list{ text-align:center; padding:10px 0 70px; }
    ul.service_content_last_list  li.pagination-last-list{padding:0 20px 0 0px; cursor:pointer; text-indent:-9999px; margin-left:5px;  }
    .pagi_img{display: -webkit-inline-box;}
    .pagi_img a img{display: -webkit-inline-box;}
    .service_content ul.service_content_last_list li{margin:0px!important;}
    h1{font-size:32px!important;}
    .acu_bt a {
        text-decoration: none;
        color: #fff;
    }
    .sel_opt {
        font-size:15px;
        width:33% !important;
        padding-left: 9px !important;
    }
    #sorting td a {color: #303030!important; }
    #sorting td a:hover {color: #777!important; }

</style>
<!--<script src="<?php echo base_url(); ?>theme/firstJob/js/tablesort.js"></script>-->
<!--<script src="<?php echo base_url(); ?>theme/firstJob/js/jquery.tablesorter.js"></script>-->
<!--<script src="https://code.jquery.com/jquery-1.10.2.js"></script>-->
<script>
    $(function () {

        var settingTableRowColor = function (tableName) {
            $('#' + tableName + ' tbody tr').removeAttr('class');
            $('#' + tableName + ' tbody tr:odd').attr('class', 'tbl_bg');
        }
        settingTableRowColor('sorting');
        $('.sort-header').on('click', function () {
            settingTableRowColor('sorting');
        });
        $('.delete-user').on('click', function () {
            var url = '<?php echo base_url(); ?>';
            var userId = $(this).attr('id');
            var accessToken = $('#accessToken').val();
            bootbox.confirm('Are you sure you want to delete?', function (result) {
                //alert(result);
                if (result) {
                    $.post(url + 'users/delete', {id: userId, accessToken: accessToken}, function (data) {
                        //bootbox.alert(data);
                        location.reload();
                    });
                }
            });

        });
    });
    //new Tablesort(document.getElementById('sorting'));
    //$("#sorting").tablesorter();
</script>

<script>
    $(function () {
        var url = '<?php echo base_url(); ?>';
        window.siteUrl = url;
        window.token = '<?php echo $token; ?>';
    });
    
    $('.delete-job').on('click', function () {
            var userId = $(this).attr('id');
            bootbox.confirm('Are you sure you want to delete?', function (result) {
                
                if (result) {
                    $.post(siteUrl + 'jobs/delete', {id: userId, accessToken: token}, function (data) {
                        
                        //bootbox.alert(data);
                        location.reload();
                    });
                }
            });

        });
</script>


<?php
if (isset($job) && $job != NULL) {
    //echo '<pre>';
    //print_r($job);
}
//echo '<pre>'; print_r($subuser);
?>
<div id="page-content-wrapper">
    <?php if (isset($message)): ?>
        <div class="alert alert-success">
            <?php echo $message; ?>
        </div>
    <?php endif; ?>
    <?php if (isset($error)): ?>
        <div class="alert alert-danger">
            <?php foreach ($error as $item): ?>
                <?php echo $item; ?>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
    <div class="container-fluid whitebg">
        <div class="row">

            <div class="col-md-12 padd_tp_bt_20">
                <h4><img src="<?php echo base_url(); ?>/theme/firstJob/image/arrow_head.png" class="prev-page"> Edit Job</h4>
                <form class="form-horizontal form_save" action="" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="inputEmail" class="control-label col-xs-2">Job ID</label>
                        <div class="col-xs-10">
                            <input type="text" class="form-control" placeholder="Job ID" value="<?php
                            if (isset($job['jobId'])): echo $job['jobId'];
                            endif;
                            ?>" name="jobId">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-2 required-field">Title Of Job</label>
                        <div class="col-xs-10">
                            <input type="text" class="form-control" placeholder="Title Of Job" value="<?php
                            if (isset($job['title'])): echo $job['title'];
                            endif;
                            ?>" name="title">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-2">Location</label>
                        <div class="col-xs-10">
                            <select data-placeholder="Location" style="width: 350px; display: none;" multiple="" class="chosen-select" tabindex="-1" name="cityState[]">
                                <option value=""></option>
                                <?php foreach ($city as $item) : ?>
                                    <option value="<?php echo $item['id']; ?>" <?php
                                    if (isset($job['cityState']) && in_array($item['id'], $job['cityState'])): echo "selected";
                                    endif;
                                    ?>><?php echo $item['city'] . ', ' . $item['state']; ?></option>
                                        <?php endforeach; ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-2">Number Of Vacancies</label>
                        <div class="col-xs-10">
                            <input type="text" class="form-control" placeholder="Number Of Vacancies" value="<?php
                            if (isset($job['vacancy'])): echo $job['vacancy'];
                            endif;
                            ?>" name="vacancy">
                        </div>
                    </div>

                    <div class="clearfix">

                        <div class="form-group">
                            <label for="inputPassword" class="control-label col-xs-2">Age</label>
                            <div class="col-xs-5">
                                <input type="text" class="form-control" placeholder="From" value="<?php
                                if (isset($job['ageFrom']) && $job['ageFrom'] != ''): echo $job['ageFrom'];
                                endif;
                                ?>" name="ageFrom">
                            </div>


                            <div class="col-xs-5">
                                <input type="text" class="form-control" placeholder="To" value="<?php
                                if (isset($job['ageTo'])): echo $job['ageTo'];
                                endif;
                                ?>" name="ageTo">
                            </div>
                        </div>

                        <div class="clearfix">


                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-2">Salary</label>
                                <div class="col-xs-5">
                                    <select class="selectpicker" name="salaryFrom">
                                        <option value="">From</option>
                                        <?php for ($i = 0; $i <= 1500000; $i+=100000): ?>
                                            <option value="<?php echo $i; ?>" <?php
                                            if (isset($job['salaryFrom']) && $job['salaryFrom'] != NULL && $i == $job['salaryFrom']): echo "selected";
                                            endif;
                                            ?>><?php echo $i; ?></option>
                                                <?php endfor; ?>
                                    </select>
                                </div>


                                <div class="col-xs-5">
                                    <select class="selectpicker" name="salaryTo">
                                        <option value="">To</option>
                                        <?php for ($i = 100000; $i <= 2000000; $i+=100000): ?>
                                            <option value="<?php echo $i; ?>" <?php
                                            if ($i == $job['salaryTo']): echo "selected";
                                            endif;
                                            ?>><?php echo $i; ?></option>
                                                <?php endfor; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="clearfix">

                                <div class="form-group">
                                    <label for="inputEmail" class="control-label col-xs-2">Availability Notice periods (In Days)</label>
                                    <div class="col-xs-10">
                                        <input type="text" class="form-control" placeholder="Availability Notice periods (In Days)" value="<?php
                                        if (isset($job['noticePeriod'])): echo $job['noticePeriod'];
                                        endif;
                                        ?>" name="noticePeriod">
                                    </div>
                                </div>



                                <div class="form-group">
                                    <label for="inputPassword" class="control-label col-xs-2">Qualification</label>
                                    <div class="col-xs-10">
                                        <select data-placeholder="Qualification" style="width: 350px; display: none;" multiple="" class="chosen-select" tabindex="-1" name="qualification[]">
                                            <option value=""></option>
                                            <?php foreach ($course as $item) : ?>
                                                <option value="<?php echo $item['id']; ?>" <?php
                                                if (isset($job['qualification']) && in_array($item['id'], $job['qualification'])): echo "selected";
                                                endif;
                                                ?>><?php echo $item['name']; ?></option>
                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>

<!--                                <div class="form-group">
                                    <label for="inputPassword" class="control-label col-xs-2">Experience</label>
                                    <div class="col-xs-10">
                                        <select class="selectpicker" name="experience"><option value="">Experience</option>
                                            <?php for ($i = 0; $i <= 20; $i+=1): ?>
                                                <option value="<?php echo $i; ?>" <?php
                                                if (isset($job['experience']) && $job['experience'] != NULL && $i == $job['experience']): echo "selected";
                                                endif;
                                                ?>><?php echo $i; ?> Years</option>
<?php endfor; ?>
                                        </select>
                                    </div>
                                </div>-->
                                
                                <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-2">Experience</label>
                                <div class="col-xs-5">
                                    <select class="selectpicker" name="expFrom"><option value="">From</option>
<?php for ($i = 0; $i <= 20; $i+=1): ?>
                                                <option value="<?php echo $i; ?>" <?php if (isset($job['expFrom']) && $job['expFrom']!=NULL && $i == $job['expFrom']): echo "selected";
    endif; ?>><?php echo $i; ?> Years</option>
<?php endfor; ?>
                                        </select>
                                </div>


                                <div class="col-xs-5">
                                    <select class="selectpicker" name="expTo"><option value="">To</option>
<?php for ($i = 0; $i <= 20; $i+=1): ?>
                                                <option value="<?php echo $i; ?>" <?php if (isset($job['expTo']) && $job['expTo']!=NULL && $i == $job['expTo']): echo "selected";
    endif; ?>><?php echo $i; ?> Years</option>
<?php endfor; ?>
                                        </select>
                                </div>
                            </div>

                                <div class="form-group">
                                    <label for="inputPassword" class="control-label col-xs-2 required-field">Open Till</label>
                                    <div class="col-xs-10">
<!--                                        <select class="selectpicker"><option>Open Till</option>
                                            <option>Ketchup</option>
                                            <option>Relish</option>
                                        </select>-->
                                        <input type="text" class="form-control" placeholder="Open Till" id="datepicker-13" value="<?php
                                        if (isset($job['openTill'])): echo date('m/d/Y', strtotime($job['openTill']));
                                        endif;
                                        ?>" name="openTill">
                                    </div>
                                </div>
                                <div class="clearfix">
                                    <div class="form-group">
                                        <label for="inputPassword" class="control-label col-xs-2 description required-field">Job Description</label>
                                        <div class="col-xs-5">
                                            <textarea rows="5" style="width:100%" placeholder="Job Description" name="description"><?php
                                                if (isset($job['description'])): echo $job['description'];
                                                endif;
                                                ?></textarea>
                                        </div>
                                        <div class="col-xs-5">
                                            Upload Video: <input type="file" name="jd" id="jd" value="<?php print_r($files); ?>"/>
                                            <div id="fileHidden">
                                                <?php if (isset($files['jd']) && $files['jd'] != NULL): ?>
                                                    <input type="hidden" name="jd" id="jdHidden" value="<?php print_r($files); ?>"/>
                                                    <?php echo $files['jd']['name']; ?>
                                            <?php endif; ?>
                                            </div>
<?php if (isset($job['jd']) && $job['jd'] != 0): ?>
                                                <br/>
                                                <div id="jdvideo"></div>
<?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="clearfix">
                                        <div class="form-group">
                                            <label for="inputPassword" class="control-label col-xs-2">Attach Assessment</label>
                                            <div class="col-xs-10">
                                                <select data-placeholder="Select Assessment" style="width: 350px; display: none;"  class="chosen-select" tabindex="-1" name="assessment">
                                                    <option value=""></option>
                                                    <?php foreach ($assessment as $item) : ?>
                                                        <option value="<?php echo $item['id']; ?>" <?php
                                                        if ($item['id'] == $job['assessment']): echo "selected";
                                                        endif;
                                                        ?>><?php echo $item['name']; ?></option>
<?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputPassword" class="control-label col-xs-2">Attach Inverview</label>
                                            <!--                                            <div class="col-xs-10">
                                                                                            <select class="selectpicker"><option>Attache Inverview</option>
                                                                                                <option>Ketchup</option>
                                                                                                <option>Relish</option>
                                                                                            </select>
                                                                                        </div>-->
                                            <div class="col-xs-10">
                                                <select data-placeholder="Select Interview" style="width: 350px; display: none;"  class="chosen-select" tabindex="-1" name="interview">
                                                    <option value=""></option>
                                                    <?php foreach ($interview as $item) : ?>
                                                        <option value="<?php echo $item['id']; ?>" <?php
                                                        if ($item['id'] == $job['interview']): echo "selected";
                                                        endif;
                                                        ?>><?php echo $item['name']; ?></option>
<?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputPassword" class="control-label col-xs-2 required-field">Add Level 1 Panel</label>
                                            <div class="col-xs-10">
                                                <select data-placeholder="Level 1 Panel" style="width: 350px; display: none;" multiple="" class="chosen-select" tabindex="-1" name="level1[]">
                                                    <option value=""></option>
                                                    <?php foreach ($subuser as $item) : ?>
                                                        <option value="<?php echo $item['id']; ?>" <?php
                                                        if (isset($job['level1']) && in_array($item['id'], $job['level1'])): echo "selected";
                                                        endif;
                                                        ?>><?php echo $item['fullname']; ?></option>
<?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputPassword" class="control-label col-xs-2">Add Level 2 Panel</label>
                                            <div class="col-xs-10">
                                                <select data-placeholder="Level 2 Panel" style="width: 350px; display: none;" multiple="" class="chosen-select" tabindex="-1" name="level2[]">
                                                    <option value=""></option>
                                                    <?php foreach ($subuser as $item) : ?>
                                                        <option value="<?php echo $item['id']; ?>" <?php
                                                        if (isset($job['level2']) && in_array($item['id'], $job['level2'])): echo "selected";
                                                        endif;
                                                        ?>><?php echo $item['fullname']; ?></option>
<?php endforeach; ?>
                                                </select>  
                                            </div>
                                        </div>
                                        <div class="col-xs-offset-2 col-xs-2 no_padding">
                                            <input type="hidden" name="accessToken" value="<?php echo $token; ?>"/>
                                            <button type="submit" class="Save_frm">SAVE</button>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /#page-content-wrapper -->
                    </div></form>


            </div></div></div></div>
<script src="<?php echo base_url(); ?>theme/firstJob/js/chosen.jquery.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>theme/firstJob/js/prism.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
    var config = {
        '.chosen-select': {},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }

    $(function () {
        $("#datepicker-19").datepicker();
    });

</script>
<script type="text/javascript">
    $(function(){
   var videoUrl = "uploads/jd/<?php echo $job['jd'] ?>";
    jwVideo(videoUrl, 'jdvideo');
    });
</script>
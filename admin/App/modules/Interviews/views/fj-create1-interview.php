<?php
if (isset($interview) && $interview != NULL) {
    //echo '<pre>';print_r($interview);  
}
?>
<div id="page-content-wrapper">
    <?php if (isset($message)): ?>
        <div class="alert alert-success">
            <?php echo $message; ?>
        </div>
    <?php endif; ?>
    <?php if (isset($error)): ?>
        <div class="alert alert-danger">
            <?php foreach ($error as $item): ?>
                <?php echo $item; ?>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
    <div class="container-fluid whitebg">
        <div class="row">

            <div class="col-md-12 padd_tp_bt_20">
                <h4><img src="image/arrow_head.png"> Add Interview Set</h4>
                <form class="form-horizontal form_save" method="post" action="" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="inputEmail" class="control-label col-xs-2">Set Name</label>
                        <div class="col-xs-10">
                            <input type="text" class="form-control" value="<?php
                            if (isset($interview['name'])): echo $interview['name'];
                            endif;
                            ?>" name="name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-2">Type</label>
                        <div class="col-xs-10">
                            <select class="selectpicker" name="type"><option value="">Select</option>
                                <option value="1" <?php if (isset($interview['type']) && $interview['type'] == 1) echo "selected"; ?>>Audio</option>
                                <option value="2" <?php if (isset($interview['type']) && $interview['type'] == 2) echo "selected"; ?>>Video</option>
                                <option value="3" <?php if (isset($interview['type']) && $interview['type'] == 3) echo "selected"; ?>>Text</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-2">Question</label>
                        <div class="col-xs-4">
                            <label for="inputPassword" class="control-label frm_in_padd">Title</label>
                            <input type="text" class="form-control" placeholder="Title" name="question[0][title]">
                        </div>

                        <div class="col-xs-3">
                            <label for="inputPassword" class="control-label frm_in_padd">File</label>
                            <input type="file" name="file_1" />
                        </div>

                        <div class="col-xs-3">
                            <label for="inputPassword" class="control-label frm_in_padd col-xs-6">Duration</label>
                            <input type="text" class="form-control" placeholder="Duration" name="question[0][duration]">
                        </div>

                    </div>

                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-2">Question</label>
                        <div class="col-xs-4">
                            <label for="inputPassword" class="control-label frm_in_padd">Title</label>
                            <input type="text" class="form-control" placeholder="Title" name="question[1][title]">
                        </div>

                        <div class="col-xs-3">
                            <label for="inputPassword" class="control-label frm_in_padd">File</label>
                            <input type="file" name="file_2" />
                        </div>

                        <div class="col-xs-3">
                            <label for="inputPassword" class="control-label frm_in_padd col-xs-6">Duration</label>
                            <input type="text" class="form-control" placeholder="Duration" name="question[1][duration]">
                        </div>

                    </div>

                    <div class="clearfix">
                        <div class="col-xs-offset-2 col-xs-2 no_padding">
                            <input type="hidden" name="accessToken" value="<?php echo $token; ?>"/>
                            <button type="submit" class="Save_frm">SAVE</button>
                        </div>


                    </div></form>
            </div>
        </div>
    </div>
    <!-- /#page-content-wrapper -->
</div>
<script src="<?php echo base_url(); ?>theme/firstJob/js/chosen.jquery.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>theme/firstJob/js/prism.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
    var config = {
        '.chosen-select': {},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }

    $(function () {
        $("#datepicker-19").datepicker();
    });

</script>

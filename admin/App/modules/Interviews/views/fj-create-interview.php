<?php
if (isset($interview) && $interview != NULL) {
    //echo '<pre>';print_r($interview);  
}
//echo "<pre>";print_r($fjQuestion); die;
$userId             = getUserId();
$userDetForPage     = getUserById($userId);
$userRoleForPage    = $userDetForPage['role'];
?>
<div id="page-content-wrapper">
    <?php if (isset($message)): ?>
        <div class="alert alert-success">
            <?php echo $message; ?>
        </div>
    <?php endif; ?>
    <?php if (isset($error)): ?>
        <div class="alert alert-danger">
            <?php foreach ($error as $item): ?>
                <?php echo $item; ?>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
    
    <div class="container-fluid whitebg">
        <div class="row">
            <div class="col-md-12 padd_tp_bt_20">
                <h4><img src="<?php echo base_url(); ?>/theme/firstJob/image/arrow_head.png" class="prev-page"> Add Interview Set</h4>
                <form class="form-horizontal form_save" name="interviewsetfrm" method="post" action="" enctype="multipart/form-data" id="interviewsetfrm">
                    <?php if ($userRoleForPage == 1) { ?>
                        <div class="form-group">
                            <label for="inputEmail" class="control-label col-xs-2 required-field">Question For</label>
                            <div class="col-xs-10">
                                <input type="radio" class="question-for-radio" name="environment" id="environment_1" value="1" <?= (($interview['environment'] == 1 || $interview['environment'] == "") ? "checked" : "") ?> />&nbsp;&nbsp;Live&nbsp;&nbsp;
                                <input type="radio" class="question-for-radio" name="environment" id="environment_2" value="2" <?= ($interview['environment'] == 2 ? "checked" : "") ?> />&nbsp;&nbsp;Practice
                            </div>
                        </div>
                    <?php } else { ?>
                        <input type="hidden" class="question-for-radio" name="environment" id="environment_1" value="1"/>
                    <?php } ?>
                    <div class="form-group" id="industryDivId" style="display: <?php if (isset($interview['environment']) && $interview['environment'] == 2) {
                        echo "block";
                    } else {
                        echo "none";
                    } ?>">
                        
                        <label for="inputEmail" class="control-label col-xs-2 required-field">Question Industry</label>
                        <div class="col-xs-10">
                            <div class="ques-title3" style="float:left; width:200px;">
                                <select data-placeholder="Industry List" style="width: 350px; display: none;" class="chosen-select3 sel_sty_padd" tabindex="2" name="industryId" id="industryId">
                                    <option value="">---Select Industry---</option>
                                        <?php foreach ($fjIndustry as $item): ?>
                                        <option value="<?php echo $item->industryId; ?>" <?php if (isset($interview['industryId']) && $interview['industryId'] == $item->industryId) echo "selected"; ?>><?php echo $item->industryName; ?></option>
                                        <?php endforeach; ?>
                                </select>
                            </div>
                            <div style="float:left; width:50px; padding:8px 0px 0px 23px; font-weight: 700">OR</div>
                            <label for="inputEmail" class="control-label col-xs-2 required-field" style="padding-left:10px;">Question Function</label>
                            <div class="ques-title4" style="float:left; width:200px; padding-left: 20px;" id="industryFunctionDiv">
                                <select data-placeholder="Function List" style="width: 350px; display: none;" class="chosen-select4 sel_sty_padd" tabindex="3" name="functionId" id="functionId">
                                    <option value="">---Select Function---</option>
                                        <?php foreach ($fjFunction as $item): ?>
                                        <option value="<?php echo $item->industryId; ?>" <?php if (isset($interview['functionId']) && $interview['functionId'] == $item->industryId) echo "selected"; ?>><?php echo $item->industryName; ?></option>
                                        <?php endforeach; ?>
                                </select>
                                <input type="text" data-validation="industryFunction" style="height: 0px; width: 0px; visibility: hidden; " />
                            </div>
                        </div>
                    </div>
                        
                    <div class="form-group">
                        <label for="inputEmail" class="control-label col-xs-2 required-field">Set Name</label>
                        <div class="col-xs-10">
                            <input type="text" class="form-control" value="<?php
                                if (isset($interview['name'])): echo $interview['name'];
                                endif;
                                ?>" name="name" id="setname_value" data-validation="required" maxlength="100">
                        </div>
                    </div>

                    <!-- FIRST SECTION QUESTIONS -->

                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-2 required-field">Type</label>
                        <div class="col-xs-10" id="blockInterviewSection1">
                            <?php if (isset($interview['environment'])) { ?> 
                                <?php if ($interview['environment'] == 1) { ?>
                                    <select class="selectpicker" name="type" id="type"><option value="" data-validation="required">Select</option>
                                        <option value="1" <?php if (isset($interview['type']) && $interview['type'] == 1) echo "selected"; ?>>Video</option>
                                        <option value="2" <?php if (isset($interview['type']) && $interview['type'] == 2) echo "selected"; ?>>Audio</option>
                                        <option value="3" <?php if (isset($interview['type']) && $interview['type'] == 3) echo "selected"; ?>>Text</option>
                                    </select>
                                <?php } 
                                elseif ($interview['environment'] == 2) { ?>
                                    <select class="selectpicker" name="type" id="type">
                                        <option selected="" value="1" <?php if (isset($interview['type']) && $interview['type'] == 1) echo "selected"; ?>>Video</option>
                                    </select>
                                <?php } ?>
                            <?php } 
                            else { ?>
                                <select class="selectpicker" name="type" id="type" data-validation="required">
                                    <option value="">Select</option>
                                    <option value="1">Video</option>
                                    <option value="2">Audio</option>
                                    <option value="3">Text</option>
                                </select>
                            <?php } ?>
                        </div>
                    </div>

                    <div id ="questionList">
                        <div class="form-group">
                            <label for="inputPassword" class="control-label col-xs-2 required-field">Question</label>
                            <div class="col-xs-4 ques-title questionOnTypeBlock" id="questions11">
                                <label for="inputPassword" class="control-label frm_in_padd">Question Title</label>
                                <select data-placeholder="Type a question" style="width: 350px; display: none;" class="chosen-select sel_sty_padd interviewQuestion" tabindex="-1" name="question[0][title]">
                                    <option value=""></option>
                                    <?php
                                    if (isset($question[0]['title'])) :
                                        foreach ($fjQuestion as $item):
                                            echo "<option value='" . $item['id'] . "' " . ((isset($question[0]['title']) && $question[0]['title'] == $item['title']) ? "selected" : "") . ">" . $item['title'] . "</option>";
                                        endforeach;
                                    endif;
                                    ?>
                                </select>
                                <input type="text" data-validation="interviewQuestion" style="height: 0px; width: 0px; visibility: hidden; " />
                            </div>
                            <?php if ($interview['type'] != 3): ?>
                                <div class="col-xs-3 file-section">
                                    <div class="col-md-12">
                                        <label for="inputPassword" class="control-label frm_in_padd" style="font-size: 12px;">Upload File(mp3, mp4)</label>
                                    </div>
                                    <div class="col-xs-8">
                                        <input type="text" class="form-control file-name interviewFileNames" name="input_file_0" id="filetextboxforimage11" readonly="readonly">
                                    </div>
                                    <div class="col-xs-4 no_padding browse-section" id="filebrowseforimage11">
                                        <button type="button" class="btn_upload">BROWSE</button>
                                        <input type="file" name="file_0" class="question-file interviewFile" />
                                        <input type="text" data-validation="interviewFile" style="height: 0px; width: 0px; visibility: hidden; " /> 
                                    </div>
                                    <div class="question-video-create-page">
                                        <div id="questionVideo11" class="question-video questionvideoshowblock" ></div>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <div class="col-xs-3">
                                <label for="inputPassword" class="control-label frm_in_padd col-xs-12">Answer Duration <span style="font-size:10px;">(Limit 600 Sec)</span></label>
                                <div class="col-md-6 no_padding">
                                    <input type="text" class="form-control questiondurationtogetid interviewDuration" placeholder="" name="question[0][duration]" id="durations11" maxlength="3">
                                    <input type="text" data-validation="interviewDuration" style="height: 0px; width: 0px; visibility: hidden; " />
                                </div>
                                <div class="col-md-6">
                                    <p class="padd_txtmin">Sec <a href="javascript:" data-toggle="modal" data-target="#selectQuestionType"><img src="<?php echo base_url(); ?>/theme/firstJob/image/plus.png" class="pad_rg_lfs add-more-question" id="addMore1"><input type="hidden" class="add-more-question-new"/></a></p>
                                </div>  
                            </div> 
                        </div>
                    </div>

                    <div id="practiceSetBlock" style="display: <?php if (isset($interview['environment']) && $interview['environment'] == 2) {
                            echo "block";
                        } else {
                            echo "none";
                        } ?>">
                        <!-- SECOND SECTION QUESTIONS -->
                        <div class="form-group">
                            <label for="inputPassword" class="control-label col-xs-2">Type</label>
                            <div class="col-xs-10">
                                <select class="selectpicker" name="type_1" id="type_1" data-validation="required">
                                    <option selected="" value="2" <?php if (isset($interview['type']) && $interview['type'] == 2) echo "selected"; ?>>Audio</option>
                                </select>
                            </div>
                        </div>

                        <div id ="questionList1">
                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-2">Question</label>
                                <div class="col-xs-4 ques-title1 questionOnTypeBlock1">
                                    <label for="inputPassword" class="control-label frm_in_padd">Question Title</label>
                                    <select data-placeholder="Type a question" style="width: 350px; display: none;" class="chosen-select1 sel_sty_padd interviewAudioQuestion" tabindex="-1" name="question1[0][title]">
                                        <option value=""></option>
                                    </select>                                    
                                    <input type="text" data-validation="interviewAudioQuestion" style="height: 0px; width: 0px; visibility: hidden; " />
                                </div>
                                <?php if ($interview['type'] != 3): ?>
                                    <div class="col-xs-3 file-section1">
                                        <div class="col-md-12">
                                            <label for="inputPassword" class="control-label frm_in_padd" style="font-size: 12px;">Upload File(mp3, mp4)</label>
                                        </div>
                                        <div class="col-xs-8">
                                            <input type="text" class="form-control file-name1 interviewQuestionFileName" readonly="readonly" name="input_file_1_0" id="filetextboxforimage21">
                                        </div>
                                        <div class="col-xs-4 no_padding browse-section">
                                            <button type="button" class="btn_upload">BROWSE</button>
                                            <input type="file" name="file_1_0" class="question-file1 interviewQuestionFile" />
                                            <input type="text" data-validation="interviewAudioQuestionFile" style="height: 0px; width: 0px; visibility: hidden; " />
                                        </div>
                                        <div class="question-video-create-page">
                                            <div id="questionVideo21" class="question-video questionvideoshowblock1" ></div>
                                        </div>
                                    </div>
                                <?php endif; ?>

                                <div class="col-xs-3">
                                    <label for="inputPassword" class="control-label frm_in_padd col-xs-12">Answer Duration <span style="font-size:10px;">(Limit 600 Sec)</span></label>
                                    <div class="col-md-6 no_padding">
                                        <input type="text" class="form-control questiondurationtogetid1 interviewAudioDuration" placeholder="" name="question1[0][duration]" id="durations21" maxlength="3">
                                        <input type="text" data-validation="interviewAudioQuestionDuration" style="height: 0px; width: 0px; visibility: hidden; " />
                                    </div>
                                    <div class="col-md-6">
                                        <p class="padd_txtmin">Sec <a href="javascript:" data-toggle="modal" data-target="#selectQuestionType1"><img src="<?php echo base_url(); ?>/theme/firstJob/image/plus.png" class="pad_rg_lfs add-more-question" id="addMore11">
                                                <input type="hidden" class="add-more-question-new"/>
                                            </a></p>
                                    </div>  
                                </div> 
                            </div>
                        </div>

                        <!-- THIRD SECTION QUESTIONS -->
                        <div class="form-group">
                            <label for="inputPassword" class="control-label col-xs-2">Type</label>
                            <div class="col-xs-10">
                                <select class="selectpicker" name="type_2" id="type_2">
                                    <option selected="" value="3" <?php if (isset($interview['type']) && $interview['type'] == 3) echo "selected"; ?>>Text</option>
                                </select>
                            </div>
                        </div>

                        <div id ="questionList2">
                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-2">Question</label>
                                <div class="col-xs-4 ques-title2 questionOnTypeBlock2">
                                    <label for="inputPassword" class="control-label frm_in_padd">Question Title</label>
                                    <select data-placeholder="Type a question" style="width: 350px; display: none;" class="chosen-select2 sel_sty_padd interviewTextQuestion" tabindex="-1" name="question2[0][title]">
                                        <option value=""></option>
                                    </select>
                                    <input type="text" data-validation="interviewTextQuestion" style="height: 0px; width: 0px; visibility: hidden; " />
                                </div>


                                <div class="col-xs-3">
                                    <label for="inputPassword" class="control-label frm_in_padd col-xs-12">Answer Duration <span style="font-size:10px;">(Limit 600 Sec)</span></label>
                                    <div class="col-md-6 no_padding">
                                        <input type="text" class="form-control questiondurationtogetid2 interviewTextDuration" placeholder="" name="question2[0][duration]" id="durations31" maxlength="3">
                                        <input type="text" data-validation="interviewTextQuestionDuration" style="height: 0px; width: 0px; visibility: hidden; " />
                                    </div>
                                    <div class="col-md-6">
                                        <p class="padd_txtmin">Sec <a href="javascript:" data-toggle="modal" data-target="#selectQuestionType2"><img src="<?php echo base_url(); ?>/theme/firstJob/image/plus.png" class="pad_rg_lfs add-more-question" id="addMore2">
                                                <input type="hidden" class="add-more-question-new"/>
                                            </a></p>
                                    </div>  
                                </div> 
                            </div>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="col-xs-offset-2 col-xs-10 no_padding">

                        <input type="hidden" name="accessToken" value="<?php echo $token; ?>"/>
                        <button type="submit" class="Save_frm pull-right save-interview" id="confirmbefore_submit" value="testing">SAVE</button>

                    </div>
                </form>

            </div>
        </div>
    </div>
</div>



<!-- PopUp for add question to interview set  -->
<div id="selectQuestionType" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <form class="form-horizontal form_save" action="<?php echo base_url(); ?>interviews/addToInterview" method="post" id="interviewQuestionForm" >
            <div class="modal-content">

                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Choose Question Bank</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger hidden" id="interviewQuestionAlert">

                    </div>
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-3"></label>
                        <div class="col-xs-9" id="questionType">
                            <select data-placeholder="questionType" style="width: 350px;" id="aQuestionType" name="questionType">
                                <option value="1">Fj Question Bank</option>
                                <option value="2">My Question Bank</option>

                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary select-interview-question">Add</button>

                    <!--                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
                </div>
            </div>
        </form>

    </div>
</div>


<!-- PopUp for add question to interview set  -->
<div id="selectQuestionType1" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <form class="form-horizontal form_save" action="<?php echo base_url(); ?>interviews/addToInterview" method="post" id="interviewQuestionForm" >
            <div class="modal-content">

                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Choose Question Bank</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger hidden" id="interviewQuestionAlert">

                    </div>
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-3"></label>
                        <div class="col-xs-9" id="questionType">
                            <select data-placeholder="questionType" style="width: 350px;" id="aQuestionType" name="questionType">
                                <option value="1">Fj Question Bank</option>
                                <option value="2">My Question Bank</option>

                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary select-interview-question1">Add</button>

                    <!--                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
                </div>
            </div>
        </form>

    </div>
</div>

<!-- PopUp for add question to interview set  -->
<div id="selectQuestionType2" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <form class="form-horizontal form_save" action="<?php echo base_url(); ?>interviews/addToInterview" method="post" id="interviewQuestionForm" >
            <div class="modal-content">

                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Choose Question Bank</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger hidden" id="interviewQuestionAlert">

                    </div>
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-3"></label>
                        <div class="col-xs-9" id="questionType">
                            <select data-placeholder="questionType" style="width: 350px;" id="aQuestionType" name="questionType">
                                <option value="1">Fj Question Bank</option>
                                <option value="2">My Question Bank</option>

                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary select-interview-question2">Add</button>

                    <!--                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
                </div>
            </div>
        </form>

    </div>
</div>

<script src="<?php echo base_url(); ?>theme/firstJob/js/chosen.jquery.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>theme/firstJob/js/prism.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">

    var config = {
        '.chosen-select': {},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: ''},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }

    var config1 = {
        '.chosen-select1': {},
        '.chosen-select1-deselect': {allow_single_deselect: true},
        '.chosen-select1-no-single': {disable_search_threshold: 10},
        '.chosen-select1-no-results': {no_results_text: ''},
        '.chosen-select1-width': {width: "95%"}
    }
    for (var selector in config1) {
        $(selector).chosen(config1[selector]);
    }

    var config2 = {
        '.chosen-select2': {},
        '.chosen-select2-deselect': {allow_single_deselect: true},
        '.chosen-select2-no-single': {disable_search_threshold: 10},
        '.chosen-select2-no-results': {no_results_text: ''},
        '.chosen-select2-width': {width: "95%"}
    }
    for (var selector in config2) {
        $(selector).chosen(config2[selector]);
    }

    var config3 = {
        '.chosen-select3': {},
        '.chosen-select3-deselect': {allow_single_deselect: true},
        '.chosen-select3-no-single': {disable_search_threshold: 10},
        '.chosen-select3-no-results': {no_results_text: ''},
        '.chosen-select3-width': {width: "95%"}
    }
    for (var selector in config3) {
        $(selector).chosen(config3[selector]);
    }

    var config4 = {
        '.chosen-select4': {},
        '.chosen-select4-deselect': {allow_single_deselect: true},
        '.chosen-select4-no-single': {disable_search_threshold: 10},
        '.chosen-select4-no-results': {no_results_text: ''},
        '.chosen-select4-width': {width: "95%"}
    }
    for (var selector in config4) {
        $(selector).chosen(config4[selector]);
    }



    $(function () {
        $("#datepicker-19").datepicker();
    });



    $(document).on('blur', '#questionList .chosen-search input', function (evt) {
        //alert($('.no-results').length);
        if ($('.no-results').length == 1) {
            //alert($('.chosen-choices .search-field input').val());
            //$('.chosen-select');
            $(this).parents('.ques-title').find('.chosen-select').append('<option>' + $(evt.target).val() + '</option>');
            $(this).parents('.ques-title').find('.chosen-select').val($(evt.target).val());
            $(this).parents('.ques-title').find('.chosen-select').trigger('chosen:updated');
        } else {
            $("#questionList .chosen-select").chosen({
                width: "300px",
                //  enable_search_threshold: 10
            }).change(function (event)
            {
                //alert(event.target);
                if (event.target)
                {
                    //var indexValue = parseInt($('#questionList .form-group').length);
                    var questionValueId = this.value;
                    var indexFileNameValue = $(this).parents('.form-group').find('.file-name').attr('id');
                    var indexDurationValue = $(this).parents('.form-group').find('.questiondurationtogetid').attr('id');
                    var indexShowVideoValue = $(this).parents('.form-group').find('.questionvideoshowblock').attr('id');

                    firstBlockForQuestionDetails(questionValueId, indexFileNameValue, indexDurationValue, indexShowVideoValue);
                    //alert(indexFileNameValue);
                    //alert(indexDurationValue);                    
                    //alert(value);
                    ///$("#result").text(value);
                }
            });
        }
    });



    $(document).on('blur', '#questionList1 .chosen-search input', function (evt) {
        //alert($('.no-results').length);
        if ($('.no-results').length == 1) {
            //alert('gggg');
            //alert($('.chosen-choices .search-field input').val());
            //$('.chosen-select');
            $(this).parents('.ques-title1').find('.chosen-select1').append('<option>' + $(evt.target).val() + '</option>');
            $(this).parents('.ques-title1').find('.chosen-select1').val($(evt.target).val());
            $(this).parents('.ques-title1').find('.chosen-select1').trigger('chosen:updated');

        } else {

            $("#questionList1 .chosen-select1").chosen({
                width: "300px",
                //  enable_search_threshold: 10
            }).change(function (event)
            {
                //alert(event.target);
                if (event.target)
                {
                    //alert('gggg');
                    //var indexValue = parseInt($('#questionList .form-group').length);
                    var questionValueId = this.value;
                    var indexFileNameValue = $(this).parents('.form-group').find('.file-name1').attr('id');
                    var indexDurationValue = $(this).parents('.form-group').find('.questiondurationtogetid1').attr('id');
                    var indexShowVideoValue = $(this).parents('.form-group').find('.questionvideoshowblock1').attr('id');

                    firstBlockForQuestionDetails(questionValueId, indexFileNameValue, indexDurationValue, indexShowVideoValue);
                    //alert(indexFileNameValue);
                    //alert(indexDurationValue);                    
                    //alert(questionValueId);
                }
            });
        }

    });

    $(document).on('blur', '#questionList2 .chosen-search input', function (evt) {
        //alert($('.no-results').length);
        if ($('.no-results').length == 1) {
            //alert($('.chosen-choices .search-field input').val());
            //$('.chosen-select');
            $(this).parents('.ques-title2').find('.chosen-select2').append('<option>' + $(evt.target).val() + '</option>');
            $(this).parents('.ques-title2').find('.chosen-select2').val($(evt.target).val());
            $(this).parents('.ques-title2').find('.chosen-select2').trigger('chosen:updated');

        } else {
            $("#questionList2 .chosen-select2").chosen({
                width: "300px",
                //  enable_search_threshold: 10
            }).change(function (event)
            {
                //alert(event.target);
                if (event.target)
                {
                    //var indexValue = parseInt($('#questionList .form-group').length);
                    var questionValueId = this.value;
                    var indexDurationValue = $(this).parents('.form-group').find('.questiondurationtogetid2').attr('id');

                    firstBlockForQuestionDetails(questionValueId, '', indexDurationValue, '');
                    //alert(indexFileNameValue);
                    //alert(questionValueId);                   
                    //alert(indexDurationValue);                    
                    //alert(value);
                    ///$("#result").text(value);
                }
            });
        }

    });


    function firstBlockForQuestionDetails(questionValueId, indexFileNameValue, indexDurationValue, indexShowVideoValue) {

        if ((questionValueId != "" && indexFileNameValue != "" && indexDurationValue != "") || (questionValueId != "" && indexDurationValue != "")) {
            // show industry drop down div
            formData = {questionValueId: questionValueId, indexFileNameValue: indexFileNameValue, indexDurationValue: indexDurationValue}; //Array 
            //alert($(this).val());
            //alert(siteUrl + "interviews/getQuestionDetailsByQuestionId");

            $.ajax({
                url: siteUrl + "interviews/getQuestionDetailsByQuestionId",
                type: "POST",
                data: formData,
                cache: false,
                success: function (result)
                {
                    $("#" + indexDurationValue).val('');
                    $("#" + indexFileNameValue).val('');
                    //$("#"+indexShowVideoValue).html('');
                    //alert(result);
                    //alert(textStatus);
                    if (result.trim() != "NotDone" || result.trim() == "") {

                        var resultArr = result.split('####');

                        if (resultArr[0].trim() != "" && resultArr[0].trim() != "NotVideo") {

                            $("#" + indexFileNameValue).val(resultArr[0].trim());

                            if (resultArr[1].trim() != "" && resultArr[1].trim() != "NotType") {
                                if (resultArr[1].trim() == "1") {
                                    var videotype = "video";
                                } else {
                                    var videotype = "audio";
                                }

                                var videoUrl = "uploads/questions/" + videotype + "/" + resultArr[0].trim();
                                //alert(videoUrl);
                                jwVideo(videoUrl, indexShowVideoValue);
                            }

                        }

                        if (resultArr[2].trim() != "" && resultArr[2].trim() != "NotDuration") {

                            $("#" + indexDurationValue).val(resultArr[2]);
                        }
                    }
                    //data - response from server
                }
            });
        }
    }



    $(document).on('blur', '.chosen-search input', function (evt) {
        //alert($('.no-results').length);
        if ($('.no-results').length == 1) {
            //alert($('.chosen-choices .search-field input').val());
            //$('.chosen-select');
            $(this).parents('.ques-title3').find('.chosen-select3').append('<option>' + $(evt.target).val() + '</option>');
            $(this).parents('.ques-title3').find('.chosen-select3').val($(evt.target).val());
            $(this).parents('.ques-title3').find('.chosen-select3').trigger('chosen:updated');

        }

    });

    $(document).on('blur', '.chosen-search input', function (evt) {
        //alert($('.no-results').length);
        if ($('.no-results').length == 1) {
            //alert($('.chosen-choices .search-field input').val());
            //$('.chosen-select');
            $(this).parents('.ques-title4').find('.chosen-select4').append('<option>' + $(evt.target).val() + '</option>');
            $(this).parents('.ques-title4').find('.chosen-select4').val($(evt.target).val());
            $(this).parents('.ques-title4').find('.chosen-select4').trigger('chosen:updated');

        }

    });


    //DOM loaded 
    $(document).ready(function () {
        //class for using to get radio button id
        $('.question-for-radio').click(function () {

            var selectedVal = "";
            var selected = $("input[type='radio'][name='environment']:checked");
            if (selected.length > 0) {
                selectedVal = selected.val();
            }

            if (selectedVal == 1)
            {
                var selectedfirstdropdownvalue = $('#type').val();

                $("#blockInterviewSection1").html('');

                var selecthtml = "<select class=\"selectpicker\" name=\"type\" id=\"type\">\n\
                                    <option value=\"0\">Select</option>\n\
                                    <option " + (selectedfirstdropdownvalue != "" && selectedfirstdropdownvalue == 1 ? " selected=\"\"" : "") + " value=\"1\">Video</option>\n\
                                    <option value=\"2\">Audio</option>\n\
                                    <option value=\"3\">Text</option>\n\
                                  </select>";
                // hide industry drop down div
                $("#industryDivId").hide();
                $("#practiceSetBlock").hide();
                $("#blockInterviewSection1").html(selecthtml);
            } else if (selectedVal == 2) {
                //$("#type").trigger('onchange');
                firstBlockQuestion();
                secondBlockQuestion();
                thirdBlockQuestion();
                $("#blockInterviewSection1").html('');
                var selecthtml = "<select class=\"selectpicker\" name=\"type\" id=\"type\">\n\
                                    <option selected=\"\" value=\"1\">Video</option>\n\
                                  </select>";
                // show industry drop down div
                $("#industryDivId").show();
                $("#practiceSetBlock").show();
                $("#blockInterviewSection1").html(selecthtml);

            }
        });
    });

    /*
     $('#industryId').on('change', function() {
     var selected = $("input[type='radio'][name='environment']:checked");
     if (selected.length > 0) {
     selectedVal = selected.val();
     }
     
     if( selectedVal == 2 ) {
     // show industry drop down div
     formData = {industryId:$(this).val()}; //Array 
     //alert($(this).val());
     //alert(siteUrl + "interviews/getindustryfunction");
     
     $.ajax({
     url : siteUrl + "interviews/getindustryfunction",
     type: "POST",
     data : formData,
     cache: false,
     success: function(result)
     {
     //alert(result);
     //alert(textStatus);
     if( result.trim() == "NotDone" ){
     var stringquery = "<select class=\"selectpicker\" name=\"functionId\" id=\"functionId\"><option value=\"\">Select Function</option></select>";
     //alert(stringquery);
     $("#industryFunctionDiv").html(stringquery);
     
     } else {
     $("#industryFunctionDiv").html(result);
     }
     //data - response from server
     }
     });
     }
     });
     */
    $('#type').on('change', function () {
        var questionTypeId = $(this).val();


        if (questionTypeId != "") {
            // show industry drop down div
            formData = {questionTypeId: questionTypeId}; //Array 
            //alert($(this).val());
            //alert(siteUrl + "interviews/getQuestionOnTypeFunction");

            $.ajax({
                url: siteUrl + "interviews/getQuestionOnTypeFunction",
                type: "POST",
                data: formData,
                cache: false,
                success: function (result)
                {
                    //alert(result);
                    //alert(textStatus);
                    if (result.trim() == "NotDone" || result.trim() == "") {
                        var stringquery = "<label for=\"inputPassword\" class=\"control-label frm_in_padd\">Question Title</label><select data-placeholder=\"Type a question\" style=\"width: 350px; display: none;\" class=\"chosen-select sel_sty_padd\" tabindex=\"-1\" name=\"question[0][title]\"><option value=\"\"></option></select>";
                        //alert(stringquery);
                        $(".questionOnTypeBlock").html(stringquery);

                    } else {
                        $(".questionOnTypeBlock").html(result);

                    }

                    for (var selector in config) {
                        $(selector).chosen(config[selector]);
                    }
                    //data - response from server
                }
            });
        }
    });

    function firstBlockQuestion() {

        var questionTypeId = 1;


        if (questionTypeId != "") {
            // show industry drop down div
            formData = {questionTypeId: questionTypeId}; //Array 
            //alert($(this).val());
            //alert(siteUrl + "interviews/getQuestionOnTypeFunction");

            $.ajax({
                url: siteUrl + "interviews/getQuestionOnTypeFunction",
                type: "POST",
                data: formData,
                cache: false,
                success: function (result)
                {
                    //alert(result);
                    //alert(textStatus);
                    if (result.trim() == "NotDone" || result.trim() == "") {
                        var stringquery = "<label for=\"inputPassword\" class=\"control-label frm_in_padd\">Question Title</label><select data-placeholder=\"Type a question\" style=\"width: 350px; display: none;\" class=\"chosen-select sel_sty_padd\" tabindex=\"-1\" name=\"question[0][title]\"><option value=\"\"></option></select>";
                        //alert(stringquery);
                        $(".questionOnTypeBlock").html(stringquery);

                    } else {
                        $(".questionOnTypeBlock").html(result);

                    }

                    for (var selector in config) {
                        $(selector).chosen(config[selector]);
                    }
                    //data - response from server
                }
            });
        }
    }

    function secondBlockQuestion() {
        var questionTypeId = 2;


        if (questionTypeId != "") {
            // show industry drop down div
            formData = {questionTypeId: questionTypeId}; //Array 
            //alert($(this).val());
            //alert(siteUrl + "interviews/getQuestionOnTypeFirstFunction");

            $.ajax({
                url: siteUrl + "interviews/getQuestionOnTypeFirstFunction",
                type: "POST",
                data: formData,
                cache: false,
                success: function (result)
                {
                    //alert(result);
                    //alert(textStatus);
                    if (result.trim() == "NotDone" || result.trim() == "") {
                        var stringquery = "<label for=\"inputPassword\" class=\"control-label frm_in_padd\">Question Title</label><select data-placeholder=\"Type a question\" style=\"width: 350px; display: none;\" class=\"chosen-select1 sel_sty_padd\" tabindex=\"-1\" name=\"question1[0][title]\"><option value=\"\"></option></select>";
                        //alert(stringquery);
                        $(".questionOnTypeBlock1").html(stringquery);

                    } else {
                        $(".questionOnTypeBlock1").html(result);
                    }

                    var config1 = {
                        '.chosen-select1': {},
                        '.chosen-select1-deselect': {allow_single_deselect: true},
                        '.chosen-select1-no-single': {disable_search_threshold: 10},
                        '.chosen-select1-no-results': {no_results_text: ''},
                        '.chosen-select1-width': {width: "95%"}
                    }
                    for (var selector in config1) {
                        $(selector).chosen(config1[selector]);
                    }
                    //data - response from server
                }
            });
        }
    }

    function thirdBlockQuestion() {
        var questionTypeId = 3;


        if (questionTypeId != "") {
            // show industry drop down div
            formData = {questionTypeId: questionTypeId}; //Array 
            //alert($(this).val());
            //alert(siteUrl + "interviews/getQuestionOnTypeFunction");

            $.ajax({
                url: siteUrl + "interviews/getQuestionOnTypeSecondFunction",
                type: "POST",
                data: formData,
                cache: false,
                success: function (result)
                {
                    //alert(result);
                    //alert(textStatus);
                    if (result.trim() == "NotDone" || result.trim() == "") {
                        var stringquery = "<label for=\"inputPassword\" class=\"control-label frm_in_padd\">Question Title</label><select data-placeholder=\"Type a question\" style=\"width: 350px; display: none;\" class=\"chosen-select2 sel_sty_padd\" tabindex=\"-1\" name=\"question2[0][title]\"><option value=\"\"></option></select>";
                        2 //alert(stringquery);
                        $(".questionOnTypeBlock2").html(stringquery);

                    } else {
                        $(".questionOnTypeBlock2").html(result);
                    }

                    var config2 = {
                        '.chosen-select2': {},
                        '.chosen-select2-deselect': {allow_single_deselect: true},
                        '.chosen-select2-no-single': {disable_search_threshold: 10},
                        '.chosen-select2-no-results': {no_results_text: ''},
                        '.chosen-select2-width': {width: "95%"}
                    }
                    for (var selector in config2) {
                        $(selector).chosen(config2[selector]);
                    }
                    //data - response from server
                }
            });
        }
    }

    $(function () {
        var url = '<?php echo base_url(); ?>';
        window.siteUrl = url;
        window.token = '<?php echo $token; ?>';
    });

    $('#confirmbefore_submit').on('click', function () {

        var setNameValue = $('#setname_value').val();
        if (setNameValue != "") {

            bootbox.confirm("Are you sure you want to add this question to the Interview Set '" + setNameValue + "'?", function (result) {

                if (result) {
                    $("form[name='interviewsetfrm']").submit();
                } else {
                    return true;
                }
            });
            return false;
        } else {
            return true;
        }

    });

    $('#industryId').on('change', function () {
        //$("#functionId").attr('selectedIndex', '-1');
        //$("option:selected").prop("selected", false)
    });


</script>
<style>
    .no-results {
        display: none!important;
    }
    .chosen-single {
        background :none!important;
        border-radius:0px !important;
        height: 34px!important;
    }
</style>






<script src="<?php echo base_url(); ?>theme/firstJob/js/validate.js"></script>
<?php
$userData = $this->session->userdata['logged_in'];
$userRole = $userData->role;
if($userRole!='1') { ?>
<script type="text/javascript">
    // Add validator For Interview Assessment
    $.formUtils.addValidator({
        name: 'interviewQuestion',
        validatorFunction: function (value, $el, config, language, $form) {                  
            var setMode;                        
            if($('#environment_1').is(':checked')) {
                setMode = true;
            }
            else if($('#environment_2').is(':checked')) {
                setMode = false;
            }
            else {
                setMode = 'undefined';
            }
            
            var hasQusNoValue;            
            var videoQuestionCount=0;            
            $('.interviewQuestion').each(function(i) {
                videoQuestionCount++;
                if ($(this).val() == '') {
                      hasQusNoValue = true;
                }
            });
            
            var hasFileNoValue;
            var videoQuestionFileCount=0;
            $('.interviewFileNames').each(function(i) {
                videoQuestionFileCount++;
                if ($(this).val() == '') {
                      hasFileNoValue = true;
                }
            });
            
            var hasDurNoValue;
            var videoQuestionDurationCount=0;
            $('.interviewDuration').each(function(i) {
                videoQuestionDurationCount++;
                if ($(this).val() == '') {
                      hasDurNoValue = true;
                }
            });
            if(setMode==false) {
                if (hasQusNoValue==true && hasDurNoValue==true && hasFileNoValue==true && videoQuestionCount==1 && videoQuestionDurationCount==1 && videoQuestionFileCount==1) {
                    return true;
                }
                else if (!hasQusNoValue && videoQuestionCount==1 && videoQuestionDurationCount==1 && videoQuestionFileCount==1) {
                    return true;
                }
                else if (!hasQusNoValue && videoQuestionCount>1 && videoQuestionDurationCount>1 && videoQuestionFileCount>1) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {               
                if (hasQusNoValue) {
                    return false;
                }
                else {
                    return true;
                } 
            }
            //return parseInt(value, 10) % 2 === 0;
        },
        errorMessage : 'All question title must be filled'
    });
    
    $.formUtils.addValidator({
        name: 'interviewFile',
        validatorFunction: function (value, $el, config, language, $form) {                  
            var setMode;                        
            if($('#environment_1').is(':checked')) {
                setMode = true;
            }
            else if($('#environment_2').is(':checked')) {
                setMode = false;
            }
            else {
                setMode = 'undefined';
            }
            
            var hasQusNoValue;            
            var videoQuestionCount=0;
            $('.interviewQuestion').each(function(i) {
                videoQuestionCount++;
                if ($(this).val() == '') {
                      hasQusNoValue = true;
                }
            });
            
            var hasFileNoValue;
            var videoQuestionFileCount=0;
            $('.interviewFileNames').each(function(i) {
                videoQuestionFileCount++;
                if ($(this).val() == '') {
                      hasFileNoValue = true;
                }
            });
            
            var hasDurNoValue;
            var videoQuestionDurationCount=0;
            $('.interviewDuration').each(function(i) {
                videoQuestionDurationCount++;
                if ($(this).val() == '') {
                      hasDurNoValue = true;
                }
            });
            
            
            if(setMode==false) {
                if (hasQusNoValue==true && hasDurNoValue==true && hasFileNoValue==true && videoQuestionCount==1 && videoQuestionDurationCount==1 && videoQuestionFileCount==1) {
                    return true;
                }
                else if (!hasFileNoValue && videoQuestionCount==1 && videoQuestionDurationCount==1 && videoQuestionFileCount==1) {
                    return true;
                }
                else if (!hasFileNoValue && videoQuestionCount>1 && videoQuestionDurationCount>1 && videoQuestionFileCount>1) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {               
                if (hasFileNoValue) {
                    return false;
                }
                else {
                    return true;
                } 
            }
            //return parseInt(value, 10) % 2 === 0;
        },
        errorMessage : 'All questions files must be filled'
    });
    
    $.formUtils.addValidator({
        name: 'interviewDuration',
        validatorFunction: function (value, $el, config, language, $form) {                   
            var setMode;                        
            if($('#environment_1').is(':checked')) {
                setMode = true;
            }
            else if($('#environment_2').is(':checked')) {
                setMode = false;
            }
            else {
                setMode = 'undefined';
            }
            var hasQusNoValue;            
            var videoQuestionCount=0;
            $('.interviewQuestion').each(function(i) {
                videoQuestionCount++;
                if ($(this).val() == '') {
                      hasQusNoValue = true;
                }
            });
            
            var hasFileNoValue;
            var videoQuestionFileCount=0;
            $('.interviewFileNames').each(function(i) {
                videoQuestionFileCount++;
                if ($(this).val() == '') {
                    hasFileNoValue = true;
                }
            });
            
            var hasDurNoValue;
            var videoQuestionDurationCount=0;
            var durationExceed;
            $('.interviewDuration').each(function(i) {
                videoQuestionDurationCount++;
                if ($(this).val() == '') {
                    hasDurNoValue = true;
                }
                else {
                    if (parseInt($(this).val())>600 || isNaN($(this).val())) {
                        durationExceed = true;
                    }
                }
            });
            
            
            if(setMode==false) {      
                if (hasQusNoValue==true && hasDurNoValue==true && hasFileNoValue==true && videoQuestionCount==1 && videoQuestionDurationCount==1 && videoQuestionFileCount==1) {
                    return true;
                }
                else if (!hasDurNoValue && !durationExceed && videoQuestionCount==1 && videoQuestionDurationCount==1 && videoQuestionFileCount==1) {
                    return true;
                }
                else if (!hasDurNoValue && !durationExceed && videoQuestionCount>1 && videoQuestionDurationCount>1 && videoQuestionFileCount>1) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {               
                if (hasDurNoValue==true || durationExceed==true) {
                    return false;
                }
                else {
                    return true;
                } 
            }
            //return parseInt(value, 10) % 2 === 0;
        },
        errorMessage :  'All questions duration (limit 600 sec) fields should be less than 600 seconds '
    });
</script>    
<?php
}
if($userRole=='1') { ?>
<script>
        // Add validator For Interview Assessment
    $.formUtils.addValidator({
        name: 'interviewQuestion',
        validatorFunction: function (value, $el, config, language, $form) {                  
            var setMode;                        
            if($('#environment_1').is(':checked')) {
                setMode = true;
            }
            else if($('#environment_2').is(':checked')) {
                setMode = false;
            }
            else {
                setMode = 'undefined';
            }
            
            var hasQusNoValue;            
            var videoQuestionCount=0;            
            $('.interviewQuestion').each(function(i) {
                videoQuestionCount++;
                if ($(this).val() == '') {
                      hasQusNoValue = true;
                }
            });
            
            var hasFileNoValue;
            var videoQuestionFileCount=0;
            $('.interviewFileNames').each(function(i) {
                videoQuestionFileCount++;
                if ($(this).val() == '') {
                      hasFileNoValue = true;
                }
            });
            
            var hasDurNoValue;
            var videoQuestionDurationCount=0;
            $('.interviewDuration').each(function(i) {
                videoQuestionDurationCount++;
                if ($(this).val() == '') {
                      hasDurNoValue = true;
                }
            });
            if(setMode==false) {
                if (hasQusNoValue==true && hasDurNoValue==true && hasFileNoValue==true && videoQuestionCount==1 && videoQuestionDurationCount==1 && videoQuestionFileCount==1) {
                    return true;
                }
                else if (!hasQusNoValue && videoQuestionCount==1 && videoQuestionDurationCount==1 && videoQuestionFileCount==1) {
                    return true;
                }
                else if (!hasQusNoValue && videoQuestionCount>1 && videoQuestionDurationCount>1 && videoQuestionFileCount>1) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {               
                if (hasQusNoValue) {
                    return false;
                }
                else {
                    return true;
                } 
            }
            //return parseInt(value, 10) % 2 === 0;
        },
        errorMessage : 'All video question title must be filled'
    });
    
    $.formUtils.addValidator({
        name: 'interviewFile',
        validatorFunction: function (value, $el, config, language, $form) {                  
            var setMode;                        
            if($('#environment_1').is(':checked')) {
                setMode = true;
            }
            else if($('#environment_2').is(':checked')) {
                setMode = false;
            }
            else {
                setMode = 'undefined';
            }
            
            var hasQusNoValue;            
            var videoQuestionCount=0;
            $('.interviewQuestion').each(function(i) {
                videoQuestionCount++;
                if ($(this).val() == '') {
                      hasQusNoValue = true;
                }
            });
            
            var hasFileNoValue;
            var videoQuestionFileCount=0;
            $('.interviewFileNames').each(function(i) {
                videoQuestionFileCount++;
                if ($(this).val() == '') {
                      hasFileNoValue = true;
                }
            });
            
            var hasDurNoValue;
            var videoQuestionDurationCount=0;
            $('.interviewDuration').each(function(i) {
                videoQuestionDurationCount++;
                if ($(this).val() == '') {
                      hasDurNoValue = true;
                }
            });
            
            
            if(setMode==false) {
                if (hasQusNoValue==true && hasDurNoValue==true && hasFileNoValue==true && videoQuestionCount==1 && videoQuestionDurationCount==1 && videoQuestionFileCount==1) {
                    return true;
                }
                else if (!hasFileNoValue && videoQuestionCount==1 && videoQuestionDurationCount==1 && videoQuestionFileCount==1) {
                    return true;
                }
                else if (!hasFileNoValue && videoQuestionCount>1 && videoQuestionDurationCount>1 && videoQuestionFileCount>1) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {               
                if (hasFileNoValue) {
                    return false;
                }
                else {
                    return true;
                } 
            }
            //return parseInt(value, 10) % 2 === 0;
        },
        errorMessage : 'All video questions files must be filled'
    });
    
    $.formUtils.addValidator({
        name: 'interviewDuration',
        validatorFunction: function (value, $el, config, language, $form) {                   
            var setMode;                        
            if($('#environment_1').is(':checked')) {
                setMode = true;
            }
            else if($('#environment_2').is(':checked')) {
                setMode = false;
            }
            else {
                setMode = 'undefined';
            }
            var hasQusNoValue;            
            var videoQuestionCount=0;
            $('.interviewQuestion').each(function(i) {
                videoQuestionCount++;
                if ($(this).val() == '') {
                      hasQusNoValue = true;
                }
            });
            
            var hasFileNoValue;
            var videoQuestionFileCount=0;
            $('.interviewFileNames').each(function(i) {
                videoQuestionFileCount++;
                if ($(this).val() == '') {
                    hasFileNoValue = true;
                }
            });
            
            var hasDurNoValue;
            var videoQuestionDurationCount=0;
            var durationExceed;
            $('.interviewDuration').each(function(i) {
                videoQuestionDurationCount++;
                if ($(this).val() == '') {
                    hasDurNoValue = true;
                }
                else {
                    if (parseInt($(this).val())>600 || isNaN($(this).val())) {
                        durationExceed = true;
                    }
                }
            });
            
            
            if(setMode==false) {      
                if (hasQusNoValue==true && hasDurNoValue==true && hasFileNoValue==true && videoQuestionCount==1 && videoQuestionDurationCount==1 && videoQuestionFileCount==1) {
                    return true;
                }
                else if (!hasDurNoValue && !durationExceed && videoQuestionCount==1 && videoQuestionDurationCount==1 && videoQuestionFileCount==1) {
                    return true;
                }
                else if (!hasDurNoValue && !durationExceed && videoQuestionCount>1 && videoQuestionDurationCount>1 && videoQuestionFileCount>1) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {               
                if (hasDurNoValue==true || durationExceed==true) {
                    return false;
                }
                else {
                    return true;
                } 
            }
            //return parseInt(value, 10) % 2 === 0;
        },
        errorMessage :  'All video questions duration (limit 600 sec) fields should be less than 600 seconds '
    });


    // Add validator For Location
    $.formUtils.addValidator({
        name: 'industryFunction',
        validatorFunction: function (value, $el, config, language, $form) {      
            var hasNoValue;
            if($('#functionId').val()!='' && $('#industryId').val()!='') {
                return false;
            }
            else if($('#functionId').val()=='' && $('#industryId').val()=='') {
                return false;
            }
            else {
                return true;
            }
            //return parseInt(value, 10) % 2 === 0;
        },
        errorMessage : 'Please choose either industry or function.'
    });
    
    // Add validator For Interview Assessment
    $.formUtils.addValidator({
        name: 'interviewAudioQuestion',
        validatorFunction: function (value, $el, config, language, $form) {   
            var hasQusNoValue;            
            var audioQuestionCount=0;
            $('.interviewAudioQuestion').each(function(i) {
                audioQuestionCount++;
                if ($(this).val() == '') {
                      hasQusNoValue = true;
                }
            });
            
            var hasFileNoValue;
            var audioQuestionFileCount=0;
            $('.interviewQuestionFileName').each(function(i) {
                audioQuestionFileCount++;
                if ($(this).val() == '') {
                      hasFileNoValue = true;
                }
            });
            
            var hasDurNoValue;
            var audioQuestionDurationCount=0;
            $('.interviewAudioDuration').each(function(i) {
                audioQuestionDurationCount++;
                if ($(this).val() == '') {
                      hasDurNoValue = true;
                }
            });
                        
            if (hasQusNoValue==true && hasDurNoValue==true && hasFileNoValue==true && audioQuestionCount==1 && audioQuestionDurationCount==1 && audioQuestionFileCount==1) {
                return true;
            }
            else if (!hasQusNoValue && audioQuestionCount==1 && audioQuestionDurationCount==1 && audioQuestionFileCount==1) {
                return true;
            }
            else if (!hasQusNoValue && audioQuestionCount>1 && audioQuestionDurationCount>1 && audioQuestionFileCount>1) {
                return true;
            }
            else {
                return false;
            }
            //return parseInt(value, 10) % 2 === 0;
        },
        errorMessage : 'All audio question title must be filled'
    });
    
    $.formUtils.addValidator({
        name: 'interviewAudioQuestionFile',
        validatorFunction: function (value, $el, config, language, $form) {      
            var hasQusNoValue;            
            var audioQuestionCount=0;
            $('.interviewAudioQuestion').each(function(i) {
                audioQuestionCount++;
                if ($(this).val() == '') {
                      hasQusNoValue = true;
                }
            });
            
            var hasFileNoValue;
            var audioQuestionFileCount=0;
            $('.interviewQuestionFileName').each(function(i) {
                audioQuestionFileCount++;
                if ($(this).val() == '') {
                      hasFileNoValue = true;
                }
            });
            
            var hasDurNoValue;
            var audioQuestionDurationCount=0;
            $('.interviewAudioDuration').each(function(i) {
                audioQuestionDurationCount++;
                if ($(this).val() == '') {
                      hasDurNoValue = true;
                }
            });
                        
            if (hasQusNoValue==true && hasDurNoValue==true && hasFileNoValue==true && audioQuestionCount==1 && audioQuestionDurationCount==1 && audioQuestionFileCount==1) {
                return true;
            }
            else if (!hasFileNoValue && audioQuestionCount==1 && audioQuestionDurationCount==1 && audioQuestionFileCount==1) {
                return true;
            }
            else if (!hasFileNoValue && audioQuestionCount>1 && audioQuestionDurationCount>1 && audioQuestionFileCount>1) {
                return true;
            }
            else {
                return false;
            }
            //return parseInt(value, 10) % 2 === 0;
        },
        errorMessage : 'All audio questions files must be filled'
    });
    
    $.formUtils.addValidator({
        name: 'interviewAudioQuestionDuration',
        validatorFunction: function (value, $el, config, language, $form) {  
            var hasQusNoValue;            
            var audioQuestionCount=0;
            $('.interviewAudioQuestion').each(function(i) {
                audioQuestionCount++;
                if ($(this).val() == '') {
                      hasQusNoValue = true;
                }
            });
            
            var hasFileNoValue;
            var audioQuestionFileCount=0;
            $('.interviewQuestionFileName').each(function(i) {
                audioQuestionFileCount++;
                if ($(this).val() == '') {
                    hasFileNoValue = true;
                }
            });
            
            var hasDurNoValue;
            var audioQuestionDurationCount=0;
            var durationExceed;
            $('.interviewAudioDuration').each(function(i) {
                audioQuestionDurationCount++;
                if ($(this).val() == '') {
                    hasDurNoValue = true;
                }
                else {
                    if (parseInt($(this).val())>600 || isNaN($(this).val())) {
                        durationExceed = true;
                    }
                }
            });
                        
            if (hasQusNoValue==true && hasDurNoValue==true && hasFileNoValue==true && audioQuestionCount==1 && audioQuestionDurationCount==1 && audioQuestionFileCount==1) {
                return true;
            }
            else if (!hasDurNoValue && !durationExceed && audioQuestionCount==1 && audioQuestionDurationCount==1 && audioQuestionFileCount==1) {
                return true;
            }
            else if (!hasDurNoValue && !durationExceed && audioQuestionCount>1 && audioQuestionDurationCount>1 && audioQuestionFileCount>1) {
                return true;
            }
            else {
                return false;
            }
            //return parseInt(value, 10) % 2 === 0;
        },
        errorMessage : 'All audio questions duration (limit 600 sec) fields should be less than 600 seconds'
    });
    
    
    
    
    // Add validator For Interview Assessment
    $.formUtils.addValidator({
        name: 'interviewTextQuestion',
        validatorFunction: function (value, $el, config, language, $form) {      
            var hasQusNoValue;            
            var textQuestionCount=0;
            $('.interviewTextQuestion').each(function(i) {
                textQuestionCount++;
                if ($(this).val() == '') {
                      hasQusNoValue = true;
                }
            });
            
            var hasDurNoValue;
            var textQuestionDurationCount=0;
            $('.interviewTextDuration').each(function(i) {
                textQuestionDurationCount++;
                if ($(this).val() == '') {
                      hasDurNoValue = true;
                }
            });
                        
            if (hasQusNoValue==true && hasDurNoValue==true && textQuestionCount==1 && textQuestionDurationCount==1) {
                return true;
            }
            else if (!hasQusNoValue && textQuestionCount==1 && textQuestionDurationCount==1) {
                return true;
            }
            else if (!hasQusNoValue && textQuestionCount>1 && textQuestionDurationCount>1) {
                return true;
            }
            else {
                return false;
            }
            //return parseInt(value, 10) % 2 === 0;
        },
        errorMessage : 'All text question title must be filled'
    });
    
    $.formUtils.addValidator({
        name: 'interviewTextQuestionDuration',
        validatorFunction: function (value, $el, config, language, $form) {  
            var hasQusNoValue;            
            var textQuestionCount=0;
            $('.interviewTextQuestion').each(function(i) {
                textQuestionCount++;
                if ($(this).val() == '') {
                      hasQusNoValue = true;
                }
            });
            
            var hasDurNoValue;
            var textQuestionDurationCount=0;
            var durationExceed;
            $('.interviewTextDuration').each(function(i) {
                textQuestionDurationCount++;
                if ($(this).val() == '') {
                    hasDurNoValue = true;
                }
                else {
                    if (parseInt($(this).val())>600 || isNaN($(this).val())) {
                        durationExceed = true;
                    }
                }
            });
                        
            if (hasQusNoValue==true && hasDurNoValue==true && textQuestionCount==1 && textQuestionDurationCount==1) {
                return true;
            }
            else if (!hasDurNoValue && !durationExceed && textQuestionCount==1 && textQuestionDurationCount==1) {
                return true;
            }
            else if (!hasDurNoValue && !durationExceed && textQuestionCount>1 && textQuestionDurationCount>1) {
                return true;
            }
            else {
                return false;
            }
            //return parseInt(value, 10) % 2 === 0;
        },
        errorMessage : 'All text questions duration (limit 600 sec) fields should be less than 600 seconds '
    });
</script>
<?php
} 
?>
    
    
<script>
    $.validate({
        form: '#interviewsetfrm',
        modules: 'file',
        onError: function ($form) {
            //alert($('.level1').val());
            return false;
        },
        onSuccess: function ($form) {
            return true;
        },
        validateOnBlur : false,
        errorMessagePosition : 'top',
        scrollToTopOnError : true // Set this property to true on longer forms
    });
</script>

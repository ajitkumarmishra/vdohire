<?php
if (isset($questions) && $questions != NULL) {
//    echo "<pre>";
//    print_r($questions);
//    die; 
}
?>
<div id="page-content-wrapper">
    <?php if (isset($message)): ?>
        <div class="alert alert-success">
            <?php echo $message; ?>
        </div>
    <?php endif; ?>
    <?php if (isset($error)): ?>
        <div class="alert alert-danger">
            <?php foreach ($error as $item): ?>
                <?php echo $item; ?>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
    <div class="container-fluid whitebg">
        <div class="row">

            <div class="col-md-12 padd_tp_bt_20">
                <h4><img src="<?php echo base_url(); ?>/theme/firstJob/image/arrow_head.png" class="prev-page"> Add Question</h4>
                <form class="form-horizontal form_save" method="post" action="" enctype="multipart/form-data" id="addQuestionForm">

                    <!--                    <div class="form-group">
                                            <label for="inputPassword" class="control-label col-xs-2 required-field">Type</label>
                                            <div class="col-xs-10">
                                                <select data-placeholder="Type a question" style="width: 350px; display: none;" class="chosen-select sel_sty_padd" tabindex="-1" name="question[0][title]">
                                                        <option value=""></option>
                    <?php foreach ($fjQuestion as $item): ?>
                                                                        <option><?php echo $item['title']; ?></option>
                    <?php endforeach; ?>
                    
                                                    </select>
                                            </div>
                                        </div>-->

                    <div id ="questionList">
                        <div class="form-group">
                            <!--                            <label for="inputPassword" class="control-label col-xs-2 required-field">Question</label>-->
                            <div class="col-xs-2 ques-title">
                                <label for="inputPassword" class="control-label frm_in_padd">Type</label>
                                <select class="selectpicker questionType" name="question[0][type]" id="type_addquestion"><option value="">Select</option>
                                    <option value="1" <?php if (isset($questions[0]['type']) && $questions[0]['type'] == 1) echo "selected"; ?>>Video</option>
                                    <option value="2" <?php if (isset($questions[0]['type']) && $questions[0]['type'] == 2) echo "selected"; ?>>Audio</option>
                                    <option value="3" <?php if (isset($questions[0]['type']) && $questions[0]['type'] == 3) echo "selected"; ?>>Text</option>
                                </select>
                                <input type="text" data-validation="questionType" style="height: 0px; width: 0px; visibility: hidden; " /> 
                            </div>


                            <div class="col-xs-4 ques-title">
                                <label for="inputPassword" class="control-label frm_in_padd">Question Title</label>
                                <input type="text" name="question[0][title]" value="" class="form-control questionTitle" placeholder="Type a question" />
                                <input type="text" data-validation="questionTitle" style="height: 0px; width: 0px; visibility: hidden; " /> 
                                <?php /* ?>
                                <select data-placeholder="Type a question" style="width: 350px; display: none;" class="chosen-select sel_sty_padd" tabindex="-1" name="question[0][title]">
                                    <option value=""></option>
                                    <?php if (isset($questions[0]['title'])) :
                                        foreach ($fjQuestion as $item):
                                            ?>
                                            <option <?php if (isset($questions[0]['title']) && $questions[0]['title'] == $item['title']) echo "selected"; ?>><?php echo $item['title']; ?></option>
                                        <?php endforeach;
                                    endif;
                                    ?>

                                </select>
                                 <?php */ ?>
                            </div>

<?php if ($interview['type'] != 3): ?>
                                <div class="col-xs-3 file-section">
                                    <div class="col-md-12">
                                        <label for="inputPassword" class="control-label frm_in_padd" style="font-size: 12px;">Upload File(mp3, mp4)</label>
                                    </div>
                                    <div class="col-xs-8">
                                        <input type="text" class="form-control file-name questionFileName" readonly>
                                    </div>
                                    <div class="col-xs-4 no_padding browse-section">
                                        <button type="button" class="btn_upload questionFile">BROWSE</button>
                                        <input type="file" name="file_0" class="my-question-file questionFile" />
                                        <input type="text" data-validation="questionFile" style="height: 0px; width: 0px; visibility: hidden; " />
                                    </div>
                                </div>
<?php endif; ?>

                            <div class="col-xs-3">
                                <label for="inputPassword" class="control-label frm_in_padd col-xs-12">Answer Duration <span style="font-size:10px;">(Limit 600 Sec)</span></label>
                                <div class="col-md-6 no_padding">
                                    <input type="text" class="form-control questionDuration" placeholder="" name="question[0][duration]" value="<?php if (isset($questions[0]['duration'])) echo $questions[0]['duration']; ?>" maxlength="3">
                                    <input type="text" data-validation="questionDuration" style="height: 0px; width: 0px; visibility: hidden; " />
                                </div>
                                <div class="col-md-6">
                                    <p class="padd_txtmin">Sec <a href="javascript:"><img src="<?php echo base_url(); ?>/theme/firstJob/image/plus.png" class="pad_rg_lfs add-more-my-question">
                                        </a></p>
                                </div>  
                            </div> 
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="col-xs-offset-2 col-xs-10 no_padding">
                        <input type="hidden" name="accessToken" value="<?php echo $token; ?>"/>
                        <button type="submit" class="Save_frm pull-right save-interview" value="testing">SAVE</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url(); ?>theme/firstJob/js/chosen.jquery.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>theme/firstJob/js/prism.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">

    var config = {
        '.chosen-select': {},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: ''},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }

    $(function () {
        $("#datepicker-19").datepicker();
    });
    $(document).on('blur', '.chosen-search input', function (evt) {
        //alert($('.no-results').length);
        if ($('.no-results').length == 1) {
            //alert($('.chosen-choices .search-field input').val());
            //$('.chosen-select');
            $(this).parents('.ques-title').find('.chosen-select').append('<option>' + $(evt.target).val() + '</option>');
            $(this).parents('.ques-title').find('.chosen-select').val($(evt.target).val());
            $(this).parents('.ques-title').find('.chosen-select').trigger('chosen:updated');

        }

    });
</script>
<style>
    .no-results {
        display: none!important;
    }
    .chosen-single {
        background :none!important;
        border-radius:0px !important;
        height: 34px!important;
    }
</style>






<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script type="text/javascript">
    // Add validator For Location
    $.formUtils.addValidator({
        name: 'questionType',
        validatorFunction: function (value, $el, config, language, $form) {      
            var hasNoValue;
            $('.questionType').each(function(i) {
                if ($(this).val() == '') {
                      hasNoValue = true;
                }
            });
            if (hasNoValue) {
                return false;
            }
            else {
                return true;
            }
            //return parseInt(value, 10) % 2 === 0;
        },
        errorMessage : 'All question type must be selected'
    });
    
    // Add validator For Location
    $.formUtils.addValidator({
        name: 'questionTitle',
        validatorFunction: function (value, $el, config, language, $form) {      
            var hasNoValue;
            $('.questionTitle').each(function(i) {
                if ($(this).val() == '') {
                      hasNoValue = true;
                }
            });
            if (hasNoValue) {
                return false;
            }
            else {
                return true;
            }
            //return parseInt(value, 10) % 2 === 0;
        },
        errorMessage : 'All question titles must be field'
    });

    // Add validator For Qualification
    $.formUtils.addValidator({
        name: 'questionFile',
        validatorFunction: function (value, $el, config, language, $form) {
            var hasNoValue;
            var fileTypeValid=0;
            var fileType;
            var fileCount=0;
            $('.questionFileName').each(function(i) {
                fileCount++;
                fileType = (document.getElementsByName("question["+i+"][type]")[0].value);
                if ($(this).val() == '') {
                      hasNoValue = true;
                }
                else {
                    var fname = $(this).val();
                    if(fileType=='1') {
                        var video = /(\.mp4)$/i;
                        if (video.test(fname))
                        fileTypeValid++;
                    }                    
                    else if(fileType=='2') {
                        var audio = /(\.mp3)$/i;
                        if (audio.test(fname))
                        fileTypeValid++;
                    }
                }
            });
            if (hasNoValue==true || fileTypeValid!=fileCount) {
                return false;
            }
            else {
                return true;
            }
        },
        errorMessage : 'Please upload mp4 file for video set or mp3 file for audio set.'
    });

    // Add validator For Interview Assessment
    $.formUtils.addValidator({
        name: 'questionDuration',
        validatorFunction: function (value, $el, config, language, $form) {      
            var hasNoValue;
            var durationExceed;
            $('.questionDuration').each(function(i) {
                if ($(this).val() == '') {
                      hasNoValue = true;
                }
                else {
                    if (parseInt($(this).val())>600 || isNaN($(this).val())) {
                        durationExceed = true;
                    }
                }
            });
            if (hasNoValue==true || durationExceed==true) {
                return false;
            }
            else {
                return true;
            }
            //return parseInt(value, 10) % 2 === 0;
        },
        errorMessage : 'All duration (limit 600 sec) fields must be field '
    });

    $.validate({
        form: '#addQuestionForm',
        modules: 'file',
        onError: function ($form) {
            //alert($('.level1').val());
            return false;
        },
        onSuccess: function ($form) {
            return true;
        },
        validateOnBlur : false,
        errorMessagePosition : 'top',
        scrollToTopOnError : false // Set this property to true on longer forms
    });
</script>



<?php  
// loggedin user id
$userId             = getUserId(); 
$userDetForPage     = getUserById($userId);
$userRoleForPage    = $userDetForPage['role'];
$urlSeg             = $this->uri->segment(2);
//echo ($urlSeg);

if($urlSeg == 'fjlist'){ ?>
    <script>
    $(function(){
        $('#questionBank').trigger('click');
        $('.service_content').hide();
        $('#fjPaging').show();
        $('#fjPaging').css({ 'display': "block" });
    });
    </script>
<?php }  
else if($urlSeg == 'mylist') {?>
    <script>
    $(function(){
        $('#myQuestions').trigger('click');
        $('.service_content').hide();
        $('#myPaging').show();
        $('#myPaging').css({ 'display': "block" });
    });
    </script>
<?php } 
else if($urlSeg == 'list') {?>
    <script>
    $(function(){
        $('#InterviewSet').trigger('click');
        $('.service_content').hide();
        $('#setPaging').show();
        $('#setPaging').css({ 'display': "block" });
    });
    </script>
<?php }?>
    
<script>
    function getContent(getId) {
//        $.post(siteUrl + 'interviews/getContentData', {getId: getId, accessToken: token}, function (data) {
//            $('#tableDataContent').html(data);
//        });
    }
    
    
    
</script>

<div id="page-content-wrapper">
    <div class="container-fluid whitebg">
        <div class="row">
            <div class="col-md-9"><h2><img src="<?php echo base_url(); ?>/theme/firstJob/image/arrow_head.png" class="prev-page">Manage Interview</h2></div>
            <div class="col-md-3 ed_job_bt"><a href="<?php echo base_url(); ?>interviews/add" class="add-new-set"><button type="button" class="add-new-set-text">Add New Interview Set</button></a></div>
            <div class="clearfix"></div>
            <div class="col-md-12 rsp_tbl">
                <ul class="nav nav-pills padd_tabs">
                    <li class="active active_tab">
                        <a data-toggle="pill" href="#home" id="InterviewSet" >Interview Sets</a>
                    </li>
                    <?php if( $userRoleForPage != 1 ) { ?>
                    <li class="">
                        <a data-toggle="pill" href="#menu1" id="questionBank" >FJ Question Bank</a>
                    </li>
                    <?php } ?>
                    <li class="">
                        <a data-toggle="pill" href="#menu2" id="myQuestions"  >My Question Bank</a>
                    </li>
                </ul>

                <div class="tab-content" id="tableDataContent">
                    <div id="home" class="tab-pane fade active in">
                        <table class="table" id="sorting">
                            <thead class="tbl_head">
                                <tr>
                                    <th style="width: 300px;"><span>Title</span></th>
                                    <th><span>Type</span></th>
                                    <th><span>No. of Questions</span></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 0;
                                foreach ($contentInterviewSet as $item):
                                    // ci instance
                                    $ci =&get_instance();
                                    //job user answer model
                                    $ci->load->model('useranswer_model');
                                    // function to get users interview available or not
                                    $userinterviewcount = $ci->useranswer_model->countForUserInterview($item->id); 
                                    $interviewcount     = $userinterviewcount['interviewcount'];                    
                                    ?>
                                    <tr <?php if ($i % 2 != 0) echo 'class="tbl_bg"'; ?>>
                                        <td><a href="<?php echo base_url(); ?>interviews/view/<?php echo $item->id; ?>"><?php echo $item->name; ?></a></td>
                                        <td><?php echo getInterviewSetType($item->type); ?></td>
                                        <td><?php echo $item->questionCount; ?></td>
                                        <td> 
                                            <?php $checkInterview = interviewExists($item->id); ?>
                                            <a href="<?php echo base_url(); ?>interviews/edit/<?php echo $item->id; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/edit.png" class="padd_rgt"></a>
                                            <?php if( $interviewcount > 0 || $checkInterview>0 ) { ?>
                                                <a href="javascript:" class="notdelete-set" id="<?php echo $item->id; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/del.png"></a>
                                            <?php } else { ?>
                                                <a href="javascript:" class="delete-set" id="<?php echo $item->id; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/del.png"></a>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                endforeach;
                                ?>
                            </tbody>
                        </table>                        

                        <div class="service_content " id="setPaging">
                            <?php if ($totalCount > $itemsPerPage): ?>
                                <ul class="service_content_last_list">
                                    <?php if ($page > 1): ?>
                                        <p class="pagi_img"><a href="<?php echo base_url(); ?>interviews/list/<?php echo $page - 1; ?>" ><img src="<?php echo base_url(); ?>/theme/firstJob/image/pagination_img2.png"></a></p>    
                                    <?php endif; ?>
                                    <input type="hidden" name="page_no" value="1" id="page_no"> 
                                    <?php for ($i = 1; $i <= $count; $i++): ?>
                                        <li><a href="<?php echo base_url(); ?>interviews/list/<?php echo $i; ?>"  <?php if ($i == $page): ?>style="color: orange;"<?php endif; ?>><?php echo $i; ?></a></li>
                                    <?php endfor; ?>
                                    <?php if ($page < $count): ?>
                                        <p class="pagi_img"><a href="<?php echo base_url(); ?>interviews/list/<?php echo $page + 1; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/pagination_img1.png"></a></p>            
                                    <?php endif; ?>
                                </ul>
                            <?php endif; ?>
                        </div>
                    </div>
                    
                    
                    <div id="menu1" class="tab-pane fade">
                        <table class="table" id="sorting1">
                            <thead class="tbl_head">
                                <tr>
                                    <th style="width: 600px;"><span>Title</span></th>
                                    <th style="width: 100px;"><span>Type</span></th>
                                    <th style="width: 100px;"><span>Duration</span></th>
                                    <th style="width: 200px;"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 0;
                                foreach ($contentFjQuestions as $itemFj):
                                    ?>
                                    <tr <?php if ($i % 2 != 0) echo 'class="tbl_bg"'; ?>>
                                        <td><?php echo $itemFj->title; ?></td>
                                        <td>
                                            <?php if( $itemFj->type=='1' || $itemFj->type=='2') { ?>
                                                <a href="javascript:" data-toggle="modal" data-target="#viewFile" class="viewFile" data-filename="<?php echo $itemFj->file; ?>" data-filetype="<?php echo $itemFj->type; ?>"><?php echo getInterviewSetType($itemFj->type); ?></a>
                                                <!--<div id ="questionVideo<?php //echo $itemFj->id; ?>"></div>-->
                                            <?php } else { echo getInterviewSetType($itemFj->type); } ?>
                                        </td>
                                        <td><?php echo $itemFj->duration; ?> Sec.</td>
                                        <td> 
                                            <a href="javascript:" data-toggle="modal" data-target="#addQuestionToInterviewSet" class="add-question-to-interviewset" data-questionid="<?php echo $itemFj->id; ?>" data-id="<?php echo $itemFj->type; ?>" title="Click on “+” to add the selected question to a pre-existing interview set of that type."><img src="<?php echo base_url(); ?>/theme/firstJob/image/add_black.png" class="padd_rgt"></a>
                                            <a href="javascript:" class="copydata-set" data-questionid="<?php echo $itemFj->id; ?>" data-id="<?php echo $itemFj->type; ?>" title="Copy FJ question to My question bank"><img src="<?php echo base_url(); ?>/theme/firstJob/image/copyicon.png"></a>
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                endforeach;
                                ?>
                            </tbody>
                        </table>
                        
                        <div class="service_content " id="fjPaging">
                            <?php if ($totalCountFj > $itemsPerPage): ?>
                                <ul class="service_content_last_list">
                                    <?php if ($page > 1): ?>
                                        <p class="pagi_img"><a href="<?php echo base_url(); ?>interviews/fjlist/<?php echo $page - 1; ?>" ><img src="<?php echo base_url(); ?>/theme/firstJob/image/pagination_img2.png"></a></p>    
                                    <?php endif; ?>
                                    <input type="hidden" name="page_no" value="1" id="page_no"> 
                                    <?php for ($i = 1; $i <= $countFj; $i++): ?>
                                        <li><a href="<?php echo base_url(); ?>interviews/fjlist/<?php echo $i; ?>"  <?php if ($i == $page): ?>style="color: orange;"<?php endif; ?>><?php echo $i; ?></a></li>
                                    <?php endfor; ?>
                                    <?php if ($page < $countFj): ?>
                                        <p class="pagi_img"><a href="<?php echo base_url(); ?>interviews/fjlist/<?php echo $page + 1; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/pagination_img1.png"></a></p>            
                                    <?php endif; ?>
                                </ul>
                            <?php endif; ?>
                        </div>

                    </div>
                    
                    
                    <div id="menu2" class="tab-pane fade">
                        <table class="table" id="sorting2">
                            <thead class="tbl_head">
                                <tr>
                                    <th style="width: 600px;"><span>Title</span></th>
                                    <th style="width: 100px;"><span>Type</span></th>
                                    <th style="width: 100px;"><span>Duration</span></th>
                                    <th style="width: 200px;"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 0;
                                foreach ($contentMyQuestions as $itemMy):                                    
                                    // ci instance
                                    $ci =&get_instance();
                                    //job user answer model
                                    $ci->load->model('interviewquestion_model');
                                    // function to get users interview available or not
                                    $userinterviewquestioncount = $ci->interviewquestion_model->countForUserInterviewQuestion($itemMy->id); 
                                    $interviewquestioncount     = $userinterviewquestioncount['interviewquestioncount'];                                    
                                    ?>
                                    <tr <?php if ($i % 2 != 0) echo 'class="tbl_bg"'; ?>>
                                        <td><?php echo $itemMy->title; ?></td>
                                        <td>
                                            <?php if( $itemMy->type=='1' || $itemMy->type=='2') { ?>
                                                <a href="javascript:" data-toggle="modal" data-target="#viewFile" class="viewFile" data-filename="<?php echo $itemMy->file; ?>" data-filetype="<?php echo $itemMy->type; ?>"><?php echo getInterviewSetType($itemMy->type); ?></a>
                                                <!--<div id ="questionMyVideo<?php //echo $itemMy->id; ?>"></div>-->
                                            <?php } else { echo getInterviewSetType($itemMy->type); } ?>
                                        </td>
                                        <td><?php echo $itemMy->duration; ?> Sec.</td>
                                        <td>
                                            <?php $checkInterviewBank = interviewBankExists($itemMy->id); ?>
                                            <a href="<?php echo base_url(); ?>interviews/editQuestion/<?php echo $itemMy->id; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/edit.png" class="padd_rgt"></a>
                                            <a href="javascript:" data-toggle="modal" data-target="#addQuestionToInterviewSet" class="add-question-to-interviewset" data-questionid="<?php echo $itemMy->id; ?>" data-id="<?php echo $itemMy->type; ?>" title="Click here to add the selected question to a pre-existing interview set of that type"> <img src="<?php echo base_url(); ?>/theme/firstJob/image/add_black.png" class="padd_rgt"></a>
                                            
                                            
                                            <?php
                                            //  get all set id where this question attached
                                            $getAllInterviewSet = $ci->interviewquestion_model->interviewSetId($itemMy->id);                                            
                                            $interviewSetIds    = $getAllInterviewSet['setID'];
                                            
                                            
                                            //  get all jobs where these interview set attached
                                            if($interviewSetIds) {
                                                $interviewSetId     = explode(',',$interviewSetIds);
                                                $implodeSetIds      = "'" . implode("','", $interviewSetId) . "'";                                                
                                                $checkInterviewBank = interviewSetJobExists($implodeSetIds);
                                                //echo "<pre>"; print_r($checkInterviewBank ); 
                                                $jobIdCount = $checkInterviewBank[0]['jobIds']; 
                                                //echo $jobIdCount;                                                
                                            }
                                            
                                            
                                            //echo $checkInterviewBank."===".$interviewquestioncount;?>
                                            
                                            
                                            <?php 
                                            // if question attached with set and set attached with some job
                                            //if( $interviewquestioncount > 0 || $checkInterviewBank>0 ) { 
                                            if($jobIdCount>0) { ?>
                                                <a href="javascript:" class="notdelete-set" id="<?php echo $itemMy->id; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/del.png"></a>
                                            <?php } else { ?>
                                                <a href="javascript:" class="quesdelete-set" id="<?php echo $itemMy->id; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/del.png"></a>
                                            <?php } ?>
                                        </td>                                        
                                    </tr>
                                    <?php
                                    $i++;
                                endforeach;
                                ?>
                            </tbody>
                        </table>
                        
                        <div class="service_content" id="myPaging">
                            <?php if ($totalCountMy > $itemsPerPage): ?>
                                <ul class="service_content_last_list">
                                    <?php if ($page > 1): ?>
                                        <p class="pagi_img"><a href="<?php echo base_url(); ?>interviews/mylist/<?php echo $page - 1; ?>" ><img src="<?php echo base_url(); ?>/theme/firstJob/image/pagination_img2.png"></a></p>    
                                    <?php endif; ?>
                                    <input type="hidden" name="page_no" value="1" id="page_no"> 
                                    <?php for ($i = 1; $i <= $countMy; $i++): ?>
                                        <li><a href="<?php echo base_url(); ?>interviews/mylist/<?php echo $i; ?>"  <?php if ($i == $page): ?>style="color: orange;"<?php endif; ?>><?php echo $i; ?></a></li>
                                    <?php endfor; ?>
                                    <?php if ($page < $countMy): ?>
                                        <p class="pagi_img"><a href="<?php echo base_url(); ?>interviews/mylist/<?php echo $page + 1; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/pagination_img1.png"></a></p>            
                                    <?php endif; ?>
                                </ul>
                            <?php endif; ?>
                        </div>
                    </div>	
                </div>
            </div>
        </div>
    </div>
</div>




<!-- PopUp for add question to interview set  -->
<div id="addQuestionToInterviewSet" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <form class="form-horizontal form_save" action="<?php echo base_url(); ?>interviews/addToInterview" method="post" id="interviewQuestionForm" >
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Question To Interview Set</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger hidden" id="interviewQuestionAlert"></div>
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-3">Interview Sets</label>
                        <div class="col-xs-9" id="interviewSets"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary add-interview-question">Save</button>
                </div>
            </div>
        </form>
    </div>
</div>


<!-- PopUp for add question to interview set  -->
<div id="viewFile" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-group" style="height:400px;">
                    <center>
                        <div id="videoSets">
                        </div>
                    </center>
                </div>
            </div>
        </div>
    </div>
</div>    


<script src="<?php echo base_url(); ?>theme/firstJob/js/chosen.jquery.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>theme/firstJob/js/prism.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
    var config = {
        '.chosen-select': {},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }

    $(function () {
        $("#datepicker-19").datepicker();
    });
    
    window.onload = function () {
        <?php
        foreach ($contentFjQuestions as $itemFj):
            if($itemFj->type == 1) {
                $type='video';
            } else if($itemFj->type == 2) {
                $type='audio';
            } ?>
            var videoUrl = "uploads/questions/<?php echo $type; ?>/<?php echo $itemFj->file; ?>";
            <?php if($itemFj->type != 3 ) { ?>
                //jwVideoInList(videoUrl, 'questionVideo<?php echo $itemFj->id; ?>');
            <?php } ?>
        <?php
        endforeach;
            
        foreach ($contentMyQuestions as $itemMy):
            if($itemMy->type == 1) {
                $type='video';
            } else if($itemMy->type == 2) {
                $type='audio';
            } ?>
            var videoUrl = "uploads/questions/<?php echo $type; ?>/<?php echo $itemMy->file; ?>";
            <?php
            if($itemMy->type != 3 ) { ?>
                //jwVideoInList(videoUrl, 'questionMyVideo<?php echo $itemMy->id; ?>');
            <?php
            } ?>
        <?php
        endforeach; ?>
    }
    
    
    $('#questionBank').click(function(){
        <?php if( $userRoleForPage != 1 )  { ?>
                $('.add-new-set').hide();
                //$('.add-new-set-text').text('');
                $('.service_content').hide();
                $('#fjPaging').show();
        <?php } ?>
        <?php
        foreach ($contentFjQuestions as $itemFj):
            if($itemFj->type == 1) {
                $type='video';
            } else if($itemFj->type == 2) {
                $type='audio';
            } ?>
            var videoUrl = "uploads/questions/<?php echo $type; ?>/<?php echo $itemFj->file; ?>";
            <?php if($itemFj->type != 3 ) { ?>
                //jwVideoInList(videoUrl, 'questionVideo<?php echo $itemFj->id; ?>');
            <?php } ?>
        <?php
        endforeach; ?>
    });
    
    $('#InterviewSet').click(function(){
        $('.add-new-set').show();
    });
    
    $('#myQuestions').click(function(){
        $('.service_content').show();
        $('.add-new-set').show();
        $('#fjPaging').show();
        <?php
        foreach ($contentMyQuestions as $itemMy):
            if($itemMy->type == 1) {
                $type='video';
            } else if($itemMy->type == 2) {
                $type='audio';
            } ?>
            var videoUrl = "uploads/questions/<?php echo $type; ?>/<?php echo $itemMy->file; ?>";
            <?php
            if($itemMy->type != 3 ) { ?>
                //jwVideoInList(videoUrl, 'questionMyVideo<?php echo $itemMy->id; ?>');
            <?php }
        endforeach; ?>
    });
</script>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Menu Controller
 * Description : Use to handle menu items
 * @author Synergy
 * @createddate : Nov 2, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 15, 2017
 */

class Menu extends MY_Controller{
	
	/**
     * Responsable for auto load the the menu_model
     * Responsable for auto load the users moduel
     * @return void
     */
	function __construct(){
		parent::__construct();
		$this->load->model("menu_model");
		$this->load->module("users");
	}
	
	/**
     * Description : Use to display the menu items
     * Author : Synergy
     * @param none
     * @return render data into view
     */
	function index(){
		$data['current'] = $this->uri->segment(1);
		$data['items'] = $this->menu_model->read();
		$data['menus'] = $this->menu_model->pages();

		if($this->users->_is_admin()){
			$data['items'][] = $this->menu_model->menu_admin();
		}
		
		$data['currentuser'] = @$this->users->userdata();

		$this->load->view("menu", $data);
	}
	
	/**
     * Description : Use to display error page
     * Author : Synergy
     * @param none
     * @return void
     */
	function _remap(){
		show_404();
	}	
}
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo base_url(); ?>"><?php if (site_logo() != '') { ?><img src="<?php echo base_url() . '/uploads/logo/' . site_logo(); ?>"><?php
                } else {
                    echo "FirstJob";
                }
                ?></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <?php foreach ($items as $item): ?>
                    <li<?php echo ($current == $item->url) ? ' class="active"' : ''; ?>>
                        <a href="<?php echo base_url($item->url); ?>"><?php echo $item->name; ?></a>
                    </li>
                <?php endforeach; ?>
                <?php if ($currentuser): ?>
                    <?php /*<li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" title="Bootstrap Related Resources">Users <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="#"><i class="fa fa-list fa-fw"></i> Users List</a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-desktop fa-fw"></i> Users Add</a>
                            </li>
                        </ul>
                    </li>*/?>
                <?php endif; ?>
                <?php foreach ($menus as $menu): ?>
                    <li<?php echo ($current == $menu->alias) ? ' class="active"' : ''; ?>>
                        <a href="<?php echo base_url($menu->alias) . '.html'; ?>"><?php echo $menu->title; ?></a>
                    </li>
                <?php endforeach; ?>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <?php if ($currentuser): ?>
                    <li class="dropdown">
                        <a aria-expanded="false" href="#" class="dropdown-toggle" data-toggle="dropdown" title="Premium Bootstrap Themes &amp; Templates"><i class="fa fa-star text-yellow"></i><?php echo $currentuser->user_nicename; ?> <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo base_url('users/details/' . $currentuser->user_nicename); ?>"><i class="fa fa-fw fa-paint-brush"></i> View Profile</a></li>
                            <li><a href="<?php echo base_url('users/account'); ?>"><i class="fa fa-fw fa-paint-brush"></i> Edit Account</a></li>
                            <li class="divider"></li>
                            <li><a href="<?php echo base_url('users/logout'); ?>"><i class="fa fa-fw fa-paint-brush"></i> Logout</a></li>
                        </ul>
                    </li>
                <?php else: ?>
                    <li><a href="<?php echo base_url('signin'); ?>">Sign in</a></li>
                <?php endif; ?>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>







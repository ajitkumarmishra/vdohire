<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Menu Model
 * Description : Handle all the CRUD operation for Menu
 * @author Synergy
 * @createddate : Nov 1, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 21, 2017
 */

class Menu_model extends CI_Model {

    /**
     * Declaring variables
     */
    public $table_cms='pages';
    
    /**
     * Responsable for inheriting parent constructor
     * @return void
     */
    function __consruct(){
        parent::__construct();
    }

    /**
     * Unused function
     */
    function create() {
        
    }

    /**
     * Description : Use to read return empty array
     * Author : Synergy
     * @param none
     * @return array
     */
    function read() {
        $menu = array();
        return $menu;
    }
    
    /**
     * Description : Use to get all pages
     * Author : Synergy
     * @param none
     * @return array
     */
    function pages(){
        $conditions=array(
            'is_active'=>'1',
            'is_deleted'=>'0',
            'include_in'=>'header'
        );
        $this->db->where($conditions);
        $query=$this->db->get($this->table_cms);
        return $query->result();
    }

    /**
     * Description : Use to get admin menu
     * Author : Synergy
     * @param none
     * @return object
     */
    function menu_admin() {
        $menu = new stdClass();
        $menu->url = "admin";
        $menu->name = "Dashboard";
        return $menu;
    }

    /**
     * Unused function
     */
    function update() {
        
    }

    /**
     * Unused function
     */
    function delete() {
        
    }
}
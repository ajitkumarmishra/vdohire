<h1>Create New User</h1>
<div class="row">
    <div class="col-sm-12 col-md-12">
        <?php if (@$error): ?>
            <div class="alert">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <?php echo $error; ?>
            </div>
        <?php endif; ?>
        <div class="well">
            <form class="form-horizontal" method="post" action="" id="userForm">
                <div class="control-group">
                    <label class="control-label" for="user_name">Name</label>
                    <div class="controls">
                        <input type="text" class="form-control" id="user_name" value="<?php echo set_value('user_name'); ?>" name="user_name">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="user_nicename">Nick Name</label>
                    <div class="controls">
                        <input type="text" class="form-control" id="user_nicename" value="<?php echo set_value('user_nicename'); ?>" name="user_nicename">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="user_mobile">Mobile</label>
                    <div class="controls">
                        <input type="text" class="form-control" id="user_mobile" value="<?php echo set_value('user_mobile'); ?>" name="user_mobile">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="user_gender">Gender</label>
                    <div class="controls">
                        <input type="text" class="form-control" id="user_gender" value="<?php echo set_value('user_gender'); ?>" name="user_gender">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="user_email">Email</label>
                    <div class="controls">
                        <input type="text" class="form-control" id="user_email" value="<?php echo set_value('user_email'); ?>" name="user_email">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="user_pass">Password</label>
                    <div class="controls">
                        <input type="text" class="form-control" id="user_pass" value="<?php echo set_value('user_pass'); ?>" name="user_pass">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="user_cnf_pass">Confirm Password</label>
                    <div class="controls">
                        <input type="text" class="form-control" id="user_cnf_pass" value="<?php echo set_value('user_cnf_pass'); ?>" name="user_cnf_pass">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="user_role">User Role</label>
                    <div class="controls">
                        <select id="user_role" class="form-control" name="user_role">
                            <option value="">Role Type</option>
                            <?php foreach ($roles as $roles) { ?>
                                <option value="<?php echo $roles->id; ?>"><?php echo ucfirst($roles->name); ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <button type="submit" class="form-control btn btn-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

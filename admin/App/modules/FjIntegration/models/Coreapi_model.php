<?php
/*
#############################################################
    -------------------- Api Listing --------------------
    =========CORE METHODS USED IN ALL API MODELS=========
        01. cleanString
        02. getJwtToken
        03. getJwtValue
        04. codeMessage
        05. queryResultArray
        06. queryRowArray
        07. customNumberFormat
#############################################################
*/
class coreapi_model extends CI_Model {
    ############################################################################
    // Global Variable Initialization
    public static $returnArray;
    public static $data;
    ############################################################################
    #
    #
    #
    ############################################################################
    // Constructor Initialization
    function __construct() {
        parent::__construct();        
        $this->load->database();
        $this->load->library('email');
        $this->load->library('session');
        coreapi_model::$returnArray  = array();
        coreapi_model::$data         = array();
    }
    ############################################################################
    #
    #
    #
    ############################################################################
    // Filter User Input Data
    function cleanString($string) {
        $string         = trim($string);
        $detagged       = strip_tags($string);
        $stripped       = stripslashes($detagged);
        $htmlEntites    = htmlentities($stripped, ENT_QUOTES, 'UTF-8');
        return $htmlEntites;
    }
    ############################################################################
    #
    #
    #
    ############################################################################
    // Convert User ID to JWT Token
    function getJwtToken($userId) {
        $ci = & get_instance();
        $token          = array();
        $token['id']    = $userId;
        $jwtKey         = $ci->config->item('jwtKey');
        $secretKey      = base64_decode($jwtKey);
        $signatureAlgo  = $ci->config->item('signatureAlgorithm');
        return JWT::encode($token, $secretKey, $signatureAlgo);
    }
    ############################################################################
    #
    #
    #
    ############################################################################
    // Convert JWT Token To User ID
    function getJwtValue($userToken) {
        $ci =& get_instance();
        $jwtKey         = $ci->config->item('jwtKey');
        $secretKey      = base64_decode($jwtKey);
        $signatureAlgo  = $ci->config->item('signatureAlgorithm');
        try {
            $token          = JWT::decode($userToken, $secretKey, $signatureAlgo);
        } catch (Exception $e) {
            coreapi_model::codeMessage('500', $e->getMessage());
        }
        return $token->id;
    }
    ############################################################################
    #
    #
    #
    ############################################################################
    // Generate Response Message with HTTP Code
    function codeMessage($code, $message){
        coreapi_model::$returnArray['code']    = $code;
        coreapi_model::$returnArray['message'] = $message;
        return coreapi_model::$returnArray;
    }
    ############################################################################
    #
    #
    #
    ############################################################################
    // Fetch All Record From DB
    function queryResultArray($query){
        $ci =& get_instance();
        try {
            $query  = $ci->db->query($query);
            try{
                $result = $query->result_array();
                return $result;
            } catch (Exception $e) {
                    coreapi_model::codeMessage('500', $e->getMessage());
            }
        } catch (Exception $e) {
            coreapi_model::codeMessage('500', $e->getMessage());
        }
    }
    ############################################################################
    #
    #
    #
    ############################################################################
    // Fetch A Single Row From DB
    function queryRowArray($query){
        $ci =& get_instance();
        //print_r($ci);exit;
        try {
            $query  = $ci->db->query($query);
            try{
                $result = $query->row_array();
                return $result;
            } catch (Exception $e) {
                    coreapi_model::codeMessage('500', $e->getMessage());
            }
        } catch (Exception $e) {
            coreapi_model::codeMessage('500', $e->getMessage());
        }
    }
    ############################################################################
    #
    #
    #
    ############################################################################
    // Gets number and convert into given currnency format
    static function count_digit($number) {
        return strlen($number);
    }
    static function divider($number_of_digits) {
        $tens="1";
        while(($number_of_digits-1)>0) {
            $tens.="0";
            $number_of_digits--;
        }
        return $tens;
    }
    function customNumberFormat($num) {
        $ext    = "";//thousand,lac, crore
        $number_of_digits = self::count_digit($num); //this is call :)
        if($number_of_digits>3){
            if($number_of_digits%2!=0)
                $divider=self::divider($number_of_digits-1);
            else
                $divider=self::divider($number_of_digits);
        }
        else {
            $divider=1;
        }
        $fraction   = $num/$divider;
        $fraction   = number_format($fraction,2);
        if($number_of_digits==4 ||$number_of_digits==5)
            $ext="k";
        if($number_of_digits==6 ||$number_of_digits==7)
            $ext="Lakhs";
        if($number_of_digits==8 ||$number_of_digits==9)
            $ext="Cr";
        return $fraction." ".$ext;
    }
    ############################################################################
    #
    #
    #
    ############################################################################
    // User Exists Or Not
    function userExists($id) {
        $userRow    = coreapi_model::queryRowArray("SELECT id FROM fj_users WHERE id='$id' AND status='1' LIMIT 0,1");
        return $userRow;
    }
    ############################################################################
    #
    #
    #
    ############################################################################
    // Job Exists Or Not
    function jobExists($fjJobId) {
        $jobRow    = coreapi_model::queryRowArray("SELECT id, createdBy, interview, assessment, jd FROM fj_jobs WHERE fjCode='$fjJobId' AND status='1' LIMIT 0,1");
        return $jobRow;
    }
    ############################################################################
    
    
    
    function limit_text($text, $limit) {
        if (str_word_count($text, 0) > $limit) {
            $words = str_word_count($text, 2);
            $pos = array_keys($words);
            $text = substr($text, 0, $pos[$limit]) . '...';
        }
        return $text;
    }
    
    
    function array_keys_exist( array $array, $keys ) {
        $count = 0;
        if ( ! is_array( $keys ) ) {
            $keys = func_get_args();
            array_shift( $keys );
        }
        foreach ( $keys as $key ) {
            if ( array_key_exists( $key, $array ) ) {
                $count ++;
            }
        }
        return count( $keys ) === $count;
    }
}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Integrationapi Model
 * Description : Handle all the CRUD operation for Integrationapi
 * @author Synergy
 * @createddate : Nov 23, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 22, 2017
 */

class integrationapi_model extends CI_Model {
    
    /*
     *  @Author : Synergy
     *  @Params : array()
     *  @Short Description : add jobs in fj_jobs table and after adding in this table save the missing arguments in fj_job_integration table
     *  @return : array with code. 
     */
    function addJobRequisition($params){

        $jobInsertArray = array();
        $jobIntegrationArray = array();

        /**************Inilizing data*****************************************/
        //Inilizing arry with guid data
        $guidResult = $coreapi_model->queryRowArray("SELECT * FROM fj_job_integration_configuration WHERE columnName='guid'");
        
        if($guidResult['is_present'] == 1) {
            $jobInsertArray['guid'] = $guid;
        }
        //end of inilizing guid data

        //Inilizing arry with position data
        $positionResult = $coreapi_model->queryRowArray("SELECT * FROM fj_job_integration_configuration WHERE columnName='position_name'");
        
        if($positionResult['is_present'] == 1) {
            $jobInsertArray['position_name'] = $position_name;
        }
        //end of inilizing position data

        //Inilizing arry with jobcode data
        $jobCodeResult = $coreapi_model->queryRowArray("SELECT * FROM fj_job_integration_configuration WHERE columnName='job_code'");
        
        if($jobCodeResult['is_present'] == 1) {
            $jobInsertArray['fjCode'] = $job_code;
        }
        //end of inilizing jobcode data

        //Inilizing arry with jobname data
        $jobNameResult = $coreapi_model->queryRowArray("SELECT * FROM fj_job_integration_configuration WHERE columnName='job_name'");
        
        if($jobNameResult['is_present'] == 1) {
            $jobInsertArray['title'] = $job_name;
        }
        //end of inilizing jobname data

        //Inilizing arry with jobname data
        $jobDescriptionResult = $coreapi_model->queryRowArray("SELECT * FROM fj_job_integration_configuration WHERE columnName='job_description'");
        
        if($jobDescriptionResult['is_present'] == 1) {
            $jobInsertArray['description'] = $job_description;
        }
        //end of inilizing jobname data


        //Inilizing arry with jobname data
        $jobCreatedResult = $coreapi_model->queryRowArray("SELECT * FROM fj_job_integration_configuration WHERE columnName='requisition_created_date'");
        
        if($jobCreatedResult['is_present'] == 1) {
            $jobInsertArray['createdAt'] = $requisition_created_date;
        }
        //end of inilizing jobname data

        //Inilizing arry with jobname data
        $jobClosedResult = $coreapi_model->queryRowArray("SELECT * FROM fj_job_integration_configuration WHERE columnName='requisition_closed_date'");
        
        if($jobClosedResult['is_present'] == 1) {
            $jobInsertArray['openTill'] = $requisition_closed_date;
        }
        //end of inilizing jobname data

        //Inilizing arry with jobname data
        $jobStateResult = $coreapi_model->queryRowArray("SELECT * FROM fj_job_integration_configuration WHERE columnName='requisition_state'");
        
        if($jobStateResult['is_present'] == 1) {
            $jobInsertArray['state'] = $requisition_state;
        }
        //end of inilizing jobname data

        //Inilizing arry with jobname data
        $jobStatusResult = $coreapi_model->queryRowArray("SELECT * FROM fj_job_integration_configuration WHERE columnName='requisition_job_status'");
        
        if($jobStatusResult['is_present'] == 1) {
            $jobInsertArray['status'] = $requisition_job_status;
        }
        //end of inilizing jobname data

        //Inilizing arry with jobname data
        $OraganizationResult = $coreapi_model->queryRowArray("SELECT * FROM fj_job_integration_configuration WHERE columnName='organization_name'");
        
        if($OraganizationResult['is_present'] == 1) {
            $jobInsertArray['organization'] = $organization_name;
        }
        //end of inilizing jobname data

        //Inilizing arry with jobname data
        $countryResult = $coreapi_model->queryRowArray("SELECT * FROM fj_job_integration_configuration WHERE columnName='country_belong'");
        
        if($countryResult['is_present'] == 1) {
            $jobInsertArray['country'] = $country_belong;
        }
        //end of inilizing jobname data

        //Inilizing arry with jobname data
        $resionResult = $coreapi_model->queryRowArray("SELECT * FROM fj_job_integration_configuration WHERE columnName='region_belong'");
        
        if($resionResult['is_present'] == 1) {
            $jobInsertArray['region'] = $region_belong;
        }
        //end of inilizing jobname data

        //Inilizing arry with jobname data
        $cityResult = $coreapi_model->queryRowArray("SELECT * FROM fj_job_integration_configuration WHERE columnName='city_belong'");
        
        if($cityResult['is_present'] == 1) {
            $jobInsertArray['city'] = $city_belong;
        }
        //end of inilizing jobname data

        //Inilizing arry with jobname data
        $postalResult = $coreapi_model->queryRowArray("SELECT * FROM fj_job_integration_configuration WHERE columnName='postal_code_belong'");
        
        if($postalResult['is_present'] == 1) {
            $jobInsertArray['postal_code'] = $postal_code_belong;
        }
        //end of inilizing jobname data

        //Inilizing arry with jobname data
        $payGradeTypeResult = $coreapi_model->queryRowArray("SELECT * FROM fj_job_integration_configuration WHERE columnName='pay_grade_type'");
        
        if($payGradeTypeResult['is_present'] == 1) {
            $jobInsertArray['pay_grade_type'] = $pay_grade_type;
        }
        //end of inilizing jobname data

        //Inilizing arry with jobname data
        $payGradeRegionResult = $coreapi_model->queryRowArray("SELECT * FROM fj_job_integration_configuration WHERE columnName='pay_grade_region'");
        
        if($payGradeRegionResult['is_present'] == 1) {
            $jobInsertArray['pay_grade_region'] = $pay_grade_region;
        }
        //end of inilizing jobname data

        //Inilizing arry with jobname data
        $payGradeGroupResult = $coreapi_model->queryRowArray("SELECT * FROM fj_job_integration_configuration WHERE columnName='pay_grade_group'");
        
        if($payGradeGroupResult['is_present'] == 1) {
            $jobInsertArray['pay_grade_group'] = $pay_grade_group;
        }
        //end of inilizing jobname data

        //Inilizing arry with jobname data
        $payGradeLevelResult = $coreapi_model->queryRowArray("SELECT * FROM fj_job_integration_configuration WHERE columnName='pay_grade_level'");
        
        if($payGradeLevelResult['is_present'] == 1) {
            $jobInsertArray['pay_grade_level'] = $pay_grade_level;
        }
        //end of inilizing jobname data

        //Inilizing arry with jobname data
        $currencyResult = $coreapi_model->queryRowArray("SELECT * FROM fj_job_integration_configuration WHERE columnName='currency'");
        
        if($currencyResult['is_present'] == 1) {
            $jobInsertArray['currency'] = $currency;
        }
        //end of inilizing jobname data

        //Inilizing arry with jobname data
        $minSalResult = $coreapi_model->queryRowArray("SELECT * FROM fj_job_integration_configuration WHERE columnName='min_sal_pay_grade_level'");
        
        if($minSalResult['is_present'] == 1) {
            $jobInsertArray['salaryFrom'] = $min_sal_pay_grade_level;
        }
        //end of inilizing jobname data

        //Inilizing arry with jobname data
        $maxSalResult = $coreapi_model->queryRowArray("SELECT * FROM fj_job_integration_configuration WHERE columnName='max_sal_pay_grade_level'");
        
        if($maxSalResult['is_present'] == 1) {
            $jobInsertArray['salaryTo'] = $max_sal_pay_grade_level;
        }
        //end of inilizing jobname data

        //Inilizing arry with jobname data
        $salCalResult = $coreapi_model->queryRowArray("SELECT * FROM fj_job_integration_configuration WHERE columnName='sal_calculation_unit'");
        
        if($salCalResult['is_present'] == 1) {
            $jobInsertArray['sal_cal_unit'] = $sal_calculation_unit;
        }
        //end of inilizing jobname data
        $this->db->insert('fj_jobs', $jobInsertArray);
        $insert_id  = $this->db->insert_id();

        /**************end of Inilizing data*****************************************/

        /**************start of Inilizing data for  fj_job_integration***************************/
        if($guidResult['is_present'] == 0) {
            $guidInsert = array('jobId' =>$insert_id,'jobIntegrationConfigurationId' => $guidResult['id'], 'columnValue' => $guid);
            $jobIntegrationArray[] = $guidInsert;
        }

        if($positionResult['is_present'] == 0) {
            $positionInsert = array('jobId' =>$insert_id,'jobIntegrationConfigurationId' => $positionResult['id'], 'columnValue' => $position_name);
            $jobIntegrationArray[] = $positionInsert;
        }

        if($jobCodeResult['is_present'] == 0) {
            $jobCodeInsert = array('jobId' =>$insert_id,'jobIntegrationConfigurationId' => $jobCodeResult['id'], 'columnValue' => $job_code);
            $jobIntegrationArray[] = $jobCodeInsert;
        }

        if($jobNameResult['is_present'] == 0) {
            $jobNameInsert = array('jobId' =>$insert_id,'jobIntegrationConfigurationId' => $jobNameResult['id'], 'columnValue' => $job_name);
            $jobIntegrationArray[] = $jobNameInsert;
        }

        if($jobDescriptionResult['is_present'] == 0) {
            $jobDescriptionInsert = array('jobId' =>$insert_id,'jobIntegrationConfigurationId' => $jobDescriptionResult['id'], 'columnValue' => $job_description);
            $jobIntegrationArray[] = $jobDescriptionInsert;
        }

        if($jobCreatedResult['is_present'] == 0) {
            $jobCreatedInsert = array('jobId' =>$insert_id,'jobIntegrationConfigurationId' => $jobCreatedResult['id'], 'columnValue' => $requisition_created_date);
            $jobIntegrationArray[] = $jobCreatedInsert;
        }

        if($jobClosedResult['is_present'] == 0) {
            $jobClosedInsert = array('jobId' =>$insert_id,'jobIntegrationConfigurationId' => $jobClosedResult['id'], 'columnValue' => $requisition_closed_date);
            $jobIntegrationArray[] = $jobClosedInsert;
        }

        if($jobStateResult['is_present'] == 0) {
            $jobStateInsert = array('jobId' =>$insert_id,'jobIntegrationConfigurationId' => $jobStateResult['id'], 'columnValue' => $requisition_state);
            $jobIntegrationArray[] = $jobStateInsert;
        }

        if($jobStatusResult['is_present'] == 0) {
            $jobStatusInsert = array('jobId' =>$insert_id,'jobIntegrationConfigurationId' => $jobStatusResult['id'], 'columnValue' => $requisition_job_status);
            $jobIntegrationArray[] = $jobStatusInsert;
        }

        if($OraganizationResult['is_present'] == 0) {
            $OraganizationInsert = array('jobId' =>$insert_id,'jobIntegrationConfigurationId' => $OraganizationResult['id'], 'columnValue' => $organization_name);
            $jobIntegrationArray[] = $OraganizationInsert;
        }

        if($countryResult['is_present'] == 0) {
            $countryInsert = array('jobId' =>$insert_id,'jobIntegrationConfigurationId' => $countryResult['id'], 'columnValue' => $country_belong);
            $jobIntegrationArray[] = $countryInsert;
        }

        if($resionResult['is_present'] == 0) {
            $resionInsert = array('jobId' =>$insert_id,'jobIntegrationConfigurationId' => $resionResult['id'], 'columnValue' => $region_belong);
            $jobIntegrationArray[] = $resionInsert;
        }

        if($cityResult['is_present'] == 0) {
            $cityInsert = array('jobId' =>$insert_id,'jobIntegrationConfigurationId' => $cityResult['id'], 'columnValue' => $city_belong);
            $jobIntegrationArray[] = $cityInsert;
        }

        if($postalResult['is_present'] == 0) {
            $postalInsert = array('jobId' =>$insert_id,'jobIntegrationConfigurationId' => $postalResult['id'], 'columnValue' => $postal_code_belong);
            $jobIntegrationArray[] = $postalInsert;
        }

        if($payGradeTypeResult['is_present'] == 0) {
            $payGradeTypeInsert = array('jobId' =>$insert_id,'jobIntegrationConfigurationId' => $payGradeTypeResult['id'], 'columnValue' => $pay_grade_type);
            $jobIntegrationArray[] = $payGradeTypeInsert;
        }

        if($payGradeRegionResult['is_present'] == 0) {
            $payGradeRegionInsert = array('jobId' =>$insert_id,'jobIntegrationConfigurationId' => $payGradeRegionResult['id'], 'columnValue' => $pay_grade_region);
            $jobIntegrationArray[] = $payGradeRegionInsert;
        }

        if($payGradeGroupResult['is_present'] == 0) {
            $payGradeGroupInsert = array('jobId' =>$insert_id,'jobIntegrationConfigurationId' => $payGradeGroupResult['id'], 'columnValue' => $pay_grade_group);
            $jobIntegrationArray[] = $payGradeGroupInsert;
        }

        if($payGradeLevelResult['is_present'] == 0) {
            $payGradeLevelInsert = array('jobId' =>$insert_id,'jobIntegrationConfigurationId' => $payGradeLevelResult['id'], 'columnValue' => $pay_grade_level);
            $jobIntegrationArray[] = $payGradeLevelInsert;
        }

        if($currencyResult['is_present'] == 0) {
            $currencyInsert = array('jobId' =>$insert_id,'jobIntegrationConfigurationId' => $currencyResult['id'], 'columnValue' => $currency);
            $jobIntegrationArray[] = $currencyInsert;
        }

        if($minSalResult['is_present'] == 0) {
            $minSalInsert = array('jobId' =>$insert_id,'jobIntegrationConfigurationId' => $minSalResult['id'], 'columnValue' => $min_sal_pay_grade_level);
            $jobIntegrationArray[] = $minSalInsert;
        }

        if($maxSalResult['is_present'] == 0) {
            $maxSalInsert = array('jobId' =>$insert_id,'jobIntegrationConfigurationId' => $maxSalResult['id'], 'columnValue' => $max_sal_pay_grade_level);
            $jobIntegrationArray[] = $maxSalInsert;
        }

        if($salCalResult['is_present'] == 0) {
            $salCalInsert = array('jobId' =>$insert_id,'jobIntegrationConfigurationId' => $salCalResult['id'], 'columnValue' => $sal_calculation_unit);
            $jobIntegrationArray[] = $salCalInsert;
        }

        $this->db->insert_batch('fj_job_integration', $jobIntegrationArray);

        $coreapi_model->codeMessage('200', $this->lang->line('success'));
        return $coreapi_model::$returnArray;
    }

    /*
     *  @Author : Synergy
     *  @Params : deviceId, campaignName
     *  @Short Description : get top one campaign in decending order after 2 days
     *  @return : true or false. 
     */
    function getCampaign($params){
        $coreapi_model = new coreapi_model();
        $curDate = date('Y-m-d H:i:s');

        $query = $this->db->query("SELECT * FROM fj_campaign WHERE deviceId = '".$params['deviceId']."' AND campaignName = '".$params['campaignName']."' ORDER BY createdAt DESC");

        $row = $query->row();

        if ($query->num_rows() > 0) {

            $campaignDate = strtotime($row->createdAt);
            $expireDate = strtotime("+2 day", $campaignDate);

            $curDateStrtotime = strtotime($curDate);
            if($curDateStrtotime > $expireDate) {
                $coreapi_model->codeMessage('503', $this->lang->line('something_wrong'));
            } else {
                $coreapi_model->codeMessage('200', $this->lang->line('success'));
            }
        } else {
            $coreapi_model->codeMessage('503', $this->lang->line('something_wrong'));
        }
        return $coreapi_model::$returnArray;
    }

    /*
     *  @Author : Synergy
     *  @Params : deviceId, campaignName
     *  @Short Description : insert campaign
     *  @return : true or false. 
     */
    function insertCampaign($params){
        $coreapi_model = new coreapi_model();
        $curDate = date('Y-m-d H:i:s');
        
        $campaignInsert = array('deviceId' => $params['deviceId'], 'campaignName' => $params['campaignName'], 'createdAt' => $curDate);

        $result = $this->db->insert('fj_campaign', $campaignInsert);

        if($result) {
            $coreapi_model->codeMessage('200', $this->lang->line('success'));
        } else {
            $coreapi_model->codeMessage('503', $this->lang->line('something_wrong'));
        }
        return $coreapi_model::$returnArray;
    }

    /*
     *  @Author : Synergy
     *  @Params : campaignName
     *  @Short Description : get top one campaign in decending order after 2 days
     *  @return : true or false. 
     */
    function getCampaignInfo($params){
        $coreapi_model = new coreapi_model();

        $query = $this->db->query("SELECT * FROM fj_campaign_jobcode WHERE campaignName = '".$params['campaignName']."' ORDER BY createdAt DESC");

        $row = $query->row();

        if ($query->num_rows() > 0) {

            $coreapi_model->codeMessage('200', $this->lang->line('success'));
            coreapi_model::$returnArray['jobCode'] = $row->jobCode;
        } else {
            $coreapi_model->codeMessage('503', $this->lang->line('something_wrong'));
        }
        return $coreapi_model::$returnArray;
    }

    /*
     *  @Author : Synergy
     *  @Params : null
     *  @Short Description : insert display record
     *  @return : true or false. 
     */
    function addDisplayRecord(){
        $coreapi_model = new coreapi_model();
        $curDate = date('Y-m-d H:i:s');
        $ip = $_SERVER['HTTP_CLIENT_IP']?$_SERVER['HTTP_CLIENT_IP']:($_SERVER['HTTP_X_FORWARDE‌​D_FOR']?$_SERVER['HTTP_X_FORWARDED_FOR']:$_SERVER['REMOTE_ADDR']);
        
        $recordInsert = array('ip' => $ip, 'createdAt' => $curDate);

        $result = $this->db->insert('fj_displayrecord', $recordInsert);

        if($result) {
            $coreapi_model->codeMessage('200', $this->lang->line('success'));
        } else {
            $coreapi_model->codeMessage('503', $this->lang->line('something_wrong'));
        }
        return $coreapi_model::$returnArray;
    }
}
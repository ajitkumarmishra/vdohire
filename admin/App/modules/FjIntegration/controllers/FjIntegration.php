<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : FjIntegration Model
 * Description : Handle CRUD operation for FjIntegration
 * @author Synergy
 * @createddate : Jan 6, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 17, 2017
 */
class FjIntegration extends MY_Controller {
    
    /**
     * Declaring static variables
     */
    protected $token;

    /**
     * Responsable for auto load the coreapi_model, integrationapi_model
     * Responsable for loading the message library
     * Responsable for initializing the token
     * Responsable for initializing the fj, jwt, url helper
     * Responsable for auto load the email, session
     * @return void
     */
    function __construct() {
        parent::__construct();
        // Load API Model
        $this->load->model('coreapi_model',             'CORE_MODEL');
        $this->load->model('integrationapi_model','INTEGRATIONAPI_MODEL');
        // Load API Message
        $this->lang->load('fjapi_message',  'english');
        // Set Token Value
        $this->token    = 'ecbcd7eaee29848978134beeecdfbc7c';
        // Other Requirements
        $this->layout   = false; 
        $this->load->helper('fj');
        $this->load->helper('jwt');
        $this->load->helper('url');
        $this->load->library('email');
        $this->load->library('session');    
        date_default_timezone_set("Asia/Calcutta");
    }
    
    //--------------- Index Method Starts ---------------//
    function index() {
        //--------------- Variable Initilization Starts ---------------//
        // Extract JSON Data
        $json       = file_get_contents("php://input");        
        // Convert The String Of Data To An Array
        $data       = json_decode($json, true);
        $response   = array();
        $result     = array();
        //--------------- Variable Initilization Ends ---------------//
        
        //--------------- API Method List Starts ---------------//

        $methodName = !empty($data['methodname']) ? $data['methodname'] : '';
        switch ($methodName) {
            case 'addJobRequisition':
                $result = $this->INTEGRATIONAPI_MODEL->$methodName($data);
            break;
            case 'getCampaign':
                $result = $this->INTEGRATIONAPI_MODEL->$methodName($data);
            break;
            case 'insertCampaign':
                $result = $this->INTEGRATIONAPI_MODEL->$methodName($data);
            break;
            case 'getCampaignInfo':
                $result = $this->INTEGRATIONAPI_MODEL->$methodName($data);
            break;
            
            default:
                $response['code']       = '500';
                $response['message']    = $this->lang->line('default_invalid');
        }
        //--------------- API Method List Ends ---------------//
   
        //--------------- Populate Response Data ---------------//
        if (!empty($result)) {            
            $response = $result;
        }
        echo json_encode($response); exit;
        //--------------- Populate Response Data ---------------//
    }

    /*
     *  @Author : Synergy
     *  @Params : null
     *  @Short Description : insert add display record
     *  @return : true or false. 
     */
    function addDisplayRecord() {
    	$result = $this->INTEGRATIONAPI_MODEL->addDisplayRecord();
    }

    /*
     *  @Author : Synergy
     *  @Params : null
     *  @Short Description : insert add display record
     *  @return : true or false. 
     */
    function testPushNotification() {
    	$data = array(
            'message'   => 'here is a message. message',
            'title'     => 'This is a title. title',
            'subtitle'  => 'This is a subtitle. subtitle',
            'tickerText'    => 'Ticker text here...Ticker text here...Ticker text here',
            'vibrate'   => 1,
            'sound'     => 1,
            'largeIcon' => 'large_icon',
            'smallIcon' => 'small_icon'
        );

        $notification = array(
            'body' => 'great match!',
            'title' => 'Portugal vs. Denmark'
        );
    	//$target = array('APA91bH1fS8sRCpQSGS48MGDUvNzSVbMShSSVifdlui-Wl4dCKXM8ADm1iKHD_I-KYCFJ8vauDRL3o2S1Rch1jvFjDBPhQ-5P-VqkDkoR7U2ni5ar1aK_eAS2pN6f-BpMwTcu672lRwklD7F7pVRBmXjeNz5nri-ow');
        $target = array('dr1Cnidvo7k:APA91bH_q0SmNKZuTVIA35w-fLR6pY-zFzbePXZxh_KMnjUPRbHLfztCvrqyq8Lj2cIFUw17GhH-O8IPs8zEC6nDdA3IIpS9qqRsgtKLWCyNYig9pJ6svgmKfuluZTa1X3N2MGPr9OTt');
    	$result = sendAndroidPushNotification($data, $target, $notification);
    	print_r($result);exit;
    }
    //--------------- Index Method Ends ---------------//
}
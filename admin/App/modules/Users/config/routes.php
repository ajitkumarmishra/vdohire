<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['users'] = "Users/create";
$route['users/forgot_password'] = "Users/forgot_password";
$route['users/reset_password'] = "Users/reset_password";
$route['users/logout'] = "Users/logout";
$route['users/account'] = "Users/account";
$route['users/account/id/(:any)'] = "Users/account/$1";
$route['users/signin'] = "Users/create";
$route['users/signup'] = "Users/create";
$route['users/create'] = "Users/create";
$route['users/mongo'] = "Users/mongo";
$route['users/details/(:any)'] = "Users/user/$1";
$route['users/add'] = "Users/addUser";
$route['users/login'] = "Users/login";
$route['users/adminlogin'] = "Users/adminLogin";
$route['users/getValue'] = "Users/getValueByToken";
$route['users/edit/(:any)'] = "Users/editUser/$1";
$route['users/view/(:any)'] = "Users/getUser/$1";
$route['users/list/(:any)'] = "Users/listUsers/$1";
$route['users/list'] = "Users/listUsers";
$route['users/list/corporate/(:any)'] = "Users/listCorporateUsers/$1";
$route['users/list/corporate'] = "Users/listCorporateUsers";
$route['users/search'] = "Users/searchCandidate";
$route['users/delete'] = "Users/deleteUser";
$route['users/imageUpload'] = "Users/profileUpload";
$route['users/validate/(:any)'] = "Users/validateKey/$1";
$route['users/addcorporate'] = "Users/addCorporateUser";
$route['users/uploadLogo'] = "Users/companyLogoUpload";
$route['users/editcorporate/(:any)'] = "Users/editCorporateUser/$1";
$route['users/forgotPassword'] = "Users/forgotPassword";
$route['users/searchCandidate'] = "Users/searchCandidate";
$route['users/listAppUsers'] = "Users/listAppUsers";
$route['users/listAppUsers/(:any)'] = "Users/listAppUsers/$1";
$route['superadmin/dashboard'] = "Users/superAdminDashboard";
$route['superadmin/dashboard/(:any)'] = "Users/superAdminDashboard/$1";
$route['users/changePassword'] = "Users/changePassword";
$route['users/invite/(:any)'] = "Users/inviteUsers/$1";
$route['users/inviteAppUsers'] = "Users/inviteUsersThroughApp";
$route['likeDislikeUserAuditionFunction'] = "Users/likeDislikeUserAuditionFunction";
$route['events/list'] = "Users/eventList";
$route['customer/activity'] = "Users/customerActivity";
$route['users/checkTimeOut'] = "Users/checkTimeOut";
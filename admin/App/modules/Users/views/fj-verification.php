
<div class="container-fluid super_login_bg">
    <div class="container">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6 padd_login">
                <?php if (isset($msg) && $msg != NULL): ?>
                <h1>
                        <?php echo $msg; ?>
                </h1>
                <?php endif; ?>
                <?php if (isset($error) && $error != NULL): ?>
                    <div class="error-list">
                        <?php foreach ($error as $item): ?>
                            <?php echo $item; ?>
                            <?php //echo "<br/>";?>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>

                <!--                <div class="center-block links_login">
                                    <a href="#">Contact Us</a>  |  
                                    <a href="#">Terms of Use</a>  |  
                                    <a href="#">About Us</a> |  
                                    <a href="#">How to Use</a>
                
                                </div>-->

            </div>
            <div class="col-md-3"></div>
        </div>
    </div>
</div> 

<style>
    .error-list {
        color: red;
        background: white;
        padding: 10px;
        border-radius: 10px;
    }
    .error-list p {
        padding-top: 0px!important;
        color: red;
    }
</style>
<script>
  
</script>

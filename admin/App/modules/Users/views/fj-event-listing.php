<?php //echo '<pre>';
//print_r($myJobs);
$userData = $this->session->userdata['logged_in'];
$userRole = $userData->role;
?>
<div id="page-content-wrapper">
    <div class="container-fluid whitebg">
        <div class="row">
            <div class="col-md-3"><h1><img src="<?php echo base_url(); ?>/theme/firstJob/image/arrow_head.png" class="prev-page">Events List </h1></div>
            <div class="col-md-4"></div>

            <div class="clearfix"></div>

            <div class="col-md-12 rsp_tbl">
                <table class="table sortable" id="sorting">
                    <thead class="tbl_head">
                        <tr>
                            <td align="center">Action</td>
                            <td align="center"><span>Event Name</span></td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            foreach ($content as $item) { ?>
                                <tr>
                                    <td align="center"><input type="radio" class="evenCheck" name="enventId" value="<?php echo $item->id; ?>"></td>
                                    <td align="center"><?php echo $item->eventName;?></td>
                                </tr>
                        <?php } ?>
                    </tbody>
                </table>

                <?php if(count($content)>0) { ?>
                <button type="button" class="acu_bt pull-right" id="submitAddjob" style="float:left!important;">Add Job</button>
                <button type="button" class="acu_bt pull-right" id="viewActivity" style="float:left!important; margin-left: 5px;">View Activities</button>
                <button type="button" class="acu_bt pull-right" id="viewCompleteActivity" style="float:left!important; margin-left: 5px;">View Completed Interview</button>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).on('click', '#submitAddjob', function () {
        var url = '<?php echo base_url(); ?>';
        var eventId = $('input[name=enventId]:checked').val();
        if(eventId > 0) {
            var actualurl = url+'users/addJob/'+eventId;
            window.location.href = actualurl;
        } else {
            alert('Please select at least one option.');
        }
    });

    $(document).on('click', '#viewActivity', function () {
        var url = '<?php echo base_url(); ?>';
        var eventId = $('input[name=enventId]:checked').val();
        if(eventId > 0) {
            var actualurl = url+'users/viewJobActivity/'+eventId;
            window.location.href = actualurl;
        } else {
            alert('Please select at least one option.');
        }
    });

    $(document).on('click', '#viewCompleteActivity', function () {
        var url = '<?php echo base_url(); ?>';
        var eventId = $('input[name=enventId]:checked').val();
        if(eventId > 0) {
            var actualurl = url+'users/viewCompletedJob/'+eventId;
            window.location.href = actualurl;
        } else {
            alert('Please select at least one option.');
        }
    });
</script>
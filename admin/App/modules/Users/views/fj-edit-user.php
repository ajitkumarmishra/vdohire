<?php
if (isset($user) && $user != NULL) {
    //print_r($user);
    if (isset($user['name']) && $user['name'] != NULL) {
        $user['fullname'] = $user['name'];
    }
}
if (isset($user['profileImage']) && $user['profileImage'] != NULL) {
    $profileImage = $user['profileImage'];
} else {
    
    if ($user['image']) {
        $profileImage = base_url() . 'uploads/userImages/' . $user['image'];
    } else {
        $profileImage = base_url() . 'uploads/userImages/defualt.png';
    }
}
?>
<?php if (isset($message)): ?>
    <div class="alert alert-success">
        <?php echo $message; ?>
    </div>
<?php endif; ?>
<?php if (isset($error)): ?>
    <div class="alert alert-danger">
        <?php foreach ($error as $item): ?>
            <?php echo $item; ?>
        <?php endforeach; ?>
    </div>
<?php endif; ?>
<form method="post" action="" name="createUser" enctype="multipart/form-data">
    <div class="col_right_bg">
        <h3 class="edit_user"><img src="<?php echo base_url(); ?>/theme/firstJob/image/arrow_head.png" class="prev-page">Edit User</h3>
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-15">
            <span class="edit_user_img"><a href="javascript:" class="uploadImage"><input type="file" name="userImage" class="userImageUpload" id="userImage"><img src="<?php echo $profileImage; ?>" alt="" id="uploadIcon" title="" class="img-responsive"></a>
                <i class="fa fa-pencil" aria-hidden="true"></i>
                <div class="pencil"><img src="<?php echo base_url(); ?>/theme/images/edit_image_icon.png" alt="" title=""></div>
                <div id="fimageName"> </div> 
            </span>
            <input type="hidden" name="profileImage" id="profileImage" />
            <input type="hidden" name="image" id="proimageName" value="<?php
if (isset($user['image'])): echo $user['image'];
endif;
?>"  />
        </div>

        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-15">
            <div class="form-horizontal" id="form-structure">
                <div class="form-group">
                    <label class="name required-field">Name<span class="nme_dot">:</span></label>
                    <div class="form_input">
                        <input type="text"  value="<?php
if (isset($user['fullname'])): echo $user['fullname'];
endif;
?>" name="name" id="name" class="form-control">
                    </div>
                </div>

                <div class="form-group">
                    <label class="name required-field">Mobile<span class="mob_dot2">:</span></label>
                    <div class="form_input">
                        <input type="text"  value="<?php
                        if (isset($user['mobile'])): echo $user['mobile'];
                        endif;
?>" name="mobile" id="mobile" class="form-control" maxlength="10">
                    </div>
                </div>

                <div class="form-group">
                    <label class="name required-field">Email<span class="email_dot">:</span></label>
                    <div class="form_input">
                        <input type="text"  value="<?php
                        if (isset($user['email'])): echo $user['email'];
                        endif;
?>" name="email" id="email" class="form-control">
                    </div>
                </div>

                <div class="form-group">
                    <label class="name">Location<span class="loc_dot">:</span></label>
                    <div class="form_input">
                        <input type="text"  value="<?php
                        if (isset($user['location'])): echo $user['location'];
                        endif;
?>" name="location" id="location" class="form-control">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-15">
            <div class="form-horizontal" id="form-structure2">
<!--                <div class="form-group">
                    <label class="name">Role<span class="rol_dot">:</span></label>
                    <div class="form_input">
                        <select class="option_pro" name="role">
                            <option value="4" <?php
                        if (isset($user['role']) && $user['role'] == 4): echo "selected";
                        endif;
?>>Evaluator L1</option>
                            <option value="5" <?php
                            if (isset($user['role']) && $user['role'] == 5): echo "selected";
                            endif;
?>>Evaluator L2</option>
                            <option value="6" <?php
                            if (isset($user['role']) && $user['role'] == 6): echo "selected";
                            endif;
?>>None</option>
                        </select>
                    </div>
                </div>-->

                <div class="form-group">
                    <label class="name">Gender<span class="gender_dot">:</span></label>
                    <div class="form_input">
                        <select class="option_pro" name="gender">
                            <option value="1" <?php
                            if (isset($user['gender']) && $user['gender'] == 1): echo "selected";
                            endif;
?>>Male</option>
                            <option value="2" <?php
                            if (isset($user['gender']) && $user['gender'] == 2): echo "selected";
                            endif;
?>>Female</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="name">DOB<span class="dob_dot">:</span></label>
                    <div class="form_input">
                        <input type="text"  value="<?php
                            if (isset($user['dob'])):
                                echo date("m/d/Y", strtotime($user['dob']));
                            endif;
?>" name="dob" id="datepicker-13" class="form-control pick-date">
                    </div>
                </div>

                <div class="form-group">
                    <label class="name">Status<span class="status_dot">:</span></label>
                    <div class="form_input">
                        <select class="option_pro" name="status">
                            <option value="1" <?php
                        if (isset($user['status']) && $user['status'] == 1): echo "selected";
                        endif;
?>>Active</option>
                            <option value="2" <?php
                            if (isset($user['status']) && $user['status'] == 2): echo "selected";
                            endif;
?>>Inactive</option>
                        </select>
                    </div>
                </div>
<div class="form-group">
                    <label class="name"><span class="rol_dot"></span></label>
                    <div class="form_input">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content_para">
        <div class="col-lg-15 col-md-15 col-sm-15 col-xs-15">
            <h3 class="para_head">About User</h3>

            <div class="para_sec">
                <textarea class="content-text" name="aboutMe"><?php if (isset($user['aboutMe'])): echo $user['aboutMe'];endif;?></textarea>
            </div>
        </div>
    </div>
    <div class="content_form">
        <div class="col-lg-15 col-md-15 col-sm-15 col-xs-15">
            <h3 class="para_head">Company Details</h3>
            <div  class="form-horizontal" id="form-structure3">
                <div class="form-group">
                    <label class="com_name" for="inputEmail1">Company Name<span class="com_dot">:</span></label>
                    <div class="col-sm-5"><input type="text"  id="company" name="company" value="<?php
                    if (isset($user['company'])): echo $user['company'];
                    endif;
                    ?>" class="form-control"></div>
                </div>
                <div class="form-group">
                    <label class="com_name" for="inputPassword1">Designation<span class="des_dot">:</span></label>
                    <div class="col-sm-5"><input type="text"  value="<?php
                        if (isset($user['designation'])): echo $user['designation'];
                        endif;
                    ?>" name="designation" id="des" class="form-control"></div>
                </div>
                <div class="form-group">
                    <label class="com_name" for="inputPassword1">Address<span class="add_dot">:</span></label>
                    <div class="col-sm-5"><input type="text" value="<?php
                        if (isset($user['address'])): echo $user['address'];
                        endif;
                    ?>" name="address" id="des" class="form-control"></div>
                </div>
            </div>

        </div>
    </div>

    <div class="content_form">
        <?php
//echo "Test".$main_content;
//echo "<pre>";print_r($permissions);
        ?>
        <div class="col-lg-15 col-md-15 col-sm-15 col-xs-15">
            <h3 class="para_head">Permissions</h3>
            <div class="form-horizontal" id="form-structure3">
                <?php foreach ($permissions as $item): ?>
                    <div class="form-group">
                        <div class="col-sm-1"><input type="checkbox" name="permissions[]" value="<?php echo $item->id; ?>" <?php
                    if (isset($user['permissions']) && in_array($item->id, $user['permissions'])): echo "checked";
                    endif;
                    ?>></div>
                        <label class="per-name"><?php echo $item->name; ?></label>
                    </div>
                <?php endforeach; ?>

            </div>
        </div>
    </div>
    <input type="hidden" name="accessToken" value="<?php echo $token; ?>"/>
    <button type="submit" class="save_btn">Save</button>
</form>

<link href="http://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<!-- Javascript -->
<script>
    $(function () {
        $("#datepicker-13").datepicker();
        // $( "#datepicker-13" ).datepicker("show");
        $('.uploadImage').on('click', function () {
            //alert('test');
            // $('.userImageUpload').trigger('click');
            //$(':input[type="file"]').click();
        });
    });

    function readURL(input) {

        //console.log(input.files[0].name);
        var arr = [];
        arr = input.files[0].name.split('.');
        //alert(arr[1]);
        var ext = ['jpg', 'png', 'jpeg'];
        //alert($.inArray( arr[1].toString(), ext ));
        if ($.inArray(arr[1].toString(), ext) == -1)
        {
            alert('Please upload jpg or png!');
            return false;
        }
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                //alert(e.target.result);
                //$('#fimageName').append(input.files[0].name);
                $('#uploadIcon').attr('src', e.target.result);
                $('#profileImage').val(e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(".userImageUpload").change(function () {
        //alert('Test');
        readURL(this);
        var url = '<?php echo base_url(); ?>';
            var file = document.getElementById("userImage").files[0]; //fetch file
            var formData = new FormData();                     
            formData.append('userImage', file); //append file to formData object

            $.ajax({
                url: url+"users/imageUpload",
                type: "POST",
                data: formData,
                processData: false, //prevent jQuery from converting your FormData into a string
                contentType: false, //jQuery does not add a Content-Type header for you
                success: function (msg) {
                    $('#proimageName').val(msg.trim());
                }
            });
    });
    
     
</script>
<style>
    #uploadIcon {
        height: 196px;
    }
</style>

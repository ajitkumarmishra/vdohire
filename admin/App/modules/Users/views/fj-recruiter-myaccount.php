<div id="page-content-wrapper" style="margin-top: 52px;">
    <div class="container-fluid whitebg">
        <div class="row">
            <div class="col-md-12 padd_tp_bt_20">
                <div class="row">
                    <div class="col-md-12" style="margin-bottom: 20px;"><span style="margin-top: 20px; margin-bottom: 10px; font-size: 19px;">Account Details</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 padd_tp_bt_20">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#myaccountInfo">My Account Info</a></li>
                            <li><a data-toggle="tab" href="#myMedia">My Media</a></li>
                        </ul>
                        <div class="tab-content" style="margin-top: 14px;">
                            <div id="myaccountInfo" class="tab-pane fade in active">
                                <table class="table sortable" id="sorting">
                                    <thead class="tbl_head">
                                        <tr>
                                            <th class="header"><span>Information</span></th>
                                            <th class="header"><span>Value</span></th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <tr>
                                            <td>Start Date</td>
                                            <td><?php echo (!empty($accountInfo) && !empty($accountInfo->startDate)) ? $accountInfo->startDate : 'NA' ?></td>
                                        </tr>
                                        <tr>
                                            <td>Current Renewal Date</td>
                                            <td><?php echo (!empty($accountInfo) && !empty($accountInfo->currentRenewalDate)) ? $accountInfo->currentRenewalDate : 'NA' ?></td>
                                        </tr>
                                        <tr>
                                            <td>Expiry Date</td>
                                            <td><?php echo (!empty($accountInfo) && !empty($accountInfo->expiryDate)) ? $accountInfo->expiryDate : 'NA' ?></td>
                                        </tr>
                                        <tr>
                                            <td>Free Recruiters</td>
                                            <td><?php echo (!empty($accountInfo) && !empty($accountInfo->freeRecruiters)) ? $accountInfo->freeRecruiters : 'NA' ?></td>
                                        </tr>
                                        <tr>
                                            <td>Charged Recruiters</td>
                                            <td><?php echo (!empty($accountInfo) && !empty($accountInfo->chargedRecruiters)) ? $accountInfo->chargedRecruiters : 'NA' ?></td>
                                        </tr>
                                        <tr>
                                            <td>Free Applications</td>
                                            <td><?php echo (!empty($accountInfo) && !empty($accountInfo->freeApplications)) ? $accountInfo->freeApplications : 'NA' ?></td>
                                        </tr>
                                        <tr>
                                            <td>Chargeable Applications Used</td>
                                            <td><?php echo (!empty($accountInfo) && !empty($accountInfo->chargeableApplicationsUsed)) ? $accountInfo->chargeableApplicationsUsed : 'NA' ?></td>
                                        </tr>
                                        <tr>
                                            <td>RM Name</td>
                                            <td><?php echo (!empty($accountInfo) && !empty($accountInfo->rmName)) ? $accountInfo->rmName : 'NA' ?></td>
                                        </tr>
                                        <tr>
                                            <td>RM Email</td>
                                            <td><?php echo (!empty($accountInfo) && !empty($accountInfo->rmEmail)) ? $accountInfo->rmEmail : 'NA' ?></td>
                                        </tr>
                                        <tr>
                                            <td>RM Contact</td>
                                            <td><?php echo (!empty($accountInfo) && !empty($accountInfo->rmContact)) ? $accountInfo->rmContact : 'NA' ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div id="myMedia" class="tab-pane fade">
                                <form method="post" action="" name="myMediaForm" id="myMediaForm">
                                    <div class="col-md-12">
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label for="inputEmail" class="control-label col-md-1 required-field">URL : </label>
                                            <div class="col-md-5">
                                                <input type="text"  value="<?php
                                                if (!empty($mediaUrl)): echo $mediaUrl;
                                                endif;
                                                ?>" name="mediaUrl" id="mediaUrl" class="form-control" required="required" placeholder="Enter media link">
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group" style="margin-top: 5px;">
                                            <div class="col-md-3 col-md-offset-3">
                                                <button type="button" class="Save_frm" id="myaccountFormSubmit" style="background: #054f72;color: white;">Save</button>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('#myaccountFormSubmit').click(function() {
        var mediaUrl = $('#mediaUrl').val();
        var urlregex = new RegExp(
        "^(http:\/\/www.|https:\/\/www.|ftp:\/\/www.|www.){1}([0-9A-Za-z]+\.)");
        var regexValue = urlregex.test(mediaUrl);

        if(mediaUrl == '') {
            alert('Please enter media url');
        } else if(regexValue == false) {
            alert('Please enter valid media url');
        } else {
            $('#myMediaForm').submit();
        }
    });
</script>
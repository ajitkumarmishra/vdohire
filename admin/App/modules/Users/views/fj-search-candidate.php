<!-- Page Content -->
<form method="post" action="searchCandidateResult" id="searchForm">
<div id="page-content-wrapper">
    <div class="container-fluid whitebg">
        <div class="row">

            <div class="col-md-12 padd_tp_bt_20">
                <h4><img src="<?php echo base_url(); ?>/theme/firstJob/image/arrow_head.png" class="prev-page">Search Candidates</h4>
                <br>
                <div class="form-horizontal form_save">
                    <div class="form-group">
                        <label for="inputEmail" class="control-label col-xs-2">Location</label>
                        <div class="col-xs-10">
                            <select data-placeholder="Location" 
                                    style="width: 350px; display: none;" multiple="" class="chosen-select search-critera" 
                                    tabindex="-1" name="location[]">
                                <option value=""></option>
                                <?php foreach ($city as $item) : ?>
                                    <option value="<?php echo $item['id']; ?>"><?php echo $item['city'] . ', ' . $item['state'] . ', ' . $item['country']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-2">Highest Qualification</label>
                        <div class="col-xs-10">
                            <select data-placeholder="Highest Qualification" 
                                    style="width: 350px; display: none;" multiple="" class="chosen-select search-critera" 
                                    tabindex="-1" name="qualification[]">
                                <option value=""></option>
                                <?php foreach ($course as $item) : ?>
                                    <option value="<?php echo $item['id']; ?>"><?php echo $item['name']; ?></option>
                                <?php endforeach; ?>


                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-2">Year Of Completion</label>
                        <div class="col-xs-10">
                            <select data-placeholder="Year Of Completion" 
                                    style="width: 350px; display: none;" multiple="" class="chosen-select search-critera" 
                                    tabindex="-1" name="year[]">
                                <option value=""></option>
                                <?php 
                                $now = date('Y');
                                for ($i=$now; $i>1970; $i--) : ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php endfor; ?>
                            </select>
                        </div>
                    </div>

                    <div class="clearfix">

                        <div class="form-group">
                            <label for="inputPassword" class="control-label col-xs-2">Experience</label>
                            <div class="col-xs-5">
                                <select class="selectpicker search-critera expFrom" name="expFrom">
                                    <option value="">From (in Years)</option>
                                    <option value="0">00 year</option>
                                    <option value="1">01 year</option>
                                    <option value="2">02 year</option>
                                    <option value="3">03 year</option>
                                    <option value="4">04 year</option>
                                    <option value="5">05 year</option>
                                    <option value="6">06 year</option>
                                    <option value="7">07 year</option>
                                    <option value="8">08 year</option>
                                    <option value="9">09 year</option>
                                    <option value="10">10 year</option>
                                </select>
                            </div>


                            <div class="col-xs-5">
                                <select class="selectpicker search-critera expTo" name="expTo">
                                    <option value="">To (in Years)</option>
                                    <option value="1">01 year</option>
                                    <option value="2">02 year</option>
                                    <option value="3">03 year</option>
                                    <option value="4">04 year</option>
                                    <option value="5">05 year</option>
                                    <option value="6">06 year</option>
                                    <option value="7">07 year</option>
                                    <option value="8">08 year</option>
                                    <option value="9">09 year</option>
                                    <option value="10">10 year</option>
                                </select>
                            </div>
                        </div>

    
                                <div class="form-group">
                                    <label for="inputPassword" class="control-label col-xs-2"></label>
                                    <div class="col-xs-10">                                        
                                        <input type="text" data-validation="lessgreaterExp" style="height: 0px; width: 0px; visibility: hidden; " /> 
                                    </div>
                                </div>
                                

                        <div class="clearfix">

                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-2">Expected Salary (P.A)</label>
                                <div class="col-xs-5">
                                    <select class="selectpicker search-critera salFrom" name="salFrom">
                                        <option value="">From (in Lakhs)</option>
                                        <option value="0">00 lakh</option>
                                        <option value="1">01 lakh</option>
                                        <option value="2">02 lakh</option>
                                        <option value="3">03 lakh</option>
                                        <option value="4">04 lakh</option>
                                        <option value="5">05 lakh</option>
                                        <option value="6">06 lakh</option>
                                        <option value="7">07 lakh</option>
                                        <option value="8">08 lakh</option>
                                        <option value="9">09 lakh</option>
                                        <option value="10">10 lakh</option>
                                    </select>
                                </div>


                                <div class="col-xs-5">
                                    <select class="selectpicker search-critera salTo" name="salTo">
                                        <option value="">To (in Lakhs)</option>
                                        <option value="1">01 lakh</option>
                                        <option value="2">02 lakh</option>
                                        <option value="3">03 lakh</option>
                                        <option value="4">04 lakh</option>
                                        <option value="5">05 lakh</option>
                                        <option value="6">06 lakh</option>
                                        <option value="7">07 lakh</option>
                                        <option value="8">08 lakh</option>
                                        <option value="9">09 lakh</option>
                                        <option value="10">10 lakh</option>
                                    </select>
                                </div>
                            </div>


    
                                <div class="form-group">
                                    <label for="inputPassword" class="control-label col-xs-2"></label>
                                    <div class="col-xs-10">                                        
                                        <input type="text" data-validation="lessgreaterSal" style="height: 0px; width: 0px; visibility: hidden; " /> 
                                    </div>
                                </div>
                            
                            
                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-2">Gender</label>
                                <div class="col-xs-10">
                                    <select class="selectpicker search-critera" name="gender"><option value="">Select</option>
                                        <option value="1">Male</option>
                                        <option value="2">Female</option>
                                    </select>
                                </div>
                            </div>
                            
                            
                            <?php
                            $userData = $this->session->userdata['logged_in'];
                            $userRole = $userData->role;
                            if($userRole=='1') { ?>
                            <div class="form-group">
                                <label for="inputEmail" class="control-label col-xs-2">Language</label>
                                <div class="col-xs-10">
                                    <input type="text" class="form-control search-critera" placeholder="Language" name="language" id="language" maxlength="10">
                                </div>
                            </div>  

                            <div class="form-group">
                                <label for="inputEmail" class="control-label col-xs-2">Pincode</label>
                                <div class="col-xs-10">
                                    <input type="text" class="form-control search-critera" placeholder="Pincode" name="pincode" id="pincode" maxlength="6">
                                </div>
                            </div> 

                            <div class="form-group">
                                <label for="inputEmail" class="control-label col-xs-2">Year of Birth</label>
                                <div class="col-xs-10">
                                    <input type="text" class="form-control search-critera" placeholder="Year of Birth" name="yearofbirth" id="yearofbirth" maxlength="4">
                                </div>
                            </div> 
                            
                            
                            <div class="form-group">
                                <label for="inputEmail" class="control-label col-xs-2">University</label>
                                <div class="col-xs-10">
                                    <select data-placeholder="University" 
                                            style="width: 350px; display: none;" multiple="" class="chosen-select search-critera" 
                                            tabindex="-1" name="university[]">
                                        <option value=""></option>
                                        <?php foreach ($university as $item) : ?>
                                            <option value="<?php echo $item['id']; ?>"><?php echo $item['name']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-2">Aadhaar Verified</label>
                                <div class="col-xs-10">
                                    <select class="selectpicker search-critera" name="aadhaarVerified"><option value="">Select</option>
                                        <option value="1">Yes</option>
                                        <option value="2">No</option>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-2">Audition</label>
                                <div class="col-xs-10">
                                    <select class="selectpicker search-critera" name="audition"><option value="">Select</option>
                                        <option value="1">Yes</option>
                                        <option value="2">No</option>
                                    </select>
                                </div>
                            </div>
                            <?php
                            } ?>

                            <div class="col-xs-offset-2 col-xs-2 no_padding">
                                <input type="hidden" name="accessToken" value="<?php echo $token; ?>"/>
                                <button type="button" class="Save_frm candidate-search" style="margin-left: 5px;">Search</button>
                            </div>  

                            </div>

                        </div>

                    </div> 

            </div>
        </div>
    </div>
</div>
</form>
<!-- /#page-content-wrapper -->
<script src="<?php echo base_url(); ?>theme/firstJob/js/chosen.jquery.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>theme/firstJob/js/prism.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
    var config = {
        '.chosen-select': {},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>


<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script type="text/javascript">
    // Add validator For Interview Assessment
    $.formUtils.addValidator({
        name : 'lessgreaterExp',
        validatorFunction : function(value, $el, config, language, $form) {
            if($('.expFrom').val()!='' && $('.expTo').val()!=''){
                if($('.expFrom').val()<$('.expTo').val()) {
                    return true;
                }
                else {
                    return false;
                }
            }
            //return parseInt(value, 10) % 2 === 0;
        },
        errorMessage : 'Experience from value must be less than experience to value'
    });
    
    
    // Add validator For Interview Assessment
    $.formUtils.addValidator({
        name : 'lessgreaterSal',
        validatorFunction : function(value, $el, config, language, $form) {
            if($('.salFrom').val()!='' && $('.salTo').val()!=''){
                if($('.salFrom').val()<$('.salTo').val()) {
                    return true;
                }
                else {
                    return false;
                }
            }
            //return parseInt(value, 10) % 2 === 0;
        },
        errorMessage : 'Salary from value must be less than salary to value'
    });
    
    $.validate({
        form: '#searchForm',
        modules: 'file',
        onError: function ($form) {
            //alert($('.level1').val());
            return false;
        },
        onSuccess: function ($form) {
            return true;
        },
    });
</script>
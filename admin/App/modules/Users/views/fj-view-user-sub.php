<?php
if (isset($user) && $user != NULL) {
    //print_r($user);
    if (isset($user['name']) && $user['name'] != NULL) {
        $user['fullname'] = $user['name'];
    }
}
if (isset($user['profileImage']) && $user['profileImage'] != NULL) {
    $profileImage = $user['profileImage'];
} else {

    if ($user['image']) {
        $profileImage = base_url() . 'uploads/userImages/' . $user['image'];
    } else {
        $profileImage = base_url() . 'theme/firstJob/image/default.png';
    }
}

$userData = $this->session->userdata['logged_in'];

?>

<div id="page-content-wrapper" style="margin-top: 52px;">
    <form method="post" action="" name="createUser" enctype="multipart/form-data">
        <?php if (isset($message)): ?>
            <div class="alert alert-success">
                <?php echo $message; ?>
            </div>
        <?php endif; ?>
        <?php if (isset($error)): ?>
            <div class="alert alert-danger">
                <?php foreach ($error as $item): ?>
                    <?php echo $item; ?>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
        <div class="container-fluid whitebg">
            <div class="row">
                <div class="col-md-12">
                    <h5><a href="<?php echo base_url().'users/list' ?>">Recruiters</a> -> View Recruiter</h5>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12" style="margin-bottom: 8px;">
                    <div style="float:left"><h3 style="font-size: 20px !important;">Recruiter Details</h3></div>
                    <div style="padding-bottom:20px;"><a href="<?php echo base_url(); ?>users/edit/<?php echo $user['id']; ?>"><button type="button" class="Save_frm pull-right" style="background: #054f72;color: white;">Edit Recruiter</button></a></div>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-12 content_para">


                    <div class="col-md-2 ed_gry_bg image-white-bg">
                        <span class="edit_user_img"><a href="javascript:" class="uploadImage"><input type="file" name="userImage" class="userImageUpload" id="userImage"><img src="<?php echo $profileImage; ?>" alt="" id="uploadIcon" title="" class="img-responsive"></a>
                        <div id="fimageName"> </div> 
                        </span>
                        <input type="hidden" name="profileImage" id="profileImage" />
                        <input type="hidden" name="image" id="proimageName" value="<?php
                        if (isset($user['image'])): echo $user['image'];
                        endif;
                        ?>"  />
                    </div>

                    <div class="col-md-5 padd_35_btm editfrm">

                        <div class="form-group">
                            <label for="inputEmail" class="control-label col-xs-4">Name : </label>
                            <div class="col-xs-8">
                                <input type="label"  value="<?php
                                if (isset($user['fullname'])): echo $user['fullname'];
                                endif;
                                ?>" name="name" id="name" class="form-control12">
                                
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="inputEmail" class="control-label col-xs-4">Mobile : </label>
                            <div class="col-xs-8">
                                <input type="text"  value="<?php
                                       if (isset($user['mobile'])): echo $user['mobile'];
                                       endif;
                                       ?>" name="mobile" id="mobile" class="form-control12" maxlength="10">
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="inputEmail" class="control-label col-xs-4">Email : </label>
                            <div class="col-xs-8">
                                <input type="text"  value="<?php
                                       if (isset($user['email'])): echo $user['email'];
                                       endif;
                                       ?>" name="email" id="email" class="form-control12">
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="inputEmail" class="control-label col-xs-4">location : </label>
                            <div class="col-xs-8">
                                <input type="text"  value="<?php
                                       if (isset($user['location'])): echo $user['location'];
                                       endif;
                                       ?>" name="location" id="location" class="form-control12">
                            </div>
                        </div>

                    </div>


                    <div class="col-md-5 padd_35_btm editfrm">

                        <div class="form-group">
                            <label for="inputEmail" class="control-label col-xs-4">Gender : </label>
                            <div class="col-xs-8">
                                <div class="selectpicker" name="gender" disabled="">
                                    <?php if (isset($user['gender']) && $user['gender'] == 1):
                                    echo "Male";
                                    else:
                                        echo "Female";
                                     endif;?>
                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="inputEmail" class="control-label col-xs-4">DOB : </label>
                            <div class="col-xs-8">
                                <input type="text"  value="<?php echo (!empty($user['dob']) && ($user['dob'] != NULL) && ($user['dob'] != '0000-00-00')) ? date('m-d-Y', strtotime($user['dob'])) : 'NA'; ?>" name="dob" id="" class="pick-date">
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="inputEmail" class="control-label col-xs-4">Status : </label>
                            <div class="col-xs-8">

                                <div class="selectpicker" name="status">
                                    <?php if (isset($user['status']) && $user['status'] == 1):
                                    echo "Active";
                                    else:
                                        echo "Inactive";
                                     endif;?>
                                </div>
                            </div>
                        </div> 
                    </div>

                    <div class="clearfix"></div>
                    <div class="">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-10">
                                <label for="inputEmail" class="control-label col-xs-2" style="margin-top: 9px;"">About Recruiter : </label>
                                <div class="col-xs-10 ed_gry_bg" style="padding-left: 10px;">  
                                    <textarea class="content-text ed_area required-field" name="aboutMe" rows="3" cols="30"><?php
                                        if (isset($user['aboutMe'])): echo $user['aboutMe'];
                                        endif;
                                        ?></textarea>
                                </div>

                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="col-md-12 content_para">
                    <div class="col-md-2">
                    </div>
                    <div class="col-md-5 padd_35_btm editfrm ed_gry_bg">
                        <h3 class="para_head" style="font-size: 21px;">Company Details</h3>
                        <div class="form-group">
                            <label for="inputEmail" class="control-label col-xs-5">Designation : </label>
                            <div class="col-xs-7">
                                <input type="text"  value="<?php
                                       if (isset($user['designation'])): echo $user['designation'];
                                       endif;
                                       ?>" name="designation" id="des" class="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail" class="control-label col-xs-5">Company Name : </label>
                            <div class="col-xs-7">
                                <input type="text"  id="name" name="company" value="<?php
                                if (isset($user['company'])): echo $user['company'];
                                endif;
                                ?>" class="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputEmail" class="control-label col-xs-5">Address : </label>
                            <div class="col-xs-7">
                                <input type="text" value="<?php
                                       if (isset($user['address'])): echo $user['address'];
                                       endif;
                                       ?>" name="address" id="des" class="">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5 ed_gry_bg">
                        <h3 class="para_head" style="font-size: 21px;">Permissions</h3>
                        <div class="form-horizontal edit_chk">

                            <?php foreach ($permissions as $item):
                                if ($item->id == '7' && $userData->role != '1') {
                                    continue;
                                } else {?>
                                    <div class="col-md-5">
                                        <label class="checkbox-inline" style="margin-right: 70px;">
                                            <div class="form-group">
                                                <input type="checkbox" name="permissions[]" value="<?php echo $item->id; ?>"
                                                    <?php
                                                        if (isset($user['permissions']) && in_array($item->id, $user['permissions'])): echo "checked";
                                                        endif;
                                                    ?>
                                                >
                                                <label class="per-name"><?php echo $item->name; ?></label>
                                            </div>
                                        </label>
                                    </div>
                            <?php } endforeach; ?>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>

        </div>

    </form>

</div>

<link href="http://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<!-- Javascript -->
<script>
    $(function () {
        $("#datepicker-13").datepicker();
        // $( "#datepicker-13" ).datepicker("show");
        $('.uploadImage').on('click', function () {
            //alert('test');
            // $('.userImageUpload').trigger('click');
            //$(':input[type="file"]').click();
        });
    });

    function readURL(input) {

        //console.log(input.files[0].name);
        var arr = [];
        arr = input.files[0].name.split('.');
        //alert(arr[1]);
        var ext = ['jpg', 'png', 'jpeg'];
        //alert($.inArray( arr[1].toString(), ext ));
        if ($.inArray(arr[1].toString(), ext) == -1)
        {
            alert('Please upload jpg or png!');
            return false;
        }
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                //alert(e.target.result);
                //$('#fimageName').append(input.files[0].name);
                $('#uploadIcon').attr('src', e.target.result);
                $('#profileImage').val(e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(".userImageUpload").change(function () {
        //alert('Test');
        readURL(this);
        var url = '<?php echo base_url(); ?>';
            var file = document.getElementById("userImage").files[0]; //fetch file
            var formData = new FormData();                     
            formData.append('userImage', file); //append file to formData object

            $.ajax({
                url: url+"users/imageUpload",
                type: "POST",
                data: formData,
                processData: false, //prevent jQuery from converting your FormData into a string
                contentType: false, //jQuery does not add a Content-Type header for you
                success: function (msg) {
                    $('#proimageName').val(msg.trim());
                }
            });
    });
    
     
</script>
<style>
    #uploadIcon {
        height: 196px;
    }
    input, select, textarea {
border: none !important;
background-color: #f1f1f1 !important;
pointer-events: none;
}
.form-group {
    border-bottom: 1px solid #ccc;
}
.edit_chk .form-group {
    border-bottom: 0px;
}
.editfrm .form-group label {
    padding-top: 0px!important;
}
</style>
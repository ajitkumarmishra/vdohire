<div class="container-fluid super_login_bg">
    <div class="container">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6 padd_login">

                <img src="<?php echo base_url(); ?>/theme/image/login_logo.png" class="center-block">
                <h3 class="text-center">Forgot Password</h3>


                <form class="login_frm" method="post" action ="">
                    <input type="text" name="email" id="forgetEmail" class="center-block" placeholder="Email">
                    <input type="hidden" id= "accessToken" name ="accessToken" value="<?php echo $token; ?>"/>
                    <button type="submit" id="forgetSubmit" class="center-block">Submit</button>
                </form>

                <?php if (isset($msg) && $msg != NULL): ?>
                    <div class="msg">
                        <?php echo $msg; ?>
                    </div>
                <?php endif; ?>
                <?php if (isset($error) && $error != NULL): ?>
                    <div class="error-list">
                            <?php echo $error; ?>
                            <?php //echo "<br/>";?>
                    </div>
                <?php endif; ?>

                <!--                <div class="center-block links_login">
                                    <a href="#">Contact Us</a>  |  
                                    <a href="#">Terms of Use</a>  |  
                                    <a href="#">About Us</a> |  
                                    <a href="#">How to Use</a>
                
                                </div>-->

            </div>
            <div class="col-md-3"></div>
        </div>
    </div>
</div> 

<style>
    .error-list {
        color: red;
        background: white;
        padding: 10px;
        border-radius: 10px;
    }
    .error-list p {
        padding-top: 0px!important;
        color: red;
    }
    h3 {
        color: white;
    }
    .msg {
        color: green;
        padding: 10px;
        border-radius: 10px;
        background: white;
    }
</style>
<script>
    setTimeout(function () {
        $('.error-list').fadeOut('fast');
    }, 2000);
</script>

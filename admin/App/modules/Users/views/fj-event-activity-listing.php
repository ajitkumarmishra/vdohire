<?php //echo '<pre>';
//print_r($myJobs);
$userData = $this->session->userdata['logged_in'];
$userRole = $userData->role;
?>
<div id="page-content-wrapper">
    <div class="container-fluid whitebg">
        <div class="row">
            <div class="col-md-8"><h1><img src="<?php echo base_url(); ?>/theme/firstJob/image/arrow_head.png" class="prev-page">Recent Job Fair Activity List - Total Activities(<?php echo count($content);?>) </h1></div>
            <div class="col-md-4" style="margin-bottom: 10px; margin-top: 20px; font-weight: 500; font-size: 26px;">
                <div class="col-md-8">
                    <label style="font-weight: 500 !important;">Select Date to filter data</label>
                </div>

                <div class="col-md-4" style="font-size: 20px; margin-top: 5px;">
                    <?php
                        $begin = new DateTime('2017-06-02');
                        $todayDate = date('Y-m-d');
                        $end = new DateTime($todayDate);

                        $daterange = new DatePeriod($begin, new DateInterval('P1D'), $end);
                    ?>
                    <select>
                        <?php foreach($daterange as $date) { ?>
                            <option value="<?php echo $date->format("Y-m-d"); ?>" <?php if($_GET['date'] == $date->format("Y-m-d")){ echo 'selected="selected"';} ?>><?php echo $date->format("Y-m-d"); ?></option>
                        <?php } ?>

                        <option value="<?php echo $todayDate; ?>" <?php
                            if(empty($_GET)) {
                                echo 'selected="selected"';
                            }
                            elseif($_GET['date'] == $todayDate)
                                {
                                    echo 'selected="selected"';
                                } ?>><?php echo $todayDate; ?></option>


                    </select>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="col-md-12 rsp_tbl">
                <table class="table sortable" id="sorting">
                    <thead class="tbl_head">
                        <tr>
                            <td align="center">User Name</td>
                            <td align="center">Email</td>
                            <td align="center">Mobile</td>
                            <td align="center">Job</td>
                            <td align="center">Company</td>
                            <td align="center">Status</td>
                            <td align="center">Interview Time</td>
                            <td align="center">Interview Source</td>
                            
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        //print_r($content);exit;
                            foreach ($content as $item) { ?>
                                <tr>
                                    <td align="center"><?php echo $item->userName;?></td>
                                    <td align="center"><?php echo $item->userEmail;?></td>
                                    <td align="center"><?php echo $item->userMobile;?></td>
                                    <td align="center"><?php echo $item->title;?></td>
                                    <td align="center"><?php echo $item->company;?></td>
                                    <td align="center">
                                        <?php
                                            if(isset($item->status)) {
                                                if($item->status == 0) {
                                                    echo 'Job Detail Viewed';
                                                } elseif($item->status == 1) {
                                                    echo 'Video Interview Started';
                                                } elseif($item->status == 2) {
                                                    echo 'Video Interview Done';
                                                } elseif($item->status == 3) {
                                                    echo 'Assessment Started';
                                                } elseif($item->status == 4) {
                                                    echo 'Resume Upload';
                                                } elseif($item->status == 5) {
                                                    echo 'Resume Uploaded';
                                                } else {
                                                    echo 'Video interview started';
                                                }
                                            } else {
                                                echo 'Video interview started';
                                            }
                                        ?>
                                    </td>
                                    <td align="center"><?php echo date('d M Y H:i:s', strtotime($item->createdAt));?></td>
                                    <td align="center">
                                        <?php
                                            if(isset($item->isource)) {
                                                echo 'Android App';
                                            } else {
                                                echo 'Jobseeker Website';
                                            }
                                        ?>
                                    </td>
                                </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).on('click', '#submitAddjob', function () {
        var url = '<?php echo base_url(); ?>';
        var eventId = $('input[name=enventId]:checked').val();
        if(eventId > 0) {
            var actualurl = url+'users/addJob/'+eventId;
            window.location.href = actualurl;
        } else {
            alert('Please select at least one option.');
        }
    });

    $('select').on('change', function() {
        var eventId = '<?php echo $this->uri->segment(3);?>';
        var url = '<?php echo base_url(); ?>';
        var date = this.value;
        window.location.href = url+'users/viewJobActivity/'+eventId+'/?date='+date;
    });
</script>
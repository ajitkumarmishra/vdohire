<?php
if (isset($user) && $user != NULL) {
//echo "<pre>"; print_r($user);
}
?>
<form method="post" action="" name="frmCorporate" enctype="multipart/form-data" id="corporateForm">
    <div id="page-content-wrapper">
        <?php if (isset($message)): ?>
            <div class="alert alert-success">
                <?php echo $message; ?>
            </div>
        <?php endif; ?>
        <?php if (isset($error)): ?>
            <div class="alert alert-danger">
                <?php foreach ($error as $item): ?>
                    <?php echo $item; ?>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
        <div class="container-fluid whitebg">
            <div class="row">

                <div class="col-md-12 padd_tp_bt_20">
                    <h4><img src="<?php echo base_url(); ?>/theme/firstJob/image/arrow_head.png" class="prev-page"> Edit Corporate</h4>
                    <div class="form-horizontal form_save">
                        <div class="form-group">
                            <label for="inputEmail" class="control-label col-xs-2 required-field ">Company Name</label>
                            <div class="col-xs-10">
                                <input type="text" class="form-control" value="<?php
                                if (isset($user['company'])): echo $user['company'];
                                endif;
                                ?>" placeholder="Company Name" name="company" data-validation="required" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail" class="control-label col-xs-2 required-field ">Company Code</label>
                            <div class="col-xs-10">
                                <input type="text" class="form-control" value="<?php
                                       if (isset($user['companyCode'])): echo $user['companyCode'];
                                       endif;
                                       ?>" placeholder="Company Code" name="companyCode" data-validation="required" >
                            </div>
                        </div>
                        <!--<div class="form-group">
                            <label for="inputPassword" class="control-label col-xs-2">Company Id</label>
                            <div class="col-xs-10">
                                <input type="text" class="form-control" placeholder="company id">
                            </div>
                        </div>-->

                        <div class="clearfix">

                            <!--                        <div class="form-group">
                                                        <label for="inputPassword" class="control-label col-xs-2">Password</label>
                                                        <div class="col-xs-4">
                                                            <input type="text" class="form-control" placeholder="password">
                                                        </div>
                            
                                                        <label for="inputPassword" class="control-label col-xs-1">Confirm:</label>
                                                        <div class="col-xs-5">
                                                            <input type="text" class="form-control" placeholder="confirm">
                                                        </div>
                                                    </div>-->

                            <div class="clearfix">


                                <div class="form-group">
                                    <label for="inputPassword" class="control-label col-xs-2 required-field" >Address Line1</label>
                                    <div class="col-xs-10">
                                        <!--<textarea rows="5" style="width:100%" placeholder="address line1" name="addressLine1" required></textarea>-->
                                        <input type="text" class="form-control" placeholder="Address Line 1" name="addressLine1" value="<?php
                                        if (isset($user['addressLine1'])): echo $user['addressLine1'];
                                        endif;
                                        ?>" data-validation="required" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword" class="control-label col-xs-2" >Address Line2</label>
                                    <div class="col-xs-10">
                                        <input type="text" class="form-control" placeholder="Address Line 2" name="addressLine2" value="<?php
                                        if (isset($user['addressLine2'])): echo $user['addressLine2'];
                                        endif;
                                        ?>" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword" class="control-label col-xs-2">City</label>
                                    <div class="col-xs-10">
                                        <select class="selectpicker" name="city" id="cityDropDown" data-validation="required">
                                            <option value=""> Please select city</option>
                                            <?php foreach ($cities as $item): ?>
                                                <option value="<?php echo $item['city'] ?>"<?php if ($item['city'] == $user['city']) echo "selected"; ?>><?php echo $item['city'] ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword" class="control-label col-xs-2">State</label>
                                    <div class="col-xs-10" id="stateDropDown">
                                        <select class="selectpicker" name="state" id="state" data-validation="required">
                                            <option value="0"> Please select state</option>
                                            <?php foreach ($states as $item): ?>
                                                <option value="<?php echo $item['state'] ?>" <?php if ($item['state'] == $user['state']) echo "selected"; ?>><?php echo $item['state'] ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword" class="control-label col-xs-2">Country</label>
                                    <div class="col-xs-10">
                                        <select class="selectpicker" name="country" data-validation="required"><option value="India">India</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword" class="control-label col-xs-2">Pin Code</label>
                                    <div class="col-xs-10">
                                        <input type="text" class="form-control" placeholder="Pin Code" name="pinCode" value="<?php
                                        if (isset($user['pinCode'])): echo $user['pinCode'];
                                        endif;
                                        ?>" data-validation="number required">
                                    </div>
                                </div>

                                <div class="clearfix">

                                    <div class="form-group">
                                        <label for="inputPassword" class="control-label col-xs-2">Phone</label>
                                        <div class="col-xs-4">
                                            <input type="text" class="form-control" placeholder="Phone" name="phone" value="<?php
                                            if (isset($user['phone'])): echo $user['phone'];
                                            endif;
                                            ?>" >
                                        </div>

                                        <label for="inputPassword" class="control-label col-xs-1">Fax</label>
                                        <div class="col-xs-5">
                                            <input type="text" class="form-control" placeholder="Fax" name="fax" value="<?php
                                            if (isset($user['fax'])): echo $user['fax'];
                                            endif;
                                            ?>" >
                                        </div>
                                    </div>

                                    <div class="clearfix">

                                        <div class="form-group">
                                            <label for="inputPassword" class="control-label col-xs-2 required-field" >E-mail:</label>
                                            <div class="col-xs-4">
                                                <input type="email" class="form-control" placeholder="Email" name="email" value="<?php
                                                if (isset($user['email'])): echo $user['email'];
                                                endif;
                                                ?>" data-validation="required" >
                                            </div>

                                            <label for="inputPassword" class="control-label col-xs-1 required-field">Web:</label>
                                            <div class="col-xs-5">
                                                <input type="text" class="form-control" placeholder="Web" name="web" value="<?php
                                                if (isset($user['web'])): echo $user['web'];
                                                endif;
                                                ?>"  data-validation="required">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="inputPassword" class="control-label col-xs-2">Company Logo</label>
                                            <div class="col-xs-4">
                                                <input type="file" class="form-control" name="companylogo" id="companylogo">
                                            </div>
                                            <div class="col-xs-2">
                                                <button type="button" class="upload_bt" id="uploadCompanyLogo">Click To Upload</button>
                                            </div>
                                            <img src="<?php
                                            if (isset($user['companyLogo']) && $user['companyLogo'] != NULL) {
                                                echo base_url() . "uploads/corporateLogo/" . $user['companyLogo'] . '"';
                                            } else {
                                                if (isset($user['coprorateImage']) && $user['coprorateImage'] != NULL) {
                                                    echo $user['coprorateImage'];
                                                }
                                            }
                                            ?>" id=<?php
                                                 if (isset($user['companyLogo']) && $user['companyLogo'] != NULL) {
                                                     echo "coprorateImageName";
                                                 } else {
                                                     if (isset($user['coprorateImage']) && $user['coprorateImage'] != NULL) {
                                                         echo "coprorateImageName";
                                                     } else {
                                                         echo "uploadImageName";
                                                     }
                                                 }
                                                 ?> />
                                        </div>

                                        <div class="form-group">
                                            <label for="inputPassword" class="control-label col-xs-2">Industry</label>
                                            <div class="col-xs-10">
                                                <select class="selectpicker" name="industry" data-validation="number required">
                                                    <?php foreach ($industries as $item): ?>
                                                        <option value="<?php echo $item['id']; ?>"><?php echo $item['name']; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="inputPassword" class="control-label col-xs-2">About Company</label>
                                            <div class="col-xs-10">
                                                <textarea rows="5" style="width:100%" placeholder="About Company" name="aboutCompany"><?php
                                                    if (isset($user['aboutCompany'])): echo $user['aboutCompany'];
                                                    endif;
                                                    ?></textarea>
                                            </div>
                                        </div>

                                        <div class="clearfix">

                                            <div class="form-group">
                                                <label for="inputPassword" class="control-label col-xs-2">User Type</label>
                                                <div class="col-xs-10">
                                                    <select class="selectpicker" name="typeFlag">
                                                        <option value="A" <?php if ($user['typeFlag'] == 'A') echo "selected"; ?>>A</option>
                                                        <option value="B" <?php if ($user['typeFlag'] == 'B') echo "selected"; ?>>B</option>
                                                        <option value="C" <?php if ($user['typeFlag'] == 'C') echo "selected"; ?>>C</option>
                                                        <option value="D" <?php if ($user['typeFlag'] == 'D') echo "selected"; ?>>D</option>
                                                        <option value="E" <?php if ($user['typeFlag'] == 'E') echo "selected"; ?>>E</option>
                                                        <option value="F" <?php if ($user['typeFlag'] == 'F') echo "selected"; ?>>F</option>
                                                        <option value="G" <?php if ($user['typeFlag'] == 'G') echo "selected"; ?>>G</option>
                                                        <option value="H" <?php if ($user['typeFlag'] == 'H') echo "selected"; ?>>H</option>
                                                        <option value="I" <?php if ($user['typeFlag'] == 'I') echo "selected"; ?>>I</option>
                                                        <option value="J">J</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="inputPassword" class="control-label col-xs-2">Valid Through</label>
                                                <div class="col-xs-5">
                                                    <div class="input-group input-append date">
                                                        <input type="text" class="form-control" name="validFrom" id="datepicker-13" value="<?php
                                                        if (isset($user['validFrom'])): echo $user['validFrom'];
                                                        endif;
                                                        ?>" data-validation="required" />
                                                        <span class="input-group-addon add-on">
                                                            <span class="glyphicon glyphicon-calendar" id="datepick1"></span></span>
                                                    </div>
                                                </div>


                                                <div class="col-xs-5">
                                                    <div class="input-group input-append date">
                                                        <input type="text" class="form-control" name="validTo" id="datepicker-14" value="<?php
                                                        if (isset($user['validTo'])): echo $user['validTo'];
                                                        endif;
                                                        ?>" data-validation="required" />
                                                        <span class="input-group-addon add-on">
                                                            <span class="glyphicon glyphicon-calendar" id="datepick2"></span></span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="inputPassword" class="control-label col-xs-2">Status</label>
                                                <div class="col-xs-10">
                                                    <select class="selectpicker" name="statusFlag">
                                                        <option value="1" <?php if ($user['statusFlag'] == 1) echo "selected"; ?>>1</option>
                                                        <option value="2" <?php if ($user['statusFlag'] == 2) echo "selected"; ?>>2</option>
                                                        <option value="3" <?php if ($user['statusFlag'] == 3) echo "selected"; ?>>3</option>
                                                        <option value="4" <?php if ($user['statusFlag'] == 4) echo "selected"; ?>>4</option>
                                                        <option value="5" <?php if ($user['statusFlag'] == 5) echo "selected"; ?>>5</option>
                                                        <option value="6" <?php if ($user['statusFlag'] == 6) echo "selected"; ?>>6</option>
                                                        <option value="7" <?php if ($user['statusFlag'] == 7) echo "selected"; ?>>7</option>
                                                        <option value="8" <?php if ($user['statusFlag'] == 8) echo "selected"; ?>>8</option>
                                                        <option value="9" <?php if ($user['statusFlag'] == 9) echo "selected"; ?>>9</option>
                                                        <option value="10" <?php if ($user['statusFlag'] == 10) echo "selected"; ?>>10</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="inputPassword" class="control-label col-xs-2">Maximum User Limit</label>
                                                <div class="col-xs-10">
                                                    <input type="text" class="form-control" placeholder="User Limit" name="userLimit" value="<?php
                                                    if (isset($user['userLimit'])): echo $user['userLimit'];
                                                    endif;
                                                    ?>" data-validation="number required" maxlength="3" />
                                                </div>
                                            </div>

                                            <input type="hidden" name="accessToken" value="<?php echo $token; ?>" />
                                            <input type="hidden" name="coprorateLogoName" id="coprorateLogoName" value="<?php
                                            if (isset($coprorateLogoName) && $coprorateLogoName != NULL) {
                                                echo $coprorateLogoName;
                                            }
                                            ?>" />
                                            <input type="hidden" name="coprorateImage" id="coprorateImage" value="<?php
                                            if (isset($coprorateImage) && $coprorateImage != NULL) {
                                                echo $coprorateImage;
                                            }
                                            ?>" />

                                            <div class="col-xs-offset-2 col-xs-2 no_padding">
                                                <button type="submit" class="Save_frm">SAVE</button>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /#page-content-wrapper -->
                        </div></div>
                </div></div></div></div>


</form>
<script>
    $(function () {
        var url = '<?php echo base_url(); ?>';
        window.siteUrl = url;
        window.token = '<?php echo $token; ?>';
    });
</script>
<style>
    .ui-datepicker-year {
        background: #f9a727;
    }    
</style>

<script src="<?php echo base_url(); ?>theme/firstJob/js/validate.js"></script>
<script>

$.validate({
    form: '#corporateForm',
    modules: 'file',
    onError: function ($form) {
        //alert($('.setquestion').val());
        //alert($('.setInterviewType').html());
        return false;
    },
    onSuccess: function ($form) {
        return true;
    },
    validateOnBlur : false,
    errorMessagePosition : 'top',
    scrollToTopOnError : false // Set this property to true on longer forms
});
</script>
<div id="page-content-wrapper">
    <?php if (isset($message)): ?>
        <div class="alert alert-success">
            <?php echo $message; ?>
        </div>
    <?php endif; ?>
    <?php if (isset($error)): ?>
        <div class="alert alert-danger">
            <?php foreach ($error as $item): ?>
                <?php echo $item; ?>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
    <div class="container-fluid whitebg">
        <div class="row">

            <div class="col-md-12 padd_tp_bt_20">
                <h4><img src="<?php echo base_url(); ?>/theme/firstJob/image/arrow_head.png" class="prev-page"> Import Candidates</h4>
                <form class="form-horizontal form_save" action="" method="post" enctype="multipart/form-data" id="importUsersForm">
                    <div class="form-group">
                        <label for="companyName" class="control-label col-xs-2 required-field">Company Name</label>
                        <div class="col-xs-10">
                            <input type="text" class="form-control" placeholder="Company Name" value="<?php
                            if (isset($user['companyName'])): echo $user['companyName'];
                            endif;
                            ?>" name="companyName" required>
                        </div>
                    </div>


                    <div class="form-group">
                        <label for="importUsers" class="control-label col-xs-2 required-field">Select File</label>
                        <div class="col-xs-10">
                            <input type="file" class="form-control" name="importUsers" id="importUsers" required>
                        </div>
                    </div>

                    
                    <div class="col-xs-offset-2 col-xs-2 no_padding">
                        <input type="hidden" name="accessToken" value="<?php echo $token; ?>"/>
                        <button type="submit" class="Save_frm">Import</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
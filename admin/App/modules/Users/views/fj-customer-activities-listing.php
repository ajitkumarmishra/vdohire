<?php //echo '<pre>';
//print_r($myJobs);
$userData = $this->session->userdata['logged_in'];
$userRole = $userData->role;
?>
<div id="page-content-wrapper">
    <div class="container-fluid whitebg">
        <div class="row">
            <div class="col-md-3"><h1><img src="<?php echo base_url(); ?>/theme/firstJob/image/arrow_head.png" class="prev-page">Customer Activity Listing</h1></div>
            <div class="col-md-4"></div>
        </div>
        <div class="row">
            <div class="col-md-5 col-md-offset-7">
                <form class="form-inline" action="" method="post">
                    <div class="form-group" style="padding-right: 20px;">
                        <label for="email">From Date:</label>
                        <input type="text" class="form-control" id="datepicker-13" placeholder="m/d/Y" name="dateFrom" value="<?php echo (isset($_POST) && !empty($_POST['dateFrom'])) ? $_POST['dateFrom'] : '' ?>" required="">
                    </div>
                    <div class="form-group" style="padding-right: 20px;">
                        <label for="pwd">To Date:</label>
                        <input type="text" class="form-control" id="datepicker-14" placeholder="m/d/Y" name="dateTo" value="<?php echo (isset($_POST) && !empty($_POST['dateTo'])) ? $_POST['dateTo'] : '' ?>" required="">
                    </div>
                    <button type="submit" class="btn btn-default">Filter</button>
                </form>
            </div>
        </div>
        <div class="row" style="margin-top: 30px;">
            <div class="col-md-12 rsp_tbl">
                <table class="table sortable" id="sorting">
                    <thead class="tbl_head">
                        <tr>
                            <td align="center">Company Name</td>
                            <td align="center">No. Of Jobs Created</td>
                            <td align="center">No. Of Recruiters Created</td>
                            <td align="center">No. Of Invitations Sent</td>
                            <td align="center">No. Of Interviews Received</td>
                            <td align="center">No. Of Interviews Shortlisted</td>
                            <td align="center">No. Of Interviews Rejected</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            foreach ($content as $item) { ?>
                                <tr>
                                    <td align="center"><?php echo $item->company;?></td>
                                    <td align="center"><?php echo $item->noOfJobsCreated;?></td>
                                    <td align="center"><?php echo $item->noOfRecruitersCreated;?></td>
                                    <td align="center"><?php echo $item->noOfInvitations;?></td>
                                    <td align="center"><?php echo $item->noOfInterviewsReceived;?></td>
                                    <td align="center"><?php echo $item->noOfInterviewsShortlisted;?></td>
                                    <td align="center"><?php echo $item->noOfInterviewsRejected;?></td>
                                </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
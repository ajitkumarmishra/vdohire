<div id="page-content-wrapper" style="margin-top: 52px;">
    <div class="container-fluid whitebg">
        <div class="row" style="margin-top: 15px;">
            <div class="col-md-3" style="margin-bottom: 20px;"><span style="margin-top: 20px; margin-bottom: 10px; font-size: 20px;">Recruiters</span></div>
            <div class="col-md-6" style="margin-bottom: 18px;"><a href="<?php echo base_url(); ?>users/add"><button type="button" class="acu_bt pull-right" style="background: #054f72 none repeat scroll 0 0; color:white; margin-top: 0px;">Add Recruiter</button></a></div>
        </div>
        <div class="row">
            <div class="clearfix"></div>
            <div class="col-md-9 rsp_tbl">
                <table class="table sortable" id="sorting">
                    <thead class="tbl_head">
                        <tr>
                            <!--<th><input type="checkbox" class="chk_bx"></th>-->
                            <th><span>Name</span></th>
                            <th><span>Location</span></th>
                            <th><span>Mobile</span></th>
                            <th><span>Active</span></th>
                            <th><span>Actions</span></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            if(count($content) > 0) {
                                $i = 0;
                                foreach ($content as $item) {
                                    ?>
                                        <?php if ($i % 2 != 0): ?>
                                        <tr>
                                        <?php else: ?>
                                        <tr>
                                        <?php endif; ?>
                                            <!-- <td><input type="checkbox"></td>-->
                                            <td><a style="text-decoration: underline" href="<?php echo base_url(); ?>users/view/<?php echo $item->id; ?>"><?php echo $item->fullname; ?></a></td>
                                            <td><?php echo $item->location; ?></td>
                                            <td><?php echo $item->mobile; ?></td>
                                            <td>
                                                <?php
                                                //if($item->status=='1' || $item->status=='2') { ?>
                                                <a href="#" class="activeInactive" id="<?php echo encryptURLparam($item->id, URLparamKey); ?>" ><?php echo ($item->status=='1'?'Active':($item->status=='2'?'Inactive':'Deleted')); ?></a>
                                                    <?php //} else echo "Deleted"; ?>
                                            </td>
                                            <td>
                                                <a href="<?php echo base_url(); ?>users/edit/<?php echo $item->id; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/edit.png" class="padd_rgt"></a>
                                                <a href="javascript:" class="delete-user" id="<?php echo $item->id; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/del.png"></a>
                                            </td>
                                        </tr>
                                    <?php
                                    $i++;
                                }
                            } else {
                                echo '<tr><td>There is not recruiter added!</td></tr>';   
                            }
                        ?>
                    </tbody>
                </table>
                <?php echo $this->pagination->create_links(); ?>
            </div>
            <div class="col-md-3" style="font-size: 14px; background-color: #eeeeee;">
                <div class="clearfix"></div>
                <div style="margin-bottom: 27px; margin-top: 15px; font-size: 18px;">Refine Results</div>
                <form action="<?php echo base_url(); ?>users/list" method="post">
                    <div class="row">
                        <span>Filter Recruiters by</span>
                        <div class="col-md-12" style="margin-top: 8px;">
                            <div class="form-group">
                                <label style="font-weight: lighter;">Keyword </label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control" name ="serachText" placeholder="Name or Location or Mobile" value ="<?php echo $searchText; ?>"
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group">
                                <div class="row">
                                    <input type="hidden" id= "accessToken" name ="accessToken" value="<?php echo $token; ?>" >
                                    <button type="submit" name="submit" value="submit" class="btn btn-default col-md-4" style="color : #fff; background-color: #054F72; margin-left: 38px; margin-top: 45px;">Apply</button>
                                    <button type="submit" name="reset" value="reset" class="btn btn-default col-md-4" style="color : #fff; background-color: #054F72; margin-left: 29px; margin-top: 45px;">Reset</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<style>
    .service_content ul.service_content_last_list li{ display:inline-block; font-size:20px; width:38px; height:38px; line-height:38px; border:0px; }
    .service_content ul.service_content_last_list li a{ color:#333333; padding:0px; text-align:center; display:block; border:0px;  }
    .service_content ul.service_content_last_list li a:hover{ color:#fff; border-radius:50px; -moz-border-radius:50px; -webkit-border-radius:50px; background:#0072dc }
    .response_img img{ max-width: 100%; }
    .service_content .service_content_last_list{ text-align:center; padding:10px 0 70px; }
    ul.service_content_last_list  li.pagination-last-list{padding:0 20px 0 0px; cursor:pointer; text-indent:-9999px; margin-left:5px;  }
    .pagi_img{display: -webkit-inline-box;}
    .pagi_img a img{display: -webkit-inline-box;}
    .service_content ul.service_content_last_list li{margin:0px!important;}
    h1{font-size:32px!important;}
    .acu_bt a {
        text-decoration: none;
        color: #fff;
    }
    .sel_opt {
        font-size:15px;
        width:33% !important;
        padding-left: 9px !important;
    }
    #sorting td a {color: #303030!important; }
    #sorting td a:hover {color: #777!important; }
    
</style>
<!--<script src="<?php echo base_url(); ?>theme/firstJob/js/tablesort.js"></script>-->
<!--<script src="<?php echo base_url(); ?>theme/firstJob/js/jquery.tablesorter.js"></script>-->
<!--<script src="https://code.jquery.com/jquery-1.10.2.js"></script>-->
<script>
    $(function () {
        var settingTableRowColor = function (tableName) {
            $('#' + tableName + ' tbody tr').removeAttr('class');
            $('#' + tableName + ' tbody tr:odd').attr('class', 'tbl_bg');
        }
        settingTableRowColor('sorting');
        $('.sort-header').on('click', function () {
            settingTableRowColor('sorting');
        });
        $('.delete-user').on('click', function () {
            var url = '<?php echo base_url(); ?>';
            var userId = $(this).attr('id');
            var accessToken = $('#accessToken').val();
            bootbox.confirm('Are you sure you want to delete?', function (result) {
                //alert(result);
                if (result) {
                    $.post(url + 'users/delete', {id: userId, accessToken: accessToken}, function (data) {
                        //bootbox.alert(data);
                        location.reload();
                    });
                }
            });

        });
    });
    //new Tablesort(document.getElementById('sorting'));
    //$("#sorting").tablesorter();
    
    
    $('.activeInactive').on('click', function () {
        var url = '<?php echo base_url(); ?>';
        var userId = $(this).attr('id');
        //alert(userId);
        bootbox.confirm('Are you sure you want to change user status?', function (result) {
            if (result) {
                $.post(url + 'users/updateUserStatus', {id: userId}, function (data) {
                    //bootbox.alert(data);
                    location.reload();
                });
            }
        });

    });
</script>
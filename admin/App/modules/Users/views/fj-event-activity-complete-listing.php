<?php //echo '<pre>';
//print_r($myJobs);
$userData = $this->session->userdata['logged_in'];
$userRole = $userData->role;
?>
<div id="page-content-wrapper">
    <div class="container-fluid whitebg">
        <div class="row">
            <div class="col-md-11"><h1><img src="<?php echo base_url(); ?>/theme/firstJob/image/arrow_head.png" class="prev-page">Recent Interview Completed For Job Fair - Total(<?php echo count($content);?>) </h1></div>

            <div class="clearfix"></div>

            <div class="col-md-12 rsp_tbl">
                <table class="table sortable" id="sorting">
                    <thead class="tbl_head">
                        <tr>
                            <td align="center">User Name</td>
                            <td align="center">Email</td>
                            <td align="center">Mobile</td>
                            <td align="center">Job</td>
                            <td align="center">Company</td>
                            <td align="center">Status</td>
                            <td align="center">Interview Time</td>
                            <td align="center">Interview Source</td>
                            
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        //print_r($content);exit;
                            foreach ($content as $item) { ?>
                                <tr>
                                    <td align="center"><?php echo $item->userName;?></td>
                                    <td align="center"><?php echo $item->userEmail;?></td>
                                    <td align="center"><?php echo $item->userMobile;?></td>
                                    <td align="center"><?php echo $item->title;?></td>
                                    <td align="center"><?php echo $item->company;?></td>
                                    <td align="center">
                                        <?php
                                            if(isset($item->status)) {
                                                if($item->status == 0) {
                                                    echo 'Job Detail Viewed';
                                                } elseif($item->status == 1) {
                                                    echo 'Video Interview Started';
                                                } elseif($item->status == 2) {
                                                    echo 'Video Interview Done';
                                                } elseif($item->status == 3) {
                                                    echo 'Assessment Started';
                                                } elseif($item->status == 4) {
                                                    echo 'Resume Upload';
                                                } elseif($item->status == 5) {
                                                    echo 'Resume Uploaded';
                                                } else {
                                                    echo 'Video interview started';
                                                }
                                            } else {
                                                echo 'Video interview Completed';
                                            }
                                        ?>
                                    </td>
                                    <td align="center"><?php echo date('d M Y H:i:s', strtotime($item->createdAt));?></td>
                                    <td align="center">
                                        <?php
                                            if($item->isource == 1) {
                                                echo 'Jobseeker Website';
                                            } else {
                                                echo 'Android App';
                                            }
                                        ?>
                                    </td>
                                </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).on('click', '#submitAddjob', function () {
        var url = '<?php echo base_url(); ?>';
        var eventId = $('input[name=enventId]:checked').val();
        if(eventId > 0) {
            var actualurl = url+'users/addJob/'+eventId;
            window.location.href = actualurl;
        } else {
            alert('Please select at least one option.');
        }
    });
</script>
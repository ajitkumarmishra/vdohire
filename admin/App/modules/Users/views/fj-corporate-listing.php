<div id="page-content-wrapper">
    <div class="container-fluid whitebg">
        <div class="row">
            <div class="col-md-4"><h1><img src="<?php echo base_url(); ?>/theme/firstJob/image/arrow_head.png" class="prev-page">Manage Corporate</h1></div>
            <div class="col-md-3"><a href="<?php echo base_url(); ?>users/addcorporate"><button type="button" class="acu_bt pull-right">Add Corporate</button></a></div>
            <form action="" method="post">
                <div class="col-md-5">
<!--                    <select class="sel_opt" id="searchDropDown" name ="serachColumn" style="width: 40% !important;">-->
                    <select class="sel_opt" id="" name ="serachColumn" style="width: 40% !important;">
                        <option value="company" class="dropOption" <?php if ($serachColumn == 'company') echo "selected"; ?>>Search By Corporate</option>
                        <option value="location" <?php if ($serachColumn == 'location') echo "selected"; ?>>Search By Location</option>
                        <option value="mobile" <?php if ($serachColumn == 'mobile') echo "selected"; ?>>Search By Mobile</option>
                    </select>
                    <?php if ($searchText): ?>
                        <div class="cross-search">X</div>
                    <?php endif; ?>
                    <input type="text" class="search_box" name ="serachText" placeholder="" value ="<?php print_r($this->session->userdata('searchText')); ?>">
                    <input type="hidden" id= "accessToken" name ="accessToken" value="<?php echo $token; ?>" >
                    <button type="submit" class="srch_bt"><img src="<?php echo base_url(); ?>/theme/firstJob/image/search.png"></button>
                </div>
            </form>

            <div class="clearfix"></div>

            <div class="col-md-12 rsp_tbl">

                <table class="table" id="sorting">
                    <thead class="tbl_head">
                        <tr>
                            <!--<th><input type="checkbox" class="chk_bx"></th>-->
                            <th><span>Corporate</span></th>
                            <th><span>Location</span></th>
                            <th><span>Mobile</span></th>
                            <th><span>User Type</span></th>
                            <th><span>Status</span></th>
                            <th><span>Expiry Date</span></th>
                            <th><span>Expired</span></th>
                            <th><span>Active</span></th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if(count($content)==0) {
                            echo "<tr><td>No Search Result Found... !!!</td></tr>";
                        }
                        else {
                        $i = 0;
                        foreach ($content as $item):
                            ?>
                            <?php if ($i % 2 != 0): ?>
                                <tr>
                                <?php else: ?>
                                <tr>
                                <?php endif; ?>
    <!-- <td><input type="checkbox"></td>-->
                                <td>
                                    <a href="javascript:" 
                                       data-toggle="modal" 
                                       data-target="#viewUserProfileDetails" 
                                       class="view-user-profile-details" 
                                       data-options='{
                                       "userId":"<?php echo $item->id; ?>",
                                       "company":"<?php echo addslashes($item->company); ?>",
                                       "addressLine1":"<?php echo addslashes($item->addressLine1); ?>",
                                       "addressLine2":"<?php echo addslashes($item->addressLine2); ?>",
                                       "city":"<?php echo addslashes($item->city); ?>",
                                       "state":"<?php echo addslashes($item->state); ?>", 
                                       "country":"<?php echo addslashes($item->country); ?>", 
                                       "pinCode":"<?php echo addslashes($item->pinCode); ?>", 
                                       "phone":"<?php echo addslashes($item->phone); ?>", 
                                       "fax":"<?php echo addslashes($item->fax); ?>", 
                                       "email":"<?php echo addslashes($item->email); ?>", 
                                       "web":"<?php echo addslashes($item->web); ?>", 
                                       "industry":"<?php echo addslashes($item->industry); ?>", 
                                       "validFrom":"<?php echo addslashes($item->validFrom); ?>", 
                                       "validTo":"<?php echo addslashes($item->validTo); ?>", 
                                       "typeFlag":"<?php echo addslashes($item->typeFlag); ?>", 
                                       "statusFlag":"<?php echo addslashes($item->statusFlag); ?>", 
                                       "userLimit":"<?php echo addslashes($item->userLimit); ?>", 
                                       "companyLogo":"<?php echo addslashes($item->companyLogo); ?>"
                                       }'>
                                        <?php echo $item->company; ?> </a></td>
                                <td><?php echo $item->country . ", " . $item->state . ", " . $item->city; ?></td>
                                <td><?php echo $item->phone; ?></td>
                                <td><?php echo $item->typeFlag; ?></td>
                                <td><?php echo $item->statusFlag; ?></td>        
                                <td><?php echo date('d, M Y', strtotime($item->validTo)); ?></td>                        
                                <td><?php if(strtotime(date('Y-m-d', strtotime($item->validTo)))>=strtotime(date('Y-m-d'))) { echo 'No'; } else { echo 'Yes'; } ?></td>
                                <td><?php if ($item->status == '1' || $item->status == '2' || $item->status == '3') { ?><a href="#" class="activeInactive" id="<?php echo encryptURLparam($item->id, URLparamKey); ?>" ><?php echo ($item->status == '1' ? 'Active' : ($item->status == '2' ? 'Inactive' : 'Deleted')); ?></a><?php } else echo "Deleted"; ?></td>
                                <td>
                                    <a href="<?php echo base_url(); ?>users/editcorporate/<?php echo $item->id; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/edit.png" class="padd_rgt"></a>
                                    <a href="javascript:" class="delete-user" id="<?php echo $item->id; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/del.png"></a>
                                </td>
                                
                                       <!--"aboutCompany":"<?php //echo htmlspecialchars (addslashes(stripslashes($item->aboutCompany))); ?>",-->
                            </tr>
                            <?php
                            $i++;
                        endforeach;
                        }
                        ?>

                    </tbody>
                </table>
            </div>
            <div class="service_content ">
            <?php echo $this->pagination->create_links(); ?>
            </div>

        </div>
    </div>
</div>

<style>
    .service_content ul.service_content_last_list li{ display:inline-block; font-size:20px; width:38px; height:38px; line-height:38px; border:0px; }
    .service_content ul.service_content_last_list li a{ color:#333333; padding:0px; text-align:center; display:block; border:0px;  }
    .service_content ul.service_content_last_list li a:hover{ color:#fff; border-radius:50px; -moz-border-radius:50px; -webkit-border-radius:50px; background:#0072dc }
    .response_img img{ max-width: 100%; }
    .service_content .service_content_last_list{ text-align:center; padding:10px 0 70px; }
    ul.service_content_last_list  li.pagination-last-list{padding:0 20px 0 0px; cursor:pointer; text-indent:-9999px; margin-left:5px;  }
    .pagi_img{display: -webkit-inline-box;}
    .pagi_img a img{display: -webkit-inline-box;}
    .service_content ul.service_content_last_list li{margin:0px!important;}
    h1{font-size:32px!important;}
    .acu_bt a {
        text-decoration: none;
        color: #fff;
    }
    .sel_opt {
        font-size:15px;
    }
</style>
<script>
    $(function () {
        var settingTableRowColor = function (tableName) {
            $('#' + tableName + ' tbody tr').removeAttr('class');
            $('#' + tableName + ' tbody tr:odd').attr('class', 'tbl_bg');
        }
        settingTableRowColor('sorting');
        $('.sort-header').on('click', function () {
            settingTableRowColor('sorting');
        });
        $('.delete-user').on('click', function () {
            var url = '<?php echo base_url(); ?>';
            var userId = $(this).attr('id');
            var accessToken = $('#accessToken').val();
            bootbox.confirm('Are you sure you want to delete?', function (result) {
                //alert(result);
                if (result) {
                    $.post(url + 'users/delete', {id: userId, accessToken: accessToken}, function (data) {
                        //bootbox.alert(data);
                        location.reload();
                    });
                }
            });

        });
    });


    $('.activeInactive').on('click', function () {
        var url = '<?php echo base_url(); ?>';
        var userId = $(this).attr('id');
        //alert(userId);
        bootbox.confirm('Are you sure you want to change user status?', function (result) {
            if (result) {
                $.post(url + 'users/updateUserStatus', {id: userId}, function (data) {
                    //bootbox.alert(data);
                    location.reload();
                });
            }
        });

    });
</script>
<style>
    .modal-content {
        -webkit-box-shadow: 0 5px 15px rgba(0,0,0,.5);
        box-shadow: 0 5px 15px rgba(0,0,0,.5);
    }

    .modal-content {
        position: relative;
        background-color: #fff;
        -webkit-background-clip: padding-box;
        background-clip: padding-box;
        border: 1px solid #999;
        border: 1px solid rgba(0,0,0,.2);
        border-radius: 6px;
        outline: 0;
        -webkit-box-shadow: 0 3px 9px rgba(0,0,0,.5);
        box-shadow: 0 3px 9px rgba(0,0,0,.5);
        width: 920px;
        margin-left:-23% !important;
    }
</style>
<div id="viewUserProfileDetails" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <form class="form-horizontal form_save" action="<?php echo base_url(); ?>knowledge/editcategory" method="post" id="knowledgeCenterCategoryForm" >
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Corporate Profile Details</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="col-xs-4" id="viewUserimage" style="padding-top: 9px;width:150px;margin-left: 76px;"></div>
                        <div class="col-xs-5" style="padding-top:7px;width: 65%;padding-left: 154px;">
                            <div id="viewUserauditionvideo" style="width:100%; margin:0px auto"></div>
                            <div style="padding-left:325px; margin-top:-33px; color:#337ab7; cursor: pointer;" id="auditionLikeDislikeBlock"></div>
                            <input type="hidden" name="hdnaudlike" id="hdnaudlike" value="1" />
                            <input type="hidden" name="hdnaudid" id="hdnaudid" value="" />
                        </div>                            
                    </div>
                    <div class="form-group">
                        <div style="float: left; width: 444px; padding-left: 30px;">

                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-4">Company Name :</label>
                                <div class="col-xs-8" id="viewcompany" style="padding-top:7px;"></div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-4">Address Line 1:</label>
                                <div class="col-xs-8" id="viewaddressLine1" style="padding-top:7px;"></div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-4">Address Line 2 :</label>
                                <div class="col-xs-8" id="viewaddressLine2" style="padding-top:7px;"></div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-4">City :</label>
                                <div class="col-xs-8" id="viewcity" style="padding-top:7px;"></div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-4">State :</label>
                                <div class="col-xs-8" id="viewstate" style="padding-top:7px;"></div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-4">Country :</label>
                                <div class="col-xs-8" id="viewcountry" style="padding-top:7px;"></div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-4">Pincode :</label>
                                <div class="col-xs-8" id="viewpinCode" style="padding-top:7px;"></div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-4">Phone Number :</label>
                                <div class="col-xs-8" id="viewphone" style="padding-top:7px;"></div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-4">Fax Number :</label>
                                <div class="col-xs-8" id="viewfax" style="padding-top:7px;"></div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-4">Email :</label>
                                <div class="col-xs-8" id="viewemail" style="padding-top:7px;"></div>
                            </div>
                        </div>
                        <div style="float: left; width:444px;">
                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-4">Web :</label>
                                <div class="col-xs-8" id="viewweb" style="padding-top:7px;"></div>
                            </div>
<!--                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-4">About Company :</label>
                                <div class="col-xs-8" id="viewaboutCompany" style="padding-top:7px;"></div>
                            </div>-->
                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-4">Valid From :</label>
                                <div class="col-xs-8" id="viewvalidFrom" style="padding-top:7px;"></div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-4">Valid To :</label>
                                <div class="col-xs-8" id="viewvalidTo" style="padding-top:7px;"></div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-4">Type :</label>
                                <div class="col-xs-8" id="viewtypeFlag" style="padding-top:7px;"></div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-4">Status :</label>
                                <div class="col-xs-8" id="viewstatusFlag" style="padding-top:7px;"></div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-4">User Limit :</label>
                                <div class="col-xs-8" id="viewuserLimit" style="padding-top:7px;"></div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-4">Company Logo :</label>
                                <div class="col-xs-8" id="viewcompanyLogo" style="padding-top:7px;"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- div class="modal-footer">
                    <button type="button" class="btn btn-primary edit-knowledge-category" data-dismiss="modal">Close</button>
                </div -->
            </div>
        </form>
    </div>
</div>


<script>
    $(document).on('click', '.view-user-profile-details', function () {
        $('#viewcompany').html('');
        $('#viewaddressLine1').html('');
        $('#viewaddressLine2').html('');
        $('#viewcity').html('');
        $('#viewstate').html('');
        $('#viewcountry').html('');
        $('#viewpinCode').html('');
        $('#viewphone').html('');
        $('#viewfax').html('');
        $('#viewemail').html('');
        $('#viewweb').html('');
        $('#viewaboutCompany').html('');
        $('#viewvalidFrom').html('');
        $('#viewvalidTo').html('');
        $('#viewtypeFlag').html('');
        $('#viewstatusFlag').html('');
        $('#viewuserLimit').html('');
        $('#viewcompanyLogo').html('');
        
        var userId = $(this).data('options').userId;
        var company = $(this).data('options').company;
        var addressLine1 = $(this).data('options').addressLine1;
        var addressLine2 = $(this).data('options').addressLine2;
        var city = $(this).data('options').city;
        var state = $(this).data('options').state;
        var country = $(this).data('options').country;
        var pinCode = $(this).data('options').pinCode;
        var phone = $(this).data('options').phone;
        var fax = $(this).data('options').fax;
        var email = $(this).data('options').email;
        var web = $(this).data('options').web;
        var aboutCompany = $(this).data('options').aboutCompany;
        var validFrom = $(this).data('options').validFrom;
        var validTo = $(this).data('options').validTo;
        var typeFlag = $(this).data('options').typeFlag;
        var statusFlag = $(this).data('options').statusFlag;
        var userLimit = $(this).data('options').userLimit;
        var companyLogo = $(this).data('options').companyLogo;

       // alert(company);

        if (companyLogo != "") {
            var profileImage = '<?php echo base_url(); ?>uploads/corporateLogo/' + companyLogo;
            var userImage = "<img src='" + profileImage + "' alt='company logo' class='img-responsive' height='150px' width='150px'>";
        } else {
            var userImage = "";
        }

        

        $('#viewcompany').html(company);
        $('#viewaddressLine1').html(addressLine1);
        $('#viewaddressLine2').html(addressLine2);
        $('#viewcity').html(city);
        $('#viewstate').html(state);
        $('#viewcountry').html(country);
        $('#viewpinCode').html(pinCode);
        $('#viewphone').html(phone);
        $('#viewfax').html(fax);
        $('#viewemail').html(email);
        $('#viewweb').html(web);
        $('#viewaboutCompany').html(aboutCompany);
        $('#viewvalidFrom').html(validFrom);
        $('#viewvalidTo').html(validTo);
        $('#viewtypeFlag').html(typeFlag);
        $('#viewstatusFlag').html(statusFlag);
        $('#viewuserLimit').html(userLimit);
        $('#viewcompanyLogo').html(userImage);

        //});
    });
</script>
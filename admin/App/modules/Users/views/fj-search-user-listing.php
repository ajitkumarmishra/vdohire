<?php //echo '<pre>';
//print_r($myJobs);
$userData = $this->session->userdata['logged_in'];
$userRole = $userData->role;
?>
<div id="page-content-wrapper">
    <div class="container-fluid whitebg">
        <div class="row">
            <div class="col-md-3"><h1><img src="<?php echo base_url(); ?>/theme/firstJob/image/arrow_head.png" class="prev-page">Invite Users(<?php echo count($content);?>) </h1></div>
            <div class="col-md-4"></div>
            <form action="<?php echo base_url(); ?>users/listAppUsers" method="post">
                <div class="col-md-5" style="width: 511px; float: right;">
                    <select class="sel_opt" name ="serachColumn" style="width: 43% !important;">
                        <option value="fullname" <?php if ($serachColumn == 'fullname') echo "selected"; ?>>Search By Candidate Name</option>
                        <option value="location" <?php if ($serachColumn == 'location') echo "selected"; ?>>Search By Location</option>
                        <option value="qualification" <?php if ($serachColumn == 'qualification') echo "selected"; ?>>Search By Qualifications</option>
                        <option value="workExperience" <?php if ($serachColumn == 'workExperience') echo "selected"; ?>>Search By Experience</option>
                        <option value="Gender" <?php if ($serachColumn == 'Gender') echo "selected"; ?>>Search By Gender</option>
                    </select>
                    <?php if($searchText):?>
                    <div class="cross-search-candidate" style="margin:5% 0 0 80%;">X</div>
                    <?php endif;?>
                    <input type="text" class="search_box" name ="serachText" placeholder="" value ="<?php echo $searchText; ?>">
                    <input type="hidden" id= "accessToken" name ="accessToken" value="<?php echo $token; ?>" >
                    <button type="submit" class="srch_bt"><img src="<?php echo base_url(); ?>/theme/firstJob/image/search.png"></button>

                </div>
            </form>

            <div class="clearfix"></div>

            <div class="col-md-12 rsp_tbl">
                <h4><?php if($userRole=='1') { ?><a href="<?php echo base_url(); ?>users/downloadCSV">Export Data In CSV</a><?php } ?></h4>
                <table class="table sortable" id="sorting">
                    <thead class="tbl_head">
                        <tr>
                            <td align="center"><input type="checkbox" class="chk_bx"></td>
                            <th><span>Candidate Name</span></th>
                            <th><span>Location</span></th>
                            <th><span>Qualifications</span></th>
                            <th><span>Experience</span></th>
                            <th><span>Gender</span></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 0;
                        if($content):
                            foreach ($content as $item):
                                ?>
                                <?php if ($i % 2 != 0): ?>
                                    <tr>
                                    <?php else: ?>
                                    <tr>
                                    <?php endif; ?>
                                    <td align="center"><input type="checkbox" class="inviteCheck" name="invite[]" value="<?php echo $item->id; ?>"></td>
                                    <td>
                                        
                                        <a id="<?php echo 'userid-'.$item->id; ?>" href="javascript:" 
                                            data-toggle="modal" 
                                            data-target="#viewUserProfileDetails" 
                                            class="view-user-profile-details" 
                                            >
                                        <?php echo $item->UserName; ?> </a></td>
                                    <td><?php echo $item->PreferredLocations; ?></td>
                                    <td>
                                        <?php 
                                        echo $item->CourseName;
                                        ?>
                                    </td>
                                    <td><?php if($item->WorkExperience) { echo $item->WorkExperience." Years"; } ?></td>
                                    <td><?php echo $item->Gender; ?></td>

                                </tr>
                                <?php
                                $i++;
                            endforeach;
                        else:
                            echo "<tr><td>Searched users not available</td></tr>";
                        endif;
                        ?>

                    </tbody>
                </table>
                <?php
                if(count($content)>0) { ?>
                <button type="button" class="acu_bt pull-right invite-app-users" style="float:left!important;" data-toggle="modal" data-target="#inviteUsersForJob">Invite</button>
                <?php
                if($userRole=='1' || $userRole=='2') { ?>
                &emsp;
                <button type="button" class="acu_bt send-email-users" data-toggle="modal" data-target="#sendEmailForUsers">Send Email</button>
                <?php } ?>
                <?php if($userRole=='1') { ?>
                &emsp;
                <button type="button" class="acu_bt send-email-csv-users" data-toggle="modal" data-target="#sendEmailForCsvUsers">Send Email via CSV</button>
                <?php 
                }
                } ?>
            </div>
            <?php echo $this->pagination->create_links(); ?>
        </div>
    </div>
</div>

<!-- PopUp for invite users for a job  -->
<div id="inviteUsersForJob" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <form class="form-horizontal form_save" action="<?php echo base_url(); ?>users/inviteAppUsers" method="post" id="inviteAppUser" >
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Invite Users</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger hidden" id="inviteAlert"></div>
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-3">Select Job(s)</label>
                        <div class="col-xs-6">
                            <select data-placeholder="My Jobs" style="width: 350px; display: none;" multiple="" class="chosen-select" tabindex="-1" name="myJobs[]" id="myJobs">
                                <option value=""></option>
                                <?php foreach ($myJobs as $item) : ?>
                                    <option value="<?php echo $item['id']; ?>"><?php echo $item['title']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-3">Select Message Send Type</label>
                        <div class="col-xs-6">
                            <input type="checkbox" name="inviteFrom[]" id="inviteFrom_1" value="1" checked="" />&nbsp;Email&nbsp;&nbsp;
                            <input type="checkbox" name="inviteFrom[]" id="inviteFrom_2" value="2" />&nbsp;SMS
                        </div>                        
                    </div>
                    <div class="form-group" style="padding-left:428px">
                        <div class="col-xs-3">
                            <input type="hidden" name="userListInvite" class="userListInvite" value="" />
                            <button type="button" class="btn btn-primary save-invite-app-users">Send</button>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <!--                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
                </div>
            </div>
        </form>
    </div>
</div>




<?php
if($userRole=='1' || $userRole=='2') { ?>

<!-- PopUp for send email users  -->
<div id="sendEmailForUsers" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <form class="form-horizontal form_save" action="#" method="post" id="sendEmailForm" >
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Send Email</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger hidden" id="sendEmailAlert"></div>
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-3 required-field">Email Subject</label>
                        <div class="col-xs-6">
                            <input type="text" class="form-control search-critera" name="emailSubject" id="emailSubject" maxlength="50" placeholder="Email Subject" data-validation="required" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-3 required-field">Email Message</label>
                        <div class="col-xs-6">
                            <script type="text/javascript" src="//cdn.tinymce.com/4/tinymce.min.js"></script>
                            <script>
                                tinymce.init({
                                    selector: 'textarea',
                                    height: 300,
                                    menubar:false,
                                    statusbar: false,
                                    toolbar: 'bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent |',
                                    content_css: [
                                      '//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
                                      '//www.tinymce.com/css/codepen.min.css'
                                    ]
                                });
                            </script>
                            <textarea type="text" class="form-control search-critera" name="emailMessage" id="emailMessage" maxlength="500" placeholder="Email Message"></textarea>
                            <input type="text" data-validation="emailMessage" style="height: 0px; width: 0px; visibility: hidden; " /> 
                        </div>                        
                    </div>
                    <div class="form-group" style="padding-left:368px">
                        <div class="col-xs-3">
                            <input type="hidden" name="userListSendEmail" class="userListSendEmail" value="" />
                            <center><button type="button" class="btn btn-primary send-email-app-users">Send Email</button></center>
                        </div>
                    </div>
                </div>
                <div class="modal-footer"></div>
            </div>
        </form>
    </div>
</div>




<?php
}
if($userRole=='1') { ?>

<!-- PopUp for send email users via csv  -->
<div id="sendEmailForCsvUsers" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <form class="form-horizontal form_save" action="#" method="post" id="sendCsvEmailForm" >
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Send Email</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger hidden" id="csvSendEmailAlert"></div>
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-3 required-field">Email Subject</label>
                        <div class="col-xs-6">
                            <input type="text" class="form-control search-critera" name="csvEmailSubject" id="csvEmailSubject" maxlength="50" placeholder="Email Subject" data-validation="required" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-3 required-field">Email Message</label>
                        <div class="col-xs-6">
                            <textarea type="text" class="form-control search-critera" name="csvEmailMessage" id="csvEmailMessage" maxlength="500" placeholder="Email Message"></textarea>
                            <input type="text" data-validation="csvEmailMessage" style="height: 0px; width: 0px; visibility: hidden; " /> 
                        </div>                        
                    </div>
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-3  required-field">Upload Email Id (CSV file)</label>
                        <div class="col-xs-6">
                            <input type="file" class="form-control search-critera" name="csvEmail" id="csvEmail" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-3"></label>
                        <div class="col-xs-4 aligncenter">
                            <?php
                            $fileSamleCsv = base_url() . 'uploads/inviteUserFiles/inviteSampleFile.csv'; ?>
                            <a href="<?php echo $fileSamleCsv; ?>"> Download Sample File</a>
                        </div>
                    </div>
                    <div class="form-group" style="padding-left:368px">
                        <div class="col-xs-3">
                            <center><button type="button" class="btn btn-primary csv-send-email-app-users">Send Email</button></center>
                        </div>
                    </div>
                </div>
                <div class="modal-footer"></div>
            </div>
        </form>
    </div>
</div>

<?php
} ?>


<style>
.modal-content {
    -webkit-box-shadow: 0 5px 15px rgba(0,0,0,.5);
    box-shadow: 0 5px 15px rgba(0,0,0,.5);
}

.modal-content {
    position: relative;
    background-color: #fff;
    -webkit-background-clip: padding-box;
    background-clip: padding-box;
    border: 1px solid #999;
    border: 1px solid rgba(0,0,0,.2);
    border-radius: 6px;
    outline: 0;
    -webkit-box-shadow: 0 3px 9px rgba(0,0,0,.5);
    box-shadow: 0 3px 9px rgba(0,0,0,.5);
    width: 920px;
    margin-left:-23% !important;
}
</style>
<!-- PopUp for view user's profile  -->
<div id="viewUserProfileDetails" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <form class="form-horizontal form_save" action="<?php echo base_url(); ?>knowledge/editcategory" method="post" id="knowledgeCenterCategoryForm" >
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">User Profile Details</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="col-xs-4" id="viewUserimage" style="padding-top: 9px;width:150px;margin-left: 76px;"></div>
                        <div class="col-xs-5" style="padding-top:7px;width: 65%;padding-left: 154px;">
                            <div id="viewUserauditionvideo" style="width:100%; margin:0px auto"></div>
                            <div style="padding-left:325px; margin-top:-33px; color:#337ab7; cursor: pointer;" id="auditionLikeDislikeBlock"></div>
                            <input type="hidden" name="hdnaudlike" id="hdnaudlike" value="1" />
                            <input type="hidden" name="hdnaudid" id="hdnaudid" value="" />
                        </div>                            
                    </div>
                    <div class="form-group">
                        <div style="float: left; width: 444px; padding-left: 30px;">
                            
                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-4">User Name :</label>
                                <div class="col-xs-8" id="viewUsername" style="padding-top:7px;"></div>
                            </div>
                            <?php
                            if($userRole=='1'){ ?>
                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-4">User Mobile :</label>
                                <div class="col-xs-8" id="viewUsermobile" style="padding-top:7px;"></div>
                            </div>
                            <?php } ?>
                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-4">User YOB :</label>
                                <div class="col-xs-8" id="viewUserdob" style="padding-top:7px;"></div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-4">User Gender :</label>
                                <div class="col-xs-8" id="viewUsergender" style="padding-top:7px;"></div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-4">User Language :</label>
                                <div class="col-xs-8" id="viewUserlang" style="padding-top:7px;"></div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-4">User Aadhar :</label>
                                <div class="col-xs-8" id="viewUseraadhar" style="padding-top:7px;"></div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-4">Facebook :</label>
                                <div class="col-xs-8" id="viewFacebook" style="padding-top:7px;"></div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-4">LinkedIn :</label>
                                <div class="col-xs-8" id="viewLinkedin" style="padding-top:7px;"></div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-4">Twitter :</label>
                                <div class="col-xs-8" id="viewTwitter" style="padding-top:7px;"></div>
                            </div>
                        </div>
                        <div style="float: left; width:444px;">
                            
                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-4">Google Plus :</label>
                                <div class="col-xs-8" id="viewGoogleplus" style="padding-top:7px;"></div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-4">Qualification :</label>
                                <div class="col-xs-8" id="viewQualification" style="padding-top:7px;"></div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-4">Education Completion Year :</label>
                                <div class="col-xs-8" id="viewCompletionyear" style="padding-top:7px;"></div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-4">Expected CTC From (Lakhs) :</label>
                                <div class="col-xs-8" id="viewExpectedctcfrom" style="padding-top:7px;"></div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-4">Expected CTC To (Lakhs) :</label>
                                <div class="col-xs-8" id="viewExpectedctcto" style="padding-top:7px;"></div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-4">Work Experience (Years) :</label>
                                <div class="col-xs-8" id="viewWorkexperience" style="padding-top:7px;"></div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-4">View Resume :</label>
                                <div class="col-xs-8" id="viewResume" style="padding-top:7px;"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- div class="modal-footer">
                    <button type="button" class="btn btn-primary edit-knowledge-category" data-dismiss="modal">Close</button>
                </div -->
            </div>
        </form>
    </div>
</div>

<style>
    .service_content ul.service_content_last_list li{ display:inline-block; font-size:20px; width:38px; height:38px; line-height:38px; border:0px; }
    .service_content ul.service_content_last_list li a{ color:#333333; padding:0px; text-align:center; display:block; border:0px;  }
    .service_content ul.service_content_last_list li a:hover{ color:#fff; border-radius:50px; -moz-border-radius:50px; -webkit-border-radius:50px; background:#0072dc }
    .response_img img{ max-width: 100%; }
    .service_content .service_content_last_list{ text-align:center; padding:10px 0 70px; }
    ul.service_content_last_list  li.pagination-last-list{padding:0 20px 0 0px; cursor:pointer; text-indent:-9999px; margin-left:5px;  }
    .pagi_img{display: -webkit-inline-box;}
    .pagi_img a img{display: -webkit-inline-box;}
    .service_content ul.service_content_last_list li{margin:0px!important;}
    h1{font-size:32px!important;}
    .acu_bt a {
        text-decoration: none;
        color: #fff;
    }
    .sel_opt {
        font-size:15px;
        width:33% !important;
        padding-left: 9px !important;
    }
    #sorting td a {color: #303030!important; }
    #sorting td a:hover {color: #777!important; }
   
</style>
<!--<script src="<?php echo base_url(); ?>theme/firstJob/js/tablesort.js"></script>-->
<!--<script src="<?php echo base_url(); ?>theme/firstJob/js/jquery.tablesorter.js"></script>-->
<!--<script src="https://code.jquery.com/jquery-1.10.2.js"></script>-->
<script src="<?php echo base_url(); ?>theme/firstJob/js/chosen.jquery.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>theme/firstJob/js/prism.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
    var config = {
        '.chosen-select': {},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }

    $(function () {
        $("#datepicker-19").datepicker();
    });

</script>
<script>
    $(function () {
        var settingTableRowColor = function (tableName) {
            $('#' + tableName + ' tbody tr').removeAttr('class');
            $('#' + tableName + ' tbody tr:odd').attr('class', 'tbl_bg');
        }
        settingTableRowColor('sorting');
        $('.sort-header').on('click', function () {
            settingTableRowColor('sorting');
        });
        $('.delete-user').on('click', function () {
            var url = '<?php echo base_url(); ?>';
            var userId = $(this).attr('id');
            var accessToken = $('#accessToken').val();
            bootbox.confirm('Are you sure you want to delete?', function (result) {
                //alert(result);
                if (result) {
                    $.post(url + 'users/delete', {id: userId, accessToken: accessToken}, function (data) {
                        //bootbox.alert(data);
                        location.reload();
                    });
                }
            });
        });
    });
    
    $(document).on('click', '.view-user-profile-details', function () {
        var stringUserid = $(this).attr('id');
        var userid = stringUserid.substr(stringUserid.indexOf("-") + 1);

        var allUserData;
        $.ajax({
            url: siteUrl + 'users/getSearchedUserData/'+ userid,
            data: '',
            async: false,
            contentType: false,
            processData: false,
            cache: false,
            type: 'POST',
            success: function (data)
            {
                var allUserData = jQuery.parseJSON(data);


                if (allUserData.userImage != "") {
                    var profileImage = '<?php echo base_url(); ?>uploads/userImages/'+allUserData.userImage+'.PNG';
                } else {
                    var profileImage = '<?php echo base_url(); ?>uploads/userImages/defualt.png';
                }
                
                var userImage = "<img src='"+profileImage+"' alt='User profile picture' class='img-responsive'>";

                $('#viewUsername').html(allUserData.userName);
                $('#viewUserimage').html(userImage);
                $('#viewUsergender').html(allUserData.userGender);
                $('#viewUserdob').html(allUserData.userDob);
                $('#viewUserlang').html(allUserData.userLang);
                $('#viewUseraadhar').html(allUserData.userAadhar);
                $('#viewFacebook').html(allUserData.facebookLink);
                $('#viewLinkedin').html(allUserData.linkedInLink);
                $('#viewTwitter').html(allUserData.twitterLink);
                $('#viewGoogleplus').html(allUserData.googlePlusLink);
                $('#viewQualification').html(allUserData.qualification);
                $('#viewCompletionyear').html(allUserData.completionYear);
                $('#viewExpectedctc').html(allUserData.expectedCtc);
                $('#viewExpectedctcfrom').html(allUserData.expectedCtcFrom);
                $('#viewExpectedctcto').html(allUserData.expectedCtcTo);
                $('#viewWorkexperience').html(allUserData.workExperience);
                $('#hdnaudlike').val(allUserData.alreadyLikeStatus);

                if( allUserData.auditionfiletype == 1 ){
                    var auditionvideotype = "video";
                } else if( allUserData.auditionfiletype == 2 ){
                    var auditionvideotype = "audio";
                }

                if( auditionvideotype != "" && allUserData.alreadyLikeStatus != "" ){
                    if(allUserData.auditionFiles!='') {
                        var auditionFileName = allUserData.auditionFiles.split(',');
                        var message =  '<video id="videoarea" preload="auto" tabindex="0" controls="" src="<?php  echo S3BUCKETPATH.S3AUDITION; ?>/'+auditionFileName[0].trim()+'"></video>';
                        message += '<center><ul id="playlist">';                
                        var m=0;
                        auditionFileName.forEach(function(auditionFileName) {
                            m++;
                            if(m==1)
                                message += '<li class="active"><a href="<?php  echo S3BUCKETPATH.S3AUDITION; ?>/'+auditionFileName.trim()+'">Audition Part '+m+'</a></li>';
                            else
                                message += '<li><a href="<?php  echo S3BUCKETPATH.S3AUDITION; ?>/'+auditionFileName.trim()+'">Audition Part '+m+'</a></li>';
                        });
                        message += '</ul></center>';
                    }
                    else {
                        var message = '<video id="videoarea" preload="auto" tabindex="0" controls="" src="<?php  echo S3BUCKETPATH.S3MERGEAUDITION; ?>/'+allUserData.auditionfile.replace('flv', 'mp4')+'"></video>';
                        message += '<center><ul id="playlist">';
                        message += '<li class="active"><a href="<?php echo S3BUCKETPATH.S3MERGEAUDITION; ?>/'+allUserData.auditionfile.replace('flv', 'mp4')+'">Audition Part 1</a></li>';
                        message += '</ul></center>';
                    }
                    $('#viewUserauditionvideo').html(message);

                    init();
                    function init(){
                        var current     = 0;
                        var audio       = $('#videoarea');
                        var playlist    = $('#playlist');
                        var tracks      = playlist.find('li a');
                        var len         = tracks.length;
                        audio[0].volume = .70;
                        //audio[0].play();
                        playlist.find('a').click(function(e){
                            e.preventDefault();
                            link    = $(this);
                            current = link.parent().index();
                            run(link, audio[0]);
                        });
                        audio[0].addEventListener('ended',function(e){
                            current++;
                            if(current == len){
                                current = 0;
                                link    = playlist.find('a')[0];
                            }else{
                                link    = playlist.find('a')[current];    
                            }
                            if(current!=0)
                                run($(link),audio[0]);
                        });
                    }
                    function run(link, player){
                            player.src  = link.attr('href');
                            par         = link.parent();
                            par.addClass('active').siblings().removeClass('active');
                            player.load();
                            player.play();
                    }
                } else {
                    $('#viewUserauditionvideo').html('');
                    $('#hdnaudid').val();
                }

                if(allUserData.resumePath) {
                    var paths = allUserData.resumePath.split('.');
                    if(paths[1] == 'doc' || paths[1] == 'docx') {
                        var imagetype = 'worddoc.png';
                    } else {
                        var imagetype = 'pdficon.jpg';
                    }
                    
                    $('#viewResume').wrap('<a href="https://s3.amazonaws.com/resumefirstjob/' + allUserData.resumePath + '" TARGET="_blank"><img src="<?php echo base_url(); ?>/theme/firstJob/image/' + imagetype + '" height="40px" width="40px"></a>');
                }

                
                if( allUserData.alreadyLikeStatus == 1 ){
                    $('#auditionLikeDislikeBlock').html("<img src='<?php echo base_url(); ?>/theme/firstJob/image/like-icon.png' title='Like Video' />");
                } else if( allUserData.alreadyLikeStatus == 2 ){
                    $('#auditionLikeDislikeBlock').html("<img src='<?php echo base_url(); ?>/theme/firstJob/image/dislike-icon.png' title='Dislike Video' />");
                }


            },
        });

        
        //});*/
    });
    
    $('#auditionLikeDislikeBlock').on('click', function() {
        var hdnaudlikeType = $("#hdnaudlike").val();
        var hdnaudid = $("#hdnaudid").val();

        if( hdnaudlikeType != "" && hdnaudid != "" ) {
            // show industry drop down div
            formData = {hdnaudlikeType:hdnaudlikeType, hdnaudId:hdnaudid}; //Array 
            //alert(hdnaudlikeType);
            //alert(siteUrl + "users/likeDislikeUserAuditionFunction");
            
            $.ajax({
                url : siteUrl + "users/likeDislikeUserAuditionFunction",
                type: "POST",
                data : formData,
                cache: false,
                success: function(result)
                {
                    //alert(result);
                    //alert(textStatus);
                    if( result.trim() == "NotDone" || result.trim() == "" ){
                        alert("Sorry, we are facing some problem with server");                        
                    } else if( result.trim() == "AlreadyLiked" ){
                        alert("You've already liked this audition");  
                    } else {                        
                        $("#hdnaudlike").val(result);
                        if( result.trim() == 1 ){
                            $("#auditionLikeDislikeBlock").html("<img src='<?php echo base_url(); ?>/theme/firstJob/image/like-icon.png' title='Like Video' />");
                        }else if( result.trim() == 2 ){
                            $("#auditionLikeDislikeBlock").html("<img src='<?php echo base_url(); ?>/theme/firstJob/image/dislike-icon.png' title='Dislike Video' />");
                        }
                    }                    
                    //data - response from server
                }
            });
        }
    });
    
    //new Tablesort(document.getElementById('sorting'));
    //$("#sorting").tablesorter();
    
    
    
    
    
    $('.close').on('click', function() {
        $.each($('audio'), function () {
            this.pause();
        })
        $.each($('video'), function () {
            this.pause();
        })
    });
</script>






<style>
#playlist li{
    cursor:pointer;
    padding:1px;
    list-style: none;
}
#videoarea {
    width:100%;
    height:200px;   
}                
#playlist,audio{background:#666;width:50%;padding:5px;}
.active a{color:#5DB0E6;text-decoration:none;}
#playlist li a{color:#eeeedd;background:#333;padding:5px;display:inline-block; font-size: 12px; text-decoration:none;}
#playlist li a:hover{text-decoration:none;}
</style>










<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script>
// Add validator For Interview Assessment
$.formUtils.addValidator({
    name : 'emailMessage',
    validatorFunction : function(value, $el, config, language, $form) {
        if(tinymce.get('emailMessage').getContent()!='') {
            return true;
        }
        else {
            return false;
        }
        //return parseInt(value, 10) % 2 === 0;
    },
    errorMessage : 'This is a required field'
});

$.validate({
    form: '#sendEmailForm',
    modules: 'file',
    onError: function ($form) {
        //alert("Dfsd");
        return false;
    },
    onSuccess: function ($form) {
        //alert("Dfsd");
        return true;
    },
});
</script>


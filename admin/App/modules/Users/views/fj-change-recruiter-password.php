<?php if (isset($user) && $user != NULL) {
//echo "<pre>"; print_r($user);
} 
//echo "<pre>"; print_r($error);
?>
<form method="post" action="" name="frmCorporate" >
    <div id="page-content-wrapper" style="margin-top: 52px;">
        <?php if (isset($msg)): ?>
    <div class="alert alert-success">
    <?php echo $msg; ?>
    </div>
<?php endif; ?>
    <?php if (isset($error) && $error!=NULL): ?>
    <div class="alert alert-danger">
        <?php 
            echo $error;
            /*foreach ($error as $item) {
                echo $item;
            }*/
        ?>
    </div>
<?php endif; ?>
        <div class="container-fluid whitebg">
            <div class="row">
                <div class="col-md-12 padd_tp_bt_20">
                    <div class="row">
                        <div class="col-md-12" style="margin-bottom: 20px;"><span style="margin-top: 20px; margin-bottom: 10px; font-size: 19px;">Change Password</span>
                        </div>
                    </div>

                    <div class="form-horizontal form_save">
                        <div class="form-group">
                            <label for="inputEmail" class="control-label col-xs-3 required-field ">Old Password</label>
                            <div class="col-xs-9">
                                <input type="password" class="form-control"  placeholder="Old Password" name="oldPassword" value="<?php if(isset($user['oldPassword'])) echo $user['oldPassword']; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail" class="control-label col-xs-3 required-field ">New Password</label>
                            <div class="col-xs-9">
                                <input type="password" class="form-control"  placeholder="New Password" name="newPassword" value="<?php if(isset($user['newPassword'])) echo $user['newPassword']; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail" class="control-label col-xs-3 required-field ">Confirm New Password</label>
                            <div class="col-xs-9">
                                <input type="password" class="form-control"  placeholder="Confirm New Password" name="confirmNewPassword" value="<?php if(isset($user['confirmNewPassword'])) echo $user['confirmNewPassword']; ?>" >
                            </div>
                        </div>
                        <!--<div class="form-group">
                            <label for="inputPassword" class="control-label col-xs-2">Company Id</label>
                            <div class="col-xs-10">
                                <input type="text" class="form-control" placeholder="company id">
                            </div>
                        </div>-->

                        <div class="clearfix">

                            <!--                        <div class="form-group">
                                                        <label for="inputPassword" class="control-label col-xs-2">Password</label>
                                                        <div class="col-xs-4">
                                                            <input type="text" class="form-control" placeholder="password">
                                                        </div>
                            
                                                        <label for="inputPassword" class="control-label col-xs-1">Confirm:</label>
                                                        <div class="col-xs-5">
                                                            <input type="text" class="form-control" placeholder="confirm">
                                                        </div>
                                                    </div>-->

                            <div class="clearfix">
                                <div class="clearfix">

                                    <div class="clearfix">
                                        <div class="clearfix">
                                            <input type="hidden" name="accessToken" value="<?php echo $token; ?>" />
                                            <input type="hidden" name="coprorateLogoName" id="coprorateLogoName" value="<?php
                                            if (isset($coprorateLogoName) && $coprorateLogoName != NULL) {
                                                echo $coprorateLogoName;
                                            }
                                            ?>" />
                                            <input type="hidden" name="coprorateImage" id="coprorateImage" value="<?php
                                            if (isset($coprorateImage) && $coprorateImage != NULL) {
                                                echo $coprorateImage;
                                            }
                                            ?>" />

                                            <div class="col-xs-offset-3" style="margin-top:35px;">
                                                <div style="padding-left: 7px;">
                                                    <button type="submit" class="Save_frm" style="background: #054f72 none repeat scroll 0 0; color:white;">Save</button>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /#page-content-wrapper -->
                        </div></div>
                </div></div></div></div>


</form>
<script>
    $(function () {
        var url = '<?php echo base_url(); ?>';
        window.siteUrl = url;
        window.token = '<?php echo $token; ?>';
    });
</script>
<style>
    .ui-datepicker-year {
        background: #f9a727;
    }    
</style>
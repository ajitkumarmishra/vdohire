
<div class="container-fluid super_login_bg">
    <div class="container">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6 padd_login">

                <a href="https://vdohire.com/"><img src="<?php echo base_url(); ?>/theme/image/login_logo.png" class="center-block"></a>
                <p class="text-center">Welcome to</p>
                <h1 class="text-center"><?php echo $roleName; ?></h1>


                <form class="login_frm" method="post" action ="<?php echo base_url(); ?><?php echo $pageUrl; ?>">
                    <input id="inputRecruiterEmail" type="text" name="email" class="center-block" placeholder="Email">
                    <span class="frgt_pass"><img src="<?php echo base_url(); ?>/theme/image/fgt_pass_img.png"> <a id="forgotPassword" href="<?php echo base_url(); ?>users/forgotPassword">Forgot Password?</a></span>
                    <input id="inputRecruiterPassword" type="password" name="password" class="center-block" placeholder="Password">
                    <input type="hidden" id= "accessToken" name ="accessToken" value="<?php echo $token; ?>"/>
                    <button id="inputRecruiterLoginButton" type="submit" class="center-block">Login</button>
                </form>

                <?php if (isset($msg) && $msg != NULL): ?>
                    <div class="error-list">
                        <?php echo $msg; ?>
                    </div>
                <?php endif; ?>
                <?php if (isset($error) && $error != NULL): ?>
                    <div class="error-list">
                        <?php foreach ($error as $item): ?>
                            <?php echo $item; ?>
                            <?php //echo "<br/>";?>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>

                <!--                <div class="center-block links_login">
                                    <a href="#">Contact Us</a>  |  
                                    <a href="#">Terms of Use</a>  |  
                                    <a href="#">About Us</a> |  
                                    <a href="#">How to Use</a>
                
                                </div>-->

            </div>
            <div class="col-md-3"></div>
        </div>
    </div>
</div> 

<style>
    .error-list {
        color: red;
        background: white;
        padding: 10px;
        border-radius: 10px;
    }
    .error-list p {
        padding-top: 0px!important;
        color: red;
    }
</style>
<script>
    setTimeout(function () {
        $('.error-list').fadeOut('fast');
    }, 2000);
</script>

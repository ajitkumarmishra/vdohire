<style>
.percent {
    font-size: 25px;
}
.para_txtxt {
    font-size: 25px;
}
.padd_add {
    padding-bottom: 0px;
    padding-top: 0px;
}
.purple {
    background: #852c9a;
    padding-top: 15px;
    padding-bottom: 10px;
    margin-bottom: 20px;
}
.green {
    background: #28b779;
    padding-top: 15px;
    padding-bottom: 10px;
    margin-bottom: 20px;
}
.gray {
    background: #777f8a;
    padding-top: 15px;
    padding-bottom: 10px;
    margin-bottom: 20px;
}
.red {
    background: #b75028;
    height: 165px;
    margin-bottom: 20px;
    padding-bottom: 10px;
    padding-top: 10px;
}
.txt_white {
    color: #fff;
}
</style>
<div id="page-content-wrapper">

    <div class="container-fluid no_padding dasboard_padd">
        <div class="row">

            <div class="col-md-3">
                <div class="purple">
                    <a href="<?php echo base_url(); ?>users/list/corporate/" title="Manage Corporates">
                    <div class="graph">
                        <span class="chart" data-percent="100">
                            <span class="percent"><?php echo ($totalClient?$totalClient['totalClient']:0); ?>
                                <p style="line-height:20px;">Total Clients</p>
                            </span>          
                            <canvas height="110" width="110"></canvas></span>
                    </div> 
                    <div class="text_ss">
                        <div class="padd_add">                           
                            <p class="text-right para_txtxt"><?php echo ($activeClient?$activeClient['activeClient']:0); ?></p>
                            <p class="text-right">Active Clients</p>      
                            <p class="text-right para_txtxt"><?php echo ($lastMonthClient?$lastMonthClient['lastMonthClient']:0); ?></p>
                            <p class="text-right">Last Month Added</p>               
                            <p class="text-right para_txtxt"><?php echo ($lastQuarterClient?$lastQuarterClient['lastQuarterClient']:0); ?></p>
                            <p class="text-right">Last Quarter Added</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            
            <div class="col-md-3">
                <div class="gray">
                    <a href="<?php echo base_url(); ?>corporate/dashboard">
                    <div class="graph">
                        <div class="padd_add txt_white" style="width: 195px;">  
                            <p class="text-right txt_white" style="font-size: 18px;">User Job Application</p>
                            <p class="text-left txt_white" style="margin: 0px; padding-left: 10px;">Total Jobs on FJ</p>                            
                            <p class="text-left" style="margin:0px; padding-left: 10px;">Number of Candidates Invited</p> 
                        </div>
                    </div> 
                    <div class="text_ss">
                        <div class="padd_add txt_white">   
                            <p class="text-right txt_white" style="font-size: 18px;">&emsp;</p>                        
                            <p class="text-right" style="margin:0 0 0px;"><?php echo ($totalJobs?$totalJobs['totalJobs']:0); ?></p>
                            <p class="text-right" style="margin:0 0 0px;"><?php echo ($invitedUsers?$invitedUsers['invitedUsers']:0); ?></p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            
            <div class="col-md-3">
                <div class="green">                    
                    <a href="<?php echo base_url(); ?>queue/list/" title="Queue Management">
                    <div class="graph">
                        <span class="chart" data-percent="100">
                            <span class="percent"><?php echo ($totalApplication?$totalApplication['totalApplication']:0); ?>
                                <p style="line-height:20px;">Applications<br>Received</p>
                            </span>          
                            <canvas height="110" width="110"></canvas>
                        </span>
                    </div> 
                    <div class="text_ss">
                        <div class="padd_add">                           
                            <p class="text-right para_txtxt"><?php echo ($pendingApplication?$pendingApplication['pendingApplication']:0); ?></p>
                            <p class="text-right">Evaluation Pending</p>            
                            <p class="text-right para_txtxt"><?php echo ($shortlistedApplication?$shortlistedApplication['shortlistedApplication']:0); ?></p>
                            <p class="text-right">Shortlisted</p>         
                            <p class="text-right para_txtxt"><?php echo ($viewedApplication?$viewedApplication['viewedApplication']:0); ?></p>
                            <p class="text-right">Viewed</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            
            <div class="col-md-3">
                <div class="red">
                    <a href="<?php echo base_url(); ?>users/list/" title="Manage Users">
                    <div class="graph">
                        <div class="padd_add txt_white" style="width: 155px;">  
                            <p class="text-right txt_white" style="font-size: 18px;">User Numbers</p>
                            <p class="text-left" style="margin:0px; padding-left: 10px;">Total App Users</p>
                            <p class="text-left" style="margin:0px; padding-left: 10px;">Total number of Auditions</p> 
                            <p class="text-left" style="margin:0px; padding-left: 10px;">Users Logged in Yesterday</p>
                        </div>
                    </div> 
                    <div class="text_ss">
                        <div class="padd_add txt_white">   
                            <p class="text-right txt_white" style="font-size: 18px;">&emsp;</p>                        
                            <p class="text-right" style="margin:0 0 0px;"><?php echo ($totalAppUser?$totalAppUser['totalAppUser']:0); ?></p>
                            <p class="text-right" style="margin:0 0 0px;"><?php echo ($totalAudition?$totalAudition['totalAudition']:0); ?></p>     
                            <p class="text-right" style="margin:0 0 0px;"><?php echo ($loggedInUser?$loggedInUser['loggedInUser']:0); ?></p> 
                        </div>
                    </div>
                    </a>
                </div>
            </div>


        </div>
    </div>


    <div class="container-fluid whitebg">
        <div class="row">
            <div class="col-md-12"><h1>Corporate List</h1></div>

            <div class="clearfix"></div>

            <div class="col-md-12 rsp_tbl">

                <table class="table" id="sorting">
                    <thead class="tbl_head">
                        <tr>
                            <th><span>Corporate</span></th>
                            <th><span>Location</span></th>
                            <th><span>Mobile</span></th>
                            <th><span>User Type</span></th>
                            <th><span>Status</span></th>
                            <th><span>Registration Date</span></th>
                            <th><span>Expiry Date</span></th>
                            <th><span>Active</span></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 0;
                        foreach ($content as $item):
                            /*
                            echo "<pre>";
                            print_r($item);
                            echo "</pre>";
                             * 
                             */
                            ?>
                            <?php if ($i % 2 != 0): ?>
                                <tr>
                                <?php else: ?>
                                <tr>
                                <?php endif; ?>
                                <td>
                                    <a href="javascript:" 
                                       data-toggle="modal" 
                                       data-target="#viewUserProfileDetails" 
                                       class="view-user-profile-details" 
                                       data-options='{
                                       "userId":"<?php echo $item->id; ?>",
                                       "company":"<?php echo addslashes($item->company); ?>",
                                       "addressLine1":"<?php echo addslashes($item->addressLine1); ?>",
                                       "addressLine2":"<?php echo addslashes($item->addressLine2); ?>",
                                       "city":"<?php echo addslashes($item->city); ?>",
                                       "state":"<?php echo addslashes($item->state); ?>", 
                                       "country":"<?php echo addslashes($item->country); ?>", 
                                       "pinCode":"<?php echo addslashes($item->pinCode); ?>", 
                                       "phone":"<?php echo addslashes($item->phone); ?>", 
                                       "fax":"<?php echo addslashes($item->fax); ?>", 
                                       "email":"<?php echo addslashes($item->email); ?>", 
                                       "web":"<?php echo addslashes($item->web); ?>", 
                                       "industry":"<?php echo addslashes($item->industry); ?>", 
                                       "aboutCompany":"<?php echo mysql_real_escape_string($item->aboutCompany); ?>", 
                                       "validFrom":"<?php echo addslashes($item->validFrom); ?>", 
                                       "validTo":"<?php echo addslashes($item->validTo); ?>", 
                                       "typeFlag":"<?php echo addslashes($item->typeFlag); ?>", 
                                       "statusFlag":"<?php echo addslashes($item->statusFlag); ?>", 
                                       "userLimit":"<?php echo addslashes($item->userLimit); ?>", 
                                       "companyLogo":"<?php echo addslashes($item->companyLogo); ?>"
                                       }'>
                                        <?php echo $item->company; ?> </a></td>
                                <td><?php echo $item->country.", ".$item->state.", ".$item->city; ?></td>
                                <td><?php echo $item->phone; ?></td>
                                <td><?php echo $item->typeFlag; ?></td>
                                <td><?php echo $item->statusFlag; ?></td>
                                <td><?php echo date('d, M-Y', strtotime($item->createdAt)); ?></td>
                                <td><?php echo date('d, M-Y', strtotime($item->validTo)); ?></td>
                                <td><?php if ($item->status == '1' || $item->status == '2') { ?><?php echo ($item->status == '1' ? 'Active' : ($item->status == '2' ? 'Inactive' : 'Deleted')); ?><?php } else echo "Deleted"; ?></td>
                                
                            </tr>
                            <?php
                            $i++;
                        endforeach;
                        ?>

                    </tbody>
                </table>
            </div>
            
            <div class="service_content ">

                <?php if ($totalCount > $itemsPerPage): ?>
                    <ul class="service_content_last_list">
                        <?php if ($page > 1): ?>
                            <p class="pagi_img"><a href="<?php echo base_url(); ?>users/list/corporate/<?php echo $page - 1; ?>" ><img src="<?php echo base_url(); ?>/theme/firstJob/image/pagination_img2.png"></a></p>    
                        <?php endif; ?>
                        <input type="hidden" name="page_no" value="1" id="page_no"> 
                        <?php for ($i = 1; $i <= $count; $i++): ?>
                            <li><a href="<?php echo base_url(); ?>users/list/corporate/<?php echo $i; ?>"  <?php if ($i == $page): ?>style="color: orange;"<?php endif; ?>><?php echo $i; ?></a></li>
                        <?php endfor; ?>
                        <?php if ($page < $count): ?>
                            <p class="pagi_img"><a href="<?php echo base_url(); ?>users/list/corporate/<?php echo $page + 1; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/pagination_img1.png"></a></p>            
                        <?php endif; ?>
                    </ul>
                <?php endif; ?>
            </div>

        </div>
    </div>
</div>


<!--<script src="js/jquery-1.12.4.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery-2.0.3.min.js"></script>-->
<script src="<?php echo base_url(); ?>theme/firstJob/js/jquery.easypiechart.min.js"></script>
<!--<script src="js/script.js"></script>-->
<script>
    $(function () {
        $(function () {
            //create instance
            $('.chart').easyPieChart({
                animate: 2000,
                scaleColor: 'rgba(0, 0, 0, 0)',
                trackColor: 'rgba(255, 255, 255, 0.2)',
                barColor: '#fff'
            });

        });
    });

    $(function () {
        $(function () {
            //create instance
            $('.chart-2').easyPieChart({
                animate: 2000,
                scaleColor: 'rgba(0, 0, 0, 0)'
            });

        });
    });
</script>
<style>
    .text_ss {
        float:right!important;
        padding-right:0px;
    }
    canvas {
        position: absolute;
        left: 6%;
    }
    body{
    font-family:  RobotoCondensed !important;
    background: #f1f1f1 !important;
}

</style>

<script>
    $(function () {

        var settingTableRowColor = function (tableName) {
            $('#' + tableName + ' tbody tr').removeAttr('class');
            $('#' + tableName + ' tbody tr:odd').attr('class', 'tbl_bg');
        }
        settingTableRowColor('sorting');
        $('.sort-header').on('click', function () {
            settingTableRowColor('sorting');
        });
        $('.delete-user').on('click', function () {
            var url = '<?php echo base_url(); ?>';
            var userId = $(this).attr('id');
            var accessToken = $('#accessToken').val();
            bootbox.confirm('Are you sure you want to delete?', function (result) {
                //alert(result);
                if (result) {
                    $.post(url + 'users/delete', {id: userId, accessToken: accessToken}, function (data) {
                        //bootbox.alert(data);
                        location.reload();
                    });
                }
            });

        });
    });
</script>


<style>
    .modal-content {
        -webkit-box-shadow: 0 5px 15px rgba(0,0,0,.5);
        box-shadow: 0 5px 15px rgba(0,0,0,.5);
    }

    .modal-content {
        position: relative;
        background-color: #fff;
        -webkit-background-clip: padding-box;
        background-clip: padding-box;
        border: 1px solid #999;
        border: 1px solid rgba(0,0,0,.2);
        border-radius: 6px;
        outline: 0;
        -webkit-box-shadow: 0 3px 9px rgba(0,0,0,.5);
        box-shadow: 0 3px 9px rgba(0,0,0,.5);
        width: 920px;
        margin-left:-23% !important;
    }
</style>
<div id="viewUserProfileDetails" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <form class="form-horizontal form_save" action="<?php echo base_url(); ?>knowledge/editcategory" method="post" id="knowledgeCenterCategoryForm" >
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Corporate Profile Details</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="col-xs-4" id="viewUserimage" style="padding-top: 9px;width:150px;margin-left: 76px;"></div>
                        <div class="col-xs-5" style="padding-top:7px;width: 65%;padding-left: 154px;">
                            <div id="viewUserauditionvideo" style="width:100%; margin:0px auto"></div>
                            <div style="padding-left:325px; margin-top:-33px; color:#337ab7; cursor: pointer;" id="auditionLikeDislikeBlock"></div>
                            <input type="hidden" name="hdnaudlike" id="hdnaudlike" value="1" />
                            <input type="hidden" name="hdnaudid" id="hdnaudid" value="" />
                        </div>                            
                    </div>
                    <div class="form-group">
                        <div style="float: left; width: 444px; padding-left: 30px;">

                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-4">Company Name :</label>
                                <div class="col-xs-8" id="viewcompany" style="padding-top:7px;"></div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-4">Address Line 1:</label>
                                <div class="col-xs-8" id="viewaddressLine1" style="padding-top:7px;"></div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-4">Address Line 2 :</label>
                                <div class="col-xs-8" id="viewaddressLine2" style="padding-top:7px;"></div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-4">City :</label>
                                <div class="col-xs-8" id="viewcity" style="padding-top:7px;"></div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-4">State :</label>
                                <div class="col-xs-8" id="viewstate" style="padding-top:7px;"></div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-4">Country :</label>
                                <div class="col-xs-8" id="viewcountry" style="padding-top:7px;"></div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-4">Pincode :</label>
                                <div class="col-xs-8" id="viewpinCode" style="padding-top:7px;"></div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-4">Phone Number :</label>
                                <div class="col-xs-8" id="viewphone" style="padding-top:7px;"></div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-4">Fax Number :</label>
                                <div class="col-xs-8" id="viewfax" style="padding-top:7px;"></div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-4">Email :</label>
                                <div class="col-xs-8" id="viewemail" style="padding-top:7px;"></div>
                            </div>
                        </div>
                        <div style="float: left; width:444px;">
                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-4">Web :</label>
                                <div class="col-xs-8" id="viewweb" style="padding-top:7px;"></div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-4">About Company :</label>
                                <div class="col-xs-8" id="viewaboutCompany" style="padding-top:7px;"></div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-4">Valid From :</label>
                                <div class="col-xs-8" id="viewvalidFrom" style="padding-top:7px;"></div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-4">Valid To :</label>
                                <div class="col-xs-8" id="viewvalidTo" style="padding-top:7px;"></div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-4">Type :</label>
                                <div class="col-xs-8" id="viewtypeFlag" style="padding-top:7px;"></div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-4">Status :</label>
                                <div class="col-xs-8" id="viewstatusFlag" style="padding-top:7px;"></div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-4">User Limit :</label>
                                <div class="col-xs-8" id="viewuserLimit" style="padding-top:7px;"></div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-4">Company Logo :</label>
                                <div class="col-xs-8" id="viewcompanyLogo" style="padding-top:7px;"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- div class="modal-footer">
                    <button type="button" class="btn btn-primary edit-knowledge-category" data-dismiss="modal">Close</button>
                </div -->
            </div>
        </form>
    </div>
</div>


<script>
    $(document).on('click', '.view-user-profile-details', function () {
        $('#viewcompany').html('');
        $('#viewaddressLine1').html('');
        $('#viewaddressLine2').html('');
        $('#viewcity').html('');
        $('#viewstate').html('');
        $('#viewcountry').html('');
        $('#viewpinCode').html('');
        $('#viewphone').html('');
        $('#viewfax').html('');
        $('#viewemail').html('');
        $('#viewweb').html('');
        $('#viewaboutCompany').html('');
        $('#viewvalidFrom').html('');
        $('#viewvalidTo').html('');
        $('#viewtypeFlag').html('');
        $('#viewstatusFlag').html('');
        $('#viewuserLimit').html('');
        $('#viewcompanyLogo').html('');
        
        
        var userId = $(this).data('options').userId;
        var company = $(this).data('options').company;
        var addressLine1 = $(this).data('options').addressLine1;
        var addressLine2 = $(this).data('options').addressLine2;
        var city = $(this).data('options').city;
        var state = $(this).data('options').state;
        var country = $(this).data('options').country;
        var pinCode = $(this).data('options').pinCode;
        var phone = $(this).data('options').phone;
        var fax = $(this).data('options').fax;
        var email = $(this).data('options').email;
        var web = $(this).data('options').web;
        var aboutCompany = $(this).data('options').aboutCompany;
        var validFrom = $(this).data('options').validFrom;
        var validTo = $(this).data('options').validTo;
        var typeFlag = $(this).data('options').typeFlag;
        var statusFlag = $(this).data('options').statusFlag;
        var userLimit = $(this).data('options').userLimit;
        var companyLogo = $(this).data('options').companyLogo;


        if (companyLogo != "") {
            var profileImage = '<?php echo base_url(); ?>uploads/corporateLogo/' + companyLogo;
            var userImage = "<img src='" + profileImage + "' alt='company logo' class='img-responsive'>";
        } else {
            var userImage = "";
        }

        

        $('#viewcompany').html(company);
        $('#viewaddressLine1').html(addressLine1);
        $('#viewaddressLine2').html(addressLine2);
        $('#viewcity').html(city);
        $('#viewstate').html(state);
        $('#viewcountry').html(country);
        $('#viewpinCode').html(pinCode);
        $('#viewphone').html(phone);
        $('#viewfax').html(fax);
        $('#viewemail').html(email);
        $('#viewweb').html(web);
        $('#viewaboutCompany').html(aboutCompany);
        $('#viewvalidFrom').html(validFrom);
        $('#viewvalidTo').html(validTo);
        $('#viewtypeFlag').html(typeFlag);
        $('#viewstatusFlag').html(statusFlag);
        $('#viewuserLimit').html(userLimit);
        $('#viewcompanyLogo').html(userImage);

        //});
    });
</script>
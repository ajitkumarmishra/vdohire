<?php
if (isset($user) && $user != NULL) {
    // print_r($user);
}
if (isset($user['profileImage']) && $user['profileImage'] != NULL) {
    $profileImage = $user['profileImage'];
} else {
    $profileImage = base_url() . '/theme/images/uploadimage.png';
}
//print_r($permissions);
?>

<div id="page-content-wrapper" style="margin-top: 52px;">
    <form method="post" action="" name="createUser" enctype="multipart/form-data" id="createSubuserForm">
        <?php if (isset($message)): ?>
            <div class="alert alert-success">
                <?php echo $message; ?>
            </div>
        <?php endif; ?>
        <?php if (isset($error)): ?>
            <div class="alert alert-danger">
                <?php foreach ($error as $item): ?>
                    <?php echo $item; ?>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
        <div class="container-fluid whitebg">
            <div class="row">
                <div class="col-md-12">
                    <h5><a href="<?php echo base_url().'users/list' ?>">Recruiters</a> -> Add Recruiter</h5>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12" style="margin-bottom: 8px;">
                    <div class="col-md-4" style="float:left; padding-left: 0px;"><h3 style="font-size: 20px !important;">Add Recruiter</h3></div>
                    <input type="hidden" name="accessToken" value="<?php echo $token; ?>"/>

                    <div class="col-md-8" style="padding-right: 30px;"><button type="submit" class="Save_frm pull-right" style="background: #054f72;color: white;">Save</button></div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12 content_para">
                    <div class="col-md-2 ed_gry_bg">
                        <span class="edit_user_img"><a href="javascript:" class="uploadImage"><input type="file" name="userImage" class="userImageUpload" id="userImage"><img src="<?php echo $profileImage; ?>" alt="" id="uploadIcon" title="" class="img-responsive"></a>
                            <div id="fimageName"> </div> 
                        </span>
                        <input type="hidden" name="profileImage" id="profileImage" value="<?php
                        if (isset($user['profileImage'])): echo $user['profileImage'];
                        endif;
                        ?>" />
                        <input type="hidden" name="image" id="proimageName" value="<?php
                        if (isset($user['image'])): echo $user['image'];
                        endif;
                        ?>"  />
                    </div>

                    <div class="col-md-5 padd_35_btm editfrm">
                        <div class="form-group">
                            <label for="inputEmail" class="control-label col-xs-4 required-field">Name : </label>
                            <div class="col-xs-8">
                                <input type="text"  value="<?php
                                if (isset($user['name'])): echo $user['name'];
                                endif;
                                ?>" name="name" id="name" class="form-control" data-validation="required">
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="inputEmail" class="control-label col-xs-4 required-field">Mobile : </label>
                            <div class="col-xs-8">
                                <input type="text" pattern="[7-9]{1}[0-9]{9}" value="<?php
                                if (isset($user['mobile'])): echo $user['mobile'];
                                endif;
                                ?>" name="mobile" id="mobile" class="form-control" maxlength="10" data-validation="required">
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="inputEmail" class="control-label col-xs-4 required-field">Email : </label>
                            <div class="col-xs-8">
                                <input type="text"  value="<?php
                                if (isset($user['email'])): echo $user['email'];
                                endif;
                                ?>" name="email" id="email" class="form-control" data-validation="required">
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="inputEmail" class="control-label col-xs-4">Location : </label>
                            <div class="col-xs-8">
                                <input type="text"  value="<?php
                                if (isset($user['location'])): echo $user['location'];
                                endif;
                                ?>" name="location" id="location" class="form-control">
                            </div>
                        </div>

                    </div>


                    <div class="col-md-5 padd_35_btm editfrm">

                        <div class="form-group">
                            <label for="inputEmail" class="control-label col-xs-4">Gender : </label>
                            <div class="col-xs-8">
                                <select class="selectpicker" name="gender" data-validation="required">
                                    <option value="1" <?php
                                    if (isset($user['gender']) && $user['gender'] == 1): echo "selected";
                                    endif;
                                    ?>>Male</option>
                                    <option value="2" <?php
                                    if (isset($user['gender']) && $user['gender'] == 2): echo "selected";
                                    endif;
                                    ?>>Female</option>
                                </select>
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="inputEmail" class="control-label col-xs-4">DOB : </label>
                            <div class="col-xs-8">
                                <input type="text"  value="<?php
                                if (isset($user['dob'])): echo $user['dob'];
                                endif;
                                ?>" name="dob" id="datepicker-13" class="form-control pick-date">
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="inputEmail" class="control-label col-xs-4">Status : </label>
                            <div class="col-xs-8">
                                <select class="selectpicker" name="status" data-validation="required">
                                    <option value="1" <?php
                                    if (isset($user['status']) && $user['status'] == 1): echo "selected";
                                    endif;
                                    ?>>Active</option>
                                    <option value="2" <?php
                                    if (isset($user['status']) && $user['status'] == 2): echo "selected";
                                    endif;
                                    ?>>Inactive</option>
                                </select>
                            </div>
                        </div> 
                    </div>

                    <div class="clearfix"></div>
                    <div class="">
                        <div class="col-xs-2"></div>
                        <div class="col-xs-10">
                                <label for="inputEmail" class="control-label col-xs-2" style="margin-top: 9px;">About Recruiter : </label>
                                <div class="col-xs-10 ed_gry_bg" style="padding-left: 10px;">  
                                    <textarea class="content-text ed_area required-field" name="aboutMe" rows="3" cols="30" style="padding:6px 12px;"><?php
                                        if (isset($user['aboutMe'])): echo $user['aboutMe'];
                                        endif;
                                        ?></textarea>
                                </div>

                        </div>
                    </div>

                </div>

                <div class="clearfix"></div>
                <div class="col-md-12 content_para">
                    <div class="col-md-2">
                    </div>
                    <div class="col-md-5 padd_35_btm editfrm ed_gry_bg">
                        <h3 class="para_head" style="font-size: 21px;">Company Details</h3>
                        <?php
                        $userDataHeader = $this->session->userdata['logged_in'];
                        $userLoggeinCompanyHeader = $userDataHeader->company;
                        $userLoggeinAddressHeader = $userDataHeader->addressLine1.', '.$userDataHeader->city;
                        ?>
                        <div class="form-group">
                            <label for="inputEmail" class="control-label col-xs-4 required-field">Designation : </label>
                            <div class="col-xs-8">
                                <input type="text"  value="<?php
                                if (isset($user['designation'])): echo $user['designation'];
                                endif;
                                ?>" name="designation" id="des" class="form-control" data-validation="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail" class="control-label col-xs-4 required-field">Company Name : </label>
                            <div class="col-xs-8">
                                <input type="text"  id="name" name="company" value="<?php
                                if (isset($user['company']))  { echo $user['company']; }
                                else { echo $userLoggeinCompanyHeader; }
                                ?>" class="form-control" data-validation="required">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputEmail" class="control-label col-xs-4 required-field">Address : </label>
                            <div class="col-xs-8">
                                <input type="text" value="<?php
                                if (isset($user['address']))  { echo $user['address']; }
                                else { echo $userLoggeinAddressHeader; }
                                ?>" name="address" id="des" class="form-control" data-validation="required">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5 ed_gry_bg">
                        <h3 class="para_head" style="font-size: 21px;">Permissions</h3>
                        <div class="col-md-12"><a href="javascript:" class="pull-right" id="checkAll">Check All</a></div>
                        <div class="clearfix"></div>
                        <div class="form-horizontal edit_chk">
                                <?php
                                $userData = $this->session->userdata['logged_in'];
                                foreach ($permissions as $item):

                                    if ($item->id == '7') {
                                        continue;
                                    } else {
                                        ?>
                                        <div class="col-md-5">
                                            <label class="checkbox-inline"">
                                                <div class="form-group">
                                                    <input type="checkbox" class="checkbox" name="permissions[]" value="<?php echo $item->id; ?>" <?php
                                                    if (isset($user['permissions']) && in_array($item->id, $user['permissions'])): echo "checked";
                                                    endif;
                                                    ?>>
                                                    <label class="per-name"><?php echo $item->name; ?></label>
                                                </div>
                                            </label>
                                        </div>
                                        <?php
                                    }
                                endforeach;
                                ?>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </form>
</div>

<link href="http://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<!-- Javascript -->
<script>
    

    $("#checkAll").click(function () {
        $(".checkbox").prop('checked', true);
    });

    $(function () {
        //$("#datepicker-13").datepicker();
        $("#datepicker-13").datepicker({
            changeYear: true,
            maxDate: 0
        });
    });

    function readURL(input) {
        //console.log(input.files[0].name);
        var arr = [];
        arr = input.files[0].name.split('.');
        //alert(arr[1]);
        var ext = ['jpg', 'png', 'jpeg'];
        //alert($.inArray( arr[1].toString(), ext ));
        if ($.inArray(arr[1].toString(), ext) == -1)
        {
            alert('Please upload jpg or png!');
            return false;
        }
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                //alert(e.target.result);
                //$('#fimageName').append(input.files[0].name);
                $('#uploadIcon').attr('src', e.target.result);
                $('#profileImage').val(e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(".userImageUpload").change(function () {
        readURL(this);
        var url = '<?php echo base_url(); ?>';
        var file = document.getElementById("userImage").files[0]; //fetch file
        var formData = new FormData();
        formData.append('userImage', file); //append file to formData object
        //alert('Testing!!!');
        $.ajax({
            url: url + "users/imageUpload",
            type: "POST",
            data: formData,
            processData: false, //prevent jQuery from converting your FormData into a string
            contentType: false, //jQuery does not add a Content-Type header for you
            success: function (msg) {
                //alert(msg);
                $('#proimageName').val(msg.trim());
            }
        });
    });
</script>







<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script>
$.validate({
    form: '#createSubuserForm',
    modules: 'file',
    onError: function ($form) {
        return false;
    },
    onSuccess: function ($form) {
        return true;
    },
    validateOnBlur : false,
    errorMessagePosition : 'top',
    scrollToTopOnError : false // Set this property to true on longer forms
});
</script>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Superadminreport Model
 * Description : Handle all the CRUD operation for Superadminreport
 * @author Synergy
 * @createddate : Nov 27, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 30, 2017
 */

class Superadminreport_model extends CI_Model {

    /**
     * Responsable for inherit the parent constructor
     * Responsable for auto load the the session and pagination library
     * @return void
     */
    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('pagination');
    }

    /**
     * Description : Use to list all corporate admin users
     * Author : Synergy
     * @param int $userId.
     * @return array of users data
     */
    function totalClient($userId) {
        try {
            $sql = "SELECT 
                        COUNT(U.id) AS totalClient
                    FROM
                        fj_users U
                    WHERE
                        U.status IN (1,2,3)  AND
                        U.role  = '2'
                        ";
            $query = $this->db->query($sql);
            $result = $query->row_array();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Description : Use to list all active corporate admin users
     * Author : Synergy
     * @param int $userId.
     * @return array of users data
     */
    function activeClient($userId) {
        try {
            $sql = "SELECT 
                        COUNT(U.id) AS activeClient
                    FROM
                        fj_users U
                    WHERE
                        U.status IN (1)  AND
                        U.role  = '2'
                        ";
            $query = $this->db->query($sql);
            $result = $query->row_array();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Description : Use to list all active corporate admin users who have registered in last 1 month
     * Author : Synergy
     * @param int $userId.
     * @return array of users data
     */
    function lastMonthClient($userId) {
        try {
            $sql = "SELECT 
                        COUNT(U.id) AS lastMonthClient
                    FROM
                        fj_users U
                    WHERE
                        U.status IN (1)                                             AND
                        U.role              = '2'                                   AND
                        YEAR(U.createdAt)   = YEAR(CURRENT_DATE - INTERVAL 1 MONTH) AND
                        MONTH(U.createdAt)  = MONTH(CURRENT_DATE - INTERVAL 1 MONTH)
                    ";
            $query = $this->db->query($sql);
            $result = $query->row_array();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Description : Use to list all active corporate admin users who have registered in last 3 month
     * Author : Synergy
     * @param int $userId.
     * @return array of users data
     */
    function lastQuarterClient($userId) {
        try {
            $sql = "SELECT 
                        COUNT(U.id) AS lastQuarterClient
                    FROM
                        fj_users U
                    WHERE
                        U.status IN (1)                                             AND
                        U.role              = '2'                                   AND
                        (U.createdAt)   >= last_day(now()) + interval 1 day - interval 3 month
                        ";
            $query = $this->db->query($sql);
            $result = $query->row_array();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Description : Use to list all active corporate admin users who have registered in last 3 month
     * Author : Synergy
     * @param int $userId.
     * @return array of users data
     */
    function totalApplication($userId) {
        try {
            $sql = "SELECT 
                        COUNT(UJ.id) AS totalApplication
                    FROM
                        fj_userJob UJ
                    ";
            $query = $this->db->query($sql);
            $result = $query->row_array();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    function viewedApplication($userId) {
        try {
            $sql = "SELECT 
                        COUNT(UJ.id) AS viewedApplication
                    FROM
                        fj_userJob UJ
                    WHERE
                        UJ.status!='1'
                    ";
            $query = $this->db->query($sql);
            $result = $query->row_array();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    function pendingApplication($userId) {
        try {
            $sql = "
                    SELECT COUNT(id) AS pendingApplication
                    FROM
                        (
                        SELECT 
                            UJ.shorlistedByL1User,
                            UJ.shorlistedByL2User,
                            UJ.id,
                            U.fullname  AS AppUserName,
                            U.email     AS AppUserEmail,
                            U.mobile    AS AppUserMobile,
                            J.fjCode    AS JobCode,
                            J.title     AS JobTitle,
                            (CASE UJ.status WHEN 1 THEN 'Pending' WHEN 2 THEN 'Shortlisted By L1' WHEN 3 THEN 'Rejected By L1' WHEN 4 THEN 'Shortlisted By L2'  WHEN 5 THEN 'Rejected By L2' ELSE '' END) as JobAplliedStatus,
                            (CASE COUNT(DISTINCT(JP.level)) WHEN 1 THEN 'L1' WHEN 2 THEN 'L2' ELSE '' END) AS JobLevel,
                            J.createdBy,
                            UJ.createdAt                                     
                        FROM 
                            fj_userJob      UJ
                        JOIN 
                            fj_jobPanel     JP,
                            fj_users        U,
                            fj_jobs         J
                        WHERE
                            UJ.jobId=JP.jobId   AND
                            UJ.userId=U.id      AND
                            UJ.jobId=J.id
                        GROUP BY UJ.id ORDER BY UJ.jobId 
                        ) FT
                    WHERE
                        (FT.JobAplliedStatus='Pending') OR
                        (FT.JobAplliedStatus='Shortlisted By L1' AND JobLevel='L2')
                        ";
            $query = $this->db->query($sql);
            $result = $query->row_array();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    function shortlistedApplication($userId) {
        try {
            $sql = "
                    SELECT COUNT(id) AS shortlistedApplication
                    FROM
                        (
                        SELECT 
                            UJ.shorlistedByL1User,
                            UJ.shorlistedByL2User,
                            UJ.id,
                            U.fullname  AS AppUserName,
                            U.email     AS AppUserEmail,
                            U.mobile    AS AppUserMobile,
                            J.fjCode    AS JobCode,
                            J.title     AS JobTitle,
                            (CASE UJ.status WHEN 1 THEN 'Pending' WHEN 2 THEN 'Shortlisted By L1' WHEN 3 THEN 'Rejected By L1' WHEN 4 THEN 'Shortlisted By L2'  WHEN 5 THEN 'Rejected By L2' ELSE '' END) as JobAplliedStatus,
                            (CASE COUNT(DISTINCT(JP.level)) WHEN 1 THEN 'L1' WHEN 2 THEN 'L2' ELSE '' END) AS JobLevel,
                            J.createdBy,
                            UJ.createdAt                            
                        FROM 
                            fj_userJob      UJ
                        JOIN 
                            fj_jobPanel     JP,
                            fj_users        U,
                            fj_jobs         J
                        WHERE
                            UJ.jobId=JP.jobId   AND
                            UJ.userId=U.id      AND
                            UJ.jobId=J.id                                     
                        GROUP BY UJ.id ORDER BY UJ.jobId 
                        ) FT
                    WHERE
                        (FT.JobAplliedStatus='Shortlisted By L1' AND JobLevel='L1') OR
                        (FT.JobAplliedStatus='Shortlisted By L2' AND JobLevel='L2')
                        ";
            $query = $this->db->query($sql);
            $result = $query->row_array();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    
    
    
    
    
    function totalJobs($userId) {
        try {
            $sql = "SELECT 
                        COUNT(J.id) AS totalJobs
                    FROM
                        fj_jobs J
                    WHERE
                        status!=3
                    ";
            $query = $this->db->query($sql);
            $result = $query->row_array();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    function auditionViewed($userId) {
        try {
            $sql = "SELECT 
                        COUNT(A.id) AS auditionViewed
                    FROM
                        fj_jobs A
                    ";
            $query = $this->db->query($sql);
            $result = $query->row_array();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    function invitedUsers($userId) {
        try {
            $sql = "SELECT 
                        COUNT(IU.id) AS invitedUsers
                    FROM
                        fj_jobInvitationForUser IU
                    ";
            $query = $this->db->query($sql);
            $result = $query->row_array();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    
    
    
    
    
    function totalAppUser($userId) {
        try {
            $sql = "SELECT 
                        COUNT(U.id) AS totalAppUser
                    FROM
                        fj_users U
                    WHERE
                        U.role='3'
                    ";
            $query = $this->db->query($sql);
            $result = $query->row_array();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    function totalAudition($userId) {
        try {
            $sql = "SELECT 
                        COUNT(UA.id) AS totalAudition
                    FROM
                        fj_userAudition UA
                    ";
            $query = $this->db->query($sql);
            $result = $query->row_array();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    function loggedInUser($userId) {
        try {
            $sql = "SELECT 
                        COUNT(UV.visitorId) AS loggedInUser
                    FROM
                        fj_visitor UV
                    WHERE
                        createdAt = CURDATE() - INTERVAL 1 DAY AND CURDATE()
                    ";
            $query = $this->db->query($sql);
            $result = $query->row_array();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Userauditionlike Model
 * Description : Handle all the CRUD operation for Userauditionlike
 * @author Synergy
 * @createddate : Nov 27, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 27, 2017
 */

class Userauditionlike_model extends CI_Model {

    /**
     * Initialize the variables
     */
    var $table = "fj_userAuditionLike";

    /**
     * Responsable for inherit the parent constructor
     * Responsable for auto load the the session and pagination library
     * @return void
     */
    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('pagination');
    }

    /**
     * checkAlreadyLikeAudition function
     * Author : Synergy
     * Discription : Use to check a logged in user has liked an audition or not
     * @param int $userId, int $auditionId
     * @return array the user's audition data.
     */
    function checkAlreadyLikeAudition($userId, $auditionId) {
        try {
            $this->db->select('*');
            $this->db->from($this->table);
            $this->db->where('userId', $userId);
            $this->db->where('auditionId', $auditionId);
            $query = $this->db->get();
            $result = $query->row_array();
            if (count($result) == 0) {
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * likeUserAudition function
     * Author : Synergy
     * Discription : Use to like an audition by logged in user
     * @param array $data
     * @return boolean or error message
     */
    function likeUserAudition($data) {       
        
        try {
            $str = $this->db->insert_string($this->table, $data);

            $query = $this->db->query($str);

            if ($query) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * deleteLikedAudition function
     * Author : Synergy
     * Discription : Use to delete a liked audition by logged in user
     * @param array $data
     * @return boolean or error message
     */
    function deleteLikedAudition($data) {
        try {
            $this->db->where('userId', $data['userId']);
            $this->db->where('auditionId', $data['auditionId']);
            $resultData = $this->db->delete($this->table);
            return $resultData;
            
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
}
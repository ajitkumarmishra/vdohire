<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Useraudition Model
 * Description : Handle all the CRUD operation for Useraudition
 * @author Synergy
 * @createddate : Nov 27, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 27, 2017
 */

class Useraudition_model extends CI_Model {

    /**
     * Initialize the variables
     */
    var $table = "fj_userAudition";

    /**
     * Responsable for inherit the parent constructor
     * Responsable for auto load the the session and pagination library
     * @return void
     */
    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('pagination');
    }

    /**
     * GetUserAuditionVideo function
     * Gets a specific user audition video.
     * @author Synergy
     * @param int $userId.
     * @return array the user's audition data.
     */
    function getUserAuditionVideo($userId) {
        try {
            $this->db->select('*');
            $this->db->from($this->table);
            $this->db->where('userId', $userId);
            $this->db->where('status', 1);
            $query = $this->db->get();
            $result = $query->row_array();
            if (count($result) == 0) {
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
}
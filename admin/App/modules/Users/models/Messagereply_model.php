<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Messagereply Model
 * Description : Handle all the CRUD operation for Messagereply
 * @author Synergy
 * @createddate : Nov 26, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 27, 2017
 */

class Messagereply_model extends CI_Model {

    /**
     * Initialize the variables
     */
    var $table = "fj_message_reply";

    /**
     * Responsable for inherit the parent constructor
     * Responsable for auto load the the session and pagination library
     * @return void
     */
    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('pagination');
    }

    /**
     * getMessage function
     * Discription : Use to get a all reply messages for a specific parent message
     * Author : Synergy
     * @param int $id.
     * @return array of message data.
     */
    function getMessage($id) {
        try {
            $this->db->select('*');
            $this->db->from($this->table);
            $this->db->where('messageId', $id);
            $query = $this->db->get();
            $result = $query->row_array();
            if (count($result) == 0) {
                throw new Exception('Unable to fetch message data.');
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * listMessage function
     * Discription : Use to list all the messages from reply table
     * Author : Synergy
     * @param int $limit, int $offset, string $serachColumn, string $searchText, string $createdBy
     * @return array of message data.
     */
    function listMessage($limit, $offset, $serachColumn = NULL, $searchText = NULL, $createdBy = NULL) {
        try {
            $currentDate = date('Y-m-d h:i:s');
            $sql = "select MR.* from fj_message_reply as MR";
                        
            if(isset($offset)) {
                $sql .= " limit " .$offset. ", " .$limit."";
            }

            $query = $this->db->query($sql);
            $result = $query->result();

            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * addMessageReply function
     * Discription : Use to create reply message on a specific parent message
     * Author : Synergy
     * @param array $data
     * @return int or boolean
     */
    function addMessageReply($data) {
        $resp = $this->db->insert($this->table, $data);
        $messageReplyId = $this->db->insert_id();
        if ($resp) {
            return $messageReplyId;
        } else {
            return false;
        }
    }
    
    /**
     * countUnreadMessage function
     * Discription : Use to get total unread messages for a user on specific parent(main) message
     * Author : Synergy
     * @param int $messageId, int $userId
     * @return array of count data or error string 
     */
    function countUnreadMessage($messageId, $userId) {
        
        try {
            
            $query = $this->db->select("count(messageReplyId) as unreadcount")
                                ->from($this->table)
                                ->where('messageId =', $messageId)
                                ->where('messageToId =', $userId)
                                ->where('messageReadStatus = 0 or messageReadStatus = 1')
                                ->get();
            
            $result = $query->row_array();
                        
            if (!$result) {
                throw new Exception('Unable to fetch unread message data.');
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * countUnreadTopBarMessage function
     * Discription : Use to get total unread messages which have status 0 for a user on specific parent(main) message
     * Author : Synergy
     * @param int $userId
     * @return array of count data or error string 
     */
    function countUnreadTopBarMessage($userId) {
        
        try {
            
            $query = $this->db->select("count(messageReplyId) as unreadcount")
                                ->from($this->table)
                                ->where('messageToId =', $userId)
                                ->where('messageReadStatus = 0')
                                ->get();
            
            $result = $query->row_array();
           
            if (!$result) {
                throw new Exception('Unable to fetch unread message data.');
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * updateMessageForTopBarRead function
     * Discription : Ues to update replay messages which have status 0
     * Author : Synergy
     * @param array $messagedata.
     * @param int $userId.
     * @return boolean.
     */
    function updateMessageForTopBarRead($messagedata, $userId) {
        
        try {
            $this->db->where('messageToId', $userId);
            
            if( $messagedata['messageReadStatus'] == 1 ){
                $this->db->where('messageReadStatus', 0);
            }
            
            $resp = $this->db->update($this->table, $messagedata);
            
            if (!$resp) {
                throw new Exception('Unable to update message data.');
                return false;
            }
            return $id;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * updateMessageForPageRead function
     * Discription : Use to update status of reply messages for specific userId and on specific parent message
     * Author : Synergy
     * @param array $messagedata.
     * @param int $messageId.
     * @param int $userId.
     * @return boolean.
     */
    function updateMessageForPageRead($messagedata, $messageId, $userId) {
        try {
            $this->db->where('messageId', $messageId);
            $this->db->where('messageToId', $userId);
            $resp = $this->db->update($this->table, $messagedata);
            if (!$resp) {
                throw new Exception('Unable to update message data.');
                return false;
            }
            return $messageId;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * getUserMessageToView function
     * Discription : Ues to get details of a particular parent message 
     * Author : Synergy
     * @param array $messageId.
     * @param int $loggedinUserId.
     * @param int $userId.
     * @return array of data.
     */
    function getUserMessageToView($messageId, $loggedinUserId, $userId) {
        
        try {
            
            $query = $this->db->select("MR.*, U1.fullname as senderfullname, U1.id as senderId, U1.email as senderemail, U2.fullname as receiverfullname, U2.id as receiverId, U1.email as receiveremail")
                                ->from($this->table.' as MR')
                                ->join('fj_users as U1', 'U1.id = MR.replyById', 'left')
                                ->join('fj_users as U2', 'U2.id = MR.messageToId', 'left')
                                ->where('MR.messageId =', $messageId)
                                ->order_by('MR.messageReplyId', "asc");
            $selectqry = $query->get();
            
            $result = $selectqry->result_array();
           
            if (!$result) {
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : User Model
 * Description : Handle all the CRUD operation for User
 * @author Synergy
 * @createddate : Nov 23, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 24, 2017
 */
class User_model extends CI_Model {

    /**
     * Initialize the variables
     */
    var $table = "users";

    /**
     * Responsable for inherit the parent constructor
     * Responsable for auto load the the session and pagination library
     * @return void
     */
    function __construct() {
        parent::__construct();
        $this->load->library("session");
        $this->load->library('pagination');
        $this->load->model('useraudition_model');
        $this->load->model('userauditionlike_model');
    }
	
	

    /**
     * Description : Use to create user
     * Author : Synergy
     * @param array $data
     * @return boolean
     */
    function create($data) {
        $str    = $this->db->insert_string($this->table, $data);
        $query  = $this->db->query($str);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }
	
	/**
     * Description : Use to insert the data in the sessionTable
     * Author : Synergy
     * @param array $data
     * @return boolean
     */
    function InsertUserSession($data) {
		
        $str    = $this->db->insert_string('fj_userSession', $data);
        $query  = $this->db->query($str);
		
        if ($query) {
            return true;
        } else {
            return false;
        }
    }
	
	/**
     * Description : Use to insert the data in the sessionTable
     * Author : Synergy
     * @param array $data
     * @return boolean
     */
    function UpdateUserSession($userId) {
		$data['status'] = '2';
		$data['logoutTime'] = date('Y-m-d H:i:s');
        $str    = $this->db->where('userId', $userId)->update('fj_userSession', $data);
        
    }
	

    /**
     * Description : Use to list limited users
     * Author : Synergy
     * @param int $showPerpage, int $offset
     * @return array of data
     */
    function read($showPerpage, $offset) {
        $this->db->limit($showPerpage, $offset);
        $this->db->from($this->table);
        $query = $this->db->get();
        return $query->result();
    }
	
	
	/**
     * Description : Use to check Multplie Login of the Users
     * Author : Synergy
     * @param int $showPerpage, int $offset
     * @return array of data
     */
    function checkMultipleLogin($userId) {
        $this->db->where('userId', $userId);
        $this->db->where('status', '1');
		
		$lastAciveTime = date('Y-m-d H:i:s', time() - 600);
		
        //$this->db->where('timeout < "'.$lastAciveTime.'"');
		$query = $this->db->get('fj_userSession');
		//echo $this->db->last_query();die;
		$countResult = $query->result_array();
		//echo "<pre>";print_r($countResult);die;
		if(count($countResult) > 0){
			if($countResult[0]['timeout'] < $lastAciveTime){
				$this->UpdateUserSession($userId);
				return 0;
			}else{
				return 1;
			}
		}else{
			return $query->num_rows();
		}
		
		
        
    }

    /**
     * Description : Use to count all the users
     * Author : Synergy
     * @param none
     * @return array of data
     */
    function count_all() {
        $query = $this->db->get($this->table);
        return $query->num_rows();
    }

    /**
     * Description : Use to get details of specific user
     * Author : Synergy
     * @param int $id
     * @return array of data or boolean
     */
    function user_by_id($id) {
        $this->db->where('id', $id);
        $query = $this->db->get($this->table);
        $query->row()->role = $this->get_role($id);
        $query->row()->role_name = $this->get_role_name($query->row()->role);

        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    /**
     * Description : Use to get details by nicename
     * Author : Synergy
     * @param string
     * @return array of data or boolean
     */
    function user_by_nicename($user_nicename) {
        $this->db->where('user_nicename', $user_nicename);
        $query = $this->db->get($this->table);
        if ($query->num_rows() > 0) {
            return $this->user_by_id($query->row()->id);
        } else {
            return false;
        }
    }

    /**
     * Description : Use to get details by nicename
     * Author : Synergy
     * @param string
     * @return array of data or boolean
     */
    function user_by_email($email) {
        $this->db->where('email', $email);
        $query = $this->db->get('fj_users');
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Description : Use to get details by email
     * Author : Synergy
     * @param string
     * @return array of data or boolean
     */
    function get_user_by_email($email) {
        $this->db->select('id');
        $this->db->from('fj_users');
        $this->db->where('email', $email);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row('id');
        } else {
            return false;
        }
    }

    /**
     * Description : Use to get details by email
     * Author : Synergy
     * @param string
     * @return array of data or boolean
     */
    function get_user_by_email1($email) {
        $this->db->select('id');
        $this->db->from('fj_users');
        $this->db->where('email', $email);
        $this->db->where_in('role', array('1','2','4','5'));
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row('id');
        } else {
            return false;
        }
    }

    /**
     * Description : Use to get details by email
     * Author : Synergy
     * @param string
     * @return array of data or boolean
     */
    function getMyAccountInfo($userId) {
        $this->db->select('*');
        $this->db->from('fj_account');
        $this->db->where('userId', $userId);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    /**
     * Description : Use to update details of a specific user
     * Author : Synergy
     * @param int $userid, array $userdata
     * @return array of data
     */
    function update($userid, $userdata) {
        $data = (array) $userdata;
        $where = "id = $userid";
        $str = $this->db->update_string($this->table, $data, $where);
        $query = $this->db->query($str);
        return $query;
    }

    /**
     * Description : Unusable function
     */
    function delete() {
        
    }

    /**
     * Description : Use to get role of a user
     * Author : Synergy
     * @param int $user_id
     * @return int
     */
    function get_role($user_id) {
        $this->db->where('user_id', $user_id);
        $query = $this->db->get('users_roles');
        if ($query->num_rows() > 0) {
            return (int) $query->row()->role_id;
        } else {
            return 0;
        }
    }

    /**
     * Description : Use to get all roles
     * Author : Synergy
     * @param none
     * @return array of data
     */
    function get_roles() {
        $query = $this->db->get('roles');
        return $query->result();
    }

    /**
     * Description : Use to get role name
     * Author : Synergy
     * @param int $role_id
     * @return array of data
     */
    function get_role_name($role_id) {
        $this->db->where('id', $role_id);
        $query = $this->db->get('roles');
        if ($query->num_rows() > 0) {
            return $query->row()->name;
        } else {
            return false;
        }
    }

    /**
     * Description : Use to validate email and password of user
     * Author : Synergy
     * @param string $user_email, string $password
     * @return array of data or boolean
     */
    function validate($user_email, $password) {
        $this->db->where('user_email', $user_email);
        $this->db->where('user_pass', $password);
        $query = $this->db->get($this->table);
        if ($query->num_rows() === 1) {
            return $query->row();
        } else {
            return false;
        }
    }

    /**
     * Description : Use to new create user
     * Author : Synergy
     * @param array $_POST
     * @return boolean
     */
    function create_user() {
        $data['user_login'] = $this->input->post('user_login', true);
        $data['user_pass'] = md5($this->input->post('user_pass', true));
        $data['user_nicename'] = $this->input->post('user_nicename', true);
        $data['user_email'] = $this->input->post('user_email', true);
        $data['activation_key'] = md5(rand(0, 1000) . 'uniquefrasehere');
        $data['status'] = '1';
        $this->db->set('registered_date', 'NOW()', false);
        $query = $this->db->insert($this->table, $data);
        if ($query) {
            $user_id = $this->db->insert_id();
            $user_value = $this->input->post('user_role');
            $this->update_role($user_id, $user_value);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Description : User to insert role of a user
     * Author : Synergy
     * @param int $userID, int $value
     * @return array of data
     */
    function update_role($userID, $value) {
        $data = array(
            'user_id' => $userID,
            'role_id' => $value
        );
        $this->db->insert('users_roles', $data);
    }

    /**
     * Description : User to update status of a user
     * Author : Synergy
     * @param array $_POST
     * @return boolean
     */
    function updateStatus() {

        if ($this->input->post('Deactivate') != null) {
            $status = "0";
        } else if ($this->input->post('Activate') != null) {
            $status = "1";
        }
        $data = array(
            'status' => $status
        );

        $arr_ids = $this->input->post('arr_ids');
        if (!empty($arr_ids)) {
            $this->db->where_in('id', $arr_ids);
            $this->db->update($this->table, $data);
        }
    }

    /**
     * Description : User to update password of a user
     * Author : Synergy
     * @param string $key, array $_POST
     * @return boolean
     */
    function reset_password($key) {
        $data = array(
            'user_pass' => md5($this->input->post('user_password')),
        );
        $this->db->where('activation_key', $key);
        $query = $this->db->update($this->table, $data);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Description : User to get key of a user
     * Author : Synergy
     * @param string $email
     * @return array of data
     */
    function getUserKey($email) {
        $this->db->where('user_email', $email);
        $query = $this->db->get($this->table);
        return $query->row()->activation_key;
    }

    /**
     * addUser function
     * Discription : User to create a user
     * Author : Synergy
     * @params array $data
     * @return int or boolean
     */
    function addUser($data) {
        $resp = $this->db->insert('fj_users', $data);
        $userId = $this->db->insert_id();
        if ($resp) {
            return $userId;
        } else {
            return false;
        }
    }

    /**
     * fileUpload function
     * Discription : Use to upload a file to directory
     * Author : Synergy
     * @param array $files
     * @param string $uploads_dir
     * @param string $imagename
     * @return string
     */
    public function fileUpload($files, $uploads_dir, $imagename) {
        try {
            $tmp_name = $files["userImage"]["tmp_name"];
            if (move_uploaded_file($tmp_name, $uploads_dir . "/" . $imagename)) {
                return $imagename;
            } else {
                //throw new Exception('Could not upload file');
            }
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    /**
     * imageUpload function
     * Discription : Uploads a file to directory
     * Author : Synergy
     * @param array $files
     * @param string $uploads_dir
     * @param string $imagename
     * @return string
     */
    public function imageUpload($tempName, $uploads_dir, $imagename) {
        try {

            if (move_uploaded_file($tempName, $uploads_dir . "/" . $imagename)) {
                return $imagename;
            } else {
                //throw new Exception('Could not upload file');
            }
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    /**
     * UpdateUser function
     * Discription : Updates a specific user by his/her id.
     * Author : Synergy
     * @param array $data.
     * @param int $id.
     * @return boolean or string or int.
     */
    function updateUser($data, $id) {
        try {
            $this->db->select('*');
            $this->db->from('fj_users');
            $this->db->where('id', $id);
            $query1 = $this->db->get();
            $result = $query1->row_array();
            if (count($result) != 0) {
                $this->db->select('*');
                $this->db->from('fj_users');
                $this->db->where('email=', $data['email']);
                $this->db->where('id!=', $id);
                $query2 = $this->db->get();
                $countResult = $query2->result_array();
                if (count($countResult) == 0) {
                    $this->db->where('id', $id);
                    $resp = $this->db->update('fj_users', $data);
                    if (!$resp) {
                        throw new Exception('Unable to update user data.');
                        return false;
                    }
                    return $id;
                }
                else {
                    return 'Email Id Already Exists';
                }
            }
            else {
                return 'Record Not Found';
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * updateUserByEmail function
     * Discription : Updates a specific user by his/her email id.
     * Author : Synergy
     * @param array $data.
     * @param int $id.
     * @return boolean or int.
     */
    function updateUserByEmail($data, $email) {
        try {
            $this->db->where('email', $email);
            $resp = $this->db->update('fj_users', $data);
            if (!$resp) {
                // throw new Exception('Unable to update user data.');
                return false;
            }
            return $id;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * GetUser function
     * Discription : Gets a specific user detail.
     * Author : Synergy
     * @param int $id.
     * @return array the users data.
     */
    function getUser($id) {
        try {
            $this->db->select('*');
            $this->db->from('fj_users');
            $this->db->where('id', $id);
            $query = $this->db->get();
            $result = $query->row_array();
            if (count($result) == 0) {
                //throw new Exception('Unable to fetch user data.');
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * GetUserByKey function
     * Discription : Gets a specific user detail.
     * Author : Synergy
     * @param int $id.
     * @return array the users data.
     */
    function getUserByKey($key) {
        try {
            $this->db->select('*');
            $this->db->from('fj_users');
            $this->db->where('activationKey', $key);
            $query = $this->db->get();
            $result = $query->row_array();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * getUserByCreatedBy function
     * Discription : Use to get all users which are created by a specific user
     * Author : Synergy
     * @param int $id.
     * @return array of users data.
     */
    function getUserByCreatedBy($id) {
        try {
            $this->db->select('*');
            $this->db->from('fj_users');
            $this->db->where('createdBy', $id);
            $query = $this->db->get();
            $result = $query->row_array();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * listUsers function
     * Discription : Use to list specified roles of users
     * Author : Synergy
     * @param int $limit, int $offset, string $serachColumn, string $searchText, int $createdBy
     * @return array of users data.
     */
    function listUsers($limit = NULL, $offset = NULL, $serachColumn = NULL, $searchText = NULL, $createdBy = NULL, $comResult) {
        try {
            $userData = $this->session->userdata['logged_in'];
            $userRole = $userData->role;
            
            $sql = "
                    SELECT  U.*,
                            (SELECT C.company FROM fj_users C WHERE U.createdBy=C.id) AS corporateName 
                    FROM fj_users   U
                    WHERE 
                        U.role!=3
                    ";
                        
            if($userRole=='1' && ($this->router->fetch_method()=='superAdminDashboard')) {
                $sql .= " AND U.role NOT IN('1','3','4') ";
            }
            if($userRole=='1' && ($this->router->fetch_method()=='listUsers')) {
                $sql .= " AND U.role NOT IN('1','3','2') ";
            }
            if($userRole!='1' && ($this->router->fetch_method()=='superAdminDashboard' || $this->router->fetch_method()=='listUsers')) {
                $sql .= "   AND U.role NOT IN('1','2','3')
                            AND U.status != '3' ";
            }
            if (isset($createdBy) && $createdBy != NULL && $userRole!='1' && $this->router->fetch_method()!='superAdminDashboard') {
                $sql .= " AND U.createdBy = '$createdBy' ";
            }

            if($userRole=='1') {
                if ($serachColumn != NULL && trim($searchText) != NULL && trim($searchText) != '') {
                    $sql .= " AND U.$serachColumn LIKE '%".trim($searchText)."%' ";
                }
            } else {
                if(trim($searchText) != NULL && trim($searchText) != '') {
                    $sql .= " AND (";
                    $sql .= " U.fullname LIKE '%".trim($searchText)."%' ";
                    $sql .= " OR U.location LIKE '%".trim($searchText)."%' ";
                    $sql .= " OR U.mobile LIKE '%".trim($searchText)."%' ";
                    $sql .= ") ";
                }
            }

            
            $sql .= " ORDER BY id desc";
            
            $query          = $this->db->query($sql);       
            $total_records  = $query->num_rows();            

            $config["base_url"]         = base_url() . $this->uri->segment(1). "/" . $this->uri->segment(2) . "/";
            $config["total_rows"]       = $total_records;
            $config["per_page"]         = 50;
            $page                       = $this->uri->segment(3, 0);
            $config['uri_segment']      = 3;
            $config['full_tag_open']    = '<ul class="pagination">';
            $config['full_tag_close']   = '</ul>';
            $config['first_link']       = false;
            $config['last_link']        = false;
            $config['first_tag_open']   = '<li>';
            $config['first_tag_close']  = '</li>';
            $config['prev_link']        = '&laquo';
            $config['prev_tag_open']    = '<li class="prev">';
            $config['prev_tag_close']   = '</li>';
            $config['next_link']        = '&raquo';
            $config['next_tag_open']    = '<li>';
            $config['next_tag_close']   = '</li>';
            $config['last_tag_open']    = '<li>';
            $config['last_tag_close']   = '</li>';
            $config['cur_tag_open']     = '<li class="active"><a href="#">';
            $config['cur_tag_close']    = '</a></li>';
            $config['num_tag_open']     = '<li>';
            $config['num_tag_close']    = '</li>';
            $this->pagination->initialize($config);

            $start_limit = ($page) * 1;
            if ($start_limit > 1) {
                $start_limit = $start_limit;
            } else {
                $start_limit = 0;
            }

            if($comResult != 'completeResult') {
                $sql .= " LIMIT " .$config["per_page"]. " OFFSET " .$start_limit."";
            }
            
            $query = $this->db->query($sql);
            $result = $query->result();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * listCorporates function
     * Discription : Use to list admin(role=2) role users
     * Author : Synergy
     * @param int $limit, int $offset, string $serachColumn, string $searchText, int $createdBy
     * @return array of users data.
     */
    function listCorporates($limit = NULL, $offset = NULL, $serachColumn = NULL, $searchText = NULL, $createdBy = NULL, $createdByRole = NULL) {
        try {
            $sql = "
                    SELECT  U.*
                    FROM fj_users   U
                    WHERE 
                        U.role=2
                    ";
            
            if ($serachColumn != NULL && trim($searchText) != NULL && $serachColumn != '' && trim($searchText) != '' ) {
                if($serachColumn=='location') {
                    $sql .= " AND ( U.city LIKE '%".trim($searchText)."%' OR U.state LIKE '%".trim($searchText)."%' OR U.country LIKE '%".trim($searchText)."%' ) ";
                }
                else if($serachColumn=='company') {
                    $sql .= " AND ( U.fullname LIKE '%".trim($searchText)."%' OR U.company LIKE '%".trim($searchText)."%' ) ";
                }
                else if($serachColumn=='mobile') {
                    $sql .= " AND ( U.phone LIKE '%".trim($searchText)."%' ) ";
                }
            }
            
            $sql    .= " ORDER BY id desc";
            
            $query          = $this->db->query($sql);            
            $total_records  = $query->num_rows();            

            $config["base_url"]         = base_url() . $this->uri->segment(1). "/" . $this->uri->segment(2) . "/" . $this->uri->segment(3) . "/";
            $config["total_rows"]       = $total_records;
            $config["per_page"]         = 50;
            $page                       = $this->uri->segment(4, 0);
            $config['uri_segment']      = 4;
            $config['full_tag_open']    = '<ul class="pagination">';
            $config['full_tag_close']   = '</ul>';
            $config['first_link']       = false;
            $config['last_link']        = false;
            $config['first_tag_open']   = '<li>';
            $config['first_tag_close']  = '</li>';
            $config['prev_link']        = '&laquo';
            $config['prev_tag_open']    = '<li class="prev">';
            $config['prev_tag_close']   = '</li>';
            $config['next_link']        = '&raquo';
            $config['next_tag_open']    = '<li>';
            $config['next_tag_close']   = '</li>';
            $config['last_tag_open']    = '<li>';
            $config['last_tag_close']   = '</li>';
            $config['cur_tag_open']     = '<li class="active"><a href="#">';
            $config['cur_tag_close']    = '</a></li>';
            $config['num_tag_open']     = '<li>';
            $config['num_tag_close']    = '</li>';

            $start_limit = ($page) * 1;
            if ($start_limit > 1) {
                $start_limit = $start_limit;
            } else {
                $start_limit = 0;
            }
            $sql .= " LIMIT " .$config["per_page"]. " OFFSET " .$start_limit."";            

            $query = $this->db->query($sql);
            $result = $query->result();
            $this->pagination->initialize($config);
            
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * listAppUsers function
     * Discription : Use to list app users
     * Author : Synergy
     * @param int $limit, int $offset, string $serachColumn, string $searchText
     * @return array of users data.
     */
    function listAppUsers($limit = NULL, $offset = NULL, $searchColumn = NULL, $searchText = NULL) {
        try {             
            $this->session->unset_userdata('some_name');
            $userData = $this->session->userdata['logged_in'];
            $userRole = $userData->role;
            $sql = "
                SELECT
                    *
                FROM
                    (
                    SELECT  
                        U.id,
                        U.fullname                                                  AS UserName,
                        U.email                                                     AS UserEmail,
                        U.mobile                                                    AS UserMobile,
                        U.dob                                                       AS DateOfBirth,
                        (CASE WHEN U.gender='1' THEN 'Male' ELSE 'Female' END)      AS Gender,
                        U.pinCode                                                   AS Pincode,
                        U.facebookLink                                              AS FacebookURL,
                        U.linkedInLink                                              AS LinkedInURL,
                        U.twitterLink                                               AS TwitterURL,
                        U.googlePlusLink                                            AS GooglePlusURL,
                        U.expectedCtcFrom                                           AS ExpectedCtcFrom,
                        U.expectedCtcTo                                             AS ExpectedCtcTo,
                        U.workExperience                                            AS WorkExperience,
                        GROUP_CONCAT(DISTINCT(UPL.locationName))                    AS PreferredLocations, 
                        GROUP_CONCAT(CONCAT(C.name) SEPARATOR ' || ')               AS CourseName,
                        GROUP_CONCAT(CONCAT(UQ.completionYear) SEPARATOR ' || ')    AS CompletionYear,
                        GROUP_CONCAT(CONCAT(UQ.percent) SEPARATOR ' || ')           AS Percent,     
                        GROUP_CONCAT(CONCAT(UQ.universityName) SEPARATOR ' || ')    AS UniversityName,                   
                        U.adharCard,
                        U.languages,
                        U.fullname,
                        U.image,
                        U.email,
                        U.mobile,
                        U.dob,
                        U.facebookLink,
                        U.linkedInLink,
                        U.twitterLink,
                        U.googlePlusLink                      
                    FROM 
                        fj_users U 
                    JOIN 
                        fj_userQualification    UQ, 
                        fj_userPreferLocations  UPL,
                        fj_courses		C
                    WHERE 
                        UQ.courseId=C.id    AND 
                        U.id=UQ.userId      AND 
                        U.id=UPL.userId     AND 
                        U.role='3'          AND 
                        U.status='1'        AND    
                        UPL.status='1'      AND 
                        UQ.status='1'   
                    GROUP BY U.id
                    ) AliasTable
                WHERE 
                    1=1 ";
        
            if ($searchColumn != NULL && $searchText != NULL) {
                
                if ($searchColumn=='location' && $searchText != NULL) {
                    $sql .=" AND AliasTable.PreferredLocations LIKE '%$searchText%'";
                }
                if ($searchColumn=='qualification' && $searchText != NULL) {
                    $sql .=" AND AliasTable.CourseName LIKE '%$searchText%'";
                }
                if ($searchColumn=='workExperience' && $searchText != NULL) {
                    $sql .=' AND AliasTable.workExperience =\'' . $searchText . '\'';
                } 
                if ($searchColumn=='Gender' && $searchText != NULL) {
                    $sql .=' AND AliasTable.gender =\'' . $searchText . '\'';
                }
                if ($searchColumn=='fullname' && $searchText != NULL) {
                    $sql .=" AND AliasTable.fullname LIKE '%$searchText%'";
                }
            }
            $sql .= " ORDER BY fullname ";
            $this->session->set_userdata(array('searchData' => $sql));
            
            $query          = $this->db->query($sql);            
            $total_records  = $query->num_rows();
            
            $config["base_url"]         = base_url() . $this->uri->segment(1). "/" . $this->uri->segment(2) . "/";
            $config["total_rows"]       = $total_records;
            $config["per_page"]         = 50;
            $page                       = $this->uri->segment(3, 0);
            $config['uri_segment']      = 3;
            $config['full_tag_open']    = '<ul class="pagination">';
            $config['full_tag_close']   = '</ul>';
            $config['first_link']       = false;
            $config['last_link']        = false;
            $config['first_tag_open']   = '<li>';
            $config['first_tag_close']  = '</li>';
            $config['prev_link']        = '&laquo';
            $config['prev_tag_open']    = '<li class="prev">';
            $config['prev_tag_close']   = '</li>';
            $config['next_link']        = '&raquo';
            $config['next_tag_open']    = '<li>';
            $config['next_tag_close']   = '</li>';
            $config['last_tag_open']    = '<li>';
            $config['last_tag_close']   = '</li>';
            $config['cur_tag_open']     = '<li class="active"><a href="#">';
            $config['cur_tag_close']    = '</a></li>';
            $config['num_tag_open']     = '<li>';
            $config['num_tag_close']    = '</li>';
            $this->pagination->initialize($config);

            $start_limit = ($page) * 1;
            if ($start_limit > 1) {
                $start_limit = $start_limit;
            } else {
                $start_limit = 0;
            }                      
            $sql .= " LIMIT " .$config["per_page"]. " OFFSET " .$start_limit."";            

            $queryResult = $this->db->query($sql);
            $result = $queryResult->result();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * ValidateLogin function
     * Discription : Validates user credentials.
     * Author : Synergy
     * @param string $email.
     * @param string $password.
     * @param string $usertype.
     * @return boolean | user row.
     */
    function validateLogin($email, $password, $usertype) {

        $hash = $this->getHashByEmail($email, $usertype);
        if (password_verify($password, $hash->password)) {
            return $hash;
        } else {
            return false;
        }
    }

    /**
     * Discription : Use to get user details
     * Author : Synergy
     * @param string $email.
     * @param string $usertype.
     * @return boolean | user row.
     */
    function getHashByEmail($email, $usertype) {
        $this->db->where('email', $email);

        if ($usertype == 'admin') {
            $this->db->where('role', 1);
        } else {
            $this->db->where('(role = 2 or role = 3 or role = 4 or role = 5)');
        }

        $query = $this->db->get('fj_users');
        $result = $query->row();
        if ($query->num_rows() === 1) {
            return $query->row();
        } else {
            return false;
        }
    }

    /**
     * Discription : Use to add permissions for a specific user
     * Author : Synergy
     * @param array $addPermissions
     * @param int $userId
     * @return string | int
     */
    function addPermissions($permissions, $userId) {
        try {
            $this->deletePermissionsByUser($userId);
            foreach ($permissions as $item) {
                $data['permissionId'] = $item;
                $data['userId'] = $userId;
                $data['createdAt'] = date('Y-m-d h:i:s');
                $data['updatedAt'] = date('Y-m-d h:i:s');
                $this->db->insert('fj_userPermission', $data);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Discription : Use to save invitation for a specific user
     * Author : Synergy
     * @param array $data
     * @return int | string
     */
    function saveInvitation($data) {
        try {
            $this->db->insert('fj_jobInvitationForUser', $data);
            $id = $this->db->insert_id();
            $hashEid = getHashEID($id);
			return $hashEid;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Discription : Use to remove permission for a specific user
     * Author : Synergy
     * @param int $userId
     * @return boolean | string
     */
    function deletePermissionsByUser($userId) {
        try {
            $this->db->where('userId', $userId);
            $this->db->delete('fj_userPermission');
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Discription : Use to search users data
     * Author : Synergy
     * @param array $data
     * @param int $limit
     * @param int $offset
     * @return array or search data
     */
    function search($data = NULL, $limit = NULL, $offset = NULL) { 
        $this->session->unset_userdata('some_name');
        $location       = $this->input->post('location', true);
        $qualification  = $this->input->post('qualification', true);
        $expFrom        = $this->input->post('expFrom', true);
        $expTo          = $this->input->post('expTo', true);
        $salaryFrom     = $this->input->post('salFrom', true);
        $salaryTo       = $this->input->post('salTo', true);
        $gender         = $this->input->post('gender', true);
        $year           = $this->input->post('year', true);        
        $language       = $this->input->post('language', true);        
        $pincode        = $this->input->post('pincode', true);        
        $yearofbirth    = $this->input->post('yearofbirth', true);        
        $university     = $this->input->post('university', true);
        $aadhaarVerified= $this->input->post('aadhaarVerified', true);
        $audition       = $this->input->post('audition', true);                

        $userData       = $this->session->userdata['logged_in'];
        $userRole       = $userData->role;
        foreach ($location as $item) {
            $rows[] = $this->getCityState($item);
        }
        
        $i = 0;
        $locationNames = array();
        foreach ($rows as $val) {
            $arr['country' . $i . '']   = "'" . $val['country'] . "'";
            $arr['state' . $i . '']     = "'" . $val['state'] . "'";
            $arr['city' . $i . '']      = "'" . $val['city'] . "'";
            //$locationNames[]            = $val['country'];
            $locationNames[]            = $val['state'];
            $locationNames[]            = $val['city'];
            $i++;
        }

        $locationList = "";
        foreach ($locationNames as $locationName) {
            if($locationList == "") {
                $locationList = " '".$locationName."' ";
            } else {
                $locationList .= ', '." '".$locationName."' ";
            }
        }
        
        if($qualification!='') {
            $qualification  = implode('\', \'', $qualification);
            $qualification  = "'$qualification'";
        }

        if($location!='') {
            $location       = implode('\', \'', $locationNames);
            $location       = "'$location'";
        }
        if($year!='') {
            $year           = implode('\', \'', $year);
            $year           = "'$year'";
        }    
        if($university!='') {
            $university     = implode('\', \'', $university);
            $university     = "'$university'";
        }

        
        if($this->session->userdata('searchSQL')=='' || $data==NULL ) {
            
            $sql = "SELECT u.id, u.fullname UserName, IF(u.gender='1', 'Male', 'Female' ) AS Gender, u.workExperience WorkExperience, u.gender , fn_getuserlocation(u.id) PreferredLocations, fn_getuserqualificaation(u.id) CourseName FROM fj_users u WHERE u.status = '1' " ;

            if ($locationList != "")
            {
                $sql .= " AND id IN(SELECT userId FROM fj_userPreferLocations WHERE locationName IN (".$locationList."))";
            }

            if ($qualification != "")
            {
                $sql .= " AND id IN(SELECT userId FROM fj_userQualification WHERE courseId IN (".$qualification."))";
            }

            if ($year != "")
            {
                $sql .= " AND id IN(SELECT userId FROM fj_userQualification WHERE completionYear IN (".$year."))";
            }

            if (isset($expFrom) && $expFrom != NULL) {
                $sql .=' AND u.workExperience >=' . $expFrom . '';
            }

            if (isset($expTo) && $expTo != NULL) {
                $sql .=' AND u.workExperience <=' . $expTo . '';
            }

            if (isset($salaryFrom) && $salaryFrom != NULL && isset($salaryTo) && $salaryTo != NULL) {
                $sql .=' AND
                            (
                                (   u.expectedCtcFrom>='.$salaryTo.'   OR 
                                    u.expectedCtcTo>='.$salaryTo.'     OR 
                                    (u.expectedCtcFrom BETWEEN '.$salaryFrom.' AND '.$salaryTo.') OR 
                                    (u.expectedCtcTo BETWEEN '.$salaryFrom.' AND '.$salaryTo.')
                                ) 
                                    AND 
                                (
                                    u.expectedCtcFrom<='.$salaryTo.' AND 
                                    '.$salaryTo.'<=u.expectedCtcTo
                                ) 
                            )';
            }


            if (isset($gender) && $gender != NULL) {
                $sql .=' AND u.gender =' . $gender . '';
            }            
            if (isset($language) && $language != NULL  && $language != '' && $userRole=='1') {
                $sql .=' AND u.languages LIKE \'%' . $language . '%\'';
            }                            
            if (isset($pincode) && $pincode != NULL  && $pincode != '' && $userRole=='1') {
                $sql .=' AND u.pinCode =\'' . $pincode . '\'';
            }
            if (isset($yearofbirth) && $yearofbirth != NULL  && $yearofbirth != '' && $userRole=='1') {
                $sql .=' AND YEAR(u.dob) =\'' . $yearofbirth . '\'';
            }
            if (isset($aadhaarVerified) && $aadhaarVerified != NULL  && $aadhaarVerified != '' && $userRole=='1') {
                //$sql .=' AND U.aadhaarVerified =\'' . $aadhaarVerified . '\'';
            }
            //echo $sql;exit;
        }
        
        if($this->session->userdata('searchSQL')=='') {            
            $this->session->set_userdata(array('searchData' => $sql));
            $this->session->set_userdata(array('searchSQL' => $sql));
        }
        
            
        if($data==NULL) {
            $query          = $this->db->query($sql);            
            $total_records  = $query->num_rows();
        }
        else {
            $sql            = $data;
            $query          = $this->db->query($sql);            
            $total_records  = $query->num_rows();
        }
        
        $config["base_url"]         = base_url() . $this->uri->segment(1). "/" . $this->uri->segment(2) . "/";
        $config["total_rows"]       = $total_records;
        $config["per_page"]         = 50;
        $page                       = $this->uri->segment(3, 0);
        $config['uri_segment']      = 3;
        $config['full_tag_open']    = '<ul class="pagination">';
        $config['full_tag_close']   = '</ul>';
        $config['first_link']       = false;
        $config['last_link']        = false;
        $config['first_tag_open']   = '<li>';
        $config['first_tag_close']  = '</li>';
        $config['prev_link']        = '&laquo';
        $config['prev_tag_open']    = '<li class="prev">';
        $config['prev_tag_close']   = '</li>';
        $config['next_link']        = '&raquo';
        $config['next_tag_open']    = '<li>';
        $config['next_tag_close']   = '</li>';
        $config['last_tag_open']    = '<li>';
        $config['last_tag_close']   = '</li>';
        $config['cur_tag_open']     = '<li class="active"><a href="#">';
        $config['cur_tag_close']    = '</a></li>';
        $config['num_tag_open']     = '<li>';
        $config['num_tag_close']    = '</li>';
        $this->pagination->initialize($config);
        $start_limit = ($page) * 1;

        if ($start_limit > 1) {
            $start_limit = $start_limit;
        } else {
            $start_limit = 0;
        }

        $sql .= " LIMIT " .$config["per_page"]. " OFFSET " .$start_limit."";                     
        $queryResult = $this->db->query($sql);
        $result = $queryResult->result();
        return $result;
    }
    
    /**
     * Discription : Use to get details of searched user specific data
     * Author : Synergy
     * @param int $userId
     * @return array or searched user data
     */
    function getSearchedUserData($userId) {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token      = $this->config->item('accessToken');
        $userData   = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $sql = "
                    SELECT 
                        U.id                                                        AS userId,
                        U.fullname                                                  AS userName,
                        U.image                                                     AS userImage,
                        U.email                                                     AS UserEmail,
                        U.mobile                                                    AS userMobile,
                        U.dob                                                       AS userDob,
                        (CASE WHEN U.gender='1' THEN 'Male' ELSE 'Female' END)      AS userGender,
                        U.adharCard                                                 AS userAadhar,
                        U.languages                                                 AS userLang,
                        U.facebookLink                                              AS facebookLink,
                        U.linkedInLink                                              AS linkedInLink,
                        U.twitterLink                                               AS twitterLink,
                        U.googlePlusLink                                            AS googlePlusLink,
                        U.expectedCtcFrom                                           AS expectedCtcFrom,
                        U.expectedCtcTo                                             AS expectedCtcTo,
                        U.workExperience                                            AS workExperience,
                        U.resume_path                                               AS resumePath, 
                        GROUP_CONCAT(CONCAT(C.name) SEPARATOR ' || ')               AS qualification,
                        GROUP_CONCAT(CONCAT(UQ.completionYear) SEPARATOR ' || ')    AS completionYear
                    FROM 
                        fj_users U 
                    LEFT JOIN 
                        fj_userQualification UQ ON UQ.userId = U.id 
                    JOIN fj_courses C ON C.id = UQ.courseId
                    WHERE

                        U.id=$userId    AND 
                        U.role='3'      AND 
                        U.status='1'";
            
            $queryResult = $this->db->query($sql);
            $result = $queryResult->row();

            $userauditionRes = $this->useraudition_model->getUserAuditionVideo($userId);
            if( $userauditionRes ){ 
                $userLoggedInId = getUserId(); 
                $result->auditionId         = $userauditionRes['id'];
                $result->auditionfile       = $userauditionRes['audition_file'];
                $result->auditionfiletype   = $userauditionRes['file_type'];
                $result->auditionFiles      = $userauditionRes['auditionFiles'];
                
                $checkAlreadyLikedAudition = $this->userauditionlike_model->checkAlreadyLikeAudition($userLoggedInId, $userauditionRes['id']);
                if( $checkAlreadyLikedAudition ){
                    $result->alreadyLikeStatus = 2;
                } else {
                    $result->alreadyLikeStatus = 1;
                }
                
            } else {
                $result->auditionId         = "";
                $result->auditionfile       = "";
                $result->auditionfiletype   = "";
                $result->auditionFiles      = "";
                $result->alreadylikestatus = "";
            }

            return $result;
            
        } else {
            redirect(base_url('users/login'));
        }  
    }

    /**
     * Discription : Use to search users data
     * Author : Synergy
     * @param array $data
     * @param int $limit
     * @param int $offset
     * @return array or search data
     */
    function searchDataDownload($userSearchData) {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }

        $token      = $this->config->item('accessToken');
        $userData   = $this->session->userdata['logged_in'];

        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            redirect(base_url('users/login'));
            $userId = 1;
        }
        
        $location       = $userSearchData['location'];
        $qualification  = $userSearchData['qualification'];
        $expFrom        = $userSearchData['expFrom'];
        $expTo          = $userSearchData['expTo'];
        $salaryFrom     = $userSearchData['salaryFrom'];
        $salaryTo       = $userSearchData['salaryTo'];
        $gender         = $userSearchData['gender'];
        $year           = $userSearchData['year'];        
        $language       = $userSearchData['language'];        
        $pincode        = $userSearchData['pincode'];        
        $yearofbirth    = $userSearchData['yearofbirth'];        
        $university     = $userSearchData['university'];
        $aadhaarVerified= $userSearchData['aadhaarVerified'];
        $audition       = $userSearchData['audition'];
        
        $userData = $this->session->userdata['logged_in'];
        $userRole = $userData->role;

        foreach ($location as $item) {
            $rows[] = $this->getCityState($item);
        }
        
        $i = 0;
        $locationNames = array();
        foreach ($rows as $val) {
            $arr['country' . $i . '']   = "'" . $val['country'] . "'";
            $arr['state' . $i . '']     = "'" . $val['state'] . "'";
            $arr['city' . $i . '']      = "'" . $val['city'] . "'";
            $locationNames[]            = $val['country'];
            $locationNames[]            = $val['state'];
            $locationNames[]            = $val['city'];
            $i++;
        }        
        if($qualification!='') {
            $qualification = implode('\', \'', $qualification);
            $qualification = "'$qualification'";
        }        
        if($location!='') {
            $location = implode('\', \'', $locationNames);
            $location = "'$location'";
        }
        if($year!='') {
            $year = implode('\', \'', $year);
            $year = "'$year'";
        }
        if($university!='') {
            $university = implode('\', \'', $university);
            $university = "'$university'";
        }
        
        $sql = "
                SELECT 
                    U.id,
                    U.fullname                                                  AS UserName,
                    U.email                                                     AS UserEmail,
                    U.mobile                                                    AS UserMobile,
                    U.dob                                                       AS DateOfBirth,
                    (CASE WHEN U.gender='1' THEN 'Male' ELSE 'Female' END)      AS Gender,
                    U.pinCode                                                   AS Pincode,
                    U.facebookLink                                              AS FacebookURL,
                    U.linkedInLink                                              AS LinkedInURL,
                    U.twitterLink                                               AS TwitterURL,
                    U.googlePlusLink                                            AS GooglePlusURL,
                    U.expectedCtcFrom                                           AS ExpectedCtcFrom,
                    U.expectedCtcTo                                             AS ExpectedCtcTo,
                    U.workExperience                                            AS WorkExperience,
                    GROUP_CONCAT(DISTINCT(UPL.locationName))                    AS PreferredLocations, 
                    GROUP_CONCAT(CONCAT(C.name) SEPARATOR ' || ')               AS CourseName,
                    GROUP_CONCAT(CONCAT(UQ.completionYear) SEPARATOR ' || ')    AS CompletionYear,
                    GROUP_CONCAT(CONCAT(UQ.percent) SEPARATOR ' || ')           AS Percent,     
                    GROUP_CONCAT(CONCAT(UQ.universityName) SEPARATOR ' || ')    AS UniversityName,                   
                    U.adharCard,
                    U.languages
                FROM 
                    fj_users U 
                JOIN 
                    fj_userQualification 	UQ, 
                    fj_userPreferLocations	UPL,
                    fj_courses		C
                WHERE 
                    UQ.courseId=C.id	AND 
                    U.id=UQ.userId		AND 
                    U.id=UPL.userId		AND 
                    U.role='3' 		AND 
                    U.status='1'            AND    
                    UPL.status='1' 		AND 
                    UQ.status='1' 		";            

        if ($year != NULL  && $year != '') {
            $sql .=' AND UQ.completionYear in (' . $year . ')';
        }
        if ($location != NULL  && $location != '') {
            $sql .=' AND UPL.locationName IN (' . $location . ')';
        }
        if ($qualification != NULL  && $qualification != '' && count($qualification)>0) {
            $sql .=' AND UQ.courseId IN (' . $qualification . ')';
        }
        if ($expFrom != NULL  && $expFrom != '') {
            $sql .=' AND U.workExperience >=' . $expFrom . '';
        }
        if ($expTo != NULL  && $expTo != '') {
            $sql .=' AND U.workExperience <=' . $expTo . '';
        }
        if ($salaryFrom != NULL  && $salaryFrom != '') {
            $sql .=' AND U.expectedCtcFrom >=' . $salaryFrom . '';
        }
        if ($salaryTo != NULL  && $salaryTo != '') {
            $sql .=' AND U.expectedCtcTo <=' . $salaryTo . '';
        }
        if ($gender != NULL  && $gender != '') {
            $sql .=' AND U.gender =' . $gender . '';
        }
        
        if ($language != NULL  && $language != '' && $userRole=='1') {
            $sql .=' AND U.languages =\'' . $language . '\'';
        }                            
        if ($pincode != NULL  && $pincode != '' && $userRole=='1') {
            $sql .=' AND U.pinCode =\'' . $pincode . '\'';
        }            
        if ($yearofbirth != NULL  && $yearofbirth != '' && $userRole=='1') {
            $sql .=' AND YEAR(U.dob) =\'' . $yearofbirth . '\'';
        }
        if ($aadhaarVerified != NULL  && $aadhaarVerified != '' && $userRole=='1') {
            //$sql .=' AND U.aadhaarVerified =\'' . $aadhaarVerified . '\'';
        }

        if ($university != NULL  && $university != '' && $userRole=='1') {
            $sql .=' AND UQ.universityId IN (' . $university . ')';
        }
        if ($audition != NULL  && $audition != '' && $userRole=='1') {
            $sql .= " AND (SELECT count(id) FROM fj_userAudition UA WHERE UA.status='1' AND UA.userId=U.id)>0";
        }
        
        $sql .= " GROUP BY U.id";
        $sql .= " ORDER BY U.fullname";
        
        $query  = $this->db->query($userSearchData);
        $result = $query->result_array();
        return $result;
        
    }

    /**
     * GetCityState function
     * Gets cities and state.
     * Author : Synergy
     * @param int $id.
     * @return array of data.
     */
    function getCityState($id) {
        try {
            $this->db->select('*');
            $this->db->from('fj_cityState');
            $this->db->where('id', $id);
            $this->db->order_by("state", "asc");
            $query = $this->db->get();
            $row = $query->row_array();
            if (count($row) == 0) {
                throw new Exception('Unable to fetch user data.');
                return false;
            }
            return $row;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * GetCities function
     * Gets all cities.
     * Author : Synergy
     * @param int $id.
     * @return array the city data.
     */
    function getCities() {
        try {
            $this->db->select('*');
            $this->db->from('fj_cityState');
            $this->db->order_by("city", "asc");
            $query = $this->db->get();
            $result = $query->result_array();
            if (count($result) == 0) {
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * GetStates function
     * Use to Get state of city.
     * Author : Synergy
     * @param int $id.
     * @return array of state data.
     */
    function getStates($city = NULL) {
        try {
            $this->db->select('state');
            $this->db->group_by('state');
            $this->db->from('fj_cityState');
            if (isset($city) && $city != NULL) {
                $this->db->where('city', $city);
            }
            $query = $this->db->get();
            $result = $query->result_array();
            if (count($result) == 0) {
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * getIndustries function
     * Use to Get all industries
     * Author : Synergy
     * @param none.
     * @return array of industries data.
     */
    function getIndustries() {
        try {
            $sql = "select * from nc_industry WHERE indus_id='0' AND is_active='1'";
            $query = $this->db->query($sql);
            $result = $query->result_array();

            if (count($result) == 0) {
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * getLocations function
     * Use to Get all the locations
     * Author : Synergy
     * @param none.
     * @return array of locations data.
     */
    function getLocations() {
        try {
            $this->db->select('*');
            $this->db->from('fj_cityState');

            $query = $this->db->get();
            $result = $query->result_array();
            if (count($result) == 0) {
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * getCorporateData function
     * Use to Get details of a particular corporate user
     * Author : Synergy
     * @param int $userId
     * @return array of user data.
     */
    function getCorporateData($userId) {
        try {
            $this->db->select('*');
            $this->db->from('fj_users');
            $this->db->where('id', $userId);
            $query = $this->db->get();
            $result = $query->row_array();
            if (count($result) == 0) {
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * updateUserStatus function
     * Use to update the statue of a particular status
     * Author : Synergy
     * @param int $userId
     * @return boolean
     */
    function updateUserStatus($userID) {
        $data = array();
        $query = $this->db->query("SELECT id, status FROM fj_users WHERE id='$userID'");
        $rowData = $query->row_array();
        if ($rowData && $rowData['status'] == '1') {
            $data['status'] = '2';
        } else if ($rowData && $rowData['status'] == '2') {
            $data['status'] = '1';
        }
         else if ($rowData && $rowData['status'] == '3') {
            $data['status'] = '1';
        }
        $this->db->where('id', $userID);
        $this->db->update('fj_users', $data);
    }

    /**
     * eventList function
     * Discription : Use to list the events
     * Author : Synergy
     * @param int $limit, int $offset, string $serachColumn, string $searchText, int $createdBy
     * @return array of users data.
     */
    function eventList($limit = NULL, $offset = NULL) {
        try {
            $userData = $this->session->userdata['logged_in'];
            $userRole = $userData->role;
            
            $sql = "SELECT * FROM fj_events ORDER BY id desc";
            
            $query = $this->db->query($sql);
            $result = $query->result();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * eventList function
     * Discription : Use to list the events
     * Author : Synergy
     * @param int $limit, int $offset, string $serachColumn, string $searchText, int $createdBy
     * @return array of users data.
     */
    function customerActivity($fromDate = NULL, $toDate = NULL) {
        try {
            $userData = $this->session->userdata['logged_in'];
            $userRole = $userData->role;
            
            if(!empty($fromDate) && !empty($toDate)) {
                $dateFrom = date('Y-m-d', strtotime($fromDate));
                $toDate = date('Y-m-d', strtotime($toDate));
                $where1 = ' (job1.createdAt BETWEEN "'.$dateFrom.'" AND "'.$toDate.'") AND ';
                $where2 = ' (user2.createdAt BETWEEN "'.$dateFrom.'" AND "'.$toDate.'") AND ';
                $where3 = ' (invite.createdAt BETWEEN "'.$dateFrom.'" AND "'.$toDate.'") AND ';
                $where4 = ' (userjob.createdAt BETWEEN "'.$dateFrom.'" AND "'.$toDate.'") AND ';
            } else {
                $dateFrom = '';
                $toDate = '';
                $where1 = '';
                $where2 = '';
                $where3 = '';
                $where4 = '';
            }

            $sql = "SELECT user.id, user.company, (SELECT count(*) FROM fj_jobs job1 JOIN fj_users user1 ON user1.id = job1.createdBy WHERE $where1 job1.status='1' AND ((job1.createdBy = user.id) OR ((user1.createdBy = user.id) AND (user1.status='1'))) ) AS noOfJobsCreated, (SELECT count(*) FROM fj_users user2 WHERE $where2 user2.createdBy = user.id AND user2.role AND user2.role IN(4,5) AND user2.status='1') AS noOfRecruitersCreated, (SELECT count(*) FROM fj_jobInvitationForUser invite JOIN fj_users user3 ON user3.id = invite.invitedBy JOIN fj_jobs job3 ON job3.id = invite.jobId WHERE $where3 job3.status='1' AND ((invite.invitedBy = user.id) OR ((user3.createdBy = user.id) AND (user3.status = '1'))) ) AS noOfInvitations, (SELECT count(*) FROM fj_userJob userjob JOIN fj_jobs job2 ON job2.id = userjob.jobId JOIN fj_users user4 ON user4.id = job2.createdBy WHERE $where4 job2.status='1' AND ((job2.createdBy = user.id) OR ((user4.createdBy = user.id) AND (user4.status='1'))) ) AS noOfInterviewsReceived, (SELECT count(*) FROM fj_userJob userjob JOIN fj_jobs jobs2 ON jobs2.id = userjob.jobId JOIN fj_users user4 ON user4.id = jobs2.createdBy WHERE $where4 userjob.status IN('2','4') AND (jobs2.status = '1') AND ((jobs2.createdBy = user.id) OR ((jobs2.createdBy = user4.createdBy) AND (user4.status='1'))) ) AS noOfInterviewsShortlisted, (SELECT count(*) FROM fj_userJob userjob JOIN fj_jobs jobs2 ON jobs2.id = userjob.jobId JOIN fj_users user4 ON user4.id = jobs2.createdBy WHERE $where4 userjob.status IN('3','5') AND (jobs2.status = '1') AND ((jobs2.createdBy = user.id) OR ((jobs2.createdBy = user4.createdBy) AND (user4.status='1'))) ) AS noOfInterviewsRejected FROM `fj_users` user WHERE user.role='2' AND user.status='1' AND user.company IS NOT NULL ORDER BY user.createdAt DESC";

            $query = $this->db->query($sql);
            $result = $query->result();
            return $result;
        } catch (Exception $e) {
            return array();
        }
    }

    /**
     * eventActivityList function
     * Discription : Use to list the event activity
     * Author : Synergy
     * @param int $eventId
     * @return array of event activity related data.
     */
    function eventActivityList($eventId, $date) {
        try {

            if($date) {
                $filterDate = "(ujp.createdAt BETWEEN '$date' AND '$date 23:59:59')";
                $filterDate1 = "(ftu.createdAt BETWEEN '$date' AND '$date 23:59:59')";
            } else {
                $filterDate = "ujp.createdAt >= DATE_FORMAT(NOW(),'%Y-%m-%d')";
                $filterDate1 = "ftu.createdAt >= DATE_FORMAT(NOW(),'%Y-%m-%d')";
            }

            $userData = $this->session->userdata['logged_in'];
            $userRole = $userData->role;
            //$sql = "SET @isource = 1; ";
            $sql = "SELECT ujp.createdAt, ujp.id, ujp.status, fu.fullname userName, fu.email userEmail, fu.mobile userMobile, fj.title, fc.company FROM `fj_userJobProcessStatus` ujp JOIN fj_users fu ON fu.id = ujp.userId JOIN fj_jobs fj ON fj.id = ujp.jobId JOIN fj_users fc ON fc.id = fj.createdBy WHERE fj.eventId = $eventId AND $filterDate AND ujp.status NOT IN (0,5) ORDER BY ujp.createdAt DESC";
            $query = $this->db->query($sql);
            //echo $sql;exit;

            $result = $query->result();
            //print_r($result);exit;


            $sql1 = "SELECT count(fua.id) totalGivenAnswers, ftu.source isource, ftu.createdAt createdAt, fu.fullname userName, fu.email userEmail, fu.mobile userMobile, fj.title title, fc.company company, fj.id ujobId FROM `fj_temp_userjob` ftu JOIN fj_userAnswers fua ON fua.jobId = ftu.jobId AND fua.userId = ftu.userId JOIN fj_jobs fj ON fj.id = ftu.jobId JOIN fj_users fu ON fu.id = ftu.userId JOIN fj_users fc ON fc.id = fj.createdBy  WHERE ftu.source = 0 AND fj.eventId = $eventId AND $filterDate1 GROUP BY fua.jobId, fua.userId";
            $query1 = $this->db->query($sql1);
            $result1 = $query1->result();
            //print_r($result1);exit;

            $arraAppAnswer = array();
            foreach ($result1 as $rowResult) {
                $sql2 = "SELECT count(fiq.id) totalQuestion FROM `fj_interviewQuestions` fiq JOIN fj_jobs fj ON fj.interview = fiq.interviewId WHERE fj.id = $rowResult->ujobId";
                $query2 = $this->db->query($sql2);
                $result2 = $query2->result();

                $totalQuestion = $result2[0]->totalQuestion;
                //echo $totalQuestion .'Hello'.$rowResult->totalGivenAnswers;exit;
                if($rowResult->totalGivenAnswers != $totalQuestion) {
                    $arraAppAnswer[] = $rowResult;
                }
            }
            
            $result3 = array_merge($result, $arraAppAnswer);
            //print_r($arraAppAnswer);exit;

            return $result3;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * viewCompletedJob function
     * Discription : Use to list the event activity
     * Author : Synergy
     * @param int $eventId
     * @return array of event activity related data.
     */
    function viewCompletedJob($eventId) {
        try {
            $userData = $this->session->userdata['logged_in'];
            $userRole = $userData->role;

            /*$sql = "SELECT ujp.createdAt, ujp.id, ujp.status, fu.fullname userName, fu.email userEmail, fu.mobile userMobile, fj.title, fc.company FROM `fj_userJobProcessStatus` ujp JOIN fj_users fu ON fu.id = ujp.userId JOIN fj_jobs fj ON fj.id = ujp.jobId JOIN fj_users fc ON fc.id = fj.createdBy WHERE fj.eventId = $eventId AND ujp.createdAt >= DATE_FORMAT(NOW(),'%Y-%m-%d') AND ujp.status NOT IN (0,5) ORDER BY ujp.createdAt DESC";
            $query = $this->db->query($sql);
            $result = $query->result();*/
            //print_r($result);exit;


            $sql1 = "SELECT count(fua.id) totalGivenAnswers, ftu.source isource, ftu.createdAt createdAt, fu.fullname userName, fu.email userEmail, fu.mobile userMobile, fj.title title, fc.company company, fj.id ujobId FROM `fj_temp_userjob` ftu JOIN fj_userAnswers fua ON fua.jobId = ftu.jobId AND fua.userId = ftu.userId JOIN fj_jobs fj ON fj.id = ftu.jobId JOIN fj_users fu ON fu.id = ftu.userId JOIN fj_users fc ON fc.id = fj.createdBy  WHERE fj.eventId = $eventId AND ftu.createdAt >= DATE_FORMAT(NOW(),'%Y-%m-%d') GROUP BY fua.jobId, fua.userId";
            $query1 = $this->db->query($sql1);
            $result1 = $query1->result();
            //print_r($result1);exit;

            /*$arraAppAnswer = array();
            foreach ($result1 as $rowResult) {
                $sql2 = "SELECT count(fiq.id) totalQuestion FROM `fj_interviewQuestions` fiq JOIN fj_jobs fj ON fj.interview = fiq.interviewId WHERE fj.id = $rowResult->ujobId";
                $query2 = $this->db->query($sql2);
                $result2 = $query2->result();

                $totalQuestion = $result2[0]->totalQuestion;
                //echo $totalQuestion .'Hello'.$rowResult->totalGivenAnswers;exit;
                if($rowResult->totalGivenAnswers != $totalQuestion) {
                    $arraAppAnswer[] = $rowResult;
                }
            }
            
            $result3 = array_merge($result, $arraAppAnswer);*/
            //print_r($arraAppAnswer);exit;

            return $result1;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
}
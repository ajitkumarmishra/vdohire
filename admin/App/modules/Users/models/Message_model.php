<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Message Model
 * Description : Handle all the CRUD operation for Message
 * @author Synergy
 * @createddate : Nov 25, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 27, 2017
 */

class Message_model extends CI_Model {

    /**
     * Initialize the variables
     */
    var $table = "fj_message";

    /**
     * Responsable for inherit the parent constructor
     * Responsable for auto load the the session and pagination library
     * @return void
     */
    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('pagination');
    }

    /**
     * GetMessage function
     * Gets a specific job detail.
     * Author : Synergy
     * @param int $id.
     * @return array the jobs data.
     */
    function getMessage($id) {
        try {
            $this->db->select('*');
            $this->db->from($this->table);
            $this->db->where('messageId', $id);
            $query = $this->db->get();
            $result = $query->row_array();
            if (count($result) == 0) {
                throw new Exception('Unable to fetch message data.');
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * listMessage function
     * Use to get list of messages for logged in user
     * Author : Synergy
     * @param int $limit, int $offset, string $serachColumn, string $searchText, int $createdBy, int $userLoggeinId
     * @return array of message data
     */
    function listMessage($limit, $offset, $serachColumn = NULL, $searchText = NULL, $createdBy = NULL, $userLoggeinId) {
        try {
            $currentDate = date('Y-m-d h:i:s');
            $sql = "select M.*, U.fullname as tousername, U1.fullname as fromusername from fj_message as M left join fj_users as U on U.id=M.userTwoid "
                    . " left join fj_users as U1 on U1.id=M.userOneId"
                    . " where M.userOneId = '".$userLoggeinId."'";
                        
            if(isset($offset)) {
                $sql .= " limit " .$offset. ", " .$limit."";
            }

            $query = $this->db->query($sql);
            $result = $query->result();
           
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * addMessage function
     * Creates a message
     * Author : Synergy
     * @params array $data
     * @return int | boolean
     */
    function addMessage($data) {
        $resp = $this->db->insert($this->table, $data);
        $messageId = $this->db->insert_id();
        if ($resp) {
            $data = array('parentMessageId' => $messageId, 'isParent' => 1);
            $this->db->where('messageId', $messageId);
            $this->db->update($this->table, $data);
            return $messageId;
        } else {
            return false;
        }
    }
    
    
    /**
     * deleteMessageThread function
     * Delete a message thread
     * Author : Synergy
     * @params int messageId
     * @return boolean | string
     */
    function deleteMessageThread($messageId) {
        try {
            $this->db->where('messageId', $messageId);
            $this->db->delete($this->table);
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * UpdateMessage function
     * Updates a specific message.
     * Author : Synergy
     * @param array $data.
     * @param int $id.
     * @return boolean | string.
     */
    function updateMessage($data, $id) {
        try {
            $this->db->where('messageId', $id);
            $resp = $this->db->update($this->table, $data);
            if (!$resp) {
                throw new Exception('Unable to update message data.');
                return false;
            }
            return $id;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    
    /**
     * CloseMessage function
     * Author : Synergy
     * Close a specific message.
     * @param array $data.
     * @param int $id.
     * @return boolean | string.
     */
    function closeMessage($data, $id) {
        try {
            $this->db->where('messageId', $id);
            $resp = $this->db->update($this->table, $data);
            if (!$resp) {
                throw new Exception('Unable to update message data.');
                return false;
            }
            return $id;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * countUnreadMessage function
     * Discription : Use to count all unred top bar message for a specific user
     * Author : Synergy
     * @param int $id.
     * @return array of message data
     */
    function countUnreadTopBarMessage($userId) {
        try {
            
            $query = $this->db->select("count(messageId) as unreadcount")
                                ->from($this->table)
                                ->where('userTwoId =', $userId)
                                ->where('messageStatus = 1')
                                ->get();
            
            $result = $query->row_array();
           
            if (!$result) {
                throw new Exception('Unable to fetch unread message data.');
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Users Controller
 * Description : Used to handle all Users related data
 * @author Synergy
 * @createddate : Nov 2, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 24, 2017
 */
class Users extends MY_Controller {

    /**
     * Responsable for auto load the message_model, messagereply_model, user_model, userauditionlike_model, superadminreport_model
     * Responsable for auto load the  form_validation, session and email library
     * Responsable for auto load fj, jwt helper
     * Check Uri Segment to display specific page role wise
     * @return void
     */
    function __construct() {
        parent::__construct();
        $this->load->model('message_model');
        $this->load->model('messagereply_model');
        $this->load->model('user_model');
        $this->load->model('userauditionlike_model');
        $this->load->model('job_model'); 
        $this->load->model('superadminreport_model');          
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('email');
        $this->load->helper('fj');
        $this->load->helper('jwt');

        $token = $this->config->item('accessToken');
        

        /**********start of prevent any type fraud activity*****************/

        if ($this->session->userdata('logged_in')) {
            $userRealData   = $this->session->userdata['logged_in'];
            $uriStringSegment = $this->uri->uri_string();
            
            $arraySegments = explode('/', $uriStringSegment);
            array_pop($arraySegments);
            $newString = implode('/', $arraySegments);

            if($userRealData->role == 1) {
                $portal = 'SuperAdminPortal';
            }
            elseif ($userRealData->role == 2 || $userRealData->role == 4 || $userRealData->role == 5) {
                $portal = 'CorporatePortal';
            }
            else{
                $portal = '';
            }
			
            if($uriStringSegment == 'users/searchCandidate') {
                if(!checkUserAccessForWebPage($userRealData->id, $userRealData->role, $portal, 'users/searchCandidate')) {
                    $this->session->sess_destroy();
                    $insertFaultData['userId'] = $userRealData->id;
                    $insertFaultData['comment'] = 'This user try to access this this page';
                    $insertFaultData['pagePath'] = 'users/searchCandidate';
                    $this->db->insert('fj_user_access_log', $insertFaultData);
                    redirect(base_url('users/login'));
                }
            }
            if($uriStringSegment == 'users/list') {
                if(!checkUserAccessForWebPage($userRealData->id, $userRealData->role, $portal, 'users/list')) {
					
                    $this->session->sess_destroy();
                    $insertFaultData['userId'] = $userRealData->id;
                    $insertFaultData['comment'] = 'This user try to access this this page';
                    $insertFaultData['pagePath'] = 'users/list';
                    $this->db->insert('fj_user_access_log', $insertFaultData);
                    redirect(base_url('users/login'));
                }
            }
            if($uriStringSegment == 'users/list/corporate') {
                if(!checkUserAccessForWebPage($userRealData->id, $userRealData->role, $portal, 'users/list/corporate')) {
                    $this->session->sess_destroy();
                    $insertFaultData['userId'] = $userRealData->id;
                    $insertFaultData['comment'] = 'This user try to access this this page';
                    $insertFaultData['pagePath'] = 'users/list/corporate';
                    $this->db->insert('fj_user_access_log', $insertFaultData);
                    redirect(base_url('users/login'));
                }
            }
            if($uriStringSegment == 'users/addcorporate') {
                if(!checkUserAccessForWebPage($userRealData->id, $userRealData->role, $portal, 'users/addcorporate')) {
                    $this->session->sess_destroy();
                    $insertFaultData['userId'] = $userRealData->id;
                    $insertFaultData['comment'] = 'This user try to access this this page';
                    $insertFaultData['pagePath'] = 'users/addcorporate';
                    $this->db->insert('fj_user_access_log', $insertFaultData);
                    redirect(base_url('users/login'));
                }
            }
            if($newString == 'users/editcorporate') {
                if(!checkUserAccessForWebPage($userRealData->id, $userRealData->role, $portal, 'users/editcorporate')) {
                    $this->session->sess_destroy();
                    $insertFaultData['userId'] = $userRealData->id;
                    $insertFaultData['comment'] = 'This user try to access this this page';
                    $insertFaultData['pagePath'] = 'users/editcorporate';
                    $this->db->insert('fj_user_access_log', $insertFaultData);
                    redirect(base_url('users/login'));
                }
            }
            if($newString == 'users/view') {
                if(!checkUserAccessForWebPage($userRealData->id, $userRealData->role, $portal, 'users/view')) {
                    $this->session->sess_destroy();
                    $insertFaultData['userId'] = $userRealData->id;
                    $insertFaultData['comment'] = 'This user try to access this this page';
                    $insertFaultData['pagePath'] = 'users/view';
                    $this->db->insert('fj_user_access_log', $insertFaultData);
                    redirect(base_url('users/login'));
                }
            }
            if($newString == 'users/edit') {
                if(!checkUserAccessForWebPage($userRealData->id, $userRealData->role, $portal, 'users/edit')) {
                    $this->session->sess_destroy();
                    $insertFaultData['userId'] = $userRealData->id;
                    $insertFaultData['comment'] = 'This user try to access this this page';
                    $insertFaultData['pagePath'] = 'users/edit';
                    $this->db->insert('fj_user_access_log', $insertFaultData);
                    redirect(base_url('users/login'));
                }
            }
        }
        
        /**********end of prevent any type of fraud activity********************/
    }

    /**
     * Description : List all the Users
     * Author : Synergy
     * @param none
     * @return array of users data 
     */
    function list_all() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('signin'));
        }
        if ($this->input->post() != null) {
            $this->user_model->updateStatus();
        }
        $includeJs = array('theme/js/custom.js', 'theme/js/bootbox.min.js');
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $config = array();
        $config["base_url"] = base_url() . "users/list_all";
        $config['total_rows'] = $this->user_model->count_all();
        $config['per_page'] = (site_limit()) ? site_limit() : DEFAULT_LIMIT_PER_PAGE;
        $config['full_tag_open'] = '<div class="pagination pagination-small pagination-right"><ul class="pagination">';
        $config['full_tag_close'] = '</ul></div>';
        $config['first_link'] = true;
        $config['last_link'] = true;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $this->session->set_userdata('logged_in', $sess_array);
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["users"] = $this->user_model->read($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        $data['main_content'] = 'users';
        $this->load->view('page', $data);
    }

    /**
     * Description : Use to get details of a user by his or her nicename
     * Author : Synergy
     * @param string $nicename
     * @return render data into view
     */
    function user($nicename) {
        $data["user"] = $this->user_model->user_by_nicename($nicename);
        if ($data["user"]) {
            $data['main_content'] = 'user';
            $this->load->view('page', $data);
        } else {
            show_404();
        }
    }

    /**
     * Description : Use to sign in into corporate user panel
     * Author : Synergy
     * @param array $_POST
     * @return render data into view
     */
    function signin() {
        if ($this->_is_logged_in()) {
            redirect('');
        }

        if ($_POST) {
            $user_email = $this->input->post('user_email', true);
            $password = $this->input->post('password', true);
            $userdata = $this->user_model->validate($user_email, md5($password));

            if ($userdata) {
                if ($userdata->status == 0) {
                    $data['error'] = "Not validated!";
                    $data['main_content'] = 'signin';
                    $this->load->view('full_width', $data);
                } else {
                    $data['userid'] = $userdata->id;
                    $data['logged_in'] = true;
                    $this->session->set_userdata($data);
                    redirect(base_url('admin'));
                }
            } else {
                $data['error'] = "Invalid Credentails!";
                $data['main_content'] = 'signin';
                $this->load->view('full_width', $data);
            }

            return;
        }
        $data['main_content'] = 'signin';
        $this->load->view('full_width', $data);
    }

    /**
     * Description : Use to signup user into corporate user panel
     * Author : Synergy
     * @param array $_POST
     * @return render data into view
     */
    function signup() {
        if ($_POST) {

            $config = array(
                array(
                    'field' => 'fullname',
                    'label' => 'Full name',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'username',
                    'label' => 'User name',
                    'rules' => 'trim|is_unique[users.user_nicename]',
                ),
                array(
                    'field' => 'email',
                    'label' => 'E-mail',
                    'rules' => 'trim|required|valid_email|is_unique[users.user_email]',
                ),
                array(
                    'field' => 'password',
                    'label' => 'Password',
                    'rules' => 'trim|required',
                )
            );

            $this->form_validation->set_rules($config);

            if ($this->form_validation->run() === false) {
                $data['error'] = validation_errors();
                $data['main_content'] = 'signup';
                $this->load->view('page', $data);
            } else {
                $data['user_login'] = $this->input->post('fullname', true);
                $data['user_pass'] = md5($this->input->post('password', true));
                $data['user_nicename'] = $this->input->post('username', true);
                $data['user_email'] = $this->input->post('email', true);
                $data['activation_key'] = md5(rand(0, 1000) . 'uniquefrasehere');

                $create = $this->user_model->create($data);

                if ($create) {

                    $this->load->library('email');

                    $this->email->from('noreply@yoursite.com', 'Site Name');
                    $this->email->to($data['user_email']);
                    $this->email->subject('Confirmation');
                    $this->email->message("Confirm your subscription <a href=''>Confirmar</a>" . $data['activation_key']);
                    $this->email->send();


                    $data['main_content'] = 'signup-success';
                    $this->load->view('page', $data);
                } else {
                    error_log("Un usuario no se pudo registrar");
                }
            }
            return;
        }
        $data['main_content'] = 'signup';
        $this->load->view('page', $data);
    }

    /**
     * Description : Use to logout a logged in user
     * Author : Synergy
     * @param null
     * @return render data into view
     */
    function logout() {
        $userData = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
            $userRole = $userData->role;
			$this->user_model->UpdateUserSession($userId);
        } else {
            $userId = 1;
            $userRole = "";
        }        
        unset($_COOKIE['sessionId']);
		setcookie('sessionId', '', time() + (100), "/"); // 86400 = 1 day
		
        $this->session->sess_destroy();
        
        if( $userRole == 1 ){
            redirect('users/login/admin');
        } else {
            redirect('users/login');
        }
    }

    /**
     * Description : Use to get details of an user
     * Author : Synergy
     * @param int $id
     * @return render data into view
     */
    function account($id = null) {

        $this->_member_area();
        if ($id != null) {
            $userID = $id;
        } else {
            $userID = $this->session->userdata('userid');
        }

        if ($_POST) {
            $userdata = new stdClass();
            $userdata->user_nicename = $this->input->post('nickname');
            $userdata->user_name = $this->input->post('user_name');
            $userdata->user_mobile = $this->input->post('user_mobile');
            $userdata->user_dob = $this->input->post('user_dob');
            $userdata->user_email = $this->input->post('email');
            $userdata->user_pass = md5($this->input->post('password'));
            $insert = $this->user_model->update($userID, $userdata);
            if ($insert) {
                $data['message'] = "Updated succesfully";
                $data['user'] = $this->user_model->user_by_id($userID);
                $data['main_content'] = 'account';
                $this->load->view('page', $data);
            }
            return;
        }

        $data['user'] = $this->user_model->user_by_id($userID);
        $data['main_content'] = 'account';
        $this->load->view('page', $data);
    }

    /**
     * Description : Use to check user is logged in or not
     * Author : Synergy
     * @param none
     * @return redirect to signin page
     */
    function _member_area() {
        if (!$this->_is_logged_in()) {
            redirect('signin');
        }
    }

    /**
     * Description : Use to check user is logged in or not
     * Author : Synergy
     * @param none
     * @return redirect to signin page
     */
    function _is_logged_in() {
        if ($this->session->userdata('logged_in')) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Description : Use to get details of signed in user
     * Author : Synergy
     * @param none
     * @return array of user data or boolean false
     */
    function userdata() {
        if ($this->_is_logged_in()) {
            return $this->user_model->user_by_id($this->session->userdata('userid'));
        } else {
            return false;
        }
    }

    /**
     * Description : Use to check if a logged in user is superadmin or not
     * Author : Synergy
     * @param none
     * @return boolean
     */
    function _is_admin() {
        $role = array('0', '1', '2', '3');
        if (@$this->users->userdata()->role === 1) {
            //if (in_array(@$this->users->userdata()->role, $role)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Description : Use to create a new user
     * Author : Synergy
     * @param array $_POST
     * @return render data into view
     */
    function create() {
        check_auth();
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('signin'));
        }
        $includeJs = array('theme/js/user.js',
            'theme/js/bootbox.min.js',
            'theme/plugins/jquery-validation/lib/jquery.js',
            'theme/plugins/jquery-validation/dist/jquery.validate.js'
        );
        $includeCss = array('theme/plugins/jquery-validation/demo/css/screen.css');
        $data['includes_for_layout_css'] = add_includes('css', $includeCss);
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('user_nicename', 'Nicename', 'trim|required');
        $data['roles'] = $this->user_model->get_roles();
        if ($this->form_validation->run() == false) {
            $data['error'] = validation_errors();
            $data['main_content'] = 'create';
        } else {
            $insert = $this->user_model->create_user();
            if ($insert) {
                $this->session->set_flashdata('message', 'Information Updated Successfully.');
            } else {
                $this->session->set_flashdata('error', 'Something Went Wrong');
            }
            redirect('users');
        }
        $this->load->view('page', $data);
    }

    /**
     * Description : Use to get mongo details
     * Author : Synergy
     * @param none
     * @return render data into view
     */
    public function mongo() {

        // Lets try to get the key
        // $results = $this->memcached_library->get('test');
        // // If the key does not exist it could mean the key was never set or expired
        // if (!$results) {
        // // Modify this Query to your liking!
        // $query = $this->cimongo->get("product")->result();
        // // Lets store the results
        // $this->memcached_library->add('test', $query);
        // // Output a basic msg
        // echo 'Alright! Stored some results from the Query... Refresh Your Browser';die;
        // } else {
        // // Output
        // var_dump($results);
        // // Now let us delete the key for demonstration sake!
        // $this->memcached_library->delete('test');
        // }
        $data = $this->cimongo->get("store_info")->result();
        foreach ($data as $document) {
            echo "<pre>";
            print_r($document);
            // echo $document["title"] . "\n";
        }
        die;
    }

    /**
     * Description : Use to display forgot password page and send forgot password data to user mail
     * Author : Synergy
     * @param array $_POST
     * @return render data into view
     */
    function forgot_password() {
        $data['meta_title'] = "Forgot Password";
        $data['meta_keyword'] = "Forgot Password";
        $data['meta_description'] = "Forgot Password";
        $data['main_content'] = 'forgot_password';
        $this->load->library('form_validation');
        $this->form_validation->set_rules('user_email', 'Email', 'trim|required|valid_email');
        if ($this->form_validation->run() == false) {
            $data['error'] = validation_errors();
        } else {
            $this->load->library('email');
            $key = $this->user_model->getUserKey($this->input->post('user_email'));
            $message = 'Please follow the link to reset your password:' . base_url() . 'users/reset_password?key=' . $key;
            $this->email->from(ADMIN_EMAIL, '99Retail Street');
            $this->email->to($this->input->post('user_email'));
            $this->email->subject('99 Retail Street Password Reset');
            $this->email->message($message);
            $this->email->send();
            $this->session->set_flashdata('message', 'Mail Sent Successfully! Please check ');
        }
        $this->load->view('page', $data);
    }

    /**
     * Description : Use to display reset password page and update logged in user password
     * Author : Synergy
     * @param array $_POST
     * @return render data into view
     */
    function reset_password() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('signin'));
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('user_password', 'New Password', 'trim|required');
        $this->form_validation->set_rules('cnf_password', 'Confirm Password', 'trim|required|matches[user_password]');
        if ($this->form_validation->run() == false) {
            $data['error'] = validation_errors();
        } else {
            $key = $this->input->get('key');
            $update = $this->user_model->reset_password($key);
            if ($update) {
                redirect('users/signin');
            } else {
                $data['error'] = 'Not Updated';
            }
        }
        $data['main_content'] = 'reset_password';
        $this->load->view('page', $data);
    }

    /**
     * Description : Use to display add user page and create new user
     * Author : Synergy
     * @param array $_POST
     * @return render data into view
     */
    function addUser() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $baseUrl = $this->config->item('base_url');
        $permissions = getPermissions();
        $userData = $this->session->userdata['logged_in'];
        $token = $this->config->item('accessToken');
		$userRole = $userData->role;
        $userId = $userData->id;

		//added code for the permission
		//USER'S PERMISSION
		$userPermissionStr = "";
		$userPermissionArr = array();

		$userPermissionRes = dashboardUserPermissions($userId);

		if( $userPermissionRes ){
			$userPermissionStr = $userPermissionRes->permissionIds;
			$userPermissionArr = explode(",", $userPermissionStr);
		}
		//ALL PERMISSION FOR ADMIN
		$adminPermission = array(6,9,10,11,12,8);
		$corporatePermission = array(1,2,3,4,5,7,8);
		//SIDE MENU FOR LISTING
		//till here added code for the permission

        if (isset($_POST) && $_POST != NULL) {
            $subusers = getSubUsers($userData->id);
            $subusersTotal = count($subusers);
            $corpUsersDetail = getUserById($userData->id);
            $permissCount = $corpUsersDetail['userLimit'];
            
            $fromEmail = $this->config->item('fromEmail');

            $resp = array();

            if ($token != $this->input->post('accessToken', true)) {
                $resp['error'] = 'You do not have pemission.';
            }

            $this->form_validation->set_rules('name', 'Name', 'trim|required');
            $this->form_validation->set_rules('mobile', 'Mobile', 'trim|required|isValidMobile[fj_users.mobile]');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[fj_users.email]');
            $this->form_validation->set_rules('company', 'Company', 'trim|required');
            $this->form_validation->set_rules('designation', 'Designation', 'trim|required');
            $this->form_validation->set_rules('address', 'Address', 'trim|required');
            $this->form_validation->set_message('is_unique', 'Email address already exist!');
            $this->form_validation->set_message('isValidMobile', 'Invalid mobile number!');
            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }
            $roleId = getRoleIdByUser($userData->id);
            if ($roleId == 2) {
                if ($subusersTotal >= $permissCount) {
                    $resp['error'] .= '<p>You  have already reached to maximum sub user limit.</p>';
                }
            }
            if (!isset($resp['error'])) {
                $data['fullname'] = $this->input->post('name', true);
                $data['role'] = 4;
                $data['mobile'] = $this->input->post('mobile', true);
                $data['gender'] = $this->input->post('gender', true);
                $data['email'] = $this->input->post('email', true);

                $dobs = $this->input->post('dob', true);
                if(isset($dobs) && !empty($dobs)) {
                    $data['dob'] = date("Y-m-d", strtotime($dobs));
                } else {
                    $data['dob'] = '';
                }
                
                $data['location'] = $this->input->post('location', true);
                $data['status'] = $this->input->post('status', true);
                $data['company'] = $this->input->post('company', true);
                $data['designation'] = $this->input->post('designation', true);
                $data['address'] = $this->input->post('address', true);
                $data['aboutMe'] = $this->input->post('aboutMe', true);
                $data['createdAt'] = date('Y-m-d h:i:s');
                $data['updatedAt'] = date('Y-m-d h:i:s');
                $data['activationKey'] = md5(rand(0, 1000) . 'uniquefrasehere');
                $password = $this->generateRandomString();
                $data['password'] = password_hash($password, PASSWORD_BCRYPT);
                $data['createdBy'] = $userData->id;
                $roleName = getRoleById($data['role']);
                $permissions = $this->input->post('permissions', true);
				
                try {
                    if (isset($_FILES['userImage']['name']) && !empty($_FILES['userImage']['name'])) {
                        $imageName = $this->fileUpload($_FILES);
                        if (!$imageName) {
                            $resp['message'] = 'There is a server problem occured to upload image. Please make sure image type is png or jpg!';
                        }
                    }
                    $data['image']  = $this->input->post('image', true);
                    $resps           = $this->user_model->addUser($data);
                    if ($resps) {
                        //Inserting company config data
                            $this->db->insert('fj_companyConfig', array('mainUserId' => $resps, 'companyId' => $userData->id, 'companyConfigParamId' => 1, 'paramValue' => 1));

                            $this->db->insert('fj_companyConfig', array('mainUserId' => $resps, 'companyId' => $userData->id, 'companyConfigParamId' => 2, 'paramValue' => 1));
                        //End of Inserting company config data
                        $this->user_model->addPermissions($permissions, $resps);
                        $temp['password'] = $password;
                        $temp['email'] = $data['email'];
                        $temp['roleName'] = $roleName;
                        $temp['name'] = $data['fullname'];
                        $temp['url'] = $baseUrl . 'users/login';                       
                        $body = $this->load->view('emails/registerTemp.php', $temp, TRUE);
                        
                        $this->email->from($fromEmail, 'VDOHire');
                        $this->email->to($data['email']);
                        
                        if ($data['role'] == 3) {
                            //Send validation mail 
                            $url = base_url() . 'users/validateKey?key=' . $data['activationKey'];
                            $message = "Please click below to complete registration<br/> <a href='" . $url . "'>Click Here</a>";
                            $this->email->subject('VDOHire Registration Confirmation');
                            $this->email->message($message);
                            $this->email->set_mailtype('html');
                            $this->email->send();
                        } 
                        else {
                            // Send username and password.      
                            $this->email->subject('VDOHire Login Credentials');
                            $this->email->message($body);                            
                            $this->email->send();           
                        }
                        $data['message'] = 'Successfully created!';
                        $data['main_content'] = 'fj-create-user-sub';
                        $data['permissions'] = getPermissions();
                        $data['token'] = $token;
                        $data['breadcrumb'] = 'users';
                        $data['userPermissionArr'] = $userPermissionArr;
                        $data['userRoleForMenu'] = $userRole;
                        $this->load->view('fj-mainpage-recuiter', $data);
                    }
                } catch (Exception $e) {
                    $resp['error'] = $e->getMessage();
                }
            } else {
                $data['main_content'] = 'fj-create-user-sub';
                $data['permissions'] = $permissions;
                $data['error'] = $resp;
                $data['user'] = $this->input->post();
                $data['token'] = $token;
                $data['breadcrumb'] = 'users';
                $data['userPermissionArr'] = $userPermissionArr;
                $data['userRoleForMenu'] = $userRole;
                $this->load->view('fj-mainpage-recuiter', $data);
            }
        } else {
            $data['main_content'] = 'fj-create-user-sub';
            $data['permissions'] = $permissions;
            $data['token'] = $token;
            $data['breadcrumb'] = 'users';
            $data['userPermissionArr'] = $userPermissionArr;
            $data['userRoleForMenu'] = $userRole;
            $this->load->view('fj-mainpage-recuiter', $data);
        }
    }

    /**
     * Description : Use to display Import users page and import csv file data into database
     * Author : Synergy
     * @param array $_POST
     * @return render data into view
     */
    function importUsers() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $baseUrl = $this->config->item('base_url');
        $userData = $this->session->userdata['logged_in'];
        $token = $this->config->item('accessToken');

        if (isset($_POST) && $_POST != NULL && isset($_FILES)) {

            $this->form_validation->set_rules('companyName', 'companyName', 'trim|required');

            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }
            
            if (!$resp['error']) {
                $data['companyName'] = $this->input->post('companyName', true);
                
                try {
                    if (isset($_FILES['importUsers'])) {
                        $uploadFilePath = 'uploads/';
        
                        $path = $uploadFilePath . 'importUsers' . $userData->id . '.csv';
                        if (file_exists($path))
                            unlink($path);
                        $file = $this->csvUpload($_FILES, $path, 'importUsers');
                        
                        if($file === false) {
                            $resp['error'] = 'Unable to import user. Please try again!';
                            $data['main_content'] = 'fj-import-users';
                            $data['error'] = $resp;
                            $data['user'] = $this->input->post();
                            $data['token'] = $token;
                            $this->load->view('fj-mainpage', $data);
                        } elseif($file == 'invalidFileFormat') {
                            $resp['error'] = 'Invalid file type. Please upload only CSV file type!';
                            $data['main_content'] = 'fj-import-users';
                            $data['error'] = $resp;
                            $data['user'] = $this->input->post();
                            $data['token'] = $token;
                            $this->load->view('fj-mainpage', $data);
                        } else {
                            $file = fopen($path, "r");
                            $i = 0;
                            while (!feof($file)) {
                                $fileData = fgetcsv($file);
                                if ($i != 0) {
                                    $fileArr[] = $fileData;
                                }
                                $i++;
                            }

                            if($fileArr) {
                                foreach ($fileArr as $item) {
                                    if(!empty($item)) {
                                        if(!$this->user_model->user_by_email($item[1])) {
                                            $firstFourCharacters = substr($item[0], 0, 4);
                                            $userInsertData['fullname'] = $item[0];
                                            $userInsertData['email'] = $item[1];
                                            $userInsertData['mobile'] = $item[2];
                                            $userInsertData['role'] = 3;
                                            $userInsertData['status'] = 1;
                                            $userInsertData['createdAt'] = date('Y-m-d h:i:s');
                                            $userInsertData['updatedAt'] = date('Y-m-d h:i:s');
                                            $password = $firstFourCharacters.$this->generateThreeDigitRandomString().'$';
                                            $userInsertData['password'] = password_hash($password, PASSWORD_BCRYPT);
                                            $userInsertData['sourceName'] = $this->input->post('companyName');;

                                            $userId = $this->user_model->addUser($userInsertData);
                                            $this->db->insert('fj_user_temp_password', array('userId' => $userId, 'password' => $password));
                                        }
                                    }
                                }
                            }
                            $data['message'] = 'Candidates Imported Successfully!';
                            $data['main_content'] = 'fj-import-users';
                            $data['token'] = $token;
                            $this->load->view('fj-mainpage', $data);

                        }
                    }
                } catch (Exception $e) {
                    $resp['error'] = $e->getMessage();
                    $data['main_content'] = 'fj-import-users';
                    $data['error'] = $resp;
                    $data['user'] = $this->input->post();
                    $data['token'] = $token;
                    $this->load->view('fj-mainpage', $data);
                }
            } else {
                $data['main_content'] = 'fj-import-users';
                $data['error'] = $resp;
                $data['user'] = $this->input->post();
                $data['token'] = $token;
                $this->load->view('fj-mainpage', $data);
            }
        } else {
            $data['main_content'] = 'fj-import-users';
            $data['token'] = $token;
            $this->load->view('fj-mainpage', $data);
        }
    }


    /**
     * FileUpload function
     * Uploads a file.
     * @param array $files.
     * @return string the uploaded file or boolean false.
     */
    function fileUpload($files) {
        $typeArr = explode('/', $files['userImage']['type']);
        $imgName = rand() . date('ymdhis') . '_user_image.' . $typeArr[1];
        $data['image'] = $imgName;
        $userImageType = $this->config->item('userImageType');
        if (in_array($typeArr[1], $userImageType)) {
            $uploadResp = $this->user_model->fileUpload($files, 'uploads/userImages', $imgName);
            return $uploadResp;
        } else {
            return false;
        }
    }

    /**
     * profileUpload function
     * Uploads a file.
     * @param array $files.
     * @return string the uploaded file or boolean false.
     */
    function profileUpload() {
        $files = $_FILES;
        $typeArr = explode('/', $files['userImage']['type']);
        $imgName = rand() . date('ymdhis') . '_user_image.' . $typeArr[1];
        $data['image'] = $imgName;
        $userImageType = $this->config->item('userImageType');
        if (in_array($typeArr[1], $userImageType)) {
            $uploadResp = $this->user_model->fileUpload($files, 'uploads/userImages', $imgName);
            echo $uploadResp;
            die();
            return $uploadResp;
        } else {
            return false;
        }
    }

    /**
     * Description : Use to display edit user page and update user data into database
     * Author : Synergy
     * @param int $id, array $_POST
     * @return render data into view
     */
    function editUser($id) {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $user = $this->user_model->getUser($id);
        $permissions = getPermissions();
        $userPermission = getUserPermissions($id);
        $token = $this->config->item('accessToken');

        $userData = $this->session->userdata['logged_in'];
        $userRole = $userData->role;
        $j = 0;
        foreach ($userPermission as $item) {
            $formattedPermissions[$j] = $item['id'];
            $j++;
        }
        if (isset($_POST) && $_POST != NULL) {
            $resp = array();
            $fromEmail = $this->config->item('fromEmail');
            if ($token != $this->input->post('accessToken', true)) {
                $resp['error'] = 'You do not have pemission.';
            }
            $this->form_validation->set_rules('name', 'Name', 'trim|required');
            $this->form_validation->set_rules('mobile', 'Mobile', 'trim|required|isValidMobile[fj_users.mobile]');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
            $this->form_validation->set_message('isValidMobile', 'Invalid mobile number!');
            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }

            if (!$id) {
                $resp['error'] = 'Undefined user!';
            }
            if (!isset($resp['error'])) {
                $data['fullname'] = $this->input->post('name', true);
                $data['mobile'] = $this->input->post('mobile', true);
                $data['gender'] = $this->input->post('gender', true);
                $data['email'] = $this->input->post('email', true);

                $dobs = $this->input->post('dob', true);
                if(isset($dobs) && !empty($dobs)) {
                    $data['dob'] = date("Y-m-d", strtotime($dobs));
                } else {
                    $data['dob'] = '';
                }

                $data['location'] = $this->input->post('location', true);
                $data['status'] = $this->input->post('status', true);
                $data['company'] = $this->input->post('company', true);
                $data['designation'] = $this->input->post('designation', true);
                $data['address'] = $this->input->post('address', true);
                $data['aboutMe'] = $this->input->post('aboutMe', true);
                $data['createdAt'] = date('Y-m-d h:i:s');
                $data['updatedAt'] = date('Y-m-d h:i:s');
                $data['activationKey'] = md5(rand(0, 1000) . 'uniquefrasehere');

                $permissions = $this->input->post('permissions', true);
                try {
                    if (isset($_FILES['userImage']['name']) && !empty($_FILES['userImage']['name'])) {
                        $imageName = $this->fileUpload($_FILES);
                        if (!$imageName) {
                            $resp['message'] = 'There is a server problem occured to upload image. Please make sure image type is png or jpg!';
                        } else {
                            unlink('uploads/userImages/' . $user['image']);
                        }
                    }
                    $data['image'] = $this->input->post('image', true);

                    $resps = $this->user_model->updateUser($data, $id);
                    if ($resps) {
                        $this->user_model->addPermissions($permissions, $resps);
                        $data['message'] = 'Successfully updated!';
                        $data['main_content'] = 'fj-edit-user-sub';
                        $data['token'] = $token;
                        $data['breadcrumb'] = 'users';
                        redirect('users/list', 'refresh');
                    }
                } catch (Exception $e) {
                    $resp['error'] = $e->getMessage();
                }
            } else {
                $data['main_content'] = 'fj-edit-user-sub';
                $data['permissions'] = $permissions;
                $data['error'] = $resp;
                $data['user'] = $this->input->post();
                $data['token'] = $token;
                $data['user']['permissions'] = $formattedPermissions;
                $data['breadcrumb'] = 'users';
                if($userRole == 1)
                    $this->load->view('fj-mainpage', $data);
                else
                    $this->load->view('fj-mainpage-recuiter', $data);
                }
        } else {
            $data['main_content'] = 'fj-edit-user-sub';
            $data['permissions'] = $permissions;
            $data['user'] = $user;
            $data['token'] = $token;
            $data['user']['permissions'] = $formattedPermissions;
            $data['breadcrumb'] = 'users';
            if($userRole == 1)
                $this->load->view('fj-mainpage', $data);
            else
                $this->load->view('fj-mainpage-recuiter', $data);
        }
    }

    /**
     * DeleteUser function
     * Discription: Deletes a user.
     * Author : Synergy
     * @param array $_POST
     * @return string the success message | error message.
     */
    function deleteUser() {
        $resp['error'] = 0;
        $token = $this->config->item('accessToken');
        $fromEmail = $this->config->item('fromEmail');
        $userId = $this->input->post('id', true);
        $users = getUserById($userId);
        $temp['email'] = $users['email'];
        $temp['fullname'] = $users['fullname'];
        if (!$userId) {
            echo $error = 'Undefined user!';
            exit;
        }
        if (checkUserInLiveJob($userId)) {
            echo $error = 'This user is already assigned to a job!';
            exit;
        }
        if ($error == 0) {
            $data['status'] = 3;
            $resp = $this->user_model->updateUser($data, $userId);
            if ($resp) {

                //Email
                $this->email->from($fromEmail, 'VDOHire');
                $this->email->to($users['email']);
                // Send username and password. 
                $this->email->subject('VDOHire');
                $body = $this->load->view('emails/deleteTemp.php', $temp, TRUE);
                $this->email->message($body);
                $this->email->set_mailtype('html');
                $this->email->send();
                //Email
                echo "Deleted successfully";
            }
        }
    }

    /**
     * CheckUserInLiveJob function
     * Discription : Checks user in live job.
     * Author : Synergy
     * @param int $userId
     * @return boolean.
     */
    function checkUserInLiveJob($userId) {
        if (checkUserInLiveJob($userId)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Description : Use to get and display details of particular user
     * Author : Synergy
     * @param int $id
     * @return render data into view
     */
    function getUser($id) {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }

        $userData = $this->session->userdata['logged_in'];
        $userRole = $userData->role;

        $user = $this->user_model->getUser($id);
        $permissions = getPermissions();
        $userPermission = getUserPermissions($id);
        $token = $this->config->item('accessToken');
        $j = 0;
        foreach ($userPermission as $item) {
            $formattedPermissions[$j] = $item['id'];
            $j++;
        }

        $data['main_content'] = 'fj-view-user-sub';
        $data['permissions'] = $permissions;
        //print '<pre>';print_r($data['permissions']);exit;
        $data['user'] = $user;
        $data['token'] = $token;
        $data['breadcrumb'] = 'users';
        $data['user']['permissions'] = $formattedPermissions;

        if($userRole == 1)
            $this->load->view('fj-mainpage', $data);
        else
            $this->load->view('fj-mainpage-recuiter', $data);
    }

    /**
     * Description : Use to get list of user and display into view
     * Author : Synergy
     * @param int $page
     * @return render data into view
     */
    function listUsers($page = null) {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $userData = $this->session->userdata['logged_in'];
        $userRole = $userData->role;
        $data['page'] = $page;
        $resp['error'] = 0;

        if(isset($_POST['submit']) && $_POST['submit'] == 'submit') {
            $searchText = $this->input->post('serachText', true);
        } else {
            $searchText = '';
        }
        $data['breadcrumb']     = 'users';
        $serachColumn = $this->input->post('serachColumn', true);
        $token = $this->config->item('accessToken');
        $fromEmail = $this->config->item('fromEmail');
        $itemsPerPage = $this->config->item('itemsPerPage');
        $data['itemsPerPage'] = $itemsPerPage;
        $createdBy = $userData->id;
        if (!$page) {
            $page = 0;
        } else {
            $page = $page - 1;
        }

        $offset = $page * $itemsPerPage;
        try {
            $result = $this->user_model->listUsers($itemsPerPage, $offset, $serachColumn, $searchText, ($userData->role!='1'?$userData->id:NULL), $comResult = NULL);

            $completeResult = $this->user_model->listUsers(NULL, NULL, $serachColumn, $searchText, ($userData->role!='1'?$userData->id:NULL), $comResult = 'completeResult');
            
            $count = ceil(count($completeResult) / $itemsPerPage);
            $data['count'] = $count;
            $data['totalCount'] = count($completeResult);
            $data['content'] = $result;

            if($userRole == 1)
                $data['main_content'] = 'fj-user-listing';
            else
                $data['main_content'] = 'fj-recruiter-user-listing';

            $data['searchText'] = $searchText;
            $data['serachColumn'] = $serachColumn;
            $data['token'] = $token;
			
			//added code for the permission
			$userId = getUserId();
			$userDet = getUserById($userId);
			$userRoleForMenu = $userDet['role'];

			if( $userRoleForMenu == 1 ) {
				$urlDashborad = 'superadmin/dashboard';
			}
			else if( $userRoleForMenu == 2 || $userRoleForMenu == 4 || $userRoleForMenu == 5 ) {
				$urlDashborad = 'corporate/dashboard'; 
			}

			//USER'S PERMISSION
			$userPermissionStr = "";
			$userPermissionArr = array();

			$userPermissionRes = dashboardUserPermissions($userId);

			if( $userPermissionRes ){
				$userPermissionStr = $userPermissionRes->permissionIds;
				$userPermissionArr = explode(",", $userPermissionStr);
			}
			//ALL PERMISSION FOR ADMIN
			$adminPermission = array(6,9,10,11,12,8);
			$corporatePermission = array(1,2,3,4,5,7,8);
			//SIDE MENU FOR LISTING
			$sideBarMenu = getMenuForSideBar();

			$currentFullBaseUrl = base_url(uri_string());
			$currentBaseUrl = str_replace(base_url(), "", $currentFullBaseUrl);
			$data['userPermissionArr'] = $userPermissionArr;
			$data['userRoleForMenu'] = $userRoleForMenu;
			//till here added code for the permission

            if($userRole == 1)
                $this->load->view('fj-mainpage', $data);
            else
                $this->load->view('fj-mainpage-recuiter', $data);
            
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Description : Use to get list of corporate users and display into view
     * Author : Synergy
     * @param int $page, array $_POST
     * @return render data into view
     */
    function listCorporateUsers($page) {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $userData = $this->session->userdata['logged_in'];
        $data['page'] = $page;
        $resp['error'] = 0;
        $serachColumn   = $this->input->post('serachColumn', true);
        $searchText     = $this->input->post('serachText', true);        

        if($_POST){
            $this->session->set_userdata('serachColumn', $serachColumn);
            $this->session->set_userdata('searchText', $searchText);    
        }
        $serachColumn   = $this->session->userdata('serachColumn');
        $searchText     = $this->session->userdata('searchText');
        
        $token = $this->config->item('accessToken');
        $fromEmail = $this->config->item('fromEmail');
        $itemsPerPage = $this->config->item('itemsPerPage');
        $data['itemsPerPage'] = $itemsPerPage;
        $createdBy = $userData->id;
        $createdByRole = $userData->role;
        if (!$page) {
            $page = 0;
        } else {
            $page = $page - 1;
        }
        $offset = $page * $itemsPerPage;
        try {
            $result = $this->user_model->listCorporates($itemsPerPage, $offset, $serachColumn, $searchText, $createdBy, $createdByRole);

            $count = ceil(count($completeResult) / $itemsPerPage);
            $data['count'] = $count;
            $data['totalCount'] = count($completeResult);
            $data['content'] = $result;
            $data['main_content'] = 'fj-corporate-listing';
            $data['searchText'] = $searchText;
            $data['serachColumn'] = $serachColumn;
            $data['token'] = $token;
            $this->load->view('fj-mainpage', $data); //die();
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Description : Use to list all the app users and display
     * Author : Synergy
     * @param int $page, array $_POST
     * @return render data into view
     */
    function listAppUsers($page) {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $userData = $this->session->userdata['logged_in'];
        $data['page'] = $page;
        $resp['error'] = 0;
        $serachColumn = $this->input->post('serachColumn', true);
        $searchText = $this->input->post('serachText', true);        
        
        $token = $this->config->item('accessToken');
        $fromEmail = $this->config->item('fromEmail');
        $itemsPerPage = $this->config->item('itemsPerPage');
        $data['itemsPerPage'] = $itemsPerPage;

        if (!$page) {
            $page = 0;
        } else {
            $page = $page - 1;
        }
        $offset = $page * $itemsPerPage;
        
        try {
            $result = $this->user_model->listAppUsers($itemsPerPage, $offset, $serachColumn, $searchText);
            $completeResult = $this->user_model->listAppUsers(NULL, NULL, $serachColumn, $searchText);
            $count = ceil(count($completeResult) / $itemsPerPage);
            $data['count'] = $count;
            $data['totalCount'] = count($completeResult);
            $data['content'] = $result;
            $data['main_content'] = 'fj-search-user-listing';
            $data['searchText'] = $searchText;
            $data['serachColumn'] = $serachColumn;
            $data['token'] = $token;
            $this->load->view('fj-mainpage', $data);
            
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Description : Use to get login into corporate panel
     * Author : Synergy
     * @param string $type, array $_POST
     * @return render data into view
     */
    function login($type = null) {
        $baseUrl = $this->config->item('base_url');
        if ($type == 'admin') {
			$data['roleName'] = 'Super Admin';
            $data['pageUrl'] = 'users/login/admin';
            $data['redirectUrl'] = $baseUrl . 'superadmin/dashboard';
        } else {
            $data['roleName'] = 'Recruiter Login';
            $data['pageUrl'] = 'users/login';
            $data['redirectUrl'] = $baseUrl . 'corporate/dashboard';
        }
        $data['main_content'] = 'fj-login';
        $token = $this->config->item('accessToken');
        $data['token'] = $token;
        if (isset($_POST) && $_POST != NULL) {
            $resp['error'] = 0;
            $email = $this->input->post('email', true);
            $password = $this->input->post('password', true);
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');

            $this->form_validation->set_rules('password', 'Password', 'trim|required');
            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }
            try {
                if (!$resp['error']) {
                    if ( trim($type) == 'admin' ) {
                        $userdata = $this->user_model->validateLogin($email, $password, 'admin');
                    }else{
                        $userdata = $this->user_model->validateLogin($email, $password, '');
                    }

                    if ($userdata) {
						$Cuurenttimeout = date('Y-m-d H:i:s', time() + 600);
						$encryptedUserid = $this->getJwtToken($userdata->id).time();
						setcookie('sessionId', $encryptedUserid, time() + (100), "/"); // 86400 = 1 day
						
						$dataSession['userId'] = $userdata->id;
						$dataSession['createDate'] = date('Y-m-d H:i:s');
						$dataSession['loginTime'] = date('Y-m-d H:i:s');
						$dataSession['ipaddress'] = $_SERVER['REMOTE_ADDR'];
						$dataSession['sessionId'] = $encryptedUserid;
						$dataSession['timeout'] = $Cuurenttimeout;
						$dataSession['status'] = '1';
						$dataSession['serverSessionID'] = $_SESSION['__ci_last_regenerate'];
						$userdata->timeout = $Cuurenttimeout;
						
						$checkUserMultipleLogin = $this->user_model->checkMultipleLogin($userdata->id);
						$checkUserMultipleLogin = 0;
						if($checkUserMultipleLogin == 0){
							if($userdata->status=='1') {
								if (trim($type)=='admin') {
									$this->user_model->InsertUserSession($dataSession);
									$this->session->set_userdata('logged_in', $userdata);
									redirect($data['redirectUrl'], 'refresh');
								}
								else if (trim($type)!='admin' && $userdata->role=='2' && date('Y-m-d', strtotime($userdata->validTo))>=date('Y-m-d')) {                        
									$this->user_model->InsertUserSession($dataSession);
									$this->session->set_userdata('logged_in', $userdata);
									redirect($data['redirectUrl'], 'refresh');
								}
								else if (trim($type)!='admin' && $userdata->role=='4') {
									$getCorporateData = $this->user_model->getCorporateData($userdata->createdBy);
									if($getCorporateData) {
										if($getCorporateData['status']=='1' && ($getCorporateData['role']=='2' || $getCorporateData['role']=='1') && date('Y-m-d', strtotime($getCorporateData['validTo']))>=date('Y-m-d')) {
											$this->user_model->InsertUserSession($dataSession);
											$this->session->set_userdata('logged_in', $userdata);
											
											redirect($data['redirectUrl'], 'refresh');
										}
										else {
											$dataSession['createDate'] = date('Y-m-d H:i:s');
											$dataSession['loginTime'] = date('Y-m-d H:i:s');
											$dataSession['ipaddress'] = $_SERVER['REMOTE_ADDR'];
											$dataSession['status'] = 3;
											$dataSession['serverSessionID'] = $_SESSION['__ci_last_regenerate'];
											
											$this->user_model->InsertUserSession($dataSession);
											$data['msg'] = "Sorry, Your Company Account is Expired.";
										}
									}
								}
								else {
									$dataSession['createDate'] = date('Y-m-d H:i:s');
									$dataSession['loginTime'] = date('Y-m-d H:i:s');
									$dataSession['ipaddress'] = $_SERVER['REMOTE_ADDR'];
									$dataSession['status'] = 3;
									$dataSession['serverSessionID'] = $_SESSION['__ci_last_regenerate'];
									
									$this->user_model->InsertUserSession($dataSession);
									$data['msg'] = "Sorry, Your Account is Expired. Please Contact To FirstJob Support Team.";
								}
								
							} else {
								$dataSession['createDate'] = date('Y-m-d H:i:s');
								$dataSession['loginTime'] = date('Y-m-d H:i:s');
								$dataSession['ipaddress'] = $_SERVER['REMOTE_ADDR'];
								$dataSession['status'] = 3;
								$dataSession['serverSessionID'] = $_SESSION['__ci_last_regenerate'];
								
								$this->user_model->InsertUserSession($dataSession);
								$data['msg'] = "Sorry, Your Account is Blocked. Please Contact To FirstJob Support Team.";
							}
						}else{
							$data['msg'] = "Sorry, You are already login from other System";
						}
                    } else {
						
						$dataSession['createDate'] = date('Y-m-d H:i:s');
						$dataSession['loginTime'] = date('Y-m-d H:i:s');
						$dataSession['ipaddress'] = $_SERVER['REMOTE_ADDR'];
						$dataSession['status'] = 3;
						$dataSession['serverSessionID'] = $_SESSION['__ci_last_regenerate'];
						
						$this->user_model->InsertUserSession($dataSession);
                        $data['msg'] = "Sorry, Wrong Emailid and Password.";
                    }

                    $this->load->view('fj-loginpage', $data);
                } else {
                    $data['error'] = $resp;
                    $this->load->view('fj-loginpage', $data);
                }
            } catch (Exception $e) {
                echo $e->getMessage();
            }
        } else {
            $this->load->view('fj-loginpage', $data);
        }
    }

    /**
     * GetJwtToken function
     * Gets jwt token by user id.
     * @param int $userId
     * @return string
     */
    function getJwtToken($userId) {
        $token = array();
        $token['id'] = $userId;
        $jwtKey = $this->config->item('jwtKey');
        $secretKey = base64_decode($jwtKey);
        $signatureAlgo = $this->config->item('signatureAlgorithm');
        return JWT::encode($token, $secretKey, $signatureAlgo);
    }

    /**
     * GetValueByToken function
     * Gets value by token
     * @param string $jwtToken
     * @return string
     */
    function getValueByToken($jwtToken) {
        $jwtKey = $this->config->item('jwtKey');
        $secretKey = base64_decode($jwtKey);
        $signatureAlgo = $this->config->item('signatureAlgorithm');
        $token = JWT::decode($jwtToken, $secretKey, $signatureAlgo);
        return $token->id;
    }

    /**
     * Description : Use to get 8 digits random string
     * Author : Synergy
     * @param none
     * @return string of randomstring
     */
    function generateRandomString($length = 8) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * Description : Use to get 3 digits random string
     * Author : Synergy
     * @param none
     * @return string of randomstring
     */
    function generateThreeDigitRandomString($length = 3) {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * Description : Use to display search candidate page
     * Author : Synergy
     * @param none
     * @return render data into view
     */
    function searchCandidate() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $this->session->unset_userdata('searchData');
        $this->session->unset_userdata('searchSQL');
        
        $userData           = $this->session->userdata['logged_in'];
        $userRole           = $userData->role;
        $userLoggedinUserId = $userData->id;
        
        $data['main_content'] = 'fj-search-candidate';        
        $this->session->unset_userdata('some_name');
        
        $token              = $this->config->item('accessToken');
        $data['token']      = $token;
        $data['city']       = $this->user_model->getCities();
        $data['course']     = getCourses();
        $data['university'] = getUniversity();
        $this->load->view('fj-mainpage', $data);
    }
    
    /**
     * Description : Use to display search candidate result
     * Author : Synergy
     * @param array $_POST
     * @return render data into view
     */
    function searchCandidateResult() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $userData               = $this->session->userdata['logged_in'];
        $userRole               = $userData->role;
        $userLoggedinUserId     = $userData->id;
        
        $data['main_content']   = 'fj-search-candidate';        
        $this->session->unset_userdata('some_name');        
        $token                  = $this->config->item('accessToken');
        $data['token']          = $token;
        
        if (isset($_POST) && $_POST != NULL) {
            $this->session->set_userdata(array('searchData' => $_POST));
            $data['content']    = $this->user_model->search(); 
            $createdByStr       = getAllCorporateAndSubuserIds($userLoggedinUserId, $userRole);            
            if( $createdByStr == "" ){
                $createdByStr = $userLoggedinUserId;
            }            
            $data['myJobs']         = getMyJobsSearch($createdByStr, $userRole);
            $data['main_content']   = 'fj-search-user-listing';
            $this->load->view('fj-mainpage', $data);
        } 
        else {
            $sql                = $this->session->userdata('searchSQL');
            $data['content']    = $this->user_model->search($sql);            
            $createdByStr       = getAllCorporateAndSubuserIds($userLoggedinUserId, $userRole);            
            if( $createdByStr == "" ){
                $createdByStr = $userLoggedinUserId;
            }            
            $data['myJobs']         = getMyJobsSearch($createdByStr, $userRole);
            $data['main_content']   = 'fj-search-user-listing';
            $this->load->view('fj-mainpage', $data);
        }
    }
    
    /**
     * Description : Use to download csv for searched candidate
     * Author : Synergy
     * @param null
     * @return File of searched data
     */
    function downloadCSV() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token      = $this->config->item('accessToken');
        $userData   = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            redirect(base_url('users/login'));
            $userId = 1;
        }        
        
        $userSearchData = $this->session->userdata['searchData'];
        
        $fields =   array(
                        'id',
                        'Username',
                        'Email Id',
                        'Mobile Number',
                        'Date Of Birth',
                        'Gender',
                        'Pincode',            
                        'Facebook URL',
                        'LinkedIn URL',
                        'Twitter URL',
                        'GooglePlus URL',                        
                        'Expected Ctc From (In Lakhs P.A)',
                        'Expected Ctc To (In Lakhs P.A)',
                        'Work Experience (In Years)',
                        'Preferred Locations',            
                        'Course Name',
                        'Completion Year',
                        'Percent',
                        'University Name',            
                        'Aadhaar Card',
                        'Languages'
                    );            
        $timestamp  = date('M-d-Y_H-i-s_A');
        $reportName = 'Search_Candidates_'.$timestamp;
        $data       = $this->user_model->searchDataDownload($userSearchData);
        $this->createCSV($fields, $data, $reportName);
    }
    
    /**
     * Description : Use to create csv for data
     * Author : Synergy
     * @param null
     * @return File of searched data
     */
    function createCSV($fields, $data, $reportName) {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }

        $displayresult.="<Table width='100%' border='1'  >";
        $displayresult.="<tr style='background:#c5be97'>";
        foreach($fields as $fieldName) {
            $displayresult.="<TD style='color:Black; text-align:center; font-size:16px; font-weight:bold;'>".ucwords($fieldName)."</TD>";
        }
        $displayresult.="</tr>";

        foreach($data as $key=>$fieldData) {
            $key++;
            if($key%2==0)   { $css = "background-color:cyan;";	$className="#EFEFEF";	}
            else            { $css = "background-color:white;";	$className="#FFFFFF";   }		
            $displayresult.="<tr bgcolor=".$className.">";
                $displayresult.="<td class=$css>".$key."</td>";
                foreach(array_slice($fieldData,1,20) as $keyData=>$rowData) {
                    $keyData++;
                    $displayresult.="<td class=$css>".$rowData."</td>";
                }
            $displayresult.="</tr>";
        }

    	$displayresult.="</Table>";
    	header('Content-Type: application/vnd.ms-excel; charset=utf-8');
    	header("Content-Disposition: attachment; filename=".$reportName.".xls");
    	header('Pragma: no-cache');
    	header('Expires: 0');
    	echo $displayresult;
    }

    /**
     * Description : Use to display forgot password page and send password to user email
     * Author : Synergy
     * @param array $_POST
     * @return render data into view
     */
    function forgotPassword() {
        include $_SERVER["DOCUMENT_ROOT"]."/PHPMailer_v5.1/class.phpmailer.php";
        $data['main_content'] = 'fj-forgot-password';
        $token = $this->config->item('accessToken');
        $baseUrl = $this->config->item('base_url');
        $data['token'] = $token;

        if (isset($_POST) && $_POST != NULL) {
            $email = $this->input->post('email');
            $result = $this->user_model->getHashByEmail($email);
            $fromEmail = $this->config->item('fromEmail');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
            if ($this->form_validation->run() == false) {
                $data['error'] = validation_errors();
                $this->load->view('fj-loginpage', $data);
            } else {
                if (!$result->id) {
                    $data['error'] = 'Email address does not exist!';
                    $this->load->view('fj-loginpage', $data);
                } else {
                    $temp['password'] = $this->generateRandomString();
                    $temp['email'] = $email;
                    $temp['name'] = ($result->fullname==''?$result->company:$result->fullname);
                    $temp['url'] = $baseUrl . 'users/login';

                    $body = $this->load->view('emails/forgotPasswordTemp.php', $temp, TRUE);

                    $inputData['password'] = password_hash($temp['password'], PASSWORD_BCRYPT);
                    $this->user_model->updateUserByEmail($inputData, $email);

                    $account="hello@firstjob.co.in";
                    $password="Hello136";
                    $to=$email;
                    $from = $account;
                    $from_name = "VDOHire";
                    $subject="VDOHire Login Credentials";

                    $mail = new PHPMailer();
                    $mail->IsSMTP();
                    $mail->CharSet = 'UTF-8';
                    $mail->Host = "smtpout.secureserver.net";
                    $mail->SMTPAuth= true;
                    $mail->Port = 25;
                    $mail->Username= $account;
                    $mail->Password= $password;
                    //$mail->SMTPSecure = 'tls';
                    $mail->From = $from;
                    $mail->FromName= $from_name;
                    $mail->isHTML(true);
                    $mail->Subject = $subject;
                    $mail->Body = $body;
                    $mail->addAddress($to);

                    if(!$mail->send()){
                      $data['msg'] = $error = 'Mail error: '.$mail->ErrorInfo;
                    }else{
                      $data['msg'] = "Your password has been sent on the registered email id. Please login and enjoy browsing.";
                    }

                    $this->load->view('fj-loginpage', $data);
                }
            }
        } else {
            $this->load->view('fj-loginpage', $data);
        }
    }

    /**
     * Description : Use to display change password page and update password
     * Author : Synergy
     * @param array $_POST
     * @return render data into view
     */
    function changePassword() {
        $oldPassword = $this->input->post('oldPassword');
        $newPassword = $this->input->post('newPassword');
        $confirmNewPassword = $this->input->post('confirmNewPassword');
        $token = $this->config->item('accessToken');
        $userData = $this->session->userdata['logged_in'];
        $userId = $userData->id;
        $userRole = $userData->role;
        $this->form_validation->set_rules('oldPassword', 'Old Password', 'trim|required');
        $this->form_validation->set_rules('newPassword', 'New Password', 'trim|required|min_length[8]|alpha_numeric');
        $this->form_validation->set_rules('confirmNewPassword', 'Confirm New Password', 'trim|required|matches[newPassword]|min_length[8]|alpha_numeric');

        if ($this->form_validation->run() == false) {
            $data['error'] = validation_errors();
        } else {
            $result = $this->user_model->getUser($userId);
            if (!password_verify($oldPassword, $result['password'])) {
                $data['error'] = 'Old password is not correct!';
            } else if ($newPassword != $confirmNewPassword) {
                $data['error'] = 'New password and confirm new password does not match!';
            } else {
                $data['password'] = password_hash($newPassword, PASSWORD_BCRYPT);
                $data['email'] = $result['email'];
                $this->user_model->updateUser($data, $userId);
                $data['error'] = 0;
            }
        }

        if($userRole == 1)
            $data['main_content'] = 'fj-change-password';
        else
            $data['main_content'] = 'fj-change-recruiter-password';

        if ($data['error'] != 0) {
            
        } else {
            $data['token'] = $token;
            if ($data['error'] !== 0) {

                $data['user'] = $this->input->post();
            } else {
                $data['msg'] = 'Password has been changed successfully';
            }

            if($userRole == 1)
                $this->load->view('fj-mainpage', $data);
            else
                $this->load->view('fj-mainpage-recuiter', $data);
        }
    }

    /**
     * Description : Use to display change password page and update password
     * Author : Synergy
     * @param array $_POST
     * @return render data into view
     */
    function myAccounts() {

        $userData = $this->session->userdata['logged_in'];
        $userId = $userData->id;
        $userRole = $userData->role;

        $data['accountInfo'] = $this->user_model->getMyAccountInfo($userId);
        $data['main_content'] = 'fj-recruiter-myaccount';

        $query = $this->db->query("SELECT * FROM fj_account WHERE userId = '".$userId."'")->row();
        $mediaUrl = '';
        if($query) {
            $mediaUrl = $query->mediaUrl;
        } else {
            $mediaUrl = '';
        }

        if (isset($_POST) && !empty($_POST)) {
            if($query) {
                $this->db->update('fj_account', array('mediaUrl' => $_POST['mediaUrl']), array('userId' => $userId));
                $mediaUrl = $_POST['mediaUrl'];
            } else {
                $this->db->insert('fj_account', array('mediaUrl' => $_POST['mediaUrl'], 'userId' => $userId));
                $mediaUrl = $_POST['mediaUrl'];
            }
        }
        $data['mediaUrl'] = $mediaUrl;
        $this->load->view('fj-mainpage-recuiter', $data);
    }
    
    /**
     * Description : Use to check or validate password
     * Author : Synergy
     * @param string $str
     * @return boolean
     */
    public function passwordcheck($str)
    {
        if (preg_match('#[0-9]#', $str) || preg_match('#[a-zA-Z]#', $str)) {
            return TRUE;
        }
        return FALSE;
    }
    
    /**
     * Description : Use to validaate key
     * Author : Synergy
     * @param string $key
     * @return render data into view
     */
    function validateKey($key) {
        $result = $this->user_model->getUserByKey($key);
        if (count($result) > 0) {
            $data['status'] = 1;
            $userId = $result['id'];
            $this->user_model->updateUser($data, $userId);
            $msg = "Activated successfully!";
        } else {
            $msg = "Invalid Request";
        }
        $data['msg'] = $msg;
        $data['main_content'] = 'fj-verification';
        $this->load->view('fj-loginpage', $data);
    }

    /**
     * AddCorporateUser function
     * Discription : Creates a new corporatge user.
     * Author : Synergy
     * @param array $_POST
     * @return render data into view
     */
    function addCorporateUser() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $baseUrl    = $this->config->item('base_url');
        $userData   = $this->session->userdata['logged_in'];
        $token      = $this->config->item('accessToken');
        if (isset($_POST) && $_POST != NULL) {
            $resp['error']  = 0;
            $fromEmail      = $this->config->item('fromEmail');
            if ($token != $this->input->post('accessToken', true)) {
                $resp['error'] = 'You do not have pemission.';
            }
            $this->form_validation->set_rules('company', 'Company', 'trim|required');
            $this->form_validation->set_rules('addressLine1', 'Address Line 1', 'trim|required');
            $this->form_validation->set_rules('city', 'City', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[fj_users.email]');
            $this->form_validation->set_message('is_unique', 'Email address already exist!');
            $this->form_validation->set_rules('validFrom', 'Valid From', 'trim|required');
            $this->form_validation->set_rules('validTo', 'Valid To', 'trim|required');
            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }

            if (!$resp['error']) {
                $data['company']        = $this->input->post('company', true);
                $data['addressLine1']   = $this->input->post('addressLine1', true);
                $data['addressLine2']   = $this->input->post('addressLine2', true);
                $data['city']           = $this->input->post('city', true);
                $data['state']          = $this->input->post('state', true);
                $data['country']        = $this->input->post('country', true);
                $data['pinCode']        = $this->input->post('pinCode', true);
                $data['phone']          = $this->input->post('phone', true);
                $data['fax']            = $this->input->post('fax', true);
                $data['email']          = $this->input->post('email', true);
                $data['web']            = $this->input->post('web', true);
                $data['industry']       = $this->input->post('industry', true);
                $data['aboutCompany']   = $this->input->post('aboutCompany', true);
                $data['validFrom']      = date("Y-m-d", strtotime($this->input->post('validFrom', true)));
                $data['validTo']        = date("Y-m-d", strtotime($this->input->post('validTo', true)));
                $data['typeFlag']       = $this->input->post('typeFlag', true);
                $data['statusFlag']     = $this->input->post('statusFlag', true);
                $data['userLimit']      = $this->input->post('userLimit', true);
                $data['createdAt']      = date('Y-m-d h:i:s');
                $data['updatedAt']      = date('Y-m-d h:i:s');
                $password               = $this->generateRandomString();
                $data['password']       = password_hash($password, PASSWORD_BCRYPT);
                $data['createdBy']      = $userData->id;
                $data['companyLogo']    = $this->input->post('coprorateLogoName', true);
                $data['companyCode']    = $this->input->post('companyCode', true);
                $data['role']           = 2;

                try {
                    if (isset($_FILES['companylogo'])) {
                        $imageName = $this->fileUpload($_FILES);
                        if (!$imageName) {
                            $resp['message'] = 'There is a server problem occured to upload image. Please make sure image type is png or jpg!';
                        }
                    }
                    $resp = $this->user_model->addUser($data);
                    if ($resp) {
                        $this->db->insert('fj_company_code', array('companyId' => $resp, 'companyName' => $this->input->post('company'), 'companyCode' => $this->input->post('companyCode'), 'createdBy' => $userData->id));

                        //Inserting company config data
                        $this->db->insert('fj_companyConfig', array('mainUserId' => $resp, 'companyId' => $resp, 'companyConfigParamId' => 1, 'paramValue' => 1));

                        $this->db->insert('fj_companyConfig', array('mainUserId' => $resp, 'companyId' => $resp, 'companyConfigParamId' => 2, 'paramValue' => 1));
                        //End of Inserting company config data

                        $temp['password']   = $password;
                        $temp['email']      = $data['email'];
                        $temp['roleName']   = $roleName;
                        $temp['name']       = $data['company'];
                        $temp['url']        = $baseUrl . 'users/login';
                        //echo "Testing......."; die();

                        $this->email->from($fromEmail, 'VDOHire');
                        $this->email->to($data['email']);
                        // Send username and password. 
                        $this->email->subject('VDOHire Login Credentials');
                        $body = $this->load->view('emails/registerTemp.php', $temp, TRUE);
                        $this->email->message($body);
                        $this->email->set_mailtype('html');
                        $this->email->send();
                        $data['message']        = 'Successfully created!';
                        $data['token']          = $token;
                        $data['cities']         = $this->user_model->getCities();
                        $data['states']         = $this->user_model->getStates($this->input->post('city', true));
                        $data['industries']     = $this->user_model->getIndustries();
                        $data['main_content']   = 'fj-add-corporate';
                        $this->load->view('fj-mainpage', $data);
                    }
                } catch (Exception $e) {
                    $resp['error'] = $e->getMessage();
                }
            } else {
                $data['error']          = $resp;
                $data['user']           = $this->input->post();
                $data['main_content']   = 'fj-add-corporate';
                $data['token']          = $token;
                $data['cities']         = $this->user_model->getCities();
                $data['states']         = $this->user_model->getStates($this->input->post('city', true));
                $data['industries']     = $this->user_model->getIndustries();
                $data['main_content']   = 'fj-add-corporate';
                $this->load->view('fj-mainpage', $data);
            }
        } else {
            $data['main_content']   = 'fj-add-corporate';
            $data['token']          = $token;
            $data['cities']         = $this->user_model->getCities();
            $data['states']         = $this->user_model->getStates();
            $data['industries']     = $this->user_model->getIndustries();
            $this->load->view('fj-mainpage', $data);
        }
    }

    /**
     * Description : Use to edit a specific corporate user
     * Author : Synergy
     * @param int $id, array $_POST
     * @return render data into view
     */
    function editCorporateUser($id) {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }

        $user           = $this->user_model->getUser($id);
        $pageTemplate   = 'fj-edit-corporate';
        $userData       = $this->session->userdata['logged_in'];
        $token = $this->config->item('accessToken');

        if (isset($_POST) && $_POST != NULL) {
            $resp['error'] = 0;
            $fromEmail = $this->config->item('fromEmail');
            if ($token != $this->input->post('accessToken', true)) {
                $resp['error'] = 'You do not have pemission.';
            }
            $this->form_validation->set_rules('company', 'Company', 'trim|required');
            $this->form_validation->set_rules('addressLine1', 'Address Line 1', 'trim|required');
            $this->form_validation->set_rules('city', 'City', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
            $this->form_validation->set_message('is_unique', 'Email address already exist!');
            $this->form_validation->set_rules('validFrom', 'Valid From', 'trim|required');
            $this->form_validation->set_rules('validTo', 'Valid To', 'trim|required');

            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }

            if (!$resp['error']) {
                $data['company'] = $this->input->post('company', true);
                $data['addressLine1'] = $this->input->post('addressLine1', true);
                $data['addressLine2'] = $this->input->post('addressLine2', true);
                $data['city'] = $this->input->post('city', true);
                $data['state'] = $this->input->post('state', true);
                $data['country'] = $this->input->post('country', true);
                $data['pinCode'] = $this->input->post('pinCode', true);
                $data['phone'] = $this->input->post('phone', true);
                $data['fax'] = $this->input->post('fax', true);
                $data['email'] = $this->input->post('email', true);
                $data['web'] = $this->input->post('web', true);
                $data['industry'] = $this->input->post('industry', true);
                $data['aboutCompany'] = $this->input->post('aboutCompany', true);
                $data['validFrom'] = date("Y-m-d", strtotime($this->input->post('validFrom', true)));
                $data['validTo'] = date("Y-m-d", strtotime($this->input->post('validTo', true)));
                $data['typeFlag'] = $this->input->post('typeFlag', true);
                $data['statusFlag'] = $this->input->post('statusFlag', true);
                $data['userLimit'] = $this->input->post('userLimit', true);
                $data['createdAt'] = date('Y-m-d h:i:s');
                $data['updatedAt'] = date('Y-m-d h:i:s');
                $password = $this->generateRandomString();
                $data['createdBy'] = $userData->id;
                $data['companyLogo'] = $this->input->post('coprorateLogoName', true);
                $data['companyCode']    = $this->input->post('companyCode', true);
                $data['role'] = 2;

                try {
                    if (isset($_FILES['companylogo'])) {
                        $imageName = $this->fileUpload($_FILES);
                        if (!$imageName) {
                            $resp['message'] = 'There is a server problem occured to upload image. Please make sure image type is png or jpg!';
                        }
                    }
                    $resp = $this->user_model->updateUser($data, $id);
                    if ($resp) {
                        $this->db->update('fj_company_code', array('companyName' => $this->input->post('company'), 'companyCode' => $this->input->post('companyCode')), array('companyId' => $id));

                        if($resp=='Record Not Found' || $resp=='Email Id Already Exists') {
                            $resp['error'] = $resp;
                            $data['error'] = array($resp);
                            $data['user'] = $this->input->post();
                            $data['main_content'] = $pageTemplate;
                            $data['token'] = $token;
                            $data['cities'] = $this->user_model->getCities();
                            $data['states'] = $this->user_model->getStates($this->input->post('city', true));
                            $data['industries'] = $this->user_model->getIndustries();
                            $data['main_content'] = 'fj-edit-corporate';
                            $this->load->view('fj-mainpage', $data);
                        }
                        else { 
                            $temp['password'] = $password;
                            $temp['email'] = $data['email'];
                            $temp['roleName'] = $roleName;
                            $this->email->from($fromEmail, 'VDOHire');
                            $this->email->to($data['email']);
                            $this->email->subject('VDOHire Login Credentials');
                            $body = $this->load->view('emails/registerTemp.php', $temp, TRUE);
                            $this->email->message($body);
                            $this->email->set_mailtype('html');
                            $this->email->send();
                            $data['message'] = 'Successfully created!';
                            $data['token'] = $token;
                            $data['cities'] = $this->user_model->getCities();
                            $data['states'] = $this->user_model->getStates($this->input->post('city', true));
                            $data['industries'] = $this->user_model->getIndustries();
                            $data['main_content'] = $pageTemplate;
                            redirect('users/list/corporate/1', 'refresh');
                        }
                    }
                } catch (Exception $e) {
                    $resp['error'] = $e->getMessage();
                }
            } else {

                $data['error'] = $resp;
                $data['user'] = $this->input->post();
                $data['main_content'] = $pageTemplate;
                $data['token'] = $token;
                $data['cities'] = $this->user_model->getCities();
                $data['states'] = $this->user_model->getStates($this->input->post('city', true));
                $data['industries'] = $this->user_model->getIndustries();
                $data['main_content'] = 'fj-edit-corporate';
                $this->load->view('fj-mainpage', $data);
            }
        } else {
            $data['main_content'] = $pageTemplate;
            $data['token'] = $token;
            $data['user'] = $user;
            $data['cities'] = $this->user_model->getCities();
            $data['states'] = $this->user_model->getStates();
            $data['industries'] = $this->user_model->getIndustries();
            $this->load->view('fj-mainpage', $data);
        }
    }

    /**
     * Description : Use to get states by city
     * Author : Synergy
     * @param array $_POST
     * @return string of html elements
     */
    function getState() {
        $city = $this->input->post('city', true);
        $states = $this->user_model->getStates($city);
        $selectDropDown = '<select class="selectpicker" name="state" id="state">';
        $selectDropDown .= '<option value="0"> Please select state</option>';
        foreach ($states as $item) {
            $selectDropDown .= '<option value="' . $item['state'] . '"> ' . $item['state'] . '</option>';
        }
        echo $selectDropDown .= '</select>';
        return;
    }

    /**
     * CompanyLogoUpload function
     * Uploads a file.
     * @return string the uploaded file or boolean false.
     */
    function companyLogoUpload() {
        $files = $_FILES;
        $typeArr = explode('/', $files['companylogo']['type']);
        $imgName = rand() . date('ymdhis') . '_company_logo.' . $typeArr[1];
        $data['image'] = $imgName;
        $userImageType = $this->config->item('userImageType');
        $tmp_name = $files["companylogo"]["tmp_name"];
        if (in_array($typeArr[1], $userImageType)) {
            $uploadResp = $this->user_model->imageUpload($tmp_name, 'uploads/corporateLogo', $imgName);
            echo $uploadResp;
            die();
            return $uploadResp;
        } else {
            return false;
        }
    }

    /**
     * Description : Use to get all locations
     * Author : Synergy
     * @param none
     * @return array of locations
     */
    function getLocation() {
        $locations = $this->user_model->getLocations();
    }

    /**
     * Description : Use to display super admin dashboard
     * Author : Synergy
     * @param int $page, array $_POST
     * @return array of data
     */
    function superAdminDashboard($page) {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $userData               = $this->session->userdata['logged_in'];
        $userRole               = $userData->role;
        $userLoggedinUserId     = $userData->id;
        $data['page']           = $page;
        $resp['error']          = 0;
        $serachColumn           = $this->input->post('serachColumn', true);
        $searchText             = $this->input->post('serachText', true);
        $token                  = $this->config->item('accessToken');
        $fromEmail              = $this->config->item('fromEmail');
        $itemsPerPage           = $this->config->item('itemsPerPage');
        $data['itemsPerPage']   = $itemsPerPage;
        $createdBy = $userData->id;
        if (!$page) {
            $page = 0;
        } else {
            $page = $page - 1;
        }
        $offset = $page * $itemsPerPage;
        try {
            $role = 2;
            $result = $this->user_model->listUsers($itemsPerPage, $offset, $serachColumn, $searchText, $createdBy);
            $completeResult = $this->user_model->listUsers(NULL, NULL, NULL, $searchText, $createdBy);
            $count                  = ceil(count($completeResult) / $itemsPerPage);
            $data['count']          = $count;
            $data['totalCount']     = count($completeResult);
            $data['content']        = $result;
            $data['main_content']   = 'fj-superadmin-dashboard';
            $data['searchText']     = $searchText;
            $data['serachColumn']   = $serachColumn;
            $data['token']          = $token;
            
            $data['totalClient']            = $this->superadminreport_model->totalClient($userLoggedinUserId);
            $data['activeClient']           = $this->superadminreport_model->activeClient($userLoggedinUserId);
            $data['lastMonthClient']        = $this->superadminreport_model->lastMonthClient($userLoggedinUserId);
            $data['lastQuarterClient']      = $this->superadminreport_model->lastQuarterClient($userLoggedinUserId);
            
            $data['totalApplication']       = $this->superadminreport_model->totalApplication($userLoggedinUserId);
            $data['viewedApplication']      = $this->superadminreport_model->viewedApplication($userLoggedinUserId);
            $data['pendingApplication']     = $this->superadminreport_model->pendingApplication($userLoggedinUserId);
            $data['shortlistedApplication'] = $this->superadminreport_model->shortlistedApplication($userLoggedinUserId);
            
            $data['totalJobs']              = $this->superadminreport_model->totalJobs($userLoggedinUserId);
            $data['auditionViewed']         = $this->superadminreport_model->auditionViewed($userLoggedinUserId);
            $data['invitedUsers']           = $this->superadminreport_model->invitedUsers($userLoggedinUserId);
            
            $data['totalAppUser']           = $this->superadminreport_model->totalAppUser($userLoggedinUserId);
            $data['totalAudition']          = $this->superadminreport_model->totalAudition($userLoggedinUserId);
            $data['loggedInUser']           = $this->superadminreport_model->loggedInUser($userLoggedinUserId);
                
            $this->load->view('fj-mainpage', $data); //die();
            
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Description : Use to get sms and email text content of a particular job
     * Author : Synergy
     * @param int $jobId
     * @return json encoded data
     */
    function getUserSmsEmailContent($jobId) {
        $userData = $this->session->userdata['logged_in'];
        $userId = $userData->id;
        $userCompany = $userData->company;
        $jobvalue = getJobDetail($jobId);

        $emailContent = "<p>Dear Candidate, </p>
                            <p>Congratulations!</p>
                            <p>Your candidature is shortlisted for the next round of interview & you are invited for a digital interview for the following job:</p>
                                Position : ".$jobvalue['title']."<br/>
                                Company : ".ucwords($userCompany)."<br/>
                                Job Code : ".$jobvalue['fjCode']."<br/>
                                <p>You can now submit your digital interview from anywhere anytime. You will simply need to record your answers to preset interview questions through your computer or smartphone. </p>
                                <p>Please use any of the below options to view more details for this job and to complete your digital interview process:</p><br/>
                                <p><i>Through Web:</i></p>
                                <p><a class='interviewUrl' href='https://vdohire.com'>Click on this link</a> to see more details and initiate your digital interview process.</p>
                                <p>Alternatively, you can also initiate the process through following steps : </p>
                                &nbsp;&nbsp;-   Log on to VDOHire.com and click on “Login for Jobseeker”.<br/>
                                &nbsp;&nbsp;-   Signup by clicking on Register. Login if you have already registered before.<br/> 
                                &nbsp;&nbsp;-   Enter the job code given above in the field which says “Apply the job code” to initiate your interview.<br/><br/><br/>
                                <p><i>Through Andorid Phone :</i></p>
                                &nbsp;&nbsp;-   Download the android app from <a href='".$this->config->item('appPlaystoreList')."'>Google Play Store.</a><br/>
                                &nbsp;&nbsp;-   Register by clicking on signup. Login if you have already registered before on VDOHire.<br/> 
                                &nbsp;&nbsp;-   Enter the job code mentioned above to initiate your interview.<br/><br/><br/>
                                <p>You may visit this page https://goo.gl/6HNpLQ to understand the process better.  In case if you face any issue with your process please mail us at support@vdohire.com</p><br/>
                                <p>We look forward to receiving your application.  Wish you good luck. </p>
                                Thanks<br/>
                                VDOHire Admin<br/>";

        $query = $this->db->query("SELECT * FROM fj_smsmail_messages WHERE userId = '".$userId."' AND jobId = '".$jobId."'");
        $row = $query->row();
        $allData = array();
        if ($query->num_rows() > 0) {
            $allData['smsContent'] = $row->smsContent;
            $allData['emailSubject'] = $row->emailSubject;
            $allData['emailContent'] = $row->emailContent;
        } else {
            $interviewUrl = $_SERVER["HTTP_HOST"].'/jobseeker/job/invitation/'.$jobHashId;
            
            $allData['smsContent'] = "You are invited by ".ucwords($userCompany).", for digital interview round for ".$jobvalue['title'].". Please login at VDOHire.com or download android app through ".$this->config->item('appPlaystoreList')." . Use code ".$jobvalue['fjCode']." initiate the process.";
            $allData['emailSubject'] = 'Invitation for digital Interview by '.ucwords($userCompany);
            $allData['emailContent'] = $emailContent;
        }
        echo json_encode($allData);
    }
    
    /**
     * Description : Use to get details of a particular user which is viewed from searched result page
     * Author : Synergy
     * @param int $userId
     * @return json encoded data
     */
    function getSearchedUserData($userId) {
        $query = $this->user_model->getSearchedUserData($userId);
        echo json_encode($query);
    }

    /**
     * Description : Use to invite list of users on a particular job
     * Author : Synergy
     * @param int $jobId, array $_POST
     * @return boolean
     */
    function inviteUsers($jobId) {
        $config['charset'] = 'utf-8';
        $config['mailtype'] = 'html';
        $config['newline']  = "\r\n";
        $config['crlf'] = "\r\n";
        $this->email->initialize($config);

        $userData = $this->session->userdata['logged_in'];
        
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
            $userRole = $userData->role;
            $userCompany = $userData->company;
        } else {
            redirect(base_url('users/login'));
        }
        
        $inviteUsersPath = $this->config->item('inviteUsersPath');
        
        $inviteFrom = $this->input->post('inviteFrom', true);
        $smsMessage = $this->input->post('smsMessage');
        $emailSubject = $this->input->post('emailSubject');
        $emailMessage = $this->input->post('emailMessage');
        $emailMessage1 = $this->input->post('emailMessage');
        
        $temp['job'] = getJobDetail($jobId);
        $jobvalue = getJobDetail($jobId);
        $path = $inviteUsersPath . 'inviteUsers' . $userData->id . '.csv';
        if (file_exists($path))
            unlink($path);
        $file = $this->excelUpload($_FILES, $path);
        $file = fopen($path, "r");
        $i = 0;
        while (!feof($file)) {
            $fileData = fgetcsv($file);
            if ($i != 0) {
                $fileArr[] = $fileData;
            } else {
                if (strtolower($fileData[1]) != 'email') {
                    echo "invalid";
                    exit;
                }
            }
            $i++;
        }

        if(count($fileArr)<=1) {
            echo "noRecord";exit;
        }

        $fromEmail = $this->config->item('fromEmail');
        
        if( $inviteFrom != "" ){
            $inviteFromArr = explode(",", $inviteFrom);
        } else {
            $inviteFromArr = array();
        }
        
        $allUserIds = array();
		
        foreach ($fileArr as $item) {
            $this->email->clear();
            $this->email->from($fromEmail, 'VDOHire');
            $email = $item[1];
            $name = $item[0];
            $mobileNumber = $item[2];            
            
            $recruiterJobId = $this->user_model->get_user_by_email1($email);
            if($recruiterJobId) {
                continue;
            }

            $deviceId = $this->user_model->get_user_by_email($email);
            if($deviceId) {
                $allUserIds[] = $deviceId;
            }
            
            if( in_array(1, $inviteFromArr) && in_array(2, $inviteFromArr) ){
                
                if (isset($email) && $email != ''  || isset($mobileNumber) && $mobileNumber != '') {
                    
                    if($deviceId) {
                        $invData['userId'] = $deviceId;
                    }

                    $invData['userName'] = $name;
                    $invData['email'] = $email;
                    $invData['userMobile'] = $mobileNumber;
                    $invData['invitationBy'] = "1,2";
                    $invData['jobId'] = $jobId;
                    $invData['invitedBy'] = $userData->id;
                    $invData['createdAt'] = date('Y-m-d h:i:s');
                    $invData['updatedAt'] = date('Y-m-d h:i:s');
                    $jobHashId = $this->user_model->saveInvitation($invData);

					$interviewUrl = 'https://'.$_SERVER["HTTP_HOST"].'/jobseeker/job/invitation/'.$jobHashId;
					
                    $doc = new DOMDocument();
                    $doc->loadHTML($emailMessage);
                    $items = $doc->getElementsByTagName('a');

                    for ($i = 0; $i < $items->length; $i++) {
                        $className = $items->item($i)->getAttribute('class');
                        if($className == 'interviewUrl') {
                            $items->item($i)->setAttribute('href', $interviewUrl);
                        }
                    }
                    // save html
                    $emailMessage = $doc->saveHTML();
                    //till here code for creating the invitation URL
					
                    $temp['email'] = $email;
                    $temp['name'] = $name;
                    $temp['emailContent'] = $emailMessage;
                    $this->email->to($email);
                    $this->email->subject($emailSubject);
                    $body = '<div class="container">
                                <div class="row">'.$emailMessage.'<div>
                            </div>';
                    $this->email->message($body);
                    $this->email->set_mailtype('html');
                    $this->email->send();
                    
                    //SMS send function
                    sendSMSMessage($mobileNumber, $smsMessage);

                    //Update insert data into smsemail table
                    $insertUpdateData['smsContent'] = $smsMessage;
                    $insertUpdateData['emailSubject'] = $emailSubject;
                    $insertUpdateData['emailContent'] = $emailMessage1;
                    $insertUpdateData['userId'] = $userId;
                    $insertUpdateData['jobId'] = $jobId;

                    $query = $this->db->query("SELECT * FROM fj_smsmail_messages WHERE userId = '".$userId."' AND jobId = '".$jobId."'");
                    $row = $query->row();
                    if ($query->num_rows() > 0) {
                        $this->db->where('userId', $userId);
                        $this->db->where('jobId', $jobId);
                        $this->db->update('fj_smsmail_messages', $insertUpdateData);
                    } else {
                        $this->db->insert('fj_smsmail_messages', $insertUpdateData);
                    }
                }
                
            } elseif( in_array(1, $inviteFromArr) && !in_array(2, $inviteFromArr) ){
                
                if (isset($email) && $email != '') {
                    
                    if($deviceId) {
                        $invData['userId'] = $deviceId;
                    }

                    $invData['userName'] = $name;
                    $invData['email'] = $email;
                    $invData['userMobile'] = $mobileNumber;
                    $invData['invitationBy'] = 1;
                    $invData['jobId'] = $jobId;
                    $invData['invitedBy'] = $userData->id;
                    $invData['createdAt'] = date('Y-m-d h:i:s');
                    $invData['updatedAt'] = date('Y-m-d h:i:s');
                    $jobHashId = $this->user_model->saveInvitation($invData);
					
					$interviewUrl = 'https://'.$_SERVER["HTTP_HOST"].'/jobseeker/job/invitation/'.$jobHashId;
					
                    $doc = new DOMDocument();
                    $doc->loadHTML($emailMessage);
                    $items = $doc->getElementsByTagName('a');

                    for ($i = 0; $i < $items->length; $i++) {
                        $className = $items->item($i)->getAttribute('class');
                        if($className == 'interviewUrl') {
                            $items->item($i)->setAttribute('href', $interviewUrl);
                        }
                    }
                    // save html
                    $emailMessage = $doc->saveHTML();
					//till here code for creating the invitation URL 

                    $temp['email'] = $email;
                    $temp['name'] = $name;
                    $temp['emailContent'] = $emailMessage;
                    $this->email->to($email);
                    $this->email->subject($emailSubject);
                    $body = '<div class="container">
                                <div class="row">'.$emailMessage.'<div>
                            </div>';
                    $this->email->message($body);                    
                    $this->email->set_mailtype('html');
                    $this->email->send();

                    //Update insert data into smsemail table
                    $insertUpdateData['smsContent'] = $smsMessage;
                    $insertUpdateData['emailSubject'] = $emailSubject;
                    $insertUpdateData['emailContent'] = $emailMessage1;
                    $insertUpdateData['userId'] = $userId;
                    $insertUpdateData['jobId'] = $jobId;

                    $query = $this->db->query("SELECT * FROM fj_smsmail_messages WHERE userId = '".$userId."' AND jobId = '".$jobId."'");
                    $row = $query->row();
                    if ($query->num_rows() > 0) {
                        $this->db->where('userId', $userId);
                        $this->db->where('jobId', $jobId);
                        $this->db->update('fj_smsmail_messages', $insertUpdateData);
                    } else {
                        $this->db->insert('fj_smsmail_messages', $insertUpdateData);
                    }
                }
            } elseif( !in_array(1, $inviteFromArr) && in_array(2, $inviteFromArr) ){
                
                if (isset($email) && $email != '' || isset($mobileNumber) && $mobileNumber != '' ) {
                    
                    if($deviceId) {
                        $invData['userId'] = $deviceId;
                    }

                    $invData['userName'] = $name;
                    $invData['email'] = $email;
                    $invData['userMobile'] = $mobileNumber;
                    $invData['invitationBy'] = 2;
                    $invData['jobId'] = $jobId;
                    $invData['invitedBy'] = $userData->id;
                    $invData['createdAt'] = date('Y-m-d h:i:s');
                    $invData['updatedAt'] = date('Y-m-d h:i:s');
                    $jobHashId = $this->user_model->saveInvitation($invData);
					
					$interviewUrl = 'https://'.$_SERVER["HTTP_HOST"].'/jobseeker/job/invitation/'.$jobHashId;
					
					$doc = new DOMDocument();
                    $doc->loadHTML($emailMessage);
                    $items = $doc->getElementsByTagName('a');

                    for ($i = 0; $i < $items->length; $i++) {
                        $className = $items->item($i)->getAttribute('class');
                        if($className == 'interviewUrl') {
                            $items->item($i)->setAttribute('href', $interviewUrl);
                        }
                    }
                    // save html
                    $emailMessage = $doc->saveHTML();

					//till here code for creating the invitation URL 
                    //SMS send function
                    sendSMSMessage($mobileNumber, $smsMessage);  

                    //Update insert data into smsemail table
                    $insertUpdateData['smsContent'] = $smsMessage;
                    $insertUpdateData['emailSubject'] = $emailSubject;
                    $insertUpdateData['emailContent'] = $emailMessage1;
                    $insertUpdateData['userId'] = $userId;
                    $insertUpdateData['jobId'] = $jobId;

                    $query = $this->db->query("SELECT * FROM fj_smsmail_messages WHERE userId = '".$userId."' AND jobId = '".$jobId."'");
                    $row = $query->row();
                    if ($query->num_rows() > 0) {
                        $this->db->where('userId', $userId);
                        $this->db->where('jobId', $jobId);
                        $this->db->update('fj_smsmail_messages', $insertUpdateData);
                    } else {
                        $this->db->insert('fj_smsmail_messages', $insertUpdateData);
                    }
                }
            }
        }
        $this->userId = $allUserIds;
        $this->jobCode = $jobvalue['fjCode'];
        $this->type = 'jobInvitation';
        
        processApiSuccess($this);
		
    }

    /**
     * Description : Use to send email to app users
     * Author : Synergy
     * @param array $_POST
     * @return boolean or redirect to search candidate page
     */
    function sendEmailUsersThroughApp() {
        $userData = $this->session->userdata['logged_in'];        
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
            $userRole = $userData->role;
            $userCompany = $userData->company;
        } else {
            redirect(base_url('users/login'));
        }        
        $userListInvite = $this->input->post('userListInvite', true);
        $emailSubject   = $this->input->post('emailSubject', true);
        $emailMessage   = $this->input->post('emailMessage', true);              
        $userList   = explode(',', $userListInvite);        
        $fromEmail  = $this->config->item('fromEmail');
        $this->email->from($fromEmail, 'VDOHire');

        $allUserIds = array();

        foreach ($userList as $user) {
            $allUserIds[] = $user;
            $userDetail     = getUserById($user);
            $email          = $userDetail['email'];            
            $temp['email']  = $email;
            $temp['name']   = $userDetail['fullname'];
            $i = 0;            
            $this->email->to($email);
            $this->email->subject($emailSubject);
            $this->email->message($emailMessage);
            $this->email->set_mailtype('html');
            $this->email->send();
            
            $currentTime            = date('Y-m-d h:i:s');
            $data['userTwoid']      = $user;
            $data['messageSubject'] = $emailSubject;
            $data['messageText']    = $emailMessage;
            $data['userOneId']      = $userId;
            $data['createdAt']      = $currentTime;
            // SAVE SENT MESSAGE
            $resp = $this->message_model->addMessage($data);
        }

        $this->userId = $allUserIds;
        $this->type = 'message';
        processApiSuccess($this);

        redirect(base_url('users/searchCandidate'));
    }
    
    /**
     * Description : Use to send email to app users from csv files
     * Author : Synergy
     * @param array $_POST
     * @return boolean or redirect to search candidate page
     */
    function sendEmailUsersThroughCSV() {
        $userData = $this->session->userdata['logged_in'];        
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
            $userRole = $userData->role;
            $userCompany = $userData->company;
        } else {
            redirect(base_url('users/login'));
        }        
        $userListInvite = $this->input->post('userListInvite', true);
        $emailSubject   = $this->input->post('emailSubject', true);
        $emailMessage   = $this->input->post('emailMessage', true);
        
        $inviteUsersPath    = $this->config->item('inviteUsersPath');        
        $path               = $inviteUsersPath . 'sendEmail' . $userData->id . date('YmdHis') . '.csv';
        if (file_exists($path))
            unlink($path);
        $file = $this->excelUpload($_FILES, $path, 'userListInvite');
        $file = fopen($path, "r");
        $i = 0;
        while (!feof($file)) {
            $fileData = fgetcsv($file);
            if ($i != 0) {
                $fileArr[] = $fileData;
            } else {
                if (strtolower($fileData[1]) != 'email') {
                    echo "invalid";
                    exit;
                }
            }
            $i++;
        }        
      
        $fromEmail  = $this->config->item('fromEmail');
        $this->email->from($fromEmail, 'VDOHire');
        foreach ($fileArr as $user) {
            $email          = $user[1];
            $temp['email']  = $email;
            $temp['name']   = $user[0];
            $i = 0;            
            $this->email->to($email);
            $this->email->subject($emailSubject);
            $this->email->message($emailMessage);
            $this->email->set_mailtype('html');
            $this->email->send();
        }
        redirect(base_url('users/searchCandidate'));
    }
    
    /**
     * excelUpload function
     * Uploads a file.
     * @param array $files.
     * @return string the uploaded file or boolean false.
     */
    function excelUpload($files, $path, $name='') {
        $userData = $this->session->userdata['logged_in'];
        $inviteUsersPath = $this->config->item('inviteUsersPath');
        $name = ($name==''?'inviteUsers':$name);
        $typeArr = explode('.', $files[$name]['name']);
        $fileName = 'inviteUsers' . $userData->id . date('YmdHis') . '.' . $typeArr[1];
        $data['image'] = $fileName;
        $fileType = array('csv');
        //print_r($typeArr); exit;        
        if (in_array($typeArr[1], $fileType)) {
            $tmp_name = $files[$name]["tmp_name"];
            if (move_uploaded_file($tmp_name, $path)) {
                return $fileName;
            } else {
                return false;
            }
        } else {
            echo "invalid";
            exit;
        }
    }

    /**
     * csvUpload function
     * Uploads a file.
     * @param array $files.
     * @return string the uploaded file or boolean false.
     */
    function csvUpload($files, $path, $name='') {
        $userData = $this->session->userdata['logged_in'];
        $inviteUsersPath = $this->config->item('inviteUsersPath');
        $name = ($name==''?'inviteUsers':$name);
        $typeArr = explode('.', $files[$name]['name']);
        $fileName = $name . $userData->id . date('YmdHis') . '.' . $typeArr[1];
        $data['image'] = $fileName;
        $fileType = array('csv');
        //print_r($typeArr); exit;        
        if (in_array($typeArr[1], $fileType)) {
            $tmp_name = $files[$name]["tmp_name"];
            if (move_uploaded_file($tmp_name, $path)) {
                return $fileName;
            } else {
                return false;
            }
        } else {
            return 'invalidFileFormat';
        }
    }

    /**
     * Description : Use to invite app users by email and message 
     * Author : Synergy
     * @param array $_POST
     * @return boolean
     */
    function inviteUsersThroughApp() {
        $userData = $this->session->userdata['logged_in'];
        
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
            $userRole = $userData->role;
            $userCompany = $userData->company;
        } else {
            redirect(base_url('users/login'));
        }
        
        $userListInvite = $this->input->post('userListInvite', true);
        $jobs = $this->input->post('myJobs', true);
        $inviteFrom = $this->input->post('inviteFrom', true);
        
        if( !empty($inviteFrom) ){
            $inviteFromArr = $inviteFrom;
        } else {
            $inviteFromArr = array();
        }
        
        $userList = explode(',', $userListInvite);
        $fromEmail = $this->config->item('fromEmail');
        $this->email->from($fromEmail, 'VDOHire');
        foreach ($userList as $user) {
            $userDetail = getUserById($user);
            $email = $userDetail['email'];
            $userMobileNumber = $userDetail['mobile'];
            
            $temp['email'] = $email;
            $temp['name'] = $userDetail['fullname'];
            $i = 0;

            foreach ($jobs as $item) {
                $temp['jobs'][$i] = getJobDetail($item);
                $i++;
            }
            
            if( in_array(1, $inviteFromArr) && in_array(2, $inviteFromArr) ){
                
                // send message for internal
                foreach($jobs as $item){
                    $invData['email'] = $email;
                    $invData['jobId'] = $item;
                    $invData['userMobile'] = $userMobileNumber;
                    $invData['invitationBy'] = "1,2";
                    $invData['invitedBy'] = $userData->id;
                    $invData['createdAt'] = date('Y-m-d h:i:s');
                    $invData['updatedAt'] = date('Y-m-d h:i:s');
                    $invData['userId'] = $user;
                    $this->user_model->saveInvitation($invData);
                
                    $currentTime = date('Y-m-d h:i:s');
                    $jobvalue = getJobDetail($item);
                    
                    $smsmessage = "You are invited by ".ucwords($userCompany).", to give the first round of interview for ".$jobvalue['title'].". Download VDOHire app through ".$this->config->item('appPlaystoreList')." and use code ".$jobvalue['fjCode']." to apply";
                    
                    //SMS send function
                    sendSMSMessage($userMobileNumber, $smsmessage);
                    
                    $data['userTwoid'] = $user;
                    $data['messageSubject'] = 'VDOHire';
                    $data['messageText'] = $smsmessage;
                    $data['userOneId'] = $userId;
                    $data['createdAt'] = $currentTime;
                    
                    // SAVE SENT MESSAGE
                    $resp = $this->message_model->addMessage($data);   

                    $messageId = $resp;

                    if( $messageId ) {
                        
                        $dataReply['messageId'] = $messageId;
                        $dataReply['replyById'] = $userId;
                        $dataReply['messageToId'] = $user;
                        $dataReply['messageText'] = $smsmessage;
                        $dataReply['messageReadStatus'] = 1;
                        $dataReply['createdAt'] = $currentTime;

                        $respReply = $this->messagereply_model->addMessageReply($dataReply);

                        $this->userId = $user;
                        $this->type = 'jobInvitation';
                        $this->jobCode = $jobvalue['fjCode'];
                        processApiSuccess($this);
                    }
                
                }
                    
                $this->email->to($email);
                $this->email->subject('VDOHire');
                $body = $this->load->view('emails/inviteFjUserTemp.php', $temp, TRUE);
                $this->email->message($body);
                $this->email->set_mailtype('html');
                $this->email->send();
            } elseif( in_array(1, $inviteFromArr) && !in_array(2, $inviteFromArr) ){

                // send message for internal
                foreach($jobs as $item){
                    $invData['email'] = $email;
                    $invData['jobId'] = $item;
                    $invData['userMobile'] = $userMobileNumber;
                    $invData['invitationBy'] = "1";
                    $invData['invitedBy'] = $userData->id;
                    $invData['createdAt'] = date('Y-m-d h:i:s');
                    $invData['updatedAt'] = date('Y-m-d h:i:s');
                    $invData['userId'] = $user;
                    $this->user_model->saveInvitation($invData);
                    
                    $currentTime = date('Y-m-d h:i:s');
                    $jobvalue = getJobDetail($item);
                    
                    $smsmessage = "You are invited by ".ucwords($userCompany).", to give the first round of interview for ".$jobvalue['title'].". Download VDOHire app through ".$this->config->item('appPlaystoreList')." and use code ".$jobvalue['fjCode']." to apply";
                     
                    $data['userTwoid'] = $user;
                    $data['messageSubject'] = 'VDOHire';
                    $data['messageText'] = $smsmessage;
                    $data['userOneId'] = $userId;
                    $data['createdAt'] = $currentTime;
                    
                    // SAVE SENT MESSAGE
                    $resp = $this->message_model->addMessage($data);   

                    $messageId = $resp;

                    if( $messageId ) {
                        
                        $dataReply['messageId'] = $messageId;
                        $dataReply['replyById'] = $userId;
                        $dataReply['messageToId'] = $user;
                        $dataReply['messageText'] = $smsmessage;
                        $dataReply['messageReadStatus'] = 1;
                        $dataReply['createdAt'] = $currentTime;

                        $respReply = $this->messagereply_model->addMessageReply($dataReply);

                        $this->userId = $user;
                        $this->type = 'jobInvitation';
                        $this->jobCode = $jobvalue['fjCode'];
                        processApiSuccess($this);  
                    }
                }
                    
                $this->email->to($email);
                $this->email->subject('VDOHire');
                $body = $this->load->view('emails/inviteFjUserTemp.php', $temp, TRUE);
                $this->email->message($body);
                $this->email->set_mailtype('html');
                $this->email->send();
                
            } elseif( !in_array(1, $inviteFromArr) && in_array(2, $inviteFromArr) ){
                foreach($jobs as $item){
                    $invData['email'] = $email;
                    $invData['jobId'] = $item;
                    $invData['userMobile'] = $userMobileNumber;
                    $invData['invitationBy'] = "2";
                    $invData['invitedBy'] = $userData->id;
                    $invData['createdAt'] = date('Y-m-d h:i:s');
                    $invData['updatedAt'] = date('Y-m-d h:i:s');
                    $invData['userId'] = $user;
                    $this->user_model->saveInvitation($invData);
                    
                    $currentTime = date('Y-m-d h:i:s');
                    $jobvalue = getJobDetail($item);
                    
                    $smsmessage = "You are invited by ".ucwords($userCompany).", to give the first round of interview for ".$jobvalue['title'].". Download VDOHire app through ".$this->config->item('appPlaystoreList')." and use code ".$jobvalue['fjCode']." to apply";
                
                    //SMS send function
                    sendSMSMessage($userMobileNumber, $smsmessage);
                    
                    $data['userTwoid'] = $user;
                    $data['messageSubject'] = 'VDOHire';
                    $data['messageText'] = $smsmessage;
                    $data['userOneId'] = $userId;
                    $data['createdAt'] = $currentTime;

                    // SAVE SENT MESSAGE
                    $resp = $this->message_model->addMessage($data);   

                    $messageId = $resp;

                    if( $messageId ) {
                        
                        $dataReply['messageId'] = $messageId;
                        $dataReply['replyById'] = $userId;
                        $dataReply['messageToId'] = $user;
                        $dataReply['messageText'] = $smsmessage;
                        $dataReply['messageReadStatus'] = 1;
                        $dataReply['createdAt'] = $currentTime;
                        
                        $respReply = $this->messagereply_model->addMessageReply($dataReply);

                        $this->userId = $user;
                        $this->type = 'jobInvitation';
                        $this->jobCode = $jobvalue['fjCode'];
                        processApiSuccess($this);
                    }
                }
            }
        }
        redirect(base_url('users/searchCandidate'));
    }
    
    /**
     * likeDislikeUserAudition
     * like dislike user's audition
     * @return string the success message | error message.
     */
    function likeDislikeUserAuditionFunction() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token = $this->config->item('accessToken');
        $userData = $this->session->userdata['logged_in'];
       
        $userLoggedinId = $userData->id;
        $resp['error'] = 0;
        $hdnaudlikeType = $this->input->post('hdnaudlikeType', true);
        $hdnaudId = $this->input->post('hdnaudId', true);
       
        if (!$hdnaudlikeType || !$hdnaudId) {
            $error = "NotDone";
            echo $error;
            exit();
        }
        if (!$resp['error']) {
            
            $checkAlreadyLikeRes = $this->userauditionlike_model->checkAlreadyLikeAudition($userLoggedinId, $hdnaudId);
            
            if( !$checkAlreadyLikeRes ){
                $data['userId'] = $userLoggedinId;
                $data['auditionId'] = $hdnaudId;
                $data['createdAt'] = date('Y-m-d h:i:s');

                $auditionLikeRes = $this->userauditionlike_model->likeUserAudition($data);
                
                if( $auditionLikeRes ){
                    echo "2";
                    exit();                                     
                } else {
                    $error = "NotDone";
                    echo $error;
                    exit();
                }
            } else if( $checkAlreadyLikeRes && $hdnaudlikeType == 2 ){
                $data['userId'] = $userLoggedinId;
                $data['auditionId'] = $hdnaudId;
                
                $auditionLikeRes = $this->userauditionlike_model->deleteLikedAudition($data);
                if( $auditionLikeRes ){
                    echo "1";       
                    exit();
                } else {
                    $error = "NotDone";
                    echo $error;
                    exit();
                }
            } else {
                $error = "AlreadyLiked";
                echo $error;
                exit();
            }
        }
    }
    
    /**
     * Description : Use to update the status of user
     * Author : Synergy
     * @param array $_POST
     * @return void
     */
    function updateUserStatus(){
         if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        if (isset($_POST) && $_POST != NULL) {
            $userId = $this->input->post('id', true);
            $userId = decryptURLparam($userId, URLparamKey);
            $resp = $this->user_model->updateUserStatus($userId);
        }
    }
    
    /**
     * Description : Use to compress the video in ffmpeg file
     * Author : Synergy
     * @param none
     * @return boolean
     */
    function videoCompress() {
        print_r(shell_exec("ffmpeg -i input.mp4 -vcodec libx264 -crf 24 output.mp4 -y 2>&1"));
    }

    /**
     * Description : Use to display search candidate result
     * Author : Synergy
     * @param array $_POST
     * @return render data into view
     */
    function eventList($page) {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        
        $result = $this->user_model->eventList();

        $data['content']    = $result;
        $data['main_content']   = 'fj-event-listing';
        $this->load->view('fj-mainpage', $data);
    }

    /**
     * Description : Use to display search candidate result
     * Author : Synergy
     * @param array $_POST
     * @return render data into view
     */
    function customerActivity() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        
        if(isset($_POST) && !empty($_POST)) {
            $result = $this->user_model->customerActivity($_POST['dateFrom'], $_POST['dateTo']);
            $data['filter'] = $_POST;
        } else {
            $result = $this->user_model->customerActivity();
            $data['filter'] = '';
        }

        $data['content']    = $result;
        $data['main_content']   = 'fj-customer-activities-listing';
        $this->load->view('fj-mainpage', $data);
    }

    /**
     * Description : Use to display job activity
     * Author : Synergy
     * @param array $_POST
     * @return render data into view
     */
    function viewJobActivity($page) {
        //print_r($_POST);exit;
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        
        $eventId = $this->uri->segment(3);

        if(isset($_GET['date'])) {
            $date = $_GET['date'];
        } else {
            $date = '';
        }
        
        $result = $this->user_model->eventActivityList($eventId, $date);

        $data['content']    = $result;
        $data['main_content']   = 'fj-event-activity-listing';
        $this->load->view('fj-mainpage', $data);
    }

    /**
     * Description : Use to display job activity
     * Author : Synergy
     * @param array $_POST
     * @return render data into view
     */
    function viewCompletedJob($page) {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        
        $eventId = $this->uri->segment(3);
        $result = $this->user_model->viewCompletedJob($eventId);

        $data['content']    = $result;
        $data['main_content']   = 'fj-event-activity-complete-listing';
        $this->load->view('fj-mainpage', $data);
    }

    /**
     * Description : Use to display add job page and create a new job
     * Author : Synergy
     * @param null or $_POST
     * @return render data into view 
     */
    function addJob() {
        $eventId = $this->uri->segment(3);
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token      = $this->config->item('accessToken');
        $userData   = $this->session->userdata['logged_in'];

        //Getting events data
        $eventData = $this->db->query("SELECT * FROM fj_events WHERE id = '$eventId'");
        $arreventData = $eventData->row_array();

        $eventCompanyData = $this->db->query("SELECT e.companyId, u.company FROM fj_eventCompany e JOIN fj_users u ON u.id = e.companyId WHERE eventId = '$eventId'");
        $arreventCompanyData = $eventCompanyData->result();
        //End of Getting events data

        
        if (isset($_POST) && $_POST != NULL) {
            $userId = $this->input->post('companyName', true);
            $companyRowData = $this->db->query("SELECT id, fullname, company, createdBy, role, validTo FROM fj_users WHERE id = '$userId'");
            $companyAllData = $companyRowData->row();

            if (isset($userId) && $companyAllData->id != NULL) {
               $userId          = $companyAllData->id;
               $userByFullName  = $companyAllData->fullname;
               $userCompany     = $companyAllData->company;
               $createdById     = $companyAllData->createdBy;

               //Get company Code
                $arrCompanyResult = $this->db->query("SELECT * FROM fj_company_code WHERE companyId = '$userId'");
                $arrCompanyData = $arrCompanyResult->row_array();
                if($arrCompanyData)
                    $companyCode = $arrCompanyData['companyCode'];
                else
                    $companyCode = '';

            } else {
                $userId = 1;
            }
            $userRole   = $companyAllData->role;
            
            if($userRole=='2' || $userRole=='1') {
                $userValidity   = $companyRowData->validTo;
            }
            else if($userRole=='4') {
                $corporateUser      = getCorporateUser($userId);
                $queryResult        = $this->db->query("SELECT * FROM fj_users WHERE id = '$corporateUser'");
                $corporateUserData  = $queryResult->row_array();
                $userValidity       = $corporateUserData['validTo'];
            }

            $panel1 = $this->input->post('level1', true);
            $panel2 = $this->input->post('level2', true);
            $resultPanel = array_intersect($panel1, $panel2);


            $resp['error'] = '';
            $fromEmail = $this->config->item('fromEmail');
            if ($token != $this->input->post('accessToken', true)) {
                $resp['error'] = 'You do not have pemission.';
            }

            $this->form_validation->set_rules('title', 'title', 'trim|required');
            $this->form_validation->set_rules('description', 'description', 'trim|required');
            $this->form_validation->set_rules('openTill', 'OpenTill', 'trim|required|isFutureDate[fj_jobs.OpenTill]');
            $this->form_validation->set_rules('level1[]', 'Level 1 Panel', 'trim|required');
            $this->form_validation->set_message('isFutureDate', 'Open Till field must have a future date!');

            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }
            if (count($resultPanel) != 0) {
                $resp['error'] .= '<p>Panel 1 user and Panel 2 user can not be same.</p>';
            }
            
            
            
            /*$datetimeObj1   = new DateTime($userValidity);
            $datetimeObj2   = new DateTime(date("Y-m-d H:i:s", strtotime($this->input->post('openTill', true))));
            $interval       = $datetimeObj1->diff($datetimeObj2);
            $dateDiff       = $interval->format('%R%a');

            if($dateDiff > 0) {
                $resp['error'] .= '<p>Open till date is not a valid date as this account expires before.</p>';
            }*/
            
            if (!$resp['error']) {

                $data['jobId'] = $this->input->post('jobId', true);
                $data['title'] = $this->input->post('title', true);
                $locations = $this->input->post('cityState', true);
                $data['noOfVacancies'] = $this->input->post('vacancy', true);
                $data['ageFrom'] = $this->input->post('ageFrom', true);
                $data['ageTo'] = $this->input->post('ageTo', true);
                $data['salaryFrom'] = $this->input->post('salaryFrom', true);
                $data['salaryTo'] = $this->input->post('salaryTo', true);
                $data['noticePeriod'] = $this->input->post('noticePeriod', true);
                $data['expFrom'] = $this->input->post('expFrom', true);
                $data['expTo'] = $this->input->post('expTo', true);

                if($this->input->post('openTill'))
                    $data['openTill'] = date("Y-m-d", strtotime($this->input->post('openTill', true)));

                $data['description'] = $this->input->post('description', true);
                $data['assessment'] = $this->input->post('assessment', true);
                $data['interview'] = $this->input->post('interview', true);
                $data['createdAt'] = date('Y-m-d h:i:s');
                $data['updatedAt'] = date('Y-m-d h:i:s');
                $data['fjCode'] = $companyCode.$this->generateRandomString();
                $data['createdBy'] = $userId;
                $ind = getUserById($userId);
                $data['industryId'] = 5;
                $data['eventId'] = $eventId;

                //$roleName = getRoleById($data['role']);

                $location = $this->input->post('location', true);
                $qualifications = $this->input->post('qualification', true);
                try {
                    if (isset($_FILES['jd'])) {
                        $jd = $this->fileUploads($_FILES);
                        if (!$jd) {
                            $resp['message'] = 'There is a server problem occured to upload image. Please make sure image type is png or jpg!';
                        }
                    }
                    $data['jd'] = $jd;
                    $resp = $this->job_model->addJob($data);
                    if ($resp) {
                        $this->job_model->addLocation($locations, $resp);
                        $this->job_model->addJobPanel($panel1, $resp, 1);
                        $this->job_model->addJobPanel($panel2, $resp, 2);
                        $this->job_model->addJobQualification($qualifications, $resp);
                        $this->email->from($fromEmail, 'VDOHire');
                        $this->email->subject('VDOHire');
                        $this->email->set_mailtype('html');
                        foreach ($panel1 as $item) {
                            $user = getUserById($item);
                            $jobDetailsData = getJobDetail($resp);
                            
                            $notificationMsg = "You are added as a Evaluator L1 for Job ". ucwords($jobDetailsData['title'])." by ".ucwords($userCompany);
                            insertNotificationForTopBarInAdmin($item, $notificationMsg, "Job Review", $userId);

                            $temp['email'] = $user['email'];
                            $temp['jobId'] = $data['jobId'];
                            $temp['type'] = 'Evaluator L1';
                            $this->email->to($item);
                            $body = $this->load->view('emails/panelTemp.php', $temp, TRUE);
                            $this->email->message($body);
                            $this->email->send();
                        }

                        foreach ($panel2 as $item) {
                            $user = getUserById($item);
                            $jobDetailsData = getJobDetail($resp);
                            
                            /* notification message for assigned  job to user ( sub-admin ) */
                            $notificationMsg = "You are added as a Evaluator L1 for Job ". ucwords($jobDetailsData['title'])." by ".ucwords($userCompany);
                            insertNotificationForTopBarInAdmin($item, $notificationMsg, "Job Review", $userId);
                                
                            $temp['email'] = $user['email'];
                            $temp['jobId'] = $data['jobId'];
                            $temp['type'] = 'Evaluator L2';
                            $this->email->to($item);
                            $body = $this->load->view('emails/panelTemp.php', $temp, TRUE);
                            $this->email->message($body);
                            $this->email->send();
                        }
                        $data['message'] = 'Successfully created!';
                        $data['main_content'] = 'fj-create-job';
                        $data['token'] = $token;
                        $data['subuser'] = getSubUsers(190);
                        $this->load->view('fj-mainpage', $data);
                    }
                } catch (Exception $e) {
                    $resp['error'] = $e->getMessage();
                }
            } else {
                $data['main_content'] = 'fj-create-job';
                $data['error'] = $resp;
                $data['job'] = $this->input->post();
                $data['city'] = getCities();
                $data['course'] = getCourses();
                $data['assessment'] = getAssessmentsByRole(190);
                $data['interview'] = getInterviewByRole(190);
                $data['files'] = $_FILES;
                $data['arreventData']        = $arreventData;
                $data['arreventCompanyData'] = $arreventCompanyData;
                $data['token'] = $token;
                $data['subuser'] = getSubUsers(190);
                $this->load->view('fj-mainpage', $data);
            }
        }
        else {
            $data['city']           = getCities();
            $data['course']         = getCourses();
            $data['assessment']     = getAssessmentsByRole(190, 2);
            $data['interview']      = getInterviewByRole(190, 2);
            $data['subuser']        = getSubUsers(190);
            $data['arreventData']        = $arreventData;
            $data['arreventCompanyData'] = $arreventCompanyData;
            $data['main_content']   = 'fj-create-job';
            $data['token']          = $token;
            $this->load->view('fj-mainpage', $data);
        }
    }

    /**
     * Description : Use to upload jd(video type) file into jd directory
     * Author : Synergy
     * @param array $files
     * @return string the uploaded file or boolean false.
     */
    function fileUploads($files) {
        $typeArr = explode('/', $files['jd']['type']);
        $imgName = rand() . date('ymdhis') . '_jd.' . $typeArr[1];
        $data['image'] = $imgName;
        $jobImageType = $this->config->item('videoType');
        if (in_array($typeArr[1], $jobImageType)) {
            $uploadResp = $this->job_model->fileUpload($files, 'uploads/jd', $imgName);
            return $uploadResp;
        } else {
            return false;
        }
    }
	
	
	function checkTimeOut(){
		die;
		
	}
}
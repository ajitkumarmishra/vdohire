<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Message Controller
 * Description : Used to handle all message related data
 * @author Synergy
 * @createddate : Nov 2, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 21, 2017
 */

class Message extends MY_Controller {

    /**
     * Responsable for auto load the message_model,messagereply_model,user_model
     * Responsable for auto load the form_validation, session, email library
     * Responsable for auto load fj, url helper
     * @return void
     */
    function __construct() {
        parent::__construct();
        $this->load->model('message_model');
        $this->load->model('messagereply_model');
        $this->load->model('user_model');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('email');
        $this->load->helper('fj');
        $this->load->helper('url');
        $token = $this->config->item('accessToken');
        if (isset($_POST) && $_POST != NULL) {
            if ($token != $_POST['accessToken']) {
                echo 'You do not have pemission.';
                exit;
            }
        }
    }

    /**
     * Description : List all the messages
     * Author : Synergy
     * @param none or array $_POST
     * @return render data into view
     */
    function listMessage($page) {
        
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        //Jobs Listing Start
        $token = $this->config->item('accessToken');
        $userData = $this->session->userdata['logged_in'];
        
        $userLoggeinId = $userData->id;
        $userRole = $userData->role;
        
        $messagedata = array();
        $data['page'] = $page;
        $resp['error'] = 0;
        $serachColumn = $this->input->post('serachColumn', true);
        $searchText = $this->input->post('serachText', true);
        $token = $this->config->item('accessToken');
        $fromEmail = $this->config->item('fromEmail');
        $itemsPerPage = $this->config->item('itemsPerPage');
        $data['itemsPerPage'] = $itemsPerPage;
        $createdBy = NULL;

        if (!$page) {
            $page = 0;
        } else {
            $page = $page - 1;
        }
        $offset = $page * $itemsPerPage;
        try {
            
            /*
             * Update message status to 1 if admin click on top message icon
             */
            $messagedata['messageStatus'] = 2;
            $messagereadRes = $this->message_model->updateMessageForTopBarRead($messagedata, $userLoggeinId);
            
            //Listing message for Inbox(Received)
            $result = $this->message_model->listMessage($itemsPerPage, $offset, $serachColumn, $searchText, $createdBy, $userLoggeinId);
            
            $completeResult = $this->message_model->listMessage(NULL, NULL, NULL, NULL, $createdBy, $userLoggeinId);
            $count = ceil(count($completeResult) / $itemsPerPage);

            $data['count'] = $count;
            $data['totalCount'] = count($completeResult);
            $data['content'] = $result;

            if($userRole == 1)
                $data['main_content'] = 'fj-message-listing';
            else
                $data['main_content'] = 'fj-recruiter-message-listing';

            $data['searchText'] = $searchText;
            $data['serachColumn'] = $serachColumn;
            $data['userLoggeinId'] = $userLoggeinId;
            $data['token'] = $token;
            
            //Listing sent messages
            $resultSentMessages = $this->message_model->listSentMessages($itemsPerPage, $offset, $serachColumn, $searchText, $createdBy, $userLoggeinId);
            
            $completeResultSentMessages = $this->message_model->listMessage(NULL, NULL, NULL, NULL, $createdBy, $userLoggeinId);
            $countSentMessages = ceil(count($completeResultSentMessages) / $itemsPerPage);

            $data['messageSentCount'] = $countSentMessages;
            $data['totalMessageSentCount'] = count($completeResultSentMessages);
            $data['sentContent'] = $resultSentMessages;

            if($userRole == 1)
                $this->load->view('fj-mainpage', $data);
            else
                $this->load->view('fj-mainpage-recuiter', $data);
            
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Description : Use to create new message
     * Author : Synergy
     * @param none or array $_POST
     * @return render data into view
     */
    function addMessage() {
        
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token = $this->config->item('accessToken');
        $userData = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
            $userRole = $userData->role;
        } else {
            redirect(base_url('users/login'));
        }
        
        $data = array();
        $dataReply = array();
        
        $appUsersList = $this->user_model->listAppUsers($userId, $userRole);
        
        
        if (isset($_POST) && $_POST != NULL) {
           
            $resp['error'] = '';
            
            $this->form_validation->set_rules('messageSubject', 'Message Subject', 'trim|required');
            $this->form_validation->set_rules('messageText', 'Message', 'trim|required');       
            
            $userTwoid = $this->input->post('userTwoid', true);             
                
            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }
            
            if ( empty($userTwoid) ) {
                $resp['error'] .= '<p>The To field is required.</p>';
            }
            
            if (!$resp['error']) {
                
                $currentTime = date('Y-m-d h:i:s');
                $data['createdAt'] = $currentTime;
                $data['updatedAt'] = $currentTime;
                
                try {
                    
                    if( !empty( $userTwoid ) ){
                        foreach( $userTwoid as $userTwoidVal ){
                            $data['userTwoid'] = $userTwoidVal;
                            $data['messageSubject'] = $this->input->post('messageSubject', true);
                            $data['messageText'] = $this->input->post('messageText', true);
                            $data['userOneId'] = $userId;
                            
                            // SAVE SENT MESSAGE
                            $resp = $this->message_model->addMessage($data);   
                            
                            $messageId = $resp;
                            
                            if( $messageId ) {
                                
                                $dataReply['messageId'] = $messageId;
                                $dataReply['replyById'] = $userId;
                                $dataReply['messageToId'] = $userTwoidVal;
                                $dataReply['messageText'] = $this->input->post('messageText', true);
                                $dataReply['messageReadStatus'] = 0;
                                $dataReply['createdAt'] = $currentTime;
                                
                                $respReply = $this->messagereply_model->addMessageReply($dataReply);   
                            }
                        }
                    }
                    
                    if ($resp) {
                        $this->session->set_flashdata('flashmessagesuccess', 'Message sent successfully!');
                        $data['token'] = $token;
                        $data['subuser'] = getSubUsers($userId);
                        redirect('message/add');
                    }
                } catch (Exception $e) {
                    $resp['error'] = $e->getMessage();
                }
            } else {
                if($userRole == 1)
                    $data['main_content'] = 'fj-create-message';
                else
                    $data['main_content'] = 'fj-recruiter-create-message';
                $data['error'] = $resp;
                $data['appusser_list'] = $appUsersList;
                $data['message'] = $this->input->post();
                $data['token'] = $token;
                $data['subuser'] = getSubUsers($userId);
                
                if($userRole == 1)
                    $this->load->view('fj-mainpage', $data);
                else
                    $this->load->view('fj-mainpage-recuiter', $data);
            }
        } else {
            $data['subuser'] = getSubUsers($userId);
            $data['appusser_list'] = $appUsersList;
            $data['token'] = $token;

            if($userRole == 1)
                $data['main_content'] = 'fj-create-message';
            else
                $data['main_content'] = 'fj-recruiter-create-message';

            if($userRole == 1)
                $this->load->view('fj-mainpage', $data);
            else
                $this->load->view('fj-mainpage-recuiter', $data);
        }
    }

    /**
     * Description : Use to delete thread of a specific message
     * Author : Synergy
     * @param array $_POST
     * @return string or redirect to list
     */
    function deleteMessageThreadMessage() {
        
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token = $this->config->item('accessToken');
        $userData = $this->session->userdata['logged_in'];
        
        $resp['error'] = 0;
        $messagesId = $this->input->post('messagesId', true);
        
        $pageNo = $this->input->post('pageNo', true);
        $pageNo = $pageNo+1;
        
        if ( $messagesId == "" ) {
            echo $error = 'Undefined message!';
            exit;
        }
        if (!$resp['error']) {
            $data['status'] = 3;
            
            if( $messagesId != "" ){
                $messagesIdArr = explode(",", $messagesId);
                
                foreach( $messagesIdArr as $messagesIdVal ){
                    // delete message thread
                    $messageDetails = $this->message_model->getMessage($messagesIdVal);
                    if($messageDetails['isParent'] != 1) {
                        $deleteMessageThreadRes = $this->message_model->deleteMessageThread($messagesIdVal);
                    }
                }                

                if ($deleteMessageThreadRes) {
                    $this->session->set_flashdata('flashmessagesuccess', 'Message thread successfully deleted!');
                    $data['token'] = $token;
                    $data['subuser'] = getSubUsers($userId);
                    redirect('message/list/'.$pageNo);
                } else {
                    $this->session->set_flashdata('flashmessageerror', 'Sorry, Message thread not deleted!');
                    $data['token'] = $token;
                    $data['subuser'] = getSubUsers($userId);
                    redirect('message/list/'.$pageNo);
                }
            } else {
                $this->session->set_flashdata('flashmessagesuccess', 'Please select message to delete!');
                $data['token'] = $token;
                $data['subuser'] = getSubUsers($userId);
                redirect('message/list/'.$pageNo);
            }
        }
    }

    /**
     * Description : Use to reply on a specific message
     * Author : Synergy
     * @param array $_POST
     * @return string or redirect to list
     */
    function replymessageMessage() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token = $this->config->item('accessToken');
        $userData = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
            $userRole = $userData->role;
        } else {
            redirect(base_url('users/login'));
        }
        
        $data = array();
        $dataReply = array();
        
        $messageId = $this->uri->segment(4);
        $userTwoid = $this->uri->segment(6);
        
        $appUsersList = $this->user_model->listAppUsers($userId, $userRole);
        
        
        if (isset($_POST) && $_POST != NULL) {
           
            $resp['error'] = '';
            
            $this->form_validation->set_rules('messageText', 'Message', 'trim|required');       
            
                
            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }
            
            if ( empty($userTwoid) ) {
                $resp['error'] .= '<p>Reply to user is required.</p>';
            }
            
            if ( empty($messageId) ) {
                $resp['error'] .= '<p>Message thread is required.</p>';
            }
            
            if (!$resp['error']) {
                
                $currentTime = date('Y-m-d h:i:s');
                $data['updatedAt'] = $currentTime;
                
                try {
                    
                    if( !empty( $userTwoid ) && !empty( $messageId )  ){
                        
                        // update message updatedAT time
                        //$resp = $this->message_model->updateMessage($data, $messageId);

                        $messageDetails = $this->message_model->getMessage($messageId);

                        $dataReply = array(
                                    'userOneId'         => $userId,
                                    'parentMessageId'   => $messageDetails['parentMessageId'],
                                    'userTwoId'         => $userTwoid,
                                    'messageSubject'    => $messageDetails['messageSubject'],
                                    'messageText'       => $this->input->post('messageText', true),
                                    'createdAt'         => date('Y-m-d H:i:s'),
                                    'updatedAt'         => date('Y-m-d H:i:s')
                                );

                        $respReply = $this->message_model->addMessage($dataReply);
                    }
                    
                    
                    if ($respReply) {
                        $this->session->set_flashdata('flashmessagesuccess', 'Message sent successfully!');
                        $data['token'] = $token;
                        $data['subuser'] = getSubUsers($userId);
                        redirect('message/replymessage/messageid/'.$messageId.'/touserid/'.$userTwoid);
                    }
                } catch (Exception $e) {
                    $resp['error'] = $e->getMessage();
                }
            } else {
                if($userRole == 1)
                    $data['main_content'] = 'fj-reply-message';
                else
                    $data['main_content'] = 'fj-recruiter-reply-message';

                $data['error'] = $resp;
                $data['appusser_list'] = $appUsersList;
                $data['messages'] = $this->input->post();
                $data['token'] = $token;
                $data['subuser'] = getSubUsers($userId);
                
                if($userRole == 1)
                    $this->load->view('fj-mainpage', $data);
                else
                    $this->load->view('fj-mainpage-recuiter', $data);
            }
        } else {
            $data['subuser'] = getSubUsers($userId);
            $data['appusser_list'] = $appUsersList;
            $data['main_content'] = 'fj-reply-message';

            if($userRole == 1)
                $data['main_content'] = 'fj-reply-message';
            else
                $data['main_content'] = 'fj-recruiter-reply-message';

            $data['token'] = $token;

            if($userRole == 1)
                $this->load->view('fj-mainpage', $data);
            else
                $this->load->view('fj-mainpage-recuiter', $data);
        }
    }

    /**
     * Description : Use to close a message thread
     * Author : Synergy
     * @param array $_POST
     * @return render data to view
     */
    function closeMessageThreadMessage() {
        
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token = $this->config->item('accessToken');
        $userData = $this->session->userdata['logged_in'];
        
        $resp['error'] = 0;
        $messageId = $this->input->post('messageId', true);
        $messageStatus = $this->input->post('messageStatus', true);
        //$messageId = 2;
        $pageNo = $this->input->post('pageNo', true);
        $pageNo = $pageNo+1;
        
        if ( $messageId == "" ) {
            echo $error = 'Undefined message!';
            exit;
        }
        if (!$resp['error']) {
            
            if( $messageId != "" ){
                
                $currentTime = date('Y-m-d h:i:s');
                $data['updatedAt'] = $currentTime;                
                $data['messageStatus'] = $messageStatus;
                
                $closeMessageThreadRes = $this->message_model->closeMessage($data, $messageId);
                
                if ($closeMessageThreadRes) {
                    $this->session->set_flashdata('flashmessagesuccess', 'Message thread successfully closed!');
                    $data['token'] = $token;
                    $data['subuser'] = getSubUsers($userId);
                    redirect('message/list/'.$pageNo);
                } else {
                    $this->session->set_flashdata('flashmessageerror', 'Sorry, Message thread not closed!');
                    $data['token'] = $token;
                    $data['subuser'] = getSubUsers($userId);
                    redirect('message/list/'.$pageNo);
                }
            } else {
                $this->session->set_flashdata('flashmessagesuccess', 'Please select message to close!');
                $data['token'] = $token;
                $data['subuser'] = getSubUsers($userId);
                redirect('message/list/'.$pageNo);
            }
        }
    }

    /**
     * Description : Use to view a message thread
     * Author : Synergy
     * @param uri segment
     * @return redirect data to view
     */
    function viewMessageThreadMessage() {
        
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token = $this->config->item('accessToken');
        $userData = $this->session->userdata['logged_in'];
        
        if (isset($userData->id) && $userData->id != NULL) {
            $userLoggedinId = $userData->id;
            $userRole = $userData->role;
        } else {
            $userLoggedinId = 1;
        }
        
        $resp['error'] = 0;
        $data = array();
        $dataReply = array();
        $messagedata = array();
        $messageId = $this->uri->segment(4);
        
        $messageListRes = $this->message_model->getMessage($messageId);

        if($messageListRes) {
            /*
             * Update message status to 1 if admin click on top message icon
             */
            //$messagedata['messageReadStatus'] = 2;
            //$messagereadRes = $this->messagereply_model->updateMessageForTopBarRead($messagedata, $userLoggedinId);
        }
        //$data['subuser'] = getSubUsers($userId);
        $data['messageListRes'] = $messageListRes;
        $data['main_content'] = 'fj-view-message';


        if($userRole == 1) {
            $data['main_content'] = 'fj-view-message';
        }
        else {
            $data['main_content'] = 'fj-recruiter-view-message';
        }

        $data['userLoggedinId'] = $userLoggedinId;
        $data['token'] = $token;

        if($userRole == 1)
            $this->load->view('fj-mainpage', $data);
        else
            $this->load->view('fj-mainpage-recuiter', $data);
    }
}
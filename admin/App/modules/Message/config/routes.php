<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['message/list'] = "Message/listMessage";
$route['message/list/(:any)'] = "Message/listMessage/$1";
$route['message/sentlist'] = "Message/listMessage";
$route['message/sentlist/(:any)'] = "Message/listMessage/$1";
$route['message/add'] = "Message/addMessage";
$route['message/delete'] = "Message/deleteMessageThreadMessage";
$route['message/closemessage'] = "Message/closeMessageThreadMessage";
$route['message/replymessage/messageid'] = "Message/replymessageMessage";
$route['message/replymessage/messageid/(:any)'] = "Message/replymessageMessage/$1";
$route['message/replymessage/messageid/(:any)/touserid/(:any)'] = "Message/replymessageMessage/$1/touserid/$2";

$route['message/viewmessage/messageid'] = "Message/viewMessageThreadMessage";
$route['message/viewmessage/messageid/(:any)'] = "Message/viewMessageThreadMessage/$1";
$route['message/viewmessage/messageid/(:any)/touserid/(:any)'] = "Message/viewMessageThreadMessage/$1/touserid/$2";
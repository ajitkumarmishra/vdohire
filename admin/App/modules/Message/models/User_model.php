<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : User Model
 * Description : Handle all the CRUD operation for User
 * @author Synergy
 * @createddate : Nov 18, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 21, 2017
 */
class User_model extends CI_Model {

    /**
     * Initialize variables
     */
    var $table = "users";

    /**
     * Inherit the parent constructor
     * Responsable for auto load the the session and pagination library
     * @return void
     */
    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('pagination');
    }

    /**
     * listUsers function
     * Discription : Use to Get all users.
     * Author : Synergy
     * @param int $limit, int $offset, string $serachColumn, string $searchText, int $createdBy
     * @return array the users data.
     */
    function listUsers($limit = NULL, $offset = NULL, $serachColumn = NULL, $searchText = NULL, $createdBy = NULL) {
        try {
            $this->db->select('*');
            $this->db->from('fj_users');
            $this->db->where('status !=', 3);
            if (isset($createdBy) && $createdBy != NULL) {
                $this->db->where('createdBy =', $createdBy);
            }
            if ($serachColumn != NULL && $searchText != NULL) {
                $this->db->like($serachColumn, $searchText, 'both');
            }
            if (isset($limit) && $limit != NULL) {
                $this->db->limit($limit, $offset);
            }
            $query = $this->db->get();
            $result = $query->result();

            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    
    /**
     * listCorporates function
     * Discription : Use to list all corporate users.
     * Author : Synergy
     * @param int $limit, int $offset, string $serachColumn, string $searchText, int $createdBy, int $role
     * @return array the users data.
     */
    function listCorporates($limit = NULL, $offset = NULL, $serachColumn = NULL, $searchText = NULL, $createdBy = NULL, $role) {
        try {
            echo $serachColumn;
            echo $searchText; //die();
            $this->db->select('*');
            $this->db->from('fj_users');
            $this->db->where('status !=', 3);
            if (isset($createdBy) && $createdBy != NULL) {
                $this->db->where('createdBy =', $createdBy);
            }
            if (isset($role) && $role != NULL) {
                $this->db->where('role =', $role);
            }
            if ($serachColumn != NULL && $searchText != NULL) {
                $this->db->like($serachColumn, $searchText, 'both');
            }
            if (isset($limit) && $limit != NULL) {
                $this->db->limit($limit, $offset);
            }
            $query = $this->db->get();
            $result = $query->result();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * listAppUsers function
     * Discription : Use to list all app users.
     * Author : Synergy
     * @param int $createdBy, int $userRole
     * @return array the users data.
     */
    function listAppUsers($createdBy, $userRole) {
        try {

            $sql = "select fju.id, fju.email, fju.fullname, fju.company from fj_users fju ";
            
            $userRole;
            if( $userRole == 1 ){
                $sql .= " where fju.role = 2  ORDER BY role, company";
            } else if ( $userRole == 2 ){
                $sql .= " where fju.role = 1 OR fju.createdBy = '".$createdBy."'  ORDER BY role, company, fullname";
            }
             else if ( $userRole == 4 ||  $userRole == 5){
                $sql1 = "SELECT createdBy FROM fj_users WHERE id='$createdBy'";
                $query = $this->db->query($sql1);
                $result = $query->row_array();
                $sql .= " where fju.role = 1 OR fju.createdBy = '".$createdBy."' OR  fju.id = '".$result['createdBy']."' OR  fju.createdBy = '".$result['createdBy']."' ORDER BY role, company, fullname";
            }

            $query = $this->db->query($sql);
            $result = $query->result();

            if (!$result) {
                
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
}
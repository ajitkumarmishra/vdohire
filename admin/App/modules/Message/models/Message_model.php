<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Message Model
 * Description : Handle all the CRUD operation for Message
 * @author Synergy
 * @createddate : Nov 16, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 21, 2017
 */
class Message_model extends CI_Model {

    /**
     * Initialize variables
     */
    var $table = "fj_message";

    /**
     * Inherit the parent constructor
     * Responsable for auto load the the session and pagination library
     * @return void
     */
    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('pagination');
    }

    /**
     * GetMessage function
     * Gets a specific message detail.
     * Author : Synergy
     * @param int $id.
     * @return array of message data.
     */
    function getMessage($id) {
        try {
            $this->db->select('*');
            $this->db->from($this->table);
            $this->db->where('messageId', $id);
            $query = $this->db->get();
            $result = $query->row_array();
            if (count($result) == 0) {
                throw new Exception('Unable to fetch message data.');
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * listMessage function
     * List all the messages for inbox.
     * Author : Synergy
     * @param int $limit, int $offset
     * @return array of message data.
     */
    function listMessage($limit = NULL, $offset = NULL, $serachColumn = NULL, $searchText = NULL, $createdBy = NULL, $userLoggeinId) {
        try {
            $currentDate = date('Y-m-d h:i:s');
            $sql = "
                    SELECT 
                        U1.fullname	AS U1Fullname,
                        U1.company	AS U1Company,
                        U1.role		AS U1Role,
                        U2.fullname	AS U2Fullname,
                        U2.company	AS U2Company,
                        U2.role		AS U2Role,
                        M.*
                    FROM
                        fj_message	M
                    JOIN
                        fj_users	U1,
                        fj_users	U2
                    WHERE
                        M.userOneId=U1.id   AND
                        M.userTwoId=U2.id   AND
                        (M.userTwoId = '".$userLoggeinId."') ORDER BY createdAt DESC";
                        
            if(isset($offset)) {
                $sql .= " LIMIT " .$offset. ", " .$limit."";
            }

            $query = $this->db->query($sql);
            $result = $query->result();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * updateMessageForTopBarRead function
     * Discription : Use to update messages on click of top bar messages icon for specific user
     * Author : Synergy
     * @param array $messagedata.
     * @param int $userId.
     * @return boolean or int or string.
     */
    function updateMessageForTopBarRead($messagedata, $userId) {
        
        try {
            $this->db->where('userTwoId', $userId);
            
            $resp = $this->db->update($this->table, $messagedata);
            
            if (!$resp) {
                throw new Exception('Unable to update message data.');
                return false;
            }
            return $userId;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * countUnreadMessage function
     * Discription : Use to count all unred top bar message for a specific user
     * Author : Synergy
     * @param int $id.
     * @return array of message data
     */
    function countUnreadTopBarMessage($userId) {
        try {
            
            $query = $this->db->select("count(messageId) as unreadcount")
                                ->from($this->table)
                                ->where('userTwoId =', $userId)
                                ->where('messageStatus = 1')
                                ->get();
            
            $result = $query->row_array();
           
            if (!$result) {
                throw new Exception('Unable to fetch unread message data.');
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * listSentMessages function
     * List all the sent messages.
     * Author : Synergy
     * @param int $limit, int $offset
     * @return array of message data.
     */
    function listSentMessages($limit = NULL, $offset = NULL, $serachColumn = NULL, $searchText = NULL, $createdBy = NULL, $userLoggeinId) {
        try {
            $currentDate = date('Y-m-d h:i:s');
            $sql = "
                    SELECT 
                        U1.fullname AS U1Fullname,
                        U1.company  AS U1Company,
                        U1.role     AS U1Role,
                        U2.fullname AS U2Fullname,
                        U2.company  AS U2Company,
                        U2.role     AS U2Role,
                        M.*
                    FROM
                        fj_message  M
                    JOIN
                        fj_users    U1,
                        fj_users    U2
                    WHERE
                        M.userOneId=U1.id   AND
                        M.userTwoId=U2.id   AND
                        (M.userOneId = '".$userLoggeinId."') ORDER BY createdAt DESC";
                        
            if(isset($offset)) {
                $sql .= " LIMIT " .$offset. ", " .$limit."";
            }

            $query = $this->db->query($sql);
            $result = $query->result();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * addMessage function
     * Creates a message
     * Author : Synergy
     * @params array $data
     * @return boolean
     */
    function addMessage($data) {
        $resp = $this->db->insert($this->table, $data);
        $messageId = $this->db->insert_id();
        if ($resp) {
            return $messageId;
        } else {
            return false;
        }
    }
    
    
    /**
     * deleteMessageThread function
     * Creates a to delete message thread
     * Author : Synergy
     * @params int $messageId
     * @return boolean
     */
    function deleteMessageThread($messageId) {
        try {
            $this->db->where('messageId', $messageId);
            $this->db->delete($this->table);
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * UpdateMessage function
     * Updates a specific message.
     * Author : Synergy
     * @param array $data.
     * @param int $id.
     * @return boolean.
     */
    function updateMessage($data, $id) {
        try {
            $this->db->where('messageId', $id);
            $resp = $this->db->update($this->table, $data);
            if (!$resp) {
                throw new Exception('Unable to update message data.');
                return false;
            }
            return $id;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    
    /**
     * CloseMessage function
     * Close a specific message.
     * Author : Synergy
     * @param array $data.
     * @param int $id.
     * @return boolean.
     */
    function closeMessage($data, $id) {
        try {
            $this->db->where('messageId', $id);
            $this->db->or_where('parentMessageId', $id); 
            $resp = $this->db->update($this->table, $data);
            if (!$resp) {
                throw new Exception('Unable to update message data.');
                return false;
            }
            return $id;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
}
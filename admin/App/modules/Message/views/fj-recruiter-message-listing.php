<!-- Page Content -->
<?php //echo "<pre>";print_r($contentMyQuestions); die();

$urlSeg = $this->uri->segment(2);
?>

<?php
if($urlSeg == 'sentlist'){ ?>
<script>
$(function(){
    $('#sendMessages').trigger('click');
});
</script>
<?php }  ?>

<?php
if($urlSeg == 'list'){ ?>
<script>
$(function(){
    $('#receivedMessages').trigger('click');
});
</script>
<?php }  ?>

<div id="page-content-wrapper" style="margin-top: 52px;">
    <div class="container-fluid whitebg">
        <div class="row">
            <div class="col-md-10" style="margin-top: 20px;"><h4 style="border-bottom: 23px; font-size: 20px;"> Messages</h4></div>
            <div class="clearfix"></div>

            <div class="col-md-12 rsp_tbl">
                <div class="row">
                    <div>
                        <div style="float: left;">
                            <ul class="nav nav-pills padd_tabs">
                                <li class="active active_tab"><a data-toggle="pill" href="#home" id="receivedMessages">Received Messages</a></li>
                                <li class=""><a data-toggle="pill" href="#menu2" id="sendMessages">Sent Messages</a></li>
                            </ul>
                        </div>
                        <div style="float: right;">
                            <a href="<?php echo base_url(); ?>message/add"><button type="button" class="acu_bt" style="background: #054f72 none repeat scroll 0 0; color:white; margin-top: 0px; margin-top: 23px;">Write New Message</button></a>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="tab-content">
                        <div id="home" class="tab-pane fade active in">
                            <table class="table">
                                <thead class="tbl_head">
                                    <tr>
                                        <th><input type="checkbox" id="ckbCheckAll"></th>
                                        <th>Sender Type</th>
                                        <th>From</th>
                                        <th>Subject</th>
                                        <th>Time</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                    <?php
                                    $i = 0; 

                                    if( !empty($content) )
                                    {
                                        // ci instance
                                        $ci =&get_instance();
                                        // reply message model
                                        $ci->load->model('messagereply_model');
                                        foreach ($content as $item){
                                            
                                            // function to get count of unread messages
                                            $unreadrepliedres = $ci->messagereply_model->countUnreadMessage($item->messageId, $userLoggeinId); 
                                            $unreadcount = $unreadrepliedres['unreadcount'];
                                            
                                            $userLogedInIdVal = getUserId();
                                            
                                            if( $userLogedInIdVal == $item->userTwoId ){
                                                $replyToUserId = $item->userOneId;
                                            } else {
                                                $replyToUserId = $item->userTwoId;
                                            }
                                            
                                
                                    ?>    
                                    <tr>
                                        <td><input type="checkbox" class="checkBoxClass" value="<?php echo $item->messageId; ?>" name="messageIds[]" id="messageIds"></td>
                                        <td>
                                            <?php
                                            if($item->U1Role=='2' || $item->U1Role=='4' || $item->U1Role=='5') {
                                                echo 'Corporate User';
                                            }                                
                                            else if($item->U1Role=='3') {
                                                echo 'Job Seeker';
                                            } else {
                                                echo 'Superadmin';
                                            }
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            if($item->U1Role=='1' || $item->U1Role=='2') {
                                                echo ucwords($item->U1Company);
                                            }                                
                                            else if($item->U1Role=='3' || $item->U1Role=='4' || $item->U1Role=='5') {
                                                echo ucwords($item->U1Fullname);
                                            }
                                            ?>
                                        </td>
                                        <td>
                                            <?php echo ucwords($item->messageSubject); ?>
                                        </td>
                                        <td><?php echo date('d-m-Y h:i:s A', strtotime($item->createdAt)); ?></td>
                                        <td>
                                            <a href="<?php echo base_url(); ?>message/replymessage/messageid/<?php echo $item->messageId; ?>/touserid/<?php echo $replyToUserId; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/rpy.png" class="padd_rgt" title="Reply Message"></a>
                                            <div class="button">
                                                <i class="fa fa-globe"></i>
                                                <?php if( $unreadcount > 0 && $unreadcount != "" ) { ?>
                                                <span class="button__badge"><?php echo $unreadcount; ?></span>
                                                <?php } ?>
                                            </div>
                                            <a href="<?php echo base_url(); ?>message/viewmessage/messageid/<?php echo $item->messageId; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/eye.png" class="padd_rgt" title="View All Messages"></a>
                                            <?php if( $item->messageStatus == 0 ) { ?>
                                                <a href="javascript:" class="close-message" id="<?php echo $item->messageId; ?>/1"><img src="<?php echo base_url(); ?>/theme/firstJob/image/open-icon.png" class="padd_rgt" title="Open Message"></a>
                                            <?php } else { ?>
                                                <a href="javascript:" class="close-message" id="<?php echo $item->messageId; ?>/0"><img src="<?php echo base_url(); ?>/theme/firstJob/image/close.png" class="padd_rgt" title="Close Message"></a>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                    <?php
                                        }
                                    } else { ?>
                                    <tr><td>Messages data not available</td></tr>
                                    <?php } ?>
                                </tbody>
                            </table>

                            <?php if( !empty($content) ) { ?>
                            <div class="col-md-12">  
                                <div class="ed_job_bt">
                                    <input type="hidden" name="hdnmessageids" id="hdnmessageids" value="" />
                                    <button type="button" class="delete-messages pull-left">Delete</button>
                                </div>
                            </div>
                            <?php } ?>
                            <div class="service_content ">

                                <?php if ($totalCount > $itemsPerPage): ?>
                                    <ul class="service_content_last_list">
                                        <?php if ($page > 1): ?>
                                            <p class="pagi_img"><a href="<?php echo base_url(); ?>message/list/<?php echo $page - 1; ?>" ><img src="<?php echo base_url(); ?>/theme/firstJob/image/pagination_img2.png"></a></p>    
                                        <?php endif; ?>
                                        <input type="hidden" name="page_no" value="1" id="page_no"> 
                                        <?php for ($i = 1; $i <= $count; $i++): ?>
                                            <li><a href="<?php echo base_url(); ?>message/list/<?php echo $i; ?>"  <?php if ($i == $page): ?>style="color: orange;"<?php endif; ?>><?php echo $i; ?></a></li>
                                        <?php endfor; ?>
                                        <?php if ($page < $count): ?>
                                            <p class="pagi_img"><a href="<?php echo base_url(); ?>message/list/<?php echo $page + 1; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/pagination_img1.png"></a></p>            
                                        <?php endif; ?>
                                    </ul>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div id="menu2" class="tab-pane fade">
                            <table class="table">
                                <thead class="tbl_head">
                                    <tr>
                                        <th><input type="checkbox" id="ckbCheckAllSent"></th>
                                        <th>Receiver Type</th>
                                        <th>To</th>
                                        <th>Subject</th>
                                        <th>Time</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                    <?php
                                    $i = 0; 

                                    if( !empty($sentContent) )
                                    {
                                        // ci instance
                                        $cis =&get_instance();
                                        // reply message model
                                        $cis->load->model('messagereply_model');
                                        foreach ($sentContent as $sentItem){
                                            
                                            // function to get count of unread messages
                                            $sentunreadrepliedres = $cis->messagereply_model->countUnreadMessage($sentItem->messageId, $userLoggeinId); 
                                            $sentunreadcount = $sentunreadrepliedres['unreadcount'];
                                            
                                            $sentUserLogedInIdVal = getUserId();
                                            
                                            if( $sentUserLogedInIdVal == $sentItem->userTwoId ){
                                                $sentReplyToUserId = $sentItem->userOneId;
                                            } else {
                                                $sentReplyToUserId = $sentItem->userTwoId;
                                            }
                                            
                                
                                    ?>    
                                    <tr>
                                        <td><input type="checkbox" class="checkBoxSentClass" value="<?php echo $sentItem->messageId; ?>" name="sentMessageIds[]" id="sentMessageIds"></td>
                                        <td>
                                            <?php
                                            if($sentItem->U1Role=='2' || $sentItem->U1Role=='4' || $sentItem->U1Role=='5') {
                                                echo 'Corporate User';
                                            }                                
                                            else if($sentItem->U1Role=='3') {
                                                echo 'Job Seeker';
                                            } else {
                                                echo 'Superadmin';
                                            }
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            if($sentItem->U2Role=='1' || $sentItem->U2Role=='2') {
                                                echo ucwords($sentItem->U1Company);
                                            }                                
                                            else if($sentItem->U2Role=='3' || $sentItem->U2Role=='4' || $sentItem->U2Role=='5') {
                                                echo ucwords($sentItem->U2Fullname);
                                            }
                                            ?>
                                        </td>
                                        <td>
                                            <?php echo ucwords($sentItem->messageSubject); ?>
                                        </td>
                                        <td><?php echo date('d-m-Y h:i:s A', strtotime($sentItem->createdAt)); ?></td>
                                        <td>
                                            <div class="button">
                                                <i class="fa fa-globe"></i>
                                                <?php if( $unreadcount > 0 && $unreadcount != "" ) { ?>
                                                <span class="button__badge"><?php echo $unreadcount; ?></span>
                                                <?php } ?>
                                            </div>
                                            <a href="<?php echo base_url(); ?>message/viewmessage/messageid/<?php echo $sentItem->messageId; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/eye.png" class="padd_rgt" title="View All Messages"></a>
                                            <?php if( $sentItem->messageStatus == 0 ) { ?>
                                                <a href="javascript:" class="close-message" id="<?php echo $sentItem->messageId; ?>/1"><img src="<?php echo base_url(); ?>/theme/firstJob/image/open-icon.png" class="padd_rgt" title="Open Message"></a>
                                            <?php } else { ?>
                                                <a href="javascript:" class="close-message" id="<?php echo $sentItem->messageId; ?>/0"><img src="<?php echo base_url(); ?>/theme/firstJob/image/close.png" class="padd_rgt" title="Close Message"></a>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                    <?php
                                        }
                                    } else { ?>
                                    <tr><td>Sent Messages data not available</td></tr>
                                    <?php } ?>
                                </tbody>
                            </table>

                            <?php if( !empty($sentContent) ) { ?>
                            <div class="col-md-12">  
                                <div class="ed_job_bt">
                                    <input type="hidden" name="hdnsentmessageids" id="hdnsentmessageids" value="" />
                                    <button type="button" class="delete-sent-messages pull-left">Delete</button>
                                </div>
                            </div>
                            <?php } ?>
                            <div class="service_content ">

                                <?php if ($totalMessageSentCount > $itemsPerPage): ?>
                                    <ul class="service_content_last_list">
                                        <?php if ($page > 1): ?>
                                            <p class="pagi_img"><a href="<?php echo base_url(); ?>message/sentlist/<?php echo $page - 1; ?>" ><img src="<?php echo base_url(); ?>/theme/firstJob/image/pagination_img2.png"></a></p>    
                                        <?php endif; ?>

                                        <?php for ($i = 1; $i <= $messageSentCount; $i++): ?>
                                            <li><a href="<?php echo base_url(); ?>message/sentlist/<?php echo $i; ?>"  <?php if ($i == $page): ?>style="color: orange;"<?php endif; ?>><?php echo $i; ?></a></li>
                                        <?php endfor; ?>
                                        <?php if ($page < $messageSentCount): ?>
                                            <p class="pagi_img"><a href="<?php echo base_url(); ?>message/sentlist/<?php echo $page + 1; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/pagination_img1.png"></a></p>            
                                        <?php endif; ?>
                                    </ul>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    </div>
</div>
<!-- /#page-content-wrapper -->

<script>
    
    $("#ckbCheckAll").click(function () {
        $(".checkBoxClass").prop('checked', $(this).prop('checked'));
        
        var output = $.map($(':checkbox[name=messageIds\\[\\]]:checked'), function(n, i){
                return n.value;
          }).join(',');
          
        if( output ){
            $('#hdnmessageids').val(output);
        } else {
            $('#hdnmessageids').val('');
        }

    });
    
    $(".checkBoxClass").click(function () {
        
        var output = $.map($(':checkbox[name=messageIds\\[\\]]:checked'), function(n, i){
                return n.value;
          }).join(',');
          
        if( output ){
            $('#hdnmessageids').val(output);
        } else {
            $('#hdnmessageids').val('');
        }
    });

    //Sent messages

    $("#ckbCheckAllSent").click(function () {
        $(".checkBoxSentClass").prop('checked', $(this).prop('checked'));
        
        var output = $.map($(':checkbox[name=sentMessageIds\\[\\]]:checked'), function(n, i){
                return n.value;
          }).join(',');
          
        if( output ){
            $('#hdnsentmessageids').val(output);
        } else {
            $('#hdnsentmessageids').val('');
        }

    });
    
    $(".checkBoxSentClass").click(function () {
        
        var output = $.map($(':checkbox[name=sentMessageIds\\[\\]]:checked'), function(n, i){
                return n.value;
          }).join(',');
          
        if( output ){
            $('#hdnsentmessageids').val(output);
        } else {
            $('#hdnsentmessageids').val('');
        }
    });

    
    $(function () {
        var url = '<?php echo base_url(); ?>';
        window.siteUrl = url;
        window.token = '<?php echo $token; ?>';
    });
    
    //DELETE MESSAGE FROM SINGLE OR MULTIPLE SELECTION ON CHECKBOX
    $('.delete-messages').on('click', function () {
        var messagesId = $('#hdnmessageids').val();
        
        if(messagesId != '') {
            bootbox.confirm('Are you sure you want to delete?', function (result) {
                if (result) {
                    $.post(siteUrl + 'message/delete', {messagesId: messagesId, pageNo:<?php echo $page;?>, accessToken: token}, function (data) {
                        location.reload();
                    });
                }
            }); 
        } else {
            bootbox.alert('Please check at least one message to delete!');
        }
    });
    
    $('.delete-sent-messages').on('click', function () {
        var messagesId = $('#hdnsentmessageids').val();
        
        if(messagesId != '') {
            bootbox.confirm('Are you sure you want to delete?', function (result) {

                if (result) {
                    //alert(messagesId);
                    $.post(siteUrl + 'message/delete', {messagesId: messagesId, pageNo:<?php echo $page;?>, accessToken: token}, function (data) {

                        //bootbox.alert(data);
                        location.reload();
                    });
                }
            });
        } else {
            bootbox.alert('Please check at least one message to delete!');
        }
    });

    $('.close-message').on('click', function () {
        var messageCloseData = $(this).attr('id');
        var messageCloseDataRes = messageCloseData.split('/');
        messageId = messageCloseDataRes[0];
        messageStatus = messageCloseDataRes[1];
        
        if( messageStatus == 0 ){
            var messateStatusStr = "Close";
        } else if( messageStatus == 1 ){
            var messateStatusStr = "Open";
        }
        //alert(messageId);
        bootbox.confirm('Are you sure you want to '+messateStatusStr+'?', function (result) {

            if (result) {
                //alert(messageId);
                $.post(siteUrl + 'message/closemessage', {messageId: messageId, messageStatus:messageStatus, pageNo:<?php echo $page;?>, accessToken: token}, function (data) {

                    //bootbox.alert(data);
                    location.reload();
                });
            }
        });

    });
</script>

<style>
/* Define how each icon button should look like */
.button {
  color: white;
  display: inline-block; /* Inline elements with width and height. TL;DR they make the icon buttons stack from left-to-right instead of top-to-bottom */
  position: relative; /* All 'absolute'ly positioned elements are relative to this one */
  padding: 2px 5px; /* Add some padding so it looks nice */
}


    /* Make the badge float in the top right corner of the button */
.button__badge {
  background-color: #51a6b9;
  border-radius: 2px;
  color: white;
 
  padding: 1px 3px;
  font-size: 10px;
  
  position: absolute; /* Position the badge within the relatively positioned button */
  top: 0;
  right: 0;
}
    </style>
<?php
if (isset($message) && $message != NULL) {
    //echo '<pre>';print_r($message);  
}
//echo '<pre>'; print_r($subuser);
?>

<!-- Page Content -->
<div id="page-content-wrapper" style="margin-top: 52px;">
    <?php if($this->session->flashdata('flashmessagesuccess')){ 
            echo "<div class=\"alert alert-success\">".$this->session->flashdata('flashmessagesuccess')."</div>"; 
          } 
    ?>
    <?php if($this->session->flashdata('flashmessageerror')){ 
            echo "<div class=\"alert alert-danger\">".$this->session->flashdata('flashmessageerror')."</div>"; 
          } 
    ?>
    <?php if (isset($message)): ?>
        <div class="alert alert-success">
            <?php echo $message; ?>
        </div>
    <?php endif; ?>
    <?php if (isset($error)): ?>
        <div class="alert alert-danger">
            <?php foreach ($error as $item): ?>
                <?php echo $item; ?>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
    <div class="container-fluid whitebg">
        <div class="row">
            <div class="col-md-12 padd_tp_bt_20">
                <h4 style="margin-bottom: 27px; font-size: 20px;"> View Message</h4>
                <table cellpadding="5" cellspacing="5" border="0">
                    
                <?php
                $i = 1; 

                if( !empty($messageListRes) )
                {
                    $colorcode = "#f9a727";
                    
                    echo "<tr>"
                        . "<td><span style=\"color:".$colorcode."; font-size:16px; font-weight:700\">".ucwords($messageListRes['messageSubject'])."</span></td>"
                        . "</tr>";
                    echo "<tr>"
                    . "<td style=\"padding: 10px; border: 1px solid #d4d4d4\">"
                        . "<table cellpading=\"5\" cellspacing=\"0\" border=\"0\" style=\"width:900px\">"
                            ."<tr>"
                                . "<td>".$messageListRes['messageText']."</td>"
                                . "<td width=\"20px\">&nbsp;</td>"
                                . "<td align=\"right\" style=\"font-size:10px\">".date( 'd-m-Y h:i:s A', strtotime( $messageListRes['createdAt'] ) )."</td>"                                    
                            ."</tr>"
                            ."<tr>";
                            echo "</tr>"
                    ."</table>"
                    ."</td>"
                    . "</tr>";
                    
                    echo "<tr><td>&nbsp;</td></tr>";
                }
                ?>                        
                </table>
                <div class="clearfix">
                    <div class="col-xs-offset-2 col-xs-2 no_padding">
                        <input type="hidden" name="accessToken" value="<?php echo $token; ?>"/>
                        <button type="button" onclick="history.go(-1);" class="Save_frm" style="background: #054f72 none repeat scroll 0 0; color: white;">Back</button>
                    </div>
                </div>
        </div>
        <!-- /#page-content-wrapper -->
    </div>
</div>

<script src="<?php echo base_url(); ?>theme/firstJob/js/chosen.jquery.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>theme/firstJob/js/prism.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
    var config = {
        '.chosen-select': {},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>



<style type="text/css">
    #mceu_36 {
        display: none !important;
    }
    #mceu_37 {
        display: none !important;
    }
</style>

<!-- Page Content -->
<div id="page-content-wrapper" style="margin-top: 52px;">
    <?php if($this->session->flashdata('flashmessagesuccess')){ 
            echo "<div class=\"alert alert-success\">".$this->session->flashdata('flashmessagesuccess')."</div>"; 
          } 
    ?>
    <?php if($this->session->flashdata('flashmessageerror')){ 
            echo "<div class=\"alert alert-danger\">".$this->session->flashdata('flashmessageerror')."</div>"; 
          } 
    ?>
    <?php if (isset($message)): ?>
        <div class="alert alert-success">
            <?php echo $message; ?>
        </div>
    <?php endif; ?>
    <?php if (isset($error)): ?>
        <div class="alert alert-danger">
            <?php foreach ($error as $item): ?>
                <?php echo $item; ?>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
    <div class="container-fluid whitebg">
        <div class="row">
            <div class="col-md-12 padd_tp_bt_20">
                <h4 style="margin-bottom: 27px; font-size: 20px;">Send Message</h4>
                <form class="form-horizontal form_save" action="" method="post" enctype="multipart/form-data">
                    
                    <div class="form-group">
                        <label for="userTwoidBlock" class="control-label col-xs-2">To</label>
                        <div class="col-xs-10">
                            
                            <select data-placeholder="Enter select user for message" style="width: 350px; display: none;" multiple="" class="chosen-select" tabindex="-1" name="userTwoid[]" id="userTwoid">
                                <option value=""></option>
                                <?php if( !empty( $appusser_list ) ) { ?>
                                    <?php foreach ($appusser_list as $item) : ?>
                                        <option value="<?php echo $item->id; ?>" <?php if (isset($job['userTwoid']) && in_array($item->id, $job['userTwoid'])): echo "selected";
                                    endif;
                                        ?>>
                                            <?php
                                            if( $item->fullname != "" ){
                                                echo $item->fullname;
                                            } else {
                                                echo $item->company;
                                            }
                                            ?>
                                        </option>
                                    <?php endforeach; ?>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="messageSubject" class="control-label col-xs-2">Subject</label>
                        <div class="col-xs-10">
                            <input type="text" class="form-control" placeholder="Message Subject" name="messageSubject" id="messageSubject" value="<?php if (isset($message['messageSubject'])): echo $message['messageSubject'];endif;?>">
                        </div>
                    </div>
                    <div class="clearfix">
                        <div class="form-group">
                            <label for="messageText" class="control-label col-xs-2">Message</label>
                            <div class="col-xs-10">
                            <script type="text/javascript" src="//cdn.tinymce.com/4/tinymce.min.js"></script>
                            <script>
                                tinymce.init({
                                    selector: 'textarea',
                                    height: 300,
                                    plugins: [
                                      'advlist autolink lists link image charmap print preview',
                                      'searchreplace visualblocks code fullscreen',
                                      'insertdatetime media table contextmenu paste code'
                                    ],
                                    toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
                                    content_css: [
                                      '//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
                                      '//www.tinymce.com/css/codepen.min.css'
                                    ]
                                });
                            </script>                            
                            <textarea rows="15" style="width:100%" placeholder="Message" name="messageText" id="messageText"><?php if (isset($message['messageText'])): echo $message['messageText'];endif;?></textarea>
                            </div>
                        </div>
                        <div class="clearfix">
                            <div class="col-xs-offset-2 col-xs-2 no_padding">
                                <input type="hidden" name="accessToken" value="<?php echo $token; ?>"/>
                                <button type="submit" class="Save_frm" style=" background: #054f72 none repeat scroll 0 0; color: white;">Send</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /#page-content-wrapper -->
    </div>
</div>

<script src="<?php echo base_url(); ?>theme/firstJob/js/chosen.jquery.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>theme/firstJob/js/prism.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
    var config = {
        '.chosen-select': {},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>



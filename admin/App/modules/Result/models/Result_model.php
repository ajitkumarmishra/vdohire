<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Job Model
 * Description : Handle all the CRUD operation for Job
 * @author Synergy
 * @createddate : Nov 22, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 22, 2017
 */

class Result_model extends CI_Model {

    /**
     * Declaring variables
     */
    var $table = "nc_interview";
    var $table_user = "users";
    var $answer_table = "nc_answers";
    var $question_table = "nc_questions";

    /**
     * Responsable for inheriting parent constructor
     * Responsable for auto load the session library
     * @return void
     */
    function __construct() {
        parent::__construct();
        $this->load->library('session');
    }

    /**
     * Description : Use to get all result with specific type
     * @author Synergy
     * @param string $type.
     * @return array of result data.
     */
    function read($type=null) {
        if ($type != null && $type != '')
            $this->db->where('type', $type);
        $this->db->where('finalstatus', '1');
        $query = $this->db->get($this->table);
        return $query->result();
    }

    /**
     * Description : Use to get user by interviewId
     * @author Synergy
     * @param int $interview_id.
     * @return array of user data.
     */
    function user_by_interview_id($interview_id) {

        $this->db->where('interview_id', $interview_id);
        $query = $this->db->get($this->table);
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    /**
     * Description : Use to get details of a user
     * @author Synergy
     * @param int $userid.
     * @return array of user data.
     */
    function user_details_id($userid) {

        $this->db->where('id', $userid);
        $query = $this->db->get($this->table_user);
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    /**
     * Description : Use to delete result result
     * @author Synergy
     * @param int $id.
     * @return boolean
     */
    function deleteresult($id) {
        $this->db->where('id', $id);
        $query = $this->db->delete($this->table);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Description : Use to get details of an interview given by specific user
     * @author Synergy
     * @param int $interview_id, int $user_id.
     * @return array of data
     */
    public function getAllResultinterviewid($interview_id, $user_id) {

        $this->db->where('interview_id', $interview_id);
        $this->db->where('user_id', $user_id);
        $this->db->from($this->answer_table);
        $result = $this->db->get();
        return $result->result();
    }

    /**
     * Description : Use to get details of a specific question 
     * @author Synergy
     * @param int $question_id
     * @return array of data
     */
    public function getResultquestion($question_id) {

        $this->db->where('question_id', $question_id);
        $this->db->select("question_id,question_file", FALSE);
        $this->db->from($this->question_table);
        $result = $this->db->get();
        return $result->result_array();
    }

    /**
     * Description : Use to get details of a specific interview 
     * @author Synergy
     * @param int $interview_id
     * @return array of data
     */
    public function getInterviewFile($interview_id) {
        $this->db->where('id', $interview_id);
        $this->db->from($this->table);
        $result = $this->db->get();
        return $result->row();
    }

    /**
     * Description : Use to get details of a specific interview 
     * @author Synergy
     * @param int $id
     * @return array of data
     */
    public function getDetailInt($id) {
        $this->db->where('id', $id);
        $this->db->from($this->table);
        $result = $this->db->get();
        return $result->row();
    }
}
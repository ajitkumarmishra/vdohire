<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Result Controller
 * Description : Use to handle all the functions related to Queue
 * @author Synergy
 * @createddate : Nov 22, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 22, 2017
 */
class Result extends MY_Controller {

    /**
     * Responsable for inheriting parent constructor
     * Responsable for auto load the the form_validation and session library
     * @return void
     */
    function __construct() {
        parent::__construct();
        $this->load->model('result_model');
        $this->load->library('form_validation');
        $this->load->library('session');
    }

    /**
     * Description : Use to display index page
     * Author : Synergy
     * @param array $_GET
     * @return render result data into view
     */
    function index() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('signin'));
        }
        if ($this->input->post() != null) {
            $this->user_model->updateStatus();
        }
        $includeJs = array('theme/js/custom.js', 'theme/js/bootbox.min.js');
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $cat = $this->input->get('cat', TRUE);
        $data["result"] = $this->result_model->read($cat);
        $data['main_content'] = 'result';
        $this->load->view('page', $data);
    }

    /**
     * Description : Use to get result
     * Author : Synergy
     * @param int $interview_id
     * @return render result data into view
     */
    function resutl_inter($interview_id) {

        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('signin'));
        }

        $details=$this->result_model->getDetailInt($interview_id);
        $data["result"] = $this->result_model->user_by_interview_id($details->interview_id);
        $data["user"] = $this->result_model->user_details_id($details->user_id);
        $answer = $this->result_model->getAllResultinterviewid($details->interview_id, $details->user_id);
        $getFile = $this->result_model->getInterviewFile($interview_id);

        if ($data["result"]) {
            $data['main_content'] = 'get_result';
            $data['interview'] = $getFile;
            $this->load->view('page', $data);
        } else {
            show_404();
        }
    }

    /**
     * Description : Use to delete a specific result
     * Author : Synergy
     * @param array $_POST
     * @return json encoded data
     */
    function delete() {
        $id = $this->input->post('delID');

        if (isset($id) and $id != '') {
            $check = $this->result_model->deleteresult($id);
            if ($check) {
                $data['status'] = true;
                $data['message'] = "Deleted Successfully";
                echo json_encode($data);
            } else {
                $data['status'] = false;
                $data['message'] = "Something Went wrong";
                echo json_encode($data);
            }
        } else {
            $data['status'] = false;
            $data['message'] = "Something Went wrong";
            echo json_encode($data);
        }
    }
}
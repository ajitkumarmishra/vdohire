<style>
    #playlist,audio{background:#666;width:400px;padding:20px;}
    .active a{color:#5DB0E6;text-decoration:none;}
    #playlist li a{color:#eeeedd;background:#333;padding:5px;display:block;}
    #playlist li a:hover{text-decoration:none;}
</style>

<div class="panel panel-default user-profile">
    <div class="panel-heading clearfix">
        <h3 class="panel-title pull-left">(<?php echo $user->user_nicename; ?>) Details</h3> 
    </div>
    <div class="list-group">
        <div class="list-group-item" style="height:70px;"> 

            <?php if ($user->user_pic) { ?>
                <img src="<?php echo base_url() . 'uploads/users/profile/' . $user->user_pic; ?>" class="img-rounded pull-left" width="50px">
                <?php
            } else {
                
            }
            ?>
        </div>
        <div class="list-group-item">
            <label>Interview ID</label>
            <h4 class="list-group-item-heading"><?php echo $result->interview_id; ?></h4>
        </div>
        <div class="list-group-item">
            <label>User Name:</label>
            <h4 class="list-group-item-heading"><?php echo $user->user_nicename; ?></h4>
        </div>
        <?php if ($interview) { ?>
            <div class="list-group-item">
                <label>Playlist:</label>
                <audio src="<?php echo base_url() . 'uploads/mergefiles/' . $interview->merge_file; ?>" controls autoplay id="audio" preload="auto" tabindex="0" controls="" type="audio/mpeg">
                </audio>
            </div>
        <?php } ?>


    </div>

</div>

<?php if ($this->session->flashdata('message')) { ?>
    <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Success!</strong> <?php echo $this->session->flashdata('message'); ?>
    </div>
<?php } ?>
<?php if ($this->session->flashdata('error')) { ?>
    <div class="alert alert-danger">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Danger!</strong> <?php echo $this->session->flashdata('error'); ?>
    </div>
<?php } ?>
<div class="row">
    <div class="col-sm-12 col-md-12">

        <form method="post" name="form1" id="form1" action="">
            <table class="table">
                <thead>
                    <tr>
                        <th><input name="check_all" type="checkbox" id="check_all" value="1" onclick="checkall(this.form)" /></th>
                        <th>Interview ID</th>
                        <th>User ID</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>Date</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbod>
                    <?php
                    foreach ($result as $results):
                        //print_r($results);
                        ?>
                        <tr class="<?php echo ($results->status == '1' ? 'success' : 'warning'); ?>">
                            <td>
                                <input name="arr_ids[]" type="checkbox" id="arr_ids[]" value="<?php echo $results->id; ?>" />
                            </td>
                            <td><a href="<?php echo base_url('result/' . $results->interview_id); ?>"><?php echo $results->interview_id; ?></a></td>
                            <td><?php echo $results->user_id; ?></td>
                            <td><?php echo $results->type; ?></td>
                            <td><?php echo ($results->status == '1' ? 'Active' : 'Inactive'); ?></td>
                            <td><?php echo $results->creation_date; ?></td>
                            <td> <a href="<?php echo base_url('result/' . $results->id); ?>" class="btn btn-mini btn-xs" name="<?php echo $results->interview_id; ?>"><span class="glyphicon glyphicon-eye-open "></span></a>
    <!--<td><a href="<?php //echo base_url('result/account/id') . '/' . $results->id;  ?>"><span class="glyphicon glyphicon-pencil pull-right"></a></td>-->
                                <a href="javascript:void(0);" class="btn btn-mini btn-xs del_Listing" name="<?php echo $results->id; ?>"><span class="glyphicon glyphicon-remove "></span></a>
                            </td>
                        </tr>
<?php endforeach; ?>
                </tbod>
            </table>

            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="left" style="padding:2px">
                        <!--<input type="submit" name="Activate" value="Activate" class="btn"/>
                        <input type="submit" name="Deactivate" value="Deactivate" class="btn" />-->
                        <input type="submit" name="Delete" value="Delete" class="btn" />
                    </td>
                </tr>
            </table>
            <input type="hidden" id="P_deleteurl" value="<?php echo base_url('result/delete'); ?>" />
        </form>
    </div>
</div>

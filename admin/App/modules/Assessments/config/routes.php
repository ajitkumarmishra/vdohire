<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['questionbank/add'] = "Assessments/addQuestionBank";
$route['questionbank/edit/(:any)'] = "Assessments/editQuestionBank/$1";

$route['questionbank/add/question/(:any)/(:any)'] = "Assessments/addQuestion/$1/$2";
$route['questionbank/add/question'] = "Assessments/addQuestion";
$route['questionbank/edit/question/(:any)'] = "Assessments/editQuestion/$1";


$route['questionbank/delete'] = "Assessments/deleteQuestionBank";
$route['assessments/question/delete'] = "Assessments/deleteQuestion";
$route['getAssessmentSetByType'] = "Assessments/getAssessmentSetByType";
$route['checkAssessment'] = "Assessments/checkAssessment";
$route['assessments/addToAssessment'] = "Assessments/addQuestionToAssessmentSet";
$route['assessments/add'] = "Assessments/addAssessment";
$route['getAssessmentQuestionRow'] = "Assessments/getAssessmentQuestionRow";
$route['assessments/edit/(:any)'] = "Assessments/editAssessment/$1";
$route['assessments/view/(:any)'] = "Assessments/getAssessment/$1";
$route['assessments/delete'] = "Assessments/deleteAssessment";

$route['assessments/list'] = "Assessments/listAssessments";
$route['assessments/list/(:any)'] = "Assessments/listAssessments/$1";
$route['assessments/fjlist/(:any)'] = "Assessments/listAssessments/$1";
$route['assessments/mylist/(:any)'] = "Assessments/listAssessments/$1";

$route['assessments/setAssessmentAsPractice'] = "Assessments/setAssessmentAsPractice";
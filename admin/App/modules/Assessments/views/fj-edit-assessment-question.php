<?php
if (isset($assessment) && $assessment != NULL) {
    //echo '<pre>';print_r($assessment);
}
//echo '<pre>';print_r($options);
?>
<div id="page-content-wrapper">
        <?php if (isset($message)): ?>
        <div class="alert alert-success">
        <?php echo $message; ?>
        </div>
    <?php endif; ?>
        <?php if (isset($error)): ?>
        <div class="alert alert-danger">
            <?php foreach ($error as $item): ?>
                <?php echo $item; ?>
        <?php endforeach; ?>
        </div>
<?php endif; ?>
    <div class="container-fluid whitebg">
        <div class="row">
            <div class="col-md-12 padd_tp_bt_20">
                <h4><img src="<?php echo base_url(); ?>/theme/firstJob/image/arrow_head.png" class="prev-page"> Edit Question</h4>
                <form class="form-horizontal form_save" method="post" action="" enctype="multipart/form-data" id="questionassessmentForm">
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-2 required-field">Type</label>
                        <div class="col-xs-10">
                            <select class="selectpicker" name="type" id="type" disabled="" data-validation="required"><option value="">Select</option>
                                <option value="1" <?php if (isset($assessment['type']) && $assessment['type'] == 1) echo "selected"; ?>>Video</option>
                                <option value="2" <?php if (isset($assessment['type']) && $assessment['type'] == 2) echo "selected"; ?>>Audio</option>
                                <option value="3" <?php if (isset($assessment['type']) && $assessment['type'] == 3) echo "selected"; ?>>Text</option>
                            </select>
                            <input type="hidden" name="type" value="<?php echo $assessment['type'];?>" />
                        </div>
                    </div>

                    <div id ="questionList">
                        <div class="form-group">
                            <label for="inputPassword" class="control-label col-xs-2 required-field">Question</label>
                            <div class="col-xs-3 ques-title">
                                <label for="inputPassword" class="control-label frm_in_padd">Title</label>
                                <input type="text" class="form-control file-name" name="question[0][title]" value="<?php if (isset($assessment['question'][0]['title'])) echo $assessment['question'][0]['title'];
else echo $assessment['title']; ?>" data-validation="required">
                            </div>
<?php if ($assessment['type'] != 3): ?>
                                <div class="col-xs-3 file-section">
                                    <div class="col-md-12">
                                        <label for="inputPassword" class="control-label frm_in_padd">File</label>
                                    </div>
                                    <div class="col-xs-8">
                                        <input type="text" class="form-control file-name questionFileName" readonly="readonly" value="<?php echo $assessment['file']; ?>">
                                    </div>
                                    <div class="col-xs-4 no_padding browse-section">
                                        <button type="button" class="btn_upload">BROWSE</button>
                                        <input type="file" name="file_0" class="my-question-file questionFile" />
                                        <input type="hidden" value="<?php if (isset($assessment['file'])) echo $assessment['file']; ?>" name="filename_0"/>
                                        <input type="text" data-validation="questionFile" style="height: 0px; width: 0px; visibility: hidden; " />
                                    </div>
                                    <br/>
                                    <div class="question-video">
                                        <div id="questionVideo_0" class="question-video" >
                                        </div>
                                    </div>
                                </div>
<?php endif; ?>
                            <div class="col-xs-3">
                                <label for="inputPassword" class="control-label frm_in_padd col-xs-12">To Question Bank</label>
                                <div class="col-md-6 no_padding">
                                    <select data-validation="required" class="selectpicker" name="question[0][questionBank]" id="questionBank"><option value="">Select</option>
                                        <?php foreach ($questionBanks as $item): ?>
                                            <option value="<?php echo $item['id']; ?>" <?php if ($assessment['question'][0]['questionBank'] == $item['id']) echo "selected";
                                        else if ($assessment['questionBank'] == $item['id']) echo "selected"; ?>><?php echo $item['name']; ?></option>
<?php endforeach; ?>
                                    </select>
                                </div>
                                <!--                                <div class="col-md-6">
                                                                    <p class="padd_txtmin">Sec <a href="javascript:"><img src="<?php echo base_url(); ?>/theme/firstJob/image/plus.png" class="pad_rg_lfs add-more-my-question">
                                                                        </a></p>
                                                                </div>  -->
                            </div> 

                        </div>

                        <div class="form-group">
<?php $j=1;foreach ($options as $item):?>
                                <label for="inputPassword" class="control-label col-xs-2"></label>
                                <div class="col-xs-10 ques-title">
                                    <div class="row">Answer<?php echo $j;?></label></div>
                                    <div class="row row-answer-assessment">
                                        <div class="col-sm-1 answer-check-assessment"> <input  data-validation="required" type="radio" name="question[0][check][]" <?php if ($item['id'] == $assessment['answer']) {echo "checked";} ?> class="answer-check" value="<?php echo $j;?>"/></div>
                                        <div class="col-sm-3">
                                            <input data-validation="required" type="text" class="form-control file-name" name="question[0][answer][<?php echo $j;?>]" value="<?php echo $item['option'] ?>">

                                        </div>
                                    </div>
                                </div>
<?php $j++; endforeach; ?>



                        </div>
                        
                        <div class="col-xs-10 ques-title">
                                <label for="inputPassword" class="control-label col-xs-10" style="color:#FF0000;padding-left: 150px;">Note : Please move the radio button to choose the correct answer</label>
                        </div>
                    





                    </div>

                    <div class="col-xs-offset-2 col-xs-10 no_padding"><br><br></div>
                    
                    <div class="clearfix"></div>
                    <div class="col-xs-offset-2 col-xs-10 no_padding">

                        <input type="hidden" name="accessToken" value="<?php echo $token; ?>"/>
                        <button type="submit" class="Save_frm pull-left save-interview" value="testing">SAVE</button>

                    </div>
                </form>

            </div>
        </div>
    </div>
</div>



<!-- PopUp for add question to interview set  -->
<div id="selectQuestionType" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <form class="form-horizontal form_save" action="<?php echo base_url(); ?>interviews/addToInterview" method="post" id="interviewQuestionForm" >
            <div class="modal-content">

                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Question Type</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger hidden" id="interviewQuestionAlert">

                    </div>
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-3"></label>
                        <div class="col-xs-9" id="questionType">
                            <select data-placeholder="questionType" style="width: 350px;" id="aQuestionType" name="questionType">
                                <option value="1">Fj Question Bank</option>
                                <option value="2">My Question Bank</option>

                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary select-interview-question">Add</button>

                    <!--                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
                </div>
            </div>
        </form>

    </div>
</div>

<script src="<?php echo base_url(); ?>theme/firstJob/js/chosen.jquery.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>theme/firstJob/js/prism.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">

    var config = {
        '.chosen-select': {},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: ''},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }

    $(function () {
        $("#datepicker-19").datepicker();
    });
    $(document).on('blur', '.chosen-search input', function (evt) {
        //alert($('.no-results').length);
        if ($('.no-results').length == 1) {
            //alert($('.chosen-choices .search-field input').val());
            //$('.chosen-select');
            $(this).parents('.ques-title').find('.chosen-select').append('<option>' + $(evt.target).val() + '</option>');
            $(this).parents('.ques-title').find('.chosen-select').val($(evt.target).val());
            $(this).parents('.ques-title').find('.chosen-select').trigger('chosen:updated');

        }

    });
<?php
if ($assessment['type'] == 1) {
    $type = 'video';
} else {
    $type = 'audio';
}
?>
    window.onload = function () {
            var videoUrl = "uploads/questions/<?php echo $type ?>/<?php echo $assessment['file'] ?>";
            //alert(videoUrl);
            jwVideo(videoUrl, 'questionVideo_0');
    };

</script>

<style>
    .no-results {
        display: none!important;
    }
    .chosen-single {
        background :none!important;
        border-radius:0px !important;
        height: 34px!important;
    }
</style>











<script src="<?php echo base_url(); ?>theme/firstJob/js/validate.js"></script>
<script type="text/javascript">
        // Add validator For Qualification
    $.formUtils.addValidator({
        name: 'questionFile',
        validatorFunction: function (value, $el, config, language, $form) {     
            var hasNoValue;
            var fileTypeValid;
            var fileType = $('#type').val();
            $('.questionFileName').each(function(i) {
                if ($(this).val() == '') {
                      hasNoValue = true;
                }
                else {
                    var fname = $(this).val();
                    if($('#type').val()=='1') {
                        var video = /(\.mp4)$/i;
                        if (video.test(fname))
                        fileTypeValid = true;
                    }                    
                    else if($('#type').val()=='2') {
                        var audio = /(\.mp3)$/i;
                        if (audio.test(fname))
                        fileTypeValid = true;
                    }
                    
                    
//                    var audio    = /\.(mp3|mp4)$/i;
//                    if (audio.test(fname))
//                    fileTypeValid=true;
                }
            });
            if (hasNoValue==true || fileTypeValid!=true) {
                return false;
            }
            else {
                return true;
            }
            //return parseInt(value, 10) % 2 === 0;
        },
        errorMessage : 'Please upload mp4 file for video set or mp3 file for audio set.'
    });
    $.validate({
        form: '#questionassessmentForm',
        modules: 'file',
        onError: function ($form) {
            //alert($('.level1').val());
            return false;
        },
        onSuccess: function ($form) {
            return true;
        },
        validateOnBlur : false,
        errorMessagePosition : 'top',
        scrollToTopOnError : false // Set this property to true on longer forms
    });
</script>
<?php
if (isset($interview) && $interview != NULL) {
    //echo '<pre>';print_r($interview);  
}
//echo "<pre>";print_r($questions); die;
?>
<div id="page-content-wrapper">
    <?php if (isset($message)): ?>
        <div class="alert alert-success">
            <?php echo $message; ?>
        </div>
    <?php endif; ?>
    <?php if (isset($error)): ?>
        <div class="alert alert-danger">
            <?php foreach ($error as $item): ?>
                <?php echo $item; ?>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
    <div class="container-fluid whitebg">
        <div class="row">

            <div class="col-md-12 padd_tp_bt_20">
                <h4><img src="<?php echo base_url(); ?>/theme/firstJob/image/arrow_head.png" class="prev-page"> Interview Set Detail</h4>
                <form class="form-horizontal form_save" method="post" action="" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="inputEmail" class="control-label col-xs-2 required-field">Set Name</label>
                        <div class="col-xs-10">
                            <input type="text" class="form-control" value="<?php
                            if (isset($interview['name'])): echo $interview['name'];
                            endif;
                            ?>" name="name" disabled="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-2">Type</label>
                        <div class="col-xs-10">
                            <input type="label" class="form-control" value="<?php if (isset($interview['type']) && $interview['type'] == 1) echo "Video";
                            else if(isset($interview['type']) && $interview['type'] == 2) {
                                echo "Audio";
                            } else if(isset($interview['type']) && $interview['type'] == 3){
                                echo "Text";
                            }
                            ?>" name="name" disabled="">
                            <input type="hidden" name="type" value="<?php echo $interview['type'] ?>"/>
                        </div>
                    </div>

                    <div id ="questionList">
                        <?php
                        $j = 0;
                        foreach ($questions as $item):
                            $duration = $item['duration'];
                            $file = $item['file'];
                            ?>
                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-2"><?php if ($j == 0) echo "Question"; ?></label>
                                <div class="col-xs-4 ques-title">
                                    <label for="inputPassword" class="control-label frm_in_padd">Title</label>
                                     <input type="text" class="form-control" placeholder="" name="question[<?php echo $j; ?>][title]" value="<?php if (isset($item['title'])) echo $item['title']; ?>" disabled="">
                                </div>
                                <?php if ($interview['type'] != 3): ?>
                                    <div class="col-xs-3 file-section">
                                        <div class="col-md-12">
                                            <label for="inputPassword" class="control-label frm_in_padd">File</label>
                                        </div>
                                        <div class="col-xs-8">

                                        </div>
                                        <div class="col-xs-4 no_padding browse-section">

                                        </div>
                                        <div id="questionVideo<?php echo $j; ?>" class="question-video" >
                                        </div>
                                    </div>
                                <?php endif; ?>

                                <div class="col-xs-3">
                                    <label for="inputPassword" class="control-label frm_in_padd col-xs-12">Answer Duration</label>
                                    <div class="col-md-6 no_padding">
                                        <input type="text" class="form-control" placeholder="" name="question[<?php echo $j; ?>][duration]" value="<?php if (isset($duration)) echo $duration; ?>" disabled="">
                                    </div>
                                    <div class="col-md-6">
                                        <p class="padd_txtmin">Sec 
                                           
                                        </p>
                                    </div>  
                                </div> 
                            </div>
                            <?php $j++;
                            ?>
                            <!--Java Script Code-->

                            <!--Java Script Code-->
                        <?php endforeach; ?>
                    </div>

                    <div class="clearfix"></div>
                    <div class="col-xs-offset-2 col-xs-10 no_padding">

                        <input type="hidden" name="accessToken" value="<?php echo $token; ?>"/>


                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url(); ?>theme/firstJob/js/chosen.jquery.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>theme/firstJob/js/prism.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">

    var config = {
        '.chosen-select': {},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: ''},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }

    $(function () {
        $("#datepicker-19").datepicker();
    });
    $(document).on('blur', '.chosen-search input', function (evt) {
        //alert($('.no-results').length);
        if ($('.no-results').length == 1) {
            //alert($('.chosen-choices .search-field input').val());
            //$('.chosen-select');
            $(this).parents('.ques-title').find('.chosen-select').append('<option>' + $(evt.target).val() + '</option>');
            $(this).parents('.ques-title').find('.chosen-select').val($(evt.target).val());
            $(this).parents('.ques-title').find('.chosen-select').trigger('chosen:updated');

        }

    });

    $(function () {
        var videoUrl = "uploads/jd/<?php echo $interview['jd'] ?>";
        jwVideo(videoUrl, 'questionVideo');
    });


</script>
<style>
    .no-results {
        display: none!important;
    }
    .chosen-single {
        background :none!important;
        border-radius:0px !important;
        height: 34px!important;
    }
    input,select, .chosen-single{
        background: white!important;
        border: 0px!important;
        cursor: initial !important;
        box-shadow: inset 0px 0px 0px 0px red !important;
    }
    #questionList .form-group {
        background: #f1f1f1;
    }
</style>

<script>
<?php
if ($interview['type'] == 1) {
    $type = 'video';
} else {
    $type = 'audio';
}
?>
    window.onload = function () {
        // alert('uploads/questions/video/<?php echo $item['file'] ?>');
<?php
$v = 0;
foreach ($questions as $item):
    ?>
            var videoUrl = "uploads/questions/<?php echo $type ?>/<?php echo $item['file'] ?>";
            //alert(videoUrl);
            jwVideo(videoUrl, 'questionVideo<?php echo $v; ?>');
    <?php
    $v++;
endforeach;
?>
    };
</script>

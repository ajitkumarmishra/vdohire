<!-- Page Content -->

<form method="post" action="" id="questionBankForm" enctype="multipart/form-data">
    <div id="page-content-wrapper">
        <?php if (isset($message)): ?>
            <div class="alert alert-success">
                <?php echo $message; ?>
            </div>
        <?php endif; ?>
        <?php if (isset($error)): ?>
            <div class="alert alert-danger">
                <?php foreach ($error as $item): ?>
                    <?php echo $item; ?>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
        <div class="container-fluid whitebg">
            <div class="row">

                <div class="col-md-12 padd_tp_bt_20">
                    <h4><img src="<?php echo base_url(); ?>/theme/firstJob/image/arrow_head.png" class="prev-page">Edit Question Bank</h4>
                    <div class="form-horizontal form_save">
                        <div class="form-group">
                            <label for="inputEmail" class="control-label col-xs-2">Name</label>
                            <div class="col-xs-10">
                                <input type="text"  value="<?php
                                if (isset($questionBank['name'])): echo $questionBank['name'];
                                endif;
                                ?>" name="name" id="name" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword" class="control-label col-xs-2">Type</label>
                            <div class="col-xs-10">
                                <select class="selectpicker search-critera" name="type" disabled=""><option value="">Select</option>
                                    <option value="1" <?php
                                    if (isset($questionBank['type']) && $questionBank['type'] == 1): echo "selected";
                                    endif;
                                    ?>>Video</option>
                                    <option value="2" <?php
                                    if (isset($questionBank['type']) && $questionBank['type'] == 2): echo "selected";
                                    endif;
                                    ?>>Audio</option>
                                    <option value="3" <?php
                                    if (isset($questionBank['type']) && $questionBank['type'] == 3): echo "selected";
                                    endif;
                                    ?>>Text</option>
                                </select>
                                <input type="hidden" name="type" value="<?php echo $questionBank['type']; ?>"/>
                            </div>
                        </div>
                        <?php if ( $questionBank['type'] == 3 ): ?>
                        <div class="form-group" id="enableOnTextSelectBlock">
                            <label for="inputEmail" class="control-label col-xs-2">Upload CSV file</label>
                            <div class="col-xs-10">
                                <input type="file" name="questionFile" id="questionFile" style="border: 0px" placeholder="">
                                <?php $fileSamleCsv = base_url() . 'uploads/questions/questioncsvfile/questionset.csv';  ?>
                                <div style="padding-top:20px;">Sample File: <a href="<?php echo $fileSamleCsv; ?>"> Download Sample File</a></div>
                            </div>
                        </div>
                        <?php endif; ?>
                        
                        <!-- <div class="form-group">
                             <label for="inputEmail" class="control-label col-xs-2">Keyword</label>
                             <div class="col-xs-10">
                                 <input type="email" class="form-control" placeholder="">
                             </div>
                         </div>  -->

                        <div class="col-xs-offset-2 col-xs-2 no_padding">
                            <input type="hidden" name="accessToken" value="<?php echo $token; ?>"/>
                            <button type="submit" class="Save_frm">Save</button>
                        </div>  

                    </div>

                </div>

            </div> 

        </div>
    </div>
</div>
</div>
</form>
<!-- /#page-content-wrapper -->
<script src="<?php echo base_url(); ?>theme/firstJob/js/chosen.jquery.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>theme/firstJob/js/prism.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
    var config = {
        '.chosen-select': {},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>
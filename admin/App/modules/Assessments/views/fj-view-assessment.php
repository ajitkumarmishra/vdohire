<?php //echo '<pre>';print_r($assessmentQuestions);?>
<div id="page-content-wrapper">
    <?php if (isset($message)): ?>
        <div class="alert alert-success">
            <?php echo $message; ?>
        </div>
    <?php endif; ?>
    <?php if (isset($error)): ?>
        <div class="alert alert-danger">
            <?php foreach ($error as $item): ?>
                <?php echo $item; ?>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
    <div class="container-fluid whitebg">
        <div class="row">

            <div class="col-md-12 padd_tp_bt_20">
                <h4><img src="<?php echo base_url(); ?>/theme/firstJob/image/arrow_head.png" class="prev-page"> View Assessment Set Detail</h4>
                <form class="form-horizontal form_save" action="" method="post">
                    <div class="form-group"> 	
                        <label for="inputEmail" class="control-label rsp_padd15 col-xs-2 rsp_width_100 required-field">Name</label>
                        <div class="col-xs-10 rsp_width_100">
                            <?php if (isset($assessment['name'])) echo $assessment['name']; ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-2 rsp_padd15 required-field">Answer Duration</label>
                        <div class="col-xs-1 rsp_width_100" style="padding-top: 8px;">
                            <?php if (isset($assessment['duration'])) echo round($assessment['duration'] /60, 2); ?>
                        </div>
                        <div class="col-xs-8 rsp_width_100"><p class="padd_txtmin">Minutes</p></div>
                    </div>
                    <div id="questionList">
                        <?php 
                        $i = 0;
                        foreach ($assessmentQuestions as $assessmentQuestion): ?>
                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-2 rsp_padd15">Question</label>
                                <div class="col-xs-5 rsp_width_100">
                                    <label for="inputPassword" class="control-label no_padding_left"><b>Number of Question</b></label>
                                    <div class="clearfix"></div>
                                    <?php echo $assessmentQuestion['totalQuestion'];?>
                                </div>

                                <div class="col-xs-4 rsp_width_100">
                                    <label for="inputPassword" class="control-label no_padding_left"><b>From Question Bank</b></label>
                                    <div class="clearfix"></div>
                                    <div>
                                        <?php foreach ($questionBanks as $item): ?>
                                            <?php if($assessmentQuestion['questionBank'] == $item['id']) {  echo $item['name']; } ?>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                                
                            </div>
                        <?php 
                        $i++;
                        endforeach; ?>
                    </div>

                    <div class="clearfix">

                        


                    </div></form>
            </div>
        </div>
    </div>
    <!-- /#page-content-wrapper -->
</div>
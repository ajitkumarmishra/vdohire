<?php 
// loggedin user id
$userId = getUserId(); 
$userDetForPage = getUserById($userId);
$userRoleForPage = $userDetForPage['role'];

//echo '<pre>';print_r($contentMyQuestionBank); die();
$urlSeg = $this->uri->segment(2);
if($urlSeg == 'fjlist'){ ?>
<script>
$(function(){
    $('#fjQuestionBank').trigger('click');
    $('.service_content').hide();
    $('#fjPaging').show();
});
</script>
<?php }  else if($urlSeg == 'mylist') { ?>
<script>
$(function(){
    $('#myAssessmentQuestions').trigger('click');
    $('.service_content').hide();
    $('#myPaging').show();
});
</script>
<?php } else if($urlSeg == 'list') {?>
<script>
$(function(){
    $('#AssessmentSet').trigger('click');
    $('.service_content').hide();
    $('#setPaging').show();
});
</script>
<?php }?>
<div id="page-content-wrapper">

    <div class="container-fluid whitebg">
        <div class="row">
            <div class="col-md-9"><h2><img src="<?php echo base_url(); ?>/theme/firstJob/image/arrow_head.png" class="prev-page">Manage Assessment Questions</h2></div>
            <div class="col-md-3 ed_job_bt"><a href="<?php echo base_url(); ?>assessments/add" class="add-new-set"><button type="button" class="add-new-set-text">Add New Assessment Set</button></a></div>

            <div class="clearfix"></div>

            <div class="col-md-12 rsp_tbl">

                <ul class="nav nav-pills padd_tabs">
                    <li class="active active_tab"><a data-toggle="pill" href="#home" id="AssessmentSet">Assessment Sets</a></li>
                    <?php if( $userRoleForPage != 1 ) { ?>
                    <li class="fjQuestionBank1"><a data-toggle="pill" href="#menu1" id="fjQuestionBank">FJ Question Banks</a></li>
                    <?php } ?>
                    <li class="myAssessmentQuestions1"><a data-toggle="pill" href="#menu2" id="myAssessmentQuestions">My Question Banks</a></li>
                </ul>

                <div class="tab-content">
                    <div id="home" class="tab-pane fade active in">
                        <table class="table" id="sorting">
                            <thead class="tbl_head">
                                <tr>
      <!--                          	<th><input type="checkbox" class="chk_bx"></th>-->
                                    <th><span>Title</span></th>
                                    <th><span>Duration</span></th>
                                    <th><span>No. of Questions</span></th>
                                    <?php if($userRoleForPage==1): ?>
                                    <th><span>Practice Set</span></th>
                                    <?php endif; ?>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 0;
                                foreach ($contentAssessmentSet as $itemAssessment):
                                    
                                    // ci instance
                                    $ci =&get_instance();
                                    //job user assessment model
                                    $ci->load->model('userassessment_model');
                                    
                                    // function to get users assessment available or not
                                    $userassessmentcount = $ci->userassessment_model->countForUserAssessment($itemAssessment->id); 
                                    $assessmentcount = $userassessmentcount['assessmentcount'];
                                    
                                    ?>
                                    <tr <?php if ($i % 2 != 0) echo 'class="tbl_bg"'; ?>>
                                        <td><a href="<?php echo base_url(); ?>assessments/view/<?php echo $itemAssessment->id; ?>"><?php echo $itemAssessment->name; ?></a></td>
                                        <td><?php echo round($itemAssessment->duration/60, 2); ?> Minutes</td>
                                        <td><?php
                                            $countArr = getNoOfQuestionsPerAssessment($itemAssessment->id);
                                            echo $countArr['count'];
                                            ?></td>
                                        <?php if($userRoleForPage==1): ?>
                                        <td><input type="checkbox" name="asspractice_set_<?=$itemAssessment->id?>" id="asspractice_set_<?=$itemAssessment->id?>" value="<?=$itemAssessment->id?>" onclick="checkedForPractice(this);" <?=$itemAssessment->practiceSetStatus==1?"checked":""?> /></td>
                                        <?php endif; ?>
                                        <td>
                                            <?php
                                            $checkAssessment = assessmentExists($itemAssessment->id);
                                            //echo $checkAssessment;
                                            ?>
                                            <a href="<?php echo base_url(); ?>assessments/edit/<?php echo $itemAssessment->id; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/edit.png" class="padd_rgt"></a>
                                            <?php if( $assessmentcount > 0 || $checkAssessment>0 ) { ?>
                                                <a href="javascript:" class="not-delete-assessment-set" id="<?php echo $itemAssessment->id; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/del.png"></a>
                                            <?php } else {
                                                ?>
                                                <a href="javascript:" class="delete-assessment-set" id="<?php echo $itemAssessment->id; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/del.png"></a>
                                            <?php } 
                                            ?>
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                endforeach;
                                ?>
                            </tbody>
                        </table>
                    </div>


                    <div id="menu1" class="tab-pane fade">
                        <?php                         
                        //echo "Count".count($contentMyQuestionBank);
                        if(count($contentFjQuestionsBank[0]['id']) == ''):?>
                        <table class="table top-head-table question-bank-table">
                            <tbody>
                                <tr class="table_brown_head">
                                    <td class="active">Title</td>
                                    <td class="active">Type</td>
                                    <td class="active">No.of Questions</td>
                                    <td class="active"></td>
                                </tr>  
                            </tbody>
                        </table>
                        <?php endif;?>
                        <?php 
                        $i = 0;
                        foreach ($contentFjQuestionsBank as $item):
                            ?>
                            <div class=" rsp_tbl">
                                <table class="table top-head-table question-bank-table">
                                    <?php if ($i == 0): ?>
                                    <tbody><tr class="table_brown_head">
                                            <td class="active">Title</td>
                                            <td class="active">Type</td>
                                            <td class="active">No.of Questions</td>
                                            <td class="active"></td>
                                        </tr>  
                                    </tbody>
                                    <?php endif; ?>
                                    <tbody>
                                        <tr class="tbl_bg_light">
                                            <td><?php echo $item['name']; ?></td>
                                            <td><?php echo getInterviewSetType($item['type']); ?>
                                                <?php if( $item['type'] != 3 ) { ?>
                                                    <div id ="questionMyVideo<?php echo $item['id']; ?>"></div>
                                                <?php } ?>
                                            </td>
                                            <td><?php 
                                            $q = getQuestionsByQuestionBank($item['id']);
                                            echo $q['count']; ?></td>
                                            <td>
                                                <div class="pull-right">
                                                    <!--<href="#"><img style="display:block !important;" src="<?php echo base_url(); ?>/theme/firstJob/image/arrow_rot.png" id="img_to" class="padd_rgt collapsed" href="#demo1<?php echo $item['id']; ?>" data-toggle="collapse" aria-expanded="false"></a>-->
                                                    <a href="#"><img src="<?php echo base_url(); ?>/theme/firstJob/image/arrow_rot.png" id="img_to" class="padd_rgt collapsed" href="#demo1<?php echo $item['id']; ?>" data-toggle="collapse" aria-expanded="false"></a>
                                                </div>	
                                            </td>
                                        </tr>  
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-12 rsp_tbl collapse" id="demo1<?php echo $item['id']; ?>" aria-expanded="false" style="height: 0px;"> 
                                <table class="table padding_bt_5">
                                    <tbody>
                                        <?php
                                        $q = 0;
                                        $arr = getQuestionOfQuestionBank($item['id']);
                                        if (count($arr) > 0):
                                            foreach ($arr as $itemQuestion):
                                                ?>
                                                <tr>
                                                    <td><?php echo $itemQuestion['title']; ?></td>
                                                    <td>
                                                        <div class="pull-right">
                                                            <a href="javascript:" data-toggle="modal" data-target="#addQuestionToAssessmentSet" class="add-question-to-assessmentset" data-questionid="<?php echo $itemQuestion['id']; ?>" data-id="<?php echo $itemQuestion['type']; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/add_black.png"></a>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php
                                                $q++;
                                            endforeach;
                                        else: ?>
                                                <tr>
                                                    <td></td>
                                                    <td>
                                                        <div class="pull-right">&nbsp;</div>
                                                    </td>
                                                </tr>
                                           <?php
                                        endif;
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        <?php $i++;
                        endforeach;
                        ?>
                    </div>
                    
                    <div id="menu2" class="tab-pane fade">
                        <?php 
                        
                        //echo "Count".count($contentMyQuestionBank);
                        if(count($contentMyQuestionBank[0]['id']) == ''):?>
                        <table class="table top-head-table question-bank-table">
                            <tbody>
                                <tr class="table_brown_head">
                                    <td class="active">Title</td>
                                    <td class="active">Type</td>
                                    <td class="active">No.of Questions</td>
                                    <td class="active"></td>
                                </tr>  
                            </tbody>
                        </table>
                        <?php endif;?>
                        <?php $i = 0;
                        foreach ($contentMyQuestionBank as $item):
                            ?>
                            <div class=" rsp_tbl">
                                <table class="table top-head-table question-bank-table">
    <?php if ($i == 0): ?>
                                        <tbody><tr class="table_brown_head">
                                                <td class="active">Title</td>
                                                <td class="active">Type</td>
                                                <td class="active">No.of Questions</td>
                                                <td class="active"></td>
                                            </tr>  
                                        </tbody>
    <?php endif; ?>
                                    <tbody>
                                        <tr class="tbl_bg_light">
                                            <td><?php echo $item['name']; ?></td>
                                            <td><?php echo getInterviewSetType($item['type']); ?>
                                                <?php if( $itemMy->type != 3 ) { ?>
                                                    <div id ="questionMyVideo<?php echo $itemMy->type; ?>"></div>
                                                <?php } ?>
                                            </td>
                                            <td><?php 
                                            $q = getQuestionsByQuestionBank($item['id']);
                                            //echo $q['count']; ?></td>
                                            <td>
                                                <?php
                                                $checkAssessmentBank = assessmentBankExists($item['id']);
                                                //print_r(assessmentBankExists($item['id']));
                                                ?>
                                                <div class="pull-right">
                                                    <a href="<?php echo base_url(); ?>questionbank/edit/<?php echo $item['id']; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/edit.png" class="padd_rgt"></a>
                                                    <?php if( $checkAssessmentBank>0 ) { ?>
                                                    <img src="<?php echo base_url(); ?>/theme/firstJob/image/del.png" class="padd_rgt" title="Sorry, this question bank cannot be deleted as it is associated with some job.">
                                                    <?php } else { ?>
                                                    <a href="javascript:" class="delete-question-bank" data-questionbankid="<?php echo $item['id']; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/del.png" class="padd_rgt"></a>
                                                    <?php } ?>
                                                    <a href="#"><img src="<?php echo base_url(); ?>/theme/firstJob/image/arrow_rot.png" id="img_to" class="padd_rgt collapsed" href="#demo<?php echo $item['id']; ?>" data-toggle="collapse" aria-expanded="false"></a>
                                                </div>	
                                            </td>
                                        </tr>  
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-12 rsp_tbl collapse" id="demo<?php echo $item['id']; ?>" aria-expanded="false" style="height: 0px;"> 
                                <table class="table padding_bt_5">
                                    <tbody>
                                        <?php
                                        $q = 0;
                                        $arr = getQuestionOfQuestionBank($item['id']);
                                        if (count($arr) > 0):
                                            foreach ($arr as $itemQuestion):
                                                ?>
                                                <tr>
                                                    <td><?php echo $itemQuestion['title']; ?></td>
                                                    <td style="width:40%;">
                                                        <div class="pull-right">
                                                            <a href="<?php echo base_url(); ?>questionbank/edit/question/<?php echo $itemQuestion['id']; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/edit.png" class="padd_rgt"></a>
                                                            <a href="javascript:" class="delete-question-assessment" data-questionid="<?php echo $itemQuestion['id']; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/del.png" class="padd_rgt"></a>
                                                            <a href="javascript:" data-toggle="modal" data-target="#addQuestionToAssessmentSet" class="add-question-to-assessmentset" data-questionid="<?php echo $itemQuestion['id']; ?>" data-id="<?php echo $itemQuestion['type']; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/add_black.png"></a>
                                                            <a href="<?php echo base_url(); ?>questionbank/add/question/<?php echo $item['id']; ?>/<?php echo $item['type']; ?>"><button type="button" class="btn_add_it btn-q-add <?php if ($q != 0) echo " add-opacity" ?>"> ADD QUESTION </button></a>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php
                                                $q++;
                                            endforeach;
                                            else: ?>
                                                <tr>
                                                    <td></td>
                                                    <td>
                                                        <div class="pull-right">
<!--                                                            <a href="<?php echo base_url(); ?>questionbank/edit/question/<?php echo $itemQuestion['id']; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/edit.png" class="padd_rgt"></a>
                                                            <a href="javascript:" class="delete-question-assessment" data-questionid="<?php echo $itemQuestion['id']; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/del.png" class="padd_rgt"></a>
                                                            <a href="javascript:" data-toggle="modal" data-target="#addQuestionToAssessmentSet" class="add-question-to-assessmentset" data-questionid="<?php echo $itemQuestion['id']; ?>" data-id="<?php echo $itemQuestion['type']; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/add_black.png"></a>-->
                                                            <a href="<?php echo base_url(); ?>questionbank/add/question/<?php echo $item['id']; ?>/<?php echo $item['type']; ?>"><button type="button" class="btn_add_it btn-q-add"> ADD QUESTION </button></a>
                                                        </div>
                                                    </td>
                                                </tr>
                                           <?php
                                        endif;
                                        ?>
                                    </tbody>
                                </table>
                            </div>
    <?php $i++;
endforeach;
?>
                  </div>	
                </div>
            </div> 
<!--                        <div class="col-md-12">  
                            <ul class="pagination pull-right">
                                <li class="active"><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                            </ul>
                        </div> -->
<div class="service_content " id="setPaging">

                <?php if ($totalCount > $itemsPerPage): ?>
                    <ul class="service_content_last_list">
                        <?php if ($page > 1): ?>
                            <p class="pagi_img"><a href="<?php echo base_url(); ?>assessments/list/<?php echo $page - 1; ?>" ><img src="<?php echo base_url(); ?>/theme/firstJob/image/pagination_img2.png"></a></p>    
                        <?php endif; ?>
                        <input type="hidden" name="page_no" value="1" id="page_no"> 
                        <?php for ($i = 1; $i <= $count; $i++): ?>
                            <li><a href="<?php echo base_url(); ?>assessments/list/<?php echo $i; ?>"  <?php if ($i == $page): ?>style="color: orange;"<?php endif; ?>><?php echo $i; ?></a></li>
                        <?php endfor; ?>
                        <?php if ($page < $count): ?>
                            <p class="pagi_img"><a href="<?php echo base_url(); ?>assessments/list/<?php echo $page + 1; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/pagination_img1.png"></a></p>            
                        <?php endif; ?>
                    </ul>
                <?php endif; ?>
            </div>
            <div class="service_content " id="fjPaging">
                <?php if ($totalCountFj > $itemsPerPage): ?>
                    <ul class="service_content_last_list">
                        <?php if ($page > 1): ?>
                            <p class="pagi_img"><a href="<?php echo base_url(); ?>assessments/fjlist/<?php echo $page - 1; ?>" ><img src="<?php echo base_url(); ?>/theme/firstJob/image/pagination_img2.png"></a></p>    
                        <?php endif; ?>
                        <input type="hidden" name="page_no" value="1" id="page_no"> 
                        <?php for ($i = 1; $i <= $countFj; $i++): ?>
                            <li><a href="<?php echo base_url(); ?>assessments/fjlist/<?php echo $i; ?>"  <?php if ($i == $page): ?>style="color: orange;"<?php endif; ?>><?php echo $i; ?></a></li>
                        <?php endfor; ?>
                        <?php if ($page < $countFj): ?>
                            <p class="pagi_img"><a href="<?php echo base_url(); ?>assessments/fjlist/<?php echo $page + 1; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/pagination_img1.png"></a></p>            
                        <?php endif; ?>
                    </ul>
                <?php endif; ?>
            </div>
            <div class="service_content " id="myPaging">
                <?php 
                if ($totalCountMy > $itemsPerPage): ?>
                    <ul class="service_content_last_list">
                        <?php if ($page > 1): ?>
                            <p class="pagi_img"><a href="<?php echo base_url(); ?>assessments/mylist/<?php echo $page - 1; ?>" ><img src="<?php echo base_url(); ?>/theme/firstJob/image/pagination_img2.png"></a></p>    
                        <?php endif; ?>
                        <input type="hidden" name="page_no" value="1" id="page_no"> 
                        <?php for ($i = 1; $i <= $countMy; $i++): ?>
                            <li><a href="<?php echo base_url(); ?>assessments/mylist/<?php echo $i; ?>"  <?php if ($i == $page): ?>style="color: orange;"<?php endif; ?>><?php echo $i; ?></a></li>
                        <?php endfor; ?>
                        <?php if ($page < $countMy): ?>
                            <p class="pagi_img"><a href="<?php echo base_url(); ?>assessments/mylist/<?php echo $page + 1; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/pagination_img1.png"></a></p>            
                        <?php endif; ?>
                    </ul>
                <?php endif; ?>
            </div>

        </div>
    </div>
</div>




<!-- PopUp for add question to assessment set  -->
<div id="addQuestionToAssessmentSet" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <form class="form-horizontal form_save" action="<?php echo base_url(); ?>assessments/addToAssessment" method="post" id="interviewQuestionForm" >
            <div class="modal-content">

                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Question To Assessment Set</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger hidden" id="interviewQuestionAlert">

                    </div>
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-3">Assessment Sets</label>
                        <div class="col-xs-9" id="assessmentSets">
    <!--                        <select data-placeholder="Location" style="width: 350px; display: none;" multiple="" class="chosen-select" tabindex="-1" name="cityState[]">
                                <option value=""></option>
                                <option>Test1</option>
                                <option>Test2</option>
    
                            </select>-->
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary add-assessment-question">Save</button>

                    <!--                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
                </div>
            </div>
        </form>

    </div>
</div>

<script src="<?php echo base_url(); ?>theme/firstJob/js/chosen.jquery.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>theme/firstJob/js/prism.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
    var config = {
        '.chosen-select': {},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }

    $(function () {
        $("#datepicker-19").datepicker();
    });
    
    window.onload = function () {
        <?php foreach ($contentFjQuestions as $itemFj):
        if($itemFj->type == 1) {
            $type='video';
        } else if($itemFj->type == 2) {
            $type='audio';
        }
        ?>
        
       // alert(videoUrl);
        <?php if($itemFj->type != 3 && $itemFj->file != "") { ?>
        var videoUrl = "uploads/questions/<?php echo $type; ?>/<?php echo $itemFj->file; ?>";
        jwVideoInList(videoUrl, 'questionVideo<?php echo $itemFj->id; ?>');
        <?php } ?>
        <?php endforeach;?>
            
        <?php foreach ($contentMyQuestionBank as $itemMy):
        if($itemMy['type'] == 1) {
            $type='video';
        } else if($itemMy['type'] == 2) {
            $type='audio';
        }
        ?>
        
       // alert(videoUrl);
        <?php if($itemMy['type'] != 3 && $itemMy['file'] != "" ) { ?>
        var videoUrl = "uploads/questions/<?php echo $type; ?>/<?php echo $itemMy['file']; ?>";
        jwVideoInList(videoUrl, 'questionMyVideo<?php echo $itemMy['id']; ?>');
        <?php } ?>
        <?php endforeach;?>
    }
    
    $('.fjQuestionBank1').click(function(){
        
        <?php if( $userRoleForPage != 1 )  { ?>
                $('.add-new-set').hide();
                //$('.add-new-set-text').text('');
                $('.service_content').hide();
                $('#fjPaging').show();
        <?php } ?>
        <?php foreach ($contentFjQuestions as $itemFj):
        if($itemFj->type == 1) {
            $type='video';
        } else if($itemFj->type == 2) {
            $type='audio';
        }
        ?>
       // alert(videoUrl);
        <?php if($itemFj->type != 3 && $itemFj->file != "") { ?>
        var videoUrl = "uploads/questions/<?php echo $type; ?>/<?php echo $itemFj->file; ?>";
        jwVideoInList(videoUrl, 'questionVideo<?php echo $itemFj->id; ?>');
        <?php } ?>
        <?php endforeach;?>

    });
    
    $('.myAssessmentQuestions1').click(function(){
        $('.add-new-set').show();
        <?php foreach ($contentMyQuestionBank as $itemMy):
        if($itemMy['type'] == 1) {
            $type='video';
        } else if($itemMy['type'] == 2) {
            $type='audio';
        }
        ?>
        // alert(videoUrl);
        <?php if($itemMy['type'] != 3 && $itemMy['file'] != "" ) { ?>
        var videoUrl = "uploads/questions/<?php echo $type; ?>/<?php echo $itemMy['file']; ?>";
        jwVideoInList(videoUrl, 'questionMyVideo<?php echo $itemMy['id']; ?>');
        <?php } ?>
        <?php endforeach;?>

    });
    

</script>

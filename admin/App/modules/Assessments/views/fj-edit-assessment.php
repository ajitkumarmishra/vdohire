<?php //echo '<pre>';print_r($assessmentQuestions);
if(isset($assessment) && $assessment != NULL) {
    //echo '<pre>';print_r($assessment); die();
}
?>
<div id="page-content-wrapper">
    <?php if (isset($message)): ?>
        <div class="alert alert-success">
            <?php echo $message; ?>
        </div>
    <?php endif; ?>
    <?php if (isset($error)): ?>
        <div class="alert alert-danger">
            <?php foreach ($error as $item): ?>
                <?php echo $item; ?>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
    <div class="container-fluid whitebg">
        <div class="row">

            <div class="col-md-12 padd_tp_bt_20">
                <h4><img src="<?php echo base_url(); ?>/theme/firstJob/image/arrow_head.png" class="prev-page"> Edit Assessment Set</h4>
                <form class="form-horizontal form_save" action="" method="post" id="assessmentForm">
                    <div class="form-group"> 	
                        <label for="inputEmail" class="control-label rsp_padd15 col-xs-2 rsp_width_100 required-field">Set Name</label>
                        <div class="col-xs-10 rsp_width_100">
                            <input type="text" class="form-control" data-validation="required" name="name" value="<?php if (isset($assessment['name'])) echo $assessment['name']; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-2 rsp_padd15 required-field">Answer Duration</label>
                        <div class="col-xs-2 rsp_width_100">
                            <input type="text" class="form-control" data-validation="number" maxlength="2" name="duration" value="<?php if (isset($oldAssessment['duration'])) echo round($oldAssessment['duration'] /60, 2); ?>">
                        </div>
                        <div class="col-xs-8 rsp_width_100"><p class="padd_txtmin">Minutes</p></div>
                    </div>
                    <div class="form-group">
                        <div class="form-group">
                            <label for="inputPassword" class="control-label col-xs-2 rsp_padd15"></label>
                            <div class="col-xs-5 rsp_width_100">
                                <label for="inputPassword" class="control-label no_padding_left"></label>
                                <div class="clearfix"></div>
                            </div>

                            <div class="col-xs-4 rsp_width_100">
                                <label for="inputPassword" class="control-label no_padding_left"></label>
                                <div class="clearfix"></div>                                
                            </div>
                            <div class="col-xs-1"><a href="javascript:"><img src="<?php echo base_url(); ?>/theme/firstJob/image/plus.png" class="plus_sep add-more-assessment-question"></a></div>
                        </div>                        
                    </div>
                    <div id="questionList">
                        <?php 
                        //echo "<pre>"; print_r($assessment); exit;
                        if(count($assessmentQuestions) == 0) { ?>
                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-2 rsp_padd15">Question</label>
                                <div class="col-xs-5 rsp_width_100">
                                	<label for="inputPassword" class="control-label no_padding_left">Number of Question</label>
                                    <div class="clearfix"></div>
                                    <input type="text" class="form-control assessmentQuestion" placeholder="" name="question[0][number]">
                                    <input type="text" data-validation="assessmentQuestion" style="height: 0px; width: 0px; visibility: hidden; " />
                                </div>
                                
                                <div class="col-xs-4 rsp_width_100">
                                	<label for="inputPassword" class="control-label no_padding_left">From Question Bank</label>
                                    <div class="clearfix"></div>
                                    <select class="selectpicker assessmentBank" name="question[0][questionBank]">
                                      <?php foreach($questionBanks as $item):?>
                                        <option value="<?php echo $item['id'];?>"><?php echo $item['name'];?></option>
                                        <?php endforeach;?>
                                    </select>
                                    <input type="text" data-validation="assessmentBank" style="height: 0px; width: 0px; visibility: hidden; " />
                                </div>
                                <!--<div class="col-xs-1"><a href="javascript:"><img src="<?php echo base_url(); ?>/theme/firstJob/image/plus.png" class="plus_sep add-more-assessment-question"></a></div>-->
                                <div class="col-xs-1"><button type="button" class="del-assessment" value="testing">x</button></div>
                            </div>
                      <?php  } 
                        else {                        
                            if(count($assessment['question']) > 0):
                                $aq = 0;
                                foreach ($assessment['question'] as $aQItem): ?>
                                    <div class="form-group">
                                        <label for="inputPassword" class="control-label col-xs-2 rsp_padd15">Question</label>
                                        <div class="col-xs-5 rsp_width_100">
                                            <label for="inputPassword" class="control-label no_padding_left">Number of Question</label>
                                            <div class="clearfix"></div>
                                            <input type="text" class="form-control assessmentQuestion" placeholder="" name="question[<?php echo $aq;?>][number]" value="<?php echo $aQItem['number'];?>">
                                            <input type="text" data-validation="assessmentQuestion" style="height: 0px; width: 0px; visibility: hidden; " />
                                        </div>

                                        <div class="col-xs-4 rsp_width_100">
                                            <label for="inputPassword" class="control-label no_padding_left">From Question Bank</label>
                                            <div class="clearfix"></div>
                                            <select class="selectpicker assessmentBank" name="question[<?php echo $aq;?>][questionBank]">
                                                <?php foreach ($questionBanks as $item): ?>
                                                    <option value="<?php echo $item['id']; ?>" <?php if($aQItem['questionBank'] == $item['id']) echo "selected";?>><?php echo $item['name']; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                            <input type="text" data-validation="assessmentBank" style="height: 0px; width: 0px; visibility: hidden; " />
                                        </div>
                                        <?php if($aq == count($assessment['question'])-1):?>
                                        <div class="col-xs-1"><a href="javascript:"><img src="<?php echo base_url(); ?>/theme/firstJob/image/plus.png" class="plus_sep add-more-assessment-question"></a></div>
                                        <?php endif;?>
                                    </div>
                                <?php 
                                    $aq++; 
                                endforeach;     
                            else:
                                $i = 0;
                                foreach ($assessmentQuestions as $assessmentQuestion): ?>
                                <div class="form-group">
                                    <label for="inputPassword" class="control-label col-xs-2 rsp_padd15">Question</label>
                                    <div class="col-xs-5 rsp_width_100">
                                        <label for="inputPassword" class="control-label no_padding_left">Number of Question</label>
                                        <div class="clearfix"></div>
                                        <input type="text" class="form-control assessmentQuestion" placeholder="" name="question[<?php echo $i;?>][number]" value="<?php echo $assessmentQuestion['totalQuestion'];?>">
                                        <input type="text" data-validation="assessmentQuestion" style="height: 0px; width: 0px; visibility: hidden; " />
                                    </div>

                                    <div class="col-xs-4 rsp_width_100">
                                        <label for="inputPassword" class="control-label no_padding_left">From Question Bank</label>
                                        <div class="clearfix"></div>
                                        <select class="selectpicker assessmentBank" name="question[<?php echo $i;?>][questionBank]">
                                            <?php foreach ($questionBanks as $item): ?>
                                                <option value="<?php echo $item['id']; ?>" <?php if($assessmentQuestion['questionBank'] == $item['id']) echo "selected";?>><?php echo $item['name']; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                        <input type="text" data-validation="assessmentBank" style="height: 0px; width: 0px; visibility: hidden; " />
                                    </div>
                                    <?php //if($i == 0):?>
                                    <!--<div class="col-xs-1"><a href="javascript:"><img src="<?php echo base_url(); ?>/theme/firstJob/image/plus.png" class="plus_sep add-more-assessment-question"></a></div>-->
                                    <?php //else: ?>
                                    <div class="col-xs-1"><button type="button" class="del-assessment" value="testing">x</button></div>
                                    <?php //endif;?>
                                </div>
                                <?php 
                                $i++;
                                endforeach;
                            endif;
                        }
                        ?>
                    </div>

                    <div class="clearfix">

                        <div class="col-xs-offset-2 col-xs-10 no_padding">
                            <button type="submit" class="Save_frm pull-right">SAVE</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /#page-content-wrapper -->
</div>









<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script type="text/javascript">
    

    // Add validator For Interview Assessment
    $.formUtils.addValidator({
        name: 'assessmentQuestion',
        validatorFunction: function (value, $el, config, language, $form) {      
            var hasNoValue;
            var durationExceed;
            $('.assessmentQuestion').each(function(i) {
                if ($(this).val() == '') {
                      hasNoValue = true;
                }
                else {
                    if (parseInt($(this).val())>600 || isNaN($(this).val())) {
                        durationExceed = true;
                    }
                }
            });
            if (hasNoValue==true || durationExceed==true) {
                return false;
            }
            else {
                return true;
            }
            //return parseInt(value, 10) % 2 === 0;
        },
        errorMessage : 'All number of question in digits & must be field '
    });

    // Add validator For Interview Assessment
    $.formUtils.addValidator({
        name: 'assessmentBank',
        validatorFunction: function (value, $el, config, language, $form) {      
            var hasNoValue;
            $('.assessmentBank').each(function(i) {
                if ($(this).val() == '') {
                      hasNoValue = true;
                }
            });
            if (hasNoValue) {
                return false;
            }
            else {
                return true;
            }
            //return parseInt(value, 10) % 2 === 0;
        },
        errorMessage : 'All duration (limit 600 sec) fields must be field '
    });

    $.validate({
        form: '#assessmentForm',
        modules: 'file',
        onError: function ($form) {
            //alert($('.level1').val());
            return false;
        },
        onSuccess: function ($form) {
            return true;
        },
        validateOnBlur : false,
        errorMessagePosition : 'top',
        scrollToTopOnError : false // Set this property to true on longer forms
    });
</script>
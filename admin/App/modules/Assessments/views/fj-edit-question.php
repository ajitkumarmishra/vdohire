<?php
if (isset($questions) && $questions != NULL) {
    //echo "<pre>"; print_r($questions);
    /* die; */
}
?>
<?php
if ($questions['type'] == 1) {
    $type = 'video';
} else {
    $type = 'audio';
}
?>
<div id="page-content-wrapper">
    <?php if (isset($message)): ?>
        <div class="alert alert-success">
            <?php echo $message; ?>
        </div>
    <?php endif; ?>
    <?php if (isset($error)): ?>
        <div class="alert alert-danger">
            <?php foreach ($error as $item): ?>
                <?php echo $item; ?>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
    <div class="container-fluid whitebg">
        <div class="row">

            <div class="col-md-12 padd_tp_bt_20">
                <h4><img src="<?php echo base_url(); ?>/theme/firstJob/image/arrow_head.png" class="prev-page"> Edit Question</h4>
                <form class="form-horizontal form_save" method="post" action="" enctype="multipart/form-data">
                    <div id ="questionList">
                        <div class="form-group">
                            <!--                            <label for="inputPassword" class="control-label col-xs-2 required-field">Question</label>-->
                            <div class="col-xs-2 ques-title">
                                <label for="inputPassword" class="control-label frm_in_padd">Type</label>
                                <select class="selectpicker" name="question[type]" id="type" disabled="" ><option value="">Select</option>
                                    <option value="1" <?php if (isset($questions['type']) && $questions['type'] == 1) echo "selected"; ?>>Video</option>
                                    <option value="2" <?php if (isset($questions['type']) && $questions['type'] == 2) echo "selected"; ?>>Audio</option>
                                    <option value="3" <?php if (isset($questions['type']) && $questions['type'] == 3) echo "selected"; ?>>Text</option>
                                </select>
                                <input type="hidden" name="question[type]" value="<?php echo $questions['type'] ?>"/>
                            </div>


                            <div class="col-xs-4 ques-title">
                                <label for="inputPassword" class="control-label frm_in_padd">Title</label>
                                <select data-placeholder="Type a question" style="width: 350px; display: none;" class="chosen-select sel_sty_padd" tabindex="-1" name="question[title]">
                                    <option value=""></option>
                                    <?php foreach ($fjQuestion as $item): ?>
                                        <option <?php if (isset($questions['title']) && $questions['title'] == $item['title']) echo "selected"; ?>><?php echo $item['title']; ?></option>
                                    <?php endforeach; ?>

                                </select>
                            </div>

                            <?php if ($interview['type'] != 3): ?>
                                <div class="col-xs-3 file-section">
                                    <div class="col-md-12">
                                        <label for="inputPassword" class="control-label frm_in_padd">File</label>
                                    </div>
                                    <div class="col-xs-8">
                                        <input type="text" class="form-control file-name">
                                    </div>
                                    <div class="col-xs-4 no_padding browse-section">
                                        <button type="button" class="btn_upload">BROWSE</button>
                                        <input type="file" name="file_0" class="my-question-file" />
                                    </div>
                                    <br/>
                                    <div class="question-video">
                                        <div id="questionVideo1" class="question-video" >
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>

                            <div class="col-xs-3">
                                <label for="inputPassword" class="control-label frm_in_padd col-xs-12">Answer Duration</label>
                                <div class="col-md-6 no_padding">
                                    <input type="text" class="form-control" placeholder="" name="question[duration]" value="<?php if (isset($questions['duration'])) echo $questions['duration']; ?>">
                                </div>
                                <div class="col-md-6">
                                    <p class="padd_txtmin">Sec 
<!--                                        <a href="javascript:"><img src="<?php echo base_url(); ?>/theme/firstJob/image/plus.png" class="pad_rg_lfs add-more-my-question">
                                        </a>-->
                                    </p>
                                </div>  
                            </div> 
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="col-xs-offset-2 col-xs-10 no_padding">

                        <input type="hidden" name="accessToken" value="<?php echo $token; ?>"/>
                        <button type="submit" class="Save_frm pull-right save-interview" value="testing">SAVE</button>

                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url(); ?>theme/firstJob/js/chosen.jquery.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>theme/firstJob/js/prism.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">

    var config = {
        '.chosen-select': {},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: ''},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }

    $(function () {
        $("#datepicker-19").datepicker();
    });
    $(document).on('blur', '.chosen-search input', function (evt) {
        //alert($('.no-results').length);
        if ($('.no-results').length == 1) {
            //alert($('.chosen-choices .search-field input').val());
            //$('.chosen-select');
            $(this).parents('.ques-title').find('.chosen-select').append('<option>' + $(evt.target).val() + '</option>');
            $(this).parents('.ques-title').find('.chosen-select').val($(evt.target).val());
            $(this).parents('.ques-title').find('.chosen-select').trigger('chosen:updated');

        }

    });

    window.onload = function () {
        var videoUrl = "uploads/questions/<?php echo $type ?>/<?php echo $questions['file'] ?>";
       // alert(videoUrl);
        jwVideo(videoUrl, 'questionVideo1');
    }


</script>
<style>
    .no-results {
        display: none!important;
    }
    .chosen-single {
        background :none!important;
        border-radius:0px !important;
        height: 34px!important;
    }
</style>

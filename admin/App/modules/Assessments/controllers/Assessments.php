<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Assessments Controller
 * Description : Used to handle Assessment related functionality like add assessment, edit assessment, add question bank, add question etc
 * @author : Synergy
 * @createddate : Dec 1, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 15, 2017
 */
class Assessments extends MY_Controller {

    /**
     * Responsable for auto load the the assessment_model Model
     * Responsable for auto load the the form_validation, session, email library
     * Responsable for auto load the the fj helper
     * Check Uri Segment to display specific page role wise
     * @return void
     */
    function __construct() {
        parent::__construct();
        $this->load->model('assessment_model');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('email');
        $this->load->helper('fj');
        $token = $this->config->item('accessToken');
        

        /**********Check any type of fraund activity*******/
        if ($this->session->userdata('logged_in')) {
            $userRealData   = $this->session->userdata['logged_in'];
            $uriStringSegment = $this->uri->uri_string();

            $arraySegments = explode('/', $uriStringSegment);
            array_pop($arraySegments);
            $newString = implode('/', $arraySegments);
            
            if($userRealData->role == 1) {
                $portal = 'SuperAdminPortal';
            }
            elseif ($userRealData->role == 2 || $userRealData->role == 4 || $userRealData->role == 5) {
                $portal = 'CorporatePortal';
            }
            else{
                $portal = '';
            }

            if($uriStringSegment == 'assessments/list') {
                if(!checkUserAccessForWebPage($userRealData->id, $userRealData->role, $portal, 'assessments/list')) {
                    $this->session->sess_destroy();
                    $insertFaultData['userId'] = $userRealData->id;
                    $insertFaultData['comment'] = 'This user try to access this this page';
                    $insertFaultData['pagePath'] = 'assessments/list';
                    $this->db->insert('fj_user_access_log', $insertFaultData);
                    redirect(base_url('users/login'));
                }
            }
            elseif($uriStringSegment == 'assessments/add') {
                if(!checkUserAccessForWebPage($userRealData->id, $userRealData->role, $portal, 'assessments/add')) {
                    $this->session->sess_destroy();
                    $insertFaultData['userId'] = $userRealData->id;
                    $insertFaultData['comment'] = 'This user try to access this this page';
                    $insertFaultData['pagePath'] = 'assessments/add';
                    $this->db->insert('fj_user_access_log', $insertFaultData);
                    redirect(base_url('users/login'));
                }
            }
            elseif($uriStringSegment == 'questionbank/add') {
                if(!checkUserAccessForWebPage($userRealData->id, $userRealData->role, $portal, 'questionbank/add')) {
                    $this->session->sess_destroy();
                    $insertFaultData['userId'] = $userRealData->id;
                    $insertFaultData['comment'] = 'This user try to access this this page';
                    $insertFaultData['pagePath'] = 'questionbank/add';
                    $this->db->insert('fj_user_access_log', $insertFaultData);
                    redirect(base_url('users/login'));
                }
            }
            elseif($newString == 'assessments/edit') {
                if(!checkUserAccessForWebPage($userRealData->id, $userRealData->role, $portal, 'assessments/edit')) {
                    $this->session->sess_destroy();
                    $insertFaultData['userId'] = $userRealData->id;
                    $insertFaultData['comment'] = 'This user try to access this this page';
                    $insertFaultData['pagePath'] = 'assessments/edit';
                    $this->db->insert('fj_user_access_log', $insertFaultData);
                    redirect(base_url('users/login'));
                }
            }
            elseif($newString == 'questionbank/edit') {
                if(!checkUserAccessForWebPage($userRealData->id, $userRealData->role, $portal, 'questionbank/edit')) {
                    $this->session->sess_destroy();
                    $insertFaultData['userId'] = $userRealData->id;
                    $insertFaultData['comment'] = 'This user try to access this this page';
                    $insertFaultData['pagePath'] = 'questionbank/edit';
                    $this->db->insert('fj_user_access_log', $insertFaultData);
                    redirect(base_url('users/login'));
                }
            }
        } 
        /****************End of handle fraud activity****************/
    }

    function getQuestionOfQuestionBank() {
        $questionBankId = $this->input->post('questionBankId', true);
        $selectedQuestion = $this->input->post('selectedQuestion', true);
        $questions = getQuestionOfQuestionBank($questionBankId);
        
        echo json_encode(array('questions' => $questions, 'selectedQuestion' => $selectedQuestion));exit;
        //echo json_encode(array('questions' => $questions));exit;
    }

    function getQuestionOfQuestionBankCount() {
        $questionBankId = $this->input->post('questionBankId', true);
        $questions = getQuestionOfQuestionBank($questionBankId);
        
        echo json_encode(array('questionCount' => count($questions)));exit;
        //echo json_encode(array('questions' => $questions));exit;
    }

    /**
     * Description : Add new assessment and add Questions(Number of questions from Question Bank)
     * Author : Synergy
     * @param array(question, name, duration)
     * @return void
     */
    function addAssessment() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $userData = $this->session->userdata['logged_in'];
        $userId = $userData->id;
        $info['main_content'] = 'fj-create-assessment';
        $info['questionBankId'] = $id;
        $info['questionBankType'] = $type;
        $info['questionBanks'] = listQuestionBank();
        if (isset($_POST) && $_POST != NULL) {
            $questions = $this->input->post('question', true);

            $name = $this->input->post('name', true);
            $duration = $this->input->post('duration', true);
            $errorQb = '';
            $qn = 1;
            foreach ($questions as $itemQ) {
                $resultQb = getQuestionOfQuestionBank($itemQ['questionBank']);
                if (!checkAtLeastOneQuestionAssessment($questions)) {
                    $errorQb .= '<p>Please select questions from at least one question bank.</p>';
                } else if (count($resultQb) == 0) {
                    $errorQb .= '<p> Question Bank selected in question ' . $qn . ' have no questions. Please add question to selected question Bank.</p>';
                } else if (count($resultQb) < $itemQ['number']) {
                    $errorQb .= '<p> Question Bank selected in question ' . $qn . ' do not have ' . $itemQ['number'] . ' questions. Please add question to selected question Bank.</p>';
                } else if (!checkAtLeastOneQuestionAssessment($questions)) {
                    $errorQb .= '<p>Please select questions from at least one question bank.</p>';
                }
                if (!$itemQ['number']) {
                    $errorQb .= '<p>Please select no of questions of question ' . $qn . '</p>';
                }
                $qn++;
            }
            $this->form_validation->set_rules('name', 'Name', 'trim|required');
            $this->form_validation->set_rules('duration', 'Duration', 'trim|required');
            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }

            if ($errorQb) {
                $resp['error'] .= $errorQb;
            }
            if (!$resp['error']) {
                $assessmentData['name'] = $name;
                $assessmentData['duration'] = $duration * 60;
                $assessmentData['createdAt'] = date('Y-m-d h:i:s');
                $assessmentData['updatedAt'] = date('Y-m-d h:i:s');
                $assessmentData['createdBy'] = $userId;
                $assessmentId = $this->assessment_model->addAssessment($assessmentData);
                foreach ($questions as $item) {
                    $number = $item['number'];
                    $questionBank = $item['questionBank'];
                    $qb = getQuestionOfQuestionBank($questionBank);
                    $results = $this->assessment_model->getRandomQuestionFromQuestionBank($number, $questionBank, count($qb));
                    $assessmentQuestion['assessmentId'] = $assessmentId;
                    foreach ($results as $item) {
                        $assessmentQuestion['questionId'] = $item['id'];
                        $this->assessment_model->addAssessmentQuestion($assessmentQuestion);
                    }
                }
                redirect(base_url('assessments/list/1'));
            } else {
                $info['error'] = $resp;
                $info['assessment'] = $this->input->post();
            }
        }
        $this->load->view('fj-mainpage', $info);
    }

    /**
     * Description : Edit existing assessment and edit Questions(Number of questions from Question Bank)
     * Author : Synergy
     * @param array(id(assessmentId),question, name, duration)
     * @return void
     */
    function editAssessment($id) {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $userData   = $this->session->userdata['logged_in'];        
        $inviteFrom = $this->input->post('inviteFrom', true);        
        $userId     = $userData->id;
        $info['main_content']           = 'fj-edit-assessment';
        $info['questionBanks']          = listQuestionBank();
        $info['assessment']             = $this->assessment_model->getAssessment($id);
        $info['oldAssessment']          = $this->assessment_model->getAssessment($id);
        $info['assessmentQuestions']    = $this->assessment_model->getAssessmentQuestions($id);

        if (isset($_POST) && $_POST != NULL) {
            $questions  = $this->input->post('question', true);
            $name       = $this->input->post('name', true);
            $duration   = $this->input->post('duration', true);
            $errorQb    = '';
            
            if(!array_key_exists('question', $_POST)) {
                $resp['error'] .= '<p>Please select at least one question bank.</p>';
            }
            $qn         = 1;
            foreach ($questions as $itemQ) {
                $resultQb = getQuestionOfQuestionBank($itemQ['questionBank']);
                if (!checkAtLeastOneQuestionAssessment($questions)) {
                    $errorQb .= '<p>Please select questions from at least one question bank.</p>';
                } else if (count($resultQb) == 0) {
                    $errorQb .= '<p> Question Bank selected in question ' . $qn . ' have no questions. Please add question to selected question Bank.</p>';
                } else if (count($resultQb) < $itemQ['number']) {
                    $errorQb .= '<p> Question Bank selected in question ' . $qn . ' do not have ' . $itemQ['number'] . ' questions. Please add question to selected question Bank.</p>';
                } else if (!$itemQ['number']) {
                    $errorQb .= '<p>Please select no of questions of question ' . $qn . '</p>';
                }
                $qn++;
            }
            $this->form_validation->set_rules('name', 'Name', 'trim|required');
            $this->form_validation->set_rules('duration', 'Duration', 'trim|required');
            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }

            if ($errorQb) {
                $resp['error'] .= $errorQb;
            }
            
            if (!$resp['error']) {
                $assessmentData['name']         = $name;
                $assessmentData['duration']     = $duration * 60;
                $assessmentData['createdAt']    = date('Y-m-d h:i:s');
                $assessmentData['updatedAt']    = date('Y-m-d h:i:s');
                $assessmentData['createdBy']    = $userId;
                $assessmentId                   = $this->assessment_model->updateAssessment($assessmentData, $id);
                $this->assessment_model->deleteAssessmentQuestions($id);
                foreach ($questions as $item) {
                    $number         = $item['number'];
                    $questionBank   = $item['questionBank'];
                    $qb             = getQuestionOfQuestionBank($questionBank);
                    $results        = $this->assessment_model->getRandomQuestionFromQuestionBank($number, $questionBank, count($qb));
                    $assessmentQuestion['assessmentId']     = $assessmentId;
                    foreach ($results as $item) {
                        $assessmentQuestion['questionId']   = $item['id'];
                        $this->assessment_model->addAssessmentQuestion($assessmentQuestion);
                    }
                }
                redirect(base_url('assessments/list/1'));
            } 
            else {
                $info['error']          = $resp;
                $info['assessment']     = $this->input->post();
                $info['oldAssessment']  = $this->assessment_model->getAssessment($id);
            }
        }
        $this->load->view('fj-mainpage', $info);
    }

    /**
     * Description : Get details of assessment and load assessment details in fj-mainpage view
     * Author : Synergy
     * @param int $id(assessmentId)
     * @return void
     */
    function getAssessment($id) {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $userData = $this->session->userdata['logged_in'];
        $userId = $userData->id;
        $info['main_content'] = 'fj-view-assessment';
        $info['questionBanks'] = listQuestionBank();
        $info['assessment'] = $this->assessment_model->getAssessment($id);
        $info['assessmentQuestions'] = $this->assessment_model->getAssessmentQuestions($id);
        $this->load->view('fj-mainpage', $info);
    }

    /**
     * Description : Simply displays add Question Bank page and if isset($_POST) creates a new question bank.
     * Author : Synergy
     * @param name, type
     * @return string the success message | error message.
     */
    function addQuestionBank() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token = $this->config->item('accessToken');
        $userData = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            
        }
        $info['main_content'] = 'fj-create-question-bank';
        $info['token'] = $token;

        if (isset($_POST) && $_POST != NULL) {
            $this->form_validation->set_rules('name', 'Name', 'trim|required');
            $this->form_validation->set_rules('type', 'Type', 'trim|required');

            $questionType = $this->input->post('type', true);
            $data['type'] = $this->input->post('type', true);

            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }
            if (!$resp['error']) {   
                
                $data['name'] = $this->input->post('name', true);
                $data['createdAt'] = date('Y-m-d h:i:s');
                $data['updatedAt'] = date('Y-m-d h:i:s');
                $data['createdBy'] = $userId;

                try {
                    $questionBankIdValue = $this->assessment_model->addQuestionBank($data);
                    if ($questionBankIdValue) {
                        
                        if( $questionType == 3 )
                        {   
                            if ( isset($_FILES["questionFile"])) {
                                
                                $textQuestionCsvPath = $this->config->item('textQuestionCsvPath');
                                
                                //if there was an error uploading the file
                                if ($_FILES["questionFile"]["error"] > 0) {
                                    $info['message'] = "Return Code: " . $_FILES["questionFile"]["error"];
                                    redirect(base_url('assessments/mylist/1'));
                                    $this->load->view('fj-mainpage', $info);
                                } else {    
                                    //Store file in directory "upload" with the name of "uploaded_file.txt"
                                    $typeArr = explode('.', $_FILES['questionFile']['name']);
                                    $fileType = array('csv');
                                    $path = $textQuestionCsvPath . "questionFile_" . time() . "_" .  $userData->id . '.' . $typeArr[1];
                                    
                                    
                                    if (in_array($typeArr[1], $fileType)) {
                                        $fileuploaded = move_uploaded_file($_FILES["questionFile"]["tmp_name"], $path);
                                        
                                        
                                        if( $fileuploaded ){
                                            $file = fopen($path, "r");
                                            
                                            $i = 0;
                                            while (!feof($file)) {
                                                $fileData = fgetcsv($file);
                                               
                                                if ($i != 0) {
                                                    $fileArr[] = $fileData;
                                                } else {
                                                    if (strtolower($fileData[0]) != 'question') {
                                                        $info['message'] = "CSV file format is not valid";
                                                        redirect(base_url('assessments/mylist/1'));
                                                        $this->load->view('fj-mainpage', $info);
                                                    }
                                                }
                                                $i++;
                                            }   
                                            
                                            foreach ($fileArr as $item) {
                                                
                                                $questionTitle = $item[0];
                                                
                                                if( $questionTitle != "" ) {
                                                    $itemvalue['title'] = $questionTitle;
                                                    $itemvalue['IsMultipleChoice'] = '1';
                                                    $itemvalue['type'] = $questionType;
                                                    $itemvalue['status'] = 1;
                                                    $itemvalue['createdAt'] = date('Y-m-d h:i:s');
                                                    $itemvalue['updatedAt'] = date('Y-m-d h:i:s');
                                                    $itemvalue['createdBy'] = $userData->id;

                                                    if( $questionTitle != "" ){
                                                        $questionId = $this->assessment_model->addQuestion($itemvalue);

                                                        //Add multiple choice items
                                                        $optionData['questionId'] = $questionId;

                                                        $optionData['createdAt'] = date('Y-m-d h:i:s');
                                                        $optionData['updatedAt'] = date('Y-m-d h:i:s');

                                                        for( $j = 1; $j<=4; $j++ ){
                                                            $optionData['option'] = $item[$j];
                                                            if ($j == $item[5]) {
                                                                $optionData['isCorrect'] = '1';
                                                            } else {
                                                                $optionData['isCorrect'] = '0';
                                                            }
                                                            $optionId = $this->assessment_model->addMultipleChoiceAnswer($optionData);
                                                            if ($optionData['isCorrect'] == '1') {
                                                                $correctOption = $optionId;
                                                            }
                                                        }

                                                        //update question
                                                        $updatequestion['answer'] = $correctOption;
                                                        $this->assessment_model->updateQuestion($updatequestion, $questionId);

                                                        //Add question to question bank
                                                        $questionBankData['questionId'] = $questionId;
                                                        $questionBankData['questionBank'] = $questionBankIdValue;
                                                        $questionBankData['createdAt'] = date('Y-m-d h:i:s');
                                                        $questionBankData['updatedAt'] = date('Y-m-d h:i:s');
                                                        $this->assessment_model->addQuestionToQuestionBank($questionBankData);
                                                    }
                                                }
                                                
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        $info['message'] = 'Successfully created!';
                        redirect(base_url('assessments/mylist/1'));
                        $this->load->view('fj-mainpage', $info);
                    }
                } catch (Exception $e) {
                    $resp['error'] = $e->getMessage();
                }
            } else {
                $info['error'] = $resp;
                $info['questionBank'] = $this->input->post();
                $this->load->view('fj-mainpage', $info);
            }
        } else {
            $this->load->view('fj-mainpage', $info);
        }
    }

    /**
     * Discription : Simply displays Edit Question Bank page and if isset($_POST) update existing question bank.
     * Author Synergy
     * @param name, type
     * @return string the success message | error message.
     */
    function getQuestionBankDetails() {
        $questionBankId = $this->input->post('questionBankId', true);
        $questionBank = $this->assessment_model->getQuestionBank($questionBankId);
        echo json_encode(array('questionBank' => $questionBank));
    }

    /**
     * Discription : Simply displays Edit Question Bank page and if isset($_POST) update existing question bank.
     * Author Synergy
     * @param name, type
     * @return string the success message | error message.
     */
    function editQuestionBank($id) {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token = $this->config->item('accessToken');
        $userData = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            redirect(base_url('users/login'));
        }
        $questionBank = $this->assessment_model->getQuestionBank($id);
        $info['main_content'] = 'fj-edit-question-bank';
        $info['token'] = $token;
        $info['questionBank'] = $questionBank;
        if (isset($_POST) && $_POST != NULL) {
            $this->form_validation->set_rules('name', 'Set Name', 'trim|required');
            $this->form_validation->set_rules('type', 'Type', 'trim|required');
            $data['type'] = $this->input->post('type', true);
            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }
            if (!$resp['error']) {
                $data['name'] = $this->input->post('name', true);
                $data['createdAt'] = date('Y-m-d h:i:s');
                $data['updatedAt'] = date('Y-m-d h:i:s');
                $data['createdBy'] = $userId;
                try {
                    $resp = $this->assessment_model->updateQuestionBank($data, $id);
                    if ($resp) {
                        
                        $questionBankIdValue = $id;
                        $questionType = $this->input->post('type', true);
                                
                        if( $questionType == 3 )
                        {   
                            if ( isset($_FILES["questionFile"])) {
                                
                                $textQuestionCsvPath = $this->config->item('textQuestionCsvPath');
                                
                                //if there was an error uploading the file
                                if ($_FILES["questionFile"]["error"] > 0) {
                                    $info['message'] = "Return Code: " . $_FILES["questionFile"]["error"];
                                    redirect(base_url('assessments/mylist/1'));
                                    $this->load->view('fj-mainpage', $info);
                                } else {    
                                    //Store file in directory "upload" with the name of "uploaded_file.txt"
                                    $typeArr = explode('.', $_FILES['questionFile']['name']);
                                    $fileType = array('csv');
                                    $path = $textQuestionCsvPath . "questionFile_" . time() . "_" .  $userData->id . '.' . $typeArr[1];
                                    
                                    
                                    if (in_array($typeArr[1], $fileType)) {
                                        $fileuploaded = move_uploaded_file($_FILES["questionFile"]["tmp_name"], $path);
                                        
                                        
                                        if( $fileuploaded ){
                                            $file = fopen($path, "r");
                                            
                                            $i = 0;
                                            while (!feof($file)) {
                                                $fileData = fgetcsv($file);
                                               
                                                if ($i != 0) {
                                                    $fileArr[] = $fileData;
                                                } else {
                                                    if (strtolower($fileData[0]) != 'question') {
                                                        $info['message'] = "CSV file format is not valid";
                                                        redirect(base_url('assessments/mylist/1'));
                                                        $this->load->view('fj-mainpage', $info);
                                                    }
                                                }
                                                $i++;
                                            }   
                                            
                                            foreach ($fileArr as $item) {
                                                
                                                $questionTitle = $item[0];
                                                
                                                if( $questionTitle != "" ) {
                                                    $itemvalue['title'] = $questionTitle;
                                                    $itemvalue['IsMultipleChoice'] = '1';
                                                    $itemvalue['type'] = $questionType;
                                                    $itemvalue['status'] = 1;
                                                    $itemvalue['createdAt'] = date('Y-m-d h:i:s');
                                                    $itemvalue['updatedAt'] = date('Y-m-d h:i:s');
                                                    $itemvalue['createdBy'] = $userData->id;

                                                    if( $questionTitle != "" ){
                                                        $questionId = $this->assessment_model->addQuestion($itemvalue);

                                                        //Add multiple choice items
                                                        $optionData['questionId'] = $questionId;

                                                        $optionData['createdAt'] = date('Y-m-d h:i:s');
                                                        $optionData['updatedAt'] = date('Y-m-d h:i:s');

                                                        for( $j = 1; $j<=4; $j++ ){
                                                            
                                                            $optionData['option'] = $item[$j];
                                                            if ($j == $item[5]) {
                                                                $optionData['isCorrect'] = '1';
                                                            } else {
                                                                $optionData['isCorrect'] = '0';
                                                            }
                                                            
                                                            $optionId = $this->assessment_model->addMultipleChoiceAnswer($optionData);
                                                            if ($optionData['isCorrect'] == '1') {
                                                                $correctOption = $optionId;
                                                            }
                                                        }

                                                        //update question
                                                        $updatequestion['answer'] = $correctOption;
                                                        $this->assessment_model->updateQuestion($updatequestion, $questionId);

                                                        //Add question to question bank
                                                        $questionBankData['questionId'] = $questionId;
                                                        $questionBankData['questionBank'] = $questionBankIdValue;
                                                        $questionBankData['createdAt'] = date('Y-m-d h:i:s');
                                                        $questionBankData['updatedAt'] = date('Y-m-d h:i:s');
                                                        $this->assessment_model->addQuestionToQuestionBank($questionBankData);
                                                    }
                                                }
                                                
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        
                        $f = 0;
                        $info['message'] = 'Successfully created!';
                        redirect(base_url('assessments/mylist/1'));
                        $this->load->view('fj-mainpage', $info);
                    }
                } catch (Exception $e) {
                    $resp['error'] = $e->getMessage();
                }
            } else {
                $info['error'] = $resp;
                $info['assessment'] = $this->input->post();
                $this->load->view('fj-mainpage', $info);
            }
        } else {
            $this->load->view('fj-mainpage', $info);
        }
    }

    /**
     * Description : Simply displays add Question Bank page and if isset($_POST) creates a new question bank.
     * Author : Synergy
     * @param name, type
     * @return string the success message | error message.
     */
    function updateAssessmentQuestionBank() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }

        $token = $this->config->item('accessToken');
        $userData = $this->session->userdata['logged_in'];

        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            $userId = '';
        }

        $errorMessage = '';
        $noOfQuestions = 0;

        if (isset($_POST) && $_POST != NULL) {
            $questionBankId = $this->input->post('hiddenQuestionBankId', true);

            $value1 = $this->input->post('hiddenQuestionBankType', true);
            $value2 = $this->input->post('assessmenQquestionType', true);
            if($value1) {
                $questionType = $value1;
            } else {
                $questionType = $value2;
            }

            $data['name'] = $this->input->post('assessmentQuestionBankname', true);
            $data['type'] = $questionType;
            $data['updatedAt'] = date('Y-m-d h:i:s');
            $data['createdBy'] = $userId;
            $data['updatedBy'] = $userId;

            try {
                //$questionBankIdValue = $this->assessment_model->addQuestionBank($data);
                $questionBankIdValue = $this->assessment_model->updateQuestionBank($data, $questionBankId);

                if ($questionBankIdValue) {
                    
                    if( $questionType == 3 )
                    {   
                        if ( isset($_FILES["assessmentQuestionFile"]['name']) && !empty($_FILES["assessmentQuestionFile"]['name'])) {
                            
                            $textQuestionCsvPath = $this->config->item('textQuestionCsvPath');
                            
                            //if there was an error uploading the file
                            if ($_FILES["assessmentQuestionFile"]["error"] > 0) {
                                $errorMessage = "Your Question Bank has been created but we get error message: " . $_FILES["assessmentQuestionFile"]["error"];
                                $returnData = array('questionBankId' => $questionBankIdValue, 'noOfQuestions' => $noOfQuestions, 'errorMessage' => $errorMessage, 'questionType' => $questionType, 'allAssessmentQuestions' => array());
                                echo json_encode($returnData);exit;

                            } else {  
                                //Store file in directory "upload" with the name of "uploaded_file.txt"
                                $typeArr = explode('.', $_FILES['assessmentQuestionFile']['name']);
                                $fileType = array('csv');
                                $path = $textQuestionCsvPath . "questionFile_" . time() . "_" .  $userData->id . '.' . $typeArr[1];
                                
                                
                                if (in_array($typeArr[1], $fileType)) {
                                    $fileuploaded = move_uploaded_file($_FILES["assessmentQuestionFile"]["tmp_name"], $path);
                                    
                                    
                                    if( $fileuploaded ){
                                        $file = fopen($path, "r");
                                        
                                        $i = 0;
                                        while (!feof($file)) {
                                            $fileData = fgetcsv($file);
                                           
                                            if ($i != 0) {
                                                $fileArr[] = $fileData;
                                            } else {
                                                if (strtolower($fileData[0]) != 'question') {
                                                    $errorMessage = "Your Question Bank has been created but CSV file format is not valid.";
                                                    $returnData = array('questionBankId' => $questionBankIdValue, 'noOfQuestions' => $noOfQuestions, 'errorMessage' => $errorMessage, 'questionType' => $questionType, 'allAssessmentQuestions' => array());
                                                    echo json_encode($returnData);exit;
                                                }
                                            }
                                            $i++;
                                        }   
                                        
                                        $allAssessmentQuestions = array();

                                        foreach ($fileArr as $item) {
                                            $questionTitle = $item[0];
                                            
                                            if( $questionTitle != "" ) {
                                                $noOfQuestions = $noOfQuestions + 1;

                                                $itemvalue['title'] = $questionTitle;
                                                $itemvalue['IsMultipleChoice'] = '1';
                                                $itemvalue['type'] = $questionType;
                                                $itemvalue['status'] = 1;
                                                $itemvalue['createdAt'] = date('Y-m-d h:i:s');
                                                $itemvalue['updatedAt'] = date('Y-m-d h:i:s');
                                                $itemvalue['createdBy'] = $userData->id;

                                                if( $questionTitle != "" ){
                                                    $questionId = $this->assessment_model->addQuestion($itemvalue);

                                                    //Add multiple choice items
                                                    $optionData['questionId'] = $questionId;

                                                    $optionData['createdAt'] = date('Y-m-d h:i:s');
                                                    $optionData['updatedAt'] = date('Y-m-d h:i:s');

                                                    for( $j = 1; $j<=4; $j++ ){
                                                        $optionData['option'] = $item[$j];
                                                        if ($j == $item[5]) {
                                                            $optionData['isCorrect'] = '1';
                                                        } else {
                                                            $optionData['isCorrect'] = '0';
                                                        }
                                                        $optionId = $this->assessment_model->addMultipleChoiceAnswer($optionData);
                                                        if ($optionData['isCorrect'] == '1') {
                                                            $correctOption = $optionId;
                                                        }
                                                    }

                                                    //update question
                                                    $updatequestion['answer'] = $correctOption;
                                                    $this->assessment_model->updateQuestion($updatequestion, $questionId);

                                                    //Add question to question bank
                                                    $questionBankData['questionId'] = $questionId;
                                                    $questionBankData['questionBank'] = $questionBankIdValue;
                                                    $questionBankData['createdAt'] = date('Y-m-d h:i:s');
                                                    $questionBankData['updatedAt'] = date('Y-m-d h:i:s');
                                                    $this->assessment_model->addQuestionToQuestionBank($questionBankData);

                                                    $allAssessmentQuestion['questionId'] = $questionId;
                                                    $allAssessmentQuestion['quesitonName'] = $questionTitle;
                                                    $allAssessmentQuestions[] = $allAssessmentQuestion;
                                                }
                                            }
                                        }
                                        $returnData = array('questionBankId' => $questionBankIdValue, 'noOfQuestions' => $noOfQuestions, 'errorMessage' => '', 'questionType' => $questionType, 'allAssessmentQuestions' => $allAssessmentQuestions);
                                        echo json_encode($returnData);exit;
                                    } else {
                                        $returnData = array('questionBankId' => $questionBankIdValue, 'noOfQuestions' => $noOfQuestions, 'errorMessage' => 'Your Question Bank has been created but CSV file could not uploaded.', 'questionType' => $questionType, 'allAssessmentQuestions' => array());
                                        echo json_encode($returnData);exit;
                                    }
                                } else {
                                    $returnData = array('questionBankId' => $questionBankIdValue, 'noOfQuestions' => $noOfQuestions, 'errorMessage' => 'Your Question Bank has been created but CSV file format is not valid', 'questionType' => $questionType, 'allAssessmentQuestions' => array());
                                        echo json_encode($returnData);exit;
                                }
                            }
                        } else {
                            $returnData = array('questionBankId' => $questionBankIdValue, 'noOfQuestions' => $noOfQuestions, 'errorMessage' => '', 'questionType' => $questionType, 'allAssessmentQuestions' => array());
                            echo json_encode($returnData);exit;
                        }
                    } else {
                        $returnData = array('questionBankId' => $questionBankIdValue, 'noOfQuestions' => $noOfQuestions, 'errorMessage' => '', 'questionType' => $questionType, 'allAssessmentQuestions' => array());
                        echo json_encode($returnData);exit;
                    }
                } else {
                    $returnData = array('questionBankId' => '', 'noOfQuestions' => $noOfQuestions, 'errorMessage' => 'Sorry! Question Bank could not created.', 'questionType' => $questionType, 'allAssessmentQuestions' => array());
                    echo json_encode($returnData);exit;
                }
            } catch (Exception $e) {
                $returnData = array('questionBankId' => '', 'noOfQuestions' => $noOfQuestions, 'errorMessage' => $e->getMessage(), 'questionType' => $questionType, 'allAssessmentQuestions' => array());
                echo json_encode($returnData);exit;
            }
        }
    }

    /**
     * Discription : Simply displays Add Assessment Question page and if isset($_POST) Add new question .
     * Author Synergy
     * @param id, type
     * @return string the success message | error message.
     */
    function addQuestion($id, $type) {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $userData = $this->session->userdata['logged_in'];
        $userId = $userData->id;
        $info['main_content'] = 'fj-add-assessment-question';
        $info['questionBankId'] = $id;
        $info['questionBankType'] = $type;
        $info['questionBanks'] = listQuestionBank();
        if (isset($_POST) && $_POST != NULL) {
            $questions = $this->input->post('question', true);
            $type = $this->input->post('type', true);
            $this->form_validation->set_rules('type', 'Type', 'trim|required');
            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }
            if ($questions[0]['title'] == '' && $type == 3) {
                $resp['error'] .= '<p>Title field  is required.</p>';
            }

            if ($_FILES['file_0']['name'] == '' && $type != 3) {
                $resp['error'] .= '<p>Please upload file of question.</p>';
            }
                        
            if ($_FILES['file_0']['name'] != '' && $type == 1 && $_FILES['file_0']['type']!='video/mp4') {
                $resp['error'] .= '<p>Please upload mp4 file only.</p>';
            }

            if (!$resp['error']) {
                // Add Question
                $questionData['title']              = $questions[0]['title'];
                $questionData['IsMultipleChoice']   = '1';
                $questionData['createdAt']          = date('Y-m-d h:i:s');
                $questionData['updatedAt']          = date('Y-m-d h:i:s');
                $questionData['createdBy']          = $userId;
                $questionData['type']               = $type;
                $questionData['answer']             = getCorrectAnswer($questions[0]['check']);
                if (isset($_FILES['file_0']) && $_FILES['file_0'] != NULL) {
                    $questionData['file'] = $this->fileUpload($_FILES['file_0'], $type);
                }                
                $questionId = $this->assessment_model->addQuestion($questionData);

                //Add multiple choice items
                $optionData['questionId'] = $questionId;

                $optionData['createdAt'] = date('Y-m-d h:i:s');
                $optionData['updatedAt'] = date('Y-m-d h:i:s');
                foreach ($questions[0]['answer'] as $key => $item) {
                    $optionData['option'] = $item;
                    if ($key == $questionData['answer']) {
                        $optionData['isCorrect'] = '1';
                    } else {
                        $optionData['isCorrect'] = '0';
                    }
                    $optionId = $this->assessment_model->addMultipleChoiceAnswer($optionData);
                    if ($optionData['isCorrect'] == '1') {
                        $correctOption = $optionId;
                    }
                }
                //Add question to question bank
                $questionBankData['questionId']     = $questionId;
                $questionBankData['questionBank']   = $questions[0]['questionBank'];
                $questionBankData['createdAt']      = date('Y-m-d h:i:s');
                $questionBankData['updatedAt']      = date('Y-m-d h:i:s');
                if (isset($questionBankData['questionBank']) && $questionBankData['questionBank'] != NULL) {
                    $resp = $this->assessment_model->addQuestionToQuestionBank($questionBankData);
                }
                $questionUpdate['answer'] = $correctOption; //die();
                $this->assessment_model->updateQuestion($questionUpdate, $questionId);
                redirect(base_url('assessments/mylist/1'));
            } 
            else {
                $info['error'] = $resp;
                $info['assessment'] = $this->input->post();
            }
        }

        $this->load->view('fj-mainpage', $info);
    }

    /**
     * Discription : Simply displays Add Assessment Question page and if isset($_POST) Add new question .
     * Author Synergy
     * @param id, type
     * @return string the success message | error message.
     */
    function addQuestionRecruiter() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }

        $userData = $this->session->userdata['logged_in'];
        $userId = $userData->id;


        if (isset($_POST) && $_POST != NULL) {
            $questions = $this->input->post('question', true);
            $questinName = $questions[0]['title'];
            $type = $this->input->post('hiddenType', true);
            $questionBankId = $this->input->post('questionBankId', true);

            // Add Question
            $questionData['title']              = $questions[0]['title'];
            $questionData['IsMultipleChoice']   = '1';
            $questionData['createdAt']          = date('Y-m-d h:i:s');
            $questionData['updatedAt']          = date('Y-m-d h:i:s');
            $questionData['createdBy']          = $userId;
            $questionData['type']               = $type;
            $questionData['answer']             = getCorrectAnswer($questions[0]['check']);
            if (isset($_FILES['file_0']['name']) && !empty($_FILES['file_0']['name'])) {
                $questionData['file'] = $this->fileUpload($_FILES['file_0'], $type);
            }
            $questionId = $this->assessment_model->addQuestion($questionData);

            //Add multiple choice items
            $optionData['questionId'] = $questionId;

            $optionData['createdAt'] = date('Y-m-d h:i:s');
            $optionData['updatedAt'] = date('Y-m-d h:i:s');
            foreach ($questions[0]['answer'] as $key => $item) {
                $optionData['option'] = $item;
                if ($key == $questionData['answer']) {
                    $optionData['isCorrect'] = '1';
                } else {
                    $optionData['isCorrect'] = '0';
                }
                $optionId = $this->assessment_model->addMultipleChoiceAnswer($optionData);
                if ($optionData['isCorrect'] == '1') {
                    $correctOption = $optionId;
                }
            }
            //Add question to question bank
            $questionBankData['questionId']     = $questionId;
            $questionBankData['questionBank']   = $questionBankId;
            $questionBankData['createdAt']      = date('Y-m-d h:i:s');
            $questionBankData['updatedAt']      = date('Y-m-d h:i:s');
            if (isset($questionBankData['questionBank']) && $questionBankData['questionBank'] != NULL) {
                $resp = $this->assessment_model->addQuestionToQuestionBank($questionBankData);
            }
            $questionUpdate['answer'] = $correctOption; 
            $this->assessment_model->updateQuestion($questionUpdate, $questionId);

            $questionBankDetailss = $this->assessment_model->getQuestionBank($questionBankId);

            echo json_encode(array('questionId' => $questionId, 'quesitonName' => $questinName, 'questionBankName' => $questionBankDetailss['name'], 'errorMessage' => ''));
        } else {
            echo json_encode(array('questionId' => '', 'errorMessage' => 'Invalid attempt.'));
        }
    }

    /**
     * Discription : Simply displays Edit Assessment Question page and if isset($_POST) update existing assessment question.
     * Author Synergy
     * @param $id
     * @return string the success message | error message.
     */
    function getQuestionDetails() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $userData = $this->session->userdata['logged_in'];
        $userId = $userData->id;
        $id = $this->input->post('questionid', true);

        $options = getOptionsofQuestion($id);
        $assessment = $this->assessment_model->getQuestion($id);
        echo json_encode(array('options' => $options, 'assessment' => $assessment));
    }
    /**
     * Discription : Simply displays Edit Assessment Question page and if isset($_POST) update existing assessment question.
     * Author Synergy
     * @param $id
     * @return string the success message | error message.
     */
    function editQuestion($id) {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $userData = $this->session->userdata['logged_in'];
        $userId = $userData->id;
        $info['main_content'] = 'fj-edit-assessment-question';
        $info['options'] = getOptionsofQuestion($id);
        $info['assessment'] = $this->assessment_model->getQuestion($id);
        $info['questionBanks'] = listQuestionBank();
        if (isset($_POST) && $_POST != NULL) {
            $questions = $this->input->post('question', true);
            $type = $this->input->post('type', true);
            $questionFileName = $this->input->post('filename_0', true);
            $this->form_validation->set_rules('type', 'Type', 'trim|required');
            
            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }
            if ($questions[0]['title'] == '' && $type == 3) {
                $resp['error'] .= '<p>Title field  is required.</p>';
            }
            if ($_FILES['file_0']['name'] == '' && $type != 3 && $questionFileName=='') {
                $resp['error'] .= '<p>Please upload file of question.</p>';
            }
                        
            if ($_FILES['file_0']['name'] != '' && $type == 1 && $_FILES['file_0']['type']!='video/mp4') {
                $resp['error'] .= '<p>Please upload mp4 file only.</p>';
            }

            if (!$resp['error']) {

                // Edit Question
                $questionData['title'] = $questions[0]['title'];
                $questionData['IsMultipleChoice'] = '1';
                $questionData['createdAt'] = date('Y-m-d h:i:s');
                $questionData['updatedAt'] = date('Y-m-d h:i:s');
                $questionData['createdBy'] = $userId;
                $questionData['type'] = $type;
                $questionData['answer'] = getCorrectAnswer($questions[0]['check']);
                if (isset($_FILES['file_0']['name']) && $_FILES['file_0']['name'] != NULL) {
                    $questionData['file'] = $this->fileUpload($_FILES['file_0'], $type);
                } else {
                    $questionData['file'] = $this->input->post('filename_0', true);
                }
                $this->assessment_model->deleteQuestionBankQuestionById($id);
                $this->assessment_model->deleteMultipleChoiceByQuestionId($id);
                $questionId = $this->assessment_model->updateQuestion($questionData, $id);

                //Add multiple choice items
                $optionData['questionId'] = $questionId;
                $optionData['createdAt'] = date('Y-m-d h:i:s');
                $optionData['updatedAt'] = date('Y-m-d h:i:s');
                foreach ($questions[0]['answer'] as $key => $item) {
                    $optionData['option'] = $item;
                    if ($key == $questionData['answer']) {
                        $optionData['isCorrect'] = '1';
                    } else {
                        $optionData['isCorrect'] = '0';
                    }
                    $optionId = $this->assessment_model->addMultipleChoiceAnswer($optionData);
                    if ($optionData['isCorrect'] == '1') {
                        $correctOption = $optionId;
                    }
                }

                //Add question to question bank
                $questionBankData['questionId'] = $questionId;
                $questionBankData['questionBank'] = $questions[0]['questionBank'];
                $questionBankData['createdAt'] = date('Y-m-d h:i:s');
                $questionBankData['updatedAt'] = date('Y-m-d h:i:s');
                $resp = $this->assessment_model->addQuestionToQuestionBank($questionBankData);
                $questionUpdate['answer'] = $correctOption;
                $this->assessment_model->updateQuestion($questionUpdate, $questionId);
                redirect(base_url('assessments/mylist/1'));
            } else {
                $info['error'] = $resp;
                $info['assessment'] = $this->input->post();
                $this->load->view('fj-mainpage', $info);
            }
        }

        $this->load->view('fj-mainpage', $info);
    }

    /**
     * Discription : Simply displays Edit Assessment Question page and if isset($_POST) update existing assessment question.
     * Author Synergy
     * @param $id
     * @return string the success message | error message.
     */
    function updateQuestion() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $userData = $this->session->userdata['logged_in'];
        $userId = $userData->id;

        //print '<pre>';print_r($_POST);
        //print '<pre>';print_r($_FILES);exit;
        if (isset($_POST) && $_POST != NULL) {
            $id = $this->input->post('questionId', true);
            $questionBankId = $this->input->post('questionBankId', true);
            $questions = $this->input->post('question', true);
            $questinName = $questions[0]['title'];
            $type = $this->input->post('hiddenType', true);
            $questionFileName = $this->input->post('filename_0', true);
            
            // Edit Question
            $questionData['title'] = $questions[0]['title'];
            $questionData['IsMultipleChoice'] = '1';
            $questionData['updatedAt'] = date('Y-m-d h:i:s');
            $questionData['createdBy'] = $userId;
            $questionData['type'] = $type;
            $questionData['answer'] = getCorrectAnswer($questions[0]['check']);
            if (isset($_FILES['file_0']['name']) && $_FILES['file_0']['name'] != NULL) {
                $questionData['file'] = $this->fileUpload($_FILES['file_0'], $type);
            } else {
                $questionData['file'] = $this->input->post('filename_0', true);
            }
            $this->assessment_model->deleteQuestionBankQuestionById($id);
            $this->assessment_model->deleteMultipleChoiceByQuestionId($id);
            $questionId = $this->assessment_model->updateQuestion($questionData, $id);

            //Add multiple choice items
            $optionData['questionId'] = $questionId;
            $optionData['createdAt'] = date('Y-m-d h:i:s');
            $optionData['updatedAt'] = date('Y-m-d h:i:s');
            foreach ($questions[0]['answer'] as $key => $item) {
                $optionData['option'] = $item;
                if ($key == $questionData['answer']) {
                    $optionData['isCorrect'] = '1';
                } else {
                    $optionData['isCorrect'] = '0';
                }
                $optionId = $this->assessment_model->addMultipleChoiceAnswer($optionData);
                if ($optionData['isCorrect'] == '1') {
                    $correctOption = $optionId;
                }
            }

            //Add question to question bank
            $questionBankData['questionId'] = $questionId;
            $questionBankData['questionBank'] = $questionBankId;
            $questionBankData['updatedAt'] = date('Y-m-d h:i:s');
            $resp = $this->assessment_model->addQuestionToQuestionBank($questionBankData);
            $questionUpdate['answer'] = $correctOption;
            $this->assessment_model->updateQuestion($questionUpdate, $questionId);
            echo json_encode(array('questionId' => $questionId, 'quesitonName' => $questinName, 'errorMessage' => ''));
        } else {
            echo json_encode(array('questionId' => '', 'quesitonName' => '', 'errorMessage' => 'Unauthorised Access!'));
        }
    }

    /**
     * Discription : Uploads assessment question a file like mp4, mp3, mpeg to directory.
     * Author Synergy
     * @param array $files, string $type.
     * @return string the uploaded file or boolean false.
     */
    function fileUpload($files, $type) {
        $typeArr        = explode('/', $files['type']);
        $fileName       = rand() . date('ymdhis') . '_question.' . ($type == 1?'mp4':'mp3');
        $type           = ($type == 1?'video/':'audio/');
        $data['image']  = $fileName;        
        $questionType = array('mp4', 'mp3', 'mpeg');
        if (in_array($typeArr[1], $questionType)) {
            $uploadResp = $this->assessment_model->fileUpload($files, 'uploads/questions/' . $type, 'q'.$fileName);
            return $uploadResp;
        } else {
            return false;
        }
    }

    /**
     * Discription : List all the assessments
     * Author Synergy
     * @param array $page, $_POST.
     * @return string the success message | error message.
     */
    function listAssessments($page) {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        
        //Assessments Listing Start
        $userData = $this->session->userdata['logged_in'];
        $userRole = $userData->role;
        $userLoggedinUserId = $userData->id;
        $data['page'] = $page;
        $resp['error'] = 0;
        $serachColumn = $this->input->post('serachColumn', true);
        $searchText = $this->input->post('serachText', true);
        $token = $this->config->item('accessToken');
        $fromEmail = $this->config->item('fromEmail');
        $itemsPerPage = $this->config->item('itemsPerPage');
        $data['itemsPerPage'] = $itemsPerPage;
        $createdBy = $userData->id;
        if (!$page) {
            $page = 0;
        } else {
            $page = $page - 1;
        }
        $offset = $page * $itemsPerPage;
        try {
            //get all corporate and sub user
            $createdByStr = getAllCorporateAndSubuserIds($userLoggedinUserId, $userRole);
            
            
            $result = $this->assessment_model->listAssessments($itemsPerPage, $offset, $serachColumn, $searchText, $createdByStr);
            $completeResult = $this->assessment_model->listAssessments(NULL, NULL, NULL, NULL, $createdByStr);
            $count = ceil(count($completeResult) / $itemsPerPage);
            $data['count'] = $count;
            $data['totalCount'] = count($completeResult);
            $data['contentAssessmentSet'] = $result;
            $data['main_content'] = 'fj-assessment-listing';
            $data['searchText'] = $searchText;
            $data['serachColumn'] = $serachColumn;
            $data['token'] = $token;

            //List Question Banks
            $resultMyQuestions = $this->assessment_model->listQuestionBank($itemsPerPage, $offset, $serachColumn, $searchText, $createdByStr, $createdByStr, $userRole);
            $completeResultMyQuestions = $this->assessment_model->listQuestionBank(NULL, NULL, $serachColumn, $searchText, $createdByStr, $createdByStr, $userRole);
            $countMy = ceil(count($completeResultMyQuestions) / $itemsPerPage);
            $data['countMy'] = $countMy;
            $data['totalCountMy'] = count($completeResultMyQuestions);
            $data['contentMyQuestionBank'] = $resultMyQuestions;
            
            /* commented due to new functionality
            //List Fj Questions
            $resultFjQuestions = $this->assessment_model->listFjQuestions($itemsPerPage, $offset, $serachColumn, $searchText, $createdBy);
            $completeResultFjQuestions = $this->assessment_model->listFjQuestions(NULL, NULL, NULL, NULL, $createdBy);
            $countFj = ceil(count($completeResultFjQuestions) / $itemsPerPage);
            $data['countFj'] = $countFj;
            $data['totalCountFj'] = count($completeResultFjQuestions);
            $data['contentFjQuestions'] = $resultFjQuestions;
            */
            
            //List Fj Questions
            $resultFjQuestions = $this->assessment_model->listFjQuestionsBank($itemsPerPage, $offset, $serachColumn, $searchText, $createdBy);
            $completeResultFjQuestions = $this->assessment_model->listFjQuestionsBank(NULL, NULL, NULL, NULL, $createdBy);
            $countFj = ceil(count($completeResultFjQuestions) / $itemsPerPage);
            $data['countFj'] = $countFj;
            $data['totalCountFj'] = count($completeResultFjQuestions);
            $data['contentFjQuestionsBank'] = $resultFjQuestions;
            
            $this->load->view('fj-mainpage', $data);
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Discription : Delete the selected question bank
     * Author Synergy
     * @param $_POST.
     * @return string the success message | error message.
     */
    function deleteQuestionBank() {
        $id = $this->input->post('questionbankid', true);
        $data['status'] = 3;
        $this->assessment_model->updateQuestionBank($data, $id);
    }

    /**
     * Discription : Delete the selected question
     * Author Synergy
     * @param $_POST.
     * @return string the success message | error message.
     */
    function deleteQuestion() {
        $id = $this->input->post('questionid', true);
        $data['status'] = 3;
        $this->assessment_model->updateQuestion($data, $id);
    }

    /**
     * Discription : Add new question to assessment set
     * Author Synergy
     * @param $_POST.
     * @return string the success message | error message.
     */
    function addQuestionToAssessmentSet() {
        $questionId = $this->input->post('questionId', true);
        $assessment = $this->input->post('assessment', true);
        try {
            foreach ($assessment as $item) {
                $assessmentQuestion['assessmentId'] = $item;
                $assessmentQuestion['questionId'] = $questionId;
                $assessmentQuestion['createdAt'] = date('Y-m-d h:i:s');
                $assessmentQuestion['updatedAt'] = date('Y-m-d h:i:s');
                $this->assessment_model->addAssessmentQuestion($assessmentQuestion);
                redirect(base_url('assessments/list/1'));
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
     * Discription : Get all the assessment set of a particular type into a dropdown
     * Author Synergy
     * @param $_POST.
     * @return dropdown list
     */
    function getAssessmentSetByType() {
        $type = $this->input->post('type', true);
        $userData = $this->session->userdata['logged_in'];
        $userId = $userData->id;
        $questionId = $this->input->post('questionId', true);
        $results = getAssessmentSetByType($type, $userId);
        $str = '<select data-placeholder="AssessmentSet" style="width: 350px; display: none;" multiple="" class="chosen-select required" tabindex="-1" name="assessment[]" id="assessmentSetsDropDown">';

        foreach ($results as $item) {
            $str .= '<option value="' . $item['id'] . '">' . $item['name'] . '</option>';
        }
        $str .= '</select>';
        $str .= '<input type="hidden" name ="questionId" class= "question-id" value="' . $questionId . '" />';
        echo $str;
        exit;
    }

    /**
     * Discription : It will check if a question exist in a specific assessment or not
     * Author Synergy
     * @param $_POST.
     * @return 0 or 1
     */
    function checkAssessment() {
        $questionId = $this->input->post('questionId', true);
        $assessment = $this->input->post('assessment', true);
        $check = 0;
        foreach ($assessment as $item) {
            if (checkQuestionExistInAssessment($questionId, $item)) {
                $check = 1;
            }
        }
        echo $check;
        exit;
    }

    /**
     * Discription : It will check if a question exist in a specific assessment or not
     * Author Synergy
     * @param $_POST.
     * @return 0 or 1
     */
    function getAssessmentQuestionRow() {
        $index = $this->input->post('index', true);
        $questionBank = listQuestionBank();
        $questionBankOption = '<option value="">-Select Question Bank-</option>';
        foreach ($questionBank as $item) {
            $questionsBanks = getQuestionOfQuestionBank($item['id']);
            $totalQuestionsOfQuestionBank = count($questionsBanks);
            $questionBankOption .= '<option value="' . $item['id'] . '">' . $item['name'] .'('.$totalQuestionsOfQuestionBank. ')</option>';
        }
        $str = getAssessmentQuestionRow($questionBankOption, $index);
        echo $str;
        exit;
    }

    /**
     * Discription : It will check if a question exist in a specific assessment or not
     * Author Synergy
     * @param $_POST.
     * @return 0 or 1
     */
    function getAssessmentQuestionRowEdit() {
        $index = $this->input->post('index', true);
        $questionBank = listQuestionBank();
        $questionBankOption = '<option value="">-Select Question Bank-</option>';
        foreach ($questionBank as $item) {
            $questionsBanks = getQuestionOfQuestionBank($item['id']);
            $totalQuestionsOfQuestionBank = count($questionsBanks);
            $questionBankOption .= '<option value="' . $item['id'] . '">' . $item['name'] .'('.$totalQuestionsOfQuestionBank. ')</option>';
        }
        $str = getAssessmentQuestionRowEdit($questionBankOption, $index);
        echo $str;
        exit;
    }

    /**
     * Discription : It will check if a question exist in a specific assessment or not
     * Author Synergy
     * @param $_POST.
     * @return 0 or 1
     */
    function getAssessmentIndividualQuestionRow() {
        $index = $this->input->post('index', true);
        $questionBank = listQuestionBank();
        $questionBankOption = '<option value="">-Select Question Bank-</option>';
        foreach ($questionBank as $item) {
            $questionBankOption .= '<option value="' . $item['id'] . '">' . $item['name'] . '</option>';
        }
        $str = getAssessmentIndividualQuestionRow($questionBankOption, $index);
        echo $str;
        exit;
    }

    /**
     * Discription : It will check if a question exist in a specific assessment or not
     * Author Synergy
     * @param $_POST.
     * @return 0 or 1
     */
    function getAssessmentIndividualQuestionRowEdit() {
        $index = $this->input->post('index', true);
        $questionBank = listQuestionBank();
        $questionBankOption = '<option value="">-Select Question Bank-</option>';
        foreach ($questionBank as $item) {
            $questionBankOption .= '<option value="' . $item['id'] . '">' . $item['name'] . '</option>';
        }
        $str = getAssessmentIndividualQuestionRowEdit($questionBankOption, $index);
        echo $str;
        exit;
    }

    /**
     * Discription : Deletes an assessment.
     * Author Synergy
     * @param $_POST.
     * @return string the success message | error message.
     */
    function deleteAssessment() {
        $error = 0;
        $token = $this->config->item('accessToken');
        $assessmentId = $this->input->post('id', true);
        if (!$assessmentId) {
            echo $error = 'Undefined assessment!';
            exit;
        }
        if (!$error) {
            $data['status'] = 3;
            $resp = $this->assessment_model->updateAssessment($data, $assessmentId);
            if ($resp) {
                echo "Deleted successfully";
            }
        }
    }

    /**
     * Discription : Used to set assessment as practice
     * Author Synergy
     * @param $_POST.
     * @return string the success message | error message.
     */
    function setAssessmentAsPractice() {
        
        $userData = $this->session->userdata['logged_in'];
        $userId = $userData->id;
        
        $checkAssessmentIdValue = $this->input->post('checkAssessmentIdValue', true);
        $checkedStatus = $this->input->post('checkedStatus', true);
        
        $data = array();
        
        if( $checkAssessmentIdValue != "" )
        {
            if( $checkedStatus == 1 ){
                $data['practiceSetStatus'] = 1;                
            } else if( $checkedStatus == 0 ){
                $data['practiceSetStatus'] = 0;
            }
            $updateRes = $this->assessment_model->updateAssessment($data, $checkAssessmentIdValue);
            
            if($updateRes){
                echo "Done";
                exit();
            } else {
                echo "NotDone";
                exit();
            }
        } else {
            echo "Blank";
            exit();
        }
    }
}
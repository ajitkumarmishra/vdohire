<?php

/*
 * Routing for Users
 */
$route['api/user'] = "User/list";
$route['api/community'] = "User/community";
$route['api/forgotpassword'] = "User/forgotpassword";
$route['api/changepassword'] = "User/changepassword";
$route['api/changepic'] = "User/changeprofilepic";
$route['api/deleteinterview'] = "User/interview_delete";
$route['api/user/signin'] = "User/signin";
$route['api/user/invite'] = "User/invite";
$route['api/user/add'] = "User/add";
$route['api/user/logout'] = "User/logout";
$route['api/user/notifications'] = "User/notifications";
$route['api/user/search_interview'] = "User/search_interview";
$route['api/user/add_user_rating'] = "User/add_user_rating";
$route['api/user/user_rating'] = "User/user_rating";
$route['api/user/webinar'] = "User/webinar";
$route['api/user/listen_interview'] = "User/listen_interview";
$route['api/user/my_interview'] = "User/my_interview";
$route['api/user/community_interview'] = "User/community_interview";
$route['api/user/verify_register'] = "User/verify_register";
$route['api/user/drop/(:any)'] = "User/drop/$1";
$route['api/user/update/(:any)'] = "User/update/$1";
$route['api/user/(:any)'] = "User/list/$1";

/*
 * Routing for Questions
 */
$route['api/questions'] = "Questions/list";
$route['api/samples'] = "Questions/sample_interview";
$route['api/questions/(:any)'] = "Questions/list/$1";

/*
 * Routing for Tips
 */
$route['api/tips'] = "Tips/list";
$route['api/tips/(:any)'] = "Tips/list/$1";

/*
 * Routing for industry & Functions
 */
$route['api/industry'] = "Industry/list";
$route['api/industry/functions/(:any)'] = "Industry/functions/$1";
$route['api/industry/(:any)'] = "Industry/list/$1";

/*
 * Routing for Ansers
 */
$route['api/answers'] = "Answers/add";
$route['api/answers/merge'] = "Answers/mergetest";

/*
 * Routing for Questions
 */
$route['api/assessments'] = "Assessments/list";
$route['api/assessments/answers'] = "Assessments/assessment_answers";
$route['api/assessments/(:any)'] = "Assessments/list/$1";
/*
 * Routing for Forums
 */

$route['api/forum'] = "Forum/list";
$route['api/listcomment'] = "Forum/list_comments";
$route['api/forum/add'] = "Forum/add";
$route['api/forum/comments'] = "Forum/comments";
$route['api/listcomment/(:any)'] = "Forum/list_comments/$1";
$route['api/forum/(:any)'] = "Forum/list/$1";

/*
 * Routing for Privacy Page
 */
$route['api/page'] = "Pages/list";
$route['api/page/(:any)'] = "Pages/list/$1";
/*
 * Routing for Rules
 */
$route['api/rules'] = "User/rules";



$route['api/comprehensive'] = "Comprehensive/list";
?>


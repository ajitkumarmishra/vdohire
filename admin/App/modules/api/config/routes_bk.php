<?php
/*
 * Routing for Customers
 */
$route['api/customers'] = "customers/list";
$route['api/customers/add'] = "customers/add";
$route['api/customers/drop/(:any)'] = "customers/drop/$1";
$route['api/customers/update/(:any)'] = "customers/update/$1";
$route['api/customers/(:any)'] = "customers/list/$1";
/*
 * Routing for Vendors
 */
$route['api/vendors'] = "vendors/list";
$route['api/vendors/add'] = "vendors/add";
$route['api/vendors/drop/(:any)'] = "vendors/drop/$1";
$route['api/vendors/update/(:any)'] = "vendors/update/$1";
$route['api/vendors/(:any)'] = "vendors/list/$1";

/*
 * Routing for Store
 */
$route['api/store'] = "store/list";
$route['api/store/add'] = "store/add";
$route['api/store/drop/(:any)'] = "store/drop/$1";
$route['api/store/update/(:any)'] = "store/update/$1";
$route['api/store/(:any)'] = "store/list/$1";
/*
 * Routing for Product
 */

$route['api/product'] = "product/list";
$route['api/product/add'] = "product/add";
$route['api/product/drop/(:any)'] = "product/drop/$1";
$route['api/product/update/(:any)'] = "product/update/$1";
$route['api/product/(:any)'] = "product/list/$1";


$route['api/country'] = "country/list";
$route['api/country/(:any)'] = "country/list/$1";
/*
 * Routing for Purchase
 */

$route['api/purchase'] = "purchase/list";
$route['api/purchase/add'] = "purchase/add";
$route['api/purchase/drop/(:any)'] = "purchase/drop/$1";
$route['api/purchase/update/(:any)'] = "purchase/update/$1";
$route['api/purchase/(:any)'] = "purchase/list/$1";

/*
 * Routing for Product Batch
 */
$route['api/productbatch'] = "productbatch/list";
$route['api/productbatch/add'] = "productbatch/add";
$route['api/productbatch/drop/(:any)'] = "productbatch/drop/$1";
$route['api/productbatch/update/(:any)'] = "productbatch/update/$1";
$route['api/productbatch/(:any)'] = "productbatch/list/$1";


/*
 * Routing for Product Batch
 */
$route['api/sales'] = "sales/list";
$route['api/sales/add'] = "sales/add";
$route['api/sales/drop/(:any)'] = "sales/drop/$1";
$route['api/sales/update/(:any)'] = "sales/update/$1";
$route['api/sales/(:any)'] = "sales/list/$1";


$route['api/inventory'] = "inventory/list";
$route['api/inventory/add'] = "inventory/add";
$route['api/inventory/drop/(:any)'] = "inventory/drop/$1";
$route['api/inventory/update/(:any)'] = "inventory/update/$1";
$route['api/inventory/(:any)'] = "inventory/list/$1";

/*
 * Routing for Product Batch
 */
$route['api/returnsale'] = "salereturn/list";
$route['api/returnsale/add'] = "salereturn/add";
$route['api/returnsale/drop/(:any)'] = "salereturn/drop/$1";
$route['api/returnsale/update/(:any)'] = "salereturn/update/$1";
$route['api/returnsale/(:any)'] = "salereturn/list/$1";
/*
 * Routing for Day Open/Close Status
 */
$route['api/daystatus'] = "daystatus/list";
$route['api/daystatus/add'] = "daystatus/add";
$route['api/daystatus/drop/(:any)'] = "daystatus/drop/$1";
$route['api/daystatus/update/(:any)'] = "daystatus/update/$1";
$route['api/daystatus/(:any)'] = "daystatus/list/$1";
/*
 * Routing for Tender
 */
$route['api/tender'] = "tender/list";
$route['api/tender/add'] = "tender/add";
$route['api/tender/drop/(:any)'] = "tender/drop/$1";
$route['api/tender/update/(:any)'] = "tender/update/$1";
$route['api/tender/(:any)'] = "tender/list/$1";
/*
 * Routing for Tax
 */
$route['api/tax'] = "tax/list";
$route['api/tax/add'] = "tax/add";
$route['api/tax/drop/(:any)'] = "tax/drop/$1";
$route['api/tax/update/(:any)'] = "tax/update/$1";
$route['api/tax/(:any)'] = "tax/list/$1";
/*
 * Routing for Bill Hold/Unhold
 */
$route['api/billhold'] = "bill_hold/list";
$route['api/billhold/add'] = "bill_hold/add";
$route['api/billhold/drop/(:any)'] = "bill_hold/drop/$1";
$route['api/billhold/update/(:any)'] = "bill_hold/update/$1";
$route['api/billhold/(:any)'] = "bill_hold/list/$1";

?>


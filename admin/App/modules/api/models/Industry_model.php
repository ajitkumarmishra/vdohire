<?php

/**
 * Created by PhpStorm.
 * User: Sanjay Yadav<yadav.sanjay@orangemantra.com>
 * Date: 09/12/2015
 * Time: 11:28 AM
 */
class Industry_model extends CI_Model {

    var $indus_table = "nc_industry";

    function __construct() {
        parent::__construct();
    }

    /**
     * @param null $ID
     * @return mixed data
     * @date : 04/12/2015
     * @description : Get Industry data
     */
    public function getAll($ID = null) {
        if ($ID != null) {
            $this->db->where('id', $ID);
        }
        $this->db->where(array('is_active', '1', 'indus_id' => '0'));
        $this->db->select('id,indus_id,name');
        $this->db->from($this->indus_table);
        $result = $this->db->get();
        return $result->result();
    }

    /**
     * @param null $ID
     * @return mixed data
     * @date : 04/12/2015
     * @description : Get Functions data
     */
    public function getFunctions($ID = null) {
        if ($ID != null) {
            $this->db->where('indus_id', $ID);
        }
        $this->db->where('is_active', '1');
        $this->db->select('id,indus_id,name');
        $this->db->from($this->indus_table);
        $result = $this->db->get();
        return $result->result();
    }

}

?>
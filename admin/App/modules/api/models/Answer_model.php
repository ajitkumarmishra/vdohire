<?php

/**
 * Created by PhpStorm.
 * User: Sanjay Yadav<yadav.sanjay@orangemantra.com>
 * Date: 09/12/2015
 * Time: 11:28 AM
 */
class Answer_model extends CI_Model {

    var $question_table = "nc_questions";
    var $ans_table = "nc_answers";

    function __construct() {
        parent::__construct();
    }

    /**
     * @param null $ID
     * @return mixed data
     * @date : 04/12/2015
     * @description : Get Question data
     */
    public function upload_answer() {
        $config = array();
        ini_set('upload_max_filesize', '200M');
        ini_set('post_max_size', '200M');
        ini_set('max_input_time', 3000);
        ini_set('max_execution_time', 3000);
        $config['upload_path'] = IMAGESPATH . 'answers/';
        $config['allowed_types'] = '*'; //'mp3|wav';
        $config['file_name'] = md5(uniqid(rand(), true));
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if ($this->upload->do_upload('answer_file')) {
            $info = $this->upload->data();
            return $info;
        } else {
            print_r($this->upload->display_errors());
        }
    }

    public function addAnswer($data) {
        try {
            $this->load->library('form_validation');
            $config = array(
                array('field' => 'interview_id', 'label' => 'Interview', 'rules' => 'required')
            );
            $this->form_validation->set_rules($config);
            if ($this->form_validation->run() == false) {
                $errors_array = '';
                foreach ($config as $row) {
                    $field = $row['field'];
                    $error = strip_tags(form_error($field));
                    if ($error)
                        $errors_array .= $error . ', ';
                }
                $message = array(
                    'status' => false,
                    'response_code' => '0',
                    'message' => rtrim($errors_array, ', ')
                );
            } else {
                $this->db->set('answer_creation_date', 'NOW()', FALSE);
                $this->db->insert($this->ans_table, $data);
                $message = array(
                    'status' => true,
                    'response_code' => '1',
                    'message' => "Success"
                );
            }
        } catch (Exception $ex) {
            $message = array(
                'status' => false,
                'response_code' => '0',
                'message' => $ex->getMessage(),
            );
        }
        return $message;
    }

    public function CheckAnswer($data) {
        $user_id = $data['user_id'];
        $ques_id = $data['question_id'];
        $interview_id = $data['interview_id'];
        $this->db->where('interview_id', $interview_id);
        $this->db->where('question_id', $ques_id);
        $this->db->where('user_id', $user_id);
        $query = $this->db->get($this->ans_table);
        if ($query->num_rows() > 0) {
            return false;
        } else {
            return true;
        }
    }

    public function setInterview_status($id = null, $status) {
        if ($id != '' && $id != null) {
            $this->db->where('interview_id', $id);
            $data = array(
                'finalstatus' => $status
            );
            $this->db->update('nc_interview', $data);
        }
    }

    public function GetType($int_id, $user_id) {
        $this->db->where('interview_id', $int_id);
        $this->db->where('user_id', $user_id);
        $this->db->select('type');
        $this->db->from('nc_interview');
        $res = $this->db->get();
        return $res->row();
    }

}

?>
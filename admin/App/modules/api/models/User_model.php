<?php

/**
 * Created by PhpStorm.
 * User: Sanjay Yadav<yadav.sanjay@orangemantra.com>
 * Date: 04/12/2015
 * Time: 11:28 AM
 */
class User_model extends CI_Model {

    var $user_table = "users";
    var $user_rating = "nc_rating";
    var $interview_table = "nc_interview";
    var $answer_table = "nc_answers";
    var $question_table = "nc_questions";

    function __construct() {
        parent::__construct();
    }

    /**
     * @param null $ID
     * @return mixed data
     * @date : 04/12/2015
     * @description : Get User data
     */
    public function getAllUsers($ID = null) {
        if ($ID != null) {
            $this->db->where('id', $ID);
        }
        $this->db->where('status', '1');
        $this->db->from($this->user_table);
        $result = $this->db->get();
        return $result->result();
    }

    public function get_interview_result($user_id, $type) {
        $this->db->where('user_id', $user_id);
        $this->db->where('finalstatus', '1');
        $this->db->where('type', $type);
        $this->db->select('id,interview_id,creation_date,(CASE WHEN `community` = "private" THEN 0 ELSE 1 END) as is_public,CONCAT("' . base_url('uploads') . '/mergefiles/",merge_file) AS  result');
        $this->db->from($this->interview_table);
        $result = $this->db->get();
        return $result->result();
    }

    public function get_details_result($user_id) {
        $this->db->where('id', $user_id);
        $this->db->select('id,user_pic');
        $this->db->from($this->user_table);
        $result = $this->db->get();
        return $result->result();
    }

    /**
     * @method : Add User
     * @param $data
     * @return bool
     * @date : 04/12/2015
     * @description : Add new User in the User table
     */
    public function create($data) {
        try {
            $this->load->library('form_validation');
            $config = array(
                array('field' => 'user_email', 'label' => 'User Email', 'rules' => 'trim|required|valid_email|is_unique[users.user_email]'),
                array('field' => 'user_mobile', 'label' => 'User Mobile', 'rules' => 'trim|required|numeric|is_unique[users.user_mobile]'),
                array('field' => 'user_pass', 'label' => 'User Password', 'rules' => 'trim|required'),
            );
            $this->form_validation->set_rules($config);
            if ($this->form_validation->run() == false) {
                $errors_array = '';
                foreach ($config as $row) {
                    $field = $row['field'];
                    $error = strip_tags(form_error($field));
                    if ($error)
                        $errors_array .= $error . ', ';
                }
                $message = array(
                    'status' => false,
                    'response_code' => '0',
                    'message' => rtrim($errors_array, ', ')
                );
            } else {
                $this->db->set('registered_date', 'NOW()', false);
                $this->db->insert($this->user_table, $data);
                $this->load->library('email');
                $this->email->from('info@firstjob.com', 'First Job');
                $this->email->to($data['user_email']);
                $this->email->subject('New Registration');
                $this->email->message('Thank you for registering with Firstjob!');
                $this->email->send();
                $message = array(
                    'status' => true,
                    'response_code' => '1',
                    'message' => "User Added Successfully"
                );
            }
        } catch (Exception $ex) {
            $message = array(
                'status' => false,
                'response_code' => '0',
                'message' => $ex->getMessage(),
            );
        }
        return $message;
    }

    public function addAnswer($data) {
        try {
            $this->load->library('form_validation');

            $config = array(
                array('field' => 'user_email', 'label' => 'User Email', 'rules' => 'trim|required|valid_email|is_unique[users.user_email]'),
                array('field' => 'user_mobile', 'label' => 'User Mobile', 'rules' => 'trim|required|numeric|is_unique[users.user_mobile]'),
                array('field' => 'user_pass', 'label' => 'User Password', 'rules' => 'trim|required'),
            );
            $this->form_validation->set_rules($config);
            if ($this->form_validation->run() == false) {
                $errors_array = '';
                foreach ($config as $row) {
                    $field = $row['field'];
                    $error = strip_tags(form_error($field));
                    if ($error)
                        $errors_array .= $error . ', ';
                }
                $message = array(
                    'status' => false,
                    'response_code' => '0',
                    'message' => rtrim($errors_array, ', ')
                );
            } else {
                $this->db->set('registered_date', 'NOW()', false);
                $this->db->insert($this->user_table, $data);
                $code = $data['random_code'];
                $msg_spek = 'Thank you for registering with Firstjob! Your code =';
                $msg = $msg_spek . $code;
                $this->load->library('email');
                $this->email->from('info@firstjob.com', 'First Job');
                $this->email->to($data['user_email']);
                $this->email->subject('New Registration');
                $this->email->message($msg);
                $this->email->send();
                $message = array(
                    'status' => true,
                    'response_code' => '1',
                    'message' => "Success",
                    'code' => $code
                );
            }
        } catch (Exception $ex) {
            $message = array(
                'status' => false,
                'response_code' => '0',
                'message' => $ex->getMessage(),
            );
        }
        return $message;
    }

    /**
     * @param $id
     * @param $data
     * @return bool
     * @date : 04/12/2015
     * @method : Update User
     * @description : To update User
     */
    public function update($id, $data) {
        try {
            $this->load->library('form_validation');
            $update_data = array(
                'user_name' => $data['user_name'],
                'user_dob' => $data['user_dob'],
                'user_gender' => $data['user_gender']
            );
            $this->db->set('user_modification_date', 'NOW()', FALSE);
            $this->db->where('id', $id);
            $query = $this->db->update($this->user_table, $update_data);
            $message = array(
                'status' => true,
                'response_code' => '1',
                'message' => "Success"
            );
        } catch (Exception $ex) {
            $message = array(
                'status' => false,
                'response_code' => '0',
                'message' => $ex->getMessage(),
            );
        }
        return $message;
    }

    /**
     * @param $id
     * @return bool
     * @date : 04/12/15
     * @method :  Delete User
     * @description : To delete User from User table
     */
    public function delete($id) {
        if (isset($id) && $id != '') {
            $this->db->where('id', $id);
            $query = $this->db->delete($this->user_table);
            if ($query) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    function validate($user_email, $password) {
        $this->db->where('user_email', $user_email);
        $this->db->where('user_pass', $password);
        $query = $this->db->get($this->user_table);
        if ($query->num_rows() === 1) {
            return $query->row();
        } else {
            return false;
        }
    }

    function valid_current_pass($user_id, $current_pass) {
        $this->db->where('id', $user_id);
        $this->db->where('user_pass', $current_pass);
        $query = $this->db->get($this->user_table);
        if ($query->num_rows() === 1) {
            return $query->row();
        } else {
            return false;
        }
    }

    // reset new password
    function updatepassword($table, $user_id, $new_pass) {
        $user_email = $this->db
                        ->select('user_email')
                        ->where('id', $user_id)
                        ->limit(1)
                        ->get('users')
                        ->result_array()[0]['user_email'];
        $useremail = $user_email;
        $this->load->library('email');
        $this->email->from('info@firstjob.com', 'First Job');
        $this->email->to($useremail);
        $this->email->subject('change Password');
        $this->email->message('Hi Your password has been changed: New Password is: ' . $new_pass);
        $this->email->send();
        $post = $this->input->post();
        $ins = array();
        $ins['user_pass'] = md5($new_pass);
        $this->db->where('id', $user_id);
        $this->db->update($table, $ins);
    }

    public function validate_password($data) {
        try {
            $this->load->library('form_validation');
            $config = array(
                array('field' => 'user_email', 'label' => 'User Email', 'rules' => 'trim|required|valid_email'),
                array('field' => 'user_mobile', 'label' => 'User Mobile', 'rules' => 'trim|required|numeric')
            );
            $this->form_validation->set_rules($config);
            if ($this->form_validation->run() == false) {
                $errors_array = '';
                foreach ($config as $row) {
                    $field = $row['field'];
                    $error = strip_tags(form_error($field));
                    if ($error)
                        $errors_array .= $error . ', ';
                }
                $message = array(
                    'status' => false,
                    'response_code' => '0',
                    'message' => rtrim($errors_array, ', ')
                );
            } else {
                $pass = $this->generateStrongPassword('9');
                $this->load->library('email');
                $this->email->from('info@firstjob.com', 'First Job');
                $this->email->to($data['user_email']);
                $this->email->subject('Forgot Password');
                $this->email->message('Hi Your password has been changed: New Password is: ' . $pass);
                $this->email->send();
                $user_email = $data['user_email'];
                $user_mobile = $data['user_mobile'];
                $updtate = array(
                    'user_pass' => md5($pass)
                );

                $cond = array(
                    'user_email' => $user_email,
                    'user_mobile' => $user_mobile
                );
                $this->db->where($cond);
                //$this->db->where('user_email', $user_email);
                //$this->db->where('user_mobile', $user_mobile);
                $query = $this->db->update($this->user_table, $updtate);
                //echo $this->db->last_query(); die;
                $message = array(
                    'status' => true,
                    'response_code' => '1',
                    'password' => $pass
                );
            }
        } catch (Exception $ex) {
            $message = array(
                'status' => false,
                'response_code' => '0',
                'message' => $ex->getMessage(),
            );
        }
        return $message;
    }

    function generateStrongPassword($length = 9, $add_dashes = false, $available_sets = 'luds') {
        $sets = array();
        if (strpos($available_sets, 'l') !== false)
            $sets[] = 'abcdefghjkmnpqrstuvwxyz';
        if (strpos($available_sets, 'u') !== false)
            $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
        if (strpos($available_sets, 'd') !== false)
            $sets[] = '23456789';
        if (strpos($available_sets, 's') !== false)
            $sets[] = '!@#$%&*?';
        $all = '';
        $password = '';
        foreach ($sets as $set) {
            $password .= $set[array_rand(str_split($set))];
            $all .= $set;
        }
        $all = str_split($all);
        for ($i = 0; $i < $length - count($sets); $i++)
            $password .= $all[array_rand($all)];
        $password = str_shuffle($password);
        if (!$add_dashes)
            return $password;
        $dash_len = floor(sqrt($length));
        $dash_str = '';
        while (strlen($password) > $dash_len) {
            $dash_str .= substr($password, 0, $dash_len) . '-';
            $password = substr($password, $dash_len);
        }
        $dash_str .= $password;
        return $dash_str;
    }

    public function updateprofile($id) {
        $file = $this->upload_profile_pic();
        if ($file['file_name'] != '') {
            $data = array(
                'user_pic' => $file['file_name']
            );
            $this->db->where('id', $id);
            $query = $this->db->update($this->user_table, $data);
            if ($query) {
                $filepath = base_url() . 'uploads/users/profile/' . $file['file_name'];
                return $filepath;
            } else {
                return false;
            }
        } else {
            $data = array(
                'user_pic' => ''
            );
            $this->db->where('id', $id);
            $query = $this->db->update($this->user_table, $data);
            $filepath = "null";
            return $filepath;
        }
    }

    protected function upload_profile_pic() {
        $config = array();
        ini_set('upload_max_filesize', '200M');
        ini_set('post_max_size', '200M');
        ini_set('max_input_time', 3000);
        ini_set('max_execution_time', 3000);
        $config['upload_path'] = IMAGESPATH . 'users/profile/';
        $config['allowed_types'] = '*'; //'mp3|wav';
        $config['file_name'] = md5(uniqid(rand(), true));
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if ($this->upload->do_upload('profile_pic')) {
            $info = $this->upload->data();
            return $info;
        } else {
            print_r($this->upload->display_errors());
            die;
        }
    }

    public function update_marge_file($interview_id, $upload_rand_file, $user_id = null) {
        try {

            $data = array(
                'merge_file' => $upload_rand_file
            );
            $marge_file_path = base_url() . 'uploads/mergefiles/' . $upload_rand_file;
            $this->db->where('interview_id', $interview_id);
            if ($user_id != null && $user_id != '')
                $this->db->where('user_id', $user_id);
            $query = $this->db->update($this->interview_table, $data);
            $message = array(
                'status' => true,
                'response_code' => '1',
                'message' => "Success",
                'result' => $marge_file_path
            );
        } catch (Exception $ex) {
            $message = array(
                'status' => false,
                'response_code' => '0',
                'message' => $ex->getMessage(),
            );
        }
        return $message;
    }

    public function getAllinterviewid($interview_id, $user_id) {

        $this->db->where('interview_id', $interview_id);
        $this->db->where('user_id', $user_id);
        $this->db->from($this->answer_table);
        $result = $this->db->get();
        return $result->result();
    }

    public function getquestion($question_id) {

        $this->db->where('question_id', $question_id);
        $this->db->select("question_id,question_file", FALSE);
        $this->db->from($this->question_table);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function getUserPic($id) {
        $this->db->select('user_pic');
        $this->db->where('id', $id);
        $query = $this->db->get('users');
        return $query->row();
    }

    public function getInterviewCreation($id) {
        $this->db->select('creation_date');
        $this->db->where('interview_id', $id);
        $query = $this->db->get('nc_interview');
        return $query->row();
    }

    public function updateCommunity($data) {
        try {
            $update_comm = array(
                'community' => $data['community']
            );
            $this->db->where('interview_id', $data['interview_id']);
            $this->db->where('user_id', $data['user_id']);
            $query = $this->db->update($this->interview_table, $update_comm);
            if ($query) {
                $message = array(
                    'status' => true,
                    'response_code' => '1',
                    'message' => "Success"
                );
            } else {
                $message = array(
                    'status' => false,
                    'response_code' => '0',
                    'message' => "Failed"
                );
            }
        } catch (Exception $ex) {
            $message = array(
                'status' => false,
                'response_code' => '0',
                'message' => $ex->getMessage(),
            );
        }
        return $message;
    }

    public function get_community_result($comm, $type) {
        //$this->db->where('user_id', $id);
        $this->db->where('community', $comm);
        $this->db->where('type', $type);
        $this->db->where('finalstatus', '1');
        $this->db->select('int.id,int.interview_id,int.user_id,int.creation_date,CONCAT("' . base_url('uploads') . '/mergefiles/",int.merge_file) AS  result,IF((`us`.`user_pic`=""),"null",CONCAT("' . base_url('uploads') . '/users/profile/",us.user_pic)) As user_pic');
        $this->db->from($this->interview_table . ' as int');
        $this->db->join('users' . " us", "us.id = int.user_id");
        $result = $this->db->get();
        //print_r($this->db->last_query());
        //die;
        return $result->result();
    }

    public function delete_interview($data) {
        $this->db->where('id', $data['interview_id']);
        //$this->db->where('user_id', $data['user_id']);
        $query = $this->db->delete($this->interview_table);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function delete_interview_answer($data) {
        $this->db->where('interview_id', $data['interview_id']);
        $this->db->where('user_id', $data['user_id']);
        $query = $this->db->delete($this->answer_table);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function getRule($data) {
        $this->db->where('rule_file_type', $data['file_type']);
        $this->db->where('rule_type', $data['rule_type']);
        $query = $this->db->get('nc_rules');
        if ($query->num_rows() > 0) {
            $message = array(
                'status' => true,
                'response_code' => '1',
                'message' => "Success",
                'data' => $query->row()
            );
        } else {
            $message = array(
                'status' => false,
                'response_code' => '0',
                'message' => "No Content"
            );
        }
        return $message;
    }

    public function addrating($data) {
        try {
            $this->load->library('form_validation');

            $config = array(
                array('field' => 'user_id', 'label' => 'User ID', 'rules' => 'required'),
                array('field' => 'auto_interview_id', 'label' => 'Auto Interview ID', 'rules' => 'required'),
                array('field' => 'rateby_user_id', 'label' => 'Rate By User ID', 'rules' => 'required'),
            );
            $this->form_validation->set_rules($config);
            if ($this->form_validation->run() == false) {
                $errors_array = '';
                foreach ($config as $row) {
                    $field = $row['field'];
                    $error = strip_tags(form_error($field));
                    if ($error)
                        $errors_array .= $error . ', ';
                }
                $message = array(
                    'status' => false,
                    'response_code' => '0',
                    'message' => rtrim($errors_array, ', ')
                );
            } else {
                $this->db->set('creation_date', 'NOW()', false);
                $this->db->insert($this->user_rating, $data);
                $message = array(
                    'status' => true,
                    'response_code' => '1',
                    'message' => "Success",
                );
            }
        } catch (Exception $ex) {
            $message = array(
                'status' => false,
                'response_code' => '0',
                'message' => $ex->getMessage(),
            );
        }
        return $message;
    }

    function getuser_interview_id($get_id) {
        $this->db->where('inter.id', $get_id);
        $this->db->select('us.id as user_id,us.user_name,inter.id as interview_id,inter.creation_date,inter.type,inter.community,IF((`us`.`user_pic`=""),"null",CONCAT("' . base_url('uploads') . '/users/profile/",us.user_pic)) As user_pic,CONCAT("' . base_url('uploads') . '/mergefiles/",inter.merge_file) AS mearge_file');

        $this->db->from('users' . ' as us');
        $this->db->join('nc_interview' . " inter", "inter.user_id = us.id");
        $query = $this->db->get();

        return $query->row();
    }

    function getwebinar($type, $file_type) {

        $this->db->where('audition_name', $type);
        $this->db->where('file_type', $file_type);
        $this->db->select('id,title,CONCAT("' . base_url('uploads') . '/audition/",audition_file) AS audition_file,audition_name,file_type');
        $this->db->from('nc_audition');
        $query = $this->db->get();
        //echo $this->db->last_query();die;
        return $query->result();
    }

    function getuser_rating($get_id) {
        $this->db->where('rat.auto_interview_id', $get_id);
        $this->db->select('rat.rateby_user_id,us.user_name,IF((`us`.`user_pic`=""),"null",CONCAT("' . base_url('uploads') . '/users/profile/",us.user_pic)) As user_pic,rat.rating_section1 as rating1,rat.rating_section2 as rating2,rat.rating_section3 as rating3,rat.rating_section4 as rating4,rat.rating_section5 as rating5,rat.comment,rat.creation_date,rat.status');
        $this->db->from('users' . ' as us');
        $this->db->join('nc_rating' . " rat", "rat.rateby_user_id = us.id");
        $query = $this->db->get();
        //echo $this->db->last_query();die;
        return $query->result();
    }

    function getuserid($table, $get_id) {
        $this->db->select('user_id');
        $this->db->where('auto_interview_id', $get_id);
        $query = $this->db->get($table);
        return $query->row();
    }

    public function Listenfile($id, $user_id) {
        $this->db->where('interview_id', $id);
        $this->db->where('user_id', $user_id);
        $this->db->select('merge_file');
        $this->db->from('nc_interview');
        $res = $this->db->get();
        if ($res) {
            $imagevalue = $res->row()->merge_file;
            if ($imagevalue != '') {
                $marge_file_path = base_url() . 'uploads/mergefiles/' . $res->row()->merge_file;
                $message = array(
                    'status' => true,
                    'response_code' => '1',
                    'message' => "Success",
                    'result' => $marge_file_path
                );
            } else {
                $message = array(
                    'status' => false,
                    'response_code' => '0',
                    'message' => "Retry",
                    'result' => 'no'
                );
            }
        }
        return $message;
    }

    public function addInvite($con) {
        $date = date("Y-m-d H:i:s");
        $data = array(
            'mobile' => $con['mobile'],
            'interview_id' => $con['interview_id'],
            'email' => '',
            'date' => $date,
            'invite_by' => $con['invite_by'],
        );
        $query = $this->db->insert('nc_invite', $data);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function CheckInvite($con) {
        $mobile = $con['mobile'];
        $interview_id = $con['interview_id'];
        $cond = array(
            'mobile' => $mobile,
            'interview_id' => $interview_id
        );
        $this->db->where($cond);
        $query = $this->db->get('nc_invite');
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function GetUserMobile($id) {
        $this->db->where('id', $id);
        $this->db->select('user_mobile');
        $query = $this->db->get($this->user_table);
        return $query->row()->user_mobile;
    }

    public function GetUserName($id) {
        $this->db->where('id', $id);
        $this->db->select('user_name');
        $query = $this->db->get($this->user_table);
        return $query->row()->user_name;
    }

    public function GetUserNotificationPic($id) {
        $this->db->where('id', $id);
        $this->db->select('user_pic');
        $query = $this->db->get($this->user_table);
        return $query->row()->user_pic;
    }

    public function GetDetInterview($id) {
        $this->db->where('id', $id);
        $this->db->select('interview_id,user_id');
        $query = $this->db->get('nc_interview');
        return $query->row();
    }

    // reg_id chek  for ios
    function checkreg_id_ios($table, $user_email) {
        $this->db->select('reg_id');
        $this->db->where('user_email', $user_email);
        $query = $this->db->get($table);
        return $query->row();
    }

    // update reg_id for ios
    function updateregid_ios($table, $user_email, $reg_id) {
        $data = array();
        $data['reg_id'] = json_encode($reg_id);
        $this->db->where('user_email', $user_email);
        $this->db->update($table, $data);
    }

    function updaterlogout($table, $user_id, $reg_id) {
        $getuser = $this->db
                        ->select('reg_id')
                        ->where('id', $user_id)
                        ->limit(1)
                        ->get('users')
                        ->result_array()[0]['reg_id'];

        $reg_user_id = json_decode($getuser);
        if (!empty($reg_user_id)) {
            $key = array_search($reg_id, $reg_user_id);
            unset($reg_user_id[$key]);
            if (count($reg_user_id) > 0) {
                $data = array();
                foreach ($reg_user_id as $k => $v) {
                    $reg_ids[] = $v;
                    $data['reg_id'] = json_encode($reg_ids);
                }
            } else {
                $data = array();
                $data['reg_id'] = '';
            }
            $this->db->where('id', $user_id);
            $this->db->update($table, $data);
        }
    }

    function invite_notification($row) {

        $mobile = $row['mobile'];
        $interview_auto_id = $row['interview_id'];
        $this->db->select('reg_id');
        $this->db->where('user_mobile', $mobile);
        $query = $this->db->get($this->user_table);
        $getuser = $query->result_array();

        foreach ($getuser as $val) {
            $register_send = $val;
            $value = $register_send['reg_id'];
            $get = json_decode($value);
            $uName = $this->GetUserName($row['invite_by']);
            $message = $uName . ' has invited you for feedback.';
            $api_key = 'AIzaSyAycBBRizG6_IJhVxeE6BihbCzuGUJb2lE';

            if (empty($api_key) || count($get) < 0) {
                $result = array(
                    'success' => '0',
                    'message' => 'api or reg id not found',
                );
                echo json_encode($result);
                die;
            }
            $registrationIDs = $get;
            $DetInter = $this->getuser_interview_id($interview_auto_id);
            $con = array(
                'mobile' => $mobile,
                'interview_id' => $interview_auto_id
            );
            $invite_check = $this->CheckInvite($con);

            if ($invite_check) {
                $DetInter->invite_status = true;
            } else {
                $DetInter->invite_status = false;
            }

            // $message = "congratulations";
            $url = 'https://android.googleapis.com/gcm/send';
            $fields = array(
                'registration_ids' => $registrationIDs,
                'data' => array("message" => $message, "title" => 'Interview Invitation', "type" => 'Interview', 'data' => $DetInter),
            );

            $headers = array(
                'Authorization: key=' . $api_key,
                'Content-Type: application/json');

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            $result = curl_exec($ch);

            curl_close($ch);
            //print_r($result); 
        }
    }

    function getreview_interview_id($get_id) {
        $this->db->where('inter.id', $get_id);
        $this->db->select('us.id as user_id,us.user_name,inter.id as interview_id,inter.creation_date,inter.type,inter.community,IF((`us`.`user_pic`=""),"null",CONCAT("' . base_url('uploads') . '/users/profile/",us.user_pic)) As user_pic,CONCAT("' . base_url('uploads') . '/mergefiles/",inter.merge_file) AS mearge_file');

        $this->db->from('nc_interview' . ' as inter');
        $this->db->join('nc_rating' . " ra", "inter.id = ra.auto_interview_id");
        $this->db->join('users' . " us", "ra.rateby_user_id = us.id");
        $query = $this->db->get();

        return $query->row();
    }

    function review_notification($row) {

        $us_id = $row['user_id'];
        $mobile = $row['mobile'];
        $interview_auto_id = $row['interview_id'];
        $this->db->select('reg_id');
        $this->db->where('id', $us_id);
        $query = $this->db->get($this->user_table);
        $getuser = $query->result_array();

        foreach ($getuser as $val) {
            $register_send = $val;
            $value = $register_send['reg_id'];
            $get = json_decode($value);
            $uName = $this->GetUserName($row['rateby_user_id']);
            $message = $uName . ' has given you feedback.';
            $api_key = 'AIzaSyAycBBRizG6_IJhVxeE6BihbCzuGUJb2lE';

            if (empty($api_key) || count($get) < 0) {
                $result = array(
                    'success' => '0',
                    'message' => 'api or reg id not found',
                );
                echo json_encode($result);
                die;
            }
            $registrationIDs = $get;
            $DetInter = $this->getuser_interview_id($interview_auto_id);
            $uPic = base_url('uploads') . '/users/profile/' . $this->GetUserNotificationPic($row['rateby_user_id']);
            $DetInter->rateby_user_pic = $uPic;
            $con = array(
                'mobile' => $mobile,
                'interview_id' => $interview_auto_id
            );
            $invite_check = $this->CheckInvite($con);

            if ($invite_check) {
                $DetInter->invite_status = true;
            } else {
                $DetInter->invite_status = false;
            }
            // $message = "congratulations";
            $url = 'https://android.googleapis.com/gcm/send';
            $fields = array(
                'registration_ids' => $registrationIDs,
                'data' => array("message" => $message, "title" => 'Interview Review', 'data' => $DetInter),
            );

            $headers = array(
                'Authorization: key=' . $api_key,
                'Content-Type: application/json');

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            $result = curl_exec($ch);

            curl_close($ch);
            //print_r($result); 
        }
    }

    public function GetPendingInvite($userID, $mob) {
        $query = $this->db->query('SELECT `us`.`id` as `user_id`, `us`.`user_name`, `inter`.`id` as `interview_id`,`inter`.`creation_date`, `inter`.`type`, `inter`.`community`,CONCAT("' . base_url('uploads') . '/users/profile/",us.user_pic) AS user_pic,CONCAT("' . base_url('uploads') . '/mergefiles/",inter.merge_file) AS mearge_file FROM `nc_invite` `inv` JOIN `nc_interview` `inter` ON `inter`.`id` = `inv`.`interview_id` JOIN `users` `us` ON `inter`.`user_id` = `us`.`id` WHERE `inv`.`interview_id` NOT IN (select auto_interview_id from nc_rating where rateby_user_id="' . $userID . '") AND `inv`.`mobile`= "' . $mob . '"');
        return $query->result();
    }

    public function GetReviewedInvite($userID, $mob) {
        $query = $this->db->query('SELECT `rat`.`rating_section1`,`rat`.`rating_section2`,`rat`.`rating_section3`,`rat`.`rating_section4`,`rat`.`rating_section5`,`rat`.`comment`,`us`.`id` as `user_id`, `us`.`user_name`, `inter`.`id` as `interview_id`,`inter`.`creation_date`, `inter`.`type`, `inter`.`community`,CONCAT("' . base_url('uploads') . '/users/profile/",us.user_pic) AS user_pic,CONCAT("' . base_url('uploads') . '/mergefiles/",inter.merge_file) AS mearge_file from `nc_rating` `rat` JOIN `nc_interview` `inter` ON `inter`.`id` = `rat`.`auto_interview_id` JOIN `users` `us` ON `inter`.`user_id` = `us`.`id` WHERE `rateby_user_id`= "' . $userID . '"');
        return $query->result();
    }

}

?>
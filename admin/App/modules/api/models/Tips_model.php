<?php

/**
 * Created by PhpStorm.
 * User: Sanjay Yadav<yadav.sanjay@orangemantra.com>
 * Date: 08/12/2015
 * Time: 05:58 PM
 */
class Tips_model extends CI_Model {

    var $tips_table = "nc_tips";

    function __construct() {
        parent::__construct();
    }

    /**
     * @param null $ID
     * @return mixed data
     * @date : 04/12/2015
     * @description : Get Tips data
     */
    public function getAllTips($ID = null) {
        if ($ID != null) {
            $this->db->where('tips_id', $ID);
        }
        $this->db->where('is_active', '1');
        $this->db->select('tips_id,tips_title,tips_description,tips_cat');
        $this->db->from($this->tips_table);
        $result = $this->db->get();
        return $result->result();
    }

}

?>
<?php

/**
 * Created by PhpStorm.
 * User: Sanjay Yadav<yadav.sanjay@orangemantra.com>
 * Date: 09/12/2015
 * Time: 11:28 AM
 */
class Question_model extends CI_Model {

    var $question_table = "nc_questions";
    var $inter_table = "nc_interview";
    var $sample_table = "nc_sample_interview";

    function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Kolkata');
    }

    /**
     * @param null $ID
     * @return mixed data
     * @date : 04/12/2015
     * @description : Get Question data
     */
    public function getAllQuestions($data = null) {
        if (isset($data['industry']) && $data['industry'] != '' && $data['industry'] != null)
            $this->db->where('question_industry', $data['industry']);

        if (isset($data['function']) && $data['function'] != '' && $data['function'] != null)
            $this->db->where('question_function', $data['function']);

        if (isset($data['question_type']) && $data['question_type'] != '' && $data['question_type'] != null)
            $this->db->where('question_type', $data['question_type']);
        $this->db->order_by('question_id', 'random');
        $this->db->where('question_status', '1');
        $this->db->select('*,CONCAT("' . base_url('uploads') . '/questions/interview/",question_file) AS question_file');
        $this->db->from($this->question_table);
        $this->db->limit('5');
        $result = $this->db->get();
        $queryresult = $result->result();
        shuffle($queryresult);
        return $queryresult;
    }

    public function validateInterview($user_id, $type) {
        $date = date("Y-m-d H:i:s");
        $data = array(
            'user_id' => $user_id,
            'status' => '1',
            'type' => $type,
            'creation_date'=>$date
        );
        if (validate_user_interview($user_id)) {
            $data['interview_id'] = validate_user_interview($user_id) + 1;
        } else {
            $data['interview_id'] = '1';
        }
        $date = date("Y-m-d H:i:s");
        //$this->db->set('creation_date', $date , false);
        $this->db->insert($this->inter_table, $data);
        $id = $this->db->insert_id();
        $interview_id = array();
        $interview_id['interview_id'] = $data['interview_id'];
        $interview_id['id'] = $id;
        return $interview_id;
    }

    public function get_sample_result($type) {
        if ($type != '') {
            $this->db->where('question_type', $type);
        }
        $this->db->select('*,CONCAT("' . base_url('uploads') . '/questions/sample/",question_file) AS  question_file');
        $this->db->from($this->sample_table);
        $result = $this->db->get();
        return $result->result();
    }

}

?>
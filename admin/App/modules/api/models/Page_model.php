<?php

/**
 * Created by PhpStorm.
 * User: Sanjay Yadav<yadav.sanjay@orangemantra.com>
 * Date: 18/12/2015
 * Time: 05:58 PM
 */
class Page_model extends CI_Model {

    var $table = "pages";

    function __construct() {
        parent::__construct();
    }

    /**
     * @param null $ID
     * @return mixed data
     * @date : 04/12/2015
     * @description : Get Tips data
     */
    public function getContent($slug = null) {
        if ($slug != null) {
            $this->db->where('alias', $slug);
        }
        $this->db->where('is_active', '1');
        $this->db->select('description');
        $this->db->from($this->table);
        $result = $this->db->get();
        return $result->row();
    }

}

?>
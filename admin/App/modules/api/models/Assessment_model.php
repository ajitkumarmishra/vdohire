<?php

/**
 * Created by PhpStorm.
 * User: Sanjay Yadav<yadav.sanjay@orangemantra.com>
 * Date: 11/12/2015
 * Time: 11:28 AM
 */
class Assessment_model extends CI_Model {

    var $assess_table = "nc_assesments";
    var $inter_table = "nc_assessment_interview";
    var $ans_table = "nc_assessment_answers";

    function __construct() {
        parent::__construct();
    }

    /**
     * @param null $ID
     * @return mixed data
     * @date : 04/12/2015
     * @description : Get Question data
     */
    public function getAllQuestions($data = null) {
        if (isset($data['industry']) && $data['industry'] != '' && $data['industry'] != null)
            $this->db->where('question_industry', $data['industry']);

        if (isset($data['function']) && $data['function'] != '' && $data['function'] != null)
            $this->db->where('question_function', $data['function']);

        $this->db->order_by('question_id', 'random');

        $this->db->where('question_status', '1');
        //$this->db->select('question_id,question_industry,question_function,question_type,experience,question_title,question_duration,question_ans,question_ans_duration,question_status,question_creation_date,question_modification_date,CONCAT("' . base_url('uploads') . '/questions/assessments/",question_file) AS question_file');
        $this->db->select('*,CONCAT("' . base_url('uploads') . '/questions/assessments/",question_file) AS question_file');
        $this->db->from($this->assess_table);
        $this->db->limit(5);
        $result = $this->db->get();
        return $result->result();
    }

    public function getAllQuestionoption($data = null) {
        if (isset($data['industry']) && $data['industry'] != '' && $data['industry'] != null)
            $this->db->where('question_industry', $data['industry']);

        if (isset($data['function']) && $data['function'] != '' && $data['function'] != null)
            $this->db->where('question_function', $data['function']);

        $this->db->where('question_status', '1');
        $this->db->select('question_ans_one,question_ans_two,question_ans_three,question_ans_four');
        $this->db->from($this->assess_table);
        $this->db->limit(5);
        $result = $this->db->get();
        return $result->result();
    }

    public function validateAssessment($user_id, $type) {

        $data = array(
            'user_id' => $user_id,
            'status' => '1',
            'type' => $type
        );
        if (validate_user_assessment_interview($user_id)) {
            $data['interview_id'] = validate_user_assessment_interview($user_id) + 1;
        } else {
            $data['interview_id'] = '1';
        }
        $this->db->set('creation_date', 'NOW()', false);
        $this->db->insert($this->inter_table, $data);
        $interview_id = $data['interview_id'];
        return $interview_id;
    }

    public function insert_assess($data) {
        try {
            $this->db->insert_batch($this->ans_table, $data);
            $message = array(
                'status' => true,
                'message' => "Success",
                'response_code' => '1'
            );
        } catch (Exception $ex) {
            $message = array(
                'status' => false,
                'response_code' => '0',
                'message' => $ex->getMessage(),
            );
        }
        return $message;
    }

}

?>
<?php

/**
 * Created by PhpStorm.
 * User: Parmod Bhardwaj<parmod@orangemantra.com>
 * Date: 10/13/2015
 * Time: 3:36 PM
 */
class Product_model extends CI_Model {

    var $product_table = "product";

    /**
     * Load Constructor Model
     */
    function __construct() {
        parent::__construct();
        $this->load->library("cimongo/cimongo");
    }

    /**
     * @param null $ID
     * @return mixed data
     * @date : 13/10/15
     * @description : get product data
     */
    public function getProducts($data = null) {
        try {
            if ($ID != null && $ID != 0) {
                $this->db->where('material_id', $ID);
            }
            if (!empty($data)) {
                if (isset($data['id'])) {
                    $this->db->where('material_id', $data['id']);
                } else if (isset($data['barcode'])) {
                    $this->db->where('barcode', $data['barcode']);
                }
            }
            $this->db->where('material_status', "1");
            $this->db->from($this->product_table);
            $result = $this->db->get();
            //echo $this->db->last_query();die;
            return $result->result();
        } catch (Exception $e) {
            log_message('Error', 'error =' . $e->getMessage());
            return false;
        }
    }

    /**
     * @method : add Product
     * @param $data
     * @return bool
     * @date : 13/10/15
     * @description : Add new product in the product table
     */
    public function addProduct($data) {
        try {
            $this->load->library('form_validation');
            $config = array(
                array('field' => 'barcode', 'label' => 'Barcode', 'rules' => 'required|min_length[10]|is_unique[product.barcode]'),
                array('field' => 'material_desc', 'label' => 'Material Desc', 'rules' => 'required')
            );
            $this->form_validation->set_rules($config);
            if ($this->form_validation->run() == false) {
                $errors_array = '';
                foreach ($config as $row) {
                    $field = $row['field'];
                    $error = strip_tags(form_error($field));
                    if ($error)
                        $errors_array .= $error . ', ';
                }
                $message = array(
                    'status' => false,
                    'response_code' => '0',
                    'message' => rtrim($errors_array, ', ')
                );
            } else {
                $this->db->set('material_creation_date', 'NOW()', FALSE);
                $this->db->insert($this->product_table, $data);
                $insert_id = $this->db->insert_id();
                $mongodata = array_merge(array('material_id' => $insert_id), $data);
                $this->cimongo->insert($this->product_table, $mongodata);
                $message = array(
                    'status' => true,
                    'response_code' => '1',
                    'message' => "Success",
                    'product_id' => $insert_id
                );
            }
        } catch (Exception $ex) {
            $message = array(
                'status' => false,
                'response_code' => '0',
                'message' => $ex->getMessage(),
            );
        }
        return $message;
    }

    /**
     * @param $id
     * @param $data
     * @return bool
     * @date : 13/10/15
     * @method : updateProduct
     * @description : To update product
     */
    public function update($id, $data) {
        try {
            $this->load->library('form_validation');
            $config = array(
                array('field' => 'barcode', 'label' => 'Barcode', 'rules' => 'required|min_length[10]'),
                array('field' => 'material_desc', 'label' => 'Material Desc', 'rules' => 'required')
            );
            $this->form_validation->set_rules($config);
            if ($this->form_validation->run() == false) {
                $errors_array = '';
                foreach ($config as $row) {
                    $field = $row['field'];
                    $error = strip_tags(form_error($field));
                    if ($error)
                        $errors_array .= $error . ', ';
                }
                $message = array(
                    'status' => false,
                    'response_code' => '0',
                    'message' => rtrim($errors_array, ', ')
                );
            } else {
                $this->db->where('material_id', $id);
                $this->db->set('material_modification_date', 'NOW()', FALSE);
                $query = $this->db->update($this->product_table, $data);
                $this->cimongo->update($this->product_table,$data,array('material_id'=>$id));
                $message = array(
                    'status' => true,
                    'response_code' => '1',
                    'message' => "Success"
                );
            }
        } catch (Exception $ex) {
            $message = array(
                'status' => false,
                'response_code' => '0',
                'message' => $ex->getMessage(),
            );
        }
        return $message;
    }

    /**
     * @param $id
     * @return bool
     * @date : 13/10/15
     * @method :  Delete Product
     * @description : To delete product from Product
     */
    public function delete($id) {
        if (isset($id) && $id != '') {
            $this->db->where('material_id', $id);
            $query = $this->db->delete($this->product_table);
            $this->cimongo->delete($this->product_table, array('material_id' => $id));
            if ($query) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

}

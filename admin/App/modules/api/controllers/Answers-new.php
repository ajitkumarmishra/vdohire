<?php

/**
 * Created by PhpStorm.
 * User: Sanjay Yadav<yadav.sanjay@orangemantra.com>
 * Date: 09/12/2015
 * Time: 11:28 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Answers extends REST_Controller {

    function __construct() {

        // Construct the parent class
        parent::__construct();
        $this->load->model('answer_model', 'ans');
        $this->load->model('user_model');
    }

    /**
     * @method : list Questions using GET Method
     * @method description: call to get list of Questions.
     * @param : question_id
     * @data: Question Data
     */
    public function list_get() {
        $data = $this->get();
        foreach ($data as $k => $v) {
            log_message('info', 'data=' . $k . '=' . $v . ',');
        }
        if (isset($data['user_id']) && $data['user_id'] != '' && $data['user_id'] != null) {
            $interview_id = $this->ques->validateInterview($data['user_id']);
            $questions = $this->ques->getAllQuestions($data);
            if (!empty($questions)) {
                $this->set_response([
                    'status' => true,
                    'response_code' => '1',
                    'interview_id' => $interview_id,
                    'data' => $questions
                        ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            } else {
                $this->set_response([
                    'status' => false,
                    'response_code' => '0',
                    'message' => 'No Content'
                        ], REST_Controller::HTTP_NO_CONTENT); // NOT_FOUND (404) being the HTTP response code
            }
        } else {
            $this->set_response([
                'status' => false,
                'response_code' => '0',
                'message' => 'Please Send User ID'
                    ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function add_post() {

        $data = $this->post();
        $stat = $data['finalstatus'];
        $finalstatus = $this->ans->setInterview_status($data['interview_id'], $data['finalstatus']);
        foreach ($data as $k => $v) {
            log_message('info', 'data=' . $k . '=' . $v . ',');
        }
        if (isset($data['user_id']) && $data['user_id'] != '' && $data['user_id'] != null) {
            $checkAnswer = $this->ans->CheckAnswer($data);
            if ($checkAnswer) {
                $file = $this->ans->upload_answer();
                if ($file['file_name'] != '' && $file['file_name'] != null) {
                    $updata = array(
                        'interview_id' => $this->post('interview_id'),
                        'question_id' => $this->post('question_id'),
                        'user_id' => $this->post('user_id'),
                        'answer_file' => $file['file_name'],
                        'answer_status' => '1',
                    );
                    $create = $this->ans->addAnswer($updata);
                    if ($stat == '1') {
                        $this->mergeFile($data['interview_id'], $data['user_id']);
                    }
                    if ($create['status']) {
                        $status = REST_Controller::HTTP_OK;
                    } else {
                        $status = REST_Controller::HTTP_OK;
                    }
                    $this->set_response($create, $status);
                } else {
                    $this->set_response([
                        'status' => false,
                        'response_code' => '0',
                        'message' => 'No Answer File Recieved'
                            ], REST_Controller::HTTP_OK);
                }
            } else {
                $this->set_response([
                    'status' => true,
                    'response_code' => '1',
                    'message' => 'Already Answered'
                        ], REST_Controller::HTTP_OK);
            }
        } else {
            $this->set_response([
                'status' => false,
                'response_code' => '0',
                'message' => 'Please Send User ID'
                    ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function mergeFile($intID, $userID, $interview_type = 'Audio') {
        $answer = $this->user_model->getAllinterviewid($intID, $userID);
        $IntType = $this->ans->GetType($intID, $userID);
        $interview_type = $IntType->type;
        $qus = array();
        foreach ($answer as $value) {

            $answer_file = IMAGESPATH . 'answers/' . $value->answer_file;

            $question_id = $value->question_id;
            $getquestion = $this->user_model->getquestion($question_id);

            $qution = $getquestion[0]['question_file'];
            $question_file = IMAGESPATH . 'questions/interview/' . $qution;

            $qus[] = $question_file;
            $qus[] = $answer_file;
        }

        if ($interview_type == 'Video') {
            $conertMp4FilesString;
            $mp4arr = array();
            $unlinkarr = array();
            foreach ($qus as $mp4):
                $nm = md5(uniqid(rand(), true)) . '.ts';
                $filename = IMAGESPATH . 'convert/' . $nm;
                $mp4arr[] = $filename;
                $unlinkarr[] = $nm;
                exec("/usr/local/bin/ffmpeg -i $mp4 -c copy -bsf:v h264_mp4toannexb -f mpegts $filename");
            endforeach;
            $conertMp4FilesString = implode("|", $mp4arr);
            $fname = md5(uniqid(rand(), true)) . '.mp4';
            $output = IMAGESPATH . 'mergefiles/' . $fname;
            exec('/usr/local/bin/ffmpeg -i "concat:'.$conertMp4FilesString.'" -c copy -bsf:a aac_adtstoasc '. $output);
            foreach ($unlinkarr as $mpgf) {
                $name = IMAGESPATH . 'convert/' . $mpgf;
                unlink($name);
            }
        } else {
            $conertMp4FilesString;
            $mp4arr = array();
            $unlinkarr = array();
            foreach ($qus as $mp4):
                $nm = md5(uniqid(rand(), true)) . '.mpg';
                $filename = IMAGESPATH . 'convert/' . $nm;
                $mp4arr[] = $filename;
                $unlinkarr[] = $nm;
                exec("/usr/local/bin/ffmpeg -i $mp4 -qscale 0 $filename");
            endforeach;
            $conertMp4FilesString = implode(" ", $mp4arr);
            $fname = md5(uniqid(rand(), true)) . '.mp4';
            $output = IMAGESPATH . 'mergefiles/' . $fname;
            exec("cat $conertMp4FilesString | /usr/local/bin/ffmpeg -f mpeg -i - -qscale 0 -vcodec mpeg4 $output");
            foreach ($unlinkarr as $mpgf) {
                $name = IMAGESPATH . 'convert/' . $mpgf;
                unlink($name);
            }
        }
        $update = $this->user_model->update_marge_file($intID, $fname, $userID);
    }

    public function mergetest_post() {
        $this->mergeFileTest('2', '70');
    }
    
    public function mergeFileTest($intID, $userID, $interview_type = 'Audio') {
        $answer = $this->user_model->getAllinterviewid($intID, $userID);
        $IntType = $this->ans->GetType($intID, $userID);
        $interview_type = $IntType->type;
        $qus = array();
        foreach ($answer as $value) {

            //$answer_file = IMAGESPATH . 'answers/' . $value->answer_file;
            $answer_file = IMAGESPATH . 'answers/' . $value->answer_file;

            $question_id = $value->question_id;
            $getquestion = $this->user_model->getquestion($question_id);

            $qution = $getquestion[0]['question_file'];
            $question_file = IMAGESPATH . 'questions/interview/' . $qution;

            $qus[] = $question_file;
            $qus[] = $answer_file;
        }
		//print_r($qus);die;
        if ($interview_type == 'Video') {
            $conertMp4FilesString;
            $mp4arr = array();
            $unlinkarr = array();
            foreach ($qus as $mp4):
                $nm = md5(uniqid(rand(), true)) . '.mpg';
                $filename = IMAGESPATH . 'convert/' . $nm;
                $mp4arr[] = $filename;
                $unlinkarr[] = $nm;
                //exec("/usr/local/bin/ffmpeg -i $mp4 -c copy -bsf:v h264_mp4toannexb -f mpegts $filename");
                exec("/usr/local/bin/ffmpeg -i $mp4 -qscale 0 $filename");
            endforeach;
            $conertMp4FilesString = implode(" ", $mp4arr);
            $fname = md5(uniqid(rand(), true)) . '.mp4';
            $output = IMAGESPATH . 'mergefiles/' . $fname; 
            //exec('/usr/local/bin/ffmpeg -i "concat:'.$conertMp4FilesString.'" -c copy -bsf:a aac_adtstoasc '. $output);
            exec('/usr/local/bin/ffmpeg -i "concat:'.$conertMp4FilesString.'" -c copy -bsf:a aac_adtstoasc '. $output);
            foreach ($unlinkarr as $mpgf) {
                $name = IMAGESPATH . 'convert/' . $mpgf;
                unlink($name);
            }
        } else {
            $conertMp4FilesString;
            $mp4arr = array();
            $unlinkarr = array();
            foreach ($qus as $mp4):
                $nm = md5(uniqid(rand(), true)) . '.mpg';
                $filename = IMAGESPATH . 'convert/' . $nm;
                $mp4arr[] = $filename;
                $unlinkarr[] = $nm;
                exec("/usr/local/bin/ffmpeg -i $mp4 -qscale 0 $filename");
            endforeach;
            $conertMp4FilesString = implode(" ", $mp4arr);
            $fname = md5(uniqid(rand(), true)) . '.mp4';
            $output = IMAGESPATH . 'mergefiles/' . $fname;
            exec("cat $conertMp4FilesString | /usr/local/bin/ffmpeg -f mpeg -i - -qscale 0 -vcodec mpeg4 $output");
            /*foreach ($unlinkarr as $mpgf) {
                $name = IMAGESPATH . 'convert/' . $mpgf;
                unlink($name);
            }*/
        }
        echo $output;
        $update = $this->user_model->update_marge_file($intID, $fname, $userID);
    }

}

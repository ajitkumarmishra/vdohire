<?php

/**
 * Created by PhpStorm.
 * User: Sanjay Yadav<yadav.sanjay@orangemantra.com>
 * Date: 04/12/2015
 * Time: 11:28 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class User extends REST_Controller {

    function __construct() {

        // Construct the parent class
        parent::__construct();
        $this->load->model('user_model');
        $this->load->helper('string');
    }

    /**
     * @method : list Customer using GET Method
     * @method description: call to get list of Customers.
     * @param : customer_id
     * @data: Customer Data
     */
    public function list_get() {
        $id = (int) $this->get('id');
        log_message('info', 'data=' . $id);
        $users = $this->user_model->getAllUsers($id);

        if (!empty($users)) {
            $this->set_response([
                'status' => true,
                'response_code' => '1',
                'message' => 'Success',
                'data' => $users
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {
            $this->set_response([
                'status' => FALSE,
                'response_code' => '0',
                'message' => 'User could not be found'
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function add_post() {

        foreach ((($this->post())) as $key => $value) {
            log_message('info', 'data=' . $key . ' =>' . $value);
        }
        $posted_data = $this->post('');
        $randcode = random_string('numeric', 16);
        $random_code = substr($randcode, 0, 4);
        $data = array(
            'user_email' => $this->post('user_email'),
            'user_pass' => md5($this->post('user_pass')),
            'user_name' => $this->post('user_name'),
            'user_nicename' => $this->slugify($this->post('user_name')),
            'user_dob' => $this->input->post('user_dob'),
            'user_mobile' => $this->post('user_mobile'),
            'user_gender' => $this->post('user_gender'),
            'random_code' => $random_code,
            'status' => '1'
        );

        $create = $this->user_model->addAnswer($data);
        if ($create['status']) {
            $status = REST_Controller::HTTP_OK;
        } else {
            $status = REST_Controller::HTTP_OK;
        }
        $this->set_response($create, $status);
    }

    /**
     * @method : Update Customer using Post Method
     * @method description: Update.
     * @param : customer_id, Data array
     * @data: Customer Data
     */
    public function update_post() {

        $id = (int) $this->get('id');
        if ($id <= 0) {
            $message = [
                'response_code' => '0',
                'message' => 'Failed !'
            ];
            $this->response($message, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
        if (isset($id) && $id != '') {
            $update = $this->user_model->update($id, $this->post());
            if ($update['status']) {
                $status = REST_Controller::HTTP_OK;
            } else {
                $status = REST_Controller::HTTP_OK;
            }
            $this->set_response($update, $status);
        } else {

            $message = [
                'response_code' => '0',
                'message' => 'Id Not Found'
            ];
            $this->set_response($message, REST_Controller::HTTP_OK);
        }
    }

    /**
     * @method : Delete Customer using DELETE Method
     * @method description: delete  Customer.
     * @param : customer__id
     * @data:
     */
    public function drop_delete() {
        $id = (int) $this->get('id');

        // Validate the id.
        if ($id <= '0') {
            // Set the response and exit
            $this->response(NULL, REST_Controller::HTTP_OK);    // BAD_REQUEST (400) being the HTTP response code
        }
        $result = $this->user_model->delete($id);

        if ($result) {
            $message = [
                'response_code' => '1',
                'message' => 'User Deleted'
            ];

            $this->set_response($message, REST_Controller::HTTP_OK);
        } else {
            $message = [
                'response_code' => '0',
                'message' => 'Bad request'
            ];

            $this->set_response($message, REST_Controller::HTTP_OK);
        }
    }

    static public function slugify($text) {
        $text = preg_replace('~[^\\pL\d]+~u', '_', $text);
        $text = trim($text, '-');
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        $text = strtolower($text);
        $text = preg_replace('~[^-\w]+~', '', $text);
        if (empty($text)) {
            return 'n-a';
        }
        return $text;
    }

    function signin_post() {
        $user_email = $this->post('user_email', true);
        $password = $this->post('user_pass', true);
        $new_reg_id = $this->input->post('reg_id');
        $type = $this->input->post('type');
        $userdata = $this->user_model->validate($user_email, md5($password));
        //Validation
        if ($userdata) {

            $checkios = $this->user_model->checkreg_id_ios('users', $user_email);
            $reg_id = json_decode($checkios->reg_id);
            if (!empty($reg_id)) {
                if (!in_array($new_reg_id, $reg_id)) {
                    $reg_id[] = $new_reg_id;
                    $this->user_model->updateregid_ios('users', $user_email, $reg_id);
                }
            } else {
                $reg_id[] = $new_reg_id;
                $this->user_model->updateregid_ios('users', $user_email, $reg_id);
            }


            $userid = $userdata->id;
            $user_name = $userdata->user_name;
            $user_pic = ($userdata->user_pic != '' && $userdata->user_pic != null ) ? base_url() . '/uploads/users/profile/' . $userdata->user_pic : NULL;
            if ($userdata->status == 0) {
                $message = [
                    'response_code' => '0',
                    'message' => 'Not validated!'
                ];
                $this->set_response($message, REST_Controller::HTTP_OK);
            } else {
                $message = [
                    'user_id' => $userid,
                    'user_name' => $user_name,
                    'user_pic' => $user_pic,
                    'response_code' => '1',
                    'message' => 'Success'
                ];

                $this->set_response($message, REST_Controller::HTTP_OK);
            }
        } else {
            $message = [
                'response_code' => '0',
                'message' => 'Invalid Credentials'
            ];

            $this->set_response($message, REST_Controller::HTTP_OK);
        }

        return;

        $data['main_content'] = 'signin';
        $this->load->view('full_width', $data);
    }

    public function logout_post() {

        $reg_id = $this->post('reg_id');
        $user_id = $this->post('user_id');


        if ($user_id != '' && $reg_id != '') {
            $this->user_model->updaterlogout('users', $user_id, $reg_id);
            $dataforapp = array("data" => array('response' => 'logoutsuccfull', 'userid' => $user_id), 'status' => 1);
            $message = [
                'response_code' => '1',
                'message' => 'Log out Successfully'
            ];
            $this->set_response($message, REST_Controller::HTTP_OK);
        } else {
            $dataforapp = array("data" => array('response' => 'Enter reg id and user id'), 'status' => 0);
            $message = [
                'response_code' => '0',
                'message' => 'Invalid Credentials'
            ];
            $this->set_response($message, REST_Controller::HTTP_OK);
        }
    }

    public function forgotpassword_post() {
        foreach ((($this->post())) as $key => $value) {
            log_message('info', 'data=' . $key . ' =>' . $value);
        }


        $data = array(
            'user_email' => $this->post('user_email'),
            'user_mobile' => $this->post('user_mobile')
        );

        $create = $this->user_model->validate_password($data);

        if ($create['status']) {
            $status = REST_Controller::HTTP_OK;
            $this->set_response($create, $status);
        } else {
            $message = [
                'response_code' => '0',
                'message' => $create['message']
            ];
            $this->set_response($message, REST_Controller::HTTP_OK);
        }
    }

    // change password 
    function changepassword_post() {
        $user_id = $this->post('user_id', true);
        $current_pass = $this->post('current_pass', true);
        $new_pass = $this->post('new_pass', true);

        if ($user_id != '') {
            $userpasswrod = $this->user_model->valid_current_pass($user_id, md5($current_pass));
            //Validation
            //$checkid = $userpasswrod->id;
            if (!$userpasswrod) {
                $message = [
                    'response_code' => '0',
                    'message' => 'Invalid Current Password'
                ];
                $this->set_response($message, REST_Controller::HTTP_OK);
            } else {

                $this->user_model->updatepassword('users', $user_id, $new_pass);
                $message = [
                    'response_code' => '1',
                    'message' => 'Success',
                    'new_password' => $new_pass
                ];

                $this->set_response($message, REST_Controller::HTTP_OK);
            }
        } else {
            $message = [
                'response_code' => '0',
                'message' => 'Invalid Credentials'
            ];

            $this->set_response($message, REST_Controller::HTTP_OK);
        }

        return;

        $data['main_content'] = 'signin';
        $this->load->view('full_width', $data);
    }

    protected function changeprofilepic_post() {
        $user_id = (int) $this->post('user_id');
        if (isset($user_id) && $user_id != '' && $user_id != null) {
            $update = $this->user_model->updateprofile($user_id);
            if ($update) {
                $message = [
                    'response_code' => '1',
                    'message' => 'Success',
                    'user_pic' => $update
                ];

                $this->set_response($message, REST_Controller::HTTP_OK);
            } else {
                $message = [
                    'response_code' => '0',
                    'message' => 'Not Updated'
                ];

                $this->set_response($message, REST_Controller::HTTP_OK);
            }
        } else {
            $message = [
                'response_code' => '0',
                'message' => 'Invalid User'
            ];

            $this->set_response($message, REST_Controller::HTTP_OK);
        }
    }

    // update file interview table
    public function listen_interview_post() {
        try {
            $interview_id = $this->post('interview_id');
            $interview_type = $this->post('type');
            $user_id = $this->post('user_id');
            if ($interview_id <= '0') {
                $message = [
                    'response_code' => '0',
                    'message' => 'Failed !'
                ];
                $this->response($message, REST_Controller::HTTP_BAD_REQUEST);
            }
            if (isset($interview_id) && $interview_id != '' && $user_id != '') {
                $update = $this->user_model->Listenfile($interview_id, $user_id);
                $status = REST_Controller::HTTP_OK;
                $this->set_response($update, $status);
            } else {

                $message = [
                    'response_code' => '0',
                    'message' => 'User ID Not Found'
                ];
                $this->set_response($message, REST_Controller::HTTP_NOT_ACCEPTABLE);
            }
        } catch (Exception $ex) {

            $message = array(
                'status' => false,
                'response_code' => '0',
                'message' => $ex->getMessage(),
            );
        }
    }

    public function my_interview_post() {
        try {
            $user_id = $this->post('user_id');
            $type = $this->post('type') ? $this->post('type') : 'Audio';
            $users_result = $this->user_model->get_interview_result($user_id, $type);
            $users_details = $this->user_model->get_details_result($user_id);
            if (!empty($users_result) && !empty($users_details)) {
                $userid = $users_details[0]->id;
                $userpic = base_url() . 'uploads/users/profile/' . $users_details[0]->user_pic;
                $this->set_response(['status' => true, 'response_code' => '1', 'message' => 'Success', 'user_id' => $userid,
                    'data' => $users_result], REST_Controller::HTTP_OK);
            } else {
                $this->set_response(['status' => FALSE, 'response_code' => '0', 'message' => 'No Content'
                        ], REST_Controller::HTTP_OK);
            }
        } catch (Exception $ex) {

            $message = array(
                'status' => false,
                'response_code' => '0',
                'message' => $ex->getMessage(),
            );
        }
    }

    public function community_post() {
        $data = $this->post();
        $id = $data['user_id'];
        if ($id <= 0) {
            $message = [
                'response_code' => '0',
                'message' => 'Failed !'
            ];
            $this->response($message, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
        if (isset($id) && $id != '') {
            if ($data['community'] != null and $data['community'] != '') {
                $update = $this->user_model->updateCommunity($data);
                if ($update['status']) {
                    $status = REST_Controller::HTTP_OK;
                } else {
                    $status = REST_Controller::HTTP_OK;
                }
                $this->set_response($update, $status);
            } else {
                $message = [
                    'response_code' => '0',
                    'message' => 'Invalid Credentials'
                ];
                $this->set_response($message, REST_Controller::HTTP_OK);
            }
        } else {
            $message = [
                'response_code' => '0',
                'message' => 'Id Not Found'
            ];
            $this->set_response($message, REST_Controller::HTTP_OK);
        }
    }

    public function community_interview_post() {
        try {
            //$user_id = $this->post('user_id');
            $community = $this->post('community');
            $type = $this->post('type') ? $this->post('type') : 'Audio';
            $users_result = $this->user_model->get_community_result($community, $type);
            //$users_details = $this->user_model->get_details_result($user_id);
            if (!empty($users_result)) {
                //$userid = $users_details[0]->id;
                //$userpic = base_url() . 'uploads/users/profile/' . $users_details[0]->user_pic;
                $this->set_response(['status' => true, 'response_code' => '1', 'message' => 'Success',
                    'data' => $users_result], REST_Controller::HTTP_OK);
            } else {
                $this->set_response(['status' => FALSE, 'response_code' => '0', 'message' => 'No result'
                        ], REST_Controller::HTTP_OK);
            }
        } catch (Exception $ex) {

            $message = array(
                'status' => false,
                'response_code' => '0',
                'message' => $ex->getMessage(),
            );
        }
    }

    public function interview_delete_post() {
        $data = $this->post();
        $det = $this->user_model->GetDetInterview($this->post('interview_id'));
        if (isset($data['interview_id'])) {
            $del = $this->user_model->delete_interview($data);
            $detail = array(
                'interview_id' => $det->interview_id,
                'user_id' => $det->user_id
            );
            $check = $this->user_model->delete_interview_answer($detail);
            if ($del) {
                $message = array(
                    'status' => true,
                    'response_code' => '1'
                );
                $this->set_response($message, REST_Controller::HTTP_OK);
            } else {
                $message = array(
                    'status' => false,
                    'response_code' => '0'
                );
                $this->set_response($message, REST_Controller::HTTP_OK);
            }
        } else {
            $message = array(
                'status' => false,
                'response_code' => '0',
                'message' => 'Invalid Credentials'
            );
            $this->set_response($message, REST_Controller::HTTP_OK);
        }
    }

    public function rules_post() {
        $data = $this->post();
        $update = $this->user_model->getRule($data);
        if ($update['status']) {
            $status = REST_Controller::HTTP_OK;
        } else {
            $status = REST_Controller::HTTP_OK;
        }
        $this->set_response($update, $status);
    }

    public function search_interview_post() {
        try {
            $get_id = $this->post('id');

            if ($this->post('user_id')) {
                $u_id = $this->post('user_id');
                $mob = $this->user_model->GetUserMobile($u_id);
            }

            if ($get_id <= '0') {
                $message = [
                    'response_code' => '0',
                    'message' => 'Failed !'
                ];
                $this->response($message, REST_Controller::HTTP_BAD_REQUEST);
            }
            if (isset($get_id) && $get_id != '') {
                $getdetails = $this->user_model->getuser_interview_id($get_id);

                $status = REST_Controller::HTTP_OK;
                if (!empty($getdetails)) {
                    $update = array(
                        'status' => '1',
                        'data' => $getdetails
                    );
                } else {
                    $update = array(
                        'status' => '0',
                        'data' => 'No Inerview !'
                    );
                }
                if ($mob) {
                    $con = array(
                        'mobile' => $mob,
                        'interview_id' => $get_id
                    );
                    $invite_check = $this->user_model->CheckInvite($con);
                    if ($invite_check) {
                        $update['invite_status'] = true;
                    } else {
                        $update['invite_status'] = false;
                    }
                } else {
                    $update['invite_status'] = false;
                }

                $this->set_response($update, $status);
            } else {

                $message = [
                    'response_code' => '0',
                    'message' => 'User ID Not Found'
                ];
                $this->set_response($message, REST_Controller::HTTP_NOT_ACCEPTABLE);
            }
        } catch (Exception $ex) {

            $message = array(
                'status' => false,
                'response_code' => '0',
                'message' => $ex->getMessage(),
            );
        }
    }

    public function webinar_post() {
        try {
            $type = $this->post('type');
            $get_type = $this->post('file_type');

            if ($get_type == '' && $type == '') {

                $message = [
                    'response_code' => '0',
                    'message' => 'Failed !'
                ];
                $this->response($message, REST_Controller::HTTP_BAD_REQUEST);
            }
            if (isset($get_type) && $get_type != '') {
                $getweb = $this->user_model->getwebinar($type, $get_type);


                $status = REST_Controller::HTTP_OK;
                if (!empty($getweb)) {
                    $update = array(
                        'status' => '1',
                        'data' => $getweb
                    );
                } else {
                    $update = array(
                        'status' => '0',
                        'data' => 'No Webinar !'
                    );
                }
                $this->set_response($update, $status);
            } else {

                $message = [
                    'response_code' => '0',
                    'message' => 'Not Found'
                ];
                $this->set_response($message, REST_Controller::HTTP_NOT_ACCEPTABLE);
            }
        } catch (Exception $ex) {

            $message = array(
                'status' => false,
                'response_code' => '0',
                'message' => $ex->getMessage(),
            );
        }
    }

    // view user rating details 
    public function user_rating_post() {
        try {
            $get_id = $this->post('id');

            if ($get_id <= '0') {
                $message = [
                    'response_code' => '0',
                    'message' => 'Failed !'
                ];
                $this->response($message, REST_Controller::HTTP_BAD_REQUEST);
            }
            if (isset($get_id) && $get_id != '') {
                $getdetails = $this->user_model->getuser_rating($get_id);
                $userid = $this->user_model->getuserid('nc_rating', $get_id);

                $status = REST_Controller::HTTP_OK;
                if (!empty($getdetails) && !empty($userid)) {

                    $user = $userid->user_id;
                    $update = array(
                        'status' => '1',
                        'response_code' => '1',
                        'user_id' => $user,
                        'auto_interview_id' => $get_id,
                        'message' => 'Success',
                        'data' => $getdetails
                    );
                } else {
                    $update = array(
                        'status' => '0',
                        'response_code' => '0',
                        'data' => 'No Rating !',
                        'message' => 'No Rating !'
                    );
                }
                $this->set_response($update, $status);
            } else {

                $message = [
                    'response_code' => '0',
                    'message' => 'User ID Not Found'
                ];
                $this->set_response($message, REST_Controller::HTTP_NOT_ACCEPTABLE);
            }
        } catch (Exception $ex) {

            $message = array(
                'status' => false,
                'response_code' => '0',
                'message' => $ex->getMessage(),
            );
        }
    }

    // add rating for particular user
    public function add_user_rating_post() {

        foreach ((($this->post())) as $key => $value) {
            log_message('info', 'data=' . $key . ' =>' . $value);
        }
        $posted_data = $this->post('');
        $mob = $this->user_model->GetUserMobile($this->post('user_id'));
        $data = array(
            'user_id' => $this->post('user_id'),
            'auto_interview_id' => $this->post('auto_interview_id'),
            'rateby_user_id' => $this->post('rateby_user_id'),
            'rating_section1' => $this->post('rating1'),
            'rating_section2' => $this->post('rating2'),
            'rating_section3' => $this->post('rating3'),
            'rating_section4' => $this->post('rating4'),
            'rating_section5' => $this->post('rating5'),
            'comment' => $this->post('comment'),
            'status' => '1'
        );

        $create = $this->user_model->addrating($data);
        if ($create['status']) {
            $res = array(
                'user_id' => $this->post('user_id'),
                'rateby_user_id' => $this->post('rateby_user_id'),
                'interview_id' => $this->post('auto_interview_id'),
                'mobile' => $mob
            );
            $this->user_model->review_notification($res);
        }
        if ($create['status']) {
            $status = REST_Controller::HTTP_OK;
        } else {
            $status = REST_Controller::HTTP_OK;
        }
        $this->set_response($create, $status);
    }

    public function invite_post() {

        foreach ((($this->post())) as $key => $value) {
            log_message('info', 'data=' . $key . ' =>' . $value);
        }
        $data = $this->post();
        $check_invite = $this->user_model->CheckInvite($data);
        if (!$check_invite) {
            $create = $this->user_model->addInvite($data);
            if ($create) {
                $this->user_model->invite_notification($data);
                $message = array(
                    'status' => true,
                    'response_code' => '1',
                    'message' => 'Successfully Invited!'
                );
            } else {
                $message = array(
                    'status' => false,
                    'response_code' => '0',
                    'message' => 'Failed'
                );
            }
        } else {
            $message = array(
                'status' => false,
                'response_code' => '0',
                'message' => 'Already Invited'
            );
        }

        $this->set_response($message, Rest_Controller::HTTP_OK);
    }

    public function notifications_post() {
        foreach ((($this->post())) as $key => $value) {
            log_message('info', 'data=' . $key . ' =>' . $value);
        }
        $data = $this->post();
        $mob = $this->user_model->GetUserMobile($data['user_id']);
        if ($data['type'] == 'pending') {
            $listing = $this->user_model->GetPendingInvite($data['user_id'], $mob);
        } else {
            $listing = $this->user_model->GetReviewedInvite($data['user_id'], $mob);
        }
        if (empty($listing)) {
            $message = array(
                'status' => false,
                'response_code' => '0',
                'message' => 'No Listing'
            );
        } else {
            $message = array(
                'status' => true,
                'response_code' => '1',
                'data' => $listing
            );
        }
        $this->set_response($message, Rest_Controller::HTTP_OK);
    }

}

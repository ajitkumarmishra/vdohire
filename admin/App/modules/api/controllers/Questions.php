<?php

/**
 * Created by PhpStorm.
 * User: Sanjay Yadav<yadav.sanjay@orangemantra.com>
 * Date: 09/12/2015
 * Time: 11:28 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Questions extends REST_Controller {

    function __construct() {

        // Construct the parent class
        parent::__construct();
        date_default_timezone_set('Asia/Kolkata');
        $this->load->model('question_model', 'ques');
    }

    /**
     * @method : list Questions using GET Method
     * @method description: call to get list of Questions.
     * @param : question_id
     * @data: Question Data
     */
    public function list_get() {
        $data = $this->get();
        foreach ($data as $k => $v) {
            log_message('info', 'data=' . $k . '=' . $v . ',');
        }
        if (isset($data['user_id']) && $data['user_id'] != '' && $data['user_id'] != null) {
            $interview_id = $this->ques->validateInterview($data['user_id']);            $interviewid = $interview_id['interview_id']; 			$insert_id = $interview_id['id'];
            $questions = $this->ques->getAllQuestions($data);
            if (!empty($questions)) {
                $this->set_response([
                    'status' => true,
                    'response_code' => '1',
                    'interview_id' => $interviewid,										'id' => $insert_id,
                    'data' => $questions
                        ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            } else {
                $this->set_response([
                    'status' => false,
                    'response_code' => '0',
                    'message' => 'No Content'
                        ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        } else {
            $this->set_response([
                'status' => false,
                'response_code' => '0',
                'message' => 'Please Send User ID'
                    ], REST_Controller::HTTP_OK);
        }
    }
    
    public function list_post() {
        $data = $this->post();
        foreach ($data as $k => $v) {
            log_message('info', 'data=' . $k . '=' . $v . ',');
        }
        if (isset($data['user_id']) && $data['user_id'] != '' && $data['user_id'] != null) {
            $interview_id = $this->ques->validateInterview($data['user_id'],$data['question_type']);
            $interviewid = $interview_id['interview_id']; 
            $insert_id = $interview_id['id']; 
            $questions = $this->ques->getAllQuestions($data);
            if (!empty($questions)) {
                $this->set_response([
                    'status' => true,
                    'response_code' => '1',
                    'interview_id' => $interviewid,
                    'id' => $insert_id,
                    'data' => $questions
                        ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            } else {
                $this->set_response([
                    'status' => false,
                    'response_code' => '0',
                    'message' => 'No Content'
                        ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        } else {
            $this->set_response([
                'status' => false,
                'response_code' => '0',
                'message' => 'Please Send User ID'
                    ], REST_Controller::HTTP_OK);
        }
    }
    
    public function sample_interview_post() {
        try {
            $type=$this->post('type')?$this->post('type'):'Audio';
            $result = $this->ques->get_sample_result($type);
            if (!empty($result)) {
                //$userpic = base_url() . 'uploads/users/profile/' . $users_details[0]->user_pic;
                $this->set_response(['status' => true, 'response_code' => '1', 'message' => 'Success',
                    'data' => $result], REST_Controller::HTTP_OK);
            } else {
                $this->set_response(['status' => FALSE, 'response_code' => '0', 'message' => 'No Content'
                        ], REST_Controller::HTTP_OK);
            }
        } catch (Exception $ex) {

            $message = array(
                'status' => false,
                'response_code' => '0',
                'message' => $ex->getMessage(),
            );
        }
    }

}

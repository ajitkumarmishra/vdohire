<?php

/**
 * Created by PhpStorm.
 * User: Sanjay Yadav<yadav.sanjay@orangemantra.com>
 * Date: 08/12/2015
 * Time: 05:28 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Tips extends REST_Controller {

    function __construct() {

        // Construct the parent class
        parent::__construct();
        $this->load->model('tips_model','tips');
    }

    /**
     * @method : list Tips using GET Method
     * @method description: call to get list of Tips.
     * @param : customer_id
     * @data: Tips Data
     */
    public function list_get() {
        $id = (int) $this->get('id');
        log_message('info', 'data=' . $id);
        $tips = $this->tips->getAllTips($id);
        
        if (!empty($tips)) {
            $this->set_response([
                'response_code' => '1',
                'data' => $tips
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {
            $this->set_response([
                'response_code' => '0',
                'message' => 'No Content'
                    ], REST_Controller::HTTP_NO_CONTENT); // NOT_FOUND (404) being the HTTP response code
        }
    }

}

<?php

/**
 * Created by PhpStorm.
 * User: Parmod Bhardwaj<parmod@orangemantra.com>
 * Date: 10/13/2015
 * Time: 3:28 PM
 */
require APPPATH . '/libraries/REST_Controller.php';

class Forum extends REST_Controller {

    /**
     * @throws Exception
     */
    public function __construct() {
        // Construct the parent class
        parent::__construct();
        $this->load->model('forum_model');
    }

    /**
     * @method : list product using GET Method
     * @method description: casll to get list of product.
     * @param : product_id
     * @data: product Data
     */
    public function list_get() {
        $id = (int) $this->get('id');
        log_message('info', 'data=' . $id);
        $forums = $this->forum_model->getAllForums($id);

        if (!empty($forums)) {
            $this->set_response([
                'response_code' => '1',
                'data' => $forums
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {
            $this->set_response([
                'response_code' => '0',
                'message' => 'No Content'
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }
    /**
     * @method : lisst product using GET Method
     * @method description: caill to get list of product.
     * @param : product_id
     * @data: product Data
     */
    public function list_comments_get() {
        $id = (int) $this->get('id');
        log_message('info', 'data=' . $id);
        $comments = $this->forum_model->getAllComments($id);

        if (!empty($comments)) {
            $this->set_response([
                'response_code' => '1',
                'data' => $comments
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {
            $this->set_response([
                'response_code' => '0',
                'message' => 'No Comment'
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }

    /**
     * @method : Add product using Post Method
     * @method description : call to add new product.
     * @param :Data  Array
     * @data: product Data
     */
    public function add_post() {

        foreach ((($this->post())) as $key => $value) {
            log_message('info', 'data=' . $key . ' =>' . $value);
        }
        $posted_data = $this->post('');
        $data = array(
            'user_id' => $this->post('user_id'),
            'forum_title' => $this->post('forum_title'),
            'forum_desc' => $this->post('forum_desc'),
            'forum_status' => 1
        );

        $create = $this->forum_model->addforum($data);
        if ($create['status']) {
            $status = REST_Controller::HTTP_OK;
        } else {
            $status = REST_Controller::HTTP_BAD_REQUEST;
        }
        $this->set_response($create, $status);
    }

    // create comments 

    public function comments_post() {

        foreach ((($this->post())) as $key => $value) {
            log_message('info', 'data=' . $key . ' =>' . $value);
        }
        $posted_data = $this->post('');        $user_id = $this->post('user_id');
        $data = array(
            'user_id' => $this->post('user_id'),
            'forum_id' => $this->post('forum_id'),
            'comment_title' => $this->post('comment_title'),
            'comment_des' => $this->post('comment_desc'),
            'comment_status' => 1
        );

        $create = $this->forum_model->addcomments($data);        
        if ($create['status']) {
            $status = REST_Controller::HTTP_OK;
        } else {
            $status = REST_Controller::HTTP_BAD_REQUEST;
        }
        $this->set_response($create, $status);
    }

    /**
     * @method : Update product using Post Method
     * @method description: Update.
     * @param : product_id, Data array
     * @data: product Data
     */
    public function update_post() {

        $id = (int) $this->get('id');
        if ($id <= 0) {
            $message = [
                'Status' => false,
                'response_code' => '0',
                'message' => 'Failed !'
            ];
            $this->response($message, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
        if (isset($id) && $id != '') {
            $update = $this->product_model->update($id, $this->post());
            if ($update['status']) {
                $status = REST_Controller::HTTP_OK;
            } else {
                $status = REST_Controller::HTTP_NOT_MODIFIED;
            }
            $this->set_response($update, $status);
        } else {

            $message = [
                'Status' => false,
                'response_code' => '0',
                'message' => 'Vendor Id Not Found'
            ];
            $this->set_response($message, REST_Controller::HTTP_NOT_ACCEPTABLE);
        }
    }

    /**
     * @method : Delete product using DELETE Method
     * @method description: delete  product.
     * @param : product_id
     * @data:
     */
    public function drop_delete() {
        $id = (int) $this->get('id');

        // Validate the id.
        if ($id <= 0) {
            // Set the response and exit
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
        $result = $this->product_model->delete($id);

        if ($result) {
            $message = [
                'status' => true,
                'response_code' => '1',
                'message' => 'Vendor Deleted'
            ];

            $this->set_response($message, REST_Controller::HTTP_OK); // NO_CONTENT (204) being the HTTP response code
        } else {
            $message = [
                'status' => false,
                'response_code' => '0',
                'message' => 'Bad request'
            ];

            $this->set_response($message, REST_Controller::HTTP_BAD_REQUEST); // NO_CONTENT (204) being the HTTP response code
        }
    }

}

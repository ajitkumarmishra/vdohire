<?php

/**
 * Created by PhpStorm.
 * User: Sanjay Yadav<yadav.sanjay@orangemantra.com>
 * Date: 09/12/2015
 * Time: 11:28 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Industry extends REST_Controller {

    function __construct() {

        // Construct the parent class
        parent::__construct();
        $this->load->model('industry_model', 'indus');
    }

    /**
     * @method : list Customer using GET Method
     * @method description: call to get list of Customers.
     * @param : customer_id
     * @data: Customer Data
     */
    public function list_get() {
        $id = (int) $this->get('id');
        log_message('info', 'data=' . $id);
        $industry = $this->indus->getAll($id);

        if (!empty($industry)) {
            $this->set_response([
                'status'=>true,
                'response_code' => '1',
                'data' => $industry
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {
            $this->set_response([
                'status'=>false,
                'response_code' => '0',
                'message' => 'No Content'
                    ], REST_Controller::HTTP_NO_CONTENT); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function functions_get() {
        $id = (int) $this->get('id');
        log_message('info', 'data=' . $id);
        $functions = $this->indus->getFunctions($id);

        if (!empty($functions)) {
            $this->set_response([
                'status'=>true,
                'response_code' => '1',
                'data' => $functions
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {
            $this->set_response([
                'status'=>false,
                'response_code' => '0',
                'message' => 'No Content'
                    ], REST_Controller::HTTP_NO_CONTENT); // NOT_FOUND (404) being the HTTP response code
        }
    }

}

<?php

/**
 * Created by PhpStorm.
 * User: Parmod Bhardwaj<parmod@orangemantra.com>
 * Date: 10/13/2015
 * Time: 3:28 PM
 */
require APPPATH . '/libraries/REST_Controller.php';

class Product extends REST_Controller {

    /**
     * @throws Exception
     */
    public function __construct() {
        // Construct the parent class
        parent::__construct();
        $this->load->model('product_model');
    }

    /**
     * @method : list product using GET Method
     * @method description: call to get list of product.
     * @param : product_id
     * @data: product Data
     */
    public function list_get() {

        //$id = (int) $this->get('id');
        $data = array();
        if ($this->get('id') != '')
            $data['id'] = (int) $this->get('id');
        if ($this->get('barcode'))
            $data['barcode'] = $this->get('barcode');

        $product = $this->product_model->getProducts($data);

        if (!empty($product)) {
            $this->set_response(['status' => true,
                'message' => 'Success',
                'response_code' => '1',
                'data' => $product], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {
            $this->set_response([
                'status' => FALSE,
                'response_code' => '0',
                'message' => 'Vendor could not be found'
                    ], REST_Controller::HTTP_NO_CONTENT); // NOT_FOUND (404) being the HTTP response code
        }
    }

    /**
     * @method : Add product using Post Method
     * @method description : call to add new product.
     * @param :Data  Array
     * @data: product Data
     */
    public function add_post() {

        foreach ((($this->post())) as $key => $value) {
            log_message('info', 'data=' . $key . ' =>' . $value);
        }
        $posted_data = $this->post('');
        $data = array(
            'barcode' => $this->post('barcode'),
            'material_desc' => $this->post('material_desc'),
            'material_mrp' => $this->post('material_mrp'),
            'material_sp' => $this->post('material_sp'),
            'material_pp' => $this->post('material_pp'),
            'material_class1' => $this->post('material_class1'),
            'material_class2' => $this->post('material_class2'),
            'material_class3' => $this->post('material_class3'),
            'material_tax_id' => $this->post('material_tax_id'),
            'manufacturer' => $this->post('manufacturer'),
            'brand' => $this->post('brand'),
            'material_pack_unit1' => $this->post('material_pack_unit1'),
            'material_pack_unit2' => $this->post('material_pack_unit2'),
            'material_pack_conversion' => $this->post('material_pack_conversion'),
            'material_strength' => $this->post('material_strength'),
            'unit_of_strength' => $this->post('unit_of_strength'),
            'material_measure' => $this->post('material_measure'),
            'material_uom' => $this->post('material_uom'),
            'material_code' => $this->post('material_code'),
            'is_prescription_relevant' => $this->post('is_prescription_relevant'),
            'is_batch_relevant' => $this->post('is_batch_relevant'),
            'material_drug_category' => $this->post('material_drug_category'),
            'material_schedule' => $this->post('material_schedule'),
            'is_deleted' => 0,
            'material_status' => 1
        );

        $create = $this->product_model->addProduct($data);
        if ($create['status']) {
            $status = REST_Controller::HTTP_OK;
        } else {
            $status = REST_Controller::HTTP_BAD_REQUEST;
        }
        $this->set_response($create, $status);
    }

    /**
     * @method : Update product using Post Method
     * @method description: Update.
     * @param : product_id, Data array
     * @data: product Data
     */
    public function update_post() {

        $id = (int) $this->get('id');
        if ($id <= 0) {
            $message = [
                'Status' => false,
                'response_code' => '0',
                'message' => 'Failed !'
            ];
            $this->response($message, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
        if (isset($id) && $id != '') {
            $update = $this->product_model->update($id, $this->post());
            if ($update['status']) {
                $status = REST_Controller::HTTP_OK;
            } else {
                $status = REST_Controller::HTTP_NOT_MODIFIED;
            }
            $this->set_response($update, $status);
        } else {

            $message = [
                'Status' => false,
                'response_code' => '0',
                'message' => 'Vendor Id Not Found'
            ];
            $this->set_response($message, REST_Controller::HTTP_NOT_ACCEPTABLE);
        }
    }

    /**
     * @method : Delete product using DELETE Method
     * @method description: delete  product.
     * @param : product_id
     * @data:
     */
    public function drop_delete() {
        $id = (int) $this->get('id');

        // Validate the id.
        if ($id <= 0) {
            // Set the response and exit
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
        $result = $this->product_model->delete($id);

        if ($result) {
            $message = [
                'status' => true,
                'response_code' => '1',
                'message' => 'Vendor Deleted'
            ];

            $this->set_response($message, REST_Controller::HTTP_OK); // NO_CONTENT (204) being the HTTP response code
        } else {
            $message = [
                'status' => false,
                'response_code' => '0',
                'message' => 'Bad request'
            ];

            $this->set_response($message, REST_Controller::HTTP_BAD_REQUEST); // NO_CONTENT (204) being the HTTP response code
        }
    }

}

<?php

/**
 * Created by PhpStorm.
 * User: Sanjay Yadav<yadav.sanjay@orangemantra.com>
 * Date: 09/12/2015
 * Time: 11:28 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Comprehensive extends REST_Controller {

    function __construct() {

        // Construct the parent class
        parent::__construct();
        $this->load->model('assessment_model', 'assess');
    }

    /**
     * @method : list Questions using GET Method
     * @method description: call to get list of Questions.
     * @param : question_id
     * @data: Question Data
     */
    public function list_get() {
        $data = $this->get();
        foreach ($data as $k => $v) {
            log_message('info', 'data=' . $k . '=' . $v . ',');
        }
        //if (isset($data['user_id']) && $data['user_id'] != '' && $data['user_id'] != null) {
            //$interview_id = $this->assess->validateAssessment($data['user_id']);
            $questions = $this->assess->getAllQuestions($data);
            if (!empty($questions)) {
                $this->set_response([
                    'status' => true,
                    'response_code' => '1',
                    'interview_id' => $interview_id,
                    'data' => $questions
                        ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            } else {
                $this->set_response([
                    'status' => false,
                    'response_code' => '0',
                    'message' => 'No Content'
                        ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
//        } else {
//            $this->set_response([
//                'status' => false,
//                'response_code' => '0',
//                'message' => 'Please Send User ID'
//                    ], REST_Controller::HTTP_OK);
//        }
    }

    public function list_post() {
        $data = $this->post();
        foreach ($data as $k => $v) {
            log_message('info', 'data=' . $k . '=' . $v . ',');
        }
        if (isset($data['user_id']) && $data['user_id'] != '' && $data['user_id'] != null) {
            $interview_id = $this->assess->validateAssessment($data['user_id'], $data['question_type']);
            $questions = $this->assess->getAllQuestions($data);

            $out = array();
            foreach ($questions as $key => $value) {
                $ans = array();
                $ans['options'][]['option'] = $value->question_ans_one;
                $ans['options'][]['option'] = $value->question_ans_two;
                $ans['options'][]['option'] = $value->question_ans_three;
                $ans['options'][]['option'] = $value->question_ans_four;
                $out[] = (object) array_merge($ans, (array) $value);
            }
            shuffle($out);
            if (!empty($questions)) {
                $this->set_response([
                    'status' => true,
                    'response_code' => '1',
                    'interview_id' => $interview_id,
                    'data' => $out
                        ], REST_Controller::HTTP_OK);
            } else {
                $this->set_response([
                    'status' => false,
                    'response_code' => '0',
                    'message' => 'No Content'
                        ], REST_Controller::HTTP_OK);
            }
        } else {
            $this->set_response([
                'status' => false,
                'response_code' => '0',
                'message' => 'Please Send User ID'
                    ], REST_Controller::HTTP_OK);
        }
    }

    public function assessment_answers_post() {
        foreach ((($this->post())) as $key => $value) {
            log_message('info', 'data=' . $key . ' =>' . $value);
        }
        $posted_data = $decode = json_decode($this->post('answer'), true);
        $insert_data = array();
        if ($decode['user_id'] == null && $decode['user_id'] == '') {
            $this->set_response([
                'status' => false,
                'response_code' => '0',
                'message' => 'Please Send User ID'
                    ], REST_Controller::HTTP_OK);
        } else {
            foreach ($posted_data['answers'] as $ansKey => $ansValue) {
                $insert_data[$ansKey] = $ansValue;
                $insert_data[$ansKey]['user_id'] = $decode['user_id'];
                $insert_data[$ansKey]['interview_id'] = $decode['interview_id'];
            }
            $create = $this->assess->insert_assess($insert_data);
            if ($create['status']) {
                $status = REST_Controller::HTTP_OK;
            } else {
                $status = REST_Controller::HTTP_OK;
            }
            $this->set_response($create, $status);
        }
    }

}

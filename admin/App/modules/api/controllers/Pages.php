<?php

/**
 * Created by PhpStorm.
 * User: Sanjay Yadav<yadav.sanjay@orangemantra.com>
 * Date: 04/12/2015
 * Time: 11:28 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Pages extends REST_Controller {

    function __construct() {

        // Construct the parent class
        parent::__construct();
        $this->load->model('page_model', 'page');
        $this->load->helper('string');
    }

    /**
     * @method : list Customer using GET Method
     * @method description: call to get list of Customers.
     * @param : customer_id
     * @data: Customer Data
     */
    public function list_get() {
        $slug = $this->get('slug');
        log_message('info', 'data=' . $slug);
        $page = $this->page->getContent($slug);
        if ($slug != '' && isset($slug)) {
            if (!empty($page)) {
                $this->set_response([
                    'status' => true,
                    'response_code' => '1',
                    'message' => 'Success',
                    'data' => $page->description
                        ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            } else {
                $this->set_response([
                    'status' => FALSE,
                    'response_code' => '0',
                    'message' => 'Page could not be found'
                        ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        } else {
            $this->set_response([
                'status' => FALSE,
                'response_code' => '0',
                'message' => 'Please Send Slug for the page'
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }

}

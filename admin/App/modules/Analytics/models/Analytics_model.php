<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Analytics Model
 * Description : Handle all the CRUD operation for Analytics
 * @author Synergy
 * @createddate : Nov 1, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 15, 2017
 */
class Analytics_model extends CI_Model {

    /**
     * Responsable for auto load the the session and pagination library
     * @return void
     */
    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('pagination');
    }

    /**
     * Description : Return total number of Job Posted and Number of Open Job Posts(Jobs Status fj-report-1)
     * Author : Synergy
     * @param $date1, $date2
     * @return int(Active jobs count) 
     */
    function totalActiveJobs($date1,$date2){
        try {
            $userData   = $this->session->userdata['logged_in'];
            $userRole   = $userData->role;
            $sql    = "SELECT count(id) AS activeCount FROM fj_jobs ";
            if($date1!='' && $date2!='') {
                $date1 = date('Y-m-d', strtotime($date1));
                $date2 = date('Y-m-d', strtotime($date2));
                //$sql .= " WHERE (DATE(openTill) BETWEEN '$date1' AND '$date2' AND  openTill>=CURDATE()";
                $sql .= " WHERE (DATE(createdAt) BETWEEN '$date1' AND '$date2') ";
                $sql .= " AND (DATE(openTill)>=CURDATE()";
                $sql .= " AND status!='3')";
            }
            else {
                $sql .= " WHERE (DATE(openTill)>=CURDATE()";
                $sql .= " AND status!='3')";
            }
            
            if($userRole=='2'){                
                $corporateId    = getCorporateUser($userData->id);
                $subUserIds     = getCorporateSubUser($corporateId, $userData->role);
                $subUserIds     = ($subUserIds!=''?','.$subUserIds:'');
                $sql .= " AND createdBy IN ('".$corporateId."'".$subUserIds.")";
            }
            $query  = $this->db->query($sql);
            $result = $query->row_array();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Description : Return total number of Inactive Jobs Posted by Loggedin user
     * Author : Synergy
     * @param $date1, $date2
     * @return int(Inactive jobs count) 
     */
    function totalInactiveJobs($date1,$date2){
        try {
            $userData   = $this->session->userdata['logged_in'];
            $userRole   = $userData->role;
            //print_r($userData);
            $sql    = "SELECT count(id) AS inactiveCount FROM fj_jobs ";
            if($date1!='' && $date2!='') {
                $date1 = date('Y-m-d', strtotime($date1));
                $date2 = date('Y-m-d', strtotime($date2));
                //$sql .= " WHERE (DATE(openTill) BETWEEN '$date1' AND '$date2' AND  openTill<=CURDATE()";
                $sql .= " WHERE (DATE(createdAt) BETWEEN '$date1' AND '$date2') ";
                $sql .= " AND (DATE(openTill)<=CURDATE()";
                $sql .= " AND status!='3')";
            }
            else {
                $sql .= " WHERE (DATE(openTill)<=CURDATE()";
                $sql .= " AND status!='3')";
            }
            
            if($userRole=='2'){                
                $corporateId    = getCorporateUser($userData->id);
                $subUserIds     = getCorporateSubUser($corporateId, $userData->role);
                $subUserIds     = ($subUserIds!=''?','.$subUserIds:'');
                $sql .= " AND createdBy IN ('".$corporateId."'".$subUserIds.")";
            }
            $query  = $this->db->query($sql);
            $result = $query->row_array();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Description : Return total active Jobs and number of active jobs Posted by Loggedin user
     * Author : Synergy
     * @param $date1, $date2
     * @return array data 
     */
    function totalJobApplication($date1,$date2){
        try {
            $userData   = $this->session->userdata['logged_in'];
            $userRole   = $userData->role;
            $sql    = "
                    SELECT 
                        J.id,
                        J.title,
                        J.fjCode,
                        UJ.userId,
                        DATE(UJ.createdAt) AS createdAt,
                        COUNT(J.id) AS applicationRecieved
                    FROM 
                        fj_jobs		J
                    LEFT JOIN
                        fj_userJob	UJ
                    ON
                        J.id=UJ.jobId 
                    WHERE
                        1=1 ";
            if($date1!='' && $date2!='') {
                $date1 = date('Y-m-d', strtotime($date1));
                $date2 = date('Y-m-d', strtotime($date2));
                //$sql .= " AND (DATE(J.openTill) BETWEEN '$date1' AND '$date2' AND  J.openTill>=CURDATE()";
                $sql .= " AND (DATE(J.createdAt) BETWEEN '$date1' AND '$date2') ";
                $sql .= " AND (DATE(J.openTill)>=CURDATE()";
                $sql .= " AND J.status!='3')";
            }
            else {
                $sql .= " AND (DATE(J.openTill)>=CURDATE()";
                $sql .= " AND J.status!='3')";
            }
            
            if($userRole=='2'){                
                $corporateId    = getCorporateUser($userData->id);
                $subUserIds     = getCorporateSubUser($corporateId, $userData->role);
                $subUserIds     = ($subUserIds!=''?','.$subUserIds:'');
                $sql .= " AND J.createdBy IN ('".$corporateId."'".$subUserIds.")";
            }
            
            $sql .= "  GROUP BY DATE(UJ.createdAt), J.id";
            $sql .= "  ORDER BY J.fjCode, UJ.createdAt";
            
            $query  = $this->db->query($sql);
            $result = $query->result_array();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Description : Return pending applications at each user level(Pending Application Status fj-report-2)
     * Author : Synergy
     * @param $date1, $date2
     * @return array data 
     */
    function pendingApplication($date1,$date2){
        try {
            $userData   = $this->session->userdata['logged_in'];
            $userRole   = $userData->role;
            $sql    = "
                        SELECT 
                            UJ.id, 
                            UJ.userId   AS appUserId, 
                            UJ.jobId, 
                            J.title, 
                            J.createdBy AS corporateId, 
                            U.fullname  AS corporateName,
                            UJ.status,
                            UJ.shorlistedByL1User,
                            UJ.shorlistedByL2User
                        FROM 
                            fj_userJob  UJ
                        JOIN 
                            fj_jobs     J, 
                            fj_users    U
                        WHERE 
                            UJ.jobId=J.id   AND 
                            J.createdBy=U.id";
            if($date1!='' && $date2!='') {
                $date1 = date('Y-m-d', strtotime($date1));
                $date2 = date('Y-m-d', strtotime($date2));
                $sql .= " AND DATE(UJ.createdAt) BETWEEN '$date1' AND '$date2'";
            }
            else {
                $sql .= " AND DATE(UJ.createdAt)<=CURDATE()";
            }
            
            
            if($userRole=='2'){                
                $corporateId    = getCorporateUser($userData->id);
                $subUserIds     = getCorporateSubUser($corporateId, $userData->role);
                $subUserIds     = ($subUserIds!=''?','.$subUserIds:'');
                $sql .= " AND J.createdBy IN ('".$corporateId."'".$subUserIds.")";
            }
            $query  = $this->db->query($sql);
            $result = $query->result_array();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Description : Return cost per shortlisted (Cost of Hire)
     * Author : Synergy
     * @param $date1, $date2
     * @return array data 
     */
    function shorlistCost($date1,$date2){
        try {
            $userData   = $this->session->userdata['logged_in'];
            $userRole   = $userData->role;
            $sql    = "
                        SELECT 
                            UJ.id, 
                            UJ.userId   AS appUserId, 
                            UJ.jobId, 
                            J.title, 
                            J.createdBy AS corporateId, 
                            U.fullname  AS corporateName,
                            UJ.status,
                            UJ.shorlistedByL1User,
                            UJ.shorlistedByL2User
                        FROM 
                            fj_userJob  UJ
                        JOIN 
                            fj_jobs     J, 
                            fj_users    U
                        WHERE 
                            UJ.jobId=J.id       AND 
                            J.createdBy=U.id    AND 
                            J.createdBy='$userId'
                     ";
            if($date1!='' && $date2!='') {
                $date1 = date('Y-m-d', strtotime($date1));
                $date2 = date('Y-m-d', strtotime($date2));
                $sql .= " AND DATE(UJ.createdAt) BETWEEN '$date1' AND '$date2'";
            }
            else {
                $sql .= " AND DATE(UJ.createdAt)<=CURDATE()";
            }
            $query  = $this->db->query($sql);
            $result = $query->result_array();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Description : Return ratio of shortlist. It is used on (Shortlist Ratio fj-report-9, Applicants demographics Status fj-report-12, Sub User Performance fj-report-7, Applicant Funnel fj-report-5 && fj-report-6, Application Received / Shortlisted Ratio, Application Viewed (Shortlisted + Rejected) / Shortlisted ratio)
     * Author : Synergy
     * @param $date1, $date2
     * @return array data 
     */
    function applicationReceivedShortlistRatio($date1,$date2){
        try {
            $userData   = $this->session->userdata['logged_in'];
            $userRole   = $userData->role;
            $sql    = "
                        SELECT
                            UJ.id,
                            (CASE WHEN AU.gender='1' THEN 'Male' ELSE 'Female' END) AS gender,
                            AU.pincode,
                            UJ.userId   AS appUserId,
                            UJ.jobId,
                            J.fjCode,
                            UJ.createdAt AS applicationReceivedDate,
                            J.title,
                            J.createdAt AS jobCreatedAt,
                            J.createdBy AS corporateId,
                            U.fullname  AS corporateName,
                            UJ.status,
                            UJ.shorlistedByL1User,
                            UJ.shorlistedByL1UserDate,
                            UJ.shorlistedByL2User,
                            UJ.shorlistedByL2UserDate
                        FROM
                            fj_userJob  UJ
                        JOIN
                            fj_jobs     J,
                            fj_users    U,
                            fj_users    AU
                        WHERE
                            UJ.jobId=J.id       AND
                            AU.id=UJ.userId		AND
                            J.createdBy=U.id
                     ";
            if($date1!='' && $date2!='') {
                $date1 = date('Y-m-d', strtotime($date1));
                $date2 = date('Y-m-d', strtotime($date2));
                //$sql .= " AND DATE(UJ.createdAt) BETWEEN '$date1' AND '$date2'";
                
                $sql .= " AND (DATE(UJ.shorlistedByL1UserDate) BETWEEN '$date1' AND '$date2'";                
                $sql .= " OR";
                $sql .= " DATE(UJ.shorlistedByL2UserDate) BETWEEN '$date1' AND '$date2')";
            }
            else {
                $sql .= " AND DATE(UJ.createdAt)<=CURDATE()";
            }
            
            if($userRole=='2'){
                $corporateId    = getCorporateUser($userData->id);
                $subUserIds     = getCorporateSubUser($corporateId, $userData->role);
                $subUserIds     = ($subUserIds!=''?','.$subUserIds:'');
                $sql .= " AND J.createdBy IN ('".$corporateId."'".$subUserIds.")";
            }
            $query  = $this->db->query($sql);
            $result = $query->result_array();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Description : Return number of application viewed(Pending + shortlisted + Rejected) on jobs by corporate or corporate sub user(Sub User Performance fj-report-7)
     * Author : Synergy
     * @param none
     * @return array data 
     */
    function jopPanelist(){
        try {
            $userData   = $this->session->userdata['logged_in'];
            $userRole   = $userData->role;
            $sql    = " SELECT Distinct(JP.userId), U.fullname, count(jobId) AS countJob, GROUP_CONCAT(DISTINCT(jobId)) AS jobIds
                        FROM fj_jobPanel JP
                        JOIN fj_users U
                        WHERE JP.userId = U.id
                        ";            
            
            $corporateId    = getCorporateUser($userData->id);
            $subUserIds     = getCorporateSubUser($corporateId, $userData->role);
            $subUserIds     = ($subUserIds!=''?','.$subUserIds:'');
            
            $sql    .= " AND U.createdBy IN ('".$corporateId."'".$subUserIds.")";
            $sql    .= " Group By JP.userId ";
            $query  = $this->db->query($sql);
            $result = $query->result_array();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Description : Return total number of views of a particular jobs and multiple views from a candidate is counted separately(fj-report-13)
     * Author : Synergy
     * @param $date1,$date2
     * @return array of data 
     */
    function jobOpenRate($date1,$date2){
        try {
            $userData   = $this->session->userdata['logged_in'];
            $userRole   = $userData->role;

            $sql    = "
                    SELECT
                        J.id,
                        J.title,
                        J.fjCode,
                        (SELECT COUNT(jobId) FROM fj_jobTracker WHERE pageVisitBy='jobSearch' AND jobId=J.id ) AS jobSearchCount,
                        (SELECT COUNT(jobId) FROM fj_jobTracker WHERE pageVisitBy='jobCode' AND jobId=J.id ) AS jobCodeCount,
                        DATE(J.createdAt) AS createdAt,
                        COUNT(J.id) AS applicationRecieved
                    FROM 
                        fj_jobs		J
                    LEFT JOIN
                        fj_jobTracker	JT
                    ON
                        J.id=JT.jobId 
                    WHERE
                        1=1 ";
            if($date1!='' && $date2!='') {
                $date1 = date('Y-m-d', strtotime($date1));
                $date2 = date('Y-m-d', strtotime($date2));
                $sql .= " AND (DATE(J.openTill) BETWEEN '$date1' AND '$date2'";
                $sql .= " AND J.status!='3')";
            }
            else {
                $sql .= " AND (J.status!='3')";
            }
            
            if($userRole=='2'){                
                $corporateId    = getCorporateUser($userData->id);
                $subUserIds     = getCorporateSubUser($corporateId, $userData->role);
                $subUserIds     = ($subUserIds!=''?','.$subUserIds:'');
                $sql .= " AND J.createdBy IN ('".$corporateId."'".$subUserIds.")";
            }
            
            $sql .= " GROUP BY J.id ORDER BY J.id";          
            
            $query  = $this->db->query($sql);
            $result = $query->result_array();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Description : Return job wise shortlist candidate like Time Per Shortlisted (fj-report-11, fj-report-14) For L1 User
     * Author : Synergy
     * @param array(date1, date2)
     * @return array of data 
     */
    function timeToHireL1Shortlisted($date1,$date2){
        try {  
            $userData   = $this->session->userdata['logged_in'];
            $userRole   = $userData->role;          
            $sql    = " 
                        SELECT 
                            allRecord.*
                        FROM 
                            (
                            SELECT
                                aliasTable.createdAt,
                                aliasTable.shorlistedByL1UserDate,
                                aliasTable.shorlistedByL2UserDate,
                                aliasTable.shorlistedByCorporateForL1User,
                                aliasTable.shorlistedByCorporateForL2User,
                                aliasTable.id,
                                aliasTable.AppUserName,
                                aliasTable.AppUserEmail,
                                aliasTable.AppUserMobile,
                                aliasTable.JobCode,
                                aliasTable.JobTitle,
                                aliasTable.JobAplliedStatus,
                                aliasTable.JobLevel,  (CASE WHEN aliasTable.shorlistedByL1User IS NULL THEN '' ELSE L1.fullname END) AS L1UserName,  (CASE WHEN aliasTable.shorlistedByL2User IS NULL THEN '' ELSE L2.fullname END) AS L2UserName,
                                U.fullname AS JobCreatedBy,
                                U.company  AS CompanyName
                            FROM
                                (
                                SELECT 
                                    UJ.shorlistedByCorporateForL1User,
                                    UJ.shorlistedByCorporateForL2User,
                                    UJ.shorlistedByL1UserDate,
                                    UJ.shorlistedByL2UserDate,
                                    UJ.shorlistedByL1User,
                                    UJ.shorlistedByL2User,
                                    UJ.id,
                                    U.fullname  AS AppUserName,
                                    U.email     AS AppUserEmail,
                                    U.mobile    AS AppUserMobile,
                                    J.fjCode    AS JobCode,
                                    J.title     AS JobTitle,
                                    (CASE UJ.status WHEN 1 THEN 'Pending' WHEN 2 THEN 'Shortlisted By L1' WHEN 3 THEN 'Rejected By L1' WHEN 4 THEN 'Shortlisted By L2'  WHEN 5 THEN 'Rejected By L2' ELSE '' END) as JobAplliedStatus,
                                    (CASE COUNT(DISTINCT(JP.level)) WHEN 1 THEN 'L1' WHEN 2 THEN 'L2' ELSE '' END) AS JobLevel,
                                    J.createdBy,
                                    UJ.createdAt                            
                                FROM 
                                    fj_userJob		UJ
                                JOIN 
                                    fj_jobPanel 	JP,
                                    fj_users            U,
                                    fj_jobs        	J
                                WHERE
                                    UJ.jobId=JP.jobId      AND
                                    UJ.userId=U.id      	AND
                                    UJ.jobId=J.id  AND DATE(UJ.createdAt)<=CURDATE() 
                                GROUP BY 
                                    UJ.id ORDER BY UJ.jobId 
                                ) aliasTable   
                            JOIN
                                fj_users U,
                                fj_users L1,
                                fj_users L2
                            WHERE
                                aliasTable.createdBy=U.id AND
                                (
                                    aliasTable.JobLevel='L1' AND aliasTable.JobAplliedStatus='Shortlisted By L1'
                                    OR
                                    aliasTable.JobLevel='L2' AND aliasTable.JobAplliedStatus='Shortlisted By L2'
                                )  AND
                                (CASE WHEN aliasTable.shorlistedByL1User IS NULL THEN '1=1' ELSE aliasTable.shorlistedByL1User=L1.id END)   AND
                                (CASE WHEN aliasTable.shorlistedByL2User IS NULL THEN '1=1' ELSE aliasTable.shorlistedByL2User=L2.id END)   AND
                                (CASE WHEN aliasTable.shorlistedByL1User IS NULL AND aliasTable.shorlistedByCorporateForL1User IS NOT NULL THEN '1=1' ELSE aliasTable.shorlistedByCorporateForL1User=L1.id END)   AND
                                (CASE WHEN aliasTable.shorlistedByL2User IS NULL AND aliasTable.shorlistedByCorporateForL2User IS NOT NULL THEN '1=1' ELSE aliasTable.shorlistedByCorporateForL2User=L2.id END)
                            GROUP BY 
                                aliasTable.id
                            ) allRecord
                        WHERE
                            JobLevel='L1' ";
            //$sql    .= "    AND shorlistedByL1UserDate >= DATE_SUB(DATE(NOW()), INTERVAL DAYOFWEEK(NOW())+6 DAY) AND shorlistedByL1UserDate < DATE_SUB(DATE(NOW()), INTERVAL DAYOFWEEK(NOW())-1 DAY)";
            $sql    .= " ORDER BY shorlistedByL1UserDate DESC ";
            $query  = $this->db->query($sql);
            $result = $query->result_array();            
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Description : Return job wise shortlist candidate like Time Per Shortlisted (fj-report-11 fj-report-14) For L2 User
     * Author : Synergy
     * @param array(date1, date2)
     * @return array of data 
     */
    function timeToHireL2Shortlisted($date1,$date2){
        try {    
            $userData   = $this->session->userdata['logged_in'];
            $userRole   = $userData->role;        
            $sql    = " 
                        SELECT 
                            allRecord.*
                        FROM 
                            (
                            SELECT
                                aliasTable.shorlistedByL1UserDate,
                                aliasTable.shorlistedByL2UserDate,
                                aliasTable.id,
                                aliasTable.AppUserName,
                                aliasTable.AppUserEmail,
                                aliasTable.AppUserMobile,
                                aliasTable.JobCode,
                                aliasTable.JobTitle,
                                aliasTable.JobAplliedStatus,
                                aliasTable.JobLevel,  (CASE WHEN aliasTable.shorlistedByL1User IS NULL THEN '' ELSE L1.fullname END) AS L1UserName,  (CASE WHEN aliasTable.shorlistedByL2User IS NULL THEN '' ELSE L2.fullname END) AS L2UserName,
                                U.fullname AS JobCreatedBy,
                                U.company  AS CompanyName,
                                aliasTable.createdAt
                            FROM
                                (
                                SELECT 
                                    UJ.shorlistedByL1UserDate,
                                    UJ.shorlistedByL2UserDate,
                                    UJ.shorlistedByL1User,
                                    UJ.shorlistedByL2User,
                                    UJ.id,
                                    U.fullname  AS AppUserName,
                                    U.email     AS AppUserEmail,
                                    U.mobile    AS AppUserMobile,
                                    J.fjCode    AS JobCode,
                                    J.title     AS JobTitle,
                                    (CASE UJ.status WHEN 1 THEN 'Pending' WHEN 2 THEN 'Shortlisted By L1' WHEN 3 THEN 'Rejected By L1' WHEN 4 THEN 'Shortlisted By L2'  WHEN 5 THEN 'Rejected By L2' ELSE '' END) as JobAplliedStatus,
                                    (CASE COUNT(DISTINCT(JP.level)) WHEN 1 THEN 'L1' WHEN 2 THEN 'L2' ELSE '' END) AS JobLevel,
                                    J.createdBy,
                                    UJ.createdAt                            
                                FROM 
                                    fj_userJob		UJ
                                JOIN 
                                    fj_jobPanel 	JP,
                                    fj_users            U,
                                    fj_jobs        	J
                                WHERE
                                    UJ.jobId=JP.jobId      AND
                                    UJ.userId=U.id      	AND
                                    UJ.jobId=J.id  AND DATE(UJ.createdAt)<=CURDATE() 
                                GROUP BY 
                                    UJ.id ORDER BY UJ.jobId 
                                ) aliasTable   
                            JOIN
                                fj_users U,
                                fj_users L1,
                                fj_users L2
                            WHERE
                                aliasTable.createdBy=U.id AND
                                (
                                    aliasTable.JobLevel='L1' AND aliasTable.JobAplliedStatus='Shortlisted By L1'
                                    OR
                                    aliasTable.JobLevel='L2' AND aliasTable.JobAplliedStatus='Shortlisted By L2'
                                )  AND
                                (CASE WHEN aliasTable.shorlistedByL1User IS NULL THEN '1=1' ELSE aliasTable.shorlistedByL1User=L1.id END)   AND
                                (CASE WHEN aliasTable.shorlistedByL2User IS NULL THEN '1=1' ELSE aliasTable.shorlistedByL2User=L2.id END)
                            GROUP BY 
                                aliasTable.id
                            ) allRecord
                        WHERE
                            JobLevel='L2' ";
            //$sql    .= "    AND shorlistedByL2UserDate >= DATE_SUB(DATE(NOW()), INTERVAL DAYOFWEEK(NOW())+6 DAY) AND shorlistedByL2UserDate < DATE_SUB(DATE(NOW()), INTERVAL DAYOFWEEK(NOW())-1 DAY)";
            $sql .= " ORDER BY shorlistedByL2UserDate DESC ";
            $query  = $this->db->query($sql);
            $result = $query->result_array();            
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Description : Return all the applications on specific jobs
     * Author : Synergy
     * @param array(date1, date2)
     * @return array of data 
     */
    function applicationViewed($date1,$date2){
        try {
            $userData   = $this->session->userdata['logged_in'];
            $userRole   = $userData->role;
            $sql    = "
                        SELECT 
                            UJ.id, 
                            UJ.userId   AS appUserId, 
                            UJ.jobId, 
                            J.title, 
                            J.createdBy AS corporateId, 
                            U.fullname  AS corporateName,
                            UJ.status,
                            UJ.shorlistedByL1User,
                            UJ.shorlistedByL2User
                        FROM 
                            fj_userJob  UJ
                        JOIN 
                            fj_jobs     J, 
                            fj_users    U
                        WHERE 
                            UJ.jobId=J.id       AND 
                            J.createdBy=U.id    AND 
                            J.createdBy='$userId'
                     ";
            if($date1!='' && $date2!='') {
                $date1 = date('Y-m-d', strtotime($date1));
                $date2 = date('Y-m-d', strtotime($date2));
                $sql .= " AND DATE(UJ.createdAt) BETWEEN '$date1' AND '$date2'";
            }
            else {
                $sql .= " AND DATE(UJ.createdAt)<=CURDATE()";
            }
            $query  = $this->db->query($sql);
            $result = $query->result_array();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Description : Return all the app users
     * Author : Synergy
     * @param array(date1, date2)
     * @return array of data 
     */
    function appUserCSV($date1,$date2){
        try {            
            $sql    = " SELECT 
                            U.id                AS Id,
                            U.fullname          AS UserName,
                            U.email		AS EmailId,
                            U.mobile		AS MobileNumber,
                            (CASE WHEN U.dob='0000-00-00' THEN '' ELSE U.dob END)     AS DateOfBirth,
                            (CASE WHEN U.gender='1' THEN 'Male' WHEN U.gender='2' THEN 'Female' ELSE '' END)    AS Gender,
                            U.facebookLink	AS FacebookUrl,
                            U.linkedInLink	AS LinkedinUrl,	
                            U.twitterLink	AS TwitterUrl,
                            U.googlePlusLink	AS GooglePlusUrl,
                            (
                                SELECT 
                                    GROUP_CONCAT(DISTINCT(C.name)) 
                                FROM 
                                    fj_userQualification UQ 
                                JOIN
                                    fj_courses	C
                                WHERE 
                                    UQ.userId=U.id	AND
                                    UQ.courseId=C.id
                            ) AS CourseName,
                            U.expectedCtcFrom	AS CTC_ExpectedFrom,
                            U.expectedCtcTo	AS CTC_ExpectedTo,
                            U.workExperience	AS WorkExperience,
                            U.adharCard		AS AadharCardNumber,
                            U.pinCode		AS Pincode,
                            U.createdAt         AS RegistrationTime
                        FROM
                            fj_users    U
                        WHERE
                            U.role='3'  ";
                            if($date1!='' && $date2!='') {
                                $date1 = date('Y-m-d', strtotime($date1));
                                $date2 = date('Y-m-d', strtotime($date2));
                                $sql .= " AND DATE(U.createdAt) BETWEEN '$date1' AND '$date2' ";
                            }
                            else {
                                $sql .= " AND DATE(U.createdAt)<=CURDATE()";
                            }
            $query  = $this->db->query($sql);
            $result = $query->result_array();            
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Description : Return all the corporate user list
     * Author : Synergy
     * @param array(date1, date2)
     * @return array of data 
     */
    function corporateUserCSV($date1,$date2){
        try {            
            $sql    = " SELECT 
                            U.id                AS Id,
                            U.fullname          AS UserName,
                            U.email		AS EmailId,
                            U.mobile		AS MobileNumber,
                            (CASE WHEN U.dob='0000-00-00' THEN '' ELSE U.dob END)     AS DateOfBirth,
                            (CASE WHEN U.gender='1' THEN 'Male' WHEN U.gender='2' THEN 'Female' ELSE '' END)    AS Gender,
                            U.location          AS Location,
                            U.company   	AS CompanyName,	
                            U.address           AS CompanyAddress,
                            U.createdAt         AS RegistrationTime
                        FROM
                            fj_users    U
                        WHERE
                            U.role='2'  ";
                            if($date1!='' && $date2!='') {
                                $date1 = date('Y-m-d', strtotime($date1));
                                $date2 = date('Y-m-d', strtotime($date2));
                                $sql .= " AND DATE(U.createdAt) BETWEEN '$date1' AND '$date2' ";
                            }
                            else {
                                $sql .= " AND DATE(U.createdAt)<=CURDATE()";
                            }
            $query  = $this->db->query($sql);
            $result = $query->result_array();            
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Description : Return all the corporate sub user report data
     * Author : Synergy
     * @param array(date1, date2)
     * @return array of data 
     */
    function corporateSubUserCSV($date1,$date2){
        try {            
            $sql    = " SELECT                 
                            U.id                AS Id,
                            (CASE WHEN SU.role='1' THEN '' ELSE SU.fullname END)    AS CorporateName,
                            U.fullname          AS UserName,
                            U.email		AS EmailId,
                            U.mobile		AS MobileNumber,
                            (CASE WHEN U.dob='0000-00-00' THEN '' ELSE U.dob END)     AS DateOfBirth,
                            (CASE WHEN U.gender='1' THEN 'Male' WHEN U.gender='2' THEN 'Female' ELSE '' END)    AS Gender,
                            U.location          AS Location,
                            U.company   	AS CompanyName,	
                            U.address           AS CompanyAddress,
                            U.createdAt         AS RegistrationTime
                        FROM
                            fj_users    U
                        JOIN
                            fj_users    SU
                        WHERE
                            U.role IN (2,4)	AND
                            U.createdBy=SU.id ";
                            if($date1!='' && $date2!='') {
                                $date1 = date('Y-m-d', strtotime($date1));
                                $date2 = date('Y-m-d', strtotime($date2));
                                $sql .= " AND DATE(U.createdAt) BETWEEN '$date1' AND '$date2' ";
                            }
                            else {
                                $sql .= " AND DATE(U.createdAt)<=CURDATE()";
                            }
                $sql .= "
                        ORDER BY
                            CorporateName";
            $query  = $this->db->query($sql);
            $result = $query->result_array();            
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Description : Return all the jobs related data which are created by logged in users(Corporate user)
     * Author : Synergy
     * @param array(date1, date2)
     * @return array of data 
     */
    function jobsCSV($date1,$date2){
        try {            
            $sql    = " SELECT 
                            id,
                            fjCode,
                            title,
                            description,
                            noOfVacancies,
                            ageFrom,
                            ageTo,
                            salaryFrom,
                            salaryTo,
                            noticePeriod,
                            expFrom,
                            expTo,
                            openTill,
                            jobLocation,
                            jobQualification,
                            status,
                            createdAt,
                            createdBy
                        FROM
                            (SELECT 
                                J.id,
                                J.fjCode,
                                J.title,
                                J.description,
                                J.noOfVacancies,
                                J.ageFrom,
                                J.ageTo,
                                J.salaryFrom,
                                J.salaryTo,
                                J.noticePeriod,
                                J.expFrom,
                                J.expTo,
                                J.openTill,
                                GROUP_CONCAT(DISTINCT(L.city)) AS jobLocation,
                                GROUP_CONCAT(DISTINCT(Q.name)) AS jobQualification,
                                (CASE J.status WHEN 1 THEN 'Active' ELSE 'Inactive' END) as status, 
                                J.createdAt,
                                (CASE WHEN U.fullname!='' THEN U.fullname ELSE U.company END) AS createdBy
                            FROM 
                                fj_jobs 			J
                            JOIN
                                fj_jobLocations		JL,
                                fj_cityState		L,
                                fj_jobQualifications	JQ,
                                fj_courses			Q,
                                fj_users                    U
                            WHERE
                                J.createdBy=U.id		AND
                                J.id=JL.jobId		AND
                                JL.location=L.id		AND
                                JQ.qualificationId=Q.id ";
                            if($date1!='' && $date2!='') {
                                $date1 = date('Y-m-d', strtotime($date1));
                                $date2 = date('Y-m-d', strtotime($date2));
                                $sql .= " AND DATE(J.createdAt) BETWEEN '$date1' AND '$date2' ";
                            }
                            else {
                                $sql .= " AND DATE(J.createdAt)<=CURDATE()";
                            }

                            $sql .= " GROUP BY J.id, JQ.jobId ) aliasTable";
            $sql .= "   GROUP BY id ";
            $query  = $this->db->query($sql);
            $result = $query->result_array();            
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Description : Return all the video jd on specific jobs
     * Author : Synergy
     * @param array(date1, date2)
     * @return array of data 
     */
    function videoJdCSV($date1,$date2){
        try {            
            $sql    = " SELECT 
                            id,
                            fjCode,
                            jd AS JD,
                            title,
                            noOfVacancies,
                            ageFrom,
                            ageTo,
                            salaryFrom,
                            salaryTo,
                            noticePeriod,
                            expFrom,
                            expTo,
                            openTill,
                            jobLocation,
                            jobQualification,
                            status,
                            createdAt,
                            createdBy
                        FROM
                            (SELECT 
                                J.jd,
                                J.id,
                                J.fjCode,
                                J.title,
                                J.noOfVacancies,
                                J.ageFrom,
                                J.ageTo,
                                J.salaryFrom,
                                J.salaryTo,
                                J.noticePeriod,
                                J.expFrom,
                                J.expTo,
                                J.openTill,
                                GROUP_CONCAT(DISTINCT(L.city)) AS jobLocation,
                                GROUP_CONCAT(DISTINCT(Q.name)) AS jobQualification,
                                (CASE J.status WHEN 1 THEN 'Active' ELSE 'Inactive' END) as status, 
                                J.createdAt,
                                U.fullname AS createdBy
                            FROM 
                                fj_jobs                 J
                            JOIN
                                fj_jobLocations         JL,
                                fj_cityState            L,
                                fj_jobQualifications    JQ,
                                fj_courses              Q,
                                fj_users                U
                            WHERE
                                J.createdBy=U.id	AND
                                J.id=JL.jobId		AND
                                JL.location=L.id	AND
                                JQ.qualificationId=Q.id ";
                            if($date1!='' && $date2!='') {
                                $date1 = date('Y-m-d', strtotime($date1));
                                $date2 = date('Y-m-d', strtotime($date2));
                                $sql .= " AND DATE(J.createdAt) BETWEEN '$date1' AND '$date2' ";
                            }
                            else {
                                $sql .= " AND DATE(J.createdAt)<=CURDATE()";
                            }

                            $sql .= " GROUP BY J.id, JQ.jobId ) aliasTable";
            $sql .= "   WHERE 
                            aliasTable.jd!='0' AND 
                            aliasTable.jd!=''
                        GROUP BY id ";
            $query  = $this->db->query($sql);
            $result = $query->result_array();            
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Description : Return all the firstjob's jobs
     * Author : Synergy
     * @param array(date1, date2)
     * @return array of data 
     */
    function jobsOnFjCSV($date1,$date2){
        try {            
            $sql    = " SELECT 
                            id,
                            fjCode,
                            title,
                            description,
                            noOfVacancies,
                            ageFrom,
                            ageTo,
                            salaryFrom,
                            salaryTo,
                            noticePeriod,
                            expFrom,
                            expTo,
                            openTill,
                            jobLocation,
                            jobQualification,
                            status,
                            createdAt,
                            createdBy
                        FROM
                            (SELECT 
                                J.id,
                                J.fjCode,
                                J.title,
                                J.description,
                                J.noOfVacancies,
                                J.ageFrom,
                                J.ageTo,
                                J.salaryFrom,
                                J.salaryTo,
                                J.noticePeriod,
                                J.expFrom,
                                J.expTo,
                                J.openTill,
                                GROUP_CONCAT(DISTINCT(L.city)) AS jobLocation,
                                GROUP_CONCAT(DISTINCT(Q.name)) AS jobQualification,
                                (CASE J.status WHEN 1 THEN 'Active' ELSE 'Inactive' END) as status, 
                                J.createdAt,
                                U.fullname AS createdBy
                            FROM 
                                fj_jobs 		J
                            JOIN
                                fj_jobLocations		JL,
                                fj_cityState		L,
                                fj_jobQualifications	JQ,
                                fj_courses		Q,
                                fj_users                U
                            WHERE
                                J.createdBy=U.id	AND
                                J.posted='1'		AND
                                J.id=JL.jobId		AND
                                JL.location=L.id	AND
                                JQ.qualificationId=Q.id ";
                            if($date1!='' && $date2!='') {
                                $date1 = date('Y-m-d', strtotime($date1));
                                $date2 = date('Y-m-d', strtotime($date2));
                                $sql .= " AND DATE(J.createdAt) BETWEEN '$date1' AND '$date2' ";
                            }
                            else {
                                $sql .= " AND DATE(J.createdAt)<=CURDATE()";
                            }

                            $sql .= " GROUP BY J.id, JQ.jobId ) aliasTable";
            $sql .= "   GROUP BY id ";
            $query  = $this->db->query($sql);
            $result = $query->result_array();            
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Description : Return all interview sets
     * Author : Synergy
     * @param array(date1, date2)
     * @return array of data 
     */
    function interviewSetsCSV($date1,$date2){
        try {            
            $sql    = " SELECT 
                            I.id,
                            I.name AS InterviewSetName,
                            (CASE I.type WHEN 1 THEN 'Video' WHEN 2 THEN 'Audio' ELSE 'Text' END) as SetType,
                            Indus.name AS IndustryName,
                            F.name AS FunctionName,
                            (CASE I.status WHEN 1 THEN 'Active' ELSE 'Inactive' END) as status, 
                            I.createdAt,
                            U.fullname AS createdBy
                        FROM 
                            fj_interviews 	I
                        JOIN
                            nc_industry		Indus,
                            nc_industry		F,
                            fj_users		U
                        WHERE
                            I.industryId=Indus.id	AND
                            I.functionId=F.id		AND
                            I.createdBy=U.id
                            ";
                if($date1!='' && $date2!='') {
                    $date1 = date('Y-m-d', strtotime($date1));
                    $date2 = date('Y-m-d', strtotime($date2));
                    $sql .= " AND DATE(I.createdAt) BETWEEN '$date1' AND '$date2' ";
                }
                else {
                    $sql .= " AND DATE(I.createdAt)<=CURDATE()";
                }
            $query  = $this->db->query($sql);
            $result = $query->result_array();            
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Description : Return all video interview sets
     * Author : Synergy
     * @param array(date1, date2)
     * @return array of data 
     */
    function interviewVideoSetsCSV($date1,$date2){
        try {            
            $sql    = " SELECT 
                            I.id,
                            I.name AS InterviewSetName,
                            (CASE I.type WHEN 1 THEN 'Video' WHEN 2 THEN 'Audio' ELSE 'Text' END) as SetType,
                            Indus.name AS IndustryName,
                            F.name AS FunctionName,
                            (CASE I.status WHEN 1 THEN 'Active' ELSE 'Inactive' END) as status, 
                            I.createdAt,
                            U.fullname AS createdBy
                        FROM 
                            fj_interviews 	I
                        JOIN
                            nc_industry		Indus,
                            nc_industry		F,
                            fj_users		U
                        WHERE
                            I.type='1'                  AND
                            I.industryId=Indus.id	AND
                            I.functionId=F.id		AND
                            I.createdBy=U.id
                            ";
                if($date1!='' && $date2!='') {
                    $date1 = date('Y-m-d', strtotime($date1));
                    $date2 = date('Y-m-d', strtotime($date2));
                    $sql .= " AND DATE(I.createdAt) BETWEEN '$date1' AND '$date2' ";
                }
                else {
                    $sql .= " AND DATE(I.createdAt)<=CURDATE()";
                }
            $query  = $this->db->query($sql);
            $result = $query->result_array();            
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Description : Return all audio interview sets
     * Author : Synergy
     * @param array(date1, date2)
     * @return array of data 
     */
    
    function interviewAudioSetsCSV($date1,$date2){
        try {            
            $sql    = " SELECT 
                            I.id,
                            I.name AS InterviewSetName,
                            (CASE I.type WHEN 1 THEN 'Video' WHEN 2 THEN 'Audio' ELSE 'Text' END) as SetType,
                            Indus.name AS IndustryName,
                            F.name AS FunctionName,
                            (CASE I.status WHEN 1 THEN 'Active' ELSE 'Inactive' END) as status, 
                            I.createdAt,
                            U.fullname AS createdBy
                        FROM 
                            fj_interviews 	I
                        JOIN
                            nc_industry		Indus,
                            nc_industry		F,
                            fj_users		U
                        WHERE
                            I.type='2'                  AND
                            I.industryId=Indus.id	AND
                            I.functionId=F.id		AND
                            I.createdBy=U.id
                            ";
                if($date1!='' && $date2!='') {
                    $date1 = date('Y-m-d', strtotime($date1));
                    $date2 = date('Y-m-d', strtotime($date2));
                    $sql .= " AND DATE(I.createdAt) BETWEEN '$date1' AND '$date2' ";
                }
                else {
                    $sql .= " AND DATE(I.createdAt)<=CURDATE()";
                }
            $query  = $this->db->query($sql);
            $result = $query->result_array();            
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Description : Return all text interview sets
     * Author : Synergy
     * @param array(date1, date2)
     * @return array of data 
     */
    function interviewTextSetsCSV($date1,$date2){
        try {            
            $sql    = " SELECT 
                            I.id,
                            I.name AS InterviewSetName,
                            (CASE I.type WHEN 1 THEN 'Video' WHEN 2 THEN 'Audio' ELSE 'Text' END) as SetType,
                            Indus.name AS IndustryName,
                            F.name AS FunctionName,
                            (CASE I.status WHEN 1 THEN 'Active' ELSE 'Inactive' END) as status, 
                            I.createdAt,
                            U.fullname AS createdBy
                        FROM 
                            fj_interviews 	I
                        JOIN
                            nc_industry		Indus,
                            nc_industry		F,
                            fj_users		U
                        WHERE
                            I.type='3'                  AND
                            I.industryId=Indus.id	AND
                            I.functionId=F.id		AND
                            I.createdBy=U.id
                            ";
                if($date1!='' && $date2!='') {
                    $date1 = date('Y-m-d', strtotime($date1));
                    $date2 = date('Y-m-d', strtotime($date2));
                    $sql .= " AND DATE(I.createdAt) BETWEEN '$date1' AND '$date2' ";
                }
                else {
                    $sql .= " AND DATE(I.createdAt)<=CURDATE()";
                }
            $query  = $this->db->query($sql);
            $result = $query->result_array();            
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Description : Return all interview questions created by firstjob
     * Author : Synergy
     * @param array(date1, date2)
     * @return array of data 
     */
    function interviewQusFromFjCSV($date1,$date2){
        try {            
            $sql    = " 
                        SELECT 
                            IQ.id,
                            I.name AS InterviewSetName,
                            Q.title AS FjQuestionUsed
                        FROM 
                            fj_interviewQuestions   IQ
                        JOIN
                            fj_question             Q,
                            fj_interviews           I
                        WHERE
                            IQ.practiceInterviewType='0'	AND
                            IQ.questionId IN   (
                                            SELECT id AS questionID
                                            FROM fj_question
                                            WHERE createdBy IN (
                                                                SELECT id AS superAdminId
                                                                FROM fj_users
                                                                WHERE role = '1'
                                                                )
                                            )           AND
                            IQ.questionId=Q.id          AND
                            IQ.interviewId=I.id
                            ";
                if($date1!='' && $date2!='') {
                    $date1 = date('Y-m-d', strtotime($date1));
                    $date2 = date('Y-m-d', strtotime($date2));
                    $sql .= " AND DATE(IQ.createdAt) BETWEEN '$date1' AND '$date2' ";
                }
                else {
                    $sql .= " AND DATE(IQ.createdAt)<=CURDATE()";
                }
            $query  = $this->db->query($sql);
            $result = $query->result_array();            
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Description : Return number of questions in specific interview set
     * Author : Synergy
     * @param array(date1, date2)
     * @return array of data 
     */
    function interviewQusNumberCSV($date1,$date2){
        try {            
            $sql = " 
                    SELECT 
                        IQ.id,
                        I.name,
                        count(IQ.interviewId) AS NumberOfQuestions
                    FROM 
                        fj_interviewQuestions	IQ
                    JOIN
                        fj_interviews			I
                    WHERE
                        IQ.interviewId=I.id ";
                if($date1!='' && $date2!='') {
                    $date1 = date('Y-m-d', strtotime($date1));
                    $date2 = date('Y-m-d', strtotime($date2));
                    $sql .= " AND DATE(IQ.createdAt) BETWEEN '$date1' AND '$date2' ";
                }
                else {
                    $sql .= " AND DATE(IQ.createdAt)<=CURDATE()";
                }
            
            $sql .= "   GROUP BY
                            IQ.interviewId";
            $query  = $this->db->query($sql);
            $result = $query->result_array();            
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Description : Return all the assessment sets
     * Author : Synergy
     * @param array(date1, date2)
     * @return array of data 
     */
    function assessmentSetsCSV($date1,$date2){
        try {            
            $sql    = " SELECT 
                            A.id,
                            A.name AS AssessmentSetName,
                            A.duration AS Duration,
                            (CASE A.status WHEN 1 THEN 'Active' ELSE 'Inactive' END) as status, 
                            A.createdAt,
                            U.fullname AS createdBy
                        FROM 
                            fj_assessments 	A
                        JOIN
                            fj_users		U
                        WHERE
                            A.createdBy=U.id
                            ";
                if($date1!='' && $date2!='') {
                    $date1 = date('Y-m-d', strtotime($date1));
                    $date2 = date('Y-m-d', strtotime($date2));
                    $sql .= " AND DATE(A.createdAt) BETWEEN '$date1' AND '$date2' ";
                }
                else {
                    $sql .= " AND DATE(A.createdAt)<=CURDATE()";
                }
            $query  = $this->db->query($sql);
            $result = $query->result_array();            
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Description : Return all the assessment and its questions
     * Author : Synergy
     * @param array(date1, date2)
     * @return array of data 
     */
    function assessmentQusFromFjCSV($date1,$date2){
        try {            
            $sql    = " 
                        SELECT 
                            AQ.id,
                            A.name AS AssessmentSetName,
                            Q.title AS FjQuestionUsed
                        FROM 
                            fj_assessmentQuestions  AQ
                        JOIN
                            fj_question             Q,
                            fj_assessments          A
                        WHERE
                            AQ.questionId IN   (
                                            SELECT id AS questionID
                                            FROM fj_question
                                            WHERE createdBy IN (
                                                                SELECT id AS superAdminId
                                                                FROM fj_users
                                                                WHERE role = '1'
                                                                )
                                            )           AND
                            AQ.questionId=Q.id          AND
                            AQ.assessmentId=A.id
                            ";
                if($date1!='' && $date2!='') {
                    $date1 = date('Y-m-d', strtotime($date1));
                    $date2 = date('Y-m-d', strtotime($date2));
                    $sql .= " AND DATE(AQ.createdAt) BETWEEN '$date1' AND '$date2' ";
                }
                else {
                    $sql .= " AND DATE(AQ.createdAt)<=CURDATE()";
                }
            $query  = $this->db->query($sql);
            $result = $query->result_array();            
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Description : Return all the assessment and number of questions in each assessment
     * Author : Synergy
     * @param array(date1, date2)
     * @return array of data 
     */
    function assessmentQusNumberCSV($date1,$date2){
        try {            
            $sql    = " 
                        SELECT 
                                AQ.id,
                                A.name,
                                count(AQ.assessmentId) AS NumberOfQuestions
                        FROM 
                                fj_assessmentQuestions	AQ
                        JOIN
                                fj_assessments          A
                        WHERE
                                AQ.assessmentId=A.id ";
                if($date1!='' && $date2!='') {
                    $date1 = date('Y-m-d', strtotime($date1));
                    $date2 = date('Y-m-d', strtotime($date2));
                    $sql .= " AND DATE(AQ.createdAt) BETWEEN '$date1' AND '$date2' ";
                }
                else {
                    $sql .= " AND DATE(AQ.createdAt)<=CURDATE()";
                }
            
            $sql .= "   GROUP BY
                            AQ.assessmentId";
            $query  = $this->db->query($sql);
            $result = $query->result_array();            
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Description : Return all the invited candidates job wise
     * Author : Synergy
     * @param array(date1, date2)
     * @return array of data 
     */
    function getJobStatusAnalyticsData($date1,$date2, $userId){
        $sql = "SELECT job.id, job.fjCode, job.title, job.createdAt, job.closedDate, user.fullname, job.status, job.posted, (SELECT COUNT(*) FROM fj_jobInvitationForUser WHERE jobId = job.id ) invited, (SELECT COUNT(*) FROM fj_userJob WHERE jobId = job.id ) totalResponded, ( SELECT count(*) FROM `fj_userJob` uj JOIN fj_jobInvitationForUser inuser ON inuser.jobId = uj.jobId AND inuser.userId = uj.userId WHERE uj.jobId = job.id) totalinvitationresponded, ( SELECT  GROUP_CONCAT(ruser.fullname) FROM fj_jobPanel panel JOIN fj_jobs pjob ON pjob.id = panel.jobId JOIN fj_users ruser ON ruser.id = panel.userId WHERE panel.jobId = job.id) recruiters FROM `fj_jobs` job JOIN fj_users user ON user.id = job.createdBy WHERE job.status IN(1, 5) AND job.createdBy = $userId ORDER BY totalResponded DESC";
        
        $query  = $this->db->query($sql);
        $result = $query->result_array();            
        return $result;
    }

    /**
     * Description : Return all the invited candidates job wise
     * Author : Synergy
     * @param array(date1, date2)
     * @return array of data 
     */
    function getjobInvitationData($date1,$date2, $userId){
        $sql = "SELECT user.fullname inviteeName, user.id inviteeId, job.title jobName, job.fjCode jobCode, job.id, ji.createdAt invitationDate, (SELECT DISTINCT status FROM fj_userJob WHERE userId = user.id AND jobId = ji.jobId) status, (SELECT DISTINCT createdAt FROM fj_userJob WHERE userId = user.id AND jobId = ji.jobId) appliedOn, (SELECT DISTINCT id FROM fj_userJob WHERE userId = user.id AND jobId = ji.jobId) userJobId, (SELECT DISTINCT uj.shorlistedByL1User FROM fj_userJob uj JOIN fj_jobPanel panel1 ON panel1.jobId = uj.jobId WHERE panel1.level='1' AND uj.userId = user.id AND uj.jobId = ji.jobId) l1Status, (SELECT DISTINCT uj.shorlistedByL2User FROM fj_userJob uj JOIN fj_jobPanel panel1 ON panel1.jobId = uj.jobId WHERE panel1.level='2' AND uj.userId = user.id AND uj.jobId = ji.jobId) l2Status FROM fj_jobInvitationForUser ji JOIN fj_users user ON user.email = ji.email JOIN fj_jobs job ON job.id = ji.jobId WHERE job.status IN(1, 5) AND ji.invitedBy = $userId";
        
        $query  = $this->db->query($sql);
        $result = $query->result_array();            
        return $result;
    }

    /**
     * Description : Return all the invited candidates job wise
     * Author : Synergy
     * @param array(date1, date2)
     * @return array of data 
     */
    function getApplicantsByJobData($date1,$date2, $userId){
        $sql = "SELECT job.id, job.title jobName, job.fjCode jobCode, uj.userId candidateId, uj.id userJobId, user.fullname, uj.createdAt appliedDate, uj.source applicationSource, uj.L1ViewedDate, uj.L2ViewedDate, uj.shorlistedByL1UserDate, uj.shorlistedByL2UserDate FROM fj_userJob uj JOIN fj_jobs job ON job.id = uj.jobId JOIN fj_users user ON user.id = uj.userId WHERE job.status IN(1, 5) AND job.createdBy = $userId"
        ;
        
        $query  = $this->db->query($sql);
        $result = $query->result_array();            
        return $result;
    }

    /**
     * Description : Return all the invited candidates job wise
     * Author : Synergy
     * @param array(date1, date2)
     * 8th report
     * @return array of data 
     */
    function getRecruitersStatus($date1,$date2, $userId){
        $sql = "SELECT levelUser.id recruiterId, levelUser.fullname recruiterName, (SELECT COUNT(*) FROM fj_jobs WHERE createdBy = levelUser.id) noOfJobsCreated, (SELECT COUNT(*) FROM fj_userJob uj1 JOIN fj_jobPanel panel1 ON panel1.jobId = uj1.jobId WHERE panel1.level='1' AND uj1.shorlistedByL1User IS NOT NULL AND uj1.shorlistedByL1User = levelUser.id) totalL1QueueEveluated, (SELECT COUNT(*) FROM fj_userJob uj1 JOIN fj_jobPanel panel1 ON panel1.jobId = uj1.jobId WHERE panel1.level='1' AND uj1.shorlistedByL1User IS NOT NULL AND uj1.shorlistedByL1User = levelUser.id AND uj1.status IN('2','4')) totalL1QueueEveluatedYes, (SELECT COUNT(*) FROM fj_userJob uj1 JOIN fj_jobPanel panel1 ON panel1.jobId = uj1.jobId WHERE panel1.level='1' AND uj1.shorlistedByL1User IS NOT NULL AND uj1.shorlistedByL1User = levelUser.id AND uj1.status IN('3','5')) totalL1QueueEveluatedNo, (SELECT COUNT(*) FROM fj_userJob uj1 JOIN fj_jobPanel panel1 ON panel1.jobId = uj1.jobId WHERE panel1.level='2' AND uj1.shorlistedByL2User IS NOT NULL AND uj1.shorlistedByL2User = levelUser.id) totalL2QueueEveluated, (SELECT COUNT(*) FROM fj_userJob uj1 JOIN fj_jobPanel panel1 ON panel1.jobId = uj1.jobId WHERE panel1.level='2' AND uj1.shorlistedByL2User IS NOT NULL AND uj1.shorlistedByL2User = levelUser.id AND uj1.status IN('2','4')) totalL2QueueEveluatedYes, (SELECT COUNT(*) FROM fj_userJob uj1 JOIN fj_jobPanel panel1 ON panel1.jobId = uj1.jobId WHERE panel1.level='2' AND uj1.shorlistedByL2User IS NOT NULL AND uj1.shorlistedByL2User = levelUser.id AND uj1.status IN('3','5')) totalL2QueueEveluatedNo, (SELECT COUNT(*) FROM fj_userJob uj1 JOIN fj_jobPanel panel1 ON panel1.jobId = uj1.jobId WHERE panel1.level='1' AND uj1.shorlistedByL1User IS NULL AND panel1.userId = levelUser.id) totalL1QueuePending, (SELECT COUNT(*) FROM fj_userJob uj1 JOIN fj_jobPanel panel1 ON panel1.jobId = uj1.jobId WHERE panel1.level='2' AND uj1.shorlistedByL1User IS NULL AND panel1.userId = levelUser.id) totalL2QueuePending, (SELECT COUNT(*) FROM fj_assessments WHERE createdBy = levelUser.id) totalAssessmentCreated, (SELECT COUNT(*) FROM fj_interviews WHERE createdBy = levelUser.id) totalInterviewCreated, (SELECT COUNT(*) FROM fj_userJobShare WHERE createdBy = levelUser.id) emailFeedbackSought FROM fj_users levelUser JOIN fj_users mainUser ON mainUser.id = levelUser.id WHERE levelUser.status = '1' AND levelUser.createdBy = $userId ORDER BY totalL1QueueEveluated DESC"
        ;
        
        $query  = $this->db->query($sql);
        $result = $query->result_array();            
        return $result;
    }

    /**
     * Description : Return all the invited candidates job wise
     * Author : Synergy
     * @param array(date1, date2)
     * @return array of data 
     */
    function getApplicantProfileByJob($date1,$date2, $userId){
        $sql = "SELECT job.id, job.title jobName, (SELECT COUNT(*) FROM fj_userJob WHERE jobId = job.id) appliedTotal, (SELECT GROUP_CONCAT(fusr.pinCode) FROM fj_userJob uj JOIN fj_users fusr ON fusr.id = uj.userId WHERE uj.jobId = job.id) pinCodes, (SELECT count(*) FROM fj_userJob uj JOIN fj_jobInvitationForUser inuser ON inuser.jobId = uj.jobId AND inuser.userId = uj.userId WHERE uj.jobId = job.id) appliedInvitedCount, (SELECT COUNT(*) FROM fj_userJob WHERE status IN('2','4') AND jobId = job.id) shortlistedTotal, (SELECT count(*) FROM fj_userJob uj JOIN fj_jobInvitationForUser inuser ON inuser.jobId = uj.jobId AND inuser.userId = uj.userId WHERE status IN('2','4') AND uj.jobId = job.id) shortlistedInvitedCount FROM fj_jobs job WHERE job.status IN(1, 5) AND job.createdBy = $userId ORDER BY appliedTotal DESC"
        ;
        
        $query  = $this->db->query($sql);
        $result = $query->result_array();            
        return $result;
    }

    /**
     * Description : Return all the invited candidates job wise
     * Author : Synergy
     * @param array(date1, date2)
     * @return array of data 
     * 7th report
     */
    function getApplicansByJobRecruiter($date1,$date2, $userId){
        $sql = "SELECT job.id, job.title jobName, job.fjCode, (SELECT COUNT(*) FROM fj_userJob WHERE jobId = job.id) totalApplictionReceived, (SELECT COUNT(*) FROM fj_userJob uj1 JOIN fj_jobPanel panel1 ON panel1.jobId = uj1.jobId WHERE panel1.level='1' AND uj1.shorlistedByL1User IS NULL AND uj1.jobId = job.id) totalPendingApplicationAtL1, (SELECT COUNT(*) FROM fj_userJob uj1 JOIN fj_jobPanel panel1 ON panel1.jobId = uj1.jobId WHERE panel1.level='1' AND uj1.shorlistedByL1User IS NOT NULL AND status IN('2','4') AND uj1.jobId = job.id) totalShortlistedApplicationAtL1, (SELECT COUNT(*) FROM fj_userJob uj1 JOIN fj_jobPanel panel1 ON panel1.jobId = uj1.jobId WHERE panel1.level='1' AND uj1.shorlistedByL1User IS NOT NULL AND status IN('3','5') AND uj1.jobId = job.id) totalRejectedApplicationAtL1,  (SELECT COUNT(*) FROM fj_userJob uj1 JOIN fj_jobPanel panel1 ON panel1.jobId = uj1.jobId WHERE panel1.level='2' AND uj1.shorlistedByL2User IS NULL AND uj1.jobId = job.id) totalPendingApplicationAtL2, (SELECT COUNT(*) FROM fj_userJob uj1 JOIN fj_jobPanel panel1 ON panel1.jobId = uj1.jobId WHERE panel1.level='2' AND uj1.shorlistedByL2User IS NOT NULL AND status IN('4') AND uj1.jobId = job.id) totalShortlistedApplicationAtL2, (SELECT COUNT(*) FROM fj_userJob uj1 JOIN fj_jobPanel panel1 ON panel1.jobId = uj1.jobId WHERE panel1.level='2' AND uj1.shorlistedByL2User IS NOT NULL AND status IN('5') AND uj1.jobId = job.id) totalRejectedApplicationAtL2, ( SELECT  GROUP_CONCAT(ruser.fullname) FROM fj_jobPanel panel2 JOIN fj_users ruser ON ruser.id = panel2.userId WHERE panel2.level = '1' AND panel2.jobId = job.id) l1recruiters, ( SELECT  GROUP_CONCAT(ruser.fullname) FROM fj_jobPanel panel2 JOIN fj_users ruser ON ruser.id = panel2.userId WHERE panel2.level = '2' AND panel2.jobId = job.id) l2recruiters FROM fj_jobs job WHERE job.status IN(1, 5) AND  job.createdBy = $userId ORDER BY totalPendingApplicationAtL1 DESC";
        
        $query  = $this->db->query($sql);
        $result = $query->result_array();            
        return $result;
    }

    /**
     * Description : Return all the invited candidates job wise
     * Author : Synergy
     * @param array(date1, date2)
     * @return array of data 
     */
    function getjobFullfillmentData($date1,$date2, $userId){
        $sql = "SELECT job.id, job.title, job.createdAt, job.closedDate, job.posted, job.noOfVacancies, (SELECT COUNT(*) FROM fj_userJob WHERE jobId = job.id) totalApplications FROM fj_jobs job WHERE job.status IN(1, 5) AND job.createdBy = $userId ORDER BY totalApplications DESC";
        
        $query  = $this->db->query($sql);
        $result = $query->result_array();            
        return $result;
    }

    /**
     * Description : Return all the invited candidates job wise
     * Author : Synergy
     * @param array(date1, date2)
     * @return array of data 
     */
    function getjobPendingApplicationData($date1,$date2, $userId){
        $sql = "SELECT job.id, job.fjCode, job.title, user.company, (SELECT COUNT(*) FROM fj_userJob WHERE jobId = job.id) totalApplicationReceivedAtMain, (SELECT COUNT(*) FROM fj_userJob WHERE jobId = job.id AND shorlistedByCorporateForL1User IS NULL) totalPendingAtMain, (SELECT COUNT(*) FROM fj_userJob uj1 JOIN fj_jobPanel panel1 ON panel1.jobId = uj1.jobId WHERE panel1.level='1' AND uj1.jobId = job.id) totalApplicationReceivedAtL1, (SELECT COUNT(*) FROM fj_userJob uj1 JOIN fj_jobPanel panel1 ON panel1.jobId = uj1.jobId WHERE panel1.level='1' AND uj1.shorlistedByL1User IS NULL AND uj1.jobId = job.id) totalPendingApplicationAtL1, (SELECT COUNT(*) FROM fj_userJob uj1 JOIN fj_jobPanel panel1 ON panel1.jobId = uj1.jobId WHERE panel1.level='1' AND uj1.shorlistedByL1User IS NOT NULL AND status IN('2','4') AND uj1.jobId = job.id) totalShortlistedApplicationAtL1, (SELECT COUNT(*) FROM fj_userJob uj1 JOIN fj_jobPanel panel1 ON panel1.jobId = uj1.jobId WHERE panel1.level='1' AND uj1.shorlistedByL1User IS NOT NULL AND status IN('3','5') AND uj1.jobId = job.id) totalRejectedApplicationAtL1, (SELECT COUNT(*) FROM fj_userJob uj1 JOIN fj_jobPanel panel1 ON panel1.jobId = uj1.jobId WHERE panel1.level='2' AND uj1.jobId = job.id) totalApplicationReceivedAtL2, (SELECT COUNT(*) FROM fj_userJob uj1 JOIN fj_jobPanel panel1 ON panel1.jobId = uj1.jobId WHERE panel1.level='2' AND uj1.shorlistedByL2User IS NULL AND uj1.jobId = job.id) totalPendingApplicationAtL2, (SELECT COUNT(*) FROM fj_userJob uj1 JOIN fj_jobPanel panel1 ON panel1.jobId = uj1.jobId WHERE panel1.level='2' AND uj1.shorlistedByL2User IS NOT NULL AND status IN('4') AND uj1.jobId = job.id) totalShortlistedApplicationAtL2, (SELECT COUNT(*) FROM fj_userJob uj1 JOIN fj_jobPanel panel1 ON panel1.jobId = uj1.jobId WHERE panel1.level='2' AND uj1.shorlistedByL2User IS NOT NULL AND status IN('5') AND uj1.jobId = job.id) totalRejectedApplicationAtL2, ( SELECT  GROUP_CONCAT(ruser.fullname) FROM fj_jobPanel panel2 JOIN fj_users ruser ON ruser.id = panel2.userId WHERE panel2.level = '1' AND panel2.jobId = job.id) l1recruiters, ( SELECT  GROUP_CONCAT(ruser.fullname) FROM fj_jobPanel panel2 JOIN fj_users ruser ON ruser.id = panel2.userId WHERE panel2.level = '2' AND panel2.jobId = job.id) l2recruiters, ( SELECT  GROUP_CONCAT(ruser.id) FROM fj_jobPanel panel2 JOIN fj_users ruser ON ruser.id = panel2.userId WHERE panel2.level = '1' AND panel2.jobId = job.id) l1recruitersId, ( SELECT  GROUP_CONCAT(ruser.id) FROM fj_jobPanel panel2 JOIN fj_users ruser ON ruser.id = panel2.userId WHERE panel2.level = '2' AND panel2.jobId = job.id) l2recruitersId, ( SELECT DISTINCT panel2.level FROM fj_jobPanel panel2 WHERE panel2.level='2' AND panel2.jobId = job.id) leveldata2, ( SELECT DISTINCT panel2.level FROM fj_jobPanel panel2 WHERE panel2.level='1' AND panel2.jobId = job.id) leveldata1 FROM `fj_jobs` job JOIN fj_users user ON user.id = job.createdBy WHERE job.status IN(1, 5) AND job.createdBy = $userId ORDER BY totalApplicationReceivedAtL1 DESC";
        
		
        $query  = $this->db->query($sql);

		
        $result = $query->result_array();       
			
        return $result;
    }

    /**
     * Description : Return all the invited candidates job wise
     * Author : Synergy
     * @param array(date1, date2)
     * @return array of data 
     */
    function invitedCandidateCSV($date1,$date2){
        try {            
            $sql    = " SELECT 
                            I.id,
                            (CASE WHEN I.userId IS NULL THEN '' ELSE FjU.fullname END) 	AS FjUserName,
                            I.email     AS InvitedUserEmail,
                            J.fjCode    AS JobCode,
                            J.title     AS JobTitle,
                            (CASE I.invitationStatus WHEN 0 THEN 'Pending' WHEN 1 THEN 'Accepted' ELSE '' END) as Status,
                            U.fullname  AS InvitedBy,
                            I.createdAt
                        FROM 
                            fj_jobInvitationForUser     I
                        JOIN
                            fj_users                    U,
                            fj_users                    FjU,
                            fj_jobs                     J
                        WHERE
                            I.invitedBy=U.id	AND
                            I.jobId=J.id	AND
                            (CASE WHEN I.userId IS NULL THEN '1=1' ELSE I.userId=FjU.id END) ";
                if($date1!='' && $date2!='') {
                    $date1 = date('Y-m-d', strtotime($date1));
                    $date2 = date('Y-m-d', strtotime($date2));
                    $sql .= " AND DATE(I.createdAt) BETWEEN '$date1' AND '$date2' ";
                }
                else {
                    $sql .= " AND DATE(I.createdAt)<=CURDATE()";
                }
                $sql .= " GROUP BY I.id ORDER BY I.invitedBy";
            $query  = $this->db->query($sql);
            $result = $query->result_array();            
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Description : Return all the invited candidates job wise on FirstJob job's
     * Author : Synergy
     * @param array(date1, date2)
     * @return array of data 
     */
    function invitedFjCandidateCSV($date1,$date2){
        try {            
            $sql    = " SELECT 
                            I.id,
                            (CASE WHEN I.userId IS NULL THEN '' ELSE FjU.fullname END) 	AS FjUserName,
                            I.email     	AS InvitedUserEmail,
                            J.fjCode    	AS JobCode,
                            J.title     	AS JobTitle,
                            (CASE I.invitationStatus WHEN 0 THEN 'Pending' WHEN 1 THEN 'Accepted' ELSE '' END)	  AS Status,
                            U.fullname		AS InvitedBy,
                            I.createdAt
                        FROM 
                            fj_jobInvitationForUser     I
                        JOIN
                            fj_users                    U,
                            fj_users                    FjU,
                            fj_jobs                     J
                        WHERE
                            I.userId!=''        AND
                            I.invitedBy=U.id	AND
                            I.jobId=J.id	AND
                            (CASE WHEN I.userId IS NULL THEN '1=1' ELSE I.userId=FjU.id END) ";
                if($date1!='' && $date2!='') {
                    $date1 = date('Y-m-d', strtotime($date1));
                    $date2 = date('Y-m-d', strtotime($date2));
                    $sql .= " AND DATE(I.createdAt) BETWEEN '$date1' AND '$date2' ";
                }
                else {
                    $sql .= " AND DATE(I.createdAt)<=CURDATE()";
                }
                $sql .= " GROUP BY I.id ORDER BY I.invitedBy";
            $query  = $this->db->query($sql);
            $result = $query->result_array();            
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Description : Return job wise candidates and its job appliced status
     * Author : Synergy
     * @param array(date1, date2)
     * @return array of data 
     */
    function jobAppliedCSV($date1,$date2){
        try {            
            $sql    = " SELECT 
                            UJ.id,
                            U.fullname  AS AppUserName,
                            U.email     AS AppUserEmail,
                            U.mobile    AS AppUserMobile,
                            C.company,
                            J.fjCode    AS JobCode,
                            J.title     AS JobTitle,
                            (CASE UJ.status WHEN 1 THEN 'Pending' WHEN 2 THEN 'Shortlisted By L1' WHEN 3 THEN 'Rejected By L1' WHEN 4 THEN 'Shortlisted By L2'  WHEN 5 THEN 'Rejected By L2' ELSE '' END) as JobAplliedStatus,
                            UJ.createdAt
                        FROM 
                            fj_userJob  UJ
                        JOIN
                            fj_users	U,
                            fj_jobs     J,                            
                            fj_users	C
                        WHERE
                            UJ.userId=U.id  AND
                            J.createdBy=C.id   AND
                            UJ.jobId=J.id";
                if($date1!='' && $date2!='') {
                    $date1 = date('Y-m-d', strtotime($date1));
                    $date2 = date('Y-m-d', strtotime($date2));
                    $sql .= " AND DATE(UJ.createdAt) BETWEEN '$date1' AND '$date2' ";
                }
                else {
                    $sql .= " AND DATE(UJ.createdAt)<=CURDATE()";
                }
                $sql .= " ORDER BY C.company";
            $query  = $this->db->query($sql);
            $result = $query->result_array();            
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Description : Return job wise candidates whose job applied status is pending
     * Author : Synergy
     * @param array(date1, date2)
     * @return array of data 
     */
    function pendingCandidateCSV($date1,$date2){
        try {            
            $sql    = " 
                        SELECT
                            aliasTable.id,
                            aliasTable.AppUserName,
                            aliasTable.AppUserEmail,
                            aliasTable.AppUserMobile,
                            aliasTable.JobCode,
                            aliasTable.JobTitle,
                            aliasTable.JobAplliedStatus,
                            aliasTable.JobLevel, ";
                            //aliasTable.shorlistedByL1User,
            $sql   .= " (CASE WHEN aliasTable.shorlistedByL1User IS NULL THEN '' ELSE L1.fullname END) AS L1UserName, ";
                            //aliasTable.shorlistedByL2User,
            $sql   .= " (CASE WHEN aliasTable.shorlistedByL2User IS NULL THEN '' ELSE L2.fullname END) AS L2UserName,
                            U.fullname AS JobCreatedBy,
                            U.company  AS CompanyName,
                            aliasTable.createdAt
                        FROM
                            (
                            SELECT 
                                UJ.shorlistedByL1User,
                                UJ.shorlistedByL2User,
                                UJ.id,
                                U.fullname  AS AppUserName,
                                U.email     AS AppUserEmail,
                                U.mobile    AS AppUserMobile,
                                J.fjCode    AS JobCode,
                                J.title     AS JobTitle,
                                (CASE UJ.status WHEN 1 THEN 'Pending' WHEN 2 THEN 'Shortlisted By L1' WHEN 3 THEN 'Rejected By L1' WHEN 4 THEN 'Shortlisted By L2'  WHEN 5 THEN 'Rejected By L2' ELSE '' END) as JobAplliedStatus,
                                (CASE COUNT(DISTINCT(JP.level)) WHEN 1 THEN 'L1' WHEN 2 THEN 'L2' ELSE '' END) AS JobLevel,
                                J.createdBy,
                                UJ.createdAt                            
                            FROM 
                                fj_userJob		UJ
                            JOIN 
                                fj_jobPanel 	JP,
                                fj_users            U,
                                fj_jobs        	J
                            WHERE
                                UJ.jobId=JP.jobId      AND
                                UJ.userId=U.id      	AND
                                UJ.jobId=J.id ";
                    if($date1!='' && $date2!='') {
                        $date1 = date('Y-m-d', strtotime($date1));
                        $date2 = date('Y-m-d', strtotime($date2));
                        $sql .= " AND DATE(UJ.createdAt) BETWEEN '$date1' AND '$date2' ";
                    }
                    else {
                        $sql .= " AND DATE(UJ.createdAt)<=CURDATE()";
                    }
                    $sql .= " GROUP BY UJ.id ORDER BY UJ.jobId ) aliasTable";
                $sql .= "   JOIN
                                fj_users U,
                                fj_users L1,
                                fj_users L2
                            WHERE
                                aliasTable.createdBy=U.id AND
                                (
                                    aliasTable.JobLevel='L1' AND aliasTable.JobAplliedStatus='Pending'
                                    OR
                                    aliasTable.JobLevel='L2' AND aliasTable.JobAplliedStatus='Shortlisted By L1'
                                )  AND
                                (CASE WHEN aliasTable.shorlistedByL1User IS NULL THEN '1=1' ELSE aliasTable.shorlistedByL1User=L1.id END)   AND
                                (CASE WHEN aliasTable.shorlistedByL2User IS NULL THEN '1=1' ELSE aliasTable.shorlistedByL2User=L2.id END)
                            GROUP BY 
                                aliasTable.id
                        ";
            $query  = $this->db->query($sql);
            $result = $query->result_array();            
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Description : Return job wise candidates whose job applied status is pending at Level1
     * Author : Synergy
     * @param array(date1, date2)
     * @return array of data 
     */
    function pendingCandidateAtL1CSV($date1,$date2){
        try {            
            $sql    = " 
                        SELECT
                            aliasTable.id,
                            aliasTable.AppUserName,
                            aliasTable.AppUserEmail,
                            aliasTable.AppUserMobile,
                            aliasTable.JobCode,
                            aliasTable.JobTitle,
                            aliasTable.JobAplliedStatus,
                            aliasTable.JobLevel, ";
                            //aliasTable.shorlistedByL1User,
            $sql   .= " (CASE WHEN aliasTable.shorlistedByL1User IS NULL THEN '' ELSE L1.fullname END) AS L1UserName, ";
                            //aliasTable.shorlistedByL2User,
            $sql   .= " (CASE WHEN aliasTable.shorlistedByL2User IS NULL THEN '' ELSE L2.fullname END) AS L2UserName,
                            U.fullname AS JobCreatedBy,
                            U.company  AS CompanyName,
                            aliasTable.createdAt
                        FROM
                            (
                            SELECT 
                                UJ.shorlistedByL1User,
                                UJ.shorlistedByL2User,
                                UJ.id,
                                U.fullname  AS AppUserName,
                                U.email     AS AppUserEmail,
                                U.mobile    AS AppUserMobile,
                                J.fjCode    AS JobCode,
                                J.title     AS JobTitle,
                                (CASE UJ.status WHEN 1 THEN 'Pending At L1' WHEN 2 THEN 'Shortlisted By L1' WHEN 3 THEN 'Rejected By L1' WHEN 4 THEN 'Shortlisted By L2'  WHEN 5 THEN 'Rejected By L2' ELSE '' END) as JobAplliedStatus,
                                (CASE COUNT(DISTINCT(JP.level)) WHEN 1 THEN 'L1' WHEN 2 THEN 'L2' ELSE '' END) AS JobLevel,
                                J.createdBy,
                                UJ.createdAt                            
                            FROM 
                                fj_userJob		UJ
                            JOIN 
                                fj_jobPanel 	JP,
                                fj_users            U,
                                fj_jobs        	J
                            WHERE
                                UJ.jobId=JP.jobId      AND
                                UJ.userId=U.id      	AND
                                UJ.jobId=J.id ";
                    if($date1!='' && $date2!='') {
                        $date1 = date('Y-m-d', strtotime($date1));
                        $date2 = date('Y-m-d', strtotime($date2));
                        $sql .= " AND DATE(UJ.createdAt) BETWEEN '$date1' AND '$date2' ";
                    }
                    else {
                        $sql .= " AND DATE(UJ.createdAt)<=CURDATE()";
                    }
                    $sql .= " GROUP BY UJ.id ORDER BY UJ.jobId ) aliasTable";
                $sql .= "   JOIN
                                fj_users U,
                                fj_users L1,
                                fj_users L2
                            WHERE
                                aliasTable.createdBy=U.id AND
                                (
                                    aliasTable.JobLevel='L1' AND aliasTable.JobAplliedStatus='Pending At L1'
                                    OR
                                    aliasTable.JobLevel='L2' AND aliasTable.JobAplliedStatus='Pending At L1'
                                )  AND
                                (CASE WHEN aliasTable.shorlistedByL1User IS NULL THEN '1=1' ELSE aliasTable.shorlistedByL1User=L1.id END)   AND
                                (CASE WHEN aliasTable.shorlistedByL2User IS NULL THEN '1=1' ELSE aliasTable.shorlistedByL2User=L2.id END)
                            GROUP BY 
                                aliasTable.id
                        ";
            $query  = $this->db->query($sql);
            $result = $query->result_array();            
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Description : Return job wise candidates whose job applied status is pending at Level2
     * Author : Synergy
     * @param array(date1, date2)
     * @return array of data 
     */
    function pendingCandidateAtL2CSV($date1,$date2){
        try {            
            $sql    = " 
                        SELECT
                            aliasTable.id,
                            aliasTable.AppUserName,
                            aliasTable.AppUserEmail,
                            aliasTable.AppUserMobile,
                            aliasTable.JobCode,
                            aliasTable.JobTitle,
                            aliasTable.JobAplliedStatus,
                            aliasTable.JobLevel, ";
                            //aliasTable.shorlistedByL1User,
            $sql   .= " (CASE WHEN aliasTable.shorlistedByL1User IS NULL THEN '' ELSE L1.fullname END) AS L1UserName, ";
                            //aliasTable.shorlistedByL2User,
            $sql   .= " (CASE WHEN aliasTable.shorlistedByL2User IS NULL THEN '' ELSE L2.fullname END) AS L2UserName,
                            U.fullname AS JobCreatedBy,
                            U.company  AS CompanyName,
                            aliasTable.createdAt
                        FROM
                            (
                            SELECT 
                                UJ.shorlistedByL1User,
                                UJ.shorlistedByL2User,
                                UJ.id,
                                U.fullname  AS AppUserName,
                                U.email     AS AppUserEmail,
                                U.mobile    AS AppUserMobile,
                                J.fjCode    AS JobCode,
                                J.title     AS JobTitle,
                                (CASE UJ.status WHEN 1 THEN 'Pending At L1' WHEN 2 THEN 'Shortlisted By L1' WHEN 3 THEN 'Rejected By L1' WHEN 4 THEN 'Shortlisted By L2'  WHEN 5 THEN 'Rejected By L2' ELSE '' END) as JobAplliedStatus,
                                (CASE COUNT(DISTINCT(JP.level)) WHEN 1 THEN 'L1' WHEN 2 THEN 'L2' ELSE '' END) AS JobLevel,
                                J.createdBy,
                                UJ.createdAt                            
                            FROM 
                                fj_userJob		UJ
                            JOIN 
                                fj_jobPanel 	JP,
                                fj_users            U,
                                fj_jobs        	J
                            WHERE
                                UJ.jobId=JP.jobId      AND
                                UJ.userId=U.id      	AND
                                UJ.jobId=J.id ";
                    if($date1!='' && $date2!='') {
                        $date1 = date('Y-m-d', strtotime($date1));
                        $date2 = date('Y-m-d', strtotime($date2));
                        $sql .= " AND DATE(UJ.createdAt) BETWEEN '$date1' AND '$date2' ";
                    }
                    else {
                        $sql .= " AND DATE(UJ.createdAt)<=CURDATE()";
                    }
                    $sql .= " GROUP BY UJ.id ORDER BY UJ.jobId ) aliasTable";
                $sql .= "   JOIN
                                fj_users U,
                                fj_users L1,
                                fj_users L2
                            WHERE
                                aliasTable.createdBy=U.id AND
                                (
                                    aliasTable.JobLevel='L2' AND aliasTable.JobAplliedStatus='Shortlisted By L1'
                                )  AND
                                (CASE WHEN aliasTable.shorlistedByL1User IS NULL THEN '1=1' ELSE aliasTable.shorlistedByL1User=L1.id END)   AND
                                (CASE WHEN aliasTable.shorlistedByL2User IS NULL THEN '1=1' ELSE aliasTable.shorlistedByL2User=L2.id END)
                            GROUP BY 
                                aliasTable.id
                        ";
            $query  = $this->db->query($sql);
            $result = $query->result_array();            
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Description : Return job wise all the shortlisted candidates
     * Author : Synergy
     * @param array(date1, date2)
     * @return array of data 
     */
    function shorlistedCandidateCSV($date1,$date2){
        try {            
            $sql    = " 
                        SELECT
                            aliasTable.id,
                            aliasTable.AppUserName,
                            aliasTable.AppUserEmail,
                            aliasTable.AppUserMobile,
                            aliasTable.JobCode,
                            aliasTable.JobTitle,
                            aliasTable.JobAplliedStatus,
                            aliasTable.JobLevel, ";
            $sql   .= " (CASE WHEN aliasTable.shorlistedByL1User IS NULL THEN '' ELSE L1.fullname END) AS L1UserName, ";
            $sql   .= " (CASE WHEN aliasTable.shorlistedByL2User IS NULL THEN '' ELSE L2.fullname END) AS L2UserName,
                            U.fullname AS JobCreatedBy,
                            U.company  AS CompanyName,
                            aliasTable.createdAt
                        FROM
                            (
                            SELECT 
                                UJ.shorlistedByL1User,
                                UJ.shorlistedByL2User,
                                UJ.id,
                                U.fullname  AS AppUserName,
                                U.email     AS AppUserEmail,
                                U.mobile    AS AppUserMobile,
                                J.fjCode    AS JobCode,
                                J.title     AS JobTitle,
                                (CASE UJ.status WHEN 1 THEN 'Pending' WHEN 2 THEN 'Shortlisted By L1' WHEN 3 THEN 'Rejected By L1' WHEN 4 THEN 'Shortlisted By L2'  WHEN 5 THEN 'Rejected By L2' ELSE '' END) as JobAplliedStatus,
                                (CASE COUNT(DISTINCT(JP.level)) WHEN 1 THEN 'L1' WHEN 2 THEN 'L2' ELSE '' END) AS JobLevel,
                                J.createdBy,
                                UJ.createdAt                            
                            FROM 
                                fj_userJob		UJ
                            JOIN 
                                fj_jobPanel 	JP,
                                fj_users            U,
                                fj_jobs        	J
                            WHERE
                                UJ.jobId=JP.jobId      AND
                                UJ.userId=U.id      	AND
                                UJ.jobId=J.id ";
                    if($date1!='' && $date2!='') {
                        $date1 = date('Y-m-d', strtotime($date1));
                        $date2 = date('Y-m-d', strtotime($date2));
                        $sql .= " AND DATE(UJ.createdAt) BETWEEN '$date1' AND '$date2' ";
                    }
                    else {
                        $sql .= " AND DATE(UJ.createdAt)<=CURDATE()";
                    }
                    $sql .= " GROUP BY UJ.id ORDER BY UJ.jobId ) aliasTable";
                $sql .= "   JOIN
                                fj_users U,
                                fj_users L1,
                                fj_users L2
                            WHERE
                                aliasTable.createdBy=U.id AND
                                (
                                    aliasTable.JobLevel='L1' AND aliasTable.JobAplliedStatus='Shortlisted By L1'
                                    OR
                                    aliasTable.JobLevel='L2' AND aliasTable.JobAplliedStatus='Shortlisted By L2'
                                )  AND
                                (CASE WHEN aliasTable.shorlistedByL1User IS NULL THEN '1=1' ELSE aliasTable.shorlistedByL1User=L1.id END)   AND
                                (CASE WHEN aliasTable.shorlistedByL2User IS NULL THEN '1=1' ELSE aliasTable.shorlistedByL2User=L2.id END)
                            GROUP BY 
                                aliasTable.id
                        ";
            //echo "<pre>"; echo $sql; exit;
            $query  = $this->db->query($sql);
            $result = $query->result_array();            
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Description : Return job wise all the shortlisted candidates at Level1
     * Author : Synergy
     * @param array(date1, date2)
     * @return array of data 
     */
    function shorlistedCandidateAtL1CSV($date1,$date2){
        try {            
            $sql    = " 
                        SELECT
                            aliasTable.id,
                            aliasTable.AppUserName,
                            aliasTable.AppUserEmail,
                            aliasTable.AppUserMobile,
                            aliasTable.JobCode,
                            aliasTable.JobTitle,
                            aliasTable.JobAplliedStatus,
                            aliasTable.JobLevel, ";
                            //aliasTable.shorlistedByL1User,
            $sql   .= " (CASE WHEN aliasTable.shorlistedByL1User IS NULL THEN '' ELSE L1.fullname END) AS L1UserName, ";
                            //aliasTable.shorlistedByL2User,
            $sql   .= " (CASE WHEN aliasTable.shorlistedByL2User IS NULL THEN '' ELSE L2.fullname END) AS L2UserName,
                            U.fullname AS JobCreatedBy,
                            U.company  AS CompanyName,
                            aliasTable.createdAt
                        FROM
                            (
                            SELECT 
                                UJ.shorlistedByL1User,
                                UJ.shorlistedByL2User,
                                UJ.id,
                                U.fullname  AS AppUserName,
                                U.email     AS AppUserEmail,
                                U.mobile    AS AppUserMobile,
                                J.fjCode    AS JobCode,
                                J.title     AS JobTitle,
                                (CASE UJ.status WHEN 1 THEN 'Pending At L1' WHEN 2 THEN 'Shortlisted By L1' WHEN 3 THEN 'Rejected By L1' WHEN 4 THEN 'Shortlisted By L2'  WHEN 5 THEN 'Rejected By L2' ELSE '' END) as JobAplliedStatus,
                                (CASE COUNT(DISTINCT(JP.level)) WHEN 1 THEN 'L1' WHEN 2 THEN 'L2' ELSE '' END) AS JobLevel,
                                J.createdBy,
                                UJ.createdAt                            
                            FROM 
                                fj_userJob		UJ
                            JOIN 
                                fj_jobPanel 	JP,
                                fj_users            U,
                                fj_jobs        	J
                            WHERE
                                UJ.jobId=JP.jobId      AND
                                UJ.userId=U.id      	AND
                                UJ.jobId=J.id ";
                    if($date1!='' && $date2!='') {
                        $date1 = date('Y-m-d', strtotime($date1));
                        $date2 = date('Y-m-d', strtotime($date2));
                        $sql .= " AND DATE(UJ.createdAt) BETWEEN '$date1' AND '$date2' ";
                    }
                    else {
                        $sql .= " AND DATE(UJ.createdAt)<=CURDATE()";
                    }
                    $sql .= " GROUP BY UJ.id ORDER BY UJ.jobId ) aliasTable";
                $sql .= "   JOIN
                                fj_users U,
                                fj_users L1,
                                fj_users L2
                            WHERE
                                aliasTable.createdBy=U.id   AND
                                (aliasTable.JobLevel='L1' AND aliasTable.JobAplliedStatus='Shortlisted By L1')  AND
                                (CASE WHEN aliasTable.shorlistedByL1User IS NULL THEN '1=1' ELSE aliasTable.shorlistedByL1User=L1.id END)   AND
                                (CASE WHEN aliasTable.shorlistedByL2User IS NULL THEN '1=1' ELSE aliasTable.shorlistedByL2User=L2.id END)
                            GROUP BY 
                                aliasTable.id
                        ";
            $query  = $this->db->query($sql);
            $result = $query->result_array();            
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Description : Return job wise all the shortlisted candidates at Level2
     * Author : Synergy
     * @param array(date1, date2)
     * @return array of data 
     */
    function shorlistedCandidateAtL2CSV($date1,$date2){
        try {            
            $sql    = " 
                        SELECT
                            aliasTable.id,
                            aliasTable.AppUserName,
                            aliasTable.AppUserEmail,
                            aliasTable.AppUserMobile,
                            aliasTable.JobCode,
                            aliasTable.JobTitle,
                            aliasTable.JobAplliedStatus,
                            aliasTable.JobLevel, ";
            $sql   .= " (CASE WHEN aliasTable.shorlistedByL1User IS NULL THEN '' ELSE L1.fullname END) AS L1UserName, ";
            $sql   .= " (CASE WHEN aliasTable.shorlistedByL2User IS NULL THEN '' ELSE L2.fullname END) AS L2UserName,
                            U.fullname AS JobCreatedBy,
                            U.company  AS CompanyName,
                            aliasTable.createdAt
                        FROM
                            (
                            SELECT 
                                UJ.shorlistedByL1User,
                                UJ.shorlistedByL2User,
                                UJ.id,
                                U.fullname  AS AppUserName,
                                U.email     AS AppUserEmail,
                                U.mobile    AS AppUserMobile,
                                J.fjCode    AS JobCode,
                                J.title     AS JobTitle,
                                (CASE UJ.status WHEN 1 THEN 'Pending At L1' WHEN 2 THEN 'Shortlisted By L1' WHEN 3 THEN 'Rejected By L1' WHEN 4 THEN 'Shortlisted By L2'  WHEN 5 THEN 'Rejected By L2' ELSE '' END) as JobAplliedStatus,
                                (CASE COUNT(DISTINCT(JP.level)) WHEN 1 THEN 'L1' WHEN 2 THEN 'L2' ELSE '' END) AS JobLevel,
                                J.createdBy,
                                UJ.createdAt                            
                            FROM 
                                fj_userJob		UJ
                            JOIN 
                                fj_jobPanel 	JP,
                                fj_users            U,
                                fj_jobs        	J
                            WHERE
                                UJ.jobId=JP.jobId      AND
                                UJ.userId=U.id      	AND
                                UJ.jobId=J.id ";
                    if($date1!='' && $date2!='') {
                        $date1 = date('Y-m-d', strtotime($date1));
                        $date2 = date('Y-m-d', strtotime($date2));
                        $sql .= " AND DATE(UJ.createdAt) BETWEEN '$date1' AND '$date2' ";
                    }
                    else {
                        $sql .= " AND DATE(UJ.createdAt)<=CURDATE()";
                    }
                    $sql .= " GROUP BY UJ.id ORDER BY UJ.jobId ) aliasTable";
                $sql .= "   JOIN
                                fj_users U,
                                fj_users L1,
                                fj_users L2
                            WHERE
                                aliasTable.createdBy=U.id AND
                                (
                                    aliasTable.JobLevel='L2' AND aliasTable.JobAplliedStatus='Shortlisted By L2'
                                )  AND
                                (CASE WHEN aliasTable.shorlistedByL1User IS NULL THEN '1=1' ELSE aliasTable.shorlistedByL1User=L1.id END)   AND
                                (CASE WHEN aliasTable.shorlistedByL2User IS NULL THEN '1=1' ELSE aliasTable.shorlistedByL2User=L2.id END)
                            GROUP BY 
                                aliasTable.id
                        ";
            $query  = $this->db->query($sql);
            $result = $query->result_array();            
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Description : Return job wise all the rejected candidates
     * Author : Synergy
     * @param array(date1, date2)
     * @return array of data 
     */
    function rejectedCandidateCSV($date1,$date2){
        try {            
            $sql    = " 
                        SELECT
                            aliasTable.id,
                            aliasTable.AppUserName,
                            aliasTable.AppUserEmail,
                            aliasTable.AppUserMobile,
                            aliasTable.JobCode,
                            aliasTable.JobTitle,
                            aliasTable.JobAplliedStatus,
                            aliasTable.JobLevel, ";
                            //aliasTable.shorlistedByL1User,
            $sql   .= " (CASE WHEN aliasTable.shorlistedByL1User IS NULL THEN '' ELSE L1.fullname END) AS L1UserName, ";
                            //aliasTable.shorlistedByL2User,
            $sql   .= " (CASE WHEN aliasTable.shorlistedByL2User IS NULL THEN '' ELSE L2.fullname END) AS L2UserName,
                            U.fullname AS JobCreatedBy,
                            U.company  AS CompanyName,
                            aliasTable.createdAt
                        FROM
                            (
                            SELECT 
                                UJ.shorlistedByL1User,
                                UJ.shorlistedByL2User,
                                UJ.id,
                                U.fullname  AS AppUserName,
                                U.email     AS AppUserEmail,
                                U.mobile    AS AppUserMobile,
                                J.fjCode    AS JobCode,
                                J.title     AS JobTitle,
                                (CASE UJ.status WHEN 1 THEN 'Pending' WHEN 2 THEN 'Shortlisted By L1' WHEN 3 THEN 'Rejected By L1' WHEN 4 THEN 'Shortlisted By L2'  WHEN 5 THEN 'Rejected By L2' ELSE '' END) as JobAplliedStatus,
                                (CASE COUNT(DISTINCT(JP.level)) WHEN 1 THEN 'L1' WHEN 2 THEN 'L2' ELSE '' END) AS JobLevel,
                                J.createdBy,
                                UJ.createdAt                            
                            FROM 
                                fj_userJob		UJ
                            JOIN 
                                fj_jobPanel 	JP,
                                fj_users            U,
                                fj_jobs        	J
                            WHERE
                                UJ.jobId=JP.jobId      AND
                                UJ.userId=U.id      	AND
                                UJ.jobId=J.id ";
                    if($date1!='' && $date2!='') {
                        $date1 = date('Y-m-d', strtotime($date1));
                        $date2 = date('Y-m-d', strtotime($date2));
                        $sql .= " AND DATE(UJ.createdAt) BETWEEN '$date1' AND '$date2' ";
                    }
                    else {
                        $sql .= " AND DATE(UJ.createdAt)<=CURDATE()";
                    }
                    $sql .= " GROUP BY UJ.id ORDER BY UJ.jobId ) aliasTable";
                $sql .= "   JOIN
                                fj_users U,
                                fj_users L1,
                                fj_users L2
                            WHERE
                                aliasTable.createdBy=U.id AND
                                (
                                    aliasTable.JobLevel='L1' AND aliasTable.JobAplliedStatus='Rejected By L1'
                                    OR
                                    aliasTable.JobLevel='L2' AND aliasTable.JobAplliedStatus='Rejected By L2'
                                    OR
                                    aliasTable.JobLevel='L2' AND aliasTable.JobAplliedStatus='Rejected By L1'
                                )  AND
                                (CASE WHEN aliasTable.shorlistedByL1User IS NULL THEN '1=1' ELSE aliasTable.shorlistedByL1User=L1.id END)   AND
                                (CASE WHEN aliasTable.shorlistedByL2User IS NULL THEN '1=1' ELSE aliasTable.shorlistedByL2User=L2.id END)
                            GROUP BY 
                                aliasTable.id
                        ";
            $query  = $this->db->query($sql);
            $result = $query->result_array();            
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    

    /**
     * Description : Return job wise all the rejected candidates at Level1
     * Author : Synergy
     * @param array(date1, date2)
     * @return array of data 
     */
    function rejectedCandidateAtL1CSV($date1,$date2){
        try {            
            $sql    = " 
                        SELECT
                            aliasTable.id,
                            aliasTable.AppUserName,
                            aliasTable.AppUserEmail,
                            aliasTable.AppUserMobile,
                            aliasTable.JobCode,
                            aliasTable.JobTitle,
                            aliasTable.JobAplliedStatus,
                            aliasTable.JobLevel, ";
                            //aliasTable.shorlistedByL1User,
            $sql   .= " (CASE WHEN aliasTable.shorlistedByL1User IS NULL THEN '' ELSE L1.fullname END) AS L1UserName, ";
                            //aliasTable.shorlistedByL2User,
            $sql   .= " (CASE WHEN aliasTable.shorlistedByL2User IS NULL THEN '' ELSE L2.fullname END) AS L2UserName,
                            U.fullname AS JobCreatedBy,
                            U.company  AS CompanyName,
                            aliasTable.createdAt
                        FROM
                            (
                            SELECT 
                                UJ.shorlistedByL1User,
                                UJ.shorlistedByL2User,
                                UJ.id,
                                U.fullname  AS AppUserName,
                                U.email     AS AppUserEmail,
                                U.mobile    AS AppUserMobile,
                                J.fjCode    AS JobCode,
                                J.title     AS JobTitle,
                                (CASE UJ.status WHEN 1 THEN 'Pending At L1' WHEN 2 THEN 'Shortlisted By L1' WHEN 3 THEN 'Rejected By L1' WHEN 4 THEN 'Shortlisted By L2'  WHEN 5 THEN 'Rejected By L2' ELSE '' END) as JobAplliedStatus,
                                (CASE COUNT(DISTINCT(JP.level)) WHEN 1 THEN 'L1' WHEN 2 THEN 'L2' ELSE '' END) AS JobLevel,
                                J.createdBy,
                                UJ.createdAt                            
                            FROM 
                                fj_userJob		UJ
                            JOIN 
                                fj_jobPanel 	JP,
                                fj_users            U,
                                fj_jobs        	J
                            WHERE
                                UJ.jobId=JP.jobId      AND
                                UJ.userId=U.id      	AND
                                UJ.jobId=J.id ";
                    if($date1!='' && $date2!='') {
                        $date1 = date('Y-m-d', strtotime($date1));
                        $date2 = date('Y-m-d', strtotime($date2));
                        $sql .= " AND DATE(UJ.createdAt) BETWEEN '$date1' AND '$date2' ";
                    }
                    else {
                        $sql .= " AND DATE(UJ.createdAt)<=CURDATE()";
                    }
                    $sql .= " GROUP BY UJ.id ORDER BY UJ.jobId ) aliasTable";
                $sql .= "   JOIN
                                fj_users U,
                                fj_users L1,
                                fj_users L2
                            WHERE
                                aliasTable.createdBy=U.id AND
                                (
                                    aliasTable.JobLevel='L1' AND aliasTable.JobAplliedStatus='Rejected By L1'
                                    OR
                                    aliasTable.JobLevel='L2' AND aliasTable.JobAplliedStatus='Rejected By L1'
                                )  AND
                                (CASE WHEN aliasTable.shorlistedByL1User IS NULL THEN '1=1' ELSE aliasTable.shorlistedByL1User=L1.id END)   AND
                                (CASE WHEN aliasTable.shorlistedByL2User IS NULL THEN '1=1' ELSE aliasTable.shorlistedByL2User=L2.id END)
                            GROUP BY 
                                aliasTable.id
                        ";
            $query  = $this->db->query($sql);
            $result = $query->result_array();            
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Description : Return job wise all the rejected candidates at Level2
     * Author : Synergy
     * @param array(date1, date2)
     * @return array of data 
     */
    function rejectedCandidateAtL2CSV($date1,$date2){
        try {            
            $sql    = " 
                        SELECT
                            aliasTable.id,
                            aliasTable.AppUserName,
                            aliasTable.AppUserEmail,
                            aliasTable.AppUserMobile,
                            aliasTable.JobCode,
                            aliasTable.JobTitle,
                            aliasTable.JobAplliedStatus,
                            aliasTable.JobLevel, ";
            $sql   .= " (CASE WHEN aliasTable.shorlistedByL1User IS NULL THEN '' ELSE L1.fullname END) AS L1UserName, ";
            $sql   .= " (CASE WHEN aliasTable.shorlistedByL2User IS NULL THEN '' ELSE L2.fullname END) AS L2UserName,
                            U.fullname AS JobCreatedBy,
                            U.company  AS CompanyName,
                            aliasTable.createdAt
                        FROM
                            (
                            SELECT 
                                UJ.shorlistedByL1User,
                                UJ.shorlistedByL2User,
                                UJ.id,
                                U.fullname  AS AppUserName,
                                U.email     AS AppUserEmail,
                                U.mobile    AS AppUserMobile,
                                J.fjCode    AS JobCode,
                                J.title     AS JobTitle,
                                (CASE UJ.status WHEN 1 THEN 'Pending At L1' WHEN 2 THEN 'Shortlisted By L1' WHEN 3 THEN 'Rejected By L1' WHEN 4 THEN 'Shortlisted By L2'  WHEN 5 THEN 'Rejected By L2' ELSE '' END) as JobAplliedStatus,
                                (CASE COUNT(DISTINCT(JP.level)) WHEN 1 THEN 'L1' WHEN 2 THEN 'L2' ELSE '' END) AS JobLevel,
                                J.createdBy,
                                UJ.createdAt                            
                            FROM 
                                fj_userJob		UJ
                            JOIN 
                                fj_jobPanel 	JP,
                                fj_users            U,
                                fj_jobs        	J
                            WHERE
                                UJ.jobId=JP.jobId      AND
                                UJ.userId=U.id      	AND
                                UJ.jobId=J.id ";
                    if($date1!='' && $date2!='') {
                        $date1 = date('Y-m-d', strtotime($date1));
                        $date2 = date('Y-m-d', strtotime($date2));
                        $sql .= " AND DATE(UJ.createdAt) BETWEEN '$date1' AND '$date2' ";
                    }
                    else {
                        $sql .= " AND DATE(UJ.createdAt)<=CURDATE()";
                    }
                    $sql .= " GROUP BY UJ.id ORDER BY UJ.jobId ) aliasTable";
                $sql .= "   JOIN
                                fj_users U,
                                fj_users L1,
                                fj_users L2
                            WHERE
                                aliasTable.createdBy=U.id AND
                                (
                                    aliasTable.JobLevel='L2' AND aliasTable.JobAplliedStatus='Rejected By L2'
                                )  AND
                                (CASE WHEN aliasTable.shorlistedByL1User IS NULL THEN '1=1' ELSE aliasTable.shorlistedByL1User=L1.id END)   AND
                                (CASE WHEN aliasTable.shorlistedByL2User IS NULL THEN '1=1' ELSE aliasTable.shorlistedByL2User=L2.id END)
                            GROUP BY 
                                aliasTable.id
                        ";
            $query  = $this->db->query($sql);
            $result = $query->result_array();            
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Description : Return report of each application on jobs
     * Author : Synergy
     * @param array(date1, date2)
     * @return array of data 
     */
    function jobApplicationReport($date1,$date2){
        try { 
            $userData   = $this->session->userdata['logged_in'];
            $userRole   = $userData->role; 

            $sql    =   " 
                        SELECT
                            aliasTable.AppUserId,
                            aliasTable.Job_Code,
                            aliasTable.Job_Title,
                            aliasTable.Job_Has_Interview,
                            aliasTable.Job_Has_Assessment,
                            aliasTable.Job_Start_Date,
                            U.company AS Company_Name,

                            aliasTable.App_User_Name,
                            aliasTable.App_User_Email,
                            aliasTable.App_User_Mobile,
                            aliasTable.App_User_Work_Experience,
                            aliasTable.App_User_YOB,
                            aliasTable.App_user_Gender,
                            aliasTable.App_User_Pincode,
                            
                            aliasTable.Application_Receive_Date,
                            aliasTable.Job_Level,
                            aliasTable.Application_Status,

                            IF(aliasTable.shorlistedByL1User IS NULL, '', L1.fullname) AS L1_User_Name, 
                            aliasTable.L1_Action_Date,
                            IF(aliasTable.shorlistedByL2User IS NULL, '', L2.fullname) AS L2_User_Name,
                            aliasTable.L2_Action_Date
                        FROM
                            (
                            SELECT 
                                UJ.shorlistedByL1User,
                                UJ.shorlistedByL1UserDate                                                               AS L1_Action_Date,
                                UJ.shorlistedByL2User,
                                UJ.shorlistedByL2UserDate                                                               AS L2_Action_Date,
                                UJ.id,
                                J.fjCode                                                                                AS Job_Code,    
                                J.title                                                                                 AS Job_Title,  
                                IF(J.interview=0  || J.interview=NULL,  'No', 'Yes')                                    AS Job_Has_Interview,
                                IF(J.assessment=0 || J.assessment=NULL, 'No', 'Yes')                                    AS Job_Has_Assessment, 
                                DATE(UJ.createdAt)                                                                      AS Job_Start_Date,
                                U.id                                                                                    AS AppUserId,
                                U.fullname                                                                              AS App_User_Name,
                                U.email                                                                                 AS App_User_Email,
                                U.mobile                                                                                AS App_User_Mobile,
                                U.workExperience                                                                        AS App_User_Work_Experience,
                                YEAR(U.dob)                                                                             AS App_User_YOB,
                                IF(U.gender='1','Male','Female')                                                        AS App_user_Gender,   
                                U.pincode                                                                               AS App_User_Pincode,          
                                DATE(UJ.createdAt)                                                                      AS Application_Receive_Date,        
                                (CASE COUNT(DISTINCT(JP.level)) WHEN 1 THEN 'L1' WHEN 2 THEN 'L2' ELSE '' END)          AS Job_Level,
                                (CASE UJ.status WHEN 1 THEN 'Pending' WHEN 2 THEN 'Shortlisted By L1' WHEN 3 THEN 'Rejected By L1' WHEN 4 THEN 'Shortlisted By L2'  WHEN 5 THEN 'Rejected By L2' ELSE '' END) AS Application_Status,
                                J.createdBy
                            FROM        fj_userJob  UJ
                            LEFT JOIN   fj_jobPanel     JP  ON  UJ.jobId=JP.jobId
                            LEFT JOIN   fj_users        U   ON  UJ.userId=U.id
                            LEFT JOIN   fj_jobs         J   ON  UJ.jobId=J.id
                            WHERE
                                1=1 ";
            
                                if($date1!='' && $date2!='') {
                                    $date1 = date('Y-m-d', strtotime($date1));
                                    $date2 = date('Y-m-d', strtotime($date2));
                                    $sql .= "
                                            AND DATE(UJ.createdAt) BETWEEN '$date1' AND '$date2' 
                                            ";
                                }
                                else {
                                    $sql .= "
                                            AND DATE(UJ.createdAt)<=CURDATE() 
                                            ";
                                }
                                
                                if($userRole=='2'){
                                    $corporateId    = getCorporateUser($userData->id);
                                    $subUserIds     = getCorporateSubUser($corporateId, $userData->role);
                                    $subUserIds     = ($subUserIds!=''?','.$subUserIds:'');
                                    $sql .= " AND J.createdBy IN ('".$corporateId."'".$subUserIds.")";
                                }
                    $sql .= " 
                            GROUP BY 
                                UJ.id 
                            ORDER BY 
                                UJ.jobId 
                            ) aliasTable
                            ";
            $sql .= "   LEFT JOIN   fj_users    U     ON    aliasTable.createdBy=U.id
                        LEFT JOIN   fj_users    L1    ON    aliasTable.shorlistedByL1User=L1.id
                        LEFT JOIN   fj_users    L2    ON    aliasTable.shorlistedByL2User=L2.id
                        GROUP BY 
                            aliasTable.id
                        ";
            $query  = $this->db->query($sql);
            $result = $query->result_array();            
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    

    /**
     * Description : Return state of a particular app user
     * Author : Synergy
     * @param $param(pincode)
     * @return string
     */
    function getAppUserState($param){
        try { 
            $userData   = $this->session->userdata['logged_in'];
            $userRole   = $userData->role; 

            $sql    =   " 
                        SELECT 
                            GROUP_CONCAT(DISTINCT(P.state)) AS App_User_State
                        FROM 
                            fj_pinCode P WHERE P.pinCode='$param'
                        ";
            $query  = $this->db->query($sql);
            $result = $query->row_array();            
            return $result['App_User_State'];
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Description : Return qualification of a particular app user
     * Author : Synergy
     * @param $param(userId)
     * @return string
     */
    function getAppUserQualification($param){
        try { 
            $userData   = $this->session->userdata['logged_in'];
            $userRole   = $userData->role; 

            $sql    =   " 
                        SELECT 
                            GROUP_CONCAT(DISTINCT(C.name)) AS App_User_Qualification
                        FROM 
                            fj_userQualification UQ 
                        JOIN 
                            fj_courses C 
                        WHERE 
                            UQ.userId='$param'  AND 
                            UQ.courseId=C.id    AND
                            UQ.status='1'
                            
                        ";
            $query  = $this->db->query($sql);
            $result = $query->row_array();            
            return $result['App_User_Qualification'];
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Description : Return passout year from a degree of a particular app user
     * Author : Synergy
     * @param $param(userId)
     * @return string
     */
    function getAppUserQualificationYear($param){
        try { 
            $userData   = $this->session->userdata['logged_in'];
            $userRole   = $userData->role; 

            $sql    =   " 
                        SELECT 
                            GROUP_CONCAT(DISTINCT(UQ.completionYear),',') AS App_User_Qualification_Year
                        FROM 
                            fj_userQualification UQ 
                        JOIN 
                            fj_courses C 
                        WHERE 
                            UQ.userId='$param'  AND 
                            UQ.courseId=C.id    AND
                            UQ.status='1'
                        ";
            $query  = $this->db->query($sql);
            $result = $query->row_array();           
            return $result['App_User_Qualification_Year'];
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Description : Return sub user report like Level1 and Level2
     * Author : Synergy
     * @param $date1, $date2
     * @return array of data
     */
    function corporateSubUserReport($date1,$date2){
        try {  
            $userData   = $this->session->userdata['logged_in'];
            $userRole   = $userData->role; 
            $sql    =   " 
                        SELECT
                            aliasTable.AppUserId,
                            IF(aliasTable.shorlistedByL1User IS NULL, '', L1.fullname) AS L1_User_Name, 
                            aliasTable.L1_Action_Date,
                            IF(aliasTable.shorlistedByL2User IS NULL, '', L2.fullname) AS L2_User_Name,
                            aliasTable.L2_Action_Date,
                            
                            aliasTable.Job_Code,
                            aliasTable.Job_Title,
                            aliasTable.Job_Has_Interview,
                            aliasTable.Job_Has_Assessment,
                            aliasTable.Job_Start_Date,
                            U.company AS Company_Name,
                            
                            aliasTable.App_User_Name,
                            aliasTable.App_User_Email,
                            aliasTable.App_User_Mobile,
                            aliasTable.App_User_Work_Experience,
                            aliasTable.App_User_YOB,
                            aliasTable.App_user_Gender,
                            aliasTable.App_User_Pincode,
                            
                            aliasTable.Application_Receive_Date,                            
                            aliasTable.Job_Level,                             
                            aliasTable.Application_Status
                            
                        FROM
                            (
                            SELECT 
                                UJ.shorlistedByL1User,
                                UJ.shorlistedByL1UserDate                                                               AS L1_Action_Date,
                                UJ.shorlistedByL2User,
                                UJ.shorlistedByL2UserDate                                                               AS L2_Action_Date,
                                UJ.id,
                                J.fjCode                                                                                AS Job_Code,    
                                J.title                                                                                 AS Job_Title,  
                                IF(J.interview=0  || J.interview=NULL,  'No', 'Yes')                                    AS Job_Has_Interview,
                                IF(J.assessment=0 || J.assessment=NULL, 'No', 'Yes')                                    AS Job_Has_Assessment, 
                                DATE(UJ.createdAt)                                                                      AS Job_Start_Date,
                                U.id                                                                                    AS AppUserId,
                                U.fullname                                                                              AS App_User_Name,
                                U.email                                                                                 AS App_User_Email,
                                U.mobile                                                                                AS App_User_Mobile,
                                U.workExperience                                                                        AS App_User_Work_Experience,
                                YEAR(U.dob)                                                                             AS App_User_YOB,
                                IF(U.gender='1','Male','Female')                                                        AS App_user_Gender,   
                                U.pincode                                                                               AS App_User_Pincode,          
                                DATE(UJ.createdAt)                                                                      AS Application_Receive_Date,        
                                (CASE COUNT(DISTINCT(JP.level)) WHEN 1 THEN 'L1' WHEN 2 THEN 'L2' ELSE '' END)          AS Job_Level,
                                (CASE UJ.status WHEN 1 THEN 'Pending' WHEN 2 THEN 'Shortlisted By L1' WHEN 3 THEN 'Rejected By L1' WHEN 4 THEN 'Shortlisted By L2'  WHEN 5 THEN 'Rejected By L2' ELSE '' END) AS Application_Status,
                                J.createdBy
                            FROM        fj_userJob  UJ
                            LEFT JOIN   fj_jobPanel     JP  ON  UJ.jobId=JP.jobId
                            LEFT JOIN   fj_users        U   ON  UJ.userId=U.id
                            LEFT JOIN   fj_jobs         J   ON  UJ.jobId=J.id
                            WHERE
                                1=1 ";            
                                if($date1!='' && $date2!='') {
                                    $date1 = date('Y-m-d', strtotime($date1));
                                    $date2 = date('Y-m-d', strtotime($date2));
                                    $sql .= "
                                            AND DATE(UJ.createdAt) BETWEEN '$date1' AND '$date2' 
                                            ";
                                }
                                else {
                                    $sql .= "
                                            AND DATE(UJ.createdAt)<=CURDATE() 
                                            ";
                                }
                            
                                if($userRole=='2'){
                                    $corporateId    = getCorporateUser($userData->id);
                                    $subUserIds     = getCorporateSubUser($corporateId, $userData->role);
                                    $subUserIds     = ($subUserIds!=''?','.$subUserIds:'');
                                    $sql .= " AND J.createdBy IN ('".$corporateId."'".$subUserIds.")";
                                }
                    $sql .= " 
                            GROUP BY 
                                UJ.id 
                            ORDER BY 
                                UJ.jobId 
                            ) aliasTable
                            ";
            $sql .= "   LEFT JOIN   fj_users    U     ON    aliasTable.createdBy=U.id
                        LEFT JOIN   fj_users    L1    ON    aliasTable.shorlistedByL1User=L1.id
                        LEFT JOIN   fj_users    L2    ON    aliasTable.shorlistedByL2User=L2.id
                        GROUP BY 
                            aliasTable.id
                        ORDER BY
                            L1_User_Name, L2_User_Name
                        ";
            $query  = $this->db->query($sql);
            $result = $query->result_array();            
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
}
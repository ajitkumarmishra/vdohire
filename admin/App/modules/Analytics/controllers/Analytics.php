<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Analytics Controller
 * Description : Used to handle all analytics related data
 * @author Synergy
 * @createddate : Nov 2, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 15, 2017
 */
class Analytics extends MY_Controller {
    /**
     * Responsable for auto load the analytics_model,form_validation,session, email library
     * Responsable for auto load fj helper
     * Check Uri Segment to display specific page role wise
     * @return void
     */
    function __construct() {
        parent::__construct();
        $this->load->model('analytics_model');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('email');
        $this->load->helper('fj');
        $token = $this->config->item('accessToken');
        if (isset($_POST) && $_POST != NULL) {
            if ($token != $_POST['accessToken']) {
                //  echo 'You do not have pemission.';
                //exit;
            }
        }

        /**********Check any type of fraund activity*******/
        if ($this->session->userdata('logged_in')) {
            $userRealData   = $this->session->userdata['logged_in'];
            $uriStringSegment = $this->uri->uri_string();
            
            if($userRealData->role == 1) {
                $portal = 'SuperAdminPortal';
            }
            elseif ($userRealData->role == 2 || $userRealData->role == 4 || $userRealData->role == 5) {
                $portal = 'CorporatePortal';
            }
            else{
                $portal = '';
            }

            if($uriStringSegment == 'analytics/list') {
                if(!checkUserAccessForWebPage($userRealData->id, $userRealData->role, $portal, 'analytics/list')) {
                    $this->session->sess_destroy();
                    $insertFaultData['userId'] = $userRealData->id;
                    $insertFaultData['comment'] = 'This user try to access this this page';
                    $insertFaultData['pagePath'] = 'analytics/list';
                    $this->db->insert('fj_user_access_log', $insertFaultData);
                    redirect(base_url('users/login'));
                }
            }
            elseif($uriStringSegment == 'analytics/totalJobs') {
                if(!checkUserAccessForWebPage($userRealData->id, $userRealData->role, $portal, 'analytics/totalJobs')) {
                    $this->session->sess_destroy();
                    $insertFaultData['userId'] = $userRealData->id;
                    $insertFaultData['comment'] = 'This user try to access this this page';
                    $insertFaultData['pagePath'] = 'analytics/totalJobs';
                    $this->db->insert('fj_user_access_log', $insertFaultData);
                    redirect(base_url('users/login'));
                }
            }
            elseif($uriStringSegment == 'analytics/applicationViewedShortlistRatio') {
                if(!checkUserAccessForWebPage($userRealData->id, $userRealData->role, $portal, 'analytics/applicationViewedShortlistRatio')) {
                    $this->session->sess_destroy();
                    $insertFaultData['userId'] = $userRealData->id;
                    $insertFaultData['comment'] = 'This user try to access this this page';
                    $insertFaultData['pagePath'] = 'analytics/applicationViewedShortlistRatio';
                    $this->db->insert('fj_user_access_log', $insertFaultData);
                    redirect(base_url('users/login'));
                }
            }
            elseif($uriStringSegment == 'analytics/applicationSubUserViewed') {
                if(!checkUserAccessForWebPage($userRealData->id, $userRealData->role, $portal, 'analytics/applicationSubUserViewed')) {
                    $this->session->sess_destroy();
                    $insertFaultData['userId'] = $userRealData->id;
                    $insertFaultData['comment'] = 'This user try to access this this page';
                    $insertFaultData['pagePath'] = 'analytics/applicationSubUserViewed';
                    $this->db->insert('fj_user_access_log', $insertFaultData);
                    redirect(base_url('users/login'));
                }
            }
            elseif($uriStringSegment == 'analytics/pendingApplication') {
                if(!checkUserAccessForWebPage($userRealData->id, $userRealData->role, $portal, 'analytics/pendingApplication')) {
                    $this->session->sess_destroy();
                    $insertFaultData['userId'] = $userRealData->id;
                    $insertFaultData['comment'] = 'This user try to access this this page';
                    $insertFaultData['pagePath'] = 'analytics/pendingApplication';
                    $this->db->insert('fj_user_access_log', $insertFaultData);
                    redirect(base_url('users/login'));
                }
            }
            elseif($uriStringSegment == 'analytics/demographicMaleFemale') {
                if(!checkUserAccessForWebPage($userRealData->id, $userRealData->role, $portal, 'analytics/demographicMaleFemale')) {
                    $this->session->sess_destroy();
                    $insertFaultData['userId'] = $userRealData->id;
                    $insertFaultData['comment'] = 'This user try to access this this page';
                    $insertFaultData['pagePath'] = 'analytics/demographicMaleFemale';
                    $this->db->insert('fj_user_access_log', $insertFaultData);
                    redirect(base_url('users/login'));
                }
            }
            elseif($uriStringSegment == 'analytics/jobOpenRate') {
                if(!checkUserAccessForWebPage($userRealData->id, $userRealData->role, $portal, 'analytics/jobOpenRate')) {
                    $this->session->sess_destroy();
                    $insertFaultData['userId'] = $userRealData->id;
                    $insertFaultData['comment'] = 'This user try to access this this page';
                    $insertFaultData['pagePath'] = 'analytics/jobOpenRate';
                    $this->db->insert('fj_user_access_log', $insertFaultData);
                    redirect(base_url('users/login'));
                }
            }
            elseif($uriStringSegment == 'analytics/costToHire') {
                if(!checkUserAccessForWebPage($userRealData->id, $userRealData->role, $portal, 'analytics/costToHire')) {
                    $this->session->sess_destroy();
                    $insertFaultData['userId'] = $userRealData->id;
                    $insertFaultData['comment'] = 'This user try to access this this page';
                    $insertFaultData['pagePath'] = 'analytics/costToHire';
                    $this->db->insert('fj_user_access_log', $insertFaultData);
                    redirect(base_url('users/login'));
                }
            }
            elseif($uriStringSegment == 'analytics/timePerShortlist') {
                if(!checkUserAccessForWebPage($userRealData->id, $userRealData->role, $portal, 'analytics/timePerShortlist')) {
                    $this->session->sess_destroy();
                    $insertFaultData['userId'] = $userRealData->id;
                    $insertFaultData['comment'] = 'This user try to access this this page';
                    $insertFaultData['pagePath'] = 'analytics/timePerShortlist';
                    $this->db->insert('fj_user_access_log', $insertFaultData);
                    redirect(base_url('users/login'));
                }
            }
            elseif($uriStringSegment == 'analytics/reports') {
                if(!checkUserAccessForWebPage($userRealData->id, $userRealData->role, $portal, 'analytics/reports')) {
                    $this->session->sess_destroy();
                    $insertFaultData['userId'] = $userRealData->id;
                    $insertFaultData['comment'] = 'This user try to access this this page';
                    $insertFaultData['pagePath'] = 'analytics/reports';
                    $this->db->insert('fj_user_access_log', $insertFaultData);
                    redirect(base_url('users/login'));
                }
            }
        } 
        /****************End of handle fraud activity****************/
    }
    
    /**
     * Description : List all the analytics type like Jobs Status, Application Funnel
     * Author : Synergy
     * @param none
     * @return array of data 
     */
    function listAnalytics() {
		
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
		$userData   = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
           $userId          = $userData->id;
           $userByFullName  = $userData->fullname;
           $userCompany     = $userData->company;
           $createdById     = $userData->createdBy;
        } else {
            $userId = 1;
        }

		$userRole   = $userData->role;
		
		if($userRole == 1){
			$data['main_content']   = 'fj-analytics-listing';
			$this->load->view('fj-mainpage', $data);
		}else{
            $date1 = '';
            $date2 = '';
            $token = $this->config->item('accessToken');

            $data['token'] = $token;

			$data['breadcrumb'] = "analytics";
			
			$data['main_content']   = 'fj-recruiter-analytics-listing';
			
            //Job Status Report1
            $data['jobStatusAnalyticsData'] = $this->analytics_model->getJobStatusAnalyticsData($date1,$date2,$userId);

            //Recruiters by job report2
            $data['jobPendingApplicationData'] = $this->analytics_model->getjobPendingApplicationData($date1,$date2,$userId);
            //print '<pre>';print_r($data['jobPendingApplicationData']);exit;
            //$data['jobPendingApplicationData'] = array();//$this->analytics_model->getjobPendingApplicationData($date1,$date2,$userId);

            //Candidates invited report3
            $data['jobInvitationData'] = $this->analytics_model->getjobInvitationData($date1,$date2,$userId);
			
            //jobs-vacancies vs application4
            $data['jobFullfillmentData'] = $this->analytics_model->getjobFullfillmentData($date1,$date2,$userId);

            //Applications by job report5
            $data['applicantsByJob'] = $this->analytics_model->getApplicantsByJobData($date1,$date2,$userId);

            //Recruiter Activity Report6
            $data['recruitersStatus'] = $this->analytics_model->getRecruitersStatus($date1,$date2,$userId);

            //Application Funal Report7
            $data['applicansByJobRecruiter'] = $this->analytics_model->getApplicansByJobRecruiter($date1,$date2,$userId);

            //Applicant Profile By Job8
            $data['applicantProfileByJob'] = $this->analytics_model->getApplicantProfileByJob($date1,$date2,$userId);
				

            
            //print '<pre>';print_r($data['applicantProfileByJob']);exit;
			$this->load->view('fj-mainpage-recuiter', $data);
		}
		
        //Analyticss Listing Start

        //Analyticss Listing End
    }
	
	function tableView() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
		$userData   = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
           $userId          = $userData->id;
           $userByFullName  = $userData->fullname;
           $userCompany     = $userData->company;
           $createdById     = $userData->createdBy;
        } else {
            $userId = 1;
        }
		if(isset($_POST['block']) &&  ( $_POST['block'] == "jobStatus" || $_POST['block'] == 'jobStatusPopUp')){
			$data['jobStatusAnalyticsData'] = $this->analytics_model->getJobStatusAnalyticsData($date1,$date2,$userId);
			$data['block'] = 'jobStatus';
			$this->load->view('fj-jobStatus-tabularView', $data);
			die;
		}else if(isset($_POST['block']) &&  ( $_POST['block'] == "PendingApplication" || $_POST['block'] == 'PendingApplicationPopUp')){
			$data['jobPendingApplicationData'] = $this->analytics_model->getjobPendingApplicationData($date1,$date2,$userId);
			$data['block'] = 'PendingApplication';
			$this->load->view('fj-jobStatus-tabularView', $data);
			die;
		}else{
			echo "sdasds";die;
		}
		
    }
	
	function graphView() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
		$userData   = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
           $userId          = $userData->id;
           $userByFullName  = $userData->fullname;
           $userCompany     = $userData->company;
           $createdById     = $userData->createdBy;
        } else {
            $userId = 1;
        }
		if(isset($_POST['block']) &&  ( $_POST['block'] == "jobStatus" || $_POST['block'] == 'jobStatusPopUp')){
			$data['jobStatusAnalyticsData'] = $this->analytics_model->getJobStatusAnalyticsData($date1,$date2,$userId);
			
			$this->load->view('fj-jobStatus-graphView', $data);
			die;
		}else{
			echo "sdasds";die;
		}
		
    }
	
	function DownloadReport() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
		$userData   = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
           $userId          = $userData->id;
           $userByFullName  = $userData->fullname;
           $userCompany     = $userData->company;
           $createdById     = $userData->createdBy;
        } else {
            $userId = 1;
        }
		
		if(isset($_GET['block']) && ( $_GET['block'] == "jobStatus" || $_GET['block'] == 'jobStatusPopUp')){
			$jobStatusAnalyticsData = $this->analytics_model->getJobStatusAnalyticsData($date1,$date2,$userId);
			
			$filename = "jobStatus.csv";
			
			$f = fopen('php://memory', 'w'); 
			$content = "Job Code,Title,Created At,Fullname,Status,Posted,Invited,Total Responded,Total Invitation Responded \n";
			foreach($jobStatusAnalyticsData as $jobStatus){
				$content.= $jobStatus['fjCode'].",";
				$content.= $jobStatus['title'].",";
				$content.= $jobStatus['createdAt'].",";
				$content.= $jobStatus['fullname'].",";
				$content.= $jobStatus['status'].",";
				$content.= $jobStatus['posted'].",";
				$content.= $jobStatus['invited'].",";
				$content.= $jobStatus['totalResponded'].",";
				$content.= $jobStatus['totalinvitationresponded']."\n";
				
				//fputcsv($f, $jobStatus, ","); 
			}
			
			header('Content-Type: application/csv');
			// tell the browser we want to save it instead of displaying it
			header('Content-Disposition: attachment; filename="'.$filename.'";');
			// make php send the generated csv lines to the browser
			//fpassthru($f);
			echo $content;
			die;
			
		}else{
			echo "sdasds";die;
		}
		
    }
    
    
    /**
     * Description : List out all the jobs of Specific Corporate/Superadmin with Active and Inactive status with graph as well(Jobs Status fj-report-1)
     * Author : Synergy
     * @param none
     * @return array of data
     */
    function totalJobs() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token      = $this->config->item('accessToken');
        $userData   = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            redirect(base_url('users/login'));
            $userId = 1;
        }        
        
        if (isset($_POST) && $_POST != NULL) {
            $date1          = $this->input->post('fromDate', true);
            $date2          = $this->input->post('toDate', true);
            $resp['error']  = '';
            $fromEmail      = $this->config->item('fromEmail');
            if ($token != $this->input->post('accessToken', true)) {
                $resp['error'] = 'You do not have pemission.';
            }            
            $this->form_validation->set_rules('fromDate',   'From Date',    'trim|required');
            $this->form_validation->set_rules('toDate',     'To Date',      'trim|required');
            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }
            if (!$resp['error']) {
                $info['main_content']           = 'fj-report-1';
                $info['token']                  = $token;
                $info['active_jobs']            = $this->analytics_model->totalActiveJobs($date1,$date2);
                $info['inactive_jobs']          = $this->analytics_model->totalInactiveJobs($date1,$date2);
                $info['totalJobApplication']    = $this->analytics_model->totalJobApplication($date1,$date2);
                $this->load->view('fj-mainpage', $info);
            } 
            else {
                $info['main_content']           = 'fj-report-1';
                $info['token']                  = $token;
                $info['active_jobs']            = $this->analytics_model->totalActiveJobs();
                $info['inactive_jobs']          = $this->analytics_model->totalInactiveJobs();
                $info['totalJobApplication']    = $this->analytics_model->totalJobApplication();
                $this->load->view('fj-mainpage', $info);
            }
        } 
        else {
            $info['main_content']           = 'fj-report-1';
            $info['token']                  = $token;
            $info['active_jobs']            = $this->analytics_model->totalActiveJobs();
            $info['inactive_jobs']          = $this->analytics_model->totalInactiveJobs();
            $info['totalJobApplication']    = $this->analytics_model->totalJobApplication();
            $this->load->view('fj-mainpage', $info);
        }
    }
        
    /**
     * Description : List all the Applicant Funnel i.e how many jobs created, pending job application, rejected job application, shortlisted job application and level wise jobs application status with graph as well(Applicant Funnel fj-report-5 && fj-report-6)
     * Author : Synergy
     * @param none
     * @return array of data 
     */
    function applicationViewedShortlistRatio() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token      = $this->config->item('accessToken');
        $userData   = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            redirect(base_url('users/login'));
            $userId = 1;
        }        
        
        if (isset($_POST) && $_POST != NULL) {
            $date1          = $this->input->post('fromDate', true);
            $date2          = $this->input->post('toDate', true);
            $resp['error']  = '';
            $fromEmail      = $this->config->item('fromEmail');
            if ($token != $this->input->post('accessToken', true)) {
                $resp['error'] = 'You do not have pemission.';
            }            
            $this->form_validation->set_rules('fromDate',   'From Date',    'trim|required');
            $this->form_validation->set_rules('toDate',     'To Date',      'trim|required');
            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }
            if (!$resp['error']) {
                $info['main_content']   = 'fj-report-6';
                $info['token']          = $token;
                $info['applicationReceivedShortlistRatio']  = $this->analytics_model->applicationReceivedShortlistRatio($date1,$date2);
                $this->load->view('fj-mainpage', $info);
            } 
            else {
                $info['main_content']   = 'fj-report-6';
                $info['token']          = $token;
                $info['applicationReceivedShortlistRatio']  = $this->analytics_model->applicationReceivedShortlistRatio();
                $this->load->view('fj-mainpage', $info);
            }
        } 
        else {
            $info['main_content']   = 'fj-report-6';
            $info['token']          = $token;
            $info['applicationReceivedShortlistRatio']      = $this->analytics_model->applicationReceivedShortlistRatio();
            $this->load->view('fj-mainpage', $info);
        }
    }
    
    /**
     * Description : List all the sub user and their performance with graph as well(Sub User Performance fj-report-7)
     * Author : Synergy
     * @param none
     * @return array of data 
     */
    function applicationSubUserViewed() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token      = $this->config->item('accessToken');
        $userData   = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            redirect(base_url('users/login'));
            $userId = 1;
        }        
        
        if (isset($_POST) && $_POST != NULL) {
            $date1          = $this->input->post('fromDate', true);
            $date2          = $this->input->post('toDate', true);
            $resp['error']  = '';
            $fromEmail      = $this->config->item('fromEmail');
            if ($token != $this->input->post('accessToken', true)) {
                $resp['error'] = 'You do not have pemission.';
            }            
            $this->form_validation->set_rules('fromDate',   'From Date',    'trim|required');
            $this->form_validation->set_rules('toDate',     'To Date',      'trim|required');
            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }
            if (!$resp['error']) {
                $info['main_content']           = 'fj-report-7';
                $info['token']                  = $token;
                $info['jopPanelist']            = $this->analytics_model->jopPanelist();
                $info['applicationReceived']    = $this->analytics_model->applicationReceivedShortlistRatio($date1,$date2);
                $this->load->view('fj-mainpage', $info);
            } 
            else {
                $info['main_content']           = 'fj-report-7';
                $info['token']                  = $token;
                $info['jopPanelist']            = $this->analytics_model->jopPanelist();
                $info['applicationReceived']    = $this->analytics_model->applicationReceivedShortlistRatio();
                $this->load->view('fj-mainpage', $info);
            }
        } 
        else {
            $info['main_content']           = 'fj-report-7';
            $info['token']                  = $token;
            $info['jopPanelist']            = $this->analytics_model->jopPanelist();
            $info['applicationReceived']    = $this->analytics_model->applicationReceivedShortlistRatio();
            $this->load->view('fj-mainpage', $info);
        }
    }
    
    /**
     * Description : List all the pending application status for L1 and L2 sub user(Pending Application Status fj-report-2)
     * Author : Synergy
     * @param none
     * @return array of data 
     */
    function pendingApplication() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token      = $this->config->item('accessToken');
        $userData   = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            redirect(base_url('users/login'));
            $userId = 1;
        }        
        
        if (isset($_POST) && $_POST != NULL) {
            $date1          = $this->input->post('fromDate', true);
            $date2          = $this->input->post('toDate', true);
            $resp['error']  = '';
            $fromEmail      = $this->config->item('fromEmail');
            if ($token != $this->input->post('accessToken', true)) {
                $resp['error'] = 'You do not have pemission.';
            }            
            $this->form_validation->set_rules('fromDate',   'From Date',    'trim|required');
            $this->form_validation->set_rules('toDate',     'To Date',      'trim|required');
            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }
            if (!$resp['error']) {
                $info['main_content']   = 'fj-report-2';
                $info['token']          = $token;
                $info['pendingApplication']    = $this->analytics_model->pendingApplication($date1,$date2);
                $this->load->view('fj-mainpage', $info);
            } 
            else {
                $info['main_content']   = 'fj-report-2';
                $info['token']          = $token;
                $info['pendingApplication']    = $this->analytics_model->pendingApplication();
                $this->load->view('fj-mainpage', $info);
            }
        } 
        else {
            $info['main_content']   = 'fj-report-2';
            $info['token']          = $token;
            $info['pendingApplication']    = $this->analytics_model->pendingApplication();
            $this->load->view('fj-mainpage', $info);
        }
    }
    
    /**
     * Description : List all the applications demographics Status like city wise, male or female applications(fj-report-12)
     * Author : Synergy
     * @param none
     * @return array of data 
     */
    function demographicMaleFemale() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token      = $this->config->item('accessToken');
        $userData   = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            redirect(base_url('users/login'));
            $userId = 1;
        }        
        
        if (isset($_POST) && $_POST != NULL) {
            $date1          = $this->input->post('fromDate', true);
            $date2          = $this->input->post('toDate', true);
            $resp['error']  = '';
            $fromEmail      = $this->config->item('fromEmail');
            if ($token != $this->input->post('accessToken', true)) {
                $resp['error'] = 'You do not have pemission.';
            }            
            $this->form_validation->set_rules('fromDate',   'From Date',    'trim|required');
            $this->form_validation->set_rules('toDate',     'To Date',      'trim|required');
            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }
            if (!$resp['error']) {
                $info['main_content']   = 'fj-report-12';
                $info['token']          = $token;
                $info['applicationReceivedShortlistRatio']    = $this->analytics_model->applicationReceivedShortlistRatio($date1,$date2);
                $this->load->view('fj-mainpage', $info);
            } 
            else {
                $info['main_content']   = 'fj-report-12';
                $info['token']          = $token;
                $info['applicationReceivedShortlistRatio']    = $this->analytics_model->applicationReceivedShortlistRatio();
                $this->load->view('fj-mainpage', $info);
            }
        } 
        else {
            $info['main_content']   = 'fj-report-12';
            $info['token']          = $token;
            $info['applicationReceivedShortlistRatio']    = $this->analytics_model->applicationReceivedShortlistRatio();
            $this->load->view('fj-mainpage', $info);
        }
    }
    
    /**
     * Description : Display graph for total number of views of a particular jobs and multiple views from a candidate is counted separately(fj-report-13)
     * Author : Synergy
     * @param none
     * @return array of data 
     */
    function jobOpenRate() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token      = $this->config->item('accessToken');
        $userData   = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            redirect(base_url('users/login'));
            $userId = 1;
        }        
        
        if (isset($_POST) && $_POST != NULL) {
            $date1          = $this->input->post('fromDate', true);
            $date2          = $this->input->post('toDate', true);
            $resp['error']  = '';
            $fromEmail      = $this->config->item('fromEmail');
            if ($token != $this->input->post('accessToken', true)) {
                $resp['error'] = 'You do not have pemission.';
            }            
            $this->form_validation->set_rules('fromDate',   'From Date',    'trim|required');
            $this->form_validation->set_rules('toDate',     'To Date',      'trim|required');
            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }
            if (!$resp['error']) {
                $info['main_content']   = 'fj-report-13';
                $info['token']          = $token;
                $info['jobOpenRate']    = $this->analytics_model->jobOpenRate($date1,$date2);
                $this->load->view('fj-mainpage', $info);
            } 
            else {
                $info['main_content']   = 'fj-report-13';
                $info['token']          = $token;
                $info['jobOpenRate']    = $this->analytics_model->jobOpenRate();
                $this->load->view('fj-mainpage', $info);
            }
        } 
        else {
            $info['main_content']   = 'fj-report-13';
            $info['token']          = $token;
            $info['jobOpenRate']    = $this->analytics_model->jobOpenRate();
            $this->load->view('fj-mainpage', $info);
        }
    }
        
    /**
     * Description : Show shortlist ratio of total job applications and total shortlisted applications (fj-report-9)
     * Author : Synergy
     * @param array(fromDate, toDate)
     * @return array of data 
     */
    function costToHire() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token      = $this->config->item('accessToken');
        $userData   = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            redirect(base_url('users/login'));
            $userId = 1;
        }        
        
        if (isset($_POST) && $_POST != NULL) {
            $date1          = $this->input->post('fromDate', true);
            $date2          = $this->input->post('toDate', true);
            $resp['error']  = '';
            $fromEmail      = $this->config->item('fromEmail');
            if ($token != $this->input->post('accessToken', true)) {
                $resp['error'] = 'You do not have pemission.';
            }            
            $this->form_validation->set_rules('fromDate',   'From Date',    'trim|required');
            $this->form_validation->set_rules('toDate',     'To Date',      'trim|required');
            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }
            if (!$resp['error']) {
                $info['main_content']           = 'fj-report-9';
                $info['token']                  = $token;
                $info['applicationReceived']    = $this->analytics_model->applicationReceivedShortlistRatio($date1,$date2);
                $this->load->view('fj-mainpage', $info);
            } 
            else {
                $info['main_content']           = 'fj-report-9';
                $info['token']                  = $token;
                $info['applicationReceived']    = $this->analytics_model->applicationReceivedShortlistRatio();
                $this->load->view('fj-mainpage', $info);
            }
        } 
        else {
            $info['main_content']           = 'fj-report-9';
            $info['token']                  = $token;
            $info['applicationReceived']    = $this->analytics_model->applicationReceivedShortlistRatio();
            $this->load->view('fj-mainpage', $info);
        }
    }
    
    /**
     * Description : Show job wise shortlist ratio like Time Per Shortlisted (fj-report-11)
     * Author : Synergy
     * @param array(fromDate, toDate)
     * @return array of data 
     */
    function timeToHire() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token      = $this->config->item('accessToken');
        $userData   = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            redirect(base_url('users/login'));
            $userId = 1;
        }        
        
        if (isset($_POST) && $_POST != NULL) {
            $date1          = $this->input->post('fromDate', true);
            $date2          = $this->input->post('toDate', true);
            $resp['error']  = '';
            $fromEmail      = $this->config->item('fromEmail');
            if ($token != $this->input->post('accessToken', true)) {
                $resp['error'] = 'You do not have pemission.';
            }            
            $this->form_validation->set_rules('fromDate',   'From Date',    'trim|required');
            $this->form_validation->set_rules('toDate',     'To Date',      'trim|required');
            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }
            if (!$resp['error']) {
                $info['main_content']           = 'fj-report-11';
                $info['token']                  = $token;
                $info['timeToHireL1Shortlisted']    = $this->analytics_model->timeToHireL1Shortlisted($date1,$date2);
                $info['timeToHireL2Shortlisted']    = $this->analytics_model->timeToHireL2Shortlisted($date1,$date2);
                $this->load->view('fj-mainpage', $info);
            } 
            else {
                $info['main_content']           = 'fj-report-11';
                $info['token']                  = $token;
                $info['timeToHireL1Shortlisted']    = $this->analytics_model->timeToHireL1Shortlisted();
                $info['timeToHireL2Shortlisted']    = $this->analytics_model->timeToHireL2Shortlisted();
                $this->load->view('fj-mainpage', $info);
            }
        } 
        else {
            $info['main_content']           = 'fj-report-11';
            $info['token']                  = $token;
                $info['timeToHireL1Shortlisted']    = $this->analytics_model->timeToHireL1Shortlisted();
                $info['timeToHireL2Shortlisted']    = $this->analytics_model->timeToHireL2Shortlisted();
            $this->load->view('fj-mainpage', $info);
        }
    }
        
    /**
     * Description : Show job wise shortlist ratio like Time Per Shortlisted (fj-report-11)
     * Author : Synergy
     * @param array(fromDate, toDate)
     * @return array of data 
     */
    function timePerShortlist() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token      = $this->config->item('accessToken');
        $userData   = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            redirect(base_url('users/login'));
            $userId = 1;
        }        
        
        if (isset($_POST) && $_POST != NULL) {
            $date1          = $this->input->post('fromDate', true);
            $date2          = $this->input->post('toDate', true);
            $resp['error']  = '';
            $fromEmail      = $this->config->item('fromEmail');
            if ($token != $this->input->post('accessToken', true)) {
                $resp['error'] = 'You do not have pemission.';
            }            
            $this->form_validation->set_rules('fromDate',   'From Date',    'trim|required');
            $this->form_validation->set_rules('toDate',     'To Date',      'trim|required');
            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }
            if (!$resp['error']) {
                $info['main_content']       = 'fj-report-14';
                $info['token']              = $token;
                $info['timePerShortlist']   = $this->analytics_model->applicationReceivedShortlistRatio($date1,$date2);
                $this->load->view('fj-mainpage', $info);
            } 
            else {
                $info['main_content']       = 'fj-report-14';
                $info['token']              = $token;
                $info['timePerShortlist']   = $this->analytics_model->applicationReceivedShortlistRatio();
                $this->load->view('fj-mainpage', $info);
            }
        } 
        else {
            $info['main_content']        = 'fj-report-14';
            $info['token']               = $token;
            $info['timePerShortlist']    = $this->analytics_model->applicationReceivedShortlistRatio();
            $this->load->view('fj-mainpage', $info);
        }
    }
    
    /**
     * Description : List out all the received shortlisted ratio
     * Author : Synergy
     * @param array(fromDate, toDate)
     * @return array of data 
     */
    function applicationReceivedShortlistRatio() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token      = $this->config->item('accessToken');
        $userData   = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            redirect(base_url('users/login'));
            $userId = 1;
        }        
        
        if (isset($_POST) && $_POST != NULL) {
            $date1          = $this->input->post('fromDate', true);
            $date2          = $this->input->post('toDate', true);
            $resp['error']  = '';
            $fromEmail      = $this->config->item('fromEmail');
            if ($token != $this->input->post('accessToken', true)) {
                $resp['error'] = 'You do not have pemission.';
            }            
            $this->form_validation->set_rules('fromDate',   'From Date',    'trim|required');
            $this->form_validation->set_rules('toDate',     'To Date',      'trim|required');
            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }
            if (!$resp['error']) {
                $info['main_content']   = 'fj-report-5';
                $info['token']          = $token;
                $info['applicationReceivedShortlistRatio']  = $this->analytics_model->applicationReceivedShortlistRatio($date1,$date2);
                $this->load->view('fj-mainpage', $info);
            } 
            else {
                $info['main_content']   = 'fj-report-5';
                $info['token']          = $token;
                $info['applicationReceivedShortlistRatio']  = $this->analytics_model->applicationReceivedShortlistRatio();
                $this->load->view('fj-mainpage', $info);
            }
        } 
        else {
            $info['main_content']   = 'fj-report-5';
            $info['token']          = $token;
            $info['applicationReceivedShortlistRatio']      = $this->analytics_model->applicationReceivedShortlistRatio();
            $this->load->view('fj-mainpage', $info);
        }
    }
    
    /**
     * Description : List out all the application shortlisted by L1 and L2 user
     * Author : Synergy
     * @param array(fromDate, toDate)
     * @return array of data 
     */
    function applicationSubUserShortlist() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token      = $this->config->item('accessToken');
        $userData   = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            redirect(base_url('users/login'));
            $userId = 1;
        }        
        
        if (isset($_POST) && $_POST != NULL) {
            $date1          = $this->input->post('fromDate', true);
            $date2          = $this->input->post('toDate', true);
            $resp['error']  = '';
            $fromEmail      = $this->config->item('fromEmail');
            if ($token != $this->input->post('accessToken', true)) {
                $resp['error'] = 'You do not have pemission.';
            }            
            $this->form_validation->set_rules('fromDate',   'From Date',    'trim|required');
            $this->form_validation->set_rules('toDate',     'To Date',      'trim|required');
            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }
            if (!$resp['error']) {
                $info['main_content']           = 'fj-report-8';
                $info['token']                  = $token;
                $info['jopPanelist']            = $this->analytics_model->jopPanelist();
                $info['applicationReceived']    = $this->analytics_model->applicationReceivedShortlistRatio($date1,$date2);
                $this->load->view('fj-mainpage', $info);
            } 
            else {
                $info['main_content']           = 'fj-report-8';
                $info['token']                  = $token;
                $info['jopPanelist']            = $this->analytics_model->jopPanelist();
                $info['applicationReceived']    = $this->analytics_model->applicationReceivedShortlistRatio();
                $this->load->view('fj-mainpage', $info);
            }
        } 
        else {
            $info['main_content']           = 'fj-report-8';
            $info['token']                  = $token;
            $info['jopPanelist']            = $this->analytics_model->jopPanelist();
            $info['applicationReceived']    = $this->analytics_model->applicationReceivedShortlistRatio();
            $this->load->view('fj-mainpage', $info);
        }
    }

    /**
     * Description : List out all the application viewed 
     * Author : Synergy
     * @param array(fromDate, toDate)
     * @return array of data 
     */
    function applicationViewed() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token      = $this->config->item('accessToken');
        $userData   = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            redirect(base_url('users/login'));
            $userId = 1;
        }        
        
        if (isset($_POST) && $_POST != NULL) {
            $date1          = $this->input->post('fromDate', true);
            $date2          = $this->input->post('toDate', true);
            $resp['error']  = '';
            $fromEmail      = $this->config->item('fromEmail');
            if ($token != $this->input->post('accessToken', true)) {
                $resp['error'] = 'You do not have pemission.';
            }            
            $this->form_validation->set_rules('fromDate',   'From Date',    'trim|required');
            $this->form_validation->set_rules('toDate',     'To Date',      'trim|required');
            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }
            if (!$resp['error']) {
                $info['main_content']   = 'fj-report-3';
                $info['token']          = $token;
                $info['pendingApplication']    = $this->analytics_model->applicationViewed($date1,$date2);
                $this->load->view('fj-mainpage', $info);
            } 
            else {
                $info['main_content']   = 'fj-report-3';
                $info['token']          = $token;
                $info['pendingApplication']    = $this->analytics_model->applicationViewed();
                $this->load->view('fj-mainpage', $info);
            }
        } 
        else {
            $info['main_content']   = 'fj-report-3';
            $info['token']          = $token;
            $info['pendingApplication']    = $this->analytics_model->applicationViewed();
            $this->load->view('fj-mainpage', $info);
        }
    }

    /**
     * Description : List out reports of jobs
     * Author : Synergy
     * @param none
     * @return array of data 
     */
    function csvReports() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        //Analyticss Listing Start
        $data['main_content']   = 'csv-reports';
        $this->load->view('fj-mainpage', $data);
        //Analyticss Listing End
    }
    
    /**
     * Description : Create CSV for download the csv data
     * Author : Synergy
     * @param (fields, data, reportName)
     * @return array of data 
     */
    function createCSV($fields, $data, $reportName) {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }

        $displayresult.="<Table width='100%' border='1'  >";
        $displayresult.="<tr style='background:#c5be97'>";
        foreach($fields as $fieldName) {
            $displayresult.="<TD style='color:Black; text-align:center; font-size:16px; font-weight:bold;'>".ucwords($fieldName)."</TD>";
        }
        $displayresult.="</tr>";
        foreach($data as $key=>$fieldData) {
            $key++;
            if($key%2==0) { $css = "background-color:cyan;";	$className="#EFEFEF";	}
            else        { $css = "background-color:white;";	$className="#FFFFFF";   }		
            $displayresult.="<tr bgcolor=".$className.">";
            $displayresult.="<td class=$css>".$key."</td>";
            foreach(array_slice($fieldData,1) as $keyData=>$rowData) {
                $keyData++;
                $displayresult.="<td class=$css>".$rowData."</td>";
            }
            $displayresult.="</tr>";
        }			
    	$displayresult.="</Table>";
    	header('Content-Type: application/vnd.ms-excel; charset=utf-8');
    	header("Content-Disposition: attachment; filename=".$reportName.".xls");
    	header('Pragma: no-cache');
    	/*header('Content-Length: 2500000');*/
    	header('Expires: 0');
    	echo $displayresult;
    }

    /**
     * Description : Create CSV for download app user data
     * Author : Synergy
     * @param (fields, data, reportName)
     * @return array of data 
     */
    function appUserCSV() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token      = $this->config->item('accessToken');
        $userData   = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            redirect(base_url('users/login'));
            $userId = 1;
        }   
        $fields =   array(
                        'Id',
                        'UserName',
                        'EmailId',
                        'MobileNumber',
                        'DateOfBirth',
                        'Gender',
                        'FacebookUrl',
                        'LinkedinUrl',
                        'TwitterUrl',
                        'GooglePlusUrl',
                        'CourseName',
                        'CTC_ExpectedFrom',
                        'CTC_ExpectedTo',
                        'WorkExperience',
                        'AadharCardNumber',
                        'Pincode',
                        'RegistrationTime'
                    );
        $timestamp  = date('M-d-Y_H-i-s_A');
        $reportName = 'List_of_All_App_User_'.$timestamp;
        if (isset($_POST) && $_POST != NULL) {
            $date1          = $this->input->post('fromDate', true);
            $date2          = $this->input->post('toDate', true);
            $resp['error']  = '';
            $fromEmail      = $this->config->item('fromEmail');
            if ($token != $this->input->post('accessToken', true)) {
                $resp['error'] = 'You do not have pemission.';
            }            
            $this->form_validation->set_rules('fromDate',   'From Date',    'trim|required');
            $this->form_validation->set_rules('toDate',     'To Date',      'trim|required');
            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }
            if (!$resp['error']) {
                $data   = $this->analytics_model->appUserCSV($date1, $date2);
                $this->createCSV($fields, $data, $reportName);
            }
        } 
        else {
            $data   = $this->analytics_model->appUserCSV();
            $this->createCSV($fields, $data, $reportName);
        }
    }

    /**
     * Description : Create CSV for download corporate user list
     * Author : Synergy
     * @param (fields, data, reportName)
     * @return array of data 
     */
    function corporateUserCSV() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token      = $this->config->item('accessToken');
        $userData   = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            redirect(base_url('users/login'));
            $userId = 1;
        }   
        $fields =   array(
                        'Id',
                        'UserName',
                        'EmailId',
                        'MobileNumber',
                        'DateOfBirth',
                        'Gender',
                        'Location',
                        'CompanyName',
                        'CompanyAddress',
                        'RegistrationTime'
                    );
        $timestamp  = date('M-d-Y_H-i-s_A');
        $reportName = 'List_of_All_Corporate_User_'.$timestamp;
        if (isset($_POST) && $_POST != NULL) {
            $date1          = $this->input->post('fromDate', true);
            $date2          = $this->input->post('toDate', true);
            $resp['error']  = '';
            $fromEmail      = $this->config->item('fromEmail');
            if ($token != $this->input->post('accessToken', true)) {
                $resp['error'] = 'You do not have pemission.';
            }            
            $this->form_validation->set_rules('fromDate',   'From Date',    'trim|required');
            $this->form_validation->set_rules('toDate',     'To Date',      'trim|required');
            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }
            if (!$resp['error']) {
                $data   = $this->analytics_model->corporateUserCSV($date1, $date2);
                $this->createCSV($fields, $data, $reportName);
            }
        } 
        else {
            $data   = $this->analytics_model->corporateUserCSV();
            $this->createCSV($fields, $data, $reportName);
        }
    }
    
    /**
     * Description : Create CSV for download corporate sub user list
     * Author : Synergy
     * @param (fields, data, reportName)
     * @return array of data 
     */
    function corporateSubUserCSV() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token      = $this->config->item('accessToken');
        $userData   = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            redirect(base_url('users/login'));
            $userId = 1;
        }   
        $fields =   array(
                        'Id',
                        'CorporateName',
                        'UserName',
                        'EmailId',
                        'MobileNumber',
                        'DateOfBirth',
                        'Gender',
                        'Location',
                        'CompanyName',
                        'CompanyAddress',
                        'RegistrationTime'
                    );
        $timestamp  = date('M-d-Y_H-i-s_A');
        $reportName = 'List_of_All_Corporate_and_Sub_User_'.$timestamp;
        if (isset($_POST) && $_POST != NULL) {
            $date1          = $this->input->post('fromDate', true);
            $date2          = $this->input->post('toDate', true);
            $resp['error']  = '';
            $fromEmail      = $this->config->item('fromEmail');
            if ($token != $this->input->post('accessToken', true)) {
                $resp['error'] = 'You do not have pemission.';
            }            
            $this->form_validation->set_rules('fromDate',   'From Date',    'trim|required');
            $this->form_validation->set_rules('toDate',     'To Date',      'trim|required');
            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }
            if (!$resp['error']) {
                $data   = $this->analytics_model->corporateSubUserCSV($date1, $date2);
                $this->createCSV($fields, $data, $reportName);
            }
        } 
        else {
            $data   = $this->analytics_model->corporateSubUserCSV();
            $this->createCSV($fields, $data, $reportName);
        }
    }

    /**
     * Description : Create CSV for download all jobs related data
     * Author : Synergy
     * @param none
     * @return array of data 
     */
    function jobsCSV() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token      = $this->config->item('accessToken');
        $userData   = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            redirect(base_url('users/login'));
            $userId = 1;
        }   
        $fields =   array(
                        'id',
                        'fjCode',
                        'title',
                        'description',
                        'noOfVacancies',
                        'ageFrom',
                        'ageTo',
                        'salaryFrom',
                        'salaryTo',
                        'noticePeriod',
                        'expFrom',
                        'expTo',
                        'openTill',
                        'jobLocation',
                        'jobQualification',
                        'status',
                        'createdAt',
                        'createdBy'
                    );
        $timestamp  = date('M-d-Y_H-i-s_A');
        $reportName = 'Total_Number_Of_Jobs_Posted_'.$timestamp;
        if (isset($_POST) && $_POST != NULL) {
            $date1          = $this->input->post('fromDate', true);
            $date2          = $this->input->post('toDate', true);
            $resp['error']  = '';
            $fromEmail      = $this->config->item('fromEmail');
            if ($token != $this->input->post('accessToken', true)) {
                $resp['error'] = 'You do not have pemission.';
            }            
            $this->form_validation->set_rules('fromDate',   'From Date',    'trim|required');
            $this->form_validation->set_rules('toDate',     'To Date',      'trim|required');
            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }
            if (!$resp['error']) {
                $data   = $this->analytics_model->jobsCSV($date1, $date2);
                $this->createCSV($fields, $data, $reportName);
            }
        } 
        else {
            $data   = $this->analytics_model->jobsCSV();
            $this->createCSV($fields, $data, $reportName);
        }
    }
    
    /**
     * Description : Create CSV for download for firstjob's job
     * Author : Synergy
     * @param none
     * @return array of data 
     */
    function jobsOnFjCSV() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token      = $this->config->item('accessToken');
        $userData   = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            redirect(base_url('users/login'));
            $userId = 1;
        }  
        $fields =   array(
                        'id',
                        'fjCode',
                        'title',
                        'description',
                        'noOfVacancies',
                        'ageFrom',
                        'ageTo',
                        'salaryFrom',
                        'salaryTo',
                        'noticePeriod',
                        'expFrom',
                        'expTo',
                        'openTill',
                        'jobLocation',
                        'jobQualification',
                        'status',
                        'createdAt',
                        'createdBy'
                    );
        $timestamp  = date('M-d-Y_H-i-s_A');
        $reportName = 'Total_Number_Of_Jobs_Posted_On_FJ_'.$timestamp;
        if (isset($_POST) && $_POST != NULL) {
            $date1          = $this->input->post('fromDate', true);
            $date2          = $this->input->post('toDate', true);
            $resp['error']  = '';
            $fromEmail      = $this->config->item('fromEmail');
            if ($token != $this->input->post('accessToken', true)) {
                $resp['error'] = 'You do not have pemission.';
            }            
            $this->form_validation->set_rules('fromDate',   'From Date',    'trim|required');
            $this->form_validation->set_rules('toDate',     'To Date',      'trim|required');
            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }
            if (!$resp['error']) {
                $data   = $this->analytics_model->jobsOnFjCSV($date1, $date2);
                $this->createCSV($fields, $data, $reportName);
            }
        } 
        else {
            $data   = $this->analytics_model->jobsOnFjCSV();
            $this->createCSV($fields, $data, $reportName);
        }
    }

    /**
     * Description : Create CSV to download for videoJdCSV
     * Author : Synergy
     * @param none
     * @return array of data 
     */
    function videoJdCSV() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token      = $this->config->item('accessToken');
        $userData   = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            redirect(base_url('users/login'));
            $userId = 1;
        }   
        $fields =   array(
                        'id',
                        'fjCode',
                        'JD',
                        'title',
                        'noOfVacancies',
                        'ageFrom',
                        'ageTo',
                        'salaryFrom',
                        'salaryTo',
                        'noticePeriod',
                        'expFrom',
                        'expTo',
                        'openTill',
                        'jobLocation',
                        'jobQualification',
                        'status',
                        'createdAt',
                        'createdBy'
                    );
        $timestamp  = date('M-d-Y_H-i-s_A');
        $reportName = 'Total_Number_Of_Video_JDs_Used_By_Client_'.$timestamp;
        if (isset($_POST) && $_POST != NULL) {
            $date1          = $this->input->post('fromDate', true);
            $date2          = $this->input->post('toDate', true);
            $resp['error']  = '';
            $fromEmail      = $this->config->item('fromEmail');
            if ($token != $this->input->post('accessToken', true)) {
                $resp['error'] = 'You do not have pemission.';
            }            
            $this->form_validation->set_rules('fromDate',   'From Date',    'trim|required');
            $this->form_validation->set_rules('toDate',     'To Date',      'trim|required');
            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }
            if (!$resp['error']) {
                $data   = $this->analytics_model->videoJdCSV($date1, $date2);
                $this->createCSV($fields, $data, $reportName);
            }
        } 
        else {
            $data   = $this->analytics_model->videoJdCSV();
            $this->createCSV($fields, $data, $reportName);
        }
    }

    /**
     * Description : Create CSV to download for interview sets
     * Author : Synergy
     * @param none
     * @return array of data 
     */
    function interviewSetsCSV() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token      = $this->config->item('accessToken');
        $userData   = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            redirect(base_url('users/login'));
            $userId = 1;
        }
        $fields =   array(
                        'id',
                        'InterviewSetName',
                        'SetType',
                        'IndustryName',
                        'FunctionName',
                        'status',
                        'createdAt',
                        'createdBy'
                    );        
        $timestamp  = date('M-d-Y_H-i-s_A');
        $reportName = 'Total_Number_of_Interview_Sets_Created_'.$timestamp;
        if (isset($_POST) && $_POST != NULL) {
            $date1          = $this->input->post('fromDate', true);
            $date2          = $this->input->post('toDate', true);
            $resp['error']  = '';
            $fromEmail      = $this->config->item('fromEmail');
            if ($token != $this->input->post('accessToken', true)) {
                $resp['error'] = 'You do not have pemission.';
            }            
            $this->form_validation->set_rules('fromDate',   'From Date',    'trim|required');
            $this->form_validation->set_rules('toDate',     'To Date',      'trim|required');
            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }
            if (!$resp['error']) {
                $data   = $this->analytics_model->interviewSetsCSV($date1, $date2);
                $this->createCSV($fields, $data, $reportName);
            }
        } 
        else {
            $data   = $this->analytics_model->interviewSetsCSV();
            $this->createCSV($fields, $data, $reportName);
        }
    }
    
    /**
     * Description : Create CSV to download for interview video sets
     * Author : Synergy
     * @param none
     * @return array of data 
     */
    function interviewVideoSetsCSV() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token      = $this->config->item('accessToken');
        $userData   = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            redirect(base_url('users/login'));
            $userId = 1;
        }  
        $fields =   array(
                        'id',
                        'InterviewSetName',
                        'SetType',
                        'IndustryName',
                        'FunctionName',
                        'status',
                        'createdAt',
                        'createdBy'
                    );
        
        $timestamp  = date('M-d-Y_H-i-s_A');
        $reportName = 'Total_Number_of_Interview_Video_Sets_Created_'.$timestamp;
        if (isset($_POST) && $_POST != NULL) {
            $date1          = $this->input->post('fromDate', true);
            $date2          = $this->input->post('toDate', true);
            $resp['error']  = '';
            $fromEmail      = $this->config->item('fromEmail');
            if ($token != $this->input->post('accessToken', true)) {
                $resp['error'] = 'You do not have pemission.';
            }            
            $this->form_validation->set_rules('fromDate',   'From Date',    'trim|required');
            $this->form_validation->set_rules('toDate',     'To Date',      'trim|required');
            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }     
            if (!$resp['error']) {
                $data   = $this->analytics_model->interviewVideoSetsCSV($date1, $date2);
                $this->createCSV($fields, $data, $reportName);
            }
        } 
        else {
            $data   = $this->analytics_model->interviewVideoSetsCSV();
            $this->createCSV($fields, $data, $reportName);
        }
    }
    
    /**
     * Description : Create CSV to download for interview audio sets
     * Author : Synergy
     * @param none
     * @return array of data 
     */
    function interviewAudioSetsCSV() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token      = $this->config->item('accessToken');
        $userData   = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            redirect(base_url('users/login'));
            $userId = 1;
        }  
        $fields =   array(
                        'id',
                        'InterviewSetName',
                        'SetType',
                        'IndustryName',
                        'FunctionName',
                        'status',
                        'createdAt',
                        'createdBy'
                    );
        
        $timestamp  = date('M-d-Y_H-i-s_A');
        $reportName = 'Total_Number_of_Interview_Audio_Sets_Created_'.$timestamp;
        if (isset($_POST) && $_POST != NULL) {
            $date1          = $this->input->post('fromDate', true);
            $date2          = $this->input->post('toDate', true);
            $resp['error']  = '';
            $fromEmail      = $this->config->item('fromEmail');
            if ($token != $this->input->post('accessToken', true)) {
                $resp['error'] = 'You do not have pemission.';
            }            
            $this->form_validation->set_rules('fromDate',   'From Date',    'trim|required');
            $this->form_validation->set_rules('toDate',     'To Date',      'trim|required');
            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }     
            if (!$resp['error']) {
                $data   = $this->analytics_model->interviewAudioSetsCSV($date1, $date2);
                $this->createCSV($fields, $data, $reportName);
            }
        } 
        else {
            $data   = $this->analytics_model->interviewAudioSetsCSV();
            $this->createCSV($fields, $data, $reportName);
        }
    }
    
    /**
     * Description : Create CSV to download for interview text sets
     * Author : Synergy
     * @param none
     * @return array of data 
     */
    function interviewTextSetsCSV() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token      = $this->config->item('accessToken');
        $userData   = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            redirect(base_url('users/login'));
            $userId = 1;
        }  
        $fields =   array(
                        'id',
                        'InterviewSetName',
                        'SetType',
                        'IndustryName',
                        'FunctionName',
                        'status',
                        'createdAt',
                        'createdBy'
                    );
        
        $timestamp  = date('M-d-Y_H-i-s_A');
        $reportName = 'Total_Number_of_Interview_Text_Sets_Created_'.$timestamp;
        if (isset($_POST) && $_POST != NULL) {
            $date1          = $this->input->post('fromDate', true);
            $date2          = $this->input->post('toDate', true);
            $resp['error']  = '';
            $fromEmail      = $this->config->item('fromEmail');
            if ($token != $this->input->post('accessToken', true)) {
                $resp['error'] = 'You do not have pemission.';
            }            
            $this->form_validation->set_rules('fromDate',   'From Date',    'trim|required');
            $this->form_validation->set_rules('toDate',     'To Date',      'trim|required');
            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }     
            if (!$resp['error']) {
                $data   = $this->analytics_model->interviewTextSetsCSV($date1, $date2);
                $this->createCSV($fields, $data, $reportName);
            }
        } 
        else {
            $data   = $this->analytics_model->interviewTextSetsCSV();
            $this->createCSV($fields, $data, $reportName);
        }
    }
    
    /**
     * Description : Create CSV to download for interview question from firstjob
     * Author : Synergy
     * @param none
     * @return array of data 
     */
    function interviewQusFromFjCSV() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token      = $this->config->item('accessToken');
        $userData   = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            redirect(base_url('users/login'));
            $userId = 1;
        }
        $fields =   array(
                        'id',
                        'InterviewSetName',
                        'FjQuestionUsed'
                    );        
        $timestamp  = date('M-d-Y_H-i-s_A');
        $reportName = 'Number_of_Interview_Questions_Used_From_FJ_Database_'.$timestamp;
        if (isset($_POST) && $_POST != NULL) {
            $date1          = $this->input->post('fromDate', true);
            $date2          = $this->input->post('toDate', true);
            $resp['error']  = '';
            $fromEmail      = $this->config->item('fromEmail');
            if ($token != $this->input->post('accessToken', true)) {
                $resp['error'] = 'You do not have pemission.';
            }            
            $this->form_validation->set_rules('fromDate',   'From Date',    'trim|required');
            $this->form_validation->set_rules('toDate',     'To Date',      'trim|required');
            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }
            if (!$resp['error']) {
                $data   = $this->analytics_model->interviewQusFromFjCSV($date1, $date2);
                $this->createCSV($fields, $data, $reportName);
            }
        } 
        else {
            $data   = $this->analytics_model->interviewQusFromFjCSV();
            $this->createCSV($fields, $data, $reportName);
        }
    }
    
    /**
     * Description : Create CSV to download for interview question numbers from firstjob
     * Author : Synergy
     * @param array(fromDate, toDate)
     * @return array of data 
     */
    function interviewQusNumberCSV() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token      = $this->config->item('accessToken');
        $userData   = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            redirect(base_url('users/login'));
            $userId = 1;
        }
        $fields =   array(
                        'id',
                        'InterviewSetName',
                        'FjQuestionUsed'
                    );        
        $timestamp  = date('M-d-Y_H-i-s_A');
        $reportName = 'Number_of_Interview_Questions_Per_Interview_Set_'.$timestamp;
        if (isset($_POST) && $_POST != NULL) {
            $date1          = $this->input->post('fromDate', true);
            $date2          = $this->input->post('toDate', true);
            $resp['error']  = '';
            $fromEmail      = $this->config->item('fromEmail');
            if ($token != $this->input->post('accessToken', true)) {
                $resp['error'] = 'You do not have pemission.';
            }            
            $this->form_validation->set_rules('fromDate',   'From Date',    'trim|required');
            $this->form_validation->set_rules('toDate',     'To Date',      'trim|required');
            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }
            if (!$resp['error']) {
                $data   = $this->analytics_model->interviewQusNumberCSV($date1, $date2);
                $this->createCSV($fields, $data, $reportName);
            }
        } 
        else {
            $data   = $this->analytics_model->interviewQusNumberCSV();
            $this->createCSV($fields, $data, $reportName);
        }
    }

    /**
     * Description : Create CSV to download for interview assessment sets
     * Author : Synergy
     * @param array(fromDate, toDate)
     * @return array of data 
     */
    function assessmentSetsCSV() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token      = $this->config->item('accessToken');
        $userData   = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            redirect(base_url('users/login'));
            $userId = 1;
        }  
        $fields =   array(
                        'id',
                        'AssessmentSetName',
                        'Duration',
                        'status',
                        'createdAt',
                        'createdBy'
                    );            
        $timestamp  = date('M-d-Y_H-i-s_A');
        $reportName = 'Total_Number_of_Assessment_Sets_Created_'.$timestamp;
        if (isset($_POST) && $_POST != NULL) {
            $date1          = $this->input->post('fromDate', true);
            $date2          = $this->input->post('toDate', true);
            $resp['error']  = '';
            $fromEmail      = $this->config->item('fromEmail');
            if ($token != $this->input->post('accessToken', true)) {
                $resp['error'] = 'You do not have pemission.';
            }
            $this->form_validation->set_rules('fromDate',   'From Date',    'trim|required');
            $this->form_validation->set_rules('toDate',     'To Date',      'trim|required');
            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }
            if (!$resp['error']) {
                $data   = $this->analytics_model->assessmentSetsCSV($date1, $date2);
                $this->createCSV($fields, $data, $reportName);
            }
        } 
        else {
            $data   = $this->analytics_model->assessmentSetsCSV();
            $this->createCSV($fields, $data, $reportName);
        }
    }
    
    /**
     * Description : Create CSV to download for interview assessment question from first job
     * Author : Synergy
     * @param array(fromDate, toDate)
     * @return array of data 
     */
    function assessmentQusFromFjCSV() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token      = $this->config->item('accessToken');
        $userData   = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            redirect(base_url('users/login'));
            $userId = 1;
        }
        $fields =   array(
                        'id',
                        'AssessmentSetName',
                        'FjQuestionUsed'
                    );        
        $timestamp  = date('M-d-Y_H-i-s_A');
        $reportName = 'Number_of_Assessment_Questions_Used_From_FJ_Database_'.$timestamp;
        if (isset($_POST) && $_POST != NULL) {
            $date1          = $this->input->post('fromDate', true);
            $date2          = $this->input->post('toDate', true);
            $resp['error']  = '';
            $fromEmail      = $this->config->item('fromEmail');
            if ($token != $this->input->post('accessToken', true)) {
                $resp['error'] = 'You do not have pemission.';
            }            
            $this->form_validation->set_rules('fromDate',   'From Date',    'trim|required');
            $this->form_validation->set_rules('toDate',     'To Date',      'trim|required');
            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }
            if (!$resp['error']) {
                $data   = $this->analytics_model->assessmentQusFromFjCSV($date1, $date2);
                $this->createCSV($fields, $data, $reportName);
            }
        } 
        else {
            $data   = $this->analytics_model->assessmentQusFromFjCSV();
            $this->createCSV($fields, $data, $reportName);
        }
    }
    
    /**
     * Description : Create CSV to download for Number of Assessment Questions Per Assessment Set
     * Author : Synergy
     * @param array(fromDate, toDate)
     * @return array of data 
     */
    function assessmentQusNumberCSV() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token      = $this->config->item('accessToken');
        $userData   = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            redirect(base_url('users/login'));
            $userId = 1;
        }
        $fields =   array(
                        'id',
                        'AssessmentSetName',
                        'FjQuestionUsed'
                    );        
        $timestamp  = date('M-d-Y_H-i-s_A');
        $reportName = 'Number_of_Assessment_Questions_Per_Assessment_Set_'.$timestamp;
        if (isset($_POST) && $_POST != NULL) {
            $date1          = $this->input->post('fromDate', true);
            $date2          = $this->input->post('toDate', true);
            $resp['error']  = '';
            $fromEmail      = $this->config->item('fromEmail');
            if ($token != $this->input->post('accessToken', true)) {
                $resp['error'] = 'You do not have pemission.';
            }            
            $this->form_validation->set_rules('fromDate',   'From Date',    'trim|required');
            $this->form_validation->set_rules('toDate',     'To Date',      'trim|required');
            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }
            if (!$resp['error']) {
                $data   = $this->analytics_model->assessmentQusNumberCSV($date1, $date2);
                $this->createCSV($fields, $data, $reportName);
            }
        } 
        else {
            $data   = $this->analytics_model->assessmentQusNumberCSV();
            $this->createCSV($fields, $data, $reportName);
        }
    }

    /**
     * Description : Create CSV to download for job applied
     * Author : Synergy
     * @param array(fromDate, toDate)
     * @return array of data 
     */
    function jobAppliedCSV() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token      = $this->config->item('accessToken');
        $userData   = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            redirect(base_url('users/login'));
            $userId = 1;
        }   
        $fields =   array(
                        'id',
                        'AppUserName',
                        'AppUserEmail',
                        'AppUserMobile',
                        'Company',
                        'JobCode',
                        'JobTitle',
                        'status',
                        'createdAt'
                    );            
        $timestamp  = date('M-d-Y_H-i-s_A');
        $reportName = 'Total_Number_of_Candidates_Applied_For_a_Job_Post_'.$timestamp;
        if (isset($_POST) && $_POST != NULL) {
            $date1          = $this->input->post('fromDate', true);
            $date2          = $this->input->post('toDate', true);
            $resp['error']  = '';
            $fromEmail      = $this->config->item('fromEmail');
            if ($token != $this->input->post('accessToken', true)) {
                $resp['error'] = 'You do not have pemission.';
            }   
            $this->form_validation->set_rules('fromDate',   'From Date',    'trim|required');
            $this->form_validation->set_rules('toDate',     'To Date',      'trim|required');
            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }
            if (!$resp['error']) {
                $data   = $this->analytics_model->jobAppliedCSV($date1, $date2);
                $this->createCSV($fields, $data, $reportName);
            }
        } 
        else {
            $data   = $this->analytics_model->jobAppliedCSV();
            $this->createCSV($fields, $data, $reportName);
        }
    }  
    
    /**
     * Description : Create CSV to download for pending candidates
     * Author : Synergy
     * @param array(fromDate, toDate)
     * @return array of data 
     */
    function pendingCandidateCSV() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token      = $this->config->item('accessToken');
        $userData   = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            redirect(base_url('users/login'));
            $userId = 1;
        }   
        $fields =   array(
                        'id',
                        'AppUserName',
                        'AppUserEmail',
                        'AppUserMobile',
                        'JobCode',
                        'JobTitle',
                        'JobAplliedStatus',
                        'JobLevel',
                        'L1UserName',
                        'L2UserName',
                        'JobCreatedBy',
                        'CompanyName',
                        'createdAt'
                    );            
        $timestamp  = date('M-d-Y_H-i-s_A');
        $reportName = 'Total_Number_of_Pending_Candidates_'.$timestamp;
        if (isset($_POST) && $_POST != NULL) {
            $date1          = $this->input->post('fromDate', true);
            $date2          = $this->input->post('toDate', true);
            $resp['error']  = '';
            $fromEmail      = $this->config->item('fromEmail');
            if ($token != $this->input->post('accessToken', true)) {
                $resp['error'] = 'You do not have pemission.';
            }   
            $this->form_validation->set_rules('fromDate',   'From Date',    'trim|required');
            $this->form_validation->set_rules('toDate',     'To Date',      'trim|required');
            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }
            if (!$resp['error']) {
                $data   = $this->analytics_model->pendingCandidateCSV($date1, $date2);
                $this->createCSV($fields, $data, $reportName);
            }
        } 
        else {
            $data   = $this->analytics_model->pendingCandidateCSV();
            $this->createCSV($fields, $data, $reportName);
        }
    }
    
    /**
     * Description : Create CSV to download for pending candidates at level 1
     * Author : Synergy
     * @param array(fromDate, toDate)
     * @return array of data 
     */
    function pendingCandidateAtL1CSV() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token      = $this->config->item('accessToken');
        $userData   = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            redirect(base_url('users/login'));
            $userId = 1;
        }   
        $fields =   array(
                        'id',
                        'AppUserName',
                        'AppUserEmail',
                        'AppUserMobile',
                        'JobCode',
                        'JobTitle',
                        'JobAplliedStatus',
                        'JobLevel',
                        'L1UserName',
                        'L2UserName',
                        'JobCreatedBy',
                        'CompanyName',
                        'createdAt'
                    );            
        $timestamp  = date('M-d-Y_H-i-s_A');
        $reportName = 'Number_of_Applications_Pending_at_L1_'.$timestamp;
        if (isset($_POST) && $_POST != NULL) {
            $date1          = $this->input->post('fromDate', true);
            $date2          = $this->input->post('toDate', true);
            $resp['error']  = '';
            $fromEmail      = $this->config->item('fromEmail');
            if ($token != $this->input->post('accessToken', true)) {
                $resp['error'] = 'You do not have pemission.';
            }   
            $this->form_validation->set_rules('fromDate',   'From Date',    'trim|required');
            $this->form_validation->set_rules('toDate',     'To Date',      'trim|required');
            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }
            if (!$resp['error']) {
                $data   = $this->analytics_model->pendingCandidateAtL1CSV($date1, $date2);
                $this->createCSV($fields, $data, $reportName);
            }
        } 
        else {
            $data   = $this->analytics_model->pendingCandidateAtL1CSV();
            $this->createCSV($fields, $data, $reportName);
        }
    }
    
    /**
     * Description : Create CSV to download for pending candidates at level 2
     * Author : Synergy
     * @param array(fromDate, toDate)
     * @return array of data 
     */
    function pendingCandidateAtL2CSV() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token      = $this->config->item('accessToken');
        $userData   = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            redirect(base_url('users/login'));
            $userId = 1;
        }   
        $fields =   array(
                        'id',
                        'AppUserName',
                        'AppUserEmail',
                        'AppUserMobile',
                        'JobCode',
                        'JobTitle',
                        'JobAplliedStatus',
                        'JobLevel',
                        'L1UserName',
                        'L2UserName',
                        'JobCreatedBy',
                        'CompanyName',
                        'createdAt'
                    );            
        $timestamp  = date('M-d-Y_H-i-s_A');
        $reportName = 'Number_of_Applications_Pending_at_L2_'.$timestamp;
        if (isset($_POST) && $_POST != NULL) {
            $date1          = $this->input->post('fromDate', true);
            $date2          = $this->input->post('toDate', true);
            $resp['error']  = '';
            $fromEmail      = $this->config->item('fromEmail');
            if ($token != $this->input->post('accessToken', true)) {
                $resp['error'] = 'You do not have pemission.';
            }   
            $this->form_validation->set_rules('fromDate',   'From Date',    'trim|required');
            $this->form_validation->set_rules('toDate',     'To Date',      'trim|required');
            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }
            if (!$resp['error']) {
                $data   = $this->analytics_model->pendingCandidateAtL2CSV($date1, $date2);
                $this->createCSV($fields, $data, $reportName);
            }
        } 
        else {
            $data   = $this->analytics_model->pendingCandidateAtL2CSV();
            $this->createCSV($fields, $data, $reportName);
        }
    }
    
    /**
     * Description : Create CSV to download for all shortlisted candidates
     * Author : Synergy
     * @param array(fromDate, toDate)
     * @return array of data 
     */
    function shorlistedCandidateCSV() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token      = $this->config->item('accessToken');
        $userData   = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            redirect(base_url('users/login'));
            $userId = 1;
        }   
        $fields =   array(
                        'id',
                        'AppUserName',
                        'AppUserEmail',
                        'AppUserMobile',
                        'JobCode',
                        'JobTitle',
                        'JobAplliedStatus',
                        'JobLevel',
                        'L1UserName',
                        'L2UserName',
                        'JobCreatedBy',
                        'CompanyName',
                        'createdAt'
                    );            
        $timestamp  = date('M-d-Y_H-i-s_A');
        $reportName = 'Total_Number_of_Shorlisted_Candidates_'.$timestamp;
        if (isset($_POST) && $_POST != NULL) {
            $date1          = $this->input->post('fromDate', true);
            $date2          = $this->input->post('toDate', true);
            $resp['error']  = '';
            $fromEmail      = $this->config->item('fromEmail');
            if ($token != $this->input->post('accessToken', true)) {
                $resp['error'] = 'You do not have pemission.';
            }   
            $this->form_validation->set_rules('fromDate',   'From Date',    'trim|required');
            $this->form_validation->set_rules('toDate',     'To Date',      'trim|required');
            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }
            if (!$resp['error']) {
                $data   = $this->analytics_model->shorlistedCandidateCSV($date1, $date2);
                $this->createCSV($fields, $data, $reportName);
            }
        } 
        else {
            $data   = $this->analytics_model->shorlistedCandidateCSV();
            $this->createCSV($fields, $data, $reportName);
        }
    }
    
    /**
     * Description : Create CSV to download for all shortlisted candidates at level 1
     * Author : Synergy
     * @param array(fromDate, toDate)
     * @return array of data 
     */
    function shorlistedCandidateAtL1CSV() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token      = $this->config->item('accessToken');
        $userData   = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            redirect(base_url('users/login'));
            $userId = 1;
        }   
        $fields =   array(
                        'id',
                        'AppUserName',
                        'AppUserEmail',
                        'AppUserMobile',
                        'JobCode',
                        'JobTitle',
                        'JobAplliedStatus',
                        'JobLevel',
                        'L1UserName',
                        'L2UserName',
                        'JobCreatedBy',
                        'CompanyName',
                        'createdAt'
                    );            
        $timestamp  = date('M-d-Y_H-i-s_A');
        $reportName = 'Number_of_Applications_Shorlisted_at_L1_'.$timestamp;
        if (isset($_POST) && $_POST != NULL) {
            $date1          = $this->input->post('fromDate', true);
            $date2          = $this->input->post('toDate', true);
            $resp['error']  = '';
            $fromEmail      = $this->config->item('fromEmail');
            if ($token != $this->input->post('accessToken', true)) {
                $resp['error'] = 'You do not have pemission.';
            }   
            $this->form_validation->set_rules('fromDate',   'From Date',    'trim|required');
            $this->form_validation->set_rules('toDate',     'To Date',      'trim|required');
            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }
            if (!$resp['error']) {
                $data   = $this->analytics_model->shorlistedCandidateAtL1CSV($date1, $date2);
                $this->createCSV($fields, $data, $reportName);
            }
        } 
        else {
            $data   = $this->analytics_model->shorlistedCandidateAtL1CSV();
            $this->createCSV($fields, $data, $reportName);
        }
    }
    
    /**
     * Description : Create CSV to download for all shortlisted candidates at level 2
     * Author : Synergy
     * @param array(fromDate, toDate)
     * @return array of data 
     */
    function shorlistedCandidateAtL2CSV() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token      = $this->config->item('accessToken');
        $userData   = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            redirect(base_url('users/login'));
            $userId = 1;
        }   
        $fields =   array(
                        'id',
                        'AppUserName',
                        'AppUserEmail',
                        'AppUserMobile',
                        'JobCode',
                        'JobTitle',
                        'JobAplliedStatus',
                        'JobLevel',
                        'L1UserName',
                        'L2UserName',
                        'JobCreatedBy',
                        'CompanyName',
                        'createdAt'
                    );            
        $timestamp  = date('M-d-Y_H-i-s_A');
        $reportName = 'Number_of_Applications_Shorlisted_at_L2_'.$timestamp;
        if (isset($_POST) && $_POST != NULL) {
            $date1          = $this->input->post('fromDate', true);
            $date2          = $this->input->post('toDate', true);
            $resp['error']  = '';
            $fromEmail      = $this->config->item('fromEmail');
            if ($token != $this->input->post('accessToken', true)) {
                $resp['error'] = 'You do not have pemission.';
            }   
            $this->form_validation->set_rules('fromDate',   'From Date',    'trim|required');
            $this->form_validation->set_rules('toDate',     'To Date',      'trim|required');
            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }
            if (!$resp['error']) {
                $data   = $this->analytics_model->shorlistedCandidateAtL2CSV($date1, $date2);
                $this->createCSV($fields, $data, $reportName);
            }
        } 
        else {
            $data   = $this->analytics_model->shorlistedCandidateAtL2CSV();
            $this->createCSV($fields, $data, $reportName);
        }
    }
    
    /**
     * Description : Create CSV to download for all rejected candidates
     * Author : Synergy
     * @param array(fromDate, toDate)
     * @return array of data 
     */
    function rejectedCandidateCSV() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token      = $this->config->item('accessToken');
        $userData   = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            redirect(base_url('users/login'));
            $userId = 1;
        }   
        $fields =   array(
                        'id',
                        'AppUserName',
                        'AppUserEmail',
                        'AppUserMobile',
                        'JobCode',
                        'JobTitle',
                        'JobAplliedStatus',
                        'JobLevel',
                        'L1UserName',
                        'L2UserName',
                        'JobCreatedBy',
                        'CompanyName',
                        'createdAt'
                    );            
        $timestamp  = date('M-d-Y_H-i-s_A');
        $reportName = 'Total_Number_of_Rejected_Candidates_'.$timestamp;
        if (isset($_POST) && $_POST != NULL) {
            $date1          = $this->input->post('fromDate', true);
            $date2          = $this->input->post('toDate', true);
            $resp['error']  = '';
            $fromEmail      = $this->config->item('fromEmail');
            if ($token != $this->input->post('accessToken', true)) {
                $resp['error'] = 'You do not have pemission.';
            }   
            $this->form_validation->set_rules('fromDate',   'From Date',    'trim|required');
            $this->form_validation->set_rules('toDate',     'To Date',      'trim|required');
            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }
            if (!$resp['error']) {
                $data   = $this->analytics_model->rejectedCandidateCSV($date1, $date2);
                $this->createCSV($fields, $data, $reportName);
            }
        } 
        else {
            $data   = $this->analytics_model->rejectedCandidateCSV();
            $this->createCSV($fields, $data, $reportName);
        }
    }
    
    /**
     * Description : Create CSV to download for all rejected candidates at level 1
     * Author : Synergy
     * @param array(fromDate, toDate)
     * @return array of data 
     */
    function rejectedCandidateAtL1CSV() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token      = $this->config->item('accessToken');
        $userData   = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            redirect(base_url('users/login'));
            $userId = 1;
        }   
        $fields =   array(
                        'id',
                        'AppUserName',
                        'AppUserEmail',
                        'AppUserMobile',
                        'JobCode',
                        'JobTitle',
                        'JobAplliedStatus',
                        'JobLevel',
                        'L1UserName',
                        'L2UserName',
                        'JobCreatedBy',
                        'CompanyName',
                        'createdAt'
                    );            
        $timestamp  = date('M-d-Y_H-i-s_A');
        $reportName = 'Number_of_Applications_Rejected_at_L1_'.$timestamp;
        if (isset($_POST) && $_POST != NULL) {
            $date1          = $this->input->post('fromDate', true);
            $date2          = $this->input->post('toDate', true);
            $resp['error']  = '';
            $fromEmail      = $this->config->item('fromEmail');
            if ($token != $this->input->post('accessToken', true)) {
                $resp['error'] = 'You do not have pemission.';
            }   
            $this->form_validation->set_rules('fromDate',   'From Date',    'trim|required');
            $this->form_validation->set_rules('toDate',     'To Date',      'trim|required');
            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }
            if (!$resp['error']) {
                $data   = $this->analytics_model->rejectedCandidateAtL1CSV($date1, $date2);
                $this->createCSV($fields, $data, $reportName);
            }
        } 
        else {
            $data   = $this->analytics_model->rejectedCandidateAtL1CSV();
            $this->createCSV($fields, $data, $reportName);
        }
    }
    
    /**
     * Description : Create CSV to download for all rejected candidates at level 2
     * Author : Synergy
     * @param array(fromDate, toDate)
     * @return array of data 
     */
    function rejectedCandidateAtL2CSV() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token      = $this->config->item('accessToken');
        $userData   = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            redirect(base_url('users/login'));
            $userId = 1;
        }   
        $fields =   array(
                        'id',
                        'AppUserName',
                        'AppUserEmail',
                        'AppUserMobile',
                        'JobCode',
                        'JobTitle',
                        'JobAplliedStatus',
                        'JobLevel',
                        'L1UserName',
                        'L2UserName',
                        'JobCreatedBy',
                        'CompanyName',
                        'createdAt'
                    );            
        $timestamp  = date('M-d-Y_H-i-s_A');
        $reportName = 'Number_of_Applications_Rejected_at_L2_'.$timestamp;
        if (isset($_POST) && $_POST != NULL) {
            $date1          = $this->input->post('fromDate', true);
            $date2          = $this->input->post('toDate', true);
            $resp['error']  = '';
            $fromEmail      = $this->config->item('fromEmail');
            if ($token != $this->input->post('accessToken', true)) {
                $resp['error'] = 'You do not have pemission.';
            }   
            $this->form_validation->set_rules('fromDate',   'From Date',    'trim|required');
            $this->form_validation->set_rules('toDate',     'To Date',      'trim|required');
            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }
            if (!$resp['error']) {
                $data   = $this->analytics_model->rejectedCandidateAtL2CSV($date1, $date2);
                $this->createCSV($fields, $data, $reportName);
            }
        } 
        else {
            $data   = $this->analytics_model->rejectedCandidateAtL2CSV();
            $this->createCSV($fields, $data, $reportName);
        }
    }

    /**
     * Description : Create CSV to download for all invited candidates
     * Author : Synergy
     * @param array(fromDate, toDate)
     * @return array of data 
     */
    function invitedCandidateCSV() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token      = $this->config->item('accessToken');
        $userData   = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            redirect(base_url('users/login'));
            $userId = 1;
        }   
        $fields =   array(
                        'id',
                        'FjUserName',
                        'InvitedUserEmail',
                        'JobCode',
                        'JobTitle',
                        'status',
                        'InvitedBy',
                        'createdAt'
                    );            
        $timestamp  = date('M-d-Y_H-i-s_A');
        $reportName = 'List_of_All_Invited_Candidate_By_Corporate_'.$timestamp;
        if (isset($_POST) && $_POST != NULL) {
            $date1          = $this->input->post('fromDate', true);
            $date2          = $this->input->post('toDate', true);
            $resp['error']  = '';
            $fromEmail      = $this->config->item('fromEmail');
            if ($token != $this->input->post('accessToken', true)) {
                $resp['error'] = 'You do not have pemission.';
            }             
            $this->form_validation->set_rules('fromDate',   'From Date',    'trim|required');
            $this->form_validation->set_rules('toDate',     'To Date',      'trim|required');
            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }
            if (!$resp['error']) {
                $data   = $this->analytics_model->invitedCandidateCSV($date1, $date2);
                $this->createCSV($fields, $data, $reportName);
            }
        } 
        else {
            $data   = $this->analytics_model->invitedCandidateCSV();
            $this->createCSV($fields, $data, $reportName);
        }
    }
    
    /**
     * Description : Create CSV to download for all Candidates Invited Through FJ Database
     * Author : Synergy
     * @param array(fromDate, toDate)
     * @return array of data 
     */
    function invitedFjCandidateCSV() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token      = $this->config->item('accessToken');
        $userData   = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            redirect(base_url('users/login'));
            $userId = 1;
        }   
        $fields =   array(
                        'id',
                        'FjUserName',
                        'InvitedUserEmail',
                        'JobCode',
                        'JobTitle',
                        'status',
                        'InvitedBy',
                        'createdAt'
                    );            
        $timestamp  = date('M-d-Y_H-i-s_A');
        $reportName = 'Number_of_Candidates_Invited_Through_FJ_Database_'.$timestamp;
        if (isset($_POST) && $_POST != NULL) {
            $date1          = $this->input->post('fromDate', true);
            $date2          = $this->input->post('toDate', true);
            $resp['error']  = '';
            $fromEmail      = $this->config->item('fromEmail');
            if ($token != $this->input->post('accessToken', true)) {
                $resp['error'] = 'You do not have pemission.';
            }             
            $this->form_validation->set_rules('fromDate',   'From Date',    'trim|required');
            $this->form_validation->set_rules('toDate',     'To Date',      'trim|required');
            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }
            if (!$resp['error']) {
                $data   = $this->analytics_model->invitedFjCandidateCSV($date1, $date2);
                $this->createCSV($fields, $data, $reportName);
            }
        } 
        else {
            $data   = $this->analytics_model->invitedFjCandidateCSV();
            $this->createCSV($fields, $data, $reportName);
        }
    }

    /**
     * Description : Create CSV to download for all job application reports
     * Author : Synergy
     * @param array(fromDate, toDate)
     * @return array of data 
     */
    function jobApplicationReport() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token      = $this->config->item('accessToken');
        $userData   = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            redirect(base_url('users/login'));
            $userId = 1;
        }   
        $fields =   array(
                        'id',
                        'Job_Code',
                        'Job_Title',
                        'Job_Has_Interview',
                        'Job_Has_Assessment',
                        'Job_Start_Date',
                        'Company_Name',
            
                        'App_User_Name',
                        'App_User_Email',
                        'App_User_Mobile',
                        'App_User_Work_Experience',
                        'App_User_YOB',
                        'App_user_Gender',
                        'App_User_Pincode',
                        'App_User_State',
                        'App_User_Qualification',
                        'App_User_Qualification_Year',
            
                        'Application_Receive_Date',
                        'Job_Level',
                        'Application_Status',
                        
                        'L1_User_Name',
                        'L1_Action_Date',
                        'L2_User_Name',
                        'L2_Action_Date'
                    );            
        $timestamp  = date('M-d-Y_H-i-s_A');
        $reportName = 'Job_Application_Report_'.$timestamp;
        if (isset($_POST) && $_POST != NULL) {
            $date1          = $this->input->post('fromDate', true);
            $date2          = $this->input->post('toDate', true);
            $resp['error']  = '';
            $fromEmail      = $this->config->item('fromEmail');
            if ($token != $this->input->post('accessToken', true)) {
                $resp['error'] = 'You do not have pemission.';
            }             
            $this->form_validation->set_rules('fromDate',   'From Date',    'trim|required');
            $this->form_validation->set_rules('toDate',     'To Date',      'trim|required');
            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }
            if (!$resp['error']) {
                $data   = $this->analytics_model->jobApplicationReport($date1, $date2);
                $this->createCSV($fields, $data, $reportName);
            }
        } 
        else {
            $data   = $this->analytics_model->jobApplicationReport();
            $this->createCSV($fields, $data, $reportName);
        }
    }

    /**
     * Description : Create CSV to download for Corporate Sub Users Report
     * Author : Synergy
     * @param array(fromDate, toDate)
     * @return array of data 
     */
    function corporateSubUserReport() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token      = $this->config->item('accessToken');
        $userData   = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            redirect(base_url('users/login'));
            $userId = 1;
        }   
        $fields =   array(
                        'id',
                                    
                        'L1_User_Name',
                        'L1_Action_Date',
                        'L2_User_Name',
                        'L2_Action_Date',
            
                        'Job_Code',
                        'Job_Title',
                        'Job_Has_Interview',
                        'Job_Has_Assessment',
                        'Job_Start_Date',
                        'Company_Name',
            
                        'App_User_Name',
                        'App_User_Email',
                        'App_User_Mobile',
                        'App_User_Work_Experience',
                        'App_User_YOB',
                        'App_user_Gender',
                        'App_User_Pincode',
                        'App_User_State',
                        'App_User_Qualification',
                        'App_User_Qualification_Year',
            
                        'Application_Receive_Date',
                        'Job_Level',
                        'Application_Status'
                    );            
        $timestamp  = date('M-d-Y_H-i-s_A');
        $reportName = 'Corporate_Sub_User_Report'.$timestamp;
        if (isset($_POST) && $_POST != NULL) {
            $date1          = $this->input->post('fromDate', true);
            $date2          = $this->input->post('toDate', true);
            $resp['error']  = '';
            $fromEmail      = $this->config->item('fromEmail');
            if ($token != $this->input->post('accessToken', true)) {
                $resp['error'] = 'You do not have pemission.';
            }             
            $this->form_validation->set_rules('fromDate',   'From Date',    'trim|required');
            $this->form_validation->set_rules('toDate',     'To Date',      'trim|required');
            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }
            if (!$resp['error']) {
                $data   = $this->analytics_model->corporateSubUserReport($date1, $date2);
                $this->createCSV($fields, $data, $reportName);
            }
        } 
        else {
            $data   = $this->analytics_model->corporateSubUserReport();
            $this->createCSV($fields, $data, $reportName);
        }
    }
}
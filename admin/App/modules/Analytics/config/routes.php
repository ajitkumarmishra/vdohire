<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['analytics/list']                 = "Analytics/listAnalytics";
$route['analytics/list/(:any)']          = "Analytics/listAnalytics/$1";

$route['analytics/question']             = "Analytics/addAnalyticsQuestion";
$route['analytics/addAnalytics']          = "Analytics/addAnalytics";
$route['analytics/view/(:any)']          = "Analytics/getAnalytics/$1";
$route['analytics/delete']               = "Analytics/deleteAnalyticsSet";
$route['analytics/edit/(:any)']          = "Analytics/editAnalytics/$1";
$route['getAnalyticsSet']                = "Analytics/getAnalyticsSet";
$route['analytics/addToAnalytics']        = "Analytics/addQuestionToAnalyticsSet";
$route['checkAnalytics']                 = "Analytics/checkAnalytics";

$route['getQuestionRow']                = "Analytics/getQuestionRow";
$route['getAnalyticsQuestionRow']        = "Analytics/getAnalyticsQuestionRow";
$route['analytics/editQuestion/(:any)']  = "Analytics/editQuestion/$1";

$route['analytics/reports']                 = "Analytics/csvReports";
$route['analytics/jobsCSV']                 = "Analytics/jobsCSV";
$route['analytics/jobsOnFjCSV']             = "Analytics/jobsOnFjCSV";
$route['analytics/interviewSetsCSV']        = "Analytics/interviewSetsCSV";
$route['analytics/interviewVideoSetsCSV']   = "Analytics/interviewVideoSetsCSV";
$route['analytics/assessmentSetsCSV']       = "Analytics/assessmentSetsCSV";
$route['analytics/jobAppliedCSV']           = "Analytics/jobAppliedCSV";
$route['analytics/shorlistedCandidateCSV']  = "Analytics/shorlistedCandidateCSV";
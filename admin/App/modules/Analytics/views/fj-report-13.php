<?php //echo "<pre>";print_r($contentMyQuestions); die();   ?>
<div id="page-content-wrapper">

    <div class="container-fluid whitebg">
        <div class="row">
            <div class="col-md-9"><h2><img src="<?php echo base_url(); ?>/theme/firstJob/image/arrow_head.png" class="prev-page">Job Open Rate</h2></div>

            <div class="clearfix"></div>

            <div class="col-md-12 rsp_tbl">
                <div class="tab-content">
                    <div id="home" class="tab-pane fade active in">
                        <form class="form-vertical form_save" action="" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="inputEmail" class="control-label col-xs-1 required-field">From</label>
                                <div class="col-xs-2">
                                    <input type="text" class="form-control" placeholder="From Date" id="datepicker-13" value="<?php
                                        if (isset($_POST['fromDate'])): echo $_POST['fromDate'];
                                        endif;
                                        ?>" name="fromDate">
                                </div>
                            </div> 
                            <div class="form-group">
                                <label for="inputEmail" class="control-label col-xs-1 required-field">To</label>
                                <div class="col-xs-2">
                                    <input type="text" class="form-control" placeholder="To Date" id="datepicker-14" value="<?php
                                        if (isset($_POST['toDate'])): echo $_POST['toDate'];
                                        endif;
                                        ?>" name="toDate">
                                </div>
                            </div>                        
                            <div class="col-xs-offset-1 col-xs-4 no_padding">
                                <input type="hidden" name="accessToken" value="<?php echo $token; ?>"/>
                                <button type="submit" class="Save_frm">Search</button>
                                <button type="reset" class="Save_frm">Reset</button>
                            </div>
                        </form>
                        
                        <br><br><br>
                        <?php
                        //echo "<pre>"; print_r($jobOpenRate); 
                        $i      = 1;
                        $str1   = '';
                        $str2   = '';
                        $str3   = '';
                        foreach ($jobOpenRate as $jobList) {
                            if($str1=='' && $str2=='') {
//                                $str1 .= "[".$jobList['jobSearchCount'].",'".$jobList['fjCode']."']";
//                                $str2 .= "[".$jobList['jobCodeCount'].",'".$jobList['fjCode']."']";
                                $str1 .= "[".$jobList['jobSearchCount'].",".$i."]";
                                $str2 .= "[".$jobList['jobCodeCount'].",".$i."]";
                                $str3 .= "'".$jobList['fjCode']."'";
                            }
                            else {
//                                $str1 .= ", [".$jobList['jobSearchCount'].",'".$jobList['fjCode']."']";
//                                $str2 .= ", [".$jobList['jobCodeCount'].",'".$jobList['fjCode']."']";
                                $str1 .= ", [".$jobList['jobSearchCount'].",".$i."]";
                                $str2 .= ", [".$jobList['jobCodeCount'].",".$i."]";
                                $str3 .= ", '".$jobList['fjCode']."'";
                            }
                            $i++;                            
                        }
                        ?>
                        
                        
                        <div id="chart1" style="margin-top:50px; margin-left:0px; width:90%; height:500px; float: left"></div>
                        
                        <script class="code" type="text/javascript">
                            $(document).ready(function(){
                                ticks = [<?=$str3;?>];
                                plot4 = $.jqplot('chart1',   [
                                                                [<?=$str1;?>],
                                                                [<?=$str2;?>],
                                                            ], {
                                            title: 'Graph shows the total number of views of a particular jobs and multiple views from a candidate is counted separately',
                                            stackSeries: true,
                                            captureRightClick: true,
                                            seriesDefaults:{
                                                renderer:$.jqplot.BarRenderer,
                                                shadowAngle: 135,
                                                rendererOptions: {
                                                    barDirection: 'horizontal',
                                                      // Highlight bars when mouse button pressed.
                                                      // Disables default highlighting on mouse over.
                                                      highlightMouseDown: true   
                                                },
                                                pointLabels: {show: true}
                                              },
                                            cursor:{
                                                show: true, 
                                                zoom: true
                                            },
                                            legend: { 
                                                renderer: jQuery.jqplot.EnhancedLegendRenderer,
                                                show: true, 
                                                location: 'n', 
                                                placement: 'outsideGrid',
                                                rendererOptions: {
                                                  numberRows: '1',
                                                },
                                            },
                                            axes: {
                                                yaxis: {
                                                    renderer: $.jqplot.CategoryAxisRenderer,
                                                    ticks: ticks
                                                }
                                            },
                                            grid: {
                                                drawGridlines: true,
                                                drawBorder: true,
                                                shadow: true
                                            },
                                            series: [
                                                { label: 'Open By Search' },
                                                { label: 'Open By Job Code' }
                                            ],
                                        });
                                    });
                        </script> 
                        
                    </div>
                </div>                
            </div>

        </div>
    </div>
</div>
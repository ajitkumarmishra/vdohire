<?php //echo "<pre>";print_r($contentMyQuestions); die();   ?>
<div id="page-content-wrapper">

    <div class="container-fluid whitebg">
        <div class="row">
            <div class="col-md-9"><h2><img src="<?php echo base_url(); ?>/theme/firstJob/image/arrow_head.png" class="prev-page">Sub User Name Vs Number of applications viewed (shortlisted + Rejected) VS Number of jobs created</h2></div>

            <div class="clearfix"></div>

            <div class="col-md-12 rsp_tbl">
                <div class="tab-content">
                    <div id="home" class="tab-pane fade active in">
                        <form class="form-vertical form_save" action="" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="inputEmail" class="control-label col-xs-1 required-field">From</label>
                                <div class="col-xs-2">
                                    <input type="text" class="form-control" placeholder="From Date" id="datepicker-13" value="<?php
                                        if (isset($_POST['fromDate'])): echo $_POST['fromDate'];
                                        endif;
                                        ?>" name="fromDate">
                                </div>
                            </div> 
                            <div class="form-group">
                                <label for="inputEmail" class="control-label col-xs-1 required-field">To</label>
                                <div class="col-xs-2">
                                    <input type="text" class="form-control" placeholder="To Date" id="datepicker-14" value="<?php
                                        if (isset($_POST['toDate'])): echo $_POST['toDate'];
                                        endif;
                                        ?>" name="toDate">
                                </div>
                            </div>                        
                            <div class="col-xs-offset-1 col-xs-4 no_padding">
                                <input type="hidden" name="accessToken" value="<?php echo $token; ?>"/>
                                <button type="submit" class="Save_frm">Search</button>
                                <button type="reset" class="Save_frm">Reset</button>
                            </div>
                        </form>
                        
                        <br><br><br>
                        
                        <?php
                        $L1userPendingJobs = 0;
                        $L2userPendingJobs = 0;
                        foreach($pendingApplication as $jobApplication) {
                            $sql    = "SELECT Distinct(level) FROM fj_jobpanel WHERE jobId='$jobApplication[jobId]'";
                            $query  = $this->db->query($sql);
                            $result = $query->result_array();
                            if(count($result)==1) {
                                if($jobApplication['status']=='1')
                                $L1userPendingJobs++;
                            }
                            if(count($result)==2) {
                                //print_r($jobApplication);
                                if($jobApplication['status']=='2')
                                $L2userPendingJobs++;
                                if($jobApplication['status']=='1')
                                $L1userPendingJobs++;
                            }
                        }                        
                        ?>
                        
                        
                        <h4>Pending Job Application At L1 User - <?php echo $L1userPendingJobs; ?></h4>
                        <h4>Pending Job Application At L2 User - <?php echo $L2userPendingJobs; ?></h4>

                        <div id="chart1" style="margin-top:20px; margin-left:20px; width:300px; height:300px; float: left"></div>
                        <div id="chart6" style="margin-top:20px; margin-left:20px; width:300px; height:300px; float: right"></div>

                        <script class="code" type="text/javascript">
                            $(document).ready(function () {
                                $.jqplot.config.enablePlugins = true;
                                var s1      = [<?php echo $L1userPendingJobs; ?>, <?php echo $L2userPendingJobs; ?>];
                                var ticks   = ['L1 Pending Jobs', 'L2 Pending Jobs'];

                                plot1 = $.jqplot('chart1', [s1], {
                                            // Only animate if we're not using excanvas (not in IE 7 or IE 8)..
                                            animate:        !$.jqplot.use_excanvas,
                                            seriesDefaults:     {
                                                                    renderer: $.jqplot.BarRenderer,
                                                                    pointLabels: {show: true}
                                                                },
                                            axes:       {
                                                                    xaxis: {
                                                                            renderer: $.jqplot.CategoryAxisRenderer,
                                                                            ticks: ticks
                                                                            }
                                                                },
                                            highlighter:    {show: false}
                                        });

                                $('#chart1').bind('jqplotDataClick',
                                    function (ev, seriesIndex, pointIndex, data) {
                                        $('#info1').html('series: ' + seriesIndex + ', point: ' + pointIndex + ', data: ' + data);
                                    }
                                );
                            });

                            $(document).ready(function () {
                                var plot1 = $.jqplot('chart6', [[['L1 Pending Jobs',<?php echo $L1userPendingJobs; ?>],['L2 Pending Jobs',<?php echo $L2userPendingJobs; ?>]]], {
                                    gridPadding: {top:0, bottom:38, left:0, right:0},
                                    seriesDefaults:{
                                        renderer:$.jqplot.PieRenderer, 
                                        trendline:{ show:false }, 
                                        rendererOptions: { padding: 8, showDataLabels: true }
                                    },
                                    legend:{
                                        show:true, 
                                        placement: 'outside', 
                                        rendererOptions: {
                                            numberRows: 2
                                        }, 
                                        location:'s',
                                        marginTop: '10px'
                                    }       
                                });
                            });
                        </script> 
                        
                    </div>
                </div>                
            </div>

        </div>
    </div>
</div>
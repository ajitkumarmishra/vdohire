<script type="text/javascript" src="<?php echo base_url(); ?>/js/Chart.js"></script>

<script>
var randomScalingFactor = function() {
        return Math.round(Math.random() * 100);
    };
	var barStackedChartData = {
		labels: [<?php foreach($jobStatusAnalyticsData as $jobStatus){
			$hasData = $jobStatus['totalResponded']+$jobStatus['totalinvitationresponded'];
			if($hasData > 0){
				echo "'".$jobStatus['title']."',";
			}
		} ?>],
		datasets: [{
			label: 'Total Received',
			 backgroundColor: "rgb(39,172,191)",
			data: [<?php foreach($jobStatusAnalyticsData as $jobStatus){
				$hasData = $jobStatus['totalResponded']+$jobStatus['totalinvitationresponded'];
				if($hasData > 0){
					echo "".$jobStatus['totalResponded'].",";
				}
		} ?>],
		}, {
			label: 'Total Invitation Responded',
			backgroundColor: "rgb(92,195,106)",
			data: [<?php foreach($jobStatusAnalyticsData as $jobStatus){
				$hasData = $jobStatus['totalResponded']+$jobStatus['totalinvitationresponded'];
				if($hasData > 0){
					echo "".$jobStatus['totalinvitationresponded'].",";
				}
		} ?>],
		}]

	};
	
	$(function () {
		jobStatus();
    });
	
	function jobStatus(){
		 var ctx = document.getElementById("jobStatusGraphView").getContext("2d");
            window.myBar = new Chart(ctx, {
                type: 'bar',
                data: barStackedChartData,
                options: {
                    title:{
                        display:false,
                        text:"Evaluation Performance",
                        position:"top",
						fontColor: "Black"
						
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false
                    },
                    responsive: true,
                    scales: {
                        xAxes: [{
                            stacked: true,
							gridLines: {
									display: false,
									color: "#000"
								},
								ticks: {
									fontColor: "#000"
								}
                        }],
                        yAxes: [{
                            stacked: true,
							gridLines: {
									display: true,
									color: "#000"
								},
								ticks: {
									beginAtZero:true,
									stepSize: 10
								}
                        }]
                    },
					legend: {
						display: false,
						position: 'top',
						labels: {
							position: 'right'
						}
					}
                }
			});
		
	}
	
	
</script>
<div class="row" style="margin: 13px 13px;">
<div id="display" style="width:700px;height:365px;overflow:scroll;">
	<div id="JobStatusTabularView"  style="display:none;"></div>
	<canvas  data-toggle="modal" data-target="#jobStatusPopup"  class="popupJobStatus"  id="jobStatusGraphView" style="display:block;"></canvas>
</div>
</div>
<?php
die;
?>
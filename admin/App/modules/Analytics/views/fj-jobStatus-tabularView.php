
<?php
if($block=="jobStatus"){
?>
<table class="table sortable" id="jobStatusTableReporting" >
                    <thead class="tbl_head">
						<tr>
							<th>S.No</th>
							<th>Job Code</th>
							<th>Title</th>
							<th>Created At</th>
							<th>Fullname</th>
							<th>Status</th>
							<th>Posted</th>
							<th>Invited</th>
							<th>Total Responded</th>
							<th>Total Invitation Responded</th>
						</tr>
					</thead>
<?php
$i =0;
foreach($jobStatusAnalyticsData as $jobStatus){
	$i++;
	?>
	<tr>
		<td><?php echo $i ?></td>
		<td><?php echo $jobStatus['fjCode'] ?></td>
		<td><?php echo $jobStatus['title'] ?></td>
		<td><?php echo $jobStatus['createdAt'] ?></td>
		<td><?php echo $jobStatus['fullname'] ?></td>
		<td><?php echo $jobStatus['status'] ?></td>
		<td><?php echo $jobStatus['posted'] ?></td>
		<td><?php echo $jobStatus['invited'] ?></td>
		<td><?php echo $jobStatus['totalResponded'] ?></td>
		<td><?php echo $jobStatus['totalinvitationresponded'] ?></td>
		
	</tr>
<?php }


?>
</table>
<?php
	die;
	}else if($block == 'PendingApplication'){ ?>
<table class="table sortable" id="jobStatusTableReporting" >
                    <thead class="tbl_head">
						<tr>
							<th>S.No</th>
							<th>Job Code</th>
							<th>Title</th>
							<th>Company</th>
							<th>Total Applications</th>
							<th>Total Pending Applications</th>
							<th>Total Applications Received at L1</th>
							<th>Total Pending Applications at L1</th>
							<th>Total Applications Received at L2</th>
							<th>Total Pending Applications at L2</th>
						</tr>
					</thead>
<?php
$i =0;
foreach($jobPendingApplicationData as $pendingApplications){
	$i++;
	?>
	<tr>
		<td><?php echo $i ?></td>
		<td><?php echo $pendingApplications['fjCode'] ?></td>
		<td><?php echo $pendingApplications['title'] ?></td>
		<td><?php echo $pendingApplications['company'] ?></td>
		<td><?php echo $pendingApplications['totalApplicationReceivedAtMain'] ?></td>
		<td><?php echo $pendingApplications['totalPendingAtMain'] ?></td>
		<td><?php echo $pendingApplications['totalApplicationReceivedAtL1'] ?></td>
		<td><?php echo $pendingApplications['totalPendingApplicationAtL1'] ?></td>
		<td><?php echo $pendingApplications['totalApplicationReceivedAtL2'] ?></td>
		<td><?php echo $pendingApplications['totalPendingApplicationAtL2'] ?></td>
		
	</tr>
<?php }

?>
</table>

<?php 
	die;
	} ?>
<?php //echo "<pre>";print_r($contentMyQuestions); die();   ?>
<div id="page-content-wrapper">

    <div class="container-fluid whitebg">
        <div class="row">
            <div class="col-md-9"><h2><img src="<?php echo base_url(); ?>/theme/firstJob/image/arrow_head.png" class="prev-page">Download CSV Reports</h2></div>

            <div class="clearfix"></div>

            <div class="col-md-12 rsp_tbl">
                <div class="tab-content">
                    <div id="home" class="tab-pane fade active in">                           
                        <?php                
                        $userData   = $this->session->userdata['logged_in'];
                        $userRole   = $userData->role;
                        if($userRole=='1') { ?>
                        <h3>Users</h3>
                        <ul class="nav ">
<li class=""><a href="<?php echo base_url(); ?>analytics/appUserCSV">List of All App Users</a></li>
<li class=""><a href="<?php echo base_url(); ?>analytics/corporateUserCSV">List of All Corporate Users</a></li>
<li class=""><a href="<?php echo base_url(); ?>analytics/corporateSubUserCSV">List of All Corporate & Their Sub Users</a></li>
                        </ul>    
                        
                        <h3>Jobs</h3>
                        <ul class="nav ">
<li class=""><a href="<?php echo base_url(); ?>analytics/jobsCSV">Total Number of Jobs Posted</a></li>
<li class=""><a href="<?php echo base_url(); ?>analytics/jobsOnFjCSV">Total Number of Jobs Posted On FJ</a></li>
<li class=""><a href="<?php echo base_url(); ?>analytics/videoJdCSV">Total Number of Video JDs Used By Client</a></li>
                        </ul>

                        <h3>User Job Applications</h3>
                        <ul class="nav ">                            
<li class=""><a href="<?php echo base_url(); ?>analytics/jobAppliedCSV">Total Number of Candidates Applied For a Job Post</a></li>
<li class=""><a href="<?php echo base_url(); ?>analytics/pendingCandidateCSV">List of All Pending Candidates By Corporate</a></li>
<li class=""><a href="<?php echo base_url(); ?>analytics/pendingCandidateAtL1CSV">Number of Applications Pending at L1</a></li>
<li class=""><a href="<?php echo base_url(); ?>analytics/pendingCandidateAtL2CSV">Number of Applications Pending at L2</a></li>
<li class=""><a href="<?php echo base_url(); ?>analytics/shorlistedCandidateCSV">List of All Shortlisted Candidates By Corporate</a></li>
<li class=""><a href="<?php echo base_url(); ?>analytics/shorlistedCandidateAtL1CSV">Number of Applications Shortlisted at L1</a></li>
<li class=""><a href="<?php echo base_url(); ?>analytics/shorlistedCandidateAtL2CSV">Number of Applications Shortlisted at L2</a></li>
<li class=""><a href="<?php echo base_url(); ?>analytics/rejectedCandidateCSV">List of All Rejected Candidates By Corporate</a></li>
<li class=""><a href="<?php echo base_url(); ?>analytics/rejectedCandidateAtL1CSV">Number of Applications Rejected at L1</a></li>
<li class=""><a href="<?php echo base_url(); ?>analytics/rejectedCandidateAtL2CSV">Number of Applications Rejected at L2</a></li>
                        </ul>

                        <h3>Interview and Assessments</h3>
                        <ul class="nav ">
<li class=""><a href="<?php echo base_url(); ?>analytics/interviewSetsCSV">Total Number of Interview Sets created</a></li>
<li class=""><a href="<?php echo base_url(); ?>analytics/interviewVideoSetsCSV">Total Number of Video Interview Sets created</a></li>
<li class=""><a href="<?php echo base_url(); ?>analytics/interviewAudioSetsCSV">Total Number of Audio Interview Sets created</a></li>
<li class=""><a href="<?php echo base_url(); ?>analytics/interviewTextSetsCSV">Total Number of Text Interview Sets created</a></li>
<li class=""><a href="<?php echo base_url(); ?>analytics/interviewQusFromFjCSV">Number of Interview Questions used from FJ Database</a></li>
<li class=""><a href="<?php echo base_url(); ?>analytics/interviewQusNumberCSV">Number of Interview Questions Per Interview Set</a></li>
<li class=""><a href="<?php echo base_url(); ?>analytics/assessmentSetsCSV">Total Number of Assessment Sets created</a></li>
<li class=""><a href="<?php echo base_url(); ?>analytics/assessmentQusFromFjCSV">Number of Assessment Questions used from FJ Database</a></li>
<li class=""><a href="<?php echo base_url(); ?>analytics/assessmentQusNumberCSV">Number of Assessment Questions Per Assessment Set</a></li>
                        </ul>
                        

                        <h3>Invites</h3>
                        <ul class="nav ">
<li class=""><a href="<?php echo base_url(); ?>analytics/invitedCandidateCSV">List of All Invited Candidates By Corporate</a></li>
<li class=""><a href="<?php echo base_url(); ?>analytics/invitedFjCandidateCSV">Number of Candidates Invited Through FJ Database</a></li>
                        </ul>                        
                        <?php
                        }
                        if($userRole=='2' || $userRole=='1') { ?>
                        <h3>Reports</h3>
                        <ul class="nav ">
<li class=""><a href="<?php echo base_url(); ?>analytics/jobApplicationReport">Job Application Report</a></li>
<li class=""><a href="<?php echo base_url(); ?>analytics/corporateSubUserReport">Corporate Sub User Report</a></li>
                        </ul>
                        <?php
                        } ?>
                    </div>
                </div>                
            </div>

        </div>
    </div>
</div>
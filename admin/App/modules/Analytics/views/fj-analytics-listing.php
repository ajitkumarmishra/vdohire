<style>
.percentAnalytics {
    color: #fff;
    display: inline-block;
    font-size: 20px;
    left: 30px;
    right: 30px;
    top: 20px;
    bottom: 10px;
    line-height: 26px;
    position: absolute;    
    z-index: 2;
}

.percentAnalytics30 {
    color: #fff;
    display: inline-block;
    font-size: 20px;
    top: 20px;
    bottom: 50px;
    left: 30px;
    right: 30px;
    line-height: 26px;
    position: absolute;    
    z-index: 2;
    height:90%;
}

.percentAnalytics, .percentAnalytics30, a, a:hover, a:active, a:focus {
    /*color: #FFFFFF;*/
    color: #FFFFFF;
    outline: none;
    border: 0px;
    outline: none;
}

.anchorTags {
    /*background-color:rgba(6, 6, 6, 0.43);*/
    background-color:rgba(255, 255, 255, 0.22);
    border-radius: 10px 10px;
    padding:2%;
    height:80%;
    -webkit-box-shadow: 5px 5px 5px 0px rgba(54, 44, 44, 0.8);
    -moz-box-shadow: 5px 5px 5px 0px rgba(54, 44, 44, 0.8);
    box-shadow: 5px 5px 5px 0px rgba(54, 44, 44, 0.8);
    color: #fffafa;
}

.purple, .green, .gray {
    background-image: url(<?php echo base_url(); ?>theme/firstJob/image/barchart.ico);
    background-repeat: no-repeat;
    background-position:bottom right;
}

.blue {
    background: #2c549a;
    padding-top: 10px;
    padding-bottom: 10px;
    height: 165px;
    margin-bottom: 20px;
    background-image: url(<?php echo base_url(); ?>theme/firstJob/image/chart-icon.png);
    background-repeat: no-repeat;
    background-position:bottom right;
}

.green {
    background-image: url(<?php echo base_url(); ?>theme/firstJob/image/piechart.png);
    background-repeat: no-repeat;
    background-position:bottom right;
}


.gray {
    background-image: url(<?php echo base_url(); ?>theme/firstJob/image/line-stacked-icon.png);
    background-repeat: no-repeat;
    background-position:bottom right;
}


.red {
    background: #b75028;
    padding-top: 10px;
    padding-bottom: 10px;
    height: 165px;
    margin-bottom: 20px;
    background-image: url(<?php echo base_url(); ?>theme/firstJob/image/line-icon.png);
    background-repeat: no-repeat;
    background-position:bottom right;
}

.orange {
    background: #db8b00;
    padding-top: 10px;
    padding-bottom: 10px;
    height: 165px;
    margin-bottom: 20px;
    background-image: url(<?php echo base_url(); ?>theme/firstJob/image/line-chart-icon.png);
    background-repeat: no-repeat;
    background-position:bottom right;
}

.cyan {
    background: #3da7b9;
    padding-top: 10px;
    padding-bottom: 10px;
    height: 165px;
    margin-bottom: 20px;
    background-image: url(<?php echo base_url(); ?>theme/firstJob/image/barchart.ico);
    background-repeat: no-repeat;
    background-position:bottom right;
}

.lightGreen {
    background: #83ba18;
    padding-top: 10px;
    padding-bottom: 10px;
    height: 165px;
    margin-bottom: 20px;
    background-image: url(<?php echo base_url(); ?>theme/firstJob/image/pie-chart-icon.png);
    background-repeat: no-repeat;
    background-position:bottom right;
}

.magenta {
    background: #9a2c53;
    padding-top: 10px;
    padding-bottom: 10px;
    height: 165px;
    margin-bottom: 20px;
    background-image: url(<?php echo base_url(); ?>theme/firstJob/image/Data-Combo-Chart-icon.png);
    background-repeat: no-repeat;
    background-position:bottom right;
}

.col-md-30 {
    width: 25%;
}
.col-md-30 {
    min-height: 1px;
    padding-left: 15px;
    padding-right: 15px;
    position: relative;
}
</style>
<div id="page-content-wrapper">

    <div class="container-fluid whitebg">
        <div class="row">
            <div class="col-md-9"><h2><img src="<?php echo base_url(); ?>/theme/firstJob/image/arrow_head.png" class="prev-page">Firstjob Reports/ Analytics</h2></div>

            <div class="clearfix"></div>

            <center>
                <?php                
                $userData   = $this->session->userdata['logged_in'];
                $userRole   = $userData->role;
                if($userRole=='1' || $userRole=='2') { ?>
                <div class="col-md-3">
                    <div class="purple">
                        <span class="percentAnalytics">
                            <a href="<?php echo base_url(); ?>analytics/totalJobs">
                                <p class="anchorTags">Jobs Status</p>
                            </a>
                        </span>
                        <div class="clearfix"></div>
                    </div>
                </div>
                
                <div class="col-md-3">
                    <div class="gray">
                        <span class="percentAnalytics">
                            <a href="<?php echo base_url(); ?>analytics/applicationViewedShortlistRatio">
                                <p class="anchorTags">Applicant Funnel</p>
                            </a>
                        </span>
                        <div class="clearfix"></div>
                    </div>
                </div>
                
                <div class="col-md-3">
                    <div class="blue">
                        <span class="percentAnalytics">
                            <a href="<?php echo base_url(); ?>analytics/applicationSubUserViewed">
                                <p class="anchorTags">Sub User Performance</p>
                            </a>
                        </span>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="green">
                        <span class="percentAnalytics">
                            <a href="<?php echo base_url(); ?>analytics/pendingApplication">
                                <p class="anchorTags">Pending Application Status</p>
                            </a>
                        </span>
                        <div class="clearfix"></div>
                    </div>
                </div>
                
                <div class="col-md-3">
                    <div class="orange">
                        <span class="percentAnalytics">
                            <a href="<?php echo base_url(); ?>analytics/demographicMaleFemale">
                                <p class="anchorTags">Applicants Demographics</p>
                            </a>
                        </span>
                        <div class="clearfix"></div>
                    </div>
                </div>
                
                <div class="col-md-3">
                    <div class="red">
                        <span class="percentAnalytics">
                            <a href="<?php echo base_url(); ?>analytics/jobOpenRate">
                                <p class="anchorTags">Job Open Rate</p>
                            </a>
                        </span>
                        <div class="clearfix"></div>
                    </div>
                </div>
                
                <div class="col-md-3">
                    <div class="cyan">
                        <span class="percentAnalytics">
                            <a href="<?php echo base_url(); ?>analytics/costToHire">
                                <p class="anchorTags">Shortlist Ratio</p>
                            </a>
                        </span>
                        <div class="clearfix"></div>
                    </div>
                </div>
                
                
<!--                <div class="col-md-3">
                    <div class="lightGreen">
                        <span class="percentAnalytics">
                            <a href="<?php echo base_url(); ?>analytics/timeToHire">
                                <p class="anchorTags">Time Per Shortlisted</p>
                            </a>
                        </span>
                        <div class="clearfix"></div>
                    </div>
                </div>-->
                
                
                <div class="col-md-3">
                    <div class="lightGreen">
                        <span class="percentAnalytics">
                            <a href="<?php echo base_url(); ?>analytics/timePerShortlist">
                                <p class="anchorTags">Time Per Shortlisted</p>
                            </a>
                        </span>
                        <div class="clearfix"></div>
                    </div>
                </div>

                
                
                
<!--                <div class="col-md-3">
                    <div class="orange">
                        <span class="percentAnalytics">
                            <a href="<?php echo base_url(); ?>analytics/applicationReceivedShortlistRatio">
                                <p class="anchorTags">Application Received<br>/ Shortlisted Ratio</p>
                            </a>
                        </span>
                        <div class="clearfix"></div>
                    </div>
                </div>-->

<!--                <div class="col-md-3">
                    <div class="red">
                        <span class="percentAnalytics">
                            <a href="<?php echo base_url(); ?>analytics/applicationSubUserShortlist">
                                <p class="anchorTags">Sub User Name Vs Number of Applications Viewed<br>(Shortlisted + Rejected)</p>
                            </a>
                        </span>
                        <div class="clearfix"></div>
                    </div>
                </div>-->

            </center>
        </div>
        
        <center>
            <div class="row">
                <div class="col-md-30">
                    <div class="magenta">
                        <span class="percentAnalytics30">
                            <a href="<?php echo base_url(); ?>analytics/reports">
                                <p class="anchorTags">Download CSV Reports</p>
                            </a>
                        </span>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </center>
        <?php } ?>
    </div>


</div>
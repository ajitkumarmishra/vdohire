<?php //echo "<pre>";print_r($contentMyQuestions); die();   ?>
<div id="page-content-wrapper">

    <div class="container-fluid whitebg">
        <div class="row">
            <div class="col-md-9"><h2><img src="<?php echo base_url(); ?>/theme/firstJob/image/arrow_head.png" class="prev-page">Sub User Performance</h2></div>

            <div class="clearfix"></div>

            <div class="col-md-12 rsp_tbl">
                <div class="tab-content">
                    <div id="home" class="tab-pane fade active in">
                        <form class="form-vertical form_save" action="" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="inputEmail" class="control-label col-xs-1 required-field">From</label>
                                <div class="col-xs-2">
                                    <input type="text" class="form-control" placeholder="From Date" id="datepicker-13" value="<?php
                                        if (isset($_POST['fromDate'])): echo $_POST['fromDate'];
                                        endif;
                                        ?>" name="fromDate">
                                </div>
                            </div> 
                            <div class="form-group">
                                <label for="inputEmail" class="control-label col-xs-1 required-field">To</label>
                                <div class="col-xs-2">
                                    <input type="text" class="form-control" placeholder="To Date" id="datepicker-14" value="<?php
                                        if (isset($_POST['toDate'])): echo $_POST['toDate'];
                                        endif;
                                        ?>" name="toDate">
                                </div>
                            </div>                        
                            <div class="col-xs-offset-1 col-xs-4 no_padding">
                                <input type="hidden" name="accessToken" value="<?php echo $token; ?>"/>
                                <button type="submit" class="Save_frm">Search</button>
                                <button type="reset" class="Save_frm">Reset</button>
                            </div>
                        </form>
                        
                        <br><br><br>
                        <?php
                        $newArrayData = array();
                        foreach ($jopPanelist as $panelUser) {
                            $rawData['panelistName']                    = $panelUser['fullname'];                            
                            ${'pendingJobs'.$panelUser['userId']}       = 0;
                            ${'shortlistedJobs'.$panelUser['userId']}   = 0;
                            ${'rejectedJobs'.$panelUser['userId']}      = 0;
                            foreach ($applicationReceived as $jobUser) {
                                $panelistJobs   = explode(',',$panelUser['jobIds']);
                                if(in_array($jobUser['jobId'], $panelistJobs)) {
                                    if($jobUser['status']=='1'){
                                        ${'pendingJobs'.$panelUser['userId']}++;
                                    }
                                    if($jobUser['status']=='2' && $jobUser['shorlistedByL1User']==$panelUser['userId']){
                                        ${'shortlistedJobs'.$panelUser['userId']}++;
                                    }
                                    if($jobUser['status']=='3' && $jobUser['shorlistedByL1User']==$panelUser['userId']){
                                        ${'rejectedJobs'.$panelUser['userId']}++;
                                    }
                                    if($jobUser['status']=='4' && $jobUser['shorlistedByL2User']==$panelUser['userId']){
                                        ${'shortlistedJobs'.$panelUser['userId']}++;
                                    }
                                    if($jobUser['status']=='5' && $jobUser['shorlistedByL2User']==$panelUser['userId']){
                                        ${'rejectedJobs'.$panelUser['userId']}++;
                                    }
                                }                                
                            }                            
                            $rawData['pendingJobs']     = ${'pendingJobs'.$panelUser['userId']};
                            $rawData['shortlistedJobs'] = ${'shortlistedJobs'.$panelUser['userId']};
                            $rawData['rejectedJobs']    = ${'rejectedJobs'.$panelUser['userId']};                            
                            $newArrayData[] = $rawData;
                        }
                        

                        foreach ($newArrayData as $dataForchart) {
                            $nameArray[]        = $dataForchart['panelistName'];
                            $pendingJobs[]      = $dataForchart['pendingJobs'];
                            $shortlistedJobs[]  = $dataForchart['shortlistedJobs'];
                            $rejectedJobs[]     = $dataForchart['rejectedJobs'];
                        }
                        ?>
                        
                        
                        <div id="chart1" style="margin-top:20px; margin-left:20px; width:100%; height:500px; float: left"></div>
                        
                        <script class="code" type="text/javascript">
                            $(document).ready(function () {
                                var s1      = <?php echo "[".implode(',',$pendingJobs)."]"; ?>;
                                var s2      = <?php echo "[".implode(',',$shortlistedJobs)."]"; ?>;
                                var s3      = <?php echo "[".implode(',',$rejectedJobs)."]"; ?>;
                                var ticks   = <?php echo "['".implode("','",$nameArray)."']"; ?>;
                                plot2       = 	$.jqplot('chart1', [s1, s2, s3], {
                                                    seriesDefaults: {
                                                                        renderer: $.jqplot.BarRenderer,
                                                                        pointLabels: {show: true}
                                                                    },
                                                    axes:           {
                                                                        xaxis:  {
                                                                                    renderer: $.jqplot.CategoryAxisRenderer,
                                                                                    ticks: ticks
                                                                                }
                                                                    },
                                                    legend:{
                                                        show:true, 
                                                        placement: 'inside', 
                                                        location:'ne'
                                                    } ,
                                                    series:[{label:'Pending Applications'}, {label:'Shortlisted Applications'}, {label:'Rejected Applications'}], 
                                                });
                                $('#chart2').bind('jqplotDataHighlight',
                                    function (ev, seriesIndex, pointIndex, data) {
                                        $('#info2').html('series: ' + seriesIndex + ', point: ' + pointIndex + ', data: ' + data);
                                });

                                $('#chart2').bind('jqplotDataUnhighlight',
                                    function (ev) {
                                        $('#info2').html('Nothing');
                                });
                            });
                        </script> 
                        
                    </div>
                </div>                
            </div>

        </div>
    </div>
</div>
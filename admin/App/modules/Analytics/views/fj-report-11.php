<?php //echo "<pre>";print_r($contentMyQuestions); die();   ?>
<div id="page-content-wrapper">

    <div class="container-fluid whitebg">
        <div class="row">
            <div class="col-md-9"><h2><img src="<?php echo base_url(); ?>/theme/firstJob/image/arrow_head.png" class="prev-page">Time of Hire</h2></div>

            <div class="clearfix"></div>

            <div class="col-md-12 rsp_tbl">
                <div class="tab-content">
                    <div id="home" class="tab-pane fade active in">
                        
                        <br><br><br>
                        
                        <?php                        
                        $L1shorlistedDateArray      = array();
                        $L1datewiseRecordArray      = array();
                        $L1newDate                  = '';
                        $L1sameDateRecordCount      = 0;
                        $L1shorlistedTimeCount      = 0;
                        $L1z        = 0;
                        $L1counter    = 1;
                        foreach($timeToHireL1Shortlisted as $L1jobApplication) {
                            if($L1z==0){
                                $L1appliedDate    = strtotime($L1jobApplication['createdAt']);
                                $L1shorlistedDate = strtotime($L1jobApplication['shorlistedByL1UserDate']);
                                $L1dateDifference = $L1shorlistedDate - $L1appliedDate;
                                $L1dateDifference = floor($L1dateDifference / (60 * 60 * 24));
                                //echo $L1dateDifference; exit;
                                $L1shorlistedDateArray[]  = $L1jobApplication['shorlistedByL1UserDate'];
                                $L1sameDateRecordCount++;
                                $L1shorlistedTimeCount = $L1shorlistedTimeCount + $L1dateDifference;
                                if(count($timeToHireL1Shortlisted)==1) {
                                    $L1datewiseRecordArray[]  = ($L1sameDateRecordCount!=0?$L1shorlistedTimeCount/$L1sameDateRecordCount:0);
                                }
                                $L1newDate  = $L1jobApplication['shorlistedByL1UserDate'];
                            }
                            else if($L1newDate==$L1jobApplication['shorlistedByL1UserDate']) {
                                $L1appliedDate          = strtotime($L1jobApplication['createdAt']);
                                $L1shorlistedDate       = strtotime($L1jobApplication['shorlistedByL1UserDate']);
                                $L1dateDifference       = $L1shorlistedDate - $L1appliedDate;
                                $L1dateDifference       = floor($L1dateDifference / (60 * 60 * 24));
                                //echo $L1dateDifference; exit;
                                $L1sameDateRecordCount++;
                                $L1shorlistedTimeCount  = $L1shorlistedTimeCount + $L1dateDifference;
                                $L1newDate              = $L1jobApplication['shorlistedByL1UserDate'];
                                if ($L1counter == count($timeToHireL1Shortlisted)){
                                    $L1datewiseRecordArray[]  = ($L1sameDateRecordCount!=0?$L1shorlistedTimeCount/$L1sameDateRecordCount:0);
                                }
                            }
                            else {
                                $L1appliedDate              = strtotime($L1jobApplication['createdAt']);
                                $L1shorlistedDate           = strtotime($L1jobApplication['shorlistedByL1UserDate']);
                                $L1dateDifference           = $L1shorlistedDate - $L1appliedDate;
                                $L1dateDifference           = floor($L1dateDifference / (60 * 60 * 24));
                                //echo $L1dateDifference; exit;
                                $L1shorlistedDateArray[]    = $L1jobApplication['shorlistedByL1UserDate'];
                                $L1datewiseRecordArray[]    = ($L1sameDateRecordCount!=0?$L1shorlistedTimeCount/$L1sameDateRecordCount:0);
                                if ($L1counter == count($timeToHireL1Shortlisted)){
                                    $L1datewiseRecordArray[]  = ($L1sameDateRecordCount!=0?$L1shorlistedTimeCount/$L1sameDateRecordCount:0);
                                }
                                $L1sameDateRecordCount      = 0;                                
                                $L1shorlistedTimeCount      = 0;
                                $L1sameDateRecordCount++;    
                                $L1shorlistedTimeCount      = $L1shorlistedTimeCount + $L1dateDifference;
                                $L1newDate                  = $L1jobApplication['shorlistedByL1UserDate'];
                            }
                            $L1z++;
                            $L1counter++;
                        }
                        
                        
                        $L2shorlistedDateArray      = array();
                        $L2datewiseRecordArray      = array();
                        $L2newDate                  = '';
                        $L2sameDateRecordCount      = 0;
                        $L2shorlistedTimeCount      = 0;
                        $L2z        = 0;
                        $L2counter    = 1;
                        foreach($timeToHireL2Shortlisted as $L2jobApplication) {
                            if($L2z==0){
                                $L2appliedDate    = strtotime($L2jobApplication['createdAt']);
                                $L2shorlistedDate = strtotime($L2jobApplication['shorlistedByL2UserDate']);
                                $L2dateDifference = $L2shorlistedDate - $L2appliedDate;
                                $L2dateDifference = floor($L2dateDifference / (60 * 60 * 24));
                                //echo $L2dateDifference; exit;
                                $L2shorlistedDateArray[]  = $L2jobApplication['shorlistedByL2UserDate'];
                                $L2sameDateRecordCount++;
                                $L2shorlistedTimeCount = $L2shorlistedTimeCount + $L2dateDifference;
                                if(count($timeToHireL2Shortlisted)==1) {
                                    $L2datewiseRecordArray[]  = ($L2sameDateRecordCount!=0?$L2shorlistedTimeCount/$L2sameDateRecordCount:0);
                                }
                                $L2newDate  = $L2jobApplication['shorlistedByL2UserDate'];
                            }
                            else if($L2newDate==$L2jobApplication['shorlistedByL2UserDate']) {
                                $L2appliedDate          = strtotime($L2jobApplication['createdAt']);
                                $L2shorlistedDate       = strtotime($L2jobApplication['shorlistedByL2UserDate']);
                                $L2dateDifference       = $L2shorlistedDate - $L2appliedDate;
                                $L2dateDifference       = floor($L2dateDifference / (60 * 60 * 24));
                                //echo $L2dateDifference; exit;
                                $L2sameDateRecordCount++;
                                $L2shorlistedTimeCount  = $L2shorlistedTimeCount + $L2dateDifference;
                                $L2newDate              = $L2jobApplication['shorlistedByL2UserDate'];
                                if ($L2counter == count($timeToHireL2Shortlisted)){
                                    $L2datewiseRecordArray[]  = ($L2sameDateRecordCount!=0?$L2shorlistedTimeCount/$L2sameDateRecordCount:0);
                                }
                            }
                            else {
                                $L2appliedDate              = strtotime($L2jobApplication['createdAt']);
                                $L2shorlistedDate           = strtotime($L2jobApplication['shorlistedByL2UserDate']);
                                $L2dateDifference           = $L2shorlistedDate - $L2appliedDate;
                                $L2dateDifference           = floor($L2dateDifference / (60 * 60 * 24));
                                //echo $L2dateDifference; exit;
                                $L2shorlistedDateArray[]    = $L2jobApplication['shorlistedByL2UserDate'];
                                $L2datewiseRecordArray[]    = ($L2sameDateRecordCount!=0?$L2shorlistedTimeCount/$L2sameDateRecordCount:0);
                                if ($L2counter == count($timeToHireL2Shortlisted)){
                                    $L2datewiseRecordArray[]  = ($L2sameDateRecordCount!=0?$L2shorlistedTimeCount/$L2sameDateRecordCount:0);
                                }
                                $L2sameDateRecordCount      = 0;                                
                                $L2shorlistedTimeCount      = 0;
                                $L2sameDateRecordCount++;    
                                $L2shorlistedTimeCount      = $L2shorlistedTimeCount + $L2dateDifference;
                                $L2newDate                  = $L2jobApplication['shorlistedByL2UserDate'];
                            }
                            $L2z++;
                            $L2counter++;
                        }
                        
                        
                        
                        
                        $shorlistedDates    = array();
                        $shorlistedValues   = array();
                        foreach($L1shorlistedDateArray as $key=>$dates) {
                            $shorlistedDates[]  = $dates;
                            $shorlistedValues[] = $L1datewiseRecordArray[$key];
                        }
                        foreach($L2shorlistedDateArray as $key=>$dates) {
                            $shorlistedDates[]  = $dates;
                            $shorlistedValues[] = $L2datewiseRecordArray[$key];
                        }
                        
                        //echo "<pre>"; print_r($shorlistedDates); print_r($shorlistedValues); exit;
                        
                        $mapData    = array();
                        foreach($shorlistedDates as $key=>$date) {
                            $mapData[] = "['".$date."', ".$shorlistedValues[$key]."]";                            
                        }
                        //echo "<pre>"; print_r($mapDataL2); exit;
                        $lineChartData  = implode(', ', $mapData);
                        ?>
                        <div id="home" class="tab-pane fade active in">
                            <div class="form-group">
                                <div class="col-xs-10">
                                    <h4></h4>
                                </div>
                            </div>
                        </div>
                        
                        <div id="chart1" style="margin-top:20px; margin-left:40px; width:90%; height:300px; float: left"></div>
                        
                        <script class="code" type="text/javascript">
                            $(document).ready(function(){
                                $.jqplot.config.enablePlugins = true;
                                var line1=[<?php echo $lineChartData; ?>];
                                var plot1 = $.jqplot('chart1', [line1], {
                                                animate:        !$.jqplot.use_excanvas,
                                                seriesColors:['#4BB2C5', '#EAA228'],
                                                title:'Default Date Axis',
                                                    axes:{
                                                        xaxis:{
                                                            renderer:$.jqplot.DateAxisRenderer,
                                                            tickInterval:'1 day'
                                                        }
                                                    },
                                                    series:[{lineWidth:4, markerOptions:{style:'square'}}]
                                                });
                                });
                        </script> 
                    </div>
                </div>                
            </div>

        </div>
    </div>
</div>
<?php //echo "<pre>";print_r($contentMyQuestions); die();   ?>
<?php //print_r($_POST); ?>
<div id="page-content-wrapper">

    <div class="container-fluid whitebg">
        <div class="row">
            <div class="col-md-9"><h2><img src="<?php echo base_url(); ?>/theme/firstJob/image/arrow_head.png" class="prev-page">Shortlist Ratio</h2></div>

            <div class="clearfix"></div>

            <div class="col-md-12 rsp_tbl">
                <div class="tab-content">
                    <div id="home" class="tab-pane fade active in">
                        <form class="form-vertical form_save" action="" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="inputEmail" class="control-label col-xs-1 required-field">From</label>
                                <div class="col-xs-2">
                                    <input type="text" class="form-control" placeholder="From Date" id="datepicker-13" value="<?php
                                        if (isset($_POST['fromDate'])): echo $_POST['fromDate'];
                                        endif;
                                        ?>" name="fromDate">
                                </div>
                            </div> 
                            <div class="form-group">
                                <label for="inputEmail" class="control-label col-xs-1 required-field">To</label>
                                <div class="col-xs-2">
                                    <input type="text" class="form-control" placeholder="To Date" id="datepicker-14" value="<?php
                                        if (isset($_POST['toDate'])): echo $_POST['toDate'];
                                        endif;
                                        ?>" name="toDate">
                                </div>
                            </div> 
                            
                            <div class="col-xs-offset-1 col-xs-5 no_padding">
                                <input type="hidden" name="accessToken" value="<?php echo $token; ?>"/>
                                <button type="submit" class="Save_frm">Search</button>
                                <button type="reset" class="Save_frm">Reset</button>
                            </div>
                        </form>
                        
                        <br><br><br>
                        
                        <?php
                        function getWeekDates($date, $start_date, $end_date) {
                            $week   =  date('W', strtotime($date));
                            $year   =  date('Y', strtotime($date));
                            $from   = date("Y-m-d", strtotime("{$year}-W{$week}+1")); //Returns the date of monday in week
                            if($from < $start_date) $from = $start_date;
                            $to     = date("Y-m-d", strtotime("{$year}-W{$week}-6"));   //Returns the date of sunday in week
                            if($to > $end_date) $to = $end_date;
                            //echo "Start Date-->".$from."End Date -->".$to;//Output : Start Date-->2012-09-03 End Date-->2012-09-09
                            $dateArray['startDate'] = $from;
                            $dateArray['endDate']   = $to;
                            return $dateArray;
                        }

                        $start_date = date('Y-m-d', strtotime('2016-01-01'));
                        $end_date   = date('Y-m-d', strtotime('2016-12-31'));
                        //$end_date1  = date('Y-m-d', strtotime('2016-12-31 + 6 days'));
                        

//                        $i=1;
//                        for($date = $start_date; $date <= $end_date; $date = date('Y-m-d', strtotime($date. ' + 7 days'))) {
//                            $getDates           = getWeekDates($date, $start_date, $end_date);
//                            //echo "Dates - ".$getDates['startDate']."___".$getDates['endDate']."<br>";
//                            $weeklyApplications = $this->analytics_model->applicationReceivedShortlistRatio($getDates['startDate'],$getDates['endDate']);
//                            
//                            $totalJobs              = 0;
//                            $totalShortlistedJobs   = 0;
//                            foreach($weeklyApplications as $jobApplication) {
//                                $sql    = "SELECT DISTINCT(level) FROM fj_jobPanel WHERE jobId='$jobApplication[jobId]'";
//                                $query  = $this->db->query($sql);
//                                $result = $query->result_array();
//                                if(count($result)==1) {
//                                    if($jobApplication['status']=='1')      { $totalJobs++; }
//                                    else if($jobApplication['status']=='3') { $totalJobs++; }
//                                    else if($jobApplication['status']=='2') { $totalJobs++; $totalShortlistedJobs++; }
//                                }
//                                else if(count($result)==2) {
//                                    if($jobApplication['status']=='1')      { $totalJobs++; }
//                                    else if($jobApplication['status']=='2') { $totalJobs++; }
//                                    else if($jobApplication['status']=='3') { $totalJobs++; }
//                                    else if($jobApplication['status']=='5') { $totalJobs++; }
//                                    else if($jobApplication['status']=='4') { $totalJobs++; $totalShortlistedJobs++; }
//                                }
//                            }
//                            //echo $totalJobs."-".$totalShortlistedJobs."<br>";
//                            $cost           = round($totalShortlistedJobs/$totalJobs, 2);                            
//                            $weekCost[$i]   = $cost;    //$weekCount[$i]  = "Week ".$i;
//                            $weekCount[$i]  = $i;
//                            $i++;
//                        } 
                        
                        
                        
                        
                        
                        
                        
                        $list   = array();
                        $month  = ($_POST['month']!=''?$_POST['month']:date('m'));
                        //$month  = date('m');
                        $year   = ($_POST['year']!=''?$_POST['year']:date('Y'));
                        /*
                        for($d=1; $d<=31; $d++) {
                            $time=mktime(12, 0, 0, $month, $d, $year);          
                            if (date('m', $time)==$month)       
                                $list[]=date('Y-m-d', $time);
                        }
                        echo "<pre>"; print_r($list); echo "</pre>";
                        */
                        
                        
                        
                        
                        
                        $i=1;
                        for($d=1; $d<=31; $d++) {
                            $time=mktime(12, 0, 0, $month, $d, $year);          
                            if (date('m', $time)==$month) {
                                $weeklyApplications = $this->analytics_model->applicationReceivedShortlistRatio(date('Y-m-d', $time),date('Y-m-d', $time));

                                $totalJobs              = 0;
                                $totalShortlistedJobs   = 0;
                                foreach($weeklyApplications as $jobApplication) {
                                    $sql    = "SELECT DISTINCT(level) FROM fj_jobPanel WHERE jobId='$jobApplication[jobId]'";
                                    $query  = $this->db->query($sql);
                                    $result = $query->result_array();
                                    if(count($result)==1) {
                                        if($jobApplication['status']=='1')      { $totalJobs++; }
                                        else if($jobApplication['status']=='3') { $totalJobs++; }
                                        else if($jobApplication['status']=='2') { $totalJobs++; $totalShortlistedJobs++; }
                                    }
                                    else if(count($result)==2) {
                                        if($jobApplication['status']=='1')      { $totalJobs++; }
                                        else if($jobApplication['status']=='2') { $totalJobs++; }
                                        else if($jobApplication['status']=='3') { $totalJobs++; }
                                        else if($jobApplication['status']=='5') { $totalJobs++; }
                                        else if($jobApplication['status']=='4') { $totalJobs++; $totalShortlistedJobs++; }
                                    }
                                }
                                //echo $totalJobs."-".$totalShortlistedJobs."<br>";
                                $cost           = round($totalShortlistedJobs/$totalJobs, 2);                            
                                //$weekCost[$i]   = $cost;    //$weekCount[$i]  = "Week ".$i;
                                $weekCost[$i]   = $totalShortlistedJobs;
                                $weekCount[$i]  = $i;   
                                $i++;                             
                            }
                        } 
                        
                        
                        
                        // Overall Shortlist Cost
                        $totalJobs              = 0;
                        $totalShortlistedJobs   = 0;
                        foreach($applicationReceived as $jobApplication) {
                            $sql    = "SELECT Distinct(level) FROM fj_jobPanel WHERE jobId='$jobApplication[jobId]'";
                            $query  = $this->db->query($sql);
                            $result = $query->result_array();
                            //echo count($result);
                            if(count($result)==1) {
                                if($jobApplication['status']=='1')      { $totalJobs++; }
                                else if($jobApplication['status']=='3') { $totalJobs++; }
                                else if($jobApplication['status']=='2') { $totalJobs++; $totalShortlistedJobs++; }
                            }
                            else if(count($result)==2) {
                                if($jobApplication['status']=='1')      { $totalJobs++; }
                                else if($jobApplication['status']=='2') { $totalJobs++; }
                                else if($jobApplication['status']=='3') { $totalJobs++; }
                                else if($jobApplication['status']=='5') { $totalJobs++; }
                                else if($jobApplication['status']=='4') { $totalJobs++; $totalShortlistedJobs++; }
                            }
                        }  
                        //echo $totalJobs."-".$totalShortlistedJobs."<br>";
                        $cost = round($totalShortlistedJobs/$totalJobs, 2);
                        ?>
                        <div id="home" class="tab-pane fade active in">
                            <div class="form-group">
                                <div class="col-xs-10">
<!--                                    <h4>Total Cost To Hire - Rs <?php echo $cost;?></h4>-->
                                </div>
                            </div>
                        </div>
                        
                        <div id="chart1" style="margin-top:20px; margin-left:40px; width:350px; height:300px; float: left"></div>
                        <div id="chart6" style="margin-top:30px; margin-right:20px; width:300px; height:300px; float: right"></div>
                        
                        
                        <form class="form-vertical form_save" action="" method="post" enctype="multipart/form-data" style="margin-left: 5%; width:100%; margin-top: 5%; float: left">
                            <div class="form-group">
                                <div class="col-xs-9">
                                    <select name="month" id="month">
                                        <option value="">---Select Month---</option>
                                        <?php
                                        $monthArray = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
                                        $k=1;
                                        foreach($monthArray as $m) { ?>
                                            <option <?php if (isset($_POST['month']) && $_POST['month']==$k) echo "selected='selected' "; ?> value="<?=$k;?>"><?=$m;?></option>
                                            <?php
                                            $k++;
                                        }
                                        ?>
                                    </select>
                                    <select name="year" id="year">
                                        <option value="">---Select Year---</option>
                                        <?php
                                        for($m=2016; $m<=2050; $m++) { ?>
                                            <option <?php if (isset($_POST['year']) && $_POST['year']==$m) echo "selected='selected' "; ?> value="<?=$m;?>"><?=$m;?></option>
                                            <?php
                                        }
                                        ?>
                                    <select>
                                    <input type="hidden" name="accessToken" value="<?php echo $token; ?>"/>
                                    <button type="submit" class="Save_frm">Search</button>                                    
                                </div>
                            </div>
                        </form>
                        <div id="chart7" style="margin-top:0%;  margin-left:00px; width:100%; height:300px; float: left"></div>
                        
                        <script class="code" type="text/javascript">
                            $(document).ready(function () {
                                $.jqplot.config.enablePlugins = true;
                                var s1      = [<?php echo $totalJobs; ?>, <?php echo $totalShortlistedJobs; ?>];
                                var ticks   = ['Total Application', 'Total Shortlisted'];

                                plot1 = $.jqplot('chart1', [s1], {
                                            // Only animate if we're not using excanvas (not in IE 7 or IE 8)..
                                            animate:        !$.jqplot.use_excanvas,
                                            seriesColors:['#4BB2C5', '#EAA228'],
                                            seriesDefaults: {
                                                                renderer: $.jqplot.BarRenderer,
                                                                rendererOptions: {varyBarColor: true},
                                                                pointLabels: {show: true}
                                                            },
                                            axes:           {
                                                                xaxis:  {
                                                                            renderer: $.jqplot.CategoryAxisRenderer,
                                                                            ticks: ticks
                                                                        }
                                                            },
                                            highlighter:    {show: false}
                                        });

                                $('#chart1').bind('jqplotDataClick',
                                    function (ev, seriesIndex, pointIndex, data) {
                                        $('#info1').html('series: ' + seriesIndex + ', point: ' + pointIndex + ', data: ' + data);
                                    }
                                );
                            });
                            
                            $(document).ready(function () {
                                var plot1 = $.jqplot('chart6', [[['Pending + Rejected',<?php echo $totalJobs-$totalShortlistedJobs; ?>],['Total Shortlisted',<?php echo $totalShortlistedJobs; ?>]]], {
                                    gridPadding: {top:0, bottom:38, left:0, right:0},
                                    seriesColors:['#4BB2C5', '#EAA228'],
                                    seriesDefaults:{
                                        renderer:$.jqplot.PieRenderer, 
                                        trendline:{ show:false }, 
                                        rendererOptions: { padding: 8, showDataLabels: true }
                                    },
                                    legend:{
                                        show:true, 
                                        placement: 'outside', 
                                        rendererOptions: {
                                            numberRows: 3
                                        }, 
                                        location:'s',
                                        marginTop: '10px'
                                    }       
                                });
                            });
                            
                            /*
                            $(document).ready(function(){
                              var line1=[['2016-07-01',4], ['2016-07-02',6], ['2016-07-07',5], ['2016-07-11',9], ['2016-07-31',8]];
                              var plot2 = $.jqplot('chart7', [line1], {
                                    title:'Customized Date Axis', 
                                    axes:   {
                                                xaxis:  {
                                                    renderer:     $.jqplot.DateAxisRenderer, 
                                                    tickOptions:  {formatString:'%d-%m-%y'},
                                                    min:          '2016-07-01', 
                                                    tickInterval: '2 day'
                                                }
                                            },
                                  series:   [{lineWidth:4, markerOptions:{style:'square'}}]
                              });
                            });
                            */
                           
                           
                            $(document).ready(function(){
                              var plot2 = $.jqplot ('chart7', [<?php echo "[".implode(',',$weekCost)."]"; ?>], {
                                  // Give the plot a title.
                                  title: 'Overall Shortlists',
                                  // You can specify options for all axes on the plot at once with
                                  // the axesDefaults object.  Here, we're using a canvas renderer
                                  // to draw the axis label which allows rotated text.
                                  axesDefaults: {
                                    labelRenderer: $.jqplot.CanvasAxisLabelRenderer
                                  },
                                  // Likewise, seriesDefaults specifies default options for all
                                  // series in a plot.  Options specified in seriesDefaults or
                                  // axesDefaults can be overridden by individual series or
                                  // axes options.
                                  // Here we turn on smoothing for the line.
                                  seriesDefaults: {
                                      rendererOptions: {
                                          smooth: true
                                      }
                                  },
                                  // An axes object holds options for all axes.
                                  // Allowable axes are xaxis, x2axis, yaxis, y2axis, y3axis, ...
                                  // Up to 9 y axes are supported.
                                  axes: {
                                    // options for each axis are specified in seperate option objects.
                                    xaxis: {
                                      label: "Days",
                                      // Turn off "padding".  This will allow data point to lie on the
                                      // edges of the grid.  Default padding is 1.2 and will keep all
                                      // points inside the bounds of the grid.
                                      pad: 0,
                                      ticks:<?php echo "['".implode("','",$weekCount)."']"; ?>
                                    },
                                    yaxis: {
	                                  	label: "Number of Shortlists",
			                            rendererOptions: { forceTickAt0: true, forceTickAt5: true },
			                            min : 0,
			                            tickInterval : 1
                                    }
                                  }
                                });
                            });

//                            $(document).ready(function () {
//                                $.jqplot.config.enablePlugins = true;
//                                var s1      = <?php echo "[".implode(',',$weekCost)."]"; ?>;
//                                var ticks   = <?php echo "['".implode("','",$weekCount)."']"; ?>;
//                                
//                                plot1 = $.jqplot('chart44', [s1], {
//                                            // Only animate if we're not using excanvas (not in IE 7 or IE 8)..
//                                            animate:        !$.jqplot.use_excanvas,
//                                            seriesDefaults: {
//                                                                renderer: $.jqplot.BarRenderer,
//                                                                rendererOptions: {
//                                                                    fillToZero: true,
//                                                                    barPadding: 0,
//                                                                    //barMargin: 2,
//                                                                    groups: 1,
//                                                                    varyBarColor: true
//                                                                },
//                                                                pointLabels: {show: true}
//                                                            },
//                                            series:         [
//                                                                {
//                                                                    pointLabels:    {
//                                                                                        show: true,
//                                                                                        location:'s',
//                                                                                        ypadding : 5,
//                                                                                        edgeTolerance : -1
//                                                                                    }
//                                                                }
//                                                            ],
//                                            axes:           {
//                                                                xaxis:  {
//                                                                            label : 'Days',
//                                                                            renderer: $.jqplot.CategoryAxisRenderer,
//                                                                            ticks: ticks
//                                                                        },
//                                                                yaxis : {
//                                                                            label : 'Cost',
//                                                                            renderer : $.jqplot.LogAxisRenderer                     
//                                                                        }
//                                                            },
//                                            highlighter:    {show: false}
//                                        });
//
//                                $('#chart1').bind('jqplotDataClick',
//                                    function (ev, seriesIndex, pointIndex, data) {
//                                        $('#info1').html('series: ' + seriesIndex + ', point: ' + pointIndex + ', data: ' + data);
//                                    }
//                                );
//                            });
                        </script> 
                        
                        <?php
                        //$end_date   = "2016-07-23";
                        //$end_date   =  date("Y-m-t", strtotime($a_date)); 
                        ?>
                        
                    </div>
                </div>                
            </div>

        </div>
    </div>
</div>
<?php //echo "<pre>";print_r($totalJobApplication); die();   ?>
<div id="page-content-wrapper">

    <div class="container-fluid whitebg">
        <div class="row">
            <div class="col-md-9"><h2><img src="<?php echo base_url(); ?>/theme/firstJob/image/arrow_head.png" class="prev-page">Jobs Status</h2></div>

            <div class="clearfix"></div>

            <div class="col-md-12 rsp_tbl">
                <div class="tab-content">
                    <div id="home" class="tab-pane fade active in">
                        <form class="form-vertical form_save" action="" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="inputEmail" class="control-label col-xs-1 required-field">From</label>
                                <div class="col-xs-2">
                                    <input type="text" class="form-control" placeholder="From Date" id="datepicker-13" value="<?php
                                        if (isset($_POST['fromDate'])): echo $_POST['fromDate'];
                                        endif;
                                        ?>" name="fromDate">
                                </div>
                            </div> 
                            <div class="form-group">
                                <label for="inputEmail" class="control-label col-xs-1 required-field">To</label>
                                <div class="col-xs-2">
                                    <input type="text" class="form-control" placeholder="To Date" id="datepicker-14" value="<?php
                                        if (isset($_POST['toDate'])): echo $_POST['toDate'];
                                        endif;
                                        ?>" name="toDate">
                                </div>
                            </div>                        
                            <div class="col-xs-offset-1 col-xs-4 no_padding">
                                <input type="hidden" name="accessToken" value="<?php echo $token; ?>"/>
                                <button type="submit" class="Save_frm">Search</button>
                                <button type="reset" class="Save_frm">Reset</button>
                            </div>
                        </form>
                        
                        <br><br><br>
                        
                        
                        <?php                        
                        $i                      = 0;
                        $jobCode                = '';                        
                        $jobCodeArray           = array();
                        $jobApplicationArray    = array();                        
                        $applicationCountArray  = array();
                        $str                    = '';
                        $totalRecord            = count($totalJobApplication);
                        foreach($totalJobApplication as $key=>$jobApplication) {
                            $i++;
                            if($jobApplication['createdAt']!='' || $jobApplication['createdAt']!=NULL) { 
                                if($jobCode=='' && $i==1) {
                                    $applicationCountArray[]    = $jobApplication['applicationRecieved'];
                                    $str                        = "['".$jobApplication['createdAt']."', ".$jobApplication['applicationRecieved']."]";
                                    $jobCodeArray[]             = $jobApplication['title']." (".$jobApplication['fjCode'].")";
                                    
                                    if($totalRecord==1) {
                                        $jobApplicationArray[]  = $str;
                                        $jobCodeArray[]         = $jobApplication['title']." (".$jobApplication['fjCode'].")";
                                    }
                                    $jobCode = $jobApplication['id'];
                                }
                                else if($jobCode=='') {
                                    $applicationCountArray[]    = $jobApplication['applicationRecieved'];
                                    $str                        = "['".$jobApplication['createdAt']."', ".$jobApplication['applicationRecieved']."]";
                                    $jobCodeArray[]             = $jobApplication['title']." (".$jobApplication['fjCode'].")";
                                    
                                    if($totalRecord==$i) {
                                        $jobApplicationArray[]  = $str;
                                        $jobCodeArray[]         = $jobApplication['title']." (".$jobApplication['fjCode'].")";
                                    }
                                    $jobCode = $jobApplication['id'];
                                }
                                else if($jobApplication['id']==$jobCode) {
                                    $applicationCountArray[]    = $jobApplication['applicationRecieved'];
                                    $str                        .= ", ['".$jobApplication['createdAt']."', ".$jobApplication['applicationRecieved']."]";
                                    $jobCodeArray[]             = $jobApplication['title']." (".$jobApplication['fjCode'].")";
                                    if($totalRecord==$i) {
                                        $jobApplicationArray[]  = $str;
                                        $jobCodeArray[]         = $jobApplication['title']." (".$jobApplication['fjCode'].")";
                                    }
                                    $jobCode = $jobApplication['id'];
                                }
                                else if($jobCode!='' && $jobApplication['id']!=$jobCode) {
                                    $applicationCountArray[]    = $jobApplication['applicationRecieved'];
                                    $jobApplicationArray[]      = $str;
                                    $jobCodeArray[]             = $jobApplication['title']." (".$jobApplication['fjCode'].")";
                                    $str                        = '';
                                    $str                        = "['".$jobApplication['createdAt']."', ".$jobApplication['applicationRecieved']."]";
                                    $jobCode                    = $jobApplication['id'];
                                    
                                    if($totalRecord==$i) {
                                        $jobApplicationArray[]  = $str;
                                        $jobCodeArray[]         = $jobApplication['title']." (".$jobApplication['fjCode'].")";
                                    }
                                    $jobCode = $jobApplication['id'];
                                }
                                //echo $jobCode."D".$i."D".$str;
                                //echo "<pre>"; print_r($jobApplication);
                            }
                            else if($jobCode!='' && $totalRecord==$i) {
                                $jobApplicationArray[]  = $str;
                            } 
                        }
                        //echo "<pre>"; print_r(array_values(array_unique($jobCodeArray)));
                        //echo "<pre>"; print_r($jobApplicationArray);                        
                        //echo "<pre>"; print_r($applicationCountArray);
                        
                        ?>
                        
                        
                        <h4>Active Jobs - <?php echo $active_jobs['activeCount']; ?></h4>
                        <h4>Inactive Jobs - <?php echo $inactive_jobs['inactiveCount']; ?></h4>
                        
                        <div id="chart1" style="margin-top:20px; margin-left:20px; width:300px; height:300px; float: left"></div>
                        <div id="chart-timber" class="chart" style="margin-top:10%; margin-left:0px; width:90%; height:400px; float: left"></div>
                        
                        <script class="code" type="text/javascript">
                            $(document).ready(function () {
                                $.jqplot.config.enablePlugins = true;
                                var s1      = [<?php echo $active_jobs['activeCount']; ?>, <?php echo $inactive_jobs['inactiveCount']; ?>];
                                var ticks   = ['Active jobs', 'Inactive jobs'];

                                plot1 = $.jqplot('chart1', [s1], {
                                            // Only animate if we're not using excanvas (not in IE 7 or IE 8)..
                                            animate:        !$.jqplot.use_excanvas,
                                            seriesColors:['#4BB2C5', '#EAA228'],
                                            seriesDefaults:     {
                                                                    renderer: $.jqplot.BarRenderer,
                                                                    rendererOptions: {varyBarColor: true},
                                                                    pointLabels: {show: true}
                                                                },
                                            axes:       {
                                                                    xaxis: {
                                                                            renderer: $.jqplot.CategoryAxisRenderer,
                                                                            ticks: ticks
                                                                            },
                                                                    yaxis:  {
                                                                            min : 0,
                                                                            //tickInterval : 1 
                                                                            }
                                                                },
                                            highlighter:    {show: false}
                                        });

                                $('#chart1').bind('jqplotDataClick',
                                    function (ev, seriesIndex, pointIndex, data) {
                                        $('#info1').html('series: ' + seriesIndex + ', point: ' + pointIndex + ', data: ' + data);
                                    }
                                );
                            });
                        </script> 
                        
                        <script class="code" type="text/javascript">
                            <?php
                                $j=0;
                                $variableList = '';
                                $variableListValue = '';
                                if(count($jobApplicationArray)>0) {
                                    foreach($jobApplicationArray as $key=>$value) { 
                                        $j++;
                                        ${'line'.$key} = '['.$value.']';          
                                        if($j==1) {
                                            $variableList .= "line".$key;
                                            $variableListValue .= ${'line'.$key};
                                        }
                                        else {
                                            $variableList .= ", line".$key;  
                                            $variableListValue .= ", ".${'line'.$key}; 
                                        }
                                    }
                                }
                                else {
                                    $variableList = "[0]";
                                }
                            ?>
                                
                            var <?php echo $variableList;?>;
                            
                            <?php
                                $varList    = explode(',', $variableList);
                                $lineData   = '';
                                foreach($varList as $val) {
                                    $lineData .= trim($val).", ";
                                }
                            ?>

                            var timberData      =   [<?php echo $variableListValue; ?>];
                            var seriesLabels    =   [
                                                        {'label': 'Business as Usual' },
                                                        {'label': 'Alternative Scenario' }
                                                    ];
                            $(document).ready(function(){
                                options = {
                                            grid: {
                                                backgroundColor: "white",
                                            },
                                            series: seriesLabels, 
                                            axesDefaults: {
                                                labelRenderer: $.jqplot.CanvasAxisLabelRenderer
                                            },
                                            seriesDefaults: { // applies to all rows
                                                lineWidth: 2,
                                                style: 'square',
                                                rendererOptions: { smooth: true }
                                            },
                                            highlighter: {
                                                show: true,
                                                sizeAdjust: 7.5
                                            },
                                            legend: {
                                                renderer: $.jqplot.EnhancedLegendRenderer,
                                                show: true,
                                                showLabels: true,
                                                location: 'ne',
                                                placement: 'outside',
                                                fontSize: '11px',
                                                fontFamily: ["Lucida Grande","Lucida Sans Unicode","Arial","Verdana","sans-serif"],
                                                rendererOptions: {
                                                    seriesToggle: 'normal'
                                                }
                                            }
                              };
                              timberPlot = $.jqplot('chart-timber', timberData, $.extend(options, {
                                  title: 'Application Received Chart For Active Jobs',
                                  axes: {
                                    xaxis: {
                                        label: 'Dates',
                                        renderer: $.jqplot.DateAxisRenderer,
                                        rendererOptions: { tickRenderer: $.jqplot.CanvasAxisTickRenderer },
                                        tickOptions: {
                                            formatString: '%d/%m/%Y',
                                            angle: -30,
                                            fontFamily: 'Arial',
                                            fontSize: '13px',
                                            fontWeight: 'bold'
                                        },
                                        //min: "01-01-2012",
                                        tickInterval: '15 days',
//                                        labelOptions: {
//                                            fontFamily: 'Arial',
//                                            fontSize: '14pt',
//                                            fontWeight: 'bold',
//                                            textColor: '#0070A3'
//                                        }
                                    },
                                    yaxis: {
                                      label: "Applications",
                                      tickOptions: {formatString:'%d'}
                                    }
                                  },
                                  series: [
                                            <?php
                                            $jobCodeArray = array_values(array_unique($jobCodeArray));
                                            foreach($jobCodeArray as $key=>$jobCode) { ?>
                                                { label: ' - <?=$jobCode;?>' },
                                                <?php
                                            }
                                            ?>
                                        ],
                              }));
                            });
                        </script>
                        
                        
                    </div>
                </div>                
            </div>

        </div>
    </div>
</div>
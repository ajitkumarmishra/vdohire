<?php //echo "<pre>";print_r($contentMyQuestions); die();   ?>
<div id="page-content-wrapper">

    <div class="container-fluid whitebg">
        <div class="row">
            <div class="col-md-9"><h2><img src="<?php echo base_url(); ?>/theme/firstJob/image/arrow_head.png" class="prev-page">Applicant Funnel</h2></div>

            <div class="clearfix"></div>

            <div class="col-md-12 rsp_tbl">
                <div class="tab-content">
                    <div id="home" class="tab-pane fade active in">
                        <form class="form-vertical form_save" action="" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="inputEmail" class="control-label col-xs-1 required-field">From</label>
                                <div class="col-xs-2">
                                    <input type="text" class="form-control" placeholder="From Date" id="datepicker-13" value="<?php
                                        if (isset($_POST['fromDate'])): echo $_POST['fromDate'];
                                        endif;
                                        ?>" name="fromDate">
                                </div>
                            </div> 
                            <div class="form-group">
                                <label for="inputEmail" class="control-label col-xs-1 required-field">To</label>
                                <div class="col-xs-2">
                                    <input type="text" class="form-control" placeholder="To Date" id="datepicker-14" value="<?php
                                        echo $_POST['toDate'];
                                        ?>" name="toDate">
                                </div>
                            </div>                        
                            <div class="col-xs-offset-1 col-xs-4 no_padding">
                                <input type="hidden" name="accessToken" value="<?php echo $token; ?>"/>
                                <button type="submit" class="Save_frm">Search</button>
                                <button type="reset" class="Save_frm">Reset</button>
                            </div>
                        </form>
                        
                        <br><br><br>
                        
                        <?php
                        //echo count($applicationReceivedShortlistRatio); 
                        $totalJobs          = 0;
                        $totalL1Jobs        = 0;
                        $totalL1Viewed      = 0;
                        $L1pendingJobs      = 0;
                        $L1shortlistedJobs  = 0;
                        $L1rejectedJobs     = 0;
                        $totalL2Jobs        = 0;
                        $jobNotQualifyToL2  = 0;
                        $totalL2Viewed      = 0;
                        $L2pendingJobs      = 0;
                        $L2shortlistedJobs  = 0;
                        $L2rejectedJobs     = 0;
                        $totalShortlist     = 0;
                        $totalRejected      = 0;
                        //print_r($applicationReceivedShortlistRatio);
                        foreach($applicationReceivedShortlistRatio as $jobApplication) {
                            $sql    = "SELECT Distinct(level) FROM fj_jobPanel WHERE jobId='$jobApplication[jobId]'";
                            $query  = $this->db->query($sql);
                            $result = $query->result_array();
                            //echo count($result)."<br>";
                            if(count($result)==1) {
                                $totalL1Jobs++;
                                if($jobApplication['status']=='1') {
                                    $totalJobs++;
                                    $L1pendingJobs++;
                                }
                                else if($jobApplication['status']=='2') {
                                    $totalJobs++;
                                    $L1shortlistedJobs++;
                                    $totalL1Viewed++;
                                    $totalShortlist++;
                                }
                                else if($jobApplication['status']=='3') {
                                    $totalJobs++;
                                    $L1rejectedJobs++;
                                    $totalL1Viewed++;
                                    $totalRejected++;
                                }
                            }
                            else if(count($result)==2) {
                                $totalL1Jobs++;
                                $totalL2Jobs++;
                                //print_r($jobApplication);
                                if($jobApplication['status']=='1') {
                                    $totalJobs++;
                                    $L1pendingJobs++;
                                }
                                else if($jobApplication['status']=='2') {
                                    $totalJobs++;
                                    $L2pendingJobs++;
                                    $totalL1Viewed++;
                                }
                                else if($jobApplication['status']=='3') {
                                    $totalJobs++;
                                    $jobNotQualifyToL2++;
                                    $totalL1Viewed++;
                                    $totalRejected++;
                                }
                                else if($jobApplication['status']=='4') {
                                    $totalJobs++;
                                    $L2shortlistedJobs++;
                                    $totalL2Viewed++;
                                    $totalShortlist++;
                                }
                                else if($jobApplication['status']=='5') {
                                    $totalJobs++;
                                    $L2rejectedJobs++;
                                    $totalL2Viewed++;
                                    $totalRejected++;
                                }
                            }
                        }                        
                        ?>
                        
                        <div id="home" class="tab-pane fade active in">
                            <div class="form-group">
                                <div class="col-xs-4">
                                    <h4>Total Jobs Application - <?php echo $totalJobs;?></h4>
                                </div>
                            </div> 
                            <div class="form-group">
                                <div class="col-xs-4">
                                    <h4>L1 Jobs Application - <?php echo $totalL1Jobs;?></h4>Pending - <?php echo $L1pendingJobs; ?>, Shortlisted - <?php echo $L1shortlistedJobs; ?>, Rejected - <?php echo $L1rejectedJobs; ?>
                                </div>
                            </div> 
                            <div class="form-group">
                                <div class="col-xs-4">
                                    <h4>L2 Jobs Application - <?php echo $totalL2Jobs;?></h4>Failed at L1 - <?php echo $jobNotQualifyToL2;?>, Pending - <?php echo $L2pendingJobs; ?>, Shortlisted - <?php echo $L2shortlistedJobs; ?>, Rejected - <?php echo $L2rejectedJobs; ?>
                                </div>
                            </div>
                        </div>
                        
                        <div id="chart1" style="margin-top:20px; margin-left:20px; width:300px; height:300px; float: left"></div>
                        
                        <div id="chart6" style="margin-top:20px; margin-left:20px; width:300px; height:300px; float: right"></div>

                        <script class="code" type="text/javascript">
                            $(document).ready(function () {
                                $.jqplot.config.enablePlugins = true;
                                var s1      = [<?php echo $L1pendingJobs+$L2pendingJobs; ?>, <?php echo $totalShortlist; ?>, <?php echo $totalRejected; ?>];
                                var ticks   = ['Pending','Shortlisted', 'Rejected'];

                                plot1 = $.jqplot('chart1', [s1], {
                                            // Only animate if we're not using excanvas (not in IE 7 or IE 8)..
                                            animate:        !$.jqplot.use_excanvas,
                                            seriesColors:['#EAA228', '#C5B47F', '#579575'],
                                            seriesDefaults:     {
                                                                    renderer: $.jqplot.BarRenderer,
                                                                    rendererOptions: {varyBarColor: true},
                                                                    pointLabels: {show: true}
                                                                },
                                            axes:       {
                                                                    xaxis: {
                                                                            renderer: $.jqplot.CategoryAxisRenderer,
                                                                            ticks: ticks
                                                                            }
                                                                },
                                            highlighter:    {show: false}
                                        });

                                $('#chart1').bind('jqplotDataClick',
                                    function (ev, seriesIndex, pointIndex, data) {
                                        $('#info1').html('series: ' + seriesIndex + ', point: ' + pointIndex + ', data: ' + data);
                                    }
                                );
                            });

                            $(document).ready(function () {
                                var plot1 = $.jqplot('chart6', [[['Pending',<?php echo $L1pendingJobs+$L2pendingJobs; ?>],['Shortlisted',<?php echo $totalShortlist; ?>],['Rejected',<?php echo $totalRejected; ?>]]], {
                                    gridPadding: {top:0, bottom:38, left:0, right:0},
                                    seriesColors:['#EAA228', '#C5B47F', '#579575'],
                                    seriesDefaults:{
                                        renderer:$.jqplot.PieRenderer, 
                                        trendline:{ show:false }, 
                                        rendererOptions: { padding: 8, showDataLabels: true }
                                    },
                                    legend:{
                                        show:true, 
                                        placement: 'outside', 
                                        rendererOptions: {
                                            numberRows: 3
                                        }, 
                                        location:'s',
                                        marginTop: '10px'
                                    }       
                                });
                            });
                        </script> 
                        
                    </div>
                </div>     
                
                
                
                
                
                 
                <br><br><br>

                <?php
                //echo count($applicationReceivedShortlistRatio); 
                $totalJobs          = 0;
                $totalL1Jobs        = 0;
                $L1pendingJobs      = 0;
                $L1shortlistedJobs  = 0;
                $L1rejectedJobs     = 0;
                $totalL2Jobs        = 0;
                $jobNotQualifyToL2  = 0;
                $L2pendingJobs      = 0;
                $L2shortlistedJobs  = 0;
                $L2rejectedJobs     = 0;
                foreach($applicationReceivedShortlistRatio as $jobApplication) {
                    $sql    = "SELECT Distinct(level) FROM fj_jobPanel WHERE jobId='$jobApplication[jobId]'";
                    $query  = $this->db->query($sql);
                    $result = $query->result_array();
                    //echo count($result);
                    if(count($result)==1) {
                        $totalL1Jobs++;
                        if($jobApplication['status']=='1') {
                            $totalJobs++;
                            $L1pendingJobs++;
                        }
                        else if($jobApplication['status']=='2') {
                            $totalJobs++;
                            $L1shortlistedJobs++;
                        }
                        else if($jobApplication['status']=='3') {
                            $totalJobs++;
                            $L1rejectedJobs++;
                        }
                    }
                    else if(count($result)==2) {
                        $totalL1Jobs++;
                        $totalL2Jobs++;
                        //print_r($jobApplication);
                        if($jobApplication['status']=='1') {
                            $totalJobs++;
                            $L1pendingJobs++;
                        }
                        else if($jobApplication['status']=='2') {
                            $totalJobs++;
                            $L2pendingJobs++;
                        }
                        else if($jobApplication['status']=='3') {
                            $totalJobs++;
                            $L1rejectedJobs++;
                            $jobNotQualifyToL2++;
                        }
                        else if($jobApplication['status']=='4') {
                            $totalJobs++;
                            $L2shortlistedJobs++;
                        }
                        else if($jobApplication['status']=='5') {
                            $totalJobs++;
                            $L2rejectedJobs++;
                        }
                    }
                }                        
                ?>
                
                
                
                <div  style="float: left; width:100%; padding-top:50px;">
                    <h3><u>Level wise</u></h3>
                    <div id="home" class="tab-pane fade active in">
                        <div class="form-group">
                            <div class="col-xs-2">
                                <h4>Total Applicants - <?php echo $totalJobs;?></h4>
                            </div>
                        </div> 
                        <div class="form-group">
                            <div class="col-xs-4">
                                <h4>L1 Applicants - <?php echo $totalL1Jobs;?></h4>Pending - <?php echo $L1pendingJobs; ?>, Shortlisted - <?php echo $L1shortlistedJobs; ?>, Rejected - <?php echo $L1rejectedJobs; ?>
                            </div>
                        </div> 
                        <div class="form-group">
                            <div class="col-xs-4">
                                <h4>L2 Applicants - <?php echo $totalL2Jobs;?></h4>Failed at L1 - <?php echo $jobNotQualifyToL2;?>, Pending - <?php echo $L2pendingJobs; ?>, Shortlisted - <?php echo $L2shortlistedJobs; ?>, Rejected - <?php echo $L2rejectedJobs; ?>
                            </div>
                        </div>
                    </div>

                    <div id="chart5" style="margin-top:20px; margin-left:20px; width:100%; height:300px; float: left"></div>
<!--                    <div id="chart7" style="margin-top:10%; margin-left:20px; width:100%; height:400px; float: left"></div>-->

                    <script class="code" type="text/javascript">
                        $(document).ready(function () {
                            $.jqplot.config.enablePlugins = true;
                            var s1      = [<?php echo $totalL1Jobs; ?>, <?php echo $L1pendingJobs; ?>, <?php echo $L1shortlistedJobs; ?>, <?php echo $L1rejectedJobs; ?>, <?php echo $totalL2Jobs; ?>, <?php echo $jobNotQualifyToL2; ?>, <?php echo $L2pendingJobs; ?>, <?php echo $L2shortlistedJobs; ?>, <?php echo $L2rejectedJobs; ?>];
                            var ticks   = ['Total L1', 'L1 Pending', 'L1 Shortlisted ', 'L1 Rejected ', 'Total L2 ', 'Failed at L1', 'L2 Pending ', 'L2 Shortlisted ', 'L2 Rejected '];

                            plot1 = $.jqplot('chart5', [s1], {
                                        // Only animate if we're not using excanvas (not in IE 7 or IE 8)..
                                        animate:        !$.jqplot.use_excanvas,
                                        seriesColors:['#4BB2C5', '#EAA228', '#73C774', '#C7754C', '#4BB2C5', '#953579', '#EAA228', '#73C774', '#C7754C'],
                                        seriesDefaults:     {
                                                                renderer: $.jqplot.BarRenderer,
                                                                rendererOptions: {varyBarColor: true},
                                                                pointLabels: {show: true}
                                                            },
                                        axes:       {
                                                                xaxis: {
                                                                        renderer: $.jqplot.CategoryAxisRenderer,
                                                                        ticks: ticks
                                                                        }
                                                            },
                                        highlighter:    {show: false}
                                    });

                            $('#chart1').bind('jqplotDataClick',
                                function (ev, seriesIndex, pointIndex, data) {
                                    $('#info1').html('series: ' + seriesIndex + ', point: ' + pointIndex + ', data: ' + data);
                                }
                            );
                        });

//                        $(document).ready(function () {
//                            var plot1 = $.jqplot('chart7', [[['Total L1 Jobs',<?php echo $totalL1Jobs; ?>],['L1 Pending Jobs',<?php echo $L1pendingJobs; ?>],['L1 Shortlisted Jobs',<?php echo $L1shortlistedJobs; ?>],['L1 Rejected Jobs',<?php echo $L1rejectedJobs; ?>],['Total L2 Jobs',<?php echo $totalL2Jobs; ?>],['Failed At L1',<?php echo $jobNotQualifyToL2; ?>],['L2 Pending Jobs',<?php echo $L2pendingJobs; ?>],['L2 Shortlisted Jobs',<?php echo $L2shortlistedJobs; ?>],['L2 Rejected Jobs',<?php echo $L2rejectedJobs; ?>]]], {
//                                gridPadding: {top:0, bottom:38, left:0, right:0},
//                                seriesColors:['#4BB2C5', '#EAA228', '#579575', '#C7754C', '#4B5DE4', '#953579', '#E68B06', '#958C12', '#C0504D'],                                    
//                                seriesDefaults:{
//                                    renderer:$.jqplot.PieRenderer, 
//                                    trendline:{ show:false }, 
//                                    rendererOptions: { padding: 8, showDataLabels: true }
//                                },
//                                legend:{
//                                    show:true, 
//                                    placement: 'outside', 
//                                    rendererOptions: {
//                                        numberRows: 3
//                                    }, 
//                                    location:'s',
//                                    marginTop: '10px'
//                                }       
//                            });
//                        });
                    </script> 
                </div>     
                        
                        
                        
                        
                        
                        
                        
                        
            </div>

        </div>
    </div>
</div>
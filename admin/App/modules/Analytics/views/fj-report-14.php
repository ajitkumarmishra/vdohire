<?php //echo "<pre>";print_r($contentMyQuestions); die();   ?>
<?php //print_r($_POST); ?>
<div id="page-content-wrapper">

    <div class="container-fluid whitebg">
        <div class="row">
            <div class="col-md-9"><h2><img src="<?php echo base_url(); ?>/theme/firstJob/image/arrow_head.png" class="prev-page">Time Per Shortlisted</h2></div>

            <div class="clearfix"></div>

            <div class="col-md-12 rsp_tbl">
                <div class="tab-content">
                    <div id="home" class="tab-pane fade active in">
                        <?php                        
                        // Overall Shortlist Cost
                        $totalJobs              = 0;
                        $totalShortlistedJobs   = 0;
                        $jobArray               = array();
                        foreach($timePerShortlist as $key=>$jobApplication) {
                            $sql    = "SELECT Distinct(level) FROM fj_jobPanel WHERE jobId='$jobApplication[jobId]'";
                            $query  = $this->db->query($sql);
                            $result = $query->result_array();
                            
                            ${$jobApplication['fjCode']}  = array();                            
                            
                            if(count($result)==1) {
                                ${$jobApplication['fjCode']}['fjCode']                  = $jobApplication['fjCode'];
                                ${$jobApplication['fjCode']}['title']                   = $jobApplication['title'];
                                ${$jobApplication['fjCode']}['jobCreatedAt']            = $jobApplication['jobCreatedAt'];
                                ${$jobApplication['fjCode']}['applicationReceivedDate'] = $jobApplication['applicationReceivedDate'];                                
                                ${$jobApplication['fjCode']}['shorlistedDate']          = ($jobApplication['status']=='2'?$jobApplication['shorlistedByL1UserDate']:'');
                                $jobArray[$jobApplication['fjCode']][]                  = ${$jobApplication['fjCode']};
                            }
                            else if(count($result)==2) {
                                ${$jobApplication['fjCode']}['fjCode']                  = $jobApplication['fjCode'];
                                ${$jobApplication['fjCode']}['title']                   = $jobApplication['title'];
                                ${$jobApplication['fjCode']}['jobCreatedAt']            = $jobApplication['jobCreatedAt'];
                                ${$jobApplication['fjCode']}['applicationReceivedDate'] = $jobApplication['applicationReceivedDate'];
                                ${$jobApplication['fjCode']}['shorlistedDate']          = ($jobApplication['status']=='4'?$jobApplication['shorlistedByL2UserDate']:'');
                                $jobArray[$jobApplication['fjCode']][]                  = ${$jobApplication['fjCode']};
                            }
                        }  
                        //echo "<pre>"; print_r(($jobArray));
                        
                        
                        
                        function firstApplicationReceivedDate($a, $b){
                            $a = strtotime($a['applicationReceivedDate']);
                            $b = strtotime($b['applicationReceivedDate']);
                            if ($a == $b) {
                                return 0;
                            }
                            return ($a < $b) ? -1 : 1;
                        }
                        function firstShorlistedDate($a, $b) {
                            $a = strtotime($a['shorlistedDate']);
                            $b = strtotime($b['shorlistedDate']);
                            if ($a == $b) {
                                return 0;
                            }
                            return ($a < $b) ? -1 : 1;
                        }
                        function finalShorlistedDate($a, $b) {
                            $a = strtotime($a['shorlistedDate']);
                            $b = strtotime($b['shorlistedDate']);
                            if ($a == $b) {
                                return 0;
                            }
                            return ($a > $b) ? -1 : 1;
                        }
                        
                        
                        
                        //echo "<pre>";
                        $graphData = array();
                        foreach($jobArray as $key=>$job){
                            ${$job} = array();
                            ${$job}['fjCode']       = $job[0]['fjCode'];
                            ${$job}['title']        = $job[0]['title'];
                            ${$job}['jobCreatedAt'] = $job[0]['jobCreatedAt'];
                            //print_r($job);
                            usort($job, 'firstApplicationReceivedDate');
                            //print_r($job);
                            ${$job}['firstApplicationReceivedDate'] = $job[0]['applicationReceivedDate'];
                            usort($job, 'finalShorlistedDate'); 
                            //print_r($job);
                            ${$job}['firstShorlistedDate'] = $job[0]['shorlistedDate'];
                            usort($job, 'shorlistedDate');                            
                            //print_r($job);
                            ${$job}['finalShorlistedDate'] = $job[0]['shorlistedDate'];
                            $graphData[$job[0]['title']." (".$job[0]['fjCode'].")"] = ${$job};
                        }
                        //echo "<pre>"; print_r(($graphData));
                        ?>
                        
                        
                        <div class="form-group">
                            <label for="inputEmail" class="control-label col-xs-2 required-field">Select Job</label>
                            <div class="col-xs-5">
                                <select class="selectpicker search-critera search-critera-graph" name="jobTitle">
                                <option value="">---Select Job---</option>
                                <?php
                                ksort($graphData);
                                foreach($graphData as $KEY=>$jobs){
                                    if($jobs['finalShorlistedDate']!='') {
                                        $numberOfDays = round(abs(strtotime(date('Y-m-d', strtotime($jobs['jobCreatedAt'])))-strtotime(date('Y-m-d', strtotime($jobs['finalShorlistedDate']))))/86400);
                                    }
                                    else {
                                        $numberOfDays = 0;
                                    }
                                    
                                    $jobs['finalShorlistedDate'] = ($jobs['finalShorlistedDate']!=''?date('Y-m-d', strtotime($jobs['finalShorlistedDate'])):'');
                                    echo "<option value='".$KEY."' "
                                            . "data-fjcode='".$jobs['fjCode']."'" 
                                            . "data-title='".$jobs['title']."'" 
                                            . "data-jobcreatedat='".date('Y-m-d', strtotime($jobs['jobCreatedAt']))."'" 
                                            . "data-firstapplicationreceiveddate='".date('Y-m-d', strtotime($jobs['firstApplicationReceivedDate']))."'" 
                                            . "data-firstshorlisteddate='".$jobs['firstShorlistedDate']."'" 
                                            . "data-finalshorlisteddate='".$jobs['finalShorlistedDate']."'" 
                                            . "data-numberofdays='".$numberOfDays."'" 
                                            . ">".$KEY."</option>";
                                }
                                ?>
                            </select>
                            </div>
                        </div> 
                        
                        <div id="chart16" style="margin-top:20px; margin-left:40px; width:90%; height:300px; float: left"></div>
                        
                            
                    </div>
                </div>                
            </div>

        </div>
    </div>
</div>


<script>
        
//        $.jqplot.config.enablePlugins = true;
//        $(document).on('change', '.search-critera-graph', function () {
//            //var line2 = [['2008-01-01', 1], ['2008-02-01', 2], ['2008-03-01', 3], ['2008-04-01', 4]];
//            
//            var selected = $(this).find('option:selected');
//            var title                            = selected.data('title'); 
//            var fjcode                           = selected.data('fjcode'); 
//            var jobcreatedate                     = selected.data('jobcreatedat'); 
//            var firstapplicationreceiveddate     = selected.data('firstapplicationreceiveddate'); 
//            var firstshorlisteddate              = selected.data('firstshorlisteddate'); 
//            var finalshorlisteddate              = selected.data('finalshorlisteddate'); 
//            var numberofdays                     = selected.data('numberofdays'); 
//            //alert(jobcreatedate);
//            $('#chart16').html('');
//
//            if(firstshorlisteddate=='') {
//                var line2=[[jobcreatedate,1], [firstapplicationreceiveddate,2]];
//            }
//            else {
//                var line2=[[jobcreatedate,1], [firstapplicationreceiveddate,2], [firstshorlisteddate,3], [finalshorlisteddate,4]];
//            }
//                
//            var plot2 = $.jqplot('chart16', [line2], {
//              axes: {
//                xaxis: {
//                  renderer: $.jqplot.DateAxisRenderer,
//                  label: 'Date',
//                  labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
//                  tickRenderer: $.jqplot.CanvasAxisTickRenderer,
//                  tickOptions: {
//                      // labelPosition: 'middle',
//                      angle: 15
//                  }
//
//                },
//                yaxis: {
//                  label: 'Step Point',
//                  labelRenderer: $.jqplot.CanvasAxisLabelRenderer
//                }
//              }
//            });
//
//        });
        
        
        
        
        


        
        
        
        
        $.jqplot.config.enablePlugins = true;
        $(document).on('change', '.search-critera-graph', function () {
            var selected = $(this).find('option:selected');
            var title                            = selected.data('title'); 
            var fjcode                           = selected.data('fjcode'); 
            var jobcreatedat                     = selected.data('jobcreatedat'); 
            var firstapplicationreceiveddate     = selected.data('firstapplicationreceiveddate'); 
            var firstshorlisteddate              = selected.data('firstshorlisteddate'); 
            var finalshorlisteddate              = selected.data('finalshorlisteddate'); 
            var numberofdays                     = selected.data('numberofdays'); 
            //alert(firstapplicationreceiveddate);
            $('#chart16').html('');
           
                if(firstshorlisteddate=='') {
                    var line1=[[jobcreatedat,1], [firstapplicationreceiveddate,2]];
                }
                else {
                    var line1=[[jobcreatedat,1], [firstapplicationreceiveddate,2], [firstshorlisteddate,3], [finalshorlisteddate,4]];
                }
                
                
                var plot1 = $.jqplot('chart16', [line1], {
                  title:'Time Per Shortlisted',
                  axes:{
                        xaxis:{
                            label: "Date",
                            renderer: $.jqplot.CategoryAxisRenderer
                        },
                        yaxis: {
                            label: "Stage",
                            rendererOptions: { forceTickAt0: true, forceTickAt5: true },
                            min : 0,
                            tickInterval : 1
                        }
                  },
                  seriesDefaults: {
                                    rendererOptions: { smooth: true }
                                  }
                });
                $('#chart16').append( "<center><div style='margin-top:35%; width:100%;'>" );
                $('#chart16').append( "<b>Stage 1</b> - Job Posted Date</br>" );
                $('#chart16').append( "<b>Stage 2</b> - First Application Received</br>" );
                $('#chart16').append( "<b>Stage 3</b> - First Application Shortlisted</br>" );
                $('#chart16').append( "<b>Stage 4</b> - Final Application Shortlisted</br></br>" );
                $('#chart16').append( "</div></center>" );
        });
</script>
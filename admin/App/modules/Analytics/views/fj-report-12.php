<?php //echo "<pre>";print_r($contentMyQuestions); die();   ?>
<div id="page-content-wrapper">

    <div class="container-fluid whitebg">
        <div class="row">
            <div class="col-md-9"><h2><img src="<?php echo base_url(); ?>/theme/firstJob/image/arrow_head.png" class="prev-page">Applicants Demographics </h2></div>

            <div class="clearfix"></div>

            <div class="col-md-12 rsp_tbl">
                <div class="tab-content">
                    <div id="home" class="tab-pane fade active in">
                        <form class="form-vertical form_save" action="" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="inputEmail" class="control-label col-xs-1 required-field">From</label>
                                <div class="col-xs-2">
                                    <input type="text" class="form-control" placeholder="From Date" id="datepicker-13" value="<?php
                                        if (isset($_POST['fromDate'])): echo $_POST['fromDate'];
                                        endif;
                                        ?>" name="fromDate">
                                </div>
                            </div> 
                            <div class="form-group">
                                <label for="inputEmail" class="control-label col-xs-1 required-field">To</label>
                                <div class="col-xs-2">
                                    <input type="text" class="form-control" placeholder="To Date" id="datepicker-14" value="<?php
                                        if (isset($_POST['toDate'])): echo $_POST['toDate'];
                                        endif;
                                        ?>" name="toDate">
                                </div>
                            </div>                        
                            <div class="col-xs-offset-1 col-xs-4 no_padding">
                                <input type="hidden" name="accessToken" value="<?php echo $token; ?>"/>
                                <button type="submit" class="Save_frm">Search</button>
                                <button type="reset" class="Save_frm">Reset</button>
                            </div>
                        </form>
                        
                        <br><br><br>
                        
                        <?php
                        $L1shortlistedJobs      = 0;
                        $L2shortlistedJobs      = 0;
                        $totalMale              = 0;
                        $totalFemale            = 0;
                        $totalShortlistMale     = 0;
                        $totalShortlistFemale   = 0;
                        $pincodeArray           = array();
                        foreach($applicationReceivedShortlistRatio as $jobApplication) {
                            $sql    = "SELECT Distinct(level) FROM fj_jobPanel WHERE jobId='$jobApplication[jobId]'";
                            $query  = $this->db->query($sql);
                            $result = $query->result_array();
                            if(count($result)==1) {
                                if($jobApplication['gender']=='Male') {
                                    $totalMale++;
                                }
                                if($jobApplication['gender']=='Female') {
                                    $totalFemale++;
                                }
                                if($jobApplication['status']=='2') {
                                    $pincodeArray[] = $jobApplication['pincode'];
                                    if($jobApplication['gender']=='Male') {
                                        $totalShortlistMale++;
                                    }                                    
                                    else if($jobApplication['gender']=='Female') {
                                        $totalShortlistFemale++;
                                    }
                                    $L1shortlistedJobs++;
                                }
                            }
                            else if(count($result)==2) {
                                if($jobApplication['gender']=='Male') {
                                    $totalMale++;
                                }
                                if($jobApplication['gender']=='Female') {
                                    $totalFemale++;
                                }
                                if($jobApplication['status']=='4') {                                    
                                    $pincodeArray[] = $jobApplication['pincode'];
                                    if($jobApplication['gender']=='Male') {
                                        $totalShortlistMale++;
                                    }                                    
                                    else if($jobApplication['gender']=='Female') {
                                        $totalShortlistFemale++;
                                    }
                                    $L2shortlistedJobs++;
                                }
                            }
                        }    

                        //echo "<pre>"; print_r($pincodeArray);
                        $pincodeArray = array_count_values($pincodeArray);
                        //echo "<pre>"; print_r($pincodeArray);
                        $pincodeLocationShortlistCount = array();
                        if(count($pincodeArray)>0) {
                            foreach($pincodeArray as $pincode=>$count){
                                $sql    = "SELECT Distinct(city) FROM fj_pinCode WHERE pinCode='$pincode' LIMIT 0,1";
                                $query  = $this->db->query($sql);
                                $result = $query->row_array();
                                //$pincodeLocationShortlistCount[$result['city']] = $count;
                                if (array_key_exists($result['city'],$pincodeLocationShortlistCount)) {
                                	$pincodeLocationShortlistCount[$result['city']] = $pincodeLocationShortlistCount[$result['city']] + $pincodeArray[$pincode];
                                }
                                else {
                                	$pincodeLocationShortlistCount[$result['city']] = $pincodeArray[$pincode];
                                }
                                //echo $pincodeArray[$pincode]."<br>";
                            }
                        }
                        //echo "<pre>"; print_r($pincodeLocationShortlistCount);
                        
                        
                        $str = '';
                        $k=0;
                        if(count($pincodeLocationShortlistCount)>0) {
                            foreach($pincodeLocationShortlistCount as $location=>$count){
                                $k++;
                                $pincodeLocationShortlistCount[$result['city']] = $count;
                                if($k==1) {
                                    $str .= "['".$location."',".$count."]";
                                }
                                else {
                                    $str .= ", ['".$location."',".$count."]";
                                }
                            }
                        }
                        //echo "<pre>"; print_r($str);
                        ?>
                        
                        <div id="home" class="tab-pane fade active in">
                            <div class="form-group">
                                <div class="col-xs-5">
                                    <h4>Total Jobs Applications (Male/ Female)</h4>
                                    <div id="chart6" style="width:300px; height:300px; float: left;"></div>
                                </div>
                            </div> 
                            <div class="form-group">
                                <div class="col-xs-5">
                                    <h4>Total Shortlisted Jobs Applications (Male/ Female)</h4>
                                    <div id="chart7" style="width:300px; height:300px; float: left;"></div> 
                                </div>
                            </div> 
                        </div>
                        
                        
                        <div id="home" class="tab-pane fade active in">
                            <div class="form-group">
                                <div class="col-xs-10" style="margin-top: 5%;">
                                    <h4>Total Shortlists - Citywise</h4>
                                    <div id="chart8" style="width:100%; height:100%; float: right;"></div>
                                </div>
                            </div> 
                        </div>
                        
                                               
                        
                        <script class="code" type="text/javascript">
                            $(document).ready(function () {
                                var plot1 = $.jqplot('chart6', [[['Male',<?php echo $totalMale; ?>],['Female',<?php echo $totalFemale; ?>]]], {
                                    gridPadding: {top:0, bottom:38, left:0, right:0},
                                    seriesColors:['#4BB2C5', '#EAA228', '#579575', '#C7754C', '#4B5DE4', '#953579', '#E68B06', '#958C12', '#C0504D'],                                    
                                    seriesDefaults:{
                                        renderer:$.jqplot.PieRenderer, 
                                        trendline:{ show:false }, 
                                        rendererOptions:{sliceMargin: 3,startAngle: -90,showDataLabels: true,dataLabels: 'value'}
                                    },
                                    legend:{
                                        show:true, 
                                        placement: 'outside', 
                                        rendererOptions: {
                                            numberRows: 5
                                        }, 
                                        location:'s',
                                        marginTop: '10px'
                                    }       
                                });
                            });
                            
                            $(document).ready(function () {
                                var plot1 = $.jqplot('chart7', [[['Shortlisted Male',<?php echo $totalShortlistMale; ?>],['Shortlisted Female',<?php echo $totalShortlistFemale; ?>]]], {
                                    gridPadding: {top:0, bottom:38, left:0, right:0},
                                    seriesColors:['#4BB2C5', '#EAA228', '#579575', '#C7754C', '#4B5DE4', '#953579', '#E68B06', '#958C12', '#C0504D'],                                    
                                    seriesDefaults:{
                                        renderer:$.jqplot.PieRenderer, 
                                        trendline:{ show:false }, 
                                        rendererOptions:{sliceMargin: 3,startAngle: -90,showDataLabels: true,dataLabels: 'value'}
                                    },
                                    legend:{
                                        show:true, 
                                        placement: 'outside', 
                                        rendererOptions: {
                                            numberRows: 3
                                        }, 
                                        location:'s',
                                        marginTop: '10px'
                                    }       
                                });
                            });
                            
                            $(document).ready(function () {
                                var plot1 = $.jqplot('chart8', [[<?php echo $str; ?>]], {
                                    gridPadding: {top:0, bottom:38, left:0, right:0},
                                    seriesDefaults:{
                                        renderer:$.jqplot.PieRenderer, 
                                        trendline:{ show:false }, 
                                        rendererOptions:{sliceMargin: 3,startAngle: -90,showDataLabels: true,dataLabels: 'value'}
                                    },
                                    legend:{
                                        show:true, 
                                        placement: 'outside', 
                                        rendererOptions: {
                                            numberRows: 1
                                        }, 
                                        location:'s',
                                        marginTop: '10px'
                                    }       
                                });
                            });
                        </script> 
                        
                    </div>
                </div>                
            </div>

        </div>
    </div>
</div>
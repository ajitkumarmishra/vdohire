<script type="text/javascript" src="<?php echo base_url(); ?>/js/Chart.js"></script>
<style>
.analytics-tab li.active {
    background: white;
    color: black;
}

.analytics-tab li {
    font-size: 16px;
    background-color: #eee !important;
}

.analytics-tab li a {
    color: black;
}
.displayDescription{
	    display: block;
    position: absolute;
    background: white;
    color: black;
    border: 1px solid silver;
    border-radius: 9px;
    margin-left: 40px;
    width: 200px;
    font-size: 12px;
    padding: 10px;
}
</style>
<?php
$ci =&get_instance();
                    
$ci->load->model('userjob_model');
$ci->load->model('job_model');

//echo "<pre>";print_r($jobPendingApplicationData);die;
$InvitationDate = array();
foreach($jobInvitationData as $invitationData){
	
	if($invitationData['invitationDate'] != ""){
		
		/*if(!isset($Invitation[$invitationData['id']]['InvitationCount']) && $Invitation[$invitationData['id']]['InvitationCount'] == "")(
			$Invitation[$invitationData['id']]['InvitationCount'] = 0;
		)*/
		
		$Invitation[$invitationData['id']] = array(
								'jobName' => $invitationData['jobName'],
								'jobCode' => $invitationData['jobCode'],
								'InvitationCount' => $Invitation[$invitationData['id']]['InvitationCount'] +1,
								'appliedCount'  =>  0
							);
							
	}
	
	if($invitationData['appliedOn'] != ""){
		/*if(!isset($InvitationDate[$invitationData['id']]['appliedCount']) && $InvitationDate[$invitationData['id']]['appliedCount'] == "")(
			$InvitationDate[$invitationData['id']]['appliedCount'] = 0;
		)*/
		$Invitation[$invitationData['id']]['appliedCount'] = $Invitation[$invitationData['id']]['appliedCount'] +1;
	}
	
}
$appliedApplicantReport = array();
foreach($applicantsByJob as $applicantJob){
	if($applicantJob['L1ViewedDate'] != "" || $applicantJob['L1ViewedDate'] != ""){
		
		$appliedApplicantReport[$applicantJob['id']] = array(
								'jobName' => $applicantJob['jobName'],
								'jobCode' => $applicantJob['jobCode'],
								'ViewedCount' => $appliedApplicantReport[$applicantJob['id']]['ViewedCount'] +1,
								'shortlistedCount'  => $appliedApplicantReport[$applicantJob['id']]['shortlistedCount']+ 0
							);
							
	}
	
	if($applicantJob['shorlistedByL1UserDate'] != "" || $applicantJob['shorlistedByL2UserDate'] != ""){
		$appliedApplicantReport[$applicantJob['id']] = array(
								'jobName' => $applicantJob['jobName'],
								'jobCode' => $applicantJob['jobCode'],
								'ViewedCount' => $appliedApplicantReport[$applicantJob['id']]['ViewedCount']+0,
								'shortlistedCount'  => $appliedApplicantReport[$applicantJob['id']]['shortlistedCount']+ 1
							);
		
	}
	
}

?>
<script>
var jobStatusChartData = {
		labels: [<?php foreach($jobStatusAnalyticsData as $jobStatus){
			$hasData = $jobStatus['totalResponded']+$jobStatus['totalinvitationresponded'];
			if($hasData > 0){
				echo "'".str_replace("'","",$jobStatus['title'])."',";
			}
		} ?>],
		datasets: [{
			label: 'Other Applications',
			 backgroundColor: "rgb(39,172,191)",
			data: [<?php foreach($jobStatusAnalyticsData as $jobStatus){
				$hasData = $jobStatus['totalResponded']+$jobStatus['totalinvitationresponded'];
				if($hasData > 0){
					echo "".($jobStatus['totalResponded']-$jobStatus['totalinvitationresponded']).",";
				}
		} ?>],
		}, {
			label: 'Invited Applications',
			backgroundColor: "rgb(140,46,10)",
			data: [<?php foreach($jobStatusAnalyticsData as $jobStatus){
				$hasData = $jobStatus['totalResponded']+$jobStatus['totalinvitationresponded'];
				if($hasData > 0){
					echo "".$jobStatus['totalinvitationresponded'].",";
				}
		} ?>],
		}]

	};
</script>
<script>
var randomScalingFactor = function() {
        return Math.round(Math.random() * 100);
    };
	
	
	
	
	var PendingApplicationChartData = {
		labels: [<?php foreach($jobPendingApplicationData as $PendingApplication){
			$hasData = $PendingApplication['totalPendingApplicationAtL1']+$PendingApplication['totalShortlistedApplicationAtL1']+$PendingApplication['totalRejectedApplicationAtL1']+$PendingApplication['totalPendingApplicationAtL2']+$PendingApplication['totalShortlistedApplicationAtL2']+$PendingApplication['totalRejectedApplicationAtL2'];
			if($hasData > 0){
				echo "'".$PendingApplication['title']."',";
			}
		} ?>],
		datasets: [{
			label: 'Shortlisted Applications At L1',
			backgroundColor: "rgb(174,189,56)",
			stack: 'Stack 0',
			data: [<?php foreach($jobPendingApplicationData as $PendingApplication){
				$hasData = $PendingApplication['totalPendingApplicationAtL1']+$PendingApplication['totalShortlistedApplicationAtL1']+$PendingApplication['totalRejectedApplicationAtL1']+$PendingApplication['totalPendingApplicationAtL2']+$PendingApplication['totalShortlistedApplicationAtL2']+$PendingApplication['totalRejectedApplicationAtL2'];
				if($hasData > 0){
					echo "".$PendingApplication['totalShortlistedApplicationAtL1'].",";
				}
		} ?>],
		},{
			label: 'Pending Applications At L1',
			backgroundColor: "rgb(39,172,191)",
			stack: 'Stack 0',
			data: [<?php foreach($jobPendingApplicationData as $PendingApplication){
				$hasData = $PendingApplication['totalPendingApplicationAtL1']+$PendingApplication['totalShortlistedApplicationAtL1']+$PendingApplication['totalRejectedApplicationAtL1']+$PendingApplication['totalPendingApplicationAtL2']+$PendingApplication['totalShortlistedApplicationAtL2']+$PendingApplication['totalRejectedApplicationAtL2'];
				if($hasData > 0){
					echo "".$PendingApplication['totalPendingApplicationAtL1'].",";
				}
		} ?>],
		},{
			label: 'Rejected Applications At L1',
			backgroundColor: "rgb(238,110,61)",
			stack: 'Stack 0',
			data: [<?php foreach($jobPendingApplicationData as $PendingApplication){
				$hasData = $PendingApplication['totalPendingApplicationAtL1']+$PendingApplication['totalShortlistedApplicationAtL1']+$PendingApplication['totalRejectedApplicationAtL1']+$PendingApplication['totalPendingApplicationAtL2']+$PendingApplication['totalShortlistedApplicationAtL2']+$PendingApplication['totalRejectedApplicationAtL2'];
				if($hasData > 0){
					echo "".$PendingApplication['totalRejectedApplicationAtL1'].",";
				}
		} ?>],
		},{
			label: 'Shortlisted Applications At L2',
			backgroundColor: "rgb(111,123,11)",
			stack: 'Stack 1',
			data: [<?php foreach($jobPendingApplicationData as $PendingApplication){
				$hasData = $PendingApplication['totalPendingApplicationAtL1']+$PendingApplication['totalShortlistedApplicationAtL1']+$PendingApplication['totalRejectedApplicationAtL1']+$PendingApplication['totalPendingApplicationAtL2']+$PendingApplication['totalShortlistedApplicationAtL2']+$PendingApplication['totalRejectedApplicationAtL2'];
				if($hasData > 0){
					echo "".$PendingApplication['totalShortlistedApplicationAtL2'].",";
				}
		} ?>],
		},{
			label: 'Pending Applications At L2',
			backgroundColor: "rgb(35,98,170)",
			stack: 'Stack 1',
			data: [<?php foreach($jobPendingApplicationData as $PendingApplication){
				$hasData = $PendingApplication['totalPendingApplicationAtL1']+$PendingApplication['totalShortlistedApplicationAtL1']+$PendingApplication['totalRejectedApplicationAtL1']+$PendingApplication['totalPendingApplicationAtL2']+$PendingApplication['totalShortlistedApplicationAtL2']+$PendingApplication['totalRejectedApplicationAtL2'];
				if($hasData > 0){
					echo "".$PendingApplication['totalPendingApplicationAtL2'].",";
				}
		} ?>],
		},{
			label: 'Rejected Applications At L2',
			backgroundColor: "rgb(177,36,36)",
			stack: 'Stack 1',
			data: [<?php foreach($jobPendingApplicationData as $PendingApplication){
				$hasData = $PendingApplication['totalPendingApplicationAtL1']+$PendingApplication['totalShortlistedApplicationAtL1']+$PendingApplication['totalRejectedApplicationAtL1']+$PendingApplication['totalPendingApplicationAtL2']+$PendingApplication['totalShortlistedApplicationAtL2']+$PendingApplication['totalRejectedApplicationAtL2'];
				if($hasData > 0){
					echo "".$PendingApplication['totalRejectedApplicationAtL2'].",";
				}
		} ?>],
		}
		
		]

	};
	
	var jobFullfillmentDataChartData = {
		labels: [<?php foreach($jobFullfillmentData as $fullfilment){
			$hasData = $fullfilment['noOfVacancies']+$fullfilment['totalApplications'];
			if($hasData > 0){
				echo "'".$fullfilment['title']."',";
			}
		} ?>],
		datasets: [{
			label: 'No of Vacancies',
			 backgroundColor: "rgb(39,172,191)",
			data: [<?php foreach($jobFullfillmentData as $fullfilment){
				$hasData = $fullfilment['noOfVacancies']+$fullfilment['totalApplications'];
				if($hasData > 0){
					echo "".($fullfilment['noOfVacancies']).",";
				}
		} ?>],
		}, {
			label: 'Total Applications',
			backgroundColor: "rgb(140,46,10)",
			data: [<?php foreach($jobFullfillmentData as $fullfilment){
				$hasData = $fullfilment['noOfVacancies']+$fullfilment['totalApplications'];
				if($hasData > 0){
					echo "".$fullfilment['totalApplications'].",";
				}
		} ?>],
		}]

	};
	
	
	var jobInvitationDataChartData = {
		labels: [<?php foreach($Invitation as $data){
				echo "'".$data['jobName']."',";
			
		} ?>],
		datasets: [{
			label: 'Invitation Count',
			 backgroundColor: "rgb(39,172,191)",
			data: [<?php foreach($Invitation as $data){
				
					echo "".($data['InvitationCount']).",";
				
		} ?>],
		}, {
			label: 'Invited Applications',
			backgroundColor: "rgb(140,46,10)",
			data: [<?php foreach($Invitation as $data){
				
					echo "".$data['appliedCount'].",";
				
		} ?>],
		}]

	};
	
	
	var appliedJobDataChartData = {
		labels: [<?php foreach($appliedApplicantReport as $data){
				echo "'".$data['jobName']."',";
			
		} ?>],
		datasets: [{
			label: 'Shortlisted Count',
			 backgroundColor: "rgb(39,172,191)",
			data: [<?php foreach($appliedApplicantReport as $data){
				
					echo "".($data['shortlistedCount']).",";
				
		} ?>],
		}, {
			label: 'Viewed Count',
			backgroundColor: "rgb(140,46,10)",
			data: [<?php foreach($appliedApplicantReport as $data){
				
					echo "".$data['ViewedCount'].",";
				
		} ?>],
		}]

	};
	
	var applicantProfileByJobDataChartData = {
		labels: [<?php foreach($applicantProfileByJob as $data){
			$hasData = $data['appliedTotal']+$data['appliedInvitedCount']+$data['shortlistedTotal']+$data['shortlistedInvitedCount'];
			if($hasData > 0){
				echo "'".$data['jobName']."',";
			}
			
		} ?>],
		datasets: [{
			label: 'Applications from top 3 cities',
			 backgroundColor: "rgb(39,172,191)",
			data: [<?php foreach($applicantProfileByJob as $data){
				$hasData = $data['appliedTotal']+$data['appliedInvitedCount']+$data['shortlistedTotal']+$data['shortlistedInvitedCount'];
				if($hasData > 0){
					echo "".($data['appliedTotal']).",";
				}
				
		} ?>],
		}, {
			label: 'Applications from other cities',
			backgroundColor: "rgb(140,46,10)",
			data: [<?php foreach($applicantProfileByJob as $data){
				$hasData = $data['appliedTotal']+$data['appliedInvitedCount']+$data['shortlistedTotal']+$data['shortlistedInvitedCount'];
				if($hasData > 0){
					echo "".$data['appliedInvitedCount'].",";
				}
				
		} ?>],
		}]

	};
	
	var applicansByJobRecruiterDataChartData = {
		labels: [<?php foreach($applicansByJobRecruiter as $data){
			$hasData = $data['totalApplictionReceived']+$data['totalPendingApplicationAtL1']+$data['totalShortlistedApplicationAtL1']+$data['totalRejectedApplicationAtL1']+$data['totalPendingApplicationAtL2']+$data['totalShortlistedApplicationAtL2'];
			if($hasData > 0){
				echo "'".$data['jobName']."',";
			}
			
		} ?>],
		datasets: [{
			label: 'Pending Applications at L1',
			backgroundColor: "rgb(39,172,191)",
			stack: 'Stack 0',
			data: [<?php foreach($applicansByJobRecruiter as $data){
				$hasData = $data['totalPendingApplicationAtL1']+$data['totalShortlistedApplicationAtL1']+$data['totalRejectedApplicationAtL1']+$data['totalPendingApplicationAtL2']+$data['totalShortlistedApplicationAtL2']+$data['totalRejectedApplicationAtL2'];
				if($hasData > 0){
					echo "".$data['totalPendingApplicationAtL1'].",";
				}
				
		} ?>],
		},{
			label: 'Shortlisted Applications at L1',
			backgroundColor: "rgb(174,189,56)",
			stack: 'Stack 0',
			data: [<?php foreach($applicansByJobRecruiter as $data){
				$hasData = $data['totalPendingApplicationAtL1']+$data['totalShortlistedApplicationAtL1']+$data['totalRejectedApplicationAtL1']+$data['totalPendingApplicationAtL2']+$data['totalShortlistedApplicationAtL2']+$data['totalRejectedApplicationAtL2'];
				if($hasData > 0){
					echo "".$data['totalShortlistedApplicationAtL1'].",";
				}
				
		} ?>],
		},{
			label: 'Rejected Applications at L1',
			backgroundColor: "rgb(238,110,61)",
			stack: 'Stack 0',
			data: [<?php foreach($applicansByJobRecruiter as $data){
				$hasData = $data['totalPendingApplicationAtL1']+$data['totalShortlistedApplicationAtL1']+$data['totalRejectedApplicationAtL1']+$data['totalPendingApplicationAtL2']+$data['totalShortlistedApplicationAtL2']+$data['totalRejectedApplicationAtL2'];
				if($hasData > 0){
					echo "".$data['totalRejectedApplicationAtL1'].",";
				}
				
		} ?>],
		},{
			label: 'Pending Applicatins at L2',
			backgroundColor: "rgb(39,172,191)",
			stack: 'Stack 1',
			data: [<?php foreach($applicansByJobRecruiter as $data){
				$hasData = $data['totalPendingApplicationAtL1']+$data['totalShortlistedApplicationAtL1']+$data['totalRejectedApplicationAtL1']+$data['totalPendingApplicationAtL2']+$data['totalShortlistedApplicationAtL2']+$data['totalRejectedApplicationAtL2'];
				if($hasData > 0){
					echo "".$data['totalPendingApplicationAtL2'].",";
				}
				
		} ?>],
		},{
			label: 'Shortlisted Applications at L2',
			backgroundColor: "rgb(174,189,56)",
			stack: 'Stack 1',
			data: [<?php foreach($applicansByJobRecruiter as $data){
				$hasData = $data['totalPendingApplicationAtL1']+$data['totalShortlistedApplicationAtL1']+$data['totalRejectedApplicationAtL1']+$data['totalPendingApplicationAtL2']+$data['totalShortlistedApplicationAtL2']+$data['totalRejectedApplicationAtL2'];
				if($hasData > 0){
					echo "".$data['totalShortlistedApplicationAtL2'].",";
				}
				
		} ?>],
		},{
			label: 'Rejected Applications at L2',
			backgroundColor: "rgb(238,110,61)",
			stack: 'Stack 1',
			data: [<?php foreach($applicansByJobRecruiter as $data){
				$hasData = $data['totalPendingApplicationAtL1']+$data['totalShortlistedApplicationAtL1']+$data['totalRejectedApplicationAtL1']+$data['totalPendingApplicationAtL2']+$data['totalShortlistedApplicationAtL2']+$data['totalRejectedApplicationAtL2'];
				if($hasData > 0){
					echo "".$data['totalRejectedApplicationAtL2'].",";
				}
				
		} ?>],
		}]

	};
	
	var recruitersStatusDataChartData = {
		labels: [<?php foreach($recruitersStatus as $data){
			$hasData = $data['noOfJobsCreated']+$data['totalL1QueueEveluated']+$data['totalL1QueueEveluatedYes']+$data['totalL1QueueEveluatedNo']+$data['totalL2QueueEveluated']+$data['totalL2QueueEveluatedYes']+$data['totalL2QueueEveluatedNo']+$data['totalL1QueuePending']+$data['totalL2QueuePending']+$data['totalAssessmentCreated']+$data['totalInterviewCreated'];
			if($hasData > 0){
				echo "'".$data['recruiterName']."',";
			}
			
		} ?>],
		datasets: [{
			label: 'Applications Shortlisted As L1',
			backgroundColor: "rgb(174,189,56)",
			stack: 'Stack 0',
			data: [<?php foreach($recruitersStatus as $data){
				$hasData = $data['noOfJobsCreated']+$data['totalL1QueueEveluated']+$data['totalL1QueueEveluatedYes']+$data['totalL1QueueEveluatedNo']+$data['totalL2QueueEveluated']+$data['totalL2QueueEveluatedYes']+$data['totalL2QueueEveluatedNo']+$data['totalL1QueuePending']+$data['totalL2QueuePending']+$data['totalAssessmentCreated']+$data['totalInterviewCreated'];
				if($hasData > 0){
					echo "".$data['totalL1QueueEveluatedYes'].",";
				}
		} ?>],
		},{
			label: 'Applications Rejected As L1',
			backgroundColor: "rgb(238,110,61)",
			stack: 'Stack 0',
			data: [<?php foreach($recruitersStatus as $data){
				$hasData = $data['noOfJobsCreated']+$data['totalL1QueueEveluated']+$data['totalL1QueueEveluatedYes']+$data['totalL1QueueEveluatedNo']+$data['totalL2QueueEveluated']+$data['totalL2QueueEveluatedYes']+$data['totalL2QueueEveluatedNo']+$data['totalL1QueuePending']+$data['totalL2QueuePending']+$data['totalAssessmentCreated']+$data['totalInterviewCreated'];
				if($hasData > 0){
					echo "".$data['totalL1QueueEveluatedNo'].",";
				}
		} ?>],
		},{
			label: 'Applications Shortlisted As L2',
			backgroundColor: "rgb(111,123,11)",
			stack: 'Stack 1',
			data: [<?php foreach($recruitersStatus as $data){
				$hasData = $data['noOfJobsCreated']+$data['totalL1QueueEveluated']+$data['totalL1QueueEveluatedYes']+$data['totalL1QueueEveluatedNo']+$data['totalL2QueueEveluated']+$data['totalL2QueueEveluatedYes']+$data['totalL2QueueEveluatedNo']+$data['totalL1QueuePending']+$data['totalL2QueuePending']+$data['totalAssessmentCreated']+$data['totalInterviewCreated'];
				if($hasData > 0){
					echo "".$data['totalL2QueueEveluatedYes'].",";
				}
		} ?>],
		},{
			label: 'Applications Rejected As L2',
			backgroundColor: "rgb(177,36,36)",
			stack: 'Stack 1',
			data: [<?php foreach($recruitersStatus as $data){
				$hasData = $data['noOfJobsCreated']+$data['totalL1QueueEveluated']+$data['totalL1QueueEveluatedYes']+$data['totalL1QueueEveluatedNo']+$data['totalL2QueueEveluated']+$data['totalL2QueueEveluatedYes']+$data['totalL2QueueEveluatedNo']+$data['totalL1QueuePending']+$data['totalL2QueuePending']+$data['totalAssessmentCreated']+$data['totalInterviewCreated'];
				if($hasData > 0){
					echo "".$data['totalL2QueueEveluatedNo'].",";
				}
		} ?>],
		}]

	};
	
	
	$(function () {
		jobStatus();
		jobStatusPopup();
		PendingApplication();
		PendingApplicationPopUp();
		JobInvitation();
		JobInvitationPopUp();
		jobFullfillment();
		jobFullfillmentPopup();
		appliedJobReport();
		appliedJobReportPopUp();
		applicantProfileByJob();
		applicantProfileByPopUp();
		applicansByJobRecruiter();
		applicansByJobRecruiterPopUp();
		recruitersStatus();
		recruitersStatusGraphViewPopup();
    });
	
  
	
	
	function jobStatus(){
		 var ctx = document.getElementById("jobStatusGraphView").getContext("2d");
            window.myBar = new Chart(ctx, {
				
                type: 'bar',
                data: jobStatusChartData,
                options: {
					responsive: true,
					maintainAspectRatio: false,
                    title:{
                        display:false,
                        text:"Evaluation Performance",
                        position:"top",
						fontColor: "Black"
						
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false
                    },
                    responsive: true,
                    scales: {
                        xAxes: [{
                            stacked: true,
							gridLines: {
									display: false,
									color: "#000"
								},
								ticks: {
									fontColor: "#000",
									autoSkip: false,
									maxRotation: 90,
									minRotation: 90,
									callback: function(value) {
										return value.substr(0, 10);//truncate
									}
								}
                        }],
                        yAxes: [{
                            stacked: true,
							gridLines: {
									display: true,
									color: "#000"
								},
								ticks: {
									min: 0,
									callback: function(value, index, values) {
										if (Math.floor(value) === value) {
											return value;
										}
									}
								}
                        }]
                    },
					legend: {
						display: false,
						position: 'top',
						labels: {
							position: 'left',
							fontSize: 10
						}
					}
                }
				
				
				
				
				
            });
		
	}
	
	function jobStatusPopup(){
		 var ctx = document.getElementById("jobStatusGraphViewPopup").getContext("2d");
            window.myBar = new Chart(ctx, {
                type: 'bar',
                data: jobStatusChartData,
                options: {
					responsive: true,
					maintainAspectRatio: false,
                    title:{
                        display:false,
                        text:"Evaluation Performance",
                        position:"top",
						fontColor: "Black"
						
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false
                    },
                    responsive: true,
                    scales: {
                        xAxes: [{
                            stacked: true,
							barPercentage: 0.5,
							gridLines: {
									display: false,
									color: "#000"
								},
								ticks: {
									fontColor: "#000",
									autoSkip: false,
									maxRotation: 90,
									minRotation: 90,
									callback: function(value) {
										return value.substr(0, 10);//truncate
									}
								}
                        }],
                        yAxes: [{
                            stacked: true,
							gridLines: {
									display: true,
									color: "#000"
								},
								ticks: {
									min: 0,
									callback: function(value, index, values) {
										if (Math.floor(value) === value) {
											return value;
										}
									}
								}
                        }]
                    },
					legend: {
						display: false,
						position: 'top',
						labels: {
							position: 'right'
						}
					}
                }
				
				
				
				
				
            });
		
	}
	
	function PendingApplication(){
		 var ctx = document.getElementById("PendingApplication").getContext("2d");
            window.myBar = new Chart(ctx, {
                type: 'bar',
                data: PendingApplicationChartData,
                options: {
					responsive: true,
					maintainAspectRatio: false,
                    title:{
                        display:false,
                        text:"Evaluation Performance",
                        position:"top",
						fontColor: "Black"
						
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false
                    },
                    responsive: true,
                    scales: {
                        xAxes: [{
                            stacked: true,
							gridLines: {
									display: false,
									color: "#000"
								},
								ticks: {
									fontColor: "#000",
									autoSkip: false,
									maxRotation: 90,
									minRotation: 90,
									callback: function(value) {
										return value.substr(0, 10);//truncate
									}
								}
                        }],
                        yAxes: [{
                            stacked: true,
							gridLines: {
									display: true,
									color: "#000"
								},
								ticks: {
									min: 0,
									callback: function(value, index, values) {
										if (Math.floor(value) === value) {
											return value;
										}
									}
								}
                        }]
                    },
					legend: {
						display: false,
						position: 'top',
						labels: {
							position: 'right'
						}
					}
                }
				
				
				
				
				
            });
		
	}
	
	function PendingApplicationPopUp(){
		 var ctx = document.getElementById("PendingApplicationGraphViewPopup").getContext("2d");
            window.myBar = new Chart(ctx, {
                type: 'bar',
                data: PendingApplicationChartData,
                options: {
					responsive: true,
					maintainAspectRatio: false,
                    title:{
                        display:false,
                        text:"Evaluation Performance",
                        position:"top",
						fontColor: "Black"
						
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false
                    },
                    responsive: true,
                    scales: {
                        xAxes: [{
                            stacked: true,
							gridLines: {
									display: false,
									color: "#000"
								},
								ticks: {
									fontColor: "#000",
									autoSkip: false,
									maxRotation: 90,
									minRotation: 90,
									callback: function(value) {
										return value.substr(0, 10);//truncate
									}
								}
                        }],
                        yAxes: [{
                            stacked: true,
							gridLines: {
									display: true,
									color: "#000"
								},
								ticks: {
									min: 0,
									callback: function(value, index, values) {
										if (Math.floor(value) === value) {
											return value;
										}
									}
								}
                        }]
                    },
					legend: {
						display: false,
						position: 'top',
						labels: {
							position: 'right'
						}
					}
                }
				
				
				
				
				
            });
		
	}
	
	
	function JobInvitation(){
		 var ctx = document.getElementById("jobInvitationsGraphView").getContext("2d");
            window.myBar = new Chart(ctx, {
                type: 'bar',
                data: jobInvitationDataChartData,
                options: {
					responsive: true,
					maintainAspectRatio: false,
                    title:{
                        display:false,
                        text:"Evaluation Performance",
                        position:"top",
						fontColor: "Black"
						
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false
                    },
                    responsive: true,
                    scales: {
                        xAxes: [{
                            stacked: false,
							gridLines: {
									display: false,
									color: "#000"
								},
								ticks: {
									fontColor: "#000",
									autoSkip: false,
									maxRotation: 90,
									minRotation: 90,
									callback: function(value) {
										return value.substr(0, 10);//truncate
									}
								}
                        }],
                        yAxes: [{
                            stacked: false,
							gridLines: {
									display: true,
									color: "#000"
								},
								ticks: {
									min: 0,
									callback: function(value, index, values) {
										if (Math.floor(value) === value) {
											return value;
										}
									}
								}
                        }]
                    },
					legend: {
						display: false,
						position: 'top',
						labels: {
							position: 'right'
						}
					}
                }
				
				
				
				
				
            });
		
	}
	
	function JobInvitationPopUp(){
		 var ctx = document.getElementById("jobInvitationsPopUpGraphViewPopup").getContext("2d");
            window.myBar = new Chart(ctx, {
                type: 'bar',
                data: jobInvitationDataChartData,
                options: {
					responsive: true,
					maintainAspectRatio: false,
                    title:{
                        display:false,
                        text:"Evaluation Performance",
                        position:"top",
						fontColor: "Black"
						
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false
                    },
                    responsive: true,
                    scales: {
                        xAxes: [{
                            stacked: false,
							gridLines: {
									display: false,
									color: "#000"
								},
								ticks: {
									fontColor: "#000",
									autoSkip: false,
									maxRotation: 90,
									minRotation: 90,
									callback: function(value) {
										return value.substr(0, 10);//truncate
									}
								}
                        }],
                        yAxes: [{
                            stacked: false,
							gridLines: {
									display: true,
									color: "#000"
								},
								ticks: {
									min: 0,
									callback: function(value, index, values) {
										if (Math.floor(value) === value) {
											return value;
										}
									}
								}
                        }]
                    },
					legend: {
						display: false,
						position: 'top',
						labels: {
							position: 'right'
						}
					}
                }
				
				
				
				
				
            });
		
	}
	
	function jobFullfillment(){
		 var ctx = document.getElementById("jobFullfillment").getContext("2d");
            window.myBar = new Chart(ctx, {
                type: 'bar',
                data: jobFullfillmentDataChartData,
                options: {
					responsive: true,
					maintainAspectRatio: false,
                    title:{
                        display:false,
                        text:"Evaluation Performance",
                        position:"top",
						fontColor: "Black"
						
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false
                    },
                    responsive: true,
                    scales: {
                        xAxes: [{
                            stacked: false,
							gridLines: {
									display: false,
									color: "#000"
								},
								ticks: {
									fontColor: "#000",
									autoSkip: false,
									maxRotation: 90,
									minRotation: 90,
									callback: function(value) {
										return value.substr(0, 10);//truncate
									}
								}
                        }],
                        yAxes: [{
                            stacked: false,
							gridLines: {
									display: true,
									color: "#000"
								},
								ticks: {
									min: 0,
									callback: function(value, index, values) {
										if (Math.floor(value) === value) {
											return value;
										}
									}
								}
                        }]
                    },
					legend: {
						display: false,
						position: 'top',
						labels: {
							position: 'right'
						}
					}
                }
				
				
				
				
				
            });
		
	}
	
	function jobFullfillmentPopup(){
		 var ctx = document.getElementById("jobFullfillmentGraphViewPopup").getContext("2d");
            window.myBar = new Chart(ctx, {
                type: 'bar',
                data: jobFullfillmentDataChartData,
                options: {
					responsive: true,
					maintainAspectRatio: false,
                    title:{
                        display:false,
                        text:"Evaluation Performance",
                        position:"top",
						fontColor: "Black"
						
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false
                    },
                    responsive: true,
                    scales: {
                        xAxes: [{
                            stacked: false,
							gridLines: {
									display: false,
									color: "#000"
								},
								ticks: {
									fontColor: "#000",
									autoSkip: false,
									maxRotation: 90,
									minRotation: 90,
									callback: function(value) {
										return value.substr(0, 10);//truncate
									}
								}
                        }],
                        yAxes: [{
                            stacked: false,
							gridLines: {
									display: true,
									color: "#000"
								},
								ticks: {
									min: 0,
									callback: function(value, index, values) {
										if (Math.floor(value) === value) {
											return value;
										}
									}
								}
                        }]
                    },
					legend: {
						display: false,
						position: 'top',
						labels: {
							position: 'right'
						}
					}
                }
				
				
				
				
				
            });
		
	}
	
	
	function appliedJobReport(){
		 var ctx = document.getElementById("jobApplicationsGraphView").getContext("2d");
            window.myBar = new Chart(ctx, {
                type: 'bar',
                data: appliedJobDataChartData,
                options: {
					responsive: true,
					maintainAspectRatio: false,
                    title:{
                        display:false,
                        text:"Evaluation Performance",
                        position:"top",
						fontColor: "Black"
						
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false
                    },
                    responsive: true,
                    scales: {
                        xAxes: [{
                            stacked: false,
							gridLines: {
									display: false,
									color: "#000"
								},
								ticks: {
									fontColor: "#000",
									autoSkip: false,
									maxRotation: 90,
									minRotation: 90,
									callback: function(value) {
										return value.substr(0, 10);//truncate
									}
								}
                        }],
                        yAxes: [{
                            stacked: false,
							gridLines: {
									display: true,
									color: "#000"
								},
								ticks: {
									min: 0,
									callback: function(value, index, values) {
										if (Math.floor(value) === value) {
											return value;
										}
									}
								}
                        }]
                    },
					legend: {
						display: false,
						position: 'top',
						labels: {
							position: 'right'
						}
					}
                }
			});
		
	}
	
	function appliedJobReportPopUp(){
		 var ctx = document.getElementById("jobApplicationsPopUpGraphViewPopup").getContext("2d");
            window.myBar = new Chart(ctx, {
                type: 'bar',
                data: appliedJobDataChartData,
                options: {
					responsive: true,
					maintainAspectRatio: false,
                    title:{
                        display:false,
                        text:"Evaluation Performance",
                        position:"top",
						fontColor: "Black"
						
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false
                    },
                    responsive: true,
                    scales: {
                        xAxes: [{
                            stacked: false,
							gridLines: {
									display: false,
									color: "#000"
								},
								ticks: {
									fontColor: "#000",
									autoSkip: false,
									maxRotation: 90,
									minRotation: 90,
									callback: function(value) {
										return value.substr(0, 10);//truncate
									}
								}
                        }],
                        yAxes: [{
                            stacked: false,
							gridLines: {
									display: true,
									color: "#000"
								},
								ticks: {
									min: 0,
									callback: function(value, index, values) {
										if (Math.floor(value) === value) {
											return value;
										}
									}
								}
                        }]
                    },
					legend: {
						display: false,
						position: 'top',
						labels: {
							position: 'right'
						}
					}
                }
			});
		
	}
	
	function applicantProfileByJob(){
		 var ctx = document.getElementById("applicantProfileByJobGraphView").getContext("2d");
            window.myBar = new Chart(ctx, {
                type: 'bar',
                data: applicantProfileByJobDataChartData,
                options: {
					responsive: true,
					maintainAspectRatio: false,
                    title:{
                        display:false,
                        text:"Evaluation Performance",
                        position:"top",
						fontColor: "Black"
						
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false
                    },
                    responsive: true,
                    scales: {
                        xAxes: [{
                            stacked: true,
							gridLines: {
									display: false,
									color: "#000"
								},
								ticks: {
									fontColor: "#000",
									autoSkip: false,
									maxRotation: 90,
									minRotation: 90,
									callback: function(value) {
										return value.substr(0, 10);//truncate
									}
								}
                        }],
                        yAxes: [{
                            stacked: true,
							gridLines: {
									display: true,
									color: "#000"
								},
								ticks: {
									min: 0,
									callback: function(value, index, values) {
										if (Math.floor(value) === value) {
											return value;
										}
									}
								}
                        }]
                    },
					legend: {
						display: false,
						position: 'top',
						labels: {
							position: 'right'
						}
					}
                }
			});
		
	}
	
	function applicantProfileByPopUp(){
		 var ctx = document.getElementById("applicantProfileByJobGraphViewPopup").getContext("2d");
            window.myBar = new Chart(ctx, {
                type: 'bar',
                data: applicantProfileByJobDataChartData,
                options: {
					responsive: true,
					maintainAspectRatio: false,
                    title:{
                        display:false,
                        text:"Evaluation Performance",
                        position:"top",
						fontColor: "Black"
						
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false
                    },
                    responsive: true,
                    scales: {
                        xAxes: [{
                            stacked: true,
							gridLines: {
									display: false,
									color: "#000"
								},
								ticks: {
									fontColor: "#000",
									autoSkip: false,
									maxRotation: 90,
									minRotation: 90,
									callback: function(value) {
										return value.substr(0, 10);//truncate
									}
								}
                        }],
                        yAxes: [{
                            stacked: true,
							gridLines: {
									display: true,
									color: "#000"
								},
								ticks: {
									min: 0,
									callback: function(value, index, values) {
										if (Math.floor(value) === value) {
											return value;
										}
									}
								}
                        }]
                    },
					legend: {
						display: false,
						position: 'top',
						labels: {
							position: 'right'
						}
					}
                }
			});
		
	}
	
	function applicansByJobRecruiter(){
		 var ctx = document.getElementById("applicansByJobRecruiterGraphView").getContext("2d");
            window.myBar = new Chart(ctx, {
                type: 'bar',
                data: applicansByJobRecruiterDataChartData,
                options: {
					responsive: true,
					maintainAspectRatio: false,
                    title:{
                        display:false,
                        text:"Evaluation Performance",
                        position:"top",
						fontColor: "Black"
						
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false
                    },
                    responsive: true,
                    scales: {
                        xAxes: [{
                            stacked: true,
							gridLines: {
									display: false,
									color: "#000"
								},
								ticks: {
									fontColor: "#000",
									autoSkip: false,
									maxRotation: 90,
									minRotation: 90,
									callback: function(value) {
										return value.substr(0, 10);//truncate
									}
								}
                        }],
                        yAxes: [{
                            stacked: true,
							gridLines: {
									display: true,
									color: "#000"
								},
								ticks: {
									min: 0,
									callback: function(value, index, values) {
										if (Math.floor(value) === value) {
											return value;
										}
									}
								}
                        }]
                    },
					legend: {
						display: false,
						position: 'top',
						labels: {
							position: 'right'
						}
					}
                }
			});
		
	}
	
	function applicansByJobRecruiterPopUp(){
		 var ctx = document.getElementById("applicansByJobRecruiterGraphViewPopup").getContext("2d");
            window.myBar = new Chart(ctx, {
                type: 'bar',
                data: applicansByJobRecruiterDataChartData,
                options: {
					responsive: true,
					maintainAspectRatio: false,
                    title:{
                        display:false,
                        text:"Evaluation Performance",
                        position:"top",
						fontColor: "Black"
						
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false
                    },
                    responsive: true,
                    scales: {
                        xAxes: [{
                            stacked: true,
							gridLines: {
									display: false,
									color: "#000"
								},
								ticks: {
									fontColor: "#000",
									autoSkip: false,
									maxRotation: 90,
									minRotation: 90,
									callback: function(value) {
										return value.substr(0, 10);//truncate
									}
								}
                        }],
                        yAxes: [{
                            stacked: true,
							gridLines: {
									display: true,
									color: "#000"
								},
								ticks: {
									min: 0,
									callback: function(value, index, values) {
										if (Math.floor(value) === value) {
											return value;
										}
									}
								}
                        }]
                    },
					legend: {
						display: false,
						position: 'top',
						labels: {
							position: 'right'
						}
					}
                }
			});
		
	}
	
	function recruitersStatus(){
		 var ctx = document.getElementById("recruitersStatusGraphView").getContext("2d");
            window.myBar = new Chart(ctx, {
                type: 'bar',
                data: recruitersStatusDataChartData,
                options: {
					responsive: true,
					maintainAspectRatio: false,
                    title:{
                        display:false,
                        text:"Evaluation Performance",
                        position:"top",
						fontColor: "Black"
						
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false
                    },
                    responsive: true,
                    scales: {
                        xAxes: [{
                            stacked: true,
							gridLines: {
									display: false,
									color: "#000"
								},
								ticks: {
									fontColor: "#000",
									autoSkip: false,
									maxRotation: 0,
									minRotation: 0,
									callback: function(value) {
										return value.substr(0, 10);//truncate
									}
								}
                        }],
                        yAxes: [{
                            stacked: false,
							gridLines: {
									display: true,
									color: "#000"
								},
								ticks: {
									min: 0,
									callback: function(value, index, values) {
										if (Math.floor(value) === value) {
											return value;
										}
									}
								}
                        }]
                    },
					legend: {
						display: false,
						position: 'top',
						labels: {
							position: 'right'
						}
					}
                }
			});
		
	}
	
	function recruitersStatusGraphViewPopup(){
		 var ctx = document.getElementById("recruitersStatusGraphViewPopup").getContext("2d");
            window.myBar = new Chart(ctx, {
                type: 'bar',
                data: recruitersStatusDataChartData,
                options: {
					responsive: true,
					maintainAspectRatio: false,
                    title:{
                        display:false,
                        text:"Evaluation Performance",
                        position:"top",
						fontColor: "Black"
						
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false
                    },
                    responsive: true,
                    scales: {
                        xAxes: [{
                            stacked: true,
							gridLines: {
									display: false,
									color: "#000"
								},
								ticks: {
									fontColor: "#000",
									autoSkip: false,
									maxRotation: 0,
									minRotation: 0,
									callback: function(value) {
										return value.substr(0, 10);//truncate
									}
								}
                        }],
                        yAxes: [{
                            stacked: false,
							gridLines: {
									display: true,
									color: "#000"
								},
								ticks: {
									min: 0,
									callback: function(value, index, values) {
										if (Math.floor(value) === value) {
											return value;
										}
									}
								}
                        }]
                    },
					legend: {
						display: false,
						position: 'top',
						labels: {
							position: 'right'
						}
					}
                }
			});
		
	}
</script>

<div id="page-content-wrapper"  style="margin-top: 67px;background:#eeeeee;">
    <div class="container-fluid whitebg">
        <div class="row">
            <h4 style="border-bottom: 23px; font-size: 20px; margin-bottom: 17px;">Analytics</h4>
        </div>
        <div class="row">
            <ul class="nav nav-tabs analytics-tab">
                <li class="active"><a data-toggle="tab" href="#jobAnalytics" style="font-size: 16px;">Jobs</a></li>
                <li><a data-toggle="tab" href="#applicationAnalytics" style="font-size: 16px;">Applications</a></li>
            </ul>
        </div>

        <div class="tab-content" style="border-left: 1px solid lightgray;border-right: 1px solid lightgray;border-bottom: 1px solid lightgray;">
            <div class="tab-pane active" id="jobAnalytics" style="overflow-y:scroll;">
                <div class="row" style="display:none;">
                    <div style="margin-top: 30px; margin-left: 27px; margin-bottom: 10px;">
                        <button type="button" class="btn btn-default" id="filterJobButton" style="width: 8%; text-align: left; font-size: 19px; border-radius: 10px; background-color: #ccc;">Filter<i class="fa fa-sort-desc" aria-hidden="true" style="font-size: 25px; padding-left: 68px;"></i></button>

                    </div>

                    <div class="clearfix"></div>
                    <div id="filterJobForm" style="margin-bottom: 40px;">
                        <div class="col-md-12">
                            <form class="form-vertical form_save" action="" method="post" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label for="inputEmail" class="control-label col-md-1 required-field" style="width: auto;">From</label>
                                    <div class="col-md-2" style="width: 9%;">
                                        <input type="text" class="form-control" placeholder="From Date" id="datepicker-13" value="<?php
                                            if (isset($_POST['fromDate'])): echo $_POST['fromDate'];
                                            endif;
                                            ?>" name="fromDate">
                                    </div>
                                </div> 
                                <div class="form-group">
                                    <label for="inputEmail" class="control-label col-md-1 required-field" style="width: auto;">To</label>
                                    <div class="col-md-2" style="width: 9%;">
                                        <input type="text" class="form-control" placeholder="To Date" id="datepicker-14" value="<?php
                                            if (isset($_POST['toDate'])): echo $_POST['toDate'];
                                            endif;
                                            ?>" name="toDate">
                                    </div>
                                </div>                        
                                <div class="col-md-4">
                                    <input type="hidden" name="accessToken" value="<?php echo $token; ?>"/>
                                    <button type="submit" class="btn btn-default">Search</button>
                                    <button type="reset" class="btn btn-default">Reset</button>
                                </div>
                            </form>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="row">
        			<div class="col-md-6">
                        <div class="col-md-12">
                            <div class="row" style="background-color: #054F72; color: #fff; font-size: 19px; margin: 13px 13px;">
                                <div class="col-md-6 pull-left" title="No of applicants through other sources corresponding to applicants invited via VDOhire" style="padding: 5px 5px;    font-size: 16px;">Job Status</div>
								
                                <div class="col-md-6 pull-right" style="padding: 5px 5px;">
                                    <i class="fa fa-download" aria-hidden="true" style="float: right; margin-left: 15px; padding-top: 3px;cursor:pointer;"   onclick="exportTableToCSV('JobStatusReport.csv','jobStatusTableReporting')" title="Please click here to download CSV file"></i>
                                    <i class="fa fa-table" aria-hidden="true" style="float: right; margin-left: 15px; padding-top: 3px;cursor:pointer;"  onclick="ChangeView('JobStatusTabularView','jobStatusGraphViewCanvas')" title="Please click here to view table"></i>
                                    <i class="fa fa-bar-chart" aria-hidden="true" style="float: right; margin-left: 15px; padding-top: 3px;cursor:pointer;"  onclick="ChangeView('jobStatusGraphViewCanvas','JobStatusTabularView')" title="Please click here to view chart"></i>
                                </div>
                            </div>
							<?php
							//echo "<pre>";print_r($jobStatusAnalyticsData);die;
							?>
            				<div class="row" style="margin: 13px 13px;">
								<div class="displayJobStatus" style="">
									<div class="tabularViewContent" id="JobStatusTabularView"  style="display:none;overflow-y: scroll;">
										<table  class="table sortable view-table" data-options='{"table":"JobStatusTabularViewPopUp", "chart":"jobStatusGraphViewPopupParent", "chartType":"jobStatusPopUp"}'   data-toggle="modal" data-target="#jobStatusPopup"   id="jobStatusTableReporting" style="">
												<thead class="tbl_head">
													<tr>
														<th>S.No</th>
														<th>Job Title</th>
														<th>Created On</th>
														<th>Close date</th>
														<th>Posted</th>
														<th>Invited</th>
														<th>Total Invitation Responded</th>
														<th>Uninvited responses</th>
														<th>Created by</th>
														<th>Recruiters</th>
														
														
													</tr>
												</thead>
											<?php
											$i =0;
											$totalJob = 0;
											foreach($jobStatusAnalyticsData as $jobs){
												$i++;
												
												$hasData = $jobs['totalResponded']+$jobs['totalinvitationresponded'];
												if($hasData > 0){
													$totalJob = $totalJob+1;
												}
												?>
												<tr>
													<td><?php echo $i ?></td>
													<td><?php echo $jobs['title'] ?></td>
													<td><?php echo date('d-m-Y', strtotime($jobs['createdAt'])) ; ?></td>
													<td><?php echo ($jobs['closedDate'] != "")?$jobs['closedDate']:"N/A" ?></td>
													<td><?php echo ($jobs['posted'] == 1)?"Yes":"No"; ?></td>
													<td><?php echo $jobs['invited'] ?></td>
													<td><?php echo $jobs['totalinvitationresponded'] ?></td>
													<td><?php echo $jobs['totalResponded']-$jobs['totalinvitationresponded'] ?></td>
													<td><?php echo $jobs['fullname'] ?></td>
													<td><?php echo ($jobs['recruiters'] != "")?$jobs['recruiters']:"N/A"; ?></td>
													
													
													
												</tr>
											<?php }

											?>
										</table>
										<input type="hidden" name="totalJobStatusCount" id="totalJobStatusCount" value="<?php echo $totalJob ?>">
									</div>
									<div class="displayJobStatusCanvas" id="jobStatusGraphViewCanvas" style="">
										
										<div class="canvasWidth" style="">
											<canvas  data-toggle="modal" data-target="#jobStatusPopup"  class="popupJobStatus view-graph" data-options='{"table":"JobStatusTabularViewPopUp", "chart":"jobStatusGraphViewPopupParent", "chartType":"jobStatusPopUp"}' id="jobStatusGraphView" style="display:block;" ></canvas>
											
										</div>
										<div class="legends" style="font-size:10px;">
												<div class="pull-left  col-md-3">
													<div class="pull-left" style="padding-top: 1%;">
															<div style="width:10px;height:10px;background:rgb(39,172,191);"></div>
													</div>
													<div class="pull-left" style="    padding-left: 3px;">Other Applications</div>
													<div style="clear:both"></div>
												</div>
												<div class="pull-left  col-md-3">
													<div class="pull-left" style="padding-top:1%;">
														<div style="width:10px;height:10px;background:rgb(140,46,10);"></div>
													</div>
													<div  class="pull-left"  style="    padding-left: 3px;">Invited Applications</div>
													<div style="clear:both"></div>
												</div>
												<div style="clear:both"></div>
											</div>
									</div>
								</div>
            				</div>
                        </div>
        			</div>
        			
					<!-- JOb Report 2 -->
					
        			<div class="col-md-6" align="center" >
        				<div class="col-md-12">
                            <div class="row" style="background-color: #054F72; color: #fff; font-size: 19px; margin: 13px 13px;">
                                <div class="col-md-6 pull-left" title="Total applications received at level 1 and level 2 corresponding to applications pending for actions" style="padding: 5px 5px;    text-align: left;    font-size: 16px;">Recruiter by Job</div>
                                <div class="col-md-6 pull-right" style="padding: 5px 5px;">
                                    <i class="fa fa-download" aria-hidden="true" style="float: right; padding-left: 15px; padding-top: 3px;cursor:pointer;"   onclick="exportTableToCSV('RecruiterByJobReport.csv','pendingApplicationTableReporting')" title="Please click here to download CSV file"></i>
                                    <i class="fa fa-table" aria-hidden="true" style="float: right; padding-left: 15px; padding-top: 3px;cursor:pointer;"  onclick="ChangeView('PendingApplicationTabularView','PendingApplicationCanvas')" title="Please click here to view table"></i>
                                    <i class="fa fa-bar-chart" aria-hidden="true" style="float: right; padding-left: 15px; padding-top: 3px;cursor:pointer;"  onclick="ChangeView('PendingApplicationCanvas','PendingApplicationTabularView')" title="Please click here to view chart"></i>
                                </div>
                            </div>
            				<div class="row" style="margin: 13px 13px;">
								<div class="displayJobStatus" style="">
									<div  class="tabularViewContent" id="PendingApplicationTabularView"  style="display:none;overflow-y: scroll;">
										<table  data-toggle="modal" data-target="#PendingApplicationPopup"  class="table sortable view-table" data-options='{"table":"PendingApplicationTabularViewPopUp", "chart":"PendingApplicationGraphViewPopupParent", "chartType":"pendingApplication"}' id="pendingApplicationTableReporting" >
											<thead class="tbl_head">
												
												<tr>
													<th>Recruiters</th>
													<?php
													$allJobArray = array();
													foreach($jobPendingApplicationData as $recruiterJob){
														if($recruiterJob['l1recruiters'] == "" && $recruiterJob['l2recruitersId'] == ""){
															
														}else{
															echo "<th>".$recruiterJob['title']."</th>";
															$allJobArray[] = $recruiterJob['id'];
														}
													}
													
													?>
													
													
												</tr>
											</thead>
											<?php
											$i =0;
											foreach($jobPendingApplicationData as $pendingApplications){
												
												if($pendingApplications['l1recruitersId'] != "" ){
													$recruiters[$pendingApplications['l1recruitersId']]['name'] = $pendingApplications['l1recruiters'];
													
													$recruiters[$pendingApplications['l1recruitersId']]['jobs'][$pendingApplications['id']] = array(
																	'jobId' => $pendingApplications['id'],
																	'jobTitle' => $pendingApplications['title'],
																	'totalReceived' => $pendingApplications['totalApplicationReceivedAtL1'],
																	'totalShortlisted' => $pendingApplications['totalShortlistedApplicationAtL1'],
																	'totalPending' => $pendingApplications['totalPendingApplicationAtL1'],
																	'totalRejected' => $pendingApplications['totalRejectedApplicationAtL1'],
																	'RecruiterType' => "1"
													);
												}
												
												if($pendingApplications['l2recruitersId'] != "" ){
													$recruiters[$pendingApplications['l2recruitersId']]['name'] = $pendingApplications['l2recruiters'];
													
													$recruiters[$pendingApplications['l2recruitersId']]['jobs'][$pendingApplications['id']] = array(
																	
																	'jobId' => $pendingApplications['id'],
																	'jobTitle' => $pendingApplications['title'],
																	'totalReceived' => $pendingApplications['totalApplicationReceivedAtL2'],
																	'totalShortlisted' => $pendingApplications['totalShortlistedApplicationAtL2'],
																	'totalPending' => $pendingApplications['totalPendingApplicationAtL2'],
																	'totalRejected' => $pendingApplications['totalRejectedApplicationAtL2'],
																	'RecruiterType' => "2"
													);
												}
											} ?>
											
											<?php 
											$NewrecruiterArray = array();
											foreach($recruiters as $key => $rec){
												$NewrecruiterArray[$key]['name'] = $rec['name'];
												foreach($allJobArray as $job){
													if(array_key_exists($job,$rec['jobs'])){
														$NewrecruiterArray[$key]['jobs'][$job] = $rec['jobs'][$job];
													}else{
														$NewrecruiterArray[$key]['jobs'][$job] = array();
													}
												}
											}
											//print '<pre>';print_r($recruiters);exit;
											foreach($NewrecruiterArray as $recruiter){
												
												
													
												?>
												<tr><td><?php echo $recruiter['name'] ?></td>
													<?php
													$initialPos = 0;
													foreach($recruiter['jobs'] as $job){ 
														if(empty($job)){
															echo "<td></td>";
														}else{
														?>
														<td><?php echo "L".$job['RecruiterType']."|".$job['totalReceived']."|".$job['totalPending']."|".$job['totalShortlisted']."|".$job['totalRejected'] ?></td>
														<?php
														}
														
														
														?>
														
													<?php }
													
													
													?>
												
												
												
												</tr>
												
												
												<?php 
												
											}
											?>
											
										</table>
									</div>
									<div class="displayJobStatusCanvas" id="PendingApplicationCanvas"  style="">
										
										
										<div class="canvasWidth" style="">
											<canvas  data-toggle="modal" data-target="#PendingApplicationPopup" class="popupJobStatus view-graph" data-options='{"table":"PendingApplicationTabularViewPopUp", "chart":"PendingApplicationGraphViewPopupParent", "chartType":"pendingApplication"}'  id="PendingApplication" style="display:block;"></canvas>
											
											<div class="legends" style="font-size:10px;margin-bottom:10px;">
												<div class="pull-left  col-md-2" style="padding-left:0px;width:14%;">
													<div class="pull-left" style="padding-top: 1%;">
															<div style="width:10px;height:10px;background:rgb(39,172,191);"></div>
													</div>
													<div class="pull-left" style="    padding-left: 3px;width:90%;text-align: left;">Pending Applications At L1</div>
													<div style="clear:both"></div>
												</div>
												<div class="pull-left  col-md-2" style="padding-left:0px;width:14%;">
													<div class="pull-left" style="padding-top:1%;">
														<div style="width:10px;height:10px;background:rgb(174,189,56);"></div>
													</div>
													<div  class="pull-left"  style="    padding-left: 3px;width:90%;text-align: left;">Shortlisted Applications At L1</div>
													<div style="clear:both"></div>
												</div>
												<div class="pull-left  col-md-2" style="padding-left:0px;width:14%;">
													<div class="pull-left" style="padding-top: 1%;">
														<div style="width:10px;height:10px;background:rgb(238,110,61);"></div>
													</div>
													<div  class="pull-left"  style="    padding-left: 3px;width:90%;text-align: left;">Rejected Applications At L2</div>
													<div style="clear:both"></div>
												</div>
												<div class="pull-left  col-md-2" style="padding-left:0px;width:14%;">
													<div class="pull-left" style="padding-top: 1%;">
														<div style="width:10px;height:10px;background:rgb(35,98,170);"></div>
													</div>
													<div  class="pull-left"  style="    padding-left: 3px;width:90%;text-align: left;">Pending Applications At L2</div>
													<div style="clear:both"></div>
												</div>
												<div class="pull-left  col-md-2" style="padding-left:0px;width:14%;">
													<div class="pull-left" style="padding-top: 1%;">
														<div style="width:10px;height:10px;background:rgb(111,123,11);"></div>
													</div>
													<div  class="pull-left"  style="    padding-left: 3px;width:90%;text-align: left;">Shortlisted Applications At L2</div>
													<div style="clear:both"></div>
												</div>
												<div class="pull-left  col-md-2" style="padding-left:0px;width:14%;">
													<div class="pull-left" style="padding-top: 1%;">
														<div style="width:10px;height:10px;background:rgb(177,36,36);"></div>
													</div>
													<div  class="pull-left"  style="    padding-left: 3px;width:90%;text-align: left;">Rejected Applications At L2</div>
													<div style="clear:both"></div>
												</div>
												<div style="clear:both"></div>
											</div>
										</div>
									</div>
								</div>
            				</div>
                        </div>
        			</div>
        			
        			<div class="col-md-6" align="center" >
        				<div class="col-md-12">
                            <div class="row" style="background-color: #054F72; color: #fff; font-size: 19px; margin: 13px 13px;">
                                <div class="col-md-6 pull-left" title="Total no of vacancies per job and total applications received corresponding to that job" style="padding: 5px 5px;text-align: left;font-size: 16px;">Jobs - Vacancies vs Application</div>
                                <div class="col-md-6 pull-right" style="padding: 5px 5px;">
                                    <i class="fa fa-download" aria-hidden="true" style="float: right; padding-left: 15px; padding-top: 3px;cursor:pointer;"   onclick="exportTableToCSV('JobVacanciesVsApplicationReport.csv','jobFullfillmentDataTableReporting')" title="Please click here to download CSV file"></i>
                                    <i class="fa fa-table" aria-hidden="true" style="float: right; padding-left: 15px; padding-top: 3px;cursor:pointer;"  onclick="ChangeView('jobFullfillmentDataTabularView','jobFullfillmentCanvas')" title="Please click here to view table"></i>
                                    <i class="fa fa-bar-chart" aria-hidden="true" style="float: right; padding-left: 15px; padding-top: 3px;cursor:pointer;"  onclick="ChangeView('jobFullfillmentCanvas','jobFullfillmentDataTabularView')" title="Please click here to view chart"></i>
                                </div>
                            </div>
							<?php  ?>
            				<div class="row" style="margin: 13px 13px;">
								<div class="displayJobStatus" style="">
									<div  class="tabularViewContent"  id="jobFullfillmentDataTabularView"  style="display:none;overflow-y: scroll;">
										<table  data-toggle="modal" data-target="#jobFullfillmentpopUp"  class="table sortable view-table" data-options='{"table":"jobFullfillmentTabularViewPopUp", "chart":"jobFullfillmentGraphViewPopupParent", "chartType":"jobFullfillment"}'  id="jobFullfillmentDataTableReporting" >
															<thead class="tbl_head">
																<tr>
																	<th>S.No</th>
																	<th>Job Title</th>
																	<th>Created On</th>
																	<th>Closed Date</th>
																	<th>Posted</th>
																	<th>Vacancies</th>
																	<th>Number of Applications</th>
																</tr>
															</thead>
										<?php
										$i =0;
										foreach($jobFullfillmentData as $fullFillmentJob){
											$i++;
											?>
											<tr>
												<td><?php echo $i ?></td>
												<td><?php echo $fullFillmentJob['title'] ?></td>
												<td><?php echo $fullFillmentJob['createdAt'] ?></td>
												<td><?php echo ($fullFillmentJob['closedDate'] !="")?$fullFillmentJob['closedDate']:"NA"; ?></td>
												<td><?php echo ($fullFillmentJob['posted'] == 1)?"Yes":"No" ?></td>
												<td><?php echo $fullFillmentJob['noOfVacancies'] ?></td>
												<td><?php echo $fullFillmentJob['totalApplications'] ?></td>
												
											</tr>
										<?php }

										?>
										</table>
									</div>
									<div class="displayJobStatusCanvas" id="jobFullfillmentCanvas" style="">
										
									
										<div class="canvasWidth" style="">
											<canvas  data-toggle="modal" data-target="#jobFullfillmentpopUp"  class="view-graph" data-options='{"table":"jobFullfillmentTabularViewPopUp", "chart":"jobFullfillmentGraphViewPopupParent", "chartType":"jobFullfillment"}' id="jobFullfillment" style="display:block;"></canvas>
											<div class="legends" style="font-size:10px;">
												<div class="pull-left  col-md-3" style="width:10%;">
													<div class="pull-left" style="padding-top:1%;">
															<div style="width:10px;height:10px;background:rgb(39,172,191);"></div>
													</div>
													<div class="pull-left" style="    padding-left: 3px;width:71%;text-align:left;">No Of Vacancies</div>
													<div style="clear:both"></div>
												</div>
												<div class="pull-left  col-md-3" style="width:10%;">
													<div class="pull-left" style="padding-top: 1%;">
														<div style="width:10px;height:10px;background:rgb(140,46,10);"></div>
													</div>
													<div  class="pull-left"  style="    padding-left: 3px;width:71%;text-align:left;">Total Applications</div>
													<div style="clear:both"></div>
												</div>
												<div style="clear:both"></div>
											</div>
										</div>
									</div>
								</div>
            				</div>
                        </div>
        			</div>
        			<div class="col-md-6">
                        <div class="col-md-12">
                            <div class="row" style="background-color: #054F72; color: #fff; font-size: 19px; margin: 13px 13px;">
                                <div class="col-md-6 pull-left" title="No of invited candidates via VDOHire corresponding to applications received through invitations sent" style="padding: 5px 5px;font-size: 16px;">Candidates Invited</div>
                                <div class="col-md-6 pull-right" style="padding: 5px 5px;">
                                    <i class="fa fa-download" aria-hidden="true" style="float: right; margin-left: 15px; padding-top: 3px;cursor:pointer;"   onclick="exportTableToCSV('CandidatesInvitedReport.csv','jobInvitationsTableReporting')" title="Please click here to download CSV file"></i>
                                    <i class="fa fa-table" aria-hidden="true" style="float: right; margin-left: 15px; padding-top: 3px;cursor:pointer;"  onclick="ChangeView('jobInvitationsTabularView','jobInvitationsGraphViewCanvas')" title="Please click here to view table"></i>
                                    <i class="fa fa-bar-chart" aria-hidden="true" style="float: right; margin-left: 15px; padding-top: 3px;cursor:pointer;"  onclick="ChangeView('jobInvitationsGraphViewCanvas','jobInvitationsTabularView')" title="Please click here to view chart"></i>
                                </div>
                            </div>
							<?php //echo "<pre>";print_r($jobInvitationData); ?>
            				<div class="row" style="margin: 13px 13px;">
								<div class="displayJobStatus" style="">
									<div class="tabularViewContent"  id="jobInvitationsTabularView"  style="display:none;overflow-y: scroll;">
										<table  data-toggle="modal" data-target="#jobInvitationsGraphViewPopUp"    class="table sortable view-table" data-options='{"table":"jobInvitationsTabularViewPopUp", "chart":"jobInvitationsGraphViewPopupParent", "chartType":"jobInvitations"}'  id="jobInvitationsTableReporting" >
												<thead class="tbl_head">
													<tr>
														<th>S.No</th>
														<th>Name</th>
														<th>Job Title</th>
														<th>Job Code</th>
														<th>Date invited</th>
														<th>Applied on</th>
														<th>Assessment Score</th>
														<th>Feedback Score</th>
														<th>L1 Result</th>
														<th>L2 Result</th>
													</tr>
												</thead>
											<?php
											$i =0;
											$totalJob = 0;
											
											foreach($jobInvitationData as $dataInvitationCount){
												$i++;
												//Feedback Calculations
												//echo "<pre>";print_r($jobInvitationData);
												$feedbacks = $ci->userjob_model->interviewComment($dataInvitationCount['userJobId']);
												$adminfeedbacks = $ci->userjob_model->adminComment($dataInvitationCount['userJobId']);
												if(!empty($feedbacks) && !empty($adminfeedbacks)){
													$feedbacksAll = array_merge($feedbacks,$adminfeedbacks);
												}else if(!empty($adminfeedbacks)){
													$feedbacksAll = $adminfeedbacks;
												}else if(!empty($feedbacks)){
													$feedbacksAll = $feedbacks;
												}else{
													$feedbacksAll = array();
												}
												
												
												$totalFeedBack = 0;
												$totalCountFeedback = 0;
												if(!empty($feedbacksAll)){
													foreach($feedbacksAll as $userFeedback){
														$totalFeedBack 		= $totalFeedBack + $userFeedback['rating'];
														$totalCountFeedback++;
													}
													$userFeedbackCal = round($totalFeedBack/$totalCountFeedback, 0);
												}else{
													$userFeedbackCal = "N/A";
												}
												
												
												//Calculation For Assessment
												$jobApplicationDetailForFilter   = $this->job_model->userAssessmentDetail_v2($dataInvitationCount['inviteeId'], $dataInvitationCount['id']);
												
												if(!empty($jobApplicationDetailForFilter)){
													foreach($jobApplicationDetailForFilter as $assessmentResult){
														if($assessmentResult['userAnswer'] == $assessmentResult['questionAnswerId']){
															$correctAssessmentAns++;
														}
														$totalAssessmentResult++;
													}
													$assessmentScore = ($correctAssessmentAns/$totalAssessmentResult)*100;
												}else{
													$assessmentScore = "N/A";
												}
												
												if($dataInvitationCount['status'] == 2 || $dataInvitationCount['status'] == 4){
													if($dataInvitationCount['status'] == 2){
														$assessmentL1 = "Shortlisted";
														$assessmentL2 = "Pending";
													}else{
														$assessmentL1 = "Shortlisted";
														$assessmentL2 = "Shortlisted";
													}
												}else if($dataInvitationCount['status'] == 3 || $dataInvitationCount['status'] == 5){
													if($dataInvitationCount['status'] == 3){
														$assessmentL1 = "Rejected";
														$assessmentL2 = "NA";
													}else{
														$assessmentL1 = "Rejected";
														$assessmentL2 = "Rejected";
													}
												}else{
													$assessmentL1 = "NA";
													$assessmentL2 = "NA";
												}
												
												?>
												<tr>
													<td><?php echo $i ?></td>
													<td><?php echo $dataInvitationCount['inviteeName'] ?></td>
													<td><?php echo $dataInvitationCount['jobName'] ?></td>
													<td><?php echo $dataInvitationCount['jobCode'] ?></td>
													<td><?php echo $dataInvitationCount['invitationDate'] ?></td>
													<td><?php echo $dataInvitationCount['appliedOn'] ?></td>
													<td><?php echo round($assessmentScore) ?></td>
													<td><?php echo $userFeedbackCal ?></td>
													<td><?php echo $assessmentL1 ?></td>
													<td><?php echo $assessmentL2 ?></td>
													
												</tr>
											<?php }

											?>
										</table>
										<input type="hidden" name="totalJobStatusCount" id="totalJobStatusCount" value="<?php echo $totalJob ?>">
									</div>
									<div class="displayJobStatusCanvas" id="jobInvitationsGraphViewCanvas" style="">
										
									
										<div class="canvasWidth" style="">
											<canvas  data-toggle="modal" data-target="#jobInvitationsGraphViewPopUp"  class="popupjobInvitations view-graph" data-options='{"table":"jobInvitationsTabularViewPopUp", "chart":"jobInvitationsGraphViewPopupParent", "chartType":"jobInvitations"}'    id="jobInvitationsGraphView" style="display:block;" ></canvas>
											<div class="legends" style="font-size:10px;">
												<div class="pull-left  col-md-3" style="width:10%">
													<div class="pull-left" style="padding-top: 1%;">
															<div style="width:10px;height:10px;background:rgb(39,172,191);"></div>
													</div>
													<div class="pull-left" style="    padding-left: 3px;width:71%;">Invitation Count</div>
													<div style="clear:both"></div>
												</div>
												<div class="pull-left  col-md-3" style="width:10%">
													<div class="pull-left" style="padding-top:1%;">
														<div style="width:10px;height:10px;background:rgb(140,46,10);"></div>
													</div>
													<div  class="pull-left"  style="    padding-left: 3px;width:89%;">Invited Applications</div>
													<div style="clear:both"></div>
												</div>
												<div style="clear:both"></div>
											</div>

										</div>
									</div>
								</div>
							</div>
						</div>
        			</div>
        			


                    <div class="clearfix"></div>
				</div>
			</div>
			<div class="tab-pane"  id="applicationAnalytics" style="overflow-y:scroll;">
				<div class="row">
					<div class="col-md-6" align="center" >
        				<div class="col-md-12">
                            <div class="row" style="background-color: #054F72; color: #fff; font-size: 19px; margin: 13px 13px;">
                                <div class="col-md-6 pull-left" title="No of applicants being viewed and corresponding action against their applications" style="padding: 5px 5px;text-align: left;font-size: 16px;">Applicants by Job</div>
                                <div class="col-md-6 pull-right" style="padding: 5px 5px;">
                                    <i class="fa fa-download" aria-hidden="true" style="float: right; padding-left: 15px; padding-top: 3px;cursor:pointer;"   onclick="exportTableToCSV('ApplicantsByJobReport.csv','jobApplicationsTableReporting')" title="Please click here to download CSV file"></i>
                                    <i class="fa fa-table" aria-hidden="true" style="float: right; padding-left: 15px; padding-top: 3px;cursor:pointer;"  onclick="ChangeView('jobApplicationsTabularView','jobApplicationsGraphViewCanvas')" title="Please click here to view table"></i>
                                    <i class="fa fa-bar-chart" aria-hidden="true" style="float: right; padding-left: 15px; padding-top: 3px;cursor:pointer;"  onclick="ChangeView('jobApplicationsGraphViewCanvas','jobApplicationsTabularView')" title="Please click here to view chart"></i>
                                </div>
                            </div>
							
            				<div class="row" style="margin: 13px 13px;">
								<div class="displayJobStatus" style="">
									<div class="tabularViewContent"  id="jobApplicationsTabularView"  style="display:none;overflow-y: scroll;">
										<table   data-toggle="modal" data-target="#jobApplicationsGraphViewPopup"   class="table sortable view-table" data-options='{"table":"jobApplicationsTabularViewPopUp", "chart":"jobApplicationsGraphViewPopupParent", "chartType":"jobApplications"}' id="jobApplicationsTableReporting" >
												<thead class="tbl_head">
													<tr>
														<th>S.No</th>
														<th>Job Title</th>
														<th>Name</th>
														<th>Job Code</th>
														<th>Date applied</th>
														<th>Source</th>
														<th>Assessment</th>
														<th>Feedback</th>
														<th>L1 Result</th>
														<th>L2 Result</th>
														<th>L1 Viewed Date</th>
														<th>L2 Viewed Date</th>
														
														<th>Shortlisted Date</th>
													</tr>
												</thead>
											<?php
											$i =0;
											$totalJob = 0;
											foreach($applicantsByJob as $appliedJob){
												$i++;
												//Feedback Calculations
												//echo "<pre>";print_r($jobInvitationData);
												$feedbacks = $ci->userjob_model->interviewComment($appliedJob['userJobId']);
												$adminfeedbacks = $ci->userjob_model->adminComment($appliedJob['userJobId']);
												if(!empty($feedbacks) && !empty($adminfeedbacks)){
													$feedbacksAll = array_merge($feedbacks,$adminfeedbacks);
												}else if(!empty($adminfeedbacks)){
													$feedbacksAll = $adminfeedbacks;
												}else if(!empty($feedbacks)){
													$feedbacksAll = $feedbacks;
												}else{
													$feedbacksAll = array();
												}
												
												
												$totalFeedBack = 0;
												$totalCountFeedback = 0;
												if(!empty($feedbacksAll)){
													foreach($feedbacksAll as $userFeedback){
														$totalFeedBack 		= $totalFeedBack + $userFeedback['rating'];
														$totalCountFeedback++;
													}
													$userFeedbackCal = round($totalFeedBack/$totalCountFeedback, 0);
												}else{
													$userFeedbackCal = "N/A";
												}
												
												
												
												//Calculation For Assessment
												$jobApplicationDetailForFilter   = $this->job_model->userAssessmentDetail_v2($appliedJob['candidateId'], $appliedJob['id']);
												
												if(!empty($jobApplicationDetailForFilter)){
													foreach($jobApplicationDetailForFilter as $assessmentResult){
														if($assessmentResult['userAnswer'] == $assessmentResult['questionAnswerId']){
															$correctAssessmentAns++;
														}
														$totalAssessmentResult++;
													}
													$assessmentScore = ($correctAssessmentAns/$totalAssessmentResult)*100;
												}else{
													$assessmentScore = "N/A";
												}
												$dateShortlisted = "NA";
												//shortlisted
												if($appliedJob['shorlistedByL1UserDate'] != "" || $appliedJob['shorlistedByL2UserDate'] != ""){
													
													$shortlistedL1 = "Shortlisted";
													$shortlistedL2 = "Shortlisted";
													$dateShortlisted = $appliedJob['shorlistedByL2UserDate'];
													
												}if($appliedJob['shorlistedByL1UserDate'] != "" || $appliedJob['shorlistedByL2UserDate'] == ""){
													
													$shortlistedL1 = "Shortlisted";
													$shortlistedL2 = "Pending";
													$dateShortlisted = $appliedJob['shorlistedByL1UserDate'];
													
												}else{
													$shortlistedL1 = "Pending";
													$shortlistedL2 = "Pending";
												}
												
												//Date Viewed
												
												if(trim($appliedJob['L1ViewedDate']) != "" || trim($appliedJob['L2ViewedDate']) != ""){
													
													$L1ViewedDate = $appliedJob['L1ViewedDate'];
													$L2ViewedDate = $appliedJob['L2ViewedDate'];
													
												}if(trim($appliedJob['L1ViewedDate']) != "" || trim($appliedJob['L2ViewedDate']) == ""){
													
													$L1ViewedDate = $appliedJob['L1ViewedDate'];
													$L2ViewedDate = "NA";
													
												}else{
													$L1ViewedDate = "NA";
													$L2ViewedDate = "NA";
												}
												?>
												<tr>
													<td><?php echo $i ?></td>
													<td><?php echo $appliedJob['jobName'] ?></td>
													<td><?php echo $appliedJob['fullname'] ?></td>
													<td><?php echo $appliedJob['jobCode'] ?></td>
													<td><?php echo $appliedJob['appliedDate'] ?></td>
													<td><?php echo ($appliedJob['applicationSource'] == 0)?"FJ database":"Invited" ?></td>
													<td><?php echo round($assessmentScore) ?></td>
													<td><?php echo round($userFeedbackCal) ?></td>
													<td><?php echo $shortlistedL1 ?></td>
													<td><?php echo $shortlistedL2 ?></td>
													<td><?php echo ($L1ViewedDate != "")?$L1ViewedDate:"NA"; ?></td>
													<td><?php echo $L2ViewedDate ?></td>
													
													<td><?php echo ($dateShortlisted != "")?$dateShortlisted:"NA"; ?></td>
													
												</tr>
											<?php }

											?>
										</table>
									</div>
									<div class="displayJobStatusCanvas" id="jobApplicationsGraphViewCanvas" style="">
										
										
										<div class="canvasWidth" style="min-height:200px;">
											<canvas  data-toggle="modal" data-target="#jobApplicationsGraphViewPopup" class="view-graph" data-options='{"table":"jobApplicationsTabularViewPopUp", "chart":"jobApplicationsGraphViewPopupParent", "chartType":"jobApplications"}' id="jobApplicationsGraphView" style="display:block;"></canvas>
											<div class="legends"  style="font-size:10px;">
												<div class="pull-left  col-md-3" style="width:10%">
													<div class="pull-left" style="padding-top: 1%;">
															<div style="width:10px;height:10px;background:rgb(39,172,191);"></div>
													</div>
													<div class="pull-left" style="    padding-left: 3px;width:71%;text-align:left;">Shortlisted Count</div>
													<div style="clear:both"></div>
												</div>
												<div class="pull-left  col-md-3" style="width:10%">
													<div class="pull-left" style="padding-top:1%;">
														<div style="width:10px;height:10px;background:rgb(140,46,10);"></div>
													</div>
													<div  class="pull-left"  style="    padding-left: 3px;width:89%;text-align:left;">Viewed Count</div>
													<div style="clear:both"></div>
												</div>
												<div style="clear:both"></div>
											</div>
										</div>
									</div>
								</div>
            				</div>
                        </div>
        			</div>
					
					<div class="col-md-6" align="center" >
        				<div class="col-md-12">
                            <div class="row" style="background-color: #054F72; color: #fff; font-size: 19px; margin: 13px 13px;">
                                <div class="col-md-6 pull-left" title="Total no of applications shortlisted and rejected by Level 1 and Level 2 recruiter" style="padding: 5px 5px;    text-align: left;font-size: 16px;">Recruiter Activity Report</div>
                                <div class="col-md-6 pull-right" style="padding: 5px 5px;">
                                    <i class="fa fa-download" aria-hidden="true" style="float: right; padding-left: 15px; padding-top: 3px;cursor:pointer;"   onclick="exportTableToCSV('RecruiterActivityReport.csv','applicansByJobRecruiterTableReporting')" title="Please click here to download CSV file"></i>
                                    <i class="fa fa-table" aria-hidden="true" style="float: right; padding-left: 15px; padding-top: 3px;cursor:pointer;"  onclick="ChangeView('recruitersStatusTabularView','recruitersStatusGraphViewCanvas')" title="Please click here to view table"></i>
                                    <i class="fa fa-bar-chart" aria-hidden="true" style="float: right; padding-left: 15px; padding-top: 3px;cursor:pointer;"  onclick="ChangeView('recruitersStatusGraphViewCanvas','recruitersStatusTabularView')" title="Please click here to view chart"></i>
                                </div>
                            </div>
							
            				<div class="row" style="margin: 13px 13px;">
								<div class="displayJobStatus" style="">
									<div class="tabularViewContent"  id="recruitersStatusTabularView"  style="display:none;overflow-y: scroll;">
										<table  data-toggle="modal" data-target="#recruitersStatusPopup"  class="table sortable view-table" data-options='{"table":"recruitersStatusTabularViewPopUp", "chart":"recruitersStatusGraphViewPopupParent", "chartType":"recruitersStatus"}' id="recruitersStatusTableReporting" >
												<thead class="tbl_head">
													<tr>
														<th>S.No</th>
														<th>Recruiter Name</th>
														<th>No Of Jobs Created</th>
														<th>Total L1 Queue Evaluated</th>
														<th>Total L1 Queue Evaluated Shortlisted</th>
														<th>Total L1 Queue Evaluated Rejected</th>
														<th>Total L1 Queue Pending</th>
														<th>Total L2 Queue Evaluated</th>
														<th>Total L2 Queue Evaluated Rejected</th>
														<th>Total L2 Queue Evaluated Rejected</th>
														
														<th>Total L2 Queue Pending</th>
														<th>Total Assessment Created</th>
														<th>Total Interview Created</th>
														<th>Email feedback sought</th>
													</tr>
												</thead>
											<?php
											$i =0;
											$totalJob = 0;
											foreach($recruitersStatus as $recruiters){
												$i++;
												
												?>
												<tr>
													<td><?php echo $i ?></td>
													<td><?php echo $recruiters['recruiterName'] ?></td>
													<td><?php echo $recruiters['noOfJobsCreated'] ?></td>
													<td><?php echo $recruiters['totalL1QueueEveluated'] ?></td>
													<td><?php echo $recruiters['totalL1QueueEveluatedYes'] ?></td>
													<td><?php echo $recruiters['totalL1QueueEveluatedNo'] ?></td>
													<td><?php echo $recruiters['totalL1QueuePending'] ?></td>
													<td><?php echo $recruiters['totalL2QueueEveluated'] ?></td>
													<td><?php echo $recruiters['totalL2QueueEveluatedYes'] ?></td>
													<td><?php echo $recruiters['totalL2QueueEveluatedNo'] ?></td>
													
													<td><?php echo $recruiters['totalL2QueuePending'] ?></td>
													<td><?php echo $recruiters['totalAssessmentCreated'] ?></td>
													<td><?php echo $recruiters['totalInterviewCreated'] ?></td>
													<td><?php echo $recruiters['emailFeedbackSought'] ?></td>
													
												</tr>
											<?php 
											}

											?>
										</table>
									</div>
									<div class="displayJobStatusCanvas" id="recruitersStatusGraphViewCanvas" style="">
										
										
										<div class="canvasWidth" style="min-height:200px;">
											<canvas  data-toggle="modal" data-target="#recruitersStatusPopup"  class="view-graph" data-options='{"table":"recruitersStatusTabularViewPopUp", "chart":"recruitersStatusGraphViewPopupParent", "chartType":"recruitersStatus"}' id="recruitersStatusGraphView" style="display:block;"></canvas>
											<div class="legends"  style="font-size:10px;">
												<div class="pull-left  col-md-3" style="width:10%;">
													<div class="pull-left" style="padding-top: 1%;">
															<div style="width:10px;height:10px;background:rgb(174,189,56);"></div>
													</div>
													<div class="pull-left" style="    padding-left: 3px;width:90%;text-align:left;">Applications Shortlisted as L1</div>
													<div style="clear:both"></div>
												</div>
												<div class="pull-left  col-md-3" style="width:10%;">
													<div class="pull-left" style="padding-top: 1%;">
														<div style="width:10px;height:10px;background:rgb(238,110,61);"></div>
													</div>
													<div class="pull-left" style="    padding-left: 3px;width:90%;text-align:left;">Applications Rejected as L1</div>
													<div style="clear:both"></div>
												</div>
												<div class="pull-left  col-md-3" style="width:10%;">
													<div class="pull-left" style="padding-top: 1%;">
														<div style="width:10px;height:10px;background:rgb(111,123,11);"></div>
													</div>
													<div class="pull-left" style="    padding-left: 3px;width:90%;text-align:left;">Applications Shortlisted as L2</div>
													<div style="clear:both"></div>
												</div>
												<div class="pull-left  col-md-3" style="width:10%;">
													<div class="pull-left" style="padding-top:1%;">
														<div style="width:10px;height:10px;background:rgb(177,36,36);"></div>
													</div>
													<div class="pull-left" style="    padding-left: 3px;width:90%;text-align:left;">Applications Rejected as L2</div>
													<div style="clear:both"></div>
												</div>
												<div style="clear:both"></div>
											</div>
										</div>
									</div>
								</div>
            				</div>
                        </div>
        			</div>
					<div class="col-md-6" align="center" >
        				<div class="col-md-12">
                            <div class="row" style="background-color: #054F72; color: #fff; font-size: 19px; margin: 13px 13px;">
                                <div class="col-md-6 pull-left" title="Total no of pending , shortlisted and rejected applications at Level 1 and level 2 corresponding to each job" style="padding: 5px 5px;text-align: left;font-size: 16px;">Application Funnel</div>
                                <div class="col-md-6 pull-right" style="padding: 5px 5px;">
                                    <i class="fa fa-download" aria-hidden="true" style="float: right; padding-left: 15px; padding-top: 3px;cursor:pointer;"   onclick="exportTableToCSV('ApplicationFunnelReport.csv','applicansByJobRecruiterTableReporting')" title="Please click here to download CSV file"></i>
                                    <i class="fa fa-table" aria-hidden="true" style="float: right; padding-left: 15px; padding-top: 3px;cursor:pointer;"  onclick="ChangeView('applicansByJobRecruiterTabularView','applicansByJobRecruiterGraphViewCanvas')" title="Please click here to view table"></i>
                                    <i class="fa fa-bar-chart" aria-hidden="true" style="float: right; padding-left: 15px; padding-top: 3px;cursor:pointer;"  onclick="ChangeView('applicansByJobRecruiterGraphViewCanvas','applicansByJobRecruiterTabularView')" title="Please click here to view chart"></i>
                                </div>
                            </div>
            				<div class="row" style="margin: 13px 13px;">
								<div class="displayJobStatus" style="">
									<div class="tabularViewContent"  id="applicansByJobRecruiterTabularView"  style="display:none;overflow-y: scroll;">
										<table  data-toggle="modal" data-target="#applicansByJobRecruiterPopup"  class="table sortable view-table" 
										data-options='{"table":"recruitersStatusTabularViewPopUp", "chart":"recruitersStatusGraphViewPopupParent", "chartType":"applicansByJobRecruiter"}' 
										id="applicansByJobRecruiterTableReporting" >
												<thead class="tbl_head">
													<tr>
														<th>S.No</th>
														<th>Job Title</th>
														<th>Job Code</th>
														<th>Applications Received</th>
														<th>Pending Applications At L1</th>
														<th>Pending Applications At L2</th>
														<th>Reject</th>
														<th>Pending</th>
														<th>Approve</th>
														<th>L1 Recruiters</th>
														<th>L2 Recruiters</th>
														<th>Final Yes</th>
														
													</tr>
												</thead>
											<?php
											$i =0;
											$totalJob = 0;
											foreach($applicansByJobRecruiter as $JobRecruiter){
												$i++;
												
												?>
												<tr>
													<td><?php echo $i ?></td>
													<td><?php echo $JobRecruiter['jobName'] ?></td>
													<td><?php echo $JobRecruiter['fjCode'] ?></td>
													<td><?php echo $JobRecruiter['totalApplictionReceived'] ?></td>
													<td><?php echo $JobRecruiter['totalPendingApplicationAtL1'] ?></td>
													<td><?php echo $JobRecruiter['totalPendingApplicationAtL2'] ?></td>
													<td><?php echo $JobRecruiter['totalRejectedApplicationAtL1'] + $JobRecruiter['totalRejectedApplicationAtL2'] ?></td>
													<td><?php echo $JobRecruiter['totalPendingApplicationAtL1'] + $JobRecruiter['totalPendingApplicationAtL2'] ?></td>
													<td><?php echo $JobRecruiter['totalShortlistedApplicationAtL1'] ?></td>
													<td><?php echo ($JobRecruiter['l1recruiters'] != "")?$JobRecruiter['l1recruiters']."(".$JobRecruiter['totalShortlistedApplicationAtL1'].")":"NA" ?></td>
													<td><?php echo ($JobRecruiter['l2recruiters'] != "")?$JobRecruiter['l2recruiters']."(".$JobRecruiter['totalShortlistedApplicationAtL2'].")":"NA" ?></td>
													
													
													<td><?php 
													if($JobRecruiter['l2recruiters'] == ""){
														echo $JobRecruiter['totalShortlistedApplicationAtL1'];
													}else{
														echo $JobRecruiter['totalShortlistedApplicationAtL2'];
													}

													?></td>
													
												</tr>
											<?php 
											}

											?>
										</table>
									</div>
									<div class="displayJobStatusCanvas" id="applicansByJobRecruiterGraphViewCanvas" style="">
										
										
									
										<div class="canvasWidth" style="min-height:200px;">
											
											<canvas  data-toggle="modal" data-target="#applicansByJobRecruiterPopup" class="view-graph" 
										data-options='{"table":"recruitersStatusTabularViewPopUp", "chart":"recruitersStatusGraphViewPopupParent", "chartType":"applicansByJobRecruiter"}'  id="applicansByJobRecruiterGraphView" style="display:block;"></canvas>
											<div class="legends"  style="font-size:10px;">
												<div class="pull-left  col-md-2" style="width:10%;">
													<div class="pull-left" style="padding-top:1%;">
															<div style="width:10px;height:10px;background:rgb(39,172,191);"></div>
													</div>
													<div class="pull-left" style="    padding-left: 3px;width:90%;text-align:left;">Pending Applications at L1</div>
													<div style="clear:both"></div>
												</div>
												<div class="pull-left  col-md-2" style="width:10%;">
													<div class="pull-left" style="padding-top: 1%;">
														<div style="width:10px;height:10px;background:rgb(174,189,56);"></div>
													</div>
													<div class="pull-left" style="    padding-left: 3px;width:90%;text-align:left;">Shortlisted Applications at L1</div>
													<div style="clear:both"></div>
												</div>
												<div class="pull-left  col-md-2" style="width:10%;">
													<div class="pull-left" style="padding-top: 1%;">
														<div style="width:10px;height:10px;background:rgb(238,110,61);"></div>
													</div>
													<div class="pull-left" style="    padding-left: 3px;width:90%;text-align:left;">Rejected Applications at L1</div>
													<div style="clear:both"></div>
												</div>
												<div class="pull-left  col-md-2" style="width:10%;">
													<div class="pull-left" style="padding-top: 1%;">
														<div style="width:10px;height:10px;background:rgb(39,172,191);"></div>
													</div>
													<div class="pull-left" style="    padding-left: 3px;width:90%;text-align:left;">Pending Applications at L2</div>
													<div style="clear:both"></div>
												</div>
												<div class="pull-left  col-md-2" style="width:10%;">
													<div class="pull-left" style="padding-top: 1%;">
														<div style="width:10px;height:10px;background:rgb(174,189,56);"></div>
													</div>
													<div class="pull-left" style="    padding-left: 3px;width:90%;text-align:left;">Shortlisted Applications at L2</div>
													<div style="clear:both"></div>
												</div>
												<div class="pull-left  col-md-2" style="width:10%;">
													<div class="pull-left" style="padding-top: 1%;">
														<div style="width:10px;height:10px;background:rgb(238,110,61);"></div>
													</div>
													<div class="pull-left" style="    padding-left: 3px;width:90%;text-align:left;">Rejected Applications at L2</div>
													<div style="clear:both"></div>
												</div>
												
												<div style="clear:both"></div>
											</div>
										</div>
									</div>
								</div>
            				</div>
                        </div>
        			</div>
					
					<div class="col-md-6" align="center" >
        				<div class="col-md-12">
                            <div class="row" style="background-color: #054F72; color: #fff; font-size: 19px; margin: 13px 13px;">
                                <div class="col-md-6 pull-left" style="padding: 5px 5px;text-align: left;    font-size: 16px;">Applicant Profile by Job</div>
                                <div class="col-md-6 pull-right" style="padding: 5px 5px;">
                                    <i class="fa fa-download" aria-hidden="true" style="float: right; padding-left: 15px; padding-top: 3px;cursor:pointer;"   onclick="exportTableToCSV('ApplicantProfileByJob.csv','applicantProfileByJobTableReporting')" title="Please click here to download CSV file"></i>
                                    <i class="fa fa-table" aria-hidden="true" style="float: right; padding-left: 15px; padding-top: 3px;cursor:pointer;"  onclick="ChangeView('applicantProfileByJobTabularView','applicantProfileByJobGraphViewCanvas')" title="Please click here to view table"></i>
                                    <i class="fa fa-bar-chart" aria-hidden="true" style="float: right; padding-left: 15px; padding-top: 3px;cursor:pointer;"  onclick="ChangeView('applicantProfileByJobGraphViewCanvas','applicantProfileByJobTabularView')" title="Please click here to view chart"></i>
                                </div>
                            </div>
            				<div class="row" style="margin: 13px 13px;">
								<div class="displayJobStatus" style="">
									<div class="tabularViewContent"  id="applicantProfileByJobTabularView"  style="display:none;overflow-y: scroll;">
										<table  data-toggle="modal" data-target="#applicantProfileByJobPopup"  class="table sortable view-table" data-options='{"table":"applicantProfileByJobTabularViewPopUp", "chart":"applicantProfileByJobGraphViewPopupParent", "chartType":"applicantProfileByJob"}'  id="applicantProfileByJobTableReporting" >
												<thead class="tbl_head">
												<tr>
													<th></th>
													<th></th>
													<th colspan="4" style="    border-left: 1px solid white;text-align: center;">Applied</th>
													<th colspan="4" style="    border-left: 1px solid white;text-align: center;">Shortlisted</th>
												</tr>
													<tr>
														<th>S.No</th>
														<th>Job Title</th>
														<th>Total</th>
														<th>Invited</th>
														<th>Other</th>
														<th>Top 3 Cities</th>
														<th>Total</th>
														<th>Invited</th>
														<th>Other</th>
														<th>Top 3 Cities</th>
													</tr>
												</thead>
											<?php
											$i =0;
											$totalJob = 0;
											foreach($applicantProfileByJob as $applicantProfile){
												$i++;
												$pinCodesArray = explode(",",$applicantProfile['pinCodes']);
												
												$cities = "";
												$totalCities = 1;
												if(!empty($pinCodesArray)){
													foreach($pinCodesArray as $pincode){
														if($totalCities > 3){
															break;
														}
														$citiesArray = $ci->userjob_model->getCityStatePincode($pincode);
														if($citiesArray->city != ""){
															$cities .= $citiesArray->city.", ";
															$totalCities++;
														}
														
													}
												}else{
													$cities = "NA";
												}
												?>
												<tr>
													<td><?php echo $i ?></td>
													<td><?php echo $applicantProfile['jobName'] ?></td>
													<td><?php echo $applicantProfile['appliedTotal'] ?></td>
													<td><?php echo $applicantProfile['appliedInvitedCount'] ?></td>
													<td><?php echo $applicantProfile['appliedTotal']-$applicantProfile['appliedInvitedCount'] ?></td>
													<td><?php echo ($cities != "")?$cities:"NA"; ?></td>
													<td><?php echo $applicantProfile['shortlistedTotal'] ?></td>
													<td><?php echo $applicantProfile['shortlistedInvitedCount'] ?></td>
													<td><?php echo $applicantProfile['shortlistedTotal']-$applicantProfile['shortlistedInvitedCount'] ?></td>
													<td><?php echo $cities ?></td>
													
												</tr>
											<?php 
											}

											?>
										</table>
									</div>
									<div class="displayJobStatusCanvas" id="applicantProfileByJobGraphViewCanvas" style="">
										
										<div class="canvasWidth" style="min-height:200px;">
											
										
											<canvas  data-toggle="modal" data-target="#applicantProfileByJobPopup"  class="view-graph" data-options='{"table":"applicantProfileByJobTabularViewPopUp", "chart":"applicantProfileByJobGraphViewPopupParent", "chartType":"applicantProfileByJob"}'  id="applicantProfileByJobGraphView" style="display:block;"></canvas>
											<div class="legends" style="font-size:10px;">
												<div class="pull-left  col-md-3" style="width:10%">
													<div class="pull-left" style="padding-top: 1%;">
															<div style="width:10px;height:10px;background:rgb(39,172,191);"></div>
													</div>
													<div class="pull-left" style="    padding-left: 3px;width:90%;text-align:left;">Applications from Top 3 cities</div>
													<div style="clear:both"></div>
												</div>
												<div class="pull-left  col-md-3" style="width:10%">
													<div class="pull-left" style="padding-top: 1%;">
														<div style="width:10px;height:10px;background:rgb(140,46,10);"></div>
													</div>
													<div class="pull-left" style="    padding-left: 3px;width:89%;text-align:left;">Applications from Other cities</div>
													<div style="clear:both"></div>
												</div>
												<div style="clear:both"></div>
											</div>
										</div>
									</div>
								</div>
            				</div>
                        </div>
        			</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="jobApplicationsGraphViewPopup" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width: 75%;height:365px;   margin-top: 70px;">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="border-bottom:1px solid black;">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body" id="share">
				
					<div class="row" style="background-color: #054F72; color: #fff; font-size: 19px; margin: 13px 13px;">
						<div class="col-md-6 pull-left" title="No of applicants being viewed and corresponding action against their applications" style="padding: 5px 5px;">Applicants by Job</div>
						<div class="col-md-6 pull-right" style="padding: 5px 5px;">
							<i class="fa fa-download" aria-hidden="true" style="float: right; margin-left: 15px; padding-top: 3px;cursor:pointer;"   onclick="exportTableToCSV('ApplicantsByJob.csv','applicansByJobRecruiterTableReporting')"  title="Please click here to download CSV file"></i>
							
							<i class="fa fa-table" aria-hidden="true" style="float: right; margin-left: 15px; padding-top: 3px;cursor:pointer;"  onclick="tabularView('jobApplications')"  title="Please click here to view table"></i>
							<i class="fa fa-bar-chart" aria-hidden="true" style="float: right; margin-left: 15px; padding-top: 3px;cursor:pointer;"  onclick="GraphView('jobApplications')"  title="Please click here to view chart"></i>
							
						</div>
					</div>
					<div class="row" style="margin: 13px 13px;">
						<div id="display" style="">
							<div id="jobApplicationsTabularViewPopUp"  style="display:none;height:365px;overflow:scroll;"></div>
							<div id="jobApplicationsGraphViewPopupParent" style="width:100%;height:365px;overflow:scroll;">
								<div class="legends"  style="font-size:10px;margin-bottom:10px;">
									<div class="pull-left  col-md-3" style="width:14%">
										<div class="pull-left" style="padding-top: 1%;">
												<div style="width:10px;height:10px;background:rgb(39,172,191);"></div>
										</div>
										<div class="pull-left" style="    padding-left: 3px;width:71%;text-align:left;">Shortlisted Count</div>
										<div style="clear:both"></div>
									</div>
									<div class="pull-left  col-md-3" style="width:14%">
										<div class="pull-left" style="padding-top:1%;">
											<div style="width:10px;height:10px;background:rgb(140,46,10);"></div>
										</div>
										<div  class="pull-left"  style="    padding-left: 3px;width:89%;text-align:left;">Viewed Count</div>
										<div style="clear:both"></div>
									</div>
									<div style="clear:both"></div>
								</div>
								<canvas  id="jobApplicationsPopUpGraphViewPopup" style="display:block;"></canvas>
							</div>
						</div>
					</div>
					
            </div>
        </div>
    </div>
</div>

<div id="jobInvitationsGraphViewPopUp" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width: 75%;height:365px;   margin-top: 70px;">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="border-bottom:1px solid black;">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body" id="share">
				
					<div class="row" style="background-color: #054F72; color: #fff; font-size: 19px; margin: 13px 13px;">
						<div class="col-md-6 pull-left" title="No of invited candidates via VDOHire corresponding to applications received through invitations sent" style="padding: 5px 5px;">Candidates Invited</div>
						<div class="col-md-6 pull-right" style="padding: 5px 5px;">
							<i class="fa fa-download" aria-hidden="true" style="float: right; margin-left: 15px; padding-top: 3px;cursor:pointer;"   onclick="exportTableToCSV('CandidatesInvited.csv','applicansByJobRecruiterTableReporting')"  title="Please click here to download CSV file"></i>
							
							<i class="fa fa-table" aria-hidden="true" style="float: right; margin-left: 15px; padding-top: 3px;cursor:pointer;"  onclick="tabularView('jobInvitations')"  title="Please click here to view table"></i>
							<i class="fa fa-bar-chart" aria-hidden="true" style="float: right; margin-left: 15px; padding-top: 3px;cursor:pointer;"  onclick="GraphView('jobInvitations')"  title="Please click here to view chart"></i>
							
						</div>
					</div>
					<div class="row" style="margin: 13px 13px;">
						<div id="display" style="">
							<div id="jobInvitationsTabularViewPopUp"  style="display:none;height:365px;overflow:scroll;"></div>
							<div id="jobInvitationsGraphViewPopupParent" style="width:100%;height:365px;overflow:scroll;">
								<div class="legends" style="font-size:10px;margin-bottom:10px;">
									<div class="pull-left  col-md-3" style="width:14%">
										<div class="pull-left" style="padding-top: 1%;">
												<div style="width:10px;height:10px;background:rgb(39,172,191);"></div>
										</div>
										<div class="pull-left" style="    padding-left: 3px;width:90%;">Invitation Count</div>
										<div style="clear:both"></div>
									</div>
									<div class="pull-left  col-md-3" style="width:14%">
										<div class="pull-left" style="padding-top:1%;">
											<div style="width:10px;height:10px;background:rgb(140,46,10);"></div>
										</div>
										<div  class="pull-left"  style="    padding-left: 3px;width:90%;">Invited Applications</div>
										<div style="clear:both"></div>
									</div>
									<div style="clear:both"></div>
								</div>
								<canvas  id="jobInvitationsPopUpGraphViewPopup" style="display:block;"></canvas>
							</div>
						</div>
					</div>
					
            </div>
        </div>
    </div>
</div>

<div id="jobFullfillmentpopUp" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width: 75%;height:365px;   margin-top: 70px;">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="border-bottom:1px solid black;">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body" id="share">
				
					<div class="row" style="background-color: #054F72; color: #fff; font-size: 19px; margin: 13px 13px;">
						<div class="col-md-6 pull-left" title="Total no of vacancies per job and total applications received corresponding to that job" style="padding: 5px 5px;">Jobs - Vacancies vs Application</div>
						<div class="col-md-6 pull-right" style="padding: 5px 5px;">
							<i class="fa fa-download" aria-hidden="true" style="float: right; margin-left: 15px; padding-top: 3px;cursor:pointer;"   onclick="exportTableToCSV('JobsVacanciesVsApplication.csv','applicansByJobRecruiterTableReporting')"  title="Please click here to download CSV file"></i>
							
							<i class="fa fa-table" aria-hidden="true" style="float: right; margin-left: 15px; padding-top: 3px;cursor:pointer;"  onclick="tabularView('jobFullfillment')"  title="Please click here to view table"></i>
							<i class="fa fa-bar-chart" aria-hidden="true" style="float: right; margin-left: 15px; padding-top: 3px;cursor:pointer;"  onclick="GraphView('jobFullfillment')"  title="Please click here to view chart"></i>
							
						</div>
					</div>
					<div class="row" style="margin: 13px 13px;">
						<div id="display" style="">
							<div id="jobFullfillmentTabularViewPopUp"  style="display:none;height:365px;overflow:scroll;"></div>
							<div id="jobFullfillmentGraphViewPopupParent" style="width:100%;height:365px;overflow:scroll;">
								<div class="legends" style="font-size:10px;margin-bottom:10px;">
									<div class="pull-left  col-md-3" style="width:14%;">
										<div class="pull-left" style="padding-top:1%;">
												<div style="width:10px;height:10px;background:rgb(39,172,191);"></div>
										</div>
										<div class="pull-left" style="    padding-left: 3px;width:71%;text-align:left;">No Of Vacancies</div>
										<div style="clear:both"></div>
									</div>
									<div class="pull-left  col-md-3" style="width:14%;">
										<div class="pull-left" style="padding-top: 1%;">
											<div style="width:10px;height:10px;background:rgb(140,46,10);"></div>
										</div>
										<div  class="pull-left"  style="    padding-left: 3px;width:71%;text-align:left;">Total Applications</div>
										<div style="clear:both"></div>
									</div>
									<div style="clear:both"></div>
								</div>
								<canvas  id="jobFullfillmentGraphViewPopup" style="display:block;"></canvas>
							</div>
						</div>
					</div>
					
            </div>
        </div>
    </div>
</div>

<div id="applicantProfileByJobPopup" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width: 75%;height:365px;   margin-top: 70px;">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="border-bottom:1px solid black;">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body" id="share">
				
					<div class="row" style="background-color: #054F72; color: #fff; font-size: 19px; margin: 13px 13px;">
						<div class="col-md-6 pull-left" style="padding: 5px 5px;">Applicant Profile by Job</div>
						<div class="col-md-6 pull-right" style="padding: 5px 5px;">
							<i class="fa fa-download" aria-hidden="true" style="float: right; margin-left: 15px; padding-top: 3px;cursor:pointer;"   onclick="exportTableToCSV('ApplicantProfileByJob.csv','applicansByJobRecruiterTableReporting')"  title="Please click here to download CSV file"></i>
							
							<i class="fa fa-table" aria-hidden="true" style="float: right; margin-left: 15px; padding-top: 3px;cursor:pointer;"  onclick="tabularView('applicantProfileByJob')"  title="Please click here to view table"></i>
							<i class="fa fa-bar-chart" aria-hidden="true" style="float: right; margin-left: 15px; padding-top: 3px;cursor:pointer;"  onclick="GraphView('applicantProfileByJob')"  title="Please click here to view chart"></i>
							
						</div>
					</div>
					<div class="row" style="margin: 13px 13px;">
						<div id="display" style="">
							<div id="applicantProfileByJobTabularViewPopUp"  style="display:none;height:365px;overflow:scroll;"></div>
							<div id="applicantProfileByJobGraphViewPopupParent" style="width:100%;height:365px;overflow:scroll;">
								<div class="legends" style="font-size:10px;margin-bottom:10px;">
									<div class="pull-left  col-md-3" style="width:20%">
										<div class="pull-left" style="padding-top: 1%;">
												<div style="width:10px;height:10px;background:rgb(39,172,191);"></div>
										</div>
										<div class="pull-left" style="    padding-left: 3px;width:90%;text-align:left;">Applications from Top 3 cities</div>
										<div style="clear:both"></div>
									</div>
									<div class="pull-left  col-md-3" style="width:20%">
										<div class="pull-left" style="padding-top: 1%;">
											<div style="width:10px;height:10px;background:rgb(140,46,10);"></div>
										</div>
										<div class="pull-left" style="    padding-left: 3px;width:89%;text-align:left;">Applications from Other cities</div>
										<div style="clear:both"></div>
									</div>
									<div style="clear:both"></div>
								</div>
								<canvas  id="applicantProfileByJobGraphViewPopup" style="display:block;"></canvas>
							</div>
						</div>
					</div>
					
            </div>
        </div>
    </div>
</div>


<div id="applicansByJobRecruiterPopup" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width: 75%;height:365px;   margin-top: 70px;">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="border-bottom:1px solid black;">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body" id="share">
				
					<div class="row" style="background-color: #054F72; color: #fff; font-size: 19px; margin: 13px 13px;">
						<div class="col-md-6 pull-left" title="Total no of pending , shortlisted and rejected applications at Level 1 and level 2 corresponding to each job" style="padding: 5px 5px;">Application Funnel</div>
						<div class="col-md-6 pull-right" style="padding: 5px 5px;">
							<i class="fa fa-download" aria-hidden="true" style="float: right; margin-left: 15px; padding-top: 3px;cursor:pointer;"   onclick="exportTableToCSV('ApplicantFunnel.csv','applicansByJobRecruiterTableReporting')"  title="Please click here to download CSV file"></i>
							
							<i class="fa fa-table" aria-hidden="true" style="float: right; margin-left: 15px; padding-top: 3px;cursor:pointer;"  onclick="tabularView('applicansByJobRecruiter')"  title="Please click here to view table"></i>
							<i class="fa fa-bar-chart" aria-hidden="true" style="float: right; margin-left: 15px; padding-top: 3px;cursor:pointer;"  onclick="GraphView('applicansByJobRecruiter')"  title="Please click here to view chart"></i>
							
						</div>
					</div>
					<div class="row" style="margin: 13px 13px;">
						<div id="display" style="">
							<div id="applicansByJobRecruiterTabularViewPopUp"  style="display:none;height:365px;overflow:scroll;"></div>
							<div id="applicansByJobRecruiterGraphViewPopupParent" style="width:100%;height:365px;overflow-y:scroll;">
								<div class="legends"  style="font-size:10px;margin-bottom: 10px;">
									<div class="pull-left  col-md-2" style="width:16%;padding-left: 0px;">
										<div class="pull-left" style="padding-top:1%;">
												<div style="width:10px;height:10px;background:rgb(39,172,191);"></div>
										</div>
										<div class="pull-left" style="    padding-left: 3px;width:90%;text-align:left;">Pending Applications at L1</div>
										<div style="clear:both"></div>
									</div>
									<div class="pull-left  col-md-2" style="width:16%;padding-left: 0px;">
										<div class="pull-left" style="padding-top: 1%;">
											<div style="width:10px;height:10px;background:rgb(174,189,56);"></div>
										</div>
										<div class="pull-left" style="    padding-left: 3px;width:90%;text-align:left;">Shortlisted Applications at L1</div>
										<div style="clear:both"></div>
									</div>
									<div class="pull-left  col-md-2" style="width:16%;padding-left: 0px;">
										<div class="pull-left" style="padding-top: 1%;">
											<div style="width:10px;height:10px;background:rgb(238,110,61);"></div>
										</div>
										<div class="pull-left" style="    padding-left: 3px;width:90%;text-align:left;">Rejected Applications at L1</div>
										<div style="clear:both"></div>
									</div>
									<div class="pull-left  col-md-2" style="width:16%;padding-left: 0px;">
										<div class="pull-left" style="padding-top: 1%;">
											<div style="width:10px;height:10px;background:rgb(39,172,191);"></div>
										</div>
										<div class="pull-left" style="    padding-left: 3px;width:90%;text-align:left;">Pending Applications at L2</div>
										<div style="clear:both"></div>
									</div>
									<div class="pull-left  col-md-2" style="width:16%;padding-left: 0px;">
										<div class="pull-left" style="padding-top: 1%;">
											<div style="width:10px;height:10px;background:rgb(174,189,56);"></div>
										</div>
										<div class="pull-left" style="    padding-left: 3px;width:90%;text-align:left;">Shortlisted Applications at L2</div>
										<div style="clear:both"></div>
									</div>
									<div class="pull-left  col-md-2" style="width:16%;padding-left: 0px;">
										<div class="pull-left" style="padding-top: 1%;">
											<div style="width:10px;height:10px;background:rgb(238,110,61);"></div>
										</div>
										<div class="pull-left" style="    padding-left: 3px;width:90%;text-align:left;">Rejected Applications at L2</div>
										<div style="clear:both"></div>
									</div>
									
									<div style="clear:both"></div>
								</div>
								<canvas  id="applicansByJobRecruiterGraphViewPopup" style="display:block;"></canvas>
								
							</div>
						</div>
					</div>
					
            </div>
        </div>
    </div>
</div>

<div id="recruitersStatusPopup" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width: 75%;height:365px;   margin-top: 70px;">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="border-bottom:1px solid black;">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body" id="share">
				
					<div class="row" style="background-color: #054F72; color: #fff; font-size: 19px; margin: 13px 13px;">
						<div class="col-md-6 pull-left" title="Total no of applications shortlisted and rejected by Level 1 and Level 2 recruiter" style="padding: 5px 5px;">Recruiter Activity Report</div>
						<div class="col-md-6 pull-right" style="padding: 5px 5px;">
							<i class="fa fa-download" aria-hidden="true" style="float: right; margin-left: 15px; padding-top: 3px;cursor:pointer;"   onclick="exportTableToCSV('RecruitersActivityReport.csv','recruitersStatusTableReporting')"  title="Please click here to download CSV file"></i>
							
							<i class="fa fa-table" aria-hidden="true" style="float: right; margin-left: 15px; padding-top: 3px;cursor:pointer;"  onclick="tabularView('recruitersStatus')"  title="Please click here to view table"></i>
							
							<i class="fa fa-bar-chart" aria-hidden="true" style="float: right; margin-left: 15px; padding-top: 3px;cursor:pointer;"  onclick="GraphView('recruitersStatus')"  title="Please click here to view chart"></i>
							
						</div>
					</div>
					<div class="row" style="margin: 13px 13px;">
						<div id="display" style="">
							<div id="recruitersStatusTabularViewPopUp"  style="display:none;height:365px;overflow:scroll;"></div>
							<div id="recruitersStatusGraphViewPopupParent" style="width:100%;height:380px;overflow:scroll;">
								<div class="legends"  style="font-size:10px;margin-bottom:10px;">
									<div class="pull-left  col-md-3" style="width:19%;">
										<div class="pull-left" style="padding-top: 1%;">
												<div style="width:10px;height:10px;background:rgb(174,189,56);"></div>
										</div>
										<div class="pull-left" style="    padding-left: 3px;width:90%;text-align:left;">Applications Shortlisted as L1</div>
										<div style="clear:both"></div>
									</div>
									<div class="pull-left  col-md-3" style="width:19%;">
										<div class="pull-left" style="padding-top: 1%;">
											<div style="width:10px;height:10px;background:rgb(238,110,61);"></div>
										</div>
										<div class="pull-left" style="    padding-left: 3px;width:90%;text-align:left;">Applications Rejected as L1</div>
										<div style="clear:both"></div>
									</div>
									<div class="pull-left  col-md-3" style="width:19%;">
										<div class="pull-left" style="padding-top: 1%;">
											<div style="width:10px;height:10px;background:rgb(111,123,11);"></div>
										</div>
										<div class="pull-left" style="    padding-left: 3px;width:90%;text-align:left;">Applications Shortlisted as L2</div>
										<div style="clear:both"></div>
									</div>
									<div class="pull-left  col-md-3" style="width:19%;">
										<div class="pull-left" style="padding-top:1%;">
											<div style="width:10px;height:10px;background:rgb(177,36,36);"></div>
										</div>
										<div class="pull-left" style="    padding-left: 3px;width:90%;text-align:left;">Applications Rejected as L2</div>
										<div style="clear:both"></div>
									</div>
									<div style="clear:both"></div>
								</div>
								<canvas  id="recruitersStatusGraphViewPopup" style="display:block;"></canvas>
							</div>
						</div>
					</div>
					
            </div>
        </div>
    </div>
</div>

<div id="jobStatusPopup" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width: 75%;height:365px;   margin-top: 70px;">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="border-bottom:1px solid black;">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body" id="share">
				
					<div class="row" style="background-color: #054F72; color: #fff; font-size: 19px; margin: 13px 13px;">
						<div class="col-md-6 pull-left" style="padding: 5px 5px;">Job Status</div>
						<div class="col-md-6 pull-right" style="padding: 5px 5px;">
							<i class="fa fa-download" aria-hidden="true" style="float: right; margin-left: 15px; padding-top: 3px;cursor:pointer;"   onclick="exportTableToCSV('JobStatusReport.csv','jobStatusTableReporting')"  title="Please click here to download CSV file"></i>
							
							<i class="fa fa-table" aria-hidden="true" style="float: right; margin-left: 15px; padding-top: 3px;cursor:pointer;"  onclick="tabularView('jobStatusPopUp')"  title="Please click here to view table"></i>
							<i class="fa fa-bar-chart" aria-hidden="true" style="float: right; margin-left: 15px; padding-top: 3px;cursor:pointer;"  onclick="GraphView('jobStatusPopUp')"  title="Please click here to view chart"></i>
							
						</div>
					</div>
					<div class="row" style="margin: 13px 13px;">
						<div id="display" style="">
							<div id="JobStatusTabularViewPopUp"  style="display:none;height:365px;overflow:scroll;"></div>
							<div id="jobStatusGraphViewPopupParent" style="width:100%;height:365px;overflow:scroll;">
								<div class="legends" style="font-size:10px;margin-bottom:10px;">
									<div class="pull-left  col-md-3" style="width:20%;">
										<div class="pull-left" style="padding-top: 1%;">
												<div style="width:10px;height:10px;background:rgb(39,172,191);"></div>
										</div>
										<div class="pull-left" style="    padding-left: 3px;">Other Applications</div>
										<div style="clear:both"></div>
									</div>
									<div class="pull-left  col-md-3" style="width:20%%;">
										<div class="pull-left" style="padding-top:1%;">
											<div style="width:10px;height:10px;background:rgb(140,46,10);"></div>
										</div>
										<div  class="pull-left"  style="    padding-left: 3px;">Invited Applications</div>
										<div style="clear:both"></div>
									</div>
									<div style="clear:both"></div>
								</div>
								<canvas  data-toggle="modal" data-target="#jobStatusPopup" id="jobStatusGraphViewPopup" style="display:block;"></canvas>
							</div>
						</div>
					</div>
					
            </div>
        </div>
    </div>
</div>

<div id="PendingApplicationPopup" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width: 88%;height:365px;   margin-top: 70px;">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="border-bottom:1px solid black;">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body" id="share">
				
					<div class="row" style="background-color: #054F72; color: #fff; font-size: 19px; margin: 13px 13px;">
						<div class="col-md-6 pull-left" title="Total applications received at level 1 and level 2 corresponding to applications pending for actions" style="padding: 5px 5px;">Recruiter by Job</div>
						<div class="col-md-6 pull-right" style="padding: 5px 5px;">
							<i class="fa fa-download" aria-hidden="true" style="float: right; padding-left: 15px; padding-top: 3px;cursor:pointer;"   onclick="exportTableToCSV('RecruiterByJobReport.csv')"></i>
							
							<i class="fa fa-table" aria-hidden="true" style="float: right; padding-left: 15px; padding-top: 3px;cursor:pointer;"  onclick="tabularView('pendingApplication')"></i>
							
							<i class="fa fa-bar-chart" aria-hidden="true" style="float: right; padding-left: 15px; padding-top: 3px;cursor:pointer;"  onclick="GraphView('pendingApplication')"></i>
						</div>
					</div>
					<div class="row" style="margin: 13px 13px;">
						<div id="display" style="">
							<div id="PendingApplicationTabularViewPopUp"    style="display:none;height:365px;overflow:scroll;"></div>
							<div id="PendingApplicationGraphViewPopupParent" style="width:100%;height:365px;overflow:scroll;">
								<div class="legends" style="font-size:10px;margin-bottom:10px;">
									<div class="pull-left  col-md-2" style="padding-left:0px;width:14%;">
										<div class="pull-left" style="padding-top: 1%;">
												<div style="width:10px;height:10px;background:rgb(39,172,191);"></div>
										</div>
										<div class="pull-left" style="    padding-left: 3px;width:90%;text-align: left;">Pending Applications At L1</div>
										<div style="clear:both"></div>
									</div>
									<div class="pull-left  col-md-2" style="padding-left:0px;width:14%;">
										<div class="pull-left" style="padding-top:1%;">
											<div style="width:10px;height:10px;background:rgb(174,189,56);"></div>
										</div>
										<div  class="pull-left"  style="    padding-left: 3px;width:90%;text-align: left;">Shortlisted Applications At L1</div>
										<div style="clear:both"></div>
									</div>
									<div class="pull-left  col-md-2" style="padding-left:0px;width:14%;">
										<div class="pull-left" style="padding-top: 1%;">
											<div style="width:10px;height:10px;background:rgb(238,110,61);"></div>
										</div>
										<div  class="pull-left"  style="    padding-left: 3px;width:90%;text-align: left;">Rejected Applications At L2</div>
										<div style="clear:both"></div>
									</div>
									<div class="pull-left  col-md-2" style="padding-left:0px;width:14%;">
										<div class="pull-left" style="padding-top: 1%;">
											<div style="width:10px;height:10px;background:rgb(35,98,170);"></div>
										</div>
										<div  class="pull-left"  style="    padding-left: 3px;width:90%;text-align: left;">Pending Applications At L2</div>
										<div style="clear:both"></div>
									</div>
									<div class="pull-left  col-md-2" style="padding-left:0px;width:14%;">
										<div class="pull-left" style="padding-top: 1%;">
											<div style="width:10px;height:10px;background:rgb(111,123,11);"></div>
										</div>
										<div  class="pull-left"  style="    padding-left: 3px;width:90%;text-align: left;">Shortlisted Applications At L2</div>
										<div style="clear:both"></div>
									</div>
									<div class="pull-left  col-md-2" style="padding-left:0px;width:14%;">
										<div class="pull-left" style="padding-top: 1%;">
											<div style="width:10px;height:10px;background:rgb(177,36,36);"></div>
										</div>
										<div  class="pull-left"  style="    padding-left: 3px;width:90%;text-align: left;">Rejected Applications At L2</div>
										<div style="clear:both"></div>
									</div>
									<div style="clear:both"></div>
								</div>
								<canvas id="PendingApplicationGraphViewPopup" style="display:block;"></canvas>
							</div>
						</div>
					</div>
					
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $( "#filterJobForm" ).hide();
    $( "#filterJobButton" ).click(function() {
        $( "#filterJobForm" ).toggle( "slow", function() {
        });
    });
	JobStatusview = "graph";
	
	function tabularView(block){
		if(block == 'jobStatusPopUp'){
			var tableData = $("#jobStatusTableReporting").html();
			$("#JobStatusTabularViewPopUp").html("<table class='table sortable' >"+tableData+"</table>");
			$("#JobStatusTabularViewPopUp").css("display","block");
			$("#jobStatusGraphViewPopupParent").css("display","none");
		}else if(block == 'pendingApplication'){
			var tableData = $("#pendingApplicationTableReporting").html();
			$("#PendingApplicationTabularViewPopUp").html("<table class='table sortable' >"+tableData+"</table>");
			$("#PendingApplicationTabularViewPopUp").css("display","block");
			$("#PendingApplicationGraphViewPopupParent").css("display","none");
		}else if(block == 'recruitersStatus'){
			var tableData = $("#recruitersStatusTableReporting").html();
			$("#recruitersStatusTabularViewPopUp").html("<table class='table sortable' >"+tableData+"</table>");
			$("#recruitersStatusTabularViewPopUp").css("display","block");
			$("#recruitersStatusGraphViewPopupParent").css("display","none");
		}else if(block == 'applicansByJobRecruiter'){
			
			var tableData = $("#applicansByJobRecruiterTableReporting").html();
			
			$("#applicansByJobRecruiterTabularViewPopUp").html("<table class='table sortable' >"+tableData+"</table>");
			$("#applicansByJobRecruiterTabularViewPopUp").css("display","block");
			$("#applicansByJobRecruiterGraphViewPopupParent").css("display","none");
		}else if(block == 'applicantProfileByJob'){
			var tableData = $("#applicantProfileByJobTableReporting").html();
			$("#applicantProfileByJobTabularViewPopUp").html("<table class='table sortable' >"+tableData+"</table>");
			$("#applicantProfileByJobTabularViewPopUp").css("display","block");
			$("#applicantProfileByJobGraphViewPopupParent").css("display","none");
		}else if(block == 'jobApplications'){
			var tableData = $("#jobApplicationsTableReporting").html();
			$("#jobApplicationsTabularViewPopUp").html("<table class='table sortable' >"+tableData+"</table>");
			$("#jobApplicationsTabularViewPopUp").css("display","block");
			$("#jobApplicationsGraphViewPopupParent").css("display","none");
		}else if(block == 'jobInvitations'){
			var tableData = $("#jobInvitationsTableReporting").html();
			$("#jobInvitationsTabularViewPopUp").html("<table class='table sortable' >"+tableData+"</table>");
			$("#jobInvitationsTabularViewPopUp").css("display","block");
			$("#jobInvitationsGraphViewPopupParent").css("display","none");
		}else if(block == 'jobFullfillment'){
			var tableData = $("#jobFullfillmentDataTableReporting").html();
			$("#jobFullfillmentTabularViewPopUp").html("<table class='table sortable' >"+tableData+"</table>");
			$("#jobFullfillmentTabularViewPopUp").css("display","block");
			$("#jobFullfillmentGraphViewPopupParent").css("display","none");
		}

	}
	
	function GraphView(block){
		if(block == "jobStatus"){
			$("#JobStatusTabularView").css("display","none");
			$("#jobStatusGraphView").css("display","block");
		}else if(block == "jobStatusPopUp"){
			$("#JobStatusTabularViewPopUp").css("display","none");
			$("#jobStatusGraphViewPopupParent").css("display","block");
		}else if(block == 'pendingApplication'){
			$("#PendingApplicationTabularViewPopUp").css("display","none");
			$("#PendingApplicationGraphViewPopupParent").css("display","block");
		}else if(block == 'recruitersStatus'){
			$("#recruitersStatusGraphViewPopupParent").css("display","block");
			$("#recruitersStatusTabularViewPopUp").css("display","none");
		}else if(block == 'applicansByJobRecruiter'){
			$("#applicansByJobRecruiterGraphViewPopupParent").css("display","block");
			$("#applicansByJobRecruiterTabularViewPopUp").css("display","none");
		}else if(block == 'applicantProfileByJob'){
			$("#applicantProfileByJobGraphViewPopupParent").css("display","block");
			$("#applicantProfileByJobTabularViewPopUp").css("display","none");
		}else if(block == 'jobApplications'){
			$("#jobApplicationsGraphViewPopupParent").css("display","block");
			$("#jobApplicationsTabularViewPopUp").css("display","none");
		}else if(block == 'jobInvitations'){
			$("#jobInvitationsGraphViewPopupParent").css("display","block");
			$("#jobInvitationsTabularViewPopUp").css("display","none");
		}else if(block == 'jobFullfillment'){
			$("#jobFullfillmentGraphViewPopupParent").css("display","block");
			$("#jobFullfillmentTabularViewPopUp").css("display","none");
		}
	}
	
	
	
	function DownloadSheet(block){
		if(block == "jobStatus"){
			var win = window.open('http://35.154.53.72/admin/analytics/DownloadReport?block='+block, '_blank');
		}else if(block == "jobStatusPopUp"){
			var win = window.open('http://35.154.53.72/admin/analytics/DownloadReport?block='+block, '_blank');
		}
	}
	
	$(".popupJobStatus").on('click',function(){
		if(JobStatusview == "graph"){
			
		}
	})
	
	
	function exportTableToCSV(filename,table) {
		var csv = [];
		var rows = document.querySelectorAll("#"+table+" tr");
		
		for (var i = 0; i < rows.length; i++) {
			var row = [], cols = rows[i].querySelectorAll("td, th");
			
			for (var j = 0; j < cols.length; j++) 
				row.push(cols[j].innerText);
			
			csv.push(row.join(","));        
		}

		// Download CSV file
		downloadCSV(csv.join("\n"), filename);
	}
	
	function downloadCSV(csv, filename) {
		var csvFile;
		var downloadLink;

		// CSV file
		csvFile = new Blob([csv], {type: "text/csv"});

		// Download link
		downloadLink = document.createElement("a");

		// File name
		downloadLink.download = filename;

		// Create a link to the file
		downloadLink.href = window.URL.createObjectURL(csvFile);

		// Hide download link
		downloadLink.style.display = "none";

		// Add the link to DOM
		document.body.appendChild(downloadLink);

		// Click download link
		downloadLink.click();
	}
	
	//
	
	$( document ).ready(function() {
		var onebar = 30;
		var windowWidth = $(window).width();
		var windowHeight = $(window).height();
		var totalJob = $("#totalJobStatusCount").val();
		
		var canvasParent = (windowWidth-200)/2;
		
		var canvasWidth = totalJob*onebar;
		if(canvasWidth > canvasParent){
			$(".displayJobStatusCanvas").css("width",canvasParent);
			$("#displayjobInvitations").css("width",canvasParent);
			
			$(".displayJobStatusCanvas").css("overflow-x","scroll");
			$("#displayjobInvitations").css("overflow-x","scroll");
			$(".canvasWidth").css("width",canvasWidth);
			$(".tabularViewContent").css('height',(windowHeight-400));
			
			$("#jobFullfillmentDataTabularView").css('height',(windowHeight-400));
			$("#jobInvitationsTabularView").css('height',(windowHeight-400));
			$("#jobApplicationsTabularView").css('height',(windowHeight-400));
			$("#applicantProfileByJobTabularView").css('height',(windowHeight-400));
			$("#applicansByJobRecruiterTabularView").css('height',(windowHeight-400));
			$("#recruitersStatusTabularView").css('height',(windowHeight-400));
			
			$(".canvasWidth").css('height',(windowHeight-400));
			$("#jobAnalytics").css('height',(windowHeight-200));
			$("#applicationAnalytics").css('height',(windowHeight-200));
		}else{
			$(".tabularViewContent").css('height',(windowHeight-400));
			
			$("#jobFullfillmentDataTabularView").css('height',(windowHeight-400));
			$("#jobInvitationsTabularView").css('height',(windowHeight-400));
			$("#jobApplicationsTabularView").css('height',(windowHeight-400));
			$("#applicantProfileByJobTabularView").css('height',(windowHeight-400));
			$("#applicansByJobRecruiterTabularView").css('height',(windowHeight-400));
			$("#recruitersStatusTabularView").css('height',(windowHeight-400));
			
			$(".canvasWidth").css('height',(windowHeight-400));
			$("#jobStatusGraphView").css("width",canvasParent);
			$("#jobAnalytics").css('height',(windowHeight-200));
			$("#applicationAnalytics").css('height',(windowHeight-200));
			
		}
	});
	
	function ChangeView(display,displayNone){
		$("#"+display).css("display","block");
		$("#"+displayNone).css("display","none");
		
	}
	
	
	$(document).on('click', '.view-table', function () {
        
        var table         = $(this).data('options').table;
        var chart         = $(this).data('options').chart;
		var ChartType     = $(this).data('options').chartType;
		tabularView(ChartType);
		$("#"+table).css("display","block");
		$("#"+chart).css("display","none");
    });

	$(document).on('click', '.view-graph', function () {
        
        var table         = $(this).data('options').table;
        var chart         = $(this).data('options').chart;
		var ChartType     = $(this).data('options').chartType;
		GraphView(ChartType);
		
    });

</script>	
<?php
if (isset($job) && $job != NULL) {
    //echo '<pre>';
    //print_r($job);
}
//echo '<pre>'; print_r($jobApplication); exit;
?>



<script type="text/javascript">
    function flashObject(url, id, width, height, version, bg, flashvars, params, att)
    {
        var pr = '';
        var attpr = '';
        var fv = '';
        var nofv = 0;
        for(i in params)
        {
            pr += '<param name="'+i+'" value="'+params[i]+'" />';
            attpr += i+'="'+params[i]+'" ';
            if(i.match(/flashvars/ig))
            {
                nofv = 1;
            }
        }
        if(nofv==0)
        {
            fv = '<param name="flashvars" value="';
            for(i in flashvars)
            {
                fv += i+'='+escape(flashvars[i])+'&';
            }
            fv += '" />';
        }
        htmlcode = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"  codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=5,0,0,0" width="'+width+'" height="'+height+'">'
    +'  <param name="movie" value="'+url+'" />'+pr+fv
    +'  <embed src="'+url+'" width="'+width+'" height="'+height+'" '+attpr+'type="application/x-shockwave-flash" pluginspage="http://www.adobe.com/go/getflashplayer"></embed>'
    +'</object>';
        document.getElementById(id).innerHTML=htmlcode;
    }
</script>

<div id="page-content-wrapper">        	
    <div class="container-fluid whitebg">
       <div class="row">
           <div class="col-md-10"><h2 class="he_editjob"><img src="<?php echo base_url(); ?>/theme/firstJob/image/arrow_head.png" class="prev-page"><?php if(isset($job['title'])) echo $job['title'];?></h2></div>                
           <div class="col-md-2 ed_job_bt"><button type="button"><a href="<?php echo base_url(); ?>jobs/edit/<?php echo $job['id']?>">Edit Job</a></button></div>                   
           <div class="clearfix"></div>

           <div class="col-md-12">
               <div class="clearfix"></div>                        
               <div class="col-md-2 yrs_sty"> <img src="<?php echo base_url(); ?>/theme/firstJob/image/yrs.png"> <span><?php if(isset($job['expFrom']) && $job['expFrom'] !=NULL) echo $job['expFrom'] .' -';?>  <?php if(isset($job['expTo']) && $job['expTo']!=NULL ) echo $job['expTo']." yrs" ?></span></div>
               <div class="col-md-10 yrs_sty"> <img src="<?php echo base_url(); ?>/theme/firstJob/image/location.png"> <span><?php $e = 0;foreach($job['locations'] as $item): 
                   echo getLocationById($item['location']);
                   if($e != count($job['locations'])-1)   
                   echo ', ';
                   $e++;
                   endforeach;?></span>
               </div>                        
               <div class="clearfix"></div>                        
               <div class="col-md-2 points_sty"><p>No. of Vacancies: <span><?php if(isset($job['noOfVacancies'])) echo $job['noOfVacancies'];?></span> </p></div>
               <div class="col-md-2 points_sty"><p>End Date: <span><?php if(isset($job['openTill'])) echo date('Y/m/d',strtotime($job['openTill']));?></span>  </p></div>
               <div class="col-md-8 points_sty"><p>Salary: <span> <?php if(isset($job['salaryFrom']) && $job['salaryFrom'] != NULL) echo $job['salaryFrom'].' -';?>  <?php if(isset($job['salaryTo']) && $job['salaryTo'] != NULL) echo $job['salaryTo']." lacs";?></span> </p></div>
               <div class="clearfix"></div>
               <div class="col-md-12 no_padding_left">
               <p><?php if(isset($job['description'])) echo $job['description'];?></p>
               </div>                        
          </div>                   
       </div>
   </div>
    
    <div class="container-fluid whitebg">
        <div class="row">
            <div class="col-md-12"><h2>Interview Attempted</h2></div>

            <div class="clearfix"></div>

            <div class="col-md-12 rsp_tbl">                    
                <table class="table">
                <thead class="tbl_head">
                    <tr>
                        <th>Name</th>
                        <th>Job Status</th>
                        <th>Job Level</th>
                        <th>L1 Username</th>
                        <th>L2 Username</th>
                        <th>View Interview / Assessment</th>
                        <th>Comments</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $j=0;
                    foreach($jobApplication as $jobUser) {
                        $j++; ?>
                      <tr class="tbl_bg">
                        <td><?php echo $jobUser->AppUserName; ?></td>
                        <td><?php echo $jobUser->JobAplliedStatus; ?></td>
                        <td><?php echo $jobUser->JobLevel; ?></td>
                        <td><?php echo $jobUser->shorlistedByL1Username; ?></td>
                        <td><?php echo $jobUser->shorlistedByL2Username; ?></td>                                     
                        <td>
                            <?php 
                            if($jobUser->mergedVideo!='') { ?>
                            View                         
                            <a
                                href="javascript:" 
                                data-toggle="modal" 
                                data-target="#shareInterview" 
                                class="share-user-interview" 
                                data-options='{"userJob":"<?php echo $jobUser->userJobId; ?>", "appUserJob":"<?php echo $job['title']; ?>", "appUserName":"<?php echo $jobUser->AppUserName;?>", "interviewVideo":"<?php echo $jobUser->mergedVideo; ?>"}'
                            >Interview</a>
                            
                            <?php
                            }
                            else { 
                                $jobUser->userApplicationCreatedAt;
                                $futureTime = date('Y-m-d H:i:s', strtotime("+5 min", strtotime($jobUser->userApplicationCreatedAt)));
                                if(strtotime(date('Y-m-d H:i:s'))>strtotime($futureTime)){ ?>
                                    <p><a   href='javascript:void(0);'
                                            data-options='{"userId":"<?php echo $jobUser->id; ?>", "jobId":"<?php echo $jobUser->JobCode; ?>"}'
                                            id='generateVideo<?php echo $j; ?>' 
                                            class='generateVideo'>Generate Video</a></p>
                                    <?php
                                }
                                //echo strtotime(date('Y-m-d H:i:s'))."==".strtotime($futureTime);
                            }                            
                            ?>
                            /
                            <a
                            href="javascript:" 
                            data-toggle="modal" 
                            data-target="#viewUserProfileDetails" 
                            class="view-user-profile-details" 
                            data-options='{"userId":"<?php echo $jobUser->id; ?>", "jobId":"<?php echo $jobUser->jobId; ?>", "interviewVideo":"<?php echo $jobUser->mergedVideo; ?>"}'
                            >Assessment</a>
                        </td> 
                        <td></td> 
                      </tr>
                    <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    
    <?php if(count($jobApplication)>0) { echo $this->pagination->create_links(); } ?>
    
</div>




<script src="<?php echo base_url(); ?>theme/firstJob/js/chosen.jquery.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>theme/firstJob/js/prism.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
    $(".generateVideo").click(function () {        
        var userId      = $(this).data('options').userId;
        var jobId       = $(this).data('options').jobId;
        var recordId    = $(this).attr('id');   
        
        var formData    = new FormData();
        formData.append('jobId', jobId);
        formData.append('userId', userId);        
        formData.append('accessToken', '<?php echo $this->config->item('accessToken'); ?>');
        
        $(this).parent().html('Merging Video, Please Wait...'); 
        $('.view-user-profile-details').prop('disabled', true);
        
        $.ajax({
            url: siteUrl + 'jobs/generateVideo/',
            data: formData,
            async: false,
            contentType: false,
            processData: false,
            cache: false,
            type: 'POST',
            success: function (data) {
                if(data) {
                    setTimeout(function(){
                        location.reload();
                    }, 5000);
                }
            },
        });
        return false;
    });
    
    
    
    var config = {
        '.chosen-select': {},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }

    $(function () {
        $("#datepicker-19").datepicker();
    });

</script>
<script type="text/javascript">
    $(function(){
   var videoUrl = "uploads/jd/<?php echo $job['jd'] ?>";
    jwVideo(videoUrl, 'jdvideo');
    });
</script>









<style>
.modal-content {
    -webkit-box-shadow: 0 5px 15px rgba(0,0,0,.5);
    box-shadow: 0 5px 15px rgba(0,0,0,.5);
}

.modal-content {
    position: relative;
    background-color: #fff;
    -webkit-background-clip: padding-box;
    background-clip: padding-box;
    border: 1px solid #999;
    border: 1px solid rgba(0,0,0,.2);
    border-radius: 6px;
    outline: 0;
    -webkit-box-shadow: 0 3px 9px rgba(0,0,0,.5);
    box-shadow: 0 3px 9px rgba(0,0,0,.5);
    width: 920px;
    margin-left:-23% !important;
}
</style>


<!-- PopUp for view user's profile  -->
<div id="viewUserProfileDetails" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">User Assessment Details</h4>
            </div>
            <div class="modal-body" id="assessmentDetail">                                      
                <table class="table">
                    <thead class="tbl_head">
                        <tr>
                            <th>Assessment Details</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).on('click', '.view-user-profile-details', function () {
        var userId          = $(this).data('options').userId;
        var jobId           = $(this).data('options').jobId;
        var interviewVideo  = $(this).data('options').interviewVideo;
        var accessToken     = '<?php echo $this->config->item('accessToken'); ?>';
        $("#assessmentDetail").html('');
        
        formData    = {userId:userId, jobId:jobId, accessToken:accessToken, interviewVideo:interviewVideo};
        $.ajax({
            url : siteUrl + "jobs/userAssessmentDetail",
            type: "POST",
            data : formData,
            cache: false,
            success: function(result)
            {
                $("#assessmentDetail").html(result);
            }
        });
    });
</script>





<!-- PopUp for view share's profile  -->
<div id="shareInterview" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Share User Interview Video</h4>
            </div>
            <div class="modal-body" id="share">                                      
                <form method="post" id="shareInterviewForm">
                <p id="errMessage"></p>
                <table class="table" border="0" bordercolor="#CCCCCC">
                    <thead class="tbl_head">
                        <tr>
                            <th colspan="3">Add Email Id to Share</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td valign="middle"><label for="inputEmail" class="control-label" style="font-weight:500">Email Id</label></td>
                            <td><input type="text" class="form-control receiverEmail"  id="receiverEmail1" placeholder="enter a valid email id"></td>
                            <td rowspan="5" align="right" width="40%">
                                <span id="userVideo"></span>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="inputEmail" class="control-label" style="font-weight:500">Email Id</label></td>
                            <td><input type="text" class="form-control receiverEmail"  id="receiverEmail2" placeholder="enter a valid email id"></td>
                        </tr>
                        <tr>
                            <td><label for="inputEmail" class="control-label" style="font-weight:500">Email Id</label></td>
                            <td><input type="text" class="form-control receiverEmail"  id="receiverEmail3" placeholder="enter a valid email id"></td>
                        </tr>
                        <tr>
                            <td><label for="inputEmail" class="control-label" style="font-weight:500">Email Id</label></td>
                            <td><input type="text" class="form-control receiverEmail"  id="receiverEmail4" placeholder="enter a valid email id"></td>
                        </tr>
                        <tr>
                            <td><label for="inputEmail" class="control-label" style="font-weight:500">Email Id</label></td>
                            <td><input type="text" class="form-control receiverEmail"  id="receiverEmail5" placeholder="enter a valid email id"></td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <span id="formButton"><button type="button" class="Save_frm" id="sendInterview">Share Interview</button></span>
                            </td>
                        </tr>
                    </tbody>
                </table>
                </form>
            </div>
        </div>
    </div>
</div>







<script>
    $(document).on('click', '.share-user-interview', function () {              
        $("#formButton").html('<button type="button" class="Save_frm" id="sendInterview">Share Interview</button>');
        document.getElementById("shareInterviewForm").reset();
        var userJob         = $(this).data('options').userJob;
        var interviewVideo  = $(this).data('options').interviewVideo;
        var accessToken     = '<?php echo $this->config->item('accessToken'); ?>';
        var appUserName     = $(this).data('options').appUserName;
        var appUserJob      = $(this).data('options').appUserJob;
        
        $("#errMessage").html('<h3 style="margin-top: 0px; margin-bottom: 0px;"><center>'+appUserName+'</center></h3>');  
        $("#userVideo").html('');
                
        formData    = {userJob:userJob, appUserName:appUserName, appUserJob:appUserJob, accessToken:accessToken, interviewVideo:interviewVideo};
        $.ajax({
            url : siteUrl + "jobs/userVideo",
            type: "POST",
            data : formData,
            cache: false,
            success: function(result)
            {
                $("#userVideo").html(result);
            }
        });
    });
    
    
    $(document).on('click', '#sendInterview', function(e) {
        // Prevent form submission
        e.preventDefault();
        
        $("#formButton").html('please wait ...');
        var email1      = $("#receiverEmail1").val();
        var email2      = $("#receiverEmail2").val();
        var email3      = $("#receiverEmail3").val();
        var email4      = $("#receiverEmail4").val();
        var email5      = $("#receiverEmail5").val();
        var videoName   = $("#videoName").val();
        var userJob     = $("#userJob").val();
        var appUserName = $("#appUserName").val();
        var appUserJob  = $("#appUserJob").val();
        var accessToken = '<?php echo $this->config->item('accessToken'); ?>';
        
        var formData    = new FormData();
        formData.append('email1', email1);
        formData.append('email2', email2);
        formData.append('email3', email3);
        formData.append('email4', email4);
        formData.append('email5', email5);
        formData.append('videoName', videoName);
        formData.append('userJob', userJob);
        formData.append('accessToken', accessToken);
        formData.append('appUserName', appUserName);
        formData.append('appUserJob', appUserJob);
        
        if(email1=='' && email2=='' && email3=='' && email4=='' && email5=='') {
            $("#formButton").html('Please fill at least 1 email id');
        }
        else {
            $("#formButton").html('Email Successfully send');      
            //$(".close").trigger("click");
            //setTimeout(function(){
                //$(".close").trigger("click");
            //}, 1000);
        }

        $.ajax({
            url: siteUrl + 'jobs/sendInterview/',
            data: formData,
            async: false,
            contentType: false,
            processData: false,
            cache: false,
            type: 'POST',
            success: function (data) {
            },
        });
        return false;
    });
</script>
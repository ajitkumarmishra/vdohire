<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Userassessment Model
 * Description : Handle all the CRUD operation for Userassessment
 * @author Synergy
 * @createddate : Dec 22, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 23, 2017
 */

class Userassessment_model extends CI_Model {

    var $table = "fj_userAssessement";

    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library("session");
        $this->load->library('pagination');
    }

    /**
     * countForUserInterview function
     * Get all users for interview
     * @param int $id.
     * @return array the count of invited jobs.
     */
    function countForUserAssessment($assessmentId) {
        
        try {
            
            $query = $this->db->select("count(id) as assessmentcount")
                                ->from($this->table)
                                ->where('assessmentId =', $assessmentId)
                                ->where('status !=', 3)
                                ->get();
            
            $result = $query->row_array();
            if (!$result) {
                throw new Exception('Unable to fetch user assessment data.');
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
}
?>



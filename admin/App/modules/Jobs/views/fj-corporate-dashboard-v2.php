<?php
$ci =&get_instance();
$ci->load->model('userjob_model');
$ci->load->model('user_model');
$ci->load->helper('fj');

//permission check 


//till here permission check

$userjobpendingcount = 0;
$userjobNewCount = 0;
$userjobpendingcountL1 = 0;
$userjobshortlistedcountL1 = 0;
$userjobshortlistedcountL2 = 0;
$userjobrejectedcountL1 = 0;
$userjobrejectedcountL2 = 0;
$totalApplications = 0;
$userjobshortlistedcount = 0;
$userjobrejectedcount = 0;
$userJobRoleFor = $userRole;
foreach($content as $jobs){
	$UserJobRole = $ci->userjob_model->getUserJobPanel($jobs->id,$AdminLoggedInuserId);

	if($UserJobRole) {
		if($UserJobRole[0]['level'] == 1){
			$userJobRoleFor = 4;
		}else if($UserJobRole[0]['level'] == 2){
			$userJobRoleFor = 5;
		}else{
			$userJobRoleFor = 2;
		}
	} else {
		$userJobRoleFor = 2;
	}
	
	
	$userjobpendingcountresL1     = $ci->userjob_model->countForUserJobs($jobs->id, 1);
	$userjobpendingcountL1        = $userjobpendingcountL1 + $userjobpendingcountresL1['appliedCount'];

	$userjobNewcountres     	= $ci->userjob_model->countForUserJobsNew($jobs->id, 1);   
	$userjobNewCount    		= $userjobNewCount + $userjobNewcountres['appliedCount'];   
	
	$userjobNewcountresForL2    = $ci->userjob_model->countForUserJobsNew($currentJob, 2);   
	$userjobNewCountForL2    	= $userjobNewcountresForL2['appliedCount'];   
							
	
	$userjobshortlistedcountresL1 = $ci->userjob_model->countForUserJobs($jobs->id, 2);   
	$userjobshortlistedcountL1    = $userjobshortlistedcountL1 + $userjobshortlistedcountresL1['appliedCount'];
	
	$userjobshortlistedcountresL2 = $ci->userjob_model->countForUserJobs($jobs->id, 4);   
	$userjobshortlistedcountL2    = $userjobshortlistedcountL2 + $userjobshortlistedcountresL2['appliedCount'];                   
	
	
	
	
	$userjobrejectedcountresL1   = $ci->userjob_model->countForUserJobs($jobs->id, 3);   
	$userjobrejectedcountL1       = $userjobrejectedcountL1 + $userjobrejectedcountresL1['appliedCount'];
	
	$userjobrejectedcountresL2    = $ci->userjob_model->countForUserJobs($jobs->id, 5);   
	$userjobrejectedcountL2       = $userjobrejectedcountL2 + $userjobrejectedcountresL2['appliedCount'];
	//L1 user
	if($userJobRoleFor == 4){
		$userjobrejectedcount 		= $userjobrejectedcountL1;
		$userjobshortlistedcount 	= $userjobshortlistedcountL1;
		$userjobpendingcount        = $userjobpendingcountL1;
		$NewApplicantsReceived     	= $userjobNewCount;
	}else if($userJobRoleFor == 5){
		$userjobrejectedcount 		= $userjobrejectedcountL2;
		$userjobshortlistedcount 	= $userjobshortlistedcountL2;
		$userjobpendingcount        = $userjobshortlistedcountL1;
		$NewApplicantsReceived     	= $userjobNewCountForL2;
		
	}else{
		$userjobrejectedcount 		= $userjobrejectedcountL2+$userjobrejectedcountL1;
		$userjobshortlistedcount 	= $userjobshortlistedcountL2+$userjobshortlistedcountL1;
		$userjobpendingcount        = $userjobpendingcountL1;
		$NewApplicantsReceived		= $userjobNewCount;
	}
	
	
}

if($userJobRoleFor == 4){
	$totalApplications = $userjobrejectedcount+$userjobshortlistedcount+$userjobNewCount+$userjobpendingcount;
}else if($userJobRoleFor == 5){
	$totalApplications = $userjobrejectedcount+$userjobshortlistedcount+$userjobpendingcount+$NewApplicantsReceived;
}else{
	$totalApplications = $userjobrejectedcount+$userjobshortlistedcount+$userjobNewCount+$userjobpendingcount;
}

?>
<style>
.canvasjs-chart-canvas{
	border:1px solid black;
}
h3{
	font-size:18px;
}
#mceu_15{
	display:none;
}
</style>


<div class="container-fluid" style="margin-top: 50px;     padding: 13px;   padding-bottom: 3px;background: #054F72;padding-left: 18px;">
<div class="row">

	<div style="float:left;font-size: 22px;color: white;">Recruiter Dashboard</div>
	<?php if(in_array('1', $userPermissionArr)|| ($userRoleForMenu == 2)){ ?>
	<div style="float:left;    margin-left: 21%;   width: 24%;">
		<form action ="<?php echo base_url() ?>jobs/search" name="searchForm" method="post" id="searchForm">
		<input class="form-control" type="" id="searchText" style="float:left;" name="search" value="" placeholder="Search Job, Applicants">
		
		<button onclick="searchResult()" id="searchButton" style="position: absolute;right: 44%;margin-top: 3px;float:right;border: none;background: none;padding-top: 6px;"><i class="fa fa-search" aria-hidden="true"></i></button>
		</form>
		<div style="clear:both"></div>
	</div>
	<?php } ?>
	<!--<div style="float:left;    padding: 12px;margin-left: 2%;    padding-top: 8px;"><a href="#" style="color:white;"><u>Advance Search</u></a></div>-->
	<div style="float: right;font-size: 18px;padding-top: 8px;color: white; padding-right: 6px;"><?php echo date("d M Y"); ?></div>
			<div style="clear:both"></div>

</div>
</div>
<div class="container-fluid" >
<div class="row" style="    border: 1px solid black;margin-top: 11px;padding: 10px;    padding: 6px;">
	<div style="float:left; font-size: 18px;    padding-top: 5px;padding-left: 21px;font-weight:bold;" id="valueOfNew">
	
	</div>
	<div style="background-color: #054f72;    float: right;">
	<?php if(in_array('3', $userPermissionArr) || ($userRoleForMenu == 2)){ ?>
	<a href="<?php echo base_url() ?>queue/list" class="btn" style="color:white;">Review Applications Now!</a>
	<?php } ?>
	</div>
			<div style="clear:both"></div>
</div>
</div>


<div class="container-fluid" >
<div class="row manageHeight">

<div class="col-md-3 pull-left" style=" margin-top: 11px;">
    <div style="width:100%;">
		<div class="col-md-5 pull-left" style="background:rgb(220, 148, 41);color:white;    padding-left: 0px;padding-right: 0px;    font-size: 18px;">
			<div style=" padding-left: 0px;">
			<div>
				<div class="pull-left" id="totalapplicationcountTest" style="font-size: 40px;padding-left: 10px;"><?php echo $totalApplications; ?></div>
				<div class="pull-right" style="font-size: 40px;padding-right: 10px;margin-top: 10px;"><i class="fa fa-envelope" aria-hidden="true"></i>
				</div>
			</div>
			<div style="clear:both"></div>
			<div style="background:rgb(183, 129, 49);">Received</div>
			</div>
		</div>
		
		<div class="col-md-5 pull-left"  style="background:#686879;color:white;    padding-left: 0px;padding-right: 0px;   font-size: 18px;">
			<div style=" padding-left: 0px;">
			<div>
				<div class="pull-left" id="totalpendingcounttest" style="font-size: 40px;padding-left: 10px;"><?php echo $userjobpendingcount+$NewApplicantsReceived; ?></div>
				<div class="pull-right" style="font-size: 40px;padding-right: 10px;margin-top: 10px;"><i class="fa fa-spinner" aria-hidden="true"></i>
				</div>
			</div>
			<div style="clear:both"></div>
			<div style="background:#41414B;">New & Pending</div>
			</div>
			
		</div>
		<div style="clear:both"></div>
		<div class="col-md-5 pull-left"  style="background:#5CC36A;color:white;    padding-left: 0px;padding-right: 0px;        font-size: 18px;">
			<div style=" padding-left: 0px;">
			<div>
				<div class="pull-left" id="totalshortistedtest" style="font-size: 40px;padding-left: 10px;"><?php echo $userjobshortlistedcount; ?></div>
				<div class="pull-right" style="font-size: 40px;padding-right: 10px;margin-top: 10px;"><i class="fa fa-thumbs-up" aria-hidden="true"></i>
				</div>
			</div>
			<div style="clear:both"></div>
			<div style="background:#3EA84B;">Shortlisted</div>
			</div>
			
		</div>
		
		<div class="col-md-5 pull-left"  style="background:#EE6E3D;color:white;    padding-left: 0px;padding-right: 0px;    font-size: 18px;">
			<div style=" padding-left: 0px;">
			<div>
				<div class="pull-left" id="totalrejectedtest" style="font-size: 40px;padding-left: 10px;"><?php echo $userjobrejectedcount; ?></div>
				<div class="pull-right" style="font-size: 40px;padding-right: 10px;margin-top: 10px;"><i class="fa fa-thumbs-down" aria-hidden="true"></i>
				</div>
			</div>
			<div style="clear:both"></div>
			<div style="background:#C44211;">Rejected</div>
			</div>
			
		</div>
		
    </div>

</div>
<div class="col-md-5 pull-left" style="   margin-top: 11px;">
    <div style="width:100%;">
		<div class="col-md-12" style="color:white;    font-size: 32px;">
			<?php if(in_array('2', $userPermissionArr) || ($userRoleForMenu == 2) ){ ?>
			<a href="<?php echo base_url() ?>jobs/add" class="btn" name="button" style="background: #054f72;font-size: 14px;     color: white;border-radius: 0;   padding: 6px 12px;">Add Job</a>
			<?php } ?>
			<?php if(in_array('2', $userPermissionArr)|| ($userRoleForMenu == 2)){ ?>
			<a class="btn sendInvitelink"  data-toggle="modal" data-target="#JobList"  name="button" style="background: #054f72;    color: white;border-radius: 0;    padding: 6px 12px;font-size: 14px;">Send Job Invitation</a>
			<?php } ?>
		</div>
		
		<div class="col-md-12" style=" font-size: 16px;">
			<h3>Active Applications<span id="activeApplications"></span></h3>
			<div id="activeApplicationsTable" class="table-responsive" style="height: 149px;">
				
				<table class="table table-hover table-striped" style="     font-size: 13px;   border: none;">
					<?php
					$activeApplications = 0;
					if($content){ ?>
						<tr style="    background:#eee;color: black;">
							<th>
								Job Title
							</th>
								<th>
								Applicant Name
							</th>
							<th>
								Status
							</th>
							<th>
								Last Action At
							</th>
						</tr>
						<?php 
						//echo "<pre>";print_r($content);die;
						foreach ($content as $item){ 
							$UserJobRole = $ci->userjob_model->getUserJobPanel($item->id,$AdminLoggedInuserId);
							if($UserJobRole) {
								if($UserJobRole[0]['level'] == 1){
									$userJobRoleFor = 4;
								}else if($UserJobRole[0]['level'] == 2){
									$userJobRoleFor = 5;
								}
							}
							
							
							$userlistofjobActive = $ci->userjob_model->listForUserJobsNewActive($item->id,$userJobRoleFor);  
							
							if(!empty($userlistofjobActive)){
									//echo $item->id."<br>";
									foreach($userlistofjobActive as $applicant){
										
										$activeApplications++;
										if($applicant['status'] == 1){
											$status = " Pending ";
											$recruiterName = "";
										}else if($applicant['status'] == 2){
											$getRecruiterDetails = $ci->user_model->getUser($applicant['shorlistedByL1User']);
											$recruiterName = $getRecruiterDetails['fullname'];
											$status = " Shortlisted by L1 ";
										}else if($applicant['status'] == 3){
											$status = " Rejected by L1 ";
											$getRecruiterDetails = $ci->user_model->getUser($applicant['shorlistedByL1User']);
											$recruiterName = $getRecruiterDetails['fullname'];
											
										}else if($applicant['status'] == 4){
											$status = " Shortlisted by L2 ";
											$getRecruiterDetails = $ci->user_model->getUser($applicant['shorlistedByL2User']);
											$recruiterName = $getRecruiterDetails['fullname'];
											
										}else if($applicant['status'] == 5){
											$status = " Rejected by  L2";
											$getRecruiterDetails = $ci->user_model->getUser($applicant['shorlistedByL2User']);
											$recruiterName = $getRecruiterDetails['fullname'];
										}
										
										$joBid = encryptURLparam($item->id, URLparamKey);
										$useId = encryptURLparam($applicant['userId'], URLparamKey);
										
										
								?>		
										<tr>
											<td><?php echo $item->title ?></td>
											<td>
											<a href="<?php echo base_url()."queue/list?JobId=".$joBid."&userId=".$useId."&query=search" ?>"><?php echo $applicant['fullname'] ?></a>
											
											</td>
											<td><?php echo $status ?> <?php echo ($recruiterName != "")?"(".$recruiterName.")":"" ?></td>
											<td><?php echo $applicant['updatedAt'] ?></td>
										</tr>
										
									<?php 
									}
									
									?>
									
							<?php
							}
							
						}
						
					}
					?>
					<?php
					if(count($content) == 0){ ?>
						
						<tr>
							<td colspan="3">No Applications</td>
						</tr>
						
					<?php }
					
					
					?>
					
				</table>
			</div>
			<input type="hidden" name="activeApplications" id="activeApplicationsValue" value="<?php echo $activeApplications ?>">
		</div>
		
		
    </div>

</div>
<div class="col-md-4 pull-left" style="  margin-top: 55px; ">
    <div style="width:100%;">
	<div class="col-md-12" style=" font-size: 16px;">
	<h3>New Applications<span id="NewApplicationsText"></span></h3>
	<input type="hidden" id="NewApplicationsTextValue">
			<div id="NewApplicationsTables" class="table-responsive" style="height: 149px;">
				<table class="table table-hover table-striped" id="NewApplicationsTable" style="font-size: 13px; border: none;">
					<?php
					$totalNewApplications = 0;
					$applicationFound = 0;
					if( $content ){ ?>
						<tr style="    background: #eee;color: black;">
							<th>
								Job Title
							</th>
								<th>
								Applicant Name
							</th>
							<th>
								Date time
							</th>
						</tr>
						<?php
												
						$maxUserJoblistId = 0;
						foreach ($content as $item){
							$UserJobRole = $ci->userjob_model->getUserJobPanel($item->id,$AdminLoggedInuserId);
							if($UserJobRole) {
								if($UserJobRole[0]['level'] == 1){
									$userJobRoleFor = 4;
								}else if($UserJobRole[0]['level'] == 2){
									$userJobRoleFor = 5;
								}
							}
							
							
							$userlistofjob = $ci->userjob_model->listForUserJobsNewOnly($item->id,$userJobRoleFor);  
							if(!empty($userlistofjob) > 0){ 
								$applicationFound = 1;
							?>
								
								<!--changes-->
								<?php 
								if($userlistofjob[0]['id'] > $maxUserJoblistId) {
									$maxUserJoblistId = $userlistofjob[0]['id'];
								}
								?>
								<!--changes-->

								<?php 
									foreach($userlistofjob as $applicant){
										
										$totalNewApplications++;
										$joBid = encryptURLparam($item->id, URLparamKey);
										$useId = encryptURLparam($applicant['userId'], URLparamKey);
								?>		
										<tr>
											<td><?php echo $item->title ?></td>
											<td>
											
												<a href="<?php echo base_url()."queue/list?JobId=".$joBid."&userId=".$useId."&query=search" ?>"><?php echo $applicant['fullname'] ?></a>
											
											</td>
											<td><?php echo $applicant['createdAt'] ?></td>
											
										</tr>
										
									<?php
									}
									?>
									
							<?php
							}else{ ?>
								
								
							<?php }
						}?>
						<input type="hidden" id="newApplicationLastId" value="<?php echo $maxUserJoblistId;?>">
						<?php 
					}
					?>
					</table>
					<table id="NoNewApplicationsFound">
					<?php
					if($applicationFound == 0){ ?>
						
						<tr>
							<td colspan="3">No Applications Found</td>
						</tr>
						
					<?php }
					
					
					?>
					</table>
					
				
			</div>
			<input type="hidden" name="totalNewApplications" id="totalNewApplications" value="<?php echo $totalNewApplications ?>">
		</div>
    </div>

</div>


	<div style="clear:both"></div>
<div id="middleSection" class="col-md-12" style="">
<div class="col-md-3 pull-left" style="    padding-left: 0px;">
    <div style="width:100%;">
		<div class="col-md-12" style=" font-size: 16px;">
			<h3>Evaluation Performance<span id="NewApplicationsText1"><?php echo "  (".count($content).")" ?></span></h3>
			
			<div id="EvaluationPerformanceTable" class="table-responsive" style="    height: 149px;">
			<table class="table table-hover table-striped" style="     font-size: 13px;   border: none;">
			
				<?php
				if( $content ){?>
				<tr style="    background: #eee;color: black;">
							<th>
								Job Title
							</th>
								<th>
								Applications
							</th>
							
						</tr>
                    <?php 
					$totalEvaluationPerformance = 0;
					foreach ($content as $item){
						$UserJobRole = $ci->userjob_model->getUserJobPanel($item->id,$AdminLoggedInuserId);
						
						if($UserJobRole) {	
							if($UserJobRole[0]['level'] == 1){
								$userJobRoleFor = 4;
							}else if($UserJobRole[0]['level'] == 2){
								$userJobRoleFor = 5;
							}else{
								$userJobRoleFor = 2;
							}
						} else {
							//$userJobRoleFor = 2;
						}
						
						$userlistofjob = $ci->userjob_model->listForUserJobsNew($item->id,$userJobRoleFor);  
						if(!empty($userlistofjob) > 0){
							
						
						?>
							<tr>
								<td style="width:40%;padding-bottom:10px; "><?php echo $item->title ?></td>
								<td>
								<div class="evaluationPerformanceClassMarquee" id="EvaluationLi<?php echo $item->id ?>"  style="display: inline-block;width: 154px;white-space: nowrap;overflow: hidden !important;text-overflow: ellipsis;">
							<?php 
							
								foreach($userlistofjob as $applicant){
									if($userJobRoleFor == 4){
										if($applicant['status'] == 1){
											$status = " Pending ";
											$recruiterName = "";
											$background = " background:#b981cc; ";
											
										}else if($applicant['status'] == 2){
											$getRecruiterDetails = $ci->user_model->getUser($applicant['shorlistedByCorporateForL1User']);
											$recruiterName = $getRecruiterDetails['fullname'];
											$status = " Shortlisted by ";
											$background = " background:#5CC36A; ";
											
										}else if($applicant['status'] == 3){
											$status = " Rejected by ";
											$getRecruiterDetails = $ci->user_model->getUser($applicant['shorlistedByCorporateForL1User']);
											$recruiterName = $getRecruiterDetails['fullname'];
											$background = " background:#EE6E3D; ";
											
										}else if($applicant['status'] == 4){
											$status = " Shortlisted by ";
											$getRecruiterDetails = $ci->user_model->getUser($applicant['shorlistedByCorporateForL2User']);
											$recruiterName = $getRecruiterDetails['fullname'];
											$background = " background:#5CC36A; ";
											
										}else if($applicant['status'] == 5){
											$status = " Rejected by ";
											$getRecruiterDetails = $ci->user_model->getUser($applicant['shorlistedByCorporateForL2User']);
											$recruiterName = $getRecruiterDetails['fullname'];
											$background = " background:#EE6E3D; ";
										}
									}else if($userJobRoleFor == 5){
										
										if($applicant['status'] == 2){
											$status = " Pending ";
											$getRecruiterDetails = $ci->user_model->getUser($applicant['shorlistedByCorporateForL2User']);
											$recruiterName = $getRecruiterDetails['fullname'];
											$background = " background:#b981cc; ";
											
										}else if($applicant['status'] == 3){
											$status = " Pending ";
											$getRecruiterDetails = $ci->user_model->getUser($applicant['shorlistedByCorporateForL2User']);
											$recruiterName = $getRecruiterDetails['fullname'];
											$background = " background:#b981cc; ";
											
										}else if($applicant['status'] == 4){
											$status = " Shortlisted by ";
											$getRecruiterDetails = $ci->user_model->getUser($applicant['shorlistedByCorporateForL2User']);
											$recruiterName = $getRecruiterDetails['fullname'];
											$background = " background:#5CC36A; ";
											
										}else if($applicant['status'] == 5){
											$status = " Rejected by ";
											$getRecruiterDetails = $ci->user_model->getUser($applicant['shorlistedByCorporateForL2User']);
											$recruiterName = $getRecruiterDetails['fullname'];
											$background = " background:#EE6E3D; ";
										}
									}else{
										if($applicant['status'] == 1){
											$status = " Pending ";
											$recruiterName = "";
											$background = " background:#b981cc; ";
											
										}else if($applicant['status'] == 2){
											$getRecruiterDetails = $ci->user_model->getUser($applicant['shorlistedByCorporateForL1User']);
											$recruiterName = $getRecruiterDetails['fullname'];
											$status = " Shortlisted by ";
											$background = " background:#5CC36A; ";
											
										}else if($applicant['status'] == 3){
											$status = " Rejected by ";
											$getRecruiterDetails = $ci->user_model->getUser($applicant['shorlistedByCorporateForL1User']);
											$recruiterName = $getRecruiterDetails['fullname'];
											$background = " background:#EE6E3D; ";
											
										}else if($applicant['status'] == 4){
											$status = " Shortlisted by ";
											$getRecruiterDetails = $ci->user_model->getUser($applicant['shorlistedByCorporateForL2User']);
											$recruiterName = $getRecruiterDetails['fullname'];
											$background = " background:#5CC36A; ";
											
										}else if($applicant['status'] == 5){
											$status = " Rejected by ";
											$getRecruiterDetails = $ci->user_model->getUser($applicant['shorlistedByCorporateForL2User']);
											$recruiterName = $getRecruiterDetails['fullname'];
											$background = " background:#EE6E3D; ";
										}
									}
									$joBid = encryptURLparam($item->id, URLparamKey);
									$useId = encryptURLparam($applicant['userId'], URLparamKey);
									
							?>	
							
									<div onclick="redirectTo('<?php echo base_url()."queue/list?JobId=".$joBid."&userId=".$useId."&query=search" ?>')" class="" title="<?php echo $applicant['fullname']." -".$status." ".$recruiterName ?>" style="display: inline-block;height: 13px;width: 22px;margin-top: 2px; cursor:pointer; margin-right: 4px;<?php echo $background; ?>">
									</div>
									
							

								<?php
									$totalEvaluationPerformance++;
								}
								?>
									<div style="clear:both"></div>
									</div>
								</td>
							</tr>
						<?php
						}
					}
				}
				?>
				<?php
					if(count($content) == 0){ ?>
						
						<tr>
							<td colspan="3">No Jobs</td>
						</tr>
						
					<?php }
					
					
					?>			
			</table>
			<input type="hidden" name="totalEvaluationPerformance" id="totalEvaluationPerformance" value="<?php echo $totalEvaluationPerformance ?>">
			
		</div>
		<table class="col-md-12" style="">
			<tr>
				<td class="pull-left" style="  font-size: 12px;padding-top: 20px;margin-right:3px;">
					<div class="pull-left" style="    height: 13px;width: 22px;margin-top: 2px; background: #27acbf;"></div>
					<div class="pull-left" style="    margin-left: 1px;">New</div>
					<div style="clear:both"></div>
				</td>
			<td class="pull-left" style="  font-size: 12px;padding-top: 20px;margin-right:3px;">
				<div class="pull-left" style="    height: 13px;width: 22px;margin-top: 2px;background: #b981cc;"></div>
				<div class="pull-left" style="    margin-left: 1px;">Pending</div>
				<div style="clear:both"></div>
			</td>
			
			<td class="pull-left" style=" font-size: 12px;padding-top: 20px;margin-right:3px;">
				<div class="pull-left" style="    height: 13px;width: 22px;margin-top: 2px;background: #5CC36A;"></div>
				<div class="pull-left" style="    margin-left: 1px;">Shortlisted</div>
				<div style="clear:both"></div>
			</td>
			<td class="pull-left" style="font-size: 12px;padding-top: 20px;margin-right:3px;">
				<div class="pull-left" style="    height: 13px;width: 22px;margin-top: 2px;background: #EE6E3D;"></div>
				<div class="pull-left" style="    margin-left: 1px;">Rejected</div>
				<div style="clear:both"></div>
			</td>
			</tr>
		</table>
    </div>
    </div>

</div>

<div class="col-md-5 pull-left" style="">
    <div style="width:100%;">
	<div class="col-md-12" style=" font-size: 16px;">
	<h3>Latest Actions<span id="latestActinText"></span></h3>
			<div id="latestActionsTable" class="table-responsive" style="height:149px;">
				<table class="table table-hover table-striped" style="     font-size: 13px;   border: none;">
					<?php
					$latestActinValue = 0;
					if( $content ){ ?>
						<tr style="    background: #eee;color: black;">
							<th>
								Job Title
							</th>
								<th>
								Recruiter Name
							</th>
							</th>
								<th>
								Action
							</th>
							<th>
								Date time
							</th>
							
						</tr>
						<?php foreach ($content as $item){
							
							$UserJobRole = $ci->userjob_model->getUserJobPanel($item->id,$AdminLoggedInuserId);
							if($UserJobRole) {
								if($UserJobRole[0]['level'] == 1){
									$userJobRoleFor = 4;
								}else if($UserJobRole[0]['level'] == 2){
									$userJobRoleFor = 5;
								}
							}
							
							$userlistofjob = $ci->userjob_model->listForUserJobsNewLatestAction($item->id,$userJobRoleFor);
							
							if(!empty($userlistofjob) > 0){
								
							?>
								
								<?php 
									foreach($userlistofjob as $applicant){
										if($applicant['status'] == 1){
											$status = " Pending ";
											$recruiterName = "";
										}else if($applicant['status'] == 2){
											$getRecruiterDetails = $ci->user_model->getUser($applicant['shorlistedByL1User']);
											$recruiterName = $getRecruiterDetails['fullname'];
											$status = " Shortlisted by L1 ";
										}else if($applicant['status'] == 3){
											$status = " Rejected by L1 ";
											$getRecruiterDetails = $ci->user_model->getUser($applicant['shorlistedByL1User']);
											$recruiterName = $getRecruiterDetails['fullname'];
											
										}else if($applicant['status'] == 4){
											$status = " Shortlisted by L2 ";
											$getRecruiterDetails = $ci->user_model->getUser($applicant['shorlistedByL2User']);
											$recruiterName = $getRecruiterDetails['fullname'];
											
										}else if($applicant['status'] == 5){
											$status = " Rejected by  L2";
											$getRecruiterDetails = $ci->user_model->getUser($applicant['shorlistedByL2User']);
											$recruiterName = $getRecruiterDetails['fullname'];
										}
										$latestActinValue++;
										$joBid = encryptURLparam($item->id, URLparamKey);
										$useId = encryptURLparam($applicant['userId'], URLparamKey);
										
								?>		
										<tr>
											<td><?php echo $item->title ?></td>
											<td><?php echo $recruiterName ?></td>
											<td><?php echo $status ?> <?php
											if($recruiterName != ""){ ?>
												
													<a href="<?php echo base_url()."queue/list?JobId=".$joBid."&userId=".$useId."&query=search" ?>">(<?php echo $applicant['fullname'] ?>)</a>
												
											<?php } ?>
											</td>
											<td><?php echo $applicant['updatedAt'] ?></td>
											
										</tr>
									<?php
									}
									?>
									
							<?php
							}
						}
					}
					?>
					<?php
					if(count($content) == 0){ ?>
						
						<tr>
							<td colspan="3">No Applications</td>
						</tr>
						
					<?php }
					
					
					?>
				</table>
			</div>
			<input type="hidden" name="latestActinValue" id="latestActinValue" value="<?php echo $latestActinValue ?>">
		</div>
		</div>

</div>

<div class="col-md-4 pull-left" style="">
    <div style="width:100%;">
	<div class="col-md-12" style=" font-size: 16px;">
	<h3>Pending Applications<span id="totalPendingApplicationsText"></span></h3>
			<div id="pendingApplicationsTable" class="table-responsive" style="    height: 149px;">
				<table class="table table-hover table-striped" style="    font-size: 13px;    border: none;">
					<?php
					$totalPendingApplications = 0;
					if( $content ){ ?>
						<tr style="    background: #eee;color: black;">
							<th>
								Job Title
							</th>
								<th>
								Applicant Name
							</th>
							<th>
								Date time
							</th>
							
						</tr>
						<?php foreach ($content as $item){
								$UserJobRole = $ci->userjob_model->getUserJobPanel($item->id,$AdminLoggedInuserId);
								
								if($UserJobRole) {
									if($UserJobRole[0]['level'] == 1){
										$userJobRoleFor = 4;
									}else if($UserJobRole[0]['level'] == 2){
										$userJobRoleFor = 5;
									}else{
										$userJobRoleFor = 2;
									}
								}
								
							$userlistofjob = $ci->userjob_model->listForUserJobsNewLatest($item->id,$userJobRoleFor);  
							if(!empty($userlistofjob) > 0){ 
							
							?>
								
								<?php 
									foreach($userlistofjob as $applicant){
										$totalPendingApplications++;
										$joBid = encryptURLparam($item->id, URLparamKey);
										$useId = encryptURLparam($applicant['userId'], URLparamKey);
								?>		
										<tr>
											<td><?php echo $item->title ?></td>
											<td>
												<a href="<?php echo base_url()."queue/list?JobId=".$joBid."&userId=".$useId."&query=search" ?>"><?php echo $applicant['fullname'] ?></a>
											</td>
											<td><?php echo $applicant['createdAt'] ?></td>
											
										</tr>
										
									<?php
									}
									?>
									
							<?php
							}
						}
					}
					?>
					<?php
					if(count($content) == 0){ ?>
						
						<tr>
							<td colspan="3">No Applications</td>
						</tr>
						
					<?php }
					
					
					?>
				
				</table>
			</div>
			<input type="hidden" name="totalPendingApplications" id="totalPendingApplications" value="<?php echo $totalPendingApplications ?>">
		</div>
    </div>

</div>
</div>



<div style="clear:both"></div>
</div>
</div>


<div id="JobList" class="modal fade" role="dialog">
    <div class="modal-dialog" style="margin-top:3%;">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <ul class="nav nav-tabs applicants-tabs">
						<li id="panellevelformid" class="active" style="cursor:pointer;color:black;    font-size: 16px;">
							<a data-toggle="tab" href="#selectJob" aria-expanded="false" style="color:black;">
								Select Job
							</a>
						</li>
						<li id="searchpanel" style="color:black;    font-size: 16px;" id="interviewformid" >
							<a  style="color:black;">
								Send Invitations
							</a>
						</li>
						
						
				</ul>
				
				
				
				
				
            </div>
            <div class="modal-body"  style="    height: 600px;overflow-y: scroll;">
				<div class="tab-content style-4" style="border-top: 1px solid grey; background: white;overflow-y: scroll;">
					<div id="selectJob" class="tab-pane fade active in">
					<table class="table table-hover table-striped" style="    font-size: 13px;    border: none;">
					<tr style="    background: #f9f9f9;color: black;">
						<th>
							Job Title
						</th>
							<th>
							Expires on
						</th>
						<th>
							Action
						</th>
						
					</tr>
				<?php
				foreach ($content as $item){
				?>
					<tr>
						<td><?php echo $item->title ?></td>
						<td><?php echo (isset($item->openTill) && ($item->openTill != '0000-00-00 00:00:00')) ? $item->openTill : 'NA' ?></td>
						<td>
						<a title="Invite users" data-toggle="tab" href="#sendInvitations"  aria-expanded="false"  href="javascript:" style="margin-right: 18px;" class="invite-users" element="<?php echo $item->title ?>"  id="<?php echo $item->id; ?>"><i class="fa fa-users" aria-hidden="true" style="color: #777;"></i></a>
						</td>
						
					</tr>
					
				<?php } ?>
				</table>
				
					</div>
					<div id="sendInvitations" class="tab-pane fade">
						 <!-- Modal content-->
        <form class="form-horizontal form_save" action="<?php echo base_url(); ?>assessments/addToAssessment"  method="post" id="interviewQuestionForm" name="interviewQuestionForm" >
            <div class="modal-content">

                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Invite Users</h4>
                    <h5 id="selectedJobs" class="modal-title" style="    font-weight: bold;padding-top: 10px;font-size: 16px;text-align: center;"></h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger hidden" id="inviteAlert">

                    </div>
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-3">Inivitation By</label>
                        <div class="col-xs-6">
                            <input type="checkbox" name="inviteFrom[]" id="inviteFrom_1" value="1" checked="" />&nbsp;Email&nbsp;&nbsp;
                            <input type="checkbox" name="inviteFrom[]" id="inviteFrom_2" value="2" />&nbsp;SMS
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-3">Upload File</label>
                        <div class="col-xs-8">
                            <input type="file" name="inviteUsers" id="inviteUsers" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-3"></label>
                        <div class="col-xs-4 aligncenter">
                            <?php
                            $fileCsv = base_url() . 'uploads/inviteUserFiles/inviteUsers' . $userId . '.csv';
                            $fileSamleCsv = base_url() . 'uploads/inviteUserFiles/inviteSampleFile.csv';
                            if ($fileEist == 1):
                                ?>
                                <a href="<?php echo $fileCsv; ?>"> Download File</a>
                            <?php else: ?>
                                <a href="<?php echo $fileSamleCsv; ?>"> Download Sample File</a>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-3">Customize Email</label>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-3">Email Subject</label>
                        <div class="col-xs-8">
                            <input type="text" class="form-control search-critera" name="emailSubject" id="emailSubject" maxlength="50" placeholder="Email Subject" data-validation="required" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-3">Email Message</label>
                        <div class="col-xs-8">
                            <script type="text/javascript" src="//cdn.tinymce.com/4/tinymce.min.js"></script>
                            <script>
                                tinymce.init({
                                    selector: '#emailMessage',
                                    height: 150,
                                    menubar:false,
                                    statusbar: false,
                                    toolbar: 'bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent |',
                                    content_css: [
                                      '//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
                                      '//www.tinymce.com/css/codepen.min.css'
                                    ]
                                });
                            </script>
                            <textarea type="text" class="form-control search-critera" name="emailMessage" id="emailMessage" maxlength="500" placeholder="Email Message"></textarea>
                            <input type="text" data-validation="emailMessage" style="height: 0px; width: 0px; visibility: hidden; " /> 
                        </div>                        
                    </div>

                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-4">Customize SMS message</label>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-3">Message</label>
                        <div class="col-xs-8">
                            <textarea class="form-control" rows="3" id="smsMessage" name="smsMessage" maxlength="200"></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3"></label>
                        <div class="col-xs-4 aligncenter">
                            <button type="button" class="btn btn-default upload-file" style="color : #fff; background-color: #054F72;">Send Invitation</button>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">


                    <!--                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
                </div>
            </div>
        </form>

					</div>
				</div>
            </div>
			<div class="modal-body" >
				<div style="clear:both"></div>
			</div>
        </div>
    </div>
</div>


<script>
if($(window).height() < 700){
	//$(".manageHeight").css("height","500px");
	//$(".manageHeight").css("overflow-y","scroll");
	
	
}
var totalPendingApplications = $("#totalPendingApplications").val();
var totalNewApplications = $("#totalNewApplications").val();
var activeApplicationsValue = $("#activeApplicationsValue").val();
var latestActinValue = $("#latestActinValue").val();
var totalEvaluationPerformance = $("#totalEvaluationPerformance").val();
$("#NewApplicationsText1").html("("+totalEvaluationPerformance+")");

if(totalPendingApplications == 0){
	$("#valueOfNew").html("You have "+totalNewApplications+" NEW APPLICATIONS");
}else if(totalNewApplications == 0){
	$("#valueOfNew").html("You have "+totalPendingApplications+" PENDING APPLICATIONS");
}else{
	$("#valueOfNew").html("You have "+totalNewApplications+" NEW APPLICATIONS and "+totalPendingApplications+" PENDING APPLICATIONS");
}
$("#activeApplications").html(" ("+activeApplicationsValue+") ");
$("#totalPendingApplicationsText").html(" ("+totalPendingApplications+") ");
$("#latestActinText").html(" ("+latestActinValue+") ");
$("#NewApplicationsText").html(" ("+totalNewApplications+") ");

$("#NewApplicationsTextValue").val(totalNewApplications);

$(document).on('click','.sendInvitelink', function(){
	$("#panellevelformid").addClass("active");
	$("#selectJob").addClass("active in");
    $("#searchpanel").removeClass("active");
    $("#sendInvitations").removeClass("active in");
})

 $(document).on('click', '.invite-users', function () {
        var jobId = $(this).attr('id');
        var valEmelemnt = $(this).attr("element");
		$("#selectedJobs").text("Selected Job: "+valEmelemnt);
        $("#panellevelformid").removeClass("active");
        $("#searchpanel").addClass("active");
        $.ajax({
            url: siteUrl + 'users/getUserSmsEmailContent/'+ jobId,
            data: '',
            async: false,
            contentType: false,
            processData: false,
            cache: false,
            type: 'POST',
            success: function (data)
            {
                var obj = jQuery.parseJSON(data);
                var smsContent = obj.smsContent;
                var emailSubject = obj.emailSubject;
                var emailContent = obj.emailContent;
                $('#smsMessage').val(smsContent);
                $('#emailSubject').val(emailSubject);
                //$('#emailMessage').val(emailContent);
                tinymce.get('emailMessage').setContent(emailContent);
            },
        });

        $('.upload-file').attr('id', jobId);
    });
	
 $(".upload-file").click(function () {
        var jobId = $(this).attr('id');
        var file = document.getElementById("inviteUsers").files[0];//fetch file
        var smsMessage = $( "textarea#smsMessage" ).val();
        var emailSubject = $( "#emailSubject" ).val();
        var emailMessage = tinymce.get('emailMessage').getContent();
        var inviteFrom = $('input[type="checkbox"][name="inviteFrom\\[\\]"]:checked').map(function() { 
                            return this.value; 
                        }).get();//fetch file
        var inviteLength= inviteFrom.length;
        
        
        if (!file) {
            $('#inviteAlert').html('<p>Please select File.</p>');
            $('#inviteAlert').removeClass('hidden');
            return false;
        }
        if ( inviteFrom == "" ) {
            $('#inviteAlert').html('<p>Please select atleast one either Email OR SMS</p>');
            $('#inviteAlert').removeClass('hidden');
            return false;
        }

        if ( inviteFrom == "1" ) {
            if ( emailSubject == "" ) {
                $('#inviteAlert').html('<p>Please enter Subject</p>');
                $('#inviteAlert').removeClass('hidden');
                return false;
            }
            if ( emailMessage == "" ) {
                $('#inviteAlert').html('<p>Please enter email content</p>');
                $('#inviteAlert').removeClass('hidden');
                return false;
            }
        }

        if ( inviteFrom == "2" ) {
            if ( emailSubject == "" ) {
                $('#inviteAlert').html('<p>Please enter sms content</p>');
                $('#inviteAlert').removeClass('hidden');
                return false;
            }
        }

        if ( inviteLength > 1 ) {
            if ( smsMessage == "" ) {
                $('#inviteAlert').html('<p>Please enter sms content</p>');
                $('#inviteAlert').removeClass('hidden');
                return false;
            }
            if ( emailSubject == "" ) {
                $('#inviteAlert').html('<p>Please enter subject</p>');
                $('#inviteAlert').removeClass('hidden');
                return false;
            }
            if ( emailMessage == "" ) {
                $('#inviteAlert').html('<p>Please enter email content</p>');
                $('#inviteAlert').removeClass('hidden');
                return false;
            } 
        }

        var formData = new FormData();
        formData.append('inviteUsers', file);
        formData.append('inviteFrom', inviteFrom);
        formData.append('smsMessage', smsMessage);
        formData.append('emailSubject', emailSubject);
        formData.append('emailMessage', emailMessage);

        
        $.ajax({
            url: siteUrl + 'users/invite/' + jobId,
            data: formData,
            async: false,
            contentType: false,
            processData: false,
            cache: false,
            type: 'POST',
            beforeSend: function () {
                $(".upload-file").html('Invitation is being send. Please wait......');
                $('.upload-file').attr('disabled',true);
            },
            success: function (data)
            {
                //alert(data);
                if (data.trim().toString() == 'invalid') {
                    $('#inviteAlert').html('<p>File must have CSV and with three columns in order (Name, Email, Phone).</p>');
                    $('#inviteAlert').removeClass('hidden');
                    $(".upload-file").html('Send Invitation');
                    $('.upload-file').attr('disabled',false);
                    return false;
                }
                else if (data.trim().toString() == 'noRecord') {
                    $('#inviteAlert').html('<p>There is no record in the file. Please enter at least one record.</p>');
                    $('#inviteAlert').removeClass('hidden');
                    $(".upload-file").html('Send Invitation');
                    $('.upload-file').attr('disabled',false);
                    return false;
                } else {
	                $('.close').trigger('click');
	                bootbox.alert('Users invited successfully!', function () {
	                    location.reload();
	                });
	            }
            },
        });
        return false;
    });
	
	function searchResult(){
		$("#searchForm").submit();
	}
	Second = 4000;
	
	setTimeout(blinkText,1000);
	
	function blinkText(){
		
		var totalPendingApplications = $("#totalPendingApplications").val();
		var totalNewApplications = $("#totalNewApplications").val();
		
		if(totalPendingApplications == 0){
			$("#valueOfNew").html("You have "+totalNewApplications+" NEW APPLICATIONS");
		}else if(totalNewApplications == 0){
			$("#valueOfNew").html("You have "+totalPendingApplications+" PENDING APPLICATIONS");
		}else{
			$("#valueOfNew").html("You have "+totalNewApplications+" NEW APPLICATIONS and "+totalPendingApplications+" PENDING APPLICATIONS");
		}
		var totalPendingNewApp = parseInt(totalPendingApplications)+parseInt(totalNewApplications);
		$("#totalpendingcounttest").text(totalPendingNewApp);
		var totalShortlistedCount = parseInt($("#totalshortistedtest").text());
		var totalRejectedCount = parseInt($("#totalrejectedtest").text());
		
		var totalApplicationsReceived = totalRejectedCount+totalShortlistedCount+totalPendingNewApp;
		$("#totalapplicationcountTest").text(totalApplicationsReceived);
		
		
		$("#valueOfNew").toggle();
		if(!$('#valueOfNew').is(':visible')){
			setTimeout(blinkText,500);
		}else{
			setTimeout(blinkText,4000);
		}
	}

	var documentHeight = $(document).height();
	$("#activeApplicationsTable").css("height",documentHeight/6);
	$("#latestActionsTable").css("height",documentHeight/6);
	$("#NewApplicationsTables").css("height",documentHeight/6);
	$("#pendingApplicationsTable").css("height",documentHeight/6);
	$("#EvaluationPerformanceTable").css("height",documentHeight/6);
	$("#middleSection").css("margin-top",documentHeight-680);
	
	marquee();
	//
	function marquee(){
		$(".evaluationPerformanceClassMarquee").each(function(){
			if ($(this)[0].scrollWidth >  $(this).innerWidth()) {
				var text = $(this).html();
				$(this).html('<marquee behavior="scroll" scrollamount="3" direction="left" onmouseover="this.stop();" onmouseout="this.start();">'+text+'</marquee>');
			}
		})
		
	}
	function redirectTo(link){
		window.location = ""+link+"";
	}


	//Use to update the new applications
	setTimeout(updateNewApplication,1000);
	
	function updateNewApplication(){
		var value = $('#newApplicationLastId').val();

		if(value != '') {
			setTimeout(updateNewApplication,60000);
			formData = {lastInterviewId: value};

			$.ajax({
	            url: siteUrl + 'jobs/getLastCreatedInterview',
	            data: formData,
	            cache: false,
	            type: 'POST',
	            success: function (data)
	            {
	                var obj = $.parseJSON(data);
	            	var interviewData = obj.interviewData;
	            	var jobId = obj.jobId;
	            	var userId = obj.userId;
	            	if(jobId != '') {
						$("#NoNewApplicationsFound").css("display","none");
		            	//console.log(interviewData[0].interviewId);
		            	$('#NewApplicationsTable > tbody > tr:first').after('<tr"><td>'+interviewData[0].jobName+'</td><td><a href="'+siteUrl+'queue/list?JobId='+jobId+'&userId='+userId+'&query=search">'+interviewData[0].candidateName+'</a></td><td>'+interviewData[0].createdAt+'</td></tr>');
						
		            	$('#newApplicationLastId').val(interviewData[0].interviewId);
		            	var appvalue = parseInt($('#NewApplicationsTextValue').val()) + 1;
		            	$('#NewApplicationsTextValue').val(appvalue);
		            	$('#totalNewApplications').val(appvalue);
		            	$("#NewApplicationsText").html("("+appvalue+")");
		            }
	            },
	        });
		}
	}


</script>
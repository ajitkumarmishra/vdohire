<?php if ($this->session->flashdata('message')) { ?>
    <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Success!</strong> <?php echo $this->session->flashdata('message'); ?>
    </div>
<?php } ?>
<?php if ($this->session->flashdata('error')) { ?>
    <div class="alert alert-danger">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Danger!</strong> <?php echo $this->session->flashdata('error'); ?>
    </div>
<?php } ?>
<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <a href="<?php echo base_url('users/create'); ?>" class="btn btn-primary pull-right">Add New User</a>
            </div>
        </div>
        <form method="post" name="form1" id="form1" action="">
            <table class="table">
                <thead>
                    <tr>
                        <th><input name="check_all" type="checkbox" id="check_all" value="1" onclick="checkall(this.form)" /></th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Mobile</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                </thead>
                <tbod>
                    <?php foreach ($users as $user): ?>
                        <tr class="<?php echo ($user->status == '1' ? 'success' : 'warning'); ?>">
                            <td>
                                <input name="arr_ids[]" type="checkbox" id="arr_ids[]" value="<?php echo $user->id; ?>" />
                            </td>
                            <td><a href="<?php echo base_url('users/details/' . $user->user_nicename); ?>"><?php echo $user->user_name; ?></a></td>
                            <td><?php echo $user->user_email; ?></td>
                            <td><?php echo $user->user_mobile; ?></td>
                            <td><?php echo ($user->status == '1' ? 'Active' : 'Inactive'); ?></td>
                            <td><a href="<?php echo base_url('users/account/id') . '/' . $user->id; ?>"><span class="glyphicon glyphicon-pencil pull-right"></a></td>
                        </tr>
                    <?php endforeach; ?>
                </tbod>
            </table>
            <div class="col-sm-12 col-md-12">
                <div class="col-sm-6 col-md-6">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td align="left" style="padding:2px">
                                <input type="submit" name="Activate" value="Activate" class="btn"/>
                                <input type="submit" name="Deactivate" value="Deactivate" class="btn" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="col-sm-6 col-md-6 pull-right">
                    <?php echo $links; ?>
                </div>
            </div>



        </form>
    </div>
</div>

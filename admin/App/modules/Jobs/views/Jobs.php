<?php

/*

 */

class Jobs extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('coreapi_model',             'CORE_MODEL');
        $this->load->model('job_model');
        $this->load->model('user_model');
        $this->load->model('userjob_model');
        $this->load->model('corporatereport_model');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('email');
        $this->load->helper('fj');
        //$this->load->helper('jwt');
        $token = $this->config->item('accessToken');
        //echo $token; exit;
//        if (isset($_POST) && $_POST != NULL) {
//            if ($token != $_POST['accessToken']) {
//                echo 'You do not have pemission.';
//                exit;
//            }
//        }
    }

    /**
     * addJob function
     * Creates a new job.
     * @return string the success message | error message.
     */
    function addJob() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token      = $this->config->item('accessToken');
        $userData   = $this->session->userdata['logged_in'];
        
        //echo "<pre>"; print_r($userData); exit;
        if (isset($userData->id) && $userData->id != NULL) {
           $userId          = $userData->id;
           $userByFullName  = $userData->fullname;
           $userCompany     = $userData->company;
           $createdById     = $userData->createdBy;
        } else {
            $userId = 1;
        }
        $userRole   = $userData->role;
        
        if($userRole=='2' || $userRole=='1') {
            $userValidity   = $userData->validTo;
        }
        else if($userRole=='4') {
            $corporateUser  = getCorporateUser($userId);
            $userValidity   = $corporateUser['validTo'];
        }
        
        //print_r($userData); exit;
        if (isset($_POST) && $_POST != NULL) {
            $panel1 = $this->input->post('level1', true);
            $panel2 = $this->input->post('level2', true);
            $resultPanel = array_intersect($panel1, $panel2);


            $resp['error'] = '';
            $fromEmail = $this->config->item('fromEmail');
            if ($token != $this->input->post('accessToken', true)) {
                $resp['error'] = 'You do not have pemission.';
            }

            $this->form_validation->set_rules('title', 'title', 'trim|required');
            //$this->form_validation->set_rules('cityState', 'CityState', 'trim|required');
            $this->form_validation->set_rules('description', 'description', 'trim|required');
            $this->form_validation->set_rules('openTill', 'OpenTill', 'trim|required|isFutureDate[fj_jobs.OpenTill]');
            $this->form_validation->set_rules('level1[]', 'Level 1 Panel', 'trim|required');
            $this->form_validation->set_message('isFutureDate', 'Open Till field must have a future date!');

            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }
            if (count($resultPanel) != 0) {
                $resp['error'] .= '<p>Panel 1 user and Panel 2 user can not be same.</p>';
            }
            
            
            
            $datetimeObj1   = new DateTime($userValidity);
            $datetimeObj2   = new DateTime(date("Y-m-d H:i:s", strtotime($this->input->post('openTill', true))));
            $interval       = $datetimeObj1->diff($datetimeObj2);
            $dateDiff       = $interval->format('%R%a');

            if($dateDiff > 0) {
                $resp['error'] .= '<p>Open till date is not a valid date as your account expires before.</p>';
            }
            
//            if(strtotime(date("Y-m-d", strtotime($this->input->post('openTill', true))))>strtotime($userValidity)) {
//                $resp['error'] .= '<p>Open till date is not a valid date as your account expires before.</p>'.$userValidity;
//            }
            if (!$resp['error']) {

                $data['jobId'] = $this->input->post('jobId', true);
                $data['title'] = $this->input->post('title', true);
                $locations = $this->input->post('cityState', true);
                $data['noOfVacancies'] = $this->input->post('vacancy', true);
                $data['ageFrom'] = $this->input->post('ageFrom', true);
                $data['ageTo'] = $this->input->post('ageTo', true);
                $data['salaryFrom'] = $this->input->post('salaryFrom', true);
                $data['salaryTo'] = $this->input->post('salaryTo', true);
                $data['noticePeriod'] = $this->input->post('noticePeriod', true);
                //$data['qualification'] = $this->input->post('qualification', true);
                $data['expFrom'] = $this->input->post('expFrom', true);
                $data['expTo'] = $this->input->post('expTo', true);
                $data['openTill'] = date("Y-m-d", strtotime($this->input->post('openTill', true)));
                $data['description'] = $this->input->post('description', true);
                $data['assessment'] = $this->input->post('assessment', true);
                $data['interview'] = $this->input->post('interview', true);
                /* $data['preInterviewText'] = $this->input->post('preInterviewText', true);
                  $data['postInterviewText'] = $this->input->post('postInterviewText', true);
                  $data['preAssessmentText'] = $this->input->post('preAssessmentText', true);
                  $data['postAssessmentText'] = $this->input->post('postAssessmentText', true); */
                $data['createdAt'] = date('Y-m-d h:i:s');
                $data['updatedAt'] = date('Y-m-d h:i:s');
                $data['fjCode'] = $this->generateRandomString();
                $data['createdBy'] = $userId;
                $ind = getUserById($userId);
                $data['industryId'] = 5;

                $roleName = getRoleById($data['role']);
                /* $panel1 = $this->input->post('level1', true);
                  $panel2 = $this->input->post('level2', true);
                  $resultPanel = array_intersect($panel1, $panel2);
                  echo '<pre>';print_r($resultPanel);die(); */
                $location = $this->input->post('location', true);
                $qualifications = $this->input->post('qualification', true);
                try {
                    if (isset($_FILES['jd'])) {
                        $jd = $this->fileUpload($_FILES);
                        if (!$jd) {
                            $resp['message'] = 'There is a server problem occured to upload image. Please make sure image type is png or jpg!';
                        }
                    }
                    $data['jd'] = $jd;
                    $resp = $this->job_model->addJob($data);
                    if ($resp) {
                        $this->job_model->addLocation($locations, $resp);
                        $this->job_model->addJobPanel($panel1, $resp, 1);
                        $this->job_model->addJobPanel($panel2, $resp, 2);
                        $this->job_model->addJobQualification($qualifications, $resp);
                        $this->email->from($fromEmail, 'First Job');
                        $this->email->subject('First Job');
                        $this->email->set_mailtype('html');
                        foreach ($panel1 as $item) {
                            $user = getUserById($item);
                            $jobDetailsData = getJobDetail($resp);
                            
                            /* notification message for assigned  job to user ( sub-admin ) */
                            $notificationMsg = "You are added as a Evaluator L1 for Job ". ucwords($jobDetailsData['title'])." by ".ucwords($userCompany);
                            insertNotificationForTopBarInAdmin($item, $notificationMsg, "Job Review", $userId);

                            $temp['email'] = $user['email'];
                            $temp['jobId'] = $data['jobId'];
                            $temp['type'] = 'Evaluator L1';
                            $this->email->to($item);
                            $body = $this->load->view('emails/panelTemp.php', $temp, TRUE);
                            $this->email->message($body);
                            $this->email->send();
                        }

			foreach ($panel2 as $item) {
                            $user = getUserById($item);
                            $jobDetailsData = getJobDetail($resp);
                            
                            /* notification message for assigned  job to user ( sub-admin ) */
                            $notificationMsg = "You are added as a Evaluator L1 for Job ". ucwords($jobDetailsData['title'])." by ".ucwords($userCompany);
                            insertNotificationForTopBarInAdmin($item, $notificationMsg, "Job Review", $userId);
                                
                            $temp['email'] = $user['email'];
                            $temp['jobId'] = $data['jobId'];
                            $temp['type'] = 'Evaluator L2';
                            $this->email->to($item);
                            $body = $this->load->view('emails/panelTemp.php', $temp, TRUE);
                            $this->email->message($body);
                            $this->email->send();
                        }
                        $data['message'] = 'Successfully created!';
                        $data['main_content'] = 'fj-create-job';
                        $data['token'] = $token;
                        $data['subuser'] = getSubUsers($userId);
                        $this->load->view('fj-mainpage', $data);
                    }
                } catch (Exception $e) {
                    $resp['error'] = $e->getMessage();
                }
            } else {
                //echo "<pre>"; print_r($_POST); die();
                $data['main_content'] = 'fj-create-job';
                $data['error'] = $resp;
                $data['job'] = $this->input->post();
                $data['city'] = getCities();
                $data['course'] = getCourses();
                $data['assessment'] = getAssessmentsByRole($userId);
                $data['interview'] = getInterviewByRole($userId);
                $data['files'] = $_FILES;
                $data['token'] = $token;
                $data['subuser'] = getSubUsers($userId);
                $this->load->view('fj-mainpage', $data);
            }
        } 
        else {
            $data['city']           = getCities();
            $data['course']         = getCourses();
            $data['assessment']     = getAssessmentsByRole($userId, $userRole);
            $data['interview']      = getInterviewByRole($userId, $userRole);
            $data['subuser']        = getSubUsers($userId);
            $data['main_content']   = 'fj-create-job';
            $data['token']          = $token;
            $this->load->view('fj-mainpage', $data);
        }
    }

    /**
     * FileUpload function
     * Uploads a file.
     * @param array $files.
     * @return string the uploaded file or boolean false.
     */
    function fileUpload($files) {
        // will get transfer in helper.
        $typeArr = explode('/', $files['jd']['type']);
        $imgName = rand() . date('ymdhis') . '_jd.' . $typeArr[1];
        $data['image'] = $imgName;
        $jobImageType = $this->config->item('videoType');
        if (in_array($typeArr[1], $jobImageType)) {
            $uploadResp = $this->job_model->fileUpload($files, 'uploads/jd', $imgName);
            return $uploadResp;
        } else {
            return false;
        }
    }

    /**
     * EditJob function
     * Updates a job.
     * @return string the success message | error message.
     */
    function editJob($id) {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $job = $this->job_model->getJob($id);
        $token = $this->config->item('accessToken');
        $userData = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
	    $userByFullName = $userData->fullname;
            $userCompany = $userData->company;
            $createdById = $userData->createdBy;
        } else {
            $userId = 1;
        }
        
        $userRole   = $userData->role;
        
        if($userRole=='2' || $userRole=='1') {
            $userValidity   = $userData->validTo;
        }
        else if($userRole=='4') {
            $corporateUser  = getCorporateUser($userId);
            $userValidity   = $corporateUser['validTo'];
        }
        
//        $date1 = $userValidity;
//        $date2 = "2014-05-17";
//        $datetimeObj1 = new DateTime($date1);
//        $datetimeObj2 = new DateTime($date2);
//        $interval = $datetimeObj1->diff($datetimeObj2);
//        $dateDiff = $interval->format('%R%a');
//
//        if($dateDiff == 0){
//        echo "$date1 is equal to the $date2";
//        }else{
//        echo ($dateDiff > 0)? "$date2 is greater than the $date1": "$date1 is greater than the $date2";
//        }
        
        $info['main_content'] = 'fj-edit-job';
        $info['job'] = $job;
        $info['token'] = $token;
        $info['city'] = getCities();
        $info['course'] = getCourses();
        $info['assessment'] = getAssessmentsByRole($userId);
        $info['interview'] = getInterviewByRole($userId);
        $info['subuser'] = getSubUsers($userId);
        $jobLocations = getLocationsByJob($id);
        $info['job']['qualification'] = getQualificationsByJob($id);
        //echo "<pre>"; print_r($info); die;
        $j = 0;
        foreach ($jobLocations as $item) {
            $jobLocationsFormatted[$j] = $item['location'];
            $j++;
        }
        $info['job']['cityState'] = $jobLocationsFormatted;
        $info['job']['level1'] = getPanelByJob($id, 1);
        $info['job']['level2'] = getPanelByJob($id, 2);
        $j = 0;
        if (isset($_POST) && $_POST != NULL) {
            $resp['error'] = '';
            $fromEmail = $this->config->item('fromEmail');
            if ($token != $this->input->post('accessToken', true)) {
                $resp['error'] = 'You do not have pemission.';
            }
            $panel1 = $this->input->post('level1', true);
            $panel2 = $this->input->post('level2', true);
            $resultPanel = array_intersect($panel1, $panel2);

            $this->form_validation->set_rules('title', 'title', 'trim|required');
            //$this->form_validation->set_rules('cityState', 'CityState', 'trim|required');
            $this->form_validation->set_rules('description', 'description', 'trim|required');
            //$this->form_validation->set_rules('openTill', 'OpenTill', 'trim|required|isFutureDate[fj_jobs.OpenTill]');
            //$this->form_validation->set_message('isFutureDate', 'Open Till field must have a future date!');
            // $this->form_validation->set_rules('level1', 'Level 1 Panel', 'trim|required');

            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }
            if (count($resultPanel) != 0) {
                $resp['error'] .= '<p>Panel 1 user and Panel 2 user can not be same.</p>';
            }
            if (!$id) {
                $resp['error'] = 'Undefined job!';
            }
            
            $datetimeObj1   = new DateTime($userValidity);
            $datetimeObj2   = new DateTime(date("Y-m-d H:i:s", strtotime($this->input->post('openTill', true))));
            $interval       = $datetimeObj1->diff($datetimeObj2);
            $dateDiff       = $interval->format('%R%a');

            if($dateDiff > 0) {
                $resp['error'] .= '<p>Open till date is not a valid date as your account expires before.</p>';
            }
            
            if (!$resp['error']) {
                $data['jobId'] = $this->input->post('jobId', true);
                $data['title'] = $this->input->post('title', true);
                $locations = $this->input->post('cityState', true);
                $data['noOfVacancies'] = $this->input->post('vacancy', true);
                $data['ageFrom'] = $this->input->post('ageFrom', true);
                $data['ageTo'] = $this->input->post('ageTo', true);
                $data['salaryFrom'] = $this->input->post('salaryFrom', true);
                $data['salaryTo'] = $this->input->post('salaryTo', true);
                $data['noticePeriod'] = $this->input->post('noticePeriod', true);
                $qualifications = $this->input->post('qualification', true);
                $data['expFrom'] = $this->input->post('expFrom', true);
                $data['expTo'] = $this->input->post('expTo', true);
                $data['openTill'] = date("Y-m-d", strtotime($this->input->post('openTill', true)));
                $data['description'] = $this->input->post('description', true);
                $data['assessment'] = $this->input->post('assessment', true);
                $data['interview'] = $this->input->post('interview', true);
                /* $data['preInterviewText'] = $this->input->post('preInterviewText', true);
                  $data['postInterviewText'] = $this->input->post('postInterviewText', true);
                  $data['preAssessmentText'] = $this->input->post('preAssessmentText', true);
                  $data['postAssessmentText'] = $this->input->post('postAssessmentText', true); */
                $data['createdAt'] = date('Y-m-d h:i:s');
                $data['updatedAt'] = date('Y-m-d h:i:s');
                //$data['fjCode'] = $this->generateRandomString();
                $data['updatedBy'] = $userId;
                $roleName = getRoleById($data['role']);
                /* $panel1 = $this->input->post('level1', true);
                  $panel2 = $this->input->post('level2', true); */
                $location = $this->input->post('location', true);
                try {
                    if (isset($_FILES['jd']['name']) && $_FILES['jd']['name'] != NULL) {
                        $jd = $this->fileUpload($_FILES);
                        /* if (!$imageName) {
                          $resp['message'] = 'There is a server problem occured to upload image. Please make sure image type is png or jpg!';
                          } else {
                          unlink('uploads/jobImages/' . $job['jd']);
                          }
                          $data['jd'] = $jd; */
                        $data['jd'] = $jd;
                    }
                    $resp = $this->job_model->updateJob($data, $id);
                    if ($resp) {
                        $this->job_model->deleteLocationsByJob($resp);
                        $this->job_model->addLocation($locations, $resp);
                        $this->job_model->deletePanelByJob($resp);
                        $this->job_model->addJobPanel($panel1, $resp, 1);
                        $this->job_model->addJobPanel($panel2, $resp, 2);
                        $this->job_model->deleteQualicationsByJob($resp);
                        $this->job_model->addJobQualification($qualifications, $resp);
                        $data['message'] = 'Successfully updated!';
                        $data['main_content'] = 'fj-edit-job';
                        $data['token'] = $token;
                        //$level1Panel = array_diff($presetPanel, $panel1);
			$level1Panel = $panel1;
                        $level2Panel = $panel2;
                        $this->email->from($fromEmail, 'First Job');
                        $this->email->subject('First Job');

                        $this->email->set_mailtype('html');

                        foreach ($level1Panel as $item) {
                            $user = getUserById($item);
                            
                            $jobDetailsData = getJobDetail($id);
                            
                            /* notification message for assigned  job to user ( sub-admin ) */
                            $notificationMsg = "You are added as a Evaluator L1 for Job ". ucwords($jobDetailsData['title'])." by ".ucwords($userCompany);
                            insertNotificationForTopBarInAdmin($item, $notificationMsg, "Job Review", $userId);
                            
                            $temp['email'] = $user['email'];
                            $temp['jobId'] = $data['jobId'];
                            $temp['type'] = 'Evaluator L1';
                            $this->email->to($item);
                            $body = $this->load->view('emails/panelTemp.php', $temp, TRUE);
                            $this->email->message($body);
                            $this->email->send();
                        }

			foreach ($level2Panel as $item) {
                            $user = getUserById($item);                            
                            $jobDetailsData = getJobDetail($id);
                            
                            /* notification message for assigned  job to user ( sub-admin ) */
                            $notificationMsg = "You are added as a Evaluator L2 for Job ". ucwords($jobDetailsData['title'])." by ".ucwords($userCompany);
                            insertNotificationForTopBarInAdmin($item, $notificationMsg, "Job Review", $userId);
                            
                            $temp['email'] = $user['email'];
                            $temp['jobId'] = $data['jobId'];
                            $temp['type'] = 'Evaluator L1';
                            $this->email->to($item);
                            $body = $this->load->view('emails/panelTemp.php', $temp, TRUE);
                            $this->email->message($body);
                            $this->email->send();
                        }

                        redirect('jobs/list/1', 'refresh');
                    }
                } catch (Exception $e) {
                    $resp['error'] = $e->getMessage();
                }
            } else {
                $info['error'] = $resp;
                $info['job'] = $this->input->post();

                $this->load->view('fj-mainpage', $info);
            }
        } else {
            $this->load->view('fj-mainpage', $info);
        }
    }

    /**
     * DeleteJob function
     * Deletes a job.
     * @return string the success message | error message.
     */
    function deleteJob() {
        $resp['error'] = 0;
        $token = $this->config->item('accessToken');
        $fromEmail = $this->config->item('fromEmail');
        $jobId = $this->input->post('id', true);
        if (!$jobId) {
            echo $error = 'Undefined job!';
            exit;
        }
        if (!$resp['error']) {
            $data['status'] = 3;
            $resp = $this->job_model->updateJob($data, $jobId);
            if ($resp) {
                $this->job_model->deletePanelByJob($jobId);
                echo "Deleted successfully";
            }
        }
    }

    /**
     * CheckJobInLiveJob function
     * Checks job in live job.
     * @return boolean.
     */
    function checkJobInLiveJob($jobId) {
        if (checkJobInLiveJob($jobId)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * GetJob function
     * Gets job specific detail.
     */
    function getJob($id) {
        //echo $id; exit;
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $itemsPerPage           = $this->config->item('itemsPerPage');
        $data['itemsPerPage']   = $itemsPerPage;
        
        $job            = $this->job_model->getJob($id);
        
        //print_r($job); exit;
        //$jobApplication = $this->job_model->getJobApplication($id);
        
        $jobApplication = $this->job_model->getJobApplication($id, $itemsPerPage, $offset);
        
        $token                      = $this->config->item('accessToken');        
        $data['main_content']       = 'fj-view-job';
        $data['job']                = $job;
        $data['jobApplication']     = $jobApplication;
        $data['token']              = $token;
        $data['job']['locations']   = getLocationsByJob($id);
        //echo "<pre>"; print_r($data); exit;
        $this->load->view('fj-mainpage', $data);
    }
    
    function userAssessmentDetail() {
        $userId = trim($this->input->post('userId', true));
        $jobId  = trim($this->input->post('jobId', true));
        $interviewVideo = trim($this->input->post('interviewVideo', true));
        $jobApplicationDetail           = $this->job_model->userAssessmentDetail($userId, $jobId);
        $data['jobApplicationDetail']   = $jobApplicationDetail;
        $details = '';
        
        $countQuestion = 0;
        $correctAnswer = 0;
        if(count($data)>0){
            $details .= "<table class='table'>
                            <thead class='tbl_head'>
                                <tr>
                                    <th>Assessment Question</th>
                                    <th>User Answer</th>
                                    <th>Correct Answer</th>
                                </tr>
                            </thead>                            
                            <tbody>";
            foreach($jobApplicationDetail as $assessmentDetail) {
                $countQuestion++;
                $color="style='color:#ff0000';";
                if($assessmentDetail->userOption==$assessmentDetail->option) { $correctAnswer++; $color="style='color:#000000';"; }
                $details .= "
                        <tr>
                            <td>".$assessmentDetail->title."</td>
                            <td ".$color.">".($assessmentDetail->userOption!=''?$assessmentDetail->userOption:'Not Attempted')."</td>
                            <td>".$assessmentDetail->option."</td>
                        </tr>
                ";
            }
            
            if($countQuestion>0)
            $percent = round(($correctAnswer/$countQuestion)*100,2);
            else
            $percent = 'No Assessment Set is connected with this job.';
            
            $details .= "
                    </tbody>
                </table>";
            
            $details .= "<center><h3 style='color:#1ea990;'>Result - ".$percent."%</h3></center>";
            
            
        }
        print_r($details);
    }
    
    function userInterview($userId, $jobId) {
        $jobApplicationDetail           = $this->job_model->userInterview($userId, $jobId);
        $data['jobApplicationDetail']   = $jobApplicationDetail;
        print_r($data);
    }

    /**
     * ListJobs function
     * Get all jobs
     * @throws Exception
     */
    function listJobs($page) {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        //Jobs Listing Start
        $userData = $this->session->userdata['logged_in'];

        $userRole = $userData->role;
        $userLoggedinUserId = $userData->id;
        $data['page'] = $page;
        $resp['error'] = 0;
        $serachColumn   = trim($this->input->post('serachColumn', true));
        $searchText     = trim($this->input->post('serachText', true));
        
        if($_POST){
            $this->session->set_userdata('serachColumn', $serachColumn);
            $this->session->set_userdata('searchText', $searchText);    
        }
        $serachColumn   = $this->session->userdata('serachColumn');
        $searchText     = $this->session->userdata('searchText');
        //print_r($this->session->userdata('searchText')); exit;
        
        
        $token = $this->config->item('accessToken');
        $fromEmail = $this->config->item('fromEmail');
        $itemsPerPage = $this->config->item('itemsPerPage');
        $data['itemsPerPage'] = $itemsPerPage;
        $createdBy = NULL;
        //$createdBy = $userData->id;
        if (!$page) {
            $page = 0;
        } else {
            $page = $page - 1;
        }
        $offset = $page * $itemsPerPage;
        try {
            
            $createdByStr = getAllCorporateAndSubuserIds($userLoggedinUserId, $userRole);
            
            /*
            if ($userRole != 1):
                if ($userRole == 2):
                    $userIdsRes = $this->user_model->getUserIdsForJob($userLoggedinUserId);
                    $userIdsStr = $userIdsRes['userIds'];
                    if ($userIdsStr != ""):
                        $createdBy = $userIdsStr . "," . $userLoggedinUserId;
                    else:
                        $createdBy = $userLoggedinUserId;
                    endif;
                elseif ($userRole == 3 || $userRole == 4 || $userRole == 5):
                    $userDataDetails = $this->user_model->getUser($userLoggedinUserId);
                    if ($userDataDetails):
                        $corporateId = $userDataDetails['createdBy'];
                        $userIdsRes = $this->user_model->getUserIdsForJob($corporateId);
                        $userIdsStr = $userIdsRes['userIds'];
                        if ($userIdsStr != ""):
                            $createdBy = $userIdsStr . "," . $corporateId;
                        else:
                            $createdBy = $userLoggedinUserId . "," . $corporateId;
                        endif;
                    else:
                        $createdBy = "";
                    endif;
                else:
                    $createdBy = "";
                endif;
            else:
                $createdBy = "";
            endif;

            if ($createdBy != "") {
                $createdByArr = explode(",", $createdBy);
                $createdByArr = array_filter($createdByArr);
                $createdByStr = implode(",", $createdByArr);
            }
            */
            
            $result         = $this->job_model->listJobs($itemsPerPage, $offset, $serachColumn, $searchText, $createdByStr, $userRole);
            //echo "<pre>";print_r($result);
            $completeResult = $this->job_model->listJobs($itemsPerPage, $offset, $serachColumn, $searchText, $createdByStr, $userRole);
            $count                  = ceil(count($completeResult) / $itemsPerPage);
            $data['count']          = $count;
            $data['totalCount']     = count($completeResult);
            $data['content']        = $result;
            $data['main_content']   = 'fj-job-listing';
            $data['searchText']     = $searchText;
            $data['serachColumn']   = $serachColumn;
            $data['token']          = $token;
            $this->load->view('fj-mainpage', $data); //die();
            if (!$result) {
                //throw new Exception('Could not get list of users!');
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }

        //Jobs Listing End
    }

    /**
     * Login function
     * Validates job credentails and sign in the system
     */
    function login() {
        $resp['error'] = 0;
        $email = $this->input->post('email', true);
        $password = $this->input->post('password', true);
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        //$this->form_validation->set_rules('password', 'Password', 'trim|required|isValidPassword[fj_jobs.password]');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        if ($this->form_validation->run() == false) {
            $resp['error'] = validation_errors();
        }
        try {
            if (!$resp['error']) {
                $jobdata = $this->job_model->validateLogin($email, $password);
                if ($jobdata) {
                    $jobdata->token = $this->getJwtToken($jobdata->id);
                    $this->session->set_jobdata($jobdata);
                } else {
                    $resp['msg'] = "Email and password do not match!";
                }
            } else {
                print_r($resp);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
     * GetJwtToken function
     * Gets jwt token by job id.
     * @param int $jobId
     * @return string
     */
    function getJwtToken($jobId) {
        $token = array();
        $token['id'] = $jobId;
        $jwtKey = $this->config->item('jwtKey');
        $secretKey = base64_decode($jwtKey);
        $signatureAlgo = $this->config->item('signatureAlgorithm');
        return JWT::encode($token, $secretKey, $signatureAlgo);
    }

    /**
     * GetValueByToken function
     * Gets value by token
     * @param string $jwtToken
     * @return string
     */
    function getValueByToken($jwtToken) {
        //$jwtToken = $this->input->post('token', true);
        $jwtKey = $this->config->item('jwtKey');
        $secretKey = base64_decode($jwtKey);
        $signatureAlgo = $this->config->item('signatureAlgorithm');
        $token = JWT::decode($jwtToken, $secretKey, $signatureAlgo);
        //echo "Job:" . $token->id;
        return $token->id;
    }

    function generateRandomString($length = 6) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    function postJob() {
        $jobId = $this->input->post('jobId', true);
        $posted = $this->input->post('posted', true);
        $data['posted'] = $posted;
        $job = $this->job_model->updateJob($data, $jobId);
        if ($posted == 1) {
            echo "Posted";
        } else {
            echo "Post";
        }
        return;
    }

    function statusJob() {
        $jobId = $this->input->post('jobId', true);
        $status = $this->input->post('status', true);
        $data['status'] = $status;
        $job = $this->job_model->updateJob($data, $jobId);
        if ($status == 1) {
            echo "Active";
        } else {
            echo "Inactive";
        }
        return;
    }

    function corporateDashboard($page) {
        if (!$this->session->userdata('logged_in')) {
            //redirect(base_url('users/login'));
        }
        //Jobs Listing Start
        $userData = $this->session->userdata['logged_in'];

        $userRole = $userData->role;
        $userLoggedinUserId = $userData->id;
        $data['userRole'] = $userRole;
        $data['page'] = $page;
        $resp['error'] = 0;
        $serachColumn = $this->input->post('serachColumn', true);
        $searchText = $this->input->post('serachText', true);
        $token = $this->config->item('accessToken');
        $fromEmail = $this->config->item('fromEmail');
        $itemsPerPage = $this->config->item('itemsPerPage');
        $data['itemsPerPage'] = $itemsPerPage;
        $createdBy = NULL;
        //$createdBy = $userData->id;
        if (!$page) {
            $page = 0;
        } else {
            $page = $page - 1;
        }
        $offset = $page * $itemsPerPage;
        try {

            if ($userRole != 1):
                if ($userRole == 2):
                    $userIdsRes = $this->user_model->getUserIdsForJob($userLoggedinUserId);
                    $userIdsStr = $userIdsRes['userIds'];
                    if ($userIdsStr != ""):
                        $createdBy = $userIdsStr . "," . $userLoggedinUserId;
                    else:
                        $createdBy = $userLoggedinUserId;
                    endif;
                elseif ($userRole == 4 || $userRole == 5):
                    $userDataDetails = $this->user_model->getUser($userLoggedinUserId);

                    if ($userDataDetails):
                        $corporateId = $userDataDetails['createdBy'];
                        $userIdsRes = $this->user_model->getUserIdsForJob($corporateId);
                        $userIdsStr = $userIdsRes['userIds'];
                        if ($userIdsStr != ""):
                            $createdBy = $userIdsStr . "," . $corporateId;
                        else:
                            $createdBy = $userLoggedinUserId . "," . $corporateId;
                        endif;
                    else:
                        $createdBy = "";
                    endif;
                else:
                    $createdBy = "";
                endif;
            else:
                $createdBy = "";
            endif;

            //$createdBy = $createdBy.",";
            if ($createdBy != "") {
                $createdByArr = explode(",", $createdBy);
                $createdByArr = array_filter($createdByArr);
                $createdByStr = implode(",", $createdByArr);
            }
            //echo $createdByStr;
            //die();
            $result = $this->job_model->listJobs($itemsPerPage, $offset, $serachColumn, $searchText, $createdByStr, $userRole);
            //echo "<pre>";print_r($result);
            $completeResult = $this->job_model->listJobs(NULL, NULL, NULL, NULL, $createdByStr, $userRole);
            $count = ceil(count($completeResult) / $itemsPerPage);
            $data['count'] = $count;
            $data['totalCount'] = count($completeResult);
            $data['content'] = $result;
            $data['main_content'] = 'fj-corporate-dashboard';
            //$data['searchText'] = $searchText;
            //$data['serachColumn'] = $serachColumn;
            $data['token'] = $token;
            $data['userId'] = $userData->id;
            $data['fileEist'] = 0;
            $inviteUsersPath = $this->config->item('inviteUsersPath');
            $path = $inviteUsersPath . 'inviteUsers' . $userData->id . '.csv';
            if (file_exists($path)) {
                $data['fileEist'] = 1;
            }
            
            $data['openJobs']               = $this->corporatereport_model->openJobs($userLoggedinUserId);
            $data['totalJobs']              = $this->corporatereport_model->totalJobs($userLoggedinUserId);
            $data['lastWeekJobs']           = $this->corporatereport_model->lastWeekJobs($userLoggedinUserId);
            $data['lastMonthJobs']          = $this->corporatereport_model->lastMonthJobs($userLoggedinUserId);
            
            $data['totalApplication']       = $this->corporatereport_model->totalApplication($userLoggedinUserId);
            $data['pendingApplication']     = $this->corporatereport_model->pendingApplication($userLoggedinUserId);
            $data['shortlistedApplication'] = $this->corporatereport_model->shortlistedApplication($userLoggedinUserId);
            $data['rejectedApplication']    = $this->corporatereport_model->rejectedApplication($userLoggedinUserId);
            
            $data['highestPendingJob']      = $this->corporatereport_model->highestPendingJob($userLoggedinUserId);
            $data['longestOpenJob']         = $this->corporatereport_model->longestOpenJob($userLoggedinUserId);            
            $data['leastApplicationJob']    = $this->corporatereport_model->leastApplicationJob($userLoggedinUserId);
            
            //$data['totalUser']              = $this->corporatereport_model->totalUser($userLoggedinUserId);
            $data['manageJobs']             = $this->corporatereport_model->manageJobs($userLoggedinUserId);
            $data['manageInterview']        = $this->corporatereport_model->manageInterview($userLoggedinUserId);
            $data['manageAssessment']       = $this->corporatereport_model->manageAssessment($userLoggedinUserId);
            $data['queueManagement']        = $this->corporatereport_model->queueManagement($userLoggedinUserId);
            $data['searchCandidate']        = $this->corporatereport_model->searchCandidate($userLoggedinUserId);
            
            
            
            
            $data['assignedJobs']                   = $this->corporatereport_model->assignedJobs($userLoggedinUserId);
            $data['assignedActiveJobs']             = $this->corporatereport_model->assignedActiveJobs($userLoggedinUserId);
            $data['assignedLastWeekJobs']           = $this->corporatereport_model->assignedLastWeekJobs($userLoggedinUserId);
            $data['assignedLastMonthJobs']          = $this->corporatereport_model->assignedLastMonthJobs($userLoggedinUserId);
            
            $data['assignedTotalApplication']       = $this->corporatereport_model->assignedTotalApplication($userLoggedinUserId);
            $data['assignedPendingApplication']     = $this->corporatereport_model->assignedPendingApplication($userLoggedinUserId);
            $data['assignedShortlistedApplication'] = $this->corporatereport_model->assignedShortlistedApplication($userLoggedinUserId);
            $data['assignedRejectedApplication']    = $this->corporatereport_model->assignedRejectedApplication($userLoggedinUserId);
            
            $data['assignedAsL1']                   = $this->corporatereport_model->assignedAsL1($userLoggedinUserId);
            $data['assignedAsL2']                   = $this->corporatereport_model->assignedAsL2($userLoggedinUserId);
            
            $data['assignedHighestPendingJob']      = $this->corporatereport_model->assignedHighestPendingJob($userLoggedinUserId);
            $data['assignedLongestOpenJob']         = $this->corporatereport_model->assignedLongestOpenJob($userLoggedinUserId);
            $data['assignedLeastApplicationJob']    = $this->corporatereport_model->assignedLeastApplicationJob($userLoggedinUserId);
            
            
            //echo "<pre>";  print_r($data);  print_r($data['longestOpenJob']['LongestJobIds']);         exit;
            $this->load->view('fj-mainpage', $data); //die();
            if (!$result) {
                //throw new Exception('Could not get list of users!');
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }

        //Jobs Listing End
    }

//    function replicate() {
//        $jobId = $this->input->post('jobId', true);
//        $this->job_model->replicateJob($jobId);
//    }
    
    function replicate() {
        $jobId              = $this->input->post('jobId', true);        
        $job                = $this->job_model->getJob($jobId);
        $jobLocations       = getLocationsByJob($jobId);
        $jobQualification   = getQualificationsByJob($jobId);
        $userData = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            $userId = 1;
        }
        
        $data['jobId']              = $job['jobId'];
        $data['title']              = $job['title'];
        $data['description']        = $job['description'];
        $data['noOfVacancies']      = $job['noOfVacancies'];
        $data['ageFrom']            = $job['ageFrom'];
        $data['ageTo']              = $job['ageTo'];
        $data['salaryFrom']         = $job['salaryFrom'];
        $data['salaryTo']           = $job['salaryTo'];
        $data['noticePeriod']       = $job['noticePeriod'];
        $data['expFrom']            = $job['expFrom'];
        $data['expTo']              = $job['expTo'];
        $data['openTill']           = $job['openTill'];
        $data['jd']                 = $job['jd'];
        $data['jdThumbnail']        = $job['jdThumbnail'];
        $data['interview']          = $job['interview'];
        $data['assessment']         = $job['assessment'];
        $data['preInterviewText']   = $job['preInterviewText'];
        $data['postInterviewText']  = $job['postInterviewText'];
        $data['preAssessmentText']  = $job['preAssessmentText'];
        $data['postAssessmentText'] = $job['postAssessmentText'];
        $data['industryId']         = $job['industryId'];
        $data['deptId']             = $job['deptId'];
        $data['status']             = '1';
        $data['posted']             = '2';        
        $data['createdAt']          = date('Y-m-d h:i:s');
        $data['updatedAt']          = date('Y-m-d h:i:s');
        $data['fjCode']             = $this->generateRandomString();
        $data['createdBy']          = $userId;
        
        $resp = $this->job_model->addJob($data);
        
        if(count($jobLocations)>0) {
            foreach($jobLocations as $location) {
                $locations[] = $location['location'];
            }
            $this->job_model->addLocation($locations, $resp);
        }
        
        if(count($jobQualification)>0) {
            $this->job_model->addJobQualification($jobQualification, $resp);
        }
        
        //echo "<pre>"; print_r($resp); exit;
        echo $resp;
         
    }
    
    
    
    function generateVideo(){
        //print_r($_POST); exit;
        $jobId  = $this->input->post('jobId', true);
        $userId = $this->input->post('userId', true);
        //print_r($jobId); die();
        $job    = $this->job_model->generateVideo($userId, $jobId);
        //print_r($jobId); die();
        if ($job == "success") {
            return "success";
        } else {
            return "fail";
        }
        
    }
    
    
    
    
    

    /**
     * GetJob function
     * Gets job specific detail.
     */
    function userVideo() {        
        $videoName  = trim($this->input->post('interviewVideo', true));        
        $userJob    = trim($this->input->post('userJob', true));
        $ENCvideo   = encryptURLparam($videoName, URLparamKey);
        $ENCuserJob = encryptURLparam($userJob, URLparamKey);
        
        $returndata = '<div id="player_32931" style="display:inline-block;">
                                <a href="https://get.adobe.com/flashplayer/">You need to install the Flash plugin</a>
                            </div>
                            <script type="text/javascript">
                                var flashvars_32931 = {};
                                var params_32931 = {
                                    quality: "high",
                                    wmode: "transparent",
                                    bgcolor: "#ffffff",
                                    allowScriptAccess: "always",
                                    allowFullScreen: "true",
                                    flashvars: "fichier=https://firstjob.co.in/admin/uploads/mergeVideo/'.$videoName.'&apercu=https://firstjob.co.in/admin/uploads/bg_black.png"
                                };
                                var attributes_32931 = {};
                                flashObject("https://firstjob.co.in/admin/uploads/v1_27.swf", "player_32931", "400", "250", "8", false, flashvars_32931, params_32931, attributes_32931);
                            </script>';
        $returndata .= "<input type='hidden' name='videoName' id='videoName' value='".$ENCvideo."' />";
        $returndata .= "<input type='hidden' name='userJob' id='userJob' value='".$ENCuserJob."' />";
        $abc = "https://firstjob.co.in/admin/uploads/mergeVideo/".$videoName;
        //https://s3.amazonaws.com/videoMerge/
        print_r($abc);
    }
    
    

    /**
     * GetJob function
     * Gets job specific detail.
     */
    function sendInterview() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token      = $this->config->item('accessToken');
        $userData   = $this->session->userdata['logged_in'];
        $userId     = $userData->id;        
        $fromEmail  = $this->config->item('fromEmail');
        
        if (isset($userData->id) && $userData->id != NULL) {        
            if (isset($_POST) && $_POST != NULL) {
                $email1     = $this->input->post('email1', true);
                $email2     = $this->input->post('email2', true);
                $email3     = $this->input->post('email3', true);
                $email4     = $this->input->post('email4', true);
                $email5     = $this->input->post('email5', true);
                $videoName  = $this->input->post('videoName', true);
                $userJob    = $this->input->post('userJob', true);
                
                $resp['error'] = '';
                if ($token != $this->input->post('accessToken', true)) {
                    $resp['error'] = 'You do not have pemission.';
                }
                
                if ($userJob == '' || $videoName == '') {
                    $resp['error'] = '<p>Something wents wrong.</p>';
                }
                
                if ($email1 == '' && $email2 == '' && $email3 == '' && $email4 == '' && $email5 == '') {
                    $resp['error'] = '<p>Please Fill Email Id.</p>';
                }
                
                // Recepeint Email //
                if($this->input->post('email1', true)!='') {
                    $this->form_validation->set_rules('email1', 'Email 1', 'trim|required|valid_email');
                    $emailId[]  = $this->input->post('email1', true);
                }
                if($this->input->post('email2', true)!='') {
                    $this->form_validation->set_rules('email2', 'Email 2', 'trim|required|valid_email');
                    $emailId[]  = $this->input->post('email2', true);
                }
                if($this->input->post('email3', true)!='') {
                    $this->form_validation->set_rules('email3', 'Email 3', 'trim|required|valid_email');
                    $emailId[]  = $this->input->post('email3', true);
                }
                if($this->input->post('email4', true)!='') {
                    $this->form_validation->set_rules('email4', 'Email 4', 'trim|required|valid_email');
                    $emailId[]  = $this->input->post('email4', true);
                }
                if($this->input->post('email5', true)!='') {
                    $this->form_validation->set_rules('email5', 'Email 5', 'trim|required|valid_email');
                    $emailId[]  = $this->input->post('email5', true);
                }
                // Recepeint Email //
                             
                if ($this->form_validation->run() == false) {
                    $resp['error'] = validation_errors();
                }
                
                
                if (!$resp['error']) {
                    $data['userJobId']  = decryptURLparam($userJob, URLparamKey);
                    $videoName          = $this->input->post('videoName', true);
                    $data['videoLink']  = decryptURLparam($videoName, URLparamKey);
                    $data['createdBy']  = $userId;
                    
                    //print_r($data); exit;
                    
                    foreach($emailId as $email) {
                        try {
                            //echo $email; exit;
                            $data['emailId']  = $email;
                            $resp = $this->job_model->sendInterview($data);
                            if ($resp) {
                                $this->email->from($fromEmail, 'First Job');
                                $this->email->subject('First Job - Interview Video');
                                $this->email->set_mailtype('html');
                                $this->email->to($email);
                                
                                $temp['videoLink']  = base_url().'page/userInterview/'.encryptURLparam($videoName, URLparamKey);;                                
                                
                                $body = $this->load->view('emails/interviewTemp.php', $temp, TRUE);
                                $this->email->message($body);
                                $this->email->send();
                            }
                        } catch (Exception $ex) {
                            $resp['error'] = $e->getMessage();
                        }
                    }
                }
            }
        }
    }
    
    // Functions written by wildnet end
}

?>


<div id="page-content-wrapper" style="margin-top: 52px;">
    <div class="container-fluid whitebg">
        <div class="row">
            <div class="col-md-3">
                <h3 style="font-size: 20px !important;">Active Jobs</h3>
            </div>
            
            <div class="col-md-5" style="padding-bottom:17px">
                <a href="<?php echo base_url(); ?>jobs/add"><button type="button" class="acu_bt pull-right" style="background: #054f72 none repeat scroll 0 0; color:white;">Add Job</button></a>
            </div>

            <div class="col-md-2" style="padding-bottom:17px">
                <a href="<?php echo base_url(); ?>jobs/listPastJobs"><button type="button" class="acu_bt pull-left" style="background: #054f72 none repeat scroll 0 0; color:white;">Past Jobs</button></a>
            </div>
        </div>
        <div class="row">
            <div class="clearfix"></div>
            <div class="col-md-9 rsp_tbl" style="font-size: 14px;">
                <table class="table sortable" id="sorting">
                    <thead class="tbl_head">
                        <tr>
                            <th><span>Job Title</span></th>
                            <th><span>Job Code</span></th>
                            <th class="th-location"><span>Location</span></th>
                            <th><span>Expiry Date</span></th>
                            <th><span>Status</span></th>
                            <th><span>Applications</span><br/><span>Pending | Shortlisted | Rejected</span></th>
                            <th><span>Action</span></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 0;
                        if( $content ):
                            foreach ($content as $item):
                                // ci instance
                                $ci =&get_instance();
                                //job user answer model
                                $ci->load->model('userjob_model');
                                $ci->load->helper('fj');
                                // function to get user's job from 
                                $userjobcountres = $ci->userjob_model->countForUserJob($item->id); 
                                $userjobcount = $userjobcountres['userjobcount'];

                                //echo "<pre>";print_r($jobs);die;
                                $userjobpendingcountres     = $ci->userjob_model->countForUserJobs($item->id, 1);
                                $userjobpendingcount        = $userjobpendingcountres['appliedCount'];
                                                  
                                $userjobshortlistedcountres = $ci->userjob_model->countForUserJobs($item->id, 2);   
                                $userjobshortlistedcount    = $userjobshortlistedcountres['appliedCount'];                    
                                
                                $userjobrejectedcountres    = $ci->userjob_model->countForUserJobs($item->id, 3);   
                                $userjobrejectedcount       = $userjobrejectedcountres['appliedCount'];

                                $levelCountArray = $ci->userjob_model->getLevelCountForJob($item->id);
                                $levelCount = $levelCountArray['levelCount'];
                                ?>
                                <tr>
                                    <td style="width: 15%;"><?php echo substr($item->title, 0, 30); echo (strlen($item->title)>30?'...':''); ?></td>
                                    <td style="width: 13%;"><?php echo $item->fjCode; ?></td>
                                    <td style="width: 15%;"><?php echo substr($item->locationName, 0, 40); echo (strlen($item->locationName)>40?'...':''); ?></td>
                                    <td style="width: 12%;"><?php echo ($item->openTill != '0000-00-00 00:00:00') ? date('d-m-Y',strtotime($item->openTill)) : 'NA'; ?></td>
                                    <td style="width: 15%;">
                                        <?php 
                                            if($item->status == 5) {
                                                echo 'Application Closed';
                                            } else if($item->status == 1 && $item->posted == 1) {
                                                echo 'Active - Job Posted';
                                            } else if(($item->status == 1) && empty($item->interview) && ($levelCount <= 0)) {
                                                echo 'Pending Interview setup and Recruiter Setup';
                                            } else if(($item->status == 1) && empty($item->interview) && ($levelCount >= 0)) {
                                                echo 'Pending Interview setup';
                                            } else if(($item->status == 1) && !empty($item->interview) && ($levelCount <= 0)) {
                                                echo 'Pending Recruiter setup';
                                            } else {
                                                echo 'Active';
                                            }
                                        ?>
                                    </td>
                                    <td style="width: 15%;"><?php echo $userjobpendingcount .' | '. $userjobshortlistedcount .' | '. $userjobrejectedcount ;?> - <a title="View Applications" style="color: #337ab7 !important;" href="<?php echo base_url().'queue/list?JobId='.encryptURLparam($item->id, URLparamKey).'&hide=1' ;?>">View</a></td>
                                    <td style="width: 15%;">
                                        <table>
                                            <tr style="background-color: white;">
                                                <?php
                                                    if(empty($item->interview) || ($levelCount <= 0)) {
                                                        $disabled = 'pointer-events: none;';
                                                    } else {
                                                        $disabled = '';
                                                    }
                                                ?>
                                                <td style="width: 17%;">
                                                    <?php  if($item->posted == 2): ?>
                                                    <a  title="Make this job searchable for job seekers"  style="cursor:pointer;margin-right: 18px; <?php echo $disabled ?>" class="post-job" id="<?php echo $item->id;?>" target="<?php echo $item->posted;?>">
                                                    <i class="fa fa-unlock" aria-hidden="true" style="color: #777;"></i>
                                                    </a>
                                                    <?php else:?>
                                                    <a title="Make this job unsearchable for job seekers"  style="cursor:pointer;margin-right: 18px; <?php echo $disabled ?>" class="post-job" id="<?php echo $item->id;?>" target="<?php echo $item->posted;?>">
                                                    <i class="fa fa-lock" aria-hidden="true" style="color: #777;"></i>
                                                    </a>
                                                    <?php endif;?>
                                                </td>
                                                <td style="width: 17%;">
                                                    <a title="Invite users" href="javascript:" style="margin-right: 18px; <?php echo $disabled ?>" class="invite-users" data-toggle="modal" data-target="#inviteUsersForJob" id="<?php echo $item->id; ?>"><i class="fa fa-users" aria-hidden="true" style="color: #777;"></i></a>
                                                </td>
                                                <td style="width: 17%;">
                                                    <a title="Stop receiving applications for this job" href="javascript:" class="closeapplication-job" id="<?php echo $item->id; ?>" style="margin-right: 18px; <?php echo $disabled ?>"><i class="fa fa-ban" aria-hidden="true"></i></a>
                                                </td>


                                                <td style="width: 17%;">
                                                    <a title="Close this job" href="javascript:" class="closethis-job" id="<?php echo $item->id; ?>" style="margin-right: 18px;"><i class="fa fa-times-circle" aria-hidden="true" style="color: #777;"></i></a>
                                                </td>

                                                <td style="width: 17%;">
                                                    <a title="Edit this job" href="<?php echo base_url(); ?>jobs/edit/<?php echo $item->id; ?>" style="margin-right: 18px;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                                </td>

                                                <td style="width: 17%;">
                                                    <?php if( $userjobcount > 0 ) { ?>
                                                        <a title="Delete this job" href="javascript:" class="notdelete-job" id="<?php echo $item->id; ?>" style="margin-right: 18px;"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                                    <?php } else { ?>
                                                        <a title="Delete this job" href="javascript:" class="delete-job" id="<?php echo $item->id; ?>" style="margin-right: 18px;"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                                    <?php } ?>
                                                </td>

                                                <td style="width: 15%;">
                                                    <a title="Replicate this job" href="javascript:" class="replciate-job-class" id="<?php echo $item->id; ?>" style="margin-right: 18px;"><i class="fa fa-refresh" aria-hidden="true"></i></a>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <?php
                                $i++;
                            endforeach;
                        else:
                            echo "<tr><td colspan=\"2\">Jobs not available.</td></tr>";
                        endif;
                        ?>
                    </tbody>
                </table>

                <div class="row">
                    <div class="col-md-8 col-md-offset-4">
                        <?php if(count($content)>0) { echo $this->pagination->create_links(); } ?>
                        <!--<div class="service_content ">
                            <?php //if ($totalCount > $itemsPerPage): ?>
                               
                            <?php //endif; ?>
                        </div>-->
                    </div>
                </div>
        
            </div>
            <div class="col-md-3" style="font-size: 14px; background-color: #eeeeee;">
            <div class="clearfix"></div>
                <div style="margin-bottom: 27px; margin-top: 15px; font-size: 18px;">Refine Results</div>
                <form action="<?php echo base_url(); ?>jobs/list/1" method="post">
                    <div class="row" style="margin-bottom:19px!important;">
                        <span>Sort Jobs by</span>
                        <div class="col-md-12" id="sortingFilters" style="margin-top:5px;">
                            <label class="radio-inline">
                              <input type="radio" id="dateFilters" name="sortJobsResult" value="dateSort" <?php echo (!empty($_SESSION['sortJobsResult']) && $_SESSION['sortJobsResult'] == 'dateSort') ? 'checked' :'' ;?>>Date
                                <?php 
                                    if(!empty($_SESSION['sortJobsResult']) && $_SESSION['sortJobsResult'] == 'dateSort') {
                                        if($_SESSION['sortingOrder'] == 'ASC') { 
                                ?>
                                <i class="fa fa-sort-asc" aria-hidden="true"><input id="deleteInput" type="hidden" name="sortingOrder" value="ASC"></i>
                                <?php } else { ?>
                                <i class="fa fa-sort-desc" aria-hidden="true"><input id="deleteInput" type="hidden" name="sortingOrder" value="DESC"></i>
                                <?php } }?>

                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="sortJobsResult" value="jobSort" <?php echo (!empty($_SESSION['sortJobsResult']) && $_SESSION['sortJobsResult'] == 'jobSort') ? 'checked' :'' ;?>>Job Title

                              <?php 
                                    if(!empty($_SESSION['sortJobsResult']) && $_SESSION['sortJobsResult'] == 'jobSort') {
                                        if($_SESSION['sortingOrder'] == 'DESC') { 
                                ?>
                                <i class="fa fa-sort-desc" aria-hidden="true"><input id="deleteInput" type="hidden" name="sortingOrder" value="DESC"></i>
                                <?php } else { ?>
                                <i class="fa fa-sort-asc" aria-hidden="true"><input id="deleteInput" type="hidden" name="sortingOrder" value="ASC"></i>
                                <?php } }?>

                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="sortJobsResult" value="locationSort" <?php echo (!empty($_SESSION['sortJobsResult']) && $_SESSION['sortJobsResult'] == 'locationSort') ? 'checked' :'' ;?>>Location

                              <?php 
                                    if(!empty($_SESSION['sortJobsResult']) && $_SESSION['sortJobsResult'] == 'locationSort') {
                                        if($_SESSION['sortingOrder'] == 'DESC') { 
                                ?>
                                <i class="fa fa-sort-desc" aria-hidden="true"><input id="deleteInput" type="hidden" name="sortingOrder" value="DESC"></i>
                                <?php } else { ?>
                                <i class="fa fa-sort-asc" aria-hidden="true"><input id="deleteInput" type="hidden" name="sortingOrder" value="ASC"></i>
                                <?php } }?>

                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <span>Filter Jobs by</span>
                        <div class="col-md-12" style="margin-top: 8px;">
                            <div class="form-group">
                                <label style="font-weight: lighter;">Keyword </label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control" name ="serachText" value ="<?php echo $this->session->userdata('searchText'); ?>" placeholder="Search for jobs">
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="form-group">
                                <label style="font-weight: lighter;" for="location">Job Location </label>
                                <div class="col-md-12">
                                    <select data-placeholder="Location" style="width: 350px; display: none;" multiple="" class="chosen-select location" tabindex="-1" name="serachColumn[]" data-validation="alphanumeric required">
                                        <option value=""></option>
                                        <?php foreach ($city as $item) : ?>
                                            <option value="<?php echo $item['id']; ?>" 
                                                <?php
                                                    if(!empty($_SESSION['serachColumn'])) {
                                                        echo in_array($item['id'], $_SESSION['serachColumn']) ? 'selected' : '' ;
                                                    }
                                                ?>
                                                >
                                                <?php echo $item['city'] . ', ' . $item['state']; ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <!--<div class="form-group">
                                <label style="font-weight: lighter;">Experience <i class="fa fa-caret-down" aria-hidden="true" style="margin-left: 7px;"></i></label>
                                <div class="row">
                                    <div class="col-md-6">
                                        <select class="form-control">
                                            <option value="">Min</option>
                                            <option>1</option>
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <select class="form-control">
                                            <option value="">Max</option>
                                            <option>1</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>-->

                            <div class="clearfix"></div>
                            <div class="form-group">
                                <div class="row">
                                    <button type="submit" name="submit" value="submit" class="btn btn-default col-md-4" style="color : #fff; background-color: #054F72; margin-left: 38px; margin-top: 45px;">Apply</button>
                                    <button type="submit" name="reset" value="reset" class="btn btn-default col-md-4" style="color : #fff; background-color: #054F72; margin-left: 29px; margin-top: 45px;">Reset</button>
                                </div>
                            <div class="form-group">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>







<!-- PopUp for invite users for a job  -->
<div id="inviteUsersForJob" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <form class="form-horizontal form_save" action="<?php echo base_url(); ?>assessments/addToAssessment" method="post" id="interviewQuestionForm" name="interviewQuestionForm" >
            <div class="modal-content">

                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Invite Users</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger hidden" id="inviteAlert">

                    </div>
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-3">Inivitation By</label>
                        <div class="col-xs-6">
                            <input type="checkbox" name="inviteFrom[]" id="inviteFrom_1" value="1" checked="" />&nbsp;Email&nbsp;&nbsp;
                            <input type="checkbox" name="inviteFrom[]" id="inviteFrom_2" value="2" />&nbsp;SMS
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-3">Upload File</label>
                        <div class="col-xs-8">
                            <input type="file" name="inviteUsers" id="inviteUsers" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-3"></label>
                        <div class="col-xs-4 aligncenter">
                            <?php
                            $fileCsv = base_url() . 'uploads/inviteUserFiles/inviteUsers' . $userId . '.csv';
                            $fileSamleCsv = base_url() . 'uploads/inviteUserFiles/inviteSampleFile.csv';
                            if ($fileEist == 1):
                                ?>
                                <a href="<?php echo $fileCsv; ?>"> Download File</a>
                            <?php else: ?>
                                <a href="<?php echo $fileSamleCsv; ?>"> Download Sample File</a>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-3">Customize Email</label>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-3">Email Subject</label>
                        <div class="col-xs-8">
                            <input type="text" class="form-control search-critera" name="emailSubject" id="emailSubject" maxlength="50" placeholder="Email Subject" data-validation="required" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-3">Email Message</label>
                        <div class="col-xs-8">
                            <script type="text/javascript" src="//cdn.tinymce.com/4/tinymce.min.js"></script>
                            <script>
                                tinymce.init({
                                    selector: '#emailMessage',
                                    height: 150,
                                    menubar:false,
                                    statusbar: false,
                                    toolbar: 'bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent |',
                                    content_css: [
                                      '//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
                                      '//www.tinymce.com/css/codepen.min.css'
                                    ]
                                });
                            </script>
                            <textarea type="text" class="form-control search-critera" name="emailMessage" id="emailMessage" maxlength="500" placeholder="Email Message"></textarea>
                            <input type="text" data-validation="emailMessage" style="height: 0px; width: 0px; visibility: hidden; " /> 
                        </div>                        
                    </div>

                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-4">Customize SMS message</label>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-3">Message</label>
                        <div class="col-xs-8">
                            <textarea class="form-control" rows="3" id="smsMessage" name="smsMessage" maxlength="200"></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3"></label>
                        <div class="col-xs-4 aligncenter">
                            <button type="button" class="btn btn-default upload-file" style="color : #fff; background-color: #054F72;">Send Invitation</button>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">


                    <!--                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
                </div>
            </div>
        </form>

    </div>
</div>



<style>
    .service_content ul.service_content_last_list li{ display:inline-block; font-size:20px; width:38px; height:38px; line-height:38px; border:0px; }
    .service_content ul.service_content_last_list li a{ color:#333333; padding:0px; text-align:center; display:block; border:0px;  }
    .service_content ul.service_content_last_list li a:hover{ color:#fff; border-radius:50px; -moz-border-radius:50px; -webkit-border-radius:50px; background:#0072dc }
    .response_img img{ max-width: 100%; }
    .service_content .service_content_last_list{ text-align:center; padding:10px 0 70px; }
    ul.service_content_last_list  li.pagination-last-list{padding:0 20px 0 0px; cursor:pointer; text-indent:-9999px; margin-left:5px;  }
    .pagi_img{display: -webkit-inline-box;}
    .pagi_img a img{display: -webkit-inline-box;}
    .service_content ul.service_content_last_list li{margin:0px!important;}
    h1{font-size:32px!important;}
    .acu_bt a {
        text-decoration: none;
        color: #fff;
    }
    .sel_opt {
        font-size:15px;
        width:33% !important;
        padding-left: 9px !important;
    }
    #sorting td a {color: #303030!important; }
    #sorting td a:hover {color: #777!important; }
    #mceu_15 {display: none;}
</style>
<script src="<?php echo base_url(); ?>theme/firstJob/js/chosen.jquery.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>theme/firstJob/js/custom-script.js"></script>
<script src="<?php echo base_url(); ?>theme/firstJob/js/bootstrap.min.js"></script>
<!--<script src="<?php echo base_url(); ?>theme/firstJob/js/tablesort.js"></script>-->
<!--<script src="<?php echo base_url(); ?>theme/firstJob/js/jquery.tablesorter.js"></script>-->
<!--<script src="https://code.jquery.com/jquery-1.10.2.js"></script>-->
<script>
    $(function () {

        var settingTableRowColor = function (tableName) {
            $('#' + tableName + ' tbody tr').removeAttr('class');
            $('#' + tableName + ' tbody tr:odd').attr('class', 'tbl_bg');
        }
        settingTableRowColor('sorting');
        $('.sort-header').on('click', function () {
            settingTableRowColor('sorting');
        });
        $('.delete-user').on('click', function () {
            var url = '<?php echo base_url(); ?>';
            var userId = $(this).attr('id');
            var accessToken = $('#accessToken').val();
            bootbox.confirm('Are you sure you want to delete?', function (result) {
                //alert(result);
                if (result) {
                    $.post(url + 'users/delete', {id: userId, accessToken: accessToken}, function (data) {
                        //bootbox.alert(data);
                        location.reload();
                    });
                }
            });

        });
    });
    //new Tablesort(document.getElementById('sorting'));
    //$("#sorting").tablesorter();
</script>

<script>
    $(function () {
        var url = '<?php echo base_url(); ?>';
        window.siteUrl = url;
        window.token = '<?php echo $token; ?>';
    });
    
    $('.delete-job').on('click', function () {
        var userId = $(this).attr('id');
        bootbox.confirm('Are you sure you want to delete?', function (result) {
            
            if (result) {
                $.post(siteUrl + 'jobs/delete', {id: userId, accessToken: token}, function (data) {
                    
                    //bootbox.alert(data);
                    location.reload();
                });
            }
        });
    });

    $('.closeapplication-job').on('click', function () {
        var jobId = $(this).attr('id');
        bootbox.confirm('Are you sure you want to stop receiving applications for this job ?', function (result) {
            
            if (result) {
                $.post(siteUrl + 'jobs/closeApplicationForJob', {id: jobId, accessToken: token}, function (data) {
                    location.reload();
                });
            }
        });
    });

    $('.closethis-job').on('click', function () {
        var jobId = $(this).attr('id');
        bootbox.confirm('Are you sure you want to close this job ?', function (result) {
            
            if (result) {
                $.post(siteUrl + 'jobs/closeJob', {id: jobId, accessToken: token}, function (data) {
                    location.reload();
                });
            }
        });
    });

    $('.replciate-job-class').on('click', function () {
        var jobId = $(this).attr('id');
        bootbox.confirm('Do you really want to replicate this job ?', function (result) {
            if (result) {
                $.post(siteUrl + 'jobs/replicate', {jobId: jobId, accessToken: token}, function (data) {
                    if (data) {
                        bootbox.alert('The job is replicated successfully!', function () {
                            window.location = siteUrl+"jobs/edit/"+data;
                        });
                    }
                });
            }
        });
    });

    var config = {
        '.chosen-select': {},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
//dateFilters
    $(document).ready(function() { 
        $('input[name=sortJobsResult]').click(function(){
            var id = $(this).attr('id');
            if(id=='dateFilters') {
                if ( $( this ).parent().children().hasClass( "fa-sort-desc" ) ) {
                    $( '.fa-sort-asc' ).removeClass( "fa-sort-asc");
                    $( '.fa-sort-desc' ).removeClass( "fa-sort-desc" );
                    $("#deleteInput").remove();
                    $( this ).parent().append( '<i class="fa fa-sort-asc" aria-hidden="true"><input id="deleteInput" type="hidden" name="sortingOrder" value="ASC"></i>' );
                } else {
                    $( '.fa-sort-desc' ).removeClass( "fa-sort-desc" );
                    $( '.fa-sort-asc' ).removeClass( "fa-sort-asc" );
                    $("#deleteInput").remove();
                    $( this ).parent().append( '<i class="fa fa-sort-desc" aria-hidden="true"><input id="deleteInput"  type="hidden" name="sortingOrder" value="DESC"></i>' );
                }
            } else {
                if ( $( this ).parent().children().hasClass( "fa-sort-asc" ) ) {
                    $( '.fa-sort-asc' ).removeClass( "fa-sort-asc");
                    $( '.fa-sort-desc' ).removeClass( "fa-sort-desc" );
                    $("#deleteInput").remove();
                    $( this ).parent().append( '<i class="fa fa-sort-desc" aria-hidden="true"><input id="deleteInput"  type="hidden" name="sortingOrder" value="DESC"></i>' );
                } else {
                    $( '.fa-sort-desc' ).removeClass( "fa-sort-desc" );
                    $( '.fa-sort-asc' ).removeClass( "fa-sort-asc" );
                    $("#deleteInput").remove();
                    $( this ).parent().append( '<i class="fa fa-sort-asc" aria-hidden="true"><input id="deleteInput" type="hidden" name="sortingOrder" value="ASC"></i>' );
                }
            }

            //$(this).toggle();
            //$('#jobsortform').submit();
        });
    });
</script>


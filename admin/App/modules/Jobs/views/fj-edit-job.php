<?php
if (isset($job) && $job != NULL) {
    //echo '<pre>';
    //print_r($job);
}
//echo '<pre>'; print_r($subuser);
?>
<div id="page-content-wrapper">
    <?php if (isset($message)): ?>
        <div class="alert alert-success">
            <?php echo $message; ?>
        </div>
    <?php endif; ?>
    <?php if (isset($error)): ?>
        <div class="alert alert-danger">
            <?php foreach ($error as $item): ?>
                <?php echo $item; ?>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
    <div class="container-fluid whitebg">
        <div class="row">

            <div class="col-md-12 padd_tp_bt_20">
                <h4><img src="<?php echo base_url(); ?>/theme/firstJob/image/arrow_head.png" class="prev-page"> Edit Job</h4>
                <form class="form-horizontal form_save" action="" method="post" enctype="multipart/form-data" id="jobForm">
                    <div class="form-group">
                        <label for="inputEmail" class="control-label col-xs-2">Job ID</label>
                        <div class="col-xs-10">
                            <input type="text" class="form-control" placeholder="Job ID" value="<?php
                            if (isset($job['jobId'])): echo $job['jobId'];
                            endif;
                            ?>" name="jobId">
                        </div>
                    </div>

                    <!--
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-2">Industry</label>
                        <div class="col-xs-10">
                            <select data-placeholder="Industry" style="width: 350px; display: none;" class="chosen-select" tabindex="-1" id="industry" name="industry">
                                <option value=""></option>
                                <?php foreach ($industry as $key=>$item) : ?>
                                    <option value="<?php echo $item['id']; ?>" <?php
                                    if (isset($job['industryId']) && in_array($job['industryId'], $industry[$key])): echo "selected";
                                    endif;
                                    ?>><?php echo $item['name']; ?></option>
                    <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    -->
                    
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-2 required-field">Title Of Job</label>
                        <div class="col-xs-10">
                            <input type="text" class="form-control" placeholder="Title Of Job" value="<?php
                            if (isset($job['title'])): echo $job['title'];
                            endif;
                            ?>" name="title" data-validation="required">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-2 required-field">Location</label>
                        <div class="col-xs-10">
                            <select data-placeholder="Location" style="width: 350px; display: none;" multiple="" class="chosen-select location" tabindex="-1" name="cityState[]">
                                <option value=""></option>
                                <?php foreach ($city as $item) : ?>
                                    <option value="<?php echo $item['id']; ?>" <?php
                                    if (isset($job['cityState']) && in_array($item['id'], $job['cityState'])): echo "selected";
                                    endif;
                                    ?>><?php echo $item['city'] . ', ' . $item['state']; ?></option>
                                        <?php endforeach; ?>
                            </select>
                                                <input type="text" data-validation="odd" style="height: 0px; width: 0px; visibility: hidden; " /> 
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-2 required-field">Number Of Vacancies</label>
                        <div class="col-xs-10">
                            <input type="text" class="form-control" placeholder="Number Of Vacancies" value="<?php
                            if (isset($job['noOfVacancies'])): echo $job['noOfVacancies'];
                            endif;
                            ?>" name="vacancy" data-validation="number required">
                        </div>
                    </div>

                    <div class="clearfix">

                        <div class="form-group">
                            <label for="inputPassword" class="control-label col-xs-2 required-field">Age</label>
                            <div class="col-xs-5">
                                <select class="selectpicker ageFrom" name="ageFrom" >
                                    <option value="">From</option>
                                            <?php for ($i = 18; $i <= 99; $i+=1): ?>
                                        <option value="<?php echo $i; ?>" <?php if (isset($job['ageFrom']) && $job['ageFrom'] != NULL && $i == $job['ageFrom']): echo "selected";
                                            endif;
                                            ?>><?php echo $i; ?></option>
<?php endfor; ?>
                                </select>
<!--                                <input type="text" class="form-control" placeholder="From" value="<?php
//                                if (isset($job['ageFrom']) && $job['ageFrom'] != ''): echo $job['ageFrom'];
//                                endif;
                                ?>" name="ageFrom" data-validation="number required">-->
                            </div>


                            <div class="col-xs-5">        
                                <select class="selectpicker ageTo" name="ageTo" >
                                    <option value="">To</option>
                                            <?php for ($i = 18; $i <= 99; $i+=1): ?>
                                        <option value="<?php echo $i; ?>" <?php if (isset($job['ageTo']) && $job['ageTo'] != NULL && $i == $job['ageTo']): echo "selected";
                                            endif;
                                            ?>><?php echo $i; ?></option>
<?php endfor; ?>
                                </select>
<!--                                <input type="text" class="form-control" placeholder="To" value="<?php
//                                if (isset($job['ageTo'])): echo $job['ageTo'];
//                                endif;
                                ?>" name="ageTo" data-validation="number required">-->
                            </div>
                        </div>

                        
                        
    
                                <div class="form-group">
                                    <label for="inputPassword" class="control-label col-xs-2"></label>
                                    <div class="col-xs-10">                                        
                                        <input type="text" data-validation="lessgreaterAge" style="height: 0px; width: 0px; visibility: hidden; " /> 
                                    </div>
                                </div>
                        
                        
                        
                        
                        
                        <div class="clearfix">


                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-2 required-field">Salary</label>
                                <div class="col-xs-5">
                                    <select class="selectpicker salFrom" name="salaryFrom" data-validation="number required">
                                        <option value="">From</option>
                                            <?php for ($i = 0, $j=0; $i <= 1000000, $j<=10; $i+=100000, $j++): ?>
                                            <option value="<?php echo $i; ?>" <?php if (isset($job['salaryFrom']) && $job['salaryFrom'] != NULL && $i == $job['salaryFrom']): echo "selected";
                                                endif;
                                                ?>><?php echo $j; ?> Lakh</option>
                                            <?php endfor; ?>
                                    </select>
                                </div>


                                <div class="col-xs-5">
                                    <select class="selectpicker salTo" name="salaryTo" data-validation="number required">
                                        <option value="">To</option>
                                                <?php for ($i = 0, $j=0; $i <= 1000000, $j<=10; $i+=100000, $j++): ?>
                                            <option value="<?php echo $i; ?>" <?php if (isset($job['salaryTo']) && $job['salaryTo'] != NULL && $i == $job['salaryTo']): echo "selected";
                                                endif;
                                                ?>><?php echo $j; ?> Lakh</option>
<?php endfor; ?>
                                    </select>
                                </div>
                            </div>

    
                                <div class="form-group">
                                    <label for="inputPassword" class="control-label col-xs-2"></label>
                                    <div class="col-xs-10">                                        
                                        <input type="text" data-validation="lessgreaterSal" style="height: 0px; width: 0px; visibility: hidden; " /> 
                                    </div>
                                </div>
                            
                            
                            
                            
                            <div class="clearfix">

                                <div class="form-group">
                                    <label for="inputEmail" class="control-label col-xs-2 ">Availability Notice periods (In Days)</label>
                                    <div class="col-xs-10">
                                        <input type="text" class="form-control noticeperiod" placeholder="Availability Notice periods (In Days)" value="<?php
                                        if (isset($job['noticePeriod'])): echo $job['noticePeriod'];
                                        endif;
                                        ?>" name="noticePeriod" data-validation="noticeperiod">
                                    </div>
                                </div>



                                <div class="form-group">
                                    <label for="inputPassword" class="control-label col-xs-2 required-field">Qualification</label>
                                    <div class="col-xs-10">
                                        <select data-placeholder="Qualification" style="width: 350px; display: none;" multiple="" class="chosen-select qualification" tabindex="-1" name="qualification[]">
                                            <option value=""></option>
                                            <?php foreach ($course as $item) : ?>
                                                <option value="<?php echo $item['id']; ?>" <?php
                                                if (isset($job['qualification']) && in_array($item['id'], $job['qualification'])): echo "selected";
                                                endif;
                                                ?>><?php echo $item['name']; ?></option>
                            <?php endforeach; ?>
                                        </select>
                                        <input type="text" data-validation="quali" style="height: 0px; width: 0px; visibility: hidden; " /> 
                                    </div>
                                </div>

<!--                                <div class="form-group">
                                    <label for="inputPassword" class="control-label col-xs-2">Experience</label>
                                    <div class="col-xs-10">
                                        <select class="selectpicker" name="experience"><option value="">Experience</option>
                                            <?php for ($i = 0; $i <= 10; $i+=1): ?>
                                                <option value="<?php echo $i; ?>" <?php
                                                if (isset($job['experience']) && $job['experience'] != NULL && $i == $job['experience']): echo "selected";
                                                endif;
                                                ?>><?php echo $i; ?> Years</option>
<?php endfor; ?>
                                        </select>
                                    </div>
                                </div>-->
                                
                                <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-2 required-field">Experience</label>
                                <div class="col-xs-5">
                                    <select class="selectpicker expFrom" name="expFrom" data-validation="number required"><option value="">From</option>
<?php for ($i = 0; $i <= 10; $i+=1): ?>
                                                <option value="<?php echo $i; ?>" <?php if (isset($job['expFrom']) && $job['expFrom']!=NULL && $i == $job['expFrom']): echo "selected";
    endif; ?>><?php echo $i; ?> Years</option>
<?php endfor; ?>
                                        </select>
                                </div>


                                <div class="col-xs-5">
                                    <select class="selectpicker expTo" name="expTo" data-validation="number required"><option value="">To</option>
<?php for ($i = 0; $i <= 10; $i+=1): ?>
                                                <option value="<?php echo $i; ?>" <?php if (isset($job['expTo']) && $job['expTo']!=NULL && $i == $job['expTo']): echo "selected";
    endif; ?>><?php echo $i; ?> Years</option>
<?php endfor; ?>
                                        </select>
                                </div>
                            </div>

    
                                <div class="form-group">
                                    <label for="inputPassword" class="control-label col-xs-2"></label>
                                    <div class="col-xs-10">                                        
                                        <input type="text" data-validation="lessgreaterExp" style="height: 0px; width: 0px; visibility: hidden; " /> 
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label for="inputPassword" class="control-label col-xs-2 required-field">Open Till</label>
                                    <div class="col-xs-10">
<!--                                        <select class="selectpicker"><option>Open Till</option>
                                            <option>Ketchup</option>
                                            <option>Relish</option>
                                        </select>-->
                                        <input type="text" class="form-control" placeholder="Open Till" id="datepicker-13" value="<?php
                                        if (isset($job['openTill'])): echo date('m/d/Y', strtotime($job['openTill']));
                                        endif;
                                        ?>" name="openTill" data-validation="required">
                                    </div>
                                </div>
                                <div class="clearfix">
                                    <div class="form-group">
                                        <label for="inputPassword" class="control-label col-xs-2 description required-field">Job Description</label>
                                        <div class="col-xs-5">
                                            <textarea rows="5" style="width:100%" placeholder="Job Description" name="description" data-validation="required"><?php
                                                if (isset($job['description'])): echo $job['description'];
                                                endif;
                                                ?></textarea>
                                        </div>
                                        <div class="col-xs-5">
                                            Upload Video: <input type="file" name="jd" id="jd" value="<?php print_r($files); ?>"/>
                                            <div id="fileHidden">
                                                <?php if (isset($files['jd']) && $files['jd'] != NULL): ?>
                                                    <input type="hidden" name="jd" id="jdHidden" value="<?php print_r($files); ?>"/>
                                                <?php echo $files['jd']['name']; ?>
                                                <?php endif; ?>
                                            </div>

                                            <?php if (isset($job['jd']) && $job['jd'] != 0): ?>
                                            <br/>
                                            <div id="jdvideo"></div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="clearfix">
                                        <div class="form-group">
                                            <label for="inputPassword" class="control-label col-xs-2">Attach Assessment</label>
                                            <div class="col-xs-10">
                                                <select data-placeholder="Select Assessment" style="width: 350px; display: none;"  class="chosen-select examassessment" tabindex="-1" name="assessment">
                                                    
                                                        <option value="0">Select Assessment</option>
                                                    <?php foreach ($assessment as $item) : ?>
                                                        <option value="<?php echo $item['id']; ?>" <?php
                                                        if ($item['id'] == $job['assessment']): echo "selected";
                                                        endif;
                                                        ?>><?php echo $item['name']; ?></option>
<?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputPassword" class="control-label col-xs-2">Attach Inverview</label>
                                            <!--                                            <div class="col-xs-10">
                                                                                            <select class="selectpicker"><option>Attache Inverview</option>
                                                                                                <option>Ketchup</option>
                                                                                                <option>Relish</option>
                                                                                            </select>
                                                                                        </div>-->
                                            <div class="col-xs-10">
                                                <select data-placeholder="Select Interview" style="width: 350px; display: none;"  class="chosen-select examinterview" tabindex="-1" name="interview">
                                                    
                                                        <option value="0">Select Interview</option>
                                                    <?php foreach ($interview as $item) : ?>
                                                        <option value="<?php echo $item['id']; ?>" <?php
                                                        if ($item['id'] == $job['interview']): echo "selected";
                                                        endif;
                                                        ?>><?php echo $item['name']; ?></option>
<?php endforeach; ?>
                                                </select>
                                                <input type="text" data-validation="prime" style="height: 0px; width: 0px; visibility: hidden; " />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputPassword" class="control-label col-xs-2 required-field">Add Level 1 Panel</label>
                                            <div class="col-xs-10">
                                                <select data-placeholder="Level 1 Panel" style="width: 350px; display: none;" multiple="" class="chosen-select level1" tabindex="-1" name="level1[]">
                                                    <option value=""></option>
                                                    <?php foreach ($subuser as $item) : ?>
                                                        <option value="<?php echo $item['id']; ?>" <?php
                                                        if (isset($job['level1']) && in_array($item['id'], $job['level1'])): echo "selected";
                                                        endif;
                                                        ?>><?php echo $item['fullname']; ?></option>
<?php endforeach; ?>
                                                </select>
                                                <input type="text" data-validation="even" style="height: 0px; width: 0px; visibility: hidden; " /> 
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputPassword" class="control-label col-xs-2">Add Level 2 Panel</label>
                                            <div class="col-xs-10">
                                                <select data-placeholder="Level 2 Panel" style="width: 350px; display: none;" multiple="" class="chosen-select" tabindex="-1" name="level2[]">
                                                    <option value=""></option>
                                                    <?php foreach ($subuser as $item) : ?>
                                                        <option value="<?php echo $item['id']; ?>" <?php
                                                        if (isset($job['level2']) && in_array($item['id'], $job['level2'])): echo "selected";
                                                        endif;
                                                        ?>><?php echo $item['fullname']; ?></option>
<?php endforeach; ?>
                                                </select>  
                                            </div>
                                        </div>
                                        <div class="col-xs-offset-2 col-xs-2 no_padding">
                                            <input type="hidden" name="accessToken" value="<?php echo $token; ?>"/>
                                            <button type="submit" class="Save_frm">SAVE</button>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /#page-content-wrapper -->
                    </div></form>


            </div></div></div></div>
<script src="<?php echo base_url(); ?>theme/firstJob/js/chosen.jquery.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>theme/firstJob/js/prism.js" type="text/javascript" charset="utf-8"></script><script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script type="text/javascript">
    





    // Add validator For Level 1
    $.formUtils.addValidator({
        name : 'even',
        validatorFunction : function(value, $el, config, language, $form) {
            if($('.level1').val()!=null) {
                return true;
            }
            else {
                return false;
            }
            //return parseInt(value, 10) % 2 === 0;
        },
        errorMessage : 'This is a required field'
    });

    // Add validator For Location
    $.formUtils.addValidator({
        name : 'odd',
        validatorFunction : function(value, $el, config, language, $form) {
            if($('.location').val()!=null) {
                return true;
            }
            else {
                return false;
            }
            //return parseInt(value, 10) % 2 === 0;
        },
        errorMessage : 'This is a required field'
    });

    // Add validator For Qualification
    $.formUtils.addValidator({
        name : 'quali',
        validatorFunction : function(value, $el, config, language, $form) {
            if($('.qualification').val()!=null) {
                return true;
            }
            else {
                return false;
            }
            //return parseInt(value, 10) % 2 === 0;
        },
        errorMessage : 'This is a required field'
    });

    // Add validator For Interview Assessment
    $.formUtils.addValidator({
        name : 'prime',
        validatorFunction : function(value, $el, config, language, $form) {
            if($('.examinterview').val()!=0 || $('.examassessment').val()!=0) {
                return true;
            }
            else {
                return false;
            }
            //return parseInt(value, 10) % 2 === 0;
        },
        errorMessage : 'Please choose either assessment or interview set'
    });
    
    
    
    
    
    // Add validator For Interview Assessment
    $.formUtils.addValidator({
        name : 'lessgreaterExp',
        validatorFunction : function(value, $el, config, language, $form) {
            if($('.expFrom').val()!='' && $('.expTo').val()!=''){
                if($('.expFrom').val()<$('.expTo').val()) {
                    return true;
                }
                else {
                    return false;
                }
            }
            //return parseInt(value, 10) % 2 === 0;
        },
        errorMessage : 'Experience from value must be less than experience to value'
    });
    
    
    // Add validator For Interview Assessment
    $.formUtils.addValidator({
        name : 'lessgreaterSal',
        validatorFunction : function(value, $el, config, language, $form) {
            if($('.salFrom').val()!='' && $('.salTo').val()!=''){
                if($('.salFrom').val()<$('.salTo').val()) {
                    return true;
                }
                else {
                    return false;
                }
            }
            //return parseInt(value, 10) % 2 === 0;
        },
        errorMessage : 'Salary from value must be less than salary to value'
    });
    
    
    // Add validator For Interview Assessment
    $.formUtils.addValidator({
        name : 'lessgreaterAge',
        validatorFunction : function(value, $el, config, language, $form) {
            if($('.ageFrom').val()=='' && $('.ageTo').val()==''){
                return true;
            }
            else if($('.ageFrom').val()!='' || $('.ageTo').val()!=''){
                if($('.ageFrom').val()<$('.ageTo').val()) {
                    return true;
                }
                else {
                    return false;
                }
            }
            //return parseInt(value, 10) % 2 === 0;
        },
        errorMessage : 'Age from value must be less than age to value'
    });
    
    // Add validator For Interview Assessment
    $.formUtils.addValidator({
        name : 'noticeperiod',
        validatorFunction : function(value, $el, config, language, $form) {
            if($('.noticeperiod').val()==''){
                return true;
            }
            else if($('.noticeperiod').val()!=''){
                if(Math.floor($('.noticeperiod').val()) == $('.noticeperiod').val() && $.isNumeric($('.noticeperiod').val())) {
                    return true;
                }
                else {
                    return false;
                }
            }
            //return parseInt(value, 10) % 2 === 0;
        },
        errorMessage : 'Notice Period must be a number or leave blank'
    });
    
    $.validate({
        form: '#jobForm',
        modules: 'file',
        onSuccess: function ($form) {
            return true;
        },
        onError: function ($form) {
            //alert($('.level1').val());
            return false;
        }
    });
    
    
    var config = {
        '.chosen-select': {},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }

    $(function () {
        $("#datepicker-19").datepicker();
    });

</script>
<script type="text/javascript">
    $(function(){
   var videoUrl = "uploads/jd/<?php echo $job['jd'] ?>";
    jwVideo(videoUrl, 'jdvideo');
    });
</script>
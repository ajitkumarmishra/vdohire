
<script type="text/javascript" src="<?php echo base_url(); ?>/js/Chart.js"></script>
<style>
.canvasjs-chart-canvas{
	border:1px solid black;
}
</style>
<script>
var randomScalingFactor = function() {
        return Math.round(Math.random() * 100);
    };
	var barStackedChartData = {
            labels: ["Week(Apr 28 - May 4)", "Week(Apr 5 - May 11)", "Week(Apr 12 - May 18)", "Week(Apr 19 - May 25)"],
            datasets: [{
                label: 'Received',
				 backgroundColor: "rgb(39,172,191)",
                data: [4, 2, 1, 2],
            }, {
                label: 'Shortlisted',
                backgroundColor: "rgb(212,110,70)",
                data: [8, 4, 3, 7],
            }, {
                label: 'Rejected',
                backgroundColor: "rgb(253,180,65)",
                data: [10, 6, 5, 3],
            }]

        };
	
	var Lineconfig = {
        type: 'line',
		scaleFontColor: '#000',
        data: {
            labels: ["Last 2 Days", "2 to 7 days", "More than 7 days"],
			
            datasets: [{
                label: "Received",
				fontColor: '#000',
                backgroundColor: "rgb(39,172,191)",
                borderColor: "rgb(39,172,191)",
                fill: false,
                data: [12, 10, 3, 5],
            }, {
                label: "Shortlisted",
                backgroundColor: "rgb(253,180,65)",
                borderColor: "rgb(253,180,65)",
				fontColor: '#000',
                fill: false,
                data: [1, 5, 3, 5],
            }]
        },
        options: {
            responsive: true,
			legend: {
                        display: true
                    },
            title:{
                display:true,
                text:'Efficiency',
				fontColor: '#000'
            },
            scales: {
                xAxes: [{
                    gridLines: {
                            display: false,
                            color: "#000"
                        },
                        ticks: {
                            fontColor: "#000"
                        }
                }],
                yAxes: [{
                    gridLines: {
							display: true,
                            color: "#000"
                        },
                        ticks: {
                            beginAtZero:true,
                            stepSize: 4
                        }
                }]
            }
        }
    };
	
	
	var barChartData = {
            labels: ["Ankur", "Gaurav", "Ajeet", "Rahul", "Hemant"],
			display: true,
			fontColor: "#000",
            datasets: [{
                label: 'Pending',
                backgroundColor: "rgb(212,110,70)",
                borderColor: "rgb(212,110,70)",
                borderWidth: 1,
                data: [6, 3, 6, 11, 14]
            }, {
                label: 'Shortlisted',
                backgroundColor: "rgb(253,180,65)",
                borderColor: "rgb(253,180,65)",
                borderWidth: 1,
                data: [4, 5, 3, 15,16]
            },{
                label: 'Rejected',
                backgroundColor: "rgb(39,172,191)",
                borderColor: "rgb(39,172,191)",
                borderWidth: 1,
                data: [8, 5, 3, 8,13]
            }]

        };

		
	var Pieconfig = {
        type: 'pie',
		height: 400,
        data: {
            datasets: [{
                data: [99,11,30],
                backgroundColor: [
                    "rgb(39,172,191)",
                    "rgb(253,180,65)",
                    "rgb(212,110,70)",
                ],
                label: 'Dataset 1'
            }],
            labels: [
                "Pending",
                "Shortlisted",
                "Rejected"
            ]
        },
        options: {
            responsive: true,
			title:{
						display:true,
						text:'Evaluations',
						fontColor: '#000'
					},
        }
    };
	
	$(function () {
		onLoad();
		showPie();
		ShowColumn();
		ShowstackedColumn();
    });
	
    function onLoad() {
		
		var ctx = document.getElementById("chartLine").getContext("2d");
        window.myLine = new Chart(ctx, Lineconfig);
	}
	
	
	function ShowColumn(){
		var ctx = document.getElementById("chartColumn").getContext("2d");
            window.myBar = new Chart(ctx, {
                type: 'bar',
                data: barChartData,
                options: {
					responsive: true,
					legend: {
								display: true
							},
					title:{
						display:true,
						text:'Recruiters',
						fontColor: '#000'
					},
					scales: {
						xAxes: [{
							gridLines: {
									display: false,
									color: "#000"
								},
								ticks: {
									fontColor: "#000"
								}
						}],
						yAxes: [{
							gridLines: {
									display: true,
									color: "#000"
								},
								ticks: {
									beginAtZero:true,
									stepSize: 4
								}
						}]
					}
				}
            });

		
	}
	
	
	function ShowstackedColumn(){
		 var ctx = document.getElementById("chartStackedColumn").getContext("2d");
            window.myBar = new Chart(ctx, {
                type: 'bar',
                data: barStackedChartData,
                options: {
                    title:{
                        display:true,
                        text:"Evaluation Performance"
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false
                    },
                    responsive: true,
                    scales: {
                        xAxes: [{
                            stacked: true,
							gridLines: {
									display: false,
									color: "#000"
								},
								ticks: {
									fontColor: "#000"
								}
                        }],
                        yAxes: [{
                            stacked: true,
							gridLines: {
									display: true,
									color: "#000"
								},
								ticks: {
									beginAtZero:true,
									stepSize: 4
								}
                        }]
                    }
                }
				
				
				
				
				
            });
		
	}
	
	function showPie(){
		var ctx = document.getElementById("chartPie").getContext("2d");
        window.myPie = new Chart(ctx, Pieconfig);
		
	}
	//blinkText
	
</script>

<div class="container-fluid" style="margin-top: 50px;     padding: 13px;   padding-bottom: 6px;background: #054F72;padding-left: 18px;">
<div class="row">

	<div style="float:left;font-size: 22px;color: white;">Recruiters Dashboard</div>
	<div style="float:left;    margin-left: 21%;   width: 24%;">
		<button style="    position: absolute;margin-top: 3px;border: none;background: none;padding-top: 6px;"><i class="fa fa-search" aria-hidden="true"></i></button>
		<input class="form-control" type="" name="search" value="">
	</div>
	<div style="float:left;    padding: 12px;margin-left: 2%;    padding-top: 8px;"><a href="#" style="color:white;"><u>Advance Search</u></a></div>
	<div style="float: right;font-size: 18px;padding-top: 8px;color: white; padding-right: 6px;"><?php echo date("d M Y"); ?></div>
			<div style="clear:both"></div>

</div>
</div>
<div class="container-fluid" >
<div class="row" style="    border: 1px solid black;margin-top: 11px;padding: 10px;    padding: 6px;">
	<div id="blinkText" style="float:left; font-size: 18px;    padding-top: 5px;padding-left: 21px;font-weight:bold;">
	You have 20 NEW APPLICATIONS and 35 PENDING APPLICATIONS from candidate
	</div>
	<div style="background-color: #054f72;    float: right;">
	<a href="#" class="btn" style="color:white;">Review Applications Now!</a>
	</div>
			<div style="clear:both"></div>
</div>
</div>


<div class="container-fluid" >
<div class="row" >
<div class="col-md-5 pull-left" style=" width:49%;   margin-right: 10px;    border: 1px solid black;    margin-top: 11px;">
    <div style="width:100%;">
        <canvas id="chartLine" width="700" height="250"></canvas>
    </div>

</div>

<div class="col-md-5 pull-right" style=" width:49%;    border: 1px solid black;   margin-top: 11px;">
    <div style="width:100%">
        <canvas id="chartColumn"  width="700" height="250"></canvas>
    </div>



</div>


	<div style="clear:both"></div>


<div class="col-md-5" style="  width:49%;   margin-right: 10px;    border: 1px solid black;   margin-top: 2%;">
    <div style="width:100%">
        <canvas id="chartStackedColumn"  width="700" height="250" ></canvas>
    </div>

</div>

<div class="col-md-5 pull-right" style="width:49%;  border: 1px solid black;   margin-top: 2%;">
    <div style="width:100%">
        <canvas id="chartPie"   width="700" height="250"></canvas>
    </div>
</div>



	<div style="clear:both"></div>
</div>
</div>
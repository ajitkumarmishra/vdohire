<?php //echo '<pre>';print_r($content);?>
<div id="page-content-wrapper">
    <div class="container-fluid whitebg">
        <div class="row">
            <div class="col-md-3"><h1><img src="<?php echo base_url(); ?>/theme/firstJob/image/arrow_head.png" class="prev-page">All Jobs</h1></div>
            <div class="col-md-4"><a href="<?php echo base_url(); ?>jobs/add"><button type="button" class="acu_bt pull-right">Add Job</button></a></div>
            <form action="<?php echo base_url(); ?>jobs/list/1" method="post">
                <div class="col-md-5">
                    <select class="sel_opt" name ="serachColumn">
                        <option value="title" <?php if($serachColumn == 'title') echo "selected";?>>Search By Title</option>
                        <option value="city" <?php if($serachColumn == 'city') echo "selected";?>>Search By Location</option>
                        <option value="experience" <?php if($serachColumn == 'experience') echo "selected";?>>Search By Experience</option>
                    </select>
                    <?php if($searchText):?>
                        <div class="cross-search">X</div>
                    <?php endif;?>
                    <input type="text" class="search_box" name ="serachText" placeholder="" value ="<?php print_r($this->session->userdata('searchText')); ?>">
                    <input type="hidden" id= "accessToken" name ="accessToken" value="<?php echo $token; ?>" >
                    <button type="submit" class="srch_bt"><img src="<?php echo base_url(); ?>/theme/firstJob/image/search.png"></button>

                </div>
            </form>

            <div class="clearfix"></div>

            <div class="col-md-12 rsp_tbl">
                <a href="<?php echo base_url(); ?>jobs/view<?php echo $item->id; ?>" ><?php echo $item->title; ?></a>
                <table class="table sortable" id="sorting">
                    <thead class="tbl_head">
                        <tr>
                            <th><span>Title</span></th>
                            <th><span>Company</span></th>
                            <th class="th-location"><span>Location</span></th>
                            <th><span>Qualifications</span></th>
                            <th><span>Experience</span></th>
                            <th><span>End Date</span></th>
                            <th><span>FJ Code</span></th>
                            <th><span>Vacancies</span></th>
                            <th><span>Post</span></th>
                            <th><span>Invite</span></th>
                            <th><span>Status</span></th>
                            <th><span>Action</span></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 0;
                        if( $content ):
                            foreach ($content as $item):
                                // ci instance
                                $ci =&get_instance();
                                //job user answer model
                                $ci->load->model('userjob_model');
                                // function to get user's job from 
                                $userjobcountres = $ci->userjob_model->countForUserJob($item->id); 
                                $userjobcount = $userjobcountres['userjobcount'];                                    
                                ?>
                                <tr>
                                    <td><a href="<?php echo base_url(); ?>jobs/view/<?php echo $item->id; ?>" ><?php echo substr($item->title, 0, 30); echo (strlen($item->title)>30?'...':''); ?></a></td>
                                    <td><?php echo $item->company; ?></td>
                                    <td><?php echo substr($item->locationName, 0, 20); echo (strlen($item->locationName)>20?'...':''); ?></td>
                                    <td><?php echo substr($item->qualificationName, 0, 10); echo (strlen($item->qualificationName)>10?'...':''); ?></td>
                                    <td><?php echo $item->expFrom.'-'.$item->expTo; ?> Years</td>
                                    <td><?php echo date('d-m-Y',strtotime($item->openTill)); ?></td>
                                    <td><?php echo $item->fjCode; ?></td>
                                    <td><?php echo $item->noOfVacancies; ?></td>
                                    <td><?php  if($item->posted == 2): ?>
                                        <a href="javascript:" class="post-job" id="<?php echo $item->id;?>" target="<?php echo $item->posted;?>">
                                        Post
                                        </a>
                                        <?php else:?>
                                        <a href="javascript:" class="post-job" id="<?php echo $item->id;?>" target="<?php echo $item->posted;?>">
                                        Posted
                                        </a>
                                        <?php endif;?>
                                    </td>
                                    <td><a href="javascript:" class="invite-users" data-toggle="modal" data-target="#inviteUsersForJob" id="<?php echo $item->id; ?>">Invite</a></td>
                                    <td>
                                        <?php 
                                        if(date('Y-m-d', strtotime($item->openTill)) >= date('Y-m-d')) : ?>
                                        Active
                                        <?php else:?>
                                        Inactive
                                        <?php endif;?>
                                    </td>
                                    <td>
                                        <a href="<?php echo base_url(); ?>jobs/edit/<?php echo $item->id; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/edit-pencil.png"></a>
                                        <?php if( $userjobcount > 0 ) { ?>
                                            <a href="javascript:" class="notdelete-job" id="<?php echo $item->id; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/del.png" width="15" height="15"></a>
                                        <?php } else { ?>
                                            <a href="javascript:" class="delete-job" id="<?php echo $item->id; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/del.png" width="15" height="15"></a>
                                        <?php } ?>
                                            <a href="javascript:" class="replciate-job" id="<?php echo $item->id; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/copy.png" width="15" height="15" title="Replicate this job."></a>
                                    </td>
                                </tr>
                                <?php
                                $i++;
                            endforeach;
                        else:
                            echo "<tr><td colspan=\"2\">Jobs not available.</td></tr>";
                        endif;
                        ?>
                    </tbody>
                </table>
            </div>
            <?php if(count($content)>0) { echo $this->pagination->create_links(); } ?>
            <div class="service_content ">
                <?php if ($totalCount > $itemsPerPage): ?>
                   
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>







<!-- PopUp for invite users for a job  -->
<div id="inviteUsersForJob" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <form class="form-horizontal form_save" action="<?php echo base_url(); ?>assessments/addToAssessment" method="post" id="interviewQuestionForm" name="interviewQuestionForm" >
            <div class="modal-content">

                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Invite Users</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger hidden" id="inviteAlert">

                    </div>
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-3">Inivitation By</label>
                        <div class="col-xs-6">
                            <input type="checkbox" name="inviteFrom[]" id="inviteFrom_1" value="1" checked="" />&nbsp;Email&nbsp;&nbsp;
                            <input type="checkbox" name="inviteFrom[]" id="inviteFrom_2" value="2" />&nbsp;SMS
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-3">Upload File</label>
                        <div class="col-xs-8">
                            <input type="file" name="inviteUsers" id="inviteUsers" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-3"></label>
                        <div class="col-xs-4 aligncenter">
                            <?php
                            $fileCsv = base_url() . 'uploads/inviteUserFiles/inviteUsers' . $userId . '.csv';
                            $fileSamleCsv = base_url() . 'uploads/inviteUserFiles/inviteSampleFile.csv';
                            if ($fileEist == 1):
                                ?>
                                <a href="<?php echo $fileCsv; ?>"> Download File</a>
                            <?php else: ?>
                                <a href="<?php echo $fileSamleCsv; ?>"> Download Sample File</a>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-4">Customize SMS message</label>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-3">Message</label>
                        <div class="col-xs-8">
                            <textarea class="form-control" rows="3" id="smsMessage" name="smsMessage" maxlength="150"></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-3">Customize Email</label>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-3">Email Subject</label>
                        <div class="col-xs-8">
                            <input type="text" class="form-control search-critera" name="emailSubject" id="emailSubject" maxlength="50" placeholder="Email Subject" data-validation="required" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-3">Email Message</label>
                        <div class="col-xs-8">
                            <script type="text/javascript" src="//cdn.tinymce.com/4/tinymce.min.js"></script>
                            <script>
                                tinymce.init({
                                    selector: '#emailMessage',
                                    height: 150,
                                    menubar:false,
                                    statusbar: false,
                                    toolbar: 'bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent |',
                                    content_css: [
                                      '//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
                                      '//www.tinymce.com/css/codepen.min.css'
                                    ]
                                });
                            </script>
                            <textarea type="text" class="form-control search-critera" name="emailMessage" id="emailMessage" maxlength="500" placeholder="Email Message"></textarea>
                            <input type="text" data-validation="emailMessage" style="height: 0px; width: 0px; visibility: hidden; " /> 
                        </div>                        
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3"></label>
                        <div class="col-xs-4 aligncenter">
                            <button type="button" class="btn btn-primary upload-file">Upload</button>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">


                    <!--                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
                </div>
            </div>
        </form>

    </div>
</div>



<style>
    .service_content ul.service_content_last_list li{ display:inline-block; font-size:20px; width:38px; height:38px; line-height:38px; border:0px; }
    .service_content ul.service_content_last_list li a{ color:#333333; padding:0px; text-align:center; display:block; border:0px;  }
    .service_content ul.service_content_last_list li a:hover{ color:#fff; border-radius:50px; -moz-border-radius:50px; -webkit-border-radius:50px; background:#0072dc }
    .response_img img{ max-width: 100%; }
    .service_content .service_content_last_list{ text-align:center; padding:10px 0 70px; }
    ul.service_content_last_list  li.pagination-last-list{padding:0 20px 0 0px; cursor:pointer; text-indent:-9999px; margin-left:5px;  }
    .pagi_img{display: -webkit-inline-box;}
    .pagi_img a img{display: -webkit-inline-box;}
    .service_content ul.service_content_last_list li{margin:0px!important;}
    h1{font-size:32px!important;}
    .acu_bt a {
        text-decoration: none;
        color: #fff;
    }
    .sel_opt {
        font-size:15px;
        width:33% !important;
        padding-left: 9px !important;
    }
    #sorting td a {color: #303030!important; }
    #sorting td a:hover {color: #777!important; }

</style>
<!--<script src="<?php echo base_url(); ?>theme/firstJob/js/tablesort.js"></script>-->
<!--<script src="<?php echo base_url(); ?>theme/firstJob/js/jquery.tablesorter.js"></script>-->
<!--<script src="https://code.jquery.com/jquery-1.10.2.js"></script>-->
<script>
    $(function () {

        var settingTableRowColor = function (tableName) {
            $('#' + tableName + ' tbody tr').removeAttr('class');
            $('#' + tableName + ' tbody tr:odd').attr('class', 'tbl_bg');
        }
        settingTableRowColor('sorting');
        $('.sort-header').on('click', function () {
            settingTableRowColor('sorting');
        });
        $('.delete-user').on('click', function () {
            var url = '<?php echo base_url(); ?>';
            var userId = $(this).attr('id');
            var accessToken = $('#accessToken').val();
            bootbox.confirm('Are you sure you want to delete?', function (result) {
                //alert(result);
                if (result) {
                    $.post(url + 'users/delete', {id: userId, accessToken: accessToken}, function (data) {
                        //bootbox.alert(data);
                        location.reload();
                    });
                }
            });

        });
    });
    //new Tablesort(document.getElementById('sorting'));
    //$("#sorting").tablesorter();
</script>

<script>
    $(function () {
        var url = '<?php echo base_url(); ?>';
        window.siteUrl = url;
        window.token = '<?php echo $token; ?>';
    });
    
    $('.delete-job').on('click', function () {
            var userId = $(this).attr('id');
            bootbox.confirm('Are you sure you want to delete?', function (result) {
                
                if (result) {
                    $.post(siteUrl + 'jobs/delete', {id: userId, accessToken: token}, function (data) {
                        
                        //bootbox.alert(data);
                        location.reload();
                    });
                }
            });

        });
</script>


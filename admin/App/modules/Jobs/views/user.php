<div class="panel panel-default user-profile">
    <div class="panel-heading clearfix">
        <h3 class="panel-title pull-left"><?php echo ucfirst($user->user_nicename); ?> Contact Details</h3> 
    </div>
    <div class="list-group">
        <div class="list-group-item">
            <img src="http://firstjob.local//theme/img/admin.jpg" class="img-rounded pull-left" width="50px">
            <label>Name</label>
            <h4 class="list-group-item-heading"><?php echo ucfirst($user->user_name); ?></h4>
        </div>
        <div class="list-group-item">
            <label>User Nicename:</label>
            <h4 class="list-group-item-heading"><?php echo $user->user_nicename; ?></h4>
        </div>
        <div class="list-group-item">
            <label>Gender:</label>
            <h4 class="list-group-item-heading"><?php echo $user->user_gender; ?></h4>
        </div>
        <div class="list-group-item">
            <label>Date of Birth:</label>
            <h4 class="list-group-item-heading"><?php echo $user->user_dob; ?></h4>
        </div>
        <a class="list-group-item" href="mailto:<?php echo $user->user_email; ?>">
            <label>Email</label>
            <h4 class="list-group-item-heading"><?php echo $user->user_email; ?></h4>
        </a>
        <a class="list-group-item" href="tel:<?php echo $user->user_mobile; ?>">
            <label>Phone</label>
            <h4 class="list-group-item-heading"><?php echo $user->user_mobile; ?></h4>
        </a>
        <div class="list-group-item">
            <label>Registration date:</label>
            <h4 class="list-group-item-heading"><?php echo $user->registered_date; ?></h4>
        </div>
    </div>
		
</div>

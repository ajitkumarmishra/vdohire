<?php
$ci =&get_instance();
$ci->load->model('userjob_model');
$ci->load->model('user_model');
$ci->load->model('job_model');
$ci->load->helper('fj');

?>
<div id="page-content-wrapper" style="margin-top: 52px;">
    <div class="container-fluid whitebg">
        <div class="row">
            <div class="col-md-3">
                <h3 style="font-size: 20px !important;margin-top: 10px;">Search Results for "<?php echo $searchText ?>"</h3>
            </div>
            <div class="col-md-9" style="float:left;    margin-left: 12%;   width: 24%;    margin-top: 10px;margin-bottom: 10px;">
				<form action ="<?php echo base_url() ?>jobs/search" name="searchForm" method="post" id="searchForm">
				<input class="form-control" type="" id="searchText" style="float:left;" name="search" value="" placeholder="Search Job, Applicants">
				
				<button onclick="searchResult()" style="    position: absolute;right:5%;margin-top: 3px;float:right;border: none;background: none;padding-top: 6px;"><i class="fa fa-search" aria-hidden="true"></i></button>
				</form>
				<div style="clear:both"></div>
			</div>
            <div style="clear:both"></div>
        </div>
        <div class="row">
            <div class="clearfix"></div>
            <div class="col-md-12 rsp_tbl" style="font-size: 14px;">
                <table class="table sortable" id="sorting">
                    <thead class="tbl_head">
                        <tr>
                            <th><span>Type</span></th>
                            <th><span>Name</span></th>
                            <th>Details</th>
                            <th><span>View</span></th>
                           
                        </tr>
                    </thead>
                    <tbody>
                        <?php
						
                        if(!empty($applicantsSearched)){
							foreach($applicantsSearched as $applicant){ 
								
								$joBid = encryptURLparam($applicant['CurrentjobId'], URLparamKey);
								$useId = encryptURLparam($applicant['CurrentuserId'], URLparamKey);
							?>
								<tr>
									<td>Applicant</td>
									<td><?php echo $applicant['fullname'] ?></td>
									<td>Applied for <?php echo $applicant['title']; ?></td>
									<td><a target="_blank" href="<?php echo base_url()."queue/list?JobId=".$joBid."&userId=".$useId."&query=search" ?>" title="Click here to view applicant's information"><i class="fa fa-eye" aria-hidden="true"></i>&nbsp;&nbsp;View</a></td>
								</tr>
							<?php }
							
						}
						
                        ?>
						<?php
						
                        if(!empty($Searchedjobs)){
                        	$userjobpendingcount = 0;
                        	$userjobNewCount = 0 ;
                        	$userjobshortlistedcount = 0;
                        	$userjobrejectedcount = 0;
							foreach($Searchedjobs as $job){
								$userjobpendingcountres     = $ci->userjob_model->countForUserJobs($job['id'], 1);
								$userjobpendingcount        = $userjobpendingcount + $userjobpendingcountres['appliedCount'];

								$userjobNewcountres     	= $ci->userjob_model->countForUserJobsNew($job['id'], 1);   
								$userjobNewCount    		= $userjobNewCount + $userjobNewcountres['appliedCount'];   
												  
								$userjobshortlistedcountres = $ci->userjob_model->countForUserJobs($job['id'], 2);   
								$userjobshortlistedcount    = $userjobshortlistedcount + $userjobshortlistedcountres['appliedCount'];                    
								
								$userjobrejectedcountres    = $ci->userjob_model->countForUserJobs($job['id'], 3);   
								$userjobrejectedcount       = $userjobrejectedcount + $userjobrejectedcountres['appliedCount'];
								$joBid = encryptURLparam($job['id'], URLparamKey);
							?>
								<tr>
									<td>Jobs</td>
									<td><?php echo $job['title'] ?></td>
									<td>
									Status: Active
									Applications:Received <?php echo $userjobpendingcount+$userjobNewCount+$userjobshortlistedcount+$userjobrejectedcount ?>
												 Unprocessed <?php echo $userjobpendingcount+$userjobNewCount ?>
												 Shortlisted <?php echo $userjobshortlistedcount ?>
												 Rejected <?php echo $userjobrejectedcount ?>
									</td>
									<td><a target="_blank"  href="<?php echo base_url()."jobs/edit/".$job['id'] ?>" title="Click here to edit this job"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;&nbsp;Edit</a>
									<a target="_blank"  href="<?php echo base_url()."queue/list?JobId=".$joBid ?>" title="Click here to view applicants for this job"><i class="fa fa-eye" aria-hidden="true"></i>&nbsp;&nbsp;View</a>
									</td>
								</tr>
							<?php }
							
						}
						if(empty($Searchedjobs) && empty($applicantsSearched)){?>
							<tr>
								<td>No Search Result Found</td>
							</tr>
						<?php }
						
                        ?>
                    </tbody>
                </table>

                <div class="row">
                    
                </div>
        
            </div>
            
</div>







    </div>
</div>
<script>
	function searchResult(){
		$("#searchForm").submit();
	}
	
</script>
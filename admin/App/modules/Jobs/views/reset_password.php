<div class="row">
    <div class="span6 offset3">
        <h1>Reset Password</h1>

        <?php if (@$error): ?>
            <div class="alert">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <?php echo $error; ?>
            </div>
        <?php endif; ?>
        <?php if ($this->session->flashdata('message')) { ?>
            <div class="alert alert-success">
                <strong>Success!</strong> <?php echo $this->session->flashdata('message'); ?>
            </div>
        <?php } ?>

        <div class="well">
            <form class="form-horizontal" method="post" action="">
                <div class="control-group">
                    <label class="control-label" for="user_password">New Pasword</label>
                    <div class="controls">
                        <input type="password" id="user_password" placeholder="New Password" name="user_password" value="<?php echo set_value('user_password');?>">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="cnf_password">Confirm New Pasword</label>
                    <div class="controls">
                        <input type="password" id="cnf_password" placeholder="Confirm New Password" name="cnf_password" value="<?php echo set_value('cnf_password');?>">
                    </div>
                </div>

                <div class="control-group">
                    <div class="controls">
                        <button type="submit" class="btn">Reset</button>
                    </div>
                </div>
            </form>
        </div>

    </div>
</div>

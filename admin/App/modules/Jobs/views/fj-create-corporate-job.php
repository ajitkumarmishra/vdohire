<style type="text/css">
    .bootstrap-dialog .bootstrap-dialog-title {
        display: block !important;
    }

    .bootstrap-dialog .bootstrap-dialog-message {
        font-size: 17px !important;
        text-align: center !important;
        padding-top: 30px;
    }

    .bootstrap-dialog-body {
        height:90px !important;
    }

    .bootstrap-dialog .modal-dialog {
        margin: 330px auto auto auto !important;
    }

    .bootstrap-dialog-footer-buttons .btn-default {
        background: #054f72 none repeat scroll 0 0 !important;
        color: white !important;
    }
    .basic-form-level {
        padding-left:0px !important;
    }

    .form-horizontal .form-group {
        margin-right: 30px !important;
        margin-left: 30px !important;
    }

    .job-tab li{
        font-size: 16px;
        background-color: #eee !important;
    }

    .nav>li>a:focus, .nav>li>a:hover {
        background:#959595 !important;
    }
    .job-tab li.active{
        background:white;
        color:black;
    }

    .job-tab li a{
        color:black;
    }


    .job-tab li a:hover{
        color:white;
        border-radius: 0px;
    }


    .job-tab li:active a{
        color:black;
    }

    .chosen-container-single .chosen-single {
        height: 35px !important;
        border-radius: 0 4px 4px 0 !important;
        background: white !important;
    }

    .chosen-container-single .chosen-single span {
        padding-top: 4px !important;
    }
    .chosen-container-multi .chosen-choices {
        border-radius: 0 4px 4px 0 !important;
    }

    .form-control {
        border-radius: 0 4px 4px 0 !important;
    }

    select {
        border-radius: 0 4px 4px 0 !important;
    }

    .input-group-addon .fa{
        font-size: 15px !important;
    }

    #mceu_18{display: none;}

</style>

<?php
    $userId             = getUserId();
    $userDetForPage     = getUserById($userId);
    $userRoleForPage    = $userDetForPage['role'];
?>

<div id="page-content-wrapper" style="margin-top: 52px;">
    <?php if (isset($message)): ?>
        <div class="alert alert-success">
            <?php echo $message; ?>
        </div>
    <?php endif; ?>
    <?php if (isset($error)): ?>
        <div class="alert alert-danger">
            <?php foreach ($error as $item): ?>
                <?php echo $item; ?>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
    <div class="container-fluid whitebg">
        <div class="row">

            <div class="col-md-12 padd_tp_bt_20">
                <h5 style="border-bottom: 30px;margin-bottom: 30px;margin-top: -6px;"> <a href="<?php echo base_url().'jobs/list' ?>">Jobs</a> -> Add Job</h5>
                <h4 style="border-bottom: 23px; font-size: 20px;"> Add Job</h4>

                <div class="row">
                    <div class="pull-left">
                    <ul class="nav nav-tabs job-tab">
                        <li id="basicformid" class="active"><a id="basicjobcontentId" data-toggle="tab" href="#basicjobcontent">Main</a></li>
                        <li id="interviewformid" ><a data-toggle="tab" href="#interviewjobcontent">Interview Setup</a></li>
                        <li id="panellevelformid"><a data-toggle="tab" href="#recruitersjobcontent">Recruiters</a></li>
                    </ul>
                    </div>
                    <div class="pull-right">
                        <div class="" style="padding-right:46px;">
                            <input type="hidden" name="accessToken" value="<?php echo $token; ?>"/>
                            <button type="button" style=" background: #054f72 none repeat scroll 0 0; color: white;" onclick="saveBasicData()" class="Save_frm">Save</button>
                        </div>
                    </div>
                </div>
                <div style="clear:both;"></div>
                

                <div class="tab-content" style="border: 1px solid #ddd">
                    <form class="form-horizontal form_save" action="" method="post" enctype="multipart/form-data" id="jobForm">
                        <div id="basicjobcontent" class="tab-pane fade in active">
                            <h3 style="padding-left: 12px; font-size: 15px;">Basic</h3>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="inputPassword" class="basic-form-level required-field">Title Of Job</label>
                                        <div class="input-group">
                                            <div class="input-group-addon" style="padding-left: 13px; padding-right: 13px;">
                                                <i class="fa fa-tag"></i> 
                                            </div>
                                            <input type="text" class="form-control" placeholder="Title Of Job" value="<?php
                                            if (isset($job['title'])): echo $job['title'];
                                            endif;
                                            ?>" name="title" id="titleOfJob">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPassword" class="basic-form-level required-field">Location</label>
                                        <div class="input-group">
                                            <div class="input-group-addon" style="padding-left: 14px; padding-right: 14px;">
                                                <i class="fa fa-location-arrow"></i> 
                                            </div>
                                            <select data-placeholder="Location" style="width: 350px; display: none;" multiple="" class="chosen-select location" tabindex="-1" name="cityState[]" id="cityState" data-validation="alphanumeric required">
                                                <option value=""></option>
                                                <?php foreach ($city as $item) : ?>
                                                    <option value="<?php echo $item['id']; ?>" <?php
                                                            if (isset($job['cityState']) && in_array($item['id'], $job['cityState'])): echo "selected";
                                                            endif;
                                                            ?>><?php echo $item['city'] . ', ' . $item['state']; ?></option>
                                                        <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPassword" class="basic-form-level">Salary (P.A)</label>
                                        <div class="row">
                                            <div class="col-xs-6" style="padding-left: 0px;">
                                                <div class="input-group">
                                                    <div class="input-group-addon" style="padding-left: 17px; padding-right: 17px;">
                                                        <i class="fa fa-inr"></i> 
                                                    </div>
                                                    <select class="selectpicker salFrom" name="salaryFrom">
                                                        <option value="">From</option>
                                                            <?php for ($i = 0, $j=0; $i <= 10000000, $j<=100; $i+=100000, $j++): ?>
                                                            <option value="<?php echo $i; ?>" <?php if (isset($job['salaryFrom']) && $job['salaryFrom'] != NULL && $i == $job['salaryFrom']): echo "selected";
                                                                endif;
                                                                ?>><?php echo $j; ?> Lakh</option>
                                                            <?php endfor; ?>
                                                    </select>
                                                </div>
                                            </div>


                                            <div class="col-xs-6" style="padding-left: 0px; padding-right: 0px;">
                                                <div class="input-group">
                                                    <div class="input-group-addon" style="padding-left: 17px; padding-right: 17px;">
                                                        <i class="fa fa-inr"></i> 
                                                    </div>
                                                    <select class="selectpicker salTo" name="salaryTo">
                                                        <option value="">To</option>
                                                                <?php for ($i = 0, $j=0; $i <= 10000000, $j<=100; $i+=100000, $j++): ?>
                                                            <option value="<?php echo $i; ?>" <?php if (isset($job['salaryTo']) && $job['salaryTo'] != NULL && $i == $job['salaryTo']): echo "selected";
                                                                endif;
                                                                ?>><?php echo $j; ?> Lakh</option>
                                                            <?php endfor; ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="inputEmail" class="basic-form-level">Job ID</label>
                                        <div class="input-group">
                                            <div class="input-group-addon" style="padding-left: 16px; padding-right: 19px;">
                                                <i class="fa fa-info"></i> 
                                            </div>
                                            <input type="text" class="form-control" placeholder="Job ID" value="<?php
                                            if (isset($job['jobId'])): echo $job['jobId'];
                                            endif;
                                            ?>" name="jobId">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPassword" class="basic-form-level required-field">Number Of Vacancies</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-users"></i> 
                                            </div>
                                            <input type="text" class="form-control" placeholder="Number Of Vacancies" value="<?php
                                                   if (isset($job['vacancy'])): echo $job['vacancy'];
                                                   endif;
                                                   ?>" name="vacancy" id="numberofVacancies">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPassword" class="basic-form-level">Open Till</label>
                                        <div class="input-group">
                                            <div class="input-group-addon" style="padding-left: 13px; padding-right: 14px;">
                                                <i class="fa fa-calendar"></i> 
                                            </div>
                                            <input type="text" class="form-control" placeholder="Open Till" id="datepicker-addJob" value="<?php
                                                if (isset($job['openTill'])): echo $job['openTill'];
                                                endif;
                                                ?>" name="openTill">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h3 style="padding-left: 12px; font-size: 15px;">Job Requirements</h3>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="inputPassword" class="basic-form-level required-field">Job Description</label>
                                        <div class="input-group">
                                            <div class="input-group-addon" style="padding-left: 13px; padding-right: 13px;">
                                                <i class="fa fa-clipboard"></i> 
                                            </div>
                                            <script type="text/javascript" src="//cdn.tinymce.com/4/tinymce.min.js"></script>
                                            <script>
                                                tinymce.init({
                                                    selector: '#jobDescription',
                                                    height: 150,
                                                    menubar:false,
                                                    statusbar: true,
                                                    toolbar: 'bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent |',
                                                    content_css: [
                                                      '//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
                                                      '//www.tinymce.com/css/codepen.min.css'
                                                    ]
                                                });
                                            </script>
                                            <textarea class="form-control" id="jobDescription" rows="9" style="width:100%; height: 185px;" placeholder="Job Description" name="description"><?php if (isset($job['description'])): echo $job['description'];
                                            endif;
                                            ?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group"">
                                        <label for="inputPassword" class="basic-form-level">Upload Video</label>
                                        <div class="input-group">
                                            <div class="input-group-addon" style="padding-left: 14px; padding-right: 14px;">
                                                <i class="fa fa-file-video-o" aria-hidden="true"></i>
                                            </div>
                                            <input class="form-control" type="file" name="jd" id="jd" value="<?php if(!empty($files)) { print_r($files); } else echo '' ?>"/>
                                        </div>
                                        <div id="fileHidden">
                                            <?php if (isset($files['jd']) && $files['jd'] != NULL): ?>
                                                <input type="hidden" name="jd" id="jdHidden" value="<?php if(!empty($files)) { print_r($files); } ?>"/>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="inputPassword" class="basic-form-level">Qualification</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-university"></i> 
                                            </div>
                                            <select data-placeholder="Qualification" style="width: 350px; display: none;" multiple="" class="chosen-select qualification" tabindex="-1" name="qualification[]" id="qualification">
                                                <option value=""></option>
                                                    <?php foreach ($course as $item) : ?>
                                                        <option value="<?php echo $item['id']; ?>" <?php
                                                        if (isset($job['qualification']) && in_array($item['id'], $job['qualification'])): echo "selected";
                                                        endif;
                                                        ?>><?php echo $item['name']; ?></option>
                                                    <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPassword" class="basic-form-level">Experience</label>
                                        <div class="row">
                                            <div class="col-xs-6" style="padding-left: 0px;">
                                                <div class="input-group">
                                                    <div class="input-group-addon" style="padding-left: 14px; padding-right: 14px;">
                                                        <i class=" fa fa-history"></i> 
                                                    </div>
                                                    <select class="selectpicker expFrom" name="expFrom"><option value="">From</option>
                                                        <?php for ($i = 0; $i <= 50; $i+=1): ?>
                                                            <option value="<?php echo $i; ?>" <?php if (isset($job['expFrom']) && $job['expFrom'] != NULL && $i == $job['expFrom']): echo "selected";
                                                        endif;
                                                        ?>><?php echo $i; ?> Years</option>
                                                                <?php endfor; ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-xs-6" style="padding-left: 0px; padding-right: 0px;">
                                                <div class="input-group">
                                                    <div class="input-group-addon" style="padding-left: 14px; padding-right: 14px;">
                                                        <i class=" fa fa-history"></i> 
                                                    </div>
                                                    <select class="selectpicker expTo" name="expTo"><option value="">To</option>
                                                    <?php for ($i = 0; $i <= 50; $i+=1): ?>
                                                        <option value="<?php echo $i; ?>" <?php if (isset($job['expTo']) && $job['expTo'] != NULL && $i == $job['expTo']): echo "selected";
                                                        endif;
                                                        ?>><?php echo $i; ?> Years</option>
                                                    <?php endfor; ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPassword" class="basic-form-level">Age</label>
                                        <div class="row">
                                            <div class="col-xs-6" style="padding-left: 0px;">
                                                <div class="input-group">
                                                    <div class="input-group-addon" style="padding-left: 14px; padding-right: 14px;">
                                                        <i class="fa fa-clock-o"></i> 
                                                    </div>                         
                                                    <select class="selectpicker ageFrom" name="ageFrom" >
                                                        <option value="">From</option>
                                                                <?php for ($i = 18; $i <= 99; $i+=1): ?>
                                                            <option value="<?php echo $i; ?>" <?php if (isset($job['ageFrom']) && $job['ageFrom'] != NULL && $i == $job['ageFrom']): echo "selected";
                                                                endif;
                                                                ?>><?php echo $i; ?></option>
                                                        <?php endfor; ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-xs-6" style="padding-left: 0px;padding-right: 0px;">
                                                <div class="input-group">
                                                    <div class="input-group-addon" style="padding-left: 14px; padding-right: 14px;">
                                                        <i class="fa fa-clock-o"></i> 
                                                    </div>       
                                                    <select class="selectpicker ageTo" name="ageTo" >
                                                        <option value="">To</option>
                                                                <?php for ($i = 18; $i <= 99; $i+=1): ?>
                                                            <option value="<?php echo $i; ?>" <?php if (isset($job['ageTo']) && $job['ageTo'] != NULL && $i == $job['ageTo']): echo "selected";
                                                                endif;
                                                                ?>><?php echo $i; ?></option>
                                                            <?php endfor; ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail" class="basic-form-level">Availability Notice periods (In Days)</label>
                                        <div class="input-group">
                                            <div class="input-group-addon" style="padding-left: 13px; padding-right: 13px;">
                                                <i class="fa fa-calendar-check-o"></i> 
                                            </div>
                                            <input type="text" class="form-control noticeperiod" placeholder="Availability Notice periods (In Days)" value="<?php
                                            if (isset($job['noticePeriod'])): echo $job['noticePeriod'];
                                            endif;
                                            ?>" name="noticePeriod" id="noticePeriod" maxlength="3">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="basic-form-level" style="float: right; color: red;">All * marked fields are mandatory</label>
                            </div>

                            <div style="display: none;">
                                <input type="hidden" name="accessToken" value="<?php echo $token; ?>"/>
                                <button id="saveBasicInfoButton" type="submit" class="Save_frm">SAVE</button>
                            </div>
                        </div>

                        <div id="interviewjobcontent" class="tab-pane fade" style="display: none; padding-top:30px;">
                            <div class="row">
                                <div class="col-md-6 col-md-offset-6" style="padding-right: 45px; padding-bottom: 39px;">
									<?php if(in_array('5', $userPermissionArr)|| ($userRoleForMenu == 2)){ ?>
                                    <button type="button" class="btn btn-default pull-right" data-toggle="modal" data-target="#manageAssessmentModal" id="resetAllAssessmetnModalContent" style="background: #054f72 none repeat scroll 0 0; color: white;">Manage Assessment Sets</button>
									<?php } ?>
									<?php if(in_array('4', $userPermissionArr)|| ($userRoleForMenu == 2)){ ?>
                                    <button type="button" class="btn btn-default pull-right" data-toggle="modal" data-target="#manageInterviewModal" id="resetAllInterviewModalContent" style="margin-right: 30px; background: #054f72 none repeat scroll 0 0; color: white;">Manage Interview Sets</button>
									<?php } ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-2 required-field">Attach Inverview</label>
                                <div class="col-xs-10">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-book" aria-hidden="true"></i> 
                                        </div>
                                        <select data-placeholder="Select Interview" style="width: 350px; display: none;"  class="chosen-select examinterview"  name="interview" id="interviewAttach">
                                            
                                                <option value="0">Select Interview</option>
                                            <?php foreach ($interview as $item) : ?>
                                                <option value="<?php echo $item['id']; ?>" <?php if (isset($job['interview']) && $item['id'] == $job['interview']): echo "selected";
                                            endif;
                                            ?>><?php echo $item['name']; ?></option>
                                                    <?php endforeach; ?>
                                        </select>
                                    </div> 
                                </div>
                            </div>

                            <div style="margin-left: 96px; margin-right: 96px; margin-bottom: 15px; max-height: 200px; overflow-y: auto;">
                                <table class="row table" id="interviewSetTableContent" style="display: none;">
                                    <caption style="font-size: 16px; color: #333;">Selected Interview Set Questions : </caption>
                                    <thead class="tbl_head">
                                        <tr>
                                            <th><span>Question</span></th>
                                            <th><span>Question Type</span></th>
                                            <th><span>Question Video</span></th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>

                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-2">Attach Assessment</label>
                                <div class="col-xs-10">
                                    <div class="input-group">
                                        <div class="input-group-addon" style="padding-left: 14px; padding-right: 15px;">
                                            <i class="fa fa-houzz"></i> 
                                        </div>
                                        <select data-placeholder="Select Assessment" style="width: 350px; display: none;"  class="chosen-select examassessment"  name="assessment" id="assessmentAttach">
                                            <option value="0">Select Assessment</option>
                                            <?php foreach ($assessment as $item) : ?>                                                    
                                                <option value="<?php echo $item['id']; ?>" <?php if (isset($job['assessment']) && $item['id'] == $job['assessment']): echo "selected";
                                                endif;
                                                ?>><?php echo $item['name']; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div style="margin-left: 96px; margin-right: 96px; margin-bottom: 15px; max-height: 200px; overflow-y: auto;">
                                <table class="row table" id="assessmentSetTableContent" style="display: none;">
                                    <caption style="font-size: 16px; color: #333;">Selected Assessment Set Questions : </caption>
                                    <thead class="tbl_head">
                                        <tr>
                                            <th><span>Question</span></th>
                                            <th><span>Question Type</span></th>
                                            <th><span>Question Video</span></th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>

                        </div>
                        <div id="recruitersjobcontent" class="tab-pane fade" style="display: none; padding-top:30px;">
                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-2 required-field">Level 1 Recruiter</label>
                                <div class="col-xs-10">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-user-plus" aria-hidden="true"></i>
                                        </div>
                                        <select id="level1Test" data-placeholder="Level 1" data-validation="level1" style="width: 350px; display: none;" multiple="" class="chosen-select level1" tabindex="-1" name="level1[]">
                                            <option value=""></option>
                                            <?php foreach ($subuser as $item) : ?>
                                                <option value="<?php echo $item['id']; ?>" <?php
                                            if (isset($job['level1']) && in_array($item['id'], $job['level1'])): echo "selected";
                                            endif;
                                                ?>><?php echo $item['fullname']; ?></option>
                                                    <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-2">Level 2 Recruiter</label>
                                <div class="col-xs-10">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-user-plus" aria-hidden="true"></i>
                                        </div>
                                        <select id="level2Test" data-placeholder="Level 2" style="width: 350px; display: none;" multiple="" class="chosen-select level2" tabindex="-1" name="level2[]">
                                            <option value=""></option>
                                        <?php foreach ($subuser as $item) : ?>
                                            <option value="<?php echo $item['id']; ?>" <?php
                                            if (isset($job['level2']) && in_array($item['id'], $job['level2'])): echo "selected";
                                            endif;
                                            ?>><?php echo $item['fullname']; ?></option>
                                        <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div style="margin-left: 57px; margin-right: 43px; margin-top: 40px; overflow-x: auto;">
                                <table class="row table">
                                    <caption style="font-size: 16px; color: #333;">Recruiters List : </caption>
                                    <thead class="tbl_head">
                                        <tr>
                                            <th>Recruiters</th>
                                            <?php
                                                $allJobArray = array();
                                                foreach($allRecruitersList as $recruiterJob){
                                                    if($recruiterJob['l1recruiters'] == "" && $recruiterJob['l2recruitersId'] == ""){
                                                        
                                                    } else {
                                                        echo "<th>".$recruiterJob['title']."</th>";
                                                        $allJobArray[] = $recruiterJob['id'];
                                                    }
                                                }
                                            ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            foreach($allRecruitersList as $pendingApplications) {
                                                
                                                if($pendingApplications['l1recruitersId'] != "" ) {
                                                    $L1RecruitersNameArray = explode(',', $pendingApplications['l1recruiters']);
                                                    $L1RecruitersIdArray = explode(',', $pendingApplications['l1recruitersId']);

                                                    foreach($L1RecruitersIdArray as $keyL1Id => $L1RecruitersId) {
                                                        $recruiters[$L1RecruitersId]['name'] = $L1RecruitersNameArray[$keyL1Id];
                                                        
                                                        $recruiters[$L1RecruitersId]['jobs'][$pendingApplications['id']] = array(
                                                                        'jobId' => $pendingApplications['id'],
                                                                        'jobTitle' => $pendingApplications['title'],
                                                                        'totalReceived' => $pendingApplications['totalApplicationReceivedAtL1'],
                                                                        'totalShortlisted' => $pendingApplications['totalShortlistedApplicationAtL1'],
                                                                        'totalPending' => $pendingApplications['totalPendingApplicationAtL1'],
                                                                        'totalRejected' => $pendingApplications['totalRejectedApplicationAtL1'],
                                                                        'RecruiterType' => "1"
                                                        );
                                                    }
                                                }
                                                
                                                if($pendingApplications['l2recruitersId'] != "" ){

                                                    $L2RecruitersNameArray = explode(',', $pendingApplications['l2recruiters']);
                                                    $L2RecruitersIdArray = explode(',', $pendingApplications['l2recruitersId']);

                                                    foreach($L2RecruitersIdArray as $keyL2Id => $L2RecruitersId) {
                                                        $recruiters[$L2RecruitersId]['name'] = $L2RecruitersNameArray[$keyL2Id];
                                                        
                                                        $recruiters[$L2RecruitersId]['jobs'][$pendingApplications['id']] = array(
                                                                        
                                                                        'jobId' => $pendingApplications['id'],
                                                                        'jobTitle' => $pendingApplications['title'],
                                                                        'totalReceived' => $pendingApplications['totalApplicationReceivedAtL2'],
                                                                        'totalShortlisted' => $pendingApplications['totalShortlistedApplicationAtL2'],
                                                                        'totalPending' => $pendingApplications['totalPendingApplicationAtL2'],
                                                                        'totalRejected' => $pendingApplications['totalRejectedApplicationAtL2'],
                                                                        'RecruiterType' => "2"
                                                        );
                                                    }
                                                }
                                            } ?>
                                            
                                            <?php 
                                            $NewrecruiterArray = array();
                                            foreach($recruiters as $key => $rec){
                                                $NewrecruiterArray[$key]['name'] = $rec['name'];
                                                foreach($allJobArray as $job){
                                                    if(array_key_exists($job,$rec['jobs'])){
                                                        $NewrecruiterArray[$key]['jobs'][$job] = $rec['jobs'][$job];
                                                    }else{
                                                        $NewrecruiterArray[$key]['jobs'][$job] = array();
                                                    }
                                                }
                                            }

                                            foreach($NewrecruiterArray as $recruiter) {
                                                    $L1RecruiterJobCount = 0;
                                                    $L2RecruiterJobCount = 0;
                                                    foreach($recruiter['jobs'] as $jobCount) { 
                                                        if(!empty($jobCount)){
                                                            if($jobCount['RecruiterType'] == 1) {
                                                                $L1RecruiterJobCount = $L1RecruiterJobCount + 1;
                                                            } else {
                                                                $L2RecruiterJobCount = $L2RecruiterJobCount + 1;
                                                            }
                                                        }
                                                    }

                                                    if(!empty($L1RecruiterJobCount) && !empty($L2RecruiterJobCount)) {
                                                        $RecruiterJobCountText = '(L1-'.$L1RecruiterJobCount.', L2-'.$L2RecruiterJobCount.')';
                                                    } else if(!empty($L1RecruiterJobCount) && empty($L2RecruiterJobCount)) {
                                                        $RecruiterJobCountText = '(L1-'.$L1RecruiterJobCount.')';
                                                    } else if(empty($L1RecruiterJobCount) && !empty($L2RecruiterJobCount)) {
                                                        $RecruiterJobCountText = '(L2-'.$L2RecruiterJobCount.')';
                                                    } else {
                                                        $RecruiterJobCountText = '';
                                                    }
                                                ?>
                                                <tr><td><b><?php echo $recruiter['name'].$RecruiterJobCountText  ?></b></td>
                                                    <?php
                                                        $initialPos = 0;
                                                        foreach($recruiter['jobs'] as $job){ 
                                                            if(empty($job)){
                                                                echo "<td></td>";
                                                            } else { ?>
                                                                <td><?php echo "L".$job['RecruiterType'] ?></td>
                                                    <?php } } ?>
                                                </tr>
                                                <?php 
                                            }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Manage Assesment Modal-->
<div id="manageAssessmentModal" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <div>
                    <div class="pull-left">
                        <h4 class="modal-title" id="manageAssessmentSetsTitle" style="cursor: pointer;">Manage Assessmet Sets</h4>
                    </div>
                    <div class="pull-left breadcrumbText">
                        <h4 class="modal-title" id="breadcrumbText" style="cursor: pointer;">&nbsp; </h4>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="modal-body" style="height: 600px; overflow-y: scroll;">
                <div id="manageInterviewSetTables">
                    <div class="row">
                        <div class="col-md-7 col-md-offset-5" style="margin-right: 0px; padding-bottom: 6px; padding-right: 0px;">
                            <button type="button" class="btn btn-primary pull-right" id="myQuestionBankButton">My Question Banks</button>
                            <button type="button" class="btn btn-info pull-right" id="fjQuestionBankButton" style="margin-right: 30px;">VH Question Banks</button>
                            <button id="addAssessmentSetButton" type="button" class="btn btn-default pull-right" style="background: #054f72 none repeat scroll 0 0; color: white; margin-right: 30px;">Add Assessment Set</button>
                        </div>
                    </div>

                    <table class="table assessmentSetTabel" id="sorting">
                        <thead class="tbl_head">
                            <tr>
                                <th><span>Title</span></th>
                                <th><span>Duration</span></th>
                                <th><span>No. of Questions</span></th>
                                <?php if($userRoleForPage==1): ?>
                                <th><span>Practice Set</span></th>
                                <?php endif; ?>
                                <th><span>Action</span></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 0;
                            foreach ($jobAssessments['contentAssessmentSet'] as $itemAssessment) {
                                // ci instance
                                $ci = &get_instance();
                                //job user assessment model
                                $ci->load->model('userassessment_model');
                                
                                // function to get users assessment available or not
                                $userassessmentcount = $ci->userassessment_model->countForUserAssessment($itemAssessment->id); 
                                $assessmentcount = $userassessmentcount['assessmentcount'];
                                //print_r($assessmentcount);exit;
                                
                                ?>
                                <tr id="tr_<?php echo $itemAssessment->id; ?>" <?php if ($i % 2 != 0) echo 'class="tbl_bg"'; ?>>
                                    <td><a href="<?php echo base_url(); ?>assessments/view/<?php echo $itemAssessment->id; ?>"><?php echo $itemAssessment->name; ?></a></td>
                                    <td><?php echo round($itemAssessment->duration/60, 2); ?> Minutes</td>
                                    <td><?php
                                        $countArr = getNoOfQuestionsPerAssessment($itemAssessment->id);
                                        $countArrRandom = getNoOfRandomQuestions($itemAssessment->id);
                                        echo $countArr['count']+$countArrRandom['count'];
                                        ?></td>
                                    <?php if($userRoleForPage==1): ?>
                                    <td><input type="checkbox" name="asspractice_set_<?php echo $itemAssessment->id ?>" id="asspractice_set_<?php echo $itemAssessment->id ?>" value="<?php echo $itemAssessment->id ?>" onclick="checkedForPractice(this);" <?php $itemAssessment->practiceSetStatus==1? "checked": "" ?> /></td>
                                    <?php endif; ?>
                                    <td>
                                        <?php
                                        $checkAssessment = assessmentExists($itemAssessment->id);
                                        //echo $checkAssessment;
                                        ?>
                                        <a href="javascript:" onclick="assessmentsetEditFunction(<?php echo $itemAssessment->id; ?>)" class="assessmentsetEdit" id="assessmentsetEdit-<?php echo $itemAssessment->id; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/edit.png" class="padd_rgt"></a>
                                        <?php if( $assessmentcount > 0 || $checkAssessment>0 ) { ?>
                                            <a href="javascript:" class="not-delete-assessment-set" id="<?php echo $itemAssessment->id; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/del.png"></a>
                                        <?php } else {
                                            ?>
                                            <a href="javascript:" class="delete-assessment-set" id="<?php echo $itemAssessment->id; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/del.png"></a>
                                        <?php } 
                                        ?>
                                    </td>
                                </tr>
                                <?php
                                $i++;
                            };
                            ?>
                        </tbody>
                    </table>
                </div>

                <div id="manageMyQuestionDiv">
                    <div class="row">
                        <div class="col-md-6 pull-left" style="padding-left: 0px;">
                            <button type="button" class="btn btn-default pull-left assessmentBackButtonL1">Back</button>
                        </div>

                        <div class="col-md-6" style="margin-right: 0px; padding-bottom: 6px; padding-right: 0px;">
                            <button id="addQuestionBankButton" type="button" class="btn btn-default pull-right" style="background: #054f72 none repeat scroll 0 0; color: white;">Add Question Bank</button>
                        </div>
                    </div>

                    <?php $i = 0;
                        foreach ($jobAssessments['contentMyQuestionBank'] as $item):
                            ?>
                            <div class=" rsp_tbl">
                                <table class="table top-head-table question-bank-table">
                                    <?php if ($i == 0): ?>
                                        <tbody><tr class="table_brown_head">
                                                <td class="active">Question Bank</td>
                                                <td class="active">Type</td>
                                                <td class="active">No.of Questions</td>
                                                <td class="active"></td>
                                            </tr>  
                                        </tbody>
                                    <?php endif; ?>
                                    <tbody>
                                        <tr class="tbl_bg_light" id="trQuestion_<?php echo $item['id'];?>">
                                            <td><?php echo $item['name']; ?></td>
                                            <td><?php echo getInterviewSetType($item['type']); ?>
                                                <?php if( $item['type'] != 3 ) { ?>
                                                    <div id ="questionMyVideo<?php echo $item['type']; ?>"></div>
                                                <?php } ?>
                                            </td>
                                            <td><?php 
                                            $q = getQuestionsByQuestionBank($item['id']);
                                            echo $q['count']; ?></td>
                                            <td>
                                                <?php
                                                $checkAssessmentBank = assessmentBankExists($item['id']);
                                                //print_r(assessmentBankExists($item['id']));
                                                ?>
                                                <div class="pull-right">
                                                    <a onclick="editAssessmentQuestionBank(<?php echo $item['id']; ?>)" class="editAssessmentQuestionBank" id="editAssessmentQuestionBank-<?php echo $item['id']; ?>" title="Edit this Question Bank" href="javascript:"><img src="<?php echo base_url(); ?>/theme/firstJob/image/edit.png" class="padd_rgt"></a>
                                                    <?php if( $checkAssessmentBank>0 ) { ?>
                                                    <img src="<?php echo base_url(); ?>/theme/firstJob/image/del.png" class="padd_rgt" title="Sorry, this Question Bank cannot be deleted as it is associated with some job.">
                                                    <?php } else { ?>
                                                    <a title="Delte this Question Bank" href="javascript:" class="delete-question-bank" data-questionbankid="<?php echo $item['id']; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/del.png" class="padd_rgt"></a>
                                                    <?php } ?>
                                                    <a onclick="addAssessmentQuestionButton(<?php echo $item['id']; ?>, <?php echo $item['type']; ?>, '<?php echo $item['name']; ?>')" class="addAssessmentQuestionButton" data-questionbankid="<?php echo $item['id']; ?>" data-questionbanktype="<?php echo $item['type']; ?>" data-questionbanktitle="<?php echo $item['name']; ?>" title="Add Question to this Question Bank" href="javascript:"><img src="<?php echo base_url(); ?>theme/firstJob/image/add_black.png" class="padd_rgt collapsed"></a>

                                                    <a title="Show Questions for this Quesiton Bank" href="#"><img src="<?php echo base_url(); ?>/theme/firstJob/image/arrow_rot.png" class="padd_rgt collapsed" href="#demo<?php echo $item['id']; ?>" data-toggle="collapse" aria-expanded="false"></a>
                                                </div>  
                                            </td>
                                        </tr>  
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-12 rsp_tbl collapse" id="demo<?php echo $item['id']; ?>" aria-expanded="false" style="height: 0px;"> 
                                <table class="table padding_bt_5">
                                    <tbody>
                                        <?php
                                        $q = 0;
                                        $arr = getQuestionOfQuestionBank($item['id']);
                                        if (count($arr) > 0):
                                            foreach ($arr as $itemQuestion):
                                                ?>
                                                <tr id="questionbankquestiontr_<?php echo $itemQuestion['id'];?>">
                                                    <td><?php echo $itemQuestion['title']; ?></td>
                                                    <td style="width:31%;">
                                                        <div class="pull-left">
                                                            <a onclick="assessmentQuestionBankQuestionRow(<?php echo $itemQuestion['id']; ?>, <?php echo $itemQuestion['type']; ?>, '<?php echo $item['name']; ?>', <?php echo $item['id']; ?>)"  class="assessmentQuestionBankQuestionRow" data-questionbanktitle="<?php echo $item['name']; ?>" data-questiontype="<?php echo $itemQuestion['type']; ?>" data-questionBankId = "<?php echo $item['id']; ?>" data-questionid="<?php echo $itemQuestion['id']; ?>" title="Edit this question" href="javascript:"><img src="<?php echo base_url(); ?>/theme/firstJob/image/edit.png" class="padd_rgt"></a>
                                                            <a title="Delte this quetion" href="javascript:" class="delete-question-assessment" data-questionid="<?php echo $itemQuestion['id']; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/del.png" class="padd_rgt"></a>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php
                                                $q++;
                                            endforeach;
                                            else: ?>
                                                <tr class="noquestionofquestionbank">
                                                    <td></td>
                                                    <td>
                                                        <div class="pull-right">
                                                            <span>No Question added in this Question Bank</span>
                                                        </div>
                                                    </td>
                                                </tr>
                                           <?php
                                        endif;
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        <?php $i++;
                        endforeach;
                    ?>
                </div>

                <div id="manageAssessmentFjQuestionBankDiv">
                    <div class="row">
                        <button type="button" class="btn btn-default pull-left assessmentBackButtonL1" style="margin-bottom: 6px;" value="testing">Back</button>

                    </div>
                    <?php                         
                        //echo "Count".count($contentMyQuestionBank);
                        if(count($jobAssessments['contentFjQuestionsBank'][0]['id']) == ''):?>
                        <table class="table top-head-table question-bank-table">
                            <tbody>
                                <tr class="table_brown_head">
                                    <td class="active">Question Bank</td>
                                    <td class="active">Type</td>
                                    <td class="active">No.of Questions</td>
                                    <td class="active"></td>
                                </tr>  
                            </tbody>
                        </table>
                        <?php endif;?>
                        <?php 
                        $i = 0;
                        foreach ($jobAssessments['contentFjQuestionsBank'] as $item):
                            ?>
                            <div class=" rsp_tbl">
                                <table class="table top-head-table question-bank-table">
                                    <?php if ($i == 0): ?>
                                    <tbody><tr class="table_brown_head">
                                            <td class="active">Question Bank</td>
                                            <td class="active">Type</td>
                                            <td class="active">No.of Questions</td>
                                            <td class="active"></td>
                                        </tr>  
                                    </tbody>
                                    <?php endif; ?>
                                    <tbody>
                                        <tr class="tbl_bg_light">
                                            <td><?php echo $item['name']; ?></td>
                                            <td><?php echo getInterviewSetType($item['type']); ?>
                                                <?php if( $item['type'] != 3 ) { ?>
                                                    <div id ="questionMyVideo<?php echo $item['id']; ?>"></div>
                                                <?php } ?>
                                            </td>
                                            <td><?php 
                                            $q = getQuestionsByQuestionBank($item['id']);
                                            echo $q['count']; ?></td>
                                            <td>
                                                <div class="pull-right">
                                                    <!--<href="#"><img style="display:block !important;" src="<?php echo base_url(); ?>/theme/firstJob/image/arrow_rot.png" id="img_to" class="padd_rgt collapsed" href="#demo1<?php echo $item['id']; ?>" data-toggle="collapse" aria-expanded="false"></a>-->
                                                    <a href="#"><img src="<?php echo base_url(); ?>/theme/firstJob/image/arrow_rot.png" id="img_to" class="padd_rgt collapsed" href="#demo1<?php echo $item['id']; ?>" data-toggle="collapse" aria-expanded="false"></a>
                                                </div>  
                                            </td>
                                        </tr>  
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-12 rsp_tbl collapse" id="demo1<?php echo $item['id']; ?>" aria-expanded="false" style="height: 0px;"> 
                                <table class="table padding_bt_5">
                                    <tbody>
                                        <?php
                                        $q = 0;
                                        $arr = getQuestionOfQuestionBank($item['id']);
                                        if (count($arr) > 0):
                                            foreach ($arr as $itemQuestion):
                                                ?>
                                                <tr>
                                                    <td><?php echo $itemQuestion['title']; ?></td>
                                                    <td>
                                                        <div class="pull-right">
                                                            <a href="javascript:" data-toggle="modal" data-target="#addQuestionToAssessmentSet" class="add-question-to-assessmentset" data-questionid="<?php echo $itemQuestion['id']; ?>" data-id="<?php echo $itemQuestion['type']; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/add_black.png"></a>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php
                                                $q++;
                                            endforeach;
                                        else: ?>
                                                <tr>
                                                    <td></td>
                                                    <td>
                                                        <div class="pull-right">&nbsp;</div>
                                                    </td>
                                                </tr>
                                           <?php
                                        endif;
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        <?php $i++;
                        endforeach;
                        ?>
                </div>

                <div id="addAssessmentSetDiv">
                    <div class="row">
                        <div class="col-md-12 padd_tp_bt_20">
                            <form class="form-horizontal form_save" action="" method="post" id="assessmentForm">
                                <div class="form-group">    
                                  <label for="inputEmail" class="control-label rsp_padd15 col-xs-2 rsp_width_100 required-field">Set Name</label>
                                    <div class="col-xs-10 rsp_width_100">
                                        <input type="text" class="form-control" data-validation="required" name="name" value="<?php if(isset($assessment['name'])) echo $assessment['name'];?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label style="padding-right: 0px;" for="inputPassword" class="control-label col-xs-2 rsp_padd15 required-field">Answer Duration</label>
                                    <div class="col-xs-2 rsp_width_100">
                                        <input type="text" class="form-control" data-validation="number" maxlength="2" name="duration" value="<?php if(isset($assessment['duration'])) echo $assessment['duration'];?>">
                                    </div>
                                     <div class="col-xs-8 rsp_width_100"><p class="padd_txtmin">Minutes</p></div>
                                </div>

                                <div class="form-group" style="margin-bottom: 5px;">
                                    <label for="inputPassword" class="control-label rsp_padd15">Please select Question Bank and random number of questions</label>
                                </div>

                                <div id="questionList">
                                    <div class="form-group" style="margin-bottom: 25px;">
                                        <div class="col-xs-6"></div>
                                        <div class="col-xs-3"><button type="button" class="btn btn-info pull-right plus_sep add-more-assessment-question">Add Random Questions</button></div>
                                        <div class="col-xs-3"><button type="button" class="btn btn-primary plus_sep add-more-assessment-individual-question">Add Individual Question</button></div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-xs-2 rsp_width_100">
                                            <label for="inputPassword" class="control-label no_padding_left">Type : </label>
                                            <div class="clearfix"></div>
                                            <span>Random</span>
                                        </div>
                                        <div class="col-xs-4 rsp_width_100">
                                            <label for="inputPassword" class="control-label no_padding_left">Question Bank</label>
                                            <div class="clearfix"></div>
                                            <select class="selectpicker assessmentBank" name="question[0][questionBank]">
                                                <option value="">-Select Quetion Bank-</option>
                                              <?php foreach($questionBanks as $item) { ?>
                                                <?php
                                                    $questionsBanks = getQuestionOfQuestionBank($item['id']);
                                                    $totalQuestionsOfQuestionBank = count($questionsBanks);
                                                ?>
                                                <option value="<?php echo $item['id'];?>"><?php echo $item['name'];?>(<?php echo $totalQuestionsOfQuestionBank ?>)</option>
                                                <?php } ?>
                                            </select>
                                            <input type="text" data-validation="assessmentQuestion" style="height: 0px; width: 0px; visibility: hidden; " />
                                        </div>
                                        <div class="col-xs-6 rsp_width_100">
                                            <label for="inputPassword" class="control-label no_padding_left">Number of Question</label>
                                            <div class="clearfix"></div>
                                            <input type="text" class="form-control assessmentQuestion" placeholder="" name="question[0][number]" maxlength="3">
                                            <input type="text" data-validation="assessmentQuestion" style="height: 0px; width: 0px; visibility: hidden; " />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-xs-2 rsp_width_100">
                                            <label for="inputPassword" class="control-label no_padding_left">Type : </label>
                                            <div class="clearfix"></div>
                                            <span>Individual</span>
                                        </div>
                                        <div class="col-xs-4 rsp_width_100">
                                            <label for="inputPassword" class="control-label no_padding_left">Question Bank</label>
                                            <div class="clearfix"></div>
                                            <select class="selectpicker assessmentBank1" id="addAssessmentIndividualQuestionBank-0" name="question[0][questionBank1]">
                                                <option value="">-Select Quetion Bank-</option>
                                              <?php foreach($questionBanks as $item):?>
                                                <option value="<?php echo $item['id'];?>"><?php echo $item['name'];?></option>
                                                <?php endforeach;?>
                                            </select>
                                            <input type="text" data-validation="assessmentQuestion" style="height: 0px; width: 0px; visibility: hidden; " />
                                        </div>
                                        <div class="col-xs-6 rsp_width_100">
                                            <label for="inputPassword" class="control-label no_padding_left">Question</label>
                                            <div class="clearfix"></div>
                                            <select class="selectpicker assessmentIndividualQuestion" id="addAssessmentIndividualQuestion-0" name="question[0][question1]">
                                                <option value="">-Select Question-</option>
                                            </select>

                                            <input type="text" data-validation="assessmentQuestion" style="height: 0px; width: 0px; visibility: hidden; " />
                                        </div>
                                    </div>
                                </div>
                              
                                <div class="clearfix">
                              
                                    <div class="col-xs-offset-2 col-xs-10 no_padding">
                                        <button type="submit" class="Save_frm pull-right" style="margin-right: 43px;     background: #054f72 none repeat scroll 0 0; color: white;">SAVE</button>
                                        <button type="button" class="btn btn-default pull-right assessmentBackButtonL1" style="    margin-right: 22px; margin-top: 3px;" value="testing">Back</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div id="addQuestionBankDiv">
                    <div class="row">
                        <div class="col-md-12 padd_tp_bt_20">
                            <form class="form-horizontal form_save" action="" method="post" enctype="multipart/form-data" id="addAssessmnetQuestionBankForm">
                                <div class="form-group">    
                                    <label for="inputEmail" class="control-label rsp_padd15 col-xs-2 rsp_width_100 required-field">Set Name</label>
                                    <div class="col-xs-10 rsp_width_100">
                                        <input type="text" class="form-control" data-validation="required" name="assessmentQuestionBankname">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword" class="control-label col-xs-2 rsp_padd15 required-field">Type</label>

                                    <div class="col-xs-10">
                                        <select class="selectpicker search-critera" name="assessmenQquestionType" id="assessmenQquestionType" data-validation="required">
                                            <option value="">Select</option>
                                            <option value="1">Video</option>
                                            <option value="3">Text</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group" id="enableOnTextSelectBlockAssessment" style="display: none">
                                    <label for="inputEmail" class="control-label col-xs-2">Upload CSV file</label>
                                    <div class="col-xs-10">
                                        <input type="file" name="assessmentQuestionFile" id="assessmentQuestionFile" style="border: 0px" placeholder="">
                                        <?php $fileSamleCsv = base_url() . 'uploads/questions/questioncsvfile/questionset.csv';  ?>
                                        <div style="padding-top:20px;">Sample File: <a href="<?php echo $fileSamleCsv; ?>"> Download Sample File</a></div>
                                    </div>
                                </div>
                    
                                <div class="clearfix"></div>
                                <div class="col-xs-offset-2 col-xs-10">
                                    <input type="hidden" name="hiddenQuestionBankId" id="hiddenQuestionBankId">
                                    <input type="hidden" name="hiddenQuestionBankType" id="hiddenQuestionBankType">
                                    <button type="submit" class="Save_frm pull-right" style="background: #054f72 none repeat scroll 0 0; color: white; margin-right: 31px;">Save</button>
                                    <button type="button" class="btn btn-default pull-right assessmentBackButtonL2" style="    margin-right: 22px; margin-top: 3px;" value="testing">Back</button>
                                </div>
                                
                            </form>
                        </div>
                    </div>
                </div>

                <div id="editAssessmentSetDiv">
                    <div class="row">
                        <div class="col-md-12 padd_tp_bt_20" id="editAssessmentSetDivContent">
                            <form class="form-horizontal form_save" action="" method="post" id="editAssessmentForm">
                                <div class="form-group">    
                                    <label for="inputEmail" class="control-label rsp_padd15 col-xs-2 rsp_width_100 required-field">Set Name</label>
                                    <div class="col-xs-10 rsp_width_100">
                                        <input type="text" class="form-control" data-validation="required" name="name" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label style="padding-right: 0px;" for="inputPassword" class="control-label col-xs-2 rsp_padd15 required-field">Answer Duration</label>
                                    <div class="col-xs-2 rsp_width_100">
                                        <input type="text" class="form-control" data-validation="number" maxlength="2" name="duration" value="">
                                    </div>
                                    <div class="col-xs-8 rsp_width_100"><p class="padd_txtmin">Minutes</p></div>
                                </div>
                                <div class="form-group" style="margin-bottom: 5px;">
                                    <label for="inputPassword" class="control-label rsp_padd15">Please select Question Bank and random number of questions</label>
                                </div>
                                <div class="form-group" style="margin-bottom: 25px;">
                                    <div class="col-xs-6"></div>
                                    <div class="col-xs-3"><button type="button" class="btn btn-info pull-right plus_sep add-more-assessment-question-edit">Add Random Questions</button></div>
                                    <div class="col-xs-3"><button type="button" class="btn btn-primary plus_sep add-more-assessment-individual-question-edit">Add Individual Question</button></div>
                                </div>

                                <div id="questionListEdit">
                                    
                                </div>
                                <div class="clearfix">
                                    <div class="col-xs-offset-2 col-xs-10 no_padding">
                                        <input type="hidden" name="editAssessemntId" value="">
                                        <button type="submit" class="Save_frm pull-right" style="margin-right: 30px; background: #054f72 none repeat scroll 0 0; color: white;">SAVE</button>
                                        <button type="button" class="btn btn-default pull-right assessmentBackButtonL1" style="    margin-right: 22px; margin-top: 3px;" value="testing">Back</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div id="assessmentQuestionBankAddQuestionDiv">
                    <div class="row">
                        <div class="col-md-12 padd_tp_bt_20">
                            <form class="form-horizontal form_save" method="post" action="" enctype="multipart/form-data" id="questionassessmentForm">
                                <div class="form-group">
                                    <label for="inputPassword" class="control-label col-xs-2 required-field">Type</label>
                                    <div class="col-xs-8">
                                        <select class="selectpicker" data-validation="required" name="type" id="questionFormType"><option value="">Select</option>
                                            <option value="1">Video</option>
                                            <option value="3">Text</option>
                                        </select>
                                    </div>
                                </div>
                                <input type="hidden" name="hiddenType"/>

                                <div id ="assessmentQuestionList">
                                    <div class="form-group">
                                        <label for="inputPassword" class="control-label col-xs-2 required-field">Question</label>
                                        <div class="col-xs-3 ques-title">
                                            <label for="inputPassword" class="control-label frm_in_padd">Title</label>
                                            <input type="text" id="questionTitleId" data-validation="required" class="form-control file-name" name="question[0][title]">
                                        </div>

                                        <div class="col-xs-3 file-section assessmentQuestionFileDiv" style="display: none;">
                                            <div class="col-md-12">
                                                <label for="inputPassword" class="control-label frm_in_padd">File</label>
                                            </div>
                                            <div class="col-xs-8">
                                                <input type="text" name="filename_0" class="form-control file-name assessmentQuestionFileName" readonly="readonly">
                                            </div>
                                            <div class="col-xs-4 no_padding browse-section">
                                                <button type="button" class="btn_upload">BROWSE</button>
                                                <input type="file" name="file_0" class="my-question-file assessmentQuestionFileData"  />
                                                <input type="text" data-validation="assessmentQuestionFileData" style="height: 0px; width: 0px; visibility: hidden; " />
                                            </div>
                                        </div>
                                            
                                    </div>
                                    
                                    <div class="form-group question-option">
                                        <label for="inputPassword" class="control-label col-xs-2"></label>
                                        <div class="col-xs-10 ques-title">
                                            <div class="row">Answer1</label></div>
                                            <div class="row row-answer-assessment">
                                                <div class="col-sm-1 answer-check-assessment"> <input data-validation="required" type="radio" name="question[0][check][]" value="1" id="questionCheck1" checked="" /></div>
                                            <div class="col-sm-3">
                                                <input data-validation="required" type="text" id="quesitonQnswer1" class="form-control file-name" name="question[0][answer][1]">
                                            
                                            </div>
                                            </div>
                                            
                                        </div>
                                        <label for="inputPassword" class="control-label col-xs-2"></label>
                                        <div class="col-xs-10 ques-title">
                                            <div class="row">Answer2</label></div>
                                            <div class="row row-answer-assessment">
                                                <div class="col-sm-1 answer-check-assessment"> <input id="questionCheck2" type="radio" name="question[0][check][]" value="2"/></div>
                                            <div class="col-sm-3">
                                            <input type="text" class="form-control file-name" id="quesitonQnswer2" data-validation="required" name="question[0][answer][2]">
                                            
                                            </div>
                                            </div>
                                            
                                        </div>
                                        <label for="inputPassword" class="control-label col-xs-2"></label>
                                        <div class="col-xs-10 ques-title">
                                            <div class="row">Answer3</label></div>
                                            <div class="row row-answer-assessment">
                                                <div class="col-sm-1 answer-check-assessment"> <input id="questionCheck3" type="radio" name="question[0][check][]" value="3" /></div>
                                            <div class="col-sm-3">
                                            <input type="text" class="form-control file-name" id="quesitonQnswer3" data-validation="required" name="question[0][answer][3]">
                                            
                                            </div>
                                            </div>
                                            
                                        </div>
                                        <label for="inputPassword" class="control-label col-xs-2"></label>
                                        <div class="col-xs-10 ques-title">
                                            <div class="row">Answer4</label></div>
                                            <div class="row row-answer-assessment">
                                                <div class="col-sm-1 answer-check-assessment"> <input id="questionCheck4" type="radio" name="question[0][check][]" value="4" /></div>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control file-name" id="quesitonQnswer4" data-validation="required" name="question[0][answer][4]">
                                            
                                            </div>
                                            </div>

                                            <label for="inputPassword" class="control-label col-xs-9" style="color:#FF0000;padding-left: 0px;">Note : Please move the radio button to choose the correct answer</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-offset-2 col-xs-10 no_padding"><br><br></div>
                                
                                <div class="clearfix"></div>
                                <div class="col-xs-offset-2 col-xs-10" style="padding-left: 30px;">
                                    
                                    <input type="hidden" name="accessToken" value="<?php echo $token; ?>"/>
                                    <input type="hidden" name="questionBankId">
                                    <input type="hidden" name="questionId">

                                    <button type="button" class="btn btn-default pull-left assessmentBackButtonL2" style="    margin-right: 22px; margin-top: 3px;">Back</button>

                                    <button type="submit" class="Save_frm pull-left save-interview" value="testing" style="background: #054f72 none repeat scroll 0 0; color: white;">Save</button>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<!--End of Manage Assesment Modal-->


<!--Interview set Modal-->
<!--Manage Assesment Modal-->
<div id="manageInterviewModal" class="modal fade" style="overflow-y: auto;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <div>
                    <div class="pull-left">
                        <h4 class="modal-title" id="manageInterviewSetTitle" style="cursor: pointer;">Manage Interview Sets</h4>
                    </div>
                    <div class="pull-left interviewBreadcrumbText">
                        <h4 class="modal-title" id="interviewBreadcrumbText" style="cursor: pointer;">&nbsp; </h4>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="modal-body">
                <div id="manageInterviewsSetsTables">
                    <div class="row">
                        <div class="col-md-7 col-md-offset-5" style="margin-right: 0px; padding-bottom: 6px; padding-right: 0px;">
                            <button type="button" class="btn btn-primary pull-right" id="myInterviewQuestionBankButton">My Question Bank</button>
                            <button type="button" class="btn btn-info pull-right" id="interviewFjQuestionBankButton" style="margin-right: 30px;">VH Question Bank</button>
                            <button id="addInterviewSetButton" type="button" class="btn btn-default pull-right" style="background: #054f72 none repeat scroll 0 0; color: white; margin-right: 30px;">Add Interview Set</button>
                        </div>
                    </div>

                    <table class="table" id="interviewSetTable">
                        <thead class="tbl_head">
                            <tr>
                                <th style="width: 300px;"><span>Title</span></th>
                                <th><span>Type</span></th>
                                <th><span>No. of Questions</span></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 0;
                            foreach ($jobIntreviews['contentInterviewSet'] as $item):
                                // ci instance
                                $ci =&get_instance();
                                //job user answer model
                                $ci->load->model('useranswer_model');
                                // function to get users interview available or not
                                $userinterviewcount = $ci->useranswer_model->countForUserInterview($item->id); 
                                $interviewcount     = $userinterviewcount['interviewcount'];                    
                                ?>
                                <tr id="trInterview_<?php echo $item->id; ?>" <?php if ($i % 2 != 0) echo 'class="tbl_bg"'; ?>>
                                    <td><?php echo $item->name; ?></td>
                                    <td><?php echo getInterviewSetType($item->type); ?></td>
                                    <td><?php echo $item->questionCount; ?></td>
                                    <td> 
                                        <?php $checkInterview = interviewExists($item->id); ?>
                                        <a href="javascript:" onclick="editInterviewSetRow(<?php echo $item->id; ?>)" class="editInterviewSetRow" id="editInterviewSetRow-<?php echo $item->id; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/edit.png" class="padd_rgt"></a>
                                        <?php if( $interviewcount > 0 || $checkInterview>0 ) { ?>
                                            <a href="javascript:" class="notdelete-set" id="<?php echo $item->id; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/del.png"></a>
                                        <?php } else { ?>
                                            <a href="javascript:" class="delete-set deleteInterviewSetRow" id="deleteInterviewSetRow-<?php echo $item->id; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/del.png"></a>
                                        <?php } ?>
                                    </td>
                                </tr>
                                <?php
                                $i++;
                            endforeach;
                            ?>
                        </tbody>
                    </table>
                </div>

                <div id="manageInterviewFjQuestionBankDiv">
                    <div class="col-md-6 pull-left">
                        <button type="button" class="btn btn-default pull-left interviewBackButtonL1" style="    margin-right: 22px; margin-top: 3px;" value="testing">Back</button>
                    </div>
                    <table class="table" id="interviewFJQuestionBankTable">
                        <thead class="tbl_head">
                            <tr>
                                <th style="width: 600px;"><span>Title</span></th>
                                <th style="width: 100px;"><span>Type</span></th>
                                <th style="width: 100px;"><span>Duration</span></th>
                                <th style="width: 200px;"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 0;
                            foreach ($jobIntreviews['contentFjQuestions'] as $itemFj):
                                ?>
                                <tr <?php if ($i % 2 != 0) echo 'class="tbl_bg"'; ?>>
                                    <td><?php echo $itemFj->title; ?></td>
                                    <td>
                                        <?php if( $itemFj->type=='1' || $itemFj->type=='2') { ?>
                                            <a href="javascript:" data-toggle="modal" data-target="#viewFile" class="viewFile" data-filename="<?php echo $itemFj->file; ?>" data-filetype="<?php echo $itemFj->type; ?>"><?php echo getInterviewSetType($itemFj->type); ?></a>
                                            <!--<div id ="questionVideo<?php //echo $itemFj->id; ?>"></div>-->
                                        <?php } else { echo getInterviewSetType($itemFj->type); } ?>
                                    </td>
                                    <td><?php echo $itemFj->duration; ?> Sec.</td>
                                    <td> 
                                        <!--<a href="javascript:" data-toggle="modal" data-target="#addQuestionToInterviewSet" class="add-question-to-interviewset" data-questionid="<?php echo $itemFj->id; ?>" data-id="<?php echo $itemFj->type; ?>" title="Click on “+” to add the selected question to a pre-existing interview set of that type."><img src="<?php echo base_url(); ?>/theme/firstJob/image/add_black.png" class="padd_rgt"></a>-->
                                        <a href="javascript:" onclick="copydataSetRecruiter(<?php echo $itemFj->id; ?>,<?php echo $itemFj->type; ?>)" class="copydata-set-recruiter" data-questionid="<?php echo $itemFj->id; ?>" data-id="<?php echo $itemFj->type; ?>" title="Copy VH question to My question bank"><img src="<?php echo base_url(); ?>/theme/firstJob/image/copyicon.png"></a>
                                    </td>
                                </tr>
                                <?php
                                $i++;
                            endforeach;
                            ?>
                        </tbody>
                    </table>
                </div>

                <div id="manageInterviewMyQuestionDiv">
                    <div class="row">
                        <div class="col-md-6 pull-left">
                            <button type="button" class="btn btn-default pull-left interviewBackButtonL1" style="    margin-right: 22px; margin-top: 3px;" value="testing">Back</button>
                        </div>
                        <div class="col-md-6 pull-right" style="margin-right: 0px; padding-bottom: 6px; padding-right: 0px;">
                            <button id="addInterviewQuestionBankButton" type="button" class="btn btn-default pull-right" style="background: #054f72 none repeat scroll 0 0; color: white;">Add Question</button>
                        </div>
                    </div>
                    <table class="table" id="interviewMyQuestionBankTable">
                        <thead class="tbl_head">
                            <tr>
                                <th style="width: 600px;"><span>Title</span></th>
                                <th style="width: 100px;"><span>Type</span></th>
                                <th style="width: 100px;"><span>Duration</span></th>
                                <th style="width: 200px;"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 0;
                            foreach ($jobIntreviews['contentMyQuestions'] as $itemMy):                                    
                                // ci instance
                                $ci =&get_instance();
                                //job user answer model
                                $ci->load->model('interviewquestion_model');
                                // function to get users interview available or not
                                $userinterviewquestioncount = $ci->interviewquestion_model->countForUserInterviewQuestion($itemMy->id); 
                                $interviewquestioncount     = $userinterviewquestioncount['interviewquestioncount'];                                    
                                ?>
                                <tr id="interviewMyQuestionTr_<?php echo $itemMy->id;?>" <?php if ($i % 2 != 0) echo 'class="tbl_bg"'; ?>>
                                    <td><?php echo $itemMy->title; ?></td>
                                    <td>
                                        <?php if( $itemMy->type=='1' || $itemMy->type=='2') { ?>
                                            <a href="javascript:" data-toggle="modal" data-target="#viewFile" class="viewFile" data-filename="<?php echo $itemMy->file; ?>" data-filetype="<?php echo $itemMy->type; ?>"><?php echo getInterviewSetType($itemMy->type); ?></a>
                                            <!--<div id ="questionMyVideo<?php //echo $itemMy->id; ?>"></div>-->
                                        <?php } else { echo getInterviewSetType($itemMy->type); } ?>
                                    </td>
                                    <td><?php echo $itemMy->duration; ?> Sec.</td>
                                    <td>
                                        <?php $checkInterviewBank = interviewBankExists($itemMy->id); ?>
                                        <a  href="javascript:" onclick="editInterviewQuestionRow(<?php echo $itemMy->id; ?>)" class="editInterviewQuestionRow" id="editInterviewQuestionRow-<?php echo $itemMy->id; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/edit.png" class="padd_rgt"></a>
                                        <!--<a href="javascript:" data-toggle="modal" data-target="#addQuestionToInterviewSet" class="add-question-to-interviewset" data-questionid="<?php echo $itemMy->id; ?>" data-id="<?php echo $itemMy->type; ?>" title="Click here to add the selected question to a pre-existing interview set of that type"> <img src="<?php echo base_url(); ?>/theme/firstJob/image/add_black.png" class="padd_rgt"></a>-->
                                        
                                        
                                        <?php
                                        //  get all set id where this question attached
                                        $getAllInterviewSet = $ci->interviewquestion_model->interviewSetId($itemMy->id);                                            
                                        $interviewSetIds    = $getAllInterviewSet['setID'];
                                        
                                        
                                        //  get all jobs where these interview set attached
                                        $jobIdCount = 0;
                                        if($interviewSetIds) {
                                            $interviewSetId     = explode(',',$interviewSetIds);
                                            $implodeSetIds      = "'" . implode("','", $interviewSetId) . "'";                                                
                                            $checkInterviewBank = interviewSetJobExists($implodeSetIds);
                                            $jobIdCount = $checkInterviewBank[0]['jobIds'];       
                                        }
                                        
                                       ?>
                                        
                                        
                                        <?php 
                                        if($jobIdCount>0) { ?>
                                            <a href="javascript:" class="notdelete-set" id="<?php echo $itemMy->id; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/del.png"></a>
                                        <?php } else { ?>
                                            <a href="javascript:" class="deleteInterviewQuestionRow" id="deleteInterviewQuestionRow-<?php echo $itemMy->id; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/del.png"></a>
                                        <?php } ?>
                                    </td>                                        
                                </tr>
                                <?php
                                $i++;
                            endforeach;
                            ?>
                        </tbody>
                    </table>
                </div>

                <div id="addInterviewSetDiv">
                    <div class="row">
                        <div class="col-md-12 padd_tp_bt_20">
                            <form class="form-horizontal form_save" name="interviewsetfrm" method="post" action="" enctype="multipart/form-data" id="interviewsetfrm">
                                    
                                <div class="form-group" style="margin-left: 0px !important; margin-right: 0px !important;">
                                    <label for="inputEmail" class="control-label col-xs-2 required-field">Set Name</label>
                                    <div class="col-xs-10">
                                        <input type="text" class="form-control" name="name" id="setname_value" data-validation="required" maxlength="100">
                                    </div>
                                </div>

                                <!-- FIRST SECTION QUESTIONS -->

                                <div class="form-group" style="margin-left: 0px !important; margin-right: 0px !important;">
                                    <label for="inputPassword" class="control-label col-xs-2 required-field">Type</label>
                                    <div class="col-xs-10" id="blockInterviewSection1">
                                        <select class="selectpicker" name="type" id="type" data-validation="required">
                                            <option value="">Select</option>
                                            <option value="1">Video</option>
                                            <option value="3">Text</option>
                                        </select>
                                    </div>
                                    <input type="hidden" name="intereviewSetEditId">
                                    <input type="hidden" name="intereviewSetEditType">
                                    <input type="hidden" id="hasanyinterviewsetquestion">
                                </div>

                                <div id ="questionList3">
                                    <input type="text" data-validation="hasclassinterviewset" style="height: 0px; width: 0px; visibility: hidden; ">
                                </div>

                                <div class="clearfix"></div>
                                <div class="col-xs-offset-2 col-xs-10">

                                    <input type="hidden" name="accessToken" value="<?php echo $token; ?>"/>
                                    <button type="submit" class="Save_frm pull-right save-interview" style="background: #054f72 none repeat scroll 0 0; color: white;" id="confirmbefore_submit" value="testing">Save</button>
                                    <button type="button" class="btn btn-default pull-right interviewBackButtonL1" style="    margin-right: 22px; margin-top: 3px;" value="testing">Back</button>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div id="questionBankAddQuestionDiv">
                    <div class="row">
                        <div class="col-md-12 padd_tp_bt_20">
                            <form class="form-horizontal form_save" method="post" action="" enctype="multipart/form-data" id="addQuestionForm">
                                <div id ="questionListInterviewAddQuestion">
                                    <div class="form-group" style="margin-left: 0px !important; margin-right: 0px !important;">
                                        <div class="col-xs-2 ques-title">
                                            <label for="inputPassword" class="control-label frm_in_padd">Type</label>
                                            <select class="selectpicker questionType" name="question[0][type]" id="type_addquestion">
                                                <option value="">Select</option>
                                                <option value="1">Video</option>
                                                <option value="3">Text</option>
                                            </select>
                                            <input type="text" data-validation="questionType" style="height: 0px; width: 0px; visibility: hidden; " /> 
                                        </div>


                                        <div class="col-xs-4 ques-title">
                                            <label for="inputPassword" class="control-label frm_in_padd">Question Title</label>
                                            <input type="text" name="question[0][title]" value="" class="form-control questionTitle" placeholder="Type a question" />
                                            <input type="text" data-validation="questionTitle" style="height: 0px; width: 0px; visibility: hidden; " /> 
                                        </div>

                                        <div class="col-xs-3 file-section" style="display: none;">
                                            <div class="col-md-12">
                                                <label for="inputPassword" class="control-label frm_in_padd" style="font-size: 12px;">Upload File(mp3, mp4)</label>
                                            </div>
                                            <div class="col-xs-8">
                                                <input type="text" class="form-control file-name questionFileName" readonly>
                                            </div>
                                            <div class="col-xs-4 no_padding browse-section">
                                                <button type="button" class="btn_upload questionFile">BROWSE</button>
                                                <input type="file" name="file_0" class="my-question-file questionFile" />
                                                <input type="text" data-validation="questionFile" style="height: 0px; width: 0px; visibility: hidden; " />
                                            </div>
                                        </div>

                                        <div class="col-xs-3">
                                            <label for="inputPassword" class="control-label frm_in_padd col-xs-12">Answer Duration <span style="font-size:10px;">(Limit 600 Sec)</span></label>
                                            <div class="col-md-6 no_padding">
                                                <input type="number" min="1" class="form-control questionDuration" placeholder="" name="question[0][duration]" maxlength="3">
                                                <input type="text" data-validation="questionDuration" style="height: 0px; width: 0px; visibility: hidden; " />
                                            </div>
                                            <div class="col-md-6">
                                                <p class="padd_txtmin">Sec <a href="javascript:"><img src="<?php echo base_url(); ?>/theme/firstJob/image/plus.png" class="pad_rg_lfs add-more-my-question-recruiter">
                                                    </a></p>
                                            </div>  
                                        </div> 
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <div class="col-xs-offset-2 col-xs-10 no_padding">
                                    <input type="hidden" name="accessToken" value="<?php echo $token; ?>"/>

                                    <button type="submit" class="Save_frm pull-right save-interview" style="background: #054f72 none repeat scroll 0 0; color: white;" value="testing">Save</button>
                                    <button type="button" class="btn btn-default pull-right interviewBackButtonL2" style="    margin-right: 22px; margin-top: 3px;" value="testing">Back</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div id="editQuestionFormDiv">
                    <div class="row">
                        <div class="col-md-12 padd_tp_bt_20">
                            <form class="form-horizontal form_save" method="post" action="" enctype="multipart/form-data" id="editQuestionForm">
                                <div id ="editQuestionList">
                                    <div class="form-group" style="margin-left: 0px !important; margin-right: 0px !important;">
                                        <div class="col-xs-2 ques-title">
                                            <label for="inputPassword" class="control-label frm_in_padd">Type</label>
                                            <select class="selectpicker" name="editQuestionType" id="editQuestionType"  data-validation="required"><option value="">Select</option>
                                                <option value="1">Video</option>
                                                <option value="3">Text</option>
                                            </select>
                                            <input type="hidden" name="editQuestionType">
                                        </div>


                                        <div class="col-xs-4 ques-title">
                                            <label for="inputPassword" class="control-label frm_in_padd">Title</label>
                                            <input type="text" class="form-control file-name" name="editQuestionTitle" data-validation="required">
                                            
                                        </div>

                                        <div class="col-xs-3 file-section" style="display: none;" id="fileselection">
                                            <div class="col-md-12">
                                                <label for="inputPassword" class="control-label frm_in_padd">File</label>
                                            </div>
                                            <div class="col-xs-8">
                                                <input type="text" name="fileSelectionName" class="form-control file-name-selection" readonly="readonly">
                                            </div>
                                            <div class="col-xs-4 no_padding browse-section">
                                                <button type="button" class="btn_upload">BROWSE</button>
                                                <input type="file" name="file_0" class="my-question-file" />
                                            </div>
                                            <br/>
                                            <div class="question-video">
                                                <div id="questionVideo1" class="question-video" ></div>
                                            </div>
                                        </div>

                                        <div class="col-xs-3">
                                            <label for="inputPassword" class="control-label frm_in_padd col-xs-12">Answer Duration <span style="font-size:10px;">(Limit 600 Sec)</span></label>
                                            <div class="col-md-6 no_padding">
                                                <input type="number" min="1" class="form-control editQuestionDuration" placeholder="" name="editQuestionDuration" maxlength="3" data-validation="editQuestionDuration">
                                            </div>
                                            <div class="col-md-6">
                                                <p class="padd_txtmin">Sec
                                                </p>
                                            </div>  
                                        </div> 
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <div class="col-xs-offset-2 col-xs-10 no_padding">

                                    <input type="hidden" name="accessToken" value="<?php echo $token; ?>"/>
                                    <input type="hidden" name="questionId">
                                    <button type="submit" class="Save_frm pull-right save-interview" value="testing" style="margin-right: 30px; background: #054f72 none repeat scroll 0 0; color: white;">SAVE</button>
                                    <button type="button" class="btn btn-default pull-right interviewBackButtonL2" style="    margin-right: 22px; margin-top: 3px;" value="testing">Back</button>

                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- End of Interview set Modal-->

<!-- PopUp for add question to interview set  -->
<div id="addQuestionToInterviewSet" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <form class="form-horizontal form_save" action="" method="post" id="interviewQuestionForm" >
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Question To Interview Set</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger hidden" id="interviewQuestionAlert"></div>
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-3">Interview Sets</label>
                        <div class="col-xs-9" id="interviewSets"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary add-interview-question-recruiter">Save</button>
                </div>
            </div>
        </form>
    </div>
</div>

<!-- PopUp for add question to assessment set  -->
<div id="addQuestionToAssessmentSet" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <form class="form-horizontal form_save" action="" method="post" id="assessmentQuestionForm" >
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Question To Assessment Set</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger hidden" id="interviewQuestionAlert">

                    </div>
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-3">Assessment Sets</label>
                        <div class="col-xs-9" id="assessmentSets">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary add-assessment-question">Save</button>
                </div>
            </div>
        </form>

    </div>
</div>


<!-- PopUp for add question to interview set  -->
<div id="selectQuestionType" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <form class="form-horizontal form_save" action="<?php echo base_url(); ?>interviews/addToInterview" method="post" id="interviewQuestionForm" >
            <div class="modal-content">

                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Choose Question Bank</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger hidden" id="interviewQuestionAlert">

                    </div>
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-3"></label>
                        <div class="col-xs-9" id="questionType">
                            <select data-placeholder="questionType" style="width: 350px;" id="aQuestionType" name="questionType">
                                <option value="1">VH Question Bank</option>
                                <option value="2">My Question Bank</option>

                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary select-interview-question">Add</button>

                    <!--                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
                </div>
            </div>
        </form>

    </div>
</div>


<!-- PopUp for add question to interview set  -->
<div id="selectQuestionType1" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <form class="form-horizontal form_save" action="<?php echo base_url(); ?>interviews/addToInterview" method="post" id="interviewQuestionForm" >
            <div class="modal-content">

                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Choose Question Bank</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger hidden" id="interviewQuestionAlert">

                    </div>
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-3"></label>
                        <div class="col-xs-9" id="questionType">
                            <select data-placeholder="questionType" style="width: 350px;" id="aQuestionType" name="questionType">
                                <option value="1">VH Question Bank</option>
                                <option value="2">My Question Bank</option>

                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary select-interview-question1">Add</button>

                    <!--                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
                </div>
            </div>
        </form>

    </div>
</div>

<!-- PopUp for add question to interview set  -->
<div id="selectQuestionType2" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <form class="form-horizontal form_save" action="<?php echo base_url(); ?>interviews/addToInterview" method="post" id="interviewQuestionForm" >
            <div class="modal-content">

                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Choose Question Bank</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger hidden" id="interviewQuestionAlert">

                    </div>
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-3"></label>
                        <div class="col-xs-9" id="questionType">
                            <select data-placeholder="questionType" style="width: 350px;" id="aQuestionType" name="questionType">
                                <option value="1">VH Question Bank</option>
                                <option value="2">My Question Bank</option>

                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary select-interview-question2">Add</button>
                </div>
            </div>
        </form>

    </div>
</div>


<script src="<?php echo base_url(); ?>theme/firstJob/js/custom-script.js"></script>
<script src="<?php echo base_url(); ?>theme/firstJob/js/chosen.jquery.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>theme/firstJob/js/prism.js" type="text/javascript" charset="utf-8"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script src="<?php echo base_url(); ?>theme/firstJob/js/bootstrap.min.js"></script>
<link href="//cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.35.4/css/bootstrap-dialog.min.css" rel="stylesheet" type="text/css">
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.35.4/js/bootstrap-dialog.min.js"></script>
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>


<script type="text/javascript">
    var config = {
        '.chosen-select': {},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: ''},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }


    var siteUrl = '<?php echo base_url(); ?>';

    $('.assessmentSetTabel').DataTable({
        responsive: true,
        lengthChange: false,
        pageLength: 10
    });

    /* Interview Set Module script starts */
        $('#interviewSetTable').DataTable({
            responsive: true,
            lengthChange: false,
            pageLength: 10
        });

        $('#interviewFJQuestionBankTable').DataTable({
            responsive: true,
            lengthChange: false,
            pageLength: 10
        });

        $('#interviewMyQuestionBankTable').DataTable({
            responsive: true,
            lengthChange: false,
            pageLength: 10
        });

        $( "#manageInterviewFjQuestionBankDiv" ).hide();
        $( "#manageInterviewMyQuestionDiv" ).hide();
        $( "#addInterviewSetDiv" ).hide();
        $( "#questionBankAddQuestionDiv" ).hide();
        $( "#editQuestionFormDiv" ).hide();

        $( "#manageInterviewSetTitle" ).click(function() {
            $( "#interviewBreadcrumbText" ).text('');
            $( ".addInterviewQuestionBreadcrumb" ).remove();
            $( "#manageInterviewMyQuestionDiv" ).hide();
            $( "#manageInterviewFjQuestionBankDiv" ).hide();
            $( "#addInterviewSetDiv" ).hide();
            $( "#manageInterviewsSetsTables" ).show();
            $( "#questionBankAddQuestionDiv" ).hide();
            $( "#editQuestionFormDiv" ).hide();
        });

        $( ".interviewBackButtonL1" ).click(function() {
            $( "#interviewBreadcrumbText" ).text('');
            $( ".addInterviewQuestionBreadcrumb" ).remove();
            $( "#manageInterviewMyQuestionDiv" ).hide();
            $( "#manageInterviewFjQuestionBankDiv" ).hide();
            $( "#addInterviewSetDiv" ).hide();
            $( "#manageInterviewsSetsTables" ).show();
            $( "#questionBankAddQuestionDiv" ).hide();
            $( "#editQuestionFormDiv" ).hide();
        });

        $( "#resetAllInterviewModalContent" ).click(function() {
            $( "#interviewBreadcrumbText" ).text('');
            $( ".addInterviewQuestionBreadcrumb" ).remove();
            $( "#manageInterviewMyQuestionDiv" ).hide();
            $( "#manageInterviewFjQuestionBankDiv" ).hide();
            $( "#addInterviewSetDiv" ).hide();
            $( "#manageInterviewsSetsTables" ).show();
            $( "#questionBankAddQuestionDiv" ).hide();
            $( "#editQuestionFormDiv" ).hide();
        });

        $( "#interviewFjQuestionBankButton" ).click(function() {
            $( "#interviewBreadcrumbText" ).text(' -> VH Question Bank');
            $( ".addInterviewQuestionBreadcrumb" ).remove();
            $( "#manageInterviewsSetsTables" ).hide();
            $( "#manageInterviewMyQuestionDiv" ).hide();
            $( "#addInterviewSetDiv" ).hide();
            $( "#manageInterviewFjQuestionBankDiv" ).show();
            $( "#editQuestionFormDiv" ).hide();
        });

        $( "#myInterviewQuestionBankButton" ).click(function() {
            $( "#interviewBreadcrumbText" ).text(' -> My Question Bank');
            $( ".addInterviewQuestionBreadcrumb" ).remove();
            $( "#manageInterviewsSetsTables" ).hide();
            $( "#manageInterviewFjQuestionBankDiv" ).hide();
            $( "#addInterviewSetDiv" ).hide();
            $( "#manageInterviewMyQuestionDiv" ).show();
            $( "#questionBankAddQuestionDiv" ).hide();
            $( "#editQuestionFormDiv" ).hide();
        });

        $("#addInterviewSetButton").click(function() {
            $('#interviewsetfrm').each(function(){
                this.reset();
            });
            $("#interviewsetfrm input[name=intereviewSetEditId]").val('');
            $("#interviewsetfrm input[name=intereviewSetEditType]").val('');

            $('#interviewsetfrm #type').prop("disabled", false);
            $('#interviewsetfrm #questionList3').html('');
            //$('#interviewsetfrm #questionList3').html('');
            $('#interviewsetfrm #questionList3').html('<div class="form-group" style="margin-left: 0px !important; margin-right: 0px !important; margin-top: 40px; margin-bottom: 17px;"><label for="inputPassword" class="control-label col-xs-6 required-field">Questions</label><div class="col-xs-6"><a class="pull-right" href="javascript:" data-toggle="modal" data-target="#selectQuestionType"><img src="<?php echo base_url(); ?>/theme/firstJob/image/plus.png" class="pad_rg_lfs add-more-question" id="addMore1"><input type="hidden" class="add-more-question-new"/></a></div></div>');

            $( "#interviewBreadcrumbText" ).text(' -> Add Interview Set');
            $( ".addInterviewQuestionBreadcrumb" ).remove();
            $( "#manageInterviewsSetsTables" ).hide();
            $( "#manageInterviewMyQuestionDiv" ).hide();
            $( "#manageInterviewFjQuestionBankDiv" ).hide();
            $( "#addInterviewSetDiv" ).show();
            $( "#editQuestionFormDiv" ).hide();
            var config = {
                    '.chosen-select': {},
                    '.chosen-select-deselect': {allow_single_deselect: true},
                    '.chosen-select-no-single': {disable_search_threshold: 10},
                    '.chosen-select-no-results': {no_results_text: ''},
                    '.chosen-select-width': {width: "95%"}
                }
                for (var selector in config) {
                    $(selector).chosen(config[selector]);
                }
        });

        $( "#addInterviewQuestionBankButton" ).click(function() {
            $( "#interviewBreadcrumbText" ).text(' -> My Question Bank');

            $( '#addQuestionForm' ).each(function(){
                this.reset();
            });

            $( '<div class="pull-left addInterviewQuestionBreadcrumb"><h4 class="modal-title"> -> Add Question</h4></div>' ).insertAfter( ".interviewBreadcrumbText" );

            $( "#manageInterviewsSetsTables" ).hide();
            $( "#manageInterviewMyQuestionDiv" ).hide();
            $( "#manageInterviewFjQuestionBankDiv" ).hide();
            $( "#addInterviewSetDiv" ).hide();
            $( "#questionBankAddQuestionDiv" ).show();
            $( "#editQuestionFormDiv" ).hide();
        });

        $( "#interviewBreadcrumbText" ).click(function() {
            var breadcrumbText = $.trim($(this).text().replace(/[\.,:->-]+/g, ""));
            if(breadcrumbText == 'My Question Bank') {
                $( "#interviewBreadcrumbText" ).text(' -> My Question Bank');
                $( ".addInterviewQuestionBreadcrumb" ).remove();
                $( "#manageInterviewsSetsTables" ).hide();
                $( "#manageInterviewFjQuestionBankDiv" ).hide();
                $( "#addInterviewSetDiv" ).hide();
                $( "#manageInterviewMyQuestionDiv" ).show();
                $( "#questionBankAddQuestionDiv" ).hide();
                $( "#editQuestionFormDiv" ).hide();
            }
        });

        $( ".interviewBackButtonL2" ).click(function() {
            //var breadcrumbText = $.trim($(this).text().replace(/[\.,:->-]+/g, ""));
            $( "#interviewBreadcrumbText" ).text(' -> My Question Bank');
            $( ".addInterviewQuestionBreadcrumb" ).remove();
            $( "#manageInterviewsSetsTables" ).hide();
            $( "#manageInterviewFjQuestionBankDiv" ).hide();
            $( "#addInterviewSetDiv" ).hide();
            $( "#manageInterviewMyQuestionDiv" ).show();
            $( "#questionBankAddQuestionDiv" ).hide();
            $( "#editQuestionFormDiv" ).hide();
        });

        function copydataSetRecruiter(questionid, questionType) {
            bootbox.confirm('Are you sure you want to copy this question to My Question Bank?', function (result) {

                if (result) {
                    $.post(siteUrl + 'interviews/fjquestioncopy', {questionId: questionid, accessToken: token}, function (data) {
                        var obj = $.parseJSON(data);
                        var questionDetails = obj.questionDetails;
                        var questionId = obj.questionId;

                        if(questionDetails.type==1) {
                            var type = 'Video';
                        } else {
                            var type = 'Text';
                        }
                        $('#interviewMyQuestionBankTable tbody').prepend('<tr id="interviewMyQuestionTr_'+questionId+'"><td>'+questionDetails.title+'</td><td>'+type+'</td><td>'+questionDetails.duration+' Sec.</td><td><a href="javascript:" onclick="editInterviewQuestionRow('+questionId+')" class="editInterviewQuestionRow" id="editInterviewQuestionRow-'+questionId+'"><img class="padd_rgt" src="'+siteUrl+'theme/firstJob/image/edit.png"></a><a style="padding-left: 3px;" href="javascript:" class="deleteInterviewQuestionRow" id="deleteInterviewQuestionRow-'+questionId+'"><img src="'+siteUrl+'theme/firstJob/image/del.png"></a></td></tr>');

                        return true;
                    });
                }
            });
        }

        //$( ".editInterviewSetRow" ).click(function() {
        function editInterviewSetRow(interviewSetId) {
            $( '#interviewsetfrm' ).each(function(){
                this.reset();
            });
            //var interviewSetIdValue = $(this).attr('id');
            //var interviewSetId = interviewSetIdValue.substr(interviewSetIdValue.indexOf("-") + 1);
            $.post(siteUrl + 'interviews/getRecruiterInterviewSet', {interviewSetId: interviewSetId}, function (data) {
                //alert(data);
                var obj = $.parseJSON(data);
                var interview = obj.interview;
                //alert(interview.type);
                var questions = obj.questions;
                var fjQuestion = obj.fjQuestion;
                var fjVideoQuestion = obj.fjVideoQuestion;
                var fjTextQuestion = obj.fjTextQuestion;
                var intereviewSetEditId = obj.intereviewSetEditId;
                //alert(interview.type);
                $("#interviewsetfrm input[name=name]").val(interview.name);
                $("#interviewsetfrm input[name=intereviewSetEditId]").val(intereviewSetEditId);
                $("#interviewsetfrm input[name=intereviewSetEditType]").val(interview.type);

                $("#type option[value='"+interview.type+"']").prop("selected", "selected");

                if(interview.type != null || interview.type != '2') {
                    $("#interviewsetfrm #type").attr("disabled", "disabled");
                } else if(interview.type == '2') {
                    $('#interviewsetfrm #type').prop("disabled", false);
                }

                $("#interviewBreadcrumbText" ).text(' -> Edit Interview Set');
                $( "#manageInterviewsSetsTables" ).hide();
                $( "#addInterviewSetDiv" ).show();
                $( "#editQuestionFormDiv" ).hide();

                var contentHtml = "";
                for(var i = 0; i < questions.length; i++) {

                    var duration = questions[i].duration;
                    var file = questions[i].file;

                    if(i==0) {
                        contentHtml = contentHtml + '<div style="float:right; padding-right: 56px;"><a href="javascript:" data-toggle="modal" data-target="#selectQuestionType"><img src="<?php echo base_url(); ?>/theme/firstJob/image/plus.png" class="pad_rg_lfs add-more-question"></a></div>';
                    }

                    if(i==0) {
                        var questionLabelText = 'Questions';
                    } else {
                        var questionLabelText = '';
                    }
                    contentHtml = contentHtml + '<div class="form-group" style="margin-left: 0px !important; margin-right: 0px !important;"><label for="inputPassword" class="control-label col-xs-2">'+ questionLabelText +'</label>';

                    contentHtml = contentHtml + '<div class="col-xs-4 ques-title questionOnTypeBlock" id="questions11"><label for="inputPassword" class="control-label frm_in_padd required-field">Question Title</label><select data-placeholder="Type a question" style="width: 350px; display: none;" class="chosen-select sel_sty_padd interviewQuestion" tabindex="-1" name="question['+i+'][title]"><option value="'+questions[i].questionId+'">'+questions[i].title+'</option>';

                    for(var j = 0; j < fjQuestion.length; j++) {
                        if(fjQuestion[j].id != questions[i].questionId) {
                            contentHtml = contentHtml + '<option value="'+fjQuestion[j].id+'">'+fjQuestion[j].title+'</option>';
                        }
                    }

                    contentHtml = contentHtml + '</select><input type="text" data-validation="interviewQuestion" style="height: 0px; width: 0px; visibility: hidden; " /></div>';
                    $("#type").trigger('onchange');



                    if(interview.type != 3) {
                        contentHtml = contentHtml + '<div class="col-xs-3 file-section"><div class="col-md-12"><label for="inputPassword" class="control-label frm_in_padd required-field" style="font-size: 12px;">Upload File(mp3, mp4, mpeg, wav, etc)</label></div><div class="col-xs-8"><input type="text" class="form-control file-name interviewFileNames" name="input_file_'+i+'" id="filetextboxforimage1'+(i+1)+'" value="'+file+'" readonly="readonly"/><input type="hidden" value="'+file+'" name="filename_'+i+'" id="questionFileUpload"/></div><div class="col-xs-4 no_padding browse-section" id="filebrowseforimage1'+(i+1)+'"><button type="button" class="btn_upload">BROWSE</button><input type="file" name="file_'+i+'" class="question-file interviewFile" /><input type="text" data-validation="interviewFile" style="height: 0px; width: 0px; visibility: hidden; " /></div><br/><div class="question-video"><div id="questionVideo1'+(i+1)+'" class="question-video questionvideoshowblock" ></div></div></div>';
                    }

                    contentHtml = contentHtml + '<div class="col-xs-3"><label for="inputPassword" class="control-label frm_in_padd col-xs-12 required-field">Answer Duration <span style="font-size:10px;">(Limit 600 Sec)</span></label><div class="col-md-6 no_padding"><input type="number" min="1" class="form-control questiondurationtogetid interviewDuration" placeholder="" id="durations1'+(i+1)+'" name="question['+i+'][duration]" value="'+duration+'" maxlength="3"><input type="text" data-validation="interviewDuration" style="height: 0px; width: 0px; visibility: hidden; " /></div><div class="col-md-4"><p class="padd_txtmin">Sec</p></div><div class="col-md-2" style="padding: 0px !important;"><button type="button" class="del-question1 pull-right" value="testing" style="float: left !important; width: auto;">x</button></div></div></div>';
                }

                $('#questionList3').html(contentHtml);
                var config = {
                    '.chosen-select': {},
                    '.chosen-select-deselect': {allow_single_deselect: true},
                    '.chosen-select-no-single': {disable_search_threshold: 10},
                    '.chosen-select-no-results': {no_results_text: ''},
                    '.chosen-select-width': {width: "95%"}
                }
                for (var selector in config) {
                    $(selector).chosen(config[selector]);
                }
            });
        }

        function editInterviewQuestionRow(questionId) {
        //$( ".editInterviewQuestionRow" ).click(function() {
            $( '#editQuestionForm' ).each(function(){
                this.reset();
            });
            //var questionIdValue = $(this).attr('id');
            //var questionId = questionIdValue.substr(questionIdValue.indexOf("-") + 1);
            $.post(siteUrl + 'interviews/getQuestionDetails', {questionId: questionId}, function (data) {
                var obj = $.parseJSON(data);
                var question = obj.question;
        
                $( "#interviewBreadcrumbText" ).text(' -> My Question Bank');

                $( '<div class="pull-left addInterviewQuestionBreadcrumb"><h4 class="modal-title"> -> Edit Question</h4></div>' ).insertAfter( ".interviewBreadcrumbText" );

                $( "#manageInterviewsSetsTables" ).hide();
                $( "#manageInterviewMyQuestionDiv" ).hide();
                $( "#manageInterviewFjQuestionBankDiv" ).hide();
                $( "#addInterviewSetDiv" ).hide();
                $( "#questionBankAddQuestionDiv" ).hide();
                $( "#editQuestionFormDiv" ).show();

                $("#editQuestionForm input[name=editQuestionTitle]").val(question.title);
                $("#editQuestionForm input[name=editQuestionDuration]").val(question.duration);
                $("#editQuestionForm input[name=questionId]").val(question.id);
                $("#editQuestionForm input[name=editQuestionType]").val(question.type);
                
                $("#editQuestionType option[value='"+question.type+"']").prop("selected", "selected");

                if(question.type != null || question.type != 2) {
                    $("#editQuestionType").attr("disabled", "disabled");

                    if(question.type==1) {
                        $( "#fileselection" ).show();
                        $("#editQuestionForm input[name=fileSelectionName]").val(question.file);
                    } else {
                        $( "#fileselection" ).hide();
                    }
                } else if(interview.type == 2) {
                    $('#editQuestionType').prop("disabled", false);
                }
                
            });
        };

        $(document).on('blur', '#questionList3 .chosen-search input', function (evt) {
            if ($('.no-results').length == 1) {
                $(this).parents('.ques-title').find('.chosen-select').append('<option>' + $(evt.target).val() + '</option>');
                $(this).parents('.ques-title').find('.chosen-select').val($(evt.target).val());
                $(this).parents('.ques-title').find('.chosen-select').trigger('chosen:updated');
            } else {
                $("#questionList3 .chosen-select").chosen({
                    width: "300px",
                }).change(function (event)
                {
                    if (event.target)
                    {
                        var questionValueId = this.value;
                        var indexFileNameValue = $(this).parents('.form-group').find('.file-name').attr('id');
                        var indexDurationValue = $(this).parents('.form-group').find('.questiondurationtogetid').attr('id');
                        var indexShowVideoValue = $(this).parents('.form-group').find('.questionvideoshowblock').attr('id');

                        firstBlockForQuestionDetails(questionValueId, indexFileNameValue, indexDurationValue, indexShowVideoValue);
                    }
                });
            }
        });

        $(document).on('blur', '#questionList1 .chosen-search input', function (evt) {
            if ($('.no-results').length == 1) {
                $(this).parents('.ques-title1').find('.chosen-select1').append('<option>' + $(evt.target).val() + '</option>');
                $(this).parents('.ques-title1').find('.chosen-select1').val($(evt.target).val());
                $(this).parents('.ques-title1').find('.chosen-select1').trigger('chosen:updated');

            } else {

                $("#questionList1 .chosen-select1").chosen({
                    width: "300px",
                }).change(function (event)
                {
                    if (event.target)
                    {
                        var questionValueId = this.value;
                        var indexFileNameValue = $(this).parents('.form-group').find('.file-name1').attr('id');
                        var indexDurationValue = $(this).parents('.form-group').find('.questiondurationtogetid1').attr('id');
                        var indexShowVideoValue = $(this).parents('.form-group').find('.questionvideoshowblock1').attr('id');

                        firstBlockForQuestionDetails(questionValueId, indexFileNameValue, indexDurationValue, indexShowVideoValue);
                    }
                });
            }

        });

        $(document).on('blur', '#questionList2 .chosen-search input', function (evt) {
            if ($('.no-results').length == 1) {
                $(this).parents('.ques-title2').find('.chosen-select2').append('<option>' + $(evt.target).val() + '</option>');
                $(this).parents('.ques-title2').find('.chosen-select2').val($(evt.target).val());
                $(this).parents('.ques-title2').find('.chosen-select2').trigger('chosen:updated');

            } else {
                $("#questionList2 .chosen-select2").chosen({
                    width: "300px",
                }).change(function (event)
                {
                    if (event.target)
                    {
                        var questionValueId = this.value;
                        var indexDurationValue = $(this).parents('.form-group').find('.questiondurationtogetid2').attr('id');

                        firstBlockForQuestionDetails(questionValueId, '', indexDurationValue, '');
                    }
                });
            }

        });


        function firstBlockForQuestionDetails(questionValueId, indexFileNameValue, indexDurationValue, indexShowVideoValue) {

            if ((questionValueId != "" && indexFileNameValue != "" && indexDurationValue != "") || (questionValueId != "" && indexDurationValue != "")) {
                formData = {questionValueId: questionValueId, indexFileNameValue: indexFileNameValue, indexDurationValue: indexDurationValue};

                $.ajax({
                    url: siteUrl + "interviews/getQuestionDetailsByQuestionId",
                    type: "POST",
                    data: formData,
                    cache: false,
                    success: function (result)
                    {
                        $("#" + indexDurationValue).val('');
                        $("#" + indexFileNameValue).val('');
                
                        if (result.trim() != "NotDone" || result.trim() == "") {

                            var resultArr = result.split('####');

                            if (resultArr[0].trim() != "" && resultArr[0].trim() != "NotVideo") {

                                $("#" + indexFileNameValue).val(resultArr[0].trim());

                                if (resultArr[1].trim() != "" && resultArr[1].trim() != "NotType") {
                                    if (resultArr[1].trim() == "1") {
                                        var videotype = "video";
                                    } else {
                                        var videotype = "audio";
                                    }

                                    var videoUrl = "uploads/questions/" + videotype + "/" + resultArr[0].trim();
                                    jwVideo(videoUrl, indexShowVideoValue);
                                }

                            }

                            if (resultArr[2].trim() != "" && resultArr[2].trim() != "NotDuration") {

                                $("#" + indexDurationValue).val(resultArr[2]);
                            }
                        }
                        //data - response from server
                    }
                });
            }
        }

        $(document).on('blur', '.chosen-search input', function (evt) {
            if ($('.no-results').length == 1) {
                $(this).parents('.ques-title3').find('.chosen-select3').append('<option>' + $(evt.target).val() + '</option>');
                $(this).parents('.ques-title3').find('.chosen-select3').val($(evt.target).val());
                $(this).parents('.ques-title3').find('.chosen-select3').trigger('chosen:updated');

            }

        });

        $(document).on('blur', '.chosen-search input', function (evt) {
            if ($('.no-results').length == 1) {
                $(this).parents('.ques-title4').find('.chosen-select4').append('<option>' + $(evt.target).val() + '</option>');
                $(this).parents('.ques-title4').find('.chosen-select4').val($(evt.target).val());
                $(this).parents('.ques-title4').find('.chosen-select4').trigger('chosen:updated');

            }

        });

        //DOM loaded 
        $(document).ready(function () {
            //class for using to get radio button id
            $('.question-for-radio').click(function () {

                var selectedVal = "";
                var selected = $("input[type='radio'][name='environment']:checked");
                if (selected.length > 0) {
                    selectedVal = selected.val();
                }

                if (selectedVal == 1)
                {
                    var selectedfirstdropdownvalue = $('#type').val();

                    $("#blockInterviewSection1").html('');

                    var selecthtml = "<select class=\"selectpicker\" name=\"type\" id=\"type\">\n\
                                        <option value=\"0\">Select</option>\n\
                                        <option " + (selectedfirstdropdownvalue != "" && selectedfirstdropdownvalue == 1 ? " selected=\"\"" : "") + " value=\"1\">Video</option>\n\
                                        <option value=\"2\">Audio</option>\n\
                                        <option value=\"3\">Text</option>\n\
                                      </select>";
                    // hide industry drop down div
                    $("#industryDivId").hide();
                    $("#practiceSetBlock").hide();
                    $("#blockInterviewSection1").html(selecthtml);
                } else if (selectedVal == 2) {
                    //$("#type").trigger('onchange');
                    firstBlockQuestion();
                    secondBlockQuestion();
                    thirdBlockQuestion();
                    $("#blockInterviewSection1").html('');
                    var selecthtml = "<select class=\"selectpicker\" name=\"type\" id=\"type\">\n\
                                        <option selected=\"\" value=\"1\">Video</option>\n\
                                      </select>";
                    // show industry drop down div
                    $("#industryDivId").show();
                    $("#practiceSetBlock").show();
                    $("#blockInterviewSection1").html(selecthtml);

                }
            });
        });

       
        $('#type').on('change', function () {
            var questionTypeId = $(this).val();


            if (questionTypeId != "") {
                // show industry drop down div
                formData = {questionTypeId: questionTypeId}; //Array 

                $.ajax({
                    url: siteUrl + "interviews/getQuestionOnTypeFunction",
                    type: "POST",
                    data: formData,
                    cache: false,
                    success: function (result)
                    {

                        if (result.trim() == "NotDone" || result.trim() == "") {
                            var stringquery = "<label for=\"inputPassword\" class=\"control-label frm_in_padd\">Question Title</label><select data-placeholder=\"Type a question\" style=\"width: 350px; display: none;\" class=\"chosen-select sel_sty_padd\" tabindex=\"-1\" name=\"question[0][title]\"><option value=\"\"></option></select>";

                            $(".questionOnTypeBlock").html(stringquery);

                        } else {
                            $(".questionOnTypeBlock").html(result);

                        }

                        for (var selector in config) {
                            $(selector).chosen(config[selector]);
                        }
                        //data - response from server
                    }
                });
            }
        });

        function firstBlockQuestion() {

            var questionTypeId = 1;

            if (questionTypeId != "") {
                // show industry drop down div
                formData = {questionTypeId: questionTypeId}; //Array 

                $.ajax({
                    url: siteUrl + "interviews/getQuestionOnTypeFunction",
                    type: "POST",
                    data: formData,
                    cache: false,
                    success: function (result)
                    {
                        if (result.trim() == "NotDone" || result.trim() == "") {
                            var stringquery = "<label for=\"inputPassword\" class=\"control-label frm_in_padd\">Question Title</label><select data-placeholder=\"Type a question\" style=\"width: 350px; display: none;\" class=\"chosen-select sel_sty_padd\" tabindex=\"-1\" name=\"question[0][title]\"><option value=\"\"></option></select>";

                            $(".questionOnTypeBlock").html(stringquery);

                        } else {
                            $(".questionOnTypeBlock").html(result);

                        }

                        for (var selector in config) {
                            $(selector).chosen(config[selector]);
                        }
                        //data - response from server
                    }
                });
            }
        }

        function secondBlockQuestion() {
            var questionTypeId = 2;
            if (questionTypeId != "") {
                // show industry drop down div
                formData = {questionTypeId: questionTypeId}; //Array 

                $.ajax({
                    url: siteUrl + "interviews/getQuestionOnTypeFirstFunction",
                    type: "POST",
                    data: formData,
                    cache: false,
                    success: function (result)
                    {
                        if (result.trim() == "NotDone" || result.trim() == "") {
                            var stringquery = "<label for=\"inputPassword\" class=\"control-label frm_in_padd\">Question Title</label><select data-placeholder=\"Type a question\" style=\"width: 350px; display: none;\" class=\"chosen-select1 sel_sty_padd\" tabindex=\"-1\" name=\"question1[0][title]\"><option value=\"\"></option></select>";

                            $(".questionOnTypeBlock1").html(stringquery);

                        } else {
                            $(".questionOnTypeBlock1").html(result);
                        }

                        var config1 = {
                            '.chosen-select1': {},
                            '.chosen-select1-deselect': {allow_single_deselect: true},
                            '.chosen-select1-no-single': {disable_search_threshold: 10},
                            '.chosen-select1-no-results': {no_results_text: ''},
                            '.chosen-select1-width': {width: "95%"}
                        }
                        for (var selector in config1) {
                            $(selector).chosen(config1[selector]);
                        }
                        //data - response from server
                    }
                });
            }
        }

        function thirdBlockQuestion() {
            var questionTypeId = 3;

            if (questionTypeId != "") {
                // show industry drop down div
                formData = {questionTypeId: questionTypeId}; //Array 

                $.ajax({
                    url: siteUrl + "interviews/getQuestionOnTypeSecondFunction",
                    type: "POST",
                    data: formData,
                    cache: false,
                    success: function (result)
                    {

                        if (result.trim() == "NotDone" || result.trim() == "") {
                            var stringquery = "<label for=\"inputPassword\" class=\"control-label frm_in_padd\">Question Title</label><select data-placeholder=\"Type a question\" style=\"width: 350px; display: none;\" class=\"chosen-select2 sel_sty_padd\" tabindex=\"-1\" name=\"question2[0][title]\"><option value=\"\"></option></select>";
                            2 //alert(stringquery);
                            $(".questionOnTypeBlock2").html(stringquery);

                        } else {
                            $(".questionOnTypeBlock2").html(result);
                        }

                        var config2 = {
                            '.chosen-select2': {},
                            '.chosen-select2-deselect': {allow_single_deselect: true},
                            '.chosen-select2-no-single': {disable_search_threshold: 10},
                            '.chosen-select2-no-results': {no_results_text: ''},
                            '.chosen-select2-width': {width: "95%"}
                        }
                        for (var selector in config2) {
                            $(selector).chosen(config2[selector]);
                        }
                        //data - response from server
                    }
                });
            }
        }

        $(function () {
            var url = '<?php echo base_url(); ?>';
            window.siteUrl = url;
            window.token = '<?php echo $token; ?>';
        });

        /*$('#confirmbefore_submit').on('click', function () {

            var setNameValue = $('#setname_value').val();
            if (setNameValue != "") {

                bootbox.confirm("Are you sure you want to add this question to the Interview Set '" + setNameValue + "'?", function (result) {

                    if (result) {
                        $("form[name='interviewsetfrm']").submit();
                    } else {
                        return true;
                    }
                });
                return false;
            } else {
                return true;
            }
        });*/


    /* End of Interview Set Moduel scripts*/

    //Add Assessment Sets script
    $( "#addAssessmentSetDiv" ).hide();
    $( "#editAssessmentSetDiv" ).hide();
    $( "#manageMyQuestionDiv" ).hide();
    $( "#addQuestionBankDiv" ).hide();
    $( "#manageAssessmentFjQuestionBankDiv" ).hide();
    $( "#assessmentQuestionBankAddQuestionDiv" ).hide();

    $( "#breadcrumbText" ).click(function() {
        var breadcrumbText = $.trim($(this).text().replace(/[\.,:->-]+/g, ""));
        if(breadcrumbText == 'Add Assessment Set') {
            $( '#assessmentForm' ).each(function(){
            this.reset();
            });
            $( '#editAssessmentForm' ).each(function(){
                this.reset();
            });
            $( "#breadcrumbText" ).text(' -> My Question Banks');
            $( ".addQuestionBankBreadcrumb" ).remove();
            $( "#addAssessmentSetDiv" ).show();
            $( "#manageInterviewSetTables" ).hide();
            $( "#manageMyQuestionDiv" ).hide();
            $( "#addQuestionBankDiv" ).hide();
            $( "#assessmentQuestionBankAddQuestionDiv" ).hide();
            $( "#manageAssessmentFjQuestionBankDiv" ).hide();
        } else if(breadcrumbText == 'My Question Banks') {
            $( '#assessmentForm' ).each(function(){
                this.reset();
            });

            $( '#editAssessmentForm' ).each(function(){
                this.reset();
            });

            $( '#addQuestionBankForm' ).each(function(){
                this.reset();
            });

            $( ".addQuestionBankBreadcrumb" ).remove();
            $( "#addAssessmentSetDiv" ).hide();
            $( "#manageInterviewSetTables" ).hide();
            $( "#manageMyQuestionDiv" ).show();
            $( "#addQuestionBankDiv" ).hide();
            $( "#manageAssessmentFjQuestionBankDiv" ).hide();
            $( "#assessmentQuestionBankAddQuestionDiv" ).hide();
        }
    });

    $( ".assessmentBackButtonL2" ).click(function() {
        $( '#assessmentForm' ).each(function(){
                this.reset();
        });

        $( '#editAssessmentForm' ).each(function(){
            this.reset();
        });

        $( '#addQuestionBankForm' ).each(function(){
            this.reset();
        });

        $( ".addQuestionBankBreadcrumb" ).remove();
        $( "#addAssessmentSetDiv" ).hide();
        $( "#manageInterviewSetTables" ).hide();
        $( "#manageMyQuestionDiv" ).show();
        $( "#addQuestionBankDiv" ).hide();
        $( "#manageAssessmentFjQuestionBankDiv" ).hide();
        $( "#assessmentQuestionBankAddQuestionDiv" ).hide();
    });

    $( "#addAssessmentSetButton" ).click(function() {
        $( '#assessmentForm' ).each(function(){
            this.reset();
        });
        $( '#editAssessmentForm' ).each(function(){
            this.reset();
        });
        $( "#breadcrumbText" ).text(' -> Add Assessment Set');
        $( ".addQuestionBankBreadcrumb" ).remove();
        $( "#addAssessmentSetDiv" ).show();
        $( "#manageInterviewSetTables" ).hide();
        $( "#manageMyQuestionDiv" ).hide();
        $( "#addQuestionBankDiv" ).hide();
        $( "#manageAssessmentFjQuestionBankDiv" ).hide();
        $( "#assessmentQuestionBankAddQuestionDiv" ).hide();
    });

    $( "#addQuestionBankButton" ).click(function() {
        
        $( '#assessmentForm' ).each(function(){
            this.reset();
        });

        $('#assessmenQquestionType').removeAttr("disabled");

        $( '#addAssessmnetQuestionBankForm' ).each(function(){
            this.reset();
        });

        $( '#addQuestionBankForm' ).each(function(){
            this.reset();
        });

        $( '#editAssessmentForm' ).each(function(){
            this.reset();
        });

        $("#addAssessmnetQuestionBankForm input[name=hiddenQuestionBankId]").val('');
        $("#addAssessmnetQuestionBankForm input[name=hiddenQuestionBankType]").val('');

        $( '<div class="pull-left addQuestionBankBreadcrumb"><h4 class="modal-title"> -> Add Question Bank</h4></div>' ).insertAfter( ".breadcrumbText" );

        $( "#addAssessmentSetDiv" ).hide();
        $( "#manageInterviewSetTables" ).hide();
        $( "#manageMyQuestionDiv" ).hide();
        $( "#addQuestionBankDiv" ).show();
        $( "#manageAssessmentFjQuestionBankDiv" ).hide();
        $( "#assessmentQuestionBankAddQuestionDiv" ).hide();
    });

    //$( ".addAssessmentQuestionButton" ).click(function() {
    function addAssessmentQuestionButton(questionbankid, questionbanktype, questionbanktitle) {
        $( '#questionassessmentForm' ).each(function(){
            this.reset();
        });


        $( '#assessmentForm' ).each(function(){
            this.reset();
        });

        $( '#addQuestionBankForm' ).each(function(){
            this.reset();
        });

        $( '#editAssessmentForm' ).each(function(){
            this.reset();
        });

        $('#questionFormType').removeAttr("disabled");

        //var questionbankid = $(this).data('questionbankid');

        //var questionbanktype = $(this).data('questionbanktype');

        //var questionbanktitle = $(this).data('questionbanktitle');

        $("#questionassessmentForm input[name=questionBankId]").val(questionbankid);
        $("#questionassessmentForm input[name=hiddenType]").val(questionbanktype);
        $("#questionassessmentForm input[name=questionId]").val('');

        $("#questionFormType option[value='"+questionbanktype+"']").prop("selected", "selected");
        $("#questionFormType").attr("disabled", "disabled");

        if(questionbanktype == 1) {
            $( ".assessmentQuestionFileDiv" ).show();
        } else {
          $( ".assessmentQuestionFileDiv" ).hide();  
        }
        $( '<div class="pull-left addQuestionBankBreadcrumb"><h4 class="modal-title"> -> Add Question To <b>'+questionbanktitle+'</b> </h4></div>' ).insertAfter( ".breadcrumbText" );

        $( "#addAssessmentSetDiv" ).hide();
        $( "#manageInterviewSetTables" ).hide();
        $( "#manageMyQuestionDiv" ).hide();
        $( "#addQuestionBankDiv" ).hide();
        $( "#manageAssessmentFjQuestionBankDiv" ).hide();
        $( "#assessmentQuestionBankAddQuestionDiv" ).show();
    }

    //$( ".assessmentQuestionBankQuestionRow" ).click(function() {

    function assessmentQuestionBankQuestionRow(questionid, questiontype, questionbanktitle, questionbankid) {
        $( '#questionassessmentForm' ).each(function(){
            this.reset();
        });

        $( '#assessmentForm' ).each(function(){
            this.reset();
        });

        $( '#addQuestionBankForm' ).each(function(){
            this.reset();
        });

        $( '#editAssessmentForm' ).each(function(){
            this.reset();
        });

        $('#questionFormType').removeAttr("disabled");

        //var questionbankid = $(this).data('questionbankid');
        //var questionbanktitle = $(this).data('questionbanktitle');
        //var questiontype = $(this).data('questiontype');
        //var questionid = $(this).data('questionid');

        $("#questionassessmentForm input[name=questionBankId]").val(questionbankid);
        $("#questionassessmentForm input[name=questionId]").val(questionid);
        $("#questionassessmentForm input[name=hiddenType]").val(questiontype);

        $.post(siteUrl + 'assessments/getQuestionDetails', {questionid: questionid}, function (data) {
            var obj = $.parseJSON(data);
            var options = obj.options;
            var assessment = obj.assessment;
            //var thisVal = "question[0]['title']";
            $("#questionassessmentForm #questionTitleId").val(assessment.title);
            $("#questionassessmentForm .assessmentQuestionFileName").val(assessment.file);
            $("#questionassessmentForm #quesitonQnswer1").val(options[0].option);
            $("#questionassessmentForm #quesitonQnswer2").val(options[1].option);
            $("#questionassessmentForm #quesitonQnswer3").val(options[2].option);
            $("#questionassessmentForm #quesitonQnswer4").val(options[3].option);

            if(options[0].isCorrect == 1){
                $("#questionassessmentForm #questionCheck1").attr('checked', 'checked');
            } else if(options[1].isCorrect == 1) {
                $("#questionassessmentForm #questionCheck2").attr('checked', 'checked');
            } else if(options[2].isCorrect == 1) {
                $("#questionassessmentForm #questionCheck3").attr('checked', 'checked');
            } else if(options[3].isCorrect == 1) {
                $("#questionassessmentForm #questionCheck4").attr('checked', 'checked');
            } 
            

            $("#questionFormType option[value='"+assessment.type+"']").prop("selected", "selected");
            $("#questionFormType").attr("disabled", "disabled");

            /*if(questionBankDetails.type != null || questionBankDetails.type != '2') {
                $("#assessmenQquestionType").attr("disabled", "disabled");
            } else if(questionBankDetails.type == '2') {
                $('#assessmenQquestionType').prop("disabled", false);
            }*/

        });


        if(questiontype == 1) {
            $( ".assessmentQuestionFileDiv" ).show();
        }
        $( '<div class="pull-left addQuestionBankBreadcrumb"><h4 class="modal-title"> -> Edit Question To <b>'+questionbanktitle+'</b> </h4></div>' ).insertAfter( ".breadcrumbText" );

        $( "#addAssessmentSetDiv" ).hide();
        $( "#manageInterviewSetTables" ).hide();
        $( "#manageMyQuestionDiv" ).hide();
        $( "#addQuestionBankDiv" ).hide();
        $( "#manageAssessmentFjQuestionBankDiv" ).hide();
        $( "#assessmentQuestionBankAddQuestionDiv" ).show();
    }
    //});

    $( "#manageAssessmentSetsTitle" ).click(function() {
        $( "#manageInterviewSetTables" ).show();
        $( "#addAssessmentSetDiv" ).hide();
        $( "#breadcrumbText" ).text('');
        $( ".addQuestionBankBreadcrumb" ).remove();
        $( "#editAssessmentSetDiv" ).hide();
        $( "#manageMyQuestionDiv" ).hide();
        $( "#addQuestionBankDiv" ).hide();
        $( "#manageAssessmentFjQuestionBankDiv" ).hide();
        $( "#assessmentQuestionBankAddQuestionDiv" ).hide();
    });

    $( ".assessmentBackButtonL1" ).click(function() {
        $( "#manageInterviewSetTables" ).show();
        $( "#addAssessmentSetDiv" ).hide();
        $( "#breadcrumbText" ).text('');
        $( ".addQuestionBankBreadcrumb" ).remove();
        $( "#editAssessmentSetDiv" ).hide();
        $( "#manageMyQuestionDiv" ).hide();
        $( "#addQuestionBankDiv" ).hide();
        $( "#manageAssessmentFjQuestionBankDiv" ).hide();
        $( "#assessmentQuestionBankAddQuestionDiv" ).hide();
    });

    $( "#resetAllAssessmetnModalContent" ).click(function() {
        $( "#manageInterviewSetTables" ).show();
        $( "#addAssessmentSetDiv" ).hide();
        $( "#breadcrumbText" ).text('');
        $( ".addQuestionBankBreadcrumb" ).remove();
        $( "#editAssessmentSetDiv" ).hide();
        $( "#manageMyQuestionDiv" ).hide();
        $( "#addQuestionBankDiv" ).hide();
        $( "#manageAssessmentFjQuestionBankDiv" ).hide();
        $( "#assessmentQuestionBankAddQuestionDiv" ).hide();
    });

    $( "#fjQuestionBankButton" ).click(function() {
        $( "#manageInterviewSetTables" ).hide();
        $( "#addAssessmentSetDiv" ).hide();
        $( "#breadcrumbText" ).text(' -> VH Question Banks');
        $( ".addQuestionBankBreadcrumb" ).remove();
        $( "#editAssessmentSetDiv" ).hide();
        $( "#manageMyQuestionDiv" ).hide();
        $( "#addQuestionBankDiv" ).hide();
        $( "#manageAssessmentFjQuestionBankDiv" ).show();
        $( "#assessmentQuestionBankAddQuestionDiv" ).hide();
    });

    $( "#myQuestionBankButton" ).click(function() {
        $( "#manageMyQuestionDiv" ).show();
        $( "#manageInterviewSetTables" ).hide();
        $( "#addAssessmentSetDiv" ).hide();
        $( "#breadcrumbText" ).text(' -> My Question Banks');
        /*$( '<div class="pull-left myQuestionBankBreadcrumb" style="cursor: pointer;"><h4 class="modal-title"> -> My Question Bank</h4></div>' ).insertAfter( ".breadcrumbText" );*/
        $( "#editAssessmentSetDiv" ).hide();
        $( "#addQuestionBankDiv" ).hide();
        $( "#manageAssessmentFjQuestionBankDiv" ).hide();
        $( "#assessmentQuestionBankAddQuestionDiv" ).hide();
    });

    $('#assessmenQquestionType').on('change', function() {
        var questionTypeId = $(this).val();
        
        // alert(questionTypeId);
        if( questionTypeId == 3 ) {
            $("#enableOnTextSelectBlockAssessment").show(); 
        } else {
            $("#enableOnTextSelectBlockAssessment").hide(); 
        }
    });

    //$( ".editAssessmentQuestionBank" ).click(function() {
    function editAssessmentQuestionBank(questionBankId) {
        //var assessmentQuestionBankIdValue = $(this).attr('id');
        //var questionBankId = assessmentQuestionBankIdValue.substr(assessmentQuestionBankIdValue.indexOf("-") + 1);

        $( '#assessmentForm' ).each(function(){
            this.reset();
        });

        $('#assessmenQquestionType').prop("disabled", false);

        $( '#addAssessmnetQuestionBankForm' ).each(function(){
            this.reset();
        });

        $( '#addQuestionBankForm' ).each(function(){
            this.reset();
        });

        $( '#editAssessmentForm' ).each(function(){
            this.reset();
        });

        $( '<div class="pull-left addQuestionBankBreadcrumb"><h4 class="modal-title"> -> Edit Question Bank</h4></div>' ).insertAfter( ".breadcrumbText" );

        $( "#addAssessmentSetDiv" ).hide();
        $( "#manageInterviewSetTables" ).hide();
        $( "#manageMyQuestionDiv" ).hide();

        $.post(siteUrl + 'assessments/getQuestionBankDetails', {questionBankId: questionBankId}, function (data) {
            var obj = $.parseJSON(data);
            var questionBankDetails = obj.questionBank;
            $("#addAssessmnetQuestionBankForm input[name=assessmentQuestionBankname]").val(questionBankDetails.name);
            $("#addAssessmnetQuestionBankForm input[name=hiddenQuestionBankId]").val(questionBankDetails.id);
            $("#addAssessmnetQuestionBankForm input[name=hiddenQuestionBankType]").val(questionBankDetails.type);

            
            if(questionBankDetails.type == '') {
                $('#assessmenQquestionType').prop("disabled", false);
            } else {
                $("#assessmenQquestionType option[value='"+questionBankDetails.type+"']").prop("selected", "selected");
                $("#assessmenQquestionType").attr("disabled", "disabled");
            }

            if(questionBankDetails.type == 3) {
                $( "#enableOnTextSelectBlockAssessment" ).show();
            } else {
                $( "#enableOnTextSelectBlockAssessment" ).hide();
            }

            $( "#addQuestionBankDiv" ).show();
            $( "#manageAssessmentFjQuestionBankDiv" ).hide();

        });
    }

    function assessmentsetEditFunction(assessmentId) {
        $( '#editAssessmentForm' ).each(function(){
            this.reset();
        });
        $('#questionListEdit').html('');
    //$( ".assessmentsetEdit" ).click(function() {
        //var assessmentIdValue = $(this).attr('id');
        //var assessmentId = assessmentIdValue.substr(assessmentIdValue.indexOf("-") + 1);
        $.post(siteUrl + 'jobs/editAssessment', {assessmentId: assessmentId}, function (data) {
            var obj = $.parseJSON(data);
            var assessmentQuestions = obj.assessmentQuestions.individual;
            var assessmentRandomQuestions = obj.assessmentQuestions.random;
            var questionBanks = obj.questionBanks;
            var assessment = obj.assessment;
            //var duration = obj.assessment.duration;
            var duration = Math.ceil(assessment.duration /60);
            $("#editAssessmentForm input[name=name]").val(assessment.name);
            $("#editAssessmentForm input[name=duration]").val(duration);
            $("#editAssessmentForm input[name=editAssessemntId]").val(obj.assessmentEditId);

            $("#breadcrumbText" ).text(' -> Edit Assessment Set');
            $( "#addAssessmentSetDiv" ).hide();
            $( "#manageInterviewSetTables" ).hide();

            $( "#editAssessmentSetDiv" ).show();

            var contentHtml = "";

            if(assessmentRandomQuestions.length > 0) {
                for(var i = 0; i < assessmentRandomQuestions.length; i++) {
                    
                    //console.log("Type: " + item.data[i].setID + " Name: " + item.data[i].setID + " Account: " + item.data[i].setID);
                    var value = assessmentRandomQuestions[i].questionBank;
                    contentHtml = contentHtml + '<div class="form-group"><div class="col-xs-2 rsp_width_100"><label for="inputPassword" class="control-label no_padding_left">Type : </label><div class="clearfix"></div><span>Random</span></div><div class="col-xs-4 rsp_width_100"><label for="inputPassword" class="control-label no_padding_left">Question Bank</label><div class="clearfix"></div><select class="selectpicker assessmentBank2" name="question['+i+'][questionBank]"><?php foreach($questionBanks as $item) { ?><option value="<?php echo $item['id'];?>" ';
                    var forLoopData = "<?php echo $item['id'] ?>";
                    //var noOfQuestionsInQuestionBank = '<?php //echo count(getQuestionOfQuestionBank($item['id'])) ?>';

                    //Getting Quetion number of question Banks
                        var formData1 = {questionBankId: forLoopData};
                        var noOfQuestionsInQuestionBank = "";
                        //alert(j);
                        $.ajax({
                            url: siteUrl + "assessments/getQuestionOfQuestionBankCount",
                            type: "POST",
                            data: formData1,
                            cache: false,
                            success: function (result1)
                            {
                                var optionObjs = $.parseJSON(result1);
                                noOfQuestionsInQuestionBank = optionObjs.questionCount;
                            },
                            async: false
                        });

                    //End of getting Question number of question Banks

                    var selected = "";
                    if(forLoopData == value){
                        selected = 'selected';
                    }

                    contentHtml = contentHtml + selected+'><?php echo $item['name'];?>('+noOfQuestionsInQuestionBank+')</option><?php } ?></select><input type="text" data-validation="assessmentQuestionEdit" style="height: 0px; width: 0px; visibility: hidden; " /></div><div class="col-xs-5 rsp_width_100"><label for="inputPassword" class="control-label no_padding_left">Number of Question</label><div class="clearfix"></div><input type="text" class="form-control assessmentQuestionEdit" placeholder=""  name="question['+i+'][number]" value="'+assessmentRandomQuestions[i].totalQuestion+'"><input type="text" data-validation="assessmentQuestionEdit" style="height: 0px; width: 0px; visibility: hidden; " /></div><div class="col-xs-1"><button type="button" class="del-assessment" value="testing">x</button></div></div>';
                }
            }

            if(assessmentQuestions.length > 0) {
                var counter = 0;
                for(var j = 0; j < assessmentQuestions.length; j++) {
                    var value1 = assessmentQuestions[j].questionBank;
                    var value2 = assessmentQuestions[j].questionId;
                    contentHtml = contentHtml + '<div class="form-group"><div class="col-xs-2 rsp_width_100"><label for="inputPassword" class="control-label no_padding_left">Type : </label><div class="clearfix"></div><span>Individual</span></div><div class="col-xs-4 rsp_width_100"><label for="inputPassword" class="control-label no_padding_left">Question Bank</label><div class="clearfix"></div><select class="selectpicker assessmentBank3" id="editAssessmentIndividualQuestionBank-'+j+'" name="question['+j+'][questionBank1]"><?php foreach($questionBanks as $item):?><option value="<?php echo $item['id'];?>" ';
                    var forLoopData = "<?php echo $item['id'] ?>";
                    var selected = "";
                    if(forLoopData == value1){
                        selected = 'selected';
                    }

                    contentHtml = contentHtml + selected+'><?php echo $item['name'];?></option><?php endforeach;?></select><input type="text" data-validation="assessmentQuestionEdit" style="height: 0px; width: 0px; visibility: hidden; " /></div><div class="col-xs-5 rsp_width_100"><label for="inputPassword" class="control-label no_padding_left">Question</label><div class="clearfix"></div><select class="selectpicker assessmentIndividualQuestionEdit" id="editAssessmentIndividualQuestion-'+j+'" name="question['+j+'][question1]"><option value="">-Select Question-</option></select><input type="text" data-validation="assessmentQuestionEdit" style="height: 0px; width: 0px; visibility: hidden; " /></div><div class="col-xs-1"><button type="button" class="del-assessment" value="testing">x</button></div></div>';

                     //Getting Quetion of question Banks
                        var formData = {questionBankId: value1, selectedQuestion:value2};
                        var questionBankOptions = "";
                        //alert(j);
                        $.ajax({
                            url: siteUrl + "assessments/getQuestionOfQuestionBank",
                            type: "POST",
                            data: formData,
                            cache: false,
                            success: function (result)
                            {
                                var optionObj = $.parseJSON(result);
                                var optionQuestions = optionObj.questions;
                                var selectedQuestion = optionObj.selectedQuestion;
                                //alert(selectedQuestion);
                                for(var k = 0; k < optionQuestions.length; k++) {
                                    var selected1 = "";
                                    if(optionQuestions[k].id == selectedQuestion){
                                        selected1 = 'selected';
                                    } else {
                                        selected1 = '';
                                    }
                                    questionBankOptions = questionBankOptions + '<option '+selected1+' value="'+optionQuestions[k].id+'">'+optionQuestions[k].title+'</option>';
                                }

                                $('#editAssessmentIndividualQuestion-'+counter).append(questionBankOptions);
                                counter++;
                                questionBankOptions = "";
                            }
                        });
                    //End of getting Question of question Banks
                }
            }
            $('#questionListEdit').html(contentHtml);
        });
    };
    
    $(document).on('change', '.assessmentBank1', function () {
        var questionBankId = $(this).val();
        var actualElementValues = $(this).attr('id');
        var actualElementId = actualElementValues.substr(actualElementValues.indexOf("-") + 1);

        if (questionBankId != "") {
            formData = {questionBankId: questionBankId}; //Array 

            $.ajax({
                url: siteUrl + "assessments/getQuestionOfQuestionBank",
                type: "POST",
                data: formData,
                cache: false,
                success: function (result)
                {   
                    var questionBankOptions = '<option value="">-Selelct Question-</option>';
                    var obj = $.parseJSON(result);
                    var questions = obj.questions;
                    for(var i = 0; i < questions.length; i++) {
                        questionBankOptions = questionBankOptions + '<option value="'+questions[i].id+'">'+questions[i].title+'</option>';
                    }

                    $('#addAssessmentIndividualQuestion-'+actualElementId).find('option').remove();
                    $('#addAssessmentIndividualQuestion-'+actualElementId).append(questionBankOptions);
                }
            });
        }
    });

    $(document).on('change', '.assessmentBank3', function () {
        var questionBankId = $(this).val();
        var actualElementValues = $(this).attr('id');
        var actualElementId = actualElementValues.substr(actualElementValues.indexOf("-") + 1);

        if (questionBankId != "") {
            formData = {questionBankId: questionBankId}; //Array 

            $.ajax({
                url: siteUrl + "assessments/getQuestionOfQuestionBank",
                type: "POST",
                data: formData,
                cache: false,
                success: function (result)
                {   
                    var questionBankOptions = '<option value="">-Selelct Question-</option>';
                    var obj = $.parseJSON(result);
                    var questions = obj.questions;
                    for(var i = 0; i < questions.length; i++) {
                        questionBankOptions = questionBankOptions + '<option value="'+questions[i].id+'">'+questions[i].title+'</option>';
                    }

                    $('#editAssessmentIndividualQuestion-'+actualElementId).find('option').remove();
                    $('#editAssessmentIndividualQuestion-'+actualElementId).append(questionBankOptions);
                }
            });
        }
    });
    // Add validator For Interview Assessment
        $.formUtils.addValidator({
            name: 'assessmentQuestionEdit',
            validatorFunction: function (value, $el, config, language, $form) {      
                var hasNoValue;
                var hasNoValueIndividual;
                var durationExceed;
                var hasNoValueQuestionBank;
                var hasNoValueIndividualQuestionBank;

                $('.assessmentQuestionEdit').each(function(i) {
                    if ($(this).val() == '') {
                          hasNoValue = true;
                    }
                    else {
                        if (parseInt($(this).val())>600 || isNaN($(this).val())) {
                            durationExceed = true;
                        }
                    }
                });

                $('.assessmentIndividualQuestionEdit').each(function(i) {
                    if ($(this).val() == '') {
                          hasNoValueIndividual = true;
                    }
                });

                $('.assessmentBank2').each(function(i) {
                    if ($(this).val() == '') {
                          hasNoValueQuestionBank = true;
                    }
                });

                $('.assessmentBank3').each(function(i) {
                    if ($(this).val() == '') {
                          hasNoValueIndividualQuestionBank = true;
                    }
                });


                if ((hasNoValue==true || durationExceed==true || hasNoValueQuestionBank==true) && (hasNoValueIndividual==true || hasNoValueIndividualQuestionBank==true)) {
                    return false;
                }
                else {
                    return true;
                }
                //return parseInt(value, 10) % 2 === 0;
            },
            errorMessage : 'Fill complete field of either Random type question or Individual type question.'
        });

        $.formUtils.addValidator({
            name: 'assessmentQuestion',
            validatorFunction: function (value, $el, config, language, $form) {      
                var hasNoValue;
                var hasNoValueIndividual;
                var durationExceed;
                var hasNoValueQuestionBank;
                var hasNoValueIndividualQuestionBank;

                $('.assessmentQuestion').each(function(i) {
                    if ($(this).val() == '') {
                          hasNoValue = true;
                    }
                    else {
                        if (parseInt($(this).val())>600 || isNaN($(this).val())) {
                            durationExceed = true;
                        }
                    }
                });

                $('.assessmentIndividualQuestion').each(function(i) {
                    if ($(this).val() == '') {
                          hasNoValueIndividual = true;
                    }
                });

                $('.assessmentBank').each(function(i) {
                    if ($(this).val() == '') {
                          hasNoValueQuestionBank = true;
                    }
                });

                $('.assessmentBank1').each(function(i) {
                    if ($(this).val() == '') {
                          hasNoValueIndividualQuestionBank = true;
                    }
                });


                if ((hasNoValue==true || durationExceed==true || hasNoValueQuestionBank==true) && (hasNoValueIndividual==true || hasNoValueIndividualQuestionBank==true)) {
                    return false;
                }
                else {
                    return true;
                }
                //return parseInt(value, 10) % 2 === 0;
            },
            errorMessage : 'Fill complete field of either Random type question or Individual type question.'
        });

        // Add validator For Interview Assessment
        /*$.formUtils.addValidator({
            name: 'assessmentBank',
            validatorFunction: function (value, $el, config, language, $form) {      
                var hasNoValue;
                var hasNoValueIndividualQuestionBank;

                $('.assessmentBank').each(function(i) {
                    if ($(this).val() == '') {
                          hasNoValue = true;
                    }
                });

                $('.assessmentBank1').each(function(i) {
                    if ($(this).val() == '') {
                          hasNoValueIndividualQuestionBank = true;
                    }
                });

                if (hasNoValue || hasNoValueIndividualQuestionBank) {
                    return false;
                }
                else {
                    return true;
                }
                //return parseInt(value, 10) % 2 === 0;
            },
            errorMessage : 'Please select question bank.'
        });*/

        // Add validator For Interview Assessment

        $.validate({
            form: '#assessmentForm',
            modules: 'file',
            onError: function ($form) {
                //alert($('.level1').val());
                return false;
            },
            onSuccess: function ($form) {
                var allData = $("#assessmentForm").serialize();
                var interviewSetName = $("#assessmentForm input[name=name]").val();
                var interviewSetDuration = $("#assessmentForm input[name=duration]").val();
                $.ajax({
                    url: siteUrl + "jobs/addAssessment",
                    type: "POST",
                    data: allData,
                    success: function (data) {
                        var obj = $.parseJSON(data);
                        var assessmentId = obj.assessmentId;
                        var noOfQuestions = obj.noOfQuestions;
                        var errorMessage = obj.errorMessage;

                        //$('#assessmentAttach').prepend($("<option></option>").attr("value",assessmentId).text(interviewSetName));

                        if(errorMessage == '') {
                            $("#assessmentAttach option").eq(1).before($("<option></option>").val(assessmentId).text(interviewSetName));
                            $("#assessmentAttach").trigger("chosen:updated");

                            $('.assessmentSetTabel tbody').prepend('<tr id="tr_'+assessmentId+'"><td>'+interviewSetName+'</td><td>'+interviewSetDuration+' Minutes</td><td>'+noOfQuestions+'</td><td><a href="javascript:" onclick="assessmentsetEditFunction('+assessmentId+')" class="assessmentsetEdit"  id="assessmentsetEdit-'+assessmentId+'"><img class="padd_rgt" src="'+siteUrl+'theme/firstJob/image/edit.png"></a><a style="padding-left: 3px;" href="javascript:" class="delete-assessment-set" id="'+assessmentId+'"><img src="'+siteUrl+'theme/firstJob/image/del.png"></a></td></tr>');

                            $( "#manageInterviewSetTables" ).show();
                            $( "#addAssessmentSetDiv" ).hide();
                            $( "#breadcrumbText" ).text('');
                            $( "#editAssessmentSetDiv" ).hide();
                        } else {
                            alert(errorMessage);
                        }
                    }
                });
                return false;
            },
            validateOnBlur : false,
            errorMessagePosition : 'top',
            scrollToTopOnError : false // Set this property to true on longer forms
        });

        $.validate({
            form: '#editAssessmentForm',
            modules: 'file',
            onError: function ($form) {
                return false;
            },
            onSuccess: function ($form) {
                var allData = $("#editAssessmentForm").serialize();
                var interviewSetName = $("#editAssessmentForm input[name=name]").val();
                var interviewSetDuration = $("#editAssessmentForm input[name=duration]").val();
                var assessmentId = $("#editAssessmentForm input[name=editAssessemntId]").val();

                $.ajax({
                    url: siteUrl + "jobs/getAssessmentAttachedCount",
                    type: "POST",
                    data: {assessmentId: assessmentId},
                    success: function (count) {
                        if(count > 0) {
                            bootbox.confirm('Are you sure you want to edit this assessment set? Because this assessment is associated with other jobs!', function (resultthis) {
                                if (resultthis) {
                                    $.ajax({
                                        url: siteUrl + "jobs/updateAssessment",
                                        type: "POST",
                                        data: allData,
                                        success: function (data) {
                                            var obj = $.parseJSON(data);
                                            var assessmentId = obj.assessmentId;
                                            var noOfQuestions = obj.noOfQuestions;
                                          
                                            $( "#editAssessmentSetDiv" ).hide();
                                            $( "#manageInterviewSetTables" ).show();
                                            $( "#addAssessmentSetDiv" ).hide();
                                            $( "#breadcrumbText" ).text('');

                                            $('#assessmentAttach option[value="'+assessmentId+'"]').text(interviewSetName);
                                            $("#assessmentAttach").trigger("chosen:updated");

                                            $('.assessmentSetTabel #tr_' + assessmentId).find("td").eq(0).html(interviewSetName);
                                            $('.assessmentSetTabel #tr_' + assessmentId).find("td").eq(1).html(interviewSetDuration + ' Minutes');
                                            $('.assessmentSetTabel #tr_' + assessmentId).find("td").eq(2).html(noOfQuestions);
                                        }
                                    });
                                }
                            });
                        } else {
                            $.ajax({
                                url: siteUrl + "jobs/updateAssessment",
                                type: "POST",
                                data: allData,
                                success: function (data) {
                                    var obj = $.parseJSON(data);
                                    var assessmentId = obj.assessmentId;
                                    var noOfQuestions = obj.noOfQuestions;
                                  
                                    $( "#editAssessmentSetDiv" ).hide();
                                    $( "#manageInterviewSetTables" ).show();
                                    $( "#addAssessmentSetDiv" ).hide();
                                    $( "#breadcrumbText" ).text('');

                                    $('#assessmentAttach option[value="'+assessmentId+'"]').text(interviewSetName);
                                    $("#assessmentAttach").trigger("chosen:updated");

                                    $('.assessmentSetTabel #tr_' + assessmentId).find("td").eq(0).html(interviewSetName);
                                    $('.assessmentSetTabel #tr_' + assessmentId).find("td").eq(1).html(interviewSetDuration + ' Minutes');
                                    $('.assessmentSetTabel #tr_' + assessmentId).find("td").eq(2).html(noOfQuestions);
                                }
                            });
                        }
                    }
                });
                return false;
            },
            validateOnBlur : false,
            errorMessagePosition : 'top',
            scrollToTopOnError : false // Set this property to true on longer forms
        });

        $.validate({
            form: '#addAssessmnetQuestionBankForm',
            modules: 'file',
            onError: function ($form) {
                return false;
            },
            onSuccess: function ($form) {
                //var allData = $("#addAssessmnetQuestionBankForm").serialize();
                var form = $('#addAssessmnetQuestionBankForm')[0];
                var allData = new FormData(form);

                var questionBankName = $("#addAssessmnetQuestionBankForm input[name=assessmentQuestionBankname]").val();
                var questionBankType = $("#addAssessmnetQuestionBankForm input[name=assessmenQquestionType]").val();
                var hiddenQuestionBankId = $("#addAssessmnetQuestionBankForm input[name=hiddenQuestionBankId]").val();
                var hiddenQuestionBankType = $("#addAssessmnetQuestionBankForm input[name=hiddenQuestionBankType]").val();

                if(hiddenQuestionBankId != '') {
                    $.ajax({
                        url: siteUrl + "assessments/updateAssessmentQuestionBank",
                        type: "POST",
                        enctype: 'multipart/form-data',
                        data: allData,
                        processData: false,
                        contentType: false,
                        cache: false,
                        success: function (data) {
                            var obj = $.parseJSON(data);
                            var noOfQuestions = obj.noOfQuestions;
                            var questionBankType = obj.questionType;
                            var allAssessmentQuestions = obj.allAssessmentQuestions;

                            if(questionBankType == '1') {
                                var type = 'Video';
                            } else {
                                var type = 'Text';
                            }

                            $('.question-bank-table tbody #trQuestion_' + hiddenQuestionBankId).find("td").eq(0).html(questionBankName);
                            $('.question-bank-table tbody #trQuestion_' + hiddenQuestionBankId).find("td").eq(1).html(type);

                            var noofques = $('.question-bank-table tbody #trQuestion_' + hiddenQuestionBankId).find("td").eq(2).html();
                            var nofquess = parseInt(noofques)+parseInt(noOfQuestions);
                            $('.question-bank-table tbody #trQuestion_' + hiddenQuestionBankId).find("td").eq(2).html(nofquess);


                            var testName = "'"+questionBankName+"'";

                            if(allAssessmentQuestions.length === 0) {
                                
                            } else {
                                $('#demo'+hiddenQuestionBankId+' .table .noquestionofquestionbank ').remove();
                                for(var i = 0; i < allAssessmentQuestions.length; i++) {
                                    $('#demo'+hiddenQuestionBankId+' .table').append('<tr id="questionbankquestiontr_'+allAssessmentQuestions[i].questionId+'"><td>'+allAssessmentQuestions[i].quesitonName+'</td><td style="width:31%;"><div class="pull-left"><a onclick="assessmentQuestionBankQuestionRow('+allAssessmentQuestions[i].questionId+', '+questionBankType+', '+testName+', '+hiddenQuestionBankId+')" class="assessmentQuestionBankQuestionRow" date-questiontype ="'+questionBankType+'" data-questionid="'+allAssessmentQuestions[i].questionId+'" data-questionBankId="'+hiddenQuestionBankId+'" title="Edit this question" href="javascript:"><img src="'+siteUrl+'theme/firstJob/image/edit.png" class="padd_rgt"></a><a title="Delte this quetion" href="javascript:" class="delete-question-assessment" data-questionid="'+allAssessmentQuestions[i].questionId+'"><img src="'+siteUrl+'/theme/firstJob/image/del.png" class="padd_rgt"></a></div></td></tr>');
                                }
                            }

                            $( "#manageInterviewSetTables" ).hide();
                            $( "#addAssessmentSetDiv" ).hide();
                            $( "#breadcrumbText" ).text('');
                            $( ".addQuestionBankBreadcrumb" ).remove();
                            $( "#editAssessmentSetDiv" ).hide();
                            $( "#addQuestionBankDiv" ).hide();
                            $( "#manageMyQuestionDiv" ).show();
                        }
                    });
                } else {
                    $.ajax({
                        url: siteUrl + "jobs/addQuestionBank",
                        type: "POST",
                        enctype: 'multipart/form-data',
                        data: allData,
                        processData: false,
                        contentType: false,
                        cache: false,
                        success: function (data) {
                            var obj = $.parseJSON(data);
                            var questionBankId = obj.questionBankId;
                            var noOfQuestions = obj.noOfQuestions;
                            var errorMessage = obj.errorMessage;
                            var questionBankType = obj.questionType;
                            var allAssessmentQuestions = obj.allAssessmentQuestions;

                            if(questionBankType == '1') {
                                var type1 = 'Video';
                            } else {
                                var type1 = 'Text';
                            }

                            var testName = "'"+questionBankName+"'";

                            $('#manageMyQuestionDiv').append('<div class="rsp_tbl"><table class="table top-head-table question-bank-table"><tbody><tr id="trQuestion_'+questionBankId+'" class="tbl_bg_light"><td>'+questionBankName+'</td><td>'+type1+'<div id="questionMyVideo"></div></td><td>'+noOfQuestions+'</td><td><div class="pull-right"><a onclick="editAssessmentQuestionBank('+questionBankId+')" class="editAssessmentQuestionBank" id="editAssessmentQuestionBank-'+questionBankId+'" title="Edit this Question Bank" href="javascript:"><img src="'+siteUrl+'theme/firstJob/image/edit.png" class="padd_rgt"></a><a title="Delete this Question Bank" href="javascript:" class="delete-question-bank" data-questionbankid="'+questionBankId+'"><img src="'+siteUrl+'theme/firstJob/image/del.png" class="padd_rgt"></a><a onclick="addAssessmentQuestionButton('+questionBankId+', '+questionBankType+', '+testName+')" title="Add Question to this Question Bank" href="#"><img src="'+siteUrl+'theme/firstJob/image/add_black.png" class="padd_rgt collapsed"></a><a title="Show Questions for this Quesiton Bank" href="#"><img src="'+siteUrl+'theme/firstJob/image/arrow_rot.png" class="padd_rgt collapsed" href="#demo'+questionBankId+'" data-toggle="collapse" aria-expanded="false"></a></div></td></tr></tbody></table></div><div class="col-md-12 rsp_tbl collapse" id="demo'+questionBankId+'" aria-expanded="false" style="height: 0px;"><table class="table padding_bt_5"><tbody></tbody></table></div>');

                            if(allAssessmentQuestions.length === 0) {
                                $('#demo'+questionBankId+' .table').append('<tr class"noquestionofquestionbank"><td></td><td><div class="pull-right"><span>No Question added in this Question Bank</span></div></td></tr>');
                            } else {
                                for(var i = 0; i < allAssessmentQuestions.length; i++) {
                                    $('#demo'+questionBankId+' .table').append('<tr id="questionbankquestiontr_'+allAssessmentQuestions[i].questionId+'"><td>'+allAssessmentQuestions[i].quesitonName+'</td><td style="width:31%;"><div class="pull-left"><a onclick="assessmentQuestionBankQuestionRow('+allAssessmentQuestions[i].questionId+', '+questionBankType+', '+testName+', '+questionBankId+')" class="assessmentQuestionBankQuestionRow" date-questiontype ="'+questionBankType+'" data-questionid="'+allAssessmentQuestions[i].questionId+'" data-questionBankId="'+questionBankId+'" title="Edit this question" href="javascript:"><img src="'+siteUrl+'theme/firstJob/image/edit.png" class="padd_rgt"></a><a title="Delte this quetion" href="javascript:" class="delete-question-assessment" data-questionid="'+allAssessmentQuestions[i].questionId+'"><img src="'+siteUrl+'/theme/firstJob/image/del.png" class="padd_rgt"></a></div></td></tr>');
                                }
                            }

                            $( "#manageInterviewSetTables" ).hide();
                            $( "#addAssessmentSetDiv" ).hide();
                            $( "#breadcrumbText" ).text('');
                            $( ".addQuestionBankBreadcrumb" ).remove();
                            $( "#editAssessmentSetDiv" ).hide();
                            $( "#addQuestionBankDiv" ).hide();
                            $( "#manageMyQuestionDiv" ).show();
                        }
                    });
                }
                return false;
            },
            validateOnBlur : true,
            errorMessagePosition : 'top',
            scrollToTopOnError : false // Set this property to true on longer forms
        });
        
    //Form Validator for Assessment Question Bank
    $.formUtils.addValidator({
        name: 'assessmentQuestionFileData',
        validatorFunction: function (value, $el, config, language, $form) {
            var hasNoValue;
            var fileTypeValid;
            var fileType = $('#questionFormType').val();
            var fileValueData = $('.assessmentQuestionFileName').val();

            if (fileValueData == '') {
                hasNoValue2 = true;
            } else {
                hasNoValue2 = false;
            }

            $('.assessmentQuestionFileData').each(function(i) {
                if ($(this).val() == '') {
                      hasNoValue = true;
                }
                /*else {
                    var fname = $(this).val();
                    if($('#questionFormType').val()=='1') {
                        var video = /(\.mp4)$/i;
                        if (video.test(fname))
                        fileTypeValid = true;
                    }                    
                    else if($('#questionFormType').val()=='2') {
                        var audio = /(\.mp3)$/i;
                        if (audio.test(fname))
                        fileTypeValid = true;
                    }
                }*/
            });
            if ((hasNoValue==true) && (hasNoValue2 == true)) {
                return false;
            }
            else {
                return true;
            }
            //return parseInt(value, 10) % 2 === 0;
        },
        errorMessage : 'Please upload mp4 file for video set or mp3 file for audio set.'
    });

    $.validate({
        form: '#questionassessmentForm',
        modules: 'file',
        required: "We need your email address to contact you",
        onError: function ($form) {
            //alert($('.level1').val());
            return false;
        },
        onSuccess: function ($form) {
            //var allData = $("#questionassessmentForm").serialize();
            var form = $('#questionassessmentForm')[0];
            var allData = new FormData(form);

            var questionBankId = $("#questionassessmentForm input[name=questionBankId]").val();
            var questionBankType = $("#questionassessmentForm input[name=hiddenType]").val();
            var questionId = $("#questionassessmentForm input[name=questionId]").val();

            if(questionId != '') {
                $.ajax({
                    url: siteUrl + "assessments/updateQuestion",
                    type: "POST",
                    enctype: 'multipart/form-data',
                    data: allData,
                    processData: false,
                    contentType: false,
                    cache: false,
                    success: function (data) {
                        var obj = $.parseJSON(data);
                        var questionId = obj.questionId;
                        var questionName = obj.quesitonName;
                        $('#questionbankquestiontr_' + questionId).find("td").eq(0).html(questionName);
                    }
                });
            } else {
                $.ajax({
                    url: siteUrl + "assessments/addQuestionRecruiter",
                    type: "POST",
                    enctype: 'multipart/form-data',
                    data: allData,
                    processData: false,
                    contentType: false,
                    cache: false,
                    success: function (data) {
                        var obj = $.parseJSON(data);
                        var questionId = obj.questionId;
                        var questionName = obj.quesitonName;
                        var questionBankName = obj.questionBankName;

                        //$('#demo'+questionBankId+'').removeClass('noquestionofquestionbank');
                        $('#demo'+questionBankId+' .table .noquestionofquestionbank ').remove();
                        var testName = "'"+questionBankName+"'";
                        $('#demo'+questionBankId+' .table').append('<tr id="questionbankquestiontr_'+questionId+'"><td>'+questionName+'</td><td style="width:31%;"><div class="pull-left"><a onclick="assessmentQuestionBankQuestionRow('+questionId+', '+questionBankType+', '+testName+', '+questionBankId+')" class="assessmentQuestionBankQuestionRow" date-questiontype ="'+questionBankType+'" data-questionid="'+questionId+'" data-questionBankId="'+questionBankId+'" title="Edit this question" href="javascript:"><img src="'+siteUrl+'theme/firstJob/image/edit.png" class="padd_rgt"></a><a title="Delte this quetion" href="javascript:" class="delete-question-assessment" data-questionid="'+questionId+'"><img src="'+siteUrl+'/theme/firstJob/image/del.png" class="padd_rgt"></a></div></td></tr>');
                        var noofques = $('.question-bank-table tbody #trQuestion_' + questionBankId).find("td").eq(2).html();
                        var nofquess = parseInt(noofques)+1;
                        $('.question-bank-table tbody #trQuestion_' + questionBankId).find("td").eq(2).html(nofquess);
                    }
                });
            }

            $( "#manageInterviewSetTables" ).hide();
            $( "#addAssessmentSetDiv" ).hide();
            //$( "#breadcrumbText" ).text('');
            $( ".addQuestionBankBreadcrumb" ).remove();
            $( "#editAssessmentSetDiv" ).hide();
            $( "#addQuestionBankDiv" ).hide();
            $( "#assessmentQuestionBankAddQuestionDiv" ).hide();
            $( "#manageMyQuestionDiv" ).show();

            return false;
        },
        validateOnBlur : false,
        errorMessagePosition : 'top',
        scrollToTopOnError : false // Set this property to true on longer forms
    });


    //End of Assessment sets script

    $( "#basicformid" ).click(function() {
        $( "#interviewjobcontent" ).hide();
        $( "#recruitersjobcontent" ).hide();
        $( "#basicjobcontent" ).show();
    });

    $( "#interviewformid" ).click(function() {
        $( "#interviewjobcontent" ).show();
        $( "#recruitersjobcontent" ).hide();
        $( "#basicjobcontent" ).hide();
    });

    $( "#panellevelformid" ).click(function() {
        $( "#interviewjobcontent" ).hide();
        $( "#recruitersjobcontent" ).show();
        $( "#basicjobcontent" ).hide();
    });
    
    function saveBasicData(){
        if ( $( "#interviewformid" ).is( ".active" ) ) {
            var interviewValue = $('.examinterview').val();
            var assessmentValue = $('.examassessment').val();

            var jobTitle1 = $('#titleOfJob').val();
            var jobId1 = $("#jobForm input[name=jobId]").val();
            var locations1 = $('.location').val();
            var numberofVacancies1 = $('#numberofVacancies').val();
            var jobDescription1 = tinymce.get('jobDescription').getContent();

            var regex1 = /^[a-zA-Z0-9\-\s]+$/;

            if(jobTitle1=='' || locations1==null || numberofVacancies1=='' || jobDescription1=='') {
                alert('Please enter all the required fields on Main Tab');
                return false;
            }
            else if(!$.isNumeric(numberofVacancies1) || numberofVacancies1<1) {
                alert('Please enter number only in Vacancies field on Main Tab');
                return false;
            }
            else if(assessmentValue ==0 && interviewValue ==0) {
                alert('Please choose interview set');
                return false;
            }
            else if(assessmentValue !=0 && interviewValue ==0) {
                alert('Please choose interview set');
                return false;
            } else if(jobId1 != '' && !regex1.test(jobId1)) {
                alert('JobId must be alphanumeric in Main Tab.');
                return false;
            } else {
                $("#saveBasicInfoButton").click();
            }
        }

        if ( $( "#panellevelformid" ).is( ".active" ) ) {
            var level1Value = $('.level1').val();
            var level2Value = $('.level2').val();

            var jobTitle2 = $('#titleOfJob').val();
            var jobId2 = $("#jobForm input[name=jobId]").val();
            var locations2 = $('.location').val();
            var numberofVacancies2 = $('#numberofVacancies').val();
            var jobDescription2 = tinymce.get('jobDescription').getContent();

            var regex2 = /^[a-zA-Z0-9\-\s]+$/;

            if(jobTitle2=='' || locations2==null || numberofVacancies2=='' || jobDescription2=='') {
                alert('Please enter all the required fields on Main Tab');
                return false;
            }
            else if(!$.isNumeric(numberofVacancies2) || numberofVacancies2<1) {
                alert('Please enter number only in Vacancies field on Main Tab');
                return false;
            }
            else if(level2Value ==null && level1Value ==null) {
                alert('Please choose Level 1 user');
                return false;
            }
            else if(level2Value !=null && level1Value ==null) {
                alert('Please choose Level 1 user');
                return false;
            } else if(jobId2 != '' && !regex2.test(jobId2)) {
                alert('JobId must be alphanumeric in Main Tab.');
                return false;
            } else {
                $("#saveBasicInfoButton").click();
            }
        }

        if ( $( "#basicformid" ).is( ".active" ) ) {
            var jobTitle = $('#titleOfJob').val();
            var jobId = $("#jobForm input[name=jobId]").val();
            var locations = $('.location').val();
            var numberofVacancies = $('#numberofVacancies').val();
            var jobDescription = tinymce.get('jobDescription').getContent();

            var expFrom = parseInt($('.expFrom').val());
            var expTo = parseInt($('.expTo').val());

            var salFrom = parseInt($('.salFrom').val());
            var salTo = parseInt($('.salTo').val());

            var ageFrom = parseInt($('.ageFrom').val());
            var ageTo = parseInt($('.ageTo').val());
           
            var noticeperiod = $('.noticeperiod').val();

            var regex = /^[a-zA-Z0-9\-\s]+$/;

            if(jobTitle=='' || locations==null || numberofVacancies=='' || jobDescription=='') {
                alert('Please enter all the required fields');
            } else if(!$.isNumeric(numberofVacancies) || numberofVacancies<1) {
                alert('Please enter number only in Vacancies field');
            } else if(expFrom > expTo) {
                alert('Experience from value must be less than experience to value');
            } else if(salFrom > salTo) {
                alert('Salary from value must be less than salary to value');
            } else if(ageFrom > ageTo) {
                alert('Age from value must be less than age to value');
            } else if(noticeperiod != '' && !$.isNumeric(noticeperiod)) {
                alert('Notice Period must be a number or leave blank');
            } else if(jobId != '' && !regex.test(jobId)) {
                alert('JobId must be alphanumeric.');
            } else {
                $("#saveBasicInfoButton").click();
            }
        }
    }

    //Setting interview, assessment and recruiters data into table
    $(document).on('change', '#interviewAttach', function() {
        $("#interviewSetTableContent").show();
        $("#interviewSetTableContent tbody").remove();
        var interviewSetId = $(this).find(":selected").val();
        $.post(siteUrl + 'interviews/getRecruiterInterviewSet', {interviewSetId: interviewSetId}, function (data) {
            var obj = $.parseJSON(data);
            var questions = obj.questions;
            
            var contentHtml = "";
            for(var i = 0; i < questions.length; i++) {

                var duration = questions[i].duration;
                var file = siteUrl + 'uploads/questions/video/'+ questions[i].file;
                if(questions[i].type == 1) {
                    var questionType = 'Video';
                    var questionVideo = '<video style="height: 193px;" controls><source src="'+file+'" type="video/mp4">Your browser does not support the video tag.</video>';
                } else {
                    var questionType = 'Text';
                    var questionVideo = 'NA';
                }

                contentHtml += '<tr><td>'+questions[i].title+'</td><td>'+questionType+'</td><td>'+questionVideo+'</td></tr>';
            }
            $('#interviewSetTableContent').append(contentHtml);
        });
    });

    $(document).on('change', '#assessmentAttach', function() {
        $("#assessmentSetTableContent").show();
        $("#assessmentSetTableContent tbody").remove();
        var assessmentId = $(this).find(":selected").val();
        $.post(siteUrl + 'jobs/editAssessment1', {assessmentId: assessmentId}, function (data) {
            var obj = $.parseJSON(data);
            var questions = obj.questions;
            
            var contentHtml = "";
            for(var i = 0; i < questions.length; i++) {

                var duration = questions[i].duration;
                var file = siteUrl + 'uploads/questions/video/'+ questions[i].file;
                if(questions[i].type == 1) {
                    var questionType = 'Video';
                    var questionVideo = '<video style="height: 193px;" controls><source src="'+file+'" type="video/mp4">Your browser does not support the video tag.</video>';
                } else {
                    var questionType = 'Text';
                    var questionVideo = 'NA';
                }

                contentHtml += '<tr><td>'+questions[i].title+'</td><td>'+questionType+'</td><td>'+questionVideo+'</td></tr>';
            }
            $('#assessmentSetTableContent').append(contentHtml);
        });
    });

    //End of setting interview, assessment and recruiters data into table
    
    $.validate({
        form: '#jobForm',
        modules: 'file',
        onError: function ($form) {
            //alert($('.level1').val());
            return false;
        },
        onSuccess: function ($form) {
            return true;
        },
    });
    
    
    var config = {
        '.chosen-select': {},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }

    var config1 = {
        '.chosen-select1': {},
        '.chosen-select1-deselect': {allow_single_deselect: true},
        '.chosen-select1-no-single': {disable_search_threshold: 10},
        '.chosen-select1-no-results': {no_results_text: ''},
        '.chosen-select1-width': {width: "95%"}
    }
    for (var selector in config1) {
        $(selector).chosen(config1[selector]);
    }

    var config2 = {
        '.chosen-select2': {},
        '.chosen-select2-deselect': {allow_single_deselect: true},
        '.chosen-select2-no-single': {disable_search_threshold: 10},
        '.chosen-select2-no-results': {no_results_text: ''},
        '.chosen-select2-width': {width: "95%"}
    }
    for (var selector in config2) {
        $(selector).chosen(config2[selector]);
    }

    var config3 = {
        '.chosen-select3': {},
        '.chosen-select3-deselect': {allow_single_deselect: true},
        '.chosen-select3-no-single': {disable_search_threshold: 10},
        '.chosen-select3-no-results': {no_results_text: ''},
        '.chosen-select3-width': {width: "95%"}
    }
    for (var selector in config3) {
        $(selector).chosen(config3[selector]);
    }

    var config4 = {
        '.chosen-select4': {},
        '.chosen-select4-deselect': {allow_single_deselect: true},
        '.chosen-select4-no-single': {disable_search_threshold: 10},
        '.chosen-select4-no-results': {no_results_text: ''},
        '.chosen-select4-width': {width: "95%"}
    }
    for (var selector in config4) {
        $(selector).chosen(config4[selector]);
    }

    $(function () {
        $("#datepicker-19").datepicker();
    });


    //Displaying Popup

    var createdJobId = "<?php echo !empty($resp) ? $resp : '' ;?>";
    var assessmentId = "<?php echo !empty($assessmentId) ? $assessmentId : '' ;?>";
    var interviewId = "<?php echo !empty($interviewId) ? $interviewId : '' ;?>";
    var panel1Data = "<?php echo !empty($panel1Data) ? implode(',', $panel1Data) : '' ;?>";
    var pane21Data = "<?php echo !empty($pane21Data) ? implode(',', $pane21Data) : '' ;?>";


    var $textAndPic = $('<div></div>');
        $textAndPic.append('<div style="margin-left:0px;" class="col-md-5"><img style="width: 108px;height:28px;" class="img-responsive" src="https://vdohire.com/admin/theme/firstJob/image/logo-new.png" /></div>');
        $textAndPic.append('<div style="padding-left:22px;font-size:18px;padding-top:3px;" class="col-md-7"><span>Add Job</span></div>');


    if(createdJobId!='' && interviewId == 0 && panel1Data =='') {
        BootstrapDialog.show({
            title: $textAndPic,
            closable: false,
            message: 'Job added successfully. Please add Interview set!',
            buttons: [{
                label: 'Ok',
                action: function(){
                    window.location.href = '<?php echo base_url()."jobs/edit/";?>'+createdJobId;
                }
            }, {
                label: 'Add Job',
                action: function(){
                    window.location.href = '<?php echo base_url()."jobs/add";?>';
                }
            }, {
                label: 'Save and go to Job List',
                action: function(){
                    window.location.href = '<?php echo base_url()."jobs/list";?>';
                }
            }]
        });
    } else if(createdJobId!='' && interviewId == 0 && panel1Data !='') {
        BootstrapDialog.show({
            title: $textAndPic,
            closable: false,
            message: 'Job added successfully. Please add Interview set!',
            buttons: [{
                label: 'Ok',
                action: function(){
                    window.location.href = '<?php echo base_url()."jobs/edit/";?>'+createdJobId;
                }
            }, {
                label: 'Add Job',
                action: function(){
                    window.location.href = '<?php echo base_url()."jobs/add";?>';
                }
            }, {
                label: 'Save and go to Job List',
                action: function(){
                    window.location.href = '<?php echo base_url()."jobs/list";?>';
                }
            }]
        });
    } else if(createdJobId!='' && interviewId != 0 && panel1Data =='') {
        BootstrapDialog.show({
            title: $textAndPic,
            closable: false,
            message: 'Job added successfully.',
            buttons: [{
                label: 'Add More Recruiters',
                action: function(){
                    window.location.href = '<?php echo base_url()."jobs/edit/";?>'+createdJobId;
                }
            }, {
                label: 'Add Job',
                action: function(){
                    window.location.href = '<?php echo base_url()."jobs/add";?>';
                }
            }, {
                label: 'Save and go to Job List',
                action: function(){
                    window.location.href = '<?php echo base_url()."jobs/list";?>';
                }
            }]
        });
    } else if(createdJobId!='' && interviewId != 0 && panel1Data !='') {
        BootstrapDialog.show({
            title: $textAndPic,
            closable: false,
            message: 'Job added successfully!',
            buttons: [{
                label: 'Add Job',
                action: function(){
                    window.location.href = '<?php echo base_url()."jobs/add";?>';
                }
            }, {
                label: 'Save and go to Job List',
                action: function(){
                    window.location.href = '<?php echo base_url()."jobs/list";?>';
                }
            }]
        });
    }
    //End of Displaying popup
</script>




<?php
$userData = $this->session->userdata['logged_in'];
$userRole = $userData->role;
if($userRole!='1') { ?>
<script type="text/javascript">
    // Add validator For Interview Assessment
    $.formUtils.addValidator({
        name: 'interviewQuestion',
        validatorFunction: function (value, $el, config, language, $form) {               
            var setMode;                        
            if($('#environment_1').is(':checked')) {
                setMode = true;
            }
            else if($('#environment_2').is(':checked')) {
                setMode = false;
            }
            else {
                setMode = 'undefined';
            }
            
            var hasQusNoValue;            
            var videoQuestionCount=0;            
            $('.interviewQuestion').each(function(i) {
                videoQuestionCount++;
                if ($(this).val() == '') {
                      hasQusNoValue = true;
                }
            });
            
            var hasFileNoValue;
            var videoQuestionFileCount=0;
            $('.interviewFileNames').each(function(i) {
                videoQuestionFileCount++;
                if ($(this).val() == '') {
                      hasFileNoValue = true;
                }
            });
            
            var hasDurNoValue;
            var videoQuestionDurationCount=0;
            $('.interviewDuration').each(function(i) {
                videoQuestionDurationCount++;
                if ($(this).val() == '') {
                      hasDurNoValue = true;
                }
            });
            if(setMode==false) {
                if (hasQusNoValue==true && hasDurNoValue==true && hasFileNoValue==true && videoQuestionCount==1 && videoQuestionDurationCount==1 && videoQuestionFileCount==1) {
                    return true;
                }
                else if (!hasQusNoValue && videoQuestionCount==1 && videoQuestionDurationCount==1 && videoQuestionFileCount==1) {
                    return true;
                }
                else if (!hasQusNoValue && videoQuestionCount>1 && videoQuestionDurationCount>1 && videoQuestionFileCount>1) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {               
                if (hasQusNoValue) {
                    return false;
                }
                else {
                    return true;
                } 
            }
            //return parseInt(value, 10) % 2 === 0;
        },
        errorMessage : 'All question title must be filled'
    });
    
    $.formUtils.addValidator({
        name: 'interviewFile',
        validatorFunction: function (value, $el, config, language, $form) {                  
            var setMode;                        
            if($('#environment_1').is(':checked')) {
                setMode = true;
            }
            else if($('#environment_2').is(':checked')) {
                setMode = false;
            }
            else {
                setMode = 'undefined';
            }
            
            var hasQusNoValue;            
            var videoQuestionCount=0;
            $('.interviewQuestion').each(function(i) {
                videoQuestionCount++;
                if ($(this).val() == '') {
                      hasQusNoValue = true;
                }
            });
            
            var hasFileNoValue;
            var fileType;
            var fileTypeValid=0;
            var videoQuestionFileCount=0;

            $('.interviewFileNames').each(function(i) {
                videoQuestionFileCount++;

                fileType = $("#interviewsetfrm select[name=type]").val();

                if ($(this).val() == '') {
                      hasFileNoValue = true;
                } else {
                    var fname = $(this).val();
                    if(fileType=='1') {
                        var video = /(\.mp4)$/i;
                        var audio = /(\.mp3)$/i;

                        if (video.test(fname)) {
                            fileTypeValid++;
                        } else if (audio.test(fname)) {
                            fileTypeValid++;
                        }
                    }
                }
            });
            
            var hasDurNoValue;
            var videoQuestionDurationCount=0;
            $('.interviewDuration').each(function(i) {
                videoQuestionDurationCount++;
                if ($(this).val() == '') {
                      hasDurNoValue = true;
                }
            });
            
            if(setMode==false) {
                if (hasQusNoValue==true && hasDurNoValue==true && hasFileNoValue==true && videoQuestionCount==1 && videoQuestionDurationCount==1 && videoQuestionFileCount==1) {
                    return true;
                }
                else if (!hasFileNoValue && videoQuestionCount==1 && videoQuestionDurationCount==1 && videoQuestionFileCount==1) {
                    return true;
                }
                else if (!hasFileNoValue && videoQuestionCount>1 && videoQuestionDurationCount>1 && videoQuestionFileCount>1) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {               
                if (hasFileNoValue == true || fileTypeValid!=videoQuestionFileCount) {
                    return false;
                }
                else {
                    return true;
                } 
            }
            //return parseInt(value, 10) % 2 === 0;
        },
        errorMessage : 'All questions files must be filled and must be mp4 or mp3.'
    });
    
    $.formUtils.addValidator({
        name: 'interviewDuration',
        validatorFunction: function (value, $el, config, language, $form) {                   
            var setMode;                        
            if($('#environment_1').is(':checked')) {
                setMode = true;
            }
            else if($('#environment_2').is(':checked')) {
                setMode = false;
            }
            else {
                setMode = 'undefined';
            }
            var hasQusNoValue;            
            var videoQuestionCount=0;
            $('.interviewQuestion').each(function(i) {
                videoQuestionCount++;
                if ($(this).val() == '') {
                      hasQusNoValue = true;
                }
            });
            
            var hasFileNoValue;
            var videoQuestionFileCount=0;
            $('.interviewFileNames').each(function(i) {
                videoQuestionFileCount++;
                if ($(this).val() == '') {
                    hasFileNoValue = true;
                }
            });
            
            var hasDurNoValue;
            var videoQuestionDurationCount=0;
            var durationExceed;
            $('.interviewDuration').each(function(i) {
                videoQuestionDurationCount++;
                if ($(this).val() == '') {
                    hasDurNoValue = true;
                }
                else {
                    if (parseInt($(this).val())>600 || isNaN($(this).val())) {
                        durationExceed = true;
                    }
                }
            });
            
            
            if(setMode==false) {      
                if (hasQusNoValue==true && hasDurNoValue==true && hasFileNoValue==true && videoQuestionCount==1 && videoQuestionDurationCount==1 && videoQuestionFileCount==1) {
                    return true;
                }
                else if (!hasDurNoValue && !durationExceed && videoQuestionCount==1 && videoQuestionDurationCount==1 && videoQuestionFileCount==1) {
                    return true;
                }
                else if (!hasDurNoValue && !durationExceed && videoQuestionCount>1 && videoQuestionDurationCount>1 && videoQuestionFileCount>1) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {               
                if (hasDurNoValue==true || durationExceed==true) {
                    return false;
                }
                else {
                    return true;
                } 
            }
            //return parseInt(value, 10) % 2 === 0;
        },
        errorMessage :  'All questions duration (limit 600 sec) fields should be less than 600 seconds '
    });
</script>    
<?php
}
if($userRole=='1') { ?>
<script>
        // Add validator For Interview Assessment
    $.formUtils.addValidator({
        name: 'interviewQuestion',
        validatorFunction: function (value, $el, config, language, $form) {                  
            var setMode;                        
            if($('#environment_1').is(':checked')) {
                setMode = true;
            }
            else if($('#environment_2').is(':checked')) {
                setMode = false;
            }
            else {
                setMode = 'undefined';
            }
            
            var hasQusNoValue;            
            var videoQuestionCount=0;            
            $('.interviewQuestion').each(function(i) {
                videoQuestionCount++;
                if ($(this).val() == '') {
                      hasQusNoValue = true;
                }
            });
            
            var hasFileNoValue;
            var videoQuestionFileCount=0;
            $('.interviewFileNames').each(function(i) {
                videoQuestionFileCount++;
                if ($(this).val() == '') {
                      hasFileNoValue = true;
                }
            });
            
            var hasDurNoValue;
            var videoQuestionDurationCount=0;
            $('.interviewDuration').each(function(i) {
                videoQuestionDurationCount++;
                if ($(this).val() == '') {
                      hasDurNoValue = true;
                }
            });
            if(setMode==false) {
                if (hasQusNoValue==true && hasDurNoValue==true && hasFileNoValue==true && videoQuestionCount==1 && videoQuestionDurationCount==1 && videoQuestionFileCount==1) {
                    return true;
                }
                else if (!hasQusNoValue && videoQuestionCount==1 && videoQuestionDurationCount==1 && videoQuestionFileCount==1) {
                    return true;
                }
                else if (!hasQusNoValue && videoQuestionCount>1 && videoQuestionDurationCount>1 && videoQuestionFileCount>1) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {               
                if (hasQusNoValue) {
                    return false;
                }
                else {
                    return true;
                } 
            }
            //return parseInt(value, 10) % 2 === 0;
        },
        errorMessage : 'All video question title must be filled'
    });
    
    $.formUtils.addValidator({
        name: 'interviewFile',
        validatorFunction: function (value, $el, config, language, $form) {                  
            var setMode;                        
            if($('#environment_1').is(':checked')) {
                setMode = true;
            }
            else if($('#environment_2').is(':checked')) {
                setMode = false;
            }
            else {
                setMode = 'undefined';
            }
            
            var hasQusNoValue;            
            var videoQuestionCount=0;
            $('.interviewQuestion').each(function(i) {
                videoQuestionCount++;
                if ($(this).val() == '') {
                      hasQusNoValue = true;
                }
            });
            
            var hasFileNoValue;
            var videoQuestionFileCount=0;
            $('.interviewFileNames').each(function(i) {
                videoQuestionFileCount++;
                if ($(this).val() == '') {
                      hasFileNoValue = true;
                }
            });
            
            var hasDurNoValue;
            var videoQuestionDurationCount=0;
            $('.interviewDuration').each(function(i) {
                videoQuestionDurationCount++;
                if ($(this).val() == '') {
                      hasDurNoValue = true;
                }
            });
            
            
            if(setMode==false) {
                if (hasQusNoValue==true && hasDurNoValue==true && hasFileNoValue==true && videoQuestionCount==1 && videoQuestionDurationCount==1 && videoQuestionFileCount==1) {
                    return true;
                }
                else if (!hasFileNoValue && videoQuestionCount==1 && videoQuestionDurationCount==1 && videoQuestionFileCount==1) {
                    return true;
                }
                else if (!hasFileNoValue && videoQuestionCount>1 && videoQuestionDurationCount>1 && videoQuestionFileCount>1) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {               
                if (hasFileNoValue) {
                    return false;
                }
                else {
                    return true;
                } 
            }
            //return parseInt(value, 10) % 2 === 0;
        },
        errorMessage : 'All video questions files must be filled'
    });
    
    $.formUtils.addValidator({
        name: 'interviewDuration',
        validatorFunction: function (value, $el, config, language, $form) {                   
            var setMode;                        
            if($('#environment_1').is(':checked')) {
                setMode = true;
            }
            else if($('#environment_2').is(':checked')) {
                setMode = false;
            }
            else {
                setMode = 'undefined';
            }
            var hasQusNoValue;            
            var videoQuestionCount=0;
            $('.interviewQuestion').each(function(i) {
                videoQuestionCount++;
                if ($(this).val() == '') {
                      hasQusNoValue = true;
                }
            });
            
            var hasFileNoValue;
            var videoQuestionFileCount=0;
            $('.interviewFileNames').each(function(i) {
                videoQuestionFileCount++;
                if ($(this).val() == '') {
                    hasFileNoValue = true;
                }
            });
            
            var hasDurNoValue;
            var videoQuestionDurationCount=0;
            var durationExceed;
            $('.interviewDuration').each(function(i) {
                videoQuestionDurationCount++;
                if ($(this).val() == '') {
                    hasDurNoValue = true;
                }
                else {
                    if (parseInt($(this).val())>600 || isNaN($(this).val())) {
                        durationExceed = true;
                    }
                }
            });
            
            
            if(setMode==false) {      
                if (hasQusNoValue==true && hasDurNoValue==true && hasFileNoValue==true && videoQuestionCount==1 && videoQuestionDurationCount==1 && videoQuestionFileCount==1) {
                    return true;
                }
                else if (!hasDurNoValue && !durationExceed && videoQuestionCount==1 && videoQuestionDurationCount==1 && videoQuestionFileCount==1) {
                    return true;
                }
                else if (!hasDurNoValue && !durationExceed && videoQuestionCount>1 && videoQuestionDurationCount>1 && videoQuestionFileCount>1) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {               
                if (hasDurNoValue==true || durationExceed==true) {
                    return false;
                }
                else {
                    return true;
                } 
            }
            //return parseInt(value, 10) % 2 === 0;
        },
        errorMessage :  'All video questions duration (limit 600 sec) fields should be less than 600 seconds '
    });


    // Add validator For Location
    $.formUtils.addValidator({
        name: 'industryFunction',
        validatorFunction: function (value, $el, config, language, $form) {      
            var hasNoValue;
            if($('#functionId').val()!='' && $('#industryId').val()!='') {
                return false;
            }
            else if($('#functionId').val()=='' && $('#industryId').val()=='') {
                return false;
            }
            else {
                return true;
            }
            //return parseInt(value, 10) % 2 === 0;
        },
        errorMessage : 'Please choose either industry or function.'
    });
    
    // Add validator For Interview Assessment


    $.formUtils.addValidator({
        name: 'interviewAudioQuestion',
        validatorFunction: function (value, $el, config, language, $form) {   
            var hasQusNoValue;            
            var audioQuestionCount=0;
            $('.interviewAudioQuestion').each(function(i) {
                audioQuestionCount++;
                if ($(this).val() == '') {
                      hasQusNoValue = true;
                }
            });
            
            var hasFileNoValue;
            var audioQuestionFileCount=0;
            $('.interviewQuestionFileName').each(function(i) {
                audioQuestionFileCount++;
                if ($(this).val() == '') {
                      hasFileNoValue = true;
                }
            });
            
            var hasDurNoValue;
            var audioQuestionDurationCount=0;
            $('.interviewAudioDuration').each(function(i) {
                audioQuestionDurationCount++;
                if ($(this).val() == '') {
                      hasDurNoValue = true;
                }
            });
                        
            if (hasQusNoValue==true && hasDurNoValue==true && hasFileNoValue==true && audioQuestionCount==1 && audioQuestionDurationCount==1 && audioQuestionFileCount==1) {
                return true;
            }
            else if (!hasQusNoValue && audioQuestionCount==1 && audioQuestionDurationCount==1 && audioQuestionFileCount==1) {
                return true;
            }
            else if (!hasQusNoValue && audioQuestionCount>1 && audioQuestionDurationCount>1 && audioQuestionFileCount>1) {
                return true;
            }
            else {
                return false;
            }
            //return parseInt(value, 10) % 2 === 0;
        },
        errorMessage : 'All audio question title must be filled'
    });
    
    $.formUtils.addValidator({
        name: 'interviewAudioQuestionFile',
        validatorFunction: function (value, $el, config, language, $form) {      
            var hasQusNoValue;            
            var audioQuestionCount=0;
            $('.interviewAudioQuestion').each(function(i) {
                audioQuestionCount++;
                if ($(this).val() == '') {
                      hasQusNoValue = true;
                }
            });
            
            var hasFileNoValue;
            var audioQuestionFileCount=0;
            $('.interviewQuestionFileName').each(function(i) {
                audioQuestionFileCount++;
                if ($(this).val() == '') {
                      hasFileNoValue = true;
                }
            });
            
            var hasDurNoValue;
            var audioQuestionDurationCount=0;
            $('.interviewAudioDuration').each(function(i) {
                audioQuestionDurationCount++;
                if ($(this).val() == '') {
                      hasDurNoValue = true;
                }
            });
                        
            if (hasQusNoValue==true && hasDurNoValue==true && hasFileNoValue==true && audioQuestionCount==1 && audioQuestionDurationCount==1 && audioQuestionFileCount==1) {
                return true;
            }
            else if (!hasFileNoValue && audioQuestionCount==1 && audioQuestionDurationCount==1 && audioQuestionFileCount==1) {
                return true;
            }
            else if (!hasFileNoValue && audioQuestionCount>1 && audioQuestionDurationCount>1 && audioQuestionFileCount>1) {
                return true;
            }
            else {
                return false;
            }
            //return parseInt(value, 10) % 2 === 0;
        },
        errorMessage : 'All audio questions files must be filled'
    });
    
    $.formUtils.addValidator({
        name: 'interviewAudioQuestionDuration',
        validatorFunction: function (value, $el, config, language, $form) {  
            var hasQusNoValue;            
            var audioQuestionCount=0;
            $('.interviewAudioQuestion').each(function(i) {
                audioQuestionCount++;
                if ($(this).val() == '') {
                      hasQusNoValue = true;
                }
            });
            
            var hasFileNoValue;
            var audioQuestionFileCount=0;
            $('.interviewQuestionFileName').each(function(i) {
                audioQuestionFileCount++;
                if ($(this).val() == '') {
                    hasFileNoValue = true;
                }
            });
            
            var hasDurNoValue;
            var audioQuestionDurationCount=0;
            var durationExceed;
            $('.interviewAudioDuration').each(function(i) {
                audioQuestionDurationCount++;
                if ($(this).val() == '') {
                    hasDurNoValue = true;
                }
                else {
                    if (parseInt($(this).val())>600 || isNaN($(this).val())) {
                        durationExceed = true;
                    }
                }
            });
                        
            if (hasQusNoValue==true && hasDurNoValue==true && hasFileNoValue==true && audioQuestionCount==1 && audioQuestionDurationCount==1 && audioQuestionFileCount==1) {
                return true;
            }
            else if (!hasDurNoValue && !durationExceed && audioQuestionCount==1 && audioQuestionDurationCount==1 && audioQuestionFileCount==1) {
                return true;
            }
            else if (!hasDurNoValue && !durationExceed && audioQuestionCount>1 && audioQuestionDurationCount>1 && audioQuestionFileCount>1) {
                return true;
            }
            else {
                return false;
            }
            //return parseInt(value, 10) % 2 === 0;
        },
        errorMessage : 'All audio questions duration (limit 600 sec) fields should be less than 600 seconds'
    });
    
    
    
    
    // Add validator For Interview Assessment
    $.formUtils.addValidator({
        name: 'interviewTextQuestion',
        validatorFunction: function (value, $el, config, language, $form) {
            var hasQusNoValue;            
            var textQuestionCount=0;
            $('.interviewTextQuestion').each(function(i) {
                textQuestionCount++;
                if ($(this).val() == '') {
                      hasQusNoValue = true;
                }
            });
            
            var hasDurNoValue;
            var textQuestionDurationCount=0;
            $('.interviewTextDuration').each(function(i) {
                textQuestionDurationCount++;
                if ($(this).val() == '') {
                      hasDurNoValue = true;
                }
            });
                        
            if (hasQusNoValue==true && hasDurNoValue==true && textQuestionCount==1 && textQuestionDurationCount==1) {
                return true;
            }
            else if (!hasQusNoValue && textQuestionCount==1 && textQuestionDurationCount==1) {
                return true;
            }
            else if (!hasQusNoValue && textQuestionCount>1 && textQuestionDurationCount>1) {
                return true;
            }
            else {
                return false;
            }
            //return parseInt(value, 10) % 2 === 0;
        },
        errorMessage : 'All text question title must be filled'
    });
    
    $.formUtils.addValidator({
        name: 'interviewTextQuestionDuration',
        validatorFunction: function (value, $el, config, language, $form) {  
            var hasQusNoValue;            
            var textQuestionCount=0;
            $('.interviewTextQuestion').each(function(i) {
                textQuestionCount++;
                if ($(this).val() == '') {
                      hasQusNoValue = true;
                }
            });
            
            var hasDurNoValue;
            var textQuestionDurationCount=0;
            var durationExceed;
            $('.interviewTextDuration').each(function(i) {
                textQuestionDurationCount++;
                if ($(this).val() == '') {
                    hasDurNoValue = true;
                }
                else {
                    if (parseInt($(this).val())>600 || isNaN($(this).val())) {
                        durationExceed = true;
                    }
                }
            });
                        
            if (hasQusNoValue==true && hasDurNoValue==true && textQuestionCount==1 && textQuestionDurationCount==1) {
                return true;
            }
            else if (!hasDurNoValue && !durationExceed && textQuestionCount==1 && textQuestionDurationCount==1) {
                return true;
            }
            else if (!hasDurNoValue && !durationExceed && textQuestionCount>1 && textQuestionDurationCount>1) {
                return true;
            }
            else {
                return false;
            }
            //return parseInt(value, 10) % 2 === 0;
        },
        errorMessage : 'All text questions duration (limit 600 sec) fields should be less than 600 seconds '
    });
</script>
<?php
} 
?>
    
    
<script>
    $.validate({
        form: '#interviewsetfrm',
        modules: 'file',
        onError: function ($form) {
            //alert($('.level1').val());
            return false;
        },
        onSuccess: function ($form) {
            //var allData = $("#interviewsetfrm").serialize();
            //var allData = new FormData($("#interviewsetfrm"));
            var form = $('#interviewsetfrm')[0];
            var allData = new FormData(form);

            var interviewSetName = $("#interviewsetfrm input[name=name]").val();
            var interviewSetType = $("#interviewsetfrm select[name=type]").val();
            var intereviewSetEditId = $("#interviewsetfrm input[name=intereviewSetEditId]").val();

            if(interviewSetType == '1') {
                var type = 'Video';
            } else {
                var type = 'Text';
            }

            if(intereviewSetEditId == '') {
                var hasanyinterviewsetquestion = $("#hasanyinterviewsetquestion").val();
                if(hasanyinterviewsetquestion > 0) {
                    $.ajax({
                        url: siteUrl + "interviews/addInterviewRecruiter",
                        type: "POST",
                        enctype: 'multipart/form-data',
                        data: allData,
                        processData: false,
                        contentType: false,
                        cache: false,
                        success: function (data) {
                            var obj = $.parseJSON(data);
                            var interviewSetId = obj.interviewSetId;
                            var noOfQuestions = obj.noOfQuestions;
                            var errorMessage = obj.errorMessage;

                            if(errorMessage == '') {
                                $("#interviewAttach option").eq(1).before($("<option></option>").val(interviewSetId).text(interviewSetName));
                                $("#interviewAttach").trigger("chosen:updated");

                                $('#interviewSetTable tbody').prepend('<tr id="trInterview_'+interviewSetId+'"><td>'+interviewSetName+'</td><td>'+type+'</td><td>'+noOfQuestions+'</td><td><a href="javascript:" onclick="editInterviewSetRow('+interviewSetId+')" class="editInterviewSetRow" id="editInterviewSetRow-'+interviewSetId+'"><img class="padd_rgt" src="'+siteUrl+'theme/firstJob/image/edit.png"></a><a style="padding-left: 3px;" href="javascript:" class="deleteInterviewSetRow" id="deleteInterviewSetRow-'+interviewSetId+'"><img src="'+siteUrl+'theme/firstJob/image/del.png"></a></td></tr>');

                                $( "#interviewBreadcrumbText" ).text('');
                                $( "#addInterviewSetDiv" ).hide();
                                $( "#manageInterviewsSetsTables" ).show();
                                $('#hasanyinterviewsetquestion').val(0);
                            } else {
                                alert(errorMessage);
                            }
                        }
                    });
                } else {
                    alert('Please add at least one question in this interview set.');
                }
                
            } else {
                $.ajax({
                    url: siteUrl + "jobs/getInterviewAttachedCount",
                    type: "POST",
                    data: {interviewSetId: intereviewSetEditId},
                    success: function (count) {

                        if(count > 0) {
                            bootbox.confirm('Are you sure you want to edit this interview set? Because this interview set is associated with other jobs!', function (resultthis) {
                                if (resultthis) {
                                    $.ajax({
                                        url: siteUrl + "interviews/editInterviewSetRecruiter",
                                        type: "POST",
                                        enctype: 'multipart/form-data',
                                        data: allData,
                                        processData: false,
                                        contentType: false,
                                        cache: false,
                                        success: function (data) {
                                            var obj = $.parseJSON(data);
                                            var interviewSetId = obj.interviewSetId;
                                            var noOfQuestions = obj.noOfQuestions;
                                            var errorMessage = obj.errorMessage;

                                            $('#interviewAttach option[value="'+interviewSetId+'"]').text(interviewSetName);
                                            $("#interviewAttach").trigger("chosen:updated");

                                            $( "#interviewBreadcrumbText" ).text('');
                                            $( "#addInterviewSetDiv" ).hide();
                                            $( "#manageInterviewsSetsTables" ).show();

                                            $('#interviewSetTable tbody #trInterview_' + interviewSetId).find("td").eq(0).html(interviewSetName);
                                            $('#interviewSetTable tbody #trInterview_' + interviewSetId).find("td").eq(1).html(type);
                                            $('#interviewSetTable tbody #trInterview_' + interviewSetId).find("td").eq(2).html(noOfQuestions);
                                        }
                                    });
                                }
                            });
                        } else {
                            $.ajax({
                                url: siteUrl + "interviews/editInterviewSetRecruiter",
                                type: "POST",
                                enctype: 'multipart/form-data',
                                data: allData,
                                processData: false,
                                contentType: false,
                                cache: false,
                                success: function (data) {
                                    var obj = $.parseJSON(data);
                                    var interviewSetId = obj.interviewSetId;
                                    var noOfQuestions = obj.noOfQuestions;
                                    var errorMessage = obj.errorMessage;

                                    $('#interviewAttach option[value="'+interviewSetId+'"]').text(interviewSetName);
                                    $("#interviewAttach").trigger("chosen:updated");
                                    $( "#interviewBreadcrumbText" ).text('');
                                    $( "#addInterviewSetDiv" ).hide();
                                    $( "#manageInterviewsSetsTables" ).show();

                                    $('#interviewSetTable tbody #trInterview_' + interviewSetId).find("td").eq(0).html(interviewSetName);
                                    $('#interviewSetTable tbody #trInterview_' + interviewSetId).find("td").eq(1).html(type);
                                    $('#interviewSetTable tbody #trInterview_' + interviewSetId).find("td").eq(2).html(noOfQuestions);
                                }
                            });
                            return false;
                        }
                    }
                });
            }
            return false;
        },
        validateOnBlur : false,
        errorMessagePosition : 'top',
        scrollToTopOnError : true // Set this property to true on longer forms
    });
</script>


<script type="text/javascript">
    // Add validator For Location
    $.formUtils.addValidator({
        name: 'questionType',
        validatorFunction: function (value, $el, config, language, $form) {      
            var hasNoValue;
            $('.questionType').each(function(i) {
                if ($(this).val() == '') {
                      hasNoValue = true;
                }
            });
            if (hasNoValue) {
                return false;
            }
            else {
                return true;
            }
            //return parseInt(value, 10) % 2 === 0;
        },
        errorMessage : 'All question type must be selected'
    });
    
    // Add validator For Location
    $.formUtils.addValidator({
        name: 'questionTitle',
        validatorFunction: function (value, $el, config, language, $form) {      
            var hasNoValue;
            $('.questionTitle').each(function(i) {
                if ($(this).val() == '') {
                      hasNoValue = true;
                }
            });
            if (hasNoValue) {
                return false;
            }
            else {
                return true;
            }
            //return parseInt(value, 10) % 2 === 0;
        },
        errorMessage : 'All question titles must be field'
    });

    // Add validator For Qualification
    $.formUtils.addValidator({
        name: 'questionFile',
        validatorFunction: function (value, $el, config, language, $form) {
            var hasNoValue;
            var fileTypeValid=0;
            var fileType;
            var fileCount=0;
            $('.questionFileName').each(function(i) {
                fileCount++;
                fileType = (document.getElementsByName("question["+i+"][type]")[0].value);
                if ($(this).val() == '') {
                      hasNoValue = true;
                }
                else {
                    var fname = $(this).val();
                    if(fileType=='1') {
                        var video = /(\.mp4)$/i;
                        if (video.test(fname))
                        fileTypeValid++;
                    }                    
                    else if(fileType=='2') {
                        var audio = /(\.mp3)$/i;
                        if (audio.test(fname))
                        fileTypeValid++;
                    }
                }
            });
            //if (hasNoValue==true || fileTypeValid!=fileCount) {
            if (hasNoValue==true) {
                return false;
            }
            else {
                return true;
            }
        },
        errorMessage : 'Please upload mp4 file or mp3 file for video type.'
    });

    // Add validator For Interview Assessment
    $.formUtils.addValidator({
        name: 'questionDuration',
        validatorFunction: function (value, $el, config, language, $form) {      
            var hasNoValue;
            var durationExceed;
            $('.questionDuration').each(function(i) {
                if ($(this).val() == '') {
                      hasNoValue = true;
                }
                else {
                    if (parseInt($(this).val())>600 || isNaN($(this).val())) {
                        durationExceed = true;
                    }
                }
            });
            if (hasNoValue==true || durationExceed==true) {
                return false;
            }
            else {
                return true;
            }
            //return parseInt(value, 10) % 2 === 0;
        },
        errorMessage : 'Duration (limit 600 sec) field must be filled and numeric'
    });

    $.validate({
        form: '#addQuestionForm',
        modules: 'file',
        onError: function ($form) {
            //alert($('.level1').val());
            return false;
        },
        onSuccess: function ($form) {
            var form = $('#addQuestionForm')[0];
            var allData = new FormData(form);

            $.ajax({
                url: siteUrl + "interviews/addQuestionRecruiter",
                type: "POST",
                enctype: 'multipart/form-data',
                data: allData,
                processData: false,
                contentType: false,
                cache: false,
                success: function (data) {
                    var obj = $.parseJSON(data);
                    var questions = obj.questions;

                    //$('#interviewAttach').append($("<option></option>").attr("value",interviewSetId).text(interviewSetName));

                    for(var i = 0; i < questions.length; i++) {
                        if(questions[i].type == '1') {
                            var type = 'Video';
                        } else {
                            var type = 'Text';
                        }
                        $('#interviewMyQuestionBankTable tbody').prepend('<tr id="interviewMyQuestionTr_'+questions[i].questionId+'"><td>'+questions[i].title+'</td><td>'+type+'</td><td>'+questions[i].duration+' Sec.</td><td><a href="javascript:" onclick="editInterviewQuestionRow('+questions[i].questionId+')" class="editInterviewQuestionRow" id="editInterviewQuestionRow-'+questions[i].questionId+'"><img class="padd_rgt" src="'+siteUrl+'theme/firstJob/image/edit.png"></a><a style="padding-left: 3px;" href="javascript:" class="deleteInterviewQuestionRow" id="deleteInterviewQuestionRow-'+questions[i].questionId+'"><img src="'+siteUrl+'theme/firstJob/image/del.png"></a></td></tr>');
                    }

                    $( "#interviewBreadcrumbText" ).text(' -> My Question Bank');
                    $( ".addInterviewQuestionBreadcrumb" ).remove();

                    $( "#questionBankAddQuestionDiv" ).hide();
                    $( "#manageInterviewMyQuestionDiv" ).show();
                }
            });
            return false;
        },
        validateOnBlur : false,
        errorMessagePosition : 'top',
        scrollToTopOnError : false // Set this property to true on longer forms
    });

    $.formUtils.addValidator({
        name: 'editQuestionDuration',
        validatorFunction: function (value, $el, config, language, $form) {      
            var hasNoValue;
            var durationExceed;
            $('.editQuestionDuration').each(function(i) {
                if ($(this).val() == '') {
                      hasNoValue = true;
                }
                else {
                    if (parseInt($(this).val())>600 || isNaN($(this).val())) {
                        durationExceed = true;
                    }
                }
            });
            if (hasNoValue==true || durationExceed==true) {
                return false;
            }
            else {
                return true;
            }
            //return parseInt(value, 10) % 2 === 0;
        },
        errorMessage : 'All duration (limit 600 sec) fields must be field '
    });

    $.validate({
        form: '#editQuestionForm',
        modules: 'file',
        onError: function ($form) {
            //alert($('.level1').val());
            return false;
        },
        onSuccess: function ($form) {
            //var allData = $("#editQuestionForm").serialize();
            var form = $('#editQuestionForm')[0];
            var allData = new FormData(form);

            $.ajax({
                url: siteUrl + "interviews/editUpdateQuestion",
                type: "POST",
                enctype: 'multipart/form-data',
                data: allData,
                processData: false,
                contentType: false,
                cache: false,
                success: function (data) {
                    var obj = $.parseJSON(data);
                    var title = obj.title;
                    var duration = obj.duration;
                    var questionId = obj.questionId;

                    if(obj.type == 1) {
                        var type = 'Video';
                    } else {
                        var type = 'Text';
                    }

                    $( "#interviewBreadcrumbText" ).text(' -> My Question Bank');
                    $( ".addInterviewQuestionBreadcrumb" ).remove();
                    $( "#manageInterviewsSetsTables" ).hide();
                    $( "#manageInterviewFjQuestionBankDiv" ).hide();
                    $( "#addInterviewSetDiv" ).hide();
                    $( "#manageInterviewMyQuestionDiv" ).show();
                    $( "#questionBankAddQuestionDiv" ).hide();
                    $( "#editQuestionFormDiv" ).hide();

                    $('#interviewMyQuestionBankTable tbody #interviewMyQuestionTr_' + questionId).find("td").eq(0).html(title);
                    $('#interviewMyQuestionBankTable tbody #interviewMyQuestionTr_' + questionId).find("td").eq(1).html(type);
                    $('#interviewMyQuestionBankTable tbody #interviewMyQuestionTr_' + questionId).find("td").eq(2).html(duration +' Sec.');
                }
            });
            return false;
        },
        validateOnBlur : false,
        errorMessagePosition : 'top',
        scrollToTopOnError : false // Set this property to true on longer forms
    });
</script>



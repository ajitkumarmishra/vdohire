<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Jobs Controller
 * Description : Used to handle all Jobs related data
 * @author Synergy
 * @createddate : Nov 3, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 15, 2017
 */

class Jobs extends MY_Controller {

    /**
     * Responsable for auto load the the coreapi_model, job_model, user_model, userjob_model, corporatereport_model
     * Responsable for auto load the form_validation, session, email library
     * Responsable for auto load fj helper
     * Check Uri Segment to display specific page role wise
     * @return void
     */
    function __construct() {
        parent::__construct();
        $this->load->model('coreapi_model', 'CORE_MODEL');
        $this->load->model('job_model');
        $this->load->model('interview_model');
        $this->load->model('industry_model');
        $this->load->model('question_model');
        $this->load->model('user_model');
        $this->load->model('jobpanel_model');
        $this->load->model('userjob_model');
        $this->load->model('corporatereport_model');
        $this->load->model('assessment_model');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('email');
        $this->load->helper('fj');
        $token = $this->config->item('accessToken');

        /********prevent any type of fraud activity*********/
        if ($this->session->userdata('logged_in')) {
            $userRealData   = $this->session->userdata['logged_in'];
            $uriStringSegment = $this->uri->uri_string();
            $arraySegments = explode('/', $uriStringSegment);
            array_pop($arraySegments);
            $newString = implode('/', $arraySegments);

            if($userRealData->role == 1) {
                $portal = 'SuperAdminPortal';
            }
            elseif ($userRealData->role == 2 || $userRealData->role == 4 || $userRealData->role == 5) {
                $portal = 'CorporatePortal';
            }
            else{
                $portal = '';
            }

            if($uriStringSegment == 'corporate/dashboard') {
                if(!checkUserAccessForWebPage($userRealData->id, $userRealData->role, $portal, 'corporate/dashboard')) {
                    $this->session->sess_destroy();
                    $insertFaultData['userId'] = $userRealData->id;
                    $insertFaultData['comment'] = 'This user try to access this this page';
                    $insertFaultData['pagePath'] = 'corporate/dashboard';
                    $this->db->insert('fj_user_access_log', $insertFaultData);
                    redirect(base_url('users/login'));
                }
            }

            if($uriStringSegment == 'jobs/list') {
                if(!checkUserAccessForWebPage($userRealData->id, $userRealData->role, $portal, 'jobs/list')) {
                    $this->session->sess_destroy();
                    $insertFaultData['userId'] = $userRealData->id;
                    $insertFaultData['comment'] = 'This user try to access this this page';
                    $insertFaultData['pagePath'] = 'jobs/list';
                    $this->db->insert('fj_user_access_log', $insertFaultData);
                    redirect(base_url('users/login'));
                }
            }

            if($uriStringSegment == 'jobs/add') {
                if(!checkUserAccessForWebPage($userRealData->id, $userRealData->role, $portal, 'jobs/add')) {
                    $this->session->sess_destroy();
                    $insertFaultData['userId'] = $userRealData->id;
                    $insertFaultData['comment'] = 'This user try to access this this page';
                    $insertFaultData['pagePath'] = 'jobs/add';
                    $this->db->insert('fj_user_access_log', $insertFaultData);
                    redirect(base_url('users/login'));
                }
            }

            if($newString == 'jobs/edit') {
                if(!checkUserAccessForWebPage($userRealData->id, $userRealData->role, $portal, 'jobs/edit')) {
                    $this->session->sess_destroy();
                    $insertFaultData['userId'] = $userRealData->id;
                    $insertFaultData['comment'] = 'This user try to access this this page';
                    $insertFaultData['pagePath'] = 'jobs/edit';
                    $this->db->insert('fj_user_access_log', $insertFaultData);
                    redirect(base_url('users/login'));
                }
            }
        }
        /********ende of prevent any type of fraud activity*********/
    }

    function getAssessmentAttachedCount() {
        $token      = $this->config->item('accessToken');
        $userData   = $this->session->userdata['logged_in'];
        $userId          = $userData->id;
        $assessmentId = $this->input->post('assessmentId', true);
        //Get information of Assignment
            $arrCompanyResulta = $this->db->query("SELECT count(*) as total, (SELECT count(*) FROM fj_userAnswers WHERE jobId = j.id AND assessmentId = '$assessmentId') interviewCount FROM fj_jobs j JOIN fj_assessments ass ON ass.id = j.assessment WHERE j.status='1' AND j.assessment = '$assessmentId'");
            $arrCompanyDataa = $arrCompanyResulta->row_array();
            echo (int)$arrCompanyDataa['total'];exit;
        //Endo of get information of assignment
    }

    function getInterviewAttachedCount() {
        $token      = $this->config->item('accessToken');
        $userData   = $this->session->userdata['logged_in'];
        $userId          = $userData->id;
        $interviewSetId = $this->input->post('interviewSetId', true);
        //Get information of Assignment
            $arrCompanyResulta = $this->db->query("SELECT count(*) as total, (SELECT count(*) FROM fj_userJob WHERE jobId = j.id) interviewCount FROM fj_jobs j JOIN fj_interviews ass ON ass.id = j.interview WHERE j.status='1' AND j.interview = '$interviewSetId'");
            $arrCompanyDataa = $arrCompanyResulta->row_array();
            echo (int)$arrCompanyDataa['total'];exit;
        //Endo of get information of assignment
    }

    function jobDetails() {
        $jobId = $this->input->post('jobId', true);
        $queryResult = $this->db->query("SELECT count(*) interviewCount, job.interview, job.assessment FROM fj_userJob userjob JOIN fj_jobs job ON job.id = userjob.jobId GROUP BY userjob.jobId HAVING userjob.jobId = '$jobId'");
        $jobData  = $queryResult->row_array();

        echo json_encode(array('jobDetails' => $jobData));
    }
    /**
     * Description : Use to display add job page and create a new job
     * Author : Synergy
     * @param null or $_POST
     * @return render data into view 
     */
    function addJob() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token      = $this->config->item('accessToken');
        $userData   = $this->session->userdata['logged_in'];
        
        if (isset($userData->id) && $userData->id != NULL) {
           $userId          = $userData->id;
           $userByFullName  = $userData->fullname;
           $userCompany     = $userData->company;
           $createdById     = $userData->createdBy;

           //Get company Code
            $arrCompanyResult = $this->db->query("SELECT * FROM fj_company_code WHERE companyId = '$userData->id'");
            $arrCompanyData = $arrCompanyResult->row_array();
            if($arrCompanyData)
                $companyCode = $arrCompanyData['companyCode'];
            else
                $companyCode = '';
        } else {
            $userId = 1;
        }
		
		

        $userRole   = $userData->role;
        
        $jobAssessments = $this->listAssessments();
        $jobIntreviews = $this->listInterviews();
        $questionBanks = listQuestionBank();
        $allRecruitersList = $this->job_model->getjobPendingApplicationData('', '', $userId);


        $info['fjQuestion']     = getFjQuestions();
        $allIndustryList        = $this->industry_model->getIndustryList(1);
        $allFunctionList        = $this->industry_model->getIndustryList(2);        
        $info['fjIndustry']     = $allIndustryList;
        $info['fjFunction']     = $allFunctionList;

        if($userRole=='2' || $userRole=='1') {
            $userValidity   = $userData->validTo;
        }
        else if($userRole=='4') {
            $corporateUser      = getCorporateUser($userId);
            $queryResult        = $this->db->query("SELECT * FROM fj_users WHERE id = '$corporateUser'");
            $corporateUserData  = $queryResult->row_array();
            $userValidity       = $corporateUserData['validTo'];
        }

        //added code for the permission
		//USER'S PERMISSION
		$userPermissionStr = "";
		$userPermissionArr = array();

		$userPermissionRes = dashboardUserPermissions($userId);

		if( $userPermissionRes ){
			$userPermissionStr = $userPermissionRes->permissionIds;
			$userPermissionArr = explode(",", $userPermissionStr);
		}
		//ALL PERMISSION FOR ADMIN
		$adminPermission = array(6,9,10,11,12,8);
		$corporatePermission = array(1,2,3,4,5,7,8);
		//SIDE MENU FOR LISTING
		//till here added code for the permission
		
        if (isset($_POST) && $_POST != NULL) {
            $panel1 = $this->input->post('level1', true);
            $selfL1User = array();
            $selfL1User[] = $userId;

            $panel2 = $this->input->post('level2', true);
            $resultPanel = array_intersect($panel1, $panel2);

            //print '<pre>';print_r($panel1);
            //print '<pre>';print_r($panel2);
            //print '<pre>';print_r($selfL1User);exit;
            $resp['error'] = '';
            $fromEmail = $this->config->item('fromEmail');
            if ($token != $this->input->post('accessToken', true)) {
                $resp['error'] = 'You do not have pemission.';
            }

            $this->form_validation->set_rules('title', 'title', 'trim|required');
            $this->form_validation->set_rules('description', 'description', 'trim|required');

            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }

            if (count($resultPanel) != 0) {
                $resp['error'] .= '<p>Panel 1 user and Panel 2 user can not be same.</p>';
            }
            
            if (!$resp['error']) {

                $data['jobId'] = $this->input->post('jobId', true);
                $data['title'] = $this->input->post('title', true);
                $locations = $this->input->post('cityState', true);
                $data['noOfVacancies'] = $this->input->post('vacancy', true);
                $data['ageFrom'] = $this->input->post('ageFrom', true);
                $data['ageTo'] = $this->input->post('ageTo', true);
                $data['salaryFrom'] = $this->input->post('salaryFrom', true);
                $data['salaryTo'] = $this->input->post('salaryTo', true);
                $data['noticePeriod'] = $this->input->post('noticePeriod', true);
                $data['expFrom'] = $this->input->post('expFrom', true);
                $data['expTo'] = $this->input->post('expTo', true);

                if($this->input->post('openTill', true)) {
                    $data['openTill'] = date("Y-m-d", strtotime($this->input->post('openTill', true)));
                } else {
                    $data['openTill'] = '';
                }
                $data['description'] = $this->input->post('description', true);
                $data['assessment'] = $this->input->post('assessment', true);
                $data['interview'] = $this->input->post('interview', true);
                $data['createdAt'] = date('Y-m-d h:i:s');
                $data['updatedAt'] = date('Y-m-d h:i:s');
                $data['fjCode'] = $companyCode.$this->generateRandomString();
                $data['createdBy'] = $userId;
                $data['posted'] = '2';
                $data['status'] = '1';
                $ind = getUserById($userId);
                $data['industryId'] = 5;

                //$roleName = getRoleById($data['role']);

                $location = $this->input->post('location', true);
                $qualifications = $this->input->post('qualification', true);
                try {
                    $jd = '';
                    if (isset($_FILES['jd']['name']) && !empty($_FILES['jd']['name'])) {
                        $jd = $this->fileUpload($_FILES);
                        if (!$jd) {
                            $resp['message'] = 'There is a server problem occured to upload image. Please make sure image type is png or jpg!';
                        }
                    }

                    if($jd)
                        $data['jd'] = $jd;
                    $resp = $this->job_model->addJob($data);
					
                    if ($resp) {
                        $this->job_model->addLocation($locations, $resp);

                        if($selfL1User)
                            $this->job_model->addJobPanel($selfL1User, $resp, 1);

                        if($panel1)
                            $this->job_model->addJobPanel($panel1, $resp, 1);

                        if($panel2)
                            $this->job_model->addJobPanel($panel2, $resp, 2);

                        if($qualifications)
                            $this->job_model->addJobQualification($qualifications, $resp);

                        $this->email->from($fromEmail, 'VDOHire');
                        $this->email->subject('VDOHire Job');
                        $this->email->set_mailtype('html');

                        if($panel1) {
                            foreach ($panel1 as $item) {
                                $user = getUserById($item);
                                $jobDetailsData = getJobDetail($resp);
                                
                                $notificationMsg = "You are added as a Evaluator L1 for Job ". ucwords($jobDetailsData['title'])." by ".ucwords($userCompany);
                                insertNotificationForTopBarInAdmin($item, $notificationMsg, "Job Review", $userId);

                                $temp['email'] = $user['email'];
                                $temp['jobId'] = $data['jobId'];
                                $temp['type'] = 'Evaluator L1';
                                $this->email->to($item);
                                $body = $this->load->view('emails/panelTemp.php', $temp, TRUE);
                                $this->email->message($body);
                                $this->email->send();
                            }
                        }

                        if($panel2) {
                            foreach ($panel2 as $item) {
                                $user = getUserById($item);
                                $jobDetailsData = getJobDetail($resp);
                                
                                /* notification message for assigned  job to user ( sub-admin ) */
                                $notificationMsg = "You are added as a Evaluator L2 for Job ". ucwords($jobDetailsData['title'])." by ".ucwords($userCompany);
                                insertNotificationForTopBarInAdmin($item, $notificationMsg, "Job Review", $userId);
                                    
                                $temp['email'] = $user['email'];
                                $temp['jobId'] = $data['jobId'];
                                $temp['type'] = 'Evaluator L2';
                                $this->email->to($item);
                                $body = $this->load->view('emails/panelTemp.php', $temp, TRUE);
                                $this->email->message($body);
                                $this->email->send();
                            }
                        }


                        $data['message'] = 'Your job has been successfully created!';
                        $data['resp'] = $resp;
                        $data['assessmentId'] = $this->input->post('assessment', true);
                        $data['interviewId'] = $this->input->post('interview', true);
                        $data['panel1Data'] = $panel1;
                        $data['pane21Data'] = $panel2;

                        if($userRole == 1) {
                            $data['main_content'] = 'fj-create-job';
                        }
                        else {
                            $data['main_content'] = 'fj-create-corporate-job';
                            $data['breadcrumb']     = 'jobs';
                        }
                        $data['token'] = $token;
                        $data['subuser'] = getSubUsers($userId);
                        $data['assessment'] = getAssessmentsByRole($userId, $userRole);
                        $data['interview'] = getInterviewByRole($userId, $userRole);
                        $data['jobAssessments'] = $jobAssessments;
                        $data['jobIntreviews'] = $jobIntreviews;
                        $data['questionBanks'] = $questionBanks;
                        $data['userPermissionArr'] = $userPermissionArr;
                        $data['userRoleForMenu'] = $userRole;
                        $data['allRecruitersList'] = $allRecruitersList;

                        if($userRole == 1)
                            $this->load->view('fj-mainpage', $data);
                        else
                            $this->load->view('fj-mainpage-recuiter', $data);
                    }
                } catch (Exception $e) {
                    $resp['error'] = $e->getMessage();
                }
            } else {
                if($userRole == 1) {
                    $data['main_content'] = 'fj-create-job';
                }
                else {
                    $data['main_content'] = 'fj-create-corporate-job';
                    $data['breadcrumb']     = 'jobs';
                }

                $data['error'] = $resp;
                $data['job'] = $this->input->post();
                $data['city'] = getCities();
                $data['course'] = getCourses();
                $data['assessment'] = getAssessmentsByRole($userId, $userRole);
                $data['interview'] = getInterviewByRole($userId, $userRole);
                $data['files'] = $_FILES;
                $data['token'] = $token;
                $data['subuser'] = getSubUsers($userId);
                $data['jobAssessments'] = $jobAssessments;
                $data['jobIntreviews'] = $jobIntreviews;
                $data['questionBanks'] = $questionBanks;
                $data['userPermissionArr'] = $userPermissionArr;
                $data['userRoleForMenu'] = $userRole;
                $data['allRecruitersList'] = $allRecruitersList;
                if($userRole == 1)
                    $this->load->view('fj-mainpage', $data);
                else
                    $this->load->view('fj-mainpage-recuiter', $data);
            }
        } 
        else {
            $data['city']           = getCities();
            $data['course']         = getCourses();
            $data['assessment']     = getAssessmentsByRole($userId, $userRole);
            $data['interview']      = getInterviewByRole($userId, $userRole);
            $data['subuser']        = getSubUsers($userId);
            //print_r($data['subuser']);exit;
            $data['jobAssessments'] = $jobAssessments;
            $data['jobIntreviews'] = $jobIntreviews;
            $data['questionBanks'] = $questionBanks;
            $data['userPermissionArr'] = $userPermissionArr;
            $data['userRoleForMenu'] = $userRole;
            $data['allRecruitersList'] = $allRecruitersList;

            if($userRole == 1) {
                $data['main_content'] = 'fj-create-job';
            }
            else {
                $data['main_content'] = 'fj-create-corporate-job';
                $data['breadcrumb']     = 'jobs';
            }

            $data['token']          = $token;

            if($userRole == 1)
                $this->load->view('fj-mainpage', $data);
            else
                $this->load->view('fj-mainpage-recuiter', $data);
        }
    }

    /**
     * Description : Use to upload jd(video type) file into jd directory
     * Author : Synergy
     * @param array $files
     * @return string the uploaded file or boolean false.
     */
	function searchJobsApplicants(){
		if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
		$userData = $this->session->userdata['logged_in'];
		
		$userLoggeinId = $userData->id;
		$userRole   = $userData->role;
		
		$allUserStrList = getAllCorporateAndSubuserIds($userLoggeinId, $userRole);
        
		$allUserIdsStr = $allUserStrList;
                //L1 & L2 level job list
        $usersJobL1L2LevelListRes = $this->jobpanel_model->getAllUserJobPenalForUser($allUserIdsStr);
        
		if( $usersJobL1L2LevelListRes['jobIdStr'] != "" ){
			$usersJobListL1L2 = $usersJobL1L2LevelListRes['jobIdStr'];
		} else {
			$usersJobListL1L2 = "";
		} 
		
		$text = $this->input->post('search');
		$data['searchText'] = $text;
		if(!empty($usersJobListL1L2)){
			
		
			$resultsJobs = $this->job_model->listJobsSearch($text,$usersJobListL1L2);
			
			$resultsAppliacnts = $this->job_model->listApplicantsSearch($text,$usersJobListL1L2);
			
			$data['applicantsSearched'] = $resultsAppliacnts;
			$data['Searchedjobs'] = $resultsJobs;
			
			if($userRole == 1) {
				$data['main_content'] = 'fj-edit-job';
			} else {
				$data['main_content'] = 'fj-search-corporate-listing';
				$data['breadcrumb'] = '';
			}
		}else{
			$data['applicantsSearched'] = array();
			$data['Searchedjobs'] = array();
			if($userRole == 1) {
				$data['main_content'] = 'fj-edit-job';
			} else {
				$data['main_content'] = 'fj-search-corporate-listing';
				$data['breadcrumb'] = '';
			}
		}
		if($userRole == 1) {
			$this->load->view('fj-mainpage', $data);
		} else {
			$this->load->view('fj-mainpage-recuiter', $data);
		}
		
		
	}
    function fileUpload($files) {
        $typeArr = explode('/', $files['jd']['type']);
        $imgName = rand() . date('ymdhis') . '_jd.' . $typeArr[1];
        $data['image'] = $imgName;
        $jobImageType = $this->config->item('videoType');
        if (in_array($typeArr[1], $jobImageType)) {
            $uploadResp = $this->job_model->fileUpload($files, 'uploads/jd', $imgName);
            return $uploadResp;
        } else {
            return false;
        }
    }

    /**
     * Description : Use to display edit job page and update the specified job
     * Author : Synergy
     * @param null or $_POST
     * @return render data into view
     */
    function editJob($id) {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $job = $this->job_model->getJob($id);

        $token = $this->config->item('accessToken');
        $userData = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
            $userByFullName = $userData->fullname;
            $userCompany = $userData->company;
            $createdById = $userData->createdBy;
        } else {
            $userId = 1;
        }
        
        $userRole   = $userData->role;
        
        $jobAssessments = $this->listAssessments();
        $jobIntreviews = $this->listInterviews();
        $questionBanks = listQuestionBank();

        $allRecruitersList = $this->job_model->getjobPendingApplicationData('', '', $userId);

        $info['fjQuestion']     = getFjQuestions();
        $allIndustryList        = $this->industry_model->getIndustryList(1);
        $allFunctionList        = $this->industry_model->getIndustryList(2);        
        $info['fjIndustry']     = $allIndustryList;
        $info['fjFunction']     = $allFunctionList;

        if($userRole=='2' || $userRole=='1') {
            $userValidity   = $userData->validTo;
        }
        else if($userRole=='4') {
            $corporateUser      = getCorporateUser($userId);
            $queryResult        = $this->db->query("SELECT * FROM fj_users WHERE id = '$corporateUser'");
            $corporateUserData  = $queryResult->row_array();
            $userValidity       = $corporateUserData['validTo'];
        }
        
        if($userRole == 1) {
            $info['main_content'] = 'fj-edit-job';
        } else {
            $info['main_content'] = 'fj-edit-corporate-job';
            $info['breadcrumb'] = 'jobs';
        }

        
        $info['job'] = $job;
        $info['token'] = $token;
        $info['city'] = getCities();
        $info['course'] = getCourses();
        $info['assessment'] = getAssessmentsByRole($userId, $userRole);
        $info['interview'] = getInterviewByRole($userId, $userRole);
        $info['subuser'] = getSubUsers($userId);
        $jobLocations = getLocationsByJob($id);
        $info['job']['qualification'] = getQualificationsByJob($id);
        $info['jobAssessments'] = $jobAssessments;
        $info['jobIntreviews'] = $jobIntreviews;
        $info['questionBanks'] = $questionBanks;

        //USER'S PERMISSION
        $userPermissionStr = "";
        $userPermissionArr = array();

        $userPermissionRes = dashboardUserPermissions($userId);

        if( $userPermissionRes ){
            $userPermissionStr = $userPermissionRes->permissionIds;
            $userPermissionArr = explode(",", $userPermissionStr);
        }
        //ALL PERMISSION FOR ADMIN
        $adminPermission = array(6,9,10,11,12,8);
        $corporatePermission = array(1,2,3,4,5,7,8);
        //SIDE MENU FOR LISTING

        $info['userPermissionArr'] = $userPermissionArr;
        $info['userRoleForMenu'] = $userRole;

        $j = 0;
        foreach ($jobLocations as $item) {
            $jobLocationsFormatted[$j] = $item['location'];
            $j++;
        }
        $info['job']['cityState'] = $jobLocationsFormatted;
        $info['job']['level1'] = getPanelByJob($id, 1);
        $info['job']['level2'] = getPanelByJob($id, 2);
        $info['allRecruitersList'] = $allRecruitersList;
		
        $j = 0;
        if (isset($_POST) && $_POST != NULL) {
            $resp['error'] = '';
            $fromEmail = $this->config->item('fromEmail');
            if ($token != $this->input->post('accessToken', true)) {
                $resp['error'] = 'You do not have pemission.';
            }
            $panel1 = $this->input->post('level1', true);
            $panel2 = $this->input->post('level2', true);
            $resultPanel = array_intersect($panel1, $panel2);

            $selfL1User = array();
            $selfL1User[] = $userId;

            $this->form_validation->set_rules('title', 'title', 'trim|required');
            $this->form_validation->set_rules('description', 'description', 'trim|required');

            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }
            if (count($resultPanel) != 0) {
                $resp['error'] .= '<p>Panel 1 user and Panel 2 user can not be same.</p>';
            }
            if (!$id) {
                $resp['error'] = 'Undefined job!';
            }
            
            if (!$resp['error']) {
                $data['jobId'] = $this->input->post('jobId', true);
                $data['title'] = $this->input->post('title', true);
                $locations = $this->input->post('cityState', true);
                $data['noOfVacancies'] = $this->input->post('vacancy', true);
                $data['ageFrom'] = $this->input->post('ageFrom', true);
                $data['ageTo'] = $this->input->post('ageTo', true);
                $data['salaryFrom'] = $this->input->post('salaryFrom', true);
                $data['salaryTo'] = $this->input->post('salaryTo', true);
                $data['noticePeriod'] = $this->input->post('noticePeriod', true);
                $qualifications = $this->input->post('qualification', true);
                $data['expFrom'] = $this->input->post('expFrom', true);
                $data['expTo'] = $this->input->post('expTo', true);

                if($this->input->post('openTill', true)) {
                    $data['openTill'] = date("Y-m-d", strtotime($this->input->post('openTill', true)));
                } else {
                    $data['openTill'] = '';
                }

                $data['description'] = $this->input->post('description', true);
                $data['assessment'] = $this->input->post('assessment', true);
                $data['interview'] = $this->input->post('interview', true);
                $data['createdAt'] = date('Y-m-d h:i:s');
                $data['updatedAt'] = date('Y-m-d h:i:s');

                $data['updatedBy'] = $userId;
                //$roleName = getRoleById($data['role']);

                $location = $this->input->post('location', true);
                try {
                    if (isset($_FILES['jd']['name']) && $_FILES['jd']['name'] != NULL) {
                        $jd = $this->fileUpload($_FILES);
                        $data['jd'] = $jd;
                    }
                    $resp = $this->job_model->updateJob($data, $id);
                    if ($resp) {
                        $this->job_model->deleteLocationsByJob($resp);
                        $this->job_model->addLocation($locations, $resp);
                        $this->job_model->deletePanelByJob($resp);

                        if($selfL1User)
                            $this->job_model->addJobPanel($selfL1User, $resp, 1);
                        
                        if($panel1)
                            $this->job_model->addJobPanel($panel1, $resp, 1);

                        if($panel2)
                            $this->job_model->addJobPanel($panel2, $resp, 2);
                        $this->job_model->deleteQualicationsByJob($resp);

                        if($qualifications)
                            $this->job_model->addJobQualification($qualifications, $resp);
                        $data['message'] = 'Successfully updated!';

                        /*if($userRole == 1) {
                            $data['main_content'] = 'fj-edit-job';
                        } else {
                            $data['main_content'] = 'fj-edit-corporate-job';
                            $data['breadcrumb']     = 'jobs';
                        }*/
                        $data['token'] = $token;

                        $level1Panel = $panel1;
                        $level2Panel = $panel2;
                        $this->email->from($fromEmail, 'First Job');
                        $this->email->subject('First Job');

                        $this->email->set_mailtype('html');

                        if($level1Panel) {
                            foreach ($level1Panel as $item) {
                                $user = getUserById($item);
                                
                                $jobDetailsData = getJobDetail($id);
                                
                                /* notification message for assigned  job to user ( sub-admin ) */
                                $notificationMsg = "You are added as a Evaluator L1 for Job ". ucwords($jobDetailsData['title'])." by ".ucwords($userCompany);
                                insertNotificationForTopBarInAdmin($item, $notificationMsg, "Job Review", $userId);
                                
                                $temp['email'] = $user['email'];
                                $temp['jobId'] = $data['jobId'];
                                $temp['type'] = 'Evaluator L1';
                                $this->email->to($item);
                                $body = $this->load->view('emails/panelTemp.php', $temp, TRUE);
                                $this->email->message($body);
                                $this->email->send();
                            }
                        }

                        if($level2Panel) {
                            foreach ($level2Panel as $item) {
                                $user = getUserById($item);                            
                                $jobDetailsData = getJobDetail($id);
                                
                                /* notification message for assigned  job to user ( sub-admin ) */
                                $notificationMsg = "You are added as a Evaluator L2 for Job ". ucwords($jobDetailsData['title'])." by ".ucwords($userCompany);
                                insertNotificationForTopBarInAdmin($item, $notificationMsg, "Job Review", $userId);
                                
                                $temp['email'] = $user['email'];
                                $temp['jobId'] = $data['jobId'];
                                $temp['type'] = 'Evaluator L1';
                                $this->email->to($item);
                                $body = $this->load->view('emails/panelTemp.php', $temp, TRUE);
                                $this->email->message($body);
                                $this->email->send();
                            }
                        }

                        $data['createdJobId'] = $id;
                        $data['assessmentId'] = $this->input->post('assessment', true);
                        $data['interviewId'] = $this->input->post('interview', true);
                        $data['panel1Data'] = $panel1;
                        $data['pane21Data'] = $panel2;

                        if($userRole == 1) {
                            $data['main_content'] = 'fj-edit-job';
                        }
                        else {
                            $data['main_content'] = 'fj-edit-corporate-job';
                            $data['breadcrumb']     = 'jobs';
                        }
                        $data['token'] = $token;
                        $data['subuser'] = getSubUsers($userId);
                        $data['assessment'] = getAssessmentsByRole($userId, $userRole);
                        $data['interview'] = getInterviewByRole($userId, $userRole);
                        $data['jobAssessments'] = $jobAssessments;
                        $data['jobIntreviews'] = $jobIntreviews;
                        $data['questionBanks'] = $questionBanks;
                        $data['userPermissionArr'] = $userPermissionArr;
                        $data['userRoleForMenu'] = $userRole;
                        $data['allRecruitersList'] = $allRecruitersList;
                        if($userRole == 1)
                            $this->load->view('fj-mainpage', $data);
                        else
                            $this->load->view('fj-mainpage-recuiter', $data);

                        //redirect('jobs/list/1', 'refresh');
                    }
                } catch (Exception $e) {
                    $resp['error'] = $e->getMessage();
                }
            } else {
                $info['error'] = $resp;
                $info['job'] = $this->input->post();

                if($userRole == 1) {
                    $this->load->view('fj-mainpage', $info);
                } else {
                    $this->load->view('fj-mainpage-recuiter', $info);
                }
            }
        } else {
            if($userRole == 1) {
                $this->load->view('fj-mainpage', $info);
            } else {
                $this->load->view('fj-mainpage-recuiter', $info);
            }
        }
    }

    /**
     * Description : Use to display edit job page and update the specified job
     * Author : Synergy
     * @param null or $_POST
     * @return render data into view
     */
    function editJobInterviewLevel() {
        $userData = $this->session->userdata['logged_in'];

        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
            $userByFullName = $userData->fullname;
            $userCompany = $userData->company;
            $createdById = $userData->createdBy;
        } else {
            $userId = 1;
        }
        
        $userRole   = $userData->role;
        
        if (isset($_POST) && $_POST != NULL) {
            
            $id = $_POST['jobIdCreated'];
            $assessment = $_POST['assessment'];
            $interview = $_POST['interview'];

            $panel1 = $_POST['level1'];
            $panel2 = $_POST['level2'];
            
            $job = $this->job_model->getJob($id);
            $token = $this->config->item('accessToken');

            
            $data['jobId'] = $id;

            if($assessment) {
                $data['assessment'] = $assessment;
            }

            if($interview) {
                $data['interview'] = $interview;
            }

            $data['updatedAt'] = date('Y-m-d h:i:s');
            $data['updatedBy'] = $userId;

            $resp = $this->job_model->updateJob($data, $id);

            if($resp) {
                $this->email->from($fromEmail, 'First Job');
                $this->email->subject('First Job');
                $this->email->set_mailtype('html');

                if($panel1) {
                    $this->job_model->addJobPanel($panel1, $resp, 1);
                    $level1Panel = $panel1;

                    foreach ($level1Panel as $item) {
                        $user = getUserById($item);
                        
                        $jobDetailsData = getJobDetail($id);
                        
                        /* notification message for assigned  job to user ( sub-admin ) */
                        $notificationMsg = "You are added as a Evaluator L1 for Job ". ucwords($jobDetailsData['title'])." by ".ucwords($userCompany);
                        insertNotificationForTopBarInAdmin($item, $notificationMsg, "Job Review", $userId);
                        
                        $temp['email'] = $user['email'];
                        $temp['jobId'] = $data['jobId'];
                        $temp['type'] = 'Evaluator L1';
                        $this->email->to($item);
                        $body = $this->load->view('emails/panelTemp.php', $temp, TRUE);
                        $this->email->message($body);
                        $this->email->send();
                    }
                }

                if($panel2) {
                    $this->job_model->addJobPanel($panel2, $resp, 2);
                    $level2Panel = $panel2;

                    foreach ($level2Panel as $item) {
                        $user = getUserById($item);                            
                        $jobDetailsData = getJobDetail($id);
                        
                        /* notification message for assigned  job to user ( sub-admin ) */
                        $notificationMsg = "You are added as a Evaluator L2 for Job ". ucwords($jobDetailsData['title'])." by ".ucwords($userCompany);
                        insertNotificationForTopBarInAdmin($item, $notificationMsg, "Job Review", $userId);
                        
                        $temp['email'] = $user['email'];
                        $temp['jobId'] = $data['jobId'];
                        $temp['type'] = 'Evaluator L1';
                        $this->email->to($item);
                        $body = $this->load->view('emails/panelTemp.php', $temp, TRUE);
                        $this->email->message($body);
                        $this->email->send();
                    }
                }
            }
            echo $id;exit;
        }
    }

    /**
     * Description : Use to delete a specified job
     * Author : Synergy
     * @param array $_POST
     * @return string the success message | error message.
     */
    function deleteJob() {
        $resp['error'] = 0;
        $token = $this->config->item('accessToken');
        $fromEmail = $this->config->item('fromEmail');
        $jobId = $this->input->post('id', true);
        if (!$jobId) {
            echo $error = 'Undefined job!';
            exit;
        }
        if (!$resp['error']) {
            $data['status'] = 3;
            $data['deletedDate'] = date('Y-m-d H:i:s');
            $resp = $this->job_model->updateJob($data, $jobId);
            if ($resp) {
                $this->job_model->deletePanelByJob($jobId);
                echo "Deleted successfully";
            }
        }
    }

    /**
     * Description : Use to delete a specified job
     * Author : Synergy
     * @param array $_POST
     * @return string the success message | error message.
     */
    function closeApplicationForJob() {
        $resp['error'] = 0;
        $token = $this->config->item('accessToken');
        $fromEmail = $this->config->item('fromEmail');
        $jobId = $this->input->post('id', true);
        if (!$jobId) {
            echo $error = 'Undefined job!';
            exit;
        }
        if (!$resp['error']) {
            $data['status'] = 5;
            $resp = $this->job_model->updateJob($data, $jobId);
            echo "Application closed for this job";
            /*if ($resp) {
                $this->job_model->deletePanelByJob($jobId);
                echo "Deleted successfully";
            }*/
        }
    }

    /**
     * Description : Use to delete a specified job
     * Author : Synergy
     * @param array $_POST
     * @return string the success message | error message.
     */
    function closeJob() {
        $resp['error'] = 0;
        $token = $this->config->item('accessToken');
        $fromEmail = $this->config->item('fromEmail');
        $jobId = $this->input->post('id', true);
        if (!$jobId) {
            echo $error = 'Undefined job!';
            exit;
        }
        if (!$resp['error']) {
            $data['status'] = 6;
            $data['closedDate'] = date('Y-m-d H:i:s');
            $resp = $this->job_model->updateJob($data, $jobId);
            echo "The specified job has been closed successfully!";
            /*if ($resp) {
                $this->job_model->deletePanelByJob($jobId);
                echo "Deleted successfully";
            }*/
        }
    }

    /**
     * Description : Use to check specified job is live or not
     * Author : Synergy
     * @param int $jobId
     * @return boolean.
     */
    function checkJobInLiveJob($jobId) {
        if (checkJobInLiveJob($jobId)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Description : Use to get details of a particular job
     * Author : Synergy
     * @param int $id
     * @return render job data into view
     */
    function getJob($id) {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $itemsPerPage           = $this->config->item('itemsPerPage');
        $data['itemsPerPage']   = $itemsPerPage;
        
        $job            = $this->job_model->getJob($id);
        $jobApplication = $this->job_model->getJobApplication($id, $itemsPerPage, $offset);
        
        $token                      = $this->config->item('accessToken');        
        $data['main_content']       = 'fj-view-job';
        $data['job']                = $job;
        $data['jobApplication']     = $jobApplication;
        $data['token']              = $token;
        $data['job']['locations']   = getLocationsByJob($id);
        $this->load->view('fj-mainpage', $data);
    }
    
    /**
     * Description : Use to get details of an assessment attepmted by app user
     * Author : Synergy
     * @param array $_POST(userId, jobId)
     * @return arry of assessment detail
     */
    function userAssessmentDetail() {
        $userId = trim($this->input->post('userId', true));
        $jobId  = trim($this->input->post('jobId', true));
        $interviewVideo = trim($this->input->post('interviewVideo', true));
        $jobApplicationDetail           = $this->job_model->userAssessmentDetail($userId, $jobId);
        $data['jobApplicationDetail']   = $jobApplicationDetail;
        $details = '';
        
        $countQuestion = 0;
        $correctAnswer = 0;
        if(count($data)>0){
            $details .= "<table class='table'>
                            <thead class='tbl_head'>
                                <tr>
                                    <th>Assessment Question</th>
                                    <th>User Answer</th>
                                    <th>Correct Answer</th>
                                </tr>
                            </thead>                            
                            <tbody>";
            foreach($jobApplicationDetail as $assessmentDetail) {
                $countQuestion++;
                $color="style='color:#ff0000';";
                if($assessmentDetail->userOption==$assessmentDetail->option) { $correctAnswer++; $color="style='color:#000000';"; }
                $details .= "
                        <tr>
                            <td>".$assessmentDetail->title."</td>
                            <td ".$color.">".($assessmentDetail->userOption!=''?$assessmentDetail->userOption:'Not Attempted')."</td>
                            <td>".$assessmentDetail->option."</td>
                        </tr>
                ";
            }
            
            if($countQuestion>0)
            $percent = round(($correctAnswer/$countQuestion)*100,2);
            else
            $percent = 'No Assessment Set is connected with this job.';
            
            $details .= "
                    </tbody>
                </table>";
            
            $details .= "<center><h3 style='color:#1ea990;'>Result - ".$percent."%</h3></center>";
            
            
        }
        print_r($details);
    }
    
    /**
     * Description : Use to get details of an job attepmted by app user
     * Author : Synergy
     * @param array $_POST(userId, jobId)
     * @return arry of user attempt data
     */
    function userInterview($userId, $jobId) {
        $jobApplicationDetail           = $this->job_model->userInterview($userId, $jobId);
        $data['jobApplicationDetail']   = $jobApplicationDetail;
        print_r($data);
    }

    /**
     * Description : Use to get list of jobs created by specific corporate user
     * Author : Synergy
     * @param array $_POST(serachColumn, serachText)
     * @return render list of jobs in view
     */
    function listJobs($page = null) {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }

        $userData = $this->session->userdata['logged_in'];

        $userRole = $userData->role;
        $userLoggedinUserId = $userData->id;
        $data['page'] = $page;
        $resp['error'] = 0;
        
        if($_POST){
            if(isset($_POST['submit']) && $_POST['submit'] == 'submit') {
                $sortJobsResult = trim($this->input->post('sortJobsResult', true));
                $serachColumn = $this->input->post('serachColumn', true);
                $searchText = trim($this->input->post('serachText', true));
                $sortingOrder = trim($this->input->post('sortingOrder', true));
            } else {
                $sortJobsResult = '';
                $serachColumn = '';
                $searchText = '';
                $sortingOrder = '';
            }
            
            $this->session->set_userdata('serachColumn', $serachColumn);
            $this->session->set_userdata('searchText', $searchText);
            $this->session->set_userdata('sortJobsResult', $sortJobsResult);
            $this->session->set_userdata('sortingOrder', $sortingOrder);
        } else {
            $serachColumn   = $this->session->userdata('serachColumn');
            $searchText     = $this->session->userdata('searchText');
            $sortJobsResult     = $this->session->userdata('sortJobsResult');
            $sortingOrder = $this->session->userdata('sortingOrder');
        }

        $token = $this->config->item('accessToken');
        $fromEmail = $this->config->item('fromEmail');
        $itemsPerPage = $this->config->item('itemsPerPage');
        $data['itemsPerPage'] = $itemsPerPage;
        $createdBy = NULL;

        if (!$page) {
            $page = 0;
        } else {
            $page = $page - 1;
        }
        $offset = $page * $itemsPerPage;
        try {
            
            $createdByStr = getAllCorporateAndSubuserIds($userLoggedinUserId, $userRole);
            
            $result = $this->job_model->listJobs($itemsPerPage, $offset, $serachColumn, $searchText, $createdByStr, $userRole, $sortJobsResult, $sortingOrder);

            $completeResult = $this->job_model->listJobs($itemsPerPage, $offset, $serachColumn, $searchText, $createdByStr, $userRole, $sortJobsResult, $sortingOrder);
            $count                  = ceil(count($completeResult) / $itemsPerPage);
            //echo $count;exit;
            $data['count']          = $count;
            $data['totalCount']     = count($completeResult);
            $data['content']        = $result;

            if($userRole == 1) {
                $data['main_content']   = 'fj-job-listing';
            } else {
                $data['breadcrumb']     = 'jobs';
                $data['main_content']   = 'fj-job-corporate-listing';
            }

            $data['searchText']     = $searchText;
            $data['serachColumn']   = $serachColumn;
            $data['sortJobsResult'] = $sortJobsResult;
            $data['token']          = $token;
            $data['city']           = getCities();
            $data['userId'] = $userData->id;
            
            $inviteUsersPath = $this->config->item('inviteUsersPath');
            $path = $inviteUsersPath . 'inviteUsers' . $userData->id . '.csv';
            if (file_exists($path)) {
                $data['fileEist'] = 1;
            } else {
                $data['fileEist'] = 0;
            }

			//added code for the permission
			$userId = getUserId();
			$userDet = getUserById($userId);
			$userRoleForMenu = $userDet['role'];

			if( $userRoleForMenu == 1 ) {
				$urlDashborad = 'superadmin/dashboard';
			}
			else if( $userRoleForMenu == 2 || $userRoleForMenu == 4 || $userRoleForMenu == 5 ) {
				$urlDashborad = 'corporate/dashboard'; 
			}

			//USER'S PERMISSION
			$userPermissionStr = "";
			$userPermissionArr = array();

			$userPermissionRes = dashboardUserPermissions($userId);

			if( $userPermissionRes ){
				$userPermissionStr = $userPermissionRes->permissionIds;
				$userPermissionArr = explode(",", $userPermissionStr);
			}
			//ALL PERMISSION FOR ADMIN
			$adminPermission = array(6,9,10,11,12,8);
			$corporatePermission = array(1,2,3,4,5,7,8);
			//SIDE MENU FOR LISTING
			$sideBarMenu = getMenuForSideBar();

			$currentFullBaseUrl = base_url(uri_string());
			$currentBaseUrl = str_replace(base_url(), "", $currentFullBaseUrl);
			$data['userPermissionArr'] = $userPermissionArr;
			$data['userRoleForMenu'] = $userRoleForMenu;
			//till here added code for the permission

            if($userRole == 1)
                $this->load->view('fj-mainpage', $data);
            else
                $this->load->view('fj-mainpage-recuiter', $data);
            if (!$result) {
                //throw new Exception('Could not get list of users!');
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Description : Use to get list of jobs created by specific corporate user
     * Author : Synergy
     * @param array $_POST(serachColumn, serachText)
     * @return render list of jobs in view
     */
    function listPastJobs($page = null) {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }

        $userData = $this->session->userdata['logged_in'];

        $userRole = $userData->role;
        $userLoggedinUserId = $userData->id;
        $data['page'] = $page;
        $resp['error'] = 0;
        
        if($_POST){
            if(isset($_POST['submit']) && $_POST['submit'] == 'submit') {
                $sortJobsResult = trim($this->input->post('sortJobsResult', true));
                $serachColumn = $this->input->post('serachColumn', true);
                $searchText = trim($this->input->post('serachText', true));
                $sortingOrder = trim($this->input->post('sortingOrder', true));
            } else {
                $sortJobsResult = '';
                $serachColumn = '';
                $searchText = '';
                $sortingOrder = '';
            }
            
            
            $this->session->set_userdata('serachColumnPast', $serachColumn);
            $this->session->set_userdata('searchTextPast', $searchText);
            $this->session->set_userdata('sortJobsResultPast', $sortJobsResult);
            $this->session->set_userdata('sortingOrderPast', $sortingOrder);
        } else {
            $serachColumn   = $this->session->userdata('serachColumnPast');
            $searchText     = $this->session->userdata('searchTextPast');
            $sortJobsResult     = $this->session->userdata('sortJobsResultPast');
            $sortingOrder = $this->session->userdata('sortingOrderPast');
        }

        $token = $this->config->item('accessToken');
        $fromEmail = $this->config->item('fromEmail');
        $itemsPerPage = $this->config->item('itemsPerPage');
        $data['itemsPerPage'] = $itemsPerPage;
        $createdBy = NULL;

        if (!$page) {
            $page = 0;
        } else {
            $page = $page - 1;
        }
        $offset = $page * $itemsPerPage;
        try {
            
            $createdByStr = getAllCorporateAndSubuserIds($userLoggedinUserId, $userRole);
            
            $result = $this->job_model->listJobs($itemsPerPage, $offset, $serachColumn, $searchText, $createdByStr, $userRole, $sortJobsResult, $sortingOrder, 'pastJobs');

            $completeResult = $this->job_model->listJobs($itemsPerPage, $offset, $serachColumn, $searchText, $createdByStr, $userRole, $sortJobsResult, $sortingOrder, 'pastJobs');
            $count                  = ceil(count($completeResult) / $itemsPerPage);
            $data['count']          = $count;
            $data['totalCount']     = count($completeResult);
            $data['content']        = $result;

            if($userRole == 1) {
                $data['main_content']   = 'fj-job-listing';
            } else {
                $data['breadcrumb']     = 'jobs';
                $data['main_content']   = 'fj-past-job-corporate-listing';
            }

            $data['searchTextPast']     = $searchText;
            $data['serachColumnPast']   = $serachColumn;
            $data['sortJobsResultPast'] = $sortJobsResult;
            $data['token']          = $token;
            $data['city']           = getCities();
			
			//added code for the permission
			$userId = getUserId();
			$userDet = getUserById($userId);
			$userRoleForMenu = $userDet['role'];

			if( $userRoleForMenu == 1 ) {
				$urlDashborad = 'superadmin/dashboard';
			}
			else if( $userRoleForMenu == 2 || $userRoleForMenu == 4 || $userRoleForMenu == 5 ) {
				$urlDashborad = 'corporate/dashboard'; 
			}

			//USER'S PERMISSION
			$userPermissionStr = "";
			$userPermissionArr = array();

			$userPermissionRes = dashboardUserPermissions($userId);

			if( $userPermissionRes ){
				$userPermissionStr = $userPermissionRes->permissionIds;
				$userPermissionArr = explode(",", $userPermissionStr);
			}
			//ALL PERMISSION FOR ADMIN
			$adminPermission = array(6,9,10,11,12,8);
			$corporatePermission = array(1,2,3,4,5,7,8);
			//SIDE MENU FOR LISTING
			$sideBarMenu = getMenuForSideBar();

			$currentFullBaseUrl = base_url(uri_string());
			$currentBaseUrl = str_replace(base_url(), "", $currentFullBaseUrl);
			$data['userPermissionArr'] = $userPermissionArr;
			$data['userRoleForMenu'] = $userRoleForMenu;
			//till here added code for the permission

            if($userRole == 1)
                $this->load->view('fj-mainpage', $data);
            else
                $this->load->view('fj-mainpage-recuiter', $data);
            if (!$result) {
                //throw new Exception('Could not get list of users!');
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Description : Use to Validates job credentails and sign in the system
     * Author : Synergy
     * @param array $_POST(email, password)
     * @return array or string
     */
    function login() {
        $resp['error'] = 0;
        $email = $this->input->post('email', true);
        $password = $this->input->post('password', true);
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        if ($this->form_validation->run() == false) {
            $resp['error'] = validation_errors();
        }
        try {
            if (!$resp['error']) {
                $jobdata = $this->job_model->validateLogin($email, $password);
                if ($jobdata) {
                    $jobdata->token = $this->getJwtToken($jobdata->id);
                    $this->session->set_jobdata($jobdata);
                } else {
                    $resp['msg'] = "Email and password do not match!";
                }
            } else {
                print_r($resp);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
     * Discription : Gets jwt token by job id.
     * Author : Synergy
     * @param int $jobId
     * @return string
     */
    function getJwtToken($jobId) {
        $token = array();
        $token['id'] = $jobId;
        $jwtKey = $this->config->item('jwtKey');
        $secretKey = base64_decode($jwtKey);
        $signatureAlgo = $this->config->item('signatureAlgorithm');
        return JWT::encode($token, $secretKey, $signatureAlgo);
    }

    /**
     * Discription : Gets value by token
     * Author : Synergy
     * @param string $jwtToken
     * @return string
     */
    function getValueByToken($jwtToken) {
        $jwtKey = $this->config->item('jwtKey');
        $secretKey = base64_decode($jwtKey);
        $signatureAlgo = $this->config->item('signatureAlgorithm');
        $token = JWT::decode($jwtToken, $secretKey, $signatureAlgo);
        return $token->id;
    }

    /*function generateRandomString($length = 6) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }*/

    /**
     * Discription : Use to get random numbers between 0 to 9
     * Author : Synergy
     * @param int $length
     * @return string
     */
    function generateRandomString($length = 6) {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * Discription : Use to update job as posted so that job is searchable in app
     * Author : Synergy
     * @param array $_POST(jobId, posted)
     * @return string
     */
    function postJob() {
        $jobId = $this->input->post('jobId', true);
        $posted = $this->input->post('posted', true);

        $job = $this->job_model->getJob($jobId);
        $openTillDate = strtotime($job['openTill']);
        $currentDate = strtotime(date('Y-m-d'));

        //$returnType = '';
        $level1Panel = getPanelByJob($jobId, 1);
        if(empty($job['interview'])) {
            $returnType = 1;
        } /*else if($openTillDate < $currentDate) {
            $returnType = 2;
        }*/ 
        else if(empty($level1Panel)) {
            $returnType = 3;
        } else {
            $data['posted'] = $posted;
            $job = $this->job_model->updateJob($data, $jobId);
            if ($posted == 1) {
                $returnType = 4;
            } else {
                $returnType = 5;
            }
        }
        echo $returnType;exit;
    }

    /**
     * Discription : Use to get status of job i.e job is active or inactive
     * Author : Synergy
     * @param array $_POST(jobId, status)
     * @return string
     */
    function statusJob() {
        $jobId = $this->input->post('jobId', true);
        $status = $this->input->post('status', true);
        $data['status'] = $status;
        $job = $this->job_model->updateJob($data, $jobId);
        if ($status == 1) {
            echo "Active";
        } else {
            echo "Inactive";
        }
        return;
    }

    /**
     * Discription : Use to display a page Dashboard for Corporate user in which all the active jobs, jobs count, job application etc will display
     * Author : Synergy
     * @param int $page, array $_POST(serachColumn, serachText)
     * @return render job data into view
     */
    function corporateDashboard($page = null) {
		
        if (!$this->session->userdata('logged_in')) {
            //redirect(base_url('users/login'));
        }
		
        $userData = $this->session->userdata['logged_in'];
		
        $userRole = $userData->role;
        $userLoggedinUserId = $userData->id;
        $data['userRole'] = $userRole;
        $data['page'] = $page;
        $resp['error'] = 0;
		
		
        $serachColumn = $this->input->post('serachColumn', true);
        $searchText = $this->input->post('serachText', true);
        $token = $this->config->item('accessToken');
        $fromEmail = $this->config->item('fromEmail');
        $itemsPerPage = $this->config->item('itemsPerPage');
        $data['itemsPerPage'] = $itemsPerPage;
        $createdBy = NULL;

        if (!$page) {
            $page = 0;
        } else {
            $page = $page - 1;
        }
        $offset = $page * $itemsPerPage;
		
        try {

            if ($userRole != 1){
                if ($userRole == 2){
					$usersJobL1L2LevelListRes = $this->jobpanel_model->getUserJobPenalForUser($userLoggedinUserId);
					
					if( $usersJobL1L2LevelListRes['jobIdStr'] != "" ){
						$usersJobListL1L2 = $usersJobL1L2LevelListRes['jobIdStr'];
                        $usersJobListL1 = "";
                        $usersJobDoneListL2 = "";
					} else {
						$usersJobListL1L2 = "";
                        $usersJobListL1 = "";
                        $usersJobDoneListL2 = "";
					} 
					
                }elseif ($userRole == 4 || $userRole == 5){
					//L1 level job list
					$usersJobL1LevelListRes = $this->jobpanel_model->getUserJobPenalForUser($userLoggedinUserId, 1);
					
					if( $usersJobL1LevelListRes['jobIdStr'] != "" ){
						$usersJobListL1 = $usersJobL1LevelListRes['jobIdStr'];
						$userLevel      = 1;
					} else {
						$usersJobListL1 = "";
					}

					//L2 level job list
					$usersJobL2LevelListRes = $this->jobpanel_model->getUserJobPenalForUser($userLoggedinUserId, 2);
					
					if( $usersJobL2LevelListRes['jobIdStr'] != "" ){
						$usersJobListL2     = $usersJobL2LevelListRes['jobIdStr'];    
					
						$allJobDoneByL2Res  = $this->userjob_model->allDoneJobsByL1($usersJobListL2);
						
						$usersJobDoneListL2 = $allJobDoneByL2Res['jobIdL2Str'];                    
						$userLevel          = 2;
					} else {
						$usersJobListL2     = "";
						$usersJobDoneListL2 = "";
					}
					$usersJobListL1L2 = "";
					$createdBy = "";
					
				} else{
                    $createdBy = "";
                }
            } else{
                $createdBy = "";
            }
			
            if ($createdBy != "") {
                $createdByArr = explode(",", $createdBy);
                $createdByArr = array_filter($createdByArr);
                $createdByStr = implode(",", $createdByArr);
            } else {
                $createdByStr = "";
            }
			if( $userRole == 2  || $userRole == 4 || $userRole == 5) {
				$result          = $this->job_model->listJobsNew($itemsPerPage, $offset, $serachColumn, $searchText, $createdBy, $usersJobListL1, $usersJobDoneListL2, $usersJobListL1L2, $userRole);
			}else{
				$result = $this->job_model->listJobs($itemsPerPage, $offset, $serachColumn, $searchText, $createdByStr, $userRole);
			}
			
			$completeResult = $this->job_model->listJobs(NULL, NULL, NULL, NULL, $createdByStr, $userRole, NULL, NULL, NULL);
            $count = ceil(count($completeResult) / $itemsPerPage);
            $data['count'] = $count;
            $data['totalCount'] = count($completeResult);
            $data['content'] = $result;
            $data['main_content'] = 'fj-corporate-dashboard-v2';
            $data['token'] = $token;
            $data['userId'] = $userData->id;
            $data['AdminLoggedInuserId'] = $userData->id;
            $data['userData'] = $userData;
            $data['fileEist'] = 0;
            $inviteUsersPath = $this->config->item('inviteUsersPath');
            $path = $inviteUsersPath . 'inviteUsers' . $userData->id . '.csv';
            if (file_exists($path)) {
                $data['fileEist'] = 1;
            }
            
            $data['openJobs']               = $this->corporatereport_model->openJobs($userLoggedinUserId);
            $data['totalJobs']              = $this->corporatereport_model->totalJobs($userLoggedinUserId);
            $data['lastWeekJobs']           = $this->corporatereport_model->lastWeekJobs($userLoggedinUserId);
            $data['lastMonthJobs']          = $this->corporatereport_model->lastMonthJobs($userLoggedinUserId);
            
            $data['totalApplication']       = $this->corporatereport_model->totalApplication($userLoggedinUserId);
            $data['pendingApplication']     = $this->corporatereport_model->pendingApplication($userLoggedinUserId);
            $data['shortlistedApplication'] = $this->corporatereport_model->shortlistedApplication($userLoggedinUserId);
            $data['rejectedApplication']    = $this->corporatereport_model->rejectedApplication($userLoggedinUserId);
            
            $data['highestPendingJob']      = $this->corporatereport_model->highestPendingJob($userLoggedinUserId);
            $data['longestOpenJob']         = $this->corporatereport_model->longestOpenJob($userLoggedinUserId);            
            $data['leastApplicationJob']    = $this->corporatereport_model->leastApplicationJob($userLoggedinUserId);
            
            //$data['totalUser']              = $this->corporatereport_model->totalUser($userLoggedinUserId);
            $data['manageJobs']             = $this->corporatereport_model->manageJobs($userLoggedinUserId);
            $data['manageInterview']        = $this->corporatereport_model->manageInterview($userLoggedinUserId);
            $data['manageAssessment']       = $this->corporatereport_model->manageAssessment($userLoggedinUserId);
            $data['queueManagement']        = $this->corporatereport_model->queueManagement($userLoggedinUserId);
            $data['searchCandidate']        = $this->corporatereport_model->searchCandidate($userLoggedinUserId);
            
            
            
            
            $data['assignedJobs']                   = $this->corporatereport_model->assignedJobs($userLoggedinUserId);
            $data['assignedActiveJobs']             = $this->corporatereport_model->assignedActiveJobs($userLoggedinUserId);
            $data['assignedLastWeekJobs']           = $this->corporatereport_model->assignedLastWeekJobs($userLoggedinUserId);
            $data['assignedLastMonthJobs']          = $this->corporatereport_model->assignedLastMonthJobs($userLoggedinUserId);
            
            $data['assignedTotalApplication']       = $this->corporatereport_model->assignedTotalApplication($userLoggedinUserId);
            $data['assignedPendingApplication']     = $this->corporatereport_model->assignedPendingApplication($userLoggedinUserId);
            $data['assignedShortlistedApplication'] = $this->corporatereport_model->assignedShortlistedApplication($userLoggedinUserId);
            $data['assignedRejectedApplication']    = $this->corporatereport_model->assignedRejectedApplication($userLoggedinUserId);
            
            $data['assignedAsL1']                   = $this->corporatereport_model->assignedAsL1($userLoggedinUserId);
            $data['assignedAsL2']                   = $this->corporatereport_model->assignedAsL2($userLoggedinUserId);
            
            $data['assignedHighestPendingJob']      = $this->corporatereport_model->assignedHighestPendingJob($userLoggedinUserId);
            $data['assignedLongestOpenJob']         = $this->corporatereport_model->assignedLongestOpenJob($userLoggedinUserId);
            $data['assignedLeastApplicationJob']    = $this->corporatereport_model->assignedLeastApplicationJob($userLoggedinUserId);
			
			//added code for the permission
			$userId = getUserId();
			$userDet = getUserById($userId);
			$userRoleForMenu = $userDet['role'];

			if( $userRoleForMenu == 1 ) {
				$urlDashborad = 'superadmin/dashboard';
			}
			else if( $userRoleForMenu == 2 || $userRoleForMenu == 4 || $userRoleForMenu == 5 ) {
				$urlDashborad = 'corporate/dashboard'; 
			}

			//USER'S PERMISSION
			$userPermissionStr = "";
			$userPermissionArr = array();

			$userPermissionRes = dashboardUserPermissions($userId);

			if( $userPermissionRes ){
				$userPermissionStr = $userPermissionRes->permissionIds;
				$userPermissionArr = explode(",", $userPermissionStr);
			}
			//ALL PERMISSION FOR ADMIN
			$adminPermission = array(6,9,10,11,12,8);
			$corporatePermission = array(1,2,3,4,5,7,8);
			//SIDE MENU FOR LISTING
			$sideBarMenu = getMenuForSideBar();

			$currentFullBaseUrl = base_url(uri_string());
			$currentBaseUrl = str_replace(base_url(), "", $currentFullBaseUrl);
			$data['userPermissionArr'] = $userPermissionArr;
			$data['userRoleForMenu'] = $userRoleForMenu;
			
			//till here added code for the permission
			
			if($userRole == 2 || $userRole == 4  || $userRole == 5){
				$this->load->view('fj-mainpage-recuiter', $data); //die();
			}else{
				$this->load->view('fj-mainpage', $data); //die();
			}
            if (!$result) {
                //throw new Exception('Could not get list of users!');
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Discription : Use to replicate a job
     * Author : Synergy
     * @param array $_POST(jobId)
     * @return string
     */
    function replicate() {
        $jobId              = $this->input->post('jobId', true);        
        $job                = $this->job_model->getJob($jobId);
        $jobLocations       = getLocationsByJob($jobId);
        $jobQualification   = getQualificationsByJob($jobId);
        $userData = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;

            $arrCompanyResult = $this->db->query("SELECT * FROM fj_company_code WHERE companyId = '$userData->id'");
            $arrCompanyData = $arrCompanyResult->row_array();
            if($arrCompanyData)
                $companyCode = $arrCompanyData['companyCode'];
            else
                $companyCode = '';

        } else {
            $userId = 1;
        }
        
        $data['jobId']              = $job['jobId'];
        $data['title']              = $job['title'];
        $data['description']        = $job['description'];
        $data['noOfVacancies']      = $job['noOfVacancies'];
        $data['ageFrom']            = $job['ageFrom'];
        $data['ageTo']              = $job['ageTo'];
        $data['salaryFrom']         = $job['salaryFrom'];
        $data['salaryTo']           = $job['salaryTo'];
        $data['noticePeriod']       = $job['noticePeriod'];
        $data['expFrom']            = $job['expFrom'];
        $data['expTo']              = $job['expTo'];
        $data['openTill']           = $job['openTill'];
        $data['jd']                 = $job['jd'];
        $data['jdThumbnail']        = $job['jdThumbnail'];
        $data['interview']          = $job['interview'];
        $data['assessment']         = $job['assessment'];
        $data['preInterviewText']   = $job['preInterviewText'];
        $data['postInterviewText']  = $job['postInterviewText'];
        $data['preAssessmentText']  = $job['preAssessmentText'];
        $data['postAssessmentText'] = $job['postAssessmentText'];
        $data['industryId']         = $job['industryId'];
        $data['deptId']             = $job['deptId'];
        $data['status']             = '1';
        $data['posted']             = '2';        
        $data['createdAt']          = date('Y-m-d h:i:s');
        $data['updatedAt']          = date('Y-m-d h:i:s');
        $data['fjCode'] = $companyCode.$this->generateRandomString();
        $data['createdBy']          = $userId;
        
        $resp = $this->job_model->addJob($data);
        
        if(count($jobLocations)>0) {
            foreach($jobLocations as $location) {
                $locations[] = $location['location'];
            }
            $this->job_model->addLocation($locations, $resp);
        }
        
        if(count($jobQualification)>0) {
            $this->job_model->addJobQualification($jobQualification, $resp);
        }

        echo $resp;
    }

    /**
     * Discription : Use to merge interview video which are given by user to display all interview video into one video
     * Author : Synergy
     * @param array $_POST(jobId, userId)
     * @return string
     */
    function generateVideo(){
        $jobId  = $this->input->post('jobId', true);
        $userId = $this->input->post('userId', true);
        $job    = $this->job_model->generateVideo($userId, $jobId);

        if ($job == "success") {
            return "success";
        } else {
            return "fail";
        } 
    }

    /**
     * Discription : Use to display job interview video in flashplayer plugin
     * Author : Synergy
     * @param array $_POST(interviewVideo, userJob, appUserName, appUserJob)
     * @return html contents
     */
    function userVideo() {
		
        $userId    = trim($this->input->post('userId', true));
        $jobId    = trim($this->input->post('jobId', true));
        $videoName  = trim($this->input->post('interviewVideo', true));        
        $userJob    = trim($this->input->post('userJob', true));
        $ENCvideo   = encryptURLparam($videoName, URLparamKey);
        $ENCuserJob = encryptURLparam($userJob, URLparamKey);       
        $appUserName= trim($this->input->post('appUserName', true));       
        $appUserJob = trim($this->input->post('appUserJob', true));
        $jobvalue = getJobDetail($jobId);
		
		$userData = $this->session->userdata['logged_in'];
        
        $userLoggeinId = $userData->id;
        $userRole = $userData->role;
        //Update userjob table after view of user interview
		if($userRole == 2){
			$updateData = array(
				   'viewed' => 1
				);

			$this->db->update('fj_userJob', $updateData, array('userId' => $userId, 'jobId' => $jobId));
		}
		if($videoName == ""){
			//getAllInterviewAnswerDetails
			$statictoken = 'ecbcd7eaee29848978134beeecdfbc7c';
			$base_url = base_url().'Fjapi';
			
			$json = array("token" => $statictoken, "methodname" => 'getUserJobQuestionsAnswers', 'jobId' => $jobId, 'userId' => $userId);                                                                    
			$data_string = json_encode($json);                
			
			$ch = curl_init($base_url);                                                                      
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
			curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
				'Content-Type: application/json',                                                                                
				'Content-Length: ' . strlen($data_string))                                                                       
			);                                                                                                                  
																																 
			$result = curl_exec($ch);
			curl_close ($ch);
			$allResult = json_decode($result);
			$questionsListDetails = $allResult->questionsList;
			//echo "<pre>";print_r($questionsListDetails);die;
			if($questionsListDetails[0]->questionFile == "" || $questionsListDetails[0]->questionFile == "0"){
				$content = "";
				$content .= "<div id='videoContent'>
							<div id='form-group'>";
				$content .= "<input type='hidden' name='totalQuestion' value='".count($questionsListDetails)."' id='totalQuestion'>
				<div style='    font-weight: bold;'>Question</div>";
				$counterForInput = 0;
				for($j =1; $j<=count($questionsListDetails);$j++){
					if($j == 1){
						$background = '#00A2E8';
					}else{
						$background = 'none';
					}
					$AnswerId = $questionsListDetails[$counterForInput]->answerId; 
						$AnswerQuestionToolttip = $questionsListDetails[$counterForInput]->question; 
						$content .= "<input type='button' style='margin-right: 7px;background:".$background."' class='questionlistbutton' onclick='getVideoAnswers(".$AnswerId.")' id='qustionsCounterDropDown_".$AnswerId."' name='".$j."'  value='".$j."'  title='".$AnswerQuestionToolttip."'>";
					$counterForInput++;
							
				}
				$content .= "<div style='margin-top: 10px;'>
				<label id='textQuiestionFlow' style='    width: 100%;padding: 10px;    border: 1px solid grey;' title='Question Text'></label>
				<select class='form-control' name='questionList' onchange='getVideoAnswers(this.value)' id='questionListDropdownMenu' title='Question Text' style='display:none;' disabled>";
				$i = 0;
				foreach($questionsListDetails as $details){
						if($i == 0){
							$AnswerFirst = $details->answerId;
						}
						$i++;
								$content .= "<option value='".$details->answerId."'>".$details->question."</option>";
							
				}
				$content .= "</select></div>";
				
				$content .= "<ul id='playlist' style='display:none;'>";
				$counter = 0;
				foreach($questionsListDetails as $details){
						if($counter == 0){
							$firstVideo = $details->answer;
						}
						$content .= "<li id='listofPlaylist_".$details->answerId."' class='playlistli'><a id='".$details->answerId."' href='https://s3.amazonaws.com/fjinterviewanswers/".$details->answer."' ></a></li>";
						$counter++;
							
				}
				$content .= "</ul>";
				
				foreach($questionsListDetails as $details){
						if($counter == 0){
							$firstVideo = $details->answer;
						}
						$content .= "<input type='hidden' name='videoAnswers' id='answerHideen".$details->answerId."' value='".$details->answer."' >";
						$counter++;
							
				}
				
				$content .=	"</div><div style='    font-weight: bold;    margin-top: 30px;'>Candidate's Response</div>
							<div id='answerContent' style='    padding: 11px;border: 1px solid black;margin-top: 17px;' >
							<video id='videoShowAnswerbox' width='400' height='200' src='https://s3.amazonaws.com/fjinterviewanswers/".$firstVideo."' type='video/mp4' controls>
							</video>
							</div>
							</div>";
				$content .= "<script>$(document).ready(function() {
								init();
								
								
								
								
							});</script>";
				$content .= "<input type='hidden' name='videoName'   id='videoName'      value='".$ENCvideo."' />";
				$content .= "<input type='hidden' name='userJob'     id='userJob'        value='".$userJob."' />";
				$content .= "<input type='hidden' name='appUserName' id='appUserName'    value='".$appUserName."' />";
				$content .= "<input type='hidden' name='appUserJob'  id='appUserJob'     value='".$appUserJob."' />";
				if($userRole == 2){
					$this->userId = $userId;
					$this->type = 'InterviewViewed';
					$this->jobCode = $jobvalue['fjCode'];
				
					processApiSuccess($this);
				}
				echo $content;die;
			}else{
				$content = "";
				$content .= "<div id='videoContent'>
							<div id='form-group'>";
				$content .= "<input type='hidden' name='totalQuestion' value='".count($questionsListDetails)."' id='totalQuestion'>
				<div style='    font-weight: bold;'>Question</div>";
				$counterForInput = 0;
				for($j =1; $j<=count($questionsListDetails);$j++){
					$AnswerQuestionToolttip = $questionsListDetails[$counterForInput]->question;
						$counterForInput++;		
						$content .= "<input type='button' style='margin-right: 7px;' class='questionlistbutton' onclick='showQuestionCounter(".$j.")' id='qustionsCounterDropDown_".$j."' name='".$j."'  value='".$j."' title='".$AnswerQuestionToolttip."'>";
							
				}
				
				$counter = 1;
				$content .= "<div style='    margin-top: 10px;'><input type='checkbox' name='ignoreQuestion' id='ignoreQuestion' value='0' checked>&nbsp;Skip Question Videos</div>";
				foreach($questionsListDetails as $details){
						if($counter == 1){
							$firstVideo = $details->answer;
						}
						$content .= '<div id="textArea_'.$counter.'" class="textArea" style="display:none;"><div id="QuestionText" style="padding: 10px;font-size: 18px;" title="Question Text">'.$details->question.'</div><div id="questionFiles" style="padding: 11px;border: 1px solid black;margin-top: 17px;"  ><video id="myQuestion_'.$counter.'" width="400" height="200" src="https://firstjob.co.in/admin/uploads/questions/video/'.$details->questionFile.'" controls></video></div><div style="    font-weight: bold;    margin-top: 30px;">Candidate\'s Response</div><div id="videoAnswers"  style="padding: 11px;border: 1px solid black;margin-top: 17px;" ><video id="myAnswer_'.$counter.'" width="400" height="200" src="https://s3.amazonaws.com/fjinterviewanswers/'.$details->answer.'" controls></video></div></div>';
						$counter++;
							
				}
				$content .= "<script>$(document).ready(function() {
								$('#textArea_1').css('display','block');
								$('#qustionsCounterDropDown_1').css('background','#00A2E8');
								video = document.getElementById('myQuestion_1');
								Answervideo = document.getElementById('myAnswer_1');
								
								if($('#ignoreQuestion').prop('checked') == true){
									//$(Answervideo).trigger('play');
								}else{
									//$(video).trigger('play');
								}
								
								video.onended = function(e) {
									$('#myAnswer_1').trigger('play');
								};
								
								Answervideo.onended = function(e) {
									showNextAuto(1);
								};
							});</script>";
				$content .= "<input type='hidden' name='videoName'   id='videoName'      value='".$ENCvideo."' />";
				$content .= "<input type='hidden' name='userJob'     id='userJob'        value='".$userJob."' />";
				$content .= "<input type='hidden' name='appUserName' id='appUserName'    value='".$appUserName."' />";
				$content .= "<input type='hidden' name='appUserJob'  id='appUserJob'     value='".$appUserJob."' />";
				if($userRole == 2){
					$this->userId = $userId;
					$this->type = 'InterviewViewed';
					$this->jobCode = $jobvalue['fjCode'];
					processApiSuccess($this);
				}
				echo $content;die;
			}
		}else{
			$returndata = '<div id="player_32931" style="display:inline-block;">
									<a href="https://get.adobe.com/flashplayer/">You need to install the Flash plugin</a>
								</div>
								<script type="text/javascript">
									var flashvars_32931 = {};
									var params_32931 = {
										quality: "high",
										wmode: "transparent",
										bgcolor: "#ffffff",
										allowScriptAccess: "always",
										allowFullScreen: "true",
										flashvars: "fichier=https://s3.amazonaws.com/videoMerge/'.$videoName.'&apercu=https://firstjob.co.in/admin/uploads/bg_black.png"
									};
									var attributes_32931 = {};
									flashObject("https://firstjob.co.in/admin/uploads/v1_27.swf", "player_32931", "400", "250", "8", false, flashvars_32931, params_32931, attributes_32931);
								</script>';
			$returndata .= "<input type='hidden' name='videoName'   id='videoName'      value='".$ENCvideo."' />";
			$returndata .= "<input type='hidden' name='userJob'     id='userJob'        value='".$userJob."' />";
			$returndata .= "<input type='hidden' name='appUserName' id='appUserName'    value='".$appUserName."' />";
			$returndata .= "<input type='hidden' name='appUserJob'  id='appUserJob'     value='".$appUserJob."' />";
			if($userRole == 2){
				$this->userId = $userId;
				$this->type = 'InterviewViewed';
				$this->jobCode = $jobvalue['fjCode'];
				processApiSuccess($this);
			}
			print_r($returndata);
		}
    }


    /**
     * Discription : Use to share interview video given by a specific user on specific job on different email ids
     * Author : Synergy
     * @param array $_POST(email1, email2, email3, email4, email5, videoName, userJob, appUserName, appUserJob)
     * @return string
     */
    function sendInterview() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
		
        $token      = $this->config->item('accessToken');
        $userData   = $this->session->userdata['logged_in'];
        $userId     = $userData->id;        
        $fromEmail  = $this->config->item('fromEmail');
        
        if (isset($userData->id) && $userData->id != NULL) {    
            if (isset($_POST) && $_POST != NULL) {
				
                $email1     = $this->input->post('email1', true);
                $email2     = $this->input->post('email2', true);
                $email3     = $this->input->post('email3', true);
                $email4     = $this->input->post('email4', true);
                $email5     = $this->input->post('email5', true);
                $videoName  = $this->input->post('videoName', true);
                $userJob    = $this->input->post('userJob', true);
                $appUserName= $this->input->post('appUserName', true);
                $appUserJob = $this->input->post('appUserJob', true);
                
                // Recepeint Email //
                if($this->input->post('email1', true)!='') {
                    $emailId[]  = $this->input->post('email1', true);
                }
                if($this->input->post('email2', true)!='') {
                    $emailId[]  = $this->input->post('email2', true);
                }
                if($this->input->post('email3', true)!='') {
                    $emailId[]  = $this->input->post('email3', true);
                }
                if($this->input->post('email4', true)!='') {
                    $emailId[]  = $this->input->post('email4', true);
                }
                if($this->input->post('email5', true)!='') {
                    $emailId[]  = $this->input->post('email5', true);
                }

                // Recepeint Email //
                
                $data['userJobId']  = $userJob;
                $videoName          = $this->input->post('videoName', true);
                $data['videoLink']  = "";
                $data['createdBy']  = $userId;

                if(!empty($emailId)) {
                    $emailId = array_unique($emailId);
    				
                    foreach($emailId as $email) {
                        $data['emailId']  = $email;
    					
                        $resp = $this->job_model->sendInterview($data);
                        if ($resp) {
                            $this->email->from($fromEmail, 'VDOHire');
                            $this->email->subject('VDOHire - Interview Video');
                            $this->email->set_mailtype('html');
                            $this->email->to($email);
                            
                            $temp['appUserName']= $appUserName;
                            $temp['appUserJob'] = $appUserJob;
                            $temp['videoLink']  = base_url().'page/userInterview/'.encryptURLparam($resp, URLparamKey);;                                
                            
                            $body = $this->load->view('emails/interviewTemp.php', $temp, TRUE);
                            $this->email->message($body);
                            $this->email->send();
                        }
                    }
                    echo '1';exit;
                } else {
                    echo '2';exit;
                }
            } else {
                echo '3';exit;
            }
        } else {
            echo '4';exit;
        }
    }

    /*Assessment Module Start
    
    /**
     * Discription : List all the assessments
     * Author Synergy
     * @param array $page, $_POST.
     * @return string the success message | error message.
     */
    function listAssessments($page = NULL) {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        
        //Assessments Listing Start
        $userData = $this->session->userdata['logged_in'];
        $userRole = $userData->role;
        $userLoggedinUserId = $userData->id;
        $data['page'] = $page;
        $resp['error'] = 0;
        $serachColumn = $this->input->post('serachColumn', true);
        $searchText = $this->input->post('serachText', true);
        $token = $this->config->item('accessToken');
        $fromEmail = $this->config->item('fromEmail');
        $itemsPerPage = $this->config->item('itemsPerPage');
        $data['itemsPerPage'] = $itemsPerPage;
        $createdBy = $userData->id;
        if (!$page) {
            $page = 0;
        } else {
            $page = $page - 1;
        }
        $offset = $page * $itemsPerPage;
        try {
            //get all corporate and sub user
            $createdByStr = getAllCorporateAndSubuserIds($userLoggedinUserId, $userRole);
            
            
            $result = $this->assessment_model->listAssessments($itemsPerPage, $offset, $serachColumn, $searchText, $createdByStr);
            $completeResult = $this->assessment_model->listAssessments(NULL, NULL, NULL, NULL, $createdByStr);
            $count = ceil(count($completeResult) / $itemsPerPage);
            $data['count'] = $count;
            $data['totalCount'] = count($completeResult);
            $data['contentAssessmentSet'] = $result;
            $data['searchText'] = $searchText;
            $data['serachColumn'] = $serachColumn;
            $data['token'] = $token;

            //List Question Banks
            $resultMyQuestions = $this->assessment_model->listQuestionBank($itemsPerPage, $offset, $serachColumn, $searchText, $createdByStr, $createdByStr, $userRole);
            $completeResultMyQuestions = $this->assessment_model->listQuestionBank(NULL, NULL, $serachColumn, $searchText, $createdByStr, $createdByStr, $userRole);
            $countMy = ceil(count($completeResultMyQuestions) / $itemsPerPage);
            $data['countMy'] = $countMy;
            $data['totalCountMy'] = count($completeResultMyQuestions);
            $data['contentMyQuestionBank'] = $resultMyQuestions;
            
            //List Fj Questions
            $resultFjQuestions = $this->assessment_model->listFjQuestionsBank($itemsPerPage, $offset, $serachColumn, $searchText, $createdBy);
            $completeResultFjQuestions = $this->assessment_model->listFjQuestionsBank(NULL, NULL, NULL, NULL, $createdBy);
            $countFj = ceil(count($completeResultFjQuestions) / $itemsPerPage);
            $data['countFj'] = $countFj;
            $data['totalCountFj'] = count($completeResultFjQuestions);
            $data['contentFjQuestionsBank'] = $resultFjQuestions;
            return $data;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Discription : Use to list interview sets
     * Author : Synergy
     * @param int $page
     * @return render data in view
     */
    function listInterviews() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        //Interviews Listing Start
        
        $userData = $this->session->userdata['logged_in'];
        $userRole = $userData->role;
        $userLoggedinUserId = $userData->id;
        
        $resp['error'] = 0;
        $serachColumn = '';
        $searchText = '';
        $token = $this->config->item('accessToken');
        $fromEmail = $this->config->item('fromEmail');
        $itemsPerPage = $this->config->item('itemsPerPage');
        $data['itemsPerPage'] = $itemsPerPage;
       // $createdBy = NULL;
        $createdBy = $userData->id;
        $page = 0;
        $offset = 0;
        $offsetFj = 0;
        $offsetMy = 0;

        try {
            
            $createdByStr = getAllCorporateAndSubuserIds($userLoggedinUserId, $userRole);
            
            
            $result = $this->interview_model->listInterviews($itemsPerPage, $offset, $serachColumn, $searchText, $createdByStr, $userRole);
            $i = 0;
            //echo "<pre>";
            
            foreach ($result as $item) {
                $questionByInterview = $this->interview_model->getQuestionsByInterview($item->id);
                $result[$i]->questionCount = count($questionByInterview);
                $i++;
            }
            $completeResult = $this->interview_model->listInterviews(NULL, NULL, NULL, NULL, $createdByStr);
            $count = ceil(count($completeResult) / $itemsPerPage);

            $data['count'] = $count;
            $data['totalCount'] = count($completeResult);
            $data['contentInterviewSet'] = $result;
            $data['main_content'] = 'fj-interview-listing';
            $data['searchText'] = $searchText;
            $data['serachColumn'] = $serachColumn;
            $data['token'] = $token;

            //List Fj Questions
            $resultFjQuestions          = $this->interview_model->listFjQuestions($itemsPerPage, $offsetFj, $serachColumn, $searchText, $createdBy);
            $completeResultFjQuestions  = $this->interview_model->listFjQuestions(NULL, NULL, NULL, NULL, $createdBy);
            $countFj                    = ceil(count($completeResultFjQuestions) / $itemsPerPage);
            $data['countFj']            = $countFj;
            $data['totalCountFj']       = count($completeResultFjQuestions);
            $data['contentFjQuestions'] = $resultFjQuestions;

            $resultMyQuestions          = getMyQuestionsPagination($itemsPerPage, $offsetMy, $serachColumn, $searchText, $createdByStr);
            $completeResultMyQuestions  = getMyQuestionsPagination(NULL, NULL, NULL, NULL, $createdByStr);
            $countMy                    = ceil(count($completeResultMyQuestions) / $itemsPerPage);            
            $data['countMy']            = $countMy;
            $data['totalCountMy']       = count($completeResultMyQuestions);
            $data['contentMyQuestions'] = $resultMyQuestions;
            
            return $data;
        } catch (Exception $e) {
            return  $e->getMessage();
            //die();
        }
        //Interviews Listing End
    }

    /**
     * Description : Add new assessment and add Questions(Number of questions from Question Bank)
     * Author : Synergy
     * @param array(question, name, duration)
     * @return void
     */
    function addAssessment() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }

        $userData = $this->session->userdata['logged_in'];
        $userId = $userData->id;
        $createdById = $userData->createdBy;
        
        if (isset($_POST) && $_POST != NULL) {
            $questions = $this->input->post('question', true);
            //print '<pre>';print_r($questions);exit;

            $name = $this->input->post('name', true);
            $duration = $this->input->post('duration', true);

            //Adding Assessment Data
            $assessmentData['name'] = $name;
            $assessmentData['duration'] = $duration * 60;
            $assessmentData['createdAt'] = date('Y-m-d h:i:s');
            $assessmentData['updatedAt'] = date('Y-m-d h:i:s');
            $assessmentData['createdBy'] = $userId;
            $assessmentData['status'] = '1';

            $assessmentSetExist = $this->assessment_model->getAlreadyCreatedAssessmentSet($userId, $createdById, $name);
            if(count($assessmentSetExist) < 1) {

                $assessmentId = $this->assessment_model->addAssessment($assessmentData);
                $totalQuestions = 0;
                foreach ($questions as $item) {
                    if((isset($item['number']) && !empty($item['number'])) && (isset($item['questionBank']) && !empty($item['questionBank']))) {
                        $number = $item['number'];
                        $totalQuestions = $totalQuestions + $number;
                        $questionBank = $item['questionBank'];

                        $this->db->insert('fj_assessmentRandomQuestionnaire', array('assessmentId' => $assessmentId, 'questionBankId' => $questionBank, 'noOfQuestions' => $number));
                    }

                    //Add individual Question
                    if((isset($item['question1']) && !empty($item['question1'])) && (isset($item['questionBank1']) && !empty($item['questionBank1']))) {
                        $individualQuestion = $item['question1'];
                        if($individualQuestion) {
                            $totalQuestions = $totalQuestions + 1;
                            $assessmentQuestion['assessmentId'] = $assessmentId;
                            $assessmentQuestion['questionId'] = $individualQuestion;
                            $assessmentQuestion['questionBank'] = $item['questionBank1'];
                            $this->assessment_model->addAssessmentQuestion($assessmentQuestion);
                        }
                    }
                    
                    /*$qb = getQuestionOfQuestionBank($questionBank);
                    $results = $this->assessment_model->getRandomQuestionFromQuestionBank($number, $questionBank, count($qb));
                    $assessmentQuestion['assessmentId'] = $assessmentId;
                    
                    foreach ($results as $item) {
                        $assessmentQuestion['questionId'] = $item['id'];
                        $this->assessment_model->addAssessmentQuestion($assessmentQuestion);
                    }*/
                }
                //End of Assessment Data
                $returnData = array('assessmentId' => $assessmentId, 'noOfQuestions' => $totalQuestions, 'errorMessage' => '');
                echo json_encode($returnData);exit;
            } else {
                $returnData = array('assessmentId' => '', 'noOfQuestions' => '', 'errorMessage' => 'There is already a set with same name. Please use another one.');
                echo json_encode($returnData);exit;
            }
        } else {
            echo 'failure';exit;
        }
    }

    /**
     * Description : Edit existing assessment and edit Questions(Number of questions from Question Bank)
     * Author : Synergy
     * @param array(id(assessmentId),question, name, duration)
     * @return void
     */
    function updateAssessment() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }

        $userData   = $this->session->userdata['logged_in'];              
        $userId     = $userData->id;

        if (isset($_POST) && $_POST != NULL) {
            $id  = $this->input->post('editAssessemntId', true);
            $questions  = $this->input->post('question', true);
            $name       = $this->input->post('name', true);
            $duration   = $this->input->post('duration', true);
            
            $assessmentData['name']         = $name;
            $assessmentData['duration']     = $duration * 60;
            $assessmentData['createdAt']    = date('Y-m-d h:i:s');
            $assessmentData['updatedAt']    = date('Y-m-d h:i:s');
            $assessmentData['createdBy']    = $userId;
            $assessmentId                   = $this->assessment_model->updateAssessment($assessmentData, $id);
            $this->assessment_model->deleteAssessmentQuestions($id);
            $totalQuestions = 0;
            foreach ($questions as $item) {
                if((isset($item['number']) && !empty($item['number'])) && (isset($item['questionBank']) && !empty($item['questionBank']))) {
                    $number         = $item['number'];
                    $totalQuestions = $totalQuestions + $number;
                    $questionBank   = $item['questionBank'];

                    $this->db->delete('fj_assessmentRandomQuestionnaire', array('assessmentId' => $assessmentId, 'questionBankId' => $questionBank));
                    $this->db->insert('fj_assessmentRandomQuestionnaire', array('assessmentId' => $assessmentId, 'questionBankId' => $questionBank, 'noOfQuestions' => $number));
                }

                //Add individual Question
                if((isset($item['question1']) && !empty($item['question1'])) && (isset($item['questionBank1']) && !empty($item['questionBank1']))) {
                    $individualQuestion = $item['question1'];
                    if($individualQuestion) {
                        $totalQuestions = $totalQuestions + 1;
                        $assessmentQuestion['assessmentId'] = $assessmentId;
                        $assessmentQuestion['questionId'] = $individualQuestion;
                        $assessmentQuestion['questionBank'] = $item['questionBank1'];
                        $this->assessment_model->addAssessmentQuestion($assessmentQuestion);
                    }
                }


                /*$qb             = getQuestionOfQuestionBank($questionBank);
                $results        = $this->assessment_model->getRandomQuestionFromQuestionBank($number, $questionBank, count($qb));
                $assessmentQuestion['assessmentId']     = $assessmentId;
                foreach ($results as $item) {
                    $assessmentQuestion['questionId']   = $item['id'];
                    $this->assessment_model->addAssessmentQuestion($assessmentQuestion);
                }*/
            }
            $returnData = array('assessmentId' => $assessmentId, 'noOfQuestions' => $totalQuestions);
            echo json_encode($returnData);exit;
        } else {
            echo 'failure';exit;
        }
    }

    /**
     * Description : Edit existing assessment and edit Questions(Number of questions from Question Bank)
     * Author : Synergy
     * @param array(id(assessmentId),question, name, duration)
     * @return void
     */
    function editAssessment() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $userData   = $this->session->userdata['logged_in'];              
        $userId     = $userData->id;
        $id = $this->input->post('assessmentId', true);

        $questionBanks          = listQuestionBank();
        $assessment            = $this->assessment_model->getAssessment($id);
        $assessmentQuestions    = $this->assessment_model->getAssessmentQuestions($id);

        $returnData = array('assessment' => $assessment, 'assessmentQuestions' => $assessmentQuestions, 'questionBanks' => $questionBanks, 'assessmentEditId' => $id);
        echo json_encode($returnData);exit;
    }

    /**
     * Description : Edit existing assessment and edit Questions(Number of questions from Question Bank)
     * Author : Synergy
     * @param array(id(assessmentId),question, name, duration)
     * @return void
     */
    function editAssessment1() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $userData   = $this->session->userdata['logged_in'];              
        $userId     = $userData->id;
        $id = $this->input->post('assessmentId', true);

        $assessment            = $this->assessment_model->getAssessment($id);
        $assessmentQuestions    = $this->assessment_model->getAssessmentQuestions($id);
        $allAssessmentIndividualQuestions = $assessmentQuestions['individual'];
        $allAssessmentRandomQuestions = $assessmentQuestions['random'];

        $allQuestions = array();
        $allQuestionDetails = array();
        if(!empty($allAssessmentRandomQuestions)) {
            foreach ($allAssessmentRandomQuestions as $assessmentRandomQuestion) {
                $qbQuestions = getQuestionOfQuestionBank($assessmentRandomQuestion['questionBank']);
                foreach( array_rand($qbQuestions, $assessmentRandomQuestion['totalQuestion']) as $k ) {
                    $allQuestions[] = $qbQuestions[$k]['id'];
                }
            }
        }

        if(!empty($allAssessmentIndividualQuestions)) {
            foreach ($allAssessmentIndividualQuestions as $assessmentIndividualQuestion) {
                $allQuestions[] = $assessmentIndividualQuestion['questionId'];
            }
        }

        if(!empty($allQuestions)) {
            foreach($allQuestions as $questionId) {
                $questionDetails = $this->assessment_model->getQuestion($questionId);
                $questionDetail['title'] = $questionDetails['title'];
                $questionDetail['type'] = $questionDetails['type'];
                $questionDetail['file'] = $questionDetails['file'];
                $allQuestionDetails[] = $questionDetail;
            }
        }

        $returnData = array('questions' => $allQuestionDetails);
        echo json_encode($returnData);exit;
    }

    /**
     * Description : Simply displays add Question Bank page and if isset($_POST) creates a new question bank.
     * Author : Synergy
     * @param name, type
     * @return string the success message | error message.
     */
    function addQuestionBank() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }

        $token = $this->config->item('accessToken');
        $userData = $this->session->userdata['logged_in'];

        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            $userId = '';
        }


        $errorMessage = '';
        $noOfQuestions = 0;
        //print '<pre>';print_r($_POST);
        //print '<pre>';print_r($_FILES);exit;
        if (isset($_POST) && $_POST != NULL) {
            $questionType = $this->input->post('assessmenQquestionType', true);
            $data['name'] = $this->input->post('assessmentQuestionBankname', true);
            $data['type'] = $this->input->post('assessmenQquestionType', true);
            $data['createdAt'] = date('Y-m-d h:i:s');
            $data['updatedAt'] = date('Y-m-d h:i:s');
            $data['createdBy'] = $userId;
            $data['updatedBy'] = $userId;

            try {
                $questionBankIdValue = $this->assessment_model->addQuestionBank($data);
                if ($questionBankIdValue) {
                    
                    if( $questionType == 3 )
                    {   
                        if ( isset($_FILES["assessmentQuestionFile"]['name']) && !empty($_FILES["assessmentQuestionFile"]['name'])) {
                            
                            $textQuestionCsvPath = $this->config->item('textQuestionCsvPath');
                            
                            //if there was an error uploading the file
                            if ($_FILES["assessmentQuestionFile"]["error"] > 0) {
                                $errorMessage = "Your Question Bank has been created but we get error message: " . $_FILES["assessmentQuestionFile"]["error"];
                                $returnData = array('questionBankId' => $questionBankIdValue, 'noOfQuestions' => $noOfQuestions, 'errorMessage' => $errorMessage, 'questionType' => $questionType, 'allAssessmentQuestions' => array());
                                echo json_encode($returnData);exit;

                            } else {  
                                //Store file in directory "upload" with the name of "uploaded_file.txt"
                                $typeArr = explode('.', $_FILES['assessmentQuestionFile']['name']);
                                $fileType = array('csv');
                                $path = $textQuestionCsvPath . "questionFile_" . time() . "_" .  $userData->id . '.' . $typeArr[1];
                                
                                
                                if (in_array($typeArr[1], $fileType)) {
                                    $fileuploaded = move_uploaded_file($_FILES["assessmentQuestionFile"]["tmp_name"], $path);
                                    
                                    
                                    if( $fileuploaded ){
                                        $file = fopen($path, "r");
                                        
                                        $i = 0;
                                        while (!feof($file)) {
                                            $fileData = fgetcsv($file);
                                           
                                            if ($i != 0) {
                                                $fileArr[] = $fileData;
                                            } else {
                                                if (strtolower($fileData[0]) != 'question') {
                                                    $errorMessage = "Your Question Bank has been created but CSV file format is not valid.";
                                                    $returnData = array('questionBankId' => $questionBankIdValue, 'noOfQuestions' => $noOfQuestions, 'errorMessage' => $errorMessage, 'questionType' => $questionType, 'allAssessmentQuestions' => array());
                                                    echo json_encode($returnData);exit;
                                                }
                                            }
                                            $i++;
                                        }   
                                        
                                        $allAssessmentQuestions = array();
                                        foreach ($fileArr as $item) {
                                            $questionTitle = $item[0];
                                            
                                            if( $questionTitle != "" ) {
                                                $noOfQuestions = $noOfQuestions + 1;

                                                $itemvalue['title'] = $questionTitle;
                                                $itemvalue['IsMultipleChoice'] = '1';
                                                $itemvalue['type'] = $questionType;
                                                $itemvalue['status'] = 1;
                                                $itemvalue['createdAt'] = date('Y-m-d h:i:s');
                                                $itemvalue['updatedAt'] = date('Y-m-d h:i:s');
                                                $itemvalue['createdBy'] = $userData->id;

                                                if( $questionTitle != "" ){
                                                    $questionId = $this->assessment_model->addQuestion($itemvalue);

                                                    //Add multiple choice items
                                                    $optionData['questionId'] = $questionId;

                                                    $optionData['createdAt'] = date('Y-m-d h:i:s');
                                                    $optionData['updatedAt'] = date('Y-m-d h:i:s');

                                                    for( $j = 1; $j<=4; $j++ ){
                                                        $optionData['option'] = $item[$j];
                                                        if ($j == $item[5]) {
                                                            $optionData['isCorrect'] = '1';
                                                        } else {
                                                            $optionData['isCorrect'] = '0';
                                                        }
                                                        $optionId = $this->assessment_model->addMultipleChoiceAnswer($optionData);
                                                        if ($optionData['isCorrect'] == '1') {
                                                            $correctOption = $optionId;
                                                        }
                                                    }

                                                    //update question
                                                    $updatequestion['answer'] = $correctOption;
                                                    $this->assessment_model->updateQuestion($updatequestion, $questionId);

                                                    //Add question to question bank
                                                    $questionBankData['questionId'] = $questionId;
                                                    $questionBankData['questionBank'] = $questionBankIdValue;
                                                    $questionBankData['createdAt'] = date('Y-m-d h:i:s');
                                                    $questionBankData['updatedAt'] = date('Y-m-d h:i:s');
                                                    $this->assessment_model->addQuestionToQuestionBank($questionBankData);

                                                    $allAssessmentQuestion['questionId'] = $questionId;
                                                    $allAssessmentQuestion['quesitonName'] = $questionTitle;
                                                    $allAssessmentQuestions[] = $allAssessmentQuestion;
                                                }
                                            }
                                        }
                                        $returnData = array('questionBankId' => $questionBankIdValue, 'noOfQuestions' => $noOfQuestions, 'errorMessage' => '', 'questionType' => $questionType, 'allAssessmentQuestions' => $allAssessmentQuestions);
                                        echo json_encode($returnData);exit;
                                    } else {
                                        $returnData = array('questionBankId' => $questionBankIdValue, 'noOfQuestions' => $noOfQuestions, 'errorMessage' => 'Your Question Bank has been created but CSV file could not uploaded.', 'questionType' => $questionType, 'allAssessmentQuestions' => array());
                                        echo json_encode($returnData);exit;
                                    }
                                } else {
                                    $returnData = array('questionBankId' => $questionBankIdValue, 'noOfQuestions' => $noOfQuestions, 'errorMessage' => 'Your Question Bank has been created but CSV file format is not valid', 'questionType' => $questionType, 'allAssessmentQuestions' => array());
                                        echo json_encode($returnData);exit;
                                }
                            }
                        } else {
                            $returnData = array('questionBankId' => $questionBankIdValue, 'noOfQuestions' => $noOfQuestions, 'errorMessage' => '', 'questionType' => $questionType, 'allAssessmentQuestions' => array());
                            echo json_encode($returnData);exit;
                        }
                    } else {
                        $returnData = array('questionBankId' => $questionBankIdValue, 'noOfQuestions' => $noOfQuestions, 'errorMessage' => '', 'questionType' => $questionType,'allAssessmentQuestions' => array());
                        echo json_encode($returnData);exit;
                    }
                } else {
                    $returnData = array('questionBankId' => '', 'noOfQuestions' => $noOfQuestions, 'errorMessage' => 'Sorry! Question Bank could not created.', 'questionType' => $questionType, 'allAssessmentQuestions' => array());
                    echo json_encode($returnData);exit;
                }
            } catch (Exception $e) {
                $returnData = array('questionBankId' => '', 'noOfQuestions' => $noOfQuestions, 'errorMessage' => $e->getMessage(), 'questionType' => $questionType, 'allAssessmentQuestions' => array());
                echo json_encode($returnData);exit;
            }
            
        }
    }

    /**
     * Discription : Add new question to assessment set
     * Author Synergy
     * @param $_POST.
     * @return string the success message | error message.
     */
    function addQuestionToAssessmentSet() {
        $questionId = $this->input->post('questionId', true);
        $assessment = $this->input->post('assessment', true);
        try {
            foreach ($assessment as $item) {
                $assessmentQuestion['assessmentId'] = $item;
                $assessmentQuestion['questionId'] = $questionId;
                $assessmentQuestion['createdAt'] = date('Y-m-d h:i:s');
                $assessmentQuestion['updatedAt'] = date('Y-m-d h:i:s');
                $this->assessment_model->addAssessmentQuestion($assessmentQuestion);
            }
            echo 'success';exit;
        } catch (Exception $e) {
            echo 'failure';exit;
        }
    }
    /*Assessment Module End*/

    /**
     * Description : Edit existing assessment and edit Questions(Number of questions from Question Bank)
     * Author : Synergy
     * @param array(id(assessmentId),question, name, duration)
     * @return void
     */
    function getLastCreatedInterview() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $userData   = $this->session->userdata['logged_in'];              
        $userId     = $userData->id;
		$userLoggeinId = $userData->id;
        $userRole = $userData->role;
		
		
        $interviewId = $this->input->post('lastInterviewId', true);
		
		$allUserStrList = getAllCorporateAndSubuserIds($userLoggeinId, $userRole);
		
			//L1 & L2 level job list
		$usersJobL1LevelListRes = $this->jobpanel_model->getUserJobPenalForUser($userLoggeinId);
		
		if( $usersJobL1LevelListRes['jobIdStr'] != "" ){
			$usersJobListL1L2 = $usersJobL1LevelListRes['jobIdStr'];
			$usersJobListL1 = "";
			$usersJobListL2     = "";
			$usersJobDoneListL2 = "";
		} else {
			$usersJobListL1L2 = "";
			$usersJobListL1 = "";
			$usersJobListL2     = "";
			$usersJobDoneListL2 = "";
		} 
		
		$userLevel = 3;
		
		
		
        $interviewData = $this->job_model->getLastCreatedInterview($interviewId, $userId,$usersJobListL1L2);
		
        if($interviewData) {
			$tobesentData = array();
			foreach($interviewData as $interview){
				$UserJobRole = $this->userjob_model->getUserJobPanel($interview->jobId,$userId);
				
				if($UserJobRole) {
					if($UserJobRole[0]['level'] == 1){
						$userJobRoleFor = 4;
					}else if($UserJobRole[0]['level'] == 2){
						$userJobRoleFor = 5;
					}
				}
				
				if($interview->status == '2' || $interview->status == '1'){
					
					if($userJobRoleFor == '5' && ( $interview->status == '2')){
						
						$data['interviewId'] = $interview->interviewId;
						$data['userId'] = $interview->userId;
						$data['jobName'] = $interview->jobName;
						$data['jobId'] = $interview->jobId;
						$data['candidateName'] = $interview->candidateName;
						$data['createdAt'] = $interview->createdAt;
						$tobesentData[] = $data;
					}else if($userJobRoleFor == '4' && $interview->status == '1'){
						
						$data['interviewId'] = $interview->interviewId;
						$data['userId'] = $interview->userId;
						$data['jobName'] = $interview->jobName;
						$data['jobId'] = $interview->jobId;
						$data['candidateName'] = $interview->candidateName;
						$data['createdAt'] = $interview->createdAt;
						$tobesentData[] = $data;
					}
					
					
				}
				
				
			}
			
			
            $jobId = encryptURLparam($interviewData[0]->jobId, URLparamKey);
            $userId = encryptURLparam($interviewData[0]->userId, URLparamKey);
        } else {
            $jobId = '';
            $userId = '';
			$tobesentData = array();
        }

        $returnData = array('interviewData' => $tobesentData, 'jobId' => $jobId, 'userId' => $userId);
        echo json_encode($returnData);exit;
    }
}

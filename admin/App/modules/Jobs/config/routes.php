<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['jobs/add'] = "Jobs/addJob";
$route['jobs/login'] = "Jobs/login";
$route['jobs/getValue'] = "Jobs/getValueByToken";
$route['jobs/edit/(:any)'] = "Jobs/editJob/$1";
$route['jobs/view/(:any)'] = "Jobs/getJob/$1";
$route['jobs/view/(:any)/(:any)'] = "Jobs/getJob/$1";
$route['jobs/list/(:any)'] = "Jobs/listJobs/$1";
$route['jobs/list'] = "Jobs/listJobs";
$route['jobs/listPast/(:any)'] = "Jobs/listPastJobs/$1";
$route['jobs/listPast'] = "Jobs/listPastJobs";
$route['jobs/postJob'] = "Jobs/postJob";
$route['jobs/delete'] = "Jobs/deleteJob";
$route['corporate/dashboard'] = "Jobs/corporateDashboard";
$route['corporate/dashboard/(:any)'] = "Jobs/corporateDashboard/$1";
$route['corporate/sendinvitation'] = "Jobs/sendInvitationFromXlsJob";
$route['jobs/replicate'] = "Jobs/replicate";
$route['jobs/getJob/(:any)'] = "Jobs/view/$1";
$route['jobs/search'] = "Jobs/searchJobsApplicants";
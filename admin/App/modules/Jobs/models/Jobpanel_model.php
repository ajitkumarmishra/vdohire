<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Jobpanel Model
 * Description : Handle all the CRUD operation for Jobpanel
 * @author Synergy
 * @createddate : Nov 10, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 21, 2017
 */

class Jobpanel_model extends CI_Model {

    /**
     * Declaring variables
     */
    var $table = "fj_jobPanel";

    /**
     * Responsable for inheriting parent constructor
     * Responsable for auto load the the session and pagination library
     * @return void
     */
    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('pagination');
    }

    /**
     * getUserJobLevel function
     * Discription : Use to get level of specific assigned job to a specific corporate user
     * @author Synergy
     * @param int $job_id, int $user_id 
     * @return array of job level data
     */
    function getUserJobLevel($job_id, $user_id) {
        
        try {
            
            $query = $this->db->select("*")
                                ->from($this->table)
                                ->where('userId =', $user_id)
                                ->where('jobId =', $job_id);
            $selectqry = $query->get();
            
            $result = $selectqry->row_array();
           
            if (!$result) {
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * getUserFromJobLevel function
     * Discription : Use to get user of specific assigned job to a specific level of user
     * @author Synergy
     * @param int $job_id, int $level
     * @return array of user level data
     */
    function getUserFromJobLevel($job_id, $level) {
        
        try {
            
            $query = $this->db->select("*")
                                ->from($this->table)
                                ->where('level =', $level)
                                ->where('jobId =', $job_id);
            $selectqry = $query->get();
            
            $result = $selectqry->row_array();
           
            if (!$result) {
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * getUserNameFromJobLevel function
     * Discription : Use to get all users name of specific assigned job to a specific level of user
     * @author Synergy
     * @param int $job_id, int $level
     * @return array of user name data
     */
    function getUserNameFromJobLevel($job_id, $level) {
        
        try {
            
            $query = $this->db->select("JP.*, GROUP_CONCAT(U.fullname SEPARATOR ',') as fullname")
                                ->from($this->table." as JP")
                                ->join('fj_users as U', 'U.id = JP.userId', 'left')
                                ->where('JP.level =', $level)
                                ->where('JP.jobId =', $job_id);
            $selectqry = $query->get();
            
            $result = $selectqry->row_array();
           
            if (!$result) {
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * checkJobLevel function
     * Discription : Use to get all users name, job details and job panel detail of specific assigned job to a specific level of user
     * @author Synergy
     * @param int $job_id, int $level
     * @return array of data
     */
    function checkJobLevel($job_id, $level) {
        
        try {
            
            $query = $this->db->select("JP.*, U.fullname, U.id as userid, U.email, J.title, J.description")
                                ->from($this->table.' as JP')
                                ->join('fj_users as U', 'U.id = JP.userId', 'left')
                                ->join('fj_jobs as J', 'J.id = JP.jobId', 'left')
                                ->where('JP.level =', $level)
                                ->where('JP.jobId =', $job_id);
            $selectqry = $query->get();
            
            $result = $selectqry->result_array();
           
            if (!$result) {
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * getUserJobPenalForUser function
     * Discription : Use to get all jobs which are assigned to a specific user at a specific level
     * @author Synergy
     * @param int $user_id, int $level
     * @return array of jobs data
     */
    function getUserJobPenalForUser($user_id, $level) {
        
        try {
            
            $query = $this->db->select("group_concat(DISTINCT jobId separator ',') as jobIdStr")
                                ->from($this->table)
                                ->where('userId =', $user_id)
                                ->group_by('userId');
            $selectqry = $query->get();
            $result = $selectqry->row_array();
           
            if (!$result) {
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * getAllUserJobPenalForUser function
     * Discription : Use to get all jobs which are assigned to specified users at level1 or level2
     * @author Synergy
     * @param array $userStrId, int $level
     * @return array of jobs data
     */
    function getAllUserJobPenalForUser($userStrId) {
        
        try {
            $query = $this->db->select("group_concat(DISTINCT jobId separator ',') as jobIdStr")
                                ->from($this->table)
                                ->where('userId in ('.$userStrId.')')
                                ->where('(level = 1 or level = 2)');
            $selectqry = $query->get();
            $result = $selectqry->row_array();
            if (!$result) {
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
}
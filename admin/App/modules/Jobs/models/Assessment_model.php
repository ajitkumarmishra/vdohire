<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Assessment Model
 * Description : Handle all the CRUD operation for Assessment
 * @author Synergy
 * @createddate : Dec 2, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 15, 2017
 */

class Assessment_model extends CI_Model {

    /**
     * Defining static variable to store tables name
     */
    var $table = "assessments";

    /**
     * Responsable for auto load the the session and pagination library
     * @return void
     */
    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('pagination');
    }

    /**
     * addQuestionBank function
     * Discription : Creates a question bank.
     * @author Synergy
     * @params array $data
     * @return questionBankId or false
     */
    function addQuestionBank($data) {
        $resp = $this->db->insert('fj_questionBank', $data);
        $questionBankId = $this->db->insert_id();
        if ($resp) {
            return $questionBankId;
        } else {
            return false;
        }
    }

    /**
     * UpdateQuestionBank function
     * Discription : Updates a specific question bank.
     * @author Synergy
     * @param array $data.
     * @param int $id.
     * @return false or id or error message.
     */
    function updateQuestionBank($data, $id) {
        try {
            $this->db->where('id', $id);
            $resp = $this->db->update('fj_questionBank', $data);
            if (!$resp) {
                throw new Exception('Unable to update question.');
                return false;
            }
            return $id;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * GetQuestionBank function
     * Discription : Gets a specific question bank detail.
     * @author Synergy
     * @param int $id.
     * @return array the interviews data or false or error message.
     */
    function getQuestionBank($id) {
        try {
            $this->db->select('*');
            $this->db->from('fj_questionBank');
            $this->db->where('id', $id);
            $query = $this->db->get();
            $result = $query->row_array();
            if (count($result) == 0) {
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * AddQuestion function
     * Discription : Creates a question.
     * @author Synergy
     * @params array $data
     * @return questionId or false
     */
    function addQuestion($data) {
        $resp = $this->db->insert('fj_question', $data);
        $questionId = $this->db->insert_id();
        if ($resp) {
            return $questionId;
        } else {
            return false;
        }
    }

    /**
     * AddMultipleChoiceAnswer function
     * Discription : Creates a multiple choice answer.
     * @author Synergy
     * @params array $data
     * @return multipleChoiceId or false
     */
    function addMultipleChoiceAnswer($data) {
        $resp = $this->db->insert('fj_multipleChoiceAnswer', $data);
        $multipleChoiceId = $this->db->insert_id();
        if ($resp) {
            return $multipleChoiceId;
        } else {
            return false;
        }
    }

    /**
     * AddMultipleChoiceAnswer function
     * Discription : Creates a multiple choice answer.
     * @author Synergy
     * @params array $data
     * @return questionBankId or false
     */
    function addQuestionToQuestionBank($data) {
        $resp = $this->db->insert('fj_questionBankQuestion', $data);
        $questionBankId = $this->db->insert_id();
        if ($resp) {
            return $questionBankId;
        } else {
            return false;
        }
    }

    /**
     * fileUpload function
     * Discription : Uploads a file to directory
     * @author Synergy
     * @param array $files
     * @param string $uploads_dir
     * @param string $imagename
     * @return fileName or false or error message
     */
    public function fileUpload($files, $uploads_dir, $fileName) {
        try {
            $fileType = pathinfo($fileName, PATHINFO_EXTENSION);
            $tmp_name = $files["tmp_name"];
            if (move_uploaded_file($tmp_name, $uploads_dir . "/" . $fileName)) {                
                if($fileType=='mp4') {
                    shell_exec("ffmpeg -i ".$uploads_dir."/".$fileName." -vcodec libx264 -crf 24 ".$uploads_dir."/q".$fileName." -y ");
                    unlink($uploads_dir."/".$fileName);
                    $fileName = "q".$fileName;
                }
                return $fileName;
            }
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    /**
     * GetQuestion function
     * Discription : Gets a specific question detail.
     * @author Synergy
     * @param int $id.
     * @return array the question data or false or error message.
     */
    function getQuestion($id) {
        try {
            $sql = "select q.*, qb.questionBank from fj_question q left join fj_questionBankQuestion qb on qb.questionId = q.id  where q.id = '" . $id . "'";
            $query = $this->db->query($sql);
            $result = $query->row_array();
            if (count($result) == 0) {
                return false;
            }
            return $result;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * deleteQuestionsById function
     * Discription : Delete specific question from database.
     * @author Synergy
     * @param int $id.
     * @return boolean or error message
     */
    function deleteQuestionsById($id) {
        try {
            $this->db->where('id', $id);
            $this->db->delete('fj_question');
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * deleteQuestionBankQuestionById function
     * Discription : Delete question from a specific question bank
     * @author Synergy
     * @param int $id.
     * @return boolean or error message
     */
    function deleteQuestionBankQuestionById($id) {
        try {
            $this->db->where('questionId', $id);
            $this->db->delete('fj_questionBankQuestion');
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * deleteMultipleChoiceByQuestionId function
     * Discription : Delete multichose question from a specific question bank
     * @author Synergy
     * @param int $id.
     * @return boolean or error message
     */
    function deleteMultipleChoiceByQuestionId($id) {
        try {
            $this->db->where('questionId', $id);
            $this->db->delete('fj_multipleChoiceAnswer');
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * updateQuestion function
     * Discription : Updates a specific question.
     * @author Synergy
     * @param array $data.
     * @param int $id.
     * @return boolean or error message.
     */
    function updateQuestion($data, $id) {
        try {
            $this->db->where('id', $id);
            $resp = $this->db->update('fj_question', $data);
            if (!$resp) {
                throw new Exception('Unable to update job data.');
                return false;
            }
            return $id;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * listAssessments function
     * Discription : Get all the Assessments.
     * @author Synergy
     * @param int $limit, $offset.
     * @return array the Assessments data or false or error message.
     */
    function listAssessments($limit, $offset, $serachColumn = NULL, $searchText = NULL, $createdBy = NULL) {
        try {
            $currentDate = date('Y-m-d h:i:s');
            $userTypeQuery  = "SELECT * FROM fj_users WHERE id='$createdBy' LIMIT 0,1";
            $userQuery      = $this->db->query($userTypeQuery);
            $userResult     = $userQuery->row();

            if ($userResult) {
                if($userResult->role!='1') {            
                    $sql = "select * from fj_assessments where status != 3";
                    if (isset($createdBy) && $createdBy != NULL) {
                        $sql .= " AND createdBy IN (" . $createdBy . ")";
                    }            
                    $sql .= " order by createdAt desc";

                    if (isset($offset)) {
                        $sql .= " limit " . $offset . ", " . $limit . "";
                    }
                    $query = $this->db->query($sql);
                    $result = $query->result();
                }
                else{
                    $sql = "SELECT 
                                * 
                            FROM 
                                fj_assessments 
                            WHERE 
                                status != 3 AND 
                                createdBy   IN (
                                                SELECT
                                                    id
                                                FROM
                                                    fj_users
                                                WHERE
                                                    role='1'
                                                )
                            ORDER BY
                                createdAt DESC ";
                    if (isset($offset)) {
                        $sql .= " LIMIT " . $offset . ", " . $limit . "";
                    }

                    $query = $this->db->query($sql);
                    $result = $query->result();
                    if (!$result) {
                        return false;
                    }                    
                }
            }
            else {
                return false;   
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * listQuestionBank function
     * Discription : Get all which are created by $createdBy.
     * @author Synergy
     * @param int $limit, string $offset, $createdBy
     * @return array the question banks or false or error message.
     */
    function listQuestionBank($limit, $offset, $serachColumn = NULL, $searchText = NULL, $createdBy = NULL, $createdByStr = NULL, $userRole = NULL) {
        try {
            $currentDate = date('Y-m-d h:i:s');
            $userTypeQuery  = "SELECT * FROM fj_users WHERE id='$createdBy' LIMIT 0,1";
            $userQuery      = $this->db->query($userTypeQuery);
            $userResult     = $userQuery->row();
            
            if ($userResult) {
                if($userResult->role!='1') {  
                    $sql = "select qb.* from fj_questionBank qb where qb.status != 3";             
                    if( $userRole != "" && $userRole != NULL  ) {
                        $userRoleValue = $userRole;
                        if( trim($userRoleValue) == 2 || trim($userRoleValue) == 4 || trim($userRoleValue) == 5 ){
                            $sql .=" AND qb.createdBy in (" . $createdBy . ")";
                        } else {
                            $sql .=" AND qb.createdBy = '" . $createdBy . "'";
                        }
                    } else {
                        $sql .=" AND qb.createdBy = '" . $createdBy . "'";
                    }

                    $sql .=" order by qb.type asc, qb.createdAt desc";

                    if (isset($offset)) {
                        $sql .= " limit " . $offset . ", " . $limit . "";
                    }

                    $query = $this->db->query($sql);
                    $result = $query->result_array();
                }
                else { 
                    $sql = "select qb.* from fj_questionBank qb where qb.status != 3";             
                    $sql .=" AND qb.createdBy in (
                                                SELECT
                                                    id
                                                FROM
                                                    fj_users
                                                WHERE
                                                    role='1'
                                                )";
                    $sql .=" order by qb.type asc, qb.createdAt desc";

                    if (isset($offset)) {
                        $sql .= " limit " . $offset . ", " . $limit . "";
                    }

                    $query = $this->db->query($sql);
                    $result = $query->result_array();                    
                }
            }
            else {
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * listMyQuestions function
     * Discription : Get all the questions which are created by loggedin user.
     * @author Synergy
     * @param int $limit, string $offset, $createdBy
     * @return array the questions or false or error message.
     */
    function listMyQuestions($limit, $offset, $serachColumn = NULL, $searchText = NULL, $createdBy = NULL) {
        try {
            $currentDate = date('Y-m-d h:i:s');
            $userTypeQuery  = "SELECT * FROM fj_users WHERE id='$createdBy' LIMIT 0,1";
            $userQuery      = $this->db->query($userTypeQuery);
            $userResult     = $userQuery->row();
            
            if ($userResult) {
                if($userResult->role!='1') {
                    $sql = "select * from fj_question where status != 3 AND createdBy='" . $createdBy . "' AND IsMultipleChoice = '1'";
                    if (isset($offset)) {
                        $sql .= " limit " . $offset . ", " . $limit . "";
                    }
                    $query = $this->db->query($sql);
                    $result = $query->result();
                }
                else { 
                    $sql = "select * from fj_question where status != 3 AND createdBy  (
                                                                                        SELECT
                                                                                            id
                                                                                        FROM
                                                                                            fj_users
                                                                                        WHERE
                                                                                            role='1'
                                                                                        ) AND IsMultipleChoice = '1'";
                    if (isset($offset)) {
                        $sql .= " limit " . $offset . ", " . $limit . "";
                    }
                    $query = $this->db->query($sql);
                    $result = $query->result();                    
                }
            }
            else {
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * AddAssessmentQuestion function
     * Discription : Add question into assessment
     * @author Synergy
     * @params array $data
     * @return $questionId or boolean
     */
    function addAssessmentQuestion($data) {

        $resp = $this->db->insert('fj_assessmentQuestions', $data);
        $questionId = $this->db->insert_id();
        if ($resp) {
            return $questionId;
        } else {
            return false;
        }
    }

    /**
     * AddAssessment function
     * Discription : Add assessment
     * @author Synergy
     * @params array $data
     * @return $assessmentId or boolean
     */
    function addAssessment($data) {

        $resp = $this->db->insert('fj_assessments', $data);
        $assessmentId = $this->db->insert_id();
        if ($resp) {
            return $assessmentId;
        } else {
            return false;
        }
    }

    /**
     * GetRandomQuestionFromQuestionBank function
     * Discription : Used to Get random question from fj question bank
     * @author Synergy
     * @params array $number, $questionBank, $countQuestionBank
     * @return array $result
     */
    function getRandomQuestionFromQuestionBank($number, $questionBank, $countQuestionBank) {
        $offset = rand(0, $countQuestionBank);
        if ($offset >= $number)
            $offset = $offset - $number;
        $sql = "select q.* from fj_question q left join fj_questionBankQuestion qb on qb.questionId=q.id where qb.questionBank='" . $questionBank . "' limit $offset, $number";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }
    
    /**
     * UpdateAssessment function
     * Discription : Updates a specific assessment.
     * @author Synergy
     * @param array $data.
     * @param int $id.
     * @return boolean or $id or error message.
     */
    function updateAssessment($data, $id) {
        try {
            $this->db->where('id', $id);
            $resp = $this->db->update('fj_assessments', $data);
            if (!$resp) {
                throw new Exception('Unable to update job data.');
                return false;
            }
            return $id;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * deleteAssessmentQuestions function
     * Discription : Delete a specific question of an assessment.
     * @author Synergy
     * @param int $id.
     * @return boolean or error message.
     */
    function deleteAssessmentQuestions($id) {
        try {
            $this->db->where('assessmentId', $id);
            $this->db->delete('fj_assessmentQuestions');
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * GetAssessment function
     * Discription : Gets a specific assessment detail.
     * @author Synergy
     * @param int $id.
     * @return array the assessment data.
     */
    function getAssessment($id) {
        try {
            $this->db->select('*');
            $this->db->from('fj_assessments');
            $this->db->where('id', $id);
            $query = $this->db->get();
            $result = $query->row_array();
            if (count($result) == 0) {
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * GetAssessment function
     * Discription : Used to fetch all the questions of a particular assessment
     * @author Synergy
     * @param int $id.
     * @return array the assessment Questions.
     */
    function getAssessmentQuestions($id) {
        $sql = "select questionId, questionBank from fj_assessmentQuestions WHERE assessmentId='".$id."' ";
        $query = $this->db->query($sql);
        $result1 = $query->result_array();

        $sql2 = "select questionBankId as questionBank, noOfQuestions as totalQuestion FROM fj_assessmentRandomQuestionnaire WHERE assessmentId='".$id."'";
        $query1 = $this->db->query($sql2);
        $result2 = $query1->result_array();

        $result['individual'] = $result1;
        $result['random'] = $result2;
        return $result;
    }
    
    /**
     * listFjQuestions function
     * Discription : Used to get all the fj question i.e created by fj team
     * @author Synergy
     * @param int $limit, string $offset
     * @return array the interviews data.
     */
    function listFjQuestions($limit, $offset, $serachColumn = NULL, $searchText = NULL, $createdBy = NULL) {
        try {
            $currentDate = date('Y-m-d h:i:s');
            $sql = "select q.* from fj_question q left join fj_users u on u.id=q.createdBy where q.status != 3 AND q.IsMultipleChoice = '1' AND u.role = '1' order by q.createdAt desc";
           
            if (isset($offset)) {
                $sql .= " limit " . $offset . ", " . $limit . "";
            }

            $query = $this->db->query($sql);
            $result = $query->result();
            if (!$result) {
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * listFjQuestions function
     * Discription : List all the fj question created by FJ team.
     * @author Synergy
     * @param int $limit, string $offset
     * @return array the question banks.
     */
    function listFjQuestionsBank($limit, $offset, $serachColumn = NULL, $searchText = NULL, $createdBy = NULL, $createdByStr = NULL, $userRole = NULL) {
        try {
            $currentDate = date('Y-m-d h:i:s');
            $sql = "select qb.* from fj_questionBank qb left join fj_users u on u.id=qb.createdBy where qb.status != 3 AND u.role = '1'";
            
            $sql .= " order by qb.type asc, createdAt desc";
            if (isset($offset)) {
                $sql .= " limit " . $offset . ", " . $limit . "";
            }

            $query = $this->db->query($sql);
            $result = $query->result_array();
            if (!$result) {
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    function getAlreadyCreatedAssessmentSet($userId, $createdById, $assessmentSetName) {
        $sql    = "SELECT * FROM fj_assessments WHERE name='$assessmentSetName' AND status='1' AND (createdBy = '$userId' OR createdBy = '$createdById')";   
                    
        $queryResult = $this->db->query($sql);
        $result = $queryResult->result();
        return $result;
    }
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Jobuserinvite Model
 * Description : Handle all the CRUD operation for Jobuserinvite
 * @author Synergy
 * @createddate : Nov 05, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 21, 2017
 */
class Jobuserinvite_model extends CI_Model {

    /**
     * Initialize variables
     */
    var $table = "fj_jobInvitationForUser";

    /**
     * Responsable for inherit  the parent constructor
     * Responsable for auto load the the session and pagination library
     * @return void
     */
    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('pagination');
    }

    /**
     * Function Name : countForInvitedJobs function
     * Discription : Get all invite job to users.
     * Author : Synergy
     * @param int $id.
     * @return array the count of invited jobs.
     */
    function countForInvitedJobs($job_id) {
        try {
            $query = $this->db->select("count(id) as invitecount")
                                ->from($this->table)
                                ->where('jobId =', $job_id)
                                ->get();
            
            $result = $query->row_array();
           
            if (!$result) {
                throw new Exception('Unable to fetch jobs data.');
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }


    /**
     * Function Name : countForInvitedPendingJobs function
     * Discription : Get all invite pending job to users.
     * Author : Synergy
     * @param int $job_id.
     * @return array the count of pending invitation jobs.
     */
    function countForInvitedPendingJobs($job_id) {
        try {
            
            $query = $this->db->select("count(id) as pendingcount")
                                ->from($this->table)
                                ->where('jobId =', $job_id)
                                ->where('invitationStatus =', 0)
                                ->get();
            
            $result = $query->row_array();
           
            if (!$result) {
                throw new Exception('Unable to fetch jobs data.');
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Jobuserinvite Model
 * Description : Handle all the CRUD operation for Userjob
 * @author Synergy
 * @createddate : Nov 05, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 21, 2017
 */

class Userjob_model extends CI_Model {

    /**
     * Initialize variables
     */
    var $table = "fj_userJob";

    /**
     * Function Name : Responsable for inherit  the parent constructor
     * Responsable for auto load the the session and pagination library
     * @return void
     */
    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('pagination');
    }

    /**
     * countForUserJobs function
     * Discription : Use to get number of times a user applied on a job
     * @author Synergy
     * @param int $job_id
     * @param int $applied_status
     * @return array the count of application
     */
    function countForUserJobs($job_id, $applied_status) {
        
        try {
            $date = date("Y-m-d H:m:s", strtotime('-24 hours', time()));
            $query = $this->db->select("count(id) as appliedCount")
                                ->from($this->table)
                                ->where('jobId =', $job_id);
            
            if( $applied_status == 1 ){
                $query->where('status =', 1);
                $query->where('createdAt <',$date);
            } else if( $applied_status == 2 ){
                $query->where('(status=2)');
				
            } else if( $applied_status == 3 ){
                $query->where('(status=3)');
            } else if( $applied_status == 4 ){
                $query->where('(status=4)');
            } else if( $applied_status == 5 ){
                $query->where('(status=5)');
            } else {
                $query->where('status =', 0);
            }
            
            $selectqry = $query->get();
            $result = $selectqry->row_array();
            
            if (!$result) {
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
	
	function countForUserJobsNew($job_id, $applied_status) {
        
        try {
            $date = date("Y-m-d H:m:s", strtotime('-24 hours', time()));
            $query = $this->db->select("count(id) as appliedCount")
                                ->from($this->table)
                                ->where('jobId =', $job_id);
            
            if( $applied_status == 1 ){
                $query->where('status =', 1);
            } else if( $applied_status == 2 ){
                $query->where('(status=2 or status = 4)');
            } else if( $applied_status == 3 ){
                $query->where('(status=3 or status = 5)');
            } else if( $applied_status == 4 ){
                $query->where('(status=2)');
            } else {
                $query->where('status =', 0);
            }
            $query->where('createdAt >=',$date);
            $selectqry = $query->get();
            $result = $selectqry->row_array();
           
            if (!$result) {
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    /**
     * Function Name : countForUserJob function
     * Discription : Get all users for job
     * Author : Synergy
     * @param int $jobId.
     * @return array the count of user's jobs.
     */
    function countForUserJob($jobId) {
        try {
            $query = $this->db->select("count(id) as userjobcount")
                                ->from($this->table)
                                ->where('jobId =', $jobId)
                                ->get();
            
            $result = $query->row_array();
           
            if (!$result) {
                throw new Exception("Unable to fetch user's job data.");
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
	
    /**
     * Function Name : countForUserJob function
     * Discription : Get all users for job
     * Author : Synergy
     * @param int $jobId.
     * @return array the count of user's jobs.
     */
    function getLevelCountForJob($jobId) {
        try {
            $query = $this->db->select("count(id) as levelCount")
                                ->from('fj_jobPanel')
                                ->where('jobId =', $jobId)
                                ->get();
            
            $result = $query->row_array();
           
            if (!$result) {
                throw new Exception("Unable to fetch user's job data.");
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

	function listForUserJobsNew($job_id,$role) {
        
        try {
            /*if($sorting == 1){
				$sortBy = "UJ.createdAt ASC";
			}elseif($sorting == 2){
				$sortBy = "UJ.createdAt DESC";
			}elseif($sorting == 3){
				$sortBy = "U.fullname ASC";
			}elseif($sorting == 4){
				$sortBy = "U.fullname DESC";
			}else{
				$sortBy = "UJ.createdAt DESC";
			}*/

            $sortBy = "UJ.createdAt DESC";

			/*if(isset($filterValues) && $filterValues != ""){
				$statusFilter = "UJ.status in (".$filterValues.")";
			}else{
				
			}*/

			if($role == 4){
				$userData = $this->session->userdata['logged_in'];
				$userLoggeinId = $userData->id;
				$userRole = $userData->role;
				$statusFilter = "UJ.status in (1,2,3)";
			}else if($role == 5){
				$userData = $this->session->userdata['logged_in'];
				$userLoggeinId = $userData->id;
				$userRole = $userData->role;
				$statusFilter = "UJ.status in (2,4,5)";
			}else{
				$statusFilter = "UJ.status in (1,2,4,3,5)";
			}
			
			
			$filterQuery = "";

			/*if($filterBy > 0){
				if($filterBy == 5 || $filterBy == 7){
					$filterQuery = " UJ.jobResume != '' ";
				}
				if($filterBy == 10){
					$location = $_GET['locationPrefered'];
					$filterQuery = " U.preferredLocation like '%".$location."%' ";
				}
			}*/
			
			
			if($filterQuery != ""){
				$query = $this->db->select("UJ.*, U.fullname,U.email, U.id as userid,U.image,U.preferredLocation, U.resume_path")
                                ->from($this->table.' as UJ')
                                ->join('fj_users as U', 'U.id = UJ.userId', 'left')
                                ->where('UJ.jobId =', $job_id)
                                ->where($statusFilter)
                                ->where($filterQuery)
								->order_by($sortBy);
            }else{
				$query = $this->db->select("UJ.*, U.fullname,U.email, U.id as userid,U.image,U.preferredLocation, U.resume_path")
                                ->from($this->table.' as UJ')
                                ->join('fj_users as U', 'U.id = UJ.userId', 'left')
                                ->where('UJ.jobId =', $job_id)
                                ->where($statusFilter)
								->order_by($sortBy);
			}
			
			
            $selectqry = $query->get();
            $result = $selectqry->result_array();
                       
            if (!$result) {
                return false;
            }
			
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
	
	function listForUserJobsNewLatest($job_id,$role) {
        
        try {
            $date = date("Y-m-d H:m:s", strtotime('-24 hours', time()));
			$sortBy = "UJ.createdAt DESC";
			
			
			if($role == 4){
				
				$statusFilter = "UJ.status in (1)";
			}else if($role == 5){
				
				$statusFilter = "UJ.status in (2)";
			}else{
				$statusFilter = "UJ.status in (1)";
			}
			
			
			
			$filterQuery = 'UJ.createdAt <"'.$date.'"';
			
			
			if($filterQuery != ""){
				$query = $this->db->select("UJ.*, U.fullname,U.email, U.id as userid,U.image,U.preferredLocation, U.resume_path")
                                ->from($this->table.' as UJ')
                                ->join('fj_users as U', 'U.id = UJ.userId', 'left')
                                ->where('UJ.jobId =', $job_id)
                                ->where($statusFilter)
                                ->where($filterQuery)
								->order_by($sortBy);
            }else{
				$query = $this->db->select("UJ.*, U.fullname,U.email, U.id as userid,U.image,U.preferredLocation, U.resume_path")
                                ->from($this->table.' as UJ')
                                ->join('fj_users as U', 'U.id = UJ.userId', 'left')
                                ->where('UJ.jobId =', $job_id)
                                ->where($statusFilter)
								->where($filterQuery)
								->order_by($sortBy);
			}
			
			
            $selectqry = $query->get();
            $result = $selectqry->result_array();
                     
            if (!$result) {
                return false;
            }
			
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
	
	function listForUserJobsNewActive($job_id,$role) {
        
        try {
            
			$sortBy = "UJ.createdAt DESC";
			$statusFilter = "UJ.status in (2,4)";
			
			if($role == 4){
				$userData = $this->session->userdata['logged_in'];
				$userLoggeinId = $userData->id;
				$userRole = $userData->role;
				$RoleFilter = " UJ.shorlistedByL1User IS NOT NULL ";
			}else if($role == 5){
				$userData = $this->session->userdata['logged_in'];
				$userLoggeinId = $userData->id;
				$userRole = $userData->role;
				$RoleFilter = " UJ.shorlistedByL2User IS NOT NULL ";
			}else{
				$RoleFilter = " 1 = 1";
			}
			
			
			$filterQuery = "";
			
			
			
			if($filterQuery != ""){
				$query = $this->db->select("UJ.*, U.fullname,U.email, U.id as userid,U.image,U.preferredLocation, U.resume_path")
                                ->from($this->table.' as UJ')
                                ->join('fj_users as U', 'U.id = UJ.userId', 'left')
                                ->where('UJ.jobId =', $job_id)
                                ->where($statusFilter)
                                ->where($filterQuery)
                                ->where($RoleFilter)
								->order_by($sortBy);
            }else{
				$query = $this->db->select("UJ.*, U.fullname,U.email, U.id as userid,U.image,U.preferredLocation, U.resume_path")
                                ->from($this->table.' as UJ')
                                ->join('fj_users as U', 'U.id = UJ.userId', 'left')
                                ->where('UJ.jobId =', $job_id)
                                ->where($statusFilter)
                                ->where($RoleFilter)
								->order_by($sortBy);
			}
			
			
            $selectqry = $query->get();
			//echo $this->db->last_query();
			//echo "<br><br>";
            $result = $selectqry->result_array();
                       
            if (!$result) {
                return false;
            }
			
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
	
	function listForUserJobsNewOnly($job_id,$role) {
        
        try {
            $date = date("Y-m-d H:m:s", strtotime('-24 hours', time()));
			$sortBy = "UJ.createdAt DESC";
			
			
			if($role == 4){
				$userData = $this->session->userdata['logged_in'];
				$userLoggeinId = $userData->id;
				$userRole = $userData->role;
				$statusFilter = "UJ.status in (1)";
			}else if($role == 5){
				$userData = $this->session->userdata['logged_in'];
				$userLoggeinId = $userData->id;
				$userRole = $userData->role;
				$statusFilter = "UJ.status in (2)";
			}else{
				$statusFilter = "UJ.status in (1)";
			}
			
			
			$filterQuery = "";
			
			
			
			if($filterQuery != ""){
				$query = $this->db->select("UJ.*, U.fullname,U.email, U.id as userid,U.image,U.preferredLocation, U.resume_path")
                                ->from($this->table.' as UJ')
                                ->join('fj_users as U', 'U.id = UJ.userId', 'left')
                                ->where('UJ.jobId =', $job_id)
                                ->where($statusFilter)
                                ->where($filterQuery)
								->where('UJ.createdAt >="'.$date.'"')
								->order_by($sortBy);
            }else{
				$query = $this->db->select("UJ.*, U.fullname,U.email, U.id as userid,U.image,U.preferredLocation, U.resume_path")
                                ->from($this->table.' as UJ')
                                ->join('fj_users as U', 'U.id = UJ.userId', 'left')
                                ->where('UJ.jobId =', $job_id)
                                ->where($statusFilter)
								->where('UJ.createdAt >="'.$date.'"')
								->order_by($sortBy);
			}
			
			
            $selectqry = $query->get();
			
            $result = $selectqry->result_array();
                       
            if (!$result) {
                return false;
            }
			
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
	
	function allDoneJobsByL1($usersJobL2) {
        
        try {
            
            $query = $this->db->select("group_concat(DISTINCT UJ.jobId separator ',') as jobIdL2Str")
                                ->from($this->table.' as UJ');
            
            if( $usersJobL2 != "" ){
                $query->where("UJ.jobId in (".$usersJobL2.")");
            } else {
                $query->where("UJ.jobId in ('')");
            }
            
            $selectqry = $query->get();
            $result = $selectqry->row_array();
                       
            if (!$result) {
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
	
	function listForUserJobsNewLatestAction($job_id,$role) {
        
        try {
            
			$sortBy = "UJ.updatedAt DESC";
			$statusFilter = "UJ.status in (2,3,4,5)";
			
			if($role == 4){
				$userData = $this->session->userdata['logged_in'];
				$userLoggeinId = $userData->id;
				$userRole = $userData->role;
				$RoleFilter = "UJ.shorlistedByL1User IS NOT NULL ";
			}else if($role == 5){
				$userData = $this->session->userdata['logged_in'];
				$userLoggeinId = $userData->id;
				$userRole = $userData->role;
				$RoleFilter = "UJ.shorlistedByL2User IS NOT NULL ";
			}else{
				$RoleFilter = " 1 = 1";
			}
			
			
			$filterQuery = "";
			
			
			
			if($filterQuery != ""){
				$query = $this->db->select("UJ.*, U.fullname,U.email, U.id as userid,U.image,U.preferredLocation, U.resume_path")
                                ->from($this->table.' as UJ')
                                ->join('fj_users as U', 'U.id = UJ.userId', 'left')
                                ->where('UJ.jobId =', $job_id)
                                ->where($statusFilter)
                                ->where($filterQuery)
                                ->where($RoleFilter)
								->order_by($sortBy);
            }else{
				$query = $this->db->select("UJ.*, U.fullname,U.email, U.id as userid,U.image,U.preferredLocation, U.resume_path")
                                ->from($this->table.' as UJ')
                                ->join('fj_users as U', 'U.id = UJ.userId', 'left')
                                ->where('UJ.jobId =', $job_id)
                                ->where($statusFilter)
                                ->where($RoleFilter)
								->order_by($sortBy);
			}
			
			
            $selectqry = $query->get();
            $result = $selectqry->result_array();
                       
            if (!$result) {
                return false;
            }
			
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Function Name : getJobApplicationLevel function
     * Discription : Use to get all applications data with level
     * Author : Synergy
     * @param int $id, string $searchType
     * @return array of application data
     */
    function getJobApplicationLevel($id, $searchType) {
        try {
            if($searchType=='totalPending') {
                $where = "
                        (CASE   WHEN 
                                (countRecord.JobLevel='L2') 
                                    THEN
                                        countRecord.JobAplliedStatus='Pending' OR
                                        countRecord.JobAplliedStatus='Shortlisted By L1' 
                                    ELSE
                                        countRecord.JobAplliedStatus='Pending'
                                END
                        ) ";
            }            
            else if($searchType=='pendingAtL1') {
                $where = " countRecord.JobAplliedStatus='Pending' ";
            }
            
            else if($searchType=='pendingAtL2') {
                $where = " countRecord.JobAplliedStatus='Shortlisted By L1' ";
            }
            
            else if($searchType=='totalShorlisted') {
                $where = "
                        (CASE   WHEN 
                                (countRecord.JobLevel='L2') 
                                    THEN
                                        countRecord.JobAplliedStatus='Shortlisted By L2' 
                                    ELSE
                                        countRecord.JobAplliedStatus='Shortlisted By L1' 
                                END
                        ) ";
            }            
            else if($searchType=='shorlistedAtL1') {
                $where = " countRecord.JobAplliedStatus='Shortlisted By L1' ";
            }            
            else if($searchType=='shorlistedAtL2') {
                $where = " countRecord.JobAplliedStatus='Shortlisted By L2' ";
            }
            
            else if($searchType=='totalRejected') {
                $where = "
                        (CASE   WHEN 
                                (countRecord.JobLevel='L2') 
                                    THEN
                                        countRecord.JobAplliedStatus='Rejected By L1' OR
                                        countRecord.JobAplliedStatus='Rejected By L2' 
                                    ELSE
                                        countRecord.JobAplliedStatus='Pending'
                                END
                        ) ";
            }            
            else if($searchType=='rejectedAtL1') {
                $where = " countRecord.JobAplliedStatus='Rejected By L1' ";
            }            
            else if($searchType=='rejectedAtL2') {
                $where = " countRecord.JobAplliedStatus='Rejected By L2' ";
            }
            
            $sql = "
                    SELECT
                        SUM(countRecord.count) AS records
                    FROM
                        (
                        SELECT
                            panelLevel.JobLevel,
                            panelLevel.JobAplliedStatus,
                            COUNT(JobAplliedStatus) AS count
                        FROM
                            (
                            SELECT
                                (CASE UJ.status WHEN 1 THEN 'Pending' WHEN 2 THEN 'Shortlisted By L1' WHEN 3 THEN 'Rejected By L1' WHEN 4 THEN 'Shortlisted By L2'  WHEN 5 THEN 'Rejected By L2' ELSE '' END) as JobAplliedStatus,
                                (CASE COUNT(DISTINCT(JP.level)) WHEN 1 THEN 'L1' WHEN 2 THEN 'L2' ELSE '' END) AS JobLevel
                            FROM 
                                fj_userJob      UJ
                            LEFT JOIN 
                                fj_jobPanel     JP      ON UJ.jobId=JP.jobId
                            LEFT JOIN 
                                fj_users        U       ON UJ.userId=U.id
                            LEFT JOIN 
                                fj_jobs         J       ON UJ.jobId=J.id
                            WHERE
                                J.id='$id'
                            GROUP BY 
                                UJ.id 
                            ORDER BY 
                                UJ.jobId
                            ) panelLevel
                        GROUP BY
                            panelLevel.JobAplliedStatus
                        ) countRecord
                    WHERE
                        ".$where."
                    ";
                    
            $query  = $this->db->query($sql);
            $result = $query->row_array();

            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
	
	function getUserJobPanel($jobId, $userId){
		$query = $this->db->select("*")
                                ->from('fj_jobPanel as UJ')
                                ->where('UJ.jobId =', $jobId)
                                ->where('UJ.userId = '.$userId);
		 $selectqry = $query->get();
            $result = $selectqry->result_array();
			
			return  $result;
								
		
	}
}
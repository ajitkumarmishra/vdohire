<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Useranswer Model
 * Description : Handle all the CRUD operation for User answer
 * @author Synergy
 * @createddate : Nov 12, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 20, 2017
 */

class Useranswer_model extends CI_Model {

    /**
     * Initializing variables
     */
    var $table = "fj_userAnswers";

    /**
     * Responsable for inherit the parent constructor
     * Responsable for auto load the session and pagination library
     * @return void
     */
    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('pagination');
    }

    /**
     * Function Name : countForUserInterview function
     * Discription : Get all users count who has given interview on a specific interview set
     * Author : Synergy
     * @param int $interview_id.
     * @return array of interview count.
     */
    function countForUserInterview($interview_id) {
        
        try {
            
            $query = $this->db->select("count(id) as interviewcount")
                                ->from($this->table)
                                ->where('interviewId =', $interview_id)
                                ->get();
            
            $result = $query->row_array();
           
            if (!$result) {
                throw new Exception('Unable to fetch interview data.');
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
}
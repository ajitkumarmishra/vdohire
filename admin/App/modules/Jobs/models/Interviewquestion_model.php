<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Interviewquestion Model
 * Description : Handle all the CRUD operation for Interviews Questions
 * @author Synergy
 * @createddate : Nov 12, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 20, 2017
 */

class Interviewquestion_model extends CI_Model {

    /**
     * Initializing variables
     */
    var $table = "fj_interviewQuestions";

    /**
     * Responsable for inherit the parent constructor
     * Responsable for auto load the session and pagination library
     * @return void
     */
    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('pagination');
    }

    /**
     * Function Name : countForUserInterviewQuestion function
     * Discription : Get all interview interviewIds count in which specific question exist
     * Author : Synergy
     * @param int $question_id.
     * @return array of interview question count.
     */
    function countForUserInterviewQuestion($question_id) {
        
        try {
            
            $query = $this->db->select("count(id) as interviewquestioncount")
                                ->from($this->table)
                                ->where('questionId =', $question_id)
                                ->where('status !=', 3)                    
                                ->get();
            
            $result = $query->row_array();
           
            if (!$result) {
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Function Name : countForUserInterview function
     * Discription : Get all interview interviewIds in which specific question exist
     * Author : Synergy
     * @param int $question_id.
     * @return array of interview setID.
     */
    function interviewSetId($question_id) {        
        try {            
            $sql    = " SELECT GROUP_CONCAT(interviewId) AS setID FROM fj_interviewQuestions WHERE questionId='$question_id' ";
            $query  = $this->db->query($sql);            
            $result = $query->row_array();

            if (!$result) {
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
}
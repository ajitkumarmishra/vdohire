<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Jobuserinvite Model
 * Description : Handle all the CRUD operation for Jobuserinvite
 * @author Synergy
 * @createddate : Nov 05, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 21, 2017
 */

class User_model extends CI_Model {

    /**
     * Initialize variables
     */
    var $table = "fj_users";

    /**
     * Responsable for inherit  the parent constructor
     * Responsable for auto load the the session and pagination library
     * @return void
     */
    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('pagination');
    }

    /**
     * Function Name : getUserIdsForJob function
     * Discription : Gets a specific job detail.
     * @param int $id.
     * @return array the jobs data.
     */
    function getUserIdsForJob($createdBy) {
        try {
            $this->db->select('GROUP_CONCAT(DISTINCT id SEPARATOR ",") as userIds');
            $this->db->from($this->table);
            $this->db->where('createdBy', $createdBy);
            $query = $this->db->get();
            $result = $query->row_array();
            if (count($result) == 0) {
                throw new Exception('Unable to fetch user ids data.');
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
	
	
    
    /**
     * Function Name : GetUser function
     * Discription : Gets a specific user detail.
     * @param int $id.
     * @return array the users data.
     */
    function getUser($id) {
		
        try {
            $this->db->select('*');
            $this->db->from('fj_users');
            $this->db->where('id', $id);
            $query = $this->db->get();
            $result = $query->row_array();
            if (count($result) == 0) {
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
}



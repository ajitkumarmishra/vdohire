<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Question Model
 * Description : Handle all the CRUD operation for Questions
 * @author Synergy
 * @createddate : Nov 12, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 20, 2017
 */

class Question_model extends CI_Model {

    /**
     * Initializing variables
     */
    var $question_table = "fj_question";

    /**
     * Responsable for inherit the parent constructor
     * @return void
     */
    function __construct() {
        parent::__construct();
    }

    /**
     * Function Name : getQuestion function
     * Discription : Get details of a particular question
     * Author : Synergy
     * @param int $id.
     * @return array of question detail data.
     */
    function getQuestion($id) {
        $this->db->where('id', $id);
        $this->db->from($this->question_table);
        $result = $this->db->get();
        return $result->row_array();
    }
    
    /**
     * Function Name : updateQuestion function
     * Discription : Use to updates a specific question.
     * Author : Synergy
     * @param array $data.
     * @param int $id.
     * @return boolean or string.
     */
    function updateQuestion($data, $id) {
        try {
            $this->db->where('id', $id);
            $resp = $this->db->update('fj_question', $data);
            if (!$resp) {
                throw new Exception('Unable to update question data.');
                return false;
            }
            return $id;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Function Name : AddQuestion function
     * Discription : Creates a question
     * Author : Synergy
     * @params array $data
     * @return boolean or int $questionId
     */
    function copyQuestion($data) {

        $resp = $this->db->insert('fj_question', $data);
        $questionId = $this->db->insert_id();
        if ($resp) {
            return $questionId;
        } else {
            return false;
        }
    }
}
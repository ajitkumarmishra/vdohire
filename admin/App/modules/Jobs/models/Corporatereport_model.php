<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Corporatereport Model
 * Description : Handle all the CRUD operation for Corporatereport
 * @author Synergy
 * @createddate : Nov 22, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 20, 2017
 */

class Corporatereport_model extends CI_Model {

    /**
     * Responsable for inheriting parent constructor
     * Responsable for auto load the the session and pagination library
     * @return void
     */
    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('pagination');
    }

    /**
     * Description : Use to get all jobs created by specific corporate User
     * Author : Synergy
     * @param int $userId
     * @return array with job data
     */
    function totalJobs($userId) {
        try {
            $sql = "
                    SELECT  
                        COUNT(J.id) AS totalJobs
                    FROM    
                        fj_jobs J
                    LEFT JOIN 
                        fj_users U 
                    ON 
                        U.id=J.createdBy 
                    WHERE 
                        J.status IN (1,2)               AND
                        (CASE
                                WHEN U.role = '2'
                                    THEN U.id 
                                WHEN U.role = '4'
                                    THEN (SELECT id FROM fj_users WHERE id=(SELECT createdBy FROM fj_users WHERE id=J.createdBy))
                                END
                        ) = '$userId' ";
            $query = $this->db->query($sql);
            $result = $query->row_array();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Description : Use to get all open/active jobs created by specific corporate User
     * Author : Synergy
     * @param int $userId
     * @return array with job data
     */
    function openJobs($userId) {
        try {
            $sql = "
                    SELECT  
                        COUNT(J.id) AS openJobs
                    FROM    
                        fj_jobs J
                    LEFT JOIN 
                        fj_users U 
                    ON 
                        U.id=J.createdBy 
                    WHERE 
                        J.status IN (1,2)               AND
                        DATE(J.openTill)>=DATE(NOW())   AND 
                        (CASE
                                WHEN U.role = '2'
                                    THEN U.id 
                                WHEN U.role = '4'
                                    THEN (SELECT id FROM fj_users WHERE id=(SELECT createdBy FROM fj_users WHERE id=J.createdBy))
                                END
                        ) = '$userId' ";
            $query = $this->db->query($sql);
            $result = $query->row_array();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Description : Use to get all closed/inactive jobs created by specific corporate User
     * Author : Synergy
     * @param int $userId
     * @return array with job data
     */
    function closedJobs($userId) {
        try {
            $sql = "
                    SELECT  
                        COUNT(J.id) AS closedJobs
                    FROM    
                        fj_jobs J
                    LEFT JOIN 
                        fj_users U 
                    ON 
                        U.id=J.createdBy 
                    WHERE 
                        J.status IN (1,2)               AND
                        DATE(J.openTill)<=DATE(NOW())   AND 
                        (CASE
                                WHEN U.role = '2'
                                    THEN U.id 
                                WHEN U.role = '4'
                                    THEN (SELECT id FROM fj_users WHERE id=(SELECT createdBy FROM fj_users WHERE id=J.createdBy))
                                END
                        ) = '$userId' ";
            $query = $this->db->query($sql);
            $result = $query->row_array();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Description : Use to get last week jobs created by specific corporate User
     * Author : Synergy
     * @param int $userId
     * @return array with job data
     */
    function lastWeekJobs($userId) {
        try {
            $sql = "
                    SELECT  
                        COUNT(J.id) AS lastWeekJobs
                    FROM    
                        fj_jobs J
                    LEFT JOIN 
                        fj_users U 
                    ON 
                        U.id=J.createdBy 
                    WHERE 
                        J.status IN (1,2)               AND
                        (CASE
                                WHEN U.role = '2'
                                    THEN U.id 
                                WHEN U.role = '4'
                                    THEN (SELECT id FROM fj_users WHERE id=(SELECT createdBy FROM fj_users WHERE id=J.createdBy))
                                END
                        ) = '$userId'                   AND 
                        DATE(J.createdAt) 
                            BETWEEN
                            DATE_SUB( CURDATE( ) , INTERVAL (dayofweek(CURDATE())+5) DAY ) 
                            AND
                            DATE_SUB( CURDATE( ) , INTERVAL (dayofweek(CURDATE())) DAY )";
            $query = $this->db->query($sql);
            $result = $query->row_array();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Description : Use to get last month jobs created by specific corporate User
     * Author : Synergy
     * @param int $userId
     * @return array with job data
     */
    function lastMonthJobs($userId) {
        try {
            $sql = "SELECT  
                        COUNT(J.id) AS lastMonthJobs
                    FROM    
                        fj_jobs J
                    LEFT JOIN 
                        fj_users U 
                    ON 
                        U.id=J.createdBy 
                    WHERE 
                        J.status IN (1,2)               AND
                        (CASE
                                WHEN U.role = '2'
                                    THEN U.id 
                                WHEN U.role = '4'
                                    THEN (SELECT id FROM fj_users WHERE id=(SELECT createdBy FROM fj_users WHERE id=J.createdBy))
                                END
                        ) = '$userId'                   AND 
                        DATE(J.createdAt) BETWEEN CURDATE() - INTERVAL 30 DAY AND CURDATE()
                    ";
            $query = $this->db->query($sql);
            $result = $query->row_array();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Description : Use to get total applications on jobs created by specific corporate User
     * Author : Synergy
     * @param int $userId
     * @return array with totalApplication data
     */
    function totalApplication($userId) {
        try {
            $sql = "SELECT COUNT(id) AS totalApplication
                    FROM fj_userJob
                    WHERE 
                        jobId IN
                                (                
                                SELECT id
                                FROM
                                    (SELECT
                                        FT.id,
                                        FT.status,
                                        FT.createdAt,
                                        (
                                            CASE
                                            WHEN (SELECT role FROM fj_users U WHERE U.id=FT.createdBy)='2'
                                                THEN (SELECT id FROM fj_users WHERE id=FT.createdBy)
                                            WHEN (SELECT role FROM fj_users U WHERE U.id=FT.createdBy)='4'
                                                THEN (SELECT id FROM fj_users WHERE id=(SELECT createdBy FROM fj_users WHERE id=FT.createdBy))
                                            END
                                        ) AS companyId
                                    FROM
                                        fj_jobs FT
                                    ) mergedData
                                WHERE
                                    mergedData.status IN (1,2)          AND
                                    mergedData.companyId = '$userId'
                                )
                        ";
            $query = $this->db->query($sql);
            $result = $query->row_array();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Description : Use to get total pending applications on jobs created by specific corporate User
     * Author : Synergy
     * @param int $userId
     * @return array with pendingApplication data
     */
    function pendingApplication($userId) {
        try {
            $sql = "
                    SELECT COUNT(id) AS pendingApplication
                    FROM
                        (
                        SELECT 
                            UJ.shorlistedByL1User,
                            UJ.shorlistedByL2User,
                            UJ.id,
                            U.fullname  AS AppUserName,
                            U.email     AS AppUserEmail,
                            U.mobile    AS AppUserMobile,
                            J.fjCode    AS JobCode,
                            J.title     AS JobTitle,
                            (CASE UJ.status WHEN 1 THEN 'Pending' WHEN 2 THEN 'Shortlisted By L1' WHEN 3 THEN 'Rejected By L1' WHEN 4 THEN 'Shortlisted By L2'  WHEN 5 THEN 'Rejected By L2' ELSE '' END) as JobAplliedStatus,
                            (CASE COUNT(DISTINCT(JP.level)) WHEN 1 THEN 'L1' WHEN 2 THEN 'L2' ELSE '' END) AS JobLevel,
                            J.createdBy,
                            UJ.createdAt                                     
                        FROM 
                            fj_userJob      UJ
                        JOIN 
                            fj_jobPanel     JP,
                            fj_users        U,
                            fj_jobs         J
                        WHERE
                            UJ.jobId=JP.jobId   AND
                            UJ.userId=U.id      AND
                            UJ.jobId=J.id       AND
                            J.createdBy IN (SELECT id FROM fj_users WHERE createdBy='$userId' OR id='$userId')                                
                        GROUP BY UJ.id ORDER BY UJ.jobId 
                        ) FT
                    WHERE
                        (FT.JobAplliedStatus='Pending') OR
                        (FT.JobAplliedStatus='Shortlisted By L1' AND JobLevel='L2')
                        ";
            $query = $this->db->query($sql);
            $result = $query->row_array();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Description : Use to get total shortlisted applications on jobs created by specific corporate User
     * Author : Synergy
     * @param int $userId
     * @return array of shortlistedApplication data
     */
    function shortlistedApplication($userId) {
        try {
            $sql = "
                    SELECT COUNT(id) AS shortlistedApplication
                    FROM
                        (
                        SELECT 
                            UJ.shorlistedByL1User,
                            UJ.shorlistedByL2User,
                            UJ.id,
                            U.fullname  AS AppUserName,
                            U.email     AS AppUserEmail,
                            U.mobile    AS AppUserMobile,
                            J.fjCode    AS JobCode,
                            J.title     AS JobTitle,
                            (CASE UJ.status WHEN 1 THEN 'Pending' WHEN 2 THEN 'Shortlisted By L1' WHEN 3 THEN 'Rejected By L1' WHEN 4 THEN 'Shortlisted By L2'  WHEN 5 THEN 'Rejected By L2' ELSE '' END) as JobAplliedStatus,
                            (CASE COUNT(DISTINCT(JP.level)) WHEN 1 THEN 'L1' WHEN 2 THEN 'L2' ELSE '' END) AS JobLevel,
                            J.createdBy,
                            UJ.createdAt                            
                        FROM 
                            fj_userJob      UJ
                        JOIN 
                            fj_jobPanel     JP,
                            fj_users        U,
                            fj_jobs         J
                        WHERE
                            UJ.jobId=JP.jobId   AND
                            UJ.userId=U.id      AND
                            UJ.jobId=J.id       AND
                            J.createdBy IN (SELECT id FROM fj_users WHERE createdBy='$userId' OR id='$userId')                                
                        GROUP BY UJ.id ORDER BY UJ.jobId 
                        ) FT
                    WHERE
                        (FT.JobAplliedStatus='Shortlisted By L1' AND JobLevel='L1') OR
                        (FT.JobAplliedStatus='Shortlisted By L2' AND JobLevel='L2')
                        ";
            $query = $this->db->query($sql);
            $result = $query->row_array();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Description : Use to get total shortlisted applications on jobs created by specific corporate User
     * Author : Synergy
     * @param int $userId
     * @return array of rejectedApplication data
     */
    function rejectedApplication($userId) {
        try {
            $sql = "
                    SELECT COUNT(id) AS rejectedApplication
                    FROM
                        (
                        SELECT 
                            UJ.shorlistedByL1User,
                            UJ.shorlistedByL2User,
                            UJ.id,
                            U.fullname  AS AppUserName,
                            U.email     AS AppUserEmail,
                            U.mobile    AS AppUserMobile,
                            J.fjCode    AS JobCode,
                            J.title     AS JobTitle,
                            (CASE UJ.status WHEN 1 THEN 'Pending' WHEN 2 THEN 'Shortlisted By L1' WHEN 3 THEN 'Rejected By L1' WHEN 4 THEN 'Shortlisted By L2'  WHEN 5 THEN 'Rejected By L2' ELSE '' END) as JobAplliedStatus,
                            (CASE COUNT(DISTINCT(JP.level)) WHEN 1 THEN 'L1' WHEN 2 THEN 'L2' ELSE '' END) AS JobLevel,
                            J.createdBy,
                            UJ.createdAt                            
                        FROM 
                            fj_userJob      UJ
                        JOIN 
                            fj_jobPanel     JP,
                            fj_users        U,
                            fj_jobs         J
                        WHERE
                            UJ.jobId=JP.jobId   AND
                            UJ.userId=U.id      AND
                            UJ.jobId=J.id       AND
                            J.createdBy IN (SELECT id FROM fj_users WHERE createdBy='$userId' OR id='$userId')                                
                        GROUP BY UJ.id ORDER BY UJ.jobId 
                        ) FT
                    WHERE
                        (FT.JobAplliedStatus='Rejected By L1') OR
                        (FT.JobAplliedStatus='Rejected By L2')
                        ";
            $query = $this->db->query($sql);
            $result = $query->row_array();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Description : Use to get total heighest pending application on jobs created by specific corporate User
     * Author : Synergy
     * @param int $userId
     * @return array of highest pending application data
     */
    function highestPendingJob($userId) {
        try {
            $sql = "
                    SELECT
                        COUNT(FinaleTable.id) AS TotalPendingJobs,
                        GROUP_CONCAT(FinaleTable.jobCode) AS PendingJobIds,
                        FinaleTable.HighestPendingUsers, 
                        GROUP_CONCAT(FinaleTable.JobId) AS jobIds
                    FROM
                        (
                        SELECT
                            count(FinalTable.jobCode) AS HighestPendingUsers ,
                            FinalTable.*
                        FROM
                            (
                            SELECT 
                                FT.*
                            FROM
                                (
                                SELECT 
                                    J.id        AS JobId,
                                    UJ.shorlistedByL1User,
                                    UJ.shorlistedByL2User,
                                    UJ.id,
                                    U.fullname  AS AppUserName,
                                    U.email     AS AppUserEmail,
                                    U.mobile    AS AppUserMobile,
                                    J.fjCode    AS JobCode,
                                    J.title     AS JobTitle,
                                    (CASE UJ.status WHEN 1 THEN 'Pending' WHEN 2 THEN 'Shortlisted By L1' WHEN 3 THEN 'Rejected By L1' WHEN 4 THEN 'Shortlisted By L2'  WHEN 5 THEN 'Rejected By L2' ELSE '' END) as JobAplliedStatus,
                                    (CASE COUNT(DISTINCT(JP.level)) WHEN 1 THEN 'L1' WHEN 2 THEN 'L2' ELSE '' END) AS JobLevel,
                                    J.createdBy,
                                    UJ.createdAt                            
                                FROM 
                                    fj_userJob      UJ
                                JOIN 
                                    fj_jobPanel     JP,
                                    fj_users            U,
                                    fj_jobs         J
                                WHERE
                                    UJ.jobId=JP.jobId   AND
                                    UJ.userId=U.id      AND
                                    UJ.jobId=J.id       AND
                                    J.createdBy IN (SELECT id FROM fj_users WHERE createdBy='$userId' OR id='$userId')
                                GROUP BY UJ.id ORDER BY UJ.jobId 
                                ) FT
                            WHERE
                                (FT.JobAplliedStatus='Pending') OR
                                (FT.JobAplliedStatus='Shortlisted By L1' AND JobLevel='L2')
                            ) FinalTable
                        GROUP BY FinalTable.jobCode
                        ORDER BY HighestPendingUsers DESC
                        ) FinaleTable
                    GROUP BY FinaleTable.HighestPendingUsers
                    ORDER BY FinaleTable.HighestPendingUsers DESC
                    LIMIT 0,1
                    ";
            $query = $this->db->query($sql);
            $result = $query->row_array();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Description : Use to get total least pending application on jobs created by specific corporate User
     * Author : Synergy
     * @param int $userId
     * @return array of least pending application data
     */
    function leastApplicationJob($userId) {
        try {
            $sql = "
                    SELECT 
                        J.fjCode, 
                        J.id, 
                        J.createdBy, 
                                    (
                                    SELECT 
                                        COUNT(UJ.jobId)
                                    FROM 
                                        fj_userJob UJ
                                    WHERE
                                        UJ.jobId = J.id
                                    ) AS receivedApplications
                    FROM 
                        fj_jobPanel JP
                    JOIN
                        fj_jobs     J
                    WHERE 
                        J.id=JP.jobId   AND
                        J.createdBy     IN (SELECT id FROM fj_users WHERE createdBy='$userId' OR id='$userId')
                    ORDER BY receivedApplications ASC
                    LIMIT 0,1
                    ";
            $query = $this->db->query($sql);
            $result = $query->row_array();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Description : Use to get longest open jobs created by specific corporate User
     * Author : Synergy
     * @param int $userId
     * @return array of least longest open jobs data
     */
    function longestOpenJob($userId) {
        try {
            $sql = "
                    SELECT 
                        id, fjCode, openTill, createdAt, GROUP_CONCAT(fjCode) AS LongestJobIds, GROUP_CONCAT(id) AS jobIds, COUNT(id) AS LongestJobCount
                    FROM
                        fj_jobs
                    WHERE
                        createdBy IN (SELECT id FROM fj_users WHERE createdBy='$userId' OR id='$userId')  AND
                        DATE(openTill)>=CURDATE()
                    GROUP BY
                        createdAt, openTill
                    ORDER BY
                        createdAt ASC, openTill DESC
                    LIMIT 0,1
                    ";
            $query = $this->db->query($sql);
            $result = $query->row_array();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Description : Use to permission to manage jobs for corporate user
     * Author : Synergy
     * @param int $userId
     * @return array of data
     */
    function manageJobs($userId) {
        try {
            $sql = "
                    SELECT 
                        COUNT(permissionId) AS manageJobs,
                        UP.permissionId,
                        P.slug,
                        U.* 
                    FROM 
                        fj_users U
                    JOIN
                        fj_userPermission 	UP,
                        fj_permissions		P
                    WHERE
                        U.createdBy='$userId'   AND
                        U.id=UP.userId          AND
                        UP.permissionId=P.id    AND
                        UP.permissionId='4'     AND
                        U.status IN (1,2)
                    GROUP BY permissionId
                    ";
            $query = $this->db->query($sql);
            $result = $query->row_array();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Description : Use to get permission to manage interview for corporate user
     * Author : Synergy
     * @param int $userId
     * @return array of data
     */
    function manageInterview($userId) {
        try {
            $sql = "
                    SELECT 
                        COUNT(permissionId) AS manageInterview,
                        UP.permissionId,
                        P.slug,
                        U.* 
                    FROM 
                        fj_users U
                    JOIN
                        fj_userPermission 	UP,
                        fj_permissions		P
                    WHERE
                        U.createdBy='$userId'   AND
                        U.id=UP.userId          AND
                        UP.permissionId=P.id    AND
                        UP.permissionId='2'     AND
                        U.status IN (1,2)
                    GROUP BY permissionId
                    ";
            $query = $this->db->query($sql);
            $result = $query->row_array();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Description : Use to get permission to manage Assessment for corporate user
     * Author : Synergy
     * @param int $userId
     * @return array of data
     */
    function manageAssessment($userId) {
        try {
            $sql = "
                    SELECT 
                        COUNT(permissionId) AS manageAssessment,
                        UP.permissionId,
                        P.slug,
                        U.* 
                    FROM 
                        fj_users U
                    JOIN
                        fj_userPermission 	UP,
                        fj_permissions		P
                    WHERE
                        U.createdBy='$userId'   AND
                        U.id=UP.userId          AND
                        UP.permissionId=P.id    AND
                        UP.permissionId='3'     AND
                        U.status IN (1,2)
                    GROUP BY permissionId
                    ";
            $query = $this->db->query($sql);
            $result = $query->row_array();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Description : Use to get permission for queue management for corporate user
     * Author : Synergy
     * @param int $userId
     * @return array of data
     */
    function queueManagement($userId) {
        try {
            $sql = "
                    SELECT 
                        COUNT(permissionId) AS queueManagement,
                        UP.permissionId,
                        P.slug,
                        U.* 
                    FROM 
                        fj_users U
                    JOIN
                        fj_userPermission 	UP,
                        fj_permissions		P
                    WHERE
                        U.createdBy='$userId'   AND
                        U.id=UP.userId          AND
                        UP.permissionId=P.id    AND
                        UP.permissionId='5'     AND
                        U.status IN (1,2)
                    GROUP BY permissionId
                    ";
            $query = $this->db->query($sql);
            $result = $query->row_array();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Description : Use to get permission to manage search candidate for corporate user
     * Author : Synergy
     * @param int $userId
     * @return array of data
     */
    function searchCandidate($userId) {
        try {
            $sql = "
                    SELECT 
                        COUNT(permissionId) AS searchCandidate,
                        UP.permissionId,
                        P.slug,
                        U.* 
                    FROM 
                        fj_users U
                    JOIN
                        fj_userPermission 	UP,
                        fj_permissions		P
                    WHERE
                        U.createdBy='$userId'   AND
                        U.id=UP.userId          AND
                        UP.permissionId=P.id    AND
                        UP.permissionId='1'     AND
                        U.status IN (1,2)
                    GROUP BY permissionId
                    ";
            $query = $this->db->query($sql);
            $result = $query->row_array();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Description : Use to get all assigned jobs to corporate L1 or L2 user
     * Author : Synergy
     * @param int $userId
     * @return array of data
     */
    function assignedJobs($userId) {
        try {
            $sql = "
                    SELECT 
                        COUNT(JP.id) AS assignedJobs
                    FROM 
                        fj_jobPanel JP
                    WHERE
                        JP.userId='$userId'
                    ";
            $query = $this->db->query($sql);
            $result = $query->row_array();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Description : Use to get all assiged active jobs to corporate L1 or L2 user
     * Author : Synergy
     * @param int $userId
     * @return array of data
     */
    function assignedActiveJobs($userId) {
        try {
            $sql = "
                    SELECT 
                        COUNT(JP.id) AS assignedActiveJobs
                    FROM 
                        fj_jobPanel JP
                    WHERE
                        JP.userId='$userId'
                    ";
            $query = $this->db->query($sql);
            $result = $query->row_array();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Description : Use to get all assiged active last week jobs
     * Author : Synergy
     * @param int $userId
     * @return array of data
     */
    function assignedLastWeekJobs($userId) {
        try {
            $sql = "
                    SELECT 
                        COUNT(JP.id) AS assignedLastWeekJobs
                    FROM 
                        fj_jobPanel JP
                    WHERE
                        JP.userId='$userId' AND
                        JP.createdAt >= curdate() - INTERVAL DAYOFWEEK(curdate())+6 DAY AND
                        JP.createdAt < curdate() - INTERVAL DAYOFWEEK(curdate())-1 DAY
                    ";
            $query = $this->db->query($sql);
            $result = $query->row_array();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Description : Use to get all assiged last month month active jobs
     * Author : Synergy
     * @param int $userId
     * @return array of data
     */
    function assignedLastMonthJobs($userId) {
        try {
            $sql = "
                    SELECT 
                        COUNT(JP.id) AS assignedLastMonthJobs
                    FROM 
                        fj_jobPanel JP
                    WHERE
                        JP.userId='$userId' AND
                        YEAR(JP.createdAt) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH) AND
                        MONTH(JP.createdAt) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH)
                    ";
            $query = $this->db->query($sql);
            $result = $query->row_array();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Description : Use to get all application on assiged active jobs to corporate L1 or L2 user
     * Author : Synergy
     * @param int $userId
     * @return array of data
     */
    function assignedTotalApplication($userId) {
        try {
            $sql = "                    
                    SELECT COUNT(id) AS assignedTotalApplication
                    FROM
                        (
                        SELECT 
                            J.id        AS JobId,
                            UJ.shorlistedByL1User,
                            UJ.shorlistedByL2User,
                            UJ.id,
                            U.fullname  AS AppUserName,
                            U.email     AS AppUserEmail,
                            U.mobile    AS AppUserMobile,
                            J.fjCode    AS JobCode,
                            J.title     AS JobTitle,
                            (CASE UJ.status WHEN 1 THEN 'Pending' WHEN 2 THEN 'Shortlisted By L1' WHEN 3 THEN 'Rejected By L1' WHEN 4 THEN 'Shortlisted By L2'  WHEN 5 THEN 'Rejected By L2' ELSE '' END) as JobAplliedStatus,
                            (CASE COUNT(DISTINCT(JP.level)) WHEN 1 THEN 'L1' WHEN 2 THEN 'L2' ELSE '' END) AS JobLevel,
                            J.createdBy,
                            UJ.createdAt                                     
                        FROM 
                            fj_userJob      UJ
                        JOIN 
                            fj_jobPanel     JP,
                            fj_users        U,
                            fj_jobs         J
                        WHERE
                            UJ.jobId=JP.jobId   AND
                            UJ.userId=U.id      AND
                            UJ.jobId=J.id                                      
                        GROUP BY UJ.id ORDER BY UJ.jobId 
                        ) FT
                    WHERE
                        JobId IN (SELECT jobId FROM fj_jobPanel JP WHERE userId='$userId')
                    ";
            $query = $this->db->query($sql);
            $result = $query->row_array();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Description : Use to get all pending application on assiged active jobs to corporate L1 or L2 user
     * Author : Synergy
     * @param int $userId
     * @return array of data
     */
    function assignedPendingApplication($userId) {
        try {
            $sql = "
                    SELECT COUNT(id) AS assignedPendingApplication
                    FROM
                        (
                        SELECT 
                            J.id        AS JobId,
                            UJ.shorlistedByL1User,
                            UJ.shorlistedByL2User,
                            UJ.id,
                            U.fullname  AS AppUserName,
                            U.email     AS AppUserEmail,
                            U.mobile    AS AppUserMobile,
                            J.fjCode    AS JobCode,
                            J.title     AS JobTitle,
                            (CASE UJ.status WHEN 1 THEN 'Pending' WHEN 2 THEN 'Shortlisted By L1' WHEN 3 THEN 'Rejected By L1' WHEN 4 THEN 'Shortlisted By L2'  WHEN 5 THEN 'Rejected By L2' ELSE '' END) as JobAplliedStatus,
                            (CASE COUNT(DISTINCT(JP.level)) WHEN 1 THEN 'L1' WHEN 2 THEN 'L2' ELSE '' END) AS JobLevel,
                            J.createdBy,
                            UJ.createdAt                                     
                        FROM 
                            fj_userJob      UJ
                        JOIN 
                            fj_jobPanel     JP,
                            fj_users        U,
                            fj_jobs         J
                        WHERE
                            UJ.jobId=JP.jobId   AND
                            UJ.userId=U.id      AND
                            UJ.jobId=J.id                                   
                        GROUP BY UJ.id ORDER BY UJ.jobId 
                        ) FT
                    WHERE
                        ((FT.JobAplliedStatus='Pending') OR
                        (FT.JobAplliedStatus='Shortlisted By L1' AND JobLevel='L2')) AND
                        FT.JobId IN (SELECT jobId FROM fj_jobPanel JP WHERE userId='$userId')
                        ";
            $query = $this->db->query($sql);
            $result = $query->row_array();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Description : Use to get all shortlisted application on assiged active jobs to corporate L1 or L2 user
     * Author : Synergy
     * @param int $userId
     * @return array of data
     */
    function assignedShortlistedApplication($userId) {
        try {
            $sql = "
                    SELECT COUNT(id) AS assignedShortlistedApplication
                    FROM
                        (
                        SELECT 
                            J.id        AS JobId,
                            UJ.shorlistedByL1User,
                            UJ.shorlistedByL2User,
                            UJ.id,
                            U.fullname  AS AppUserName,
                            U.email     AS AppUserEmail,
                            U.mobile    AS AppUserMobile,
                            J.fjCode    AS JobCode,
                            J.title     AS JobTitle,
                            (CASE UJ.status WHEN 1 THEN 'Pending' WHEN 2 THEN 'Shortlisted By L1' WHEN 3 THEN 'Rejected By L1' WHEN 4 THEN 'Shortlisted By L2'  WHEN 5 THEN 'Rejected By L2' ELSE '' END) as JobAplliedStatus,
                            (CASE COUNT(DISTINCT(JP.level)) WHEN 1 THEN 'L1' WHEN 2 THEN 'L2' ELSE '' END) AS JobLevel,
                            J.createdBy,
                            UJ.createdAt                            
                        FROM 
                            fj_userJob      UJ
                        JOIN 
                            fj_jobPanel     JP,
                            fj_users        U,
                            fj_jobs         J
                        WHERE
                            UJ.jobId=JP.jobId   AND
                            UJ.userId=U.id      AND
                            UJ.jobId=J.id       
                        GROUP BY UJ.id ORDER BY UJ.jobId 
                        ) FT
                    WHERE
                        ((FT.JobAplliedStatus='Shortlisted By L1' AND JobLevel='L1') OR
                        (FT.JobAplliedStatus='Shortlisted By L2' AND JobLevel='L2')) AND
                        FT.JobId IN (SELECT jobId FROM fj_jobPanel JP WHERE userId='$userId')
                        ";
            $query = $this->db->query($sql);
            $result = $query->row_array();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Description : Use to get all rejected application on assiged active jobs to corporate L1 or L2 user
     * Author : Synergy
     * @param int $userId
     * @return array of data
     */
    function assignedRejectedApplication($userId) {
        try {
            $sql = "
                    SELECT COUNT(id) AS assignedRejectedApplication
                    FROM
                        (
                        SELECT 
                            J.id        AS JobId,
                            UJ.shorlistedByL1User,
                            UJ.shorlistedByL2User,
                            UJ.id,
                            U.fullname  AS AppUserName,
                            U.email     AS AppUserEmail,
                            U.mobile    AS AppUserMobile,
                            J.fjCode    AS JobCode,
                            J.title     AS JobTitle,
                            (CASE UJ.status WHEN 1 THEN 'Pending' WHEN 2 THEN 'Shortlisted By L1' WHEN 3 THEN 'Rejected By L1' WHEN 4 THEN 'Shortlisted By L2'  WHEN 5 THEN 'Rejected By L2' ELSE '' END) as JobAplliedStatus,
                            (CASE COUNT(DISTINCT(JP.level)) WHEN 1 THEN 'L1' WHEN 2 THEN 'L2' ELSE '' END) AS JobLevel,
                            J.createdBy,
                            UJ.createdAt                            
                        FROM 
                            fj_userJob      UJ
                        JOIN 
                            fj_jobPanel     JP,
                            fj_users        U,
                            fj_jobs         J
                        WHERE
                            UJ.jobId=JP.jobId   AND
                            UJ.userId=U.id      AND
                            UJ.jobId=J.id       
                        GROUP BY UJ.id ORDER BY UJ.jobId 
                        ) FT
                    WHERE
                        ((FT.JobAplliedStatus='Rejected By L1') OR
                        (FT.JobAplliedStatus='Rejected By L2')) AND
                        FT.JobId IN (SELECT jobId FROM fj_jobPanel JP WHERE userId='$userId')
                        ";
            $query = $this->db->query($sql);
            $result = $query->row_array();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Description : Use to get all assiged active jobs assigned by L1
     * Author : Synergy
     * @param int $userId
     * @return array of data
     */
    function assignedAsL1($userId) {
        try {
            $sql = "
                    SELECT 
                        COUNT(JP.id) AS assignedAsL1
                    FROM 
                        fj_jobPanel JP
                    WHERE
                        JP.userId='$userId' AND
                        JP.level='1'
                    ";
            $query = $this->db->query($sql);
            $result = $query->row_array();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Description : Use to get all assiged active jobs assiged by L2 user
     * Author : Synergy
     * @param int $userId
     * @return array of data
     */
    function assignedAsL2($userId) {
        try {
            $sql = "
                    SELECT 
                        COUNT(JP.id) AS assignedAsL2
                    FROM 
                        fj_jobPanel JP
                    WHERE
                        JP.userId='$userId' AND
                        JP.level='2'
                    ";
            $query = $this->db->query($sql);
            $result = $query->row_array();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Description : Use to get job on which heighest application is pending of a specific corporate user
     * Author : Synergy
     * @param int $userId
     * @return array of data
     */
    function assignedHighestPendingJob($userId) {
        try {
            $sql = "
                    SELECT
                        COUNT(FinaleTable.id) AS TotalPendingJobs,
                        GROUP_CONCAT(FinaleTable.jobCode) AS PendingJobIds,
                        FinaleTable.HighestPendingUsers, 
                        GROUP_CONCAT(FinaleTable.JobId) AS jobIds
                    FROM
                        (
                        SELECT
                            count(FinalTable.jobCode) AS HighestPendingUsers ,
                            FinalTable.*
                        FROM
                            (
                            SELECT 
                                FT.*
                            FROM
                                (
                                SELECT 
                                    J.id        AS JobId,
                                    UJ.shorlistedByL1User,
                                    UJ.shorlistedByL2User,
                                    UJ.id,
                                    U.fullname  AS AppUserName,
                                    U.email     AS AppUserEmail,
                                    U.mobile    AS AppUserMobile,
                                    J.fjCode    AS JobCode,
                                    J.title     AS JobTitle,
                                    (CASE UJ.status WHEN 1 THEN 'Pending' WHEN 2 THEN 'Shortlisted By L1' WHEN 3 THEN 'Rejected By L1' WHEN 4 THEN 'Shortlisted By L2'  WHEN 5 THEN 'Rejected By L2' ELSE '' END) as JobAplliedStatus,
                                    (CASE COUNT(DISTINCT(JP.level)) WHEN 1 THEN 'L1' WHEN 2 THEN 'L2' ELSE '' END) AS JobLevel,
                                    J.createdBy,
                                    UJ.createdAt                            
                                FROM 
                                    fj_userJob      UJ
                                JOIN 
                                    fj_jobPanel     JP,
                                    fj_users            U,
                                    fj_jobs         J
                                WHERE
                                    UJ.jobId=JP.jobId   AND
                                    UJ.userId=U.id      AND
                                    UJ.jobId=J.id       
                                GROUP BY UJ.id ORDER BY UJ.jobId 
                                ) FT
                            WHERE
                                JobId IN (SELECT jobId FROM fj_jobPanel JP WHERE userId='$userId')  AND
                                ((FT.JobAplliedStatus='Pending') OR
                                (FT.JobAplliedStatus='Shortlisted By L1' AND JobLevel='L2'))
                            ) FinalTable
                        GROUP BY FinalTable.jobCode
                        ORDER BY HighestPendingUsers DESC
                        ) FinaleTable
                    GROUP BY FinaleTable.HighestPendingUsers
                    ORDER BY FinaleTable.HighestPendingUsers DESC
                    LIMIT 0,1
                    ";
            $query = $this->db->query($sql);
            $result = $query->row_array();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Description : Use to get job on which least application found on specific corporate user 
     * Author : Synergy
     * @param int $userId
     * @return array of data
     */
    function assignedLeastApplicationJob($userId) {
        try {       
            $sql = "
                    SELECT 
                        GROUP_CONCAT(J.fjCode) AS LeastJobIds, GROUP_CONCAT(J.id) AS jobIds
                    FROM 
                        fj_jobPanel JP
                    JOIN
                        fj_jobs     J
                    WHERE 
                        J.id=JP.jobId           AND
                        JP.userId = '$userId' 	AND 
                        JP.jobId NOT IN (
                                        SELECT 
                                            DISTINCT(jobId)
                                        FROM 
                                            fj_userJob
                                        WHERE
                                            jobId IN (SELECT jobId FROM fj_jobPanel JP WHERE userId='$userId')
                                        )
                    ";
            $query = $this->db->query($sql);
            $result = $query->row_array();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Description : Use to get longest pending open job on specific corporate user 
     * Author : Synergy
     * @param int $userId
     * @return array of data
     */
    function assignedLongestOpenJob($userId) {
        try {
            $sql = "
                    SELECT 
                        id, fjCode, openTill, createdAt, GROUP_CONCAT(fjCode) AS LongestJobIds, GROUP_CONCAT(id) AS jobIds, COUNT(id) AS LongestJobCount
                    FROM
                        fj_jobs
                    WHERE
                        id IN (SELECT jobId FROM fj_jobPanel JP WHERE userId='$userId')  AND
                        DATE(openTill)>=CURDATE()
                    GROUP BY
                        createdAt, openTill
                    ORDER BY
                        createdAt ASC, openTill DESC
                    LIMIT 0,1
                    ";
            $query = $this->db->query($sql);
            $result = $query->row_array();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
}
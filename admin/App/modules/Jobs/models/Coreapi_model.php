<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : coreapi Model
 * Description : Hold all the core functions used for jobs
 * @author Synergy
 * @createddate : Nov 21, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 20, 2017
 */

/*
#############################################################
    -------------------- Api Listing --------------------
    =========CORE METHODS USED IN ALL API MODELS=========
        01. cleanString
        02. getJwtToken
        03. getJwtValue
        04. codeMessage
        05. queryResultArray
        06. queryRowArray
        07. customNumberFormat
#############################################################
*/
class coreapi_model extends CI_Model {
    
    /**
     * Declaring static variables
     */
    public static $returnArray;
    public static $data;

    /**
     * Responsable for inheriting parent constructor
     * Responsable for auto loading database
     * Responsable for auto load the email and session library
     * Responsable for initializing global variable
     * @return void
     */
    function __construct() {
        parent::__construct();        
        $this->load->database();
        $this->load->library('email');
        $this->load->library('session');
        coreapi_model::$returnArray  = array();
        coreapi_model::$data         = array();
    }

    /**
     * Description : Filter User Input Data
     * Author : Synergy
     * @param string $string
     * @return htmlEntites
     */
    function cleanString($string) {
        $string         = trim($string);
        $detagged       = strip_tags($string);
        $stripped       = stripslashes($detagged);
        $htmlEntites    = htmlentities($stripped, ENT_QUOTES, 'UTF-8');
        return $htmlEntites;
    }
    
    /**
     * Description : Convert User ID to JWT Token
     * Author : Synergy
     * @param string $string
     * @return string
     */
    function getJwtToken($userId) {
        $token          = array();
        $token['id']    = $userId;
        $jwtKey         = $this->config->item('jwtKey');
        $secretKey      = base64_decode($jwtKey);
        $signatureAlgo  = $this->config->item('signatureAlgorithm');
        return JWT::encode($token, $secretKey, $signatureAlgo);
    }
    
    /**
     * Description : Convert User ID to JWT Token
     * Author : Synergy
     * @param string $string
     * @return string jwt value
     */
    function getJwtValue($userToken) {
        $jwtKey         = $this->config->item('jwtKey');
        $secretKey      = base64_decode($jwtKey);
        $signatureAlgo  = $this->config->item('signatureAlgorithm');
        try {
            $token          = JWT::decode($userToken, $secretKey, $signatureAlgo);
        } catch (Exception $e) {
            coreapi_model::codeMessage('500', $e->getMessage());
        }
        return $token->id;
    }
    
    /**
     * Description : Generate Response Message with HTTP Code
     * Author : Synergy
     * @param int $code, string $message
     * @return array
     */
    function codeMessage($code, $message){
        coreapi_model::$returnArray['code']    = $code;
        coreapi_model::$returnArray['message'] = $message;
        return coreapi_model::$returnArray;
    }
    
    /**
     * Description : Fetch All Record From DB
     * Author : Synergy
     * @param string $query
     * @return array with data
     */
    function queryResultArray($query){
        try {
            $query  = $this->db->query($query);
            try{
                $result = $query->result_array();
                return $result;
            } catch (Exception $e) {
                    coreapi_model::codeMessage('500', $e->getMessage());
            }
        } catch (Exception $e) {
            coreapi_model::codeMessage('500', $e->getMessage());
        }
    }
    
    /**
     * Description : Fetch A Single Row From DB
     * Author : Synergy
     * @param string $query
     * @return array with data
     */
    function queryRowArray($query){
        try {
            $query  = $this->db->query($query);
            try{
                $result = $query->row_array();
                return $result;
            } catch (Exception $e) {
                    coreapi_model::codeMessage('500', $e->getMessage());
            }
        } catch (Exception $e) {
            coreapi_model::codeMessage('500', $e->getMessage());
        }
    }
    
    /**
     * Description : Gets the length of a digit
     * Author : Synergy
     * @param string $number
     * @return int
     */
    static function count_digit($number) {
        return strlen($number);
    }

    /**
     * Description : Get tens in a number
     * Author : Synergy
     * @param string $number_of_digits
     * @return int
     */
    static function divider($number_of_digits) {
        $tens="1";
        while(($number_of_digits-1)>0) {
            $tens.="0";
            $number_of_digits--;
        }
        return $tens;
    }

    /**
     * Description : convert number into lacs, thousands, crore etc
     * Author : Synergy
     * @param string $num
     * @return string
     */
    function customNumberFormat($num) {
        $ext    = "";//thousand,lac, crore
        $number_of_digits = self::count_digit($num); //this is call :)
        if($number_of_digits>3){
            if($number_of_digits%2!=0)
                $divider=self::divider($number_of_digits-1);
            else
                $divider=self::divider($number_of_digits);
        }
        else {
            $divider=1;
        }
        $fraction   = $num/$divider;
        $fraction   = number_format($fraction,2);
        if($number_of_digits==4 ||$number_of_digits==5)
            $ext="k";
        if($number_of_digits==6 ||$number_of_digits==7)
            $ext="Lakhs";
        if($number_of_digits==8 ||$number_of_digits==9)
            $ext="Cr";
        return $fraction." ".$ext;
    }
    
    /**
     * Description : Check User Exists Or Not
     * Author : Synergy
     * @param int $id
     * @return array of data
     */
    function userExists($id) {
        $userRow    = coreapi_model::queryRowArray("SELECT id FROM fj_users WHERE id='$id' AND status='1' LIMIT 0,1");
        return $userRow;
    }
    
    /**
     * Description : Check Job Exists Or Not
     * Author : Synergy
     * @param int $fjJobId
     * @return array of data
     */
    function jobExists($fjJobId) {
        $jobRow    = coreapi_model::queryRowArray("SELECT id, createdBy, interview, assessment, jd FROM fj_jobs WHERE fjCode='$fjJobId' AND status='1' LIMIT 0,1");
        return $jobRow;
    }

    /**
     * Description : Use to limit the text
     * Author : Synergy
     * @param string $text, int $limit
     * @return string
     */
    function limit_text($text, $limit) {
        if (str_word_count($text, 0) > $limit) {
            $words = str_word_count($text, 2);
            $pos = array_keys($words);
            $text = substr($text, 0, $pos[$limit]) . '...';
        }
        return $text;
    }
    
    /**
     * Description : Use to check keys exists into an array or not
     * Author : Synergy
     * @param array $array, array $keys
     * @return string
     */
    function array_keys_exist( array $array, $keys ) {
        $count = 0;
        if ( ! is_array( $keys ) ) {
            $keys = func_get_args();
            array_shift( $keys );
        }
        foreach ( $keys as $key ) {
            if ( array_key_exists( $key, $array ) ) {
                $count ++;
            }
        }
        return count( $keys ) === $count;
    }
}

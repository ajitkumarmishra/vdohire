<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Analytics Model
 * Description : Handle all the CRUD operation for Jobs
 * @author Synergy
 * @createddate : Nov 05, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 20, 2017
 */
class Job_model extends CI_Model {

    /**
     * Initialize variables
     */
    var $table = "jobs";
    static $interview           = 'interview';
    static $assessment          = 'assessment';
    static $interviewTable      = 'fj_interviews';
    static $assessmentTable     = 'fj_assessments';
    static $interviewFieldId    = 'interviewId';
    static $assessmentFieldId   = 'assessmentId';
    static $mergeTextFile       = 'F.txt';
    
    /**
     * Responsable for inherit  the parent constructor
     * Responsable for auto load the the session and pagination library
     * @return void
     */
    function __construct() {
        parent::__construct();
        $this->load->library("session");
        $this->load->library('pagination');
        $this->load->helper('s3');
        $this->load->model('coreapi_model', 'coreModel');
    }

    /**
     * Description : Use to create new job
     * Author : Synergy
     * @params array $data
     * @return boolean
     */
    function addJob($data) {
        $resp = $this->db->insert('fj_jobs', $data);
        $jobId = $this->db->insert_id();
        if ($resp) {
            return $jobId;
        } else {
            return false;
        }
    }

    /**
     * Description : Use to Uploads a file to directory
     * Author : Synergy
     * @param array $files
     * @param string $uploads_dir
     * @param string $imagename
     * @return boolean
     */
    public function fileUpload($files, $uploads_dir, $imagename) {
        try {
            $tmp_name = $files["jd"]["tmp_name"];
            if (move_uploaded_file($tmp_name, $uploads_dir . "/" . $imagename)) {
                return $imagename;
            } else {
            }
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    /**
     * Description : Use to Updates a specific job.
     * Author : Synergy
     * @param array $data.
     * @param int $id.
     * @return boolean or int or string.
     */
    function updateJob($data, $id) {
        try {
            $this->db->where('id', $id);
            $resp = $this->db->update('fj_jobs', $data);
            if (!$resp) {
                throw new Exception('Unable to update job data.');
                return false;
            }
            return $id;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Description : Use to gets a specific job detail.
     * Author : Synergy
     * @param int $id.
     * @return array the jobs data or string error message or boolean.
     */
    function getJob($id) {
        try {
            $this->db->select('*');
            $this->db->from('fj_jobs');
            $this->db->where('id', $id);
            $query = $this->db->get();
            $result = $query->row_array();
            if (count($result) == 0) {
                throw new Exception('Unable to fetch job data.');
                return false;
            }
            return $result;
        } catch (Exception $e) {
            //echo $e->getMessage();
            //die();
        }
    }
    
    /**
     * Description : Use to get all application on a specific job
     * Author : Synergy
     * @param int $id ,int $limit, int $offset
     * @return array the job application data.
     */
    function getJobApplication($id, $limit = NULL, $offset = NULL) {
        try {
            $sql = "
                    SELECT
                        (SELECT audition_file FROM fj_userAudition WHERE userId=UJ.userId AND status=1) AS audition ,
                        U.*,
                        J.id AS jobId,
                        J.fjCode    AS JobCode,
                        U.fullname  AS AppUserName,
                        UJ.mergedVideo,
                        
                        UJ.id AS userJobId,
                        (CASE UJ.status WHEN 1 THEN 'Pending' WHEN 2 THEN 'Shortlisted By L1' WHEN 3 THEN 'Rejected By L1' WHEN 4 THEN 'Shortlisted By L2'  WHEN 5 THEN 'Rejected By L2' ELSE '' END) as JobAplliedStatus,
                        (CASE COUNT(DISTINCT(JP.level)) WHEN 1 THEN 'L1' WHEN 2 THEN 'L2' ELSE '' END) AS JobLevel,
                        J.createdBy AS jobCreatedBy,
                        UJ.createdAt AS userApplicationCreatedAt                              
                    FROM 
                        fj_userJob      UJ
                    LEFT JOIN 
                        fj_jobPanel     JP
                    ON
                        JP.jobId = UJ.jobId
                    LEFT JOIN 
                        fj_users        U
                    ON
                        U.id = UJ.userId
                    LEFT JOIN 
                        fj_jobs         J
                    ON
                        J.id = UJ.jobId
                    WHERE
                        J.id='$id'
                    GROUP BY 
                        UJ.id 
                    ORDER BY 
                        UJ.id DESC";


            $query          = $this->db->query($sql);            
            $total_records  = $query->num_rows();
            
            $config["base_url"]         = base_url() . $this->uri->segment(1). "/" . $this->uri->segment(2) . "/" . $this->uri->segment(3). "/";            
            $config["total_rows"]       = $total_records;
            $config["per_page"]         = 50;
            $page                       = $this->uri->segment(4, 0);
            $config['uri_segment']      = 4;
            $config['full_tag_open']    = '<ul class="pagination">';
            $config['full_tag_close']   = '</ul>';
            $config['first_link']       = false;
            $config['last_link']        = false;
            $config['first_tag_open']   = '<li>';
            $config['first_tag_close']  = '</li>';
            $config['prev_link']        = '&laquo';
            $config['prev_tag_open']    = '<li class="prev">';
            $config['prev_tag_close']   = '</li>';
            $config['next_link']        = '&raquo';
            $config['next_tag_open']    = '<li>';
            $config['next_tag_close']   = '</li>';
            $config['last_tag_open']    = '<li>';
            $config['last_tag_close']   = '</li>';
            $config['cur_tag_open']     = '<li class="active"><a href="#">';
            $config['cur_tag_close']    = '</a></li>';
            $config['num_tag_open']     = '<li>';
            $config['num_tag_close']    = '</li>';
            $this->pagination->initialize($config);

            $start_limit = ($page) * 1;
            if ($start_limit > 1) {
                $start_limit = $start_limit;
            } else {
                $start_limit = 0;
            }
                      
            $sql .= " LIMIT " .$config["per_page"]. " OFFSET " .$start_limit."";            

            $queryResult = $this->db->query($sql);
            $result = $queryResult->result();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Description : Use to get assessment details of a user on a specific job
     * Author : Synergy
     * @param int $userId, int $jobId
     * @return array the user assessment data.
     */
    function userAssessmentDetail($userId, $jobId) {
        try {
            $sql = "
                    SELECT
                        FINAL.*,
                        (SELECT MC.option FROM fj_multipleChoiceAnswer MC WHERE MC.id=FINAL.userAnswer) AS userOption
                    FROM
                        (
                        SELECT
                            RES.*,
                            (SELECT UA.answer FROM fj_userAnswers UA WHERE UA.userId='$userId' AND UA.questionId=RES.questionId AND UA.jobId='$jobId') AS userAnswer
                        FROM
                            (
                            SELECT
                                J.assessment        AS assessmentId,
                                Q.id                AS questionId,
                                Q.title,
                                Q.type              AS questionType,
                                Q.IsMultipleChoice  AS IsQuestionMultiple,
                                Q.answer            AS questionAnswerId,
                                MCA.option
                            FROM
                                fj_jobs                 J
                            JOIN
                                fj_assessmentQuestions  AQ,
                                fj_question             Q,
                                fj_multipleChoiceAnswer MCA
                            WHERE
                                J.assessment=AQ.assessmentId    AND
                                AQ.status='1'                   AND
                                AQ.questionId=Q.id              AND
                                Q.answer=MCA.id                 AND
                                J.id='$jobId'
                            GROUP BY
                                Q.id
                            )   RES
                        )   FINAL
                    
                    ";
            $query  = $this->db->query($sql);
            $result = $query->result();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    
    protected function allJobIdQuery($locationIDs){
        $jobData   = $this->coreModel->queryRowArray("
                                                    SELECT GROUP_CONCAT( DISTINCT jobId ) AS jobID
                                                    FROM fj_jobLocations 
                                                    WHERE location IN ($locationIDs)
                                                ");
        return $jobData;
    }

    /**
     * Description : Use to get list of jobs created by specific corporate 
     * Author : Synergy
     * @param int $id.
     * @return array of jobs data.
     */
    function listJobs($limit, $offset, $serachColumn = NULL, $searchText = NULL, $createdBy, $userRole, $sortJobsResult, $sortingOrder, $jobType = NULL) {
        try {
            $currentDate    = date('Y-m-d h:i:s');            
            $userRoleValue  = $userRole;

            if(($userRoleValue != 1) && !empty($serachColumn)) {
                $locationIDs = implode(',', $serachColumn);
                $jobIdData   = $this->allJobIdQuery($locationIDs);
                $jobsIDs    = $jobIdData['jobID'];
                $inWhere = " AND J.id IN (".$jobsIDs.")";
            } else {
                $inWhere = " AND 1=1";
            }

            if(!empty($jobType) && ($jobType == 'pastJobs')) {
                $jobTypeFilter = " J.status IN ('6')";
            } else {
                $jobTypeFilter = " J.status IN ('1','2','5')";
            }

            $sql = " SELECT
                        (CASE WHEN U.role<>2 THEN (SELECT company FROM fj_users WHERE id=U.createdBy) ELSE U.company END) AS company,
                        JOBS.*
                    FROM
                        (
                        SELECT                            
                            (
                            SELECT      GROUP_CONCAT(CONCAT('''', C.id, '''' )) 
                            FROM        fj_courses C 
                            LEFT JOIN   fj_jobQualifications JQ
                            ON          C.id = JQ.qualificationId
                            WHERE       J.id=JQ.jobId
                            ) 
                            AS qualificationId,
                            
                            (
                            SELECT      GROUP_CONCAT(C.name) 
                            FROM        fj_courses C 
                            LEFT JOIN   fj_jobQualifications JQ
                            ON          C.id = JQ.qualificationId
                            WHERE       J.id=JQ.jobId
                            ) 
                            AS qualificationName,
                            
                            (
                            SELECT      GROUP_CONCAT(CONCAT('''', L.id, '''' ))
                            FROM        fj_cityState L 
                            LEFT JOIN   fj_jobLocations JL
                            ON          L.id = JL.location
                            WHERE       J.id=JL.jobId
                            ) 
                            AS locationId,
                            
                            (
                            SELECT      GROUP_CONCAT(L.city) 
                            FROM        fj_cityState L 
                            LEFT JOIN   fj_jobLocations JL
                            ON          L.id = JL.location
                            WHERE       J.id=JL.jobId
                            ) 
                            AS locationName,
                            
                            J.*
                        FROM
                            fj_jobs                 J
                        WHERE
                            $jobTypeFilter  $inWhere
                        GROUP BY
                            J.id
                        ) JOBS
                    JOIN
                        fj_users U
                    WHERE
                        U.id=JOBS.createdBy ";
            
            if( $createdBy != "" && $createdBy != NULL && $userRole!='1' ){
                $sql .= " AND JOBS.createdBy IN (" . $createdBy . ")";
            }
            
            if($userRoleValue == 1) {
                if(trim($serachColumn) == 'city' && $searchText!='') {
                    $sql .=" AND JOBS.locationName LIKE '%" . $searchText . "%'";  
                }
                else if(trim($serachColumn) == 'title' && $searchText!='') {
                    $sql .=" AND JOBS.title LIKE '%" . $searchText . "%'";  
                }
                else if (trim($serachColumn) == 'experience' && $searchText!='') {
                    $sql .=" AND ( JOBS.expFrom <= $searchText AND $searchText <= JOBS.expTo  )";  
                }
                else {
                    $sql .=" AND 1=1";  
                }
            } else {
                if($searchText) {
                    $sql .= " AND (JOBS.title LIKE '%".$searchText."%' OR JOBS.description LIKE '%".$searchText."%') ";
                } else {
                    $sql .=" AND (1=1) ";
                }
            }
            
            if($sortJobsResult) {
                if($sortJobsResult == 'dateSort') {
                    $sql .= " ORDER BY JOBS.openTill $sortingOrder";
                } else if($sortJobsResult == 'jobSort') {
                    $sql .= " ORDER BY JOBS.title $sortingOrder";
                } else if($sortJobsResult == 'locationSort') {
                    $sql .= " ORDER BY locationName $sortingOrder";
                } else {
                    $sql .= " ORDER BY JOBS.id $sortingOrder";
                }
            } else {
                $sql .= " ORDER BY JOBS.id DESC";
            }
            //echo $sql;exit;
            
            $query          = $this->db->query($sql);            
            $total_records  = $query->num_rows();
            
            $config["base_url"]         = base_url() . $this->uri->segment(1). "/" . $this->uri->segment(2) . "/";
            $config["total_rows"]       = $total_records;
            $config["per_page"]         = 10;
            $page                       = $this->uri->segment(3, 0);
            $config['num_links'] = 9;
            $config['uri_segment']      = 3;
            $config['full_tag_open']    = '<ul class="pagination">';
            $config['full_tag_close']   = '</ul>';
            $config['first_link']       = false;
            $config['last_link']        = false;
            $config['first_tag_open']   = '<li>';
            $config['first_tag_close']  = '</li>';
            $config['prev_link']        = '&laquo';
            $config['prev_tag_open']    = '<li class="prev">';
            $config['prev_tag_close']   = '</li>';
            $config['next_link']        = '&raquo';
            $config['next_tag_open']    = '<li>';
            $config['next_tag_close']   = '</li>';
            $config['last_tag_open']    = '<li>';
            $config['last_tag_close']   = '</li>';
            $config['cur_tag_open']     = '<li class="active"><a href="#">';
            $config['cur_tag_close']    = '</a></li>';
            $config['num_tag_open']     = '<li>';
            $config['num_tag_close']    = '</li>';
            $this->pagination->initialize($config);

            $start_limit = ($page) * 1;
            if ($start_limit > 1) {
                $start_limit = $start_limit;
            } else {
                $start_limit = 0;
            }
                      
            $sql .= " LIMIT " .$config["per_page"]. " OFFSET " .$start_limit."";            
            $queryResult = $this->db->query($sql);
            $result = $queryResult->result();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
	
	function listApplicantsSearch($text, $usersJobListL1L2){
		try {
			
            $query =  $this->db->select('U.*,U.id as CurrentuserId, J.title, J.id as CurrentjobId')
							->from('fj_users as U')
							 ->join('fj_userJob as UJ', 'U.id = UJ.userId', 'left')
							 ->join('fj_jobs as J', 'J.id = UJ.jobId', 'left')
							->where('fullname LIKE "%'.$text.'%"')
							//->where('J.openTill >= CURDATE()')
							//->where('J.status in("1","2","5")')
							->where('UJ.jobId in ('.$usersJobListL1L2.') ');

            $selectqry = $query->get();
            $result = $selectqry->result_array();
			//echo $this->db->last_query();exit;
			
            if (count($result) == 0) {
                throw new Exception('Unable to fetch job data.');
                return false;
            }
            return $result;
        } catch (Exception $e) {
            //echo $e->getMessage();
            //die();
        }
		
		
	}
	function listJobsSearch($searchText,$usersJobListL1L2) {
        try {
            
            $query =  $this->db->select('*')
							->from('fj_jobs as J')
							->where('title LIKE "%'.$searchText.'%"')
							//->where('status in ("1","2")')
							//->where('J.openTill >= CURDATE()')
							->where("J.id in (".$usersJobListL1L2.")");
							
            $selectqry = $query->get();
            $result = $selectqry->result_array();
			
			
            return $result;
        } catch (Exception $e) {
            //echo $e->getMessage();
            //die();
        }
    }

    /**
     * Description : Use to get all jobs by email id
     * Author : Synergy
     * @param int $email.
     * @return array of jobs data or boolean.
     */
    function getHashByEmail($email) {
        $this->db->where('email', $email);
        $query = $this->db->get('fj_jobs');
        if ($query->num_rows() === 1) {
            return $query->row();
        } else {
            return false;
        }
    }

    /**
     * Description : Use to add locations for a specific job
     * Author : Synergy
     * @param string $locations, int $jobId.
     * @return int or string
     */
    function addLocation($locations, $jobId) {
        try {
            foreach ($locations as $item) {
                $data['location'] = $item;
                $data['jobId'] = $jobId;
                $data['createdAt'] = date('Y-m-d h:i:s');
                $data['updatedAt'] = date('Y-m-d h:i:s');
                $this->db->insert('fj_jobLocations', $data);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Description : Use to delete locations for a specific job
     * Author : Synergy
     * @param int $jobId.
     * @return boolean or string
     */
    function deleteLocationsByJob($jobId) {
        try {
            $this->db->where('jobId', $jobId);
            $this->db->delete('fj_jobLocations');
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Description : Use to delete qualicications of specific job
     * Author : Synergy
     * @param int $jobId.
     * @return boolean or string
     */
    function deleteQualicationsByJob($jobId) {
        try {
            $this->db->where('jobId', $jobId);
            $this->db->delete('fj_jobQualifications');
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Description : Use to delete job panel i.e assigned jobs of specific job
     * Author : Synergy
     * @param int $jobId.
     * @return boolean or string
     */
    function deletePanelByJob($jobId) {
        try {
            $this->db->where('jobId', $jobId);
            $this->db->delete('fj_jobPanel');
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Description : Use to delete job panel i.e assigned jobs of specific job
     * Author : Synergy
     * @param array $panel, int $jobId, int $level
     * @return int or string
     */
    function addJobPanel($panel, $jobId, $level) {
        try {
            foreach ($panel as $item) {
                $data['userId'] = $item;
                $data['jobId'] = $jobId;
                $data['level'] = $level;
                $data['createdAt'] = date('Y-m-d h:i:s');
                $data['updatedAt'] = date('Y-m-d h:i:s');
                $this->db->insert('fj_jobPanel', $data);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Description : Use to replicate an existing job
     * Author : Synergy
     * @param int $id
     * @return int
     */
    function replicateJob($id) {
        $sql = "INSERT INTO fj_jobs(jobId, fjCode, title, description, noOfVacancies, ageFrom, ageTo, salaryFrom,"
                . " salaryTo, noticePeriod, qualification, experience, openTill, location, jd, interview, assessment,"
                . " preInterviewText, postInterviewText, preAssessmentText, postAssessmentText, industryId, deptId, "
                . "status, posted, createdAt, updatedAt, createdBy, updatedBy) "
                . "SELECT jobId, fjCode, title, description, noOfVacancies, ageFrom, ageTo, salaryFrom,"
                . " salaryTo, noticePeriod, qualification, experience, openTill, location, jd, interview, assessment,"
                . " preInterviewText, postInterviewText, preAssessmentText, postAssessmentText, industryId, deptId, "
                . "status, posted, createdAt, updatedAt, createdBy, updatedBy FROM fj_jobs where id ='".$id."'";
        $this->db->query($sql);
    }

    /**
     * Description : Use to add qualifications for specific job 
     * Author : Synergy
     * @param array $qualifications, int $jobId
     * @return int or string
     */
    function addJobQualification($qualifications, $jobId) {
        try {
            foreach ($qualifications as $item) {
                $data['qualificationId'] = $item;
                $data['jobId'] = $jobId;
                $data['createdAt'] = date('Y-m-d h:i:s');
                $this->db->insert('fj_jobQualifications', $data);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        } 
    }

    /**
     * Description : Use to add video name in a specific path 
     * Author : Synergy
     * @param string $fileNameCreate, string $tempVideoName
     * @return void
     */
    protected function addVideoName($fileNameCreate, $tempVideoName) {
        $qus    = "\r\nfile ".$tempVideoName;
        $fh     = fopen($fileNameCreate, 'a') or die("can't open file");
        fwrite($fh, $qus);
        fclose($fh);
    }
    
    /**
     * Description : Use to create temporary image name 
     * Author : Synergy
     * @param string $questionText, string $tempQusImageName
     * @return void
     */
    protected function createImage($questionText, $tempQusImageName) {
        // Create Image
        header('Content-type: image/png');
        $text           = "Question :\n\n";
        $text          .= $questionText;
        $text_length    = 45;
        $wrap_text      = wordwrap($text, $text_length, "<br />", true);
        $fontsize       = 14;
        $fontfile       = 'OpenSans-Regular.ttf';        
        $line_text      = str_replace('<br />', "\n", $wrap_text);        
        header ("Content-type: image/png");
        $string         = $text;
        $im             = $this->make_wrapped_txt($string, 0, 4, 4, 200);
        imagepng($im, $tempQusImageName);
        imagedestroy($im);
    }
    
    /**
     * Description : Use to make text wrapped
     * Author : Synergy
     * @param string $txt, int $color, int $space, int $font, int $w
     * @return string
     */
    function make_wrapped_txt($txt, $color=000000, $space=4, $font=4, $w=300) {
        if (strlen($color) != 6) $color = 000000;
        $int = hexdec($color);
        $h = imagefontheight($font);
        $fw = imagefontwidth($font);
        $txt = explode("\n", wordwrap($txt, ($w / $fw), "\n"));
        $lines = count($txt);
        $im = imagecreate($w, (($h * $lines) + ($lines * $space)));
        $bg = imagecolorallocate($im, 255, 255, 255);
        $color = imagecolorallocate($im, 0xFF & ($int >> 0x10), 0xFF & ($int >> 0x8), 0xFF & $int);
        $y = 0;
        foreach ($txt as $text) {
          $x = (($w - ($fw * strlen($text))) / 2); 
          imagestring($im, $font, $x, $y, $text, $color);
          $y += ($h + $space);
        }
        return $im;
    }
    
    /**
     * Description : Use to check if a user has answer on a partcular set and job
     * Author : Synergy
     * @param int $jobId, int $userId, string $setType
     * @return array or data
     */
    protected function setAnswerExists($jobId, $userId, $setType) {
        $tableName      = ($setType==self::$interview?self::$interviewTable:self::$assessmentTable);
        $fieldId        = ($setType==self::$interview?self::$interviewFieldId:self::$assessmentFieldId);

        $SETqusResult   = $this->coreModel->queryResultArray(" SELECT 
                                                                A.*,
                                                                J.title,
                                                                Q.name,
                                                                Q.type,
                                                                QB.title,
                                                                QB.file
                                                            FROM 
                                                                fj_userAnswers  A
                                                            LEFT JOIN
                                                                fj_jobs         J
                                                            ON
                                                                J.id = A.jobId
                                                            LEFT JOIN
                                                                ".$tableName."  Q
                                                            ON
                                                                Q.id = A.".$fieldId."
                                                            LEFT JOIN
                                                                fj_question     QB
                                                            ON
                                                                QB.id = A.questionId
                                                            WHERE
                                                                A.jobId=".$jobId."     AND
                                                                A.userId=".$userId."
                                                           ");
        return $SETqusResult;
    }

    /**
     * Description : Use to generate interview video as file type ffmpeg
     * Author : Synergy
     * @param int $jobId, string $jobCode
     * @return array or data
     */
    public function generateVideo($userId, $jobCode) {
		
        $id         = $this->coreModel->cleanString($userId);
        $fjJobId    = $this->coreModel->cleanString($jobCode);
        
        // Check Params Empty Or Not
        if($fjJobId!='' && $id!='') {
            // User Exists Or Not
            $userRow    = $this->coreModel->userExists($id);
            if(count($userRow)>0) {
                // Job Exists Or Not
                $jobRow = $this->coreModel->jobExists($fjJobId);
                if(count($jobRow)>0){
                    // User's Answers To corresponding Job Exists Or Not
                    $answerResult  = $this->setAnswerExists($jobRow['id'], $id, 'interview');

                    $userjobresultdata = $this->coreModel->queryRowArray("SELECT source FROM fj_temp_userjob WHERE userId='$id' AND jobId='$jobRow[id]' ORDER BY createdAt DESC LIMIT 0,1");
                    
                    $interviewSource = $userjobresultdata['source'];
                    if($interviewSource == 1) {
                        $directoryPath = '/var/www/html/jobseeker/';
                    } else {
                        $directoryPath = '/var/www/html/admin/';
                    }
                        try {                 
                            if(count($answerResult)>0) {
                                // Set Final Merged Video Name

                                $mergeVideoName = $id.$jobRow['id'];
                                $tempFile       = array();
                                $myFile         = $directoryPath.self::$mergeTextFile;
                                $handle         = fopen ($myFile, "w+");
                                fclose($handle);

                                $fileNameCreate = $directoryPath.$mergeVideoName.".txt";
                                $fp             = fopen($fileNameCreate,"wb");
                                fwrite($fp,"");
                                fclose($fp);

                                // Loop All User Answers For The Particular Job
                                foreach($answerResult as $key=>$answerRow) {
                                    //echo $answerRow['answer'];
                                    // Question Is Video Format
                                    if($answerRow['file']!='' && $answerRow['type']=='1'){                                
                                        $tempQusVideoName = $directoryPath."qus".$answerRow['id'].$answerRow['jobId'].$answerRow['userId'].".flv";
                                        $tempQusVideoNameTemp = "qus".$answerRow['id'].$answerRow['jobId'].$answerRow['userId'].".flv";

                                        $tempAnsVideoName = $directoryPath."ans".$answerRow['id'].$answerRow['jobId'].$answerRow['userId'].".flv";
                                        

                                        shell_exec("ffmpeg -y -i ".$directoryPath."uploads/questions/video/".$answerRow['file']." -vf scale=640:480  -vcodec libx264 -preset medium -crf 23 -acodec libmp3lame -ar 44100 -q:a 5 ".$directoryPath."qus1".$tempQusVideoNameTemp);

                                        shell_exec("ffmpeg -y -i ".$directoryPath."qus1".$tempQusVideoNameTemp." -vf pad='720:720:40:120:black'  -vcodec libx264 -preset medium -crf 23 -acodec libmp3lame -ar 44100 -q:a 5 ".$tempQusVideoName);
                                        $this->addVideoName($fileNameCreate, $tempQusVideoName);


                                        if($interviewSource == 1) {
                                            shell_exec("ffmpeg -y -i ".$directoryPath."uploads/answers/".$answerRow['answer']." -vf scale=640:480  -vcodec libx264 -preset medium -crf 23 -acodec libmp3lame -ar 44100 -q:a 5 ".$tempAnsVideoName);

                                            $tempAnsVideoName1 = $directoryPath."ans1".$answerRow['id'].$answerRow['jobId'].$answerRow['userId'].".flv";
                                            shell_exec("ffmpeg -y -i ".$directoryPath."".$tempAnsVideoName." -vf scale=720:720  -vcodec libx264 -preset medium -crf 23 -acodec libmp3lame -ar 44100 -q:a 5 ".$tempAnsVideoName1);
                                        } else {
                                            shell_exec("ffmpeg -y -i ".$directoryPath."uploads/answers/".$answerRow['answer']." -vf scale=640:480,transpose='2'  -vcodec libx264 -preset medium -crf 23 -acodec libmp3lame -ar 44100 -q:a 5 ".$tempAnsVideoName);

                                            $tempAnsVideoName1 = $directoryPath."ans1".$answerRow['id'].$answerRow['jobId'].$answerRow['userId'].".flv";
                                            shell_exec("ffmpeg -y -i ".$directoryPath."".$tempAnsVideoName." -vf pad='720:720:120:40:black'  -vcodec libx264 -preset medium -crf 23 -acodec libmp3lame -ar 44100 -q:a 5 ".$tempAnsVideoName1);
                                        }
                                        $this->addVideoName($fileNameCreate, $tempAnsVideoName1); 


                                        $tempFile[] = "qus1".$tempQusVideoName;
                                        $tempFile[] = $tempQusVideoName;
                                        $tempFile[] = $tempAnsVideoName;
                                        $tempFile[] = $tempAnsVideoName1;
                                    }
                                    // Question Is Audio & Text Format
                                    else if($answerRow['file']!='' && $answerRow['type']=='2'){
                                        $tempQusImageName   = $directoryPath."temp".$answerRow['id'].$answerRow['jobId'].$answerRow['userId'].".png";
                                        $tempQusVideoMP4    = $directoryPath."temp".$answerRow['id'].$answerRow['jobId'].$answerRow['userId'].".mp4";
                                        $this->createImage($answerRow['title'], $tempQusImageName);

                                        shell_exec("ffmpeg -y -i ".$directoryPath."uploads/questions/audio/".$answerRow['file']." -loop 1 -f image2 -i ".$tempQusImageName."  -vf scale=720:720 -codec:v mpeg4 -flags:v +qscale -global_quality:v 0 -codec:a libmp3lame -t 06 ".$tempQusVideoMP4);
                                        $tempQusVideoName = $directoryPath."qus".$answerRow['id'].$answerRow['jobId'].$answerRow['userId'].".flv";
                                        shell_exec("ffmpeg -y -i ".$tempQusVideoMP4." -vf scale=720:720  -vcodec libx264 -preset medium -crf 23 -acodec libmp3lame -ar 44100 -q:a 5 ".$tempQusVideoName);
                                        $this->addVideoName($fileNameCreate, $tempQusVideoName);
                                        
                                        if($interviewSource == 1) {
                                            $tempAnsVideoName = $directoryPath."ans".$answerRow['id'].$answerRow['jobId'].$answerRow['userId'].".flv";
                                            shell_exec("ffmpeg -y -i ".$directoryPath."uploads/answers/".$answerRow['answer']." -vf scale=640:480  -vcodec libx264 -preset medium -crf 23 -acodec libmp3lame -ar 44100 -q:a 5 ".$tempAnsVideoName);
                                            $tempAnsVideoName1 = $directoryPath."ans1".$answerRow['id'].$answerRow['jobId'].$answerRow['userId'].".flv";
                                            shell_exec("ffmpeg -y -i ".$tempAnsVideoName." -vf scale=720:720  -vcodec libx264 -preset medium -crf 23 -acodec libmp3lame -ar 44100 -q:a 5 ".$tempAnsVideoName1);
                                        } else {
                                            $tempAnsVideoName = $directoryPath."ans".$answerRow['id'].$answerRow['jobId'].$answerRow['userId'].".flv";
                                            shell_exec("ffmpeg -y -i ".$directoryPath."uploads/answers/".$answerRow['answer']." -vf scale=640:480,transpose='2'  -vcodec libx264 -preset medium -crf 23 -acodec libmp3lame -ar 44100 -q:a 5 ".$tempAnsVideoName);
                                            $tempAnsVideoName1 = $directoryPath."ans1".$answerRow['id'].$answerRow['jobId'].$answerRow['userId'].".flv";
                                            shell_exec("ffmpeg -y -i ".$tempAnsVideoName." -vf pad='720:720:120:40:black'  -vcodec libx264 -preset medium -crf 23 -acodec libmp3lame -ar 44100 -q:a 5 ".$tempAnsVideoName1);
                                        }
                                        $this->addVideoName($fileNameCreate, $tempAnsVideoName1); 
                                        
                                        
                                        $tempFile[] = $tempQusImageName;
                                        $tempFile[] = $tempQusVideoFLV;
                                        $tempFile[] = $tempQusVideoMP4;
                                        $tempFile[] = $tempQusVideoName;
                                        $tempFile[] = $tempAnsVideoName;
                                        $tempFile[] = $tempAnsVideoName1;
                                    }
                                    // Question Is Text Format Only
                                    else {                                
                                        $tempQusImageName   = $directoryPath."temp".$answerRow['id'].$answerRow['jobId'].$answerRow['userId'].".png";
                                        $tempQusVideoMP4    = $directoryPath."temp".$answerRow['id'].$answerRow['jobId'].$answerRow['userId'].".mp4";
                                        $this->createImage($answerRow['title'], $tempQusImageName);

                                        shell_exec("ffmpeg -y -i ".$directoryPath."uploads/testAudio.mp3 -loop 1 -f image2 -i ".$tempQusImageName."  -vf scale=720:720 -codec:v mpeg4 -flags:v +qscale -global_quality:v 0 -codec:a libmp3lame -t 06 ".$tempQusVideoMP4);                             
                                        $tempQusVideoName = $directoryPath."qus".$answerRow['id'].$answerRow['jobId'].$answerRow['userId'].".flv";
                                        shell_exec("ffmpeg -y -i ".$tempQusVideoMP4." -vf scale=720:720  -vcodec libx264 -preset medium -crf 23 -acodec libmp3lame -ar 44100 -q:a 5 ".$tempQusVideoName);
                                        $this->addVideoName($fileNameCreate, $tempQusVideoName);
                                        
                                        if($interviewSource == 1) {
                                            $tempAnsVideoName = $directoryPath."ans".$answerRow['id'].$answerRow['jobId'].$answerRow['userId'].".flv";
                                            shell_exec("ffmpeg -y -i ".$directoryPath."uploads/answers/".$answerRow['answer']." -vf scale=640:480  -vcodec libx264 -preset medium -crf 23 -acodec libmp3lame -ar 44100 -q:a 5 ".$tempAnsVideoName);
                                            $tempAnsVideoName1 = $directoryPath."ans1".$answerRow['id'].$answerRow['jobId'].$answerRow['userId'].".flv";
                                            shell_exec("ffmpeg -y -i ".$tempAnsVideoName." -vf scale=720:720  -vcodec libx264 -preset medium -crf 23 -acodec libmp3lame -ar 44100 -q:a 5 ".$tempAnsVideoName1);
                                        } else {
                                            $tempAnsVideoName = $directoryPath."ans".$answerRow['id'].$answerRow['jobId'].$answerRow['userId'].".flv";
                                            shell_exec("ffmpeg -y -i ".$directoryPath."uploads/answers/".$answerRow['answer']." -vf scale=640:480,transpose='2'  -vcodec libx264 -preset medium -crf 23 -acodec libmp3lame -ar 44100 -q:a 5 ".$tempAnsVideoName);
                                            $tempAnsVideoName1 = $directoryPath."ans1".$answerRow['id'].$answerRow['jobId'].$answerRow['userId'].".flv";
                                            shell_exec("ffmpeg -y -i ".$tempAnsVideoName." -vf pad='720:720:120:40:black'  -vcodec libx264 -preset medium -crf 23 -acodec libmp3lame -ar 44100 -q:a 5 ".$tempAnsVideoName1);
                                        }
                                        $this->addVideoName($fileNameCreate, $tempAnsVideoName1); 
                                        

                                        $tempFile[] = $tempQusImageName;
                                        //$tempFile[] = $tempQusVideoFLV;
                                        $tempFile[] = $tempQusVideoMP4;
                                        $tempFile[] = $tempQusVideoName;
                                        $tempFile[] = $tempAnsVideoName;
                                        $tempFile[] = $tempAnsVideoName1;
                                    }
                                }

                                // Combine Video
                                
                                $jkdfhh = shell_exec("ffmpeg -y -f concat -i  ".$fileNameCreate." -c copy  -vf scale=720:720 -vcodec libx264 -preset medium -crf 23 -acodec libmp3lame -ar 44100 -q:a 5  ".$directoryPath."uploads/mergeVideo/".$mergeVideoName.".flv 2>&1");
                                //print_r($jkdfhh);exit;
                                

                                $uploadFile = $directoryPath.'uploads/mergeVideo/'.$mergeVideoName.'.flv';
                                $bucketName = 'videoMerge';
                                $s3         = new S3(awsAccessKey, awsSecretKey);
                                
                                if ($s3->putBucket($bucketName, S3::ACL_PUBLIC_READ)) {

                                    if ($s3->putObjectFile($uploadFile, $bucketName, baseName($uploadFile), S3::ACL_PUBLIC_READ)) {

                                        $userRowJob = $this->coreModel->queryRowArray("SELECT id, status, createdAt FROM fj_temp_userjob WHERE userId='$userRow[id]' AND jobId='$jobRow[id]' ORDER BY createdAt DESC LIMIT 0,1");
                                        if(count($userRowJob)==0) {
                                            // Save Applied Job Information
                                            $insertData    = array(
                                                                'userId'    => (int)$id,
                                                                'jobId'     => (int)$jobRow['id'],
                                                                'status'    => '1',
                                                                'createdAt' => date('Y-m-d H:i:s'),
                                                                'mergedVideo' => (string)$mergeVideoName.".flv"
                                                            );
                                            $this->db->insert('fj_temp_userjob', $insertData);

                                            $result2 = shell_exec('ffmpeg -i ' . escapeshellcmd($directoryPath.'uploads/mergeVideo/'.$mergeVideoName) . ' 2>&1');

                                            //preg_match('/(?<=Duration: )(\d{2}:\d{2}:\d{2})\.\d{2}/', $result2, $match2);
                                            //$this->db->insert('fj_interview_time_check', array('duration' => $match2[1], 'file' => $mergeVideoName));

                                            unlink($fileNameCreate);
                                            unlink($uploadFile);
                                            foreach($tempFile as $tempFile) {
                                                unlink($tempFile);
                                            }

                                            //Removing answer part video
                                            foreach ($allAnswerVideo as $answerPart) {
                                                $answerVideoFile = $directoryPath.'uploads/answers/'.$answerPart;
                                                $answerBucketName = 'fjinterviewanswers';

                                                if ($s3->putBucket($answerBucketName, S3::ACL_PUBLIC_READ)) {
                                                    if ($s3->putObjectFile($answerVideoFile, $answerBucketName, baseName($answerVideoFile), S3::ACL_PUBLIC_READ)) {
                                                        unlink($answerVideoFile);
                                                    }
                                                }
                                            }
                                        }
                                        else {
                                            $whereData  = array('userId'=> (int)$id, 'jobId' => (int)$jobRow['id']);
                                            $updateData = array('mergedVideo' => (string)$mergeVideoName.".flv");
                                            $updateJob  = $this->db->where($whereData)->update('fj_temp_userjob', $updateData);

                                            $result2 = shell_exec('ffmpeg -i ' . escapeshellcmd($directoryPath.'uploads/mergeVideo/'.$mergeVideoName) . ' 2>&1');

                                            //preg_match('/(?<=Duration: )(\d{2}:\d{2}:\d{2})\.\d{2}/', $result2, $match2);
                                            //$this->db->insert('fj_interview_time_check', array('duration' => $match2[1], 'file' => $mergeVideoName));
                                            
                                            unlink($fileNameCreate);
                                            unlink($uploadFile);
                                            foreach($tempFile as $tempFile) {
                                                unlink($tempFile);
                                            }

                                            //Removing answer part video
                                            foreach ($allAnswerVideo as $answerPart) {
                                                $answerVideoFile = $directoryPath.'uploads/answers/'.$answerPart;
                                                $answerBucketName = 'fjinterviewanswers';

                                                if ($s3->putBucket($answerBucketName, S3::ACL_PUBLIC_READ)) {
                                                    if ($s3->putObjectFile($answerVideoFile, $answerBucketName, baseName($answerVideoFile), S3::ACL_PUBLIC_READ)) {
                                                        unlink($answerVideoFile);
                                                    }
                                                }
                                            }
                                        }
                                        $message    = "success";
                                    } 
                                    else {
                                        $message    = "something wemt wrong";
                                    }
                                } else {
                                    $message    = "something wemt wrong";
                                }
                            }              
                            else {
                                $message    = "no job";
                            }
                        } catch (Exception $e) {
                            echo 'Caught exception: ',  $e->getMessage(), "\n";
                        }
                }
                else {
                    $message    = "no job";
                }
            }
            else {
                $message    = "no user";
            }
        }
        else {
            $message    = "invalid attempt";
        }
        return $message;
    }

    /**
     * Description : Use to generate interview video as file type ffmpeg
     * Author : Synergy
     * @param int $jobId, string $jobCode
     * @return array or data
     */
    public function generatesadasVideo($userId, $jobCode) {
        $id         = $this->coreModel->cleanString($userId);
        $fjJobId    = $this->coreModel->cleanString($jobCode);
        
        // Check Params Empty Or Not
        if($fjJobId!='' && $id!='') {
            // User Exists Or Not
            $userRow    = $this->coreModel->userExists($id);
            if(count($userRow)>0) {
                // Job Exists Or Not
                $jobRow = $this->coreModel->jobExists($fjJobId);
                if(count($jobRow)>0){
                    // User's Answers To corresponding Job Exists Or Not
                    $answerResult  = $this->setAnswerExists($jobRow['id'], $id, 'interview');                    
                                        
                    if(count($answerResult)>0) {
                        // Set Final Merged Video Name
                        $mergeVideoName = $id.$jobRow['id'];
                        $tempFile       = array();
                        $myFile         = self::$mergeTextFile;
                        $handle         = fopen ($myFile, "w+");
                        fclose($handle);

                        $fileNameCreate = $mergeVideoName.".txt";
                        $fp             = fopen($fileNameCreate,"wb");
                        fwrite($fp,"");
                        fclose($fp);

                        // Loop All User Answers For The Particular Job
                        foreach($answerResult as $key=>$answerRow) {
                            //echo $answerRow['answer'];
                            // Question Is Video Format
                            if($answerRow['file']!='' && $answerRow['type']=='1'){
                                $tempQusVideoName = "qus".$answerRow['id'].$answerRow['jobId'].$answerRow['userId'].".flv";
                                $tempAnsVideoName = "ans".$answerRow['id'].$answerRow['jobId'].$answerRow['userId'].".flv";

                                shell_exec("ffmpeg -y -i /var/www/html/admin/uploads/questions/video/".$answerRow['file']." -vf scale=480:320  -vcodec libx264 -preset medium -crf 23 -acodec libmp3lame -ar 44100 -q:a 5 ".$tempQusVideoName);
                                $this->addVideoName($fileNameCreate, $tempQusVideoName);

                                shell_exec("ffmpeg -y -i /var/www/html/admin/uploads/answers/".$answerRow['answer']." -vf scale=480:320  -vcodec libx264 -preset medium -crf 23 -acodec libmp3lame -ar 44100 -q:a 5 ".$tempAnsVideoName);
                                $this->addVideoName($fileNameCreate, $tempAnsVideoName); 

                                $tempFile[] = $tempQusVideoName;
                                $tempFile[] = $tempAnsVideoName;

                            }
                            // Question Is Audio & Text Format
                            else if($answerRow['file']!='' && $answerRow['type']=='2'){                                
                                $tempQusImageName   = "temp".$answerRow['id'].$answerRow['jobId'].$answerRow['userId'].".png";
                                $tempQusVideoMP4    = "temp".$answerRow['id'].$answerRow['jobId'].$answerRow['userId'].".mp4";
                                $tempQusVideoName   = "qus".$answerRow['id'].$answerRow['jobId'].$answerRow['userId'].".flv";
                                $tempAnsVideoName   = "ans".$answerRow['id'].$answerRow['jobId'].$answerRow['userId'].".flv";
                                $this->createImage($answerRow['title'], $tempQusImageName);

                                shell_exec("ffmpeg -y -i /var/www/html/admin/uploads/questions/audio/".$answerRow['file']." -loop 1 -f image2 -i ".$tempQusImageName."  -vf scale=480:320 -codec:v mpeg4 -flags:v +qscale -global_quality:v 0 -codec:a libmp3lame -t 06 ".$tempQusVideoMP4);

                                shell_exec("ffmpeg -y -i /var/www/html/admin/".$tempQusVideoMP4." -vf scale=480:320  -vcodec libx264 -preset medium -crf 23 -acodec libmp3lame -ar 44100 -q:a 5 ".$tempQusVideoName);
                                $this->addVideoName($fileNameCreate, $tempQusVideoName);

                                shell_exec("ffmpeg -y -i /var/www/html/admin/uploads/answers/".$answerRow['answer']." -vf scale=480:320  -vcodec libx264 -preset medium -crf 23 -acodec libmp3lame -ar 44100 -q:a 5 ".$tempAnsVideoName);
                                $this->addVideoName($fileNameCreate, $tempAnsVideoName);                                

                                $tempFile[] = $tempQusImageName;
                                $tempFile[] = $tempQusVideoFLV;
                                $tempFile[] = $tempQusVideoMP4;
                                $tempFile[] = $tempQusVideoName;
                                $tempFile[] = $tempAnsVideoName;                                
                            }
                            // Question Is Text Format Only
                            else {
                                //echo $userId.$jobCode; exit;
                                $tempQusImageName   = "temp".$answerRow['id'].$answerRow['jobId'].$answerRow['userId'].".png";
                                $tempQusVideoMP4    = "temp".$answerRow['id'].$answerRow['jobId'].$answerRow['userId'].".mp4";
                                $this->createImage($answerRow['title'], $tempQusImageName);

                                shell_exec("ffmpeg -y -i /var/www/html/admin/uploads/testAudio.mp3 -loop 1 -f image2 -i ".$tempQusImageName."  -vf scale=480:320 -codec:v mpeg4 -flags:v +qscale -global_quality:v 0 -codec:a libmp3lame -t 06 ".$tempQusVideoMP4);

                                $tempQusVideoName = "qus".$answerRow['id'].$answerRow['jobId'].$answerRow['userId'].".flv";
                                shell_exec("ffmpeg -y -i /var/www/html/admin/".$tempQusVideoMP4." -vf scale=480:320  -vcodec libx264 -preset medium -crf 23 -acodec libmp3lame -ar 44100 -q:a 5 ".$tempQusVideoName." ");

                                $this->addVideoName($fileNameCreate, $tempQusVideoName);
                                $tempAnsVideoName = "ans".$answerRow['id'].$answerRow['jobId'].$answerRow['userId'].".flv";
                                shell_exec("ffmpeg -y -i /var/www/html/admin/uploads/answers/".$answerRow['answer']." -vf scale=480:320  -vcodec libx264 -preset medium -crf 23 -acodec libmp3lame -ar 44100 -q:a 5 ".$tempAnsVideoName." ");

                                $this->addVideoName($fileNameCreate, $tempAnsVideoName);                                
                                $tempFile[] = $tempQusImageName;
                                $tempFile[] = $tempQusVideoFLV;
                                $tempFile[] = $tempQusVideoMP4;
                                $tempFile[] = $tempQusVideoName;
                                $tempFile[] = $tempAnsVideoName;
                            }
                        }
                        // Combine Video
                        shell_exec("ffmpeg -y -f concat -i ".$fileNameCreate." -c copy  -vf scale=480:320 -vcodec libx264 -preset medium -crf 23 -acodec libmp3lame -ar 44100 -q:a 5  uploads/mergeVideo/".$mergeVideoName.".flv ");

                        foreach($tempFile as $tempFile) {
                            unlink($tempFile);
                        }
                        unlink($fileNameCreate);

                        $whereData  = array('userId'=> (int)$id, 'jobId' => (int)$jobRow['id']);
                        $updateData = array('mergedVideo' => (string)$mergeVideoName.".flv");
                        $updateJob  = $this->db->where($whereData)->update('fj_userJob', $updateData);
                        $message    = "success";
                    }              
                    else {
                        $message    = "no job";
                    }
                }
                else {
                    $message    = "no job";
                }
            }
            else {
                $message    = "no user";
            }
        }
        else {
            $message    = "invalid attempt";
        }
        return $message;
    }

    /**
     * Description : Use to share interview video given by a specific user on specific job on different email ids
     * Author : Synergy
     * @param int $jobId, string $jobCode
     * @return array or data
     */
    public function sendInterview($data) {
        $resp           = $this->db->insert('fj_userJobShare', $data);
        if ($resp) {            
            $userJobShareId = $this->db->insert_id();
            return $userJobShareId;
        } else {
            return false;
        }
    }
	
	function listJobsNew($limit, $offset, $serachColumn = NULL, $searchText = NULL, $createdBy = NULL, $usersJobListL1, $usersJobListL2, $usersJobListL1L2, $userRole) {
        try {
            $currentDate = date('Y-m-d h:i:s');
            $sql = "select j.id, j.jobId, j.fjCode, j.title, j.openTill, j.assessment, j.interview from fj_jobs as j";
            
            if( $usersJobListL1 != "" && $usersJobListL2 != "" )
            {   
                $sql .= " where ( j.id in (".$usersJobListL1.") or j.id in (".$usersJobListL2.") )"; 
                
            } else if( $usersJobListL1 != "" && $usersJobListL2 == "" ) {
                $sql .= " where j.id in (".$usersJobListL1.")";
            } else if( $usersJobListL1 == "" && $usersJobListL2 != "" ) {
                $sql .= " where j.id in (".$usersJobListL2.")";
            } else if( $usersJobListL1L2 != "" && $userRole == 2 ) {
                $sql .= " where j.id in (".$usersJobListL1L2.")";
            } else {
                $sql .= " where j.id in ('')";
            }
            //$sql .= " and j.openTill >= CURDATE()";
			$sql .= " and j.status in ('1','2','5') ";
			$sql .= " order by j.title asc ";
            
			
            $query = $this->db->query($sql);
            $result = $query->result();

            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    function getLastCreatedInterview($interviewId, $userId,$usersJobListL1L2) {
        $sql = "SELECT uj.id interviewId,uj.status, uj.userId userId, job.title jobName, job.id jobId, fu.fullname candidateName, uj.createdAt FROM fj_userJob uj JOIN fj_jobs job ON job.id = uj.jobId JOIN fj_users fu ON fu.id = uj.userId WHERE job.id in (".$usersJobListL1L2.") AND uj.id > $interviewId";
		
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result;
    }

    function getjobPendingApplicationData($date1,$date2, $userId){
        $sql = "SELECT job.id, job.fjCode, job.title, user.company, (SELECT COUNT(*) FROM fj_userJob WHERE jobId = job.id) totalApplicationReceivedAtMain, (SELECT COUNT(*) FROM fj_userJob WHERE jobId = job.id AND shorlistedByCorporateForL1User IS NULL) totalPendingAtMain, (SELECT COUNT(*) FROM fj_userJob uj1 JOIN fj_jobPanel panel1 ON panel1.jobId = uj1.jobId WHERE panel1.level='1' AND uj1.jobId = job.id) totalApplicationReceivedAtL1, (SELECT COUNT(*) FROM fj_userJob uj1 JOIN fj_jobPanel panel1 ON panel1.jobId = uj1.jobId WHERE panel1.level='1' AND uj1.shorlistedByL1User IS NULL AND uj1.jobId = job.id) totalPendingApplicationAtL1, (SELECT COUNT(*) FROM fj_userJob uj1 JOIN fj_jobPanel panel1 ON panel1.jobId = uj1.jobId WHERE panel1.level='1' AND uj1.shorlistedByL1User IS NOT NULL AND status IN('2','4') AND uj1.jobId = job.id) totalShortlistedApplicationAtL1, (SELECT COUNT(*) FROM fj_userJob uj1 JOIN fj_jobPanel panel1 ON panel1.jobId = uj1.jobId WHERE panel1.level='1' AND uj1.shorlistedByL1User IS NOT NULL AND status IN('3','5') AND uj1.jobId = job.id) totalRejectedApplicationAtL1, (SELECT COUNT(*) FROM fj_userJob uj1 JOIN fj_jobPanel panel1 ON panel1.jobId = uj1.jobId WHERE panel1.level='2' AND uj1.jobId = job.id) totalApplicationReceivedAtL2, (SELECT COUNT(*) FROM fj_userJob uj1 JOIN fj_jobPanel panel1 ON panel1.jobId = uj1.jobId WHERE panel1.level='2' AND uj1.shorlistedByL2User IS NULL AND uj1.jobId = job.id) totalPendingApplicationAtL2, (SELECT COUNT(*) FROM fj_userJob uj1 JOIN fj_jobPanel panel1 ON panel1.jobId = uj1.jobId WHERE panel1.level='2' AND uj1.shorlistedByL2User IS NOT NULL AND status IN('4') AND uj1.jobId = job.id) totalShortlistedApplicationAtL2, (SELECT COUNT(*) FROM fj_userJob uj1 JOIN fj_jobPanel panel1 ON panel1.jobId = uj1.jobId WHERE panel1.level='2' AND uj1.shorlistedByL2User IS NOT NULL AND status IN('5') AND uj1.jobId = job.id) totalRejectedApplicationAtL2, ( SELECT  GROUP_CONCAT(ruser.fullname) FROM fj_jobPanel panel2 JOIN fj_users ruser ON ruser.id = panel2.userId WHERE panel2.level = '1' AND panel2.jobId = job.id) l1recruiters, ( SELECT  GROUP_CONCAT(ruser.fullname) FROM fj_jobPanel panel2 JOIN fj_users ruser ON ruser.id = panel2.userId WHERE panel2.level = '2' AND panel2.jobId = job.id) l2recruiters, ( SELECT  GROUP_CONCAT(ruser.id) FROM fj_jobPanel panel2 JOIN fj_users ruser ON ruser.id = panel2.userId WHERE panel2.level = '1' AND panel2.jobId = job.id) l1recruitersId, ( SELECT  GROUP_CONCAT(ruser.id) FROM fj_jobPanel panel2 JOIN fj_users ruser ON ruser.id = panel2.userId WHERE panel2.level = '2' AND panel2.jobId = job.id) l2recruitersId, ( SELECT DISTINCT panel2.level FROM fj_jobPanel panel2 WHERE panel2.level='2' AND panel2.jobId = job.id) leveldata2, ( SELECT DISTINCT panel2.level FROM fj_jobPanel panel2 WHERE panel2.level='1' AND panel2.jobId = job.id) leveldata1 FROM `fj_jobs` job JOIN fj_users user ON user.id = job.createdBy WHERE job.status IN(1, 5) AND job.createdBy = $userId ORDER BY totalApplicationReceivedAtL1 DESC";
        
        
        $query  = $this->db->query($sql);
        $result = $query->result_array();  
        return $result;
    }
}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Admin Controller
 * Description : Used to handle login page
 * @author : Synergy
 * @createddate : Nov 10, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 15, 2017
 */
class Admin extends MY_Controller {

    /**
     * Responsable for auto load the users module
     * @return void
     */
    function __construct() {
        parent::__construct();
        $this->load->module("users");
    }

    /**
     * Description : Display index page
     * Author : Synergy
     * @param null
     * @return array of data 
     */
    function index() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('signin'));
        }
        $data['main_content'] = 'dashboard';
        $this->load->view('page', $data);
    }
}

<link type="text/css" rel="stylesheet" href="http://themifycloud.com/demos/templates/KAdmin/KAdmin-Light/styles/font-awesome.min.css">
<!-- <link type="text/css" rel="stylesheet" href="http://themifycloud.com/demos/templates/KAdmin/KAdmin-Light/styles/main.css"> -->
<style>
.page-title-breadcrumb .page-header .page-title {
    display: inline-block;
    font-size: 25px;
    font-weight: 300;
}
#sum_box .icon {
    color: #999999;
    float: right;
    font-size: 55px;
    margin-bottom: 0;
     
    margin-top: 11px !important;
}

.page-content {
    padding-bottom: 50px;
    padding-top: 20px;
}
.panel{
	width:100% !important;
	}
.panel.db.mbm{ padding:10px;}
</style>


<div class="page-title-breadcrumb" id="title-breadcrumb-option-demo">
    <div class="page-header pull-left">
        <div class="page-title">Dashboard</div>
    </div>
    <ol class="breadcrumb page-breadcrumb pull-right">
        <li><i class="fa fa-home"></i>&nbsp;<a href="#">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
        <li class="hidden"><a href="#">Dashboard</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
        <li class="active">Dashboard</li>
    </ol>
    <div class="clearfix">
    </div>
</div>

<div class="page-content">
        <div class="row mbl" id="sum_box">
        <div class="col-sm-6 col-md-4">
            <div class="panel profit db mbm">
                <div class="panel-body">
                    <p class="icon">
                        <i class="icon fa fa-shopping-cart"></i>
                    </p>
                    <h4 class="value">
                        <span data-duration="0" data-step="1" data-end="50" data-start="10" data-counter="">189</span><span>$</span></h4>
                    <p class="description">
                        Profit description</p>
                    <div class="progress progress-sm mbn">
                        <div class="progress-bar progress-bar-success" style="width: 80%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="80" role="progressbar">
                            <span class="sr-only">80% Complete (success)</span></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4">
            <div class="panel income db mbm">
                <div class="panel-body">
                    <p class="icon">
                        <i class="icon fa fa-money"></i>
                    </p>
                    <h4 class="value">
                        <span>812</span><span>$</span></h4>
                    <p class="description">
                        Income detail</p>
                    <div class="progress progress-sm mbn">
                        <div class="progress-bar progress-bar-info" style="width: 60%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="60" role="progressbar">
                            <span class="sr-only">60% Complete (success)</span></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4">
            <div class="panel task db mbm">
                <div class="panel-body">
                    <p class="icon">
                        <i class="icon fa fa-signal"></i>
                    </p>
                    <h4 class="value">
                        <span>155</span></h4>
                    <p class="description">
                        Task completed</p>
                    <div class="progress progress-sm mbn">
                        <div class="progress-bar progress-bar-danger" style="width: 50%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="50" role="progressbar">
                            <span class="sr-only">50% Complete (success)</span></div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
    
<!--2-->    
    <div class="row mbl">
        <div class="col-lg-8">
        	<div class="panel">&nbsp;</div>
        </div>
        <div class="col-lg-4">
        	<div class="panel">&nbsp;</div>
        </div>
    </div>
<!--3-->

<div class="row mbl">
        <div class="col-lg-4">
        	<div class="panel">&nbsp;</div>
        </div>
        <div class="col-lg-8">
        	<div class="panel">&nbsp;</div>
        </div>
    </div>
</div>

<!--<div class="row">
    <div class="col-sm-12 col-md-12">
        <h2>Dashboard</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut egestas pulvinar odio nec dignissim. Aenean vulputate, lorem condimentum sollicitudin blandit, leo elit fermentum mauris, eget congue nunc mi vel turpis. Donec consequat, tellus eu pretium semper, nibh lectus blandit eros, quis auctor</p>
    </div>
</div>
-->
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Industry Model
 * Description : Handle all the CRUD operation for Industry
 * @author Synergy
 * @createddate : Dec 10, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 16, 2017
 */
class Industry_model extends CI_Model {

    /**
     * Defining static variable to store tables name
     */
    var $indus_table = "nc_industry";

    /**
     * Responsable for inherit the parent constructor
     * @return void
     */
    function __construct() {
        parent::__construct();
    }

    /**
     * Discription : Use to get list of industries
     * @author Synergy
     * @params int $showPerpage, int $offset
     * @return array of industry data
     */
    function getList($showPerpage, $offset) {
        $conditions=array(
            'indus_id'=>'0'
        );
        $this->db->where($conditions);
        $this->db->limit($showPerpage, $offset);
        $this->db->from($this->indus_table);
        $result = $this->db->get();
        return $result->result();
    }

    /**
     * Discription : Use to update the status of a specific industry
     * @author Synergy
     * @params array $_POST
     * @return boolean
     */
    function updateStatus() {
        if ($this->input->post('Delete') != null) {
            $arr_ids = $this->input->post('arr_ids');
            if (!empty($arr_ids)) {
                $this->db->where_in('id', $arr_ids);
                $this->db->delete($this->indus_table);
            }
        } else {
            if ($this->input->post('Deactivate') != null) {
                $status = "0";
            } else if ($this->input->post('Activate') != null) {
                $status = "1";
            }
            $data = array(
                'is_active' => $status
            );
            $arr_ids = $this->input->post('arr_ids');
            if (!empty($arr_ids)) {
                $this->db->where_in('id', $arr_ids);
                $this->db->update($this->indus_table, $data);
            }
        }
    }

    /**
     * Discription : Use to count the number of industries in industry table
     * @author Synergy
     * @params none
     * @return int(number of industries)
     */
    function count_all() {
        $query = $this->db->get($this->indus_table);
        return $query->num_rows();
    }

    /**
     * Discription : Use to delete the specific industry from industry table
     * @author Synergy
     * @params int $id
     * @return boolean
     */
    function deleteIndustry($id) {
        $this->db->where('id', $id);
        $query = $this->db->delete($this->indus_table);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Discription : Use to create new industry
     * @author Synergy
     * @params array $_POST
     * @return boolean
     */
    public function createIndustry() {
        $data = array(
            'indus_id' => '0',
            'name' => $this->input->post('name'),
            'is_active' => '1'
        );
        $query = $this->db->insert($this->indus_table, $data);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Discription : Use to update a specific industry
     * @author Synergy
     * @params int $id, array $_POST
     * @return boolean
     */
    function updateIndustry($id) {
        $data = array(
            'name' => $this->input->post('name'),
            'is_active' => $this->input->post('is_active'),
        );
        $this->db->where('id', $id);
        $query = $this->db->update($this->indus_table, $data);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Discription : Use to get details of a specific industry
     * @author Synergy
     * @params int $id
     * @return array industry data
     */
    function getIndustry($id) {
        $this->db->where('id', $id);
        $this->db->from($this->indus_table);
        $result = $this->db->get();
        return $result->row();
    }
    
    /**
     * Discription : Use to get the list of industry
     * @author Synergy
     * @params none
     * @return array industry data
     */
    function getIndustryList(){
        $conditions=array(
            'is_active'=>'1',
            'indus_id'=>'0'
        );
        $this->db->where($conditions);
        $this->db->from($this->indus_table);
        $result = $this->db->get();
        return $result->result();
    }
    
    /**
     * Discription : Use to create function for industry
     * @author Synergy
     * @params array $_POST
     * @return boolean
     */
    function createFunction(){
        $data = array(
            'indus_id' => $this->input->post('indus_id'),
            'name' => $this->input->post('name'),
            'is_active' => '1'
        );
        $query = $this->db->insert($this->indus_table, $data);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * Discription : Use to update status of functions for industry
     * @author Synergy
     * @params array $_POST
     * @return boolean
     */
    function updateFunctionStatus() {
        if ($this->input->post('Delete') != null) {
            $arr_ids = $this->input->post('arr_ids');
            if (!empty($arr_ids)) {
                $this->db->where_in('id', $arr_ids);
                $this->db->delete($this->indus_table);
            }
        } else {
            if ($this->input->post('Deactivate') != null) {
                $status = "0";
            } else if ($this->input->post('Activate') != null) {
                $status = "1";
            }
            $data = array(
                'is_active' => $status
            );
            $arr_ids = $this->input->post('arr_ids');
            if (!empty($arr_ids)) {
                $this->db->where_in('id', $arr_ids);
                $this->db->update($this->indus_table, $data);
            }
        }
    }
    
    /**
     * Discription : Use to count all the functions of a particular industry
     * @author Synergy
     * @params int $id
     * @return int function count
     */
    function count_all_functions($id) {
        $conditions=array(
            'indus_id'=>$id
        );
        $this->db->where($conditions);
        $query = $this->db->get($this->indus_table);
        return $query->num_rows();
    }
    
    /**
     * Discription : Use to get list of functions
     * @author Synergy
     * @params int $id, int $showPerpage, int $offset
     * @return array of data
     */
    function getFunctionList($id,$showPerpage, $offset) {
        $conditions=array(
            'indus_id'=>$id
        );
        $this->db->where($conditions);
        $this->db->limit($showPerpage, $offset);
        $this->db->from($this->indus_table);
        $result = $this->db->get();
        return $result->result();
    }
    
    /**
     * Discription : Use to get the details of a function
     * @author Synergy
     * @params int $id
     * @return array of data
     */
    function getFunction($id) {
        $this->db->where('id', $id);
        $this->db->from($this->indus_table);
        $result = $this->db->get();
        return $result->row();
    }

    /**
     * Discription : Use to update the details of a specific function
     * @author Synergy
     * @params int $id, array $_POST
     * @return boolean
     */
    function updateFunction($id) {
        $data = array(
            'indus_id' => $this->input->post('indus_id'),
            'name' => $this->input->post('name')
        );
        $this->db->where('id', $id);
        $query = $this->db->update($this->indus_table, $data);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * Discription : Use to get list of functions of a specific industry
     * @author Synergy
     * @params int $id, array $_POST
     * @return boolean
     */
    function getListOfFunction($id) {
        $this->db->where('indus_id', $id);
        $this->db->from($this->indus_table);
        $result = $this->db->get();
        return $result->result();
    }
}
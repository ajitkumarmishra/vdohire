<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Audition Model
 * Description : Handle all the CRUD operation for Audition
 * @author Synergy
 * @createddate : Dec 3, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 16, 2017
 */
class Audition_model extends CI_Model {

    /**
     * Defining static variable to store tables name
     */
    static $auditionSetTable    = "fj_auditionSet";
    static $auditionSetQusTable = "fj_auditionSetQuestions";
    static $auditionQusTable    = "fj_auditionQuestions";
    static $auditionVideo       = "fj_userAudition";
    static $auditionVideoTempTable    = "fj_temp_useraudition";
    static $interviewVideoTempTable    = "fj_temp_userjob";
    static $interviewVideo    = "fj_userjob";
    static $userTable           = "fj_users";

    /**
     * Responsable for auto load the the session and pagination library
     * @return void
     */
    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('pagination');
    }
    
    /**
     * Discription : Used to get total number of audition set
     * Author : Synergy
     * @param $limit, $offset
     * @return int total recortd or string error message 
     */
    function listAuditionsTotalrecords($limit, $offset, $serachColumn = NULL, $searchText = NULL, $createdBy = NULL) {
        try {
            $currentDate = date('Y-m-d h:i:s');
            $sql    = "SELECT A.*, U.fullname "
                    . "FROM ".Audition_model::$auditionSetTable."   A "
                    . "LEFT JOIN ".Audition_model::$userTable."     U "
                    . "ON A.createdBy=U.id "
                    . "WHERE A.status != 3";
            $query          = $this->db->query($sql);             
            $total_records  = $query->num_rows();
            return $total_records;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Discription : Used to get list of audition sets
     * Author : Synergy
     * @param $limit, $offset
     * @return array audition data or string error message 
     */
    function listAuditions($limit, $offset, $serachColumn = NULL, $searchText = NULL, $createdBy = NULL) {
        try {
            $currentDate = date('Y-m-d h:i:s');
            $sql    = "SELECT A.*, U.fullname "
                    . "FROM ".Audition_model::$auditionSetTable."   A "
                    . "LEFT JOIN ".Audition_model::$userTable."     U "
                    . "ON A.createdBy=U.id "
                    . "WHERE A.status != 3 "
                    . "ORDER BY A.id desc";            
            $query  = $this->db->query($sql);             
            $page   = ($this->uri->segment(3, 0)=='set'?$this->uri->segment(4, 0):0);
            $per_page       = '10';
            $start_limit = ($page) * 1;
            if ($start_limit > 1) {
                $start_limit = $start_limit;
            } else {
                $start_limit = 0;
            }                      
            $sql .= " LIMIT " .$per_page. " OFFSET " .$start_limit."";            

            $queryResult = $this->db->query($sql);
            $result = $queryResult->result();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Discription : Used to get total number of temp auditions
     * Author : Synergy
     * @param $limit, $offset
     * @return int total recortd or string error message 
     */
    function listAuditionTempVideosTotalRecords($limit, $offset) {
        try {
            $sql    = "SELECT A.*, U.fullname "
                    . "FROM ".Audition_model::$auditionVideoTempTable."   A "
                    . "LEFT JOIN ".Audition_model::$userTable."     U "
                    . "ON A.createdBy=U.id "
                    . "WHERE A.status = 1";
            $query          = $this->db->query($sql);             
            $total_records  = $query->num_rows();
            return $total_records;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Discription : Used to get list of temp auditions
     * Author : Synergy
     * @param $limit, $offset
     * @return array audition data or string error message 
     */
    function listAuditionTempVideos($limit, $offset) {
        try {
            $sql    = "SELECT A.*, U.fullname "
                    . "FROM ".Audition_model::$auditionVideoTempTable."   A "
                    . "LEFT JOIN ".Audition_model::$userTable."     U "
                    . "ON A.createdBy=U.id "
                    . "WHERE A.status = 1 "
                    . "ORDER BY A.id desc";            
            $query  = $this->db->query($sql);             

            $page   = ($this->uri->segment(3, 0)=='set'?$this->uri->segment(4, 0):0);
            $per_page       = '10';
            $start_limit = ($page) * 1;

            if ($start_limit > 1) {
                $start_limit = $start_limit;
            } else {
                $start_limit = 0;
            }

            $sql .= " LIMIT " .$per_page. " OFFSET " .$start_limit."";            

            $queryResult = $this->db->query($sql);
            $result = $queryResult->result();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Discription : Used to get total number of temp interviews temp videos
     * Author : Synergy
     * @param $limit, $offset
     * @return int total recortd or string error message 
     */
    function listInterviewTempVideosTotalRecords($limit, $offset) {
        try {
            $sql    = "SELECT I.*, U.fullname "
                    . "FROM ".Audition_model::$interviewVideoTempTable."   I "
                    . "LEFT JOIN ".Audition_model::$userTable."     U "
                    . "ON I.userId=U.id "
                    . "WHERE I.status = 1";
            $query          = $this->db->query($sql);             
            $total_records  = $query->num_rows();
            return $total_records;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Discription : Used to get list of temp interviews
     * Author : Synergy
     * @param $limit, $offset
     * @return array audition data or string error message 
     */
    function listInterviewTempVideos($limit, $offset) {
        $sql    = "SELECT I.*, U.fullname, JU.company companyName, FJ.title jobName, FJ.id fjJobId, FJ.fjCode fjfjCode, U.id fjUserId "
                . "FROM ".Audition_model::$interviewVideoTempTable."   I "
                . "LEFT JOIN ".Audition_model::$userTable."     U "
                . "ON I.userId=U.id "
                . "LEFT JOIN fj_jobs FJ "
                . "ON FJ.id=I.jobId "
                . "LEFT JOIN fj_users JU "
                . "ON JU.id=FJ.createdBy "
                . "WHERE I.status = 1 "
                . "ORDER BY I.id desc";
        $query  = $this->db->query($sql);             
        $page   = ($this->uri->segment(3, 0)=='set'?$this->uri->segment(4, 0):0);
        $per_page       = '200';

        $start_limit = ($page) * 1;

        if ($start_limit > 1) {
            $start_limit = $start_limit;
        } else {
            $start_limit = 0;
        }

        $sql .= " LIMIT " .$per_page. " OFFSET " .$start_limit."";            
        $queryResult = $this->db->query($sql);
        $result = $queryResult->result();
        return $result;
        
    }

    /**
     * Discription : Used to update audition status and add in original audition table and remove from tempt table
     * Author : Synergy
     * @param int $id
     * @return int or boolean or string
     */
    function updateAuditionVideo($data, $id) {
        try {
            $this->db->where('id', $id);
            $resp = $this->db->update(Audition_model::$auditionVideoTempTable, $data);
            if ($resp) {
                if($data['status'] == 2) {
                    $this->db->select('*');
                    $this->db->from(Audition_model::$auditionVideoTempTable);
                    $this->db->where('id', $id);
                    $query  = $this->db->get();
                    $result = $query->row_array();

                    $this->db->delete(Audition_model::$auditionVideo, array('userId' => $result['userId'])); 

                    $insertData = array(
                        'userId' => $result['userId'],
                        'auditionFiles' => $result['auditionFiles'],
                        'auditionNumber' => $result['auditionNumber'],
                        'createdAt' => date('Y-m-d H:i:s'),
                        'updatedAt' => date('Y-m-d H:i:s'),
                        'createdBy' => $result['userId'],
                        'updatedBy' => $result['userId'],
                        'status' => 1
                    );

                    $this->db->insert(Audition_model::$auditionVideo, $insertData);
                    
                    return $id;
                } else {
                    return $id;
                }
            } else {
                throw new Exception('Unable to update audition data.');
                return false;
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Discription : Used to update interview status and add in original interview table and remove from tempt table
     * Author : Synergy
     * @param int $id
     * @return int or boolean or string
     */
    function updateInterviewVideo($data, $id) {
        try {
            $this->db->where('id', $id);

            $resp = $this->db->update(Audition_model::$interviewVideoTempTable, $data);
            if ($resp) {
                if($data['status'] == 2) {
                    $this->db->select('*');
                    $this->db->from(Audition_model::$interviewVideoTempTable);
                    $this->db->where('id', $id);
                    $query  = $this->db->get();
                    $result = $query->row_array();

                    $insertData = array(
                        'userId' => $result['userId'],
                        'jobId' => $result['jobId'],
                        'mergedVideo' => $result['mergedVideo'],
                        'jobResume' => $result['jobResume'],
                        'source' => $result['source'],
                        'createdAt' => date('Y-m-d H:i:s'),
                        'updatedAt' => date('Y-m-d H:i:s'),
                        'status' => 1
                    );

                    //print_r($insertData);exit;
                    $this->db->insert('fj_userJob', $insertData);
                    return $id;
                } else {
                    return $id;
                }
            } else {
                return false;
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Discription : Used to get list of questions in an audition set
     * Author : Synergy
     * @param int $id
     * @return array question data or string error message 
     */
    function getQuestionsByAudition($id) {
        try {
            $sql =      " SELECT AQ.*, Q.title, Q.duration "
                    .   " FROM ".Audition_model::$auditionSetQusTable."     AQ "
                    .   " LEFT JOIN ".Audition_model::$auditionQusTable."   Q"
                    .   " ON Q.id=AQ.questionId "
                    .   " WHERE AQ.auditionId = '".$id."'";
            $query = $this->db->query($sql);
            $result = $query->result_array();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Discription : Used to get number of questions
     * Author : Synergy
     * @param int $limit, int $offset
     * @return int number of question or string error message 
     */
    function listFjAuditionQuestionsTotalrecords($limit, $offset, $serachColumn = NULL, $searchText = NULL, $createdBy = NULL) {
        try {
            $currentDate = date('Y-m-d h:i:s');
            $sql    = " SELECT Q.*, U.fullname "
                    . " FROM ".Audition_model::$auditionQusTable."  Q "
                    . " LEFT JOIN ".Audition_model::$userTable."     U "
                    . " ON Q.createdBy=U.id "
                    . " WHERE Q.status != 3 AND Q.type = 1 ";            
            $query          = $this->db->query($sql);
            $total_records  = $query->num_rows();
            return $total_records;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Discription : Used to get list of all question
     * Author : Synergy
     * @param int $limit, int $offset
     * @return array question data or string error message 
     */
    function listFjAuditionQuestions($limit, $offset, $serachColumn = NULL, $searchText = NULL, $createdBy = NULL) {
        try {
            $currentDate = date('Y-m-d h:i:s');
            $sql    = " SELECT Q.*, U.fullname "
                    . " FROM ".Audition_model::$auditionQusTable."  Q "
                    . " LEFT JOIN ".Audition_model::$userTable."     U "
                    . " ON Q.createdBy=U.id "
                    . " WHERE Q.status != 3 AND Q.type = 1 "
                    . " ORDER BY Q.id desc";
            $query          = $this->db->query($sql);            
            $total_records  = $query->num_rows();
            $page           = ($this->uri->segment(3, 0)=='question'?$this->uri->segment(4, 0):0);
            $per_page       = '10';

            $start_limit = ($page) * 1;
            if ($start_limit > 1) {
                $start_limit = $start_limit;
            } else {
                $start_limit = 0;
            }                      
            $sql .= " LIMIT " .$per_page. " OFFSET " .$start_limit."";            

            $queryResult    = $this->db->query($sql);
            $result         = $queryResult->result();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Discription : Used to add question in a specific audition
     * Author : Synergy
     * @param array $data
     * @return int $questionId or boolean false
     */
    function addAuditionQuestion($data) {
        $resp = $this->db->insert(Audition_model::$auditionQusTable, $data);
        $questionId = $this->db->insert_id();
        if ($resp) {
            return $questionId;
        } else {
            return false;
        }
    }
    
    /**
     * Discription : Used to add new audition
     * Author : Synergy
     * @param array $data
     * @return int $auditionId or boolean false
     */
    function addAudition($data) {
        $resp = $this->db->insert(Audition_model::$auditionSetTable, $data);
        $auditionId = $this->db->insert_id();
        if ($resp) {
            return $auditionId;
        } else {
            return false;
        }
    }
    
    /**
     * Discription : Used to add new question in audition set
     * Author : Synergy
     * @param array $data
     * @return int $questionId or boolean false
     */
    function addAuditionSetQuestion($data) {
        $resp = $this->db->insert(Audition_model::$auditionSetQusTable, $data);
        $questionId = $this->db->insert_id();
        if ($resp) {
            return $questionId;
        } else {
            return false;
        }
    }
    
    /**
     * Discription : Used to get details of a specific audition set
     * Author : Synergy
     * @param int $id
     * @return array of data or string error message
     */
    function getAudition($id) {
        try {            
            $this->db->select('*');
            $this->db->from(Audition_model::$auditionSetTable);
            $this->db->where('id', $id);
            $query  = $this->db->get();
            $result = $query->row_array();
            if (count($result) == 0) {
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Discription : Use to get duration of specific question in specific audition set
     * Author : Synergy
     * @param array $data
     * @return array of data or string error message
     */
    function auditionQuestionDuration($data) {
        try {            
            $this->db->select('*');
            $this->db->from(Audition_model::$auditionQusTable);
            $this->db->where('id', $data);
            $query  = $this->db->get();
            $result = $query->row_array();
            if (count($result) == 0) {
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Discription : Use to update an audition set
     * Author : Synergy
     * @param array $data, int $id
     * @return boolean false, int $id or string error message
     */
    function updateAuditionSet($data, $id) {
        try {
            $this->db->where('id', $id);
            $resp = $this->db->update(Audition_model::$auditionSetTable, $data);
            if (!$resp) {
                throw new Exception('Unable to delete audition data.');
                return false;
            }
            return $id;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Discription : Use to update a question in an audition set
     * Author : Synergy
     * @param array $data, int $id
     * @return boolean false, int $id or string error message
     */
    function updateAuditionQuestion($data, $qusId) {
        try {
            $this->db->where('id', $qusId);
            $resp = $this->db->update(Audition_model::$auditionQusTable, $data);
            if (!$resp) {
                throw new Exception('Unable to delete audition data.');
                return false;
            }
            return $id;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Discription : Use to delete a question from an audition set
     * Author : Synergy
     * @param int $id
     * @return boolean or string error message
     */
    function deleteQuestionsById($id) {
        try {
            $this->db->where('id', $id);
            $this->db->where('type', '2');
            $this->db->delete(Audition_model::$auditionQusTable);
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Discription : Use to delete questions with specific audition set
     * Author : Synergy
     * @param int $id
     * @return boolean or string error message
     */
    function deleteQuestionsByAuditionSet($id) {
        try {
            $this->db->where('auditionId', $id);
            $this->db->delete(Audition_model::$auditionSetQusTable);
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Discription : Use to delete a specific question from a specific audition set
     * Author : Synergy
     * @param int $id
     * @return boolean or string error message
     */
    function deleteQuestionInAuditionSet($auditionId, $questionId) {
        try {
            $this->db->where('auditionId', $auditionId);
            $this->db->where('questionId', $questionId);
            $this->db->delete(Audition_model::$auditionSetQusTable);
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Discription : Use to delete all questions from a specific audition set
     * Author : Synergy
     * @param int $auditionId, int $questionId
     * @return boolean or string error message
     */
    function deleteAllQuestionsByAuditionSet($auditionId, $questionId) {
        try {
            $sql = "DELETE FROM `fj_auditionSetQuestions` WHERE `auditionId` = '$auditionId' AND id NOT IN ($questionId)";
            $queryResult    = $this->db->query($sql);
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Discription : Use to get all audition sets
     * Author : Synergy
     * @param none
     * @return boolean flase or array audition data or string error message
     */
    function getAuditionSet() {
        try {            
            $this->db->select('*');
            $this->db->from(Audition_model::$auditionSetTable);
            $this->db->where_in('status', array(1,2));
            $query  = $this->db->get();
            $result = $query->result_array();
            if (count($result) == 0) {
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Discription : Use to check if a question exist in audition set or not
     * Author : Synergy
     * @param int $question, int $audition
     * @return int
     */
    function checkQuestionExistInAudition($question, $audition) {
        $sql = "SELECT * FROM ".Audition_model::$auditionSetQusTable." WHERE auditionId = '" . $audition . "' AND questionId = '" . $question . "'";
        $query = $this->db->query($sql);
        return $query->num_rows();
    }
    
    /**
     * Discription : Use to check if a question exist in audition set or not
     * Author : Synergy
     * @param int $question, int $audition
     * @return int
     */
    function checkQuestionExist($question) {
        $sql = "SELECT * FROM ".Audition_model::$auditionQusTable." WHERE auditionId = '" . $audition . "' AND questionId = '" . $question . "'";
        $query = $this->db->query($sql);
        return $query->num_rows();
    }
    
    /**
     * Discription : Use to get all the audition questions
     * Author : Synergy
     * @param none
     * @return array of data or boolean false or error message
     */
    function getAuditionQuestions() {
        try {            
            $this->db->select('*');
            $this->db->from(Audition_model::$auditionQusTable);
            $this->db->where('status', '1');
            $this->db->where('type', '1');
            $query  = $this->db->get();
            $result = $query->result_array();
            if (count($result) == 0) {
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Discription : Use to check if a question exist in audition set or not
     * Author : Synergy
     * @param none
     * @return int $result or string error message
     */
    function auditionSetQuestionExist($auditionId, $questionId) {
        try {            
            $this->db->select('*');
            $this->db->from(Audition_model::$auditionSetQusTable);
            $this->db->where('questionId', $questionId);
            $this->db->where('auditionId', $auditionId);
            $query  = $this->db->get();
            $result = $query->row_array();
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Discription : Use to get the name of Question by id
     * Author : Synergy
     * @param int $id
     * @return array result data
     */
    function getAuditionQuestionName($id) {
        try {            
            $this->db->select('*');
            $this->db->from(Audition_model::$auditionQusTable);
            $this->db->where('id', $id);
            $query  = $this->db->get();
            $result = $query->result_array();
            if (count($result) == 0) {
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
}

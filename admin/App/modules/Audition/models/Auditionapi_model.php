<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Discussion Model
 * Description : Handle all the CRUD operation for Audition
 * @author Synergy
 * @createddate : Dec 3, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 16, 2017
 */
class auditionapi_model extends CI_Model {
    function addAudition($posts, $files){
        if(coreapi_model::array_keys_exist( $posts, 'userId', 'title', 'fileType')  &&  ($posts['userId']!='' && $posts['title']!='' && $posts['fileType']!='')) {
            $dir_path_video     = "uploads/audition/video/";
            $dir_path_audio     = "uploads/audition/audio/";
            $userId             = coreapi_model::cleanString($posts['userId']);
            $id                 = coreapi_model::getJwtValue($userId);
            $auditionTitle      = coreapi_model::cleanString($posts['title']);
            $auditionFileType   = coreapi_model::cleanString($posts['fileType']);
            $auditionFile       = $files['auditionFile'];
        
            $userRow    = coreapi_model::userExists($id);
            if(count($userRow)>0) {
                if(($auditionFile['error']===0) && ($auditionFile['type']=='video/mp4' || $auditionFile['type']=='audio/mp3')) {
                    $auditionFile['name'] = $id.uniqid().date(YmdHis).($auditionFileType=='1'?'.MP4':'.MP3');
                    $filename       = $auditionFile['name'];
                    $target_path    = ($auditionFileType=='1'?$dir_path_video:$dir_path_audio) . basename($filename);
                    try {
                        if(!move_uploaded_file($auditionFile['tmp_name'], $target_path)) {
                            coreapi_model::codeMessage('500', $this->lang->line('something_wrong'));
                        }
                        else {
                            $this->db->where(array('userId'=>$id))->update('fj_audition', array('status'=>'2'));
                            $auditionData = array(
                                                'userId'        => (int)$id,
                                                'title'         => (string)$auditionTitle,
                                                'file_type'     => (string)$auditionFileType,
                                                'audition_file' => (string)$filename,
                                                'audition_name' => 'audition',
                                                'createdAt'     => date('Y-m-d H:i:s')
                                            );
                            $this->db->insert('fj_audition', $auditionData);
                            $insert_id      = $this->db->insert_id();
                            if($insert_id) {
                                coreapi_model::codeMessage('200', $this->lang->line('success'));
                            }
                            else {
                                coreapi_model::codeMessage('500', $this->lang->line('something_wrong'));
                            } 
                        }
                    } catch (Exception $e) {
                            coreapi_model::codeMessage('500', $e->getMessage());
                    }
                }
                else {
                    coreapi_model::codeMessage('202', $this->lang->line('invalid_audition_file'));
                }
            }       
            else {
                coreapi_model::codeMessage('500', $this->lang->line('invalid_user'));
            }
        }
        else {
            coreapi_model::codeMessage('202', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }
    
    
    function auditionHistory($params){
        $userId = coreapi_model::cleanString($params['userId']);
        $id     = coreapi_model::getJwtValue($userId);
        if($userId!='') {
            $auditionResult = coreapi_model::queryResultArray("SELECT title, file_type, audition_file FROM fj_audition WHERE userId='$id' AND status IN (1,2) ORDER BY status ASC");
            if(count($auditionResult)>0){
                foreach($auditionResult as $auditionRow){
                    $rowData['title']           = (string)$auditionRow['title'];
                    $rowData['file_type']       = (string)($auditionRow['file_type']=='1'?'Video':'Audio');
                    $rowData['audition_file']   = (string)($auditionRow['file_type']=='1'?base_url() . '/uploads/audition/video/'.$auditionRow['audition_file']:base_url() . '/uploads/audition/audio/'.$auditionRow['audition_file']);
                    coreapi_model::$data[]  = $rowData;
                }
                coreapi_model::codeMessage('200', $this->lang->line('success'));
                coreapi_model::$returnArray['data']    = coreapi_model::$data;
            }
            else {
                coreapi_model::codeMessage('500', $this->lang->line('no_audition_found'));
            }
        }
        else {
            coreapi_model::codeMessage('500', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }
    
    
    
    
    
    
    function auditionSet($params){
        $userId     = coreapi_model::cleanString($params['userId']);
        $id         = coreapi_model::getJwtValue($userId);
        $industryId = coreapi_model::cleanString($params['industryId']);
        $functionId = coreapi_model::cleanString($params['functionId']);
        if($userId!='' && $industryId!='' && $functionId!='') {
            $query = "
                    SELECT 
                        aSet.name, aQ.title, aQ.duration, aQ.id, aSet.id AS setId
                    FROM 
                        fj_auditionSet          aSet
                    JOIN 
                        fj_auditionSetQuestions aSetQ,
                        fj_auditionQuestions    aQ
                    WHERE 
                        aSet.status='1'                 AND
                        aSet.id=aSetQ.auditionId        AND
                        aSetQ.questionId=aQ.id          AND
                        aSet.industryId='$industryId'   AND
                        aSet.functionId='$functionId' ";
            $auditionResult = coreapi_model::queryResultArray($query);
            if(count($auditionResult)>0){
                foreach($auditionResult as $auditionRow){
                    $rowData['auditionSetId']   = (string)$auditionRow['setId'];
                    $rowData['auditionSet']     = (string)$auditionRow['name'];
                    coreapi_model::$data[]  = $rowData;
                }
                coreapi_model::codeMessage('200', $this->lang->line('success'));
                coreapi_model::$returnArray['data']    = coreapi_model::$data;
            }
            else {
                coreapi_model::codeMessage('500', $this->lang->line('no_audition_found'));
            }
        }
        else {
            coreapi_model::codeMessage('500', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }
    
    function auditionSetData($params){
        $userId         = coreapi_model::cleanString($params['userId']);
        $id             = coreapi_model::getJwtValue($userId);
        $industryId     = coreapi_model::cleanString($params['industryId']);
        $functionId     = coreapi_model::cleanString($params['functionId']);
        $auditionSetId  = coreapi_model::cleanString($params['setId']);
        if($userId!='') {
            $query = "
                    SELECT 
                        aSet.name, aQ.title, aQ.duration, aQ.id, aSet.id AS setId
                    FROM 
                        fj_auditionSet          aSet
                    JOIN 
                        fj_auditionSetQuestions aSetQ,
                        fj_auditionQuestions    aQ
                    WHERE 
                        aSet.status='1'                 AND
                        aSet.id=aSetQ.auditionId        AND
                        aSetQ.questionId=aQ.id          AND
                        aSet.industryId='$industryId'  AND
                        aSet.functionId='$functionId'  AND
                        aSet.id='$auditionSetId' ";
            $auditionResult = coreapi_model::queryResultArray($query);
            if(count($auditionResult)>0){
                foreach($auditionResult as $auditionRow){
                    $rowData['auditionSetId']   = (string)$auditionRow['setId'];
                    $rowData['auditionSet']     = (string)$auditionRow['name'];
                    $rowData['questionId']      = (string)$auditionRow['id'];
                    $rowData['question']        = (string)$auditionRow['title'];
                    $rowData['duration']        = (string)$auditionRow['duration'];
                    coreapi_model::$data[]  = $rowData;
                }
                coreapi_model::codeMessage('200', $this->lang->line('success'));
                coreapi_model::$returnArray['data']    = coreapi_model::$data;
            }
            else {
                coreapi_model::codeMessage('500', $this->lang->line('no_audition_found'));
            }
        }
        else {
            coreapi_model::codeMessage('500', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }
    
    
    function auditionData($params){
        $userId         = coreapi_model::cleanString($params['userId']);
        $id             = coreapi_model::getJwtValue($userId);
        if($userId!='') {
            $query = "
                    SELECT 
                        aSet.name, aQ.title, aQ.duration, aQ.id, aSet.id AS setId
                    FROM 
                        fj_auditionSet          aSet
                    JOIN 
                        fj_auditionSetQuestions aSetQ,
                        fj_auditionQuestions    aQ
                    WHERE 
                        aSet.status='1'                 AND
                        aSet.id=aSetQ.auditionId        AND
                        aSetQ.questionId=aQ.id          ";
            $auditionResult = coreapi_model::queryResultArray($query);
            if(count($auditionResult)>0){
                foreach($auditionResult as $auditionRow){
                    $rowData['auditionSetId']   = (string)$auditionRow['setId'];
                    $rowData['auditionSet']     = (string)$auditionRow['name'];
                    $rowData['questionId']      = (string)$auditionRow['id'];
                    $rowData['question']        = (string)$auditionRow['title'];
                    $rowData['duration']        = (string)$auditionRow['duration'];
                    coreapi_model::$data[]  = $rowData;
                }
                coreapi_model::codeMessage('200', $this->lang->line('success'));
                coreapi_model::$returnArray['data']    = coreapi_model::$data;
            }
            else {
                coreapi_model::codeMessage('500', $this->lang->line('no_audition_found'));
            }
        }
        else {
            coreapi_model::codeMessage('500', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }
}
<?php

class discussionapi_model extends CI_Model {
    function forumExists($id) {
        $forumRow    = coreapi_model::queryRowArray("SELECT id FROM fj_discussionForum WHERE id='$id' AND status='1' LIMIT 0,1");
        return $forumRow;
    }
    
    function createForum($params){
        $userId = coreapi_model::cleanString($params['userId']);
        $id     = coreapi_model::getJwtValue($userId);
        $topic  = coreapi_model::cleanString($params['topic']);
        if($userId!='' && $topic!='') {
            $userRow    = coreapi_model::userExists($id);
            if(count($userRow)>0){
                $insertData = array(                        
                                    'userId'        => $id,
                                    'description'   => $topic,
                                    'type'          => '1'
                                );
                $this->db->insert('fj_discussionForum', $insertData);
                $insert_id      = $this->db->insert_id();
                if($insert_id) {
                    coreapi_model::codeMessage('200', $this->lang->line('success'));
                }
                else {
                    coreapi_model::codeMessage('500', $this->lang->line('something_wrong'));
                }
            }
            else {
                coreapi_model::codeMessage('500', $this->lang->line('invalid_user'));
            }
        }
        else {
            coreapi_model::codeMessage('500', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }
    
    
    function getForumList($params){
        // pagination pending
        $forumResult   = coreapi_model::queryResultArray("
                                                            SELECT 
                                                                F.*,
                                                                U.image,
                                                                U.fullname
                                                            FROM 
                                                                fj_discussionForum F
                                                            JOIN
                                                                fj_users   U
                                                            WHERE
                                                                F.userId=U.id   AND  
                                                                F.status='1'    AND 
                                                                F.type='1' 
                                                            ORDER BY
                                                                createdAt DESC
                                                            ");
        if(count($forumResult)>0) {
            foreach($forumResult as $forumRow) {
                // Forum Details
                $rowData['forumId']     = (string)$forumRow['id'];
                $rowData['topic']       = (string)ucwords(coreapi_model::limit_text($forumRow['description'], 20));
                $rowData['username']    = (string)ucwords($forumRow['fullname']);
                $rowData['thumbnail']   = (string)($forumRow['image']!=''?base_url() . '/uploads/userImages/' . $forumRow['image'].".PNG":'');
                coreapi_model::$data[]  = $rowData;
            }
            coreapi_model::codeMessage('200', $this->lang->line('success'));
            coreapi_model::$returnArray['data']    = coreapi_model::$data;                
        }
        else {
            coreapi_model::codeMessage('500', $this->lang->line('no_forum_found'));
        }
        return coreapi_model::$returnArray;
    }
    
    
    function getForumDetail($params){
        $forumId    = coreapi_model::cleanString($params['forumId']);
        if($forumId!='') {
            $forumResult   = coreapi_model::queryResultArray("
                                                        SELECT 
                                                            F.*,
                                                            U.image,
                                                            U.fullname
                                                        FROM 
                                                            fj_discussionForum F
                                                        JOIN
                                                            fj_users   U
                                                        WHERE
                                                            F.userId=U.id   AND  
                                                            F.status='1'    AND 
                                                            (F.id='$forumId'       OR  F.discussionReplyId='$forumId')
                                                        ORDER BY
                                                            F.createdAt ASC
                                                    ");
            if(count($forumResult)>0) {
                foreach($forumResult as $forumRow) {
                    // Forum Details
                    $rowData['forumId']     = (string)$forumRow['id'];
                    $rowData['topic']       = (string)ucwords($forumRow['description']);
                    $rowData['username']    = (string)ucwords($forumRow['fullname']);
                    $rowData['thumbnail']   = (string)($forumRow['image']!=''?base_url() . '/uploads/userImages/' . $forumRow['image'].".PNG":'');
                    coreapi_model::$data[]  = $rowData;
                }         
                coreapi_model::codeMessage('200', $this->lang->line('success'));
                coreapi_model::$returnArray['data']    = coreapi_model::$data;                
            }
            else {
                coreapi_model::codeMessage('500', $this->lang->line('invalid_forum'));
            }
        }
        else {
            coreapi_model::codeMessage('500', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }
    
    
    function replyForum($params){
        $userId     = coreapi_model::cleanString($params['userId']);
        $id         = coreapi_model::getJwtValue($userId);
        $forumId    = coreapi_model::cleanString($params['forumId']);
        $message    = coreapi_model::cleanString($params['message']);
        if($userId!='' && $forumId!='' && $message!='') {
            $userRow    = coreapi_model::userExists($id);
            if(count($userRow)>0){
                $forumRow    = $this->forumExists($id);
                if(count($forumRow)>0){
                    $insertData = array(                        
                                        'userId'            => $id,
                                        'description'       => $message,
                                        'type'              => '2',
                                        'discussionReplyId' => $forumId
                                    );
                    $this->db->insert('fj_discussionForum', $insertData);
                    $insert_id      = $this->db->insert_id();
                    if($insert_id) {
                        coreapi_model::codeMessage('200', $this->lang->line('success'));
                    }
                    else {
                        coreapi_model::codeMessage('500', $this->lang->line('something_wrong'));
                    }
                }
                else {
                    coreapi_model::codeMessage('500', $this->lang->line('invalid_user'));
                }
            }
            else {
                coreapi_model::codeMessage('500', $this->lang->line('invalid_user'));
            }
        }
        else {
            coreapi_model::codeMessage('500', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }
    
}


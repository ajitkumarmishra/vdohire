<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Analytics Controller
 * Description : Used to handle all analytics related data
 * @author Synergy
 * @createddate : Nov 2, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 15, 2017
 */

class Audition extends MY_Controller {

    /**
     * Responsable for auto load the the audition_model,industry_model
     * Responsable for auto load the the form_validation,session,email library
     * Responsable for auto load fj helper
     * Check Uri Segment to display specific page role wise
     * @return void
     */
    function __construct() {
        parent::__construct();
        $this->load->model('audition_model');
        $this->load->model('industry_model');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('email');
        $this->load->helper('fj');
        $token = $this->config->item('accessToken');
        if (isset($_POST) && $_POST != NULL) {
            if ($token != $_POST['accessToken']) {
                //echo 'You do not have pemission.';
                //exit;
            }
        }

        /**********Check any type of fraund activity*******/
        if ($this->session->userdata('logged_in')) {
            $userRealData   = $this->session->userdata['logged_in'];
            $uriStringSegment = $this->uri->uri_string();
            $arraySegments = explode('/', $uriStringSegment);
            array_pop($arraySegments);
            $newString = implode('/', $arraySegments);
            
            if($userRealData->role == 1) {
                $portal = 'SuperAdminPortal';
            }
            else{
                $portal = '';
            }

            if($uriStringSegment == 'audition/list') {
                if(!checkUserAccessForWebPage($userRealData->id, $userRealData->role, $portal, 'audition/list')) {
                    $this->session->sess_destroy();
                    $insertFaultData['userId'] = $userRealData->id;
                    $insertFaultData['comment'] = 'This user try to access this this page';
                    $insertFaultData['pagePath'] = 'audition/list';
                    $this->db->insert('fj_user_access_log', $insertFaultData);
                    redirect(base_url('users/login'));
                }
            }
            elseif($uriStringSegment == 'audition/addAudition') {
                if(!checkUserAccessForWebPage($userRealData->id, $userRealData->role, $portal, 'audition/addAudition')) {
                    $this->session->sess_destroy();
                    $insertFaultData['userId'] = $userRealData->id;
                    $insertFaultData['comment'] = 'This user try to access this this page';
                    $insertFaultData['pagePath'] = 'audition/addAudition';
                    $this->db->insert('fj_user_access_log', $insertFaultData);
                    redirect(base_url('users/login'));
                }
            }
            elseif($newString == 'audition/edit') {
                if(!checkUserAccessForWebPage($userRealData->id, $userRealData->role, $portal, 'audition/edit')) {
                    $this->session->sess_destroy();
                    $insertFaultData['userId'] = $userRealData->id;
                    $insertFaultData['comment'] = 'This user try to access this this page';
                    $insertFaultData['pagePath'] = 'audition/edit';
                    $this->db->insert('fj_user_access_log', $insertFaultData);
                    redirect(base_url('users/login'));
                }
            }
        } 
        /****************End of handle fraud activity****************/
    }
    
    /**
     * Description : List all the auditions sets created by specific user
     * Author : Synergy
     * @param int $page, array $_POST
     * @return array of data or error message
     */
    function listAudition($page) {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        //Auditions Listing Start
        $userData       = $this->session->userdata['logged_in'];
        $data['page']   = $page;
        $resp['error']  = 0;
        $serachColumn   = $this->input->post('serachColumn', true);
        $searchText     = $this->input->post('serachText', true);
        $token          = $this->config->item('accessToken');
        $fromEmail      = $this->config->item('fromEmail');
        $itemsPerPage   = $this->config->item('itemsPerPage');
        $createdBy      = $userData->id;
        
        if (!$page) {
            $page   = 0;
        } else {
            $page   = $page - 1;
        }        
        $data['itemsPerPage']   = $itemsPerPage;
        $offset                 = $page * $itemsPerPage;
        try {
            $result = $this->audition_model->listAuditions($itemsPerPage, $offset, $serachColumn, $searchText, $createdBy);
            $i = 0;
            foreach ($result as $item) {
                $questionByAudition         = $this->audition_model->getQuestionsByAudition($item->id);
                $result[$i]->questionCount  = count($questionByAudition);
                $i++;
            }
            $total_records              = $this->audition_model->listAuditionsTotalrecords($itemsPerPage, $offset, $serachColumn, $searchText, $createdBy);            
            $config1["base_url"]        = base_url() . $this->uri->segment(1). "/" . $this->uri->segment(2) . "/set/";
            $config1["total_rows"]      = $total_records;
            $config1["per_page"]        = 10;
            $config1['uri_segment']     = 4;
            $config1['full_tag_open']   = '<ul class="pagination">';
            $config1['full_tag_close']  = '</ul>';
            $config1['first_link']      = false;
            $config1['last_link']       = false;
            $config1['first_tag_open']  = '<li>';
            $config1['first_tag_close'] = '</li>';
            $config1['prev_link']       = '&laquo';
            $config1['prev_tag_open']   = '<li class="prev">';
            $config1['prev_tag_close']  = '</li>';
            $config1['next_link']       = '&raquo';
            $config1['next_tag_open']   = '<li>';
            $config1['next_tag_close']  = '</li>';
            $config1['last_tag_open']   = '<li>';
            $config1['last_tag_close']  = '</li>';
            $config1['cur_tag_open']    = ($this->uri->segment(3, 0)=='set'?'<li class="active"><a href="#">':'<li><a href="#">');
            $config1['cur_tag_close']   = '</a></li>';
            $config1['num_tag_open']    = '<li>';
            $config1['num_tag_close']   = '</li>';            
            $this->pagination->initialize($config1);
            $data['pagination']         = $this->pagination->create_links();
            $count                          = ceil(count($completeResult) / $itemsPerPage);
            $data['count']                  = $count;
            $data['totalCount']             = count($completeResult);
            $data['contentAuditionSet']     = $result;
            $data['main_content']           = 'fj-audition-listing';
            $data['searchText']             = $searchText;
            $data['serachColumn']           = $serachColumn;
            $data['token']                  = $token;

            //List Fj Questions
            $resultFjQuestions          = $this->audition_model->listFjAuditionQuestions($itemsPerPage, $offset, $serachColumn, $searchText, $createdBy);            
            $total_records_qus          = $this->audition_model->listFjAuditionQuestionsTotalrecords($itemsPerPage, $offset, $serachColumn, $searchText, $createdBy);            
            $config["base_url"]         = base_url() . $this->uri->segment(1). "/" . $this->uri->segment(2) . "/question/";
            $config["total_rows"]       = $total_records_qus;
            $config["per_page"]         = 10;
            $config['uri_segment']      = 4;
            $config['full_tag_open']    = '<ul class="pagination">';
            $config['full_tag_close']   = '</ul>';
            $config['first_link']       = false;
            $config['last_link']        = false;
            $config['first_tag_open']   = '<li>';
            $config['first_tag_close']  = '</li>';
            $config['prev_link']        = '&laquo';
            $config['prev_tag_open']    = '<li class="prev">';
            $config['prev_tag_close']   = '</li>';
            $config['next_link']        = '&raquo';
            $config['next_tag_open']    = '<li>';
            $config['next_tag_close']   = '</li>';
            $config['last_tag_open']    = '<li>';
            $config['last_tag_close']   = '</li>';
            $config['cur_tag_open']    = ($this->uri->segment(3, 0)=='question'?'<li class="active"><a href="#">':'<li><a href="#">');
            $config['cur_tag_close']    = '</a></li>';
            $config['num_tag_open']     = '<li>';
            $config['num_tag_close']    = '</li>';            
            $this->pagination->initialize($config);
            $data['paginationQus']      = $this->pagination->create_links();
            
            $countFj                        = ceil(count($completeResultFjQuestions) / $itemsPerPage);
            $data['countFj']                = $countFj;
            $data['totalCountFj']           = count($completeResultFjQuestions);
            $data['contentFjQuestions']     = $resultFjQuestions;
            //List Fj Questions
            $this->load->view('fj-mainpage', $data);
            if (!$result) {
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * Description : Used to approve audition video
     * Author : Synergy
     * @param array $_POST
     * @return string the success message | error message.
     */
    function approveVideo($page = '') {

        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }

        $userData       = $this->session->userdata['logged_in'];
        $data['page']   = $page;
        $resp['error']  = 0;
        $serachColumn   = $this->input->post('serachColumn', true);
        $searchText     = $this->input->post('serachText', true);
        $token          = $this->config->item('accessToken');
        $fromEmail      = $this->config->item('fromEmail');
        //$itemsPerPage   = $this->config->item('itemsPerPage');
        $itemsPerPage   = 200;
        $createdBy      = $userData->id;
        
        if (!$page) {
            $page   = 0;
        } else {
            $page   = $page - 1;
        }        
        $data['itemsPerPage']   = $itemsPerPage;
        $offset                 = $page * $itemsPerPage;
        try {

            //Listing of Audition Video
            $result = $this->audition_model->listAuditionTempVideos($itemsPerPage, $offset);
            //print_r($result);exit;

            $total_records              = $this->audition_model->listAuditionTempVideosTotalRecords($itemsPerPage, $offset);            
            $config1["base_url"]        = base_url() . $this->uri->segment(1). "/" . $this->uri->segment(2) . "/";
            $config1["total_rows"]      = $total_records;
            $config1["per_page"]        = 200;
            $config1['uri_segment']     = 4;
            $config1['full_tag_open']   = '<ul class="pagination">';
            $config1['full_tag_close']  = '</ul>';
            $config1['first_link']      = false;
            $config1['last_link']       = false;
            $config1['first_tag_open']  = '<li>';
            $config1['first_tag_close'] = '</li>';
            $config1['prev_link']       = '&laquo';
            $config1['prev_tag_open']   = '<li class="prev">';
            $config1['prev_tag_close']  = '</li>';
            $config1['next_link']       = '&raquo';
            $config1['next_tag_open']   = '<li>';
            $config1['next_tag_close']  = '</li>';
            $config1['last_tag_open']   = '<li>';
            $config1['last_tag_close']  = '</li>';
            $config1['cur_tag_open']    = ($this->uri->segment(3, 0)=='set'?'<li class="active"><a href="#">':'<li><a href="#">');
            $config1['cur_tag_close']   = '</a></li>';
            $config1['num_tag_open']    = '<li>';
            $config1['num_tag_close']   = '</li>';            
            $this->pagination->initialize($config1);
            $data['pagination']         = $this->pagination->create_links();
            
            //$completeResult = $this->audition_model->listAuditions(NULL, NULL, NULL, NULL, $createdBy);
            $count                          = ceil(count($result) / $itemsPerPage);
            $data['count']                  = $count;
            $data['totalCount']             = count($result);
            
            $data['contentAuditionSet']     = $result;
            $data['main_content']           = 'fj-approve-video-listing';
            $data['searchText']             = $searchText;
            $data['serachColumn']           = $serachColumn;
            $data['token']                  = $token;
            
            //Listing of Interview video

            //List Fj Questions
            $resultFjQuestions          = $this->audition_model->listInterviewTempVideos($itemsPerPage, $offset, $serachColumn, $searchText, $createdBy);            
            $total_records_qus          = $this->audition_model->listInterviewTempVideosTotalRecords($itemsPerPage, $offset, $serachColumn, $searchText, $createdBy);            
            $config["base_url"]         = base_url() . $this->uri->segment(1). "/" . $this->uri->segment(2) . "/";
            $config["total_rows"]       = $total_records_qus;
            $config["per_page"]         = 200;
            //$page                       = $this->uri->segment(4, 0);
            $config['uri_segment']      = 4;
            $config['full_tag_open']    = '<ul class="pagination">';
            $config['full_tag_close']   = '</ul>';
            $config['first_link']       = false;
            $config['last_link']        = false;
            $config['first_tag_open']   = '<li>';
            $config['first_tag_close']  = '</li>';
            $config['prev_link']        = '&laquo';
            $config['prev_tag_open']    = '<li class="prev">';
            $config['prev_tag_close']   = '</li>';
            $config['next_link']        = '&raquo';
            $config['next_tag_open']    = '<li>';
            $config['next_tag_close']   = '</li>';
            $config['last_tag_open']    = '<li>';
            $config['last_tag_close']   = '</li>';
            //$config['cur_tag_open']     = '<li class="active"><a href="#">';
            $config['cur_tag_open']    = ($this->uri->segment(3, 0)=='question'?'<li class="active"><a href="#">':'<li><a href="#">');
            $config['cur_tag_close']    = '</a></li>';
            $config['num_tag_open']     = '<li>';
            $config['num_tag_close']    = '</li>';            
            $this->pagination->initialize($config);
            $data['paginationQus']      = $this->pagination->create_links();
            
            $countFj                        = ceil(count($resultFjQuestions) / $itemsPerPage);
            $data['countFj']                = $countFj;
            $data['totalCountFj']           = count($resultFjQuestions);
            $data['contentFjQuestions']     = $resultFjQuestions;
            
            $this->load->view('fj-mainpage', $data);
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Description : Used to update audition status and add in original audition table and remove from tempt table
     * Author : Synergy
     * @param array $_POST
     * @return string the success message | error message.
     */
    function updateAuditionVideo() {
        $error          = 0;
        $token          = $this->config->item('accessToken');
        $auditionId    = $this->input->post('id', true);
        $type    = $this->input->post('type', true);
        if (!$auditionId) {
            echo $error = 'Undefined Audition!';
            exit;
        }
        if (!$error) {
            if($type == 'approve')
                $data['status'] = 2;
            else
                $data['status'] = 3;
            $resp = $this->audition_model->updateAuditionVideo($data, $auditionId);
            if ($resp) {
                echo "Updated successfully";
            }
        }
    }

    /**
     * Description : Used to update interview status and add in original interview table and remove from tempt table
     * Author : Synergy
     * @param array $_POST
     * @return string the success message | error message.
     */
    function updateInterviewVideo() {
        $error          = 0;
        $token          = $this->config->item('accessToken');
        $interviewId    = $this->input->post('id', true);
        $type    = $this->input->post('type', true);
        if (!$interviewId) {
            echo $error = 'Undefined Interview!';
            exit;
        }
        if (!$error) {
            if($type == 'approve')
                $data['status'] = 2;
            else
                $data['status'] = 3;
            $resp = $this->audition_model->updateInterviewVideo($data, $interviewId);
            if ($resp) {
                echo "Updated successfully";
            }
        }
    }

    /**
     * Description : Used to add question for audition
     * Author : Synergy
     * @param array $_POST
     * @return string the success message | error message.
     */
    function addAuditionQuestion() {
        if (!$this->session->userdata('logged_in')) {
            //redirect(base_url('users/login'));
        }
        $token      = $this->config->item('accessToken');
        $userData   = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            //redirect(base_url('users/login'));
            $userId = 1;
        }
        $info['main_content']   = 'fj-create-audition-question';
        $info['token']          = $token;

        if (isset($_POST) && $_POST != NULL) {
            $questions          = $this->input->post('question', true);
            $fjadminqus         = $this->input->post('fjadminqus');
            $info['questions']  = $questions;
            if ($this->form_validation->run() == false) {
                $resp['error']  = validation_errors();
            }
            if ($questions[0]['title'] == '') {
                $resp['error'] .= '<p>Title field of first question is required.</p>';
            }
            if ($questions[0]['duration'] == '') {
                $resp['error'] .= '<p>Answer Duration field of first question is required.</p>';
            }
            if (!$resp['error']) {
                $data['createdAt']  = date('Y-m-d h:i:s');
                $data['updatedAt']  = date('Y-m-d h:i:s');
                $data['createdBy']  = $userId;
                try {
                    $f = 0;
                    if (count($questions) > 0) {
                        foreach ($questions as $item) {
                            if($item['title']!='' && $item['duration']!='') {
                                $item['createdAt']  = date('Y-m-d h:i:s');
                                $item['updatedAt']  = date('Y-m-d h:i:s');
                                $item['createdBy']  = $userId;                                
                                $item['type']       = ($fjadminqus?'1':'2');
                                $questionId         = $this->audition_model->addAuditionQuestion($item);
                            }
                            $f++;
                        }
                    }
                    redirect(base_url('audition/list/1'));
                } catch (Exception $e) {
                    $resp['error'] = $e->getMessage();
                }
            } else {
                $info['error']      = $resp;
                $info['audition']   = $this->input->post();
                $this->load->view('fj-mainpage', $info);
            }
        } 
        else {
            $this->load->view('fj-mainpage', $info);
        }
    }
    
    /**
     * Description : Used to get questin bank for Audition
     * Author : Synergy
     * @param array $_POST
     * @return string
     */
    function getAuditionQuestionBankRow() {
        $index              = $this->input->post('index', true);
        $str                = getNewAuditionQuestionBankRow($index);
        echo $str;
        exit;
    }
    
    /**
     * Description : Used to get question for Audition
     * Author : Synergy
     * @param array $_POST
     * @return string
     */
    function getAuditionQuestionRow() {
        $index              = $this->input->post('index', true);
        $auditionQuestion   = $this->audition_model->getAuditionQuestions(); 
        $str                = getNewAuditionQuestionRow($fjQuestion, $index, $auditionQuestion);
        echo $str;
        exit;
    }
    
    /**
     * Description : Used to add audition set
     * Author : Synergy
     * @param array $_POST
     * @return string
     */
    function addAudition() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token      = $this->config->item('accessToken');
        $userData   = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        }

        $info['auditionQuestion']   = $this->audition_model->getAuditionQuestions();
        $info['ncIndustry']         = $this->industry_model->getIndustryList();
        $info['main_content']       = 'fj-create-audition';
        $info['token']              = $token;

        if (isset($_POST) && $_POST != NULL) {
            $this->form_validation->set_rules('name', 'Set Name', 'trim|required');
            $questions      = $this->input->post('question', true);
            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }
            if ($questions[0]['title'] == '') {
                $resp['error'] .= '<p>Title field of first question is required.</p>';
            }
            if ($questions[0]['duration'] == '') {
                $resp['error'] .= '<p>Answer Duration field of first question is required.</p>';
            }
            if (!$resp['error']) {
                $data['name']       = $this->input->post('name', true);                                    
                $data['createdAt']  = date('Y-m-d h:i:s');
                $data['updatedAt']  = date('Y-m-d h:i:s');
                $data['createdBy']  = $userId;
                $data['updatedBy']  = 0;
                $data['status']     = 2;
                try {
                    $activeAuditions = ($this->audition_model->getAuditionSet());
                    
                    $resp = $this->audition_model->addAudition($data);
                    if ($resp) {
                        $f = 0; 
                        if (count($questions) > 0) {
                            foreach ($questions as $item) {
                                if($item['title']!='' && $item['duration']!='') {
                                    
                                    if(is_numeric($item['title']) && (int)($item['title'])!=0) {
                                        $qusDuration = $this->audition_model->auditionQuestionDuration($item['title']);
                                        
                                        if($item['duration']==$qusDuration['duration']){
                                            $auditionQuestion['auditionId']     = $resp;
                                            $auditionQuestion['questionId']     = $item['title'];
                                            $auditionQuestion['createdAt']      = date('Y-m-d h:i:s');
                                            $this->audition_model->addAuditionSetQuestion($auditionQuestion);
                                        }
                                        else {
                                            $item['title']      = $qusDuration['title'];
                                            $item['duration']   = $item['duration'];
                                            $item['createdAt']  = date('Y-m-d h:i:s');
                                            $item['createdBy']  = $userId;
                                            $questionId         = $this->audition_model->addAuditionQuestion($item);
                                            $auditionQuestion['auditionId']     = $resp;
                                            $auditionQuestion['questionId']     = $questionId;
                                            $auditionQuestion['createdAt']      = date('Y-m-d h:i:s');
                                            $this->audition_model->addAuditionSetQuestion($auditionQuestion);
                                        }
                                    }
                                    else {  
                                        $item['title']      = $item['title'];
                                        $item['createdAt']  = date('Y-m-d h:i:s');
                                        $item['createdBy']  = $userId;
                                        $questionId         = $this->audition_model->addAuditionQuestion($item);
                                        $auditionQuestion['auditionId']     = $resp;
                                        $auditionQuestion['questionId']     = $questionId;
                                        $auditionQuestion['createdAt']      = date('Y-m-d h:i:s');
                                        $this->audition_model->addAuditionSetQuestion($auditionQuestion);
                                    }
                                }
                                $f++;
                            }
                            
                        }
                        $info['message'] = 'Successfully created!';
                        redirect(base_url('audition/view/'.$resp));
                        $this->load->view('fj-mainpage', $info);
                    }
                } catch (Exception $e) {
                    $resp['error'] = $e->getMessage();
                }
            } else {
                $info['error']      = $resp;
                $info['audition']  = $this->input->post();
                $this->load->view('fj-mainpage', $info);
            }
        } 
        else {
            $this->load->view('fj-mainpage', $info);
        }
    }

    /**
     * Description : Used to get specific audition set details
     * Author : Synergy
     * @param int $id
     * @return void
     */
    function getAudition($id) {
        $audition                   = $this->audition_model->getAudition($id);
        $data['ncIndustry']         = $this->industry_model->getIndustryList();
        $data['industryFunction']   = $this->industry_model->getListOfFunction($audition['industryId']);
        $data['questions']          = $this->audition_model->getQuestionsByAudition($id);
        $token                      = $this->config->item('accessToken');
        $data['main_content']       = 'fj-view-audition';
        $data['audition']           = $audition;
        $data['token']              = $token;
        $this->load->view('fj-mainpage', $data);
    }
    
    /**
     * Description : Used to delete specific audition set details
     * Author : Synergy
     * @param int $id
     * @return string success or failure
     */
    function deleteAuditionSet() {
        $error          = 0;
        $token          = $this->config->item('accessToken');
        $fromEmail      = $this->config->item('fromEmail');
        $auditionId    = $this->input->post('id', true);
        if (!$auditionId) {
            echo $error = 'Undefined Audition!';
            exit;
        }
        if (!$error) {
            $data['status'] = 3;
            $resp = $this->audition_model->updateAuditionSet($data, $auditionId);
            if ($resp) {
                echo "Deleted successfully";
            }
        }
    }
    
    /**
     * Description : Used to edit specific audition set details
     * Author : Synergy
     * @param int $id, array $_POST
     * @return array of data or error message
     */
    function editAudition($id) {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token      = $this->config->item('accessToken');
        $userData   = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            $userId = 1;
        }

        $audition               = $this->audition_model->getAudition($id);
        $info['main_content']   = 'fj-edit-audition';
        $info['token']          = $token;
        $info['audition']       = $audition;
        $info['questions']          = $this->audition_model->getQuestionsByAudition($id);
        $info['auditionQuestion']   = $this->audition_model->getAuditionQuestions(); 
        
        if (isset($_POST) && $_POST != NULL) {
            $this->form_validation->set_rules('name',           'Set Name',         'trim|required');          
            $questions      = $this->input->post('question', true);
            $itemId         = $this->input->post('itemId', true);
            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }
            if ($questions[0]['title'] == '') {
                $resp['error'] .= '<p>Title field of first question is required.</p>';
            }
            if ($questions[0]['duration'] == '') {
                $resp['error'] .= '<p>Answer Duration field of first question is required.</p>';
            }

            if (!$resp['error']) {
                $data['name']       = $this->input->post('name', true);
                $data['createdAt']  = date('Y-m-d h:i:s');
                $data['updatedAt']  = date('Y-m-d h:i:s');
                $data['createdBy']  = $userId;
                try {
                    $resp = $this->audition_model->updateAuditionSet($data, $id);
                    if ($resp) {
                        $f = 0;
                        $auditionQuestions = $this->audition_model->getQuestionsByAudition($id);                        
                        
                        if (count($questions) > 0) {
                            $auditionSetQuestions = array();
                            
                            foreach ($questions as $key=>$item) {
                                if($item['title']!='' && $item['duration']!='') {
                                    
                                    if(is_numeric($item['title']) && (int)($item['title'])!=0) {
                                        $qusDuration = $this->audition_model->auditionQuestionDuration($item['title']);
                                        
                                        // if type 2 update duration
                                        if($qusDuration['type']=='2') {
                                            $qusId              = $item['title'];
                                            $item['title']      = $qusDuration['title'];
                                            $item['updatedAt']  = date('Y-m-d h:i:s');
                                            $item['updatedBy']  = $userId;     
                                            $questionId         = $this->audition_model->updateAuditionQuestion($item, $qusId);
                                            $auditionSetQuestions[] = $itemId[$f];
                                        }
                                        
                                        // if type 1 & duration changes then add new qus & remove previous one
                                        else if($qusDuration['type']=='1' && $item['duration']!=$qusDuration['duration']){
                                            $this->audition_model->deleteQuestionInAuditionSet($id, $item['title']);                                            
                                            $item['title']      = $qusDuration['title'];
                                            $item['createdAt']  = date('Y-m-d h:i:s');
                                            $item['createdBy']  = $userId;
                                            $questionId         = $this->audition_model->addAuditionQuestion($item);

                                            $auditionQuestion['auditionId']     = $resp;
                                            $auditionQuestion['questionId']     = $questionId;
                                            $auditionQuestion['createdAt']      = date('Y-m-d h:i:s');

                                            if(($key = array_search($item['title'], $itemId)) !== false) {
                                                unset($itemId[$key]);
                                            }
                                            
                                            $insertionId            = $this->audition_model->addAuditionSetQuestion($auditionQuestion);
                                            $auditionSetQuestions[] = $insertionId; 
                                        }
                                        
                                        // if type 1 same qus and same duration
                                        else if($qusDuration['type']=='1' && $item['duration']==$qusDuration['duration']){

                                            $auditionQuestion['auditionId']     = $resp;
                                            $auditionQuestion['questionId']     = $item['title'];
                                            $auditionQuestion['createdAt']      = date('Y-m-d h:i:s');
                                            $chkRecord                          = $this->audition_model->auditionSetQuestionExist($resp, $item['title']);

                                            if(count($chkRecord)=='0'){
                                                $insertionId            = $this->audition_model->addAuditionSetQuestion($auditionQuestion);
                                                $auditionSetQuestions[] = $insertionId;
                                            }
                                            else if(count($chkRecord)>0){
                                                $auditionSetQuestions[] = $itemId[$f];
                                            }
                                        }                                    
                                        else {  
                                            $item['title']      = $item['title'];
                                            $item['createdAt']  = date('Y-m-d h:i:s');
                                            $item['createdBy']  = $userId;
                                            $questionId         = $this->audition_model->addAuditionQuestion($item);
                                            $auditionQuestion['auditionId']     = $resp;
                                            $auditionQuestion['questionId']     = $questionId;
                                            $auditionQuestion['createdAt']      = date('Y-m-d h:i:s');
                                            $insertionId = $this->audition_model->addAuditionSetQuestion($auditionQuestion);
                                            $auditionSetQuestions[] = $insertionId; 
                                        }
                                    }                                    
                                    else {  
                                        $item['title']      = $item['title'];
                                        $item['createdAt']  = date('Y-m-d h:i:s');
                                        $item['createdBy']  = $userId;
                                        $questionId         = $this->audition_model->addAuditionQuestion($item);
                                        $auditionQuestion['auditionId']     = $resp;
                                        $auditionQuestion['questionId']     = $questionId;
                                        $auditionQuestion['createdAt']      = date('Y-m-d h:i:s');
                                        $insertionId = $this->audition_model->addAuditionSetQuestion($auditionQuestion);
                                        $auditionSetQuestions[] = $insertionId; 
                                    }
                                } 
                                $f++;
                            }
                            $auditionSetQuestionsList = "'".join("','",array_filter($auditionSetQuestions))."'";
                            $this->audition_model->deleteAllQuestionsByAuditionSet($id, $auditionSetQuestionsList);
                        }
                        $info['message'] = 'Successfully created!';
                        redirect(base_url('audition/list/1'));
                        $this->load->view('fj-mainpage', $info);
                    }
                } catch (Exception $e) {
                    $resp['error'] = $e->getMessage();
                }
            } else {
                $info['error'] = $resp;
                $info['audition'] = $this->input->post();
                $this->load->view('fj-mainpage', $info);
            }
        } 
        else {
            $this->load->view('fj-mainpage', $info);
        }
    }
    
    /**
     * Description : Used to check if a question exist in specific audition or not
     * Author : Synergy
     * @param array $_POST
     * @return boolean
     */
    function checkAudition() {
        $questionId = $this->input->post('questionId', true);
        $audition   = $this->input->post('audition', true);
        $check      = 0;
        foreach ($audition as $item) {
            if ($this->audition_model->checkQuestionExistInAudition($questionId, $item)) {
                $check = 1;
            }
        }
        echo $check;
        exit;
    }
    
    /**
     * Description : Used to add question to audition set
     * Author : Synergy
     * @param array $_POST
     * @return boolean
     */
    function addQuestionToAuditionSet() {
        $questionId = $this->input->post('questionId', true);
        $audition   = $this->input->post('auditionSet', true);
        try {
            foreach ($audition as $item) {
                $auditionQuestion['auditionId'] = $item;
                $auditionQuestion['questionId'] = $questionId;
                $auditionQuestion['createdAt'] = date('Y-m-d h:i:s');
                $auditionQuestion['updatedAt'] = date('Y-m-d h:i:s');
                $this->audition_model->addAuditionSetQuestion($auditionQuestion);
                redirect(base_url('audition/list/1'));
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
     * Description : Used to get industry function in an dropdown
     * Author : Synergy
     * @param array $_POST
     * @return string(dropdown with options)
     */
    function getIndustryFunction() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token = $this->config->item('accessToken');
        $userData = $this->session->userdata['logged_in'];
       
        $resp['error'] = 0;
        $indestryId = $this->input->post('industryId', true);
       
        if (!$indestryId) {
            $error = "NotDone";
            echo $error;
            exit();
        }
        if (!$resp['error']) {
            $allIndustryFunctionRes = $this->industry_model->getListOfFunction($indestryId);
            
            if( $allIndustryFunctionRes ){
                $stringquery = "<select class=\"selectpicker\" name=\"functionId\" id=\"functionId\"><option value=\"\">Select Function</option>";
                foreach ($allIndustryFunctionRes as $item){
                    $stringquery .= "<option value=\"".$item->id."\">".$item->name."</option>";
                }
                $stringquery .="</select>";
            } else {
                $stringquery = "<select class=\"selectpicker\" name=\"functionId\" id=\"functionId\"><option value=\"\">Select Function</option>";
                $stringquery .="</select>";
            }
            
            echo $stringquery;
            exit();
        }
    }

    /**
     * Description : Used to get audition set in a dropdown box
     * Author : Synergy
     * @param array none
     * @return string(dropdown with options)
     */
    function getAuditionSet() {
        $userData   = $this->session->userdata['logged_in'];
        $userId     = $userData->id;
        $questionId = $this->input->post('questionId', true); //die;
        $results    = $this->audition_model->getAuditionSet();
        $str        = '<select data-placeholder="Audition Sets" style="width: 350px; display: none;" multiple="" class="chosen-select required" tabindex="-1" name="auditionSet[]" id="auditionSetDropDown">';
        foreach ($results as $item) {
            $str   .= '<option value="' . $item['id'] . '">' . $item['name'] . '</option>';
        }
        $str .= '</select>';
        $str .= '<input type="hidden" name ="questionId" class= "question-id" value="' . $questionId . '" />';
        echo $str;
        exit;
    }

    /**
     * Description : Used to update audition set
     * Author : Synergy
     * @param none
     * @return string
     */
    function changeActiveAudition() {
        $error  = 0;
        $token  = $this->config->item('accessToken');
        $auditionId  = (int)$this->uri->segment(3);
        if($auditionId==0 && $auditionId=='') {
            echo $error = 'Undefined Audition!';
            exit;
        }
        if (!$error) {
            $activeAuditions = ($this->audition_model->getAuditionSet());
            foreach($activeAuditions as $auditionRow) {
                $data['status'] = 2;
                $resp = $this->audition_model->updateAuditionSet($data, $auditionRow['id']);
            }
            $data['status'] = 1;
            $resp = $this->audition_model->updateAuditionSet($data, $auditionId);
            if ($resp) {
                echo "Updated successfully";
            }
        }
    }

    /**
     * Description : Used to get question details
     * Author : Synergy
     * @param array $_POST
     * @return string
     */
    function getQuestionDetailsByQuestionId() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }

        $token              = $this->config->item('accessToken');
        $userData           = $this->session->userdata['logged_in'];       
        $resp['error']      = 0;
        $questionValueId    = $this->input->post('questionValueId', true);        
        if (!$questionValueId) {
            $error = "NotDone";
            echo $error; exit();
        }
        if (!$resp['error']) {

            $questionDetailsRes = getAuditionQuestionsById($questionValueId);            
            if( $questionDetailsRes ){                
                if( $questionDetailsRes['duration'] != "" && $questionDetailsRes['duration'] != null ){
                    $questionDetailsStr .= "####".$questionDetailsRes['duration'];
                } else {
                    $questionDetailsStr .= "####NotDuration";
                }                
                echo $questionDetailsStr;
                exit();            
            } else {
                echo "NotDone";
                exit();     
            }            
        }
    }

    /**
     * Description : Used to delete a question from an audition
     * Author : Synergy
     * @param array $_POST
     * @return string
     */
    function deleteAuditionQuestion() {
        $error              = 0;
        $token              = $this->config->item('accessToken');
        $fromEmail          = $this->config->item('fromEmail');
        $auditionQuestionId = $this->input->post('id', true);
        if (!$auditionQuestionId) {
            echo $error = 'Undefined Audition Question!';
            exit;
        }
        if (!$error) {
            $data['status'] = 3;
            $resp = $this->audition_model->updateAuditionQuestion($data, $auditionQuestionId);
            if ($resp) {
                echo "Deleted successfully";
            }
        }
    }

    /**
     * Description : Used to edit question in an audition
     * Author : Synergy
     * @param array $id
     * @return array or string
     */
    function editAuditionQuestion($id) {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }
        $token      = $this->config->item('accessToken');
        $userData   = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        } else {
            $userId = 1;
        }
        $info['main_content']       = 'fj-edit-audition-question';
        $info['token']              = $token;
        $info['auditionQuestion']   = $this->audition_model->getAuditionQuestionName($id);
        
        if (isset($_POST) && $_POST != NULL) {    
            $questions      = $this->input->post('question', true);
            $itemId         = $this->input->post('itemId', true);
            
            if ($this->form_validation->run() == false) {
                $resp['error'] = validation_errors();
            }
            if ($questions[0]['title'] == '') {
                $resp['error'] .= '<p>Title field of first question is required.</p>';
            }
            if ($questions[0]['duration'] == '') {
                $resp['error'] .= '<p>Answer Duration field of first question is required.</p>';
            }
            if ($questions[0]['id'] == '') {
                $resp['error'] .= '<p>Invalid Attempt.</p>';
            }
            if (!$resp['error']) {
                try {
                    if (count($questions) > 0) {
                        foreach ($questions as $key=>$item) {
                            if($item['title']!='' && $item['duration']!='' && $item['id']!='') {
                                $data['title']      = $item['title'];
                                $data['duration']   = $item['duration']; 
                                $data['updatedAt']  = date('Y-m-d h:i:s');
                                $data['updatedBy']  = $userId;
                                $resp               = $this->audition_model->updateAuditionQuestion($data, $item['id']);
                            } 
                        }
                        $info['message'] = 'Successfully updated!';
                        redirect(base_url('audition/list/question'));
                        $this->load->view('fj-mainpage', $info);
                    }
                } catch (Exception $e) {
                    $resp['error'] = $e->getMessage();
                }
            } else {
                $info['error'] = $resp;
                $info['audition'] = $this->input->post();
                $this->load->view('fj-mainpage', $info);
            }
        } 
        else {
            $this->load->view('fj-mainpage', $info);
        }
    }
}
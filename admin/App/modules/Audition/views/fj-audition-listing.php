<?php //echo "<pre>";print_r($contentMyQuestions); die();

$urlSeg = $this->uri->segment(3);
?>

<?php
if($urlSeg == 'question'){ ?>
<script>
$(function(){
    $('#myAuditionQuestions').trigger('click');
});
</script>
<?php }  ?>

<?php
if($urlSeg == 'set'){ ?>
<script>
$(function(){
    $('#AuditionSet').trigger('click');
});
</script>
<?php }  ?>



<div id="page-content-wrapper">

    <div class="container-fluid whitebg">
        <div class="row">
            <div class="col-md-9"><h2><img src="<?php echo base_url(); ?>/theme/firstJob/image/arrow_head.png" class="prev-page">Manage Audition</h2></div>
            <div class="col-md-3 ed_job_bt"><a href="<?php echo base_url(); ?>audition/addAudition" class="add-new-set"><button type="button" class="audition-text">Add New Audition Set</button></a></div>

            <div class="clearfix"></div>

            <div class="col-md-12 rsp_tbl">
                <ul class="nav nav-pills padd_tabs">
                    <li class="active active_tab"><a data-toggle="pill" href="#home" id="AuditionSet">Audition Sets</a></li>
                    <li class=""><a data-toggle="pill" href="#menu2" id="myAuditionQuestions">Audition Question Bank</a></li>
                </ul>

                <div class="tab-content">
                    <div id="home" class="tab-pane fade active in">
                        <table class="table" id="sorting">
                            <thead class="tbl_head">
                                <tr>
                                    <th></th>
                                    <th><span>Audition Set</span></th>
                                    <th><span>No. of Questions</span></th>
                                    <th><span>Submitted By </span></th>
                                    <th><span>Date Time</span></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 0;
                                foreach ($contentAuditionSet as $item): ?>
                                    <tr <?php if ($i % 2 != 0) echo 'class="tbl_bg"'; ?>>
                                        <td><input type="radio" class="activeAudition" name="activeAudition" value="<?php echo $item->id;?>" <?php if($item->status=='1') echo "checked='true'";?>  ></td>
                                        <td><a href="<?php echo base_url(); ?>audition/view/<?php echo $item->id; ?>"><?php echo $item->name; ?></a></td>                                        
                                        <td><?php echo $item->questionCount; ?></td>
                                        <td><?php echo $item->fullname; ?></td>
                                        <td><?php echo date('M, d-Y', strtotime($item->createdAt)); ?></td>
                                        <td> 
                                            <a href="<?php echo base_url(); ?>audition/edit/<?php echo $item->id; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/edit.png" class="padd_rgt"></a>
                                            <a href="javascript:" class="delete-audition-set" id="<?php echo $item->id; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/del.png"></a>
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                endforeach;
                                ?>
                            </tbody>
                        </table>    
                        <?php echo $pagination; //$this->pagination->create_links(); ?>
                    </div>

                    <div id="menu2" class="tab-pane fade">
                        <table class="table" id="sorting2">
                            <thead class="tbl_head">
                                <tr>
                                    <!--<th><input type="checkbox" class="chk_bx"></th>-->
                                    <th width="50%"><span>Title</span></th>
                                    <th width="10%"><span>Duration</span></th>
                                    <th width="15%"><span>Submitted By </span></th>
                                    <th width="10%"><span>Date Time</span></th>
                                    <th width="15%"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 0;
                                foreach ($contentFjQuestions as $itemMy):
                                    ?>
                                    <tr <?php if ($i % 2 != 0) echo 'class="tbl_bg"'; ?>>
                                        <!--<td><input type="checkbox"></td>-->
                                        <td><?php echo $itemMy->title; ?></td>
                                        <td><?php echo $itemMy->duration; ?> Sec.</td>
                                        <td><?php echo $itemMy->fullname; ?></td>
                                        <td><?php echo date('M, d-Y', strtotime($itemMy->createdAt)); ?></td>
                                        <td> 
                                            <a href="javascript:" 
                                               data-toggle="modal" 
                                               data-target="#addQuestionToAuditionSet" 
                                               class="add-question-to-auditionset" 
                                               data-questionid="<?php echo $itemMy->id; ?>"
                                               title="Click here to add the selected question to a pre-existing audition set of that type"> <img src="<?php echo base_url(); ?>/theme/firstJob/image/add_black.png"></a>
                                            
                                            &emsp;
                                            <a href="<?php echo base_url(); ?>audition/editAuditionQuestion/<?php echo $itemMy->id; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/edit.png"></a>
                                            &emsp;
                                            <a href="javascript:" class="delete-audition-question" id="<?php echo $itemMy->id; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/del.png"></a>
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                endforeach;
                                ?>
                            </tbody>
                        </table>
                    
                        <?php echo $paginationQus; //$this->pagination->create_links(); ?>
                    </div>
                </div>
                
            </div>

        </div>
    </div>
</div>





<style>
    .service_content ul.service_content_last_list li{ display:inline-block; font-size:20px; width:38px; height:38px; line-height:38px; border:0px; }
    .service_content ul.service_content_last_list li a{ color:#333333; padding:0px; text-align:center; display:block; border:0px;  }
    .service_content ul.service_content_last_list li a:hover{ color:#fff; border-radius:50px; -moz-border-radius:50px; -webkit-border-radius:50px; background:#0072dc }
    .response_img img{ max-width: 100%; }
    .service_content .service_content_last_list{ text-align:center; padding:10px 0 70px; }
    ul.service_content_last_list  li.pagination-last-list{padding:0 20px 0 0px; cursor:pointer; text-indent:-9999px; margin-left:5px;  }
    .pagi_img{display: -webkit-inline-box;}
    .pagi_img a img{display: -webkit-inline-box;}
    .service_content ul.service_content_last_list li{margin:0px!important;}
    h1{font-size:32px!important;}
    .acu_bt a {
        text-decoration: none;
        color: #fff;
    }
    .sel_opt {
        font-size:15px;
        width:33% !important;
        padding-left: 9px !important;
    }
    #sorting td a {color: #303030!important; }
    #sorting td a:hover {color: #777!important; }

</style>
<!-- PopUp for add question to audition set  -->
<div id="addQuestionToAuditionSet" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <form class="form-horizontal form_save" action="<?php echo base_url(); ?>audition/addToAudition" method="post" id="auditionQuestionForm" >
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Question To Audition Set</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger hidden" id="auditionQuestionAlert"></div>
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-3">Audition Sets</label>
                        <div class="col-xs-9" id="auditionSets"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary add-audition-question">Save</button>
                    <!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
                </div>
            </div>
        </form>
    </div>
</div>

<script src="<?php echo base_url(); ?>theme/firstJob/js/chosen.jquery.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>theme/firstJob/js/prism.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
    var config = {
        '.chosen-select': {},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }

    $(function () {
        $("#datepicker-19").datepicker();
    });
    
    $(document).on('click', '.activeAudition', function () {
        var setId = ($(this).val());
        $.ajax({
            url : siteUrl + "audition/changeActiveAudition/"+setId,
            type: "POST",
            data : setId,
            cache: false,
            success: function(result)
            {
                if( result.trim() == "Updated successfully" ){
                    alert('Updated successfully');
                }
            }
        });
    });

</script>
<style>
    /*#sorting1 th  {
        width:33%;
    }*/
</style>
<?php
if (isset($audition) && $audition != NULL) {
    //echo '<pre>';print_r($audition);  
}
?>
<div id="page-content-wrapper">
    <?php if (isset($message)): ?>
    <div class="alert alert-success">
        <?php echo $message; ?>
    </div>
    <?php endif; ?>
    <?php if (isset($error)): ?>
    <div class="alert alert-danger">
        <?php foreach ($error as $item): ?>
            <?php echo $item; ?>
        <?php endforeach; ?>
    </div>
    <?php endif; ?>
    <div class="container-fluid whitebg">
        <div class="row">
            <div class="col-md-12 padd_tp_bt_20">
                <h4><img src="<?php echo base_url(); ?>/theme/firstJob/image/arrow_head.png" class="prev-page"> Edit Audition Set</h4>
                <form class="form-horizontal form_save" method="post" action="" enctype="multipart/form-data" id="auditionset">                    
                    <div class="form-group">
                        <div class="col-xs-9">
                            <label for="inputEmail" class="control-label frm_in_padd required-field">Set Name</label>
                            <input type="text" class="form-control" value="<?php
                            if (isset($audition['name'])): echo $audition['name'];
                            endif;
                            ?>" name="name" data-validation="required" maxlength="100">
                        </div>
                    </div>
                    
                    <div id ="questionList">
                        <?php
                        $j = 0;
                        //echo "<pre>"; print_r($questions); exit;
                        foreach ($questions as $item):
                            //echo "<pre>"; print_r($questions);
                            $duration = $item['duration']; ?>                        
                            <?php if ($j == 0) { ?>
                        <div style="float:right; padding-right: 56px;" class="col-xs-3 ques-title"">
                                <a href="javascript:" data-toggle="modal" data-target="#selectQuestionType">
                                    <img src="<?php echo base_url(); ?>/theme/firstJob/image/plus.png" class="pad_rg_lfs add-more-audition-question">
                                </a>
                            </div>
                            <?php } ?>
                            <div class="form-group">
                                <div class="col-xs-9 ques-title">
                                    <label for="inputPassword" class="control-label frm_in_padd">Question Text</label>
                                    <select data-placeholder="Type a question" style="width: 350px; display: none;" class="chosen-select sel_sty_padd auditionquestion" tabindex="-1" name="question[<?php echo $j; ?>][title]">                                        
                                        <option value="<?php echo $item['questionId']; ?>"><?php echo $item['title']; ?></option>
                                        <?php foreach ($auditionQuestion as $item1): ?>
                                            <option value="<?php echo $item1['id']; ?>"  <?php if (isset($item['questionId']) && $item['questionId'] == $item1['id']) echo "selected"; ?>><?php echo $item1['title']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <input type="hidden" name="itemId[]" value="<?php echo $item['id']; ?>" />
                                    <?php
                                    if($j==0) { ?>
                                    <input type="text" data-validation="auditionQusData" style="height: 0px; width: 0px; visibility: hidden; " />
                                    <?php
                                    } ?>
                                </div>

                                <div class="col-xs-3">
                                    <label for="inputPassword" class="control-label frm_in_padd col-xs-12">Answer Duration</label>
                                    <div class="col-md-6 no_padding">
                                        <input type="text" class="form-control auditionduration questiondurationtogetid" placeholder="" id="durations<?php echo $j; ?>" name="question[<?php echo $j; ?>][duration]" value="<?php if (isset($duration)) echo $duration; ?>">
                                        <?php
                                        if($j==0) { ?>
                                        <input type="text" data-validation="auditionData" style="height: 0px; width: 0px; visibility: hidden; " />
                                        <?php
                                        } ?>
                                    </div>
                                    <div class="col-md-4">
                                        <p class="padd_txtmin">Sec 
                                        </p>
                                    </div>
                                    <div class="col-md-2">                                       
                                        <button type="button" class="del-question pull-right" value="testing">x</button>
                                    </div>
                                </div> 
                            </div>
                            <?php $j++;
                            ?>
                            <!--Java Script Code-->

                            <!--Java Script Code-->
                        <?php endforeach; ?>
                    </div>

                    <div class="clearfix"></div>
                    <div class="col-xs-offset-2 col-xs-10 no_padding">

                        <input type="hidden" name="accessToken" value="<?php echo $token; ?>"/>
                        <button type="submit" class="Save_frm pull-right save-audition" value="testing">SAVE</button>

                    </div>
                </form>

            </div>
        </div>
    </div>
</div>






<script src="<?php echo base_url(); ?>theme/firstJob/js/chosen.jquery.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>theme/firstJob/js/prism.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
    var config = {
        '.chosen-select': {},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: ''},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
    $(function () {
        $("#datepicker-19").datepicker();
    });
</script>
<style>
    .no-results {
        display: none!important;
    }
    .chosen-single {
        background :none!important;
        border-radius:0px !important;
        height: 34px!important;
    }
    #questionList .form-group {
       /* border-bottom: 2px solid #f1f1f1;*/
    }
</style>

<script>
    <?php 
    if($audition['type'] == 1) {
        $type = 'video';
    } else {
        $type = 'audio';
    }
    ?>
    window.onload = function () {
       // alert('uploads/questions/video/<?php echo $item['file'] ?>');
       <?php $v =0;foreach ($questions as $item):?>
        var videoUrl = "uploads/questions/<?php echo $type?>/<?php echo $item['file']?>";
        //alert(videoUrl);
        jwVideo(videoUrl, 'questionVideo<?php echo $v; ?>');
        <?php $v++; endforeach;?>
    };
</script>




<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script>
// Add validator For Interview Assessment
$.formUtils.addValidator({
    name : 'auditionData',
    validatorFunction : function(value, $el, config, language, $form) {
      
        var hasNoValue;
        var durationExceed;
        $('.auditionduration').each(function(i) {
            if ($(this).val() == '') {
                  hasNoValue = true;
            }
            else {
                if (parseInt($(this).val())>600 || isNaN($(this).val())) {
                    durationExceed = true;
                }
            }
        });
        if (hasNoValue==true || durationExceed==true) {
            return false;
        }
        else {
            return true;
        }
        //return parseInt(value, 10) % 2 === 0;
    },
    errorMessage : 'All duration (limit 600 sec) fields must be filled'
});

// Add validator For Interview Assessment
$.formUtils.addValidator({
    name : 'auditionQusData',
    validatorFunction : function(value, $el, config, language, $form) {
      
        var hasNoValue;
        $('.auditionquestion').each(function(i) {
            if ($(this).val() == '') {
                  hasNoValue = true;
            }
        });
        if (hasNoValue) {
            return false;
        }
        else {
            return true;
        }
        //return parseInt(value, 10) % 2 === 0;
    },
    errorMessage : 'All audition question fields must be filled'
});

$.validate({
    form: '#auditionset',
    modules: 'file',
    onError: function ($form) {
        //alert($('.setquestion').val());
        //alert($('.setInterviewType').html());
        return false;
    },
    onSuccess: function ($form) {
        return true;
    },
    validateOnBlur : false,
    errorMessagePosition : 'top',
    scrollToTopOnError : true // Set this property to true on longer forms
});
</script>





<script>    
    $(document).on('blur', '#questionList .chosen-search input', function (evt) {
        //alert($('.no-results').length);
        if ($('.no-results').length == 1) {
            $(this).parents('.ques-title').find('.chosen-select').append('<option>' + $(evt.target).val() + '</option>');
            $(this).parents('.ques-title').find('.chosen-select').val($(evt.target).val());
            $(this).parents('.ques-title').find('.chosen-select').trigger('chosen:updated');
        }else {
            $("#questionList .chosen-select").chosen({
                width: "300px",
              //  enable_search_threshold: 10
            }).change(function(event)
            {
                //alert(event.target);
                if(event.target)
                {
                    var questionValueId = this.value;
                    var indexDurationValue = $(this).parents('.form-group').find('.questiondurationtogetid').attr('id');
                    firstBlockForQuestionDetails(questionValueId, '', indexDurationValue, '');
                    //alert(questionValueId);
                }
            });
        }
    });
    
    function firstBlockForQuestionDetails(questionValueId, indexFileNameValue, indexDurationValue, indexShowVideoValue){        
        if( ( questionValueId != "" && indexFileNameValue != "" && indexDurationValue != "" ) || ( questionValueId != "" && indexDurationValue != "" ) ) {
            // show industry drop down div
            //alert(indexDurationValue);
            formData = {questionValueId:questionValueId, indexFileNameValue:indexFileNameValue, indexDurationValue:indexDurationValue}; //Array 
            //alert($(this).val());
            //alert(siteUrl + "interviews/getQuestionDetailsByQuestionId");            
            $.ajax({
                url : siteUrl + "audition/getQuestionDetailsByQuestionId",
                type: "POST",
                data : formData,
                cache: false,
                success: function(result)
                {
                    $("#"+indexDurationValue).val('');
                    //alert(result);
                    if( result.trim() != "NotDone" || result.trim() == "" ){
                        var resultArr = result.split('####');  
                        //alert(resultArr);
                        if( resultArr[1].trim() != "" && resultArr[1].trim() != "NotDuration" ){                            
                            $("#"+indexDurationValue).val(resultArr[1]);
                        }
                    }                    
                    //data - response from server
                }
            });
        }
    }
</script>

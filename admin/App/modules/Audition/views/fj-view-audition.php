<?php
if (isset($audition) && $audition != NULL) {
    //echo '<pre>';print_r($audition);  
}
//echo "<pre>";print_r($questions); die;
?>
<div id="page-content-wrapper">
    <?php if (isset($message)): ?>
    <div class="alert alert-success">
        <?php echo $message; ?>
    </div>
    <?php endif; ?>
    <?php if (isset($error)): ?>
    <div class="alert alert-danger">
        <?php foreach ($error as $item): ?>
            <?php echo $item; ?>
        <?php endforeach; ?>
    </div>
    <?php endif; ?>
    <div class="container-fluid whitebg">
        <div class="row">
            <div class="col-md-12 padd_tp_bt_20">
                <h4><img src="<?php echo base_url(); ?>/theme/firstJob/image/arrow_head.png" class="prev-page"> Audition Set Detail</h4>
                <form class="form-horizontal form_save" method="post" action="" enctype="multipart/form-data">
                    <!--
                    <div class="form-group" id="industryDivId">
                        <label for="inputEmail" class="control-label col-xs-2 required-field">Question Industry</label>
                        <div class="col-xs-10">
                            <div style="float:left; width:200px;">
                                <select class="selectpicker" name="industryId" id="industryId" disabled="">
                                    <option value="">Select Industry</option>
                                    <?php foreach ($ncIndustry as $item): ?>
                                        <option value="<?php echo $item->id; ?>" <?php if (isset($audition['industryId']) && $audition['industryId'] == $item->id) echo "selected"; ?>><?php echo $item->name; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <label for="inputEmail" class="control-label col-xs-2 required-field" style="padding-left:10px;">Industry Function</label>
                            <div style="float:left; width:200px; padding-left: 20px;" id="industryFunctionDiv">
                                <select class="selectpicker" name="functionId" id="functionId" disabled="">
                                    <option value="">Select</option>
                                    <?php if(isset($industryFunction) && !empty($industryFunction) ) { ?>
                                        <?php foreach ($industryFunction as $item): ?>
                                            <option value="<?php echo $item->id; ?>" <?php if (isset($audition['functionId']) && $audition['functionId'] == $item->id) echo "selected"; ?>><?php echo $item->name; ?></option>
                                        <?php endforeach; ?>
                                    <?php } ?>
                                </select>                                
                            </div>
                        </div>
                    </div>
                    -->
                    <div class="form-group">
                        <label for="inputEmail" class="control-label col-xs-2 required-field">Set Name</label>
                        <div class="col-xs-10">
                            <input type="text" class="form-control" value="<?php
                            if (isset($audition['name'])): echo $audition['name'];
                            endif;
                            ?>" name="name" disabled="" readonly="">
                        </div>
                    </div>

                    <div id ="questionList">
                        <?php
                        $j = 0;
                        //echo "<pre>"; print_r($questions);
                        foreach ($questions as $item):
                            $duration   = $item['duration']; ?>
                            <div class="form-group">
                                <label for="inputPassword" class="control-label col-xs-2"><?php if ($j == 0) echo "Question"; ?></label>
                                <div class="col-xs-4 ques-title">
                                    <label for="inputPassword" class="control-label frm_in_padd">Title</label>
                                     <input type="text" class="form-control" placeholder="" name="question[<?php echo $j; ?>][title]" value="<?php if (isset($item['title'])) echo $item['title']; ?>" disabled="">
                                </div>
                                
                                <div class="col-xs-3">
                                    <label for="inputPassword" class="control-label frm_in_padd col-xs-12">Answer Duration</label>
                                    <div class="col-md-6 no_padding">
                                        <input type="text" class="form-control" placeholder="" name="question[<?php echo $j; ?>][duration]" value="<?php if (isset($duration)) echo $duration; ?>" disabled="">
                                    </div>
                                    <div class="col-md-6">
                                        <p class="padd_txtmin">Sec</p>
                                    </div>  
                                </div> 
                            </div>
                            <?php 
                            $j++;
                            ?>
                        <?php endforeach; ?>
                    </div>

                    <div class="clearfix"></div>
                    <div class="col-xs-offset-2 col-xs-10 no_padding">
                        <input type="hidden" name="accessToken" value="<?php echo $token; ?>"/>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<style>
    .no-results {
        display: none!important;
    }
    .chosen-single {
        background :none!important;
        border-radius:0px !important;
        height: 34px!important;
    }
    input,select, .chosen-single{
        background: white!important;
        border: 0px!important;
        cursor: initial !important;
        box-shadow: inset 0px 0px 0px 0px red !important;
    }
    #questionList .form-group {
        background: #f1f1f1;
    }
</style>
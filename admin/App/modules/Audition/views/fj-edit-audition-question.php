<div id="page-content-wrapper">
    <?php if (isset($message)): ?>
    <div class="alert alert-success">
        <?php echo $message; ?>
    </div>
    <?php endif; ?>
    <?php if (isset($error)): ?>
    <div class="alert alert-danger">
        <?php foreach ($error as $item): ?>
            <?php echo $item; ?>
        <?php endforeach; ?>
    </div>
    <?php endif; ?>
    <div class="container-fluid whitebg">
        <div class="row">
            <div class="col-md-12 padd_tp_bt_20">
                <h4><img src="<?php echo base_url(); ?>/theme/firstJob/image/arrow_head.png" class="prev-page"> Edit Question</h4>
                <form class="form-horizontal form_save" method="post" action="" enctype="multipart/form-data" id="auditionset">
                    <div id ="questionList">
                        <div class="form-group">                            
                            <div class="col-xs-9 ques-title">
                                <label for="inputPassword" class="control-label frm_in_padd">Title</label>
                                <input type="text" class="form-control auditionquestion" placeholder="" name="question[0][title]" value="<?php if (isset($auditionQuestion[0]['title'])) echo $auditionQuestion[0]['title']; ?>" maxlength="150">
                                <input type="text" data-validation="auditionQusData" style="height: 0px; width: 0px; visibility: hidden; " />
                            </div>                            
                            <div class="col-xs-3">
                                <label for="inputPassword" class="control-label frm_in_padd col-xs-12">Answer Duration <span style="font-size:10px;">(Limit 600 Sec)</span></label>
                                <div class="col-md-6 no_padding">
                                    <input type="text" class="form-control auditionduration" placeholder="" name="question[0][duration]" value="<?php if (isset($auditionQuestion[0]['duration'])) echo $auditionQuestion[0]['duration']; ?>" maxlength="3">
                                    <input type="text" data-validation="auditionData" style="height: 0px; width: 0px; visibility: hidden; " />
                                    <input type="hidden" name="question[0][id]" value="<?php if (isset($auditionQuestion[0]['id'])) echo $auditionQuestion[0]['id']; ?>">
                                </div>
                            </div> 
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="col-xs-offset-2 col-xs-10 no_padding">
                        <input type="hidden" name="accessToken" value="<?php echo $token; ?>"/>
                        <button type="submit" class="Save_frm pull-right save-interview" value="testing">SAVE</button>
                    </div>
                    <input type="hidden" name="fjadminqus" id="fjadminqus" value="1">
                </form>

            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url(); ?>theme/firstJob/js/chosen.jquery.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>theme/firstJob/js/prism.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">

    var config = {
        '.chosen-select': {},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: ''},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }

    $(function () {
        $("#datepicker-19").datepicker();
    });
    $(document).on('blur', '.chosen-search input', function (evt) {
        //alert($('.no-results').length);
        if ($('.no-results').length == 1) {
            //alert($('.chosen-choices .search-field input').val());
            //$('.chosen-select');
            $(this).parents('.ques-title').find('.chosen-select').append('<option>' + $(evt.target).val() + '</option>');
            $(this).parents('.ques-title').find('.chosen-select').val($(evt.target).val());
            $(this).parents('.ques-title').find('.chosen-select').trigger('chosen:updated');
        }
    });
</script>
<style>
    .no-results {
        display: none!important;
    }
    .chosen-single {
        background :none!important;
        border-radius:0px !important;
        height: 34px!important;
    }
</style>




<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script>
// Add validator For Interview Assessment
$.formUtils.addValidator({
    name : 'auditionData',
    validatorFunction : function(value, $el, config, language, $form) {
      
        var hasNoValue;
        var durationExceed;
        $('.auditionduration').each(function(i) {
            if ($(this).val() == '') {
                  hasNoValue = true;
            }
            else {
                if (parseInt($(this).val())>600 || isNaN($(this).val())) {
                    durationExceed = true;
                }
            }
        });
        if (hasNoValue==true || durationExceed==true) {
            return false;
        }
        else {
            return true;
        }
        //return parseInt(value, 10) % 2 === 0;
    },
    errorMessage : 'All duration (limit 600 sec) fields must be filled '
});

// Add validator For Interview Assessment
$.formUtils.addValidator({
    name : 'auditionQusData',
    validatorFunction : function(value, $el, config, language, $form) {
      
        var hasNoValue;
        $('.auditionquestion').each(function(i) {
            if ($(this).val() == '') {
                  hasNoValue = true;
            }
        });
        if (hasNoValue) {
            return false;
        }
        else {
            return true;
        }
        //return parseInt(value, 10) % 2 === 0;
    },
    errorMessage : 'All audition question fields must be filled'
});

$.validate({
    form: '#auditionset',
    modules: 'file',
    onError: function ($form) {
        //alert($('.setquestion').val());
        //alert($('.setInterviewType').html());
        return false;
    },
    onSuccess: function ($form) {
        return true;
    },
    validateOnBlur : false,
    errorMessagePosition : 'top',
    scrollToTopOnError : true // Set this property to true on longer forms
});
</script>

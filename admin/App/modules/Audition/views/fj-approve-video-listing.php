<?php //echo "<pre>";print_r($contentMyQuestions); die();

$urlSeg = $this->uri->segment(3);
?>

<?php
if($urlSeg == 'videolist'){ ?>
<script>
$(function(){
    //$('#myAuditionQuestions').trigger('click');
});
</script>
<?php }  ?>

<?php
if($urlSeg == 'videolist'){ ?>
<script>
$(function(){
    //$('#AuditionSet').trigger('click');
});
</script>
<?php }  ?>


<script type="text/javascript">
    function flashObject(url, id, width, height, version, bg, flashvars, params, att)
    {
        var pr = '';
        var attpr = '';
        var fv = '';
        var nofv = 0;
        for(i in params)
        {
            pr += '<param name="'+i+'" value="'+params[i]+'" />';
            attpr += i+'="'+params[i]+'" ';
            if(i.match(/flashvars/ig))
            {
                nofv = 1;
            }
        }
        if(nofv==0)
        {
            fv = '<param name="flashvars" value="';
            for(i in flashvars)
            {
                fv += i+'='+escape(flashvars[i])+'&';
            }
            fv += '" />';
        }
        htmlcode = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"  codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=5,0,0,0" width="'+width+'" height="'+height+'">'
    +'  <param name="movie" value="'+url+'" />'+pr+fv
    +'  <embed src="'+url+'" width="'+width+'" height="'+height+'" '+attpr+'type="application/x-shockwave-flash" pluginspage="http://www.adobe.com/go/getflashplayer"></embed>'
    +'</object>';
        document.getElementById(id).innerHTML=htmlcode;
    }
</script>

<div id="page-content-wrapper">

    <div class="container-fluid whitebg">
        <div class="row">
            <div class="col-md-9"><h2><img src="<?php echo base_url(); ?>/theme/firstJob/image/arrow_head.png" class="prev-page">Approve Video</h2></div>

            <div class="clearfix"></div>

            <div class="col-md-12 rsp_tbl">
                <ul class="nav nav-pills padd_tabs">
                    <li class="active active_tab"><a data-toggle="pill" href="#home" id="AuditionSet">Video Resume</a></li>
                    <li class=""><a data-toggle="pill" href="#menu2" id="myAuditionQuestions">Interview Videos</a></li>
                </ul>

                <div class="tab-content">
                    <div id="home" class="tab-pane fade active in">
                        <table class="table" id="sorting">
                            <thead class="tbl_head">
                                <tr>
                                    <th><span>Submitted By </span></th>
                                    <th><span>Date Time</span></th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 0;
                                foreach ($contentAuditionSet as $item): ?>
                                    <tr <?php if ($i % 2 != 0) echo 'class="tbl_bg"'; ?>>
                                        <td>
                                            <a href="javascript:" 
                                            data-toggle="modal" 
                                            data-target="#viewUserProfileDetails" 
                                            class="view-user-profile-details" 
                                            data-options='{
                                            "userId":"<?php echo $item->userId; ?>",
                                            "userName":"<?php echo $item->fullname; ?>",
                                            "auditionfile":"<?php echo $item->audition_file; ?>", 
                                            "auditionfiletype":"<?php echo $item->file_type; ?>", 
                                            "auditionFiles":"<?php echo $item->auditionFiles; ?>",
                                            "auditionId":"<?php echo $item->id; ?>"}'>
                                        <?php echo $item->fullname; ?> </a></td>
                                        </td>
                                        <td><?php echo date('M, d-Y', strtotime($item->createdAt)); ?></td>
                                        <td>
                                            <a href="javascript:" class="approve-audition-video" id="approveAudition-<?php echo $item->id; ?>" style="color : #337ab7">Approve | </a>
                                            <a href="javascript:" class="reject-audition-video" id="rejectAudition-<?php echo $item->id; ?>" style="color : #337ab7"> Reject</a>
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                endforeach;
                                ?>
                            </tbody>
                        </table>    
                        <?php echo $pagination; //$this->pagination->create_links(); ?>
                    </div>

                    <div id="menu2" class="tab-pane fade">
                        <table class="table" id="sorting2">
                            <thead class="tbl_head">
                                <tr>
                                    <th><span>Submitted By </span></th>
                                    <th><span>Company </span></th>
                                    <th><span>Job </span></th>
                                    <th><span>Date Time</span></th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $j = 0;
								$i = 0;
								//echo "<pre>";print_r($contentFjQuestions);die;
                                foreach ($contentFjQuestions as $itemMy):
                                    ?>
                                    <tr <?php if ($j % 2 != 0) echo 'class="tbl_bg"'; ?>>
                                        <td>
                                        <a
                                            href="javascript:" 
                                            data-toggle="modal" 
                                            data-target="#shareInterview" 
                                            class="share-user-interview" 
                                            data-options='{"userJob":"<?php echo $itemMy->id; ?>", "appUserName":"<?php echo $itemMy->fullname;?>", "interviewVideo":"<?php echo $itemMy->mergedVideo;?>","jobId":"<?php echo $itemMy->jobId;?>","userId":"<?php echo $itemMy->userId;?>", "companyName":"<?php echo $itemMy->companyName; ?>", "jobName":"<?php echo $itemMy->jobName; ?>"}'
                                            >
                                        <?php echo $itemMy->fullname; ?> </a></td>
                                        </td>
                                        <td><?php echo $itemMy->companyName; ?></td>
                                        <td><?php echo $itemMy->jobName; ?></td>
                                        <td><?php echo date('M, d-Y', strtotime($itemMy->createdAt)); ?></td>
                                        <td>
                                            <?php // if($itemMy->mergedVideo) { ?>
                                            <a href="javascript:" class="approve-interview-video" id="approveInterview-<?php echo $itemMy->id; ?>" style="color : #337ab7">Approve | </a>
                                            <a href="javascript:" class="reject-interview-video" id="rejectInterview-<?php echo $itemMy->id; ?>" style="color : #337ab7"> Reject</a>
                                            <?php /*} else { ?>
                                                <a   href='javascript:void(0);'
                                                            data-options='{"userId":"<?php echo $itemMy->fjUserId; ?>", "jobId":"<?php echo $itemMy->fjfjCode; ?>"}' 
                                                            class='generateVideo'>Generate Video</a>
                                            <?php } */ ?>
                                        </td>
                                    </tr>
                                    <?php
                                    $j++;
                                endforeach;
                                ?>
                            </tbody>
                        </table>
                    
                        <?php echo $paginationQus; //$this->pagination->create_links(); ?>
                    </div>
                </div>
                
            </div>

        </div>
    </div>
</div>





<style>
    .service_content ul.service_content_last_list li{ display:inline-block; font-size:20px; width:38px; height:38px; line-height:38px; border:0px; }
    .service_content ul.service_content_last_list li a{ color:#333333; padding:0px; text-align:center; display:block; border:0px;  }
    .service_content ul.service_content_last_list li a:hover{ color:#fff; border-radius:50px; -moz-border-radius:50px; -webkit-border-radius:50px; background:#0072dc }
    .response_img img{ max-width: 100%; }
    .service_content .service_content_last_list{ text-align:center; padding:10px 0 70px; }
    ul.service_content_last_list  li.pagination-last-list{padding:0 20px 0 0px; cursor:pointer; text-indent:-9999px; margin-left:5px;  }
    .pagi_img{display: -webkit-inline-box;}
    .pagi_img a img{display: -webkit-inline-box;}
    .service_content ul.service_content_last_list li{margin:0px!important;}
    h1{font-size:32px!important;}
    .acu_bt a {
        text-decoration: none;
        color: #fff;
    }
    .sel_opt {
        font-size:15px;
        width:33% !important;
        padding-left: 9px !important;
    }
    #sorting td a {color: #303030!important; }
    #sorting td a:hover {color: #777!important; }

</style>

<!-- PopUp for view user's profile video  -->
<div id="viewUserProfileDetails" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <form class="form-horizontal form_save" action="<?php echo base_url(); ?>knowledge/editcategory" method="post" id="knowledgeCenterCategoryForm" >
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Video Resume</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="col-xs-4" id="viewUserimage" style="padding-top: 9px;width:150px;margin-left: 76px;"></div>
                        <div class="col-xs-5" style="padding-top:7px;width: 65%;padding-left: 154px;">
                            <div id="viewUserauditionvideo" style="width:100%; margin:0px auto"></div>
                        </div>                            
                    </div>
                    <div class="form-group">
                        <div style="float: left; width: 444px; padding-left: 30px;">
                            
                            <div class="form-group">
                                <label class="control-label col-xs-4">User Name :</label>
                                <div class="col-xs-8" id="viewUsername" style="padding-top:7px;"></div>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <!-- div class="modal-footer">
                    <button type="button" class="btn btn-primary edit-knowledge-category" data-dismiss="modal">Close</button>
                </div -->
            </div>
        </form>
    </div>
</div>


<!-- PopUp for view share's profile  -->
<div id="shareInterview" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Approve Video</h4>
            </div>
            <div class="modal-body" id="share">                                      
                <form method="post" id="shareInterviewForm">
                <table class="table" border="0" bordercolor="#CCCCCC">
                    <thead class="tbl_head">
                        <tr>
                            <th colspan="3" align="center">Watch Video</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td valign="middle"><label class="control-label" style="font-weight:500">Candidate Name : </label></td>
                            <td><div id="appUsername"><div></td>
                        </tr>

                        <tr>
                            <td valign="middle"><label class="control-label" style="font-weight:500">Company Name : </label></td>
                            <td><div id="companyName"><div></td>
                        </tr>

                        <tr>
                            <td valign="middle"><label class="control-label" style="font-weight:500">Job Name : </label></td>
                            <td><div id="jobName"><div></td>
                        </tr>

                        <tr>
                            <td rowspan="5" colspan="2" align="center" width="100%">
                                <span id="userVideo"></span>
                            </td>
                        </tr>
                    </tbody>
                </table>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>theme/firstJob/js/chosen.jquery.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>theme/firstJob/js/prism.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
    var config = {
        '.chosen-select': {},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }

    $(function () {
        $("#datepicker-19").datepicker();
    });
    
    $(document).on('click', '.activeAudition', function () {
        var setId = ($(this).val());
        $.ajax({
            url : siteUrl + "audition/changeActiveAudition/"+setId,
            type: "POST",
            data : setId,
            cache: false,
            success: function(result)
            {
                if( result.trim() == "Updated successfully" ){
                    alert('Updated successfully');
                }
            }
        });
    });

    $(".generateVideo").click(function () {
        var userId      = $(this).data('options').userId;
        var jobId       = $(this).data('options').jobId;
        var recordId    = $(this).attr('id');   
        
        var formData    = new FormData();
        formData.append('jobId', jobId);
        formData.append('userId', userId);        
        formData.append('accessToken', '<?php echo $this->config->item('accessToken'); ?>');
        
        $(this).parent().html('Merging Video, Please Wait...'); 
        //$('.view-user-profile-details').prop('disabled', true);
        
        $.ajax({
            url: siteUrl + 'jobs/generateVideo/',
            data: formData,
            async: false,
            contentType: false,
            processData: false,
            cache: false,
            type: 'POST',
            success: function (data) {
                window.location.reload();
            },
        });
        return false;
    });



    $(document).on('click', '.view-user-profile-details', function () {

        document.addEventListener('contextmenu', event => event.preventDefault());
        var userId    = $(this).data('options').userId;
        var userName  = $(this).data('options').userName;        
        var auditionfile  = $(this).data('options').auditionfile;
        var auditionfiletype  = $(this).data('options').auditionfiletype;
        
        var auditionFiles  = $(this).data('options').auditionFiles;
        //alert(auditionFiles);

        var auditionId  = $(this).data('options').auditionId;
        
        $('#viewUsername').html(userName);

        if( auditionfiletype == 1 ){
            var auditionvideotype = "video";
        } else if( auditionfiletype == 2 ){
            var auditionvideotype = "audio";
        }
        

        if( auditionvideotype != "" ){
            if(auditionFiles!='') {
                var auditionFileName = auditionFiles.split(',');
                var message =  '<video id="videoarea" preload="auto" tabindex="0" controls="" src="<?php  echo S3BUCKETPATH.S3AUDITION; ?>/'+auditionFileName[0].trim()+'"></video>';
                message += '<center><ul id="playlist">';                
                var m=0;
                auditionFileName.forEach(function(auditionFileName) {
                    m++;
                    if(m==1)
                        message += '<li class="active"><a href="<?php  echo S3BUCKETPATH.S3AUDITION; ?>/'+auditionFileName.trim()+'">Audition Part '+m+'</a></li>';
                    else
                        message += '<li><a href="<?php  echo S3BUCKETPATH.S3AUDITION; ?>/'+auditionFileName.trim()+'">Audition Part '+m+'</a></li>';
                });
                message += '</ul></center>';
            }
            else {
                var message = '<video id="videoarea" preload="auto" tabindex="0" controls="" src="<?php  echo S3BUCKETPATH.S3MERGEAUDITION; ?>/'+auditionfile.replace('flv', 'mp4')+'"></video>';
                message += '<center><ul id="playlist">';
                message += '<li class="active"><a href="<?php echo S3BUCKETPATH.S3MERGEAUDITION; ?>/'+auditionfile.replace('flv', 'mp4')+'">Audition Part 1</a></li>';
                message += '</ul></center>';
            }
            $('#viewUserauditionvideo').html(message);

            init();
            function init(){
                var current     = 0;
                var audio       = $('#videoarea');
                var playlist    = $('#playlist');
                var tracks      = playlist.find('li a');
                var len         = tracks.length;
                audio[0].volume = .70;
                //audio[0].play();
                playlist.find('a').click(function(e){
                    e.preventDefault();
                    link    = $(this);
                    current = link.parent().index();
                    run(link, audio[0]);
                });
                audio[0].addEventListener('ended',function(e){
                    current++;
                    if(current == len){
                        current = 0;
                        link    = playlist.find('a')[0];
                    }else{
                        link    = playlist.find('a')[current];    
                    }
                    if(current!=0)
                        run($(link),audio[0]);
                });
            }
            function run(link, player){
                    player.src  = link.attr('href');
                    par         = link.parent();
                    par.addClass('active').siblings().removeClass('active');
                    player.load();
                    player.play();
            }
        } else {
            $('#viewUserauditionvideo').html('');
            $('#hdnaudid').val();
        }
    });

    $(document).on('click', '.share-user-interview', function () {
        //("#errMessage").html('');
        var userJob         = $(this).data('options').userJob;
        var interviewVideo  = $(this).data('options').interviewVideo;
        var accessToken     = '<?php echo $this->config->item('accessToken'); ?>';
        var appUserName     = $(this).data('options').appUserName;
        var companyName     = $(this).data('options').companyName;
        var jobName     = $(this).data('options').jobName;
        var jobId     = $(this).data('options').jobId;
        var userId     = $(this).data('options').userId;
        
        $('#appUsername').html(appUserName);
        $('#companyName').html(companyName);
        $('#jobName').html(jobName);
        $("#userVideo").html('');
        formData    = {userJob:userJob,userId:userId,jobId:jobId, accessToken:accessToken, interviewVideo:interviewVideo};
        $.ajax({
            url : siteUrl + "jobs/userVideo",
            type: "POST",
            data : formData,
            cache: false,
            success: function(result)
            {
                $("#userVideo").html(result);
            }
        });
    });
	
	
	function init(){
		
		var Answer = $("#questionListDropdownMenu").val();
		$('#questionListDropdownMenu').val(Answer);
		var text = $("#questionListDropdownMenu option[value='"+Answer+"']").text();
		$("#textQuiestionFlow").text(text);
		var SingleVideoAnswer = document.getElementById('videoShowAnswerbox');
		SingleVideoAnswer.onended = function(e) {
			var Answer = $("#questionListDropdownMenu").val();
			$('#questionListDropdownMenu option:selected').next().attr('selected', 'selected');
			var NextAnswer = $("#questionListDropdownMenu").val();
			if(NextAnswer == Answer){
			}else{
				getVideoAnswers(NextAnswer);
			}
		};
		
	}
	
	
	
	function getVideoAnswers(valueSelected){
		var Answer = $("#answerHideen"+valueSelected).val();
		//alert(Answer);
		
		$("#videoShowAnswerbox").attr('src',"https://s3.amazonaws.com/fjinterviewanswers/"+Answer);
		$("#videoShowAnswerbox").trigger('play');
		
		$( ".questionlistbutton" ).each(function() {
			$(this).css("background","none");
		});
		$('#qustionsCounterDropDown_'+valueSelected).css("background", "#00A2E8");
		$('#questionListDropdownMenu').val(valueSelected);
		var text = $("#questionListDropdownMenu option[value='"+valueSelected+"']").text();
		$("#textQuiestionFlow").text(text);
		var SingleVideoAnswer = document.getElementById('videoShowAnswerbox');
		SingleVideoAnswer.onended = function(e) {
			var Answer = $("#questionListDropdownMenu").val();
			$('#questionListDropdownMenu option:selected').next().attr('selected', 'selected');
			var NextAnswer = $("#questionListDropdownMenu").val();
			if(NextAnswer == Answer){
			}else{
				getVideoAnswers(NextAnswer);
			}
		};
		//$("#listofPlaylist_"+valueSelected).addClass('active');
		
	}
	
	
	function showPrevious(counter){
		var counterNew = counter-1;
		var nextToken = counterNew-1;
		var totalQuestion = $("#totalQuestion").val();
		if(counterNew < 1){
			$("#butttonprevious"+counter).css("color","grey");	
		}else{
			if(nextToken < 1){
				$("#butttonprevious"+counterNew).css("color","grey");	
			}
			$("#textArea_"+counter).css("display","none");
			$("#textArea_"+counterNew).css("display","block");
			$( ".questionlistbutton" ).each(function() {
				$(this).css("background","none");
			});
			$('#qustionsCounterDropDown_'+counterNew).css("background", "#00A2E8");
		}
	}
	var video = "";
	video.onended = function(e) {
		alert("video ended");
	};
	
	
	function showNext(counter){
		
		var counterNew = counter+1;
		var nextToken = counterNew+1;
		var totalQuestion = $("#totalQuestion").val();
		if(counterNew > totalQuestion){
			$("#butttonnext"+counter).css("color","grey");	
		}else{
			
			if(nextToken == totalQuestion){
				$("#butttonnext"+counterNew).css("color","grey");	
			}
			$("#textArea_"+counter).css("display","none");
			$("#textArea_"+counterNew).css("display","block");
			$( ".questionlistbutton" ).each(function() {
				$(this).css("background","none");
			});
			video = document.getElementById('myQuestion_'+counterNew);
			$('#qustionsCounterDropDown_'+counterNew).css("background", "#00A2E8");
		}
		
	}
	
	function showNextAuto(counter){
		
		var counterNew = counter+1;
		var nextToken = counterNew+1;
		var totalQuestion = $("#totalQuestion").val();
		if(counterNew > totalQuestion){
			$("#butttonnext"+counter).css("color","grey");	
		}else{
			
			if(nextToken == totalQuestion){
				$("#butttonnext"+counterNew).css("color","grey");	
			}
			$("#textArea_"+counter).css("display","none");
			$("#textArea_"+counterNew).css("display","block");
			$( ".questionlistbutton" ).each(function() {
				$(this).css("background","none");
			});
			video = document.getElementById('myQuestion_'+counterNew);
			Answervideo = document.getElementById('myAnswer_'+counterNew);
			
			if($("#ignoreQuestion").prop('checked') == true){
				$(Answervideo).trigger('play');
			}else{
				$(video).trigger('play');
			}
			
			
			
			video.onended = function(e) {
				$(Answervideo).trigger('play');
			};
			Answervideo.onended = function(e) {
				showNextAuto(counterNew);
			};
			$('#qustionsCounterDropDown_'+counterNew).css("background", "#00A2E8");
		}
		
	}
	
	function showQuestionCounter(counter){
		
		$( ".textArea" ).each(function() {
			$(this).css("display","none");
		});
		$( ".questionlistbutton" ).each(function() {
			$(this).css("background","none");
		});
		
		$( "video" ).each(function() {
			
			$(this).trigger('pause');
		});
		
		$("#textArea_"+counter).css("display","block");
		$("#qustionsCounterDropDown_"+counter).css("background", "#00A2E8");
		
		video = document.getElementById('myQuestion_'+counter);
		Answervideo = document.getElementById('myAnswer_'+counter);
		
		if($("#ignoreQuestion").prop('checked') == true){
			$(Answervideo).trigger('play');
		}else{
			$(video).trigger('play');
		}
		
		
		
		video.onended = function(e) {
			$(Answervideo).trigger('play');
		};
		Answervideo.onended = function(e) {
			//showNextAuto(counter);
		};
		
		
	}
</script>

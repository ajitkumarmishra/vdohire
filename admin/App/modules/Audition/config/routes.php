<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/


$route['audition/list']                 = "Audition/listAudition";
$route['audition/list/(:any)']          = "Audition/listAudition/$1";
$route['audition/list/question']        = "Audition/listAudition";
$route['audition/list/question/(:any)'] = "Audition/listAudition/$1";
$route['audition/list/set']             = "Audition/listAudition";
$route['audition/list/set/(:any)']      = "Audition/listAudition/$1";
$route['audition/question']             = "Audition/addAuditionQuestion";
$route['audition/addAudition']          = "Audition/addAudition";
$route['audition/view/(:any)']          = "Audition/getAudition/$1";
$route['audition/delete']               = "Audition/deleteAuditionSet";
$route['audition/edit/(:any)']          = "Audition/editAudition/$1";
$route['getAuditionSet']                = "Audition/getAuditionSet";
$route['audition/addToAudition']        = "Audition/addQuestionToAuditionSet";
$route['checkAudition']                 = "Audition/checkAudition";
$route['getQuestionRow']                = "Audition/getQuestionRow";
$route['getAuditionQuestionRow']        = "Audition/getAuditionQuestionRow";
$route['getAuditionQuestionBankRow']    = "Audition/getAuditionQuestionBankRow";
$route['audition/editQuestion/(:any)']  = "Audition/editQuestion/$1";
$route['approve/videolist']             = "Audition/approveVideo";
$route['approve/videolist/(:any)']      = "Audition/approveVideo/$1";
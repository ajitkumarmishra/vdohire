<div id="page-content-wrapper">
    <div class="container-fluid whitebg">
        <div class="row">
            <div class="col-md-9"><h4><img src="<?php echo base_url(); ?>/theme/firstJob/image/arrow_head.png" class="prev-page">Discussion Forum Detail</h4></div>
            
            <div class="clearfix"></div>

            <div class="col-md-12 rsp_tbl">
                <div class="tab-content">
                    <div id="home" class="tab-pane fade active in">
                        <table class="table">
                            <thead class="tbl_head">
                                <tr>
                                    <th><span><?php echo $comments[count($comments)-1]['description']; ?></span></th>
                                </tr>
                            </thead>
                        </table>
                        
                        <div class="ed_job_bt">
                                <a  href="javascript:" 
                                    data-toggle="modal" 
                                    data-target="#addDiscussionForumComment" 
                                    class="add-discussion-comment" 
                                    data-options='{"discussionId":"<?php echo $comments[count($comments)-1]['id']; ?>"}' class="add-new-set"><button type="button">Add Comment</button></a></div>
                        <div class="clearfix"></div>
                        </br>
                        
                        <table class="table" id="sorting">
                            <thead class="tbl_head">
                                <tr>
<!--                                    <th><span><?php //echo $comments[0]['description']; ?></span></th>-->
                                    <th><span>Comments</span></th>
                                    <th><span>Created By</span></th>
                                    <th><span>Date Time</span></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 0;
                                foreach ($comments as $k=>$item):
                                    //if ($k < 1) continue;
                                    ?>
                                    <tr <?php if ($i % 2 != 0) echo 'class="tbl_bg"'; ?>>
                                        <td><?php echo $item['description']; ?></td>
                                        <td><?php echo $item['fullname']; ?></td>
                                        <td><?php echo date('M, d Y,  h:i:s A', strtotime($item['createdAt'])); ?></td>
                                        <td>
                                            <?php if($item['type']=='2') { ?><a href="javascript:" class="delete-comment" id="<?php echo $item['id']; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/del.png"></a><?php } ?>
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                endforeach;
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            

            <div class="service_content " id="setPaging">
                <?php if ($totalCount > $itemsPerPage): ?>
                <ul class="service_content_last_list">
                    <?php if ($page > 1): ?>
                        <p class="pagi_img"><a href="<?php echo base_url(); ?>knowledge/list/<?php echo $page - 1; ?>" ><img src="<?php echo base_url(); ?>/theme/firstJob/image/pagination_img2.png"></a></p>    
                    <?php endif; ?>
                    <input type="hidden" name="page_no" value="1" id="page_no"> 
                    <?php for ($i = 1; $i <= $count; $i++): ?>
                        <li><a href="<?php echo base_url(); ?>knowledge/list/<?php echo $i; ?>"  <?php if ($i == $page): ?>style="color: orange;"<?php endif; ?>><?php echo $i; ?></a></li>
                    <?php endfor; ?>
                    <?php if ($page < $count): ?>
                        <p class="pagi_img"><a href="<?php echo base_url(); ?>knowledge/list/<?php echo $page + 1; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/pagination_img1.png"></a></p>            
                    <?php endif; ?>
                </ul>
                <?php endif; ?>
            </div>

        </div>
    </div>
</div>


<!-- PopUp for add question to interview set  -->
<div id="addDiscussionForumComment" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <form class="form-horizontal form_save" action="<?php echo base_url(); ?>discussion/comment" method="post" id="DiscussionForumTopicForm" >
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Discussion Forum Comment</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger hidden" id="discussionForumTopicAlert"></div>
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-3">Add Comment</label>
                        <div class="col-xs-9" id="discussionForumTopic"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary add-discussion-forum-comment">Update</button>
                </div>
            </div>
        </form>
    </div>
</div>




<script>
    $(document).on('click', '.add-discussion-comment', function () {
        var discussionId  = $(this).data('options').discussionId;   
        $('#discussionForumTopicAlert').addClass('hidden');
        var data = '<input type="text"  class="form-control" value="" id="discussion" name="discussion" required="required"><input type="hidden" class="form-control" value="'+discussionId+'" id="discussionId" name="discussionId">';
        $('#discussionForumTopic').html(data);
    });

    $(document).on('click', '.add-discussion-forum-comment', function () {
        var discussion    = $('#discussion').val();
        var discussionId  = $('#discussionId').val();
        var url         = '<?php echo base_url(); ?>';
        //alert(discussion);
        if (discussion=='') {
            $('#discussionForumTopicAlert').html('Please Enter Comment.');
            $('#discussionForumTopicAlert').removeClass('hidden');
        } else {
            $('#DiscussionForumTopicForm').submit();
        }
    });
    
    
      
    $(function () {
        var settingTableRowColor = function (tableName) {
            $('#' + tableName + ' tbody tr').removeAttr('class');
            $('#' + tableName + ' tbody tr:odd').attr('class', 'tbl_bg');
        }
        settingTableRowColor('sorting');
        $('.sort-header').on('click', function () {
            settingTableRowColor('sorting');
        });
        $('.delete-comment').on('click', function () {
            var url         = '<?php echo base_url(); ?>';
            var commentId   = $(this).attr('id');
            var accessToken = $('#accessToken').val();
            bootbox.confirm('Are you sure you want to delete?', function (result) {
                //alert(result);
                if (result) {
                    $.post(url + 'discussion/deletecomment', {id: commentId, accessToken: accessToken}, function (data) {
                        //bootbox.alert(data);
                        location.reload();
                    });
                }
            });

        });
    });
</script>
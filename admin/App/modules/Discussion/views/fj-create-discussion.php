<?php
if (isset($discussion) && $discussion != NULL) {
    //echo '<pre>';print_r($interview);  
}
?>
<div id="page-content-wrapper">
    <?php if (isset($message)): ?>
    <div class="alert alert-success">
        <?php echo $message; ?>
    </div>
    <?php endif; ?>
    <?php if (isset($error)): ?>
    <div class="alert alert-danger">
        <?php foreach ($error as $item): ?>
            <?php echo $item; ?>
        <?php endforeach; ?>
    </div>
    <?php endif; ?>
    
    <div class="container-fluid whitebg">
        <div class="row">
            <div class="col-md-12 padd_tp_bt_20">
                <h4><img src="<?php echo base_url(); ?>/theme/firstJob/image/arrow_head.png" class="prev-page"> Add Discussion Topic</h4>
                <form class="form-horizontal form_save" method="post" action="" enctype="multipart/form-data" id="discussionset">
                    <div class="form-group">
                        <label for="inputPassword" class="control-label col-xs-2 required-field">Discussion Topic</label>
                        <div class="col-xs-10  ques-title">
                            <select data-placeholder="Type Discussion Topic" style="width: 350px; display: none;" class="chosen-select sel_sty_padd discussiontopic" tabindex="-1" name="discussion">
                                <option value=""></option>
                            </select>
                            <input type="text" data-validation="discussion" style="height: 0px; width: 0px; visibility: hidden; " />
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-xs-offset-2 col-xs-10 no_padding">
                        <input type="hidden" name="accessToken" value="<?php echo $token; ?>"/>
                        <button type="submit" class="Save_frm pull-right save-interview" value="testing">SAVE</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>


<script src="<?php echo base_url(); ?>theme/firstJob/js/chosen.jquery.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>theme/firstJob/js/prism.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
    var config = {
        '.chosen-select':           {},
        '.chosen-select-deselect':  {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results':{no_results_text: ''},
        '.chosen-select-width':     {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }

    $(function () {
        $("#datepicker-19").datepicker();
    });
    $(document).on('blur', '.chosen-search input', function (evt) {
        //alert($('.no-results').length);
        if ($('.no-results').length == 1) {
            //alert($('.chosen-choices .search-field input').val());
            //$('.chosen-select');
            $(this).parents('.ques-title').find('.chosen-select').append('<option>' + $(evt.target).val() + '</option>');
            $(this).parents('.ques-title').find('.chosen-select').val($(evt.target).val());
            $(this).parents('.ques-title').find('.chosen-select').trigger('chosen:updated');

        }

    });
</script>

<style>
    .no-results {
        display: none!important;
    }
    .chosen-single {
        background :none!important;
        border-radius:0px !important;
        height: 34px!important;
    }
</style>




<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script>
// Add validator For Interview Assessment
$.formUtils.addValidator({
    name : 'discussion',
    validatorFunction : function(value, $el, config, language, $form) {
      
        var hasNoValue;
        $('.discussiontopic').each(function(i) {
            if ($(this).val() == '') {
                  hasNoValue = true;
            }
        });
        if (hasNoValue) {
            return false;
        }
        else {
            return true;
        }
        //return parseInt(value, 10) % 2 === 0;
    },
    errorMessage : 'This is a required field'
});

$.validate({
    form: '#discussionset',
    modules: 'file',
    onError: function ($form) {
        //alert($('.setquestion').val());
        //alert($('.setInterviewType').html());
        return false;
    },
    onSuccess: function ($form) {
        return true;
    },
});
</script>

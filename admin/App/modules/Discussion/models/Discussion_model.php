<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Discussion Model
 * Description : Handle all the CRUD operation for Discussion
 * @author Synergy
 * @createddate : Oct 3, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 15, 2017
 */
class Discussion_model extends CI_Model {
    
    /**
     * Defining static variable to store tables name
     */
    static $tableDiscussion = "fj_discussionForum";
    static $tableUsers      = "fj_users";
    
    /**
     * Responsable for auto load the the session and pagination library
     * @return void
     */
    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('pagination');
    }
    
    /**
     * Discription : Return all the Discussion Forum or topic.
     * Author : Synergy
     * @param $limit, $offset
     * @return array of discussion forum data.
     */

    function listDiscussionForum($limit, $offset, $serachColumn = NULL, $searchText = NULL, $createdBy = NULL) {
        try {
            $currentDate    = date('Y-m-d h:i:s');
            $sql            = " SELECT D.*, U.fullname
                                FROM ".Discussion_model::$tableDiscussion." D
                                JOIN ".Discussion_model::$tableUsers."      U
                                WHERE D.userId=U.id AND D.status='1' AND D.discussionReplyId='0'
                                ORDER BY createdAt DESC";

            if (isset($offset)) {
                $sql .= " limit " . $offset . ", " . $limit . "";
            }

            $query  = $this->db->query($sql);
            $result = $query->result();

            if (!$result) {
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Discription : Return all the comments on a specific Discussion Forum
     * Author : Synergy
     * @param int $id.
     * @return array of data.
     */
    function getDiscussionComments($id) {
        try {          
            $sql    = " SELECT D.*, U.fullname
                        FROM ".Discussion_model::$tableDiscussion." D
                        JOIN ".Discussion_model::$tableUsers."      U
                        WHERE D.userId=U.id AND D.status='1' AND (D.id='$id' OR D.discussionReplyId='$id')
                        ORDER BY id DESC";
            $query  = $this->db->query($sql);
            $result = $query->result_array();
            if (count($result) == 0) {
                return false;
            }
            return $result;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    
    /**
     * Discription : Update Discussion forum
     * Author : Synergy
     * @param array $data, int $id, string $comment
     * @return array the interviews data.
     */
    function updateDiscussionForum($data, $id, $comment='') {
        try {
            $this->db->where('id', $id);
            $resp = $this->db->update(Discussion_model::$tableDiscussion, $data);
            if (!$resp) {
                throw new Exception('Unable to update data.');
                return false;
            }
            if($comment=='comment'){
                $this->db->where('discussionReplyId', $id);
                $respcomment = $this->db->update(Discussion_model::$tableDiscussion, $data);
                if (!$respcomment) {
                    throw new Exception('Unable to update data.');
                    return false;
                }
            }
            return $id;
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Discription : Add new discussion forum(topic)
     * Author : Synergy
     * @param array $data.
     * @return array the interviews data.
     */
    function addDiscussionForumTopic($data) {
        $resp           = $this->db->insert(Discussion_model::$tableDiscussion, $data);
        $discussionId   = $this->db->insert_id();
        if ($resp) {
            return $discussionId;
        } else {
            return false;
        }
    }
}



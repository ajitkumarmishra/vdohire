<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Discussion Controller
 * Description : Used to handle discussion related functionality like add discussion, edit discussion etc
 * @author : Synergy
 * @createddate : Nov 15, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 15, 2017
 */
class Discussion extends MY_Controller {
    /**
     * Responsable for auto load the the discussion_model,form_validation,session and email library
     * Sets the default timezone Asia/Calcutta
     * Check Uri Segment to display specific page role wise
     * @return void
     */
    function __construct() {
        parent::__construct();
        $this->load->model('discussion_model');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('email');
        $this->load->helper('fj');
        date_default_timezone_set("Asia/Calcutta"); 
        $token = $this->config->item('accessToken');
        if (isset($_POST) && $_POST != NULL) {
            if ($token != $_POST['accessToken']) {
                //  echo 'You do not have pemission.';
                //exit;
            }
        }

        /**********Check any type of fraund activity*******/
        if ($this->session->userdata('logged_in')) {
            $userRealData   = $this->session->userdata['logged_in'];
            $uriStringSegment = $this->uri->uri_string();
            
            if($userRealData->role == 1) {
                $portal = 'SuperAdminPortal';
            }
            else{
                $portal = '';
            }

            if($uriStringSegment == 'discussion/list') {
                if(!checkUserAccessForWebPage($userRealData->id, $userRealData->role, $portal, 'discussion/list')) {
                    $this->session->sess_destroy();
                    $insertFaultData['userId'] = $userRealData->id;
                    $insertFaultData['comment'] = 'This user try to access this this page';
                    $insertFaultData['pagePath'] = 'discussion/list';
                    $this->db->insert('fj_user_access_log', $insertFaultData);
                    redirect(base_url('users/login'));
                }
            }
        } 
        /****************End of handle fraud activity****************/
    }
    
    
    /**
     * Description : List all the Discussion Forum created by Super admin or App User
     * Author : Synergy
     * @param page number
     * @return array of data 
     */
    function listDiscussionForum($page) {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }

        $userData               = $this->session->userdata['logged_in'];
        $data['page']           = $page;
        $resp['error']          = 0;
        $serachColumn           = $this->input->post('serachColumn', true);
        $searchText             = $this->input->post('serachText', true);
        $token                  = $this->config->item('accessToken');
        $fromEmail              = $this->config->item('fromEmail');
        $itemsPerPage           = $this->config->item('itemsPerPage');
        $data['itemsPerPage']   = $itemsPerPage;
        $createdBy = $userData->id;
        if (!$page) {
            $page = 0;
        } else {
            $page = $page - 1;
        }
        $offset = $page * $itemsPerPage;
        try {
            $result                         = $this->discussion_model->listDiscussionForum($itemsPerPage, $offset, $serachColumn, $searchText, $createdBy);
            $completeResult                 = $this->discussion_model->listDiscussionForum(NULL, NULL, NULL, NULL, $createdBy);
            $count                          = ceil(count($completeResult) / $itemsPerPage);
            $data['count']                  = $count;
            $data['totalCount']             = count($completeResult);
            $data['contentDiscussionForum'] = $result;
            $data['main_content']           = 'fj-discussion-listing';
            $data['searchText']             = $searchText;
            $data['serachColumn']           = $serachColumn;
            $data['token']                  = $token;

            $this->load->view('fj-mainpage', $data);
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }
    
    /**
     * Description : List all the comments on a particular Discussion Forum
     * Author : Synergy
     * @param id(discussionId)
     * @return array of data 
     */
    function getDiscussionComments($id) {
        $token                  = $this->config->item('accessToken');
        $data['token']          = $token;
        $data['main_content']   = 'fj-view-discussion';
        $data['comments']         = $this->discussion_model->getDiscussionComments($id);
        $this->load->view('fj-mainpage', $data);
    }
    
    /**
     * Description : Delete a particular comment on of specific Discussion Forum
     * Author : Synergy
     * @param id(discussionId)
     * @return string 
     */
    function deleteDiscussionComment() {
        $resp['error']  = 0;
        $token          = $this->config->item('accessToken');
        $discussionId   = $this->input->post('id', true);
        if (!$discussionId) {
            echo $error = 'Undefined Comment!';
            exit;
        }
        if ($resp['error'] == 0) {
            $data['status'] = 3;
            $resp = $this->discussion_model->updateDiscussionForum($data, $discussionId);
            if ($resp) {
                echo "Deleted successfully";
            }
        }
    }

    /**
     * Description : Delete a particular Discussion 
     * Author : Synergy
     * @param id(discussionId)
     * @return string 
     */
    function deleteDiscussion() {
        $resp['error']  = 0;
        $token          = $this->config->item('accessToken');
        $discussionId   = $this->input->post('id', true);
        if (!$discussionId) {
            echo $error = 'Undefined Comment!';
            exit;
        }
        if ($resp['error'] == 0) {
            $data['status'] = 3;
            $resp = $this->discussion_model->updateDiscussionForum($data, $discussionId, 'comment');
            if ($resp) {
                echo "Deleted successfully";
            }
        }
    }
    
    /**
     * Description : Update a specific Discussion Forum 
     * Author : Synergy
     * @param discussion, discussionId
     * @return string 
     */
    function editDiscussionForumTopic() {
        $resp['error']  = 0;
        $token          = $this->config->item('accessToken');
        $discussion     = $this->input->post('discussion', true);
        $discussionId   = $this->input->post('discussionId', true);
        if ($discussion=='' || $discussionId=='') {
            echo $error = 'Undefined Topic!';
            exit;
        }
        if ($resp['error'] == 0) {
            $data['description'] = $discussion;
            $resp = $this->discussion_model->updateDiscussionForum($data, $discussionId);
            if ($resp) {
                redirect($_SERVER['HTTP_REFERER']);
            }
        }
    }

    /**
     * Description : Add a new discussion
     * Author : Synergy
     * @param discussion(Name of discussion of topic)
     * @return string 
     */
    function addDiscussionForumTopic() {

        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }

        $token      = $this->config->item('accessToken');
        $userData   = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        }

        $info['main_content']   = 'fj-create-discussion';
        $info['token']          = $token;
        
        if (isset($_POST) && $_POST != NULL) {
            $this->form_validation->set_rules('discussion',    'Discussion Topic',      'trim|required');            
            $description   = $this->input->post('discussion',    true);
            if ($this->form_validation->run() == false) {
                $resp['error']  = validation_errors();
            }
            if (!$resp['error']) {
                $data['description']    = $description;
                $data['createdAt']      = date('Y-m-d h:i:s');
                $data['userId']         = $userId;
                $data['type']           = '1';
                $data['status']         = '1';

                try {
                    $resp = $this->discussion_model->addDiscussionForumTopic($data);                    
                    redirect(base_url('discussion/list'));
                } catch (Exception $e) {
                    $resp['error'] = $e->getMessage();
                }
            } else {
                $info['error']      = $resp;
                $info['discussion'] = $this->input->post();
                $this->load->view('fj-mainpage', $info);
            }
        } 
        else {
            $this->load->view('fj-mainpage', $info);
        }
    }

    /**
     * Description : Add a new comment on a particular discussion(topic)
     * Author : Synergy
     * @param discussion(comment text), discussionId
     * @return string 
     */
    function addComment() {
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('users/login'));
        }

        $token      = $this->config->item('accessToken');
        $userData   = $this->session->userdata['logged_in'];
        if (isset($userData->id) && $userData->id != NULL) {
            $userId = $userData->id;
        }      
        if (isset($_POST) && $_POST != NULL) {
            $this->form_validation->set_rules('discussion',    'Discussion Comment',      'trim|required');            
            $description    = $this->input->post('discussion',      true);
            $discussionId   = $this->input->post('discussionId',    true);
            if ($this->form_validation->run() == false) {
                $resp['error']  = validation_errors();
            }
            if (!$resp['error']) {
                $data['description']        = $description;
                $data['discussionReplyId']  = $discussionId;
                $data['createdAt']          = date('Y-m-d h:i:s');
                $data['userId']             = $userId;
                $data['type']               = '2';
                $data['status']             = '1';

                try {
                    $resp = $this->discussion_model->addDiscussionForumTopic($data);    
                    redirect($_SERVER['HTTP_REFERER']);
                } catch (Exception $e) {
                    $resp['error'] = $e->getMessage();
                }
            } else {
                $info['error']      = $resp;
                $this->load->view('fj-mainpage', $info);
            }
        }
    }
}
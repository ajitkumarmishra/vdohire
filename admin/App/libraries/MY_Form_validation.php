<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 * CodeIgniter MongoDB Library
 *
 * A library to interact with the NoSQL database MongoDB.
 * For more information see http://www.mongodb.org
 *
 * @package		CodeIgniter
 * @author		Alessandro Arnodo | a.arnodo@gmail.com | @vesparny
 * @copyright	Copyright (c) 2012, Alessandro Arnodo.
 * @license		http://www.opensource.org/licenses/mit-license.php
 * @link
 * @version		Version 1.1.0
 *
 */

/**
 * MY_Form_validation
 *
 * Override core methods forcing them to interact with  MongoDB.
 * @since v1.1
 */
class MY_Form_validation extends CI_Form_validation {

    public function __construct($rules = array()) {
        log_message('debug', '*** Hello from MY_Session ***');
        $this->CI = & get_instance();
        //$this->CI->load->library("cimongo/cimongo");
        parent::__construct($rules);
    }

    /**
     * is_unique
     *
     */
    public function is_unique($str, $field) {
        list($table, $field) = explode('.', $field);
        $query = $this->CI->db->limit(1)->get_where($table, array($field => $str));

        return $query->num_rows() === 0;
    }

    /**
     * 
     * @param string $phone
     * @return boolean
     */
    function isValidMobile($phone) {
        if (preg_match("/^[0-9]{10}$/", $phone)) {
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * 
     * @param string $password
     * @return boolean
     */
    function isValidPassword($password) {
        $uppercase = preg_match('@[A-Z]@', $password);
        $lowercase = preg_match('@[a-z]@', $password);
        $number = preg_match('@[0-9]@', $password);
        if (!$uppercase || !$lowercase || !$number || strlen($password) < 8) {
            return false;
        } else {
            return true;
        }
    }
    
    /**
     * 
     * @param string $phone
     * @return boolean
     */
    function isFutureDate($date) {
        $currentDate = strtotime(date('Y-m-d h:i:s'));
        if (strtotime($date) > $currentDate) {
            return true;
        } else {
            return false;
        }
    }

}

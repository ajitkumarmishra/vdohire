<?php
if (!function_exists('getRoleById')) {

    /**
     * getRoleById function
     * Gets role of a user.
     * @params int $id the role id.
     * @return string the role.
     * 
     */
    function getRoleById($id) {
        $CI = &get_instance();
        $CI->db->where('id', $id);
        $query = $CI->db->get('fj_roles');
        if ($query->num_rows() > 0)
            return $query->row()->name;
    }

}
if (!function_exists('addLog')) {

    /**
     * AddLog function
     * Adds log detail.
     * @param int $userId the user id.
     * @param string $activity the activity name.
     * @param int $userCreated the created user id.
     */
    function addLog($userId, $activity, $userCreated = NULL) {
        $ci = &get_instance();
		
        $data['userId'] = $userId;
        $data['activity'] = $activity;
        $data['activityTime'] = date('Y-m-d h:i:s');
        $data['userCreated'] = $userCreated;
        $data['ipAddress'] = $_SERVER['REMOTE_ADDR'];
        $ci->db->insert('fj_logs', $data);
        //$insid=$ci->db->insert_id();
    }

}
if (!function_exists('getJwtToken')) {
	
function getJwtToken($userId) {
		$CI = & get_instance();
		$CI->load->helper('jwt');
        $token = array();
        $token['id'] = $userId;
        $jwtKey = $CI->config->item('jwtKey');
        $secretKey = base64_decode($jwtKey);
        $signatureAlgo = $CI->config->item('signatureAlgorithm');
        return JWT::encode($token, $secretKey, $signatureAlgo);
    }
}

if (!function_exists('getValueByToken')) {
    /**
     * GetValueByToken function
     * Gets value by token
     * @param string $jwtToken
     * @return string
     */
    function getValueByToken($jwtToken) {
		$CI = & get_instance();
		$CI->load->helper('jwt');
        $jwtKey = $CI->config->item('jwtKey');
        $secretKey = base64_decode($jwtKey);
        $signatureAlgo = $CI->config->item('signatureAlgorithm');
        $token = JWT::decode($jwtToken, $secretKey, $signatureAlgo);
        return $token->id;
    }
}

if (!function_exists('getUserPermissions')) {

    /**
     * GetUserPermissions function
     * Gets user permissions.
     * @param int $userId the user id.
     * @return array $results the array.
     */
    function getUserPermissions($userId) {
        $ci = &get_instance();
        $ci->db->select('fj_permissions.id');
        $ci->db->from('fj_permissions');
        $ci->db->join('fj_userPermission', 'fj_permissions.id=fj_userPermission.permissionId', 'left');
        $ci->db->where('fj_userPermission.userId', $userId);
        $query = $ci->db->get();
        $results = $query->result_array();
        return $results;
    }

}


if (!function_exists('getPermissions')) {

    /**
     * GetPermissions function
     * Get all pemissions.
     * @return array $results the permissions data.
     */
    function getPermissions() {
        $ci = &get_instance();
        $ci->db->select('*');
        $ci->db->from('fj_permissions');
        $ci->db->where('fj_permissions.parent', 0);
        $ci->db->where('fj_permissions.id in (1,2,3,4,5,7)');
        $query = $ci->db->get();
        $results = $query->result();
        return $results;
    }

}

if (!function_exists('getMenuForSideBar')) {

    /**
     * GetPermissions function
     * Get all pemissions.
     * @return array $results the permissions data.
     */
    function getMenuForSideBar() {
		
        $ci = &get_instance();
        $ci->db->select('*');
        $ci->db->from('fj_permissions');
        $ci->db->where('fj_permissions.parent', 0);
        $query = $ci->db->get();
        $results = $query->result();
		
        return $results;
    }
}


if (!function_exists('dashboardUserPermissions')) {

    /**
     * GetPermissions function
     * Get all pemissions.
     * @return array $results the permissions data.
     */
    function dashboardUserPermissions($userId) {
		
        $ci = &get_instance();
        $ci->db->select('GROUP_CONCAT(DISTINCT permissionId SEPARATOR ",") as permissionIds');
        $ci->db->from('fj_userPermission');
        $ci->db->where('fj_userPermission.userId', $userId);
        $query = $ci->db->get();
        $results = $query->row();
        return $results;
    }
}

if (!function_exists('checkUserInLiveJob')) {

    /**
     * checkUserInLiveJob function
     * Checks user is in live job.
     * @return int.
     */
    function checkUserInLiveJob($userId) {
        $ci = &get_instance();
        $ci->db->select('*');
        $ci->db->from('fj_jobPanel');
        $ci->db->where('fj_jobPanel.userId', $userId);
        $query = $ci->db->get();
        $result = $query->row_array();
        if (!$result['jobId']) {
            return false;
        } else {
            $isExpired = isJobExpired($result['jobId']);
            return $isExpired;
        }
    }

}

if (!function_exists('isJobExpired')) {

    /**
     * isJobExpired function
     * Checks job is expired or not.
     * @return int.
     */
    function isJobExpired($jobId) {
        $ci = &get_instance();
        $ci->db->select('*');
        $ci->db->from('fj_jobs');
        $ci->db->where('id', $jobId);
        $query = $ci->db->get();
        $result = $query->row_array();
        $currentTime = date('Y-m-d h:i:s');
        if ($result['openTill'] < $currentTime) {
            return false;
        } else {
            return true;
        }
    }

}

if (!function_exists('getRoleIdByUser')) {

    /**
     * getRoleById function
     * Gets role of a user.
     * @params int $id the role id.
     * @return string the role.
     * 
     */
    function getRoleIdByUser($id) {
        $CI = &get_instance();
        $CI->db->where('id', $id);
        $query = $CI->db->get('fj_users');
        if ($query->num_rows() > 0)
            return $query->row()->role;
    }

}

if (!function_exists('getCourses')) {

    /**
     * getCourses function
     * Gets courses.
     * @return array.
     * 
     */
    function getCourses() {
        $CI = &get_instance();
        $query = $CI->db->get('fj_courses');
        $results = $query->result_array();
        return $results;
    }

}

if (!function_exists('getCityState')) {

    /**
     * getCityState function
     * Gets courses.
     * @return array.
     * 
     */
    function getCityState($id) {
        $CI = &get_instance();
        $CI->db->where('id', $id);
        $query = $CI->db->get('fj_cityState');
        $row = $query->row_array();
        return $row;
    }

}

if (!function_exists('getQualificationById')) {

    /**
     * getCityState function
     * Gets courses.
     * @return array.
     * 
     */
    function getQualificationById($id) {
        $CI = &get_instance();
        $CI->db->where('id', $id);
        $query = $CI->db->get('fj_courses');
        $row = $query->row_array();
        return $row['name'];
    }

}

if (!function_exists('getGenderById')) {

    /**
     * getCityState function
     * Gets courses.
     * @return array.
     * 
     */
    function getGenderById($id) {
        if ($id == 1) {
            return "Male";
        } else {
            return "Female";
        }
    }

}

if (!function_exists('getCities')) {

    /**
     * getCityState function
     * Gets courses.
     * @return array.
     * 
     */
    function getCities() {
        $CI = &get_instance();
        $query = $CI->db->get('fj_cityState');
        $result = $query->result_array();
        return $result;
    }

}

if (!function_exists('getAssessments')) {

    /**
     * getAssessments function
     * Get assessments.
     * @return array.
     * 
     */
    function getAssessments() {
        $CI = &get_instance();
        $query = $CI->db->get('fj_assessments');
        $result = $query->result_array();
        return $result;
    }

}

if (!function_exists('getInterviews')) {

    /**
     * getInterviews function
     * Get interviews.
     * @return array.
     * 
     */
    function getInterviews() {
        $CI = &get_instance();
        $query = $CI->db->get('fj_interviews');
        $result = $query->result_array();
        return $result;
    }

}

if (!function_exists('getSubUsers')) {

    /**
     * getSubUsers function
     * Get sub users.
     * @return array.
     * 
     */

    function getSubUsers($id) {
        $CI = &get_instance();
        //$CI->db->where('createdBy', $id);
        $corporate  = $CI->db->query("SELECT * FROM fj_users WHERE id='$id'");        
        $row        = $corporate->row_array();
        $role       = $row['role'];
        
        
        if($role=='1') {
            $sql = "SELECT 
                        U.*
                    FROM
                        fj_users		U
                    JOIN
                        fj_userPermission	UP
                    WHERE
                        U.id=UP.userId		AND
                        U.createdBy   IN (
                                        SELECT
                                            id
                                        FROM
                                            fj_users
                                        WHERE
                                            role='1'
                                        )       AND
                        U.role='4' 		AND
                        status!=3		AND
                        (UP.permissionId='5')";
            $query = $CI->db->query($sql);
            $result = $query->result_array();
        }      
        else if($role=='2') {
            /*$sql = "SELECT 
                        U.*
                    FROM
                        fj_users		U
                    JOIN
                        fj_userPermission	UP
                    WHERE
                        U.id=UP.userId		AND
                        U.createdBy='".$id."' 	AND
                        U.role='4' 		AND
                        status!=3		AND
                        (UP.permissionId='5')";*/

            $sql = "SELECT * FROM fj_users WHERE (createdBy='".$id."' AND status='1') OR id='".$id."'";

            $query = $CI->db->query($sql);
            $result = $query->result_array();
        }
        else if($role=='4' || $role=='5') {            
            $subUserQry  = $CI->db->query("SELECT * FROM fj_users WHERE id='$id'");        
            $rowUser    = $subUserQry->row_array();
            //print_r($rowUser);
            /*$sql = "
                    SELECT 
                        U.*
                    FROM
                        fj_users		U
                    JOIN
                        fj_userPermission	UP
                    WHERE
                        U.id=UP.userId                          AND
                        U.createdBy='".$rowUser['createdBy']."' AND
                        U.role='4'                              AND
                        status!=3                               AND
                        (UP.permissionId='5')";
                        return $sql;exit;*/
            $sql = "SELECT * FROM fj_users WHERE (createdBy='".$rowUser['createdBy']."' AND status='1') OR id='".$rowUser['createdBy']."'";
            $query = $CI->db->query($sql);
            $result = $query->result_array();            
        }
        return $result;
    }

}

if (!function_exists('getUserById')) {

    /**
     * getUserById function
     * Gets role of a user.
     * @params int $id the role id.
     * @return string the role.
     * 
     */
    function getUserById($id) {
        $CI = &get_instance();
        $CI->db->where('id', $id);
        $query = $CI->db->get('fj_users');
        if ($query->num_rows() > 0)
            return $query->row_array();
    }

}

if (!function_exists('getLocationsByJob')) {

    /**
     * getLocationsByJob function
     * @params int $id the role id.
     * @return the Array.
     * 
     */
    function getLocationsByJob($id) {
        $CI = &get_instance();
        $CI->db->where('jobId', $id);
        $query = $CI->db->get('fj_jobLocations');
        if ($query->num_rows() > 0)
            return $query->result_array();
    }

}

if (!function_exists('getLocationById')) {

    /**
     * getLocationById function
     * @params int $id the role id.
     * @return the Array.
     * 
     */
    function getLocationById($id) {
        $CI = &get_instance();
        $CI->db->where('id', $id);
        $query = $CI->db->get('fj_cityState');
        if ($query->num_rows() > 0)
            return $query->row()->city;
    }

}

if (!function_exists('getPanelByJob')) {

    /**
     * getPanelByJob function
     * @params int $id the role id.
     * @return the Array.
     * 
     */
    function getPanelByJob($id, $level) {
        $CI = &get_instance();
        $CI->db->where('jobId', $id);
        $CI->db->where('level', $level);
        $query = $CI->db->get('fj_jobPanel');
        if ($query->num_rows() > 0) {
            $results = $query->result_array();
            $j = 0;
            foreach ($results as $item) {
                $formattedResult[$j] = $item['userId'];
                $j++;
            }
            return $formattedResult;
        }
    }

}

if (!function_exists('getQualificationsByJob')) {

    /**
     * getQualificationsByJob function
     * @params int $id the role id.
     * @return the Array.
     * 
     */
    function getQualificationsByJob($id) {
        $CI = &get_instance();
        $CI->db->where('jobId', $id);
        $query = $CI->db->get('fj_jobQualifications');
        //if ($query->num_rows() > 0)
        $results = $query->result_array();
        $j = 0;

        if($results) {
            foreach ($results as $item) {
                $formattedResult[$j] = $item['qualificationId'];
                $j++;
            }
            return $formattedResult;
        } else {
            return array();
        }
        
    }

}

// if (!function_exists('getFjQuestions')) {

//     /**
//      * getFjQuestions function
//      * @return the Array.
//      * 
//      */
//     function getFjQuestions($quesType = null) {
//         $CI = &get_instance();
//         //$CI->db->where('jobId', $id);
//         $sql = 'select q.* from fj_question q left join fj_users u on u.id=q.createdBy where q.status != 3 AND u.role = 1';
//         if( $quesType != "" && $quesType != null ){
//             $sql .=" and q.type='".$quesType."'";
//         }
//         //echo $sql;
//         $query = $CI->db->query($sql);
//         if ($query->num_rows() > 0)
//             return $query->result_array();
//     }
// }


if (!function_exists('getFjQuestions')) {

    /**
     * getFjQuestions function
     * @return the Array.
     * 
     */
    function getFjQuestions($quesType = null) {
        $CI = &get_instance();
        //$CI->db->where('jobId', $id);
        $sql = 'select q.* from fj_question q left join fj_users u on u.id=q.createdBy where q.status != 3 AND u.role = 1 ';
        if( $quesType != "" && $quesType != null ){
            $sql .=" and q.type='".$quesType."'";
        }
        $sql .= " ORDER BY q.title";
        $query = $CI->db->query($sql);
        if ($query->num_rows() > 0)
            return $query->result_array();
    }

}


if (!function_exists('getQuestionsByInterview')) {

    /**
     * getQuestionsByInterview function
     * @return the Array.
     * 
     */
    function getQuestionsByInterview($id) {
        $CI = &get_instance();
        //$CI->db->where('jobId', $id);
        $sql = "select * from fj_question q left join fj_interviewQuestions iq on iq.questionId = q.id where iq.interviewId = '" . $id . "' and q.status!=3 order by iq.id";
        $query = $CI->db->query($sql);
        if ($query->num_rows() > 0)
            return $query->result_array();
    }

}

if (!function_exists('insertNotificationForTopBarInAdmin')) {

    /**
     * getQuestionsByInterview function
     * @return the Array.
     * 
     */
    function insertNotificationForTopBarInAdmin($userId, $notifyMessage, $notifyMessageType, $createdByUserId) {
        $ci = &get_instance();
        $data['userId'] = $userId;
        $data['userIdCreatedBy'] = $createdByUserId;
        $data['notifyMessage'] = $notifyMessage;
        $data['notifyMessageType'] = $notifyMessageType;
        $data['createdAt'] = date('Y-m-d H:i:s');
        $ci->db->insert('fj_notification', $data);
    }

}

if (!function_exists('getInterviewSetType')) {

    /**
     * getQuestionsByInterview function
     * @return the Array.
     * 
     */
    function getInterviewSetType($id) {
        if ($id == 1) {
            $type = 'Video';
        } else if ($id == 2) {
            $type = 'Audio';
        } else {
            $type = 'Text';
        }
        return $type;
    }

}

if (!function_exists('getInterviewSetByType')) {

    /**
     * getInterviewSetByType function
     * @return the Array.
     * 
     */
    function getInterviewSetByType($type, $createdBy) {
        $CI         = &get_instance();
        $sqlusers   = "select * from fj_users where id='".$createdBy."'";
        $queryusers = $CI->db->query($sqlusers);
        $resultsusers = $queryusers->row_array();
        //print_r($resultsusers); exit;
        if($resultsusers['role']=='1') {
            $sql    = "SELECT * FROM fj_interviews WHERE createdBy='".$createdBy."' AND status!=3";//die;
            $query  = $CI->db->query($sql);
            $results = $query->result_array();
            return $results;
        }
        else if($resultsusers['role']=='2') {
            $sql    = "SELECT * FROM fj_interviews WHERE createdBy IN (SELECT id FROM fj_users where createdBy='".$createdBy."' OR id='".$createdBy."') AND status!=3";//die;
            $query  = $CI->db->query($sql);
            $results = $query->result_array();
            return $results;
        }
        else {
            $sqlCorpusers       = "select * from fj_users where id='".$resultsusers['createdBy']."'";
            $queryCorpusers     = $CI->db->query($sqlCorpusers);
            $resultsCorpusers   = $queryCorpusers->row_array();
            
            $sql    = "SELECT * FROM fj_interviews WHERE createdBy IN (SELECT id FROM fj_users where createdBy='".$resultsCorpusers['id']."' OR id='".$resultsCorpusers['id']."') AND status!=3";//die;
            $query  = $CI->db->query($sql);
            $results = $query->result_array();
            return $results;
        }
        //echo "<pre>";print_r($results); die();
    }

}

if (!function_exists('getAssessmentSetByType')) {

    /**
     * getAssessmentSetByType function
     * @return the Array.
     * 
     */
    function getAssessmentSetByType($type, $createdBy) {
        $CI         = &get_instance();
        $sqlusers   = "select * from fj_users where id='".$createdBy."'";
        $queryusers = $CI->db->query($sqlusers);
        $resultsusers = $queryusers->row_array();
        //print_r($resultsusers); exit;
        if($resultsusers['role']=='1') {
            $sql    = "SELECT * FROM fj_assessments WHERE createdBy='".$createdBy."' AND status!=3";//die;
            $query  = $CI->db->query($sql);
            $results = $query->result_array();
            return $results;
        }
        else if($resultsusers['role']=='2') {
            $sql    = "SELECT * FROM fj_assessments WHERE createdBy IN (SELECT id FROM fj_users where createdBy='".$createdBy."' OR id='".$createdBy."') AND status!=3";//die;
            $query  = $CI->db->query($sql);
            $results = $query->result_array();
            return $results;
        }
        else {
            $sqlCorpusers       = "select * from fj_users where id='".$resultsusers['createdBy']."'";
            $queryCorpusers     = $CI->db->query($sqlCorpusers);
            $resultsCorpusers   = $queryCorpusers->row_array();
            
            $sql    = "SELECT * FROM fj_assessments WHERE createdBy IN (SELECT id FROM fj_users where createdBy='".$resultsCorpusers['id']."' OR id='".$resultsCorpusers['id']."') AND status!=3";//die;
            $query  = $CI->db->query($sql);
            $results = $query->result_array();
            return $results;
        }
//        //echo "Type". $type; die;
//        $CI = &get_instance();
//        /*$CI = &get_instance();
//        $CI->db->where('type', $type);
//        $CI->db->where('createdBy', $createdBy);
//        $CI->db->where('status !=', 3);*/
//        $sql = "select * from fj_assessments where createdBy='".$createdBy."' AND status!=3";//die;
//        $query = $CI->db->query($sql);
//        return $results = $query->result_array();
        
        //echo "<pre>";print_r($results); die();
    }

}

if (!function_exists('checkQuestionExistInInterview')) {

    /**
     * getQuestionsByInterview function
     * @return the Array.
     * 
     */
    function checkQuestionExistInInterview($question, $interview) {
        $CI = &get_instance();
        //$CI->db->where('jobId', $id);
        $sql = "select * from fj_interviewQuestions where interviewId = '" . $interview . "' AND questionId = '" . $question . "'";
        $query = $CI->db->query($sql);
        return $query->num_rows();
    }

}

if (!function_exists('checkQuestionExistInAssessment')) {

    /**
     * getQuestionsByInterview function
     * @return the Array.
     * 
     */
    function checkQuestionExistInAssessment($question, $interview) {
        $CI = &get_instance();
        //$CI->db->where('jobId', $id);
        $sql = "select * from fj_assessmentQuestions where assessmentId = '" . $interview . "' AND questionId = '" . $question . "'";
        $query = $CI->db->query($sql);
        return $query->num_rows();
    }

}

if (!function_exists('getMyQuestionRow')) {

    /**
     * getMyQuestionRow function
     * @return the Array.
     * 
     */
    function getMyQuestionRow($fjQuestion, $index) {
        $str = '<div class="form-group" style="margin-left: 0px !important; margin-right: 0px !important;">
                            <div class="col-xs-2 ques-title">
                                <label for="inputPassword" class="control-label frm_in_padd">Type</label>
                                <select class="selectpicker questionType" name="question[' . $index . '][type]" id="type_addquestion"><option value="">Select</option>
                                    <option value="1">Video</option>
                                    <option value="3">Text</option>
                                </select>
                            </div>
                            <div class="col-xs-4 ques-title">
                                <label for="inputPassword" class="control-label frm_in_padd">Question Title</label>
                                <input type="text"  name="question[' . $index . '][title]" value="" class="form-control questionTitle" placeholder="Type a question" />
                                    ';
                                    // <select data-placeholder="Type a question" style="width: 350px;" class="sel_sty_padd questionTitle" tabindex="-1" name="question[' . $index . '][title]">
                                    //<option value=""></option>' . $fjQuestion . '
                                    $str .='<option value=""></option>
                                </select>
                            </div>

                            <div class="col-xs-3 file-section" style="display: none;">
                                <div class="col-md-12">
                                    <label for="inputPassword" class="control-label frm_in_padd" style="font-size: 12px;">Upload File(mp3, mp4)</label>
                                </div>
                                <div class="col-xs-8">
                                    <input type="text" class="form-control file-name questionFileName" readonly>
                                </div>
                                <div class="col-xs-4 no_padding browse-section">
                                    <button type="button" class="btn_upload">BROWSE</button>
                                    <input type="file" name="file_' . $index . '" class="my-question-file questionFile">
                                </div>
                            </div>

                            <div class="col-xs-3">
                                <label for="inputPassword" class="control-label frm_in_padd col-xs-12">Answer Duration <span style="font-size:10px;">(Limit 600 Sec)</span></label>
                                <div class="col-md-6 no_padding">
                                    <input type="text" class="form-control questionDuration" placeholder="" name="question[' . $index . '][duration]" maxlength="3">
                                </div>
                                <div class="col-md-3">
                                    <p class="padd_txtmin">Sec</p>
                                </div>
                            <div class="col-md-3">                                       
                                <button type="button" class="del-question del-addquestion pull-right" value="testing">&emsp;x</button>
                            </div>'
                . '</div>'
                . '</div>';
        return $str;
    }

}

if (!function_exists('getMyQuestions')) {

    /**
     * getFjQuestions function
     * @return the Array.
     * 
     */
    function getMyQuestions($id) {
        $CI = &get_instance();
        //$CI->db->where('jobId', $id);
        $sql = "select * from fj_question where createdBy='" . $id . "' and status!=3";
        $query = $CI->db->query($sql);
        if ($query->num_rows() > 0)
            return $query->result();
    }

}

if (!function_exists('getMyQuestionsPagination')) {

    /**
     * getFjQuestions function
     * @return the Array.
     * 
     */
    function getMyQuestionsPagination($limit, $offset, $serachColumn = NULL, $searchText = NULL, $createdBy = NULL) {
        $CI = &get_instance();
        //$CI->db->where('jobId', $id);
        $userTypeQuery  = "SELECT * FROM fj_users WHERE id='$createdBy' LIMIT 0,1";
        $userQuery      = $CI->db->query($userTypeQuery);
        $userResult     = $userQuery->row();
        if ($userResult) {
            if($userResult->role!='1') {
                $sql = "select * from fj_question where status!=3 and IsMultipleChoice='0'";        
                if (isset($createdBy) && $createdBy != NULL) {
                    $sql .= " AND createdBy IN (" . $createdBy . ")";
                }
                $sql .= " order by createdAt desc, type asc";    
                if (isset($offset)) {
                    $sql .= " limit " . $offset . ", " . $limit . "";
                }
                //echo $sql;die();
                $query = $CI->db->query($sql);
                if ($query->num_rows() > 0)
                    return $query->result();
            }
            else {
                $sql = "select * from fj_question where status!=3 and IsMultipleChoice='0'";        
                if (isset($createdBy) && $createdBy != NULL) {
                    $sql .= " AND createdBy IN (
                                                SELECT
                                                    id
                                                FROM
                                                    fj_users
                                                WHERE
                                                    role='1'
                                                )";
                }
                $sql .= " order by createdAt desc, type asc";    
                if (isset($offset)) {
                    $sql .= " limit " . $offset . ", " . $limit . "";
                }
                //echo $sql;die();
                $query = $CI->db->query($sql);
                if ($query->num_rows() > 0)
                    return $query->result();
                
            }
        }
        
        
    }

}

// if (!function_exists('getMyQuestionsNew')) {

//     /**
//      * getFjQuestions function
//      * @return the Array.
//      * 
//      */
//     function getMyQuestionsNew($id, $quesType=null) {
//         $CI = &get_instance();
//         //$CI->db->where('jobId', $id);
//         $sql = "select * from fj_question where createdBy='" . $id . "' and status!=3";
//         if( $quesType != "" && $quesType != null ){
//             $sql .=" and type='".$quesType."'";
//         }
//         $query = $CI->db->query($sql);
//         if ($query->num_rows() > 0)
//             return $query->result_array();
//     }

// }


if (!function_exists('getMyQuestionsNew')) {

    /**
     * getFjQuestions function
     * @return the Array.
     * 
     */
    function getMyQuestionsNew($id, $quesType=null) {
        $CI = &get_instance();
        //$CI->db->where('jobId', $id);
        $sql = "select * from fj_question where createdBy='" . $id . "' and status!=3";
        if( $quesType != "" && $quesType != null ){
            $sql .=" and type='".$quesType."'";
        }        
        $sql .= " ORDER BY title";
        $query = $CI->db->query($sql);
        if ($query->num_rows() > 0)
            return $query->result_array();
    }

}


if (!function_exists('getCorrectAnswer')) {

    /**
     * getCorrectAnswer function
     * @return the Array.
     * 
     */
    function getCorrectAnswer($arr) {
        //echo '<pre>';print_r($arr); die();
        foreach ($arr as $key => $item) {
            $answer = $item;
        }
        return $answer;
    }

}

if (!function_exists('listQuestionBank')) {

    /**
     * listQuestionBank function
     * @return the Array.
     * 
     */
    function listQuestionBank() {
        $CI = &get_instance();
        $userData = $CI->session->userdata('logged_in');
        $userId = $userData->id;
        $userRole = $userData->role;
        $userCreatedBy = $userData->createdBy;
        //echo "<pre>"; print_r($userData);
        //$CI->db->where('jobId', $id);
        if($userRole=='2' || $userRole=='1') {
          //$sql = "select QB.* from fj_questionBank as QB left join fj_users as U on U.id=QB.createdBy where (QB.createdBy='".$userId."' OR U.role=1 ) and QB.status!=3";
          $sql = "
                    SELECT
                        QB.*
                    FROM
                        fj_questionBank QB
                    WHERE
                        QB.status<>'3'          AND
                        (
                        QB.createdBY='$userId'  OR
                        QB.createdBY IN (
                                        SELECT
                                            id
                                        FROM
                                            fj_users    U
                                        WHERE
                                            createdBy='$userId'
                                        )
                        )
                    UNION
                    SELECT
                        QB.*
                    FROM
                        fj_questionBank QB
                    JOIN
                        fj_users        U
                    WHERE
                        QB.status<>'3'      AND
                        U.role='1'          AND
                        QB.createdBY=U.id
                    ORDER BY 
                        name";
        }
        else if($userRole=='4' || $userRole=='5'){
          $sql = "
                    SELECT
                        QB.*
                    FROM
                        fj_questionBank QB
                    WHERE
                        QB.status<>'3'          AND
                        (
                        QB.createdBY='$userId'  OR
                        QB.createdBY IN (
                                        SELECT
                                            id
                                        FROM
                                            fj_users    U
                                        WHERE
                                            U.id='$userCreatedBy'   OR
                                            createdBy='$userCreatedBy'
                                        )
                        )
                    UNION
                    SELECT
                        QB.*
                    FROM
                        fj_questionBank QB
                    JOIN
                        fj_users        U
                    WHERE
                        QB.status<>'3'      AND
                        U.role='1'          AND
                        QB.createdBY=U.id
                    ORDER BY 
                        name
                        
                        
                  ";
        }
        $query = $CI->db->query($sql);
        if ($query->num_rows() > 0)
            return $query->result_array();
    }

}

if (!function_exists('getOptionsofQuestion')) {

    /**
     * getOptionsofQuestion function
     * @return the Array.
     * 
     */
    function getOptionsofQuestion($id) {
        $CI = &get_instance();
        //$CI->db->where('jobId', $id);
        $sql = "select * from fj_multipleChoiceAnswer where questionId = '" . $id . "'";
        $query = $CI->db->query($sql);
        if ($query->num_rows() > 0)
            return $query->result_array();
    }

}

if (!function_exists('getQuestionOfQuestionBank')) {

    /**
     * getQuestionOfQuestionBank function
     * @return the Array.
     * 
     */
    function getQuestionOfQuestionBank($id) {
        $CI = &get_instance();
        //$CI->db->where('jobId', $id);
        //$sql = "select * from fj_multipleChoiceAnswer where questionId = '" . $id . "'";
        $sql = "select q.* from fj_question q left join fj_questionBankQuestion qbq on qbq.questionId=q.id where qbq.questionBank='".$id."' AND q.status != 3";
        $query = $CI->db->query($sql);
        if ($query->num_rows() > 0)
            return $query->result_array();
    }

}


if (!function_exists('GetAssessmentQuestionRow')) {

    /**
     * GetAssessmentQuestionRow function
     * @return the Array.
     * 
     */
    function getAssessmentQuestionRow($questionBank, $index) {
        $str = '<div class="form-group">
                    <div class="col-xs-2 rsp_width_100">
                        <label for="inputPassword" class="control-label no_padding_left">Type : </label>
                        <div class="clearfix"></div>
                        <span>Random</span>
                    </div>
                    <div class="col-xs-4 rsp_width_100">
                        <label for="inputPassword" class="control-label no_padding_left">Question Bank</label>
                        <div class="clearfix"></div>
                        <select class="selectpicker assessmentBank" name="question['.$index.'][questionBank]">
                                                                  '.$questionBank.'
                                                                </select>
                    </div>
                    <div class="col-xs-5 rsp_width_100">
                    	<label for="inputPassword" class="control-label no_padding_left">Number of Question</label>
                        <div class="clearfix"></div>
                        <input type="text" class="form-control assessmentQuestion" placeholder="" name="question['.$index.'][number]" maxlength="3">
                    </div>
                    <div class="col-xs-1"><button type="button" class="del-assessment" value="testing">x</button></div>
                </div>';
        return $str;
    }

}

if (!function_exists('getAssessmentQuestionRowEdit')) {

    /**
     * GetAssessmentQuestionRow function
     * @return the Array.
     * 
     */
    function getAssessmentQuestionRowEdit($questionBank, $index) {
        $str = '<div class="form-group">
                    <div class="col-xs-2 rsp_width_100">
                        <label for="inputPassword" class="control-label no_padding_left">Type : </label>
                        <div class="clearfix"></div>
                        <span>Random</span>
                    </div>
                    <div class="col-xs-4 rsp_width_100">
                        <label for="inputPassword" class="control-label no_padding_left">Question Bank</label>
                        <div class="clearfix"></div>
                        <select class="selectpicker assessmentBank2" name="question['.$index.'][questionBank]">
                                                                  '.$questionBank.'
                                                                </select>
                    </div>
                    <div class="col-xs-5 rsp_width_100">
                        <label for="inputPassword" class="control-label no_padding_left">Number of Question</label>
                        <div class="clearfix"></div>
                        <input type="text" class="form-control assessmentQuestionEdit" placeholder="" name="question['.$index.'][number]" maxlength="3">
                    </div>
                    <div class="col-xs-1"><button type="button" class="del-assessment" value="testing">x</button></div>
                </div>';
        return $str;
    }

}

if (!function_exists('getAssessmentIndividualQuestionRow')) {

    /**
     * GetAssessmentQuestionRow function
     * @return the Array.
     * 
     */
    function getAssessmentIndividualQuestionRow($questionBank, $index) {
        $str = '<div class="form-group">
                    <div class="col-xs-2 rsp_width_100">
                        <label for="inputPassword" class="control-label no_padding_left">Type : </label>
                        <div class="clearfix"></div>
                        <span>Individual</span>
                    </div>
                    <div class="col-xs-4 rsp_width_100">
                        <label for="inputPassword" class="control-label no_padding_left">Question Bank</label>
                        <div class="clearfix"></div>
                        <select class="selectpicker assessmentBank1" id="addAssessmentIndividualQuestionBank-'.$index.'" name="question['.$index.'][questionBank1]">
                          '.$questionBank.'
                        </select>
                    </div>
                    <div class="col-xs-5 rsp_width_100">
                        <label for="inputPassword" class="control-label no_padding_left">Question</label>
                        <div class="clearfix"></div>
                        <select class="selectpicker assessmentIndividualQuestion" id="addAssessmentIndividualQuestion-'.$index.'" name="question['.$index.'][question1]">
                            <option value="">-Select Question-</option>
                        </select>
                    </div>
                    <div class="col-xs-1"><button type="button" class="del-assessment" value="testing">x</button></div>
                </div>';
        return $str;
    }
}

if (!function_exists('getAssessmentIndividualQuestionRowEdit')) {

    /**
     * GetAssessmentQuestionRow function
     * @return the Array.
     * 
     */
    function getAssessmentIndividualQuestionRowEdit($questionBank, $index) {
        $str = '<div class="form-group">
                    <div class="col-xs-2 rsp_width_100">
                        <label for="inputPassword" class="control-label no_padding_left">Type : </label>
                        <div class="clearfix"></div>
                        <span>Individual</span>
                    </div>
                    <div class="col-xs-4 rsp_width_100">
                        <label for="inputPassword" class="control-label no_padding_left">Question Bank</label>
                        <div class="clearfix"></div>
                        <select class="selectpicker assessmentBank3" id="editAssessmentIndividualQuestionBank-'.$index.'" name="question['.$index.'][questionBank1]">
                          '.$questionBank.'
                        </select>
                    </div>
                    <div class="col-xs-5 rsp_width_100">
                        <label for="inputPassword" class="control-label no_padding_left">Question</label>
                        <div class="clearfix"></div>
                        <select class="selectpicker assessmentIndividualQuestionEdit" id="editAssessmentIndividualQuestion-'.$index.'" name="question['.$index.'][question1]">
                            <option value="">-Select Question-</option>
                        </select>
                    </div>
                    <div class="col-xs-1"><button type="button" class="del-assessment" value="testing">x</button></div>
                </div>';
        return $str;
    }
}

if (!function_exists('getNoOfQuestionsPerAssessment')) {

    /**
     * GetNoOfQuestionsPerAssessment function
     * @return the Array.
     * 
     */
    function getNoOfQuestionsPerAssessment($id) {
        $CI = &get_instance();
        //$CI->db->where('jobId', $id);
        $sql = "select count(*) as count from fj_assessmentQuestions where assessmentId='" . $id . "'";
        $query = $CI->db->query($sql);
        if ($query->num_rows() > 0)
            return $query->row_array();
    }
}

if (!function_exists('getNoOfRandomQuestions')) {

    /**
     * GetNoOfQuestionsPerAssessment function
     * @return the Array.
     * 
     */
    function getNoOfRandomQuestions($id) {
        $CI = &get_instance();
        //$CI->db->where('jobId', $id);
        $sql = "select SUM(noOfQuestions) as count from fj_assessmentRandomQuestionnaire GROUP BY assessmentId HAVING assessmentId='" . $id . "'";
        $query = $CI->db->query($sql);
        if ($query->num_rows() > 0)
            return $query->row_array();
    }
}

if (!function_exists('checkAtLeastOneQuestionAssessment')) {

    /**
     * checkAtLeastOneQuestionAssessment function
     * @return the boolean.
     * 
     */
    function checkAtLeastOneQuestionAssessment($arr) {
        $flag = false;
        foreach($arr as $item) {
            if($item['number'] != NULL && $item['questionBank'] != NULL) {
                $flag = true;
            }
        }
        return $flag;
    }

}

if (!function_exists('getJobDetail')) {

    /**
     * getJobDetail function
     * @return the Array.
     * 
     */
    function getJobDetail($id) {
        $CI = &get_instance();
        //$CI->db->where('jobId', $id);
        $sql = "select * from fj_jobs where id='" . $id . "'";
        $query = $CI->db->query($sql);
        if ($query->num_rows() > 0)
            return $query->row_array();
    }

}

if (!function_exists('getMyJobs')) {

    /**
     * getMyJobs function
     * @return the Array.
     * 
     */
    function getMyJobs($id) {
        $CI = &get_instance();
        $sql = "select * from fj_jobs where createdBy='" . $id . "' AND status != 3";
        
        $query = $CI->db->query($sql);
        if ($query->num_rows() > 0)
            return $query->result_array();
    }

}

if (!function_exists('getMyJobsSearch')) {

    /**
     * getMyJobsSearch function
     * @return the Array.
     * 
     */
    function getMyJobsSearch($createdBy, $userRoleValue) {
        $CI = &get_instance();
        //$sql = "select * from fj_jobs where status != 3 ";
        $sql = "
                SELECT
                    (CASE WHEN U.role<>2 THEN (SELECT company FROM fj_users WHERE id=U.createdBy) ELSE U.company END) AS company,
                    JOBS.*
                FROM
                    (
                    SELECT
                        J.*
                    FROM
                        fj_jobs J
                    WHERE
                        J.status IN ('1','2')
                    GROUP BY
                        J.id
                    ) JOBS
                LEFT JOIN
                    fj_users U
                ON
                    U.id=JOBS.createdBy
                WHERE
                    JOBS.openTill>=CURDATE()
            ";
        if($userRoleValue==1) {
            $getAllAdmin    = "SELECT id FROM fj_users WHERE role=1";
            $adminQuery     = $CI->db->query($getAllAdmin);
            $adminResult    = $adminQuery->result_array();
            $corporate      = '';
            $subUserId      = '';
            $records        = count($adminResult);
            $k = 0;
            foreach($adminResult as $adminId) {
                $k++;
                $corporateId    = getCorporateUser($adminId['id']);
                $corporate      .= "'".$corporateId."'";    
                $corporate      .= ($records==$k?'':',');
                
                
                $subUserSql =  "SELECT
                                    GROUP_CONCAT(CONCAT('''', id, '''' )) AS subUserIds 
                                FROM
                                    fj_users
                                WHERE
                                    createdBy='$corporateId'
                                ";
                if($userRoleValue==1){
                    $subUserSql .= " AND role<>'2'";
                }
                $subUserQuery  = $CI->db->query($subUserSql);
                $subUserResult = $subUserQuery->row_array();
                if(count($subUserResult)>0){
                    $subUserId .= $subUserResult['subUserIds'];
                }
            }
            
            $subUserIds     = ($subUserId!=''?','.$subUserId:'');
            $sql            .= " AND JOBS.createdBy IN (".$corporate."".$subUserIds.")";
        }
        else {
            $corporateId    = getCorporateUser($createdBy);
            $subUserIds     = getCorporateSubUser($corporateId, $userRoleValue);
            $subUserIds     = ($subUserIds!=''?','.$subUserIds:'');
            $sql            .= " AND JOBS.createdBy IN ('".$corporateId."'".$subUserIds.")";
        }
        //echo $sql; exit;
        $query = $CI->db->query($sql);
        if ($query->num_rows() > 0)
            return $query->result_array();
    }

}

if (!function_exists('getQuestionsByQuestionBank')) {

    /**
     * getQuestionsByQuestionBank function
     * @return the Array.
     * 
     */
    function getQuestionsByQuestionBank($id) {
        $CI = &get_instance();
        $sql = "select count(*) as count from fj_questionBankQuestion as qb left join fj_question as q on q.id=qb.questionId where qb.questionBank='" . $id . "' and q.status!=3";
        
        $query = $CI->db->query($sql);
        if ($query->num_rows() > 0)
            return $query->row_array();
    }

}

if (!function_exists('getQuestionsById')) {

    /**
     * getQuestionsByQuestionBank function
     * @return the Array.
     * 
     */
    function getQuestionsById($questionId) {
        $CI = &get_instance();
        $sql = "select * from fj_question where id='" . $questionId . "' and status!=3";
        $query = $CI->db->query($sql);
        if ($query->num_rows() > 0)
            return $query->row_array();
    }

}

if (!function_exists('getUserId')) {

    /**
     * getUserId function
     * @return the Array.
     * 
     */
    function getUserId() {
        $CI = &get_instance();
        $userData = $CI->session->userdata['logged_in'];
        $userId = $userData->id;
        return $userId;
    }

}






if (!function_exists('getStatusType')) {

    /**
     * getQuestionsByInterview function
     * @return the Array.
     * 
     */
    function getStatusType($id) {
        if ($id == 1) {
            $type = 'Active';
        } else if ($id == 2) {
            $type = 'Inactive';
        } else {
            $type = 'Deleted';
        }
        return $type;
    }

}




if (!function_exists('getFjCategory')) {

    /**
     * getFjQuestions function
     * @return the Array.
     * 
     */
    function getFjCategory() {
        $CI = &get_instance();
        //$CI->db->where('jobId', $id);
        $sql = "select Distinct(categoryName) from fj_knowledgeCenterCategory WHERE status='1' AND (categoryName <> '' OR categoryName<>NULL) ORDER BY categoryName ASC";
        $query = $CI->db->query($sql);
        if ($query->num_rows() > 0)
            return $query->result_array();
    }

}

if (!function_exists('getFjTopics')) {

    /**
     * getFjQuestions function
     * @return the Array.
     * 
     */
    function getFjTopics() {
        $CI = &get_instance();
        //$CI->db->where('jobId', $id);
        $sql = "select Distinct(topic) from fj_knowledgeCenterTopics WHERE status='1' AND (topic <> '' OR topic<>NULL) ORDER BY topic ASC";
        $query = $CI->db->query($sql);
        if ($query->num_rows() > 0)
            return $query->result_array();
    }

}



if (!function_exists('getIndustry')) {

    /**
     * getInterviews function
     * Get interviews.
     * @return array.
     * 
     */
    function getIndustry() {
        $CI = &get_instance();
        $sql = "select * from nc_industry WHERE is_active='1' AND indus_id='0' ORDER BY name ASC";
        $query = $CI->db->query($sql);
        $result = $query->result_array();
        return $result;
    }

}




if (!function_exists('getNewAuditionQuestionBankRow')) {

    /**
     * getNewAuditionQuestionRow function
     * @return the Array.
     * 
     */
    function getNewAuditionQuestionBankRow($index) {
        $str = '<div class="form-group">
                    <div class="col-xs-9 ques-title">
                        <label for="inputPassword" class="control-label frm_in_padd">Title</label>
                        <input type="text" class="form-control auditionquestion" placeholder="" name="question[' . $index . '][title]" value="" maxlength="150">
                    </div>

                    <div class="col-xs-3">
                        <label for="inputPassword" class="control-label frm_in_padd col-xs-12">Answer Duration <span style="font-size:10px;">(Limit 600 Sec)</span></label>
                        <div class="col-md-6 no_padding">
                            <input type="text" class="form-control auditionduration questiondurationtogetid" id="durations' . $index . '"  placeholder="" name="question[' . $index . '][duration]" maxlength="3">
                        </div>
                        <div class="col-md-4">
                            <p class="padd_txtmin">Sec</p>
                        </div>';
//                        <div class="col-md-6">
//                            <p class="padd_txtmin">Sec <a href="javascript:"><img src="' . base_url() . '/theme/firstJob/image/plus.png" class="pad_rg_lfs add-more-audition-question"></a></p>
//                        </div>
                        
        $str .=         '<div class="col-md-2">
                            <button type="button" class="del-auditionRow del-question  pull-right" value="testing">x</button>
                        </div>
                    </div>
                </div>';
        return $str;
    }

}




if (!function_exists('getNewAuditionQuestionRow')) {

    /**
     * getNewAuditionQuestionRow function
     * @return the Array.
     * 
     */
    function getNewAuditionQuestionRow($fjQuestion, $index, $auditionQuestion) {
        $str = '<div class="form-group">
                    <div class="col-xs-9 ques-title">
                        <label for="inputPassword" class="control-label frm_in_padd">Title</label>
                        <select data-placeholder="Type a question" style="width: 350px; display: none;" class="chosen-select sel_sty_padd auditionquestion" tabindex="-1" name="question[' . $index . '][title]">
                            <option value=""></option>';
                            foreach ($auditionQuestion as $item):
                                $str .= '<option value="'.$item['id'].'">'.$item['title'].'</option>';
                            endforeach;
                            $str .='</select>
                    </div>

                    <div class="col-xs-3">
                        <label for="inputPassword" class="control-label frm_in_padd col-xs-12">Answer Duration <span style="font-size:10px;">(Limit 600 Sec)</span></label>
                        <div class="col-md-6 no_padding">
                            <input type="text" class="form-control auditionduration questiondurationtogetid" id="durations' . $index . '"  placeholder="" name="question[' . $index . '][duration]" maxlength="3">
                        </div>
                        <div class="col-md-4">
                            <p class="padd_txtmin">Sec</p>
                        </div>';
//                        <div class="col-md-6">
//                            <p class="padd_txtmin">Sec <a href="javascript:"><img src="' . base_url() . '/theme/firstJob/image/plus.png" class="pad_rg_lfs add-more-audition-question"></a></p>
//                        </div>
                        
        $str .=         '<div class="col-md-2">
                            <button type="button" class="del-auditionRow del-question  pull-right" value="testing">x</button>
                        </div>
                    </div>
                </div>';
        return $str;
    }

}

if (!function_exists('getAllCorporateAndSubuserIds')) {

    /**
     * getInterviews function
     * Get interviews.
     * @return array.
     * 
     */
    function getAllCorporateAndSubuserIds($userLoggedinUserId, $userRole) {
        
        $createdByStr = "";
        //$CI = &get_instance();
        //$CI->load->model('modules/Jobs/models/user_model');
        
        if( $userRole != 1 ):
            
            if( $userRole == 2 ):
                
                $userIdsRes = getUserIdsForJob($userLoggedinUserId);
                $userIdsStr = $userIdsRes[0]['userIds'];
                
                if( $userIdsStr != "" ):
                    $createdByNew = $userIdsStr.",".$userLoggedinUserId;
                else:
                    $createdByNew = $userLoggedinUserId;
                endif;
            elseif( $userRole == 3 || $userRole == 4 || $userRole == 5 ):
                
                $userDataDetails = getUserById($userLoggedinUserId);
                if( $userDataDetails ):
                    
                    $corporateId = $userDataDetails['createdBy'];
                    $userIdsRes = getUserIdsForJob($corporateId);
                    $userIdsStr = $userIdsRes[0]['userIds'];
                    
                    if( $userIdsStr != "" ):
                        
                        $createdByNew = $userIdsStr.",".$corporateId;
                    else:
                        $createdByNew = $userLoggedinUserId.",".$corporateId;
                    endif;
                else:
                    $createdByNew = "";
                endif;
            else:
                $createdByNew = "";
            endif;                
        else:
            $createdByNew = $userLoggedinUserId;
        endif;

        
        if( $createdByNew != "" ){
            $createdByArr = explode(",", $createdByNew);
            $createdByArr = array_filter($createdByArr);
            $createdByStr = implode(",", $createdByArr);
        }            
        
        return $createdByStr;
    }

}  

if (!function_exists('sendSMSMessage')) {
    
    function sendSMSMessage($to, $message){
        
        $host     = 'http://bulkpush.mytoday.com/BulkSms/SingleMsgApi';
        $feedId   = '358805';
        $username = '9958412749';
        $password = 'atmaa';
        
        $message  = urlencode($message);
        $url      = $host.'?feedid='.$feedId.'&username='.$username.'&password='.$password.'&To=91'.$to.'&Text='.$message.'&senderid=91'.$to;
        
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.73 Safari/537.36"); 
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 300); 
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 300);
        // Execute Curl
        $response = curl_exec($curl);
                        
        if (curl_errno($curl) ) {
            $result = 'ERROR -> ' . curl_errno($curl) . ': ' . curl_error($curl);
            echo $result;
            curl_close($curl); // close cURL handler
            exit;
        }
        else {
            if (empty($response)) {
                $result = 'ERROR -> ' . curl_errno($curl) . ': ' . curl_error($curl);
                echo $result;
                curl_close($curl); exit;
            } 
            else {
                $returnCode = (int)curl_getinfo($curl, CURLINFO_HTTP_CODE);
                if (empty($returnCode)) {
                     die("No HTTP code was returned");
                } 
                else {    
                    // echo results
                    if($returnCode==200) {
                        $xml = simplexml_load_string($response);
                        foreach ($xml as $key) {
                            if($key->ERROR){
                                //foreach ($key->ERROR as $errorVal){
                                    //echo $errorVal->CODE[0];
                                //}
                                return FALSE;
                            }
                            else {
                                //$key['SUBMITDATE'];
                                //$key['TID'];
                                return TRUE;
                            }
                        }
                    }

                }
            }
        }
        //Close the handle
        curl_close($curl);
    }
}
    

/**
* getUserIdsForJob function
* Gets a specific job detail.
* @param int $id.
* @return array the jobs data.
*/
function getUserIdsForJob($createdBy) {
   $CI = &get_instance();
    
   try {
        
        $sql = "select GROUP_CONCAT(DISTINCT id SEPARATOR ',') as userIds from fj_users WHERE createdBy ='".$createdBy."'";
        
        $query = $CI->db->query($sql);
        $result = $query->result_array();
        if (count($result) == 0) {
            //throw new Exception('Unable to fetch user ids data.');
            return false;
        }
       return $result;
   } catch (Exception $e) {
       echo $e->getMessage();
       die();
   }
}
    
    
function encryptURLparam($id, $key)
{
	
    $id = base_convert($id, 10, 36); // Save some space
    $data = mcrypt_encrypt(MCRYPT_BLOWFISH, $key, $id, 'ecb');
    $data = bin2hex($data);
	
    return $data;
}

function decryptURLparam($encrypted_id, $key)
{ 
    $data = pack('H*', $encrypted_id); // Translate back to binary
    $data = mcrypt_decrypt(MCRYPT_BLOWFISH, $key, $data, 'ecb');
    $data = base_convert($data, 36, 10);

    return $data;
}


if (!function_exists('getAssessmentsByRole')) {
    function getAssessmentsByRole($userId, $userRole) {
        $CI = &get_instance();
        $corporateId    = getCorporateUser($userId);
        $subUserIds     = getCorporateSubUser($corporateId, $userRole);
        $subUserIds     = ($subUserIds!='' ? ','.$subUserIds : '');
        //echo $corporateId; exit;
        //echo $subUserIds; exit;
        //print_r($corporateId); exit;
        $createdByIN = "'".$corporateId."'".$subUserIds;        
        $createdByIN = str_replace(',,', ',', $createdByIN);
        if($subUserIds!=''){
            $subUserIds = ",".$subUserIds;
        }
        $sql =  "
                SELECT 
                    A.*
                FROM
                    fj_assessments  A
                WHERE
                    A.createdBy IN ($createdByIN)   AND
                    A.status='1'
                ORDER BY
                    A.name
                ";        
        //print_r($sql); exit;
        $query  = $CI->db->query($sql);
        $result = $query->result_array();
        return $result;
    }
}

if (!function_exists('getInterviewByRole')) {
    function getInterviewByRole($userId, $role) {
        $CI = &get_instance();
        $corporateId    = getCorporateUser($userId);
        $subUserIds     = getCorporateSubUser($corporateId, $role);
        $subUserIds     = ($subUserIds!=''?','.$subUserIds:'');
        $createdByIN = "'".$corporateId."'".$subUserIds;        
        $createdByIN = str_replace(',,', ',', $createdByIN);
        $sql =  "
                    SELECT 
                        I.*
                    FROM
                        fj_interviews  I
                    WHERE
                        I.createdBy IN ($createdByIN)   AND
                        I.status='1'
                    ORDER BY
                        I.name
                ";        
        //print_r($corporateId); exit;
        $query  = $CI->db->query($sql);
        $result = $query->result_array();
        return $result;
    }
}

if (!function_exists('getCorporateUser')) {
    function getCorporateUser($userId) {
        $CI = &get_instance();
        $sql =  "SELECT
                    *
                FROM
                    fj_users
                WHERE
                    id='$userId'
                ";
        $query  = $CI->db->query($sql);
        $result = $query->row_array();
        if(count($result)>0){
            if($result['role']=='2' || $result['role']=='1'){
                return $result['id'];
            }
            else {
                return getCorporateUser($result['createdBy']);
            }
        }
    }
}

if (!function_exists('getCorporateSubUser')) {
    function getCorporateSubUser($corporateId, $role) {
        $CI = &get_instance();
        $sql =  "SELECT
                    GROUP_CONCAT(CONCAT('''', id, '''' )) AS subUserIds 
                FROM
                    fj_users
                WHERE
                    createdBy='$corporateId'
                ";
        if($role==1){
            $sql .= " AND role='3'";
        }
        $query  = $CI->db->query($sql);
        $result = $query->row_array();
        //print_r($sql);
        if(count($result)>0){
            return $result['subUserIds'];
        }
    }
}

if (!function_exists('getAuditionQuestionsById')) {
    /**
     * getQuestionsByQuestionBank function
     * @return the Array.
     * 
     */
    function getAuditionQuestionsById($questionId) {
        $CI = &get_instance();
        $sql = "select * from fj_auditionQuestions where id='" . $questionId . "' and status!=3";
        $query = $CI->db->query($sql);
        if ($query->num_rows() > 0)
            return $query->row_array();
    }
}

if (!function_exists('assessmentExists')) {   
    function assessmentExists($assessmentId) {
        $sql = "SELECT id FROM fj_jobs WHERE assessment='$assessmentId'";
        $CI = &get_instance();
        $query = $CI->db->query($sql);
        $result = $query->num_rows();
        return $result;
    }
}

if (!function_exists('interviewExists')) {   
    function interviewExists($interviewId) {
        $sql = "SELECT id FROM fj_jobs WHERE interview='$interviewId'";
        $CI = &get_instance();
        $query = $CI->db->query($sql);
        $result = $query->num_rows();
        return $result;
    }
}
 
if (!function_exists('assessmentBankExists')) {   
    function assessmentBankExists($assessmentId) {
        $sql = "SELECT
                    id
                FROM
                    fj_assessmentQuestions
                WHERE
                    questionId IN
                                (SELECT 
                                    questionId
                                FROM 
                                    fj_questionBankQuestion
                                WHERE
                                    questionBank='$assessmentId'
                                 )";
        $CI = &get_instance();
        $query = $CI->db->query($sql);
        $result = $query->num_rows();
        return $result;
    }
}
 
if (!function_exists('interviewBankExists')) {   
    function interviewBankExists($interviewId) {
        $sql = "SELECT
                    id
                FROM
                    fj_interviewQuestions
                WHERE
                    questionId= '$interviewId'
                ";
        //echo $sql;
        $CI = &get_instance();
        $query = $CI->db->query($sql);
        $result = $query->num_rows();
        return $result;
    }
}

if (!function_exists('interviewSetJobExists')) {   
    function interviewSetJobExists($interviewSetId) {
        $sql = "SELECT
                    count(id) AS jobIds
                FROM
                    fj_jobs
                WHERE
                    interview IN ($interviewSetId)
                ";
        //echo $sql;
        $CI     = &get_instance();
        $query  = $CI->db->query($sql);
        //echo $this->db->last_query();
        //echo "<br>";
        $result = $query->result_array();
        return $result;
    }
}
 
if (!function_exists('getUniversity')) {

    /**
     * getUniversity function
     * Gets University.
     * @return array.
     * 
     */
    function getUniversity() {
        $CI = &get_instance();
        $query = $CI->db->get('fj_university');
        $results = $query->result_array();
        return $results;
    }
} 

if (!function_exists('send_gcm_notify')) {
    function send_gcm_notify($params) {
        $registration_ids   = !empty($params['device_token']) ? $params['device_token'] : '';
        $title              = !empty($params['title']) ? $params['title'] : '';
        $message            = !empty($params['message']) ? $params['message'] : '';
        $notiData           = !empty($params['data']) ? $params['data'] : '';
        $clickAction        = !empty($params['clickAction']) ? $params['clickAction'] : '';
        $requestArray       = array();
        $noti               = json_decode($notiData);
        
        if (!empty($registration_ids) && $registration_ids != '') {                
            $fields = array(
                            'registration_ids'  => $registration_ids,
                            'priority'          => "high",
                            'notification'      => array(
                                                        "title"         => $title, 
                                                        "body"          => $message,
                                                        "icon"          => "name_of_icon",
                                                        "click_action"  => $clickAction
                                                    ),
                            'data'              => array("data"=>$notiData)
                        );        
            $headers = array(
                            GOOGLE_GCM_URL,
                            'Content-Type: application/json',
                            'Authorization: key=' . GOOGLE_API_KEY
                        );
            
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, GOOGLE_GCM_URL);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            
            $result = curl_exec($ch);
            if ($result === FALSE) {
                die('Problem occurred: ' . curl_error($ch));
            }
            curl_close($ch);
            return $result;
        }
    }
}

function cleanString($string) {
    $string         = trim($string);
    $detagged       = strip_tags($string);
    $stripped       = stripslashes($detagged);
    $htmlEntites    = preg_replace("/&#?[a-z0-9]+;/i","",htmlentities($stripped, ENT_QUOTES, 'UTF-8'));
    return $htmlEntites;
}

if (!function_exists('sendAndroidPushNotification')) {
    /**
     * sendAndroidPushNotification function
     * send push notification to logged users
     * @params $data = array('post_id'=>'12345','post_title'=>'A Blog post')
     *         $target = single tocken id or topic name OR
     *         $target = array('token1','token2','...'); // up to 1000 in one request
     * @return true or false.
     * 
     */
    function sendAndroidPushNotification($data,$target,$notification) {
        //FCM api URL
        $url = 'https://fcm.googleapis.com/fcm/send';
        //api_key available in Firebase Console -> Project Settings -> CLOUD MESSAGING -> Server key
        //$server_key = 'AAAAC4cWCmQ:APA91bGH4hgpd-7vUqg_Q6kXvOQgwpP-cRHX2zuMpvUK8Haz_1YZwS-6GHafYjrQKjee7sdwlic6y_bOx0T6Nr1X62ZkID7_BYwH5LmnIayw1UQjqERFmqykr6WfWk0kVDRmk0Laa9It';

        $server_key = 'AAAAC4cWCmQ:APA91bGH4hgpd-7vUqg_Q6kXvOQgwpP-cRHX2zuMpvUK8Haz_1YZwS-6GHafYjrQKjee7sdwlic6y_bOx0T6Nr1X62ZkID7_BYwH5LmnIayw1UQjqERFmqykr6WfWk0kVDRmk0Laa9It';
                    
        $fields = array();
        //$fields['notification'] = $notification;
        $fields['data'] = $data;
        if(is_array($target)){
            $fields['registration_ids'] = $target;
        }else{
            $fields['to'] = $target;
        }

        //print_r($fields);exit;
        //header with content_type api key
        $headers = array(
            'Content-Type:application/json',
          'Authorization:key='.$server_key
        );
                    
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        //print_r($result);exit;
        if ($result === FALSE) {
            die('FCM Send Error: ' . curl_error($ch));
        }
        curl_close($ch);
        return $result;
    }
}

if (!function_exists('callStack')) {
    /**
     * callStack function
     * perform action at the time of api call
     * @params null
     * @return true or false.
     * 
     */
    function callStack($stacktrace) {
        print str_repeat("=", 50) ."\n";
        $i = 1;
        foreach($stacktrace as $node) {
            print "$i. ".basename($node['file']) .":" .$node['function'] ."(" .$node['line'].")\n";
            $i++;
        }
    }
}

if (!function_exists('getCalledFromFunction')) {
    /**
     * getCalledFromFunction function
     * perform action at the time of api call
     * @params null
     * @return true or false.
     * 
     */
    function getCalledFromFunction($stacktrace) {
        $i = 1;
        foreach($stacktrace as $node) {
            if($i == 2)
                return $node['function'];
            $i++;
        }
    }
}

if (!function_exists('getCalledFromController')) {
    /**
     * getCalledFromController function
     * perform action at the time of api call
     * @params null
     * @return true or false.
     * 
     */
    function getCalledFromController($stacktrace) {
        $i = 1;
        foreach($stacktrace as $node) {
            if($i == 2) {
                if(isset($node['file'])) {
                    return basename($node['file'],".php");
                } else {
                    return '';
                }
            }
            $i++;
        }
    }
}

if (!function_exists('processApiEntry')) {
    /**
     * processApiEntry function
     * perform action at the time of api call
     * @params null
     * @return true or false.
     * 
     */
    function processApiEntry() {
        $funCalls = getCalledFromFunction(debug_backtrace());
        print_r($funCalls);exit;
        $result = array('message' => 'success');
        return json_encode($result);
    }
}

if (!function_exists('processApiExit')) {
    /**
     * processApiExit function
     * perform action at the time of api exit
     * @params null
     * @return true or false.
     * 
     */
    function processApiExit() {
        $funCalls = getCalledFromFunction(debug_backtrace());
        $result = array('message' => 'success');
        return json_encode($result);
    }
}

if (!function_exists('processApiSuccess')) {
    /**
     * processApiSuccess function
     * perform action when api return success
     * @params null
     * @return true or false.
     * 
     */
    function processApiSuccess($thisVariable) {
        $CI = &get_instance();
        $CI->load->library('session');
        $loggedInuserData = $CI->session->userdata['logged_in'];
        $companyName = $loggedInuserData->company;

        $lastCallerClass = get_class($thisVariable);//Users
        $userIds = $thisVariable->userId;//190
        

        $funController = getCalledFromController(debug_backtrace());//Null
        
        $funCalls = getCalledFromFunction(debug_backtrace());//inviteUsers
        
        $firstMatch = substr($funCalls, 0, 4);
        $secondMatch = substr($funCalls, 0, 3);
        $thirdMatch = substr($funCalls, 0, 6);

        if($firstMatch == 'send' || $secondMatch == 'add' || $thirdMatch == 'create' || $thirdMatch == 'invite' || $firstMatch == 'user' || $firstMatch == 'view') {
            if($funController) {
                $sql = "SELECT epa.name action, epc.title title, epc.message message FROM fj_event_processing_configuration epc JOIN fj_event_processing_action epa ON epa.id = epc.eventProcessingActionId WHERE epc.controller='$funController' AND epc.model='$lastCallerClass' AND epc.functionName='$funCalls' AND epc.crud = 'C'";
            } else {
                $notitype = $thisVariable->type;

                if($notitype == 'shortlisted')
                    $typeWhere = ' AND epc.type = "shortlisted"';
                elseif ($notitype == 'rejected')
                    $typeWhere = ' AND epc.type = "rejected"';
                else
                    $typeWhere = ' AND 1=1';

                $sql = "SELECT epa.name action, epc.title title, epc.message message FROM fj_event_processing_configuration epc JOIN fj_event_processing_action epa ON epa.id = epc.eventProcessingActionId WHERE epc.controller='$lastCallerClass' AND epc.functionName='$funCalls' AND epc.crud = 'C' $typeWhere";
            }
            $query = $CI->db->query($sql);

            if ($query->num_rows() > 0) {

                $result = $query->row_array();
                //print_r($result);exit;
                if($userIds) {
                    if(is_array($userIds)){
                        $target = array();
                        foreach($userIds as $userId) {
                            $sqlUser = "SELECT id, deviceId FROM fj_users WHERE id='$userId' AND deviceId IS NOT NULL AND status = 1";
                            $queryUser = $CI->db->query($sqlUser);

                            if ($queryUser->num_rows() > 0) {
                                $resultUser = $queryUser->row_array();
                                $target[] = $resultUser['deviceId'];
                            }
                        }
                    } else {
                        $target = '';
                        $sqlUser = "SELECT id, deviceId FROM fj_users WHERE id='$userIds' AND deviceId IS NOT NULL AND status = 1";
                        $queryUser = $CI->db->query($sqlUser);

                        if ($queryUser->num_rows() > 0) {
                            $resultUser = $queryUser->row_array();
                            $target = $resultUser['deviceId'];
                        }
                    }

                    if($result['action'] == 'MessageApp') {
                        $data = array(
                            'message'   => $result['message'],
                            'title'     => $result['title'],
                            'vibrate'   => 1,
                            'sound'     => 1,
                        );

                        $notification = array(
                            'body' => $result['message'],
                            'title' => $result['title']
                        );

                        if($target) {
                            sendAndroidPushNotification($data, $target, $notification);
                        }
                    }
                    elseif($result['action'] == 'NotificationApp') {
                        $notificationType = $thisVariable->type;

                        if($notificationType == 'jobInvitation' || $notificationType == 'InterviewViewed' || $notificationType == 'shortlisted' || $notificationType == 'rejected' || $notificationType == 'message') {

                            $jobCode = '';
                            if($thisVariable->jobCode) {
                                $jobCode = $thisVariable->jobCode;
                                //getting job name from job code
                                $jobSql = "SELECT title FROM fj_jobs WHERE fjCode='$jobCode'";
                                $jobQuery = $CI->db->query($jobSql);

                                if ($jobQuery->num_rows() > 0) {
                                    $jobResult = $jobQuery->row();
                                    $jobName = $jobResult->title;
                                }
                                //end of getting job name from job code
                            }

                            if (strpos($result['title'], '<companyName>') !== false) {
                                $actualTitle = str_replace("<companyName>", $companyName, $result['title']);
                            } else {
                                $actualTitle = $result['title'];
                            }

                            if (strpos($result['message'], '<jobName>') !== false) {
                                $actualMessage = str_replace("<jobName>", $jobName, $result['message']);
                            } else {
                                $actualMessage = $result['message'];
                            }

                            if (strpos($actualMessage, '<companyName>') !== false) {
                                $actualMessage = str_replace("<companyName>", $companyName, $actualMessage);
                            } else {
                                $actualMessage = $actualMessage;
                            }

                            $data = array(
                                'type'   => $notificationType,
                                'jobCode' => $jobCode,
                                'message'   => $actualMessage,
                                'title'     => $actualTitle,
                            );

                            $notification = array(
                                'body' => $actualMessage,
                                'title' => $actualTitle
                            );

                            /*Saving Notifications is Table*/
                            if(is_array($userIds)){
                                foreach($userIds as $userId) {
                                    $notificationInsertData = array(
                                        'userId' => $userId,
                                        'sender' => $companyName,
                                        'type' => $notificationType,
                                        'jobCode' => $jobCode,
                                        'status' => 0,
                                        'message' => $actualMessage,
                                        'createdAt' => date('Y-m-d H:i:s')
                                    );
                                    $CI->db->insert('fj_userNotifications', $notificationInsertData);
                                }
                            } else {
                                $notificationInsertData = array(
                                    'userId' => $userIds,
                                    'sender' => $companyName,
                                    'status' => 0,
                                    'message' => $actualMessage,
                                    'createdAt' => date('Y-m-d H:i:s')
                                );
                                $CI->db->insert('fj_userNotifications', $notificationInsertData);
                            }
                            /*End of Saving notification is app*/
                        } else {
                            $data = array(
                                'message'   => $result['message'],
                                'title'     => $result['title'],
                                'vibrate'   => 1,
                                'sound'     => 1,
                            );

                            $notification = array(
                                'body' => $result['message'],
                                'title' => $result['title']
                            );
                        }

                        if($target) {
                            sendAndroidPushNotification($data, $target, $notification);
                        }
                    }
                }
            }
        }
    }
}

if (!function_exists('processApiFailure')) {
    /**
     * processApiFailure function
     * perform action when some error occurs on ongoing process
     * @params null
     * @return true or false.
     * 
     */
    function processApiFailure() {
        $funCalls = getCalledFromFunction(debug_backtrace());
        $result = array('message' => 'success');
        return json_encode($result);
    }
}

if (!function_exists('checkUserAccessForWebPage')) {
    /**
     * checkUserAccessForWebPage function
     * Get the permissions for users on web pages.
     * @params int $id the role id.
     * @return true or false.
     * 
     */
    function checkUserAccessForWebPage($userId, $roleId, $application, $pageRelativeUrl) {
        $CI = &get_instance();
        $CIS = &get_instance();

        $CI->db->where('id', $userId);
        $CI->db->where('role', $roleId);
        $query = $CI->db->get('fj_users');
		
        if ($query->num_rows() > 0) {
            $CIS->db->where('roleId', $roleId);
            $CIS->db->where('application', $application);
            $CIS->db->where('pageUrl', $pageRelativeUrl);
            $queryAccess = $CIS->db->get('fj_user_access');
			//echo $CIS->db->last_query();die;
            if ($queryAccess->num_rows() > 0) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}

if (!function_exists('getHashEID')) {
    /**
     * getHashEID function
     * Get hash id of entity
     * @params int $strInput
     * @return true or false.
     * 
     */
    function getHashEID($strInput) {
        $hashEid = hash('sha512', $strInput);

        $CI = &get_instance();
        $CI->db->where('eid', $strInput);
        $query = $CI->db->get('fj_hashEID');

        if ($query->num_rows() > 0) {
            return $hashEid;
        } else {
            $insertArray = array('eid' => $strInput, 'hashEid' => $hashEid);
            $CI->db->insert('fj_hashEID', $insertArray);
            return $hashEid;
        }
    }
}

if (!function_exists('getEID')) {
    /**
     * getEID function
     * Get entity id of entity
     * @params string $hashEID
     * @return true or false.
     * 
     */
    function getEID($hashEID) {

        $CI = &get_instance();
        $CI->db->where('hashEid', $hashEID);
        $query = $CI->db->get('fj_hashEID');

        if ($query->num_rows() > 0) {
            $result = $query->row();
            return $result->eid;
        } else {
            return false;
        }
    }
}

if (!function_exists('sendEmailApi')) {
    /**
     * getEID function
     * Get entity id of entity
     * @params string $hashEID
     * @return true or false.
     * 
     */
    /*
     *  @Author : Synergy
     *  @Params : array
     *  @Short Description : Use to send email
     *  @return : array with code. 
     */
    function sendEmailApi($params) {
        include $_SERVER['DOCUMENT_ROOT']."/PHPMailer_v5.1/class.phpmailer.php";
        $account="firstjob";
        $password="a1cf$4861ed42";
        $to="ajitchandour@gmail.com";
        $from = 'gaurav.gupta@firstjob.co.in';
        $from_name = 'Firstjob';
        $subject = "Net Core API test";

        $mail = new PHPMailer();
        $mail->IsSMTP();
        $mail->CharSet = 'UTF-8';
        $mail->Host = "smtp.falconide.com";
        $mail->SMTPAuth= true;
        $mail->Port = 25;
        $mail->Username= $account;
        $mail->Password= $password;
        //$mail->SMTPSecure = 'tls';
        $mail->From = $from;
        $mail->FromName= $from_name;
        $mail->isHTML(true);
        $mail->Subject = $subject;
        $mail->Body = 'Hello! This is testing';
        $mail->addAddress($to);

        if(!$mail->send()){
          return false;
        }else{
          return true;
        }
    }
}

if (!function_exists('sendEmailFalconideApi')) {
    /*
     *  @Author : Synergy
     *  @Params : array
     *  @Short Description : Use to send email
     *  @return : array with code. 
     */
    function sendEmailFalconideApi($api_type='', $api_activity='', $api_input='') {
        $data = array();
        $result = http_post_form("https://api.falconide.com/falconapi/web.send.rest", $api_input);
        return $result;
    }
}

if (!function_exists('http_post_form')) {
    /*
     *  @Author : Synergy
     *  @Params : array
     *  @Short Description : Use to send email
     *  @return : array with code. 
     */
    function http_post_form($url,$data,$timeout=20) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url); 
        curl_setopt($ch, CURLOPT_FAILONERROR, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); 
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout); 
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
        curl_setopt($ch, CURLOPT_POST, 1); 
        curl_setopt($ch, CURLOPT_RANGE,"1-2000000");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
        curl_setopt($ch, CURLOPT_REFERER, @$_SERVER['REQUEST_URI']);
        $result = curl_exec($ch); 
        $result = curl_error($ch) ? curl_error($ch) : $result;
        curl_close($ch);
        return $result;
    }
}


if (!function_exists('addJobRequisition')) {
    /**
     * addJobRequisition function
     * Import job into table.
     * @params i$params.
     * @return true or false.
     * 
     */
    function addJobRequisition($params){
        $CI     = &get_instance();

        $guid = $params['guid'];
        $position_name   = $params['position_name'];
        $job_code   = $params['job_code'];
        $job_name   = $params['job_name'];
        $job_description   = $params['job_description'];
        $requisition_created_date   = $params['requisition_created_date'];
        $requisition_closed_date   = $params['requisition_closed_date'];
        $requisition_state   = $params['requisition_state'];
        $requisition_job_status   = $params['requisition_job_status'];
        $organization_name   = $params['organization_name'];
        $country_belong   = $params['country_belong'];
        $region_belong   = $params['region_belong'];
        $city_belong   = $params['city_belong'];
        $postal_code_belong   = $params['postal_code_belong'];
        $pay_grade_type   = $params['pay_grade_type'];
        $pay_grade_region   = $params['pay_grade_region'];
        $pay_grade_group   = $params['pay_grade_group'];
        $pay_grade_level   = $params['pay_grade_level'];
        $currency   = $params['currency'];
        $min_sal_pay_grade_level   = $params['min_sal_pay_grade_level'];
        $max_sal_pay_grade_level   = $params['max_sal_pay_grade_level'];
        $sal_calculation_unit   = $params['sal_calculation_unit'];
        $createdBy   = $params['createdBy'];
        $companyjobcode= $params['fjCode'];

        $jobInsertArray = array();
        $jobIntegrationArray = array();



        /**************Inilizing data*****************************************/
        //Inilizing arry with guid data

        $jobInsertArray['updatedAt'] = date('Y-m-d h:i:s');
        $jobInsertArray['createdBy'] = $createdBy;
        $jobInsertArray['fjCode'] = $companyjobcode;
        $guidResult = $CI->db->query("SELECT * FROM fj_job_integration_configuration WHERE columnName='guid'")->row_array();
        
        if($guidResult['is_present'] == 1) {
            $jobInsertArray['guid'] = $guid;
        }
        //end of inilizing guid data

        //Inilizing arry with position data
        $positionResult = $CI->db->query("SELECT * FROM fj_job_integration_configuration WHERE columnName='position_name'")->row_array();
        
        if($positionResult['is_present'] == 1) {
            $jobInsertArray['position_name'] = $position_name;
        }
        //end of inilizing position data

        //Inilizing arry with jobcode data
        $jobCodeResult = $CI->db->query("SELECT * FROM fj_job_integration_configuration WHERE columnName='job_code'")->row_array();
        
        if($jobCodeResult['is_present'] == 1) {
            $jobInsertArray['jobId'] = $job_code;
        }
        //end of inilizing jobcode data

        //Inilizing arry with jobname data
        $jobNameResult = $CI->db->query("SELECT * FROM fj_job_integration_configuration WHERE columnName='job_name'")->row_array();
        
        if($jobNameResult['is_present'] == 1) {
            $jobInsertArray['title'] = $job_name;
        }
        //end of inilizing jobname data

        //Inilizing arry with jobname data
        $jobDescriptionResult = $CI->db->query("SELECT * FROM fj_job_integration_configuration WHERE columnName='job_description'")->row_array();
        
        if($jobDescriptionResult['is_present'] == 1) {
            $jobInsertArray['description'] = $job_description;
        }
        //end of inilizing jobname data


        //Inilizing arry with jobname data
        $jobCreatedResult = $CI->db->query("SELECT * FROM fj_job_integration_configuration WHERE columnName='requisition_created_date'")->row_array();
        
        if($jobCreatedResult['is_present'] == 1 && !empty($requisition_created_date)) {
            $jobInsertArray['createdAt'] = $requisition_created_date;
        } else {
            $jobInsertArray['createdAt'] = date('Y-m-d h:i:s');
        }
        //end of inilizing jobname data

        //Inilizing arry with jobname data
        $jobClosedResult = $CI->db->query("SELECT * FROM fj_job_integration_configuration WHERE columnName='requisition_closed_date'")->row_array();
        
        if($jobClosedResult['is_present'] == 1) {
            $jobInsertArray['openTill'] = $requisition_closed_date;
        }
        //end of inilizing jobname data

        //Inilizing arry with jobname data
        $jobStateResult = $CI->db->query("SELECT * FROM fj_job_integration_configuration WHERE columnName='requisition_state'")->row_array();
        
        if($jobStateResult['is_present'] == 1) {
            $jobInsertArray['state'] = $requisition_state;
        }
        //end of inilizing jobname data

        //Inilizing arry with jobname data
        $jobStatusResult = $CI->db->query("SELECT * FROM fj_job_integration_configuration WHERE columnName='requisition_job_status'")->row_array();
        
        if($jobStatusResult['is_present'] == 1) {
            $jobInsertArray['status'] = $requisition_job_status;
        }
        //end of inilizing jobname data

        //Inilizing arry with jobname data
        $OraganizationResult = $CI->db->query("SELECT * FROM fj_job_integration_configuration WHERE columnName='organization_name'")->row_array();
        
        if($OraganizationResult['is_present'] == 1) {
            $jobInsertArray['organization'] = $organization_name;
        }
        //end of inilizing jobname data

        //Inilizing arry with jobname data
        $countryResult = $CI->db->query("SELECT * FROM fj_job_integration_configuration WHERE columnName='country_belong'")->row_array();
        
        if($countryResult['is_present'] == 1) {
            $jobInsertArray['country'] = $country_belong;
        }
        //end of inilizing jobname data

        //Inilizing arry with jobname data
        $resionResult = $CI->db->query("SELECT * FROM fj_job_integration_configuration WHERE columnName='region_belong'")->row_array();
        
        if($resionResult['is_present'] == 1) {
            $jobInsertArray['region'] = $region_belong;
        }
        //end of inilizing jobname data

        //Inilizing arry with jobname data
        $cityResult = $CI->db->query("SELECT * FROM fj_job_integration_configuration WHERE columnName='city_belong'")->row_array();
        
        if($cityResult['is_present'] == 1) {
            $jobInsertArray['city'] = $city_belong;
        }
        //end of inilizing jobname data

        //Inilizing arry with jobname data
        $postalResult = $CI->db->query("SELECT * FROM fj_job_integration_configuration WHERE columnName='postal_code_belong'")->row_array();
        
        if($postalResult['is_present'] == 1) {
            $jobInsertArray['postal_code'] = $postal_code_belong;
        }
        //end of inilizing jobname data

        //Inilizing arry with jobname data
        $payGradeTypeResult = $CI->db->query("SELECT * FROM fj_job_integration_configuration WHERE columnName='pay_grade_type'")->row_array();
        
        if($payGradeTypeResult['is_present'] == 1) {
            $jobInsertArray['pay_grade_type'] = $pay_grade_type;
        }
        //end of inilizing jobname data

        //Inilizing arry with jobname data
        $payGradeRegionResult = $CI->db->query("SELECT * FROM fj_job_integration_configuration WHERE columnName='pay_grade_region'")->row_array();
        
        if($payGradeRegionResult['is_present'] == 1) {
            $jobInsertArray['pay_grade_region'] = $pay_grade_region;
        }
        //end of inilizing jobname data

        //Inilizing arry with jobname data
        $payGradeGroupResult = $CI->db->query("SELECT * FROM fj_job_integration_configuration WHERE columnName='pay_grade_group'")->row_array();
        
        if($payGradeGroupResult['is_present'] == 1) {
            $jobInsertArray['pay_grade_group'] = $pay_grade_group;
        }
        //end of inilizing jobname data

        //Inilizing arry with jobname data
        $payGradeLevelResult = $CI->db->query("SELECT * FROM fj_job_integration_configuration WHERE columnName='pay_grade_level'")->row_array();
        
        if($payGradeLevelResult['is_present'] == 1) {
            $jobInsertArray['pay_grade_level'] = $pay_grade_level;
        }
        //end of inilizing jobname data

        //Inilizing arry with jobname data
        $currencyResult = $CI->db->query("SELECT * FROM fj_job_integration_configuration WHERE columnName='currency'")->row_array();
        
        if($currencyResult['is_present'] == 1) {
            $jobInsertArray['currency'] = $currency;
        }
        //end of inilizing jobname data

        //Inilizing arry with jobname data
        $minSalResult = $CI->db->query("SELECT * FROM fj_job_integration_configuration WHERE columnName='min_sal_pay_grade_level'")->row_array();
        
        if($minSalResult['is_present'] == 1) {
            $jobInsertArray['salaryFrom'] = $min_sal_pay_grade_level;
        }
        //end of inilizing jobname data

        //Inilizing arry with jobname data
        $maxSalResult = $CI->db->query("SELECT * FROM fj_job_integration_configuration WHERE columnName='max_sal_pay_grade_level'")->row_array();
        
        if($maxSalResult['is_present'] == 1) {
            $jobInsertArray['salaryTo'] = $max_sal_pay_grade_level;
        }
        //end of inilizing jobname data

        //Inilizing arry with jobname data
        $salCalResult = $CI->db->query("SELECT * FROM fj_job_integration_configuration WHERE columnName='sal_calculation_unit'")->row_array();
        
        if($salCalResult['is_present'] == 1) {
            $jobInsertArray['sal_cal_unit'] = $sal_calculation_unit;
        }
        //end of inilizing jobname data
        $CI->db->insert('fj_jobs', $jobInsertArray);
        $insert_id  = $CI->db->insert_id();

        /**************end of Inilizing data*****************************************/

        /**************start of Inilizing data for  fj_job_integration***************************/
        if($guidResult['is_present'] == 0) {
            $guidInsert = array('jobId' =>$insert_id,'jobIntegrationConfigurationId' => $guidResult['id'], 'columnValue' => $guid);
            $jobIntegrationArray[] = $guidInsert;
        }

        if($positionResult['is_present'] == 0) {
            $positionInsert = array('jobId' =>$insert_id,'jobIntegrationConfigurationId' => $positionResult['id'], 'columnValue' => $position_name);
            $jobIntegrationArray[] = $positionInsert;
        }

        if($jobCodeResult['is_present'] == 0) {
            $jobCodeInsert = array('jobId' =>$insert_id,'jobIntegrationConfigurationId' => $jobCodeResult['id'], 'columnValue' => $job_code);
            $jobIntegrationArray[] = $jobCodeInsert;
        }

        if($jobNameResult['is_present'] == 0) {
            $jobNameInsert = array('jobId' =>$insert_id,'jobIntegrationConfigurationId' => $jobNameResult['id'], 'columnValue' => $job_name);
            $jobIntegrationArray[] = $jobNameInsert;
        }

        if($jobDescriptionResult['is_present'] == 0) {
            $jobDescriptionInsert = array('jobId' =>$insert_id,'jobIntegrationConfigurationId' => $jobDescriptionResult['id'], 'columnValue' => $job_description);
            $jobIntegrationArray[] = $jobDescriptionInsert;
        }

        if($jobCreatedResult['is_present'] == 0) {
            $jobCreatedInsert = array('jobId' =>$insert_id,'jobIntegrationConfigurationId' => $jobCreatedResult['id'], 'columnValue' => $requisition_created_date);
            $jobIntegrationArray[] = $jobCreatedInsert;
        }

        if($jobClosedResult['is_present'] == 0) {
            $jobClosedInsert = array('jobId' =>$insert_id,'jobIntegrationConfigurationId' => $jobClosedResult['id'], 'columnValue' => $requisition_closed_date);
            $jobIntegrationArray[] = $jobClosedInsert;
        }

        if($jobStateResult['is_present'] == 0) {
            $jobStateInsert = array('jobId' =>$insert_id,'jobIntegrationConfigurationId' => $jobStateResult['id'], 'columnValue' => $requisition_state);
            $jobIntegrationArray[] = $jobStateInsert;
        }

        if($jobStatusResult['is_present'] == 0) {
            $jobStatusInsert = array('jobId' =>$insert_id,'jobIntegrationConfigurationId' => $jobStatusResult['id'], 'columnValue' => $requisition_job_status);
            $jobIntegrationArray[] = $jobStatusInsert;
        }

        if($OraganizationResult['is_present'] == 0) {
            $OraganizationInsert = array('jobId' =>$insert_id,'jobIntegrationConfigurationId' => $OraganizationResult['id'], 'columnValue' => $organization_name);
            $jobIntegrationArray[] = $OraganizationInsert;
        }

        if($countryResult['is_present'] == 0) {
            $countryInsert = array('jobId' =>$insert_id,'jobIntegrationConfigurationId' => $countryResult['id'], 'columnValue' => $country_belong);
            $jobIntegrationArray[] = $countryInsert;
        }

        if($resionResult['is_present'] == 0) {
            $resionInsert = array('jobId' =>$insert_id,'jobIntegrationConfigurationId' => $resionResult['id'], 'columnValue' => $region_belong);
            $jobIntegrationArray[] = $resionInsert;
        }

        if($cityResult['is_present'] == 0) {
            $cityInsert = array('jobId' =>$insert_id,'jobIntegrationConfigurationId' => $cityResult['id'], 'columnValue' => $city_belong);
            $jobIntegrationArray[] = $cityInsert;
        }

        if($postalResult['is_present'] == 0) {
            $postalInsert = array('jobId' =>$insert_id,'jobIntegrationConfigurationId' => $postalResult['id'], 'columnValue' => $postal_code_belong);
            $jobIntegrationArray[] = $postalInsert;
        }

        if($payGradeTypeResult['is_present'] == 0) {
            $payGradeTypeInsert = array('jobId' =>$insert_id,'jobIntegrationConfigurationId' => $payGradeTypeResult['id'], 'columnValue' => $pay_grade_type);
            $jobIntegrationArray[] = $payGradeTypeInsert;
        }

        if($payGradeRegionResult['is_present'] == 0) {
            $payGradeRegionInsert = array('jobId' =>$insert_id,'jobIntegrationConfigurationId' => $payGradeRegionResult['id'], 'columnValue' => $pay_grade_region);
            $jobIntegrationArray[] = $payGradeRegionInsert;
        }

        if($payGradeGroupResult['is_present'] == 0) {
            $payGradeGroupInsert = array('jobId' =>$insert_id,'jobIntegrationConfigurationId' => $payGradeGroupResult['id'], 'columnValue' => $pay_grade_group);
            $jobIntegrationArray[] = $payGradeGroupInsert;
        }

        if($payGradeLevelResult['is_present'] == 0) {
            $payGradeLevelInsert = array('jobId' =>$insert_id,'jobIntegrationConfigurationId' => $payGradeLevelResult['id'], 'columnValue' => $pay_grade_level);
            $jobIntegrationArray[] = $payGradeLevelInsert;
        }

        if($currencyResult['is_present'] == 0) {
            $currencyInsert = array('jobId' =>$insert_id,'jobIntegrationConfigurationId' => $currencyResult['id'], 'columnValue' => $currency);
            $jobIntegrationArray[] = $currencyInsert;
        }

        if($minSalResult['is_present'] == 0) {
            $minSalInsert = array('jobId' =>$insert_id,'jobIntegrationConfigurationId' => $minSalResult['id'], 'columnValue' => $min_sal_pay_grade_level);
            $jobIntegrationArray[] = $minSalInsert;
        }

        if($maxSalResult['is_present'] == 0) {
            $maxSalInsert = array('jobId' =>$insert_id,'jobIntegrationConfigurationId' => $maxSalResult['id'], 'columnValue' => $max_sal_pay_grade_level);
            $jobIntegrationArray[] = $maxSalInsert;
        }

        if($salCalResult['is_present'] == 0) {
            $salCalInsert = array('jobId' =>$insert_id,'jobIntegrationConfigurationId' => $salCalResult['id'], 'columnValue' => $sal_calculation_unit);
            $jobIntegrationArray[] = $salCalInsert;
        }

        $returnResult = $CI->db->insert_batch('fj_job_integration', $jobIntegrationArray);

        if($returnResult) {
            return true;
        } else {
            return false;
        }
    }
}
?>
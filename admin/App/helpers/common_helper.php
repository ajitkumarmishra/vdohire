<?php

/* This function is used to include the CSS and JS (Sanjay Yadav) */
if (!function_exists('add_includes')) {

    function add_includes($type, $files, $options = NULL, $prepend_base_url = TRUE) {
        $ci = &get_instance();
        $includes = array();
        if (is_array($files) and ! empty($files)) {
            foreach ($files as $file) {
                if ($prepend_base_url) {
                    $ci->load->helper('url');
                    $file = base_url() . $file;
                }

                $includes[$type][] = array(
                    'file' => $file,
                    'options' => $options
                );
            }
        } else {
            if ($prepend_base_url) {
                $ci->load->helper('url');
                $file = base_url() . $files;
            }

            $includes[$type][] = array(
                'file' => $file,
                'options' => $options
            );
        }

        return $includes;
    }

}

/* End Of function add_includes  */

if (!function_exists('getActiveMenu')) {

    function getActiveMenu($url) {
        $CI = &get_instance();
        $alias = $CI->uri->segment(2);
        $mfl = $CI->router->fetch_class();
        $mthd = $CI->router->fetch_method();
        if ($mfl == $url && $mthd != 'setting') {
            echo "active-menu";
        }
    }

}

/* End Of function add_includes  */

if (!function_exists('getActiveResultMenu')) {

    function getActiveResultMenu($url) {
        $CI = &get_instance();
        $alias = $CI->input->get('cat',true);
        if ($alias == $url) {
            echo "active-menu";
        }
    }

}

if (!function_exists('getActiveStatus')) {

    function getActiveStatus($url) {
        $CI = &get_instance();
        $alias = $CI->uri->segment(2);
        $mfl = $CI->router->fetch_class();
        $mthd = $mfl . '/' . $CI->router->fetch_method();
        if ($mthd == $url) {
            echo "active";
        }
    }

}

if (!function_exists('check_auth')) {

    function check_auth() {
        $CI = &get_instance();
        $CI->load->module("users");
        //if (!$CI->users->_is_admin()) {
        if (!$CI->users->userdata()) {
            redirect('');
        }
    }

}

if (!function_exists('site_title')) {

    function site_title() {
        $CI = &get_instance();
        $CI->load->module("cms");
        $title = $CI->cms->getSitetitle();
        $title = @unserialize($title);
        return $title['c_title'];
    }

}
if (!function_exists('site_logo')) {

    function site_logo() {
        $CI = &get_instance();
        $CI->db->where('alias', 'logo');
        $query = $CI->db->get('cms');
        return $query->row()->content;
    }

}
if (!function_exists('state_name')) {

    function state_name($id) {
        $CI = &get_instance();
        $CI->db->where('id', $id);
        $query = $CI->db->get('state');
        if ($query->num_rows() > 0)
            return $query->row()->state_name;
    }

}
if (!function_exists('country_name')) {

    function country_name($id) {
        $CI = &get_instance();
        $CI->db->where('country_code', $id);
        $query = $CI->db->get('country');
        if ($query->num_rows() > 0)
            return $query->row()->country_name;
    }

}


if (!function_exists('getGoogleAnalyticCode')) {

    function getGoogleAnalyticCode($alias) {
        $CI = &get_instance();
        $CI->db->start_cache();
        $CI->db->where('alias', $alias);
        $query = $CI->db->get('cms');
        $CI->db->flush_cache();
        $qqq = $query->row();
        $yourCode = array();
        $yourCode['content'] = unserialize($qqq->content);
        return $yourCode['content']['description'];
    }

}

if (!function_exists('getPages')) {

    function getPages($slug = null) {
        $CI = &get_instance();
        $CI->db->start_cache();
        $con = array(
            'include_in' => $slug,
            'is_active' => '1',
            'is_deleted' => '0'
        );
        $CI->db->where($con);
        $query = $CI->db->get('pages');
        $CI->db->flush_cache();
        $qqq = $query->result();
        return $qqq;
    }

}

if (!function_exists('state_codes')) {

    function state_codes() {
        $CI = &get_instance();
        $query = $CI->db->get('state');
        $states = $query->result();
        $data = array();
        foreach ($states as $list) {
            $data[strtolower($list->state_name)] = $list->id;
        }
        return $data;
    }

}

if (!function_exists('getActiveCollapse')) {

    function getActiveCollapse($url) {
        $CI = &get_instance();
        $alias = $CI->uri->segment(2);
        $mfl = $CI->router->fetch_class();
        $mthd = $CI->router->fetch_method();
        if ($mfl == $url && $mthd != 'setting') {
            echo "in Active";
        }
    }

}

if (!function_exists('getActiveCollapseSetting')) {

    function getActiveCollapseSetting($url) {
        $CI = &get_instance();
        $alias = $CI->uri->segment(2);
        $mfl = $CI->router->fetch_class();
        $mthd = $CI->router->fetch_method();
        if ($mthd == 'setting') {
            echo "in Active";
        }
    }

}

if (!function_exists('site_limit')) {

    function site_limit() {
        $CI = &get_instance();
        $CI->load->module("cms");
        $title = $CI->cms->getSitetitle();
        $title = @unserialize($title);
        return $title['c_limit'];
    }

}

if (!function_exists('industry_list')) {

    function industry_list() {
        $CI = &get_instance();
        $CI->db->where(array('indus_id' => '0', 'is_active' => '1'));
        $query = $CI->db->get('nc_industry');
        $data = array();
        foreach ($query->result() as $list) {
            $data[$list->id] = $list->name;
        }
        return $data;
    }

}
if (!function_exists('functions_list')) {

    function functions_list($id) {
        $CI = &get_instance();
        $CI->db->where(array('indus_id' => $id, 'is_active' => '1'));
        $query = $CI->db->get('nc_industry');
        return $query->result();
    }

}

if (!function_exists('validate_user_interview')) {

    function validate_user_interview($id) {
        $CI = &get_instance();
        $CI->db->where('user_id', $id);
        $CI->db->select_max('interview_id');
        $query = $CI->db->get('nc_interview');
        if ($query->num_rows() > 0) {
            return $query->row()->interview_id;
        } else {
            return false;
        }
    }

}

if (!function_exists('validate_user_assessment_interview')) {

    function validate_user_assessment_interview($id) {
        $CI = &get_instance();
        $CI->db->where('user_id', $id);
        $CI->db->select_max('interview_id');
        $query = $CI->db->get('nc_assessment_interview');
        if ($query->num_rows() > 0) {
            return $query->row()->interview_id;
        } else {
            return false;
        }
    }

}

if (!function_exists('getForumUser')) {

    function getForumUser($id) {
        $CI = &get_instance();
        $CI->db->where('id', $id);
        $query = $CI->db->get('users');
        if ($query->num_rows() > 0) {
            return $query->row()->user_name;
        } else {
            return false;
        }
    }

}
?>
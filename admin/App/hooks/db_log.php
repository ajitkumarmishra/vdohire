<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// Name of Class as mentioned in $hook['post_controller]
class Db_log {
 
    function __construct() {
       // Anything except exit() :P
    }
 
    // Name of function same as mentioned in Hooks Config
    function logQueries() {
 
        $CI = & get_instance();
 
        $filepath = APPPATH . 'logs/Query-log-' . date('Y-m-d') . '.php'; // Creating Query Log file with today's date in application/logs folder
        $handle = fopen($filepath, "a+");                 // Opening file with pointer at the end of the file
 
        $times = $CI->db->query_times;                   // Get execution time of all the queries executed by controller
        foreach ($CI->db->queries as $key => $query) { 
            $sql = $query . " \n Execution Time:" . $times[$key]; // Generating SQL file alongwith execution time
            fwrite($handle, $sql . "\n\n");              // Writing it in the log file
            //echo $query; exit;
            $data['query']      = $query;
            $data['execTime']   = $times[$key];
            //print_r($data); exit;
            $data['path']       = current_url();            
            //echo current_url(); exit;
            $resp       = $CI->db->insert('fj_querylog', $data);
            //echo $CI->db->last_query(); exit;
        }
 
        fclose($handle);      // Close the file
    }
	
	// Name of function same as mentioned in Hooks Config
    function logRecruiterActivity() {
        $CI = & get_instance();

        if(isset($CI->session->userdata['logged_in'])) {
            $userData = $CI->session->userdata['logged_in'];

            $data['userId'] = $userData->id;
            $data['methodName'] = current_url();
            $CI->db->insert('fj_recruiterActivityLog', $data);
        }
    }
	
	
	
	function IncreaseTimeOut(){
		$CI = & get_instance();
		$CI->load->helper('fj');
		$logoutUrl = 'logout';
		
		/*
		if($CI->uri->segment(2) != $logoutUrl){
			
			if(isset($CI->session->userdata['logged_in'])) {
				
				$userData = $CI->session->userdata['logged_in'];
				
				if(isset($userData->timeout) && $userData->timeout != ""){
					
					$timeOutTime = strtotime($userData->timeout);
					$currentDateTime = strtotime(date('y-m-d H:i:s'));
					
					
					if($timeOutTime < $currentDateTime){
							
							
						$data['status'] = '2';
						$CI->db->where(array('userId' => $userData->id, 'serverSessionID' => $_SESSION['__ci_last_regenerate']))->update('fj_userSession',$data);
						
						
						$CI->session->sess_destroy();
						redirect('users/login');
					}else{
						
						
						$data['timeout'] = date('Y-m-d H:i:s', time() + 600);
						$_SESSION['logged_in']->timeout = $data['timeout'];
						$CI->db->where(array('userId' => $userData->id, 'status' => '1'))->update('fj_userSession',$data);
						
						
						if($CI->uri->segment(2) == '' || $CI->uri->segment(2) == 'login'){
							//$data['redirectUrl'] = base_url() . 'corporate/dashboard';
							//redirect($data['redirectUrl']);
						}else{
							
						}
						
					}
				}else{
					
				}
			}else{
				
				if(isset($_COOKIE['sessionId']) && $_COOKIE['sessionId'] != ""){
					
					//$data['redirectUrl'] = base_url() . 'users/login';
					//redirect($data['redirectUrl'], 'refresh');
					//$decryptedUserid = getValueByToken($_COOKIE['sessionId']);
					
					if($decryptedUserid == ""){
						
					}else{
						
						
						
						/*setcookie('sessionId', $_COOKIE['sessionId'], time() + (100), "/"); // 86400 = 1 day
								
						
						
						$data['redirectUrl'] = base_url() . 'corporate/dashboard';
						$CI->db->where('id', $decryptedUserid);
						
						$query = $CI->db->get('fj_users');
						$userdata = $query->row();
						
						$CI->session->set_userdata('logged_in', $userdata);
						
						
					}
					
				}else{
					
				}
			}
		}else{
			
		}*/
	}
	
 
}
<?php $this->load->view('includes/header'); ?>
<?php echo modules::run("menu"); ?>
<?php $this->load->view('includes/banner'); ?>
<div class="container">
    <div class="row">
        <div class="col-sm-3 col-md-3">
            <?php $this->load->view('includes/sidebar'); ?>
        </div>
        <div class="col-sm-9 col-md-9">
            <?php $this->load->view($main_content); ?>
        </div>
    </div>
</div>
<?php $this->load->view('includes/footer'); ?>
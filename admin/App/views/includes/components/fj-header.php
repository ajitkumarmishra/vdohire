<!DOCTYPE HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>firstJob</title>
<link rel="stylesheet" href="<?php echo base_url(); ?>/theme/css/firstJob/global.css" />
<link rel="stylesheet" href="<?php echo base_url(); ?>/theme/css/firstJob/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>/theme/css/firstJob/responsive.css">
<link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700' rel='stylesheet' type='text/css'>
<script src="<?php echo base_url(); ?>/theme/js/firstJob/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>/theme/js/firstJob/bootstrap.js"></script>
<script src="<?php echo base_url(); ?>/theme/js/firstJob/bootbox.min.js"></script>
</head>
<body>
<header class="header">
 <div class="container">
  <div class="row">
   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-15 ">
    <div class="logo">
     <img src="<?php echo base_url(); ?>/theme/images/logo_img.png" alt="" title="" class="img-responsive">
    </div>
    <span class="menu_icon"><a href="#"><img src="<?php echo base_url(); ?>/theme/images/menu_icon.png" alt="" title="" class="img-responsive"></a></span>
    <div class="cart_sec">
     <ul>
      <li><span class="bell"><a href="#"><img src="<?php echo base_url(); ?>/theme/images/bell_icon.png" alt="" title="" class="img-responsive"></a><a href="#" class="bell_num">5</a></span></li>
      <li class="msg_icon"><span class="bell"><a href="#"><img src="<?php echo base_url(); ?>/theme/images/message_icon.png" alt="" title="" class="img-responsive"></a><a href="#" class="msg_num">23</a></span></li>
      <li class="setting_icon"><span class="bell"><a href="#"><img src="<?php echo base_url(); ?>/theme/images/setting_icon.png" alt="" title="" class="img-responsive"></a><a href="#"></a></span></li>
     </ul>
    </div>
   </div>
   
   <div class="col-lg-9 col-md-9 col-sm-6 col-xs-15">
    <span class="signin_img"></span>
    <div class="header-profile-sec">
      <a data-toggle="dropdown" class="dropdown-toggle dropdown" href="#" aria-expanded="false"><span class="header-profile">Amit Kumar<b class="caret"></b></span></a>
       <ul class="dropdown-menu">
            <li><a href="#" class="nini"><span class="img__profile_border"><img title="" alt="" src="<?php echo base_url(); ?>/theme/images/01_profile_cart_fem_img.png"></span>Nini Carter</a></li>
            <li><a href="#" class="nini"><span class="img__profile_border"><img title="" alt="" src="<?php echo base_url(); ?>/theme/images/profile_cart_imagebox3.png"></span>My Purchases</a></li>
            <li><a href="#" class="nini"><span class="img__profile_border"><img title="" alt="" src="<?php echo base_url(); ?>/theme/images/profile_cart_imagebox2.png"></span>My Appoinments</a></li>
            <li class="header-nav-last-img"><a href="#" class="nini"><span class="img__profile_border"><img title="" alt="" src="<?php echo base_url(); ?>/theme/images/profile_cart_imagebox1.png"></span>Logout</a></li>
         </ul>
     </div>
    </div>
  </div>
 </div> 
 

      
        
</header>

<div class="panel-group RelatedBoards" id="accordion">
    <div class="side-profile">
        <div class="profile-userpic">
            <img src="<?php echo base_url(); ?>/theme/img/admin.jpg" class="img-responsive" alt="">
        </div>

        <div class="profile-usertitle">
            <div class="profile-usertitle-name">
                Welcome Admin
            </div>
            <div class="profile-usertitle-job">
                Developer
            </div>
        </div>

    </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                    Users <span class="text-primary pull-right"><i class="fa fa-fw">&#xf107;</i></span></a>
            </h4>
        </div>
        <div id="collapseOne" class="panel-collapse collapse <?php getActiveCollapse('users'); ?>">
            <div class="panel-body">
                <table class="table">
                    <tr>
                        <td class="<?php getActiveMenu('users'); ?>" >
                            <a href="<?php echo base_url('users'); ?>">Users List <span class="glyphicon glyphicon-th-list text-primary pull-right sub-icon"></span></a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                    CMS <span class="text-primary pull-right"><i class="fa fa-fw">&#xf107;</i></span><!--<span class="glyphicon glyphicon-chevron-down text-primary pull-right"></span>--></a>
            </h4>
        </div>
        <div id="collapseTwo" class="panel-collapse collapse <?php getActiveCollapse('cms'); ?>">
            <div class="panel-body">
                <table class="table">
                    <tr>
                        <td class="<?php getActiveMenu('cms'); ?>">
                            <a href="<?php echo base_url('cms'); ?>"><span class="glyphicon glyphicon-th-list text-primary pull-right sub-icon"></span>Pages</a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Setting <span class="text-primary pull-right"><i class="fa fa-fw">&#xf107;</i></span></a>
            </h4>
        </div>
        <div id="collapseThree" class="panel-collapse collapse <?php getActiveCollapseSetting('setting'); ?>">
            <div class="panel-body">
                <table class="table">
                    <tr>
                        <td class="<?php getActiveMenu('cms'); ?>">
                            <a href="<?php echo base_url('cms/setting'); ?>"><span class="glyphicon glyphicon-cog text-primary pull-right sub-icon"></span>Setting</a>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="panel-body">
                <table class="table">
                    <tr>
                        <td class="<?php getActiveMenu('cms'); ?>">
                            <a href="<?php echo base_url('cms/rules'); ?>"><span class="glyphicon glyphicon-cog text-primary pull-right sub-icon"></span>Guide & Rules</a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a href="<?php echo base_url('industry'); ?>">
                    Industry & Functions <span class="text-primary pull-right"><i class="fa fa-fw">&#xf107;</i></span></a>
            </h4>
        </div>
        
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseQues">
                    Questions <span class="text-primary pull-right"><i class="fa fa-fw">&#xf107;</i></span></a>
            </h4>
        </div>
        <div id="collapseQues" class="panel-collapse collapse <?php getActiveCollapse('questions'); ?>">
            <div class="panel-body">
                <table class="table">
                    <tr>
                        <td class="<?php getActiveMenu('Questions'); ?>">
                            <a href="<?php echo base_url('questions'); ?>">Assessment Questions<span class="glyphicon glyphicon-th-list text-primary pull-right sub-icon"></span></a>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="panel-body">
                <table class="table">
                    <tr>
                        <td class="<?php getActiveMenu('interview'); ?>">
                            <a href="<?php echo base_url('interview'); ?>">Interview Questions<span class="glyphicon glyphicon-th-list text-primary pull-right sub-icon"></span></a>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="panel-body">
                <table class="table">
                    <tr>
                        <td class="<?php getActiveMenu('sample'); ?>">
                            <a href="<?php echo base_url('sample'); ?>">Sample Interviews<span class="glyphicon glyphicon-th-list text-primary pull-right sub-icon"></span></a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseNews">
                    News & Updates <span class="text-primary pull-right"><i class="fa fa-fw">&#xf107;</i></span> <!--<span class="glyphicon glyphicon-chevron-down text-primary pull-right"></span>--></a>
            </h4>
        </div>
        <div id="collapseNews" class="panel-collapse collapse <?php getActiveCollapse('news'); ?>">
            <div class="panel-body">
                <table class="table">
                    <tr>
                        <td class="<?php getActiveMenu('news'); ?>">
                            <a href="<?php echo base_url('news'); ?>"><span class="glyphicon glyphicon-th-list text-primary pull-right sub-icon"></span>Manage News</a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
	<div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne11">
                    Forum<span class="text-primary pull-right"><i class="fa fa-fw">&#xf107;</i></span></a>
            </h4>
        </div>
        <div id="collapseOne11" class="panel-collapse collapse <?php getActiveCollapse('forum'); ?>">
            <div class="panel-body">
                <table class="table">
                    <tr>
                        <td class="<?php getActiveMenu('forum'); ?>" >
                            <a href="<?php echo base_url('forum'); ?>">Forum List <span class="glyphicon glyphicon-th-list text-primary pull-right sub-icon"></span></a>
                        </td>
                    </tr>
                </table>
            </div>
			
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseResult">
                    User Interaction View <span class="text-primary pull-right"><i class="fa fa-fw">&#xf107;</i></span><!--<span class="glyphicon glyphicon-chevron-down text-primary pull-right"></span>--></a>
            </h4>
        </div>
        <div id="collapseResult" class="panel-collapse collapse <?php getActiveCollapse('result'); ?>">
            <div class="panel-body">
                <table class="table">
                    <tr>
                        <td class="<?php getActiveResultMenu('Audio'); ?>">
                            <a href="<?php echo base_url('result/?cat=Audio'); ?>"><span class="glyphicon glyphicon-th-list text-primary pull-right sub-icon"></span>Audio</a>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="panel-body">
                <table class="table">
                    <tr>
                        <td class="<?php getActiveResultMenu('Video'); ?>">
                            <a href="<?php echo base_url('result/?cat=Video'); ?>"><span class="glyphicon glyphicon-th-list text-primary pull-right sub-icon"></span>Video</a>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="panel-body">
                <table class="table">
                    <tr>
                        <td class="<?php getActiveResultMenu('Audition'); ?>">
                            <a href="<?php echo base_url('result/?cat=Audition'); ?>"><span class="glyphicon glyphicon-th-list text-primary pull-right sub-icon"></span>Audition</a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>    
    <div class="panel panel-default">
        <div class="panel-heading"> 
            <h4 class="panel-title"> 
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOneAudtion">Audition<span class="text-primary pull-right"><i class="fa fa-fw">&#xf107;</i></span></a>
            </h4> 
        </div>  
        <div id="collapseOneAudtion" class="panel-collapse collapse <?php getActiveCollapse('Audition'); ?>">    
            <div class="panel-body">   
                <table class="table">  
                    <tr>              
                        <td class="<?php getActiveMenu('Audition'); ?>" > 
                            <a href="<?php echo base_url('audition'); ?>">Audition List <span class="glyphicon glyphicon-th-list text-primary pull-right sub-icon"></span></a>  
                        </td> 
                    </tr>   
                    <tr>      
                        <td class="<?php getActiveMenu('Audition-Add'); ?>" >     
                            <a href="<?php echo base_url('audition/add'); ?>">Audition Add <span class="glyphicon glyphicon-th-list text-primary pull-right sub-icon"></span></a>
                        </td> 
                    </tr>	
                </table>    
            </div>	
        </div>  
    </div>
</div>
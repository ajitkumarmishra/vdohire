<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title><?php
            if (isset($meta_title)) {
                echo $meta_title;
            }else{
                echo site_title();
            }
            ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="<?php
        if (isset($meta_description)) {
            echo $meta_description;
        }
        ?>">
        <meta name="keywords" content="<?php
        if (isset($meta_keyword)) {
            echo $meta_keyword;
        }
        ?>">
        <meta name="author" content="">

        <!-- Le styles -->
        <link href="<?php echo base_url(); ?>/theme/css/bootstrap.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>/theme/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>/theme/css/style.css" rel="stylesheet">
        <style type="text/css">
            body {
                padding-top: 60px;
                padding-bottom: 40px;
            }
        </style>
        <link href="<?php echo base_url(); ?>/theme/css/bootstrap-theme.css" rel="stylesheet">

        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <!-- Le fav and touch icons -->
        <link rel="shortcut icon" href="favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="apple-touch-icon-57-precomposed.png">

        <!-- PAGE LEVEL STYLES -->
        <?php if (isset($includes_for_layout_css['css']) AND count($includes_for_layout_css['css']) > 0): ?>
            <?php foreach ($includes_for_layout_css['css'] as $css): ?>
                <link rel="stylesheet" type="text/css" href="<?php echo $css['file']; ?>"<?php echo ($css['options'] === NULL ? '' : ' media="' . $css['options'] . '"'); ?>>
            <?php endforeach; ?>
        <?php endif; ?>
        <!-- END PAGE LEVEL  STYLES -->


        <!-- PAGE LEVEL  Header Javascript -->
        <?php if (isset($includes_for_layout_js['js']) AND count($includes_for_layout_js['js']) > 0): ?>
            <?php foreach ($includes_for_layout_js['js'] as $js): ?>
                <?php if ($js['options'] !== NULL AND $js['options'] == 'header'): ?>
                    <script type="text/javascript" src="<?php echo $js['file']; ?>"></script>
                <?php endif; ?>
            <?php endforeach; ?>
        <?php endif; ?>
        <!-- END PAGE LEVEL  Header Javascript -->


        <script>
            var base_url="<?php echo base_url(); ?>";
        </script>

    </head>

    <body>

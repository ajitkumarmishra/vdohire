<?php
if (!$this->session->userdata('logged_in')) {
    redirect(base_url('users/login'));
}
//added code for the permission
			$userId = getUserId();
			$userDet = getUserById($userId);
			$userRoleForMenu = $userDet['role'];

			if( $userRoleForMenu == 1 ) {
				$urlDashborad = 'superadmin/dashboard';
			}
			else if( $userRoleForMenu == 2 || $userRoleForMenu == 4 || $userRoleForMenu == 5 ) {
				$urlDashborad = 'corporate/dashboard'; 
			}

			//USER'S PERMISSION
			$userPermissionStr = "";
			$userPermissionArr = array();

			$userPermissionRes = dashboardUserPermissions($userId);

			if( $userPermissionRes ){
				$userPermissionStr = $userPermissionRes->permissionIds;
				$userPermissionArr = explode(",", $userPermissionStr);
			}
			//ALL PERMISSION FOR ADMIN
			$adminPermission = array(6,9,10,11,12,8);
			$corporatePermission = array(1,2,3,4,5,7,8);
			//SIDE MENU FOR LISTING
			$sideBarMenu = getMenuForSideBar();

			$currentFullBaseUrl = base_url(uri_string());
			$currentBaseUrl = str_replace(base_url(), "", $currentFullBaseUrl);
			$data['userPermissionArr'] = $userPermissionArr;
			$data['userRoleForMenu'] = $userRoleForMenu;
			//till here added code for the permission
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>VDOHire</title>
        <link href="<?php echo base_url(); ?>theme/firstJob/css/style.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url(); ?>theme/firstJob/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url(); ?>theme/firstJob/css/mtimepicker.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url(); ?>theme/firstJob/css/prism.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url(); ?>theme/firstJob/css/chosen.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url(); ?>theme/firstJob/css/jquery.ui.datepicker.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url(); ?>theme/firstJob/font-awesome-4.6.3/css/font-awesome.css" rel="stylesheet" type="text/css">
        <link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
        <script src="<?php echo base_url(); ?>theme/firstJob/js/jquery.js"></script>
        
        <script src="<?php echo base_url(); ?>theme/firstJob/js/mtimepicker.js"></script>
        <link href="<?php echo base_url(); ?>theme/firstJob/css/custom-style.css" rel="stylesheet">
        <script src="<?php echo base_url(); ?>theme/firstJob/js/jquery.tablesorter.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>theme/firstJob/js/jwplayer.js"></script>
        <link href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
        <!--[if lt IE 9]><script language="javascript" type="text/javascript" src="<?php echo base_url(); ?>/theme/firstJob/js/excanvas.js"></script><![endif]-->
        <!-- script language="javascript" type="text/javascript" src="<?php echo base_url(); ?>/theme/firstJob/js/jquery.min.js"></script -->
        <script language="javascript" type="text/javascript" src="<?php echo base_url(); ?>/theme/firstJob/js/jquery.jqplot.min.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/theme/firstJob/css/jquery.jqplot.css" />
        <script class="include" type="text/javascript" src="<?php echo base_url(); ?>/theme/firstJob/plugins/jqplot.cursor.js"></script>
        <script class="include" type="text/javascript" src="<?php echo base_url(); ?>/theme/firstJob/plugins/jqplot.highlighter.js"></script>
        <script class="include" type="text/javascript" src="<?php echo base_url(); ?>/theme/firstJob/plugins/jqplot.barRenderer.min.js"></script>
        <script class="include" type="text/javascript" src="<?php echo base_url(); ?>/theme/firstJob/plugins/jqplot.pieRenderer.min.js"></script>
        <script class="include" type="text/javascript" src="<?php echo base_url(); ?>/theme/firstJob/plugins/jqplot.categoryAxisRenderer.min.js"></script>
        <script class="include" type="text/javascript" src="<?php echo base_url(); ?>/theme/firstJob/plugins/jqplot.pointLabels.min.js"></script>
        <script class="include" type="text/javascript" src="<?php echo base_url(); ?>/theme/firstJob/plugins/jqplot.dateAxisRenderer.min.js"></script>
        
        
        <script type="text/javascript" src="<?php echo base_url(); ?>/theme/firstJob/plugins/jqplot.enhancedLegendRenderer.min.js"></script>
        <!--<script class="include" type="text/javascript" src="<?php //echo base_url(); ?>/theme/firstJob/plugins/jqplot.canvasAxisLabelRenderer.min.js"></script>
        <script class="include" type="text/javascript" src="<?php //echo base_url(); ?>/theme/firstJob/plugins/jqplot.canvasTextRenderer.min.js"></script>-->
        <!--
        <script type="text/javascript" src="<?php //echo base_url(); ?>/theme/firstJob/plugins/jqplot.canvasTextRenderer.js"></script>
        <script type="text/javascript" src="<?php //echo base_url(); ?>/theme/firstJob/plugins/jqplot.canvasAxisTickRenderer.js"></script>-->

        <style>
        video::-internal-media-controls-download-button {
            display:none;
        }

        video::-webkit-media-controls-enclosure {
            overflow:hidden;
        }

        video::-webkit-media-controls-panel {
            width: calc(100% + 30px); /* Adjust as needed */
        }
		.user_profile{
			float:left;
			margin-top: 0px;
			margin-top: 0px;
			background: white;
			border: 1px solid black;
			font-size: 16px;
			padding: 6px;
			height: auto;
			width: auto;
			    margin-top: 8px;
		}
		.user-mail{
			    float: left;
    padding-top: 17px;
    padding-right: 30px;
    cursor: pointer;
		}
		.row{
			margin-left:0px;
			margin-right:0px;
		}
		.notification{
			float:right;
            font-size: 21px;
            padding-top: 15px;
		}
		
		.account li:hover{
			background:#eee;
		}
		.account li a:hover{
			background:#eee;
		}
        </style>
    </head>
    <body>
        <nav class="navbar navbar-default navbar-fixed-top" style="background:#EB9B2C">
            <div class="container-fluid">
				<div class="row">
                    <?php
                        /******changed to display anchor tag to go on home page********/
                        $ci =&get_instance();
                        $ci->load->library('session');
                        
                        $tokenHeader = $this->config->item('accessToken');
                        $userDataHeader = $this->session->userdata['logged_in'];
                        if($userDataHeader->role == 1) {
                            $loggedinDashboard = 'superadmin/dashboard';
                        } else {
                            $loggedinDashboard = 'corporate/dashboard';
                        }
                        /******changed to display anchor tag to on home page********/
                    ?>
                    <div class="navbar-header">
                        <div class="">

                            <a href="<?php echo base_url().$loggedinDashboard; ?>">
								<img src="<?php echo base_url(); ?>/theme/firstJob/image/logo-new.png" class="img-responsive" style="padding-top: 4px;"></a>
                            
                        </div>
                    </div>
					
                    <?php                        
                    // ci instance
                    
                    /*
                    if (!$this->session->userdata('logged_in')) {
                        redirect(base_url('users/login'));
                    } else {
                        $userRoleForMenu = $userDataHeader->role;

                        if( $userRoleForMenu == 1 ) {
                            redirect(base_url('superadmin/dashboard'));
                        }
                        else if( $userRoleForMenu == 2 || $userRoleForMenu == 4 || $userRoleForMenu == 5 ) {
                            redirect(base_url('corporate/dashboard'));
                        }
                    }*/
                    
                    $userLoggeinIdHeader = $userDataHeader->id;
                    $userLoggeinFullnameHeader = $userDataHeader->fullname;
                    $userLoggeinCompanyHeader = $userDataHeader->company;
                    $userLoggeinCompanyLogo = $userDataHeader->companyLogo;
                    $userLoggeinImageHeader = $userDataHeader->image;
                    $userLoggeinRoleHeader = $userDataHeader->role;
                        
                    // notification model
                    $ci->load->model('../../../modules/Notification/models/notification_model');
                    
                    // function to get count of unread notification
                    $unreadnotifyheader = $ci->notification_model->countUnreadTopBarNotification($userLoggeinIdHeader); 
                    $unreadnotifycount = $unreadnotifyheader['unreadcount'];
                    
                    $unreadnotifylistheader = $ci->notification_model->latestNotificationForTopBar($userLoggeinIdHeader);
                    
                    //print_r($userDataHeader);
					$Applicants = "";
					$Home = "";
                    //echo $breadcrumb;exit;
					if(isset($breadcrumb) && $breadcrumb != ""){
						if($breadcrumb == 'applicants'){
							$Applicants = 'background: #054f72;';
						} else if($breadcrumb == 'jobs') {
                            $Jobs = 'background: #054f72;';
                        } else if($breadcrumb == 'users') {
                            $Recruiters = 'background: #054f72;';
                        }else if($breadcrumb == 'analytics') {
                            $Analytics = 'background: #054f72;';
                        } else{
							$Home = 'background: #054f72;';
						}
					}else{
							$Home = 'background: #054f72;';
						}
                    ?>
					<ul class="nav navbar-nav" style="    margin-left: 99px;font-size: 33px;">
						  <li><a href="<?php echo base_url()."corporate/dashboard"?>" style="<?php echo isset($Home) ? $Home : '' ?> color: white; padding-top: 25px;font-size: 20px;">Home</a></li>
						  <?php if(in_array('2', $userPermissionArr)|| ($userRoleForMenu == 2)){ ?>
						  <li><a href="<?php echo base_url()."jobs/list"?>" style="<?php echo isset($Jobs) ? $Jobs : '' ?> color: white;    padding-top: 25px;font-size: 20px;">Jobs</a></li>
						  <?php } ?>
						  <?php if(in_array('8', $userPermissionArr)|| ($userRoleForMenu == 2)){ ?>
						  <li><a href="<?php echo base_url()."users/list"?>" style="<?php echo isset($Recruiters) ? $Recruiters : '' ?>color: white;    padding-top: 25px;font-size: 20px;">Recruiters</a></li>
						  <?php } ?>
						  <?php if(in_array('3', $userPermissionArr)|| ($userRoleForMenu == 2)){ ?>
						  <li class=""><a style="<?php echo isset($Applicants) ? $Applicants : '' ?> color: white;font-size: 20px; padding-top:25px;" href="<?php echo base_url()."queue/list"?>" >Applicants</a></li>
						  
						  <?php } ?>
						  <?php if($userRoleForMenu == 2){ ?>
						  <li><a href="<?php echo base_url()."analytics/list" ?>" style="<?php echo isset($Analytics) ? $Analytics : '' ?> color: white;    padding-top: 25px;font-size: 20px;">Reports</a></li>
						  <?php } ?>
						  
	 
					</ul>
					<div class="user_profile" style="float:right;padding: 3px; margin-top:14px;">
							<img src="<?php echo base_url()."/theme/firstJob/image/user_image.png" ?>" alt="userimage" style="    width: 26px;">
                            
                            <div class="dropdown inline_blk">	     
                                <span class="username" data-toggle="dropdown">
                                    <?php
                                    if( $userLoggeinRoleHeader == 2 ):
                                        echo ($userLoggeinCompanyHeader!=""?ucwords($userLoggeinCompanyHeader):"");
                                    else:
                                        echo ($userLoggeinFullnameHeader!=""?ucwords($userLoggeinFullnameHeader):"");
                                    endif;
                                    ?>
                                    <i class="fa fa-caret-down" name="menu-down-arrow" id="menu-down-arrow"></i>
                                </span>
                                <ul class="account dropdown-menu" style="width:50px;left:-47px;">
                                    <?php if($userLoggeinRoleHeader == 2) { ?>
                                        <li style="    border-bottom: none;"><a href="<?php echo base_url(); ?>users/myAccounts">My Account</a></li>
                                    <?php } ?>
                                    <li style="    border-bottom: none;"><a href="<?php echo base_url(); ?>users/changePassword">Change Password</a></li>
                                    <li style="    border-bottom: none;"><a href="<?php echo base_url(); ?>users/logout">Logout</a></li>
                                </ul>
                            </div>
                    </div>
					<?php if( $unreadnotifycount > 0 && $unreadnotifycount != "" ) { ?>
						<div class="notification">
                            <a href="<?php echo base_url(); ?>notification/list/1" style="color:white;"><i class="fa fa-bell" aria-hidden="true"></i><span class="noti_conter" style="display: <?=(($unreadnotifycount>0 && $unreadnotifycount != "")?"block":"none")?>"><?=(($unreadnotifycount>0 && $unreadnotifycount != "")?$unreadnotifycount:"")?></span></a>
                        </div>
                        <?php } else { ?>
						<div class="notification">    
                            <a href="<?php echo base_url(); ?>notification/list/1" style="color:white;"><i class="fa fa-bell" aria-hidden="true"></i><span class="noti_conter" style="display: <?=(($unreadnotifycount>0 && $unreadnotifycount != "")?"block":"none")?>"><?=(($unreadnotifycount>0 && $unreadnotifycount != "")?$unreadnotifycount:"")?></span></a>
                        </div>
                        <?php } ?>
						
					<?php if($unreadnotifylistheader) { ?>
                        <ul class="dropdown-menu tp" id="navNotification">
                                <?php $i = 1; foreach($unreadnotifylistheader as $unreadnotifylistheaderval): ?>
                                
                                <?php if($i==count($unreadnotifylistheader)): ?>
                                <li class="no_border_bt"><a href="#"><?php echo $unreadnotifylistheaderval->notifyMessage; ?></a></li>
                                <?php else: ?>
                                <li><a href="#"><?php echo $unreadnotifylistheaderval->notifyMessage; ?></a></li>
                                <?php endif; ?>
                                <?php $i++; endforeach;?>
                                <li><a href="<?php echo base_url(); ?>notification/list/1">View All</a></li>
                            </ul>
                        <style>
                            #navNotification {
                                width: 500px;
                                height: 200px;
                                line-height: 2em;
                                border: 1px solid #ccc;
                                padding: 0;
                                margin: 0;
                                overflow: scroll;
                                overflow-x: hidden;
                            }

                            #navNotification li {
                                border-top: 1px solid #ccc;
                            }

                            #navNotification ul ul {
                                text-indent: 1em;
                            }
                        </style>
                    <?php } ?>
	  
                    <?php
                    // reply message model
                    $ci->load->model('../../../modules/Message/models/message_model');
                    
                    // function to get count of unread messages
                    $unreadrepliedresheader = $ci->message_model->countUnreadTopBarMessage($userLoggeinIdHeader); 
                    $unreadheadercount = $unreadrepliedresheader['unreadcount'];
                    ?>
					<div class="user-mail" style="float:right; padding-top: 15px; font-size: 21px;">
                        <a href="<?php echo base_url(); ?>message/list/1" style="color:white;"><i class="fa fa-envelope" aria-hidden="true"></i><span class="noti_mail" style="display: <?=(($unreadheadercount>0 && $unreadheadercount != "")?"block":"none")?>"><?=(($unreadheadercount>0 && $unreadheadercount != "")?$unreadheadercount:"")?></span></a>
					</div>
                </div>
            </div>
        </nav>
		<div style="clear:both"></div>
		
        <?php
        $videoType = $this->config->config['videoType'];
        $audioType = $this->config->config['audioType'];
        ?>
        <script>

            $(function () {
                window.siteUrl = '<?php echo base_url(); ?>';
                window.token = '<?php echo $this->config->item('accessToken'); ?>';
                window.videoExtension = '<?php echo json_encode($videoType); ?>';
                window.audioExtension = '<?php echo json_encode($audioType); ?>';
                /*$('.prev-page').on('click', function () {
                    var httpReferrer = '<?php //echo isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '' ; ?>';
                    if(!empty(httpReferrer)) {
                        window.location.href = httpReferrer;
                    }
                });
                $('.cross-search').on('click', function () {
                    $('.search_box').val('');
                    $(this).closest("form").submit();
                });
                $('.cross-search-candidate').on('click', function () {
                    var httpReferrer = '<?php //echo isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '' ; ?>';
                    if(!empty(httpReferrer)) {
                        window.location.href = httpReferrer;
                    }
                });*/
            });
        </script>

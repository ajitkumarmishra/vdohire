<?php
if (!$this->session->userdata('logged_in')) {
    redirect(base_url('users/login'));
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>VDOHire</title>
        <link href="<?php echo base_url(); ?>theme/firstJob/css/style.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url(); ?>theme/firstJob/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url(); ?>theme/firstJob/css/mtimepicker.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url(); ?>theme/firstJob/css/prism.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url(); ?>theme/firstJob/css/chosen.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url(); ?>theme/firstJob/css/simple-sidebar.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url(); ?>theme/firstJob/css/jquery.ui.datepicker.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url(); ?>theme/firstJob/font-awesome-4.6.3/css/font-awesome.css" rel="stylesheet" type="text/css">
        <link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
        <script src="<?php echo base_url(); ?>theme/firstJob/js/jquery.js"></script>
        <script src="<?php echo base_url(); ?>theme/firstJob/js/custom-script.js"></script>
        <script src="<?php echo base_url(); ?>theme/firstJob/js/mtimepicker.js"></script>
        <link href="<?php echo base_url(); ?>theme/firstJob/css/custom-style.css" rel="stylesheet">
        <script src="<?php echo base_url(); ?>theme/firstJob/js/jquery.tablesorter.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>theme/firstJob/js/jwplayer.js"></script>
        
        <!--[if lt IE 9]><script language="javascript" type="text/javascript" src="<?php echo base_url(); ?>/theme/firstJob/js/excanvas.js"></script><![endif]-->
        <!-- script language="javascript" type="text/javascript" src="<?php echo base_url(); ?>/theme/firstJob/js/jquery.min.js"></script -->
        <script language="javascript" type="text/javascript" src="<?php echo base_url(); ?>/theme/firstJob/js/jquery.jqplot.min.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/theme/firstJob/css/jquery.jqplot.css" />
        <script class="include" type="text/javascript" src="<?php echo base_url(); ?>/theme/firstJob/plugins/jqplot.cursor.js"></script>
        <script class="include" type="text/javascript" src="<?php echo base_url(); ?>/theme/firstJob/plugins/jqplot.highlighter.js"></script>
        <script class="include" type="text/javascript" src="<?php echo base_url(); ?>/theme/firstJob/plugins/jqplot.barRenderer.min.js"></script>
        <script class="include" type="text/javascript" src="<?php echo base_url(); ?>/theme/firstJob/plugins/jqplot.pieRenderer.min.js"></script>
        <script class="include" type="text/javascript" src="<?php echo base_url(); ?>/theme/firstJob/plugins/jqplot.categoryAxisRenderer.min.js"></script>
        <script class="include" type="text/javascript" src="<?php echo base_url(); ?>/theme/firstJob/plugins/jqplot.pointLabels.min.js"></script>
        <script class="include" type="text/javascript" src="<?php echo base_url(); ?>/theme/firstJob/plugins/jqplot.dateAxisRenderer.min.js"></script>
        
        
        <script type="text/javascript" src="<?php echo base_url(); ?>/theme/firstJob/plugins/jqplot.enhancedLegendRenderer.min.js"></script>
        <script class="include" type="text/javascript" src="<?php echo base_url(); ?>/theme/firstJob/plugins/jqplot.canvasAxisLabelRenderer.min.js"></script>
        <script class="include" type="text/javascript" src="<?php echo base_url(); ?>/theme/firstJob/plugins/jqplot.canvasTextRenderer.min.js"></script>
        
        <script type="text/javascript" src="<?php echo base_url(); ?>/theme/firstJob/plugins/jqplot.canvasTextRenderer.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>/theme/firstJob/plugins/jqplot.canvasAxisTickRenderer.js"></script>

        <style>
        video::-internal-media-controls-download-button {
            display:none;
        }

        video::-webkit-media-controls-enclosure {
            overflow:hidden;
        }

        video::-webkit-media-controls-panel {
            width: calc(100% + 30px); /* Adjust as needed */
        }
        </style>
    </head>
    <body>
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid no_padding">
                <div class="row">
                    <?php
                        /******changed to display anchor tag to go on home page********/
                        $ci =&get_instance();
                        $ci->load->library('session');
                        
                        $tokenHeader = $this->config->item('accessToken');
                        $userDataHeader = $this->session->userdata['logged_in'];
                        if($userDataHeader->role == 1) {
                            $loggedinDashboard = 'superadmin/dashboard';
                        } else {
                            $loggedinDashboard = 'corporate/dashboard';
                        }
                        /******changed to display anchor tag to on home page********/
                    ?>
                    <div class="col-md-3 rsp_logo">
                        <div class="logo">

                            <a href="<?php echo base_url().$loggedinDashboard; ?>"><img src="<?php echo base_url(); ?>/theme/firstJob/image/logo.png" class="img-responsive"></a>
                            <a href="#menu-toggle" id="menu-toggle"><span class="circle_menu">
                                <img src="<?php echo base_url(); ?>/theme/firstJob/image/menu.png" class="img_menu"></span>
                            </a>
                        </div>
                    </div>
                    <?php                        
                    // ci instance
                    
                    /*
                    if (!$this->session->userdata('logged_in')) {
                        redirect(base_url('users/login'));
                    } else {
                        $userRoleForMenu = $userDataHeader->role;

                        if( $userRoleForMenu == 1 ) {
                            redirect(base_url('superadmin/dashboard'));
                        }
                        else if( $userRoleForMenu == 2 || $userRoleForMenu == 4 || $userRoleForMenu == 5 ) {
                            redirect(base_url('corporate/dashboard'));
                        }
                    }*/
                    
                    $userLoggeinIdHeader = $userDataHeader->id;
                    $userLoggeinFullnameHeader = $userDataHeader->fullname;
                    $userLoggeinCompanyHeader = $userDataHeader->company;
                    $userLoggeinCompanyLogo = $userDataHeader->companyLogo;
                    $userLoggeinImageHeader = $userDataHeader->image;
                    $userLoggeinRoleHeader = $userDataHeader->role;
                        
                    // notification model
                    $ci->load->model('../../../modules/Notification/models/notification_model');
                    
                    // function to get count of unread notification
                    $unreadnotifyheader = $ci->notification_model->countUnreadTopBarNotification($userLoggeinIdHeader); 
                    $unreadnotifycount = $unreadnotifyheader['unreadcount'];
                    
                    $unreadnotifylistheader = $ci->notification_model->latestNotificationForTopBar($userLoggeinIdHeader);
                    
                    //print_r($userDataHeader);
                    ?>
                    <div class="col-md-6 no_padding_ico rsp_ico">
                        <?php if( $unreadnotifycount > 0 && $unreadnotifycount != "" ) { ?>
                        <div class="notification" type="button" data-toggle="dropdown" aria-expanded="true">
                            <a href="<?php echo base_url(); ?>notification/list/1"><img src="<?php echo base_url(); ?>/theme/firstJob/image/notification.png" alt="notification"><span class="noti_conter" style="display: <?=(($unreadnotifycount>0 && $unreadnotifycount != "")?"block":"none")?>"><?=(($unreadnotifycount>0 && $unreadnotifycount != "")?$unreadnotifycount:"")?></span></a>
                        </div>
                        <?php } else { ?>
                        <div class="notification" type="button">
                            <a href="<?php echo base_url(); ?>notification/list/1"><img src="<?php echo base_url(); ?>/theme/firstJob/image/notification.png" alt="notification"><span class="noti_conter" style="display: <?=(($unreadnotifycount>0 && $unreadnotifycount != "")?"block":"none")?>"><?=(($unreadnotifycount>0 && $unreadnotifycount != "")?$unreadnotifycount:"")?></span></a>
                        </div>
                        <?php } ?>
                        <?php if($unreadnotifylistheader) { ?>
                        <ul class="dropdown-menu tp" id="navNotification">
                                <?php $i = 1; foreach($unreadnotifylistheader as $unreadnotifylistheaderval): ?>
                                
                                <?php if($i==count($unreadnotifylistheader)): ?>
                                <li class="no_border_bt"><a href="#"><?php echo $unreadnotifylistheaderval->notifyMessage; ?></a></li>
                                <?php else: ?>
                                <li><a href="#"><?php echo $unreadnotifylistheaderval->notifyMessage; ?></a></li>
                                <?php endif; ?>
                                <?php $i++; endforeach;?>
                                <li><a href="<?php echo base_url(); ?>notification/list/1">View All</a></li>
                            </ul>
                        <style>
#navNotification {
    width: 500px;
    height: 200px;
    line-height: 2em;
    border: 1px solid #ccc;
    padding: 0;
    margin: 0;
    overflow: scroll;
    overflow-x: hidden;
}

#navNotification li {
    border-top: 1px solid #ccc;
}

#navNotification ul ul {
    text-indent: 1em;
}
                        </style>
                        <?php } ?>
                        <?php
                        // reply message model
                        $ci->load->model('../../../modules/Message/models/messagereply_model');
                        
                        // function to get count of unread messages
                        $unreadrepliedresheader = $ci->messagereply_model->countUnreadTopBarMessage($userLoggeinIdHeader); 
                        $unreadheadercount = $unreadrepliedresheader['unreadcount'];
                        ?>
                        <div class="mail"><a href="<?php echo base_url(); ?>message/list/1"><img src="<?php echo base_url(); ?>/theme/firstJob/image/message.png" alt="message"><span class="noti_mail" style="display: <?=(($unreadheadercount>0 && $unreadheadercount != "")?"block":"none")?>"><?=(($unreadheadercount>0 && $unreadheadercount != "")?$unreadheadercount:"")?></span></a></div>
                        <!-- <div class="Setting"><img src="<?php echo base_url(); ?>/theme/firstJob/image/setting.png" alt="setting"></div> -->
                    </div>

                    <div class="col-md-3 user_detail rsp_pro">
                        <div class="user_profile">
                            <?php
                            if($userLoggeinRoleHeader=='1') {
                                echo "<img src=\"".base_url()."theme/images/logo_img.png\"  alt=\"userimage\" style=\"width:46px; height:46px;\">";
                            }
                            else if($userLoggeinRoleHeader=='2') {
                                if($userLoggeinCompanyLogo!='')
                                echo "<img src=\"".base_url()."uploads/corporateLogo/".$userLoggeinCompanyLogo."\" alt=\"userimage\" style=\"width:46px; height:46px;\">";
                                else
                                echo "<img src=\"".base_url()."/theme/firstJob/image/user_image.png\" alt=\"userimage\">";
                            }
                            else if($userLoggeinRoleHeader=='4' || $userLoggeinRoleHeader=='5') {
                                if($userLoggeinImageHeader!='')
                                echo "<img src=\"".base_url()."uploads/userImages/".$userLoggeinImageHeader."\" alt=\"userimage\" style=\"width:46px; height:46px;\">";
                                else
                                echo "<img src=\"".base_url()."/theme/firstJob/image/user_image.png\" alt=\"userimage\" style=\"width:46px; height:46px;\">";
                            }
                            else {
                                echo "<img src=\"".base_url()."/theme/firstJob/image/user_image.png\" alt=\"userimage\" style=\"width:46px; height:46px;\">";
                            }
                            ?>
                            <div class="dropdown inline_blk">	     
                                <span class="username" data-toggle="dropdown">
                                    <?php
                                    if( $userLoggeinRoleHeader == 2 || $userLoggeinRoleHeader == 4 || $userLoggeinRoleHeader == 5):
                                        echo ($userLoggeinCompanyHeader!=""?ucwords($userLoggeinCompanyHeader):"");
                                    else:
                                        echo ($userLoggeinFullnameHeader!=""?ucwords($userLoggeinFullnameHeader):"");
                                    endif;
                                    ?>
                                    <i class="fa fa-caret-down" name="menu-down-arrow" id="menu-down-arrow"></i>
                                </span>
                                <ul class="dropdown-menu" style="width:50px;">
                                    <li><a href="<?php echo base_url(); ?>users/changePassword">Change Password</a></li>
                                    <li><a href="<?php echo base_url(); ?>users/logout">Logout</a></li>
                                </ul>
                            </div>
                        </div>
                        <div>Welcome
                            <?php
                                if( empty($userLoggeinFullnameHeader) )
                                    echo $userLoggeinCompanyHeader;
                                else
                                    echo $userLoggeinFullnameHeader;
                            ?>
                        </div>
                    </div>

                </div>
            </div>
        </nav>
        <?php
        $videoType = $this->config->config['videoType'];
        $audioType = $this->config->config['audioType'];
        ?>
        <script>

            $(function () {
                window.siteUrl = '<?php echo base_url(); ?>';
                window.token = '<?php echo $this->config->item('accessToken'); ?>';
                window.videoExtension = '<?php echo json_encode($videoType); ?>';
                window.audioExtension = '<?php echo json_encode($audioType); ?>';
                $('.prev-page').on('click', function () {
                    var httpReferrer = '<?php echo $_SERVER['HTTP_REFERER']; ?>';
                    window.location.href = httpReferrer;
                });
                $('.cross-search').on('click', function () {
                    $('.search_box').val('');
                    $(this).closest("form").submit();
                    //$('form').submit();
                    //var httpReferrer = '<?php echo $_SERVER['HTTP_REFERER']; ?>';
                    //window.location.href = httpReferrer;
                });
                $('.cross-search-candidate').on('click', function () {
                    var httpReferrer = '<?php echo $_SERVER['HTTP_REFERER']; ?>';
                    window.location.href = httpReferrer;
                });
            });
        </script>

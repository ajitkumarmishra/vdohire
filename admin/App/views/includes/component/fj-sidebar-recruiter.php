<?php  $userId = getUserId(); 
$userDet = getUserById($userId);
$userRoleForMenu = $userDet['role'];

if( $userRoleForMenu == 1 ) {
    $urlDashborad = 'superadmin/dashboard';
}
else if( $userRoleForMenu == 2 || $userRoleForMenu == 4 || $userRoleForMenu == 5 ) {
    $urlDashborad = 'corporate/dashboard'; 
}

//USER'S PERMISSION
$userPermissionStr = "";
$userPermissionArr = array();

$userPermissionRes = dashboardUserPermissions($userId);
if( $userPermissionRes ){
    $userPermissionStr = $userPermissionRes->permissionIds;
    $userPermissionArr = explode(",", $userPermissionStr);
}
//ALL PERMISSION FOR ADMIN
$adminPermission = array(6,9,10,11,12,8);
$corporatePermission = array(1,2,3,4,5,7,8);
//SIDE MENU FOR LISTING
$sideBarMenu = getMenuForSideBar();

$currentFullBaseUrl = base_url(uri_string());
$currentBaseUrl = str_replace(base_url(), "", $currentFullBaseUrl);

?>
<div id="sidebar-wrapper">
    <ul class="sidebar-nav">
        <?php if( $currentBaseUrl == $urlDashborad ){ ?>
        <li><a class="active_page" href="<?php echo base_url().$urlDashborad;?>"><img style="width:28px;" src="<?php echo base_url(); ?>/theme/image/dashboard_icon_yl.png"> Dashboard</a></li>
        <?php } else { ?>
        <li><a href="<?php echo base_url().$urlDashborad;?>"><img style="width:28px;" src="<?php echo base_url(); ?>/theme/image/dashboard_icon.png"> Dashboard</a></li>
        <?php } ?>
        <?php 
        foreach ($sideBarMenu as $item):
            
            if ($currentBaseUrl == $item->menuUrl):
                $activePage = "class='active_page'";
                $menuIconString = $item->menuIcon;
                $explodeIcon = explode('/', $menuIconString); // split all parts

                $endIcon = '';
                $beginIcon = '';

                if(count($explodeIcon) > 0):
                    $endIcon = array_pop($explodeIcon); // removes the last element, and returns it
                    $endIconArr = explode(".", $endIcon);
                    $menuIconImage = "theme/image/".$endIconArr[0]."_yl.".$endIconArr[1];
                else:
                    $menuIconImage = $item->menuIcon;
                endif;
            else:
                $activePage = "";
                $menuIconImage = $item->menuIcon;
            endif;
                    
            
            if( $userRoleForMenu == 1 ):
                if( $item->menuUrl == "" ):
                    echo "<li><a href=\"#\"><img style=\"width:28px;\" src=\"".base_url().$menuIconImage."\">".$item->name."</a></li>";
                else:   
                    echo "<li><a ".$activePage." href=\"".base_url().$item->menuUrl."\"><img style=\"width:28px;\" src=\"".base_url().$menuIconImage."\">".$item->name."</a></li>";
                endif;
            elseif( $userRoleForMenu == 2 ):
                if( in_array($item->id, $corporatePermission) ):
                    if( $item->menuUrl == "" ):
                        echo "<li><a class=\"active_page\" href=\"#\"><img style=\"width:28px;\" src=\"".base_url().$menuIconImage."\">".$item->name."</a></li>";
                    else:
                        echo "<li><a ".$activePage." href=\"".base_url().$item->menuUrl."\"><img style=\"width:28px;\" src=\"".base_url().$menuIconImage."\">".$item->name."</a></li>";
                    endif;
                endif;
            elseif( $userRoleForMenu != 1 && $userRoleForMenu != 2 ):
                if( !in_array($item->id, $adminPermission) ):
                    if( in_array($item->id, $userPermissionArr) ):
                        echo "<li><a ".$activePage." href=\"".base_url().$item->menuUrl."\"><img style=\"width:28px;\" src=\"".base_url().$menuIconImage."\">".$item->name."</a></li>";
                    endif;
                endif;
            endif;
        
        endforeach; 
        ?>
        <li><a href="#">&emsp;</a></li>
        <li><a href="#">&emsp;</a></li>
        <li><a href="#">&emsp;</a></li>
        <li><a href="#">&emsp;</a></li>
        
    </ul>
</div>

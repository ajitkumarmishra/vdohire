<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>VDOHire</title>
    <link href="<?php echo base_url(); ?>theme/firstJob/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>theme/firstJob/css/simple-sidebar.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>theme/firstJob/font-awesome-4.6.3/css/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>theme/firstJob/css/style.css" rel="stylesheet" type="text/css">
    <script src="<?php echo base_url(); ?>theme/firstJob/js/jquery.js"></script>
    <link href="<?php echo base_url(); ?>theme/firstJob/css/custom-style.css" rel="stylesheet">
</head>
<body>

<div class="container">
    <div class="row">
        Hi <?php echo ucfirst($name);?>,<br/><br/>
        You are receiving this e-mail because you requested for your password information.<br/><br/>
        Please find the updated login details mentioned below:<br/><br/>
        Your new password is : <?php echo $password; ?><br/><br/>
        Please keep this information safe.<br/><br/>
        Enjoy browsing the VDOHire platform.<br/><br/>
        Thanks<br/><br/>
        Team VDOHire
    </div>
</div>
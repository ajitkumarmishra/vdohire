<div class="container">
    <div class="row">
        Hello <?php echo $name;?>,<br/><br/>
        We have received your Forgot Password Request.<br/><br/>
        Please find the updated login details mentioned below:<br/><br/>
        URL : <a href="<?php echo $url;?>"><?php echo $url; ?></a><br/>
        Email : <?php echo $email; ?> <br/>
        Password : <?php echo $password; ?><br/><br/>
        After login, you can change the password as per your requirement.<br/><br/>
        Note - If you haven’t requested for password change, please immediately contact at <a href="mailto:hello@firstjob.co.in">hello@firstjob.co.in</a><br/><br/>
        Enjoy browsing the VDOHire platform.<br/><br/>
        <h3>Happy Hiring !</h3>
        Team VDOHire
    </div>
</div>


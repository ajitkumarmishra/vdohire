<?php                        
// ci instance
$ci =&get_instance();
$ci->load->library('session');

$tokenHeader                = $this->config->item('accessToken');
$userDataHeader             = $this->session->userdata['logged_in'];
$userLoggeinIdHeader        = $userDataHeader->id;
$userLoggeinFullnameHeader  = $userDataHeader->fullname;
$userLoggeinCompanyHeader   = $userDataHeader->company;
$userLoggeinCompanyLogo     = $userDataHeader->companyLogo;
$userLoggeinImageHeader     = $userDataHeader->image;
$userLoggeinRoleHeader      = $userDataHeader->role;
?>
<div class="container">
    <div class="row">
        Hello,<br/><br/>
        You have been invited to view the interview submitted by <b><?php echo ucfirst($appUserName);?></b> for the opening <b><?php echo ucwords($appUserJob);?></b>.  You can view the interview by <a href="<?php echo $videoLink;?>">clicking here </a>.<br/>
        You can also share your feedback through the link.<br/><br/>
        
        Thanks,<br/>
        <?php 
        if( $userLoggeinRoleHeader == 2 ):
            echo ($userLoggeinCompanyHeader!=""?ucwords($userLoggeinCompanyHeader):"");
        else:
            echo ($userLoggeinFullnameHeader!=""?ucwords($userLoggeinFullnameHeader):"");
        endif;
        ?><br/><br/>
        
        This is a system generated email. VDOHire. 
    </div>
</div>


function echeck(str) {

    var at = "@"
    var dot = "."
    var lat = str.indexOf(at)
    var lstr = str.length
    var ldot = str.indexOf(dot)
    if (str.indexOf(at) == -1) {
        alert("Invalid E-mail ID")
        return false
    }

    if (str.indexOf(at) == -1 || str.indexOf(at) == 0 || str.indexOf(at) == lstr) {
        alert("Invalid E-mail ID")
        return false
    }

    if (str.indexOf(dot) == -1 || str.indexOf(dot) == 0 || str.indexOf(dot) == lstr - 1) {
        alert("Invalid E-mail ID")
        return false
    }

    if (str.indexOf(at, (lat + 1)) != -1) {
        alert("Invalid E-mail ID")
        return false
    }

    if (str.substring(lat - 1, lat) == dot || str.substring(lat + 1, lat + 2) == dot) {
        alert("Invalid E-mail ID")
        return false
    }

    if (str.indexOf(dot, (lat + 2)) == -1) {
        alert("Invalid E-mail ID")
        return false
    }

    if (str.indexOf(" ") != -1) {
        alert("Invalid E-mail ID")
        return false
    }

    return true
}

// JavaScript Document

//****************************************************************************
// for CONTACT us page -- Free Sample part //
function jobContValidate(registration_frm)
{
    if (registration_frm.vName.value == "")
    {
        alert("Please enter Name");
       // document.getElementById("vName").style.border = '1px solid #f00';
        registration_frm.vName.focus();
        return false;
    }
    else if (registration_frm.vName.value != "") {
        //document.getElementById("vName").style.border = '1px solid #f2f2f2';
    }
	
    // email //
    if (registration_frm.vEmail.value == "")
    {
        alert("Please enter the Email");
       // document.getElementById("vEmail").style.border = '1px solid #f00';
        registration_frm.vEmail.focus();
        return false;
    }
    else if (registration_frm.vEmail.value != "") {
      //  document.getElementById("vEmail").style.border = '1px solid #f2f2f2';
    }
    if (echeck(registration_frm.vEmail.value) == false) {
        //email.value="";
		alert("Please enter the valid Email");
        registration_frm.vEmail.focus();
        return false;
    }
	
	// phone //
	
	if (registration_frm.vPhone.value == "")
    {
        alert("Please enter Mobile No.");
       // document.getElementById("vPhone").style.border = '1px solid #f00';
        registration_frm.vPhone.focus();
        return false;
	  
      
    }
	
	
	// Msg //
	if (registration_frm.vMsg.value == "")
    {
        alert("Please enter Message");
       // document.getElementById("vSubject").style.border = '1px solid #f00';
        registration_frm.vMsg.focus();
        return false;
    }
    
	
    
	var vName = $('#vName').val();
	var vEmail = $('#vEmail').val();
	var vPhone = $('#vPhone').val();
	var vMsg = $('#vMsg').val();
	
	$.ajax({
        url: "mailquery.php?mode=sendfrm",	
        type: "POST",       
		data: $("#contact-form").serialize(),
        cache: false,
        success: function (html) { 
			//alert(html);
            //var t = html.split('@@==@@');			
            if(html==1)
            { 
			 $('#vName').val('');
			 $('#vEmail').val('');
			 $('#vPhone').val('');
			 $('#vMsg').val('');
			 
			 			 
			 $('.send').hide();
             $('#msgshow').html('Thank you for submitting your information!');
            }else{
             $('#msgshow').html('Something wrong. Please try again.');
            }
        }		
    });	
	
	return false;
	
}


//****************************************************************************
// for CONTACT us page -- Request Demo part //
function empContValidate(registration_frm)
{
    if (registration_frm.eName.value == "")
    {
        alert("Please enter Name");
       // document.getElementById("vName").style.border = '1px solid #f00';
        registration_frm.eName.focus();
        return false;
    }
    else if (registration_frm.eName.value != "") {
        //document.getElementById("vName").style.border = '1px solid #f2f2f2';
    }
	
    // email //
    if (registration_frm.eEmail.value == "")
    {
        alert("Please enter the Email");
       // document.getElementById("vEmail").style.border = '1px solid #f00';
        registration_frm.eEmail.focus();
        return false;
    }
    else if (registration_frm.eEmail.value != "") {
      //  document.getElementById("vEmail").style.border = '1px solid #f2f2f2';
    }
    
	if (echeck(registration_frm.eEmail.value) == false) {
        //email.value="";
		alert("Please enter the valid Email");
        registration_frm.eEmail.focus();
        return false;
    }
	
	// phone //
	
	if (registration_frm.ePhone.value == "")
    {
        alert("Please enter Mobile No.");
       // document.getElementById("vPhone").style.border = '1px solid #f00';
        registration_frm.ePhone.focus();
        return false;
	  
      
    }
	
	// Designation //
	if (registration_frm.eDesigna.value == "")
    {
        alert("Please enter Designation");
       // document.getElementById("vSubject").style.border = '1px solid #f00';
        registration_frm.eDesigna.focus();
        return false;
    }
	
	
	// Company Name //
	if (registration_frm.eComname.value == "")
    {
        alert("Please enter Company Name");
       // document.getElementById("vSubject").style.border = '1px solid #f00';
        registration_frm.eComname.focus();
        return false;
    }
	
	
	
	// Msg //
	if (registration_frm.vMsge.value == "")
    {
        alert("Please enter Message");
       // document.getElementById("vSubject").style.border = '1px solid #f00';
        registration_frm.vMsge.focus();
        return false;
    }
    
	
    
	var eName = $('#eName').val();
	var eEmail = $('#eEmail').val();
	var ePhone = $('#ePhone').val();
	var eDesigna = $('#eDesigna').val();
	var eComname = $('#eComname').val();
	var vMsge = $('#vMsge').val();
	
	$.ajax({
        url: "mailquery.php?mode=sendfrmreq",	
        type: "POST",       
		data: $("#contact-formRequest").serialize(),
        cache: false,
        success: function (html) {			
            if(html == 1)
            { 
			 $('#eName').val('');
			 $('#eEmail').val('');
			 $('#ePhone').val('');
			 $('#eDesigna').val('');
			 $('#eComname').val('');
			 $('#vMsge').val('');
			 
			 			 
			 $('.send').hide();
             $('#msgshow').html('Thank you for submitting your information!');
            }else{
             $('#msgshow').html('Something wrong. Please try again.');
            }
        }		
    });	
	
	return false;
}

//****************************************************************************
// for enquery form page --  //
function hdfcContValidate(registration_frm)
{
    if (registration_frm.name.value == "")
    {
        alert("Please enter Name");
       // document.getElementById("vName").style.border = '1px solid #f00';
        registration_frm.name.focus();
        return false;
    }
    else if (registration_frm.name.value != "") {
        //document.getElementById("vName").style.border = '1px solid #f2f2f2';
    }
    
    // email //
    if (registration_frm.email.value == "")
    {
        alert("Please enter the Email");
       // document.getElementById("vEmail").style.border = '1px solid #f00';
        registration_frm.email.focus();
        return false;
    }
    else if (registration_frm.email.value != "") {
      //  document.getElementById("vEmail").style.border = '1px solid #f2f2f2';
    }
    if (echeck(registration_frm.email.value) == false) {
        //email.value="";
        alert("Please enter the valid Email");
        registration_frm.email.focus();
        return false;
    }
    
    // phone //
    
    if (registration_frm.Contact.value == "")
    {
        alert("Please enter Mobile No.");
       // document.getElementById("vPhone").style.border = '1px solid #f00';
        registration_frm.Contact.focus();
        return false;
      
      
    }
    

    $.ajax({
        url: "mailquery.php?mode=sendEnquire",  
        type: "POST",       
        data: $("#enquire-formRequest").serialize(),
        cache: false,
        success: function (html) { 
            //alert(html);
            //var t = html.split('@@==@@');         
            if(html==1)
            { 
             $('#name').val('');
             $('#email').val('');
             $('#Contact').val('');
             
                         
             alert("Thank you for submitting your enquire. Respective person will contact you soon!");
             window.location.href = "https://firstjob.co.in/hdfc.php";
            }else{
             $('#msgshow').html('Something wrong. Please try again.');
            }
        }       
    }); 
    
    return false;
    
}



(function($) {
    var winwidth = $(window).width();
    var App = {
 
    /**
    * Init Function
    */
    init: function() {
        App.HomeOpacity();
        App.Preloader();
		//App.PreloaderHome();
        App.Animations();

    },

 
    HomeOpacity: function() {
        var h = window.innerHeight;
        $(window).on('scroll', function() {
            var st = $(this).scrollTop();
            $('#home').css('opacity', (1) );
//            $('#home').css('opacity', (1-st/h) );
        });
    },

 
    /**
    * Preloader
    */
    Preloader: function() {
        $(window).load(function() {
            $('#status').delay(100).fadeOut('slow');
            $('#preloader').delay(100).fadeOut('slow');
            $('body').delay(100).css({'overflow':'visible'});
            setTimeout(function(){$('#banner').addClass('animated fadeInUp')},1000);
			
		} )
    },
	

    /**
    * Animations
    */
 Animations: function() {
               
			$('.inrwhatisfst').waypoint(function(){
				setTimeout(function(){$('.inrwhatisfst').addClass('animated fadeInDown')},500);
			 },{ offset: '80%' });
			 
			$('#whois01').waypoint(function() {
				setTimeout(function(){$('#whois01').addClass('animated fadeInLeft')},500);
			},{ offset: '80%' });
			$('#whois02').waypoint(function(){
				setTimeout(function(){$('#whois02').addClass('animated fadeInDown')},1000);
			},{ offset: '80%' });
			$('#whois03').waypoint(function(){
				setTimeout(function(){$('#whois03').addClass('animated fadeInRight')},1500);
			},{ offset: '80%' });
			$('#howto01').waypoint(function(){
				setTimeout(function(){$('#howto01').addClass('animated fadeInDown')},400);
			},{ offset: '80%' });
			$('#howto02').waypoint(function(){
				setTimeout(function(){$('#howto02').addClass('animated fadeInLeft')},700);
			},{ offset: '80%' });
			$('#howto03').waypoint(function(){
				setTimeout(function(){$('#howto03').addClass('animated fadeInRight')},1000);
			},{ offset: '80%' });
			$('#howto04').waypoint(function(){
				setTimeout(function(){$('#howto04').addClass('animated fadeInLeft')},1300);
			},{ offset: '80%' });
			$('#howto05').waypoint(function(){
				setTimeout(function(){$('#howto05').addClass('animated fadeInRight')},1600);	
			},{ offset: '80%' });
			$('#howto06').waypoint(function(){
				setTimeout(function(){$('#howto06').addClass('animated fadeInUp')},1900);
			},{ offset: '80%' });
			$('.forstop01').waypoint(function(){
				setTimeout(function(){$('.forstop01').addClass('animated fadeInDown')},500);
			},{ offset: '80%' });
			$('#infopart').waypoint(function(){
				setTimeout(function(){$('#infopart').addClass('animated fadeInDown')},500);
			},{ offset: '80%' });
			$('#cont_form').waypoint(function(){
				setTimeout(function(){$('#cont_form').addClass('animated fadeInUp')},500);
			},{ offset: '80%' });
			   
        //$('#partofwhtfstjob').waypoint(function() {
//			setTimeout(function(){$('#partofwhtfstjob').addClass('animated fadeInUp')},500);
//         },{ offset: '90%' });
//		 
//		  $('.CurrTwo').waypoint(function() {;
//			setTimeout(function(){$('.CurrTwo').addClass('animated fadeInRight')},500);
//         },{ offset: '90%' });
		 
		 
    },
}
$(function() {
  App.init();
  });
	
})(jQuery);
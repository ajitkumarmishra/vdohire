<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!--<title>:: Get Your First Job Today ::</title>-->
<title>New Age Hiring Engine – Hire Through App-based Video Interview</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="author" content="" />
<meta name="description" content="Hiring people is now #Easier, #Faster & #Cheaper.  Reach more people, curtail cost of hiring, reduce lead time in hiring & multiply recruiter capability. Request for demo now!" />
<meta name="keywords" content="recruitment firm, recruitment agency, best recruiting firms" />
<meta name="Resource-type" content="" />
<link href="css/reset.css" rel="stylesheet" type="text/css" />

<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
<link rel="icon" href="images/favicon.ico" type="image/x-icon">

<link rel="stylesheet" type="text/css" href="css/jquery.fullPage.css" />
<link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" />
<link href="css/animate.css" rel="stylesheet" type="text/css" />
<link href="css/jquery.bxslider.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/jobseeker_home.css" rel="stylesheet" type="text/css" />
<link href="css/responsive_media.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/css.css"/>
<link rel="stylesheet" href="css/font-awesome.css"/>
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-104414051-1', 'auto');
  ga('send', 'pageview');

</script>


</head>
<body>
<div id="preloader">
  <div id="status">
    <div class="spinner">
      <div class="bounce1"><img src="images/bx_loader.gif" alt="" /></div>
    </div>
  </div>
</div>

<div id="fullpageemployer">
    <nav>
        <div class="topnav">
            <div class="inrnavi">
                <div class="navi" style="margin-top: -35px;">
                    <ul style="margin-top: -7px;">
                        <li style="padding-left: 0px; padding-right: 15px;"><a class="selected" href="https://vdohire.com">EMPLOYER</a></li>
                        <li>|</li>
                        <li style="padding-left: 15px; padding-right: 15px;"><a href="jobseekers" style="color: #383838">JOBSEEKER</a></li>
                        <!--<li>|</li>
                        <li style="padding-left: 15px;"><a href="https://vdohire.com/blog/" style="color: #383838">BLOG</a></li>-->

                        <li><a class="btn-lg" id="loginForEmployer" style="background-color: #383838; color: #fff; padding: 5px 10px; font-size: 15px;" href="https://vdohire.com/admin">Login for Employer</a></li>
                        <li id="loginButton"><a class="login-button btn-lg" id="loginForJobseeker" style="background-color: #383838; color: #fff; padding: 5px 10px; font-size: 15px;" href="javascript:">Login for Jobseeker</a></li>
                    </ul>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </nav>
    <div class="clear"></div>
    
	<!---part1-->
	<div class="section " id="section0">
                <header>
                    <div class="bann" id="banner">
                        <div class="ban_content">
                            <div class="mainlogo" style="min-height: 110px;"><a href="http://vdohire.com"><img src="images/logo.png" style="width: 341px;"></a></div>
                            <div class="clear"></div>
                            <p style="color: black;">EASIER. FASTER. CHEAPER</p>
                            <div class="clear"></div>
                            <a href="#request-a-demo" class="btnfirst" style="text-decoration: none;"><span>REQUEST A DEMO</span></a>
                            <div class="clear"></div>
                        </div>
                        <div class="flexslider">
                          <ul class="slides">
                            <li class=""><img id="bannerImg" src="https://www.vdohire.com/images/employer/employ-banner/1-new.jpg" ></li>
							
                            <!--<li class="bannbg06"></li>
                            <li class="bannbg07"></li>
                            <li class="bannbg08"></li>-->
                          </ul>
                        </div>
                    <div class="clear"></div>
                    </div>
                <div class="clear"></div>
                </header>
            <div class="clear"></div>  
	</div>
    
    <!---------part 2------------------>
	<div class="section employ_second_bg" id="section1">	
              <section>
                 <div class="benefit_isfstjob">
                   <div class="inr_benefstjob">
                    <h2>Benefits of using VDOHire</h2>
                    <div class="clear"></div>
                    <ul>
                    	<li id="beneis01">
                        	<img src="images/employer/employsec/bene01.png">
                            <p>Reach out to more people</p>
                        </li>
                        <li id="beneis02">
                        	<img src="images/employer/employsec/bene02.png">
                            <p> Reduce lead time in hiring</p>
                        </li>
                        <li id="beneis03">
                        	<img src="images/employer/employsec/bene03.png">
                            <p> Curtail cost of hiring</p>
                        </li>
                        
                        <li id="beneis04">
                        	<img src="images/employer/employsec/bene04.png">
                            <p>Multiply recruiter capability<!--Proliferate recruiter capability--></p>
                        </li>
                        
                        <li id="beneis05">
                        	<img src="images/employer/employsec/bene05.png">
                            <p>Reduce subjectivity in hiring decisions</p>
                        </li>
                        
                        <li id="beneis06">
                        	<img src="images/employer/employsec/bene06.png">
                            <p>Source from any channel<br> Process through VDOHire</p>
                        </li>
                    </ul>
                    <div class="clear"></div>
                </div>
            <div class="clear"></div>	
            </div>
                <div class="clear"></div>
                </section>
       		<div class="clear"></div>
	</div>
    
    <!---------part 3------------------>
	<div class="section" id="section2">
		   <section>
              <div class="VedioInterview">
                 <div class="VedioIMG">
                   <img src="images/employer/employvedio/left-bg.jpg" width="100%"/>
                 </div>
                 <div class="InterViewText">
                   <div class="innertext">
                    <h3>Video Interview :</h3> 
                    <span>the new-age hiring engine</span>
                    <div class="clear"></div>
                    <div class="border_bott"></div>
                   
                   <div class="clear"></div>
                    <ul class="bxslider">
                      <li>
                        <div class="InnertextSlide">
                          <h4>No Scheduling Conflicts :</h4>
                          <p> No scheduling hassles and delays. Lets you scrutinise candidates through videos, waiting for you in a queue. Sanctity of interview retained as responses are spontaneous and not practiced. </p>
                        </div>
                      </li>
                      <li>
                        <div class="InnertextSlide">
                          <h4>Customized Interview Questions :</h4>
                          <p>Choose from available sets of interview questions available by industry and by function. Or create your own. In Text, Audio or Video format.</p>
                        </div>
                      </li>
                       <li>
                        <div class="InnertextSlide">
                          <h4>Maintain A Digital Record :</h4>
                          <p> Not sure what goes on inside an interview room and why someone is rejected? Now you will have a digital record of the interview as well as the evaluation. </p>
                        </div>
                      </li>
                      
                       <li>
                        <div class="InnertextSlide">
                          <h4>Positive Experience for Jobseekers as well :</h4>
                          <p> No longer does the jobseeker have to come back again and again. A positive experience for the jobseeker might lead to greater hiring success, and more satisfied new entrants.</p>
                        </div>
                      </li>
                    </ul>
                                        
                   </div>
                 </div>
              </div>
           </section>
       <div class="clear"></div>
	</div>
    
    <!---------part 4------------------>
    <div class="section whats_more" id="section3">
		<section>
            <div class="whats_gride">
               <div class="whats_inner_grid">
                 <h2>Explore more features </h2>
                 <p>Added advantages of using VDOHire</p>
                 
                 <ul>
                   <li id="Whats01">
                     <div class="Licontent">
                        <img src="images/employer/employwhats/whats01.png"/>
                        <h5>Assessments</h5>
                        <p>Ready-to-use assessments for additional evaluation; integration on demand with external solutions</p>
                     </div>
                   </li>
                   
                   <li id="Whats02">
                     <div class="Licontent">
                        <img src="images/employer/employwhats/whats02.png"/>
                        <h5>Audition</h5>
                        <p>Analyze a candidate’s audition, apart from basic information, before inviting him/her for automated interview</p>
                     </div>
                   </li>
                   
                   
                   <li id="Whats03">
                     <div class="Licontent">
                        <img src="images/employer/employwhats/whats03.png"/>
                        <h5>Employer Branding</h5>
                        <p>Multiple, customisable options of enabling your organisation to stand out</p>
                     </div>
                   </li>
                 </ul>
               </div> 
            </div>
        </section>
     <div class="clear"></div>
	</div>
    
    <!---------part 5------------------>
    <div class="section Clients_bg" id="section4">
		<section>
            <div class="clients_grid">
              <div class="clients_inner_grid">
                 <h2>How Analytics of VDOHire Can Help Clients?</h2>
                 <p>Track recruiting activities and derive meaningful insights</p>
              </div>
             </div>
             <div class="clients_grid_lrftrightpart">
              <div class="clients_Left_text">
                   <div class="Left_content">
                     <ul>
                       <li>Monitor evaluator's productivity</li>
                       <li>Track job post status</li>
                       <li>Understand Behavior of job-seekers</li>
                       <li>Get objective data based on digital imprints</li>
                     </ul>
                   </div>
              </div>
              
              <div class="clients_Right_IMG">
                 <img src="images/employer/employclients/clients-right-img.png"/>
              </div>
            <div class="clear"></div>
            </div>
        </section>
        <div class="clear"></div>
	</div>
    
    <!---------part 6------------------>
    <div class="section Media_bg" id="section5">
        <section>
            <div class="Media_grid">
                <div class="Media_Inner_grid">
                  <h4>In Media</h4>
                    <ul class="bxsliderMedia">
                        <!--//////////Page 1/////////-->
                        <li>
                           <div class="InnerDivMedia">
                             <a href="http://economictimes.indiatimes.com/jobs/labour-ministry-ties-up-with-top-job-portals-for-national-career-service/articleshow/53835502.cms" target="_blank">
                               <img src="images/media/1.png"/>
                             </a>
                          </div>
                          
                           <div class="InnerDivMedia">
                             <a href="http://www.business-standard.com/article/pti-stories/govt-inks-mous-with-firms-to-enhance-quality-of-job-services-116082301127_1.html" target="_blank">
                               <img src="images/media/2.png"/>
                             </a>
                          </div>
                          
                           <div class="InnerDivMedia">
                             <a href="http://www.moneycontrol.com/news/business/vertical-networks-catching-up-give-generic-onestough-fight_7558541.html" target="_blank">
                               <img src="images/media/3.png"/>
                             </a>
                          </div>
                          
                           <div class="InnerDivMedia">
                             <a href="http://www.financialexpress.com/industry/tech/vertical-networks-catching-up-give-generic-ones-a-tough-fight/401429/" target="_blank">
                               <img src="images/media/4.png"/>
                             </a>
                          </div>
                          
                           <div class="InnerDivMedia">
                             <a href="http://timesofindia.indiatimes.com/city/gurgaon/An-app-that-helps-jobseekers-find-jobs/articleshow/53902280.cms" target="_blank">
                              <img src="images/media/5.png"/>
                             </a>
                          </div>
                          
                           <div class="InnerDivMedia">
                             <a href="http://news24online.com/vertical-networks-catching-up-give-generic-ones-a-tough-fight-22/" target="_blank">
                              <img src="images/media/6.png"/>
                             </a>
                          </div>
                          
                           <div class="InnerDivMedia">
                             <a href="http://indiatoday.intoday.in/story/vertical-networks-catching-up-give-generic-ones-a-tough-fight/1/778244.html" target="_blank">
                               <img src="images/media/7.png"/>
                             </a>
                          </div>
                          
                           <div class="InnerDivMedia">
                             <a href="http://www.asianage.com/business/vertical-sites-provide-competition-077" target="_blank">
                               <img src="images/media/8.png"/>
                             </a>
                          </div>
                        </li>
                        <!---//////////Page 1/////////--> 
                        
                        <!---//////////Page 2/////////-->
                        <li>
                            <div class="InnerDivMedia">
                             <a href="http://www.newsr.in/n/Technology/759s60uga/Labour-Ministry-facilitates-employment-opportunities.htm" target="_blank">
                              <img src="images/media/9.png"/>
                             </a>
                           </div>
                           
                           <div class="InnerDivMedia">
                             <a href="http://newsnow.in/news/vertical-networks-catching-up-give-generic-ones-a-tough-fight" target="_blank">
                              <img src="images/media/10.png"/>
                             </a>
                           </div>
                           
                           <div class="InnerDivMedia">
                             <a href="http://www2.staffingindustry.com/row/Editorial/Daily-News/India-Labour-Ministry-signs-Memoranda-of-Understanding-with-job-firms-to-enhance-service-quality-39073" target="_blank">
                              <img src="images/media/11.png"/>
                             </a>
                           </div>
                           
                           <div class="InnerDivMedia">
                             <a href="http://www.techgig.com/tech-news/editors-pick/Labour-ministry-ties-up-with-top-job-portals-for-national-career-service-62300" target="_blank">
                              <img src="images/media/12.png"/>
                             </a>
                           </div>
                           
                           <div class="InnerDivMedia">
                             <a href="http://www.gadgetsnow.com/jobs/Labour-ministry-ties-up-with-companies-to-provide-career-service/articleshow/53841092.cms" target="_blank">
                              <img src="images/media/13.png"/>
                             </a>
                           </div>
                           
                           <div class="InnerDivMedia">
                             <a href="http://twocircles.net/2016aug26/1472236134.html#.WA8IRo997IW" target="_blank">
                              <img src="images/media/14.png"/>
                             </a>
                           </div>
                           
                           <div class="InnerDivMedia">
                             <a href="http://in.shafaqna.com/EN/03005513" target="_blank">
                              <img src="images/media/15.png"/>
                             </a>
                           </div>
                           
                           <div class="InnerDivMedia">
                             <a href="http://huntnews.in/p/index?uc_param_str=dnfrpfbivesscpgimibtbmntnijblauputoggdnw" target="_blank">
                              <img src="images/media/16.png"/>
                             </a>
                           </div>
                        </li>
                        <!--//////////Page 2/////////-->
                        <!--//////////Page 3/////////-->
                        <!--<li>
                        <div class="InnerDivMedia">
                         <a href="http://www.daijiworld.com/news/news_disp.asp?n_id=411185" target="_blank">
                          <img src="images/media/17.png"/>
                         </a>
                        </div>
                       
                      
                       <div class="InnerDivMedia">
                         <a href="http://www.daijiworld.com/news/news_disp.asp?n_id=411185" target="_blank">
                          <img src="images/media/18.png"/>
                         </a>
                       </div>
                       
                       <div class="InnerDivMedia">
                         <a href="http://english.mathrubhumi.com/education/news/news-updates/labour-ministry-facilitates-job-opportunities-english-news-1.1311233" target="_blank">
                          <img src="images/media/19.png"/>
                         </a>
                       </div>
                       
                       <div class="InnerDivMedia">
                         <a href="http://www.maeeshat.in/2016/08/labour-ministry-facilitates-employment-opportunities/" target="_blank">
                          <img src="images/media/20.png"/>
                         </a>
                       </div>
                       
                        <div class="InnerDivMedia">
                         <a href="http://www.newkerala.com/news/2016/fullnews-110402.html" target="_blank">
                          <img src="images/media/21.png"/>
                         </a>
                       </div>
                        </li>-->
                        <!---//////////Page 3/////////-->
                    </ul>
                </div>
            </div>
          
            <!--/////////////////////////////////////////////////////////Mobile Media Slider////////////////////////////////////////////////////////////-->
            <div class="Media_gridMOB">
                <div class="Media_Inner_gridMOB">
                  <h4>In Media</h4>
                  <ul class="bxsliderMediaMOBILE">

                     <li>
                       <div class="InnerDivMedia">
                         <a href="http://economictimes.indiatimes.com/jobs/labour-ministry-ties-up-with-top-job-portals-for-national-career-service/articleshow/53835502.cms" target="_blank">
                           <img src="images/media/1.png"/>
                         </a>
                      </div>
                      
                       <div class="InnerDivMedia">
                         <a href="http://www.business-standard.com/article/pti-stories/govt-inks-mous-with-firms-to-enhance-quality-of-job-services-116082301127_1.html" target="_blank">
                           <img src="images/media/2.png"/>
                         </a>
                      </div>
                      
                       <div class="InnerDivMedia">
                         <a href="http://www.moneycontrol.com/news/business/vertical-networks-catching-up-give-generic-onestough-fight_7558541.html" target="_blank">
                           <img src="images/media/3.png"/>
                         </a>
                      </div>
                      
                       <div class="InnerDivMedia">
                         <a href="http://www.financialexpress.com/industry/tech/vertical-networks-catching-up-give-generic-ones-a-tough-fight/401429/" target="_blank">
                           <img src="images/media/4.png"/>
                         </a>
                      </div>
                      
                       <div class="InnerDivMedia">
                         <a href="http://epaperbeta.timesofindia.com/" target="_blank">
                          <img src="images/media/5.png"/>
                         </a>
                      </div>
                      
                       <div class="InnerDivMedia">
                         <a href="http://news24online.com/vertical-networks-catching-up-give-generic-ones-a-tough-fight-22/" target="_blank">
                          <img src="images/media/6.png"/>
                         </a>
                      </div>
                      
                       <div class="InnerDivMedia">
                         <a href="http://indiatoday.intoday.in/story/vertical-networks-catching-up-give-generic-ones-a-tough-fight/1/778244.html" target="_blank">
                           <img src="images/media/7.png"/>
                         </a>
                      </div>
                      
                       <div class="InnerDivMedia">
                         <a href="http://www.asianage.com/business/vertical-sites-provide-competition-077" target="_blank">
                           <img src="images/media/8.png"/>
                         </a>
                      </div>
                    </li>
                    
                     <li>
                        <div class="InnerDivMedia">
                         <a href="http://www.newsr.in/n/Technology/759s60uga/Labour-Ministry-facilitates-employment-opportunities.htm" target="_blank">
                          <img src="images/media/9.png"/>
                         </a>
                       </div>
                       
                       <div class="InnerDivMedia">
                         <a href="http://newsnow.in/news/vertical-networks-catching-up-give-generic-ones-a-tough-fight" target="_blank">
                          <img src="images/media/10.png"/>
                         </a>
                       </div>
                       
                       <div class="InnerDivMedia">
                         <a href="http://www2.staffingindustry.com/row/Editorial/Daily-News/India-Labour-Ministry-signs-Memoranda-of-Understanding-with-job-firms-to-enhance-service-quality-39073" target="_blank">
                          <img src="images/media/11.png"/>
                         </a>
                       </div>
                       
                       <div class="InnerDivMedia">
                         <a href="http://www.techgig.com/tech-news/editors-pick/Labour-ministry-ties-up-with-top-job-portals-for-national-career-service-62300" target="_blank">
                          <img src="images/media/12.png"/>
                         </a>
                       </div>
                       
                       <div class="InnerDivMedia">
                         <a href="http://www.gadgetsnow.com/jobs/Labour-ministry-ties-up-with-companies-to-provide-career-service/articleshow/53841092.cms" target="_blank">
                          <img src="images/media/13.png"/>
                         </a>
                       </div>
                       
                       <div class="InnerDivMedia">
                         <a href="http://twocircles.net/2016aug26/1472236134.html#.WA8IRo997IW" target="_blank">
                          <img src="images/media/14.png"/>
                         </a>
                       </div>
                       
                       <div class="InnerDivMedia">
                         <a href="http://in.shafaqna.com/EN/03005513" target="_blank">
                          <img src="images/media/15.png"/>
                         </a>
                       </div>
                       
                       <div class="InnerDivMedia">
                         <a href="http://huntnews.in/p/index?uc_param_str=dnfrpfbivesscpgimibtbmntnijblauputoggdnw" target="_blank">
                          <img src="images/media/16.png"/>
                         </a>
                       </div>
                    </li>
                  </ul>
                </div>
                <!--/////////////////////////////////////////////////////////Mobile Media Slider////////////////////////////////////////////////////////////--> 
            </div>
        </section>
        <div class="clear"></div>
    </div>

    <!---------part 6------------------>
    <div class="section Media_bg" id="section6">
        <section>
            <div class="Media_grid">
                <div class="Media_Inner_grid_client">
                    <div class="Media_Inner_grid">
                      <h4>Our Clients</h4>
                        <ul class="bxsliderMedia">
                            <!--//////////Page 1/////////-->
                            <li>
                               <div class="InnerDivMedia">
                                 <img src="images/cams-online-logo.png"/>
                              </div>
                              
                               <div class="InnerDivMedia">
                                 <img src="images/centum-logo.png"/>
                              </div>
                              
                               <div class="InnerDivMedia">
                                 <img src="images/future-generali-logo.png"/>
                              </div>
                              
                               <div class="InnerDivMedia">
                                 <img src="images/greenpeace-logo.png"/>
                                 </a>
                              </div>
                              
                               <div class="InnerDivMedia">
                                 <img src="images/hdfc-life-logo.png"/>
                              </div>

                              <div class="InnerDivMedia">
                                 <img src="images/vistara.png"/>
                                 </a>
                              </div>

                              <div class="InnerDivMedia">
                                 <img src="images/startbeans-logo.png"/>
                               </div>

                               <div class="InnerDivMedia">
                                 <img src="images/kwench-logo.png"/>
                               </div>
                              
                            </li>
                            <!---//////////Page 1/////////--> 
                            
                            <!---//////////Page 2/////////-->
                            <li>
                               <div class="InnerDivMedia">
                                 <img src="images/shopers-stop-logo.png"/>
                               </div>
                               
                               <div class="InnerDivMedia">
                                 <img src="images/indiafirst-logo.png"/>
                               </div>
                               
                               
                            </li>
                            <!--//////////Page 2/////////-->
                            <!--//////////Page 3/////////-->
                            <!--<li>
                            <div class="InnerDivMedia">
                             <a href="http://www.daijiworld.com/news/news_disp.asp?n_id=411185" target="_blank">
                              <img src="images/media/17.png"/>
                             </a>
                            </div>
                           
                          
                           <div class="InnerDivMedia">
                             <a href="http://www.daijiworld.com/news/news_disp.asp?n_id=411185" target="_blank">
                              <img src="images/media/18.png"/>
                             </a>
                           </div>
                           
                           <div class="InnerDivMedia">
                             <a href="http://english.mathrubhumi.com/education/news/news-updates/labour-ministry-facilitates-job-opportunities-english-news-1.1311233" target="_blank">
                              <img src="images/media/19.png"/>
                             </a>
                           </div>
                           
                           <div class="InnerDivMedia">
                             <a href="http://www.maeeshat.in/2016/08/labour-ministry-facilitates-employment-opportunities/" target="_blank">
                              <img src="images/media/20.png"/>
                             </a>
                           </div>
                           
                            <div class="InnerDivMedia">
                             <a href="http://www.newkerala.com/news/2016/fullnews-110402.html" target="_blank">
                              <img src="images/media/21.png"/>
                             </a>
                           </div>
                            </li>-->
                            <!---//////////Page 3/////////-->
                        </ul>
                    </div>
                </div>
            </div>
          
            <!--/////////////////////////////////////////////////////////Mobile Media Slider////////////////////////////////////////////////////////////-->
            <div class="Media_gridMOB">
                <div class="Media_Inner_gridMOB">
                  <h4>In Media</h4>
                  <ul class="bxsliderMediaMOBILE">

                     <li>
                       <div class="InnerDivMedia">
                         <a href="http://economictimes.indiatimes.com/jobs/labour-ministry-ties-up-with-top-job-portals-for-national-career-service/articleshow/53835502.cms" target="_blank">
                           <img src="images/media/1.png"/>
                         </a>
                      </div>
                      
                       <div class="InnerDivMedia">
                         <a href="http://www.business-standard.com/article/pti-stories/govt-inks-mous-with-firms-to-enhance-quality-of-job-services-116082301127_1.html" target="_blank">
                           <img src="images/media/2.png"/>
                         </a>
                      </div>
                      
                       <div class="InnerDivMedia">
                         <a href="http://www.moneycontrol.com/news/business/vertical-networks-catching-up-give-generic-onestough-fight_7558541.html" target="_blank">
                           <img src="images/media/3.png"/>
                         </a>
                      </div>
                      
                       <div class="InnerDivMedia">
                         <a href="http://www.financialexpress.com/industry/tech/vertical-networks-catching-up-give-generic-ones-a-tough-fight/401429/" target="_blank">
                           <img src="images/media/4.png"/>
                         </a>
                      </div>
                      
                       <div class="InnerDivMedia">
                         <a href="http://epaperbeta.timesofindia.com/" target="_blank">
                          <img src="images/media/5.png"/>
                         </a>
                      </div>
                      
                       <div class="InnerDivMedia">
                         <a href="http://news24online.com/vertical-networks-catching-up-give-generic-ones-a-tough-fight-22/" target="_blank">
                          <img src="images/media/6.png"/>
                         </a>
                      </div>
                      
                       <div class="InnerDivMedia">
                         <a href="http://indiatoday.intoday.in/story/vertical-networks-catching-up-give-generic-ones-a-tough-fight/1/778244.html" target="_blank">
                           <img src="images/media/7.png"/>
                         </a>
                      </div>
                      
                       <div class="InnerDivMedia">
                         <a href="http://www.asianage.com/business/vertical-sites-provide-competition-077" target="_blank">
                           <img src="images/media/8.png"/>
                         </a>
                      </div>
                    </li>
                    
                     <li>
                        <div class="InnerDivMedia">
                         <a href="http://www.newsr.in/n/Technology/759s60uga/Labour-Ministry-facilitates-employment-opportunities.htm" target="_blank">
                          <img src="images/media/9.png"/>
                         </a>
                       </div>
                       
                       <div class="InnerDivMedia">
                         <a href="http://newsnow.in/news/vertical-networks-catching-up-give-generic-ones-a-tough-fight" target="_blank">
                          <img src="images/media/10.png"/>
                         </a>
                       </div>
                       
                       <div class="InnerDivMedia">
                         <a href="http://www2.staffingindustry.com/row/Editorial/Daily-News/India-Labour-Ministry-signs-Memoranda-of-Understanding-with-job-firms-to-enhance-service-quality-39073" target="_blank">
                          <img src="images/media/11.png"/>
                         </a>
                       </div>
                       
                       <div class="InnerDivMedia">
                         <a href="http://www.techgig.com/tech-news/editors-pick/Labour-ministry-ties-up-with-top-job-portals-for-national-career-service-62300" target="_blank">
                          <img src="images/media/12.png"/>
                         </a>
                       </div>
                       
                       <div class="InnerDivMedia">
                         <a href="http://www.gadgetsnow.com/jobs/Labour-ministry-ties-up-with-companies-to-provide-career-service/articleshow/53841092.cms" target="_blank">
                          <img src="images/media/13.png"/>
                         </a>
                       </div>
                       
                       <div class="InnerDivMedia">
                         <a href="http://twocircles.net/2016aug26/1472236134.html#.WA8IRo997IW" target="_blank">
                          <img src="images/media/14.png"/>
                         </a>
                       </div>
                       
                       <div class="InnerDivMedia">
                         <a href="http://in.shafaqna.com/EN/03005513" target="_blank">
                          <img src="images/media/15.png"/>
                         </a>
                       </div>
                       
                       <div class="InnerDivMedia">
                         <a href="http://huntnews.in/p/index?uc_param_str=dnfrpfbivesscpgimibtbmntnijblauputoggdnw" target="_blank">
                          <img src="images/media/16.png"/>
                         </a>
                       </div>
                    </li>
                  </ul>
                </div>
                <!--/////////////////////////////////////////////////////////Mobile Media Slider////////////////////////////////////////////////////////////--> 
            </div>
        </section>
        <div class="clear"></div>
    </div>

    <!---------part 7------------------>
    <div class="section Request_bg" id="section7">
		<section>
        	<div class="informantion_cent">                
                <div class="clear"></div>
                <div class="cont_form" id="cont_form" style="margin-top:-30px;">
                    <h2 style="color:#faa425;">REQUEST A DEMO</h2>
                    <form name="myform" id="contact-formRequest" method="post" >
                        <p><input type="text" placeholder="NAME" id="eName" name="eName"></p>
                        <p><input type="email" placeholder="EMAIL*" id="eEmail" name="eEmail" required></p>
                        <p><input type="text" placeholder="COMPANY NAME" id="eComname" name="eComname"></p>
                        <p><input type="text" maxlength="10" placeholder="CONTACT NUMBER" id="ePhone" name="ePhone"></p>
                        <p><textarea id="vMsge" name="vMsge" placeholder="MESSAGE" style="height:90px;"></textarea></p>
                        <div class="clear"></div>
                        <p style="background:none;"><span id="msgshow"></span></p>
                        <p class="send"><button type="button" id="submit" value="submit" onclick="empContValidate()">SUBMIT</button></p>
                    </form>
                </div>
            <div class="clear"></div>
            </div>
        </section>  

    	<!--<footer>
            <div class="comfoot">
                <div class="inrfoot">
                    <div class="socilicon">
                        <p>CONNECT</p>
                        <ul>
                            <li><a href=""><img src="images/foot_fbicon.png"></a></li>
                            <li><a href=""><img src="images/foot_lindicon.png"></a></li>
                            <li><a href=""><img src="images/foot_twiticon.png"></a></li>
                        </ul>
                    </div>
                <div class="clear"></div>    
                </div>
            <div class="clear"></div>
            </div>
            <div class="clear"></div>
        </footer>-->
        <div class="clear"></div>
	</div>
</div>

<!-- The login modal. Don't display it initially -->
<form id="loginForm" method="post" class="form-horizontal" style="display: none;">
    <div class="container">
        <div class="col-lg-12 col-md-12 col-xs-12 ">
    <div class="form-group">
        <div class="col-lg-12 col-md-12 col-xs-12 col-xs-offset-3">
            <h4 style="color: #06577a;font-weight: bold;">Log in</h4>
            
        </div>
    </div>
    <div class="form-group">
        <?php //print_r($_COOKIE);?>
        <div class="col-xs-5 col-xs-offset-3">
            <input type="text" class="form-control" value="<?php echo isset($_COOKIE['fj_email'])?$_COOKIE['fj_email']:""; ?>" name="email" id="email" placeholder="Email Id *" required />
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-5 col-xs-offset-3">
            <input type="password" class="form-control" value="<?php echo isset($_COOKIE['fj_password'])?$_COOKIE['fj_password']:""; ?>" name="password" id="password" placeholder="Password *"/>
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-5 col-xs-offset-3">
            <div class="checkbox">
                <label><input type="checkbox" value="rememberMe" id="rememberMe" name="rememberMe" <?php echo isset($_COOKIE['fj_email'])? 'checked="checked"':""; ?>>Remember Me</label>
            </div>
            <a style="margin-top: -20px; font-weight: 400 !important; color: #999 !important; font-size: 14px !important;" data-target="#forgotpasswordModal" data-toggle="modal" id="forgot_password" href="" class="pull-right">Forgot your password?</a>
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-5 col-xs-offset-3">
            <button type="submit" class="btn btn-default pull-left" id="jobseekerLogin"><i class="fa fa-lock fa-3" aria-hidden="true"></i>Log in</button>
            <div class="pull-right" style="padding-top: 9px;">
                Don't have an account? <a class="signup-button" id="jobseekerRegister" href="javascript:">Register</a>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-5 col-xs-offset-3 text-center social-link">
            <span class="social-title">Sign In with</span>
            <div class="social-buttons">
                <button id="facebook-icon"  onclick="location.href = 'https://vdohire.com/fb/fbconfig.php';"  type="button" class="button-separation"></button>
                <button id="linkedin-icon"   onclick="location.href = 'https://vdohire.com/LinkedInSocialLogin.php';"  type="button" class="button-separation"></button>
                <button id="google-plus-icon"  onclick="location.href = 'https://vdohire.com/googleLogin/google_login.php';"  type="button" class="button-separation"></button>
            </div>
        </div>
    </div>

        </div>
    </div>
</form>

<!-- The SignUp modal. Don't display it initially -->
<form id="signupForm" method="post" class="form-horizontal" style="display: none;">
    <div class="container">
        <div class="col-lg-12 col-md-12 col-xs-12 ">
            <div class="form-group">
                <div class="col-lg-12 col-md-12 col-xs-12 ">
                    <h4 style="color: #06577a;font-weight: bold;">Sign up for VDOHire</h4>
                    <p>VDOHire is entirely free for job seekers, gives you immediate access to hundreds of jobs and tools to apply for the desired jobs with Resume Document and Video Interview.</p>
                    
                </div>
            </div>
            
            <div class="form-group">
                <div class="col-lg-5 col-md-5 col-xs-5 ">
                    <input type="text" class="form-control" name="fullname" id="fullname" placeholder="Full Name *" required />
                    
                </div>
                <div class="col-lg-5 col-md-5 col-xs-5">
                    <input type="text" class="form-control" name="mobile" id="mobile" placeholder="Mobile *"  maxlength="10" required />
                    
                    <input type="hidden" class="form-control" name="username" id="username" placeholder="User Name *" value="ssddas" required />
                    
                </div>
                <div class="col-lg-5 col-md-5 col-xs-5">
                    
                    
                </div>
            </div>
            
            <div class="form-group">
                <div class="col-lg-5 col-md-5 col-xs-5">
                    <input type="text" class="form-control" name="signupEmail" id="signupEmail" placeholder="Email *" required />
                    
                </div>
                <div class="col-lg-5 col-md-5  col-xs-5">
                    <input type="password" class="form-control" name="Signuppassword" id="Signuppassword" placeholder="Password *"/>
                    
                </div>
            </div>
            <div class="form-group">
                
            </div>

            <div class="form-group">
                
            </div>
            <div class="form-group">
                <div class="col-lg-5 col-md-5  col-xs-5">
                    <div class="checkbox">
                        <label><input name="signupterms" id="signupterms" type="checkbox" value="rememberMe"><p>I have read the <a href="#" class="termstoAgree" data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal"><b style="color:#06577a;">Terms of Use</b></a> and declare that I agree</p></label>
                    </div>
                    
                </div>
            </div>
                
            <div class="form-group">
                <div class="col-lg-12 col-md-12  col-xs-12">
                    <div class="pull-right">
                        <div style="position:relative;height: 117px;">
                            <div >
                            <button  id="SignUp" class="btn-lg" style="border-color: #f4a835;background: #f4a835;color:white;font-weight:bold;box-shadow: 1px 1px 1px 1px black;" alt="Free Forever">
                                <i class="fa fa-user-plus" aria-hidden="true"></i>&nbsp;Sign up</button>
                                <img src="images/icon/Free1.png" style="    height: 60px;">
                            </div>
                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

<!--forgot password modal-->
<!--modal-->
<div id="forgotpasswordModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="z-index: 1111">
  <div class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h1 class="text-center" style="margin-left: 15%;">Forgot your password?</h1>
      </div>
      <div class="modal-body">
          <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="text-center">
                          
                          <p style="padding-left: 15px;">If you have forgotten your password you can reset it here.</p>
                            <div class="panel-body">
                                <fieldset>
                                    <div class="form-group">
                                        <input id="foremail" class="form-control input-lg" placeholder="E-mail Address" name="email" type="email">
                                    </div>
                                    <input id="forgotpasswordid" class="btn btn-lg btn-primary btn-block" value="Send My Password" type="submit" style="width: 40%; padding-left: 13px; margin-left: 28%;">
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
      </div>
      <div class="modal-footer">
          <div class="col-md-12">
          <!--<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>-->
          </div>    
      </div>
  </div>
  </div>
</div>
<!--End of forgot password modal-->

<!-- The SignUp modal. Don't display it initially -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="z-index:11111" >
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header" style="border-bottom:none;">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h2 id="myModalLabel" style="color:#08688F;font-size: 20px;">Terms of Use</h2>
      </div>
      <div class="modal-body" style="height: 500px;overflow-y: auto;">
          <!DOCTYPE html>



<h1 style="color:#08688F;text-align: center;font-size: 20px;">Privacy Policy</h1>
<div class="text">
    <p>Synergy Relationship Management Services P Ltd. (“VDOHire") is committed to protecting the
        privacy of our users, and protects any Personal Information that the users share with us, in
        an appropriate manner. This Privacy Statement sets forth the online data collection, usage
        policies and practices that apply to this website and as well as users of our on-demand
        video interview platform, tools and services offered by VDOHire ("Service"). This policy
        applies to ("VDOHire" or "VDOHire.co.in") and VDOHire Android app offered at Google Play
        (<a href="https://play.google.com/store/apps/details?id=com.VDOHire.com&hl=en" target="blank">https://play.google.com/store/apps/details?id=com.VDOHire.com&hl=en</a>), which is operated
        and owned by Synergy Relationship Management Services P Ltd. It is VDOHire's
        policy to comply with data protection legislation.</p>
    <p>This regulates the processing of information relating to you and grants you various rights in
        respect of your personal data.</p>
    <p>By registering or by using this site or mobile application, you explicitly accept, without
        limitation or qualification, the collection, use and transfer of the personal information
        provided by you in the manner described in this Statement. Please read this Statement
        carefully as it affects your rights and liabilities under law. If you do not accept the
        Privacy Statement stated herein or disagree with the way we collect and process personal
        information collected on the website, please do not use it.</p>
    <p>Amendments to this policy will be posted to this URL and will be effective when posted. Your
        continued use of this site following the posting of any amendment, modification, or change
        shall constitute your acceptance of the amendment. We will notify you and/or the primary
        VDOHire Service account holder by email when material changes are made to the privacy
        policy prior to that change becoming effective.</p>
    <h2 style="color: #08688F">INFORMATION COLLECTED</h2>
    <p>We collect two kinds of information from you: non-personal information and personal
        information.</p>
    <p>Personal information is information we collect through this website that we can use to
        specifically identify you, and may include your:</p>
    <ul>
        <li>Name, telephone number, address and e-mail address</li>
        <li>Billing information</li>
        <li>Past employment history</li>
        <li>Information you provide in video profile, video interview, resume, biodata, online
            tests, including responses to test questions
        </li>
    </ul>
    <p>We collect personal information from you through the website, mobile application only when
        you voluntarily share it with us, such as when you fill out a registration form, an online
        survey, record and upload your video profile or video interview or send us an e-mail; if you
        choose to apply for a job with us or our clients; or when you use one of our online
        forms.</p>
    <p>We do not store your information like password of your e-mail account. The password of your
        VDOHire account automatically get saved in the VDOHire server when you create an account
        or register with VDOHire and not authorized by VDOHire for access unless desired under any
        legal obligation.</p>
    <p>Non-personal information is information we collect through this website that does not
        identify you by name or as an individual person. It may include information like:</p>
    <ul>
        <li>Type of web browser software your computer uses.</li>
        <li>Pages your computer visited and date and time your computer accessed our pages.</li>
        <li>Personal information that has been de-identified and</li>
        <li>The IP (internet protocol) address associated with your computer.</li>
        <li>The unique identification number of your mobile handset.</li>
    </ul>
    <p>We may also proctor tests using webcam. We may capture live images through the web camera
        enabled tests. This is to prevent you from navigating away from the test window while the
        exam is ongoing. Also, to prevent impersonation and use of unfair means.</p>
    <p>You have the right to request the modification or deletion of any personal information
        retained by VDOHire that is inaccurate, except that information relating to your test
        scores and performance, as set forth below. (You may request for modification or deletion of
        any personal information by emailing our Customer Support at <a
                href="mailto:hello@VDOHire.co.in">hello@VDOHire.co.in</a> or by contacting us by
        telephone or postal mail at the contact information listed below. You must understand that
        VDOHire, in its sole discretion, will determine whether personal information should be
        modified or deleted. We will respond to your request to access within 30 days).</p>
    <p>We use this information, which does not identify individual users, to analyze trends, to
        administer the Website, to track users movements around the Website and to gather
        demographic information about our user base as a whole.</p>
    <h2 style="color: #08688F">COOKIES AND OTHER TRACKING TECHNOLOGIES</h2>
    <p>We collect non-personal information from your computer's web browser. A cookie is an
        information file that the website places on your computer for record keeping and site
        navigation purposes. The cookie placed on your computer by the website does not store your
        name, e-mail address or any other personal information about you. We use both session ID
        cookies and persistent cookies. A session ID cookie expires when you close your browser. We
        use session cookies to make it easier for you to navigate our website. A persistent cookie
        remains on your hard drive for an extended period of time. We set a persistent cookie to
        store your corporate logo, so that you may see it across pages. Your browser software can be
        set to reject cookies, including the cookie from our website. However, please note that if
        you reject our cookies, you will not be able to use this website.</p>
    <h2 style="color: #08688F">Web Beacons / Gifs</h2>
    <p>Our third party advertising partner employs a software technology called clear gifs (a.k.a.
        Web Beacons/Web Pixel), that help us better manage content on our website by informing us
        what content is effective. Clear gifs are tiny graphics with a unique identifier, similar in
        function to cookies, and are used to track the online movements of Web users. In contrast to
        cookies, which are stored on a user's computer hard drive, clear gifs are embedded invisibly
        on Web pages and are about the size of the period at the end of this sentence. We sometimes
        tie in the information gathered by clear gifs to our customers personal information to send
        mailers.</p>
    <h2 style="color: #08688F">Flash LSOs</h2>
    <p>We use local shared objects, also known as Flash cookies, to store your preferences such as
        volume control or display content based upon what you view on our website to personalize
        your visit. Third Parties, with whom we partner to provide certain features on our website
        or to display advertising based upon your Web browsing activity, use Flash cookies to
        collect and store information. Flash cookies are different from browser cookies because of
        the amount of, type of, and how data is stored. Cookie management tools provided by your
        browser will not remove Flash cookies. </p>
    <h2 style="color: #08688F">3rd Party Tracking</h2>
    <p>The use of cookies by our partners, affiliates, tracking utility company, service providers
        is not covered by our Privacy Policy. We do not have access or control over these cookies.
        Our partners, affiliates, tracking utility company, service providers use session ID and
        persistent cookies to make it easier for you to navigate our website.</p>
    <h2 style="color: #08688F">USE AND DISCLOSURE OF INFORMATION</h2>
    <p>We use the information we gather on the website and our mobile application for the purposes
        of:</p>
    <ul>
        <li>Providing our services</li>
        <li>Responding to any queries you may have;</li>
        <li>Operating and improving the website</li>
        <li>Fostering a positive user experience;</li>
        <li>For analytical purposes and to research, develop and improve programs, products,
            services and content;
        </li>
        <li>To remove your personal identifiers (your name and e-mail address). In this case, you
            would no longer be identified as a single unique individual. Once we have de-identified
            information, it is non-personal information and we may use and treat it like other
            non-personal information;
        </li>
        <li>Contact you regarding any problems or questions we have relating to your use of the
            website, or, in our discretion, notify you of changes to our Privacy Statement, Terms of
            Use or other policy or terms that affect you or your use of the website
        </li>
        <li>To enforce this Privacy Statement or the Terms of Use</li>
        <li>To protect our rights or property;</li>
        <li>To protect someone's health, safety or welfare; or</li>
        <li>Comply with a law or regulation, court order or other legal process.</li>
    </ul>
    <h2 style="color: #08688F">SHARING PERSONAL INFORMATION WITH THIRD PARTIES</h2>
    <p>Unless otherwise disclosed in this Privacy Policy or at the time you provide your
        information, VDOHire will only share your personal information with third parties under the
        following limited circumstances:</p>

    <p>We may share the personal information you provide with the organization to which you are
        applying for a job, and with any of its suppliers who are involved in the application
        process. We may also share your personal information with our parent, subsidiary, or
        affiliated companies (Related Companies) for our or their internal business purposes.</p>
    <p>In the ordinary course of business, we also will share some personal information with
        companies that we hire to perform services or functions on our behalf. For example, we may
        use different vendors or suppliers to process your credit card information, to manage our
        contact us chat service or to ship you products that you order from the website. In such
        cases those third parties may have access to your personal information in order to provide
        the services on our behalf and will not use it for any other purposes.</p>
    <p>Some of our pages utilize framing techniques to serve content from our partners while
        preserving the look and feel of our website. Please be aware that you are providing your
        personal information to these third parties and not to first job.co.in</p>
    <p>We may co-operate with law enforcement authorities in investigating and prosecuting website
        users who violate our rules or engage in behavior which is harmful (or illegal) to other
        users. VDOHire may transfer and disclose information about our users, including personal
        information, to enforce this Privacy Policy and the other rules about your use of this
        website, protect our rights or property, protect someone else's safety or welfare, or comply
        with a law or regulation, court order or other legal process.</p>
    <p>VDOHire reserves the right to disclose and transfer user information, including personal
        information, in connection with a corporate sale, merger, dissolution, or acquisition, or
        similar transaction. In such cases, you will be notified via email and/or a prominent notice
        on our website of any change in ownership or uses of your personal information, as well as
        any choices you may have regarding your personal information.</p>
    <h2 style="color: #08688F">RETENTION AND YOUR CHOICES AND ABILITY TO ACCESS PERSONAL
        INFORMATION</h2>
    <p>VDOHire provides individuals reasonable opportunity to access and update their personal
        information, subject to the qualifications noted below. You have the right to obtain
        confirmation from VDOHire that we have retained personal information about you, as well as
        a written description of the nature of that personal information, the purposes for which it
        is being used, the sources of the personal information and a list of the recipients with
        whom we have shared your personal information. You also have the right to request the
        modification or deletion of any personal information retained by VDOHire that is
        inaccurate. Please contact us at the address appearing below to inquire about your personal
        information. VDOHire reserves the right to charge a modest fee for providing you access to
        your personal information.</p>
    <p>Even if you are no longer registered, we will maintain copies of your information in our
        internal records, systems and databases. We will retain and use your information as
        necessary to comply with our legal obligations, resolve disputes, and enforce our
        agreements. We will continue to treat your personal information in accordance with this
        Privacy Policy.</p>
    <p>If you wish to subscribe to our newsletter(s), we will use your name and email address to
        send the newsletter to you. Out of respect for your privacy, you may choose to stop
        receiving our newsletter or marketing emails by following the unsubscribe instructions
        included in these emails or you can contact us at <a href="mailto:hello@VDOHire.co.in">hello@VDOHire.co.in</a>
    </p>
    <h2 style="color: #08688F">LINKS TO OTHER ONLINE RESOURCES</h2>
    <p>This website and mobile application may contain links to other online resources. We provide
        the links for your convenience, but we do not review, control, or monitor the privacy
        practices of online resources operated by others. This Privacy Policy does not apply to any
        other online resources where a link to this Privacy Policy does not appear. We are not
        responsible for the performance of sites operated by third parties or for your business
        dealings with them. Therefore, whenever you leave this website we recommend that you review
        each site's privacy practices and make your own conclusions regarding the adequacy of these
        practices.</p>
    <h2 style="color: #08688F">BLOG</h2>
    <p>Our website offers publicly accessible blogs or community forums. The blog is managed by a
        third party application that may require you to register to post a comment. We do not have
        access or control of the information posted to the blog. You will need to contact or login
        into the third party application if you want the personal information that was posted to the
        comments section removed. To learn how the third party application uses your information,
        please review their privacy policy.</p>
    <h2 style="color: #08688F">TESTIMONIALS</h2>
    <p>We display personal testimonials of satisfied customers on our website in addition to other
        endorsements. With your consent we may post your testimonial along with your name. If you
        wish to update or delete your testimonial, you can contact us at <a
                href="mailto:hello@VDOHire.co.in">hello@VDOHire.co.in</a></p>
    <h2 style="color: #08688F">COLLECTION AND USE OF 3RD PARTY PERSONAL INFORMATION</h2>
    <p>You may also provide personal information about other people, such as their name and email
        address. This information is only used for the sole purpose of completing your request or
        for whatever reason it may have been provided.</p>
    <h2 style="color: #08688F">SOCIAL MEDIA WIDGETS</h2>
    <p>Our website includes Social Media Features, such as the Facebook Like button and Widgets,
        such as the Share this button or interactive mini-programs that run on our website. These
        Features may collect your IP address, which page you are visiting on our website, and may
        set a cookie to enable the Feature to function properly. Social Media Features and Widgets
        are either hosted by a third party or hosted directly on our website. Your interactions with
        these Features are governed by the privacy policy of the company providing it.</p>
    <h2 style="color: #08688F">CONSENT TO TRANSFER</h2>
    <p>This website is operated in India. If you are located in the United States, European Union,
        Canada or elsewhere outside of India, please be aware that any information you provide to us
        will be transferred to India. By using the website or providing us with your information,
        you consent to this transfer.</p>
    <h2 style="color: #08688F">SECURITY</h2>
    <p>We have implemented commercially reasonable electronic, physical and procedural safeguards
        designed to help protect against the loss, unauthorized access or disclosure, alteration or
        destruction of the information under our control. When you enter sensitive information (such
        as a credit card number) on our order forms, we encrypt the transmission of that information
        using secure socket layer technology (SSL). Certain features of our websites may require you
        to use an access code, password or similar unique identifier. You are responsible for
        protecting the secrecy of your account information. VDOHire recommends that you use the
        latest browsers so as to take advantage of advances in security technology. Although we
        strive to protect your encrypted information, no data transmission over the Internet is
        completely secure. There is some risk associated with any data transmission, including the
        risk that personal information may be intercepted in transit. Therefore, we cannot guarantee
        its absolute security. If you have any questions about security on our website, you can
        contact us at <a href="mailto:hello@VDOHire.co.in">hello@VDOHire.co.in</a></p>
    <h2 style="color: #08688F">CHILDREN'S PRIVACY</h2>
    <p>VDOHire does not knowingly collect or solicit any information from anyone under the age of
        16 or knowingly allow such persons to register. The Service and its content are not directed
        at children under the age of 16. In the event that we learn that we have collected personal
        information from a child under age 16, we will delete that information as quickly as
        possible. If you believe that we might have any information from or about a child under 16,
        please contact us via email.</p>

    <h2 style="color: #08688F">HOW TO CONTACT US ABOUT A DECEASED USER</h2>
    <p>In the event of the death of an VDOHire User, please contact us. We will usually conduct our
        communication via email; should we require any other information, we will contact you at the
        email address you have provided in your request.</p>

    <h2 style="color: #08688F">ACCEPTANCE & PRIVACY POLICY CHANGES</h2>
    <p>By using this website and mobile application, you accept our privacy practices, as outlined
        in this Privacy Policy. VDOHire reserves the right to modify, alter or otherwise update
        this Privacy Policy at any time. We will post any new or revised policies on the website.
        However, VDOHire will use your personal information in a manner consistent with the Privacy
        Policy in effect at the time you submitted the information, unless you consent to the new or
        revised policy.</p>

    <h2 style="color: #08688F">CONTACT US</h2>
    <p>If you have questions or concerns about this Privacy Policy, please contact us through the
        information below:</p>
    <h2 style="color: #08688F">India</h2>
    <address>
        Mr. Ankur Paliwal<br>
        Deputy Manager<br>
        Synergy Relationship Management Services P Ltd.<br>
        C-2/4, Sushant Lok - 1,Gurgaon, Haryana,<br>
        India - 122002,<br>
        Email - <a href="mailto:hello@VDOHire.co.in">hello@VDOHire.co.in</a>
    </address>


</div>


      </div>
      <div class="modal-footer" style="border-top:none;">
        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>

<!-- The login modal. Don't display it initially -->
<!--<script src="js/jquery.min.js"></script>-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/jquery.fullPage.js"></script>
<script defer src="js/jquery.flexslider.js"></script>
<script type="text/javascript" src="js/validation.js"></script>
<script src="js/owl.carousel.js"></script>
<script type="text/javascript" src="js/jquery.bxslider.js"></script> 

<script src="js/bootstrap.min.js"></script>
<script src="js/bootbox.min.js"></script>





<script type="application/javascript" src="js/waypoints.min.js"></script>
<script type="text/javascript" src="js/main.js"></script>
<script type="text/javascript" src="js/home.js"></script>
<script src="js/formValidation.min.js"></script>
<script src="js/min/bootstrap.min.js"></script>




<script type="text/javascript">
$(document).ready(function() {
	$('#fullpageemployer').fullpage({
			anchors: ['get-a-job', 'benefits-of-using-firstjob', 'vedio-inertview', 'explore-more-features', 'firstJob-help-clients', 'in-media', 'in-clients', 'request-a-demo'],
			navigation: false,
			navigationPosition: 'right',
			navigationTooltips: ['First page', 'Second page', 'Thired page', 'Forth page', 'Fifth page', 'Sixth page', 'Seventh page', 'Eighth page'],
			responsiveWidth: 1100,
			afterLoad:function(event){
				var url = window.location.href;
				var type = url.split('#');
				var hash = '';
				if(type.length > 1)
				  hash = type[1];
				  
				 if(hash == 'benefits-of-using-firstjob'){
					$('#beneis01').append(function() {
						setTimeout(function(){$('#beneis01').addClass('animated fadeInDown')},600);
					});
					$('#beneis02').append(function(){
						setTimeout(function(){$('#beneis02').addClass('animated fadeInDown')},600);
					});
					$('#beneis03').append(function(){
						setTimeout(function(){$('#beneis03').addClass('animated fadeInDown')},600);
					});
					$('#beneis04').append(function(){
						setTimeout(function(){$('#beneis04').addClass('animated fadeInUp')},1000);
					});
					$('#beneis05').append(function(){
						setTimeout(function(){$('#beneis05').addClass('animated fadeInUp')},1000);
					});
					$('#beneis06').append(function(){
						setTimeout(function(){$('#beneis06').addClass('animated fadeInUp')},1000);
					});
				}
				if(hash == 'vedio-inertview'){
					$('.innertext').append(function(){
						setTimeout(function(){$('.innertext').addClass('animated fadeInUp')},600);
					});
				}
				if(hash == 'explore-more-features'){
					$('#Whats01').append(function(){
						setTimeout(function(){$('#Whats01').addClass('animated fadeInUp')},600);
					});
					
					$('#Whats02').append(function(){
						setTimeout(function(){$('#Whats02').addClass('animated fadeInUp')},600);
					});
					
					$('#Whats03').append(function(){
						setTimeout(function(){$('#Whats03').addClass('animated fadeInUp')},600);
					});
				}
				if(hash == 'firstJob-help-clients'){
					$('.clients_Left_text').append(function(){
						setTimeout(function(){$('.clients_Left_text').addClass('animated fadeInLeft')},600);
					});
					
					$('.clients_Right_IMG').append(function(){
						setTimeout(function(){$('.clients_Right_IMG').addClass('animated fadeInRight')},600);
					});
				}

                if(hash == 'in-media'){
                    $('.Media_Inner_grid').append(function(){
                        setTimeout(function(){$('.Media_Inner_grid').addClass('animated fadeInDown')},500);
                    });
                }

                if(hash == 'in-clients'){
                    $('.Media_Inner_grid_client').append(function(){
                        setTimeout(function(){$('.Media_Inner_grid_client').addClass('animated fadeInDown')},500);
                    });
                }

				if(hash == 'request-a-demo'){
					$('#cont_form').append(function(){
						setTimeout(function(){$('#cont_form').addClass('animated fadeInUp')},500);
					});
				}
			}
	});
});

//********************************************
$(document).ready(function() {
	var winw = $(window).width();
	var winheigh = $(window).height();
	$('.flexslider ul.slides li').css({'width':winw,'height':winheigh});
		$('.flexslider').flexslider({
		auto: true,
		animation: "slide",
		controlNav: true,
		directionNav: false,
		slideshowSpeed: 4000,
	});
	
	$('.bxslider').bxSlider({
		 mode: 'vertical',
		 controls: true,
		 pager: false,
		 speed: 5000,
		 auto: true,
         autoControls: true,
		 pause: 1000,
         randomStart : true,
    });

    $('.bxsliderMedia').bxSlider({
         controls: true,
         pager:true,
         speed:6000,
         auto: true,
         autoControls: true,
         pause: 300,
         randomStart : true,
    });
    
    $('.bxsliderMediaMOBILE').bxSlider({
       minSlides: 1,
       maxSlides: 2,
       slideWidth: 360,
       slideMargin: 10,
       controls: true,
         pager: false,
         speed: 5000,
         auto: true,
         autoControls: true,
         pause: 300,
         randomStart : true,
    });
});

//******************************************************
$('#vPhone').keyup(function() {
	if (this.value.match(/[^0-9]/g)) {
		this.value = this.value.replace(/[^0-9]/g, '');
		$('.formError').show();
		$('.formError').html('Enter only number');
		setTimeout(function(){
			$('.formError').hide();
		},5000);
	}
});
var documentHeight = $(window).height();
$("#bannerImg").css("height",documentHeight);
//bannerImg
//****************************************************

</script>



</body>
</html>
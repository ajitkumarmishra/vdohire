<?php
require('http.php');
require('oauth_client.php');
require('config.php');


$client = new oauth_client_class;

// set the offline access only if you need to call an API
// when the user is not present and the token may expire
$client->offline = FALSE;

$client->debug = false;
$client->debug_http = true;
$client->redirect_uri = REDIRECT_URL;

$client->client_id = CLIENT_ID;
$application_line = __LINE__;
$client->client_secret = CLIENT_SECRET;

if (strlen($client->client_id) == 0 || strlen($client->client_secret) == 0)
  die('Please go to Google APIs console page ' .
          'http://code.google.com/apis/console in the API access tab, ' .
          'create a new client ID, and in the line ' . $application_line .
          ' set the client_id to Client ID and client_secret with Client Secret. ' .
          'The callback URL must be ' . $client->redirect_uri . ' but make sure ' .
          'the domain is valid and can be resolved by a public DNS.');

/* API permissions
 */
$client->scope = SCOPE;
if (($success = $client->Initialize())) {
  if (($success = $client->Process())) {
    if (strlen($client->authorization_error)) {
		$client->error = $client->authorization_error;
		$success = false;
    } elseif (strlen($client->access_token)) {
		$success = $client->CallAPI(
              'https://www.googleapis.com/oauth2/v1/userinfo', 'GET', array(), array('FailOnAccessError' => true), $user);
    }
  }
  $success = $client->Finalize($success);
}
if ($client->exit)
  exit;
if ($success) {
	
  // Now check if user exist with same email ID
  $_SESSION["user_id"] = $user->id;
  
  $_SESSION['FBID'] = $user->id;         
        $_SESSION['FULLNAME'] = $user->name;
	    $_SESSION['EMAIL'] =  $user->email;
  
 
} else {
  $_SESSION["e_msg"] = $client->error;
}

?>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<body>

<div class="container" style="width:100%;">
<nav class="navbar navbar-inverse" id="navheight" style="    margin-bottom: 0px;    height: 76px;    background: #EB9B2C;border: none;">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="https://vdohire.com/jobseeker/users"><img src="https://vdohire.com/jobseeker/images/logo.png" rel="stylesheet"></a>
    </div>
    
    
    
  </div>
</nav>
<div style="background:#054F72;height: 158px;font-size: 33px;color: white;text-align: center;    padding: 52px;">
You have successfully logged in using Google. Please provide the information.
</div>
  <!-- Modal -->
  <div class="modal" id="myModal" role="dialog">
    <div class="modal-dialog" style="    width: 50%;">
    
      <!-- Modal content-->
      <div class="modal-content" style="margin-top: 30%;">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Basic Details</h4>
        </div>
        <div class="modal-body">
          <form id="Register" method="post" class="form-horizontal fv-form fv-form-bootstrap" style="" novalidate="novalidate"><button type="submit" class="fv-hidden-submit" style="display: none; width: 0px; height: 0px;"></button>
    <div id="page-content-wrapper">
        <div class="container-fluid whitebg">
            <div class="row">
                <div class="col-md-12 padd_tp_bt_20">
                    <div class="form-horizontal form_save">
            <div class="form-group has-feedback">
                <label for="inputEmail" class="control-label col-xs-3 required-field "><span style="color:red;">*</span>Email</label>
                <div class="col-xs-9">
                    <input type="text" class="form-control" placeholder="Email" name="email" id="email" value="<?php echo isset($_SESSION['EMAIL'])?$_SESSION['EMAIL']:""; ?>" data-fv-field="email" <?php echo isset($_SESSION['EMAIL'])? "readonly" : ""; ?>>
					<i class="form-control-feedback" data-fv-icon-for="email" style="display: none;"></i>
				</div>
            </div>
			
			<div class="form-group has-feedback">
                <label for="inputEmail" class="control-label col-xs-3 required-field "><span style="color:red;">*</span>Name</label>
                <div class="col-xs-9">
                    <input type="text" class="form-control" placeholder="Name" name="name" data-validation="length" data-validation-length="min6" id="name" value="<?php echo isset($_SESSION['FULLNAME'])?$_SESSION['FULLNAME']:""; ?>" data-fv-field="name"><i class="form-control-feedback" data-fv-icon-for="name" style="display: none;"></i>
                </div>
            </div>
			<div class="form-group has-feedback">
                <label for="inputEmail" class="control-label col-xs-3 required-field "><span style="color:red;">*</span>Phone</label>
                <div class="col-xs-9">
                    <input type="text" class="form-control" placeholder="Phone" name="phone" data-validation="length" data-validation-length="min6" id="phone" value="" data-fv-field="phone"><i class="form-control-feedback" data-fv-icon-for="phone" style="display: none;"></i>
                </div>
            </div>
			
            <div class="form-group has-feedback">
                <label for="inputEmail" class="control-label col-xs-3 required-field "><span style="color:red;">*</span>New Password</label>
                <div class="col-xs-9">
                    <input type="text" class="form-control" placeholder="New Password" name="newPassword" data-validation="length" data-validation-length="min6" id="newPassword" value="" data-fv-field="newPassword"><i class="form-control-feedback" data-fv-icon-for="newPassword" style="display: none;"></i>
                </div>
            </div>
            <div class="form-group has-feedback">
                <label for="inputEmail" class="control-label col-xs-3 required-field "><span style="color:red;">*</span>Confirm New Password</label>
                <div class="col-xs-9">
                    <input type="text" class="form-control" placeholder="Confirm New Password" name="confirmNewPassword" id="confirmNewPassword" value="" data-fv-field="confirmNewPassword"><i class="form-control-feedback" data-fv-icon-for="confirmNewPassword" style="display: none;"></i>
                </div>
            </div>

            <div class="form-group">
                <div class="col-xs-5 col-xs-offset-3">
                    <input type="hidden" name="token" id="token" value="ecbcd7eaee29848978134beeecdfbc7c">
                    <input type="hidden" name="methodname" id="methodname" value="signUp_v2">
					<input type="hidden" name="fb_id" id="fb_id" value="<?php echo isset($_SESSION['FBID'])?$_SESSION['FBID']:""; ?>">
                    <input type="hidden" name="userId" id="userId" value="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpZCI6IjE5MCJ9.TfL2ffRt2Ik_m13LfWlBi85IWy3hcW98WaSPYDOAkloDAM7jdDT18vmCDPrMBCtulQV_n-1sW2CJEsZRI4SMgQ">
                    <button type="submit" class="btn btn-default pull-right" onclick="signUp();" style="background: #054F72; color: white;">SAVE</button>
                </div>
            </div>

        </div>
    </div>
        </div>
    </div>
    </div>
</form>
        </div>
        <div class="modal-footer">
          <div class=""><span style="color:red;">*</span>Required Field.</div>
        </div>
      </div>
      
    </div>
  </div>
  
</div>
  </body>
    <script src="https://firstjob.co.in/js/formValidation.min.js"></script>
<script src="https://firstjob.co.in/js/min/bootstrap.min.js"></script>
<?php
if(isset($_SESSION["user_id"]) && $_SESSION["user_id"] != ""){
?>
<script type="text/javascript">
$(document).ready(function() {
    fb_login();
});
</script>
<?php
}
?>
<script type="text/javascript">
/*for fb login*/
function signUp(){
	$('#Register').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                email: {
                    validators: {
                        notEmpty: {
                            message: 'The username is required'
                        },
                        emailAddress: {
                            message: 'The value is not a valid email address'
                        }
                    }
                },
                name: {
                    validators: {
                        notEmpty: {
                            message: 'Enter Your Full Name'
                        }
                    }
                },
				phone: {
                    validators: {
                        notEmpty: {
                            message: 'Enter Phone Number'
                        }
                    }
                },
				newPassword: {
                    validators: {
                        notEmpty: {
                            message: 'The password is required'
                        },
                        stringLength: {
                            min: 6,
                            max: 30,
                            message: 'The password must be more than 6 characters long'
                        }
                    }
                },
				confirmNewPassword: {
                    validators: {
                        notEmpty: {
                            message: 'The password is required'
                        },
                        stringLength: {
                            min: 6,
                            max: 30,
                            message: 'The password must be more than 6 characters long'
                        }
                    }
                }
				
            }
        })
		.on('success.form.fv', function(e) {
            // Prevent form submission
            e.preventDefault();

            var fullname = $("#name").val();
            var username = $("#name").val();
            var inputEmail = $("#email").val();
            var mobile = $("#phone").val();
            var email = $("#email").val();
            var inputPassword = $("#newPassword").val();
            var methodname = 'signUp_v4';
            var token = 'ecbcd7eaee29848978134beeecdfbc7c';
            var gender = 'male';
            var pincode = '110075';
            var deviceId = '1234';
            var deviceType = 'android';
            var social_networking_type = '4';
			var networking_id = '<?php echo $_SESSION['FBID'];?>';
            var js_obj = {social_networking_type:social_networking_type,networking_id:networking_id, fullname: fullname, username: username, email: inputEmail, password: inputPassword,token: token,methodname: methodname,
    mobile:mobile,gender:gender,pincode:pincode,deviceId:deviceId,deviceType:deviceType,};

            var encoded = JSON.stringify( js_obj );

            var data= encoded;

            $.ajax({
                    url : "https://"+window.location.host+"/jobseeker/Fjapi",
                    data: data,
                    type: "POST",
                    success: function(response){
                        console.log(response);
                        item = $.parseJSON(response);
                        if(item.code == "200"){
							
                            $.ajax({
                                url : "https://"+window.location.host+"/jobseeker/users/setSessionSignUp",
                                data: response,
                                type: "POST",
                                success: function(response){
                                    
                                    if(response == "success"){
                                        window.location="https://"+window.location.host+"/jobseeker/users";
                                    }else{
                                        alert(response);
                                    }
                                }
                            });
                            
                        }else{
                            alert(item.message);
                        }
                    }
            });
        });
        
        
}
function fb_login(){
	
    var methodname = 'isUserExists_V1'; //isUserExists     signIn
	var token = 'ecbcd7eaee29848978134beeecdfbc7c';
	var email = '<?php echo $_SESSION['EMAIL']; ?>';
	
	var social_networking_type = '4';
	var networking_id = '<?php echo $_SESSION['FBID'];?>';
	var email = $("#email").val();
	
	var js_obj = {social_networking_type: social_networking_type,networking_id:networking_id, token: token, methodname: methodname,email:email};
	var encoded = JSON.stringify( js_obj );
	var data= encoded;
	$.ajax({
		url : "https://"+window.location.host+"/jobseeker/Fjapi",
		data: data,
		type: "POST",
		success: function(response){
			console.log(response);
			item = $.parseJSON(response);
			if(item.code == "200"){
				var tokenUser = item.token;
				$.ajax({
					url : "https://"+window.location.host+"/jobseeker/users/setSession",
					data: response,
					type: "POST",
					success: function(response){
						
						if(response == "success"){
							window.location = "https://"+window.location.host+"/jobseeker/users";
						}else{
							alert(response);
						}
					}
				});
				
			}else if(item.code == "202"){
				//show popup 
				$("#myModal").show();
			}
		}
	});
}
</script>
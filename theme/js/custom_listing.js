$(document).on("click", ".del_Listing", function () {
    var delID = $(this).attr('name');
    var deleteUrl = $("#P_deleteurl").val();
    bootbox.confirm("Are you sure to delete this ?", function (result) {
        if (result == true) {
            $.ajax({
                type: 'POST',
                url: deleteUrl,
                data: {delID: delID},
                success: function (res) {
                    var obj = JSON.parse(res);
                    if (obj['status'] == true) {
                        location.reload();
                    } else {
                        alert("Bad request");
                    }
                },
            });
        }
    });
});

function checkall(objForm){
	len = objForm.elements.length;
	var i=0;
	for( i=0 ; i<len ; i++) {
		if (objForm.elements[i].type=='checkbox') {
			objForm.elements[i].checked=objForm.check_all.checked;
		}
	}
}
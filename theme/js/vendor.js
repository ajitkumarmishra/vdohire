$().ready(function () {
    $("#vendorForm").validate({
        rules: {
            vendor_name: "required",
            vendor_address1: "required",
            vendor_address2: "required",
            vendor_landline: {
                required: true,
                minlength: 3,
                maxlength: 15,
                number: true
            },
            vendor_mobile:{
                required: true,
                minlength: 3,
                maxlength: 15,
                number: true
            },
            vendor_city: "required",
            vendor_district: "required",
            vendor_state: "required",
            vendor_country: "required",
            vendor_tin: "required",
            vendor_account: "required",
            vendor_bank: "required",
            vendor_ifsc: {
                required: true
            },
            vendor_branch:"required"

        },
        messages: {
            vendor_name: "Please enter name",
            vendor_address1: "Please enter address",
            vendor_address2: "Please enter address",
        }
    });
    $('#vendor_country').change(function () {
        var ID = this.value;
        $.ajax({
            url: base_url + "/vendor/getStates/",
            type: 'post',
            data: {country_code: ID},
            success: function (result) {
                var obj = JSON.parse(result);
                $("#vendor_state").html('<option value="">Select State</option>')
                for (i = 0, len = obj.length; i < len; ++i) {
                    $("#vendor_state").append('<option value="' + obj[i]['id'] + '">' + obj[i]['state_name'] + '</option>');
                }
            }
        });
    });
})
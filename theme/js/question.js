$().ready(function () {
    $("#quesFormCreate").validate({
        rules: {
            question_industry: "required",
            experience: "required",
            question_title: "required",
            question_file: "required",
            question_ans: "required"
        },
        messages: {
            question_industry: "Please Select Industry type",
        }
    });
})
$().ready(function () {
    $("#quesFormEdit").validate({
        rules: {
            question_industry: "required",
            experience: "required",
            question_title: "required",
        },
        messages: {
            question_industry: "Please Select Industry type",
        }
    });
})

$().ready(function () {
    $("#sampleFormCreate").validate({
        rules: {
            question_type: "required",
            question_title: "required",
            question_file: "required",
        },
        messages: {
            question_title: "Please Select Question Title",
        }
    });
})

$().ready(function () {
    $("#sampleFormEdit").validate({
        rules: {
            question_title: "required",
        },
        messages: {
            question_title: "Please Select Question Title",
        }
    });
})


$().ready(function () {
    
    $("#interviewFormCreate").validate({
        rules: {
            question_industry: "required",
            experience: "required",
            question_title: "required",
            question_file: "required",
        },
        messages: {
            question_industry: "Please Select Industry type",
        }
    });
})

$().ready(function () {
    $("#interviewFormEdit").validate({
        rules: {
            question_industry: "required",
            experience: "required",
            question_title: "required"
        },
        messages: {
            question_industry: "Please Select Industry type",
        }
    });
})

$('#question_industry').change(function () {
    var ID = this.value;
    $.ajax({
        url: base_url + "/questions/getIndusFunctions/",
        type: 'post',
        data: {industry: ID},
        success: function (result) {
            var obj = JSON.parse(result);
            $("#question_function").html('<option value="">Select Function</option>')
            for (i = 0, len = obj.length; i < len; ++i) {
                $("#question_function").append('<option value="' + obj[i]['id'] + '">' + obj[i]['name'] + '</option>');
            }
        }
    });
});

$('#question_ans_one').blur(function () {
    var ID = this.value;
    $.ajax({
        url: base_url + "/questions/get_question1/",
        type: 'post',
        data: {answer1: ID},
        success: function (result) {
            var obj = JSON.parse(result);
            // $("#get_answer1").html('value=""')
            $("#get_answer1").val(obj);

        }
    });
});

$('#question_ans_two').blur(function () {
    var ID = this.value;
    $("#get_answer2").val(ID);
});

$('#question_ans_three').blur(function () {
    var ID = this.value;
    $("#get_answer3").val(ID);
});

$('#question_ans_four').blur(function () {
    var ID = this.value;
    $("#get_answer4").val(ID);
});


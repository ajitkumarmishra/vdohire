$(document).on("click", ".del_Listing", function () {
    var delID = $(this).attr('name');
    var deleteUrl = $("#P_deleteurl").val();
    bootbox.confirm("Are you sure to delete this ?", function (result) {
        if (result == true) {
            $.ajax({
                type: 'POST',
                url: deleteUrl,
                data: {delID: delID},
                success: function (res) {
                    var obj = JSON.parse(res);
                    if (obj['status'] == true) {
                        location.reload();
                    } else {
                        bootbox.alert(obj['message']);
                    }
                },
            });
        }
    });
});

$().ready(function () {
    $("#storeForm").validate({
        rules: {
            counter_id: "required",
            store_name: "required",
            store_address1: "required",
            store_landline: {
                required: true,
                minlength: 3,
                maxlength: 15,
                number: true
            },
            store_mobile: {
                required: true,
                minlength: 3,
                maxlength: 15,
                number: true
            },
            store_city: "required",
            store_district: "required",
            store_state: "required",
            store_country: "required",
            store_currency: "required",
            store_central_tax: "required",
            store_state_tax: "required",
            store_zip: {
                required: true,
                number: true
            }

        },
        messages: {
            counter_id: "Please enter Counter ID",
            store_name: "Please enter Store Name",
            store_address1: "Please enter Adress",
        }
    });
    $('#store_country').change(function () {
        var ID = this.value;
        //var base_url = window.location.origin;
        $.ajax({
            url: base_url + "/store/getStates/",
            type: 'post',
            data: {country_code: ID},
            success: function (result) {
                var obj = JSON.parse(result);
                $("#store_state").html('<option value="">Select State</option>')
                for (i = 0, len = obj.length; i < len; ++i) {
                    $("#store_state").append('<option value="' + obj[i]['id'] + '">' + obj[i]['state_name'] + '</option>');
                }
            }
        });
    });
})
$.validator.setDefaults({
    submitHandler: function () {
        alert("submitted!");
    }
});

function checkall(objForm) {
    len = objForm.elements.length;
    var i = 0;
    for (i = 0; i < len; i++) {
        if (objForm.elements[i].type == 'checkbox') {
            objForm.elements[i].checked = objForm.check_all.checked;
        }
    }
}
$().ready(function () {
    $("#taxForm").validate({
        rules: {
            tax_description: "required",
            tax_rate: "required",
            tax_country: "required",
            tax_state: "required",
        },
        messages: {
            tax_description: "Please enter Tax Description",
            tax_rate: "Please enter Tax Rate",
            tax_country: "Please Select Country",
            tax_state: "Please Select State",
        }
    });
    $("#allocationForm").validate({
        rules: {
            product_barcode: "required",
            country_code: "required",
            state_id: "required",
            tax_id: "required",
        },
        messages: {
            product_barcode: "Please Select product",
            country_code: "Please Select Country",
            state_id: "Please Select State",
            tax_id:"Please Select tax"
        }
    });

    $('#tax_country').change(function () {
        var ID = this.value;
        $.ajax({
            url: base_url + "/tax/getStates/",
            type: 'post',
            data: {country_code: ID},
            success: function (result) {
                var obj = JSON.parse(result);
                $("#tax_state").html('<option value="">Select State</option>')
                for (i = 0, len = obj.length; i < len; ++i) {
                    $("#tax_state").append('<option value="' + obj[i]['id'] + '">' + obj[i]['state_name'] + '</option>');
                }
            }
        });
    });
    $('#country_code').change(function () {
        var ID = this.value;
        console.log(base_url);
        $.ajax({
            url: base_url + "/tax/getStates/",
            type: 'post',
            data: {country_code: ID},
            success: function (result) {
                var obj = JSON.parse(result);
                $("#state_id").html('<option value="">Select State</option>')
                for (i = 0, len = obj.length; i < len; ++i) {
                    $("#state_id").append('<option value="' + obj[i]['id'] + '">' + obj[i]['state_name'] + '</option>');
                }
            }
        });
    });
    $('#state_id').change(function () {
        var ID = this.value;
        //var base_url = window.location.origin;
        $.ajax({
            url: base_url + "/tax/getTaxList/",
            type: 'post',
            data: {state_id: ID},
            success: function (result) {
                var obj = JSON.parse(result);
                $("#tax_id").html('<option value="">Select Tax rate</option>')
                for (i = 0, len = obj.length; i < len; ++i) {
                    $("#tax_id").append('<option value="' + obj[i]['tax_id'] + '">' + obj[i]['tax_rate'] + '</option>');
                }
            }
        });
    })
})
$.validator.setDefaults({
    submitHandler: function () {
        alert("submitted!");
    }
});

function checkall(objForm) {
    len = objForm.elements.length;
    var i = 0;
    for (i = 0; i < len; i++) {
        if (objForm.elements[i].type == 'checkbox') {
            objForm.elements[i].checked = objForm.check_all.checked;
        }
    }
}
function import_csv(){
    $('input[type=file]').click();
}
function submit_form(){
    $("#import_btn").click();
}

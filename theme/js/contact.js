$().ready(function () {
    $("#contactForm").validate({
        rules: {
            first_name: "required",
            last_name: "required",
            email: {
                required: true,
                email: true
            },
            phone: {
                required: true,
                minlength: 3,
                maxlength: 15,
                number: true
            },
        },
        messages: {
            first_name: "Please enter First name"
        }
    });
})
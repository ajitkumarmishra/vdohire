$().ready(function () {
    $("#tipsForm").validate({
        rules: {
            tips_title: "required",
            tips_description: "required",
            tips_cat: "required",
        },
        messages: {
            tips_title: "Please Select Industry type",
        }
    });
})

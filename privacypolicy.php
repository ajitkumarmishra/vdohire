
<!DOCTYPE html>
<html lang="en-us">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>:: Get Your First Job Today ::</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="author" content="" />
<meta name="description" content="" />
<meta name="keywords"  content="" />
<meta name="Resource-type" content="" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
<link rel="icon" href="images/favicon.ico" type="image/x-icon">
<style>
.text {
	font:16px/24px 'Edmondsans-Medium';
   float: center;
   margin: 10px;
   padding: 10px;
   max-width: auto;
   height: auto;
   line-height: 19px;
  
}  



/* ------------------------------------- 
    HEADER 
------------------------------------- */
table.head-wrap { width: 100%;}

.header.container table td.logo { padding: 15px; }
.header.container table td.label { padding: 15px; padding-left:0px;} 
p{margin-top: 0;
text-align: justify;}
</style>
</head>
<body>
<table class="head-wrap" >
  <tr>
    <td></td>
    <td class="header container">
    <td><a href="https://firstjob.co.in"><img style="height:64px;margin-left: 190px;margin-top: 10px" src="images/logo1.png" /></a></td>
      
        
            <td align="right">
              <ul type="none">
                <li style="float: right;margin-top: -16px;margin-right: 250px;"><a style="text-decoration: none;" href="https://firstjob.co.in/blog/">BLOG</a></li>
               <li style="float: right;margin-top: -16px;margin-right: 20px;"><a style="text-decoration: none;" href="employers">EMPLOYER</a></li>
                <li style="float: right;margin-top: -16px;/* margin-right: -101px; */margin-right: 26px;"><a style="text-decoration: none;" href="https://firstjob.co.in">JOBSEEKER</a></li>
              </ul>
            </td>
          </tr>
        </table>
        </div>
        
    </td>
    <td></td>
  </tr>
</table><!-- /HEADER -->

<h1 style="text-align: center;font-size: 44px;">Privacy Policy</h1>
<div class="text">
 					 <p>Synergy Relationship Management Services P Ltd. (“FirstJob”) is committed to protecting the privacy of our users, and protects any Personal Information that the users share with us, in an appropriate manner. This Privacy Statement sets forth the online data collection, usage policies and practices that apply to this website and as well as users of our on-demand video interview platform, tools and services offered by FirstJob (“Service”). This policy applies to  (“FirstJob" or “<a href="https://www.FirstJob.co.in"> FirstJob.co.in</a>) and FirstJob Android app offered at Google Play (<a href="https://play.google.com/store/apps/details?id=com.firstjob.com&hl=en">https://play.google.com/store/apps/details?id=com.firstjob.com&hl=en</a>), which is operated and owned by Synergy Relationship Management Services P Ltd. It is FirstJob's policy to comply with data protection legislation.</p>
  						<p>This regulates the processing of information relating to you and grants you various rights in respect of your personal data.</p>
  							<p>By registering or by using this site or mobile application, you explicitly accept, without limitation or qualification, the collection, use and transfer of the personal information provided by you in the manner described in this Statement. Please read this Statement carefully as it affects your rights and liabilities under law. If you do not accept the Privacy Statement stated herein or disagree with the way we collect and process personal information collected on the website, please do not use it.</p>
 								 <p>Amendments to this policy will be posted to this URL and will be effective when posted. Your continued use of this site following the posting of any amendment, modification, or change shall constitute your acceptance of the amendment. We will notify you and/or the primary FirstJob Service account holder by email when material changes are made to the privacy policy prior to that change becoming effective.</p>
  									<h2 style="color: #08688F">INFORMATION COLLECTED</h2>
 										 <p>We collect two kinds of information from you: non-personal information and personal information.</p>
 											<p>Personal information is information we collect through this website that we can use to specifically identify you, and may include your:</p>
 											 <ul>
  												<li>Name, telephone number, address and e-mail address</li>
  													<li>Billing information </li>
  														<li>Past employment history</li>
  															<li>Information you provide in video profile, video interview, resume, biodata, online tests, including responses to test questions</li>
  															</ul>
  														<p>We collect personal information from you through the website, mobile application only when you voluntarily share it with us, such as when you fill out a registration form, an online survey, record and upload your video profile or video interview or send us an e-mail; if you choose to apply for a job with us or our clients; or when you use one of our online forms.</p>
  													<p>We do not store your information like password of your e-mail account. The password of your FirstJob account automatically get saved in the FirstJob server when you create an account or register with FirstJob and not authorized by FirstJob for access unless desired under any legal obligation.</p>
  												<p>Non-personal information is information we collect through this website that does not identify you by name or as an individual person. It may include information like:</p>
  											<ul>
  										<li>Type of web browser software your computer uses.</li>
  									<li>Pages your computer visited and date and time your computer accessed our pages. </li>
  								<li>Personal information that has been de-identified and </li>
  							<li>The IP (internet protocol) address associated with your computer. </li>
  						<li>The unique identification number of your mobile handset.</li>
  					</ul>
  				<p>We may also proctor tests using webcam. We may capture live images through the web camera enabled tests. This is to prevent you from navigating away from the test window while the exam is ongoing. Also, to prevent impersonation and use of unfair means.</p>
  			<p>You have the right to request the modification or deletion of any personal information retained by FirstJob that is inaccurate, except that information relating to your test scores and performance, as set forth below. (You may request for modification or deletion of any personal information by emailing our Customer Support at <a href="mailto:hello@firstjob.co.in">hello@firstjob.co.in</a> or by contacting us by telephone or postal mail at the contact information listed below. You must understand that FirstJob, in its sole discretion, will determine whether personal information should be modified or deleted. We will respond to your request to access within 30 days).</p>
  				<p>We use this information, which does not identify individual users, to analyze trends, to administer the Website, to track users movements around the Website and to gather demographic information about our user base as a whole.</p>
  					<h2 style="color: #08688F">COOKIES AND OTHER TRACKING TECHNOLOGIES</h2>
  						<p>We collect non-personal information from your computer's web browser. A cookie is an information file that the website places on your computer for record keeping and site navigation purposes. The cookie placed on your computer by the website does not store your name, e-mail address or any other personal information about you. We use both session ID cookies and persistent cookies. A session ID cookie expires when you close your browser. We use session cookies to make it easier for you to navigate our website. A persistent cookie remains on your hard drive for an extended period of time. We set a persistent cookie to store your corporate logo, so that you may see it across pages. Your browser software can be set to reject cookies, including the cookie from our website. However, please note that if you reject our cookies, you will not be able to use this website.</p>
   							<h2 style="color: #08688F">Web Beacons / Gifs</h2>
   								<p>Our third party advertising partner employs a software technology called clear gifs (a.k.a. Web Beacons/Web Pixel), that help us better manage content on our website by informing us what content is effective. Clear gifs are tiny graphics with a unique identifier, similar in function to cookies, and are used to track the online movements of Web users. In contrast to cookies, which are stored on a user's computer hard drive, clear gifs are embedded invisibly on Web pages and are about the size of the period at the end of this sentence. We sometimes tie in the information gathered by clear gifs to our customers personal information to send mailers.</p>
   									<h2 style="color: #08688F">Flash LSOs</h2>
   										<p>We use local shared objects, also known as Flash cookies, to store your preferences such as volume control or display content based upon what you view on our website to personalize your visit. Third Parties, with whom we partner to provide certain features on our website or to display advertising based upon your Web browsing activity, use Flash cookies to collect and store information. Flash cookies are different from browser cookies because of the amount of, type of, and how data is stored. Cookie management tools provided by your browser will not remove Flash cookies. </p>
   											<h2 style="color: #08688F">3rd Party Tracking</h2>
  												<p>The use of cookies by our partners, affiliates, tracking utility company, service providers is not covered by our Privacy Policy. We do not have access or control over these cookies. Our partners, affiliates, tracking utility company, service providers use session ID and persistent cookies to make it easier for you to navigate our website.</p>
  													<h2 style="color: #08688F">USE AND DISCLOSURE OF INFORMATION</h2>
  														<p>We use the information we gather on the website and our mobile application for the purposes of:</p>
  														<ul>
  															<li>Providing our services </li>
  															<li>Responding to any queries you may have; </li>
  															<li>Operating and improving the website </li>
  															<li>Fostering a positive user experience; </li>
  															<li>For analytical purposes and to research, develop and improve programs, products, services and content; </li>
  															<li>To remove your personal identifiers (your name and e-mail address). In this case, you would no longer be identified as a single unique individual. Once we have de-identified information, it is non-personal information and we may use and treat it like other non-personal information; </li>
  															<li>Contact you regarding any problems or questions we have relating to your use of the website, or, in our discretion, notify you of changes to our Privacy Statement, Terms of Use or other policy or terms that affect you or your use of the website </li>
  															<li>To enforce this Privacy Statement or the Terms of Use</li>
  															<li>To protect our rights or property; </li>
  															<li>To protect someone's health, safety or welfare; or </li>
  															<li>Comply with a law or regulation, court order or other legal process. </li>
  															</ul>
  														<h2 style="color: #08688F">SHARING PERSONAL INFORMATION WITH THIRD PARTIES</h2>
  														<p>Unless otherwise disclosed in this Privacy Policy or at the time you provide your information, FirstJob will only share your personal information with third parties under the following limited circumstances:</p>

													<p>We may share the personal information you provide with the organization to which you are applying for a job, and with any of its suppliers who are involved in the application process. We may also share your personal information with our parent, subsidiary, or affiliated companies (Related Companies) for our or their internal business purposes.</p>
												<p>In the ordinary course of business, we also will share some personal information with companies that we hire to perform services or functions on our behalf. For example, we may use different vendors or suppliers to process your credit card information, to manage our contact us chat service or to ship you products that you order from the website. In such cases those third parties may have access to your personal information in order to provide the services on our behalf and will not use it for any other purposes.</p>
											<p>Some of our pages utilize framing techniques to serve content from our partners while preserving the look and feel of our website. Please be aware that you are providing your personal information to these third parties and not to first job.co.in</p>
										<p>We may co-operate with law enforcement authorities in investigating and prosecuting website users who violate our rules or engage in behavior which is harmful (or illegal) to other users. FirstJob may transfer and disclose information about our users, including personal information, to enforce this Privacy Policy and the other rules about your use of this website, protect our rights or property, protect someone else's safety or welfare, or comply with a law or regulation, court order or other legal process.</p>
									<p>FirstJob reserves the right to disclose and transfer user information, including personal information, in connection with a corporate sale, merger, dissolution, or acquisition, or similar transaction. In such cases, you will be notified via email and/or a prominent notice on our website of any change in ownership or uses of your personal information, as well as any choices you may have regarding your personal information.</p>
								<h2 style="color: #08688F">RETENTION AND YOUR CHOICES AND ABILITY TO ACCESS PERSONAL INFORMATION</h2>
							<p>FirstJob provides individuals reasonable opportunity to access and update their personal information, subject to the qualifications noted below. You have the right to obtain confirmation from FirstJob that we have retained personal information about you, as well as a written description of the nature of that personal information, the purposes for which it is being used, the sources of the personal information and a list of the recipients with whom we have shared your personal information. You also have the right to request the modification or deletion of any personal information retained by FirstJob that is inaccurate. Please contact us at the address appearing below to inquire about your personal information. FirstJob reserves the right to charge a modest fee for providing you access to your personal information.</p>
						<p>Even if you are no longer registered, we will maintain copies of your information in our internal records, systems and databases. We will retain and use your information as necessary to comply with our legal obligations, resolve disputes, and enforce our agreements. We will continue to treat your personal information in accordance with this Privacy Policy.</p>
					<p>If you wish to subscribe to our newsletter(s), we will use your name and email address to send the newsletter to you. Out of respect for your privacy, you may choose to stop receiving our newsletter or marketing emails by following the unsubscribe instructions included in these emails or you can contact us at <a href="mailto:hello@firstjob.co.in">hello@firstjob.co.in</a></p>	
				<h2 style="color: #08688F">LINKS TO OTHER ONLINE RESOURCES</h2>
				<p>This website and mobile application may contain links to other online resources. We provide the links for your convenience, but we do not review, control, or monitor the privacy practices of online resources operated by others. This Privacy Policy does not apply to any other online resources where a link to this Privacy Policy does not appear. We are not responsible for the performance of sites operated by third parties or for your business dealings with them. Therefore, whenever you leave this website we recommend that you review each site's privacy practices and make your own conclusions regarding the adequacy of these practices.</p>
			<h2 style="color: #08688F">BLOG</h2>	
		<p>Our website offers publicly accessible blogs or community forums. The blog is managed by a third party application that may require you to register to post a comment. We do not have access or control of the information posted to the blog. You will need to contact or login into the third party application if you want the personal information that was posted to the comments section removed. To learn how the third party application uses your information, please review their privacy policy.</p>	
			<h2 style="color: #08688F">TESTIMONIALS</h2>
				<p>We display personal testimonials of satisfied customers on our website in addition to other endorsements. With your consent we may post your testimonial along with your name. If you wish to update or delete your testimonial, you can contact us at <a href="mailto:hello@firstjob.co.in">hello@firstjob.co.in</a></p>
					<h2 style="color: #08688F">COLLECTION AND USE OF 3RD PARTY PERSONAL INFORMATION</h2>
						<p>You may also provide personal information about other people, such as their name and email address. This information is only used for the sole purpose of completing your request or for whatever reason it may have been provided.</p>
							<h2 style="color: #08688F">SOCIAL MEDIA WIDGETS</h2>
								<p>Our website includes Social Media Features, such as the Facebook Like button and Widgets, such as the Share this button or interactive mini-programs that run on our website. These Features may collect your IP address, which page you are visiting on our website, and may set a cookie to enable the Feature to function properly. Social Media Features and Widgets are either hosted by a third party or hosted directly on our website. Your interactions with these Features are governed by the privacy policy of the company providing it.</p>
									<h2 style="color: #08688F">CONSENT TO TRANSFER</h2>
										<p>This website is operated in India. If you are located in the United States, European Union, Canada or elsewhere outside of India, please be aware that any information you provide to us will be transferred to India. By using the website or providing us with your information, you consent to this transfer.</p>
											<h2 style="color: #08688F">SECURITY</h2>
												<p>We have implemented commercially reasonable electronic, physical and procedural safeguards designed to help protect against the loss, unauthorized access or disclosure, alteration or destruction of the information under our control. When you enter sensitive information (such as a credit card number) on our order forms, we encrypt the transmission of that information using secure socket layer technology (SSL). Certain features of our websites may require you to use an access code, password or similar unique identifier. You are responsible for protecting the secrecy of your account information. FirstJob recommends that you use the latest browsers so as to take advantage of advances in security technology. Although we strive to protect your encrypted information, no data transmission over the Internet is completely secure. There is some risk associated with any data transmission, including the risk that personal information may be intercepted in transit. Therefore, we cannot guarantee its absolute security. If you have any questions about security on our website, you can contact us at <a href="mailto:hello@firstjob.co.in">hello@firstjob.co.in</a></p>
                          <h2 style="color: #08688F">CHILDREN'S PRIVACY</h2>
                          <p>FirstJob does not knowingly collect or solicit any information from anyone under the age of 16 or knowingly allow such persons to register. The Service and its content are not directed at children under the age of 16. In the event that we learn that we have collected personal information from a child under age 16, we will delete that information as quickly as possible. If you believe that we might have any information from or about a child under 16, please contact us via email.</p>

                          <h2 style="color: #08688F">HOW TO CONTACT US ABOUT A DECEASED USER</h2>
                          <p>In the event of the death of an FirstJob User, please contact us. We will usually conduct our communication via email; should we require any other information, we will contact you at the email address you have provided in your request.</p>

                          <h2 style="color: #08688F">ACCEPTANCE & PRIVACY POLICY CHANGES</h2>
                          <p>By using this website and mobile application, you accept our privacy practices, as outlined in this Privacy Policy. FirstJob reserves the right to modify, alter or otherwise update this Privacy Policy at any time. We will post any new or revised policies on the website. However, FirstJob will use your personal information in a manner consistent with the Privacy Policy in effect at the time you submitted the information, unless you consent to the new or revised policy.</p>

															<h2 style="color: #08688F">CONTACT US</h2>
																<p>If you have questions or concerns about this Privacy Policy, please contact us through the information below:</p>
																 <h2 style="color: #08688F">India</h2>
																 <address>
																		Mr. Ankur Paliwal<br>
																		Deputy Manager<br>
																		Synergy Relationship Management Services P Ltd.<br>
																		C-2/4, Sushant Lok - 1,Gurgaon, Haryana,<br>
																		India - 122002,<br>
                                    Email - <a href="mailto:hello@firstjob.co.in">hello@firstjob.co.in</a>
																		</address>

 





</div>



</body>
</html>

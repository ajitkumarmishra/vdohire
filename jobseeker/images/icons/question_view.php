
    <link rel="stylesheet" href="https://cdn.webrtc-experiment.com/style.css">
	<link rel="stylesheet" href="<?php echo base_url();?>css/progress-circle.css">
	<link rel="stylesheet" href="<?php echo base_url();?>css/progress-circle-small.css">
    <style>
    audio {
        vertical-align: bottom;
        width: 10em;
    }
    video {
        max-width: 100%;
        vertical-align: top;
    }
    input {
        border: 1px solid #d9d9d9;
        border-radius: 1px;
        font-size: 2em;
        margin: .2em;
        width: 30%;
    }
    p,
    .inner {
        padding: 1em;
    }
    li {
        border-bottom: 1px solid rgb(189, 189, 189);
        border-left: 1px solid rgb(189, 189, 189);
        padding: .5em;
    }
    label {
        display: inline-block;
        width: 8em;
    }
    </style>

    <style>
        .recordrtc button {
            font-size: inherit;
        }

        .recordrtc button, .recordrtc select {
            vertical-align: middle;
            line-height: 1;
            padding: 2px 5px;
            height: auto;
            font-size: inherit;
            margin: 0;
        }

        .recordrtc, .recordrtc .header {
            display: block;
            text-align: center;
            padding-top: 0;
        }

        .recordrtc video {
            width: 70%;
        }

        .recordrtc option[disabled] {
            display: none;
        }
    </style>

    <script src="https://cdn.webrtc-experiment.com/RecordRTC.js"></script>
    <script src="https://cdn.webrtc-experiment.com/gif-recorder.js"></script>
    <script src="https://cdn.webrtc-experiment.com/getScreenId.js"></script>

    <!-- for Edige/FF/Chrome/Opera/etc. getUserMedia support -->
    <script src="https://cdn.webrtc-experiment.com/gumadapter.js"></script>
</head>

<body>
    

</body>

</html>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#"><img src="<?php echo base_url(); ?>images/logo.png" rel="stylesheet"></a>
    </div>
      <div class="collapse navbar-collapse pull-right" id="myNavbar" style="margin-right:112px;">
      
    </div>
  </div>
</nav>
<center>
    <div class="large-12 column" id="myModal" style="position: fixed;display:none;top: 400px;left: 700px;z-index:1;">
			  <div id="progress-circle" class="progress-circle progress-0"><span id="percet_text">0</span>
			  </div>
	</div>
	<div class="large-12 column" id="loading" style="position: fixed;display:none;top: 400px;left: 700px;z-index:1;">
			  <img src ="<?php echo base_url()?>images/icons/spinner.gif">
	</div>
		  </center>

    <div class="row" style="    background: #f8f8f8;margin-right: 0px;">
		<div id="tabs" style="background: #054F72;    height: 95px;padding: 0 169px;">
            <center style="color:white;    font-size: 25px;text-align: center;padding: 35px;">
				<div  class="col-md-6 col-lg-6 col-sm-6 pull-left">
					<?php echo $invitationDetails->title ?>
				</div>
				<div  class="col-md-6 col-lg-6 col-sm-6 pull-left">
					<?php echo $invitationDetails->companyName ?>
				</div>
            </center>
        </div>
		
        <div class="container-full" style="background: white;">
            <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #054F72;height: 33px;">
                
            </div>
            <div  class="col-md-8 col-lg-8 col-sm-8 pull-left" style="    font-size: 17px;color: white;font-weight: bold;width: 80%;background: #EB9B2C ;">
				
                <div  class="col-md-1 col-lg-1 col-sm-1 pull-left" style=" padding-right:0px;padding-left:0px;   width: 27%;">
                    <center>
					<div class="large-12 column" id="myModal" style="position: static;display:block;top: 400px;left: 700px;z-index:1;">
					  <div id="progress-circle1" class="progress-circle-small progress-small-0" style="  margin-top:2px;margin-bottom:0px;   width: 49px;height: 49px;">
						<span id="percet_text1" style="font-size: 10px;    margin-top: -13px;margin-left: -13px;line-height: 27px;    width: 25px;height: 25px;">0</span>
					  </div>
					</div>
					</center>
                    <center style="font-size:14px;">Video Interview</center>
                </div>
                <div  class="col-md-1 col-lg-1 col-sm-1 pull-left" style="padding-right:0px;padding-left:0px;       margin-top: 19px;">
                    <center><img src="<?php echo base_url()?>images/icons/arrow.png" style="width:58px"></center>
                </div>
                <div  class="col-md-1 col-lg-1 col-sm-1 pull-left" style=" padding-right:0px;padding-left:0px;      width: 27%;">
					<center>
						<div  class="img-circle" style="background: white;padding: 10px; margin-top:2px;width: 56px;height: 56px;">
							<img src="<?php echo base_url()?>images/icons/multpli-white.png" style="    width: 37px;">
							
						</div>
					</center>
                    <center style="font-size:14px;">Assessments</center>
                </div>
                <div  class="col-md-1 col-lg-1 col-sm-1 pull-left" style="    margin-top: 19px;">
                    <center>
						<img src="<?php echo base_url()?>images/icons/arrow.png" style="width:58px">
					</center>
                    
                </div>
                <div  class="col-md-1 col-lg-1 col-sm-1 pull-left" style="    width: 27%;">
                    
					<center>
						<div  class="img-circle" style=" margin-top:2px;background: white;padding: 10px;width: 56px;height: 56px;">
							<img src="<?php echo base_url()?>images/icons/CV.png" style="    width: 37px;">
							
						</div>
					</center>
                    <center style="font-size:14px;">Upload Resume</center>
                    
                </div>
				
            </div>
            <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #054F72;height: 33px;">
                
            </div>
            
        </div>
        <div style="clear:both"></div>
        <div class="container-full" style="background: white;">
            <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #f8f8f8;height: 30px;">
						
                </div>
            <div  class="col-md-8 col-lg-8 col-sm-8 pull-left" style="border:1px solid lightgrey;padding-bottom: 10px;padding-top: 20px;width: 80%;background: white ;">
                <div class="col-lg-12">
                    <!-- /.row -->
                    <center>
					
							<span style="color:black;font-weight:bold;">Question: </span><span  class="img-circle" style="background:#39a6e9;padding: 10px;    margin: 1rem;width: 56px;height: 56px;">
                                <span id="questionItemNo" style="font-weight:bold;">1</span>

							</span>
						<p style="font-size: 15px;font-weight: bold;" id="QuestionAsPerSynergy">
							Ensure your audio and webcam are working! Don't worry, you are not being recorder yet
						</p>
						<p id="demo" class="pull-right" style="border-top:6px solid #eb9b2c; font-size:18px;border-radius:5px 5px 5px 5px;background: #054f72;color: white;width:113px;"></p>
					</center>
                    
                    
                <!-- /.col-lg-4 -->
					<center>
        

						
						
						<section class="experiment recordrtc"  style="    border: none;display:none">
						<div style="margin-top: 10px;">
							<video id="recording-player" controls muted></video>
						</div>
            <h2 class="header" style="margin-top:0px;border:none;">
                <select class="recording-media" style="display:none;">
                    <option value="record-audio-plus-video">Microphone+Camera</option>
                    <option value="record-audio">Microphone</option>
                    <option value="record-screen">Full Screen</option>
                    <option value="record-audio-plus-screen">Microphone+Screen</option>
                </select>


                <select class="media-container-format" style="display:none;">
                    <option>vp8</option>
                    <option>vp9</option>
                    <option>h264</option>
                    <option>opus</option>
                    <option>pcm</option>
                    <option>gif</option>
                    <option>whammy</option>
                </select>

                <input type="checkbox" id="chk-MultiStreamRecorder" style="margin:0;width:auto; display:none;">


                <button  onclick="IamDone()" id="btn-start-recording" style="    padding: 16px;font-size: 18px;background: #07B1ED;border: none;   margin-left: 46px;margin-top: 10px;margin-right: 50px;">Start Recording</button>
                <div id="btn-pause-recording" style="display: none; font-size: 15px;"></div>
                <select class="media-resolutions" style="display:none;">
                    <option value="default">Default resolutions</option>
                    <option value="1920x1080">1080p</option>
                    <option value="1280x720">720p</option>
                    <option value="3840x2160">4K Ultra HD (3840x2160)</option>
                </select>

                <select class="media-framerates" style="display:none;">
                    <option value="default">Default framerates</option>
                    <option value="5">5 fps</option>
                    <option value="15">15 fps</option>
                    <option value="24">24 fps</option>
                    <option value="30">30 fps</option>
                    <option value="60">60 fps</option>
                </select>

                <select class="media-bitrates" style="display:none;">
                    <option value="default">Default bitrates</option>
                    <option value="8000000000">1 GB bps</option>
                    <option value="800000000">100 MB bps</option>
                    <option value="8000000">1 MB bps</option>
                    <option value="800000">100 KB bps</option>
                    <option value="8000">1 KB bps</option>
                    <option value="800">100 Bytes bps</option>
                </select>
            </h2>

            <div style="text-align: center; display: none;">
                <button id="save-to-disk" style="display: none;">Save To Disk</button>
				<button id="upload-to-php" style="display: none;">Upload to PHP</button>
                <button id="open-new-tab" style="display: none;">Open New Tab</button>
				<button id="upload-to-youtube" style="vertical-align:top;display: none;" >Upload to YouTube</button>
                
            </div>

            
        </section>
							<!-- /.row -->
					</center>
					<section class="answerButton" style="    border: none;display:block;    margin-top: 9%;">
						<center>
								<button onclick="answerNow()" type="button" class="btn btn-primary" style="    padding: 16px;font-size: 18px;background: #07B1ED;border: none;   margin-left: 10px;margin-top: 10px;margin-right: 50px;">ANSWER NOW</button>
						</center>
					</section>
                <!-- /.col-lg-8 -->
                </div>   
            <!-- /.row -->
                
            <!-- /.row -->

                </div>

            <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #f8f8f8;height: 30px;">

                </div>

        </div>
        
        <?php
        
        ?>
        
    <!-- /#page-wrapper -->
    </div>
</div>
<!-- /#wrapper -->



<script src="<?php echo base_url(); ?>theme/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url(); ?>theme/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>theme/js/bootbox.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->

    <!-- Custom Theme JavaScript -->
    <script src="http://formvalidation.io/vendor/formvalidation/js/formValidation.min.js"></script>
<script src="http://formvalidation.io/vendor/formvalidation/js/framework/bootstrap.min.js"></script>
<script>

jQuery(function () {

  $(document).keydown(function (evt) {
    if (evt.keyCode == 116) { // down arrow
       alert("Your interview will be cancelled"); // prevents the usual scrolling behaviour
       window.location="<?php echo base_url()?>users";
    }
  });
  window.onbeforeunload = function (e) {
		e = e || window.event;

		// For IE and Firefox prior to version 4
		if (e) {
			e.preventDefault();
			e.stopPropagation();
			e.returnValue = 'Sure?';
		}
		e.preventDefault();
		e.stopPropagation();
		window.location="<?php echo base_url()?>users";
		// For Safari
		return 'Sure?';
                
		};

	});
</script>

<script>
// Set the date we're counting down to
	var seconds = 10;

// Update the count down every 1 second
	var x = setInterval(function() {

    // Get todays date and time
    var now = new Date().getTime();
	if(seconds <= 0){
		document.getElementById("demo").innerHTML = "0 sec";
	}else{
		seconds --;
		var hour = seconds/(60*60);
		var mins = seconds/(60);
		
		// Time calculations for days, hours, minutes and seconds
		
		
		// Output the result in an element with id="demo"
		document.getElementById("demo").innerHTML = seconds + " sec";
    }
    // If the count down is over, write some text 
    
}, 1000);
</script>

<script type="text/javascript">

</script>
<script>
var nextQuestion = 0;
var QuestionArray = [];
var TotalQuestion = 0;
var counter = 0;
var jobCode = "<?php echo $_SESSION['jobCode']; ?>";
var interviewSet = "<?php echo $_SESSION['interviewSet']; ?>";
var ActiveQuestionId = 0;
var timeLimit = 0;
var MyTimer = "";
var answerNowTimer = "";
var UploadTimerLimit = '';
$(document).ready(function(){
    $.fn.serializeObject = function()
    {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
    

});





$(document).ready(function() {
	var methodname = 'interviewQuestionListInvite';
	var tokenUser = '<?php echo $_SESSION['userId'] ?>';
	var fjCode = '<?php echo $_SESSION['jobCode']?>';
	var js_obj = {token: 'ecbcd7eaee29848978134beeecdfbc7c',methodname: methodname,userId:tokenUser, jobCode:fjCode};
	var encoded = JSON.stringify( js_obj );
	var data= encoded;
	$.ajax({
		  url : "<?php echo base_url(); ?>Fjapi",
		  data: data,
		  type: "POST",
		  success: function(response){
				item = $.parseJSON(response);
				if(item.code == "200"){
					
					var setId = item.data[0].setID;
					
					
					for(var i = 0; i < item.data.length; i++)
					{	QuestionArray.push(item.data[i].questionID);
						if(i == 0){
							nextQuestion = item.data[i].questionID;
							showQuestionNext();
						}
					   //console.log("Type: " + item.data[i].setID + " Name: " + item.data[i].setID + " Account: " + item.data[i].setID);
							/**/
					}
					TotalQuestion = QuestionArray.length;
					
				}else{
					//alert(item.message);
				}
		  }
	});
	
});

function checkForTheAssessment(){
	$("#myModal").css("display","none");
	$("#loading").css("display","block");
	var jobCode = '<?php echo $_SESSION['jobCode']?>';
	var methodname = 'assessmentQuestionList_v2';
	var tokenUser = '<?php echo $_SESSION['userId'] ?>';
	var js_obj = {token: 'ecbcd7eaee29848978134beeecdfbc7c',methodname: methodname,jobCode:jobCode,userId:tokenUser};

	var encoded = JSON.stringify( js_obj );

	var data= encoded;
	$.ajax({
		  url : "<?php echo base_url(); ?>Fjapi",
		  data: data,
		  type: "POST",
		  success: function(response){
				item = $.parseJSON(response);
				if(item.code == "200"){
					$("#loading").css("display","none");
					$("#cover").css("display","none");
					  window.onbeforeunload = null;
					window.location="<?php echo base_url()?>quidelines";
				}else{
					$("#loading").css("display","none");
					$("#cover").css("display","none");
					  window.onbeforeunload = null;
					window.location="<?php echo base_url()?>uploadResume";
				}
		  }
	});
}

function answerNow(){
	seconds = UploadTimerLimit;
	$(".recordrtc").show();
	$(".answerButton").hide();
	
	startRecording1();
	clearTimeout(answerNowTimer);
}

function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}

function showQuestionNext(){
	var methodname = 'interviewQuestionDetailInvite';
	var tokenUser = '<?php echo $_SESSION['userId'] ?>';
	var interviewSet = '<?php echo $_SESSION['interviewSet'] ?>';
	ActiveQuestionId = QuestionArray[counter];
	seconds = 9;
	var js_obj = {token: 'ecbcd7eaee29848978134beeecdfbc7c',methodname: methodname,userId:tokenUser, jobCode:jobCode,interviewSet:interviewSet, questionId:ActiveQuestionId};

	var encoded = JSON.stringify( js_obj );

	var data= encoded;
	$.ajax({
		  url : "<?php echo base_url(); ?>Fjapi",
		  data: data,
		  type: "POST",
		  success: function(response){
				item = $.parseJSON(response);
				if(item.code == "200"){
					var obj = JSON.parse(response);
					var questionId = JSON.stringify(obj.data[0].questionId);
					ActiveQuestionId = questionId.replace(/\"/g, "");
					
					var title = JSON.stringify(obj.data[0].title);
					title = title.replace(/\"/g, "");
					timeLimit = JSON.stringify(obj.data[0].duration);
					timeLimit = timeLimit.replace(/\"/g, "");
					seconds = 9;
					var duration = JSON.stringify(obj.data[0].duration);
					var questionNo = counter+1;
					$("#questionItemNo").text(questionNo+"/"+TotalQuestion);
					$("#QuestionAsPerSynergy").text(title);
					$("#myModal").css("display","none");
					$("#cover").css("display","none");
					var percentComplete = 100/TotalQuestion*(counter);
					percentComplete = Math.ceil(percentComplete);
					$("#progress-circle1").attr("class","progress-circle-small progress-"+percentComplete);
                    $("#percet_text1").text(percentComplete);
					$("#demo").css("display","block");
					answerNowTimer = window.setTimeout(answerNow, 9000);
					UploadTimerLimit = timeLimit;
					MyTimer = window.setTimeout(IamDone, (timeLimit*1000)+9000);
					
				}else{
					//alert(item.message);
				}
		  }
	});
}
</script>

<!-- video rtc-->
	
 <script>
            (function() {
                var params = {},
                    r = /([^&=]+)=?([^&]*)/g;

                function d(s) {
                    return decodeURIComponent(s.replace(/\+/g, ' '));
                }

                var match, search = window.location.search;
                while (match = r.exec(search.substring(1))) {
                    params[d(match[1])] = d(match[2]);

                    if(d(match[2]) === 'true' || d(match[2]) === 'false') {
                        params[d(match[1])] = d(match[2]) === 'true' ? true : false;
                    }
                }

                window.params = params;
            })();

            function addStreamStopListener(stream, callback) {
                var streamEndedEvent = 'ended';

                if ('oninactive' in stream) {
                    streamEndedEvent = 'inactive';
                }

                stream.addEventListener(streamEndedEvent, function() {
                    callback();
                    callback = function() {};
                }, false);

                stream.getAudioTracks().forEach(function(track) {
                    track.addEventListener(streamEndedEvent, function() {
                        callback();
                        callback = function() {};
                    }, false);
                });

                stream.getVideoTracks().forEach(function(track) {
                    track.addEventListener(streamEndedEvent, function() {
                        callback();
                        callback = function() {};
                    }, false);
                });
            }
        </script>
<script>
            var recordingPlayer = document.querySelector('#recording-player');
            var recordingMedia = document.querySelector('.recording-media');
            var mediaContainerFormat = document.querySelector('.media-container-format');
            var mimeType = 'video/webm';
            var fileExtension = 'webm';
            var type = 'video';
            var recorderType;
            var defaultWidth;
            var defaultHeight;
			var button = "";
			var EncodedContent = "";
			var fileName = "";
			var chunkSize = 1024*1024;
			var tokenUser = '<?php echo $_SESSION['userId'] ?>';
			var fileNameArray = [];
			
            var btnStartRecording = document.querySelector('#btn-start-recording');

            window.onbeforeunload = function() {
                btnStartRecording.disabled = false;
                recordingMedia.disabled = false;
                mediaContainerFormat.disabled = false;
            };
			
			function IamDone(){
				button = btnStartRecording;

                if(button.innerHTML === 'I AM DONE') {
                    btnPauseRecording.style.display = 'none';
                    button.disabled = true;
                    button.disableStateWaiting = true;
                    setTimeout(function() {
                        button.disabled = false;
                        button.disableStateWaiting = false;
                    }, 2000);

                    button.innerHTML = 'Star Recording';

                    function stopStream() {
                        if(button.stream && button.stream.stop) {
                            button.stream.stop();
                            button.stream = null;
                        }

                        if(button.stream instanceof Array) {
                            button.stream.forEach(function(stream) {
                                stream.stop();
                            });
                            button.stream = null;
                        }

                        videoBitsPerSecond = null;
                    }

                    if(button.recordRTC) {
                        if(button.recordRTC.length) {
                            button.recordRTC[0].stopRecording(function(url) {
                                if(!button.recordRTC[1]) {
                                    button.recordingEndedCallback(url);
                                    stopStream();

                                    saveToDiskOrOpenNewTab(button.recordRTC[0]);
                                    return;
                                }

                                button.recordRTC[1].stopRecording(function(url) {
                                    button.recordingEndedCallback(url);
                                    stopStream();
                                });
                            });
                        }
                        else {
                            button.recordRTC.stopRecording(function(url) {
                                button.recordingEndedCallback(url);
                                stopStream();

                                saveToDiskOrOpenNewTab(button.recordRTC);
                            });
                        }
                    }

                    return;
                }

                if(!event) return;

                button.disabled = true;

                var commonConfig = {
                    onMediaCaptured: function(stream) {
                        button.stream = stream;
                        if(button.mediaCapturedCallback) {
                            button.mediaCapturedCallback();
                        }

                        button.innerHTML = 'I AM DONE';
                        button.disabled = false;
                    },
                    onMediaStopped: function() {
                        button.innerHTML = 'Start Recording';

                        if(!button.disableStateWaiting) {
                            button.disabled = false;
                        }
                    },
                    onMediaCapturingFailed: function(error) {
                        if(error.name === 'PermissionDeniedError' && !!navigator.mozGetUserMedia) {
                            InstallTrigger.install({
                                'Foo': {
                                    // URL: 'https://addons.mozilla.org/en-US/firefox/addon/enable-screen-capturing/',
                                    URL: 'https://addons.cdn.mozilla.net/user-media/addons/655146/enable_screen_capturing_in_firefox-1.1.001-fx.xpi?filehash=sha256%3Acb13851aaca148fcbd6672fc2dddfb9653c52a529588fb27ad018e834fbb3099',
                                    toString: function() {
                                        return this.URL;
                                    }
                                }
                            });
                        }

                        commonConfig.onMediaStopped();
                    }
                };

                if(mediaContainerFormat.value === 'h264') {
                    mimeType = 'video/webm\;codecs=h264';
                    fileExtension = 'mp4';

                    if(typeof MediaRecorder.isTypeSupported === 'function') {
                        if(MediaRecorder.isTypeSupported('video/mpeg')) {
                            mimeType = 'video/mpeg';
                        }
                        else {
                            mimeType = 'video/webm\;codecs=h264';
                        }
                    }
                }

                if(mediaContainerFormat.value === 'vp8') {
                    mimeType = 'video/webm\;codecs=vp8';
                    fileExtension = 'webm';
                    recorderType = null;
                    type = 'video';
                }

                if(mediaContainerFormat.value === 'vp9') {
                    mimeType = 'video/webm\;codecs=vp9';
                    fileExtension = 'webm';
                    recorderType = null;
                    type = 'video';
                }

                if(mediaContainerFormat.value === 'pcm') {
                    mimeType = 'audio/wav';
                    fileExtension = 'wav';
                    recorderType = StereoAudioRecorder;
                    type = 'audio';
                }

                if(mediaContainerFormat.value === 'opus') {
                    mimeType = 'audio/webm';
                    fileExtension = 'webm'; // ogg or webm?
                    recorderType = null;
                    type = 'audio';
                }

                if(mediaContainerFormat.value === 'whammy') {
                    mimeType = 'video/webm';
                    fileExtension = 'webm';
                    recorderType = WhammyRecorder;
                    type = 'video';
                }

                if(mediaContainerFormat.value === 'gif') {
                    mimeType = 'image/gif';
                    fileExtension = 'gif';
                    recorderType = GifRecorder;
                    type = 'gif';
                }

                if(recordingMedia.value === 'record-audio') {
                    captureAudio(commonConfig);

                    button.mediaCapturedCallback = function() {
                        var options = {
                            type: type,
                            mimeType: mimeType,
                            leftChannel: params.leftChannel || false,
                            disableLogs: params.disableLogs || false
                        };

                        if(params.sampleRate) {
                            options.sampleRate = parseInt(params.sampleRate);
                        }

                        if(params.bufferSize) {
                            options.bufferSize = parseInt(params.bufferSize);
                        }

                        if(recorderType) {
                            options.recorderType = recorderType;
                        }

                        if(videoBitsPerSecond) {
                            options.videoBitsPerSecond = videoBitsPerSecond;
                        }

                        if(webrtcDetectedBrowser === 'edge') {
                            options.numberOfAudioChannels = 1;
                        }

                        button.recordRTC = RecordRTC(button.stream, options);

                        button.recordingEndedCallback = function(url) {
                            setVideoURL(url);
                        };

                        button.recordRTC.startRecording();
                        btnPauseRecording.style.display = '';
                    };
                }

                if(recordingMedia.value === 'record-audio-plus-video') {
                    captureAudioPlusVideo(commonConfig);

                    button.mediaCapturedCallback = function() {
                        if(typeof MediaRecorder === 'undefined') { // opera or chrome etc.
                            button.recordRTC = [];

                            if(!params.bufferSize) {
                                // it fixes audio issues whilst recording 720p
                                params.bufferSize = 16384;
                            }

                            var options = {
                                type: 'audio', // hard-code to set "audio"
                                leftChannel: params.leftChannel || false,
                                disableLogs: params.disableLogs || false,
                                video: recordingPlayer
                            };

                            if(params.sampleRate) {
                                options.sampleRate = parseInt(params.sampleRate);
                            }

                            if(params.bufferSize) {
                                options.bufferSize = parseInt(params.bufferSize);
                            }

                            if(params.frameInterval) {
                                options.frameInterval = parseInt(params.frameInterval);
                            }

                            if(recorderType) {
                                options.recorderType = recorderType;
                            }

                            if(videoBitsPerSecond) {
                                options.videoBitsPerSecond = videoBitsPerSecond;
                            }

                            if(chkMultiStreamRecorder.checked === true) {
                                options.previewStream = function(previewStream) {
                                    setVideoURL(previewStream, true);
                                };
                            }

                            var audioRecorder = RecordRTC(button.stream, options);

                            options.type = type;
                            var videoRecorder = RecordRTC(button.stream, options);

                            // to sync audio/video playbacks in browser!
                            videoRecorder.initRecorder(function() {
                                audioRecorder.initRecorder(function() {
                                    audioRecorder.startRecording();
                                    videoRecorder.startRecording();
                                    btnPauseRecording.style.display = '';
                                });
                            });

                            button.recordRTC.push(audioRecorder, videoRecorder);

                            button.recordingEndedCallback = function() {
                                var audio = new Audio();
                                audio.src = audioRecorder.toURL();
                                audio.controls = false;
                                audio.autoplay = false;

                                recordingPlayer.parentNode.appendChild(document.createElement('hr'));
                                recordingPlayer.parentNode.appendChild(audio);

                                if(audio.paused) audio.play();
                            };
                            return;
                        }

                        var options = {
                            type: type,
                            mimeType: mimeType,
                            disableLogs: params.disableLogs || false,
                            getNativeBlob: false, // enable it for longer recordings
                            video: recordingPlayer
                        };

                        if(recorderType) {
                            options.recorderType = recorderType;

                            if(recorderType == WhammyRecorder || recorderType == GifRecorder) {
                                options.canvas = options.video = {
                                    width: defaultWidth || 320,
                                    height: defaultHeight || 240
                                };
                            }
                        }

                        if(videoBitsPerSecond) {
                            options.videoBitsPerSecond = videoBitsPerSecond;
                        }

                        if(chkMultiStreamRecorder.checked === true) {
                            options.previewStream = function(previewStream) {
                                setVideoURL(previewStream, true);
                            };

                            var width = 320;
                            var height = 240;

                            var select = document.querySelector('.media-resolutions');
                            var value = select.value;

                            if(value != 'default') {
                                value = value.split('x');

                                if(value.length == 2) {
                                    width = parseInt(value[0]);
                                    height = parseInt(value[1]);
                                }
                            }

                            options.video = {
                                width: width,
                                height: height
                            };
                        }

                        button.recordRTC = RecordRTC(button.stream, options);

                        button.recordingEndedCallback = function(url) {
                            setVideoURL(url);
                        };

                        button.recordRTC.startRecording();
                        btnPauseRecording.style.display = '';
                    };
                }

                if(recordingMedia.value === 'record-screen') {
                    captureScreen(commonConfig);

                    button.mediaCapturedCallback = function() {
                        var options = {
                            type: type,
                            mimeType: mimeType,
                            disableLogs: params.disableLogs || false,
                            getNativeBlob: false, // enable it for longer recordings
                            video: recordingPlayer
                        };

                        if(recorderType) {
                            options.recorderType = recorderType;

                            if(recorderType == WhammyRecorder || recorderType == GifRecorder) {
                                options.canvas = options.video = {
                                    width: defaultWidth || 320,
                                    height: defaultHeight || 240
                                };
                            }
                        }

                        if(videoBitsPerSecond) {
                            options.videoBitsPerSecond = videoBitsPerSecond;
                        }

                        button.recordRTC = RecordRTC(button.stream, options);

                        button.recordingEndedCallback = function(url) {
                            setVideoURL(url);
                        };

                        button.recordRTC.startRecording();
                        btnPauseRecording.style.display = '';
                    };
                }

                // note: audio+tab is supported in Chrome 50+
                // todo: add audio+tab recording
                if(recordingMedia.value === 'record-audio-plus-screen') {
                    captureAudioPlusScreen(commonConfig);

                    button.mediaCapturedCallback = function() {
                        var options = {
                            type: type,
                            mimeType: mimeType,
                            disableLogs: params.disableLogs || false,
                            getNativeBlob: false, // enable it for longer recordings
                            video: recordingPlayer
                        };

                        if(recorderType) {
                            options.recorderType = recorderType;

                            if(recorderType == WhammyRecorder || recorderType == GifRecorder) {
                                options.canvas = options.video = {
                                    width: defaultWidth || 320,
                                    height: defaultHeight || 240
                                };
                            }
                        }

                        if(videoBitsPerSecond) {
                            options.videoBitsPerSecond = videoBitsPerSecond;
                        }

                        button.recordRTC = RecordRTC(button.stream, options);

                        button.recordingEndedCallback = function(url) {
                            setVideoURL(url);
                        };

                        button.recordRTC.startRecording();
                        btnPauseRecording.style.display = '';
                    };
                }
            };
			
			
			function startRecording1(){
				var button = btnStartRecording;
				
                button.disabled = true;

                var commonConfig = {
                    onMediaCaptured: function(stream) {
                        button.stream = stream;
                        if(button.mediaCapturedCallback) {
                            button.mediaCapturedCallback();
                        }

                        button.innerHTML = 'I AM DONE';
                        button.disabled = false;
                    },
                    onMediaStopped: function() {
                        button.innerHTML = 'I AM DONE';

                        if(!button.disableStateWaiting) {
                            button.disabled = false;
                        }
                    },
                    onMediaCapturingFailed: function(error) {
                        if(error.name === 'PermissionDeniedError' && !!navigator.mozGetUserMedia) {
                            InstallTrigger.install({
                                'Foo': {
                                    // URL: 'https://addons.mozilla.org/en-US/firefox/addon/enable-screen-capturing/',
                                    URL: 'https://addons.cdn.mozilla.net/user-media/addons/655146/enable_screen_capturing_in_firefox-1.1.001-fx.xpi?filehash=sha256%3Acb13851aaca148fcbd6672fc2dddfb9653c52a529588fb27ad018e834fbb3099',
                                    toString: function() {
                                        return this.URL;
                                    }
                                }
                            });
                        }

                        commonConfig.onMediaStopped();
                    }
                };

                if(mediaContainerFormat.value === 'h264') {
                    mimeType = 'video/webm\;codecs=h264';
                    fileExtension = 'mp4';

                    if(typeof MediaRecorder.isTypeSupported === 'function') {
                        if(MediaRecorder.isTypeSupported('video/mpeg')) {
                            mimeType = 'video/mpeg';
                        }
                        else {
                            mimeType = 'video/webm\;codecs=h264';
                        }
                    }
                }

                if(mediaContainerFormat.value === 'vp8') {
                    mimeType = 'video/webm\;codecs=vp8';
                    fileExtension = 'webm';
                    recorderType = null;
                    type = 'video';
                }

                if(mediaContainerFormat.value === 'vp9') {
                    mimeType = 'video/webm\;codecs=vp9';
                    fileExtension = 'webm';
                    recorderType = null;
                    type = 'video';
                }

                if(mediaContainerFormat.value === 'pcm') {
                    mimeType = 'audio/wav';
                    fileExtension = 'wav';
                    recorderType = StereoAudioRecorder;
                    type = 'audio';
                }

                if(mediaContainerFormat.value === 'opus') {
                    mimeType = 'audio/webm';
                    fileExtension = 'webm'; // ogg or webm?
                    recorderType = null;
                    type = 'audio';
                }

                if(mediaContainerFormat.value === 'whammy') {
                    mimeType = 'video/webm';
                    fileExtension = 'webm';
                    recorderType = WhammyRecorder;
                    type = 'video';
                }

                if(mediaContainerFormat.value === 'gif') {
                    mimeType = 'image/gif';
                    fileExtension = 'gif';
                    recorderType = GifRecorder;
                    type = 'gif';
                }

                if(recordingMedia.value === 'record-audio') {
                    captureAudio(commonConfig);

                    button.mediaCapturedCallback = function() {
                        var options = {
                            type: type,
                            mimeType: mimeType,
                            leftChannel: params.leftChannel || false,
                            disableLogs: params.disableLogs || false
                        };

                        if(params.sampleRate) {
                            options.sampleRate = parseInt(params.sampleRate);
                        }

                        if(params.bufferSize) {
                            options.bufferSize = parseInt(params.bufferSize);
                        }

                        if(recorderType) {
                            options.recorderType = recorderType;
                        }

                        if(videoBitsPerSecond) {
                            options.videoBitsPerSecond = videoBitsPerSecond;
                        }

                        if(webrtcDetectedBrowser === 'edge') {
                            options.numberOfAudioChannels = 1;
                        }

                        button.recordRTC = RecordRTC(button.stream, options);

                        button.recordingEndedCallback = function(url) {
                            setVideoURL(url);
                        };

                        button.recordRTC.startRecording();
                        btnPauseRecording.style.display = '';
                    };
                }

                if(recordingMedia.value === 'record-audio-plus-video') {
                    captureAudioPlusVideo(commonConfig);
					
                    button.mediaCapturedCallback = function() {
                        if(typeof MediaRecorder === 'undefined') { // opera or chrome etc.
                            button.recordRTC = [];

                            if(!params.bufferSize) {
                                // it fixes audio issues whilst recording 720p
                                params.bufferSize = 16384;
                            }

                            var options = {
                                type: 'audio', // hard-code to set "audio"
                                leftChannel: params.leftChannel || false,
                                disableLogs: params.disableLogs || false,
                                video: recordingPlayer
                            };

                            if(params.sampleRate) {
                                options.sampleRate = parseInt(params.sampleRate);
                            }

                            if(params.bufferSize) {
                                options.bufferSize = parseInt(params.bufferSize);
                            }

                            if(params.frameInterval) {
                                options.frameInterval = parseInt(params.frameInterval);
                            }

                            if(recorderType) {
                                options.recorderType = recorderType;
                            }

                            if(videoBitsPerSecond) {
                                options.videoBitsPerSecond = videoBitsPerSecond;
                            }

                            if(chkMultiStreamRecorder.checked === true) {
                                options.previewStream = function(previewStream) {
                                    setVideoURL(previewStream, true);
                                };
                            }

                            var audioRecorder = RecordRTC(button.stream, options);

                            options.type = type;
                            var videoRecorder = RecordRTC(button.stream, options);

                            // to sync audio/video playbacks in browser!
                            videoRecorder.initRecorder(function() {
                                audioRecorder.initRecorder(function() {
                                    audioRecorder.startRecording();
                                    videoRecorder.startRecording();
                                    btnPauseRecording.style.display = '';
                                });
                            });

                            button.recordRTC.push(audioRecorder, videoRecorder);

                            button.recordingEndedCallback = function() {
                                var audio = new Audio();
                                audio.src = audioRecorder.toURL();
                                audio.controls = false;
                                audio.autoplay = false;

                                recordingPlayer.parentNode.appendChild(document.createElement('hr'));
                                recordingPlayer.parentNode.appendChild(audio);

                                if(audio.paused) audio.play();
                            };
                            return;
                        }

                        var options = {
                            type: type,
                            mimeType: mimeType,
                            disableLogs: params.disableLogs || false,
                            getNativeBlob: false, // enable it for longer recordings
                            video: recordingPlayer
                        };

                        if(recorderType) {
                            options.recorderType = recorderType;

                            if(recorderType == WhammyRecorder || recorderType == GifRecorder) {
                                options.canvas = options.video = {
                                    width: defaultWidth || 320,
                                    height: defaultHeight || 240
                                };
                            }
                        }

                        if(videoBitsPerSecond) {
                            options.videoBitsPerSecond = videoBitsPerSecond;
                        }

                        if(chkMultiStreamRecorder.checked === true) {
                            options.previewStream = function(previewStream) {
                                setVideoURL(previewStream, true);
                            };

                            var width = 320;
                            var height = 240;

                            var select = document.querySelector('.media-resolutions');
                            var value = select.value;

                            if(value != 'default') {
                                value = value.split('x');

                                if(value.length == 2) {
                                    width = parseInt(value[0]);
                                    height = parseInt(value[1]);
                                }
                            }

                            options.video = {
                                width: width,
                                height: height
                            };
                        }

                        button.recordRTC = RecordRTC(button.stream, options);

                        button.recordingEndedCallback = function(url) {
                            setVideoURL(url);
                        };

                        button.recordRTC.startRecording();
                        btnPauseRecording.style.display = '';
                    };
                }

                if(recordingMedia.value === 'record-screen') {
                    captureScreen(commonConfig);

                    button.mediaCapturedCallback = function() {
                        var options = {
                            type: type,
                            mimeType: mimeType,
                            disableLogs: params.disableLogs || false,
                            getNativeBlob: false, // enable it for longer recordings
                            video: recordingPlayer
                        };

                        if(recorderType) {
                            options.recorderType = recorderType;

                            if(recorderType == WhammyRecorder || recorderType == GifRecorder) {
                                options.canvas = options.video = {
                                    width: defaultWidth || 320,
                                    height: defaultHeight || 240
                                };
                            }
                        }

                        if(videoBitsPerSecond) {
                            options.videoBitsPerSecond = videoBitsPerSecond;
                        }

                        button.recordRTC = RecordRTC(button.stream, options);

                        button.recordingEndedCallback = function(url) {
                            setVideoURL(url);
                        };

                        button.recordRTC.startRecording();
                        btnPauseRecording.style.display = '';
                    };
                }

                // note: audio+tab is supported in Chrome 50+
                // todo: add audio+tab recording
                if(recordingMedia.value === 'record-audio-plus-screen') {
                    captureAudioPlusScreen(commonConfig);

                    button.mediaCapturedCallback = function() {
                        var options = {
                            type: type,
                            mimeType: mimeType,
                            disableLogs: params.disableLogs || false,
                            getNativeBlob: false, // enable it for longer recordings
                            video: recordingPlayer
                        };

                        if(recorderType) {
                            options.recorderType = recorderType;

                            if(recorderType == WhammyRecorder || recorderType == GifRecorder) {
                                options.canvas = options.video = {
                                    width: defaultWidth || 320,
                                    height: defaultHeight || 240
                                };
                            }
                        }

                        if(videoBitsPerSecond) {
                            options.videoBitsPerSecond = videoBitsPerSecond;
                        }

                        button.recordRTC = RecordRTC(button.stream, options);

                        button.recordingEndedCallback = function(url) {
                            setVideoURL(url);
                        };

                        button.recordRTC.startRecording();
                        btnPauseRecording.style.display = '';
                    };
                }
            };
			
            

            function captureVideo(config) {
                captureUserMedia({video: true}, function(videoStream) {
                    config.onMediaCaptured(videoStream);

                    addStreamStopListener(videoStream, function() {
                        config.onMediaStopped();
                    });
                }, function(error) {
                    config.onMediaCapturingFailed(error);
                });
            }

            function captureAudio(config) {
                captureUserMedia({audio: true}, function(audioStream) {
                    config.onMediaCaptured(audioStream);

                    addStreamStopListener(audioStream, function() {
                        config.onMediaStopped();
                    });
                }, function(error) {
                    config.onMediaCapturingFailed(error);
                });
            }

            function captureAudioPlusVideo(config) {
                captureUserMedia({video: true, audio: true}, function(audioVideoStream) {
                    config.onMediaCaptured(audioVideoStream);

                    if(audioVideoStream instanceof Array) {
                        audioVideoStream.forEach(function(stream) {
                            addStreamStopListener(stream, function() {
                                config.onMediaStopped();
                            });
                        });
                        return;
                    }

                    addStreamStopListener(audioVideoStream, function() {
                        config.onMediaStopped();
                    });
                }, function(error) {
                    config.onMediaCapturingFailed(error);
                });
            }

            function captureScreen(config) {
                window.getScreenId = function(chromeMediaSource, chromeMediaSourceId) {
                    var screenConstraints = {
                        audio: false,
                        video: {
                            mandatory: {
                                chromeMediaSourceId: chromeMediaSourceId,
                                chromeMediaSource: chromeMediaSource
                            }
                        }
                    };

                    if(webrtcDetectedBrowser == 'firefox') {
                        screenConstraints = {
                            video: {
                                mediaSource: 'window'
                            }
                        }
                    }

                    captureUserMedia(screenConstraints, function(screenStream) {
                        config.onMediaCaptured(screenStream);

                        addStreamStopListener(screenStream, function() {
                            // config.onMediaStopped();

                            //btnStartRecording.onclick();
                        });
                    }, function(error) {
                        config.onMediaCapturingFailed(error);
                    });
                };

                if(webrtcDetectedBrowser == 'firefox') {
                    window.getScreenId();
                }

                window.postMessage('get-sourceId', '*');
            }

            function captureAudioPlusScreen(config) {
                window.getScreenId = function(chromeMediaSource, chromeMediaSourceId) {
                    var screenConstraints = {
                        audio: false,
                        video: {
                            mandatory: {
                                chromeMediaSourceId: chromeMediaSourceId,
                                chromeMediaSource: chromeMediaSource
                            }
                        }
                    };

                    if(webrtcDetectedBrowser == 'firefox') {
                        screenConstraints = {
                            video: {
                                mediaSource: 'window',
                            },
                            audio: true
                        }
                    }

                    captureUserMedia(screenConstraints, function(screenStream) {
                        if(webrtcDetectedBrowser == 'firefox') {
                            config.onMediaCaptured(screenStream);

                            addStreamStopListener(screenStream, function() {
                                config.onMediaStopped();
                            });
                            return;
                        }

                        captureUserMedia({audio: true}, function(audioStream) {
                            // merge audio tracks into the screen
                            screenStream.addTrack(audioStream.getAudioTracks()[0]);

                            config.onMediaCaptured(screenStream);

                            addStreamStopListener(screenStream, function() {
                                config.onMediaStopped();
                            });
                        }, function(error) {
                            config.onMediaCapturingFailed(error);
                        });
                    }, function(error) {
                        config.onMediaCapturingFailed(error);
                    });
                };

                if(webrtcDetectedBrowser == 'firefox') {
                    window.getScreenId();
                }

                window.postMessage('get-sourceId', '*');
            }

            var videoBitsPerSecond;

            function setVideoBitrates() {
                var select = document.querySelector('.media-bitrates');
                var value = select.value;

                if(value == 'default') {
                    videoBitsPerSecond = null;
                    return;
                }

                videoBitsPerSecond = parseInt(value);
            }

            function getFrameRates(mediaConstraints) {
                if(!mediaConstraints.video) {
                    return mediaConstraints;
                }

                var select = document.querySelector('.media-framerates');
                var value = select.value;

                if(value == 'default') {
                    return mediaConstraints;
                }

                value = parseInt(value);

                if(webrtcDetectedBrowser == 'firefox') {
                    mediaConstraints.video.frameRate = value;
                    return mediaConstraints;
                }

                if(!mediaConstraints.video.mandatory) {
                    mediaConstraints.video.mandatory = {};
                    mediaConstraints.video.optional = [];
                }

                var isScreen = recordingMedia.value.toString().toLowerCase().indexOf('screen') != -1;
                if(isScreen) {
                    mediaConstraints.video.mandatory.maxFrameRate = value;
                }
                else {
                    mediaConstraints.video.mandatory.minFrameRate = value;
                }

                return mediaConstraints;
            }

            function setGetFromLocalStorage(selectors) {
                selectors.forEach(function(selector) {
                    var storageItem = selector.replace(/\.|#/g, '');
                    if(localStorage.getItem(storageItem)) {
                        document.querySelector(selector).value = localStorage.getItem(storageItem);
                    }

                    addEventListenerToUploadLocalStorageItem(selector, ['change', 'blur'], function() {
                        localStorage.setItem(storageItem, document.querySelector(selector).value);
                    });
                });
            }

            function addEventListenerToUploadLocalStorageItem(selector, arr, callback) {
                arr.forEach(function(event) {
                    document.querySelector(selector).addEventListener(event, callback, false);
                });
            }

            setGetFromLocalStorage(['.media-resolutions', '.media-framerates', '.media-bitrates', '.recording-media', '.media-container-format']);

            function getVideoResolutions(mediaConstraints) {
                if(!mediaConstraints.video) {
                    return mediaConstraints;
                }

                var select = document.querySelector('.media-resolutions');
                var value = select.value;

                if(value == 'default') {
                    return mediaConstraints;
                }

                value = value.split('x');

                if(value.length != 2) {
                    return mediaConstraints;
                }

                defaultWidth = parseInt(value[0]);
                defaultHeight = parseInt(value[1]);

                if(webrtcDetectedBrowser == 'firefox') {
                    mediaConstraints.video.width = defaultWidth;
                    mediaConstraints.video.height = defaultHeight;
                    return mediaConstraints;
                }

                if(!mediaConstraints.video.mandatory) {
                    mediaConstraints.video.mandatory = {};
                    mediaConstraints.video.optional = [];
                }

                var isScreen = recordingMedia.value.toString().toLowerCase().indexOf('screen') != -1;

                if(isScreen) {
                    mediaConstraints.video.mandatory.maxWidth = defaultWidth;
                    mediaConstraints.video.mandatory.maxHeight = defaultHeight;
                }
                else {
                    mediaConstraints.video.mandatory.minWidth = defaultWidth;
                    mediaConstraints.video.mandatory.minHeight = defaultHeight;
                }

                return mediaConstraints;
            }

            function captureUserMedia(mediaConstraints, successCallback, errorCallback) {
                if(mediaConstraints.video == true) {
                    mediaConstraints.video = {};
                }

                setVideoBitrates();

                mediaConstraints = getVideoResolutions(mediaConstraints);
                mediaConstraints = getFrameRates(mediaConstraints);

                var isBlackBerry = !!(/BB10|BlackBerry/i.test(navigator.userAgent || ''));
                if(isBlackBerry && !!(navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia)) {
                    navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
                    navigator.getUserMedia(mediaConstraints, successCallback, errorCallback);
                    return;
                }

                if(chkMultiStreamRecorder.checked === true) {
                    captureAllCameras(successCallback, errorCallback);
                    return;
                }

                navigator.mediaDevices.getUserMedia(mediaConstraints).then(function(stream) {
                    successCallback(stream);

                    setVideoURL(stream, true);
                }).catch(function(error) {
                    if(error && error.name === 'ConstraintNotSatisfiedError') {
                        alert('Your camera or browser does NOT supports selected resolutions or frame-rates. \n\nPlease select "default" resolutions.');
                    }

                    errorCallback(error);
                });
            }

            function setMediaContainerFormat(arrayOfOptionsSupported) {
                var options = Array.prototype.slice.call(
                    mediaContainerFormat.querySelectorAll('option')
                );

                var localStorageItem;
                if(localStorage.getItem('media-container-format')) {
                    localStorageItem = localStorage.getItem('media-container-format');
                }

                var selectedItem;
                options.forEach(function(option) {
                    option.disabled = true;

                    if(arrayOfOptionsSupported.indexOf(option.value) !== -1) {
                        option.disabled = false;

                        if(localStorageItem && arrayOfOptionsSupported.indexOf(localStorageItem) != -1) {
                            if(option.value != localStorageItem) return;
                            option.selected = true;
                            selectedItem = option;
                            return;
                        }

                        if(!selectedItem) {
                            option.selected = true;
                            selectedItem = option;
                        }
                    }
                });
            }

            recordingMedia.onchange = function() {
                var options = [];
                if(recordingMedia.value === 'record-audio') {
                    setMediaContainerFormat(['opus', 'pcm']);
                    return;
                }
                setMediaContainerFormat(['vp8', 'vp9', 'h264', 'gif', 'whammy']);
            };
            recordingMedia.onchange();

            if(webrtcDetectedBrowser === 'edge') {
                // webp isn't supported in Microsoft Edge
                // neither MediaRecorder API
                // so lets disable both video/screen recording options

                console.warn('Neither MediaRecorder API nor webp is supported in Microsoft Edge. You cam merely record audio.');

                recordingMedia.innerHTML = '<option value="record-audio">Audio</option>';
                setMediaContainerFormat(['pcm']);
            }

            function saveToDiskOrOpenNewTab(recordRTC) {
                var fileName = 'RecordRTC-' + (new Date).toISOString().replace(/:|\./g, '-') + '.' + fileExtension;

                document.querySelector('#save-to-disk').parentNode.style.display = 'block';
                document.querySelector('#save-to-disk').onclick = function() {
                    if(!recordRTC) return alert('No recording found.');

                    var file = new File([recordRTC.getBlob()], fileName, {
                        type: mimeType
                    });

                    invokeSaveAsDialog(file, file.name);
                };

                document.querySelector('#open-new-tab').onclick = function() {
                    if(!recordRTC) return alert('No recording found.');

                    var file = new File([recordRTC.getBlob()], fileName, {
                        type: mimeType
                    });

                    window.open(URL.createObjectURL(file));
                };

                // upload to PHP server
                document.querySelector('#upload-to-php').disabled = false;
                //document.querySelector('#upload-to-php').onclick = function() {
                    if(!recordRTC) return alert('No recording found.');
                    this.disabled = true;

                    var button = this;
                    uploadToPHPServer(fileName, recordRTC, function(progress, fileURL) {
                        if(progress === 'ended') {
                            button.disabled = false;
                            button.innerHTML = 'Click to download from server';
                            button.onclick = function() {
                                SaveFileURLToDisk(fileURL, fileName);
                            };

                            setVideoURL(fileURL);
                            return;
                        }
                        button.innerHTML = progress;
                    });
               /// };

                // upload to YouTube!
                document.querySelector('#upload-to-youtube').disabled = false;
                document.querySelector('#upload-to-youtube').onclick = function() {
                    if(!recordRTC) return alert('No recording found.');
                    this.disabled = true;

                    var button = this;
                    uploadToYouTube(fileName, recordRTC, function(percentageComplete, fileURL) {
                        if(percentageComplete == 'uploaded') {
                            button.disabled = false;
                            button.innerHTML = 'Uploaded. However YouTube is still processing.';
                            button.onclick = function() {
                                window.open(fileURL);
                            };
                            return;
                        }
                        if(percentageComplete == 'processed') {
                            button.disabled = false;
                            button.innerHTML = 'Uploaded & Processed. Click to open YouTube video.';
                            button.onclick = function() {
                                window.open(fileURL);
                            };

                            document.querySelector('h1').innerHTML = 'Your video has been uploaded. Default privacy type is <span>private</span>. Please visit <a href="https://www.youtube.com/my_videos?o=U" target="_blank">youtube.com/my_videos</a> to change to <span>public</span>.';
                            window.scrollTo(0, 0);
                            return;
                        }
                        if(percentageComplete == 'failed') {
                            button.disabled = false;
                            button.innerHTML = 'YouTube failed transcoding the video.';
                            button.onclick = function() {
                                window.open(fileURL);
                            };
                            return;
                        }
                        button.innerHTML = percentageComplete + '% uploaded to YouTube.';
                    });
                };
            }

            function uploadToPHPServer(fileName, recordRTC, callback) {
				$("#progress-circle").attr("class","progress-circle progress-0");
                    $("#percet_text").text(0);
					$("#myModal").css("display","block");
					$("#cover").css("display","block");
					
                var blob = recordRTC instanceof Blob ? recordRTC : recordRTC.getBlob();
                var reader = new FileReader();
				$("video").attr("src","");
				reader.readAsDataURL(blob);
				reader.onload = function () {
					//console.log(reader.result);
					EncodedContent = reader.result.split(',')[1];
					readContent = 1;
				};
				reader.onloadend = function(){
					
					//console.log(EncodedContent);
					var len = EncodedContent.length;
					
					var start = 0;
					var end = chunkSize;
					var count = 1;
					var totalCount = Math.ceil(len/chunkSize);
					var d = new Date();
					var timeStamp = d.getTime();
					var fileName = timeStamp+".mp4";
					sendBytes(EncodedContent.slice(start, end),start,end,len,count,totalCount,fileName);
				}
				reader.onerror = function (error) {
					//console.log('Error: ', error);
				};
            }
function sendBytes(content,start,end,len,count,totalCount,fileName){
    $("#demo").css("display","none");
    if(end >= len){
        lastChunk = 1;
    }else{
        lastChunk = 0;
    }
	methodname = "interviewQuestionAnswer_v2";
	var d = new Date();
    
	ActiveQuestionId = QuestionArray[counter];
	
	
	fileNameArray.push(fileName);
    var js_obj = {token: 'ecbcd7eaee29848978134beeecdfbc7c',methodname: methodname,userId:tokenUser,jobCode:jobCode,interviewSet:interviewSet,questionId:ActiveQuestionId, filename:fileName,fileFailChunk:0,isLastChunk:lastChunk, answer:content, isInvite :'<?php echo $isInvite; ?>' };
    var encoded = JSON.stringify( js_obj );
    var dataToSend = encoded;
    
    var req;
    req = new XMLHttpRequest();

    var url = "<?php echo base_url(); ?>Fjapi";


    req.open('POST', url, true);
    req.setRequestHeader("Content-Type", "application/json");
    req.send(dataToSend);
    req.onreadystatechange = function () {
        if(req.responseText != ""){
            var response = JSON.parse(req.responseText);

            if (req.readyState == 4) {
                if (response.code == 200) {
                    var display = Math.ceil(100/totalCount)*count;
                    if(display > 100){
                        display = 100;
                    }
                    $("#progress-circle").attr("class","progress-circle progress-"+display);
                    $("#percet_text").text(display);
                    count++;
                    if(lastChunk == 1){
                        
						$(".recordrtc").hide();
						$(".answerButton").show();
						
						if(counter+1 == TotalQuestion){
							$("#progress-circle1").attr("class","progress-circle-small progress-100");
							$("#percet_text1").text(100);
							checkForTheAssessment();
						}else{
							counter++;
							showQuestionNext();
						}
						
                    }
                    if (end <= len){
                        start = end;
                        end = start + chunkSize;
                        sendBytes(EncodedContent.slice(start, end),start,end,len,count,totalCount,fileName);
                    }


                }else if (response.code == 503) {
                    //debugger;
                    sendBytes(EncodedContent.slice(start, end),start,end,len,count,totalCount,fileName);
                }
            }
        }
    }
}

            function makeXMLHttpRequest(url, data, callback) {
                var request = new XMLHttpRequest();
                request.onreadystatechange = function() {
                    if (request.readyState == 4 && request.status == 200) {
                        callback('upload-ended');
                    }
                };

                request.upload.onloadstart = function() {
                    callback('Upload started...');
                };

                request.upload.onprogress = function(event) {
                    callback('Upload Progress ' + Math.round(event.loaded / event.total * 100) + "%");
                };

                request.upload.onload = function() {
                    callback('progress-about-to-end');
                };

                request.upload.onload = function() {
                    callback('progress-ended');
                };

                request.upload.onerror = function(error) {
                    callback('Failed to upload to server');
                };

                request.upload.onabort = function(error) {
                    callback('Upload aborted.');
                };

                request.open('POST', url);
                request.send(data);
            }

            function SaveFileURLToDisk(fileUrl, fileName) {
                var hyperlink = document.createElement('a');
                hyperlink.href = fileUrl;
                hyperlink.target = '_blank';
                hyperlink.download = fileName || fileUrl;

                (document.body || document.documentElement).appendChild(hyperlink);
                hyperlink.onclick = function() {
                   (document.body || document.documentElement).removeChild(hyperlink);
                };

                var mouseEvent = new MouseEvent('click', {
                    view: window,
                    bubbles: true,
                    cancelable: true
                });

                hyperlink.dispatchEvent(mouseEvent);
                
                // NEVER use "revoeObjectURL" here
                // you can use it inside "onclick" handler, though.
                // (window.URL || window.webkitURL).revokeObjectURL(hyperlink.href);

                // if you're writing cross-browser function:
                if(!navigator.mozGetUserMedia) { // i.e. if it is NOT Firefox
                   window.URL.revokeObjectURL(hyperlink.href);
                }
            }

            function getURL(arg) {
                var url = arg;

                if(arg instanceof Blob || arg instanceof File) {
                    url = URL.createObjectURL(arg);
                }

                if(arg instanceof RecordRTC || arg.getBlob) {
                    url = URL.createObjectURL(arg.getBlob());
                }

                if(arg instanceof MediaStream || arg.getTracks || arg.getVideoTracks) {
                    url = URL.createObjectURL(arg);
                }

                return url;
            }

            function setVideoURL(arg, forceNonImage) {
                var url = getURL(arg);

                var parentNode = recordingPlayer.parentNode;
                parentNode.removeChild(recordingPlayer);
                parentNode.innerHTML = '';

                var elem = 'video';
                if(type == 'gif' && !forceNonImage) {
                    elem = 'img';
                }
                if(type == 'audio') {
                    elem = 'audio';
                }

                recordingPlayer = document.createElement(elem);
                
                if(arg instanceof MediaStream) {
                    recordingPlayer.muted = true;
                }

                recordingPlayer.addEventListener('loadedmetadata', function() {
                    if(navigator.userAgent.toLowerCase().indexOf('android') == -1) return;

                    // android
                    setTimeout(function() {
                        if(typeof recordingPlayer.play === 'function') {
                            recordingPlayer.play();
                        }
                    }, 2000);
                }, false);

                recordingPlayer.poster = '';
                recordingPlayer.src = url;
				recordingPlayer.setAttribute("style", "width:284px;border: 1px solid black;margin-left:6%;");

                if(typeof recordingPlayer.play === 'function') {
                    recordingPlayer.play();
                }

                recordingPlayer.addEventListener('ended', function() {
                    url = getURL(arg);
                    recordingPlayer.src = url;
                });

                recordingPlayer.controls = true;
                parentNode.appendChild(recordingPlayer);
            }
        </script>

        <script>
            /* upload_youtube_video.js Copyright 2017 Google Inc. All Rights Reserved. */

            function uploadToYouTube(fileName, recordRTC, callback) {
                var blob = recordRTC instanceof Blob ? recordRTC : recordRTC.getBlob();
                
                blob = new File([blob], 'RecordRTC-' + (new Date).toISOString().replace(/:|\./g, '-') + '.' + fileExtension, {
                    type: mimeType
                });

                uploadVideo.callback = callback;
                uploadVideo.uploadFile(fileName, blob);
            }

            var uploadVideo;

            var signinCallback = function (result){
              if(result.access_token) {
                uploadVideo = new UploadVideo();
                uploadVideo.ready(result.access_token);

                document.querySelector('#signinButton').style.display = 'none';
              }
            };

            var STATUS_POLLING_INTERVAL_MILLIS = 60 * 1000; // One minute.

            var UploadVideo = function() {
              this.tags = ['recordrtc'];
              this.categoryId = 28; // via: http://stackoverflow.com/a/35877512/552182
              this.videoId = '';
              this.uploadStartTime = 0;
            };


            UploadVideo.prototype.ready = function(accessToken) {
              this.accessToken = accessToken;
              this.gapi = gapi;
              this.authenticated = true;
              false && this.gapi.client.request({
                path: '/youtube/v3/channels',
                params: {
                  part: 'snippet',
                  mine: true
                },
                callback: function(response) {
                  if (!response.error) {
                    // response.items[0].snippet.title -- channel title
                    // response.items[0].snippet.thumbnails.default.url -- channel thumbnail
                  }
                }.bind(this)
              });
            };

            UploadVideo.prototype.uploadFile = function(fileName, file) {
              var metadata = {
                snippet: {
                  title: fileName,
                  description: fileName,
                  tags: this.tags,
                  categoryId: this.categoryId
                },
                status: {
                  privacyStatus: 'private'
                }
              };
              var uploader = new MediaUploader({
                baseUrl: 'https://www.googleapis.com/upload/youtube/v3/videos',
                file: file,
                token: this.accessToken,
                metadata: metadata,
                params: {
                  part: Object.keys(metadata).join(',')
                },
                onError: function(data) {
                  var message = data;
                  try {
                    var errorResponse = JSON.parse(data);
                    message = errorResponse.error.message;
                  } finally {
                    alert(message);
                  }
                }.bind(this),
                onProgress: function(data) {
                  var bytesUploaded = data.loaded;
                  var totalBytes = parseInt(data.total);
                  var percentageComplete = parseInt((bytesUploaded * 100) / totalBytes);

                  uploadVideo.callback(percentageComplete);
                }.bind(this),
                onComplete: function(data) {
                  var uploadResponse = JSON.parse(data);
                  this.videoId = uploadResponse.id;
                  this.videoURL = 'https://www.youtube.com/watch?v=' + this.videoId;
                  uploadVideo.callback('uploaded', this.videoURL);

                  setTimeout(this.pollForVideoStatus, 2000);
                }.bind(this)
              });
              this.uploadStartTime = Date.now();
              uploader.upload();
            };

            UploadVideo.prototype.pollForVideoStatus = function() {
              this.gapi.client.request({
                path: '/youtube/v3/videos',
                params: {
                  part: 'status,player',
                  id: this.videoId
                },
                callback: function(response) {
                  if (response.error) {
                    uploadVideo.pollForVideoStatus();
                  } else {
                    var uploadStatus = response.items[0].status.uploadStatus;
                    switch (uploadStatus) {
                      case 'uploaded':
                        uploadVideo.callback('uploaded', uploadVideo.videoURL);
                        uploadVideo.pollForVideoStatus();
                        break;
                        case 'processed':
                        uploadVideo.callback('processed', uploadVideo.videoURL);
                        break;
                        default:
                        uploadVideo.callback('failed', uploadVideo.videoURL);
                        break;
                    }
                  }
                }.bind(this)
              });
            };

        </script>

        <script>
            /* cors_upload.js Copyright 2015 Google Inc. All Rights Reserved. */

            var DRIVE_UPLOAD_URL = 'https://www.googleapis.com/upload/drive/v2/files/';

            var RetryHandler = function() {
              this.interval = 1000; // Start at one second
              this.maxInterval = 60 * 1000; // Don't wait longer than a minute 
            };

            RetryHandler.prototype.retry = function(fn) {
              setTimeout(fn, this.interval);
              this.interval = this.nextInterval_();
            };

            RetryHandler.prototype.reset = function() {
              this.interval = 1000;
            };

            RetryHandler.prototype.nextInterval_ = function() {
              var interval = this.interval * 2 + this.getRandomInt_(0, 1000);
              return Math.min(interval, this.maxInterval);
            };

            RetryHandler.prototype.getRandomInt_ = function(min, max) {
              return Math.floor(Math.random() * (max - min + 1) + min);
            };

            var MediaUploader = function(options) {
              var noop = function() {};
              this.file = options.file;
              this.contentType = options.contentType || this.file.type || 'application/octet-stream';
              this.metadata = options.metadata || {
                'title': this.file.name,
                'mimeType': this.contentType
              };
              this.token = options.token;
              this.onComplete = options.onComplete || noop;
              this.onProgress = options.onProgress || noop;
              this.onError = options.onError || noop;
              this.offset = options.offset || 0;
              this.chunkSize = options.chunkSize || 0;
              this.retryHandler = new RetryHandler();

              this.url = options.url;
              if (!this.url) {
                var params = options.params || {};
                params.uploadType = 'resumable';
                this.url = this.buildUrl_(options.fileId, params, options.baseUrl);
              }
              this.httpMethod = options.fileId ? 'PUT' : 'POST';
            };

            MediaUploader.prototype.upload = function() {
              var self = this;
              var xhr = new XMLHttpRequest();

              xhr.open(this.httpMethod, this.url, true);
              xhr.setRequestHeader('Authorization', 'Bearer ' + this.token);
              xhr.setRequestHeader('Content-Type', 'application/json');
              xhr.setRequestHeader('X-Upload-Content-Length', this.file.size);
              xhr.setRequestHeader('X-Upload-Content-Type', this.contentType);

              xhr.onload = function(e) {
                if (e.target.status < 400) {
                  var location = e.target.getResponseHeader('Location');
                  this.url = location;
                  this.sendFile_();
                } else {
                  this.onUploadError_(e);
                }
              }.bind(this);
              xhr.onerror = this.onUploadError_.bind(this);
              xhr.send(JSON.stringify(this.metadata));
            };

            MediaUploader.prototype.sendFile_ = function() {
              var content = this.file;
              var end = this.file.size;

              if (this.offset || this.chunkSize) {
                // Only bother to slice the file if we're either resuming or uploading in chunks
                if (this.chunkSize) {
                  end = Math.min(this.offset + this.chunkSize, this.file.size);
                }
                content = content.slice(this.offset, end);
              }

              var xhr = new XMLHttpRequest();
              xhr.open('PUT', this.url, true);
              xhr.setRequestHeader('Content-Type', this.contentType);
              xhr.setRequestHeader('Content-Range', 'bytes ' + this.offset + '-' + (end - 1) + '/' + this.file.size);
              xhr.setRequestHeader('X-Upload-Content-Type', this.file.type);
              if (xhr.upload) {
                xhr.upload.addEventListener('progress', this.onProgress);
              }
              xhr.onload = this.onContentUploadSuccess_.bind(this);
              xhr.onerror = this.onContentUploadError_.bind(this);
              xhr.send(content);
            };

            MediaUploader.prototype.resume_ = function() {
              var xhr = new XMLHttpRequest();
              xhr.open('PUT', this.url, true);
              xhr.setRequestHeader('Content-Range', 'bytes */' + this.file.size);
              xhr.setRequestHeader('X-Upload-Content-Type', this.file.type);
              if (xhr.upload) {
                xhr.upload.addEventListener('progress', this.onProgress);
              }
              xhr.onload = this.onContentUploadSuccess_.bind(this);
              xhr.onerror = this.onContentUploadError_.bind(this);
              xhr.send();
            };

            MediaUploader.prototype.extractRange_ = function(xhr) {
              var range = xhr.getResponseHeader('Range');
              if (range) {
                this.offset = parseInt(range.match(/\d+/g).pop(), 10) + 1;
              }
            };

            MediaUploader.prototype.onContentUploadSuccess_ = function(e) {
              if (e.target.status == 200 || e.target.status == 201) {
                this.onComplete(e.target.response);
              } else if (e.target.status == 308) {
                this.extractRange_(e.target);
                this.retryHandler.reset();
                this.sendFile_();
              }
            };

            MediaUploader.prototype.onContentUploadError_ = function(e) {
              if (e.target.status && e.target.status < 500) {
                this.onError(e.target.response);
              } else {
                this.retryHandler.retry(this.resume_.bind(this));
              }
            };

            MediaUploader.prototype.onUploadError_ = function(e) {
              this.onError(e.target.response); // TODO - Retries for initial upload
            };

            MediaUploader.prototype.buildQuery_ = function(params) {
              params = params || {};
              return Object.keys(params).map(function(key) {
                return encodeURIComponent(key) + '=' + encodeURIComponent(params[key]);
              }).join('&');
            };

            MediaUploader.prototype.buildUrl_ = function(id, params, baseUrl) {
              var url = baseUrl || DRIVE_UPLOAD_URL;
              if (id) {
                url += id;
              }
              var query = this.buildQuery_(params);
              if (query) {
                url += '?' + query;
              }
              return url;
            };
        </script>

        <script>
            // MultiStreamRecorder
            var chkMultiStreamRecorder = document.querySelector('#chk-MultiStreamRecorder');

            chkMultiStreamRecorder.addEventListener('change', function() {
                if(chkMultiStreamRecorder.checked === true) {
                    // localStorage.setItem('chkMultiStreamRecorder', true);
                }
                else {
                    // chkMultiStreamRecorder.removeItem('chkMultiStreamRecorder');
                }
            }, false);

            if(localStorage.getItem('chkMultiStreamRecorder')) {
                // chkMultiStreamRecorder.checked = true;
            }

            if(webrtcDetectedBrowser != 'chrome') {
                chkMultiStreamRecorder.disabled = true;
                chkMultiStreamRecorder.checked = false;
            }
            
            function captureAllCameras(successCallback, errorCallback) {
                var streams = [];
                var donotDuplicateDevices = {};

                DetectRTC.videoInputDevices.forEach(function(device, idx) {
                    var mediaConstraints = {
                        audio: true,
                        video: {
                            mandatory: {},
                            optional: [{
                                sourceId: device.id
                            }]
                        }
                    };

                    if(webrtcDetectedBrowser === 'firefox') {
                        mediaConstraints = {
                            audio: true,
                            video: {
                                deviceId: device.id
                            }
                        };
                    }

                    navigator.mediaDevices.getUserMedia(mediaConstraints).then(function(stream) {
                        if(!donotDuplicateDevices[device.id]) {
                            donotDuplicateDevices[device.id] = true;

                            if(streams.length < 5) {
                                // only 4-streams are allowed, currently
                                streams.push(stream);
                            }
                        }

                        if(idx == DetectRTC.videoInputDevices.length - 1) {
                            successCallback(streams);
                        }
                    }).catch(function(error) {
                        if(error && error.name === 'ConstraintNotSatisfiedError') {
                            alert('Your camera or browser does NOT supports selected resolutions or frame-rates. \n\nPlease select "default" resolutions.');
                        }

                        errorCallback(error);
                    });
                })
            }

            [mediaContainerFormat, chkMultiStreamRecorder, recordingMedia].forEach(function(element) {
                element.addEventListener('change', function() {
                    if(chkMultiStreamRecorder.checked === true) {
                        if(mediaContainerFormat.value.toString().search(/vp8|vp9|h264/g) === -1) {
                            mediaContainerFormat.value = 'vp8';
                        }

                        if(recordingMedia.value.toString().search(/video/g) === -1) {
                            recordingMedia.value = 'record-audio-plus-video';
                            recordingMedia.onchange();
                        }
                    }
                }, false);
            });
        </script>

        <script>
            var btnPauseRecording = document.querySelector('#btn-pause-recording');
            btnPauseRecording.onclick = function() {
                if(!btnStartRecording.recordRTC) {
                    btnPauseRecording.style.display = 'none';
                    return;
                }

                btnPauseRecording.disabled = true;
                if(btnPauseRecording.innerHTML === 'Pause') {
                    btnStartRecording.disabled = true;
                    btnStartRecording.style.fontSize = '15px';
                    btnStartRecording.recordRTC.pauseRecording();
                    recordingPlayer.pause();

                    btnPauseRecording.style.fontSize = 'inherit';
                    setTimeout(function() {
                        btnPauseRecording.innerHTML = 'Resume Recording';
                        btnPauseRecording.disabled = false;
                    }, 2000);
                }

                if(btnPauseRecording.innerHTML === 'Resume Recording') {
                    btnStartRecording.disabled = false;
                    btnStartRecording.style.fontSize = 'inherit';
                    btnStartRecording.recordRTC.resumeRecording();
                    recordingPlayer.play();

                    btnPauseRecording.style.fontSize = '15px';
                    btnPauseRecording.innerHTML = 'Pause';
                    setTimeout(function() {
                        btnPauseRecording.disabled = false;
                    }, 2000);
                }
            };
        </script>

	
/*
* This js is being used on basic profile page
*
*/

var base_URL = $("#currentbase_url").val();

function saveBasicInfo(){
    //var forData = $('#createSubuserForm').serialize();
    //var data= JSON.stringify($('#createSubuserForm').serializeObject());
    var userId = $("#userId").val();
    var token = $("#token").val();
    var methodname = $("#methodname").val();
    var name = $("#name").val();
    var email = $("#email").val();
    var mobile = $("#mobile").val();
    var gender = $("#gender").val();
    var dob = $("#datepicker-13").val();
    var pincode = $("#pincode").val();
    
    
    var js_obj = {userId: userId, token: token,methodname: methodname, basicInformation: [{fullname: name, email: email, mobile: mobile, gender: gender, dob: dob, pincode: pincode}] };

	var encoded = JSON.stringify( js_obj );

	var data= encoded;

    $.ajax({
          url : base_URL+"Fjapi",
          data: data,
          type: "POST",
          success: function(response){
                item = $.parseJSON(response);
                if(item.code == "200"){
                    var obj = JSON.parse(response);
                    var fullname = JSON.stringify(obj.basicInfo[0].fullname);
                    var email = JSON.stringify(obj.basicInfo[0].email);
                    var mobile = JSON.stringify(obj.basicInfo[0].mobile);
                    var dob = JSON.stringify(obj.basicInfo[0].dob);
                    var pincode = JSON.stringify(obj.basicInfo[0].pincode);
                    var gender = JSON.stringify(obj.basicInfo[0].gender);
                    var profilePercent = JSON.stringify(obj.basicInfo[0].profilePercent);
                    
                    $("#name").val(fullname.replace(/\"/g, ""));
                    $("#mobile").val(mobile.replace(/\"/g, ""));
                    $("#email").val(email.replace(/\"/g, ""));
                    $("#pincode").val(pincode.replace(/\"/g, ""));
                    $("#datepicker-13").val(dob.replace(/\"/g, ""));
                    $("#gender").val(gender.replace(/\"/g, ""));
                    //window.location="<?php echo base_url()?>users/home";
                }else{
                    alert(item.message);
                }
          }
    });
}
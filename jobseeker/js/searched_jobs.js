var base_URL = $("#currentbase_url").val();
var jobCode = $("currentjobCode").val();
var tokenUser = $("#currentUserId").val();


var count = 0;


$(document).ready(function(){
    $("#locationFilterName_chosen").css("margin-left", "27px");
    $("#locationFilterName_chosen").css("font-weight", "400");
    $("#locationFilterName_chosen input[type='text']").css("color", "#555");
});

//infinteScrollContent
var TotalMessageCounter = $("#endPoint").val();
var totalPages = Math.ceil(TotalMessageCounter/25);
var pageCounter = 1;
//var pageCounterStart = 1*25;
$(window).scroll(function() {
    if($(window).scrollTop() + $(window).height() == $(document).height()) {
        if(pageCounter < totalPages){
            $.ajax({
                url : base_URL+"job/searched-jobs",
                data : 'startPoint='+pageCounter+'&filterType=ajax',
                type: "POST",
                success: function(response){
                    
                    $("#infinteScrollContent"+pageCounter).html(response);
                    var id = pageCounter+1;
                    $("<tbody id='infinteScrollContent"+id+"'></tbody>").insertAfter("#infinteScrollContent"+pageCounter);
                    pageCounter++;
                    
                }
            });
        }
    }
});
    

/*
* This js is being used on for the assesment page
*
*/
var base_URL = $("#currentbase_url").val();
var jobCode = $("#currentjobCode").val();
var tokenUser = $("#currentuserId").val();

var seconds = 10;
var QuestionArray = [];
var nextQuestion = 0;
var TotalQuestion = 0;
var currentQuestion = 0;
var previousQuestion = 0;
var setId = 0;
var counter = 0;
var timeLimit = 0;
var questionAns = [];

jQuery(function () {

	$(document).keydown(function (evt) {
		if (evt.keyCode == 116) { // down arrow
		   alert("Your interview will be cancelled"); // prevents the usual scrolling behaviour
		   
		   window.location= base_URL+"users";
		}
	});
	window.onbeforeunload = function (e) {
		e = e || window.event;

		// For IE and Firefox prior to version 4
		if (e) {
			e.returnValue = 'Sure?';
		}

		// For Safari
		return 'Sure?';
        window.location = base_URL+"users";
	};

});

var x = setInterval(function() {

	var now = new Date().getTime();
	seconds --;
	var hour = seconds/(60*60);
	var mins = Math.floor(seconds/(60));
    var showSeconds = seconds - mins*60;
	if(mins > 1){
		document.getElementById("demo").innerHTML = "Time Left : "+mins+" mins<br>"+showSeconds + " secs";
	}else{
		document.getElementById("demo").innerHTML = "Time Left : "+mins+" min<br>"+showSeconds + " secs";
	}
	
    
}, 1000);



$(document).ready(function() {
	
	var methodname = 'assessmentQuestionList_v3';
	getUserJobProcessStatus();
	
  
	var js_obj = {token: 'ecbcd7eaee29848978134beeecdfbc7c',methodname: methodname,jobCode:jobCode,userId:tokenUser};

	var encoded = JSON.stringify( js_obj );

	var data= encoded;
	$.ajax({
		  url : base_URL+"Fjapi",
		  data: data,
		  type: "POST",
		  success: function(response){
				item = $.parseJSON(response);
				if(item.code == "200"){
					setId = item.data[0].setID;
					
					
					for(var i = 0; i < item.data.length; i++)
					{	QuestionArray.push(item.data[i].questionID);
						questionAns[item.data[i].questionID] = "";
						if(i == 0){
							nextQuestion = item.data[i].questionID;
							showQuestionNext();
							currentQuestion = item.data[i].questionID;
							previousQuestion = 0;
						}
					   //console.log("Type: " + item.data[i].setID + " Name: " + item.data[i].setID + " Account: " + item.data[i].setID);
							/**/
					}
					TotalQuestion = QuestionArray.length;
					seconds = item.duration;
					timeLimit = item.duration;
					$("#TotalQuestions").text(TotalQuestion);
					window.setTimeout(submitAssessment, (timeLimit*1000)+9000);
				}else{
					//alert(item.message);
				}
		  }
	});
		
});

function setUserJobProcessStatus(){
	var methodname = 'setUserJobProcessStatus';
	var userId = tokenUser;
	var status = 3;
	
	var js_obj = {token: 'ecbcd7eaee29848978134beeecdfbc7c',methodname: methodname,userId:userId, jobCode:jobCode, status:status};
	var encoded = JSON.stringify( js_obj );
	var data= encoded;
	$.ajax({
		  url : base_URL+"Fjapi",
		  data: data,
		  type: "POST",
		  success: function(response){
				item = $.parseJSON(response);
				if(item.code == '200'){
					if(item.status == '1'){
						
					}
				}
				
		  }
	});
	
}

function getUserJobProcessStatus(){
	var methodname = 'getUserJobProcessStatus';
	var userId = tokenUser;
	
	var js_obj = {token: 'ecbcd7eaee29848978134beeecdfbc7c',methodname: methodname,userId:userId, jobCode:jobCode};
	var encoded = JSON.stringify( js_obj );
	var data= encoded;
	$.ajax({
		  url : base_URL+"Fjapi",
		  data: data,
		  type: "POST",
		  success: function(response){
				item = $.parseJSON(response);
				if(item.code == '200'){
					if(item.status == '2'){
						setUserJobProcessStatus();
					}else{
						alert("Sorry! Your job has been terminated. You can't give the interview again for the same job.");
						window.onbeforeunload = null;
						window.location = base_URL+"users";
					}
				}
				
		  }
	});
	
}

function submitAssessment(){
	window.onbeforeunload = null;
	var InvitationId = $("#currentinvitationId").val();
	if(InvitationId == ""){
		window.location = base_URL+"uploadResume";
	}else{
		window.location = base_URL+"thankyou";
	}
}

function getNextQuestion(){
	getAnswers();
	if((counter+1) > TotalQuestion){
		$("#submitQuestion").css("display","block");
		$('#NextQuestion').attr('disabled','disabled');
		window.onbeforeunload = null;
		var InvitationId = $("#currentinvitationId").val();
		if(InvitationId == ""){
			window.location = base_URL+"uploadResume";
		}else{
			window.location = base_URL+"thankyou";
		}
		
	}else if((counter+1) == TotalQuestion){
		$("#submitQuestion").css("display","block");
		counter++;
		showQuestionNext();
	}else{
		counter++;
		$("#submitQuestion").css("display","none");
		$('#NextQuestion').removeAttr('disabled');
		$('#previousQuestion').removeAttr('disabled');
		showQuestionNext();
	}
}



function PreviousQuestions(){
	getAnswers();
	if((counter-1) == 0){
		$("#submitQuestion").css("display","block");
		$('#NextQuestion').removeAttr('disabled');
		$('#previousQuestion').attr('disabled','disabled');
		counter--;
		showQuestionNext();
	}else{
		$('#previousQuestion').removeAttr('disabled');
		$('#NextQuestion').removeAttr('disabled');
		
		$("#submitQuestion").css("display","none");
		counter--;
		showQuestionNext();
	}
}


function showQuestionNext(){
	
	var methodname = 'assessmentQuestionDetail_v2';

	var questionId = QuestionArray[counter];
	
	var js_obj = {token: 'ecbcd7eaee29848978134beeecdfbc7c',methodname: methodname,userId:tokenUser, jobCode:jobCode, assessmentSet:setId, questionId:questionId};

	var encoded = JSON.stringify( js_obj );

	var data= encoded;
	$.ajax({
		  url : base_URL+"Fjapi",
		  data: data,
		  type: "POST",
		  success: function(response){
				item = $.parseJSON(response);
				if(item.code == "200"){
					currentQuestion = item.data[0].questionId;
					title = item.data[0].title;
					options = item.data[0].options;
					src = item.data[0].file;
					
					
					$("#QuestionBank").html(title);
                    var CurrentQuestionCounter = counter+1;
					$("#questionCount").text(CurrentQuestionCounter+"/"+TotalQuestion);
					$("#OptionsnBank").html("");
					if(src != ""){
						$("#FileBank").css('display','block');
						$("#FileBank").attr('src', src);
					}else{
							$("#FileBank").css('display','none');
					}
					for(var i = 0; i < item.data[0].options.length; i++)
					{	
						$("#OptionsnBank").append("<input type='radio' name='OptionsBankAns' value='"+item.data[0].options[i].optionId+"' id='"+item.data[0].options[i].optionId+"' >"+item.data[0].options[i].optionValue+"<br>");
					}
					$("#"+questionAns[currentQuestion]).attr('checked', 'checked');
				}else{
					//alert(item.message);
				}
		  }
	});
}
var usersAnswers = [];

function getAnswers(){
	var methodname = 'assessmentQuestionAnswer_v2';
	var Answers = [];
	$.each($("input[name='OptionsBankAns']:checked"), function(){            
		Answers.push($(this).val());
	});
	var allAnswers = Answers.join(", ");
	questionId = QuestionArray[counter];
	questionAns[QuestionArray[counter]] = allAnswers;
	
	
	//console.log(Answers);
	if(allAnswers != ""){
		var js_obj = {token: 'ecbcd7eaee29848978134beeecdfbc7c',methodname: methodname,userId:tokenUser, jobCode:jobCode,assessmentSet:setId, questionId:questionId, answer:allAnswers};

		var encoded = JSON.stringify( js_obj );

		var data= encoded;
		$.ajax({
			  url : base_URL+"Fjapi",
			  data: data,
			  type: "POST",
			  success: function(response){
					
			  }
		});
	}
}
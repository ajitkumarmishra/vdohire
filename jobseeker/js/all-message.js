var base_URL = $("#currentbase_url").val();
var jobCode = $("#currentjobCode").val();
var tokenUser = $("#currentuserId").val();

var TotalMessageCounter = $("#TotalMessageCounter").val();
var totalPages = Math.ceil(TotalMessageCounter/50);
var pageCounter = 1;
$(window).scroll(function() {
	if($(window).scrollTop() + $(window).height() == $(document).height()) {
		
		if(pageCounter < totalPages){
			$.ajax({
				url : base_URL+"getMessageNext",
				data : 'page='+pageCounter,
				type: "POST",
				success: function(response){
					
					$("#infinteScrollContent"+pageCounter).html(response);
					var id = pageCounter+1;
					$("<tbody id='infinteScrollContent"+id+"'></tbody>").insertAfter("#infinteScrollContent"+pageCounter);
					pageCounter++;
					
				}
			});
		}
	}
});
var tokenUser = $("#currentuserId").val();
function getmessageDetailsById(id) {
    var js_obj = {token: 'ecbcd7eaee29848978134beeecdfbc7c',methodname: 'getMessageDetails',userId:tokenUser, messageId:id};
    var encoded = JSON.stringify( js_obj );
    var data= encoded;
    $.ajax({
          url : base_URL+"Fjapi",
          data: data,
          type: "POST",
          success: function(response){
                item = $.parseJSON(response);
                if(item.code == "200"){
                    $("#message_title").text(item.data.messageSubject);
                    $("#message_company_name").text(item.data.senderName);
                    $("#message_date_time").text(item.data.time);
                    var messageContent = item.data.message;
                    
                    $("#message_message").html(messageContent);
                }else{
                    alert(item.message);
                }
            }
    });
}

function ReplyTomessage(id){
	var js_obj = {token: 'ecbcd7eaee29848978134beeecdfbc7c',methodname: 'getMessageDetails',userId:tokenUser, messageId:id};
    var encoded = JSON.stringify( js_obj );
    var data= encoded;
    $.ajax({
          url : base_URL+"Fjapi",
          data: data,
          type: "POST",
          success: function(response){
                item = $.parseJSON(response);
                if(item.code == "200"){
                    $("#message_titleReply").html("<b>Subject: </b><span style='font-size:15px;'> Re:"+item.data.messageSubject+"</span>");
                    $("#message_company_nameReply").text(item.data.senderName);
                    $("#message_date_timeReply").text(item.data.time);
                    $("#message_id").val(id);
                    $("#senderId").val(item.data.senderId);
                    var messageContent = item.data.message;
                    
                    $("#message_messageReply").html(messageContent);
                }else{
                    alert(item.message);
                }
            }
    });
	
}

function replyTo(senderId){
	var id = $("#message_id").val();
	var senderId = $("#senderId").val();
	var message = $("#replyBox").val();
	var messageSubject = $("#message_titleReply").text();
	var js_obj = {token: 'ecbcd7eaee29848978134beeecdfbc7c',methodname: 'sendMessage',userId:tokenUser,toId:senderId,messageText:message,messageSubject:messageSubject, parentMessageId:id};
    var encoded = JSON.stringify( js_obj );
    var data= encoded;
    $.ajax({
          url : base_URL+"Fjapi",
          data: data,
          type: "POST",
          success: function(response){
                item = $.parseJSON(response);
                if(item.code == "200"){
                    window.location.reload();
                }else{
                    alert(item.message);
                }
            }
    });
}
/*
* This js is being used on resume page
*
*/

var base_URL = $("#currentbase_url").val();
var tokenUser = $("#currentuserId").val();
var userName = $("#currentUserName").val();

var EncodedContent = "";
var fileName = "";
var chunkSize = 70024;

$("#resumeFile").change(function(){
    $("#changepasswordbutton").css("display","block");
});


function UploadResume(){
    //var data= JSON.stringify($('#uploadResume').serializeObject());

    var formData = new FormData();
    formData.append('resumeFile', $('#resumeFile')[0].files[0]);
    fileName = $('#resumeFile')[0].files[0].name;
    //alert($('#resumeFile')[0].files[0].size);
    
    var ext = $('#resumeFile').val().split('.').pop().toLowerCase();
    if($.inArray(ext, ['pdf','doc','docx']) == -1) {
        alert('Please select Doc, Docx or PDF file.');
        return false;
    }
    
    
    
    var file = document.getElementById("resumeFile").files[0];

    var reader = new FileReader();

    reader.readAsDataURL(file);
    reader.onload = function () {
        //console.log(reader.result);
        EncodedContent = reader.result.split(',')[1];
        readContent = 1;
    };
    reader.onloadend = function(){
        $("#myModal").css("display","block");
        $("#cover").css("display","block");
        //console.log(EncodedContent);
        var len = EncodedContent.length;
        
        var start = 0;
        var end = chunkSize;
        var count = 1;
        var totalCount = Math.ceil(len/chunkSize);
        
        
        sendResume(EncodedContent.slice(start, end),start,end,len,count,totalCount);
    }
    reader.onerror = function (error) {
        console.log('Error: ', error);
    };

}
    
function sendResume(content,start,end,len,count,totalCount){
    
    if(end >= len){
        lastChunk = 1;
    }else{
        lastChunk = 0;
    }
    var methodname = 'editResume_v2';
    var js_obj = {token: 'ecbcd7eaee29848978134beeecdfbc7c',methodname: methodname,userId:tokenUser, resumeName:fileName,fileFailChunk:0,isLastChunk:lastChunk, resumeFile:content };
    var encoded = JSON.stringify( js_obj );
    var dataToSend = encoded;
    
    var req;
    req = new XMLHttpRequest();

    var url = base_URL+"Fjapi";


    req.open('POST', url, true);
    req.setRequestHeader("Content-Type", "application/json");
    req.send(dataToSend);
    req.onreadystatechange = function () {
        if(req.responseText != ""){
            var response = JSON.parse(req.responseText);

            if (req.readyState == 4) {
                if (response.code == 200) {
                    var display = Math.ceil(100/totalCount)*count;
                    if(display > 100){
                        display = 100;
                    }
                    $("#progress-circle").attr("class","progress-circle progress-"+display);
                    $("#percet_text").text(display);
                    count++;
                    if(lastChunk == 1){
                        $("#myModal").css("display","none");
                        //return false;
                        alert('You have uploaded your resume successfully.');
                        window.location.reload();
                    }
                    if (end <= len){
                        start = end;
                        end = start + chunkSize;
                        sendResume(EncodedContent.slice(start, end),start,end,len,count,totalCount);
                    }


                }else if (response.code == 503) {
                    //debugger;
                    sendResume(EncodedContent.slice(start, end),start,end,len,count,totalCount);
                }
            }
        }
    }
}



function makeActive(resumeId,name){
    
    //var forData = $('#createSubuserForm').serialize();
        //var data= JSON.stringify($('#createSubuserForm').serializeObject());
        var userId = $("#userId").val();
        var token = $("#token").val();
        var methodname = 'setUserActiveResume';
        
        var js_obj = {userId: userId, token: token,methodname: methodname,resumeId:resumeId};

        var encoded = JSON.stringify( js_obj );

        var data= encoded;

        
        $.ajax({
              url : base_URL+"Fjapi",
              data: data,
              type: "POST",
              success: function(response){
                    item = $.parseJSON(response);
                    if(item.code == "200"){
                        alert('Your resume "'+name+'" is currently active');
                        window.location.reload();
                    }else{
                        alert(item.message);
                    }
              }
        });
}


function deleteResume(resumeId){
    var r = confirm('Are you sure you want to delete selected resume?');

    if (r == true) {
        var userId = $("#userId").val();
        var token = $("#token").val();
        var methodname = 'deleteUserResume';
        
        var js_obj = {userId: userId, token: token,methodname: methodname,resumeId:resumeId};

        var encoded = JSON.stringify( js_obj );

        var data= encoded;

        
        $.ajax({
              url : base_URL+"Fjapi",
              data: data,
              type: "POST",
              success: function(response){
                    item = $.parseJSON(response);
                    if(item.code == "200"){
                        alert('You have deleted the selected resume successfully.');
                        window.location.reload();
                    }else{
                        alert(item.message);
                    }
              }
        });
    } else {
        //txt = "You pressed Cancel!";
    }
}
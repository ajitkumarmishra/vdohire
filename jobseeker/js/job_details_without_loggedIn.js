var base_URL = $("#currentbase_url").val();
var jobCode = $("#currentjobCode").val();
var tokenUser = $("#currentuserId").val();
if ($('#positionDivContainer')[0].scrollWidth >  $('#positionDivContainer').innerWidth()) {
    //Text has over-flowed
	var text = $('#positionDivContainer').text();
	$('#positionDivContainer').html("<marquee>"+text+"</marquee>");
}
if ($('#locationDivContainer')[0].scrollWidth >  $('#locationDivContainer').innerWidth()) {
    //Text has over-flowed
	var text = $('#locationDivContainer').text();
	$('#locationDivContainer').html("<marquee>"+text+"</marquee>");
}
if ($('#CompanyNameContainer')[0].scrollWidth >  $('#CompanyNameContainer').innerWidth()) {
    //Text has over-flowed
	var text = $('#CompanyNameContainer').text();
	$('#CompanyNameContainer').html("<marquee>"+text+"</marquee>");
}
var registerEnable = 0;
function SignIn(){
	$('#login').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                emailLogin: {
                    validators: {
                        notEmpty: {
                            message: 'The username is required'
                        },
                        emailAddress: {
                            message: 'The value is not a valid email address'
                        }
                    }
                },
				newPasswordlogin: {
                    validators: {
                        notEmpty: {
                            message: 'The password is required'
                        },
                        stringLength: {
                            min: 6,
                            max: 30,
                            message: 'The password must be more than 6 characters long'
                        }
                    }
                }
				
            }
        })
		.on('success.form.fv', function(e) {
            // Prevent form submission
            e.preventDefault();
			
            var email = $("#emailLogin").val();
            var password = $("#newPasswordlogin").val();
            var methodname = 'signIn';
            var token = 'ecbcd7eaee29848978134beeecdfbc7c';
            
            var js_obj = {email: email, password: password, token: token, methodname: methodname};

            var encoded = JSON.stringify( js_obj );

            var data= encoded;
            

            $.ajax({
                    url : "https://"+window.location.host+"/jobseeker/Fjapi",
                    data: data,
                    type: "POST",
                    success: function(response){
                        console.log(response);
                        item = $.parseJSON(response);
                        if(item.code == "200"){
                            
                            $.ajax({
                                url : "https://"+window.location.host+"/jobseeker/users/setSession",
                                data: response,
                                type: "POST",
                                success: function(response){
                                    
                                    if(response == "success"){
                                        window.location="https://"+window.location.host+"/jobseeker/media/question";
                                    }else{
                                        alert(response);
                                    }
                                }
                            });
                            
                        }else{
                            alert(item.message);
                        }
                    }
            });
        });    
}

function signUp(){
	$('#Register').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                email: {
                    validators: {
                        notEmpty: {
                            message: 'The Email is required'
                        },
                        emailAddress: {
                            message: 'The value is not a valid email address'
                        }
                    }
                },
                name: {
                    validators: {
                        notEmpty: {
                            message: 'Enter Your Name'
                        },
                        stringLength: {
                            message: 'Name should have at least 2 characters',
                            min: 2
                        }
                    }
                },
				phone: {
                    validators: {
                        /*notEmpty: {
                            message: 'Enter Phone Number'
                        },
                        numeric:{
                            message: 'Please Enter Number only'
                        },
                        stringLength: {
                            message: 'Mobile number must have 10 digits',
                            min: 10
                        },*/
                        callback: {
                            message: 'Pleae enter valid Mobile Number',
                            callback: function(value, validator) {
                                var regex = "^([0|\+[0-9]{1,5})?([7-9][0-9]{9})$";
                                if (value.match(regex)) {
                                    return true;
                                } else {
                                    return false;
                                }
                            }
                        }
                    }
                },
				newPassword: {
                    validators: {
                        notEmpty: {
                            message: 'The password is required'
                        },
                        stringLength: {
                            min: 6,
                            max: 30,
                            message: 'The password must be more than 6 characters long'
                        }
                    }
                },
				confirmNewPassword: {
                    validators: {
                        notEmpty: {
                            message: 'The confirm password is required'
                        },
                        identical: {
                            field: 'newPassword',
                            message: 'The password and its confirm are not the same'
                        }
                    }
                }
				
            }
        })
		.on('success.form.fv', function(e) {
            // Prevent form submission
            e.preventDefault();
			if(registerEnable == 0){
				registerEnable = 1;
				$("#registerButton").prop("disabled", true);
				var fullname = $("#name").val();
				var username = $("#name").val();
				var inputEmail = $("#email").val();
				var mobile = $("#phone").val();
				var email = $("#email").val();
				var inputPassword = $("#newPassword").val();
				var methodname = 'signUp_v3';
				var token = 'ecbcd7eaee29848978134beeecdfbc7c';
				var gender = 'male';
				var pincode = '110075';
				var deviceId = '1234';
				var deviceType = 'android';
				var social_networking_type = '1';
				var networking_id = '';
				var js_obj = {social_networking_type:social_networking_type,networking_id:networking_id, fullname: fullname, username: username, email: inputEmail, password: inputPassword,token: token,methodname: methodname,
			mobile:mobile,gender:gender,pincode:pincode,deviceId:deviceId,deviceType:deviceType,};

				var encoded = JSON.stringify( js_obj );

				var data= encoded;

				
				$.ajax({
						url : "https://"+window.location.host+"/jobseeker/Fjapi",
						data: data,
						type: "POST",
						success: function(response){
							console.log(response);
							var item = $.parseJSON(response);
							if(item.code == "200"){
								
								$.ajax({
									url : "https://"+window.location.host+"/jobseeker/users/setSessionSignUp",
									data: response,
									type: "POST",
									success: function(responses){
										if(responses == "success"){
											window.location="https://"+window.location.host+"/jobseeker/media/question";
										}else{
											alert('Something went wrong!');
											window.location.reload();
										}
									}
								});
								
							}else{
								alert(item.message);
								registerEnable = 0;
								$("#registerButton").prop("disabled", false);
							}
						}
				});
		}
    });    
}

function ShowAboutCompany(){      
    $("#showJobDescription").css("background","#d3d3d3");
    $("#showJobDescription").css("color","black");

    $("#ShowAboutCompany").css("background","#054f72");
    $("#ShowAboutCompany").css("color","white");

    $("#showAboutUs").show();
    $("#companyVideoDiv").show();
    $("#Requirements").hide();
    $("#showJobDescription1").hide();
    $("#screeningProcess").css("display","none");
}
    
function showJobDescription(){
    $("#showJobDescription").css("background","#054f72");
    $("#showJobDescription").css("color","white");

    $("#ShowAboutCompany").css("background","#d3d3d3");
    $("#ShowAboutCompany").css("color","black");    
    $("#showAboutUs").hide();
    $("#companyVideoDiv").hide();
    $("#showJobDescription1").show();
    $("#screeningProcess").css("display","block");
}



$(document).ready(function() {
	alert(window.location.host);
    $('#fullpage').fullpage({
        anchors: ['get-a-job','what-is-firstjob', 'who-is-firstjob-for', 'how-to-get-the-perfect-job', 'are-you-ready-for-your-first-job', 'our-vision', 'in-media', 'stay-connected'],
        navigation: false,
        navigationPosition: 'right',
        navigationTooltips: ['First page', 'Second page', 'Thired page', 'Forth page', 'Fifth page', 'Sixth page', 'Seventh page','Eatth page'],
        responsiveWidth: 1100,
        afterLoad:function(event){
            var url = window.location.href;
            var type = url.split('#');
            var hash = '';
            if(type.length > 1)
              hash = type[1];

            if(hash == 'what-is-firstjob'){
                    $('.inrwhatisfst').append(function(){
                            setTimeout(function(){$('.inrwhatisfst').addClass('animated fadeInDown')},500);
                     });
            }
            if(hash == 'who-is-firstjob-for'){
                    $('#whois01').append(function() {
                            setTimeout(function(){$('#whois01').addClass('animated fadeInLeft')},500);
                    });
                    $('#whois02').append(function(){
                            setTimeout(function(){$('#whois02').addClass('animated fadeInDown')},1000);
                    });
                    $('#whois03').append(function(){
                            setTimeout(function(){$('#whois03').addClass('animated fadeInRight')},1500);
                    });
            }
            if(hash == 'how-to-get-the-perfect-job'){
                    $('#howto01').append(function(){
                            setTimeout(function(){$('#howto01').addClass('animated fadeInDown')},400);
                    });
                    $('#howto02').append(function(){
                            setTimeout(function(){$('#howto02').addClass('animated fadeInLeft')},700);
                    });
                    $('#howto03').append(function(){
                            setTimeout(function(){$('#howto03').addClass('animated fadeInRight')},1000);
                    });
                    $('#howto04').append(function(){
                            setTimeout(function(){$('#howto04').addClass('animated fadeInLeft')},1300);
                    });
                    $('#howto05').append(function(){
                            setTimeout(function(){$('#howto05').addClass('animated fadeInRight')},1600);	
                    });
                    $('#howto06').append(function(){
                            setTimeout(function(){$('#howto06').addClass('animated fadeInUp')},1900);
                    });
            }
            if(hash == 'are-you-ready-for-your-first-job'){
                    $('.forstop01').append(function(){
                            setTimeout(function(){$('.forstop01').addClass('animated fadeInDown')},500);
                    });
            }
            if(hash == 'our-vision'){
                    $('#infopart').append(function(){
                            setTimeout(function(){$('#infopart').addClass('animated fadeInDown')},500);
                    });
            }
            if(hash == 'in-media'){
                    $('.Media_Inner_grid').append(function(){
                            setTimeout(function(){$('.Media_Inner_grid').addClass('animated fadeInDown')},500);
                    });
            }

            if(hash == 'stay-connected'){
                    $('#cont_form').append(function(){
                            setTimeout(function(){$('#cont_form').addClass('animated fadeInUp')},500);
                    });
            }
        }
    });
});
function checkBrowser(){
    c = navigator.userAgent.search("Chrome");
    f = navigator.userAgent.search("Firefox");
    m8 = navigator.userAgent.search("MSIE 8.0");
    m9 = navigator.userAgent.search("MSIE 9.0");
    var ua = window.navigator.userAgent;
	
	if (/Edge\/\d./i.test(navigator.userAgent)){
   // This is Microsoft Edge
		 $("#loginButton").css("display","none");
        $("#signupButton").css("display","none");
	}else if (c > -1) {
        $("#loginButton").css("display","block");
        $("#signupButton").css("display","block");
    } else if (f > -1) {
        $("#loginButton").css("display","none");
        $("#signupButton").css("display","none");
    } else if (m9 > -1) {
        $("#loginButton").css("display","none");
        $("#signupButton").css("display","none");
    } else if (m8 > -1) {
        $("#loginButton").css("display","none");
        $("#signupButton").css("display","none");
    }else{
        $("#loginButton").css("display","none");
        $("#signupButton").css("display","none");
    }
}

//********************************************
$(document).ready(function() {
	var winw = $(window).width();
	var winheigh = $(window).height();
	$('.flexslider ul.slides li').css({'width':winw,'height':winheigh});
		$('.flexslider').flexslider({
		auto: true,
		animation: "slide",
		controlNav: true,
		directionNav: false,
		slideshowSpeed: 4000,
       // animationSpeed: 4000,
	});
        
        checkBrowser();
	
  //******************************************
  $("#whtfst_job").owlCarousel({
	  navigation : false,
	  pagination : true,
	  paginationSpeed: 1000,
      autoPlay: 10000,
      goToFirstSpeed: 2000,
	  slideSpeed : 800,
      //autoHeight : true,
	  singleItem : true,
	  transitionStyle : 'goDown',
	  afterMove:function(event){
		  var owl = $(".whtfst_job").data('owlCarousel');
		  var currentItem = this.currentItem;
		  for(i=0; i<=4; i++){
			  if(i == currentItem){
		   			owl.jumpTo(i);
			  }
		  }
	  }
  });
  
  $(".whtfst_job").owlCarousel({
	  navigation : false,
	  pagination : false,
	  paginationSpeed: 1000,
      autoPlay: 10000,
      goToFirstSpeed: 2000,
	  slideSpeed : 800,
	  singleItem : true,
	  transitionStyle : 'goDown'
  });
  
});

//*****************************************************
$(".forstop01").mouseenter(function(){
	$('.forstop01 .card-container').flip("stopautoflip");
});
$(".forstop01").mouseleave(function(){
	$('.forstop01 .card-container').flip();
});
//******************************************************
$('#vPhone').keyup(function() {
	if (this.value.match(/[^0-9]/g)) {
		this.value = this.value.replace(/[^0-9]/g, '');
		$('.formError').show();
		$('.formError').html('Enter only number');
		setTimeout(function(){
			$('.formError').hide();
		},5000);
	}
});
//****************************************************


$(document).ready(function(e) {
    $('.bxsliderMedia').bxSlider({
		 controls: true,
		 pager:true,
		 speed:6000,
		 auto: true,
         autoControls: true,
		 pause: 300,
         randomStart : true,
    });
	
	$('.bxsliderMediaMOBILE').bxSlider({
	   minSlides: 1,
	   maxSlides: 2,
	   slideWidth: 360,
	   slideMargin: 10,
	   controls: true,
		 pager: false,
		 speed: 5000,
		 auto: true,
         autoControls: true,
		 pause: 300,
         randomStart : true,
    });
});

    $(document).ready(function() {
        /**********Login functionality**************/
        $('#loginForm').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                email: {
                    validators: {
                        notEmpty: {
                            message: 'The username is required'
                        },
                        emailAddress: {
                            message: 'The value is not a valid email address'
                        }
                    }
                },
                password: {
                    validators: {
                        notEmpty: {
                            message: 'The password is required'
                        }
                    }
                }
            }
        })
        
        .on('success.form.fv', function(e) {
            // Prevent form submission
            e.preventDefault();

            var email = $("#email").val();
            var password = $("#password").val();
            var methodname = 'signIn';
            var token = 'ecbcd7eaee29848978134beeecdfbc7c';
            
            var js_obj = {email: email, password: password, token: token, methodname: methodname};

            var encoded = JSON.stringify( js_obj );

            var data= encoded;
            

            $.ajax({
                    url : "http://35.154.53.72/jobseeker/Fjapi",
                    data: data,
                    type: "POST",
                    success: function(response){
                        console.log(response);
                        item = $.parseJSON(response);
                        if(item.code == "200"){
                            var tokenUser = item.token;
							
                            if(document.getElementById('rememberMe').checked){
                                document.cookie = "fj_email" + "=" + email;
                                document.cookie = "fj_password" + "=" + password;
                            }else{
								document.cookie = "fj_email" + "=";
                                document.cookie = "fj_password" + "=";
							}
							
                                document.cookie = "userId" + "=" + tokenUser;
                                document.cookie = "userName" + "=" + item.userName;
                                document.cookie = "token" + "=" + tokenUser;
                                document.cookie = "userEmail" + "=" + item.userEmail;
                            $.ajax({
                                url : "http://35.154.53.72/jobseeker/users/setSession",
                                data: response,
                                type: "POST",
                                success: function(response){
                                    
                                    if(response == "success"){
                                        window.location="https://35.154.53.72/jobseeker/users";
                                    }else{
                                        alert(response);
                                    }
                                }
                            });
                            
                        }else{
                            alert(item.message);
                        }
                    }
            });
        });
        
        $('#signupForm').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                fullname: {
                    validators: {
                        notEmpty: {
                            message: 'Please Enter Full Name'
                        }
                    }
                },
                username: {
                    validators: {
                        notEmpty: {
                            message: 'Please Enter User Name'
                        }
                    }
                },
                mobile: {
                    validators: {
                        notEmpty: {
                            message: 'Please Enter Mobile No'
                        },
                        numeric:{
                            message: 'Please Enter Number only'
                        }
                    }
                },
                signupEmail: {
                    validators: {
                        notEmpty: {
                            message: 'The username is required'
                        },
                        emailAddress: {
                            message: 'The value is not a valid email address'
                        }
                    }
                },
                Signuppassword: {
                    validators: {
                        notEmpty: {
                            message: 'The password is required'
                        }
                    }
                },
                signupterms: {
                    validators: {
                        notEmpty: {
                            message: 'Please agree to the terms and conditions'
                        }
                    }
                }
            }
        })
        
        .on('success.form.fv', function(e) {
            // Prevent form submission
            e.preventDefault();

            var fullname = $("#fullname").val();
            var username = $("#fullname").val();
            var inputEmail = $("#signupEmail").val();
            var mobile = $("#mobile").val();
            var email = $("#signupEmail").val();
            var inputPassword = $("#Signuppassword").val();
            var methodname = 'signUp_v3';
            var token = 'ecbcd7eaee29848978134beeecdfbc7c';
            var gender = 'male';
            var pincode = '110075';
            var deviceId = '1234';
            var deviceType = 'android';
            
            var js_obj = {fullname: fullname, username: username, email: inputEmail, password: inputPassword,token: token,methodname: methodname,
    mobile:mobile,gender:gender,pincode:pincode,deviceId:deviceId,deviceType:deviceType,};

            var encoded = JSON.stringify( js_obj );

            var data= encoded;

            $.ajax({
                    url : "http://35.154.53.72/jobseeker/Fjapi",
                    data: data,
                    type: "POST",
                    success: function(response){
                        console.log(response);
                        item = $.parseJSON(response);
                        if(item.code == "200"){
                            var tokenUser = item.token;

                            $.ajax({
                                url : "http://35.154.53.72/jobseeker/users/setSessionSignUp",
                                data: response,
                                type: "POST",
                                success: function(response){
                                    
                                    if(response == "success"){
                                        window.location="https://35.154.53.72/jobseeker/users";
                                    }else{
                                        alert(response);
                                    }
                                }
                            });
                            
                        }else{
                            alert(item.message);
                        }
                    }
            });
        });

        
        $('#forgotpasswordid').on('click', function() {
            var emailORmobile = $("#foremail").val();
            var methodname = 'recoverPassword';
            var token = 'ecbcd7eaee29848978134beeecdfbc7c';
            var js_obj = {emailORmobile: emailORmobile,token: token,methodname: methodname};

            var encoded = JSON.stringify( js_obj );

            var data= encoded;

            $.ajax({
                    url : "http://35.154.53.72/jobseeker/Fjapi",
                    data: data,
                    type: "POST",
                    success: function(response){
                        console.log(response);
                        item = $.parseJSON(response);
                        if(item.code == "200"){
                            alert('Email has been sent with further instructions to '+emailORmobile+' Please check your email.');
                            window.location="http://35.154.53.72";
                        }else{
                            alert(item.message);
                        }
                    }
            });
        });
        /* Login button click handler */
        $('.login-button').on('click', function() {
            bootbox
                .dialog({
                    title: '<img src="images/logo1.png" style="width:150px;">',
                    message: $('#loginForm'),
                    onEscape: function() {  $('#signupForm').hide().appendTo('body');},
                    backdrop: true,
                    show: false /* We will show it manually later */
                })
                .on('shown.bs.modal', function() {
                    $(".ban_content").hide();
                    $(".topnav").hide();
                    $('#loginForm')
                        .show();                             /* Show the login form */
                        //.formValidation('resetForm', true); /* Reset form */
                })
                .on('hide.bs.modal', function(e) {
                    /**
                     * Bootbox will remove the modal (including the body which contains the login form)
                     * after hiding the modal
                     * Therefor, we need to backup the form
                     */
                    $(".ban_content").show();
                    $(".topnav").show();
                    $('#loginForm').hide().appendTo('body');
                })
                .modal('show');
        });

        /**********Login functionality End**************/
        
        /* SignUp button click handler */
        $('.signup-button').on('click', function() {
            bootbox
                .dialog({
                    title: '<img src="images/logo1.png" style="width:150px;">',
                    message: $('#signupForm'),
                    onEscape: function() {  $('#signupForm').hide().appendTo('body');},
                    backdrop: true,
                    show: false /* We will show it manually later */
                })
                .on('shown.bs.modal', function() {
                    $(".ban_content").hide();
                    $(".topnav").hide();
                    $('#signupForm').show();                             /* Show the login form */
					//$('#fullname').attr("placeholder","color:red;");
                    
                        //.formValidation('resetForm', true); /* Reset form */
                })
                .on('hide.bs.modal', function(e) {
                    /**
                     * Bootbox will remove the modal (including the body which contains the login form)
                     * after hiding the modal
                     * Therefor, we need to backup the form
                     */
                    $(".ban_content").show();
                    $(".topnav").show();
                    $('#signupForm').hide().appendTo('body');
                    
                })
                .modal('show');
        });

        /**********SignUp functionality End**************/
        /* SignUp button click handler */
       

        /**********SignUp functionality End**************/
    });
var base_URL = $("#currentbase_url").val();
var jobCode = $("currentjobCode").val();
var tokenUser = $("#currentUserId").val();

var count = 0;


$(document).ready(function(){
    $('#dataTables-example').DataTable({
            responsive: true,
            lengthMenu: [ [-1], ["All"] ],
             paging: false
    });
	getTickers();

});

function getEducationalTImes(url){
    $('#NewsDetails').modal('hide');
    $('#EducationNews').modal('show');
    $('#EducationNewsBody').attr('src', url);

}

$("#ckbCheckAll").click(function () {
    $(".checkBoxClass").prop('checked', $(this).prop('checked'));
    
    var output = $.map($(':checkbox[name=notifyIds\\[\\]]:checked'), function(n, i){
            return n.value;
      }).join(',');
      
    if( output ){
        $('#hdnnotificationids').val(output);
    } else {
        $('#hdnnotificationids').val('');
    }
});

$(".checkBoxClass").click(function () {
    
    var output = $.map($(':checkbox[name=notifyIds\\[\\]]:checked'), function(n, i){
            return n.value;
      }).join(',');
      
    if( output ){
        $('#hdnnotificationids').val(output);
    } else {
        $('#hdnnotificationids').val('');
    }
});

//DELETE MESSAGE FROM SINGLE OR MULTIPLE SELECTION ON CHECKBOX
$('.delete-notifications').on('click', function () {
    var notificationsId = $('#hdnnotificationids').val();
    
    if(notificationsId != '') {
        bootbox.confirm('Are you sure you want to delete?', function (result) {

            if (result) {
                var token = 'ecbcd7eaee29848978134beeecdfbc7c';
                var methodname = 'deleteNotification';

                var js_obj = {notificationsId: notificationsId, token: token, methodname: methodname};
                var encoded = JSON.stringify( js_obj );
                var data = encoded;

                $.ajax({
                    url : base_URL+"Fjapi",
                    data:  data,
                    type: "POST",
                    success: function(response){
                        var item = $.parseJSON(response);
                        if(item.code == "200"){
                            
                            //$('#changepassword').hide();
                            //bootbox.hideAll();
                            //bootbox.alert("Selected notification deleted successfully!");
                             location.reload();
                            
                        }else{
                            bootbox.hideAll();
                            bootbox.alert(item.message);
                            //alert(item.message);
                        }
                    }
                });
            }
        });
    } else {
        bootbox.alert('Please select at least one notification to delete!');
    }

});


function getTickers(){
    
    var methodname = 'getTickerList';//getTickerList getTopTickers
    var token = 'ecbcd7eaee29848978134beeecdfbc7c';
    var userId = tokenUser;

    var js_obj = {userId: userId, token: token, methodname: methodname};

    var encoded = JSON.stringify( js_obj );

    var data= encoded;
    
    
}
    


var base_URL = $("#currentbase_url").val();
var jobCode = $("#currentjobCode").val();
var tokenUser = $("#currentuserId").val();

function saveForJob(){
    var methodname = 'saveJob';
    var verifyJobString = '1111';

    var js_obj = {token: 'ecbcd7eaee29848978134beeecdfbc7c',methodname: methodname,jobCode:jobCode,userId:tokenUser};

    var encoded = JSON.stringify( js_obj );

    var data= encoded;
    $.ajax({
              url : base_URL+"Fjapi",
              data: data,
              type: "POST",
              success: function(response){
                            item = $.parseJSON(response);
                            if(item.code == "200"){
                                    alert("Job is successfully saved. Later you can apply for this job from dashboard.");
                                    window.location = base_URL+"users";
                            }else{
                            }
              }
    });

}

function setUserJobProcessStatus(){
	var methodname = 'setUserJobProcessStatus';
	var userId = tokenUser;
	var status = '0';
	
	var js_obj = {token: 'ecbcd7eaee29848978134beeecdfbc7c',methodname: methodname,userId:userId, jobCode:jobCode, status:status};
	var encoded = JSON.stringify( js_obj );
	var data= encoded;
	$.ajax({
		  url : base_URL+"Fjapi",
		  data: data,
		  type: "POST",
		  success: function(response){
				item = $.parseJSON(response);
				if(item.code == '200'){
					if(item.status == '1'){
						
					}
				}
				
		  }
	});
	
}
$(document).ready(function() {
	getUserJobProcessStatus();
});


function getUserJobProcessStatus(){
	var methodname = 'getUserJobProcessStatus';
	var userId = tokenUser;
	
	var js_obj = {token: 'ecbcd7eaee29848978134beeecdfbc7c',methodname: methodname,userId:userId, jobCode:jobCode};
	var encoded = JSON.stringify( js_obj );
	var data= encoded;
	$.ajax({
		  url : base_URL+"Fjapi",
		  data: data,
		  type: "POST",
		  success: function(response){
				item = $.parseJSON(response);
				if(item.code == '200'){
					if(item.status == '0'){
						setUserJobProcessStatus();
					}else{
						alert("Sorry! Your job has been terminated. You can't give the interview again for the same job.");
						window.onbeforeunload = null;
						window.location = base_URL+"users";
					}
				}
				
		  }
	});
	
}
var base_URL = $("#currentbase_url").val();
var jobCode = $("#currentjobCode").val();
var tokenUser = $("#currentuserId").val();



function ShowAboutCompany(){
    $("#showRequirements").css("background","#d3d3d3");
    $("#showRequirements").css("color","black");

    $("#showJobDescription1").css("background","#d3d3d3");
    $("#showJobDescription1").css("color","black");

    $("#ShowAboutCompany").css("background","#29ABC1");
    $("#ShowAboutCompany").css("color","white");

    $("#showAboutUs").show();
    $("#companyVideoDiv").show();
    $("#Requirements").hide();
    $("#showJobDescription").hide();
    $("#screeningProcess").css("display","none");
}
    
    
function showRequirements(){
    $("#showRequirements").css("background","#29ABC1");
    $("#showRequirements").css("color","white");
    
    $("#showJobDescription1").css("background","#d3d3d3");
    $("#showJobDescription1").css("color","black");
    
    $("#ShowAboutCompany").css("background","#d3d3d3");
    $("#ShowAboutCompany").css("color","black");
            
    $("#showAboutUs").hide();
    $("#companyVideoDiv").hide();
    $("#Requirements").show();
    $("#showJobDescription").hide();
    $("#screeningProcess").hide();
    //$("#screeningProcess").css("display","block");
}
    
function showJobDescription(){
    $("#showRequirements").css("background","#d3d3d3");
    $("#showRequirements").css("color","black");

    $("#showJobDescription1").css("background","#29ABC1");
    $("#showJobDescription1").css("color","white");

    $("#ShowAboutCompany").css("background","#d3d3d3");
    $("#ShowAboutCompany").css("color","black");    
    $("#showAboutUs").hide();
    $("#companyVideoDiv").hide();
    $("#Requirements").hide();
    $("#showJobDescription").show();
    $("#screeningProcess").show();
    $("#screeningProcess").css("display","block");
        
}

if ($('#positionDivContainer')[0].scrollWidth >  $('#positionDivContainer').innerWidth()) {
    //Text has over-flowed
    var text = $('#positionDivContainer').text();
    $('#positionDivContainer').html("<marquee>"+text+"</marquee>");
}
if ($('#CompanyNameContainer')[0].scrollWidth >  $('#CompanyNameContainer').innerWidth()) {
    //Text has over-flowed
    var text = $('#CompanyNameContainer').text();
    $('#CompanyNameContainer').html("<marquee>"+text+"</marquee>");
}

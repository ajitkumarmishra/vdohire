/*
* This js is being used on edit profile page
*
*/

var base_URL = $("#currentbase_url").val();
	var tokenUser = $("#currentuserId").val();
	var userName = $("#currentUserName").val();
	
$(document).ready(function(){
    var WindowHeight = $(window).height();
	onLoad();
	var divheight = $("#mainBodyContent").height();
	$("#mainBodyContent").css("height", divheight+40+"px");
    checkUserMessageStatus();
    /*$("#navheight").css("height",(WindowHeight*6.5)/100);
    $("#tabs").css("height",(WindowHeight*13)/100);
    $("#homeHeading").css("height",(WindowHeight*3.5)/100);
    $("#mainBodyContent").css("height",(WindowHeight*72)/100);*/
    getTickers();
});

function getmessageDetailsById(id) {
    var js_obj = {token: 'ecbcd7eaee29848978134beeecdfbc7c',methodname: 'getMessageDetails',userId:tokenUser, messageId:id};
    var encoded = JSON.stringify( js_obj );
    var data= encoded;
    $.ajax({
          url : base_URL+"Fjapi",
          data: data,
          type: "POST",
          success: function(response){
                item = $.parseJSON(response);
                if(item.code == "200"){
                    $("#message_title").text(item.data.messageSubject);
                    $("#message_company_name").text(item.data.senderName);
                    $("#message_date_time").text(item.data.time);
                    var messageContent = item.data.message;
                    
                    $("#message_message").html(messageContent);
                }else{
                    alert(item.message);
                }
            }
    });
}
    
function checkUserMessageStatus(){
	
    var js_obj = {token: 'ecbcd7eaee29848978134beeecdfbc7c',methodname: 'checkWelcomeMessageDisplayed',userId:tokenUser};
    var encoded = JSON.stringify( js_obj );
    var data= encoded;
    $.ajax({
          url : base_URL+"Fjapi",
          data: data,
          type: "POST",
          success: function(response){
                item = $.parseJSON(response);
                if(item.code == "200"){
                    if(item.status == "0"){
                        document.getElementById('welcomeToFirstJob').click();
                    }
                }else{
                    
                }
            }
    });
}

	if(userName == "") {

    $(document).on('click', '.showmessages', function () {
        var messageSubject    = $(this).data('options').messageSubject;
            alert(messageSubject);
    });


       
    function setheightChart(){
        $("svg").attr("height","200");
    }

    function saveBasicInfo(){
        //var forData = $('#createSubuserForm').serialize();
        //var data= JSON.stringify($('#createSubuserForm').serializeObject());
        var userId = $("#userId").val();
        var token = $("#token").val();
        var methodname = $("#methodname").val();
        var name = $("#name").val();
        var email = $("#email").val();
        var mobile = $("#mobile").val();
        var gender = $("#gender").val();
        var dob = $("#datepicker-13").val();
        var pincode = $("#pincode").val();
        
        
        var js_obj = {userId: userId, token: token,methodname: methodname, basicInformation: [{fullname: name, email: email, mobile: mobile, gender: gender, dob: dob, pincode: pincode}] };

        var encoded = JSON.stringify( js_obj );

        var data= encoded;

        $.ajax({
              url : base_URL+"Fjapi",
              data: data,
              type: "POST",
              success: function(response){
                    item = $.parseJSON(response);
                    if(item.code == "200"){
                        var obj = JSON.parse(response);
                        var fullname = JSON.stringify(obj.basicInfo[0].fullname);
                        var email = JSON.stringify(obj.basicInfo[0].email);
                        var mobile = JSON.stringify(obj.basicInfo[0].mobile);
                        var dob = JSON.stringify(obj.basicInfo[0].dob);
                        var pincode = JSON.stringify(obj.basicInfo[0].pincode);
                        var gender = JSON.stringify(obj.basicInfo[0].gender);
                        var profilePercent = JSON.stringify(obj.basicInfo[0].profilePercent);
                        
                        $("#name").val(fullname.replace(/\"/g, ""));
                        $("#mobile").val(mobile.replace(/\"/g, ""));
                        $("#email").val(email.replace(/\"/g, ""));
                        $("#pincode").val(pincode.replace(/\"/g, ""));
                        $("#datepicker-13").val(dob.replace(/\"/g, ""));
                        $("#gender").val(gender.replace(/\"/g, ""));
                    }else{
                        alert(item.message);
                    }
              }
        });
    
    }
}


var count = 0;

function getEducationalTImes(url){
    
    var width = $( window ).width();
    var height = $( window ).height();
    
    var x = screen.width/2 - 800/2;
    var y = screen.height/2 - 800/2;
    
    myWindow = window.open(url, "", "width=800,height=800,left="+x+",top="+y);
    //$('#EducationNewsBody').attr('src', 'http://35.154.53.72/jobseeker/TestChart.php');
}

function getTickers(){
    
    var methodname = 'getTickerList';//getTickerList getTopTickers
    var token = 'ecbcd7eaee29848978134beeecdfbc7c';
    var userId = tokenUser;

    var js_obj = {userId: userId, token: token, methodname: methodname};

    var encoded = JSON.stringify( js_obj );

    var data= encoded;
   $.ajax({
            url : base_URL+"Fjapi",
            data: data,
            type: "POST",
            success: function(response){
                
                var obj = JSON.parse(response);
                
                if(obj.data[count].title == 'undefined'){
                    count = 0;
                }
                var Header = JSON.stringify(obj.data[count].title);
                Header = Header.replace(/\"/g, "");
                var Image = JSON.stringify(obj.data[count].thumbnail);
                Image = Image.replace(/\"/g, "");
                
                var URLWithQuotes = JSON.stringify(obj.data[count].link);
                URL = URLWithQuotes.replace(/\"/g, "");
                var Description = JSON.stringify(obj.data[count].description);
                var Description = Description.replace(/\"/g, "");
                var Description = Description.replace(/(?:\\[rn])+/g, "</br>");
                $(".newsHeader").html("<a href='javascript://' style='text-decoration:none;color:#08688F;'>"+Header+"</a>");
                URL = "'"+URL+"'";
                
                $(".newsImage").html("<img src='"+Image+"' style='width:100%;'>");
                $("#NewsTicker").html('<div><a href="javascript://"  onclick="getEducationalTImes('+URL+');" data-toggle="modal" style="color:#2590e6;text-decoration:none;"><i class="fa fa-newspaper-o" aria-hidden="true"></i>&nbsp;&nbsp;<u>'+Header+'</u></a></div><div id="newsTickerHover" style="">Latest Job News</div>');
                $(".newsBody").html(Description);
                count++;

            },
            complete: function(){
                setTimeout(getTickers, 15000);
            }
    });
}
function onLoad(){
        var ctx = document.getElementById("myChart");
        Chart.defaults.global.defaultFontColor = '#fff';

        var myChart = new Chart(ctx, {
            scaleFontColor: '#fff',
            type: 'bar',
            data: {
                labels: ["Applied", "Viewed", "Shortlisted", "Rejected"],
                datasets: [{
                    label: 'Jobs in 30 Days',
                    data: [$("#AppliedJobsCount").val(), $("#viewedJobs").val(),$("#shortlistedJobs").val(),$("#RejectedJobs").val()],
                    barThickness: 10,
                    legend: {
                        display: false
                    },

                    backgroundColor: [
                        'rgba(240, 107, 74, 1.0)',
                        'rgba(54, 162, 235,  1.0)',
                        'rgba(255, 206, 86,  1.0)',
                        'rgba(75, 192, 192,  1.0)'
                    ],
                    borderColor: [
                        'rgba(240, 107, 74,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)'
                    ],
                    borderWidth: 0
                }]
            },

            options: {
                legend: {
                    display: false
                },
                scales: {
                    xAxes: [{
                        gridLines: {
                            display: false,
                            color: "#FFF"
                        },
                        ticks: {
                            fontColor: "#FFF"
                        }
                    }],
                    yAxes: [{
                        gridLines: {
                            color: "#FFF"
                        },
                        ticks: {
                            beginAtZero:true,
                            stepSize: $("#YAxisCounter").val()
                        }
                    }]
                }
            }
        });
        //ctx.height = 500;
    }
function getJobDetailspAge(){
    var SearchedJobCode = $("#searchedJobCode").val();
    if(SearchedJobCode != ""){
        var js_obj = {token: 'ecbcd7eaee29848978134beeecdfbc7c', methodname: 'verifyJob', userId:tokenUser, referrenceJobId:SearchedJobCode, searchBy: 'jobSearch', deviceUniqueId: '11111'};
        var encoded = JSON.stringify( js_obj );
        var data= encoded;
        $.ajax({
              url : base_URL+"Fjapi",
              data: data,
              type: "POST",
              success: function(response){
                    item = $.parseJSON(response);
                    if(item.code == "200"){
                        window.location = base_URL+"job/details/"+SearchedJobCode;
                    }else{
                        alert('Either Job is closed or Invalid Job Code. Please use another job!');
                    }
                }
        });
    }else{
        $("#JobcodeValidation").html('<a href="#" class="close" data-dismiss="alert">&times;</a><strong>Error!</strong> Please Enter the Job Code.')
        $("#JobcodeValidation").show(1000);
    }
    
    setTimeout(function() {
    $("#JobcodeValidation").hide()
    }, 4000);
}


function ReplyTomessage(id){
	var js_obj = {token: 'ecbcd7eaee29848978134beeecdfbc7c',methodname: 'getMessageDetails',userId:tokenUser, messageId:id};
    var encoded = JSON.stringify( js_obj );
    var data= encoded;
    $.ajax({
          url : base_URL+"Fjapi",
          data: data,
          type: "POST",
          success: function(response){
                item = $.parseJSON(response);
                if(item.code == "200"){
                    $("#message_titleReply").html("<b>Subject: </b><span style='font-size:15px;'> Re:"+item.data.messageSubject+"</span>");
                    $("#message_company_nameReply").text(item.data.senderName);
                    $("#message_date_timeReply").text(item.data.time);
                    $("#message_id").val(id);
                    $("#senderId").val(item.data.senderId);
                    var messageContent = item.data.message;
                    
                    $("#message_messageReply").html(messageContent);
                }else{
                    alert(item.message);
                }
            }
    });
	
}

function replyTo(senderId){
	var id = $("#message_id").val();
	var senderId = $("#senderId").val();
	var message = $("#replyBox").val();
	var messageSubject = $("#message_titleReply").text();
	var js_obj = {token: 'ecbcd7eaee29848978134beeecdfbc7c',methodname: 'sendMessage',userId:tokenUser,toId:senderId,messageText:message,messageSubject:messageSubject, parentMessageId:id};
    var encoded = JSON.stringify( js_obj );
    var data= encoded;
    $.ajax({
          url : base_URL+"Fjapi",
          data: data,
          type: "POST",
          success: function(response){
                item = $.parseJSON(response);
                if(item.code == "200"){
                    window.location.reload();
                }else{
                    alert(item.message);
                }
            }
    });
}
/*
* This js is being used on edit basic profile page
*
*/

var base_URL = $("#currentbase_url").val();
var tokenUser = $("#currentuserId").val();

var EncodedContent = "";
var fileName = "";
var chunkSize = 70024;

$("#profileImage").change(function(){
        if (this.files && this.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                
                $('#uploadIcon').attr('src',e.target.result);
            };

            reader.readAsDataURL(this.files[0]);
        }
        
        $("#showUploadButton").css("display","block");
 });
 
 
$(document).ready(function(){
    $("#search-box").keyup(function(){
        var methodname = 'searchUniversity';
        var university = $("#search-box").val();
        var js_obj = {token: 'ecbcd7eaee29848978134beeecdfbc7c',methodname: methodname,universityName:university};

        var encoded = JSON.stringify( js_obj );

        var data= encoded;
        
        $.ajax({
                type: "POST",
                url : base_URL+"Fjapi",
                data: data,
                beforeSend: function(){
                    $("#search-box").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
                },
                success: function(response){
                    item = $.parseJSON(response);
                    if(item.code == "200"){
                        $("#suggesstion-box").html('');
                        for(var i=0; i<item.data.length; i++){
                                
                            var text = "'"+item.data[i].name+"'";
                            $("#suggesstion-box").show();
                            $("#suggesstion-box").append('<div><a href="javascript://" onclick="selectCountry('+text+','+item.data[i].id+')">'+text+'</a><div>');
                            $("#search-box").css("background","#FFF");
                            
                        }
                        
                    }

                }
        });
    });
});

$(document).ready(function(){
    $('input[name="isPresent"]').click(function(){
        if($(this).prop("checked") == true){
            $('#dateTo').val('');
            $("#dateTo").prop('disabled', true);
        }
        else if($(this).prop("checked") == false){
            $("#dateTo").prop('disabled', false);
        }
    });
});

$("#datepicker-13").datepicker( {
    minViewMode: 'years',
    autoclose: true,
    startDate: '1950',
    endDate: '+0d',
    format: 'yyyy'
});

$('#dateFrom').datepicker({
    autoclose: true,
    minViewMode: 'days',
    minViewMode: 'months',
    endDate: '+0d',
    format: 'M-yyyy'
});

$('#dateTo').datepicker({
    autoclose: true,
    minViewMode: 'days',
    minViewMode: 'months',
    endDate: '+0d',
    format: 'M-yyyy'
});

function selectCountry(val,id) {
    $("#search-box").val(val);
    $("#suggesstion-box").hide();
    $("#universityId").val(id);
}


    
    function uploadProfilePic(){
        //var data= JSON.stringify($('#uploadResume').serializeObject());
		var ext = $('#profileImage').val().split('.').pop().toLowerCase();
		if($.inArray(ext, ['jpg','jpeg','png']) == -1) {
			alert('Please select JPG, JPEG, PNG file.');
			return false;
		}
    
		
        var formData = new FormData();
        formData.append('resumeFile', $('#profileImage')[0].files[0]);
        fileName = $('#profileImage')[0].files[0].name;
        //alert($('#resumeFile')[0].files[0].size);
        
        var file = document.getElementById("profileImage").files[0];
		
		
    
        var reader = new FileReader();

        reader.readAsDataURL(file);
        reader.onload = function () {
            //console.log(reader.result);
            EncodedContent = reader.result.split(',')[1];
            readContent = 1;
        };
        reader.onloadend = function(){
            $("#myModal").css("display","block");
            $("#cover").css("display","block");
            $("#progress-circle").attr("class","progress-circle progress-50");
                    $("#percet_text").text(50);
            //console.log(EncodedContent);
            var len = EncodedContent.length;
            
            var start = 0;
            var end = chunkSize;
            var count = 1;
            var totalCount = Math.ceil(len/chunkSize);
            
            
            sendImage(EncodedContent,start,end,len,count,totalCount);
        }
        reader.onerror = function (error) {
            console.log('Error: ', error);
        };
    }
    
       
function sendImage(content,start,end,len,count,totalCount){
    
    if(end >= len){
        lastChunk = 1;
    }else{
        lastChunk = 0;
    }
    lastChunk = 1;
    
    var methodname = 'uploadUserImage';
    var js_obj = {token: 'ecbcd7eaee29848978134beeecdfbc7c',methodname: methodname,userId:tokenUser, imageName:fileName,fileFailChunk:0,isLastChunk:lastChunk, image:content };
    var encoded = JSON.stringify( js_obj );
    var dataToSend = encoded;
    
    var req;
    req = new XMLHttpRequest();

    var url = base_URL+"Fjapi";


    req.open('POST', url, true);
    req.setRequestHeader("Content-Type", "application/json");
    req.send(dataToSend);
    req.onreadystatechange = function () {
        if(req.responseText != ""){
            var response = JSON.parse(req.responseText);

            if (req.readyState == 4) {
                if (response.code == 200) {
                    var display = Math.ceil(100/totalCount)*count;
                    if(display > 100){
                        display = 100;
                    }
                     display = 100;
                    $("#progress-circle").attr("class","progress-circle progress-"+display);
                    $("#percet_text").text(display);
                    count++;
                    if(lastChunk == 1){
                        $("#myModal").css("display","none");
                        window.location.reload();
                        return false;
                    }
                    


                }else if (response.code == 503) {
                    //debugger;
                    sendImage(EncodedContent.slice(start, end),start,end,len,count,totalCount);
                }
            }
        }
    }
}
$('body').click(function() {
   $('#suggesstion-box').text('');
});


//Save Qualification
$(document).ready(function() {
    $('#addQualificationFrom')
    .find('[name="universityId"]')
        .chosen({
            width: '100%',
            inherit_select_classes: true
        })
        // Revalidate the color when it is changed
        .change(function(e) {
            $('#addQualificationFrom').formValidation('revalidateField', 'universityId');
        })
        .end()
    .formValidation({
        framework: 'bootstrap',
        excluded: ':disabled',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            courseId: {
                validators: {
                    notEmpty: {
                        message: 'Please select course'
                    }
                }
            },
            universityId: {
                validators: {
                    callback: {
                        message: 'Please select university',
                        callback: function(value, validator) {
                            // Get the selected options
                            var options = validator.getFieldElements('universityId').val();
                            return (options != null && options.length >= 1);
                        }
                    }
                }
            },
            institute: {
                validators: {
                    notEmpty: {
                        message: 'Please enter institute name'
                    },
                    stringLength: {
                        message: 'Institute name should have at least 3 characters',
                        min: 3
                    },
                    regexp: {
                        regexp: /^[a-z\s]+$/i,
                        message: 'The Institute name can consist of alphabetical characters and spaces only'
                    }
                }
            },
            completionYear: {
                validators: {
                    notEmpty: {
                        message: 'Please enter completion year'
                    },
                    digits: {
                        message: 'The completion year can contain digits only'
                    },
                    stringLength: {
                        message: 'Please enter a valid year',
                        min: 4,
                        max: 4
                    },
                }
            },
            percent: {
                validators: {
                    notEmpty: {
                        message: 'Please enter percent'
                    },
                    numeric: {
                        message: 'Please enter valid percentage',
                        decimalSeparator: '.'
                    },
                    stringLength: {
                        message: 'Please enter correct lenght',
                        min: 2,
                        max: 5
                    }
                }
            }
        }
    })
    
    .on('success.form.fv', function(e) {
        // Prevent form submission
        e.preventDefault();
        var data = JSON.stringify($('#addQualificationFrom').serializeObject());
        $.ajax({
            url : base_URL+"Fjapi",
            data: data,
            type: "POST",
            success: function(response){
                console.log(response);
                var item = $.parseJSON(response);
                if(item.code == "200"){
                    var redirectUrl = base_URL+"editbasicProfile?tab=menu2";
                    window.location = redirectUrl;
                    window.location.reload();
                }else{
                    alert(item.message);
                }
            }

        });
    });
});

//Save Qualification
$(document).ready(function() {
    $('#addEmploymentFrom').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            companyName: {
                validators: {
                    notEmpty: {
                        message: 'Please enter compnay name'
                    },
                    stringLength: {
                        message: 'Company name should have at least 3 characters',
                        min: 3
                    },
                    regexp: {
                        regexp: /^[A-Za-z\s]+$/i,
                        message: 'Company name can consist of alphabetical characters and spaces only'
                    }
                }
            },
            roleOrPosition: {
                validators: {
                    notEmpty: {
                        message: 'Please enter your role'
                    },
                    stringLength: {
                        message: 'Role/Position should have at least 3 characters',
                        min: 3
                    },
                    regexp: {
                        regexp: /^[A-Za-z\s]+$/i,
                        message: 'Role/Position can consist of alphabetical characters and spaces only'
                    }
                }
            },
            responsibilities: {
                validators: {
                    notEmpty: {
                        message: 'Please enter your responsibilities'
                    },
                    stringLength: {
                        message: 'responsibilities should have at least 3 characters',
                        min: 3
                    },
                }
            }
        }
    })
    
    .on('success.form.fv', function(e) {
        // Prevent form submission
        e.preventDefault();
        var data = JSON.stringify($('#addEmploymentFrom').serializeObject());
        $.ajax({
            url : base_URL+"Fjapi",
            data: data,
            type: "POST",
            success: function(response){
                console.log(response);
                var item = $.parseJSON(response);
                if(item.code == "200"){
                    var redirectUrl = base_URL+"editbasicProfile";
                    window.location = redirectUrl;
                    window.location.reload();
                }else{
                    alert(item.message);
                }
            }
        });
    });
});

//Save other information data
$(document).ready(function() {
    $('#otherInformationForm')
    .find('[name="preferredLocation"]')
        .chosen({
            width: '100%',
            inherit_select_classes: true
        })
        // Revalidate the color when it is changed
        .change(function(e) {
            $('#otherInformationForm').formValidation('revalidateField', 'preferredLocation');
        })
        .end()
    .formValidation({
        framework: 'bootstrap',
        excluded: ':disabled',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            preferredLocation: {
                validators: {
                    callback: {
                        message: 'Please choose your favourite locations',
                        callback: function(value, validator) {
                            // Get the selected options
                            var options = validator.getFieldElements('preferredLocation').val();
                            return (options != null && options.length >= 1);
                        }
                    }
                }
            },
            expectedCtcFrom: {
                validators: {
                    notEmpty: {
                        message: 'Please enter min salary expectation'
                    }
                }
            },
            expectedCtcTo: {
                validators: {
                    notEmpty: {
                        message: 'Please enter max salary expectation'
                    }
                }
            },
            workExperience: {
                validators: {
                    notEmpty: {
                        message: 'Please enter your experiance'
                    }
                }
            },
            adharCard: {
                validators: {
                    notEmpty: {
                        message: 'Please enter your aadhar card number'
                    },
                    digits: {
                        message: 'The Aadhar Card can contain digits only'
                    },
                    stringLength: {
                        message: 'Please enter a valid Aadhar number',
                        min: 12,
                        max: 12
                    },
                }
            },
            languages: {
                validators: {
                    notEmpty: {
                        message: 'Please enter your perferred languages'
                    },
                    regexp: {
                        regexp: /^[A-Za-z,\s]+$/i,
                        message: 'Please enter your languages with comma separated'
                    }
                }
            }
        }
    })
    
    .on('success.form.fv', function(e) {
        // Prevent form submission
        e.preventDefault();
        var data = JSON.stringify($('#otherInformationForm').serializeObject());
        $.ajax({
            url : base_URL+"Fjapi",
            data: data,
            type: "POST",
            success: function(response){
                console.log(response);
                var item = $.parseJSON(response);
                if(item.code == "200"){
                    var redirectUrl = base_URL+"editbasicProfile";
                    window.location = redirectUrl;
                    window.location.reload();
                }else{
                    alert(item.message);
                }
            }
        });
    });
});

//Save other information data
$(document).ready(function() {
    $('#socialInformationForm').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            facebookLink: {
                validators: {
                    callback: {
                        message: 'Please enter valid facebook url',
                        callback: function(value, validator) {
                            if(value=='') {
                                return true;
                            } else {
                                var fbRegex = "(?:(?:http|https):\\/\\/)?(?:www.)?facebook.com\\/(?:(?:\\w)*#!\\/)?(?:pages\\/)?(?:[?\\w\\-]*\\/)?(?:profile.php\\?id=(?=\\d.*))?([\\w\\-]*)?";
                                if (value.match(fbRegex)) {
                                    return true;
                                } else {
                                    return false;
                                }
                            }
                        }
                    }
                }
            },
            linkedInLink: {
                validators: {
                    callback: {
                        message: 'Please enter your valid linkdin link',
                        callback: function(value, validator) {
                            if(value=='') {
                                return true;
                            } else {
                                var linkedInRegex = "^https?://((www|\\w\\w)\\.)?linkedin.com/((in/[^/]+/?)|(pub/[^/]+/((\\w|\\d)+/?){3}))$";
                                if (value.match(linkedInRegex)) {
                                    return true;
                                } else {
                                    return false;
                                }
                            }
                        }
                    }
                }
            },
            twitterLink: {
                validators: {
                    callback: {
                        message: 'Please enter your valid twitter link',
                        callback: function(value, validator) {
                            if(value=='') {
                                return true;
                            } else {
                                var twitterRegex = "/http(?:s)?:\\/\\/(?:www\\.)?twitter.com\\/[a-zA-Z0-9]/";
                                if (value.match(twitterRegex)) {
                                    return true;
                                } else {
                                    return false;
                                }
                            }
                        }
                    }
                }
            },
            googlePlusLink: {
                validators: {
                    callback: {
                        message: 'Please enter your valid googlePlus link',
                        callback: function(value, validator) {
                            if(value=='') {
                                return true;
                            } else {
                                var googlePlusRegex = "((http|https):\\/\\/)?(www[.])?plus\\.google\\.com\\/.?\\/?.?\\/?([0-9]*)";
                                if (value.match(googlePlusRegex)) {
                                    return true;
                                } else {
                                    return false;
                                }
                            }
                        }
                    }
                }
            }
        }
    })
    
    .on('success.form.fv', function(e) {
        // Prevent form submission
        e.preventDefault();
        var data = JSON.stringify($('#socialInformationForm').serializeObject());
        console.log(data);
        $.ajax({
            url : base_URL+"Fjapi",
            data: data,
            type: "POST",
            success: function(response){
                console.log(response);
                var item = $.parseJSON(response);
                if(item.code == "200"){
                    var redirectUrl = base_URL+"editbasicProfile";
                    window.location = redirectUrl;
                    window.location.reload();
                }else{
                    alert(item.message);
                }
            }
        });
    });
});

//Use to delete the specifc educational qualification
function deleteQualification(id){
    var methodname = 'removeQualification';
    var js_obj = {token: 'ecbcd7eaee29848978134beeecdfbc7c', methodname: methodname, userId: tokenUser, id: id};
    var encoded = JSON.stringify( js_obj );
    var data = encoded;
    
    bootbox.confirm({
        title: "Delete Educational Qualification?",
        message: "Do you want to delete this qualification? This cannot be undone.",
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if(result == true) {
                $.ajax({
                    url : base_URL+"Fjapi",
                    data: data,
                    type: "POST",
                    success: function(response){
                        var item = $.parseJSON(response);
                        if(item.code == "200"){
                            var redirectUrl = base_URL+"editbasicProfile";
                            window.location = redirectUrl;
                            window.location.reload();
                        }else{
                            alert(item.message);
                        }
                    }
                });
            }
        }
    });
}

//Use to delete the specifc educational qualification
function editQualification(id){
    var methodname = 'getEducationDetails';
    var js_obj = {token: 'ecbcd7eaee29848978134beeecdfbc7c', methodname: methodname, userId: tokenUser, id: id};
    var encoded = JSON.stringify( js_obj );
    var data = encoded;
    
    $.ajax({
        url : base_URL+"Fjapi",
        data: data,
        type: "POST",
        success: function(response){
            var item = $.parseJSON(response);
            //alert(item.educationalInfo['courseName']);
            if(item.code == "200"){
                //$("#universityId option[value="+item.educationalInfo['universityId']+"]").attr("selected", "selected");
                $('#universityId').val(item.educationalInfo['universityId']);
                $('#universityId').trigger("chosen:updated");
                $('#courseId').val(item.educationalInfo['courseId']);
                $("#institute").val(item.educationalInfo['institute']);
                $("#completionYear").val(item.educationalInfo['completionYear']);
                $("#percent").val(item.educationalInfo['percent']);
                $("#educationId").val(item.educationalInfo['id']);
                //$("#addEducationModal").find('.text-center').text('Edit Qualification');
                $("#addEducationModal").modal("show");
            }else{
                alert(item.message);
            }
        }
    });
}

//Use to edit the specific employment information
function editEmployment(id){
    var methodname = 'getEmploymentDetails';
    var js_obj = {token: 'ecbcd7eaee29848978134beeecdfbc7c', methodname: methodname, userId: tokenUser, id: id};
    var encoded = JSON.stringify( js_obj );
    var data = encoded;
    
    $.ajax({
        url : base_URL+"Fjapi",
        data: data,
        type: "POST",
        success: function(response){
            var item = $.parseJSON(response);
            //alert(item.employmentInfo['companyName']);
            if(item.code == "200"){
                $('#companyName').val(item.employmentInfo['companyName']);
                $("#roleOrPosition").val(item.employmentInfo['roleOrPosition']);
                $("#dateFrom").val(item.employmentInfo['dateFrom']);
                $("#dateTo").val(item.employmentInfo['dateTo']);
                $("#responsibilities").val(item.employmentInfo['responsibilities']);
                //$("#isPresent").val(item.employmentInfo['isPresent']);
                $("#employmentId").val(item.employmentInfo['id']);

                if(item.employmentInfo['isPresent'] == 1) {
                    $('#isPresent').prop('checked', true);
                }

                //$("#addEmploymentHistoryModal").find('.text-center').text('Edit Employment History');
                $("#addEmploymentHistoryModal").modal("show");
            }else{
                alert(item.message);
            }
        }
    });
}

//Use to delete the specifc educational qualification
function deleteEmployment(id){
    var methodname = 'removeEmployment';
    var js_obj = {token: 'ecbcd7eaee29848978134beeecdfbc7c', methodname: methodname, userId: tokenUser, id: id};
    var encoded = JSON.stringify( js_obj );
    var data = encoded;
    
    bootbox.confirm({
        title: "Delete Employment?",
        message: "Do you want to delete this employment details? This cannot be undone.",
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if(result == true) {
                $.ajax({
                    url : base_URL+"Fjapi",
                    data: data,
                    type: "POST",
                    success: function(response){
                        var item = $.parseJSON(response);
                        if(item.code == "200"){
                            var redirectUrl = base_URL+"editbasicProfile";
                            window.location = redirectUrl;
                            window.location.reload();
                        }else{
                            alert(item.message);
                        }
                    }
                });
            }
        }
    });
}


$('[data-dismiss=modal]').on('click', function (e) {
    var $t = $(this),
        target = $t[0].href || $t.data("target") || $t.parents('.modal') || [];

  $(target)
    .find("input[type=text],input[type=textarea],input[type=select], input[name=id]")
       .val('')
       .end()
    .find("select")
       .val('').trigger('chosen:updated')
       .end()
    .find("input[type=checkbox], input[type=radio]")
       .prop("checked", "")
       .end();
});

$('[data-backdrop=modal]').on('click', function (e) {
  alert("dsad");
});
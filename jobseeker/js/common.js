
var base_URL = $("#commonBaseUrl").val();
var tokenUser = $("#currentuserId").val();

$(document).ready(function(){
    $.fn.serializeObject = function()
    {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
    

});

function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}
function validateEmail(Email) {
    var pattern = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

    return $.trim(Email).match(pattern) ? true : false;
}

function saveBasicInfo(){
        //var forData = $('#createSubuserForm').serialize();
        //var data= JSON.stringify($('#createSubuserForm').serializeObject());
        var userId = $("#userId").val();
        var token = $("#token").val();
        var error = 0;
        var methodname = $("#methodnameForBasic").val();
        var name = $("#name").val();
        if(name == ""){
            $("#nameError").css("display","block");
            $("#nameError").text("Please Enter Name.");
            error = 1;
        }else{
			$("#nameError").css("display","none");
		}
		if(name.length <= 2){
            $("#nameError").css("display","block");
            $("#nameError").text("Name should be more than 2 characters.");
            error = 1;
        }else{
			$("#nameError").css("display","none");
		}
		
		if(!name.match(/^[a-zA-Z ]*$/)){
            $("#nameError").css("display","block");
            $("#nameError").text("Please Enter Valid Name.");
            error = 1;
        }else{
			$("#nameError").css("display","none");
		}
		
		
		
		var mobile = $("#mobile").val();
		if(mobile == ""){
            $("#mobileError").css("display","block");
			$("#mobileError").text("Please Enter Mobile No.");
            error = 1;
        }else{
			$("#mobileError").css("display","none");
		}
		if(!mobile.match(/(7|8|9)\d{9}/)){
            $("#mobileError").css("display","block");
            $("#mobileError").text("Please Enter Valid Mobile No.");
            error = 1;
        }else{
			$("#mobileError").css("display","none");
		}
        var email = $("#email").val();
		
        if( !validateEmail(email) ) { 
			$("#emailError").css("display","block");
            error = 1;
		}else{
			$("#emailError").css("display","none");
		}
		
        var atpos = email.indexOf("@");
        var dotpos = email.lastIndexOf(".");
		
		var spacePos = email.lastIndexOf(" ");
		
        if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length || spacePos>0) {
            $("#emailError").css("display","block");
            error = 1;
        }else{
			$("#emailError").css("display","none");
		}
		
        var pincode = $("#pincode").val();
        if(pincode != ""){
            var pat=/^[1-9][0-9]{5}$/;
            if(!pat.test(pincode)){
                $("#pinCodeError").css("display","block");
                $("#pinCodeError").text("Please enter valid pin code.");
                error = 1;
            } else {
                $("#pinCodeError").css("display","none");
            }
        }else{
            $("#pinCodeError").css("display","none");
        }

        if(error == 1){
            return false;
        }

        var gender = $("#gender").val();
        var dob = $("#datepicker-13").val();

        var js_obj = {userId: userId, token: token,methodname: methodname, fullname: name, email: email, mobile: mobile, gender: gender, dob: dob, pincode: pincode};

        var encoded = JSON.stringify( js_obj );

        var data= encoded;

        $.ajax({
              url : base_URL+"Fjapi",
              data: data,
              type: "POST",
              success: function(response){
                    item = $.parseJSON(response);
                    if(item.code == "200"){
                        alert('Your profile has been updated successfully.');
                        window.location.reload();
                    }else{
                        alert(item.message);
                    }
              }
        });
    }

	   
$(document).ready(function() {
    $('#changepassword').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            oldPassword: {
                validators: {
                    notEmpty: {
                        message: 'The username is required'
                    }
                }
            },
            newPassword: {
                validators: {
                    notEmpty: {
                        message: 'The password is required'
                    },
                    stringLength: {
                        min: 6,
                        max: 30,
                        message: 'The password must be more than 6 characters long'
                    },
                    different: {
                        field: 'oldPassword',
                        message: 'The New password cannot be the same as Old password'
                    }
                }
            },
            confirmNewPassword: {
                validators: {
                    notEmpty: {
                        message: 'The password is required'
                    },
                    stringLength: {
                        min: 6,
                        max: 30,
                        message: 'The password must be more than 6 characters long'
                    },
                    different: {
                        field: 'oldPassword',
                        message: 'The New password cannot be the same as Old password'
                    },
                    identical: {
                        field: 'newPassword',
                        message: 'The password and its confirm must be the same'
                    }
                }
            }
        }
    })
    
    .on('success.form.fv', function(e) {
        // Prevent form submission
        e.preventDefault();
        var data= JSON.stringify($('#changepassword').serializeObject());

        $.ajax({
            url : base_URL+"Fjapi",
            data: data,
            type: "POST",
            success: function(response){
                var item = $.parseJSON(response);
                if(item.code == "200"){
                    
            		//$('#changepassword').hide();
                    bootbox.hideAll();
                    bootbox.alert("Password changed successfully!");
                    
                }else{
                    bootbox.hideAll();
                    bootbox.alert(item.message);
                    //alert(item.message);
                }
            }
        });
    });
        
    /**********Login functionality**************/
    
    $('.changePassword').on('click', function() {
        bootbox
            .dialog({
                title: 'Change Password',
                message: $('#changepassword'),
                onEscape: function() {  $('#changepassword').hide().appendTo('body');},
                backdrop: true,
                show: false /* We will show it manually later */
            })
            .on('shown.bs.modal', function() {
				$(".modal-dialog").css("width","60%");
                $('#changepassword').show();                             /* Show the login form */
                    //.formValidation('resetForm', true); /* Reset form */
            })
            .on('hide.bs.modal', function(e) {
                $('#changepassword').data('formValidation').resetForm(true);
                $('#changepassword div').removeClass('has-error');
                $('#changepassword div').removeClass('has-success');
                $('#changepassword i').removeClass('glyphicon-ok');
                $('#changepassword i').removeClass('glyphicon-remove');
                $('#changepassword .help-block').remove();

                /**
                 * Bootbox will remove the modal (including the body which contains the login form)
                 * after hiding the modal
                 * Therefor, we need to backup the form
                 */
				 
                $('#changepassword').hide().appendTo('body');
            })
            .modal('show');
    });
});

//Search jobs code starting
$(document).ready(function() {
    
    $("#showsearchedJobsOptionbutton").click(function(e){

        var searchKeyword = $("input[name='searchKeyword']").val();
        var searchYearsFrom =  $("#searchYearsFrom option:selected" ).val();
        //var searchMonth =  $( "#searchMonth option:selected" ).val();
        var searchExpectedSalary =  $( "#searchExpectedSalary option:selected" ).val();
        var locationNameLength = '';
        $('select[name^="locationName"]').each(function() {
            locationNameLength = $(this).val().length;
        });

        if(locationNameLength > 0 || searchKeyword || searchYearsFrom || searchExpectedSalary) {
            $("#searchJobsForJobseeker").submit();
        } else {
            alert('Please select at least one field to search job!');
        }

    });
        
    /**********Search Jobs functionality**************/
    
    $('.searchJobsForJobseeker').on('click', function() {

        var methodname = 'getRecentJobSearchList';
        var token = 'ecbcd7eaee29848978134beeecdfbc7c';
        var userId = tokenUser;
        var js_obj = {userId: userId, token: token, methodname: methodname};
        var encoded = JSON.stringify( js_obj );


        $.ajax({
            url : base_URL+"Fjapi",
            data: encoded,
            type: "POST",
            success: function(response){
                var item = $.parseJSON(response);
                if(item.code == "200"){
                    if(item.data.length > 0) {
                        $("#searchJobContent ul").text('');
                        $("#searchJobContent h4").text('Recent Searches');
                        $.each( item.data, function( i, val ) {
                            var content = '';
                            var query = '?';
                            if(val['searchKeyword'] != null && val['searchKeyword'] != "") {
                                content = content.concat(val['searchKeyword']);
                                query = query.concat('searchKeyword='+val['searchKeyword']);
                            }

                            if(val['locations'] != null && val['locations'] != "") {
                                if(val['searchKeyword'] != null && val['searchKeyword'] != "") {
                                    query = query.concat('&locationName='+val['locations']);
                                    content = content.concat(', '+val['locations']);
                                } else {
                                    query = query.concat('locationName='+val['locations']);
                                    content = content.concat(val['locations']);
                                }
                            }

                            if(val['experiance'] != null && val['experiance'] != "") {
                                if((val['searchKeyword'] != null && val['searchKeyword'] != "") || (val['locations'] != null && val['locations'] != "")) {
                                    query = query.concat('&yearsFrom='+val['experiance']);
                                    content = content.concat(', '+val['experiance']+' yr');
                                } else {
                                    query = query.concat('yearsFrom='+val['experiance']);
                                    content = content.concat(val['experiance']+' yr');
                                }
                            }
                            if(val['salaryRange'] != null && val['salaryRange'] != "") {
                                if((val['searchKeyword'] != null && val['searchKeyword'] != "") || (val['locations'] != null && val['locations'] != "") || (val['experiance'] != null && val['experiance'] != "")) {
                                    query = query.concat('&expectedSalary='+val['salaryRange']);

                                    if(val['salaryRange'] > 0) {
                                        content = content.concat(', '+val['salaryRange'].slice(0,-5)+' lakh');
                                    } else {
                                        content = content.concat(', <1 lakh');
                                    }
                                } else {
                                    query = query.concat('expectedSalary='+val['salaryRange']);

                                    if(val['salaryRange'] > 0) {
                                        content = content.concat(val['salaryRange'].slice(0,-5)+' lakh');
                                    } else {
                                        content = content.concat('<1 lakh');
                                    }
                                }
                            }


                            /*var content = ((val['searchKeyword'] != "") || (val['searchKeyword'] != 'null')) ? val['searchKeyword'] : "";
                            content = content.concat((val['locations'] != "" || val['locations'] != 'null') ? ', '+val['locations'] : "");
                            content = content.concat((val['experiance'] != "" || val['experiance'] != 'null') ? ', '+val['experiance'] : "");
                            content = content.concat((val['salaryRange'] != "" || val['salaryRange'] != 'null') ? ', '+val['salaryRange'] : "");*/
                            $("#searchJobContent ul").append('<li title="'+content+'" style="list-style-type: none; margin-bottom: 5px;"><a href="/jobseeker/job/searched-jobs'+query+'" style="color:#09c; text-decoration: none;"><span class="tab">'+content+'</span></a></li>');
                        });
                    }
                }
            }
        });

        bootbox.dialog({
            title: 'Search Jobs',
            message: $('#searchJobsForJobseeker'),
            onEscape: function() {  $('#searchJobsForJobseeker').hide().appendTo('body');},
            backdrop: true,
            show: false /* We will show it manually later */
        })
        .on('shown.bs.modal', function() {
            $(".modal-dialog").css("width","60%");
            $('#searchJobsForJobseeker').show();                             /* Show the login form */
                //.formValidation('resetForm', true); /* Reset form */
        })
        .on('hide.bs.modal', function(e) {
            /**
             * Bootbox will remove the modal (including the body which contains the login form)
             * after hiding the modal
             * Therefor, we need to backup the form
             */
            $('#searchJobsForJobseeker').hide().appendTo('body');
        })
        .modal('show');
    });
});

var config = {
	'.chosen-select': {},
	'.chosen-select-deselect': {allow_single_deselect: true},
	'.chosen-select-no-single': {disable_search_threshold: 10},
	'.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
	'.chosen-select-width': {width: "95%"}
	}
	for (var selector in config) {
		$(selector).chosen(config[selector]);
	}
	
	function showCOntentURL (url){

		 $.ajax({
			  url : url,
			  type: "POST",
			  success: function(response){
					$("#popupContentEditProfile").html(response)
				}
		});
	}

	function deleteAuditions(id){
		var methodname = 'auditionDelete';
		var userId = $("#currentuserId").val();
		var result = confirm("Are you sure you want to delete your Video Resume?");
		if(result == false){
			return false;
		}
		var js_obj = {token: 'ecbcd7eaee29848978134beeecdfbc7c',methodname: methodname,userId:userId, auditionId:id};
		var encoded = JSON.stringify( js_obj );
		var data= encoded;
		$.ajax({
			  url : base_URL+"Fjapi",
			  data: data,
			  type: "POST",
			  success: function(response){
					item = $.parseJSON(response);
					if(item.code == '200'){
						alert("Audition has been deleted Successfully.");
						$("#auditionAvailable").html("<div style='text-align: center;'>No Audition Available!</div>");
						
						//window.location.reload();
					}
					
			  }
		});
		
		
	}
		

	function showAuditionForQuestion(){
		$("#iconLoader").css("display","block");
		var q1 = $("#questionDropdown").val();
		var page = $("#pageNumber").val();
		$("#audition_0").css("display","none");
		$("#audition_1").css("display","none");
		$("#audition_2").css("display","none");
		$("#audition_3").css("display","none");
		$("#audition_"+q1).css("display","block");
		
		$("#auditionVideo_0").trigger('pause');
		$("#auditionVideo_1").trigger('pause');
		$("#auditionVideo_2").trigger('pause');
		$("#auditionVideo_3").trigger('pause');
		$("#auditionVideo_"+q1).trigger('play');

	}

	$( document ).ready(function() {
		
		$('#messageModal').on('hide.bs.modal', function (e) {
			$("#auditionVideoPlayer").attr("src","");
		})
	});

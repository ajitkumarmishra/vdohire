/*
* This js is being used on edit profile page
*
*/

var base_url = $("#currentbase_url").val();

$(document).ready(function(){
    
    $("#editProfileContinue").click(function(){
        var radioValue = $("input[name='editProfile']:checked").val();

        if(radioValue == 'manuallyEditProfile'){
            window.location.href = base_url+'editbasicProfile';
        } else if(radioValue == 'uploadResumeDocument') {
            window.location.href = base_url+'resumeUpload';
        } else if(radioValue == 'sendResumeDocumentByEmail') {
            window.location.href = base_url+'sendDucumentByEmail';
        } else {
            window.location.href = '';
        }
    });
});
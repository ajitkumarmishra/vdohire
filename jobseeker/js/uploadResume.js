var base_URL = $("#currentbase_url").val();
var jobCode = $("#currentjobCode").val();
var tokenUser = $("#currentuserId").val();

var EncodedContent = "";
var fileName = "";
var chunkSize = 70024;

var fjCode = jobCode;
var methodname = 'editResume_v2';

jQuery(function () {

  //$(document).keydown(function (evt) {
  //  if (evt.keyCode == 116) { // down arrow
  //     alert("Your interview will be cancelled"); // prevents the usual scrolling behaviour
  //     window.location = base_URL+"users";
  //  }
  //});

    window.onbeforeunload = function (e) {
    	e = e || window.event;

    	// For IE and Firefox prior to version 4
    	if (e) {
    		e.returnValue = 'Sure?';
    	}

    	// For Safari
    	return 'Sure?';
            window.location = base_URL+"users";
    };
});
	

$(document).ready(function() {
	getUserJobProcessStatus();
});

function setUserJobProcessStatus(){
	var methodname = 'setUserJobProcessStatus';
	var userId = tokenUser;
	var status = 4;
	
	var js_obj = {token: 'ecbcd7eaee29848978134beeecdfbc7c',methodname: methodname,userId:userId, jobCode:jobCode, status:status};
	var encoded = JSON.stringify( js_obj );
	var data= encoded;
	$.ajax({
		  url : base_URL+"Fjapi",
		  data: data,
		  type: "POST",
		  success: function(response){
				item = $.parseJSON(response);
				if(item.code == '200'){
					if(item.status == '1'){
						
					}
				}
				
		  }
	});
	
}

function getUserJobProcessStatus(){
	var methodname = 'getUserJobProcessStatus';
	var userId = tokenUser;
	
	var js_obj = {token: 'ecbcd7eaee29848978134beeecdfbc7c',methodname: methodname,userId:userId, jobCode:jobCode};
	var encoded = JSON.stringify( js_obj );
	var data= encoded;
	$.ajax({
		  url : base_URL+"Fjapi",
		  data: data,
		  type: "POST",
		  success: function(response){
				item = $.parseJSON(response);
				if(item.code == '200'){
					if(item.status == '3' || item.status == '1'){
						setUserJobProcessStatus();
					}else if(parseInt(item.status) < 3){
						alert("Sorry! Your job has been terminated. You can't give the interview again for the same job.");
						window.onbeforeunload = null;
						window.location = base_URL+"users";
					}
				}
				
		  }
	});
	
}
	
$("#resumeFile").change(function(){
	$("#changepasswordbutton").css("display","block");
});

$("#resumeUploadSkip").click(function(){
    window.onbeforeunload = null;
    window.location = base_URL+"thankyou";
});
 
 
 
function sendResumeThis(resumeId){
	//var forData = $('#createSubuserForm').serialize();
        //var data= JSON.stringify($('#createSubuserForm').serializeObject());
        var userId = $("#userId").val();
        var token = $("#token").val();
        var methodname = 'setJobResume';
        
        var js_obj = {userId: userId, token: token,methodname: methodname,resumeId:resumeId,jobCode:jobCode};

		var encoded = JSON.stringify( js_obj );

		var data= encoded;

        
        $.ajax({
              url : base_URL+"Fjapi",
              data: data,
              type: "POST",
              success: function(response){
                    item = $.parseJSON(response);
                    if(item.code == "200"){
						window.onbeforeunload = null;
                        alert('Your resume has been uploaded. Thanks');
                        window.location = base_URL+"thankyou";
                    } else{
                        alert(item.message);
                    }
              }
        });
	
}

 
function Upload(){
    //var data= JSON.stringify($('#uploadResume').serializeObject());

    var formData = new FormData();
    formData.append('resumeFile', $('#resumeFile')[0].files[0]);
    fileName = $('#resumeFile')[0].files[0].name;
	
	var ext = $('#resumeFile').val().split('.').pop().toLowerCase();
	if($.inArray(ext, ['pdf','doc','docx']) == -1) {
		alert('Please select Doc, Docx or PDF file.');
		return false;
	}
	
    //alert($('#resumeFile')[0].files[0].size);
    
    var file = document.getElementById("resumeFile").files[0];

    var reader = new FileReader();

    reader.readAsDataURL(file);
    reader.onload = function () {
        //console.log(reader.result);
        EncodedContent = reader.result.split(',')[1];
        readContent = 1;
    };
    reader.onloadend = function(){
        $("#myModal").css("display","block");
        //console.log(EncodedContent);
        var len = EncodedContent.length;
        
        var start = 0;
        var end = chunkSize;
        var count = 1;
        var totalCount = Math.ceil(len/chunkSize);
        
        
        sendBytes(EncodedContent.slice(start, end),start,end,len,count,totalCount);
    }
    reader.onerror = function (error) {
        console.log('Error: ', error);
    };

}



   
function sendBytes(content,start,end,len,count,totalCount){
    
    if(end >= len){
        lastChunk = 1;
    }else{
        lastChunk = 0;
    }
    var js_obj = {token: 'ecbcd7eaee29848978134beeecdfbc7c',methodname: methodname,userId:tokenUser, resumeName:fileName,fileFailChunk:0,isLastChunk:lastChunk, resumeFile:content, fjCode: fjCode };
    var encoded = JSON.stringify( js_obj );
    var dataToSend = encoded;
    
    var req;
    req = new XMLHttpRequest();

    var url = base_URL+"Fjapi";


    req.open('POST', url, true);
    req.setRequestHeader("Content-Type", "application/json");
    req.send(dataToSend);
    req.onreadystatechange = function () {
        if(req.responseText != ""){
            var response = JSON.parse(req.responseText);

            if (req.readyState == 4) {
                if (response.code == 200) {
                    var display = Math.ceil(100/totalCount)*count;
                    if(display > 100){
                        display = 100;
                    }
                    $("#progress-circle").attr("class","progress-circle progress-"+display);
                    $("#percet_text").text(display);
                    count++;
                    if(lastChunk == 1){
                        $("#myModal").css("display","none");
						window.onbeforeunload = null;
                        alert('Your resume has been uploaded. Thanks!');
						window.location = base_URL+"thankyou";
                        return false;
                    }
                    if (end <= len){
                        start = end;
                        end = start + chunkSize;
                        sendBytes(EncodedContent.slice(start, end),start,end,len,count,totalCount);
                    }


                }else if (response.code == 503) {
                    //debugger;
                    sendBytes(EncodedContent.slice(start, end),start,end,len,count,totalCount);
                }
            }
        }
    }
}


function currentTime(){
    
    var d = new Date();

    d = d.getFullYear() + "-" + ('0' + (d.getMonth() + 1)).slice(-2) + "-" + ('0' + d.getDate()).slice(-2) + " " + ('0' + d.getHours()).slice(-2) + ":" + ('0' + d.getMinutes()).slice(-2) + ":" + ('0' + d.getSeconds()).slice(-2);
    return d;
}


function sleep(miliseconds) {
   var currentTime = new Date().getTime();

   while (currentTime + miliseconds >= new Date().getTime()) {
   }
}
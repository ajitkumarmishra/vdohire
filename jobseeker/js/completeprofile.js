/*
* This js is being used on complete profile page
*
*/

var base_URL = $("#currentbase_url").val();

$(document).ready(function(){
    $("#search-box").keyup(function(){
        var methodname = 'searchUniversity';
        var university = $("#search-box").val();
        var js_obj = {token: 'ecbcd7eaee29848978134beeecdfbc7c',methodname: methodname,universityName:university};

        var encoded = JSON.stringify( js_obj );

        var data= encoded;
        
        
        
        $.ajax({
                type: "POST",
                url : base_URL+"Fjapi",
                data: data,
                beforeSend: function(){
                    $("#search-box").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
                },
                success: function(response){
                    item = $.parseJSON(response);
                    if(item.code == "200"){
                        $("#suggesstion-box").html('');
                        for(var i=0; i<item.data.length; i++){
                                
                            var text = "'"+item.data[i].name+"'";
                            $("#suggesstion-box").show();
                            $("#suggesstion-box").append('<div><a href="javascript://" onclick="selectCountry('+text+','+item.data[i].id+')">'+text+'</a><div>');
                            $("#search-box").css("background","#FFF");
                            
                        }
                        
                    }

                }
        });
    });
});

function selectCountry(val,id) {
    $("#search-box").val(val);
    $("#suggesstion-box").hide();
    $("#universityId").val(id);
}

function completeInfo(){
    //var forData = $('#createSubuserForm').serialize();
    //var data= JSON.stringify($('#createSubuserForm').serializeObject());
    var userId = $("#userId").val();
    var token = $("#token").val();
    var methodname = $("#methodname").val();
    var name = $("#name").val();
    var passwordVal = $("#password").val();
    var mobile = $("#mobile").val();
    var confirmVal = $("#confirm").val();
    
    
    var js_obj = {userId: userId, token: token,methodname: methodname,fullname: name, mobile: mobile, password: passwordVal, confirm: confirmVal};

    var encoded = JSON.stringify( js_obj );

    var data= encoded;

    $.ajax({
          url : base_URL+"Fjapi",
          data: data,
          type: "POST",
          success: function(response){
                item = $.parseJSON(response);
                if(item.code == "200"){
                    window.location = base_URL+"users";
                }else{
                    alert(item.message);
                }
          }
    });
}
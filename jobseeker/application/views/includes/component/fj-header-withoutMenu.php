<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="refresh" content="IE=edge" content="10">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
		
<link rel="shortcut icon" href="https://vdohire.com/images/favicon.ico" type="image/x-icon">
<link rel="icon" href="https://vdohire.com/images/favicon.ico" type="image/x-icon">

        <title>VDOhire Job Seeker Portal</title>
        <script src="<?php echo base_url(); ?>theme/vendor/jquery/jquery.min.js"></script>

        <!-- Bootstrap Core CSS -->
        <link href="<?php echo base_url(); ?>theme/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="<?php echo base_url(); ?>theme/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

        <!-- DataTables CSS -->
        <link href="<?php echo base_url(); ?>theme/vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

        <!-- DataTables Responsive CSS -->
        <link href="<?php echo base_url(); ?>theme/vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
    
        <!-- Custom CSS -->
        <link href="<?php echo base_url(); ?>theme/dist/css/sb-admin-2.css" rel="stylesheet">

        <!-- Morris Charts CSS -->
        <link href="<?php echo base_url(); ?>theme/vendor/morrisjs/morris.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="<?php echo base_url(); ?>theme/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!--				<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Montserrat"/>-->
<!--        <link href="--><?php //echo base_url(); ?><!--css/flat-ui.css" rel="stylesheet" type="text/css" />-->

        <link rel="stylesheet" href="<?php echo base_url(); ?>css/montserrat.css"/>
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/roboto.css"/>
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/edmondsans.css"/>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
        
        <input type="hidden" name="commonBaseUrl" id="commonBaseUrl" value="<?php echo base_url()?>">
<!--        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>-->
		<script src="<?php echo base_url(); ?>js/common.min.js"/></script>
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/progress-circle.css">
		

    </head>
    <body>

        <input type="hidden" name="currentbase_url" value="<?php echo base_url()?>" id="currentbase_url">
        <input type="hidden" name="currentuserId" value="<?php echo $_SESSION['userId'] ?>" id="currentuserId">
        <input type="hidden" name="currentjobCode" value="<?php echo isset($_SESSION['jobCode'])?$_SESSION['jobCode']:""; ?>" id="currentjobCode">
        <input type="hidden" name="currentinterviewSet" value="<?php echo isset($_SESSION['interviewSet'])?$_SESSION['interviewSet']:""; ?>" id="currentinterviewSet">
        <input type="hidden" name="currentUserName" value="<?php echo $_SESSION['userName']; ?>" id="currentUserName">
        <input type="hidden" name="currentinvitationId" value="<?php echo isset($_SESSION['invitationId'])?$_SESSION['invitationId']:""; ?>" id="currentinvitationId">
<!-- Navigation -->
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>                        
            </button>
            <a class="navbar-brand" href="<?php echo base_url() ?>users"><img src="<?php echo base_url(); ?>images/logo.png" rel="stylesheet"></a>
        </div>


    </div>
</nav>
<center>
    <div class="large-12 column" id="myModal" style="position: fixed;display:none;top: 400px;left: 45%;z-index:1;">
			  <div id="progress-circle" class="progress-circle progress-0"><span id="percet_text">0</span>
			  </div>
	</div>
	<div class="large-12 column" id="loading" style="position: fixed;display:none;top: 400px;left: 700px;z-index:1;">
			  <!--<img src ="<?php //echo base_url()?>images/icons/spinner.gif">-->
	</div>
</center>
<div class="row" style="    background: #f8f8f8;margin-right: 0px;">
	<?php
	if($main_content == "video"){
		echo '<div id="tabs" style="background: #054F72;    height: 59px;padding: 0 169px;">
            <center style="color:white;     font-size: 25px;text-align: center;padding: 35px;   padding-top: 12px;padding-bottom: 12px;">
                VIDEO INTERVIEW for '.$invitationDetails->title.'
            </center>
        </div>';
	}else if($main_content == "audition_question_view"){
		echo '<div id="tabs" style="background: #054F72;    height: 59px;padding: 0 169px;">
            <center style="color:white;     font-size: 25px;text-align: center;padding: 35px;   padding-top: 12px;padding-bottom: 12px;">
                VIDEO RESUME
            </center>
        </div>';
	}else if($main_content == "question" || $main_content == "setup"){
		echo '<div id="tabs" style="background: #054F72;    height: 59px;padding: 0 169px;">
            <center style="color:white;     font-size: 25px;text-align: center;padding: 35px;   padding-top: 12px;padding-bottom: 12px;">
                VIDEO INTERVIEW for '.$invitationDetails->title.'
            </center>
        </div>';
	}else if($main_content == "videoResumeInstruction"){
		echo '<div id="tabs" style="background: #054F72;    height: 59px;padding: 0 169px;">
            <center style="color:white;     font-size: 25px;text-align: center;padding: 35px;   padding-top: 12px;padding-bottom: 12px;">
                VIDEO RESUME INSTRUCTION
            </center>
        </div>';
	}else if($main_content == "videoResumeSetup"){
		echo '<div id="tabs" style="background: #054F72;    height: 59px;padding: 0 169px;">
            <center style="color:white;     font-size: 25px;text-align: center;padding: 35px;   padding-top: 12px;padding-bottom: 12px;">
                SETUP AUDIO & VIDEO
            </center>
        </div>';
	}else if($main_content == "question_view" || 
				$main_content == "uploadResume" || 
				$main_content == "assessmentGuidliance" || 
				$main_content == "thankyou" ||
				$main_content == "assessment"
				
			){
		echo '<div id="tabs" style="background: #054F72;    height: 95px;padding: 0 169px;">
            <center style="color:white;    font-size: 25px;text-align: center;padding: 35px;">
				<div  class="col-md-6 col-lg-6 col-sm-6 pull-left">
					'.$invitationDetails->title.'
				</div>
				<div  class="col-md-6 col-lg-6 col-sm-6 pull-left"  id="CompanyNameContainer">
					'. $invitationDetails->companyName.'
				</div>
            </center>
        </div>';
	}else{
	?>
        <div id="tabs" style="background: #054F72;    height: 97px;">
        <ul class="nav nav-tabs nav-justified">
            <li class="active pull-left" style="    width: 18%;margin-top: 6px;">
                <a href="javascript://" style="text-decoration:none;width:100%;">
                    <table style="width:100%;">
                        <tbody>
                            <tr>
                                <td>
                                    <div style="text-align: center;    padding: 13px;padding-right: 0px;padding-left: 0px;"><b>Position</b><br>
									<span  id="positionDivContainer"  style="font-size:15px;    display: inline-grid;width: 228px;white-space: nowrap;overflow: hidden !important;text-overflow: ellipsis;;" title="<?php echo $content->title; ?>"><?php echo $content->title; ?></span>
									</div></td>
                            </tr>

                        </tbody>
                    </table>
                </a>
            </li>
            <li class="active pull-left" style="    width: 18%;margin-top: 6px;">
                <a href="javascript://" style="text-decoration:none;width:100%;">
                    <table style="width:100%;">
                        <tbody>
                            <tr>
                                <td cellpadding="0">
                                    <div style="text-align: center;    padding: 13px;padding-right: 0px;padding-left: 0px;"><b>Company</b><br>
									
									<span style="font-size:15px;display: inline-grid;width: 228px;white-space: nowrap;overflow: hidden !important;text-overflow: ellipsis;" id="CompanyNameContainer"><?php echo $content->companyName; ?></span></div></td>
                            </tr>
                        </tbody>
                    </table>
                </a>
            </li>

            <li class="active pull-left" style="    width: 12%;margin-top: 6px;">
                <a href="javascript://" style="text-decoration:none;width:100%;"   class="active" >
                    <table style="width:100%;">
                        <tbody>
                            <tr><td cellpadding="0">
                                    <div style="text-align: center;    padding: 13px;padding-right: 0px;padding-left: 0px;">
                                        <b>Location</b><br>
										<span id="locationDivContainer" style="font-size:15px;display: inline-grid;width: 228px;white-space: nowrap;overflow: hidden !important;text-overflow: ellipsis;">
                                        <?php
                                            $detailLocation = array();
                                            foreach ($content->location as $location) {
                                                $detailLocation[] = $location->city;
                                            }
                                            if($detailLocation)
                                                echo implode(', ', $detailLocation);
                                            else
                                                echo '-';
                                        ?>
										</span>
                                    </div></td>
                            </tr>

                        </tbody>
                    </table>
                </a>
            </li>

            <li class="active pull-left" style="    width: 12%;margin-top: 6px;">
                <a href="javascript://" style="text-decoration:none;width:100%;"   class="active" >
                    <table style="width:100%;">
                        <tbody>
                            <tr>
                                <td>
                                    <div style="text-align: center;    padding: 13px;padding-right: 0px;padding-left: 0px;">
                                        <span class="texto_grande" style="color: white;font-size: 16px;"><b>Vacancies</b></span><br>
										<span style="font-size:15px"><?php echo !empty($content->vaccancies) ? $content->vaccancies : '-' ; ?></span></div></td>
                            </tr>
                        </tbody>
                    </table>
                </a>
            </li>

            <li class="active pull-left" style="    width: 20%;margin-top: 6px;">
                <a href="javascript://" style="text-decoration:none;width:100%;"   class="active" >
                    <table style="width:100%;">
                        <tbody>
                            <tr>
                                <td cellpadding="0">
                                    <div style="text-align: center;    padding: 13px;padding-right: 0px;padding-left: 0px;">
                                        <span class="texto_grande" style="color: white;font-size: 18px;"><b>Salary&nbsp;(INR)</b><br>
										<span style="font-size:15px">
                                            <?php
                                            //var_dump($content->maxSalary);exit;
                                                /*if($content->maxSalary != '0') {
                                                    echo $content->minSalary; ?> - <?php echo $content->maxSalary;
                                                } else {
                                                    echo '-';
                                                }*/

                                                if(($content->maxSalary == 0) && !empty($content->maxSalary)) {
                                                    echo '-';
                                                } else {
                                                    echo $content->minSalary; ?> - <?php echo $content->maxSalary;
                                                }
                                            ?>
                                        <span></span></div></td>
                            </tr>
                            
                        </tbody>
                    </table>
                </a>
            </li>

            <li class="active pull-left" style="    width: 18%;margin-top: 6px;">
                <a href="javascript://" style="text-decoration:none;width:100%;"   class="active" >
                    <table style="width:100%;">
                        <tbody>
                            <tr>
                                <td cellpadding="0">
                                    <div style="text-align: center;   padding: 13px;padding-right: 0px;padding-left: 0px;"><span class="texto_grande" style="color: white;font-size: 16px;">
                                            <b>Last date to Apply</b><br>
                                            <span style="font-size:15px">
                                                <?php
                                                    if(($content->openTill != '0000-00-00 00:00:00') && !empty($content->openTill)) {
                                                        echo date('d M, Y', strtotime($content->openTill)); 
                                                    } else {
                                                        echo '-';
                                                    }
                                                    
                                                ?>
                                            </span>
                                        </span></div></td>
                            </tr>
                        </tbody>
                    </table>
                </a>
            </li>

        </ul>
        <!-- /.col-lg-12 -->
    </div>
	<?php } ?>
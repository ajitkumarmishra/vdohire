<?php if($main_content == 'editbasicprofile' || $main_content == 'resume' || $main_content == 'send_resume_document' || $main_content == "videoResume"){
	
}else{
	if(isset($_SESSION['userId']) && $_SESSION['userId'] != ""){
	?>
<nav class="navbar navbar-inverse" id="navheight">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="<?php echo base_url()?>users"><img src="<?php echo base_url(); ?>images/logo.png" rel="stylesheet"></a>
    </div>
    <?php /*if($content->eventName) { ?>
        <div class="navbar-header row" style="padding: 10px;
        
        text-shadow: 2px 1px 0px black;
        color: white;">
		<?php
		$obj =& get_instance();

		$obj->load->model('coreapi_model','CORE_MODEL');
		$eventJwtToken = $obj->CORE_MODEL->getJwtToken(1);
		?>
          <span ci="<?php echo $ci ?>" class="text-center" style=""><a href="<?php echo base_url().'job/event-jobs/'.$eventJwtToken."?eventName=".$content->eventName; ?>" style="    color: white;
        font-size: 20px;
        font-weight: bold;
        text-decoration: none;"><u>Please click here for <?php echo $content->eventName; ?></u></a></span>
        </div>
    <?php }*/
	?>
	<?php 
	if($this->config->item('eventLive') == 0){ ?>
		<div class="navbar-header row" style="padding: 10px;
        
			text-shadow: 2px 1px 0px black;
			color: white;">
			
			  <span ci="" class="text-center" style=""><a href="<?php echo base_url().'job/hot_jobs'; ?>" style="    color: white;
			font-size: 20px;
			font-weight: bold;
			text-decoration: none;"><u>Hot Jobs August 2017</u></a></span>
        </div>
	<?php }else{
	?>
		<div class="navbar-header row" style="padding: 10px;
        
			text-shadow: 2px 1px 0px black;
			color: white;">
			
			  <span ci="" class="text-center" style=""><a href="<?php echo base_url().'job/event-jobs/eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpZCI6MX0.PwwXbVqCFZzYVwq5o0GRhNhqoL6Pn5j5DIpIsMQxaJgqClRQrX6svShfpcZWVtdpUvuziXF1gP1wgrML8hmrfw?eventName=Virtual%20Job%20Fair%20June%202017'; ?>" style="    color: white;
			font-size: 20px;
			font-weight: bold;
			text-decoration: none;"><u>Virtual Job Fair July 2017</u></a></span>
        </div>

	<?php
	}
	?>
    <div class="collapse navbar-collapse pull-right" id="myNavbar" style="margin-right:112px;">
        <ul class="nav navbar-nav">
            <!--<li style="background:white;border: 1px solid black;    margin-right: 21px;">
                <a href="#" style="color:black;text-decoration: none;padding-left: 7px;">
                    <i class="fa fa-user-plus" style="font-size: 19px;"></i>
                    <span class="dashboard-invite-friends" style="font-weight:600;">Invite Friends</span>
                </a>
            </li>
            <li  style="border: 1px solid black;background:white;    margin-right: 21px;" class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#" style="color:black;text-decoration: none;padding-left: 7px;">
                    <i class="fa fa-video-camera" style="font-size: 19px;"></i>
                    <span class="dashboard-video-profile" style="font-weight:600;">Video Profile</span>
                </a>
            </li>-->
		<?php
					
if(isset($_SESSION['userImage']) && $_SESSION['userImage'] != ""){
	$image = base_url()."uploads/userImages/".$_SESSION['userImage'];
}else{
	$image = "https://firstjob.co.in/admin/theme/firstJob/image/default.png";
}

//Get Notification unread count
$getNotificationUnreadCount = 0;

//print_r($allNotificationList);exit;
foreach ($allNotificationList as $notiCount) {
    if($notiCount->status == 0) {
        $getNotificationUnreadCount++;
    }
}
//End of get Notification unread count
					?>

            <li style="margin-right: 21px;">
                <?php
                    if($getNotificationUnreadCount > 0) {
                        $notiMessage = 'You have '.$getNotificationUnreadCount.' new notifications. Please click here to view notifications.';
                    } else {
                        $notiMessage = '';
                    }
                ?>
                <a title="<?php echo $notiMessage;?>" class="dropdown-toggle" data-toggle="dropdown" href="#" style="color:black;text-decoration: none;padding-left: 7px; background: none !important;">
                    <?php if($getNotificationUnreadCount > 0) { ?>
                        <span class="badge" style="background-color: red !important; position: absolute; margin-left: 32%; top: 0px;"><?php echo $getNotificationUnreadCount;?></span>
                    <?php } ?>
                    <i class="fa fa-bell fa-fw" style="color: white;  font-size: 25px;"></i> <!--<i class="fa fa-caret-down" style="color: white;"></i>-->
                </a>
                <ul class="dropdown-menu dropdown-messages" style="right: 0; left: inherit; overflow-y: scroll; height:340px; width: 400px;">
                    <li><div class="col-md-12 col-lg-12 col-sm-12" style="min-height: 44px; font-size: 20px; background: #69686D; margin-top: -5px;"><div style="padding-top: 7px; color: white;">NOTIFICATIONS</div></div>
                    </li>
                    <?php if(count($allNotificationList) > 0) { foreach($allNotificationList as  $notificationrow) { ?>
                    <li>
                        <a href="#" style="text-decoration: none;">
                            <div>
                                <strong><?php echo $notificationrow->sender ?></strong>
                                <span class="pull-right text-muted">
                                    <em style="font-size: 12px;"><?php echo date('d M Y', strtotime($notificationrow->createdAt)) ?></em>
                                </span>
                            </div>
                            <div><?php echo $notificationrow->message ?></div>
                        </a>
                    </li>
                    <li class="divider"></li>
                    <?php } } else { ?>
                        <li>
                            <a href="#" style="text-decoration: none;">
                                <div>
                                    <span class="pull-right text-muted">
                                        <em>You have no notification!</em>
                                    </span>
                                </div>
                            </a>
                        </li>
                    <?php } ?>

                    <?php if(count($allNotificationList) > 0) { ?>
                        <li>
                            <div class="col-md-12 col-lg-12 col-sm-12">
                                <div class="col-md-7 col-lg-7 col-sm-7" style="border-right: 1px solid #333;">
                                    <a style="color: #69686D !important; text-decoration: none; margin-left: 8px;" text-align="left" href="<?php echo base_url().'read-notifications';?>">
                                        <strong>MARK ALL AS READ</strong>
                                    </a>
                                </div>
                                <div class="col-md-5 col-lg-5 col-sm-5">
                                    <a style="color: #69686D !important; text-decoration: none; margin-left: 29px;" text-align="right" href="<?php echo base_url().'notifications';?>">
                                        <strong>VIEW ALL</strong>
                                    </a>
                                </div>
                            </div>
                        </li>
                    <?php } ?>
                </ul>
                <!-- /.dropdown-messages -->
            </li>

            <li  style="border: 1px solid black;background:white;    margin-right: 21px;">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#" style="color:black;text-decoration: none;padding-left: 7px;">
                    <img src="<?php echo $image;?>" style="    width: 19px;"><span  style="font-weight:600;">&nbsp;<?php echo isset($_SESSION['userName'])?$_SESSION['userName']:"New User" ?></span>&nbsp;<span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li style="background:white;"><a href="javascript://"  data-toggle="modal" data-target="#EditprofileModal" class="EditprofileModalPopup" id="EditprofileModalPopup"   style="color:black;text-decoration: none;"><i class="fa fa-user fa-fw"></i>Edit Profile</a></li>
                    
					<li style="background:white;"><a href="#" style="color:black;text-decoration: none;" class="changePassword" ><i class="fa fa-key" aria-hidden="true"></i>&nbsp;Change Password</a></li>
                    <li style="background:white;"><a href="<?php echo base_url();?>users/logout" style="color:black;text-decoration: none;"><i class="fa fa-sign-out fa-fw"></i>Logout</a></li>
                </ul>
            </li>
        </ul>
    </div>
  </div>
</nav>
<center>
    <div class="large-12 column" id="myModal" style="position: fixed;display:none;top: 400px;left: 45%;z-index:11111;">
			  <div id="progress-circle" class="progress-circle progress-0"><span id="percet_text">0</span>
			  </div>
	</div>
	</center>
<?php } 
}
?>
    
<div class="row" style="    background: #f8f8f8;margin-right: 0px;margin-left: 0px;">
<?php if($main_content == 'editbasicprofile' || $main_content == 'resume' || $main_content == 'send_resume_document'  || $main_content == "videoResume" || $main_content == 'hot-jobs'){?>
<div id="tabs" style="background: #054F72;">
<?php }else{ ?>
	<div id="tabs" style="background: #054F72;    height: 109px;padding: 0 169px;">
<?php } ?>
				<?php
					if($main_content == 'hot-jobs'){
						$url = "'".base_url()."editProfile'";
						echo '<div style="height:30px;"></div>';
					}else if($main_content == 'editbasicprofile'){
						$url = "'".base_url()."editProfile'";
						echo '<div style="color:white;    font-size: 25px;padding: 9px;    padding-left: 47px;">
								<div class="">
									<a style="margin-right: 18px;cursor:pointer;color: white;" onclick="showCOntentURL('.$url.')"  title="Back to Edit Profile"><i class="fa fa-arrow-left" aria-hidden="true"></i></a><span>Edit Profile<span>
								</div>
							</div>';
					}else if($main_content == 'resume'){
						$url = "'".base_url()."editProfile'";
						echo '<div style="color:white;    font-size: 25px;padding: 9px;    padding-left: 47px;">
								<div class="">
									<a style="margin-right: 18px;cursor:pointer;    color: white;" onclick="showCOntentURL('.$url.')" title="Back to Edit Profile"><i class="fa fa-arrow-left" aria-hidden="true"></i></a><span>Upload Resume Document</span>
								</div>
							</div>';
					}else if($main_content == 'videoResume'){
						$url = "'".base_url()."editProfile'";
						echo '<div style="color:white;    font-size: 25px;padding: 9px;    padding-left: 47px;">
								<div class="">
									<a style="margin-right: 18px;cursor:pointer;    color: white;" onclick="showCOntentURL('.$url.')" title="Back to Edit Profile"><i class="fa fa-arrow-left" aria-hidden="true"></i></a><span>Upload Video Resume</span>
								</div>
							</div>';
					}else if($main_content == 'send_resume_document'){
						$url = "'".base_url()."editProfile'";
						echo '<div style="color:white;    font-size: 25px;padding: 9px;    padding-left: 47px;">
								<div>
									<a style="margin-right: 18px; cursor:pointer;   color: white;" onclick="showCOntentURL('.$url.')" title="Back to Edit Profile"><i class="fa fa-arrow-left" aria-hidden="true"></i></a><span>Send Resume Document by Email</span>
								</div>
							</div>';
						
					}else{
					$backgroundEditProfile = "";
					$backgroundIndex = "";
					$backgroundDiscussion = "";
					$backgroundKnowledge = "";
					$backgroundSearch = "";
					
					if($main_content == 'index'){
						$backgroundIndex = "background: URL(".base_url()."images/blue1.png) no-repeat;";
					}
					if($main_content == 'discussion' || $main_content == 'forum-details'){
						$backgroundDiscussion = "background: URL(".base_url()."images/blue1.png) no-repeat;";
					}
					
					if($main_content == 'knowledgeDataCategoryView' || $main_content == 'knowledgeDataCategory' || $main_content == 'knowledgeDataForTopic'){
						$backgroundKnowledge = "background: URL(".base_url()."images/blue1.png) no-repeat;";
					}
					
					if($main_content == 'edit_profile'){
						$backgroundEditProfile = "background: URL(".base_url()."images/blue1.png) no-repeat;";
					}
					
					if($main_content == 'searched-jobs'){
						$backgroundSearch = "background: URL(".base_url()."images/blue1.png) no-repeat;";
					}
					
				?>
                <ul class="nav nav-tabs nav-justified">
					
                    <li class="active pull-left" style="    width: 19%;cursor: pointer;margin-top: 6px;">
						<a href="<?php echo base_url()?>users" style="text-decoration:none;cursor:pointer;">
                       <table style="<?php echo $backgroundIndex; ?>background-size: 90px 90px;width: 90px;height: 90px;">
							<tbody>
								<tr><td cellpadding="0"><div style="text-align: center;"><i class="fa fa-home sub-menu-icon" style="font-size: 25px;color: white;"></i> </div></td>
								</tr>
								<tr><td cellpadding="0"><div style="text-align: center;margin-top: -15px;"><span class="texto_grande" style="color: white;font-size: 16px;">Home</span></div></td>
								</tr>
							</tbody>
						</table>
						</a>
                    </li>
					<li class="active pull-left" style="    width: 19%;cursor: pointer;margin-top: 6px;">
                        <a href="javascript://" style="text-decoration:none;cursor:pointer;"   class="searchJobsForJobseeker active" >
                       <table style="<?php echo $backgroundSearch; ?>background-size:100% 90px;width: 90px;height: 90px;">
                            <tbody>
                                <tr><td cellpadding="0"><div style="text-align: center;"><i class="fa fa-search" aria-hidden="true" style="font-size: 25px;color: white;"></i></div></td>
                                </tr>
                                <tr><td cellpadding="0"><div style="text-align: center;margin-top: -15px;"><span class="texto_grande" style="color: white;font-size: 16px;">Search Jobs</span></div></td>
                                </tr>
                            </tbody>
                        </table>
                        </a>
                    </li>
					<li class="active pull-left" style="    width: 19%;cursor: pointer;margin-top: 6px;">
						<a href="<?php echo base_url()."discussion-forum" ?>"  style="width:58%;text-decoration:none;cursor:pointer;<?php echo $backgroundDiscussion; ?>background-size:100% 96px;">
                       <table style="    width: 100%;height: 90px;">
							<tbody>
								<tr><td cellpadding="0"><div style="text-align: center;color:white;"><i class="fa fa-comments" aria-hidden="true"></i></div></td>
								</tr>
								<tr><td cellpadding="0"><div style="text-align: center;margin-top: -15px;"><span class="texto_grande" style="color: white;font-size: 16px;">Discussion Forum</span></div></td>
								</tr>
							</tbody>
						</table>
						</a>
                    </li>
					
					<li class="active pull-left" style="    width: 19%;cursor: pointer;margin-top: 6px;">
						<a href="<?php echo base_url()."knowledge-center"?>"  style="width:63%;text-decoration:none;cursor:pointer;<?php echo $backgroundKnowledge; ?>background-size:100% 96px; ">
                       <table style="   width: 100%;height: 90px;">
							<tbody>
								<tr><td cellpadding="0"><div style="text-align: center;color: white;"><i class="fa fa-book" aria-hidden="true"></i></div></td>
								</tr>
								<tr><td cellpadding="0"><div style="text-align: center;margin-top: -15px;"><span class="texto_grande" style="color: white;font-size: 16px;">Knowledge Center</span></div></td>
								</tr>
							</tbody>
						</table>
						</a>
                    </li>
					
					
					
					<li class="active pull-left" style="    width: 19%;cursor: pointer;margin-top: 6px;">
						<a href="<?php echo base_url()?>users/logout" style="text-decoration:none;cursor:pointer;"   class="changePassword active" >
                       <table style="background-size: 90px 90px;width: 90px;height: 90px;">
							<tbody>
								<tr><td cellpadding="0"><div style="text-align: center;"><i class="fa fa-power-off" aria-hidden="true" style="font-size: 25px;color: white;"></i></div></td>
								</tr>
								<tr><td cellpadding="0"><div style="text-align: center;margin-top: -15px;"><span class="texto_grande" style="color: white;font-size: 16px;">Logout</span></div></td>
								</tr>
							</tbody>
						</table>
						</a>
                    </li>
                   
                    
                </ul>
					<?php } ?>
        <!-- /.col-lg-12 -->
        </div>
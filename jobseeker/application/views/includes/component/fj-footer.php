<style>
.form-group.required .control-label:after { 
   content:"*";
   color:red;
}
.form-group.required .control-label:after{
	float:left;
	
}
.form-group.required .control-label:after { 
   content:"*";
   color:red;
}
.form-group.required .control-label:after{
	float:left;
	
}

.fileinput-button{
	display:none;
	
}
.edit_user_img:hover .fileinput-button{
	display:block;
}

.datepicker-years {
    display: block !important;
}
.editProfileNav>li.active {
	background-color: #27acbf;
}
.editProfileNav>li:hover{
	background-color: #27acbf;
}
.chosen-choices {
    border: 1px solid #ccc;
    border-radius: 4px;
    min-height: 34px;
    padding: 6px 12px;
}
.chosenContainer .form-control-feedback {
    /* Adjust feedback icon position */
    right: -15px;
}
.chosenContainer .form-control {
    height: inherit; 
    padding: 0px;
}

</style>
<form id="changepassword" method="post" class="form-horizontal" style="display: none;">
    <div id="page-content-wrapper">
        <div class="container-fluid whitebg">
            <div class="row">
                <div class="col-md-12 padd_tp_bt_20">
                    <div class="form-horizontal form_save">
            <div class="form-group">
                <label for="inputEmail" class="control-label col-xs-3 required-field"><span style="color:red;">*</span>Old Password</label>
                <div class="col-xs-9">
                    <input type="password" class="form-control" placeholder="Old Password" name="oldPassword" id="oldPassword" value="">
                </div>
            </div>

            <div class="form-group">
                <label for="inputEmail" class="control-label col-xs-3 required-field "><span style="color:red;">*</span>New Password</label>
                <div class="col-xs-9">
                    <input type="password" class="form-control" placeholder="New Password" name="newPassword" data-validation="length" data-validation-length="min6" id="newPassword"  value="">
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail" class="control-label col-xs-3 required-field "><span style="color:red;">*</span>Confirm New Password</label>
                <div class="col-xs-9">
                    <input type="password" class="form-control" placeholder="Confirm New Password" name="confirmNewPassword"  id="confirmNewPassword" value="">
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-5 col-xs-offset-3">
                    <input type="hidden" name="token" id="token" value="ecbcd7eaee29848978134beeecdfbc7c">
                    <input type="hidden" name="methodname" id="methodname" value="changePassword">
                    <input type="hidden" name="userId" id="userId" value="<?php echo $token ?>">
                    <button type="submit" class="btn btn-default pull-left" style="background: #054F72; color: white;">Save</button>
                </div>
            </div>
            <div style="float:right;">
                <span style="color:red;font-size:19px;">*</span><span style="font-size:12px">&nbsp;&nbsp;Required Fields.</span>
            </div>

        </div>
    </div>
        </div>
    </div>
    </div>
</form>
<!--Use to display popup for search Jobs-->
<!--Use to display popup for search Jobs-->
<form id="searchJobsForJobseeker" method="post" action="<?php echo base_url().'job/searched-jobs'?>" class="form-horizontal" style="display: none;">
    <div id="page-content-wrapper">
        <div class="container-fluid whitebg">
            <div class="row">
                <div class="col-md-12 padd_tp_bt_20">
                    <div class="form-horizontal form_save">
                        <div class="form-group" id="searchJobContent">
                            <div class="row">
                                <div class="col-xs-8 col-xs-offset-3">
                                    <h4 class="noBorder_large_title"></h4>
                                    <ul class="p0L10">
                                        <!--<li title="mca fresher, 1 yr, 1 lakh" style="list-style-type: none; margin-bottom: 5px;">
                                            <a data-count="2" href="javascript:void(0);" style="color:#09c; text-decoration: none;">
                                                <span class="rsLabel">mca fresher,1 yr,1 lakh</span>
                                            </a>
                                        </li>-->
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <label for="inputEmail" class="control-label col-xs-3">Keyword</label>
                                <div class="col-xs-8">
                                    <input type="text" class="form-control" placeholder="Search for jobs or companies" name="searchKeyword" id="searchkeyWord" value="">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <label for="inputEmail" class="control-label col-xs-3">Location</label>
                                <div class="col-xs-8">


                                    <select data-placeholder="Enter the cities you want to work in" style="display: none;" multiple="" class="chosen-select location form-control" tabindex="-1" name="locationName[]" id="searchLocationName" data-validation="alphanumeric required">
                                        <option value=""></option>
                                        <?php foreach ($city as $item) : ?>
                                            <option value="<?php echo $item['city']; ?>"><?php echo $item['city']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label for="yearsFrom" class="control-label col-xs-3">Work Experience</label>
                                <div class="col-xs-3">
                                    <select class="form-control col-xs-3" name="yearsFrom"  id="searchYearsFrom">
                                        <option value="">Select</option>
                                        <?php for ($i = 0; $i <= 30; $i++) : ?>
                                            <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                        <?php endfor; ?>
                                    </select>
                                </div>

                                <div class="col-xs-1" style="padding: 0;">
                                    <label class="control-label">Years</label>
                                </div>

                                <!--<div class="col-xs-4">
                                    <select class="form-control col-md-3" name="searchMonth"  id="searchMonth">
                                        <option value="">Select</option>
                                        <?php //for ($j = 0; $j <= 11; $j++) : ?>
                                            <option value="<?php //echo $j; ?>"><?php //echo $j; ?></option>
                                        <?php //endfor; ?>
                                    </select>
                                </div>
                                
                                <div class="col-xs-1" style="padding: 0;">
                                    <label class="control-label">Month</label>
                                </div>-->
                            </div>

                        </div>
                        

                        <div class="form-group">
                            <div class="row">
                                <label for="yearsFrom" class="control-label col-xs-3">Expected Salary</label>

                                <div class="col-xs-3">
                                    <select class="form-control" name="expectedSalary"  id="searchExpectedSalary">
                                        <option value="">Min</option>
                                        <option value="0"><1</option>
                                        <?php for ($k = 1; $k <= 50; $k++) : ?>
                                            <option value="<?php echo $k.'00000'; ?>"><?php echo $k; ?></option>
                                        <?php endfor; ?>
                                    </select>
                                </div>

                                <div class="col-xs-2" style="padding: 0;">
                                    <label class="control-label">In Lakhs</label>
                                </div>

                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-5 col-xs-offset-3">
                                <input type="hidden" name="token" id="token" value="ecbcd7eaee29848978134beeecdfbc7c">
                                <input type="hidden" name="methodname" id="methodname" value="searchJobs">
                                <input type="hidden" name="userId" id="userId" value="<?php echo $token ?>">
                                <input type="hidden" name="startPoint" id="startPoint" value="0">
                                <input type="hidden" name="jobsLimit" id="jobsLimit" value="25">
                                <button type="button" id="showsearchedJobsOptionbutton" class="btn btn-default pull-left" style="background: #054F72; color: white;">SEARCH</button>

                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<!--End of displaying popup to display search Jobs-->


<div class="modal fade" id="EditprofileModal">
    <div class="modal-dialog" style="width:80%;">
        <div class="modal-content">
            
			
            <div class="modal-body" style="    height: 600px;padding-left: 0px;padding-right: 0px;padding-top: 0px;">
			<div id="popupContentEditProfile">
				<div  class="col-md-12 col-lg-12 col-sm-12" style="font-size: 17px;color: white;font-weight: bold;background: #EB9B2C ;">
					<div><h2 style="font-size:16px;color:black;font-weight:bold;">Edit Profile</h2></div>
				</div>
       
    <div style="clear:both"></div>
    <div class="container-full" style="background: white;">
      
        <div  class="col-md-12 col-lg-12 col-sm-12" style="padding-bottom: 23px;margin-bottom: 20px;padding-top: 20px;background: white ;">
            <div class="col-lg-12">
                <div class="row">
                    <form class="form-horizontal">
                        <fieldset>
                            
                            <div class="form-group">
                                <div class="col-xs-12">
                                    <div class="col-xs-3">
                                        <label>
                                            <a class="btn btn-primary" data-toggle="modal" data-target="#EditbasicprofileModal"  href="javascript://" id="manuallyEditProfile" name="editProfile" value="manuallyEditProfile" type="radio" style="     height: 100px;   padding-top: 38px;width: 186px;text-decoration: none;">Edit Profile Manually</a>
                                        </label>
                                    </div>
                                    <div class="col-xs-3">
                                        <label>
                                            <a class="btn btn-primary" data-toggle="modal" data-target="#EditResumeModal"  id="uploadResumeDocument" name="editProfile" value="uploadResumeDocument" title="Upload Resume Document (PDF, Doc, Docx)" type="radio" style="     height: 100px;   padding-top: 38px;width: 186px;text-decoration: none;">Upload Resume</a>
                                        </label>
                                    </div>
									<div class="col-xs-3">
                                        <label>
                                            <a class="btn btn-primary"   data-toggle="modal" data-target="#EditSendModalPopup"  id="SendResumeDocument" name="editProfile" value="uploadResumeDocument" title="Send Resume Document (PDF, Doc, Docx)" type="radio" style="     height: 100px;   padding-top: 38px;width: 186px;text-decoration: none;">Send Resume by Email</a>
                                        </label>
                                    </div>
                                    <div class="col-xs-3">
                                        <label>
                                            <a class="btn btn-primary"   data-toggle="modal" data-target="#EditVideoModal"  href="javascript://"  id="videoResume"  type="radio" style="    padding-top: 38px;    height: 100px;width: 186px;text-decoration: none;">Upload Video Resume</a>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            
                        </fieldset>
                    </form>
                </div>
                <!-- /.row -->
            </div>
        </div>
    <!-- /#page-wrapper -->
    </div>
			</div>
                
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<div class="modal fade" id="EditbasicprofileModal">
    <div class="modal-dialog" style="width:80%;">
        <div class="modal-content">
            
			
            <div class="modal-body" style="    height: 600px;padding-left: 0px;padding-right: 0px;padding-top: 0px;">
			<div id="popupContentEditProfile">
				<div style="color:white;    background: #054f72;    font-size: 25px;padding: 9px;    padding-left: 47px;">
					<div class="">
						<a style="margin-right: 18px;cursor:pointer;color: white;" data-dismiss="modal" aria-hidden="true"  ><i class="fa fa-arrow-left" aria-hidden="true"></i></a><span>Edit Profile<span>
					</div>
				</div>
      
				<div style="clear:both"></div>
				<div class="container-full" style="background: white;">
            <div  class="col-md-3 col-lg-3 col-sm-3 pull-left" style="background: #f8f8f8;height: 30px;">
				<ul class="nav editProfileNav" style="background-color: #f8f8f8;">
								<li style="    width: 100%;" class="active"><a style="    width: 100%;background:none;color: black; text-decoration: none; font-size: 16px; font-weight: bold;" data-toggle="tab" href="#menu1">Personal Information</a></li>
								<li style="    width: 100%;" ><a style="    width: 100%;background:none;color: black; text-decoration: none; font-size: 16px; font-weight: bold;" data-toggle="tab" href="#menu2">Educational Information</a></li>
								<li style="    width: 100%;" ><a style="    width: 100%;background:none;color: black; text-decoration: none; font-size: 16px; font-weight: bold;" data-toggle="tab" href="#menu3">Employment History</a></li>
								<li style="    width: 100%;" ><a style="    width: 100%;background:none;color: black; text-decoration: none; font-size: 16px; font-weight: bold;" data-toggle="tab" href="#menu4">Extra Information</a></li>
								<li style="    width: 100%;" ><a style="    width: 100%;background:none;color: black; text-decoration: none; font-size: 16px; font-weight: bold;" data-toggle="tab" href="#menu5">Social Media Information</a></li>
				</ul>
			</div>
            <div  class="col-md-9 col-lg-9 col-sm-9 pull-left" style="border:1px solid lightgrey;padding-bottom: 23px;margin-bottom: 20px;padding-top: 20px;background: white ;">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="tab-content">
                            <div id="menu1" class="tab-pane fade in active">
                                <form method="post" action="" name="createUser" enctype="multipart/form-data" id="createSubuserForm" class="has-validation-callback" style="margin-top: 24px;">
                                    <div class="col-md-3 content_para pull-left">
                                        <?php 
                                            if(isset($_SESSION['userImage']) && $_SESSION['userImage'] != ""){
                                                $image = base_url()."uploads/userImages/".$_SESSION['userImage'];
                                            }else{
                                                $image = "https://firstjob.co.in/admin/theme/firstJob/image/default.png";
                                            }
                                        ?>
                                        <div class="col-md-12 ed_gry_bg" style="">
                                            <div class="edit_user_img" style="    margin-top: 20%;"><a href="javascript:" class="uploadImage">
                                                    <img src="<?php echo $image;?>" alt="" id="uploadIcon" title="" class="img-responsive"></a>
                                                    <span class="btn btn-success fileinput-button" style="position: relative;overflow: hidden;display: inline-block;    margin-top: 5px;    width: 100%;    background: #11AAF0;border: #11AAF0;">
                                                        
                                                        <span>Select Photo</span>
                                                        <!-- The file input field used as target for the file upload widget -->
                                                        <input type="file" name="uploadProfile"  id="profileImage" value="" style="    cursor: pointer;    top: 0;    right: 0;    margin: 0;    opacity: 0; position: absolute;bottom: 0px;color: white;">
                                                        
                                                    </span>
                                                    
                                                    <div style="">
                                                        <input type="button" name="Upload" value="Upload" id="showUploadButton"  style="display:none;    width: 100%;    margin-top: 7px;" class="btn btn-primary"  onclick="uploadProfilePic()">
                                                    </div>
                                                <div id="fimageName"> </div> 
                                            </div>
                                        </div>
                                    </div>



									<div class="col-md-9 content_para pull-left">



										<div class="form-group required" style="margin-top:20px;">
											<label for="inputEmail" class="control-label col-xs-4 required-field required" required="true">Name : </label>
											<div class="col-xs-8">
												<input type="text" value="<?php echo $basic->fullname; ?>" name="param[basicInformation][]['fullname']"  placeholder="Name"  id="name" class="form-control" data-validation="required" required="required">
												<div class="alert-error" id="nameError" style="display:none;padding-top:10px;color:red;font-size: 12px;">
													<strong>Please enter name.</strong>
												</div>
											</div>
										</div>
										<div class="clearfix"></div>
                                        <div class="form-group required" style="margin-top:20px;">
                                            <label for="inputEmail" class="control-label col-xs-4 required-field">Email : </label>
                                            <div class="col-xs-8">
                                                <input type="text" value=" <?php echo $basic->email; ?>" name="param[basicInformation][]['email']" id="email" placeholder="Email"  class="form-control" data-validation="required">
                                                <div class="alert-error" id="emailError" style="display:none;padding-top:10px;color:red;font-size: 12px;">
                                                    <strong>Please enter a valid Email.</strong>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>
                                        <div class="form-group required" style="margin-top:20px;">
                                            <label for="inputEmail" class="control-label col-xs-4 required-field">Mobile : </label>
                                            <div class="col-xs-8">
                                                <input type="text" value="<?php echo $basic->mobile; ?>" name="param[basicInformation][]['mobile']" id="mobile" placeholder="Mobile"  class="form-control" maxlength="10" data-validation="required"  pattern="[7-9]{1}[0-9]{9}">
                                                <div class="alert-error" id="mobileError" style="display:none;padding-top:10px;color:red;font-size: 12px;">
                                                    <strong>Please enter Mobile No.</strong>
                                                </div>
                                            </div>
                                        </div>

										<div class="clearfix"></div>

										<div class="form-group" style="margin-top:20px;">
											<label for="inputEmail" class="control-label col-xs-4">Gender : </label>
											<div class="col-xs-8">
											<?php if($basic->gender == 1){ ?>
												<select class="selectpicker form-control" name="param[basicInformation][]['gender']" id="gender" data-validation="required">
													<option value="1" selected="selected">Male</option>
													<option value="2">Female</option>
												</select>
											<?php }else{ ?>
												<select class="selectpicker form-control" name="param[basicInformation][]['gender']" id="gender" data-validation="required">
													<option value="1" selected="selected">Male</option>
													<option value="2" selected="selected">Female</option>
												</select>
											
											<?php }  ?>
											</div>
										</div>

                                        <div class="clearfix"></div>
                                        <div class="form-group" style="margin-top:20px;">
                                            <label for="inputEmail" class="control-label col-xs-4">Year Of Birth : </label>
                                            <div class="col-xs-8">
                                                <input type="text" value="<?php echo $basic->dob; ?>" name="param[basicInformation][]['dob']" id="datepicker-13"  placeholder="YYYY" class="form-control pick-date hasDatepicker">
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>
                                        <div class="form-group" style="margin-top:20px;">
                                            <label for="inputEmail" class="control-label col-xs-4">Pin Code : </label>
                                            <div class="col-xs-8">
                                                <input type="text" value="<?php echo $basic->pincode; ?>" name="param[basicInformation][]['pincode']" placeholder="Pincode"  id="pincode" class="form-control">
                                                <div class="alert-error" id="pinCodeError" style="display:none;padding-top:10px;color:red;font-size: 12px;">
                                                    <strong>Please enter valid pin code.</strong>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>


                                    <div class="form-group" style="margin-top:20px;">
                                        <input type="hidden" name="param[accessToken]" value="f1r$tj0b098">
                                        <input type="hidden" id="methodnameForBasic" name="param[methodname]" value="updateDetailsOfJobsSeeker">
                                        <input type="hidden" id ="userId" name="param[userId]" value="<?php echo $_SESSION['userId'] ?>">
                                        <input type="hidden" id="token" name="param[token]" value="ecbcd7eaee29848978134beeecdfbc7c">
                                        <button class="btn btn-lg btn-primary btn-block"  name="Submit" value="Save" type="button" onclick="saveBasicInfo()" style="    width: 15%;margin-left: 52%;    background: #11AAF0;border: #11AAF0;">SAVE</button>             
                                    </div>
                                    <div style="float:right;">
                                        <span style="color:red;font-size:19px;">*</span><span style="font-size:12px">&nbsp;&nbsp;Required Fields.</span>
                                    </div>
                                </form>
                            </div>

                            <div id="menu2" class="tab-pane fade">
                                <div  style="margin: 14px; float: right; padding: 10px;">
                                    <span><button type="button" data-toggle="modal" data-target="#addEducationModal"><i class="fa fa-plus" aria-hidden="true"></i></button></span>
                                </div>
                                
                                <div class="">
                                    <table class="table">
                                        <thead>
                                          <tr>
                                            <th>Course</th>
                                            <th>University</th>
                                            <th>Institute</th>
                                            <th>Year Of Completion</th>
                                            <th>Marks Percentage</th>
                                            <th>Action</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
											
											foreach($educationalInfo as $educationDetail) { ?>
                                            <tr>
                                                <td><?php echo $educationDetail->courseName;?></td>
                                                <td><?php echo $educationDetail->universityName;?></td>
                                                <td><?php echo $educationDetail->institute;?></td>
                                                <td><?php echo $educationDetail->completionYear;?></td>
                                                <td><?php echo $educationDetail->percent.'%';?></td>
                                                <td>
                                                    <button type="button" title="Please click here to delete this education information." onclick="editQualification(<?php echo $educationDetail->id;?>)"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                                                    <button type="button" title="Please click here to delete this education information." onclick="deleteQualification(<?php echo $educationDetail->id;?>)"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                                </td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div id="menu3" class="tab-pane fade">
                                <div  style="margin: 14px; float: right; padding: 10px;">
                                    <span><button type="button" data-toggle="modal" data-target="#addEmploymentHistoryModal"><i class="fa fa-plus" aria-hidden="true"></i></button></span>
                                </div>
                                <div class="">
                                    <table class="table">
                                        <thead>
                                          <tr>
                                            <th>Company</th>
                                            <th>Role</th>
                                            <th>Responsibilities</th>
                                            <th>Period</th>
                                            <th>Action</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach($employmentInfo as $employmentDetail) { ?>
                                            <tr>
                                                <td><?php echo $employmentDetail->companyName;?></td>
                                                <td><?php echo $employmentDetail->roleOrPosition;?></td>
                                                <td><?php echo $employmentDetail->responsibilities;?></td>
                                                <td><?php echo $employmentDetail->dateFrom.' - ';?> 
                                                <?php echo !empty($employmentDetail->dateTo) ? $employmentDetail->dateTo : 'Present' ;?>
                                                </td>
                                                <td>
                                                    <button type="button" title="Please click here to edit this employment information." onclick="editEmployment(<?php echo $employmentDetail->id;?>)"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                                                    <button type="button" title="Please click here to delete this employment information." onclick="deleteEmployment(<?php echo $employmentDetail->id;?>)"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                                </td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div id="menu4" class="tab-pane fade">
                                <form role="form" method="post" id="otherInformationForm" style="margin-top: 24px;">
                                    <div class="row">
                                        <div class="col-md-4 col-md-offset-1">
                                            <label class="control-label">Location : </label>
                                        </div>
                                        <div class="col-md-6 col-md-offset-right-1">
                                            <?php
                                                $locationArray = array();
                                                foreach ($otherInfo->preferredLocation as $preferredLocationRow) {
                                                    $locationArray[] = $preferredLocationRow->name;
                                                }
                                            ?>
                                            <div class="form-group">
                                                <select data-placeholder="Enter the cities you want to work in" style="display: none;" multiple="" class="chosen-select location form-control" tabindex="-1" name="preferredLocation" id="preferredLocation" data-validation="alphanumeric required">
                                                    <option value=""></option>
                                                    <?php foreach ($city as $item) : ?>
                                                        <option value="<?php echo $item['city']; ?>" <?php echo in_array($item['city'], $locationArray) ? 'selected' :'' ;?>><?php echo $item['city']; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4 col-md-offset-1">
                                            <label class="control-label">Expected CTC(in lacs p.s) : </label>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <select class="selectpicker form-control" name="expectedCtcFrom" id="expectedCtcFrom" data-validation="required">
                                                    <option value="">CTC-From</option>
                                                    <?php for ($i = 0; $i <= 10; $i++) : ?>
                                                        <option value="<?php echo $i; ?>" <?php echo $i==$otherInfo->expectedCtcFrom ? 'selected' :'' ;?>><?php echo $i; ?></option>
                                                    <?php endfor; ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <select class="selectpicker form-control" name="expectedCtcTo" id="expectedCtcTo" data-validation="required">
                                                    <option value="">CTC-To</option>
                                                    <?php for ($t = 0; $t <= 9; $t++) : ?>
                                                        <option value="<?php echo $t; ?>" <?php echo $t==$otherInfo->expectedCtcTo ? 'selected' :'' ;?>><?php echo $t; ?></option>
                                                    <?php endfor; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4 col-md-offset-1">
                                            <label class="control-label">Work Emperience (Years) : </label>
                                        </div>
                                        <div class="col-md-6 col-md-offset-right-1">
                                            <div class="form-group">
                                                <select class="selectpicker form-control" name="workExperience" id="workExperience" data-validation="required">
                                                    <option value="">Select</option>
                                                    <?php for ($w = 0; $w <= 10; $w++) : ?>
                                                        <option value="<?php echo $w; ?>" <?php echo $w==$otherInfo->workExperience ? 'selected' :'' ;?>><?php echo $w; ?></option>
                                                    <?php endfor; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4 col-md-offset-1">
                                            <label class="control-label">Aadhar Card Number </label>
                                        </div>
                                        <div class="col-md-6 col-md-offset-right-1">
                                            <div class="form-group">
                                                <input type="text" class="form-control" value="<?php echo $otherInfo->adharCard; ?>" id="adharCard" name="adharCard" placeholder="Enter Aadhar Card Number">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4 col-md-offset-1">
                                            <label class="control-label">Languages </label>
                                        </div>
                                        <div class="col-md-6 col-md-offset-right-1">
                                            <div class="form-group">
                                                <input type="text" class="form-control" value="<?php echo $otherInfo->languages; ?>" id="languages" name="languages" placeholder="Enter your languages with comma separated">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <input type="hidden" name="token" id="token" value="ecbcd7eaee29848978134beeecdfbc7c">
                                        <input type="hidden" name="methodname" value="editOtherInfo">
                                        <input type="hidden" name="userId" value="<?php echo $_SESSION['userId'] ?>">

                                        <div class="col-md-2 col-md-offset-5">
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-lg btn-primary btn-block" style="    padding: 10px 16px;background: #11AAF0;border: #11AAF0;"><span class=""></span> SAVE </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <div id="menu5" class="tab-pane fade">
                                <form role="form" method="post" id="socialInformationForm" style="margin-top: 24px;">

                                    <div class="row">
                                        <div class="col-md-4 col-md-offset-1">
                                            <label class="control-label">Facebook : </label>
                                        </div>
                                        <div class="col-md-6 col-md-offset-right-1">
                                            <div class="form-group">
                                                <input type="text" class="form-control" value="<?php echo $socialInfo->facebookLink; ?>" id="facebookLink" name="facebookLink" placeholder="Plese enter your facebook link">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4 col-md-offset-1">
                                            <label class="control-label">LinkedIn : </label>
                                        </div>
                                        <div class="col-md-6 col-md-offset-right-1">
                                            <div class="form-group">
                                                <input type="text" class="form-control" value="<?php echo $socialInfo->linkedInLink; ?>" id="linkedInLink" name="linkedInLink" placeholder="Please enter your linkdin link">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4 col-md-offset-1">
                                            <label class="control-label">Twitter : </label>
                                        </div>
                                        <div class="col-md-6 col-md-offset-right-1">
                                            <div class="form-group">
                                                <input type="text" class="form-control" value="<?php echo $socialInfo->twitterLink; ?>" id="twitterLink" name="twitterLink" placeholder="Enter your twitter link">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4 col-md-offset-1">
                                            <label class="control-label">Googel Plus : </label>
                                        </div>
                                        <div class="col-md-6 col-md-offset-right-1">
                                            <div class="form-group">
                                                <input type="text" class="form-control" value="<?php echo $socialInfo->googlePlusLink; ?>" id="googlePlusLink" name="googlePlusLink" placeholder="Enter your google plus link">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <input type="hidden" name="token" id="token" value="ecbcd7eaee29848978134beeecdfbc7c">
                                        <input type="hidden" name="methodname" value="editSocialInfo">
                                        <input type="hidden" name="userId" value="<?php echo $_SESSION['userId'] ?>">

                                        <div class="col-md-2 col-md-offset-5">
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-lg btn-warning btn-block" style="     padding: 10px 16px;   background: #11AAF0;border: #11AAF0;"><span class=""></span> SAVE </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
			<div class="clearfix"></div>
            
        </div>
			</div>
			</div>
			</div>
</div>
</div>
<!-- /#wrapper -->

<!-- edd education Modal -->
<div class="modal fade" id="addEducationModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content" style="background-color: #999 !important;">
        <div class="modal-header" style="padding:15px 50px;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="text-center"><span></span> Save Qualification</h4>
        </div>
        <div class="modal-body" style="padding:40px 50px;">
          <form role="form" id="addQualificationFrom" method="post">
            <div class="form-group">
                
                <select class="form-control selectpicker" id="courseId" name="courseId">
                    <option value="">Select Course Type*</option>
                    <?php foreach($courseData as $courseRow) { ?>
                    <option value="<?php echo $courseRow->id;?>"><?php echo $courseRow->name;?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <select data-placeholder="University*" style="display: none;" class="chosen-select form-control" tabindex="-1" name="universityId" id="universityId">
                    <option value=""></option>
                    <?php foreach ($universityData as $universityRow) : ?>
                        <option value="<?php echo $universityRow->id; ?>"><?php echo $universityRow->name; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
              <input type="text" class="form-control" id="institute" name="institute" placeholder="Institute*">
            </div>
            <div class="form-group">
              <input type="text" class="form-control" id="completionYear" name="completionYear" placeholder="Year of Completion(e.g 2014)*">
            </div>
            <div class="form-group">
              <input type="text" class="form-control" id="percent" name="percent" placeholder="%(percentage e.g 58.48)*">
            </div>

            <input type="hidden" name="token" id="token" value="ecbcd7eaee29848978134beeecdfbc7c">
            <input type="hidden" name="methodname" value="editEducationalInfo">
            <input type="hidden" name="userId" value="<?php echo $token ?>">
            <input type="hidden" name="id" value="" id="educationId">

            <button type="submit" class="btn btn-warning btn-block"><span class=""></span> SAVE</button>
          </form>
        </div>
      </div>
    </div>
</div>
<!---add education modal end-->

<!-- edd education Modal -->
  <div class="modal fade" id="addEmploymentHistoryModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content" style="background-color: #999 !important;">
        <div class="modal-header" style="padding:15px 50px;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="text-center"><span></span> Save Employment</h4>
        </div>
        <div class="modal-body" style="padding:40px 50px;">
          <form role="form" id="addEmploymentFrom" method="post">
            <div class="form-group">
                <input type="text" class="form-control" id="companyName" name="companyName" placeholder="Company*">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" id="roleOrPosition" name="roleOrPosition" placeholder="Role/Position*">
            </div>
            
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        <input type="text" class="form-control" id="dateFrom" name="dateFrom" placeholder="From*">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <input type="text" class="form-control" id="dateTo" name="dateTo" placeholder="To">
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="form-group">
                        <label style="margin-top: 4px;" class="checkbox-inline"><input id="isPresent" name="isPresent" type="checkbox" value="1">Present</label>
                    </div>
                </div>
            </div>


            <div class="form-group">
              <textarea class="form-control" rows="3" id="responsibilities" name="responsibilities" placeholder="Responsibilities*"></textarea>
            </div>

            <input type="hidden" name="token" id="token" value="ecbcd7eaee29848978134beeecdfbc7c">
            <input type="hidden" name="methodname" value="editEmploymentInfo">
            <input type="hidden" name="userId" value="<?php echo $token ?>">
            <input type="hidden" name="id" value="" id="employmentId">

            <button type="submit" class="btn btn-warning btn-block"><span class=""></span>SAVE</button>
          </form>
        </div>
      </div>
    </div>
  </div>
<!---add education modal end-->

<!-- Basic profile js JavaScript -->

			</div>
                
            </div>
            <div class="modal-footer">
				
            </div>     
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->




<div class="modal fade" id="EditResumeModal">
    <div class="modal-dialog" style="width:80%;">
        <div class="modal-content">
		<form id="messageReplyBox">
            
            <div class="modal-body" style="    height: 600px;padding-left: 0px;padding-right: 0px;padding-top: 0px;">
			<div id="popupContentEditProfile">
				 <div style="color:white;    background: #054f72;    font-size: 25px;padding: 9px;    padding-left: 47px;">
								<div class="">
									<a style="margin-right: 18px;cursor:pointer;    color: white;"  data-dismiss="modal" aria-hidden="true"  title="Back to Edit Profile"><i class="fa fa-arrow-left" aria-hidden="true"></i></a><span>Upload Resume Document</span>
								</div>
							</div>
        <div class="container-full" style="background: white;">
           
            <div  class="col-md-12 col-lg-12 col-sm-12 pull-left" style="border:1px solid lightgrey;padding-bottom: 23px;margin-bottom: 20px;padding-top: 20px;background: white ;">

                <div class="col-lg-12" style="margin-bottom: 60px">
                    <center><span style="font-size: 14px;">Only PDF, Doc, Docx file can be uploaded. You can keep multiple copies of resumes. But only one resume can be active.</span></center>
                </div>
                <?php if(count($resumelist) > 0) { ?>
                    <div class="col-lg-12">
        				<?php
        				$i = 1;
        				foreach($resumelist as $list){ ?>
                            <div style="margin-bottom: 15px" class="col-md-3 col-lg-3 col-sm-3 pull-left"></div>
        					<div style="margin-bottom: 15px" class="col-md-3 col-lg-3 col-sm-3 pull-left"><a target="_blank" href="<?php echo $list->url ?>"><?php echo $list->resumeName ?></a></div>
        					<!--<div class="col-md-2 col-lg-2 col-sm-2 pull-left"><?php if($list->active == 1){ echo "Active"; }else{ echo "Inactive"; } ?></div>-->
        					<div style="margin-bottom: 15px;color: black;" class="col-md-3 col-lg-3 col-sm-3 pull-left"><?php echo $list->createdAt ?></div>
        					<div style="margin-bottom: 15px" class="col-md-3 col-lg-3 col-sm-3 pull-left">
        					<button id="changepasswordbutton1" name="Submit" value="Save" title="Please click here to make this resume active." type="button" onclick="makeActive(<?php echo $list->resumeId ?>,'<?php echo $list->resumeName?>')">
                            <?php if($list->active == 1) { ?>
                                <i class="fa fa-check" aria-hidden="true"></i>
                            <?php } else { ?>
                                <i class="fa fa-cloud-upload" aria-hidden="true"></i>
                            <?php } ?>
                                </button>
        					<button id="changepasswordbutton1" name="Submit" value="Save" type="button" title="Please click here to delete resume."  onclick="deleteResume(<?php echo $list->resumeId ?>)"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
        					
        					</div>
							<div class="clearfix"></div>
        				<?php 
        				$i++;
        				}
        				?>
						<div style="    float: right;margin-top: 16px;">
                            <i class="fa fa-cloud-upload" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Please click on this button to make this resume active.<br>
						<i class="fa fa-trash-o" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Please click on this button to delete resume.

						</div>
    				</div>

                <?php } ?>
				
				<div class="col-lg-12" style="    margin-top: 19px;">
				<hr style="border-top: 1px solid #000;">
                    <div class="row">
		<form method="post" action="" name="createUser" enctype="multipart/form-data" id="createSubuserForm" class="has-validation-callback">
			<div id="page-content-wrapper">
                        <div class="container-fluid whitebg">
            <div class="row">


                <div class="clearfix"></div>
				<?php
				//print_r($basic);die;
				?>
                <div class="col-md-12 content_para">

					<form method="post" action="" name="frmUsers" id="uploadResume">
    <div id="page-content-wrapper">
        <div class="container-fluid whitebg">
            <div class="row">
                <div class="col-md-12 padd_tp_bt_20">
                    <div class="form-horizontal form_save">
                        <div class="form-group">
                            <label for="inputEmail" class="control-label col-xs-3 required-field ">Upload Resume</label>
                            <div class="col-xs-9">
                                <input type="file" class="form-control" id="resumeFile" placeholder="Old Password" name="resumeFile" value="">
                                <input type="hidden" class="form-control" placeholder="Old Password" id="resumeName" name="resumeName" value="test">
                            </div>
                        </div>
                        
                        

                        <div class="clearfix">


                            <div class="clearfix">
                                <div class="clearfix">
                                    <div class="clearfix">
                                        <div class="clearfix">
                                            <input type="hidden" name="token" id="token" value="ecbcd7eaee29848978134beeecdfbc7c">
                                            <input type="hidden" name="methodname" id="methodname" value="editResume_v2">
                                            <input type="hidden" name="userId" id="userId" value="<?php echo $_SESSION['userId'] ?>">
                                            
                                            <div class="col-xs-offset-2 col-xs-2 no_padding" style="    margin-left: 24%;">
                                                <button class="btn btn-lg btn-primary btn-block" id="changepasswordbutton" style="display:none;" name="Submit" value="Save" type="button" onclick="UploadResume()">UPLOAD</button>  			
                
                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /#page-content-wrapper -->
                        </div>
					</div>
                </div>
			</div>
		</div>
	</div>


</form>
                        


                </div>

                <div class="clearfix"></div>
                			
                </div>
            </div>

        </div>

    </div>

</form>
	</div>

                </div>
            <!-- /.row -->
            
          

                </div>

			</div>
                
            </div>
                  
			</form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade" id="EditSendModalPopup" >
    <div class="modal-dialog" style="width:80%;">
        <div class="modal-content">
		<form id="messageReplyBox">
            
            <div class="modal-body" style="    height: 600px;padding-left: 0px;padding-right: 0px;padding-top: 0px;">
			<div id="popupContentEditProfile">
				 <div style="color:white;    background: #054f72;    font-size: 25px;padding: 9px;    padding-left: 47px;">
								<div>
									<a style="margin-right: 18px; cursor:pointer;   color: white;"  data-dismiss="modal" aria-hidden="true"  title="Back to Edit Profile"><i class="fa fa-arrow-left" aria-hidden="true"></i></a><span>Send Resume Document by Email</span>
								</div>
							</div>
        <div class="container-full" style="background: white;">
            
            <div  class="col-md-12 col-lg-12 col-sm-12 " style="">
                <div class="col-lg-12">
                    <p>Please send resume document by email to "<b>resume@vdohire.com</b>".</p>
                    <p>The email should be sent from your registered email account "<b><?php echo $_SESSION['userEmail'];?></b>".</p>
                    <p>Only PDF, DOC, DOCX documents will be accepted.</p>
                    <p>Please attach only one document with one email.</p>
                    <p>It will take about 30 minutes to process your resume document sent by email.</p>
                </div>
            <!-- /.row -->
            
            
            <!-- /.row -->

                </div>

        </div>
			</div>
                
            </div>
                  
			</form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="EditVideoModal">
    <div class="modal-dialog" style="width:80%;">
        <div class="modal-content">
		<form id="messageReplyBox">
            
            <div class="modal-body" style="    height: 600px;padding-left: 0px;padding-right: 0px;padding-top: 0px;">
			<div id="popupContentEditProfile">
				 <div style="color:white;    background: #054f72;    font-size: 25px;padding: 9px;    padding-left: 47px;">
					<div class="">
						<a style="margin-right: 18px;cursor:pointer; color: white;"  data-dismiss="modal" aria-hidden="true" title="Back to Edit Profile"><i class="fa fa-arrow-left" aria-hidden="true"></i></a><span>Upload Video Resume</span>
					</div>
				</div>
        <div class="container-full" style="background: white;">
			<div>
				<a class="btn btn-primary" href="<?php echo base_url()."videoResume/instruction"; ?>" style="    padding:8px;
                    font-size: 14px; background: #11AAF0;border: none;color:white;    text-decoration: none;    padding-left: 7px;padding-right: 7px;margin-top:10px; margin-bottom: 10px; float: right;margin-right: 13px;">ADD VIDEO RESUME
				</a>
			</div>
            
			<div class="clearfix"></div>
            <?php if(count($Videoresumelist) > 0) { ?>
            <div  class="col-md-12 col-lg-12 col-sm-12 pull-left" id="auditionAvailable" style="border:1px solid lightgrey;padding-bottom: 23px;margin-bottom: 20px;padding-top: 20px;background: white ;">
			<div class="col-lg-12" style="margin-bottom: 60px">
                    
                </div>
                
                    <div class="col-lg-12">
        				<?php
        				$i = 1;
						//echo "<pre>";print_r($Videoresumelist);
        				foreach($Videoresumelist as $list){ ?>
        					<div style="margin-bottom: 15px" class="col-md-3 col-lg-3 col-sm-3 pull-left">
							<a target="_blank" href="<?php echo $list->auditionUrl ?>">Audition <?php echo $i; ?></a></div>
        					
        					
        					<div style="margin-bottom: 15px;color: black;" class="col-md-1 col-lg-1 col-sm-1 pull-right"><?php echo $list->status ?></div>
							<div style="margin-bottom: 15px;color: black;" class="col-md-1 col-lg-1 col-sm-1 pull-right"><a href="javascript://" onclick="deleteAuditions(<?php echo $list->id ?>)"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div>
        					
							<div class="clearfix"></div>
        				<?php 
        				$i++;
        				}
        				?>
    				</div>

				<div class="clearfix"></div>
				<hr style="    border-top: 1px solid black;">
                    <div id="page-content-wrapper">
                        <div class="container-fluid whitebg">
                            <div class="row">
                			
                                <div class="col-md-12 padd_tp_bt_20">
                				<div class="col-md-12 text-center pull-right">
                					Questions: <select class="selectpicker" id="questionDropdown" onchange="showAuditionForQuestion()" style="color:black;    margin-top: 20px;margin-bottom: 27px;width: 263px;">
                									<option value="0">Tell us something about yourself?</option>
                									<option value="1">Tell us about your strengths?</option>
                									<option value="2">Tell us about your weakness?</option>
                									<option value="3">Tell us about any of your project/work that you are proud of?</option>
                								</select>
                				</div>
                                <div class="col-md-12 text-center pull-right">
                						<?php
                						
                						$auditionsVideos = $Videoresumelist[0];
                						$auditionArray = array_filter(explode(",", $auditionsVideos->auditionAnsFiles));
                						
                						if(!empty($auditionArray)){
                							for($count = 0;$count<=count($auditionArray);$count++){?>
                								<?php
                								$url = 'https://s3.amazonaws.com/fjauditions/';
                								if($count == 0){ ?>
                									
                									<div id="audition_<?php echo $count ?>" style="display:block;">
                										<video id="auditionVideo_<?php echo $count; ?>" style="width:400px;" src='<?php echo $url.trim($auditionArray[$count]) ?>' controls>
                									</div>
                								<?php }else{
                								?>
                									<div id="audition_<?php echo $count ?>" style="display:none">
                										<video id="auditionVideo_<?php echo $count; ?>" style="width:400px;"  src='<?php echo $url.trim($auditionArray[$count]) ?>' controls>
                									</div>
                								<?php } ?>
                							<?php }
                						
                						}
                						?>
                						
                					
                					</div>
                                </div>
                			</div>
                		</div>
                	</div>


            <!-- /.row -->

                </div>
			<?php } else {
				echo "<div style='clear:both'></div><center style='    margin-top: 11%;' id='auditionNotAvaialble'>Sorry No Video Resume Available.</center>";
				
			} ?>
            
        </div>
			</div>
                
            </div>
                  
			</form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->




<input type="hidden" name="commonBaseUrl" id="commonBaseUrl" value="<?php echo base_url()?>">
<script src="<?php echo base_url() ?>theme/js/formValidation.min.js"></script>
<script src="<?php echo base_url() ?>theme/js/chosen.jquery.js"></script>
<script src="<?php echo base_url() ?>theme/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>js/editbasicprofile.js"></script>

<!--<script src="<?php //echo base_url(); ?>js/editbasicprofile.js"></script>-->

		<script src="<?php echo base_url(); ?>js/common.js"/></script>
    <script src="<?php echo base_url(); ?>js/min/resume.min.js"></script>
    <!-- jQuery -->
	
	<style>
	
	#cover{ 
		position:fixed; top:0; left:0; background:rgba(0,0,0,0.6); z-index:0; width:100%; height:100%; display:none; }
	</style>
	<div class="modal fade" id="ThankYouMessage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="    opacity: 1;margin-top: 24%;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header" style="border-bottom:none;">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h2 id="myModalLabel" style="color:#08688F;font-size: 20px;margin-top: 33px;">
                <img src="<?php echo base_url();?>images/logo1.png" style="width:150px;">
            </h2>
      </div>
        <div class="modal-body">
			<div style="text-align: center;">
					<div id="progress-circle" class="progress-circle progress-0" style="    width: 93px;height: 93px;">
						<span id="percet_text" style="    height: 41px;    width: 41px;    line-height: 45px;margin-left: -22px;margin-top: -19px;">0</span>
					</div>
			</div>
            <p style="font-size:16px;">Please wait while your video interview is being uploaded</p>
        </div>
        
    </div>
  </div>
</div>
	<div id="cover" > </div>
	
    
</body>
</html>
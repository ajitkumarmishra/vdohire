<?php if($main_content == 'editbasicprofile' || $main_content == 'resume' || $main_content == 'send_resume_document' || $main_content == 'edit_profile'){
	
}else{ ?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="refresh" content="IE=edge" content="10">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
		
<link rel="shortcut icon" href="https://vdohire.com/images/favicon.ico" type="image/x-icon">
<link rel="icon" href="https://vdohire.com/images/favicon.ico" type="image/x-icon">

        <title>VDOhire Job Seeker Portal</title>
        <script src="<?php echo base_url(); ?>theme/vendor/jquery/jquery.min.js"></script>

        <!-- Bootstrap Core CSS -->
        <link href="<?php echo base_url(); ?>theme/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="<?php echo base_url(); ?>theme/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

        <!-- DataTables CSS -->
        <link href="<?php echo base_url(); ?>theme/vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

        <!-- DataTables Responsive CSS -->
        <link href="<?php echo base_url(); ?>theme/vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
    
        <!-- Custom CSS -->
        <link href="<?php echo base_url(); ?>theme/dist/css/sb-admin-2.css" rel="stylesheet">

        <!-- Morris Charts CSS -->
        <link href="<?php echo base_url(); ?>theme/vendor/morrisjs/morris.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="<?php echo base_url(); ?>theme/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!--				<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Montserrat"/>-->
<!--        <link href="--><?php //echo base_url(); ?><!--css/flat-ui.css" rel="stylesheet" type="text/css" />-->

        <link rel="stylesheet" href="<?php echo base_url(); ?>css/montserrat.css"/>
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/roboto.css"/>
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/edmondsans.css"/>
		<link rel="stylesheet" href="<?php echo base_url(); ?>theme/dist/css/chosen.css"/>
				
        <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-multiselect-widget/2.0.1/jquery.multiselect.css" rel="stylesheet">

        <input type="hidden" name="commonBaseUrl" id="commonBaseUrl" value="<?php echo base_url()?>">
<!--        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>-->
		
		
		<link rel="stylesheet" href="<?php echo base_url();?>css/progress-circle.css">
		

    </head>
    <body>

        <input type="hidden" name="currentbase_url" value="<?php echo base_url()?>" id="currentbase_url">
        <input type="hidden" name="currentuserId" value="<?php echo $_SESSION['userId'] ?>" id="currentuserId">
        <input type="hidden" name="currentjobCode" value="<?php echo isset($_SESSION['jobCode'])?$_SESSION['jobCode']:""; ?>" id="currentjobCode">
        <input type="hidden" name="currentinterviewSet" value="<?php echo isset($_SESSION['interviewSet'])?$_SESSION['interviewSet']:""; ?>" id="currentinterviewSet">
        <input type="hidden" name="currentUserName" value="<?php echo $_SESSION['userName']; ?>" id="currentUserName">
        <input type="hidden" name="currentinvitationId" value="<?php echo $_SESSION['invitationId']; ?>" id="currentinvitationId">

<?php } ?>
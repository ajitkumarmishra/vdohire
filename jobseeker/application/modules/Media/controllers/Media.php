<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Users Controller
 * Description : Used to handle all the jobseeker related data
 * @author Synergy
 * @createddate : April 3, 2016
 * @modificationlog : Initializing the controlelr
 * @change on Mar 24, 2017
 */
class Media extends MY_Controller {

    public $allNotificationList = array();
    /**
     * Responsable for inherit the parent connstructor
     * @return void
     */
    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('cookie');
        $this->load->helper('jwt');
        $this->load->helper('fj');

        $this->getEditProfileData();
        //Use to get Locations for search Job
        $this->searchLocationList = getLocationCities();

        $userRealData = $this->session->get_userdata();
        //print_r($userRealData);exit;

        if (isset($userRealData['userId'])) {
            /* Get Notification List for user */
            $userId = $userRealData['userId'];

            $statictoken = 'ecbcd7eaee29848978134beeecdfbc7c';
            if($this->config->item('HTTPSENABLED') == 1){
				$base_url = 'https://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
			}else{
				$base_url = 'http://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
			}
            $json = array("token" => $statictoken, "methodname" => 'getUserNotifications', 'userId' => $userId);                                                                    
            $data_string = json_encode($json);                                                                         
                                                                                                                                 
            $ch = curl_init($base_url);                                                                      
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
                'Content-Type: application/json',                                                                                
                'Content-Length: ' . strlen($data_string))                                                                       
            );                                                                                                                   
                                                                                                                                 
            $result = curl_exec($ch);
            $allResult = json_decode($result);
            $this->allNotificationList = $allResult->data;

            /* End of get notification list fo user */
        }
    }

    /**
     * Function Naem : index
     * Discription : Use to display login page for job seeker
     * @author Synergy
     * @param none
     * @return render data into main view page
     */
    
    function assessments() {
        $session = $this->session->get_userdata();
        $statictoken = 'ecbcd7eaee29848978134beeecdfbc7c';
        if($this->config->item('HTTPSENABLED') == 1){
			$base_url = 'https://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
		}else{
			$base_url = 'http://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
		}
		$data = array("token" => $statictoken, "methodname" => 'verifyJob', 'referrenceJobId' => $_SESSION['jobCode'], 'deviceUniqueId' => '1111111', 'searchBy' => 'job' );                                                                    
        $data_string = json_encode($data);                
		
        $ch = curl_init($base_url);                                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data_string))                                                                       
        );                                                                                                                   
                                                                                                                             
        $result = curl_exec($ch);
		
        $allResult = json_decode($result);
        $invitaionDetails = $allResult->data;
       
        
        
        $data['invitationDetails'] = $invitaionDetails[0];
		
		//assessment details
		$assessmentJson = array("token" => $statictoken, "methodname" => 'assessmentQuestionList_v2', 'jobCode' => $_SESSION['jobCode'], 'userId' => $_SESSION['userId'] );                                                                    
        $data_string = json_encode($assessmentJson);                
		
        $ch = curl_init($base_url);                                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data_string))                                                                       
        );                                                                                                                   
                                                                                                                             
        $result = curl_exec($ch);
		
        $allResult = json_decode($result);
        $assessmentDetails = $allResult->data;
       
        
        
        $data['assessmentDetails'] = $assessmentDetails[0];
		$_SESSION['AssessmentSetId'] = $data['assessmentDetails']->setID;
        $data['city'] = $this->searchLocationList;
        $data['allNotificationList'] = $this->allNotificationList;
		//echo "<pre>";print_r($_SESSION);die;	

        if(isset($_SESSION['userId']) && $_SESSION['userId'] != ""){
            $data['token'] = $_SESSION['userId'];
            
            $data['main_content'] = 'assessment';
            $this->load->view('fj-mainpage-withoutMenu', $data);
        } else {
            redirect("https://" . $_SERVER["HTTP_HOST"] . "", 'refresh');
        }
    }

    function index() {
        $session = $this->session->get_userdata();
        $token = $session['token'];
        $userEmail = $session['userEmail'];
        $userName = $session['userName'];
        $userId = $session['userId'];

        if (isset($userId) && $userId != "") {
            $data['token'] = $userId;
            $data['userName'] = $userName;
            $data['email'] = $userEmail;
            $data['city'] = $this->searchLocationList;
            $data['allNotificationList'] = $this->allNotificationList;
            $data['main_content'] = 'index';
            $this->load->view('fj-mainpage-withoutMenu', $data);
        } else {
            redirect("https://" . $_SERVER["HTTP_HOST"] . "", 'refresh');
        }

        //echo 'hello';exit;
    }

    function setup() {
        $this->load->library('session');
        $this->load->helper('cookie');
        //$invitationId = $this->uri->segment('3');
        //$session = $this->session->get_userdata();
        //$_SESSION['invitationId'] = $invitationId;
        
		$statictoken = 'ecbcd7eaee29848978134beeecdfbc7c';
        if($this->config->item('HTTPSENABLED') == 1){
			$base_url = 'https://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
		}else{
			$base_url = 'http://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
		}
		$data = array("token" => $statictoken, "methodname" => 'verifyJob', 'referrenceJobId' => $_SESSION['jobCode'], 'deviceUniqueId' => '1111111', 'searchBy' => 'job' );                                                                    
        $data_string = json_encode($data);                
		
        $ch = curl_init($base_url);                                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data_string))                                                                       
        );                                                                                                                   
                                                                                                                             
        $result = curl_exec($ch);
		
        $allResult = json_decode($result);
        $invitaionDetails = $allResult->data;
       
        
        
        $data['invitationDetails'] = $invitaionDetails[0];
		
		//check save Jobs
		$checkAppliedJobjson = array("token" => $statictoken, "methodname" => 'getSavedJobStatus', 'userId' => $_SESSION['userId'], 'jobCode' => $_SESSION['jobCode']);                                                                    
        $data_string = json_encode($checkAppliedJobjson);                
		
        $ch = curl_init($base_url);                                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data_string))                                                                       
        );                                                                                                                  
                                                                                                                             
        $result = curl_exec($ch);
		
		curl_close ($ch);
		$allResult = json_decode($result);
        $data['checkSavedJobs'] = $allResult->code;
		
		//getFirstQuestin list
		$checkAppliedJobjson = array("token" => $statictoken, "methodname" => 'interviewQuestionListInvite', 'userId' => $_SESSION['userId'], 'jobCode' => $_SESSION['jobCode']);                                                                    
        $data_string = json_encode($checkAppliedJobjson);                
		
        $ch = curl_init($base_url);                                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data_string))                                                                       
        );                                                                                                                  
                                                                                                                             
        $result = curl_exec($ch);
		
		curl_close ($ch);
		$allResult = json_decode($result);
		$questIonList = $allResult->data;
		$firstQuestionDetails = $questIonList[0];
		$firstQuestion = $firstQuestionDetails->questionID;
		$interviewSet = $firstQuestionDetails->setID;
		
		//show question detail invite
		$checkAppliedJobjson = array("token" => $statictoken, "methodname" => 'interviewQuestionDetailInvite', 'userId' => $_SESSION['userId'],'interviewSet' => $interviewSet,'questionId' => $firstQuestion, 'jobCode' => $_SESSION['jobCode']);                                                                    
        $data_string = json_encode($checkAppliedJobjson);                
		
        $ch = curl_init($base_url);                                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data_string))                                                                       
        );                                                                                                                  
                                                                                                                             
        $result = curl_exec($ch);
		
		curl_close ($ch);
		$allResult = json_decode($result);
		$QuestionDetails = $allResult->data;
		$FstQuestionDetails = $QuestionDetails[0];
		$data['FirstQuestionDuration'] = $FstQuestionDetails->duration;
		
		
		
		
		
		if(isset($_SESSION['userId']) && $_SESSION['userId'] != "") {
			$data['main_content'] = 'setup';
			$this->load->view('fj-mainpage-withoutMenu', $data);
		} else {
			redirect(base_url(),'refresh');
		}
    }

    function question() {
        $statictoken = 'ecbcd7eaee29848978134beeecdfbc7c';
        if($this->config->item('HTTPSENABLED') == 1) {
			$base_url = 'https://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
		} else {
			$base_url = 'http://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
		}

		$dataverifyJob = array("token" => $statictoken, "methodname" => 'verifyJob', 'referrenceJobId' => $_SESSION['jobCode'], 'deviceUniqueId' => '1111111', 'searchBy' => 'job' );

        $data_stringverifyJob = json_encode($dataverifyJob);
        $chVerify = curl_init($base_url);                                                                      
        curl_setopt($chVerify, CURLOPT_CUSTOMREQUEST, "POST");                         
        curl_setopt($chVerify, CURLOPT_POSTFIELDS, $data_stringverifyJob);                   
        curl_setopt($chVerify, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($chVerify, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($chVerify, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($chVerify, CURLOPT_HTTPHEADER, array(                                   
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_stringverifyJob))
        );

        $resultdataverifyJob = curl_exec($chVerify);
        $allResultverifyJob = json_decode($resultdataverifyJob);
        $invitaionDetails = $allResultverifyJob->data;
		$data['invitationDetails'] = $invitaionDetails[0];

	   //set visitor log
		$ip =  $_SERVER['REMOTE_ADDR'] ;
		$page = "videoInstruction";
		$useragent = $_SERVER ['HTTP_USER_AGENT'];
		$cookie = isset($_COOKIE['visitor_cookie']) ? $_COOKIE['visitor_cookie'] : '';
		$userEmail = isset($_SESSION['userEmail']) ? $_SESSION['userEmail'] : '';
		
		if($cookie == "") {
            $cookie_value = rand(10000,99999).date('YmdHis');
			setcookie('visitor_cookie', $cookie_value, time() + (7200), "/"); // 86400 = 1 day
            $cookie = $cookie_value;
		}

		$jsonVisitor = array("token" => $statictoken, "methodname" => 'userVisitLog', "ip" => $ip, "useragent" => $useragent, "page" => $page,  "cookie" => $cookie, "userEmail" => $userEmail);

		$data_string_visitor = json_encode($jsonVisitor);
		$ch_visitor = curl_init($base_url);
		curl_setopt($ch_visitor, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch_visitor, CURLOPT_POSTFIELDS, $data_string_visitor);
        curl_setopt($ch_visitor, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch_visitor, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch_visitor, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch_visitor, CURLOPT_HTTPHEADER, array(                                        
			'Content-Type: application/json',
			'Content-Length: ' . strlen($data_string_visitor))
		);
		$curlResult = curl_exec($ch_visitor);

	   //interviewQuestionListInvite
	   $dataInvite = array("token" => $statictoken, "methodname" => 'interviewQuestionListInvite', 'jobCode' => $_SESSION['jobCode'], 'userId' => $_SESSION['userId'] );
        $data_stringQuestionListInvite = json_encode($dataInvite);                
        $ch = curl_init($base_url);                                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_stringQuestionListInvite);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_stringQuestionListInvite))
        );
        $result = curl_exec($ch);
        $allResult = json_decode($result);
        $JobDetails = $allResult;
		
		$data['jobActivationCode'] = $JobDetails->code;
		$data['jobActivationMessage'] = $JobDetails->message;
		
		//check save Jobs
		$checkAppliedJobjson = array("token" => $statictoken, "methodname" => 'getSavedJobStatus', 'userId' => $_SESSION['userId'], 'jobCode' => $_SESSION['jobCode']);
        $data_stringSavedJobStatus = json_encode($checkAppliedJobjson);                
        $chSavedJobStatus = curl_init($base_url);                                                                      
        curl_setopt($chSavedJobStatus, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($chSavedJobStatus, CURLOPT_POSTFIELDS, $data_stringSavedJobStatus);
        curl_setopt($chSavedJobStatus, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($chSavedJobStatus, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($chSavedJobStatus, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($chSavedJobStatus, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_stringSavedJobStatus))
        );
        $resultSavedJobStatus = curl_exec($chSavedJobStatus);
		curl_close ($chSavedJobStatus);

		$allResultSavedJobStatus = json_decode($resultSavedJobStatus);
        $data['checkSavedJobs'] = $allResultSavedJobStatus->code;
		
        if(isset($_SESSION['userId']) && $_SESSION['userId'] != "") {
			$data['token'] = $_SESSION['userId'];
			$data['userName'] = isset($_SESSION['userName']) ? $_SESSION['userName'] : '';
			$data['email'] = $_SESSION['userEmail'];
			$data['main_content'] = 'question';
			$this->load->view('fj-mainpage-withoutMenu', $data);
		} else {
			redirect(base_url(),'refresh');
		}  
    }

    function videoInterview() {
        //$this->load->helper('cookie');
        $this->load->library('session');

        $session = $this->session->get_userdata();
        $this->load->helper('cookie');

        $session = $this->session->get_userdata();
        $token = $session['token'];
        $userEmail = $session['userEmail'];
        $userName = $session['userName'];
        $userId = $session['userId'];
        if (isset($userId) && $userId != "") {
            $data['token'] = $userId;
            $data['userName'] = $userName;
            $data['email'] = $userEmail;
            $data['main_content'] = 'video';
            $this->load->view('fj-mainpage-withoutMenu', $data);
        } else {
            redirect("https://" . $_SERVER["HTTP_HOST"] . "", 'refresh');
        }
    }

    function questionView() {
        $session = $this->session->get_userdata();
        $statictoken = 'ecbcd7eaee29848978134beeecdfbc7c';
        if($this->config->item('HTTPSENABLED') == 1){
			$base_url = 'https://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
		}else{
			$base_url = 'http://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
		}
		$data = array("token" => $statictoken, "methodname" => 'verifyJob', 'referrenceJobId' => $_SESSION['jobCode'], 'deviceUniqueId' => '1111111', 'searchBy' => 'job' );                                                                    
        $data_string = json_encode($data);                
		
        $ch = curl_init($base_url);                                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data_string))                                                                       
        );                                                                                                                   
                                                                                                                             
        $result = curl_exec($ch);
		
        $allResultJob = json_decode($result);
        $invitaionDetails = $allResultJob->data;
		
		//get InterviewSet
		$data = array("token" => $statictoken, "methodname" => 'interviewQuestionListInvite', 'userId' => $_SESSION['userId'], 'jobCode' => $_SESSION['jobCode']);                                                                    
        $data_string = json_encode($data);                
		
        $ch = curl_init($base_url);                                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data_string))                                                                       
        );                                                                                                                   
                                                                                                                             
        $result = curl_exec($ch);
		
        $allResult = json_decode($result);
        $InterviewSetDetails = $allResult->data;
		
        
        
        $data['InterviewSetDetails'] = $InterviewSetDetails[0];
		
		$_SESSION['interviewSet'] = $data['InterviewSetDetails']->setID;
		if(isset($_SESSION['invitationId']) && $_SESSION['invitationId'] != ""){
			$data['isInvite'] = 1;
		}else{
			$data['isInvite'] = 1;//change this to 0 later
		}
		
		$data['invitationDetails'] = $invitaionDetails[0];
		
        if(isset($_SESSION['userId']) && $_SESSION['userId'] != ""){
            $data['token'] = $_SESSION['userId'];
            $data['userName'] = $_SESSION['userName'];
            $data['email'] = $_SESSION['userEmail'];
            $data['main_content'] = 'question_view';
            $this->load->view('fj-mainpage-withoutMenu', $data);
        } else {
            redirect(base_url(), 'refresh');
        }
    }

    function SubmitAns() {
        $a = array('101' => 'ab', '122' => 'cd');
        $b = array('ab' => '11111', '1e2' => '222222222');
        array_push($a, $b);
        echo "<pre>";
        print_r($a);
        die;

        //$this->load->library('session');
        $SesssionArray = $_SESSION['AssessmentAns'];
        //echo "<pre>";print_r($SesssionArray);die;
        //$counter = count($SesssionArray);
        $inputSesssionArray = $SesssionArray . "$%$" . $_POST['question'] . ":" . $_POST['ans'] . "$%$";

        $_SESSION['AssessmentAns'] = $inputSesssionArray;
        /* foreach($this->session->get_userdata('AssessmentAns') as $key=>$data){
          $SesssionArray[$key] = $data;

          } */

        echo "<pre>";
        print_r($_SESSION);
        die;
    }

    function uploadResume() {
        //$this->load->helper('cookie');
        $this->load->library('session');

        $session = $this->session->get_userdata();
        $this->load->helper('cookie');
		
		$statictoken = 'ecbcd7eaee29848978134beeecdfbc7c';
        if($this->config->item('HTTPSENABLED') == 1){
			$base_url = 'https://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
		}else{
			$base_url = 'http://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
		}
		$data = array("token" => $statictoken, "methodname" => 'verifyJob', 'referrenceJobId' => $_SESSION['jobCode'], 'deviceUniqueId' => '1111111', 'searchBy' => 'job' );                                                                    
        $data_string = json_encode($data);                
		
        $ch = curl_init($base_url);                                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data_string))                                                                       
        );                                                                                                                   
                                                                                                                             
        $result = curl_exec($ch);
		
        $allResult = json_decode($result);
        $invitaionDetails = $allResult->data;
		
		
		
		$data = array("token" => $statictoken, "methodname" => 'getUserResumes', 'userId' => $_SESSION['userId']);                                                                    
		$data_string = json_encode($data);                                                                                   
																															 
		$ch = curl_init($base_url);                                                                      
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);		
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
			'Content-Type: application/json',                                                                                
			'Content-Length: ' . strlen($data_string))                                                                       
		);                                                                                                                   
																															 
		$result = curl_exec($ch);
		$allResult = json_decode($result);
		$resumeList = $allResult->resumeList;
		$data['resumelist'] = $resumeList;
		$data['invitationDetails'] = $invitaionDetails[0];
		
		
         if(isset($_SESSION['userId']) && $_SESSION['userId'] != ""){
            $data['token'] = $userId;
            $data['userName'] = $userName;
            $data['email'] = $userEmail;
            $data['main_content'] = 'uploadResume';
            $this->load->view('fj-mainpage-withoutMenu', $data);
        } else {
            redirect("https://" . $_SERVER["HTTP_HOST"] . "", 'refresh');
        }
    }

    function quidelines() {
        //$this->load->helper('cookie');
        $this->load->library('session');
		
        $session = $this->session->get_userdata();
        $this->load->helper('cookie');
		$statictoken = 'ecbcd7eaee29848978134beeecdfbc7c';
        if($this->config->item('HTTPSENABLED') == 1){
			$base_url = 'https://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
		}else{
			$base_url = 'http://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
		}
		$data = array("token" => $statictoken, "methodname" => 'verifyJob', 'referrenceJobId' => $_SESSION['jobCode'], 'deviceUniqueId' => '1111111', 'searchBy' => 'job' );                                                                    
        $data_string = json_encode($data);                
		
        $ch = curl_init($base_url);                                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data_string))                                                                       
        );                                                                                                                   
                                                                                                                             
        $result = curl_exec($ch);
		
        $allResult = json_decode($result);
        $invitaionDetails = $allResult->data;
       
        
        
        $data['invitationDetails'] = $invitaionDetails[0];
        
		
		
		
        if(isset($_SESSION['userId']) && $_SESSION['userId'] != ""){
            $data['token'] = $userId;
            $data['userName'] = $userName;
            $data['email'] = $userEmail;
            $data['main_content'] = 'assessmentGuidliance';
            $this->load->view('fj-mainpage-withoutMenu', $data);
        } else {
            redirect("https://" . $_SERVER["HTTP_HOST"] . "", 'refresh');
        }
    }

    function thankyou() {
        //$this->load->helper('cookie');
        $this->load->library('session');
		$session = $this->session->get_userdata();
		
		$statictoken = 'ecbcd7eaee29848978134beeecdfbc7c';
        if($this->config->item('HTTPSENABLED') == 1){
			$base_url = 'https://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
		}else{
			$base_url = 'http://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
		}
		$data = array("token" => $statictoken, "methodname" => 'verifyJob', 'referrenceJobId' => $_SESSION['jobCode'], 'deviceUniqueId' => '1111111', 'searchBy' => 'job' );                                                                    
        $data_string = json_encode($data);                
		
        $ch = curl_init($base_url);                                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data_string))                                                                       
        );                                                                                                                   
                                                                                                                             
        $result = curl_exec($ch);
		
        $allResult = json_decode($result);
        $invitaionDetails = $allResult->data;
		$data['invitationDetails'] = $invitaionDetails[0];
		
		//set visitor log
		$ip =  $_SERVER['REMOTE_ADDR'] ;
		$page = "thankyou";
		$useragent = $_SERVER ['HTTP_USER_AGENT'];
		$cookie = $_COOKIE['visitor_cookie'];
		$userEmail = isset($_SESSION['userEmail'])?$_SESSION['userEmail']:"";
		
		if($cookie == ""){
			setcookie('visitor_cookie', $cookie_value, time() + (7200), "/"); // 86400 = 1 day
		}
		$jsonVisitor = array("token" => $statictoken, "methodname" => 'userVisitLog', "ip" => $ip, "useragent" => $useragent, "page" => $page,  "cookie" => $cookie, "userEmail" => $userEmail);
		
		$data_string_visitor = json_encode($jsonVisitor);                                                                                   
																															 
		$ch_visitor = curl_init($base_url);                                                                      
		curl_setopt($ch_visitor, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
		curl_setopt($ch_visitor, CURLOPT_POSTFIELDS, $data_string_visitor);                                                                  
		curl_setopt($ch_visitor, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch_visitor, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch_visitor, CURLOPT_HTTPHEADER, array(                                                                          
			'Content-Type: application/json',                                                                                
			'Content-Length: ' . strlen($data_string_visitor))                                                                       
		);                                                                                                                   
		$curlResult = curl_exec($ch_visitor);
		

        $session = $this->session->get_userdata();
        $token = $session['token'];
        $userEmail = $session['userEmail'];
        $userName = $session['userName'];
        $userId = $session['userId'];
        if (isset($userId) && $userId != "") {
            $data['token'] = $userId;
            $data['userName'] = $userName;
            $data['email'] = $userEmail;
            $data['main_content'] = 'thankyou';
            $this->load->view('fj-mainpage-withoutMenu', $data);
        } else {
            redirect("https://" . $_SERVER["HTTP_HOST"] . "", 'refresh');
        }
    }
}
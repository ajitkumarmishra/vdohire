

        <div class="container-full" style="background: white;color:white;">
            <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #054F72;height: 33px;">
                
            </div>
			<div  class="col-md-8 col-lg-8 col-sm-8 pull-left" style="font-size: 17px;color: white;font-weight: bold;width: 80%;background: #EB9B2C ;">
				<?php
				if($invitationDetails->assessmentDuration == ""){ ?>
					<div  class="col-md-1 col-lg-1 col-sm-1 pull-left" style=" padding-right:0px;padding-left:0px;   width: 17%;">
					</div>
					<div  class="col-md-1 col-lg-1 col-sm-1 pull-left" style=" padding-right:0px;padding-left:0px;   width: 27%;">
						<center>
							<div  class="img-circle" style="background: white;padding: 10px;width: 56px;height: 56px;">
								<img src="<?php echo base_url()?>images/icons/2000px-Checkmark_green.png" style="    width: 37px;">
				
							</div>
						</center>
						<center style="font-size:14px;">Video Interview</center>
					</div>
				
                
				
					<div  class="col-md-1 col-lg-1 col-sm-1 pull-left" style="    margin-top: 19px;">
						<center>
							<img src="<?php echo base_url()?>images/icons/arrow.png" style="width:58px">
						</center>
						
					</div>
				
					<div  class="col-md-1 col-lg-1 col-sm-1 pull-left" style="    width: 27%;">
						
						<center>
							<div  class="img-circle" style=" margin-top:2px;background: white;padding: 10px;width: 56px;height: 56px;">
								<img src="<?php echo base_url()?>images/icons/CV.png" style="    width: 37px;">
								
							</div>
						</center>
						<center style="font-size:14px;">Upload Resume</center>
						
					</div>
				<?php }else{ ?>
					<div  class="col-md-1 col-lg-1 col-sm-1 pull-left" style=" padding-right:0px;padding-left:0px;   width: 27%;">
						<center>
							<div  class="img-circle" style="background: white;padding: 10px;width: 56px;height: 56px;">
								<img src="<?php echo base_url()?>images/icons/2000px-Checkmark_green.png" style="    width: 37px;">
				
							</div>
						</center>
						<center style="font-size:14px;">Video Interview</center>
					</div>
				
                <div  class="col-md-1 col-lg-1 col-sm-1 pull-left" style="padding-right:0px;padding-left:0px;       margin-top: 19px;">
                    <center><img src="<?php echo base_url()?>images/icons/arrow.png" style="width:58px"></center>
                </div>
                <div  class="col-md-1 col-lg-1 col-sm-1 pull-left" style=" padding-right:0px;padding-left:0px;      width: 27%;">
					<center>
						<div  class="img-circle" style="background: white;padding: 10px; margin-top:2px;width: 56px;height: 56px;">
							<img src="<?php echo base_url()?>images/icons/2000px-Checkmark_green.png" style="    width: 37px;">
							
						</div>
					</center>
                    <center style="font-size:14px;">Assessments</center>
                </div>
				
                <div  class="col-md-1 col-lg-1 col-sm-1 pull-left" style="    margin-top: 19px;">
                    <center>
						<img src="<?php echo base_url()?>images/icons/arrow.png" style="width:58px">
					</center>
                    
                </div>
				
                <div  class="col-md-1 col-lg-1 col-sm-1 pull-left" style="    width: 27%;">
                    
					<center>
						<div  class="img-circle" style=" margin-top:2px;background: white;padding: 10px;width: 56px;height: 56px;">
							<img src="<?php echo base_url()?>images/icons/CV.png" style="    width: 37px;">
							
						</div>
					</center>
                    <center style="font-size:14px;">Upload Resume</center>
                    
                </div>
				<?php }
				?>
                
				
            </div>
			
            
            <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #054F72;height: 33px;">
                
            </div>
            
        </div>
        <div style="clear:both"></div>
        <div class="container-full" style="background: white;">
            <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #f8f8f8;height: 30px;">

                </div>
            <div  class="col-md-8 col-lg-8 col-sm-8 pull-left" style="border:1px solid lightgrey;padding-bottom: 23px;margin-bottom: 20px;padding-top: 20px;width: 80%;background: white ;">
			<div class="col-lg-12" style="margin-bottom: 60px">
                    <center><span style="font-size: 14px;">Only PDF, Doc, Docx file can be uploaded. You can keep multiple copies of resumes. But only one resume can be active.</span></center>
                </div>
                <?php if(count($resumelist) > 0) { ?>
                    <div class="col-lg-12">
        				<?php
        				$i = 1;
        				foreach($resumelist as $list){ ?>
                            <div style="margin-bottom: 15px" class="col-md-3 col-lg-3 col-sm-3 pull-left"></div>
        					<div style="margin-bottom: 15px" class="col-md-3 col-lg-3 col-sm-3 pull-left"><a target="_blank" href="<?php echo $list->url ?>"><?php echo $list->resumeName ?></a></div>
        					<!--<div class="col-md-2 col-lg-2 col-sm-2 pull-left"><?php if($list->active == 1){ echo "Active"; }else{ echo "Inactive"; } ?></div>-->
        					<div style="margin-bottom: 15px;color: black;" class="col-md-3 col-lg-3 col-sm-3 pull-left"><?php echo $list->createdAt ?></div>
        					<div style="margin-bottom: 15px" class="col-md-3 col-lg-3 col-sm-3 pull-left">
        					<input type="radio" onclick="sendResumeThis(<?php echo $list->resumeId ?>)" name="resumeFileSelected">

        					
        					</div>
							<div class="clearfix"></div>
        				<?php 
        				$i++;
        				}
        				?>
    				</div>

                <?php } ?>
                <form method="post" action="" name="frmUsers" id="uploadResume">
    <div id="page-content-wrapper">
        <div class="container-fluid whitebg">
            <div class="row">
			
                <div class="col-md-12 padd_tp_bt_20">
				<hr style="    border-top: 1px solid black;">
                    <div class="form-horizontal form_save">
					
					<div class="clearfix"></div>
					<div class="form-group">
						<label for="inputEmail" class="control-label col-xs-3 required-field ">Upload Resume</label>
						<div class="col-xs-9">
							<input type="file" class="form-control" id="resumeFile" placeholder="Old Password" name="resumeFile" value="">
							<input type="hidden" class="form-control" placeholder="Old Password" id="resumeName" name="resumeName" value="test">
						</div>
					</div>
                        <div class="clearfix">
                            <div class="clearfix">
                                <div class="clearfix">
                                    <div class="clearfix">
                                        <div class="clearfix">
                                            <input type="hidden" name="token" id="token" value="ecbcd7eaee29848978134beeecdfbc7c">
                                            <input type="hidden" name="methodname" id="methodname" value="editResume_v2">
                                            <input type="hidden" name="userId" id="userId" value="<?php echo $_SESSION['userId'] ?>">
                                            
                                            <div class="col-xs-offset-3 col-xs-2 no_padding">
                                                <button class="btn btn-lg btn-primary btn-block" id="changepasswordbutton" name="Submit" value="Save" type="button" onclick="Upload()" style="display:none;">UPLOAD</button>
											</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /#page-content-wrapper -->
                        </div>
					</div>
                </div>
			</div>
		</div>
	</div>
</form>
<div class="clearfix"></div>
<div class="col-md-10 col-md-offset-2" style="padding-left: 3px !important; margin-top: 23px;">
	<?php
		$companyJobCode = $_SESSION['jobCode'];
        $CI =& get_instance();

        $jobCreatedBy = $CI->db->query("SELECT job.createdBy, user.role FROM fj_jobs job JOIN fj_users user ON user.id = job.createdBy WHERE job.fjCode='$companyJobCode'")->row_array();
        
        $createdById = $jobCreatedBy['createdBy'];

        if($jobCreatedBy['role'] == 2) {
   
            $companyQueryConfig = $CI->db->query("SELECT paramValue FROM fj_companyConfig WHERE mainUserId='$createdById' AND companyConfigParamId = 3 AND paramValue = 1")->row_array();
        } else {

        	$companyQuery = $CI->db->query("SELECT id, role, createdBy FROM fj_users WHERE id='$createdById'")->row_array();
        	$actualCompanyId = $companyQuery['createdBy'];

            $companyQueryConfig = $CI->db->query("SELECT paramValue FROM fj_companyConfig WHERE mainUserId='$actualCompanyId' AND companyConfigParamId = 3 AND paramValue = 1")->row_array();
        }
	?>

	<?php if(count($companyQueryConfig) > 0) { ?>
		<span>Note: Please note that this is an optional step. You can skip by clicking on Skip Button.</span>
		<button type="button" id="resumeUploadSkip">Skip</button>
	<?php } ?>
</div>
<!-- /.row -->
</div>

<div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #f8f8f8;height: 30px;">				
</div>

</div>
<!-- /#page-wrapper -->
</div>
</div>
<!-- /#wrapper -->



<script src="<?php echo base_url(); ?>theme/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url(); ?>theme/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>theme/js/bootbox.min.js"></script>
<script>

if ($('#CompanyNameContainer')[0].scrollWidth >  $('#CompanyNameContainer').innerWidth()) {
    //Text has over-flowed
	var text = $('#CompanyNameContainer').text();
	$('#CompanyNameContainer').html("<marquee>"+text+"</marquee>");
}
</script>
    <!-- Metis Menu Plugin JavaScript -->

    <!-- Custom Theme JavaScript -->
<script src="<?php echo base_url(); ?>theme/js/formValidation.min.js"></script>
<script src="<?php echo base_url(); ?>theme/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>js/uploadResume.js"></script>

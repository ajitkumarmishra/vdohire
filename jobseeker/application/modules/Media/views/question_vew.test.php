
<!--
> Muaz Khan     - www.MuazKhan.com
> MIT License   - www.WebRTC-Experiment.com/licence
> Documentation - github.com/muaz-khan/RecordRTC
> and           - RecordRTC.org
-->
<!DOCTYPE html>
<html lang="en">

<head>
    <title>RecordRTC Audio+Video Recording & Uploading to PHP Server</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <link rel="author" type="text/html" href="https://plus.google.com/+MuazKhan">
    <meta name="author" content="Muaz Khan">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <link rel="stylesheet" href="https://cdn.webrtc-experiment.com/style.css">

    <style>
    audio {
        vertical-align: bottom;
        width: 10em;
    }
    video {
        max-width: 100%;
        vertical-align: top;
    }
    input {
        border: 1px solid #d9d9d9;
        border-radius: 1px;
        font-size: 2em;
        margin: .2em;
        width: 30%;
    }
    p,
    .inner {
        padding: 1em;
    }
    li {
        border-bottom: 1px solid rgb(189, 189, 189);
        border-left: 1px solid rgb(189, 189, 189);
        padding: .5em;
    }
    label {
        display: inline-block;
        width: 8em;
    }
    </style>

    <style>
        .recordrtc button {
            font-size: inherit;
        }

        .recordrtc button, .recordrtc select {
            vertical-align: middle;
            line-height: 1;
            padding: 2px 5px;
            height: auto;
            font-size: inherit;
            margin: 0;
        }

        .recordrtc, .recordrtc .header {
            display: block;
            text-align: center;
            padding-top: 0;
        }

        .recordrtc video {
            width: 70%;
        }

        .recordrtc option[disabled] {
            display: none;
        }
    </style>

    <script src="https://cdn.webrtc-experiment.com/RecordRTC.js"></script>
    <script src="https://cdn.webrtc-experiment.com/gif-recorder.js"></script>
    <script src="https://cdn.webrtc-experiment.com/getScreenId.js"></script>

    <!-- for Edige/FF/Chrome/Opera/etc. getUserMedia support -->
    <script src="https://cdn.webrtc-experiment.com/gumadapter.js"></script>
</head>

<body>
    

</body>

</html>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#"><img src="<?php echo base_url(); ?>images/logo.png" rel="stylesheet"></a>
    </div>
      <div class="collapse navbar-collapse pull-right" id="myNavbar" style="margin-right:112px;">
      
    </div>
  </div>
</nav>
    

    <div class="row" style="    background: #f8f8f8;margin-right: 0px;">
        <div id="tabs" style="background: #054F72;padding: 0px 169px;">
            <center style="color:white;    font-size: 25px;text-align: center;padding: 80px;">
                <span class="companyName"></span> has selected you for a VIDEO INTERVIEW for <span class="jobTitle"></span>
            </center>
        <!-- /.col-lg-12 -->
        </div>
        <div class="container-full" style="background: white;">
            <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #054F72;height: 33px;">
                
            </div>
            <div  class="col-md-8 col-lg-8 col-sm-8 pull-left" style="width: 80%;background: #EB9B2C ;">
                <div  class="col-md-2 col-lg-2 col-sm-2 pull-left">
                    <center><img src="<?php echo base_url()?>images/icons/interview.png" style="width:82px"></center>
                    <center>Video Interview</center>
                </div>
                <div  class="col-md-1 col-lg-1 col-sm-1 pull-left">
                    <center><img src="<?php echo base_url()?>images/icons/arrow.png" style="width:82px"></center>
                </div>
                <div  class="col-md-2 col-lg-2 col-sm-2 pull-left">
                    <center><img src="<?php echo base_url()?>images/icons/Multiple.png" style="width:82px"></center>
                    <center>Multiple choice questions</center>
                </div>
                <div  class="col-md-1 col-lg-1 col-sm-1 pull-left">
                    <center><img src="<?php echo base_url()?>images/icons/arrow.png" style="width:82px"></center>
                    
                </div>
                <div  class="col-md-2 col-lg-2 col-sm-2 pull-left">
                    <center><img src="<?php echo base_url()?>images/icons/cloud.png" style="width:82px"></center>
                    <center>Submit Interview</center>
                    
                </div>
            </div>
            <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #054F72;height: 33px;">
                
            </div>
            
        </div>
        <div style="clear:both"></div>
        <div class="container-full" style="background: white;">
            <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #f8f8f8;height: 30px;">

                </div>
            <div  class="col-md-8 col-lg-8 col-sm-8 pull-left" style="border:1px solid lightgrey;padding-bottom: 23px;margin-bottom: 20px;padding-top: 20px;width: 80%;background: white ;">
                <div class="col-lg-12">
                    <!-- /.row -->
                    <center>
						<p style="font-size: 15px;font-weight: bold;" id="QuestionAsPerSynergy">
					Ensure your audio and webcam are working! Don't worry, you are not being recorder yet
						</p>
					</center>
                    
                    
                <!-- /.col-lg-4 -->
                <center>
        

        <section class="experiment recordrtc" style="    border: none;display:none">
            


            <br>
			<video controls muted></video>
			<center>
			<select class="recording-media" style="display:none;">
                    <option value="record-video">Video</option>
                    <option value="record-audio">Audio</option>
                    <option value="record-screen">Screen</option>
            </select>
			<select class="media-container-format" style="display:none;">
				<option>WebM</option>
				<option disabled>Mp4</option>
				<option disabled>WAV</option>
				<option disabled>Ogg</option>
				<option>Gif</option>
			</select>
                    <button type="button" class="btn btn-primary" style="    padding: 16px;font-size: 18px;background: #E12F2F;border: none;   margin-left: 10px;margin-top: 10px;margin-right: 50px;">Start Recording</button>
            </center>
			<div style="text-align: center; display: block;">
                <button id="save-to-disk">Save To Disk</button>
                <button id="open-new-tab">Open New Tab</button>
                <button id="upload-to-server">Upload To Server</button>
            </div>
			 <center>
                    <button onclick="uploadVideo()" type="button" class="btn btn-primary" style="    padding: 16px;font-size: 18px;background: #07B1ED;border: none;   margin-left: 10px;margin-top: 10px;margin-right: 50px;">I AM DONE</button>
            </center>
			</section>
                    <!-- /.row -->
                </center>
			<section class="answerButton" style="    border: none;display:block">
            <center>
                    <button onclick="answerNow()" type="button" class="btn btn-primary" style="    padding: 16px;font-size: 18px;background: #07B1ED;border: none;   margin-left: 10px;margin-top: 10px;margin-right: 50px;">ANSWER NOW</button>
            </center>
			</section>
                <!-- /.col-lg-8 -->
                </div>   
            <!-- /.row -->
                
            <!-- /.row -->

                </div>

            <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #f8f8f8;height: 30px;">

                </div>

        </div>
        
        <?php
        
        ?>
        
    <!-- /#page-wrapper -->
    </div>
</div>
<!-- /#wrapper -->

<div class="recordrtc"></div>

<script src="<?php echo base_url(); ?>theme/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url(); ?>theme/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>theme/js/bootbox.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->

    <!-- Custom Theme JavaScript -->
    <script src="http://formvalidation.io/vendor/formvalidation/js/formValidation.min.js"></script>
<script src="http://formvalidation.io/vendor/formvalidation/js/framework/bootstrap.min.js"></script>
<script type="text/javascript">
window.onload = function(){
    //setTimeout(showQuestionNext, 10);
}
</script>
<script>
var nextQuestion = 0;
var QuestionArray = [];
var TotalQuestion = 0;
var counter = 0;

$(document).ready(function(){
    $.fn.serializeObject = function()
    {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
    

});



$(document).ready(function() {
	
	var methodname = 'verifyJob';
	var tokenUser = '<?php echo $token ?>';
	var verifyJobString = '1111';
	var fjCode = readCookie('fjCode');
	
  
	var js_obj = {token: 'ecbcd7eaee29848978134beeecdfbc7c',methodname: methodname,referrenceJobId:'SYNE828077',deviceUniqueId:'1111111',searchBy:'job'};

	var encoded = JSON.stringify( js_obj );

	var data= encoded;
	$.ajax({
		  url : "<?php echo base_url(); ?>Fjapi",
		  data: data,
		  type: "POST",
		  success: function(response){
				item = $.parseJSON(response);
				if(item.code == "200"){
					var obj = JSON.parse(response);
					var JobTitle = JSON.stringify(obj.data[0].title);
					var company = JSON.stringify(obj.data[0].companyName);
					var fjCode = JSON.stringify(obj.data[0].jobCode);
					fjCode = fjCode.replace(/\"/g, "");
					$( ".jobTitle" ).each(function() {
						$( this ).text(JobTitle.replace(/\"/g, ""));
					});
					
					$( ".companyName" ).each(function() {
						$( this ).text(company.replace(/\"/g, ""));
					});
					document.cookie = "fjCode" + "=" + fjCode;
					//window.location="<?php echo base_url()?>users/home";
				}else{
					//alert(item.message);
				}
		  }
	});
					
					
					
				
});


$(document).ready(function() {
	
	var methodname = 'interviewQuestionList';
	var tokenUser = '<?php echo $token ?>';
	var fjCode = readCookie('fjCode');
	
  
	var js_obj = {token: 'ecbcd7eaee29848978134beeecdfbc7c',methodname: methodname,userId:tokenUser, jobCode:'SYNE067936'};

	var encoded = JSON.stringify( js_obj );

	var data= encoded;
	$.ajax({
		  url : "<?php echo base_url(); ?>Fjapi",
		  data: data,
		  type: "POST",
		  success: function(response){
				item = $.parseJSON(response);
				if(item.code == "200"){
					
					var setId = item.data[0].setID;
					
					document.cookie = "interviewSet" + "=" + setId;
					for(var i = 0; i < item.data.length; i++)
					{	QuestionArray.push(item.data[i].questionID);
						if(i == 0){
							nextQuestion = item.data[i].questionID;
							showQuestionNext();
						}
					   //console.log("Type: " + item.data[i].setID + " Name: " + item.data[i].setID + " Account: " + item.data[i].setID);
							/**/
					}
					TotalQuestion = QuestionArray.length;
					
				}else{
					//alert(item.message);
				}
		  }
	});
	
});

function answerNow(){
	$(".recordrtc").show();
	$(".answerButton").hide();
	alert(readCookie('questionId'));
	alert(readCookie('interviewSet'));
}

function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function saveForJob(){
	var methodname = 'saveJob';
	var tokenUser = '<?php echo $token ?>';
	var verifyJobString = '1111';
	var fjCode = readCookie('fjCode'); 
	
	var js_obj = {token: 'ecbcd7eaee29848978134beeecdfbc7c',methodname: methodname,jobCode:fjCode.replace(/\"/g, ""),userId:tokenUser};

	var encoded = JSON.stringify( js_obj );

	var data= encoded;
	$.ajax({
		  url : "<?php echo base_url(); ?>Fjapi",
		  data: data,
		  type: "POST",
		  success: function(response){
				item = $.parseJSON(response);
				if(item.code == "200"){
					alert(item.message);
					//window.location="<?php echo base_url()?>users/home";
				}else{
					//alert(item.message);
				}
		  }
	});
	
}

function showQuestionNext(){
	var methodname = 'interviewQuestionDetail';
	var tokenUser = '<?php echo $token ?>';
	var fjCode = readCookie('fjCode');
	var interviewSet = readCookie('interviewSet');
	var questionId = QuestionArray[counter];
	
	var js_obj = {token: 'ecbcd7eaee29848978134beeecdfbc7c',methodname: methodname,userId:tokenUser, jobCode:'SYNE067936',interviewSet:interviewSet, questionId:questionId};

	var encoded = JSON.stringify( js_obj );

	var data= encoded;
	$.ajax({
		  url : "<?php echo base_url(); ?>Fjapi",
		  data: data,
		  type: "POST",
		  success: function(response){
				item = $.parseJSON(response);
				if(item.code == "200"){
					var obj = JSON.parse(response);
					var questionId = JSON.stringify(obj.data[0].questionId);
					questionId = questionId.replace(/\"/g, "");
					
					var title = JSON.stringify(obj.data[0].title);
					title = title.replace(/\"/g, "");
					var duration = JSON.stringify(obj.data[0].duration);
					$("#QuestionAsPerSynergy").text(title);
					
					document.cookie = "questionId" + "=" + questionId;
					
				}else{
					//alert(item.message);
				}
		  }
	});
}
</script>

<!-- video rtc-->

<script>
            (function() {
                var params = {},
                    r = /([^&=]+)=?([^&]*)/g;

                function d(s) {
                    return decodeURIComponent(s.replace(/\+/g, ' '));
                }

                var match, search = window.location.search;
                while (match = r.exec(search.substring(1))) {
                    params[d(match[1])] = d(match[2]);

                    if(d(match[2]) === 'true' || d(match[2]) === 'false') {
                        params[d(match[1])] = d(match[2]) === 'true' ? true : false;
                    }
                }

                window.params = params;
            })();
        </script>

        <script>
            var recordingDIV = document.querySelector('.recordrtc');
            var recordingMedia = recordingDIV.querySelector('.recording-media');
            var recordingPlayer = recordingDIV.querySelector('video');
            var mediaContainerFormat = recordingDIV.querySelector('.media-container-format');
			
			var EncodedContent = "";
			var fileName = "";
			var chunkSize = 70024;

			
			
            recordingDIV.querySelector('button').onclick = function() {
                var button = this;

                if(button.innerHTML === 'Stop Recording') {
                    button.disabled = true;
                    button.disableStateWaiting = true;
                    setTimeout(function() {
                        button.disabled = false;
                        button.disableStateWaiting = false;
                    }, 2 * 1000);

                    button.innerHTML = 'Star Recording';

                    function stopStream() {
                        if(button.stream && button.stream.stop) {
                            button.stream.stop();
                            button.stream = null;
                        }
                    }

                    if(button.recordRTC) {
                        if(button.recordRTC.length) {
                            button.recordRTC[0].stopRecording(function(url) {
                                if(!button.recordRTC[1]) {
                                    button.recordingEndedCallback(url);
                                    stopStream();

                                    saveToDiskOrOpenNewTab(button.recordRTC[0]);
                                    return;
                                }

                                button.recordRTC[1].stopRecording(function(url) {
                                    button.recordingEndedCallback(url);
                                    stopStream();
                                });
                            });
                        }
                        else {
                            button.recordRTC.stopRecording(function(url) {
                                button.recordingEndedCallback(url);
                                stopStream();

                                saveToDiskOrOpenNewTab(button.recordRTC);
                            });
                        }
                    }

                    return;
                }

                button.disabled = true;

                var commonConfig = {
                    onMediaCaptured: function(stream) {
                        button.stream = stream;
                        if(button.mediaCapturedCallback) {
                            button.mediaCapturedCallback();
                        }

                        button.innerHTML = 'Stop Recording';
                        button.disabled = false;
                    },
                    onMediaStopped: function() {
                        button.innerHTML = 'Start Recording';

                        if(!button.disableStateWaiting) {
                            button.disabled = false;
                        }
                    },
                    onMediaCapturingFailed: function(error) {
                        if(error.name === 'PermissionDeniedError' && !!navigator.mozGetUserMedia) {
                            InstallTrigger.install({
                                'Foo': {
                                    // https://addons.mozilla.org/firefox/downloads/latest/655146/addon-655146-latest.xpi?src=dp-btn-primary
                                    URL: 'https://addons.mozilla.org/en-US/firefox/addon/enable-screen-capturing/',
                                    toString: function () {
                                        return this.URL;
                                    }
                                }
                            });
                        }

                        commonConfig.onMediaStopped();
                    }
                };

                if(recordingMedia.value === 'record-video') {
                    captureVideo(commonConfig);

                    button.mediaCapturedCallback = function() {
                        button.recordRTC = RecordRTC(button.stream, {
                            type: mediaContainerFormat.value === 'Gif' ? 'gif' : 'video',
                            disableLogs: params.disableLogs || false,
                            canvas: {
                                width: params.canvas_width || 320,
                                height: params.canvas_height || 240
                            },
                            frameInterval: typeof params.frameInterval !== 'undefined' ? parseInt(params.frameInterval) : 20 // minimum time between pushing frames to Whammy (in milliseconds)
                        });

                        button.recordingEndedCallback = function(url) {
                            recordingPlayer.src = null;
                            recordingPlayer.srcObject = null;

                            if(mediaContainerFormat.value === 'Gif') {
                                recordingPlayer.pause();
                                recordingPlayer.poster = url;

                                recordingPlayer.onended = function() {
                                    recordingPlayer.pause();
                                    recordingPlayer.poster = URL.createObjectURL(button.recordRTC.blob);
                                };
                                return;
                            }

                            recordingPlayer.src = url;
                            recordingPlayer.play();

                            recordingPlayer.onended = function() {
                                recordingPlayer.pause();
                                recordingPlayer.src = URL.createObjectURL(button.recordRTC.blob);
                            };
                        };

                        button.recordRTC.startRecording();
                    };
                }

                if(recordingMedia.value === 'record-audio') {
                    captureAudio(commonConfig);

                    button.mediaCapturedCallback = function() {
                        button.recordRTC = RecordRTC(button.stream, {
                            type: 'audio',
                            bufferSize: typeof params.bufferSize == 'undefined' ? 0 : parseInt(params.bufferSize),
                            sampleRate: typeof params.sampleRate == 'undefined' ? 44100 : parseInt(params.sampleRate),
                            leftChannel: params.leftChannel || false,
                            disableLogs: params.disableLogs || false,
                            recorderType: webrtcDetectedBrowser === 'edge' ? StereoAudioRecorder : null
                        });

                        button.recordingEndedCallback = function(url) {
                            var audio = new Audio();
                            audio.src = url;
                            audio.controls = true;
                            recordingPlayer.parentNode.appendChild(document.createElement('hr'));
                            recordingPlayer.parentNode.appendChild(audio);

                            if(audio.paused) audio.play();

                            audio.onended = function() {
                                audio.pause();
                                audio.src = URL.createObjectURL(button.recordRTC.blob);
                            };
                        };

                        button.recordRTC.startRecording();
                    };
                }

                if(recordingMedia.value === 'record-audio-plus-video') {
                    captureAudioPlusVideo(commonConfig);

                    button.mediaCapturedCallback = function() {

                        if(webrtcDetectedBrowser !== 'firefox') { // opera or chrome etc.
                            button.recordRTC = [];

                            if(!params.bufferSize) {
                                // it fixes audio issues whilst recording 720p
                                params.bufferSize = 16384;
                            }

                            var audioRecorder = RecordRTC(button.stream, {
                                type: 'audio',
                                bufferSize: typeof params.bufferSize == 'undefined' ? 0 : parseInt(params.bufferSize),
                                sampleRate: typeof params.sampleRate == 'undefined' ? 44100 : parseInt(params.sampleRate),
                                leftChannel: params.leftChannel || false,
                                disableLogs: params.disableLogs || false,
                                recorderType: webrtcDetectedBrowser === 'edge' ? StereoAudioRecorder : null
                            });

                            var videoRecorder = RecordRTC(button.stream, {
                                type: 'video',
                                disableLogs: params.disableLogs || false,
                                canvas: {
                                    width: params.canvas_width || 320,
                                    height: params.canvas_height || 240
                                },
                                frameInterval: typeof params.frameInterval !== 'undefined' ? parseInt(params.frameInterval) : 20 // minimum time between pushing frames to Whammy (in milliseconds)
                            });

                            // to sync audio/video playbacks in browser!
                            videoRecorder.initRecorder(function() {
                                audioRecorder.initRecorder(function() {
                                    audioRecorder.startRecording();
                                    videoRecorder.startRecording();
                                });
                            });

                            button.recordRTC.push(audioRecorder, videoRecorder);

                            button.recordingEndedCallback = function() {
                                var audio = new Audio();
                                audio.src = audioRecorder.toURL();
                                audio.controls = true;
                                audio.autoplay = true;

                                audio.onloadedmetadata = function() {
                                    recordingPlayer.src = videoRecorder.toURL();
                                    recordingPlayer.play();
                                };

                                recordingPlayer.parentNode.appendChild(document.createElement('hr'));
                                recordingPlayer.parentNode.appendChild(audio);

                                if(audio.paused) audio.play();
                            };
                            return;
                        }

                        button.recordRTC = RecordRTC(button.stream, {
                            type: 'video',
                            disableLogs: params.disableLogs || false,
                            // we can't pass bitrates or framerates here
                            // Firefox MediaRecorder API lakes these features
                        });

                        button.recordingEndedCallback = function(url) {
                            recordingPlayer.srcObject = null;
                            recordingPlayer.muted = false;
                            recordingPlayer.src = url;
                            recordingPlayer.play();

                            recordingPlayer.onended = function() {
                                recordingPlayer.pause();
                                recordingPlayer.src = URL.createObjectURL(button.recordRTC.blob);
                            };
                        };

                        button.recordRTC.startRecording();
                    };
                }

                if(recordingMedia.value === 'record-screen') {
                    captureScreen(commonConfig);

                    button.mediaCapturedCallback = function() {
                        button.recordRTC = RecordRTC(button.stream, {
                            type: mediaContainerFormat.value === 'Gif' ? 'gif' : 'video',
                            disableLogs: params.disableLogs || false,
                            canvas: {
                                width: params.canvas_width || 320,
                                height: params.canvas_height || 240
                            }
                        });

                        button.recordingEndedCallback = function(url) {
                            recordingPlayer.src = null;
                            recordingPlayer.srcObject = null;

                            if(mediaContainerFormat.value === 'Gif') {
                                recordingPlayer.pause();
                                recordingPlayer.poster = url;
                                recordingPlayer.onended = function() {
                                    recordingPlayer.pause();
                                    recordingPlayer.poster = URL.createObjectURL(button.recordRTC.blob);
                                };
                                return;
                            }

                            recordingPlayer.src = url;
                            recordingPlayer.play();
                        };

                        button.recordRTC.startRecording();
                    };
                }

                if(recordingMedia.value === 'record-audio-plus-screen') {
                    captureAudioPlusScreen(commonConfig);

                    button.mediaCapturedCallback = function() {
                        button.recordRTC = RecordRTC(button.stream, {
                            type: 'video',
                            disableLogs: params.disableLogs || false,
                            // we can't pass bitrates or framerates here
                            // Firefox MediaRecorder API lakes these features
                        });

                        button.recordingEndedCallback = function(url) {
                            recordingPlayer.srcObject = null;
                            recordingPlayer.muted = false;
                            recordingPlayer.src = url;
                            recordingPlayer.play();

                            recordingPlayer.onended = function() {
                                recordingPlayer.pause();
                                recordingPlayer.src = URL.createObjectURL(button.recordRTC.blob);
                            };
                        };

                        button.recordRTC.startRecording();
                    };
                }
            };

            function captureVideo(config) {
                captureUserMedia({video: true}, function(videoStream) {
                    recordingPlayer.srcObject = videoStream;
                    recordingPlayer.play();

                    config.onMediaCaptured(videoStream);

                    videoStream.onended = function() {
                        config.onMediaStopped();
                    };
                }, function(error) {
                    config.onMediaCapturingFailed(error);
                });
            }

            function captureAudio(config) {
                captureUserMedia({audio: true}, function(audioStream) {
                    recordingPlayer.srcObject = audioStream;
                    recordingPlayer.play();

                    config.onMediaCaptured(audioStream);

                    audioStream.onended = function() {
                        config.onMediaStopped();
                    };
                }, function(error) {
                    config.onMediaCapturingFailed(error);
                });
            }

            function captureAudioPlusVideo(config) {
                captureUserMedia({video: true, audio: true}, function(audioVideoStream) {
                    recordingPlayer.srcObject = audioVideoStream;
                    recordingPlayer.play();

                    config.onMediaCaptured(audioVideoStream);

                    audioVideoStream.onended = function() {
                        config.onMediaStopped();
                    };
                }, function(error) {
                    config.onMediaCapturingFailed(error);
                });
            }

            function captureScreen(config) {
                getScreenId(function(error, sourceId, screenConstraints) {
                    if (error === 'not-installed') {
                        document.write('<h1><a target="_blank" href="https://chrome.google.com/webstore/detail/screen-capturing/ajhifddimkapgcifgcodmmfdlknahffk">Please install this chrome extension then reload the page.</a></h1>');
                    }

                    if (error === 'permission-denied') {
                        alert('Screen capturing permission is denied.');
                    }

                    if (error === 'installed-disabled') {
                        alert('Please enable chrome screen capturing extension.');
                    }

                    if(error) {
                        config.onMediaCapturingFailed(error);
                        return;
                    }

                    captureUserMedia(screenConstraints, function(screenStream) {
                        recordingPlayer.srcObject = screenStream;
                        recordingPlayer.play();

                        config.onMediaCaptured(screenStream);

                        screenStream.onended = function() {
                            config.onMediaStopped();
                        };
                    }, function(error) {
                        config.onMediaCapturingFailed(error);
                    });
                });
            }

            function captureAudioPlusScreen(config) {
                getScreenId(function(error, sourceId, screenConstraints) {
                    if (error === 'not-installed') {
                        document.write('<h1><a target="_blank" href="https://chrome.google.com/webstore/detail/screen-capturing/ajhifddimkapgcifgcodmmfdlknahffk">Please install this chrome extension then reload the page.</a></h1>');
                    }

                    if (error === 'permission-denied') {
                        alert('Screen capturing permission is denied.');
                    }

                    if (error === 'installed-disabled') {
                        alert('Please enable chrome screen capturing extension.');
                    }

                    if(error) {
                        config.onMediaCapturingFailed(error);
                        return;
                    }

                    screenConstraints.audio = true;

                    captureUserMedia(screenConstraints, function(screenStream) {
                        recordingPlayer.srcObject = screenStream;
                        recordingPlayer.play();

                        config.onMediaCaptured(screenStream);

                        screenStream.onended = function() {
                            config.onMediaStopped();
                        };
                    }, function(error) {
                        config.onMediaCapturingFailed(error);
                    });
                });
            }

            function captureUserMedia(mediaConstraints, successCallback, errorCallback) {
                navigator.mediaDevices.getUserMedia(mediaConstraints).then(successCallback).catch(errorCallback);
            }

            function setMediaContainerFormat(arrayOfOptionsSupported) {
                var options = Array.prototype.slice.call(
                    mediaContainerFormat.querySelectorAll('option')
                );

                var selectedItem;
                options.forEach(function(option) {
                    option.disabled = true;

                    if(arrayOfOptionsSupported.indexOf(option.value) !== -1) {
                        option.disabled = false;

                        if(!selectedItem) {
                            option.selected = true;
                            selectedItem = option;
                        }
                    }
                });
            }

            recordingMedia.onchange = function() {
                if(this.value === 'record-audio') {
                    setMediaContainerFormat(['WAV', 'Ogg']);
                    return;
                }
                setMediaContainerFormat(['WebM', /*'Mp4',*/ 'Gif']);
            };

            if(webrtcDetectedBrowser === 'edge') {
                // webp isn't supported in Microsoft Edge
                // neither MediaRecorder API
                // so lets disable both video/screen recording options

                console.warn('Neither MediaRecorder API nor webp is supported in Microsoft Edge. You cam merely record audio.');

                recordingMedia.innerHTML = '<option value="record-audio">Audio</option>';
                setMediaContainerFormat(['WAV']);
            }

            if(webrtcDetectedBrowser === 'firefox') {
                // Firefox implemented both MediaRecorder API as well as WebAudio API
                // Their MediaRecorder implementation supports both audio/video recording in single container format
                // Remember, we can't currently pass bit-rates or frame-rates values over MediaRecorder API (their implementation lakes these features)

                recordingMedia.innerHTML = '<option value="record-audio-plus-video">Audio+Video</option>'
                                            + '<option value="record-audio-plus-screen">Audio+Screen</option>'
                                            + recordingMedia.innerHTML;
            }

            // disabling this option because currently this demo
            // doesn't supports publishing two blobs.
            // todo: add support of uploading both WAV/WebM to server.
            if(false && webrtcDetectedBrowser === 'chrome') {
                recordingMedia.innerHTML = '<option value="record-audio-plus-video">Audio+Video</option>'
                                            + recordingMedia.innerHTML;
                console.info('This RecordRTC demo merely tries to playback recorded audio/video sync inside the browser. It still generates two separate files (WAV/WebM).');
            }

            function saveToDiskOrOpenNewTab(recordRTC) {
                recordingDIV.querySelector('#save-to-disk').parentNode.style.display = 'block';
                recordingDIV.querySelector('#save-to-disk').onclick = function() {
                    if(!recordRTC) return alert('No recording found.');

                    recordRTC.save();
                };

                recordingDIV.querySelector('#open-new-tab').onclick = function() {
                    if(!recordRTC) return alert('No recording found.');

                    window.open(recordRTC.toURL());
                };

                recordingDIV.querySelector('#upload-to-server').disabled = false;
                recordingDIV.querySelector('#upload-to-server').onclick = function() {
                    if(!recordRTC) return alert('No recording found.');
                    this.disabled = true;

                    var button = this;
                    uploadToServer(recordRTC, function(progress, fileURL) {
                        if(progress === 'ended') {
                            button.disabled = false;
                            button.innerHTML = 'Click to download from server';
                            button.onclick = function() {
                                window.open(fileURL);
                            };
                            return;
                        }
                        button.innerHTML = progress;
                    });
                };
            }

            var listOfFilesUploaded = [];
			 
			function getBase64(file) {
				var reader = new FileReader();
				var dataTest = "";
					reader.readAsDataURL(file);
					reader.onload = function () {
						console.log(reader.result);
						//return dataTest;
					};
					reader.onerror = function (error) {
						console.log('Error: ', error);
					};
			}
            function uploadToServer(recordRTC, callback) {
                var blob = recordRTC instanceof Blob ? recordRTC : recordRTC.blob;
                getBase64(blob);																										
                var reader = new FileReader();

				reader.readAsDataURL(blob);
				reader.onload = function () {
					//console.log(reader.result);
					EncodedContent = reader.result.split(',')[1];
					readContent = 1;
				};
				reader.onloadend = function(){
					$("#myModal").css("display","block");
					//console.log(EncodedContent);
					var len = EncodedContent.length;
					
					var start = 0;
					var end = chunkSize;
					var count = 1;
					var totalCount = Math.ceil(len/chunkSize);
					
					
					sendBytes(EncodedContent.slice(start, end),start,end,len,count,totalCount);
				}
				reader.onerror = function (error) {
					console.log('Error: ', error);
				};
            }
			
			function sendBytes(content,start,end,len,count,totalCount){
    
				if(end >= len){
					lastChunk = 1;
				}else{
					lastChunk = 0;
				}
				var js_obj = {token: 'ecbcd7eaee29848978134beeecdfbc7c',methodname: methodname,userId:tokenUser, resumeName:fileName,fileFailChunk:0,isLastChunk:lastChunk, resumeFile:content };
				var encoded = JSON.stringify( js_obj );
				var dataToSend = encoded;
				
				var req;
				req = new XMLHttpRequest();

				var url = "<?php echo base_url()?>fjapiReg";


				req.open('POST', url, true);
				req.setRequestHeader("Content-Type", "application/json");
				req.send(dataToSend);
				req.onreadystatechange = function () {
					if(req.responseText != ""){
						var response = JSON.parse(req.responseText);

						if (req.readyState == 4) {
							if (response.code == 200) {
								var display = Math.ceil(100/totalCount)*count;
								if(display > 100){
									display = 100;
								}
								$("#progress-circle").attr("class","progress-circle progress-"+display);
								$("#percet_text").text(display);
								count++;
								if(lastChunk == 1){
									$("#myModal").css("display","none");
									return false;
								}
								if (end <= len){
									start = end;
									end = start + chunkSize;
									sendBytes(EncodedContent.slice(start, end),start,end,len,count,totalCount);
								}


							}else if (response.code == 503) {
								//debugger;
								sendBytes(EncodedContent.slice(start, end),start,end,len,count,totalCount);
							}
						}
					}
				}
			}
			
			

            function makeXMLHttpRequest(url, data, callback) {
                var request = new XMLHttpRequest();
                request.onreadystatechange = function() {
                    if (request.readyState == 4 && request.status == 200) {
                        callback('upload-ended');
                    }
                };

                request.upload.onloadstart = function() {
                    callback('Upload started...');
                };

                request.upload.onprogress = function(event) {
                    callback('Upload Progress ' + Math.round(event.loaded / event.total * 100) + "%");
                };

                request.upload.onload = function() {
                    callback('progress-about-to-end');
                };

                request.upload.onload = function() {
                    callback('progress-ended');
                };

                request.upload.onerror = function(error) {
                    callback('Failed to upload to server');
                    console.error('XMLHttpRequest failed', error);
                };

                request.upload.onabort = function(error) {
                    callback('Upload aborted.');
                    console.error('XMLHttpRequest aborted', error);
                };

                request.open('POST', url);
                request.send(data);
            }

            window.onbeforeunload = function() {
                recordingDIV.querySelector('button').disabled = false;
                recordingMedia.disabled = false;
                mediaContainerFormat.disabled = false;

                if(!listOfFilesUploaded.length) return;

                listOfFilesUploaded.forEach(function(fileURL) {
                    var request = new XMLHttpRequest();
                    request.onreadystatechange = function() {
                        if (request.readyState == 4 && request.status == 200) {
                            if(this.responseText === ' problem deleting files.') {
                                alert('Failed to delete ' + fileURL + ' from the server.');
                                return;
                            }

                            listOfFilesUploaded = [];
                            alert('You can leave now. Your files are removed from the server.');
                        }
                    };
                    request.open('POST', 'delete.php');

                    var formData = new FormData();
                    formData.append('delete-file', fileURL.split('/').pop());
                    request.send(formData);
                });

                return 'Please wait few seconds before your recordings are deleted from the server.';
            };
        </script>
		
    <footer>
        <p>
            <a href="https://www.webrtc-experiment.com/">WebRTC Experiments</a> � <a href="https://plus.google.com/+MuazKhan" rel="author" target="_blank">Muaz Khan</a>
            <a href="mailto:muazkh@gmail.com" target="_blank">muazkh@gmail.com</a>
        </p>
    </footer>

    <!-- commits.js is useless for you! -->
    <script>
        window.useThisGithubPath = 'muaz-khan/RecordRTC';
    </script>
    <script src="https://cdn.webrtc-experiment.com/commits.js" async></script>
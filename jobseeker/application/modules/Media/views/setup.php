<style>
    p,.inner {
        padding: 1em;
    }
    li {
        border-bottom: 1px solid rgb(189, 189, 189);
        border-left: 1px solid rgb(189, 189, 189);
        padding: .5em;
    }
    label {
        display: inline-block;
        width: 8em;
    }

    h1 span {
        background: yellow;
        border: 2px solid #8e1515;
        padding: 2px 8px;
        margin: 2px 5px;
        border-radius: 7px;
        color: #8e1515;
        display: inline-block;
    }
    </style>

    <style>
        .recordrtc button {
            font-size: inherit;
        }

        .recordrtc button, .recordrtc select {
            vertical-align: middle;
            line-height: 1;
            padding: 2px 5px;
            height: auto;
            font-size: inherit;
            margin: 0;
        }

        .recordrtc, .recordrtc .header {
            display: block;
            text-align: center;
            padding-top: 0;
        }

        .recordrtc video, .recordrtc img {
            max-width: 100%!important;
            vertical-align: top;
        }

        .recordrtc audio {
            vertical-align: bottom;
        }

        .recordrtc option[disabled] {
            display: none;
        }

        .recordrtc select {
            font-size: 17px;
        }
    </style>

    <script src="<?php echo base_url(); ?>js/min/webRtc/RecordRTC.min.js"></script>
    <script src="<?php echo base_url(); ?>js/min/webRtc/gif-recorder.min.js"></script>

    <!-- for Edge/FF/Chrome/Opera/etc. getUserMedia support -->
    <script src="<?php echo base_url(); ?>js/min/webRtc/gumadapter.min.js"></script>
    <script src="<?php echo base_url(); ?>js/min/webRtc/DetectRTC.min.js"> </script>


		
        <div class="container-full" style="background: white;">
            <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #054F72;height: 19px;">
                
            </div>
            <div  class="col-md-8 col-lg-8 col-sm-8 pull-left" style="width: 80%;background: #EB9B2C ;">
                <h3 style="    color: black;font-weight: bold;font-size: 16px;margin-top:10px;margin-bottom:10px;">Ensure your Audio and Webcam are working</h3>
				
                
            </div>
            <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #054F72;height: 19px;">
                
            </div>
            
        </div>
        <div style="clear:both"></div>
        <div class="container-full" style="background: white;">
            <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #f8f8f8;height: 30px;">

                </div>
            <div  class="col-md-8 col-lg-8 col-sm-8 pull-left" style="border:1px solid lightgrey;padding-bottom: 10px;padding-top: 10px;width: 80%;background: white ;">
                <div class="col-lg-12">
                    <!-- /.row -->
                    
                    
                    
                <!-- /.col-lg-4 -->
                <center>
        

				<section class="experiment recordrtc">
					<div style="margin-top: 10px;">
						<video id="recording-player" style=" width: 496px;height: 279px;border: 1px solid black;" controls muted></video>
					</div>
            <h2 class="header" style="margin-top:10px;" >
                <select class="recording-media" style="display:none">
                    <option value="record-audio-plus-video">Microphone+Camera</option>
                    <option value="record-audio">Microphone</option>
                    <option value="record-screen">Full Screen</option>
                    <option value="record-audio-plus-screen">Microphone+Screen</option>
                </select>

                

                <select class="media-container-format" style="display:none">
                    <option>vp8</option>
                    <option>vp9</option>
                    <option>h264</option>
                    <option>opus</option>
                    <option>pcm</option>
                    <option>gif</option>
                    <option>whammy</option>
                </select>

                <input type="checkbox" id="chk-MultiStreamRecorder" style="margin:0;width:auto;display:none" checked="checked">


                <button id="btn-start-recording" class="btn btn-primary" style="    padding:8px;font-size: 18px;background: #E12F2F;color:white">Start Test Recording</button>
                <div id="btn-pause-recording" style="display: none; font-size: 15px;"></div>

                <select class="media-resolutions" style="display:none">
                    <option value="default">Default resolutions</option>
                    <option value="1920x1080">1080p</option>
                    <option value="1280x720">720p</option>
                    <option value="3840x2160">4K Ultra HD (3840x2160)</option>
                </select>

                <select class="media-framerates" style="display:none">
                    <option value="default">Default framerates</option>
                    <option value="5">5 fps</option>
                    <option value="15">15 fps</option>
                    <option value="24">24 fps</option>
                    <option value="30">30 fps</option>
                    <option value="60">60 fps</option>
                </select>

                <select class="media-bitrates" style="display:none">
                    <option value="default">Default bitrates</option>
                    <option value="8000000000">1 GB bps</option>
                    <option value="800000000">100 MB bps</option>
                    <option value="8000000">1 MB bps</option>
                    <option value="800000">100 KB bps</option>
                    <option value="8000">1 KB bps</option>
                    <option value="800">100 Bytes bps</option>
                </select>
            </h2>

            

            
        </section>
                    <!-- /.row -->
                </center>
                
                <!-- /.col-lg-8 -->
                </div>   
            <!-- /.row -->
			
                <div class="col-lg-12">
				<?php
					if(isset($_SESSION['invitationId']) && $_SESSION['invitationId'] != ""){?>
							<p>Click on <b>"Start Interview"</b> if you are ready to move ahead and take the Interview. Once you click on <b>"Start Interview"</b> you cannot go back while the interview is on. You will need to complete the Interview process in one go.</p>
					<?php }else {
						if($checkSavedJobs != 200){?>
							
							 <p style="    padding: 0px;" >Click on <b>"Start Interview"</b> if you are ready to move ahead and take the Interview, or <b>"Save for Later"</b> if you would like to complete the interview process later. The saved job will be available on your Dashboard. Once you click on <b>"Start Interview"</b> you cannot go back while the interview is on. You will need to complete the Interview process in one go.</p>
						<?php }else{ ?>
							 <p>Click on <b>"Start Interview"</b> if you are ready to move ahead and take the Interview. Once you click on <b>"Start Interview"</b> you cannot go back while the interview is on. You will need to complete the Interview process in one go.</p>
							 
						<?php }
					}
						?>
				
               
             
                </div>
                <div class="col-lg-12">
            <!-- /.row -->
               <div class="col-lg-4">
                    <!-- /.panel -->
                    <center>
									<a  class="btn btn-primary" href="<?php echo base_url()?>media/questionview" style="color:white;text-decoration: none;    padding: 16px;
                                    font-size: 18px;background: #07B1ED;border: none;   margin-left: 10px;margin-top: 10px;margin-right: 50px;">START INTERVIEW</a>
                    </center>

                </div>
                <!-- /.col-lg-8 -->
                <div class="col-lg-4">
                    <!-- /.panel -->
                    
                    <!-- /.panel -->
                </div>
                <div class="col-lg-4">
				<?php
					if(isset($_SESSION['invitationId']) && $_SESSION['invitationId'] != "") {

					}else {
						if($checkSavedJobs != 200){
						?>
                    <!-- /.panel -->
                    <center><button onclick="saveForJob();" type="button" class="btn btn-primary" style="    padding: 16px;
    font-size: 18px;background: #7FCBB1;border: none;   margin-left: 10px;margin-top: 10px;margin-right: 50px;">SAVE FOR LATER</button>
                    </center>
					<?php }else{ ?>
						<center><button onclick="saveForJob();" type="button" class="btn btn-primary" style="    padding: 16px;
    font-size: 18px;background: #7FCBB1;border: none;   margin-left: 10px;margin-top: 10px;margin-right: 50px;" disabled>SAVE FOR LATER</button>
                    </center>
						
					<?php } } ?>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-8 -->
                
                </div>
            <!-- /.row -->

                </div>

            <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #f8f8f8;height: 30px;">

                </div>

        </div>
        
        <?php
        
        ?>
        
    <!-- /#page-wrapper -->
    </div>
</div>
<!-- /#wrapper -->


<script src="<?php echo base_url(); ?>theme/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url(); ?>theme/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>theme/js/bootbox.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->

    <!-- Custom Theme JavaScript -->
<script src="<?php echo base_url(); ?>theme/js/formValidation.min.js"></script>
<script src="<?php echo base_url(); ?>theme/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>js/min/setup.min.js"></script>
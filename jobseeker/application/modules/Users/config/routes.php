<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//
//$route['users/jobhunter'] = "Users/jobhunter";
//$route['users/home'] = "Users/home";
//$route['users/editDetails'] = "Users/editDetails";
//$route['changepassword'] = "Users/changePasswordjobHunter";
//$route['uploadResume'] = "Users/uploadResume";
$route['users/logout'] = "Users/LogOut";
$route['users'] = "Users/index";
$route['users/setSession'] = "Users/setSession";
$route['users/setSessionSignUp'] = "Users/setSessionSignUp";
$route['basicProfile'] = "Users/showBasicProfile";
$route['editbasicProfile'] = "Users/editBasicProfile";
$route['resumeUpload'] = "Users/resume";
$route['editProfile'] = "Users/editProfile";
$route['complete-profile'] = "Users/completeProfile";
$route['sendDucumentByEmail'] = "Users/send_document_email";
$route['setSessionfromApp'] = "Users/setSessionfromApp";
$route['showAllMessages'] = "Users/showAllMessages";
$route['showAlljobInvitations'] = "Users/showAlljobInvitations";
$route['showAllsavedJobs'] = "Users/showAllsavedJobs";
$route['showAllappliedJobs'] = "Users/showAllappliedJobs";
$route['getMessageNext'] = "Users/getMessageNext";
$route['allClosedJobInvitations'] = "Users/allClosedJobInvitations";
$route['notifications'] = "Users/getAllNotifications";
$route['read-notifications'] = "Users/readAllNotifications";
//$route['encodeFile'] = "Users/encodeFile";
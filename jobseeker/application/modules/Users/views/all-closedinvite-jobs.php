<style>
.dataTables_filter{
	    margin-top: 6px;
    margin-right: 6px;
}
</style>
    <!-- Navigation -->
 
    

    <div class="row" style="    background: #f8f8f8;margin-right: 0px;">
        <div id="tabs" style="background: #054F72;    height: 158px;padding: 0 169px;">
                <ul class="nav nav-tabs nav-justified">
                    <li class="active pull-left" style="    width: 25%;cursor: pointer;margin-top: 36px;">
						<a href="<?php echo base_url()?>users" style="text-decoration:none;cursor:pointer;">
                       <table style="background: URL(images/blue1.png) no-repeat;background-size: 90px 90px;width: 90px;height: 90px;">
							<tbody>
								<tr><td cellpadding="0"><div style="text-align: center;"><i class="fa fa-home sub-menu-icon" style="font-size: 25px;color: white;"></i> </div></td>
								</tr>
								<tr><td cellpadding="0"><div style="text-align: center;margin-top: -15px;"><span class="texto_grande" style="color: white;font-size: 16px;">Home</span></div></td>
								</tr>
							</tbody>
						</table>
						</a>
                    </li>
					<li class="active pull-left" style="    width: 25%;cursor: pointer;margin-top: 36px;">
						<a href="<?php echo base_url()?>editProfile" style="text-decoration:none;cursor:pointer;">
                       <table style="background-size: 90px 90px;    width: 85px;;height: 90px;">
							<tbody>
								<tr><td cellpadding="0"><div style="text-align: center;"><img src="<?php echo base_url();?>images/icons/profile.png" style="width:25px;"></div></td>
								</tr>
								<tr><td cellpadding="0"><div style="text-align: center;margin-top: -15px;"><span class="texto_grande" style="color: white;font-size: 16px;">Edit Profile</span></div></td>
								</tr>
							</tbody>
						</table>
						</a>
                    </li>
					
					<li class="active pull-left" style="    width: 25%;cursor: pointer;margin-top: 36px;">
						<a href="javascript://" style="text-decoration:none;cursor:pointer;"   class="changePassword active" >
                       <table style="background-size: 90px 90px;width: 90px;height: 90px;">
							<tbody>
								<tr><td cellpadding="0"><div style="text-align: center;"><i class="fa fa-key" aria-hidden="true" style="font-size: 25px;color: white;"></i></div></td>
								</tr>
								<tr><td cellpadding="0"><div style="text-align: center;margin-top: -15px;"><span class="texto_grande" style="color: white;font-size: 16px;">Change Password</span></div></td>
								</tr>
							</tbody>
						</table>
						</a>
                    </li>
					
					<li class="active pull-left" style="    width: 25%;cursor: pointer;margin-top: 36px;">
						<a href="<?php echo base_url()?>users/logout" style="text-decoration:none;cursor:pointer;"   class="changePassword active" >
                       <table style="background-size: 90px 90px;width: 90px;height: 90px;">
							<tbody>
								<tr><td cellpadding="0"><div style="text-align: center;"><i class="fa fa-power-off" aria-hidden="true" style="font-size: 25px;color: white;"></i></div></td>
								</tr>
								<tr><td cellpadding="0"><div style="text-align: center;margin-top: -15px;"><span class="texto_grande" style="color: white;font-size: 16px;">Logout</span></div></td>
								</tr>
							</tbody>
						</table>
						</a>
                    </li>
                   
                    
					<!--<li class="pull-left"  style="width: 25%;    margin-top: 36px;">
                        <a style="    cursor: pointer;text-decoration: none;background-size: 98px 94px;height: 100%;margin-top: 11px;width: 100px;margin-left: 0px;"  class="changePassword active" >
                            <i class="fa fa-key" aria-hidden="true" style="font-size: 40px;"></i><br>
                            <span class="texto_grande">Change Password</span>
                        </a>
                    </li>-->
					
                    
                </ul>
        <!-- /.col-lg-12 -->
        </div>
        <div class="container-full" style="background: white;">
            <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #054F72;height: 33px;">
                
            </div>
            <div  class="col-md-8 col-lg-8 col-sm-8 pull-left" style="width: 80%;background: #EB9B2C ;height: 33px;">
                <h5 class="pull-left" style="font-weight: bold;font-size: 18px;">Closed Job Invitations <?php echo "(".count($jobInvitations).")"; ?></h5>
                <h5 class="pull-right"  style="font-weight: bold;font-size: 18px;"><a href="<?php echo base_url()."showAlljobInvitations"?>">Open Job Invitations</a></h5>
            </div>
            <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #054F72;height: 33px;">
                
            </div>
            
        </div>

        <div class="container-full" style="background: white;">
            <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #f8f8f8;height: 30px;">
            </div>

            <div  class="col-md-8 col-lg-8 col-sm-8 pull-left" style="    margin-bottom: 20px;padding-top: 20px;width: 80%;background: white ;">
                
                <div class="col-lg-12">
                <!-- /.row -->

                    <!-- /.panel -->
                    <div class="panel panel-default">
                        
                        <!-- /.panel-heading -->
                        <div class="panel-body">

                                <table width="100%" class="table table-striped table-hover" id="dataTables-example">
                                    <thead style="background:#69686D;color:white;">
                                        <th style="width:40%;border-bottom-width: 0px;">Company</th>
                                        <th style="width:40%;border-bottom-width: 0px;">Job Title</th>
                                        <th style="width:20%;border-bottom-width: 0px;">Date Invited</th>
                                    </thead>
                                    <tbody>
                                        <?php foreach($jobInvitations as $row) { ?>
										<?php if($j%2  == 0){
													$backGroundColor = "#E5E5E5";
												}else{
													$backGroundColor = "#fff";
													
												}
												?>
                                        <tr style="background-color:<?php echo $backGroundColor; ?>">
											<td><?php echo $row->company; ?></td>
                                            <td><a href="<?php echo base_url()."job/invitation/".$row->invitationId;?>"><?php echo $row->jobName; ?></a></td>
                                            <td><?php echo date('d M, Y', strtotime($row->invitationDate)); ?></td>
                                            
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>

                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.col-lg-8 -->
                </div>
                <!-- /.row -->
            </div>

            <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #f8f8f8;height: 30px;">
            </div>
        </div>
    <!-- /#page-wrapper -->
    </div>
</div>

<script src="<?php echo base_url(); ?>theme/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url(); ?>theme/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>theme/js/bootbox.min.js"></script>

    <!-- Metis Menu Plugin JavaScript 
    <script src="<?php echo base_url(); ?>theme/vendor/metisMenu/metisMenu.min.js"></script>-->

    <!-- DataTables JavaScript -->
    <script src="<?php echo base_url(); ?>theme/vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>theme/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>theme/vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Morris Charts JavaScript ->
    <!--<script src="<?php echo base_url(); ?>theme/vendor/raphael/raphael.min.js"></script>
    <script src="<?php echo base_url(); ?>theme/vendor/morrisjs/morris.min.js"></script>
    <script src="<?php echo base_url(); ?>theme/data/morris-data.js"></script>

    <!-- Custom Theme JavaScript 
    <script src="<?php echo base_url(); ?>theme/dist/js/sb-admin-2.js"></script>
    <script src="<?php echo base_url(); ?>theme/js/formValidation.min.js"></script>-->
    <script src="<?php echo base_url(); ?>theme/js/bootstrap.min.js"></script>

<?php
//Get Notification unread count
$getNotificationUnreadCount = 0;

//print_r($allNotificationList);exit;
foreach ($allNotificationList as $notiCount) {
    if($notiCount->status == 0) {
        $getNotificationUnreadCount++;
    }
}
//End of get Notification unread count
?>
<!-- Navigation -->

        <div  class="col-md-12 col-lg-12 col-sm-12" style="font-size: 17px;color: white;font-weight: bold;background: #EB9B2C ;">
            <div><h2 style="font-size:16px;color:black;font-weight:bold;">Edit Profile</h2></div>
        </div>
       
    <div style="clear:both"></div>
    <div class="container-full" style="background: white;">
      
        <div  class="col-md-12 col-lg-12 col-sm-12" style="padding-bottom: 23px;margin-bottom: 20px;padding-top: 20px;background: white ;">
            <div class="col-lg-12">
                <div class="row">
                    <form class="form-horizontal">
                        <fieldset>
                            
                            <div class="form-group">
                                <div class="col-xs-12">
                                    <div class="col-xs-3">
                                        <label>
                                            <a class="btn btn-primary" onclick="showCOntentURL('<?php echo base_url()."editbasicProfile"; ?>')"  href="javascript://" id="manuallyEditProfile" name="editProfile" value="manuallyEditProfile" type="radio" style="     height: 100px;   padding-top: 38px;width: 186px;text-decoration: none;">Edit Profile Manually</a>
                                        </label>
                                    </div>
                                    <div class="col-xs-3">
                                        <label>
                                            <a class="btn btn-primary"   onclick="showCOntentURL('<?php echo base_url()."videoResume/index"; ?>')" id="uploadResumeDocument" name="editProfile" value="uploadResumeDocument" title="Upload Resume Document (PDF, Doc, Docx)" type="radio" style="     height: 100px;   padding-top: 38px;width: 186px;text-decoration: none;">Upload Video Resume</a>
                                        </label>
                                    </div>
									<div class="col-xs-3">
                                        <label>
                                            <a class="btn btn-primary"   onclick="showCOntentURL('<?php echo base_url()."resumeUpload"; ?>')" id="uploadResumeDocument" name="editProfile" value="uploadResumeDocument" title="Upload Resume Document (PDF, Doc, Docx)" type="radio" style="     height: 100px;   padding-top: 38px;width: 186px;text-decoration: none;">Upload Resume</a>
                                        </label>
                                    </div>
                                    <div class="col-xs-3">
                                        <label>
                                            <a class="btn btn-primary"  onclick="showCOntentURL('<?php echo base_url()."sendDucumentByEmail"; ?>')" href="javascript://"  id="sendResumeDocumentByEmail" name="editProfile" value="sendResumeDocumentByEmail" type="radio" style="    padding-top: 38px;    height: 100px;width: 186px;text-decoration: none;">Send Resume by Email</a>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            
                        </fieldset>
                    </form>
                </div>
                <!-- /.row -->
            </div>
        </div>
    <!-- /#page-wrapper -->
    </div>
</div>
<!-- /#wrapper -->



<script src="<?php echo base_url(); ?>theme/vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url(); ?>theme/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>theme/js/bootbox.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->

<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url(); ?>theme/js/formValidation.min.js"></script>
<script src="<?php echo base_url(); ?>theme/js/bootstrap.min.js"></script>

<!-- Edit profile js JavaScript -->
<script src="<?php echo base_url(); ?>js/min/editprofile.min.js"></script>
<style>
.dataTables_filter{
        margin-top: 6px;
    margin-right: 6px;
}
</style>

    
    

    <div class="row" style="    background: #f8f8f8;margin-right: 0px;">
        
        <div class="container-full" style="background: white;">
            <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #054F72;height: 33px;">
                
            </div>
            <div  class="col-md-8 col-lg-8 col-sm-8 pull-left" style="width: 80%;background: #EB9B2C ;height: 33px;">
                <h5 style="font-weight: bold;font-size: 18px;">Notifications <?php echo "(".count($allNotificationList).")"; ?></h5>
            </div>
            <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #054F72;height: 33px;">
                
            </div>
            
        </div>

        <div class="container-full" style="background: white;">
            <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #f8f8f8;height: 30px;">
            </div>

            <div  class="col-md-8 col-lg-8 col-sm-8 pull-left" style="margin-bottom: 20px;padding-top: 20px;width: 80%;background: white ;">
                
                <div class="col-lg-12">
                <!-- /.row -->

                    <!-- /.panel -->
                    <div class="panel panel-default">
                        
                        <!-- /.panel-heading -->
                        <div class="panel-body">

                                <table width="100%" class="table table-striped table-hover" id="dataTables-example">
                                    <thead style="background:#69686D;color:white;">
                                        <th style="width:10%;border-bottom-width: 0px;"><input type="checkbox" class="chk_bx" id="ckbCheckAll"></th>
                                        <th style="width:20%;border-bottom-width: 0px;">Company</th>
                                        <th style="width:55%;border-bottom-width: 0px;">Message</th>
                                        <th style="width:15%;border-bottom-width: 0px;">Date</th>
                                    </thead>
                                    <tbody>
                                        <?php //print_r($allNotificationList);exit;?>
                                        <?php 
										$j = 0;
										foreach($allNotificationList as $row) { ?>
                                        <?php 
										if($j%2  == 0){
                                            $backGroundColor = "#E5E5E5";
                                        }else{
											$backGroundColor = "#fff";
                                                    
                                        }
										$j++;
                                                ?>
                                        <tr style="background-color:<?php echo $backGroundColor; ?>">
                                            <td><input type="checkbox" class="checkBoxClass" value="<?php echo $row->id; ?>" name="notifyIds[]" id="notifyIds"></td>
                                            <td><?php echo $row->sender; ?></td>
                                            <td><?php echo $row->message; ?></td>
                                            <td><?php echo date('d M, Y', strtotime($row->createdAt)); ?></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>

                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.col-lg-8 -->
                </div>
                <!-- /.row -->

                <?php if( !empty($allNotificationList) ) { ?>
                    <div class="col-md-12">  
                        <div class="ed_job_bt" style="padding-top: 20px;">
                            <input type="hidden" name="hdnnotificationids" id="hdnnotificationids" value="" />
                            <button style="background: rgba(0, 0, 0, 0) none repeat scroll 0 0; border: 2px solid #707075; border-radius: 5px; padding: 6px 25px;" type="button" class="delete-notifications pull-left">Delete</button>
                        </div>
                    </div>
                <?php } ?>

            </div>

            <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #f8f8f8;height: 30px;">
            </div>
        </div>
    <!-- /#page-wrapper -->
    </div>
</div>

<script src="<?php echo base_url(); ?>theme/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url(); ?>theme/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>theme/js/bootbox.min.js"></script>

    <!-- Metis Menu Plugin JavaScript 
    <script src="<?php echo base_url(); ?>theme/vendor/metisMenu/metisMenu.min.js"></script>-->

    <!-- DataTables JavaScript -->
    <script src="<?php echo base_url(); ?>theme/vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>theme/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>theme/vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Morris Charts JavaScript 
    <script src="<?php echo base_url(); ?>theme/vendor/raphael/raphael.min.js"></script>
    <script src="<?php echo base_url(); ?>theme/vendor/morrisjs/morris.min.js"></script>
    <script src="<?php echo base_url(); ?>theme/data/morris-data.js"></script>-->

    <!-- Custom Theme JavaScript 
    <script src="<?php echo base_url(); ?>theme/dist/js/sb-admin-2.js"></script>-->
    <script src="<?php echo base_url(); ?>theme/js/formValidation.min.js"></script>
    <script src="<?php echo base_url(); ?>theme/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>js/user_notifications.js"></script>
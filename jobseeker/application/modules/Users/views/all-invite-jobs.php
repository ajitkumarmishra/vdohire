<style>
.dataTables_filter{
	    margin-top: 6px;
    margin-right: 6px;
}
</style>
    <!-- Navigation -->

        <div class="container-full" style="background: white;">
            <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #054F72;height: 33px;">
                
            </div>
            <div  class="col-md-8 col-lg-8 col-sm-8 pull-left" style="width: 80%;background: #EB9B2C ;height: 33px;">
                <h5 class="pull-left" style="font-weight: bold;font-size: 18px;">Open Job Invitations <?php echo "(".count($jobInvitations).")"; ?></h5>
                <h5 class="pull-right"  style="font-weight: bold;font-size: 18px;"><a href="<?php echo base_url()."allClosedJobInvitations"?>">Closed Job Invitations</a></h5>
            </div>
            <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #054F72;height: 33px;">
                
            </div>
            
        </div>

        <div class="container-full" style="background: white;">
            <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #f8f8f8;height: 30px;">
            </div>

            <div  class="col-md-8 col-lg-8 col-sm-8 pull-left" style="    margin-bottom: 20px;padding-top: 20px;width: 80%;background: white ;">
                
                <div class="col-lg-12">
                <!-- /.row -->

                    <!-- /.panel -->
                    <div class="panel panel-default">
                        
                        <!-- /.panel-heading -->
                        <div class="panel-body">

                                <table width="100%" class="table table-striped table-hover" id="dataTables-example">
                                    <thead style="background:#69686D;color:white;">
                                        <th style="width:40%;border-bottom-width: 0px;">Company</th>
                                        <th style="width:40%;border-bottom-width: 0px;">Job Title</th>
                                        <th style="width:20%;border-bottom-width: 0px;">Date Invited</th>
                                    </thead>
                                    <tbody>
                                        <?php foreach($jobInvitations as $row) { ?>
										<?php if($j%2  == 0){
													$backGroundColor = "#E5E5E5";
												}else{
													$backGroundColor = "#fff";
													
												}
												?>
                                        <tr style="background-color:<?php echo $backGroundColor; ?>">
											<td><?php echo $row->company; ?></td>
                                            <td><a href="<?php echo base_url()."job/invitation/".$row->invitationId;?>"><?php echo $row->jobName; ?></a></td>
                                            <td><?php echo date('d M, Y', strtotime($row->invitationDate)); ?></td>
                                            
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>

                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.col-lg-8 -->
                </div>
                <!-- /.row -->
            </div>

            <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #f8f8f8;height: 30px;">
            </div>
        </div>
    <!-- /#page-wrapper -->
    </div>
</div>

<script src="<?php echo base_url(); ?>theme/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url(); ?>theme/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>theme/js/bootbox.min.js"></script>

    <!-- Metis Menu Plugin JavaScript 
    <script src="<?php echo base_url(); ?>theme/vendor/metisMenu/metisMenu.min.js"></script>-->

    <!-- DataTables JavaScript -->
    <script src="<?php echo base_url(); ?>theme/vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>theme/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>theme/vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Morris Charts JavaScript
    <script src="<?php echo base_url(); ?>theme/vendor/raphael/raphael.min.js"></script>
    <script src="<?php echo base_url(); ?>theme/vendor/morrisjs/morris.min.js"></script>
    <script src="<?php echo base_url(); ?>theme/data/morris-data.js"></script>-->

    <!-- Custom Theme JavaScript 
    <script src="<?php echo base_url(); ?>theme/dist/js/sb-admin-2.js"></script>
    <script src="<?php echo base_url(); ?>theme/js/formValidation.min.js"></script>-->
    <script src="<?php echo base_url(); ?>theme/js/bootstrap.min.js"></script>
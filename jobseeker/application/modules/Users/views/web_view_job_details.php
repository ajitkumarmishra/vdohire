
<style>
video::-internal-media-controls-download-button {
    display:none;
}

video::-webkit-media-controls-enclosure {
    overflow:hidden;
}

video::-webkit-media-controls-panel {
    width: calc(100% + 30px); /* Adjust as needed */
}
#tabs > ul li > a > table:hover{
	background:none;
}
</style>
<!-- Navigation -->
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>                        
            </button>
            <a class="navbar-brand" href="<?php echo base_url() ?>users"><img src="<?php echo base_url(); ?>images/logo.png" rel="stylesheet"></a>
        </div>


    </div>
</nav>




<div class="row" style="    background: #f8f8f8;margin-right: 0px;">
    <div id="tabs" style="background: #054F72;    height: 97px;">
        <ul class="nav nav-tabs nav-justified">
            <li class="active pull-left" style="    width: 18%;margin-top: 6px;">
                <a href="javascript://" style="text-decoration:none;width:100%;">
                    <table style="width:100%;">
                        <tbody>
                            <tr>
                                <td>
                                    <div style="text-align: center;    padding: 13px;padding-right: 0px;padding-left: 0px;"><b>Position</b><br>
									<span style="font-size:15px;"><?php echo $content->title; ?></span>
									</div></td>
                            </tr>

                        </tbody>
                    </table>
                </a>
            </li>
            <li class="active pull-left" style="    width: 18%;margin-top: 6px;">
                <a href="javascript://" style="text-decoration:none;width:100%;">
                    <table style="width:100%;">
                        <tbody>
                            <tr>
                                <td cellpadding="0">
                                    <div style="text-align: center;    padding: 13px;padding-right: 0px;padding-left: 0px;"><b>Company</b><br>
									<span style="font-size:15px"><?php echo $content->companyName; ?></span></div></td>
                            </tr>
                        </tbody>
                    </table>
                </a>
            </li>

            <li class="active pull-left" style="    width: 12%;margin-top: 6px;">
                <a href="javascript://" style="text-decoration:none;width:100%;"   class="active" >
                    <table style="width:100%;">
                        <tbody>
                            <tr><td cellpadding="0">
                                    <div style="text-align: center;    padding: 13px;padding-right: 0px;padding-left: 0px;">
                                        <b>Location</b><br>
										<span style="font-size:15px"><?php
                                        foreach ($content->location as $location) {
                                            echo $location->state;
                                        }
                                        ?>
										</span>
                                    </div></td>
                            </tr>

                        </tbody>
                    </table>
                </a>
            </li>

            <li class="active pull-left" style="    width: 12%;margin-top: 6px;">
                <a href="javascript://" style="text-decoration:none;width:100%;"   class="active" >
                    <table style="width:100%;">
                        <tbody>
                            <tr>
                                <td>
                                    <div style="text-align: center;    padding: 13px;padding-right: 0px;padding-left: 0px;">
                                        <span class="texto_grande" style="color: white;font-size: 16px;"><b>Vacancies</b></span><br>
										<span style="font-size:15px"><?php echo $content->vaccancies; ?></span></div></td>
                            </tr>
                        </tbody>
                    </table>
                </a>
            </li>

            <li class="active pull-left" style="    width: 20%;margin-top: 6px;">
                <a href="javascript://" style="text-decoration:none;width:100%;"   class="active" >
                    <table style="width:100%;">
                        <tbody>
                            <tr>
                                <td cellpadding="0">
                                    <div style="text-align: center;    padding: 13px;padding-right: 0px;padding-left: 0px;">
                                        <span class="texto_grande" style="color: white;font-size: 18px;"><b>Salary&nbsp;(INR)</b><br>
										<span style="font-size:15px"><?php echo $content->minSalary; ?> - <?php echo $content->maxSalary; ?><span></span></div></td>
                            </tr>
                            
                        </tbody>
                    </table>
                </a>
            </li>

            <li class="active pull-left" style="    width: 18%;margin-top: 6px;">
                <a href="javascript://" style="text-decoration:none;width:100%;"   class="active" >
                    <table style="width:100%;">
                        <tbody>
                            <tr>
                                <td cellpadding="0">
                                    <div style="text-align: center;   padding: 13px;padding-right: 0px;padding-left: 0px;"><span class="texto_grande" style="color: white;font-size: 16px;">
                                            <b>Last date to Apply</b><br><span style="font-size:15px"><?php echo date('d M, Y', strtotime($content->openTill)); ?></span>
                                        </span></div></td>
                            </tr>
                        </tbody>
                    </table>
                </a>
            </li>

            <!--<li class="active pull-left" style="    width: 21%;cursor: pointer;margin-top: 6px;">
                    <a href="<?php echo base_url() ?>users/logout" style="text-decoration:none;cursor:pointer;"   class="changePassword active" >
<table style="background-size: 90px 90px;width: 90px;height: 90px;">
                            <tbody>
                                    <tr><td cellpadding="0"><div style="text-align: center;"><i class="fa fa-power-off" aria-hidden="true" style="font-size: 25px;color: white;"></i></div></td>
                                    </tr>
                                    <tr><td cellpadding="0"><div style="text-align: center;margin-top: -15px;"><span class="texto_grande" style="color: white;font-size: 16px;">Logout</span></div></td>
                                    </tr>
                            </tbody>
                    </table>
                    </a>
</li>-->


            <!--<li class="pull-left"  style="width: 25%;    margin-top: 36px;">
<a style="    cursor: pointer;text-decoration: none;background-size: 98px 94px;height: 100%;margin-top: 11px;width: 100px;margin-left: 0px;"  class="changePassword active" >
<i class="fa fa-key" aria-hidden="true" style="font-size: 40px;"></i><br>
<span class="texto_grande">Change Password</span>
</a>
</li>-->


        </ul>
        <!-- /.col-lg-12 -->
    </div>
    <!--<div id="tabs" style="background: #054F72;height: 200px;padding: 0 169px;">
        <center style="color:white;    font-size: 25px;text-align: center;padding: 80px;">
                                    <div class="">
                                            <a style="margin-right: 18px;    color: white;" href="javascript:history.go(-1)"></a><span>Please review the JOB Description and APPLY NOW<span>
                                    </div>
        </center>
    
</div>-->
    <div class="container-full" style="background: white;color:white;">
        <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #054F72;height: 38px;">

        </div>
        <div  class="col-md-8 col-lg-8 col-sm-8 pull-left" style="font-size: 17px;color: white;font-weight: bold;width: 80%;background: #EB9B2C ;">
            <div class="btn-group btn-group-lg" style="padding: 10px 0px 10px 0px;">
                <div  class="pull-left" style="width: auto;margin-right:30px;">
                    <button onclick="showJobDescription()" id="showJobDescription"  type="button" class="btn btn-primary" style="font-weight: 600;padding: 13px;background: #054f72;font-size: 17px;border: none;">JOB DESCRIPTION</button>
                </div>
                <div  class=" pull-left" style="    width: auto;margin-right:30px;">
                    <button onclick="ShowAboutCompany()" id="ShowAboutCompany" type="button" class="btn btn-primary" style="font-weight: 600;padding: 13px;background: #d3d3d3;font-size: 17px;border: none;color:black;">ABOUT COMPANY</button>
                </div>

            </div>
            <div style="clear:both"></div>
        </div>
        <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #054F72;height: 38px;">
        </div>
    </div>
    <div style="clear:both"></div>
    <div class="container-full" style="background: white;">
        <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #f8f8f8;    height: 23px;">
        </div>
        <div  class="col-md-8 col-lg-8 col-sm-8 pull-left" style="border:1px solid lightgrey;padding-bottom: 10px;padding-top: 0px;width: 80%;background: white ;">
            <div>
                <div class="col-lg-12" style="    height: 350px;overflow-y: auto;">
                    <div class="col-lg-6" id="showAboutUs" style="display:none;">
                        <h3 style="margin-top:10px;"><?php echo $content->companyName; ?></h3>
                        <?php echo $content->aboutCompany; ?>
                    </div>
                    <div class="col-lg-6" id="showJobDescription1">
                        <h3 style="margin-top:10px;">Job Description</h3>
<?php echo $content->description; ?>
                        <div style="clear:both"></div>
                        <!--<h3>Introduction Video</h3>
                        <video src="<?php //echo 'https://35.154.53.72/jobseeker/video/test.webm';  ?>" style="    height: 288px;" controls></video>-->
                    </div>
                    <div class="col-lg-6" style="" id="screeningProcess">
                        <h3 style="margin-top:10px;">Screening Process</h3>
                        <p><b>The screening process for this position consists of:</b><br>
                        <ul>
                            <li>An Interview, with "<?php if($content->interviewQuestion == 1){ echo $content->interviewQuestion.'" question'; }else{  echo $content->interviewQuestion.'" questions'; } ?> with a total answering time of "<?php echo $content->interviewDuration ?>" seconds</li>
							<?php
							if($content->assessmentDuration == ""){
							}else{
							?>
                            <li>An Assessment, with "<?php if($content->assessmentQuestion == 1){ echo $content->assessmentQuestion.'" question'; }else{  echo $content->assessmentQuestion.'" questions'; } ?> , with multiple-choice answer options, with a total provided time of "<?php echo $content->assessmentDuration ?>" seconds</li>
							<?php } ?>
                        </ul>
                        </p>
                        <br>
                        <p>
                            Click on <b>"Apply"</b> to initiate the FirstJob Digital Screening experience or <b>"Save for Later"</b> if you would like to complete the screening process later. The saved job will be available on your Dashboard. If you click on "Apply", you will be taken through a set of guidelines to ensure you are ready for the Interview. This should not take more than 5 minutes. The Job Interview will start only when you click on the "Start Interview" button. Till that point, you can choose "Save for Later" at any stage. 
                        </p>
                        <!-- /.col-lg-8 -->
                    </div>

                    <!-- /.col-lg-8 -->
                </div>
            </div>



            <!-- /.row -->
            <div class="col-lg-12" style="">
                <!-- /.row -->
                <div class="col-lg-6">
                    <?php if ($checkAppliedJob == 200) { ?>
                        <center>
						<a class="btn btn-primary"  href="<?php echo base_url() ?>media/question" style="    padding: 16px;
                                    font-size: 18px;background: #11AAF0;border: none;   margin-left: 10px;margin-right: 50px;color:white;    text-decoration: none;    padding-left: 60px;padding-right: 63px;">APPLY</a>
                        </center>
<?php } else { ?>



                        <center><button type="button" class="btn btn-primary" style="    padding: 16px;
                                    font-size: 18px;background: #11AAF0;border: none;   margin-left: 10px;margin-right: 50px;" disabled>ALREADY APPLIED</a></button>
									</center>
<?php } ?>
                </div>
                
                <!-- /.col-lg-8 -->
                <div class="col-lg-6">
                    <!-- /.panel -->
                    <?php if ($checkAppliedJob == 200 && $checkSavedJobs != 200) { ?>
                        <center><button type="button" onclick="saveForJob()" class="btn btn-primary" style="padding: 16px;font-size: 18px;background: #7FCFB4;border: none;   margin-left:10px;margin-right: 50px;">SAVE FOR LATER</button>
                        </center>
<?php } else { ?>
                  <center><button type="button" onclick="saveForJob()" class="btn btn-primary" style="padding: 16px;font-size: 18px;background: #7FCFB4;border: none;   margin-left:10px;margin-right: 50px;" disabled>SAVE FOR LATER</button>
                        </center>    
<?php } ?>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-8 -->

            </div>
            <!-- /.row -->
            <!-- /.row -->
        </div>
    </div>
    <!-- /#page-wrapper -->
</div>
</div>
<!-- /#wrapper -->



<script src="<?php echo base_url(); ?>theme/vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url(); ?>theme/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>theme/js/bootbox.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->

<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url(); ?>theme/js/formValidation.min.js"></script>
<script src="<?php echo base_url(); ?>theme/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>js/min/job_details.min.js"></script>



<?php
//Get Notification unread count
$getNotificationUnreadCount = 0;

//print_r($allNotificationList);exit;
foreach ($allNotificationList as $notiCount) {
    if($notiCount->status == 0) {
        $getNotificationUnreadCount++;
    }
}
//End of get Notification unread count
?>
    <!-- Navigation -->
    <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#"><img src="<?php echo base_url(); ?>images/logo.png" rel="stylesheet"></a>
    </div>
      <div class="collapse navbar-collapse pull-right" id="myNavbar" style="margin-right:112px;">
      <ul class="nav navbar-nav">
            <!--<li style="background:white;border: 1px solid black;    margin-right: 21px;">
                <a href="#" style="color:black;text-decoration: none;padding-left: 7px;">
                    <i class="fa fa-user-plus" style="font-size: 19px;"></i>
                    <span class="dashboard-invite-friends" style="font-weight:600;">Invite Friends</span>
                </a>
            </li>
            <li  style="border: 1px solid black;background:white;    margin-right: 21px;" class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#" style="color:black;text-decoration: none;padding-left: 7px;">
                    <i class="fa fa-video-camera" style="font-size: 19px;"></i>
                    <span class="dashboard-video-profile" style="font-weight:600;">Video Profile</span>
                </a>
            </li>-->
			<?php
					
					if(isset($_SESSION['userImage']) && $_SESSION['userImage'] != ""){
						$image = base_url()."uploads/userImages/".$_SESSION['userImage'];
					}else{
						$image = "https://firstjob.co.in/admin/theme/firstJob/image/default.png";
					}
					
					?>

            <li style="margin-right: 21px;">
                <?php
                    if($getNotificationUnreadCount > 0) {
                        $notiMessage = 'You have '.$getNotificationUnreadCount.' new notifications. Please click here to view notifications.';
                    } else {
                        $notiMessage = '';
                    }
                ?>
                <a title="<?php echo $notiMessage;?>" class="dropdown-toggle" data-toggle="dropdown" href="#" style="color:black;text-decoration: none;padding-left: 7px; background: none !important;">
                    <?php if($getNotificationUnreadCount > 0) { ?>
                        <span class="badge" style="background-color: red !important; position: absolute; margin-left: 32%; top: 0px;"><?php echo $getNotificationUnreadCount;?></span>
                    <?php } ?>
                    <i class="fa fa-bell fa-fw" style="color: white;  font-size: 32px;"></i> <!--<i class="fa fa-caret-down" style="color: white;"></i>-->
                </a>
                <ul class="dropdown-menu dropdown-messages" style="right: 0; left: inherit; overflow-y: scroll; height:340px; width: 400px;">
                    <li><div class="col-md-12 col-lg-12 col-sm-12" style="min-height: 44px; font-size: 20px; background: #69686D; margin-top: -5px;"><div style="padding-top: 7px; color: white;">NOTIFICATIONS</div></div>
                    </li>
                    <?php if(count($allNotificationList) > 0) { foreach($allNotificationList as  $notificationrow) { ?>
                    <li>
                        <a href="#" style="text-decoration: none;">
                            <div>
                                <strong><?php echo $notificationrow->sender ?></strong>
                                <span class="pull-right text-muted">
                                    <em style="font-size: 12px;"><?php echo date('d M Y', strtotime($notificationrow->createdAt)) ?></em>
                                </span>
                            </div>
                            <div><?php echo $notificationrow->message ?></div>
                        </a>
                    </li>
                    <li class="divider"></li>
                    <?php } } else { ?>
                        <li>
                            <a href="#" style="text-decoration: none;">
                                <div>
                                    <span class="pull-right text-muted">
                                        <em>You have no notification!</em>
                                    </span>
                                </div>
                            </a>
                        </li>
                    <?php } ?>

                    <?php if(count($allNotificationList) > 0) { ?>
                        <li>
                            <div class="col-md-12 col-lg-12 col-sm-12">
                                <div class="col-md-7 col-lg-7 col-sm-7" style="border-right: 1px solid #333;">
                                    <a style="color: #69686D !important; text-decoration: none; margin-left: 8px;" text-align="left" href="<?php echo base_url().'read-notifications';?>">
                                        <strong>MARK ALL AS READ</strong>
                                    </a>
                                </div>
                                <div class="col-md-5 col-lg-5 col-sm-5">
                                    <a style="color: #69686D !important; text-decoration: none; margin-left: 29px;" text-align="right" href="<?php echo base_url().'notifications';?>">
                                        <strong>VIEW ALL</strong>
                                    </a>
                                </div>
                            </div>
                        </li>
                    <?php } ?>
                </ul>
                <!-- /.dropdown-messages -->
            </li>
            
            <li  style="border: 1px solid black;background:white;    margin-right: 21px;">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#" style="color:black;text-decoration: none;padding-left: 7px;">
                    <img src="<?php echo $image;?>" style="    width: 19px;"><span  style="font-weight:600;"><?php echo $userName?></span><span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li style="background:white;"><a href="#" style="color:black;text-decoration: none;"><i class="fa fa-user fa-fw"></i>User Profile</a></li>
                    <li style="background:white;"><a href="#" style="color:black;text-decoration: none;" class="changePassword" ><i class="fa fa-key" aria-hidden="true"></i>&nbsp;Change Password</a></li>
                    <li style="background:white;"><a href="<?php echo base_url();?>users/logout" style="color:black;text-decoration: none;"><i class="fa fa-sign-out fa-fw"></i>Logout</a></li>
                </ul>
            </li>
        </ul>
    </div>
  </div>
</nav>
    

    <div class="row" style="    background: #f8f8f8;margin-right: 0px;">
        <div id="tabs" style="background: #054F72;height: 200px;padding: 0 169px;">
                <ul class="nav nav-tabs nav-justified">
                    <li class="active pull-left" style="width: 20%;">
                        <a class="text-center" style="text-decoration: none;background: URL(images/blue1.png) no-repeat;    background-size: 119px 77px;">
                            <i class="fa fa-home sub-menu-icon" style="font-size: 36px;"></i> <br>
                            <span class="texto_grande">Home</span>
                        </a>
                        </li>
                    
                </ul>
        <!-- /.col-lg-12 -->
        </div>
        <div class="container-full" style="background: white;color:white;">
            <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #054F72;height: 33px;">
                
            </div>
            <div  class="col-md-8 col-lg-8 col-sm-8 pull-left" style="font-size: 17px;color: white;font-weight: bold;width: 80%;background: #EB9B2C ;">
                
                <div><h2>Basic Profile</h2></div>
                
            </div>
            <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #054F72;height: 33px;">
                
            </div>
            
        </div>
        <div style="clear:both"></div>
        <div class="container-full" style="background: white;">
            <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #f8f8f8;height: 30px;">

                </div>
            <div  class="col-md-8 col-lg-8 col-sm-8 pull-left" style="border:1px solid lightgrey;padding-bottom: 23px;margin-bottom: 20px;padding-top: 20px;width: 80%;background: white ;">
                <div class="col-lg-12">
                    <div class="row">
		<form method="post" action="" name="createUser" enctype="multipart/form-data" id="createSubuserForm" class="has-validation-callback">
			<div id="page-content-wrapper">
                        <div class="container-fluid whitebg">
            <div class="row">
                <div class="col-md-12"><h1><img src="https://firstjob.co.in/admin//theme/firstJob/image/arrow_head.png" class="prev-page">Basic Profile</h1></div>


                <div class="clearfix"></div>
				<?php
				//print_r($basic);die;
				?>
                <div class="col-md-12 content_para">


                    <div class="col-md-2 ed_gry_bg">
<!--                        <img src="https://firstjob.co.in/admin//theme/firstJob/image/axda.png" class="img-thumbnail">-->
                        <span class="edit_user_img"><a href="javascript:" class="uploadImage">
                                <img src="https://firstjob.co.in/admin/theme/firstJob/image/default.png" alt="" id="uploadIcon" title="" class="img-responsive"></a>
<!--                            <i class="fa fa-pencil" aria-hidden="true"></i>
                            <div class="pencil"><img src="https://firstjob.co.in/admin//theme/images/edit_image_icon.png" alt="" title=""></div>-->
                            <div id="fimageName"> </div> 
                        </span>
                    </div>
<div class="clearfix"></div>

                        <div class="form-group">
                            <label for="inputEmail" class="control-label col-xs-4 required-field">Name : </label>
                            <div class="col-xs-8">
                                <?php echo $basic->fullname; ?>
                            </div>
                        </div>
<div class="clearfix"></div>

                        <div class="form-group">
                            <label for="inputEmail" class="control-label col-xs-4 required-field">Mobile : </label>
                            <div class="col-xs-8">
                                <?php echo $basic->mobile; ?>
                            </div>
                        </div>
<div class="clearfix"></div>

                        <div class="form-group">
                            <label for="inputEmail" class="control-label col-xs-4 required-field">Email : </label>
                            <div class="col-xs-8">
                                <?php echo $basic->email; ?>
                            </div>
                        </div>
<div class="clearfix"></div>

                        <div class="form-group">
                            <label for="inputEmail" class="control-label col-xs-4">Pin Code : </label>
                            <div class="col-xs-8">
                                <?php echo $basic->pincode; ?>
                            </div>
                        </div>

                        <div class="clearfix"></div>

                        <div class="form-group">
                            <label for="inputEmail" class="control-label col-xs-4">Gender : </label>
                            <div class="col-xs-8">
                                <?php if($basic->gender == 1){ echo "Male"; }else{ echo "Female"; } ?>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        


                </div>

                <div class="clearfix"></div>
                			
                </div>
            </div>

        </div>

    </div>

</form>
	</div>

                </div>
            <!-- /.row -->
            
                <div class="col-lg-12">
            <!-- /.row -->
               
                <!-- /.col-lg-8 -->
                <div class="col-lg-4">
                    <!-- /.panel -->
                    
                    <!-- /.panel -->
                </div>
                <div class="col-lg-4">
                    <!-- /.panel -->
					
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-8 -->
                
                </div>
            <!-- /.row -->

                </div>

            <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #f8f8f8;height: 30px;">
						
                </div>

        </div>
        
        <?php
        
        ?>
        
    <!-- /#page-wrapper -->
    </div>
</div>
<!-- /#wrapper -->



<script src="<?php echo base_url(); ?>theme/vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url(); ?>theme/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>theme/js/bootbox.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->

<!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url(); ?>theme/js/formValidation.min.js"></script>
    <script src="<?php echo base_url(); ?>theme/js/bootstrap.min.js"></script>


<!-- Basic profile js JavaScript -->
<script src="<?php echo base_url(); ?>js/min/basicprofile.min.js"></script>

<?php
$AppliedJobsCount = 0;
$RejectedJobs = 0;
$shortlistedJobs = 0;
$aapliedJobs = 0;
$viewedJobs = 0;
$showChart = 0;
$avgProfileCOmpleteness = "80";
$avgAppliedJobs = "8";
if(count($appliedJobs)>0){
	$showChart = 1;
}
//echo "<pre>";print_r($appliedJobs );die;
foreach($appliedJobs as $apJpbs){
	/*if($apJpbs->appliedStatus == 'Pending'){
		$AppliedJobsCount++;
	}*/
    $AppliedJobsCount = count($appliedJobs);
	
	if($apJpbs->appliedStatus == 'Rejected'){
		$RejectedJobs++;
	}
	
	if($apJpbs->appliedStatus == 'Shortlisted'){
		$shortlistedJobs++;
	}
	$viewedJobs = $apJpbs->viewed;
	
}
if($AppliedJobsCount > 0){
$YAxisCounter = ceil($AppliedJobsCount/5);
}else{
$YAxisCounter = 1;	
}

?>


<script src="<?php echo base_url()?>theme/dist/js/Chart.js"></script>
<script>
    function onLoad(){
        var ctx = document.getElementById("myChart");
        Chart.defaults.global.defaultFontColor = '#fff';

        var myChart = new Chart(ctx, {
            scaleFontColor: '#fff',
            type: 'bar',
            data: {
                labels: ["Applied", "Viewed", "Shortlisted", "Rejected"],
                datasets: [{
                    label: 'Jobs in 30 Days',
                    data: [<?php echo $AppliedJobsCount ?>, <?php echo $viewedJobs ?>, <?php echo $shortlistedJobs ?>, <?php echo $RejectedJobs; ?>],
                    barThickness: 10,
                    legend: {
                        display: false
                    },

                    backgroundColor: [
                        'rgba(240, 107, 74, 1.0)',
                        'rgba(54, 162, 235,  1.0)',
                        'rgba(255, 206, 86,  1.0)',
                        'rgba(75, 192, 192,  1.0)'
                    ],
                    borderColor: [
                        'rgba(240, 107, 74,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)'
                    ],
                    borderWidth: 0
                }]
            },

            options: {
                legend: {
                    display: false
                },
                scales: {
                    xAxes: [{
                        gridLines: {
                            display: false,
                            color: "#FFF"
                        },
                        ticks: {
                            fontColor: "#FFF"
                        }
                    }],
                    yAxes: [{
                        gridLines: {
                            color: "#FFF"
                        },
                        ticks: {
                            beginAtZero:true,
                            stepSize: <?php echo $YAxisCounter?>
                        }
                    }]
                }
            }
        });
        //ctx.height = 500;
    }

</script>

<style>
text{
    fill: white;
}
#newsTickerHover {
	display:none;
	 box-shadow: 2px 2px 2px 2px;background: black;width: 111px;border-radius: 5px;color: white;padding: 5px;width: 150px;
	    position: absolute;
}
#NewsTicker:hover #newsTickerHover {
	
	display:block;
}
td{
	color:black;
}
.table{
	    margin-bottom: 0px;
}

</style>
    <!-- Navigation -->
    <nav class="navbar navbar-inverse" id="navheight">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="<?php echo base_url()?>users"><img src="<?php echo base_url(); ?>images/logo.png" rel="stylesheet"></a>
    </div>
    <?php if($content->eventName) { ?>
        <div class="navbar-header row" style="padding: 10px;
        /* background: white; */
        text-shadow: 2px 1px 0px black;
        color: white;">
          <span class="text-center" style=""><a href="<?php echo base_url().'job/event-jobs/'.$content->id."?eventName=".$content->eventName; ?>" style="    color: white;
        font-size: 20px;
        font-weight: bold;
        text-decoration: none;"><u>Please click here for <?php echo $content->eventName; ?></u></a></span>
        </div>
    <?php } ?>

    
    <div class="collapse navbar-collapse pull-right" id="myNavbar" style="margin-right:112px;">
        <ul class="nav navbar-nav">
            <!--<li style="background:white;border: 1px solid black;    margin-right: 21px;">
                <a href="#" style="color:black;text-decoration: none;padding-left: 7px;">
                    <i class="fa fa-user-plus" style="font-size: 19px;"></i>
                    <span class="dashboard-invite-friends" style="font-weight:600;">Invite Friends</span>
                </a>
            </li>
            <li  style="border: 1px solid black;background:white;    margin-right: 21px;" class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#" style="color:black;text-decoration: none;padding-left: 7px;">
                    <i class="fa fa-video-camera" style="font-size: 19px;"></i>
                    <span class="dashboard-video-profile" style="font-weight:600;">Video Profile</span>
                </a>
            </li>-->
		<?php
					
					if(isset($_SESSION['userImage']) && $_SESSION['userImage'] != ""){
						$image = base_url()."uploads/userImages/".$_SESSION['userImage'];
					}else{
						$image = "https://firstjob.co.in/admin/theme/firstJob/image/default.png";
					}
					
					?>
            <li  style="border: 1px solid black;background:white;    margin-right: 21px;">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#" style="color:black;text-decoration: none;padding-left: 7px;">
                    <img src="<?php echo $image;?>" style="    width: 19px;"><span  style="font-weight:600;">&nbsp;<?php echo $userName?></span>&nbsp;<span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li style="background:white;"><a href="<?php echo base_url(); ?>editProfile" style="color:black;text-decoration: none;"><i class="fa fa-user fa-fw"></i>Edit Profile</a></li>
                    <li style="background:white;"><a href="#" style="color:black;text-decoration: none;" class="changePassword" ><i class="fa fa-key" aria-hidden="true"></i>&nbsp;Change Password</a></li>
                    <li style="background:white;"><a href="<?php echo base_url();?>users/logout" style="color:black;text-decoration: none;"><i class="fa fa-sign-out fa-fw"></i>Logout</a></li>
                </ul>
            </li>
        </ul>
    </div>
  </div>
</nav>
    

    <div class="row" style="    background: #f8f8f8;margin-right: 0px;">
        
			<div id="tabs" style="background: #054F72;    height: 109px;padding: 0 169px;">
                <ul class="nav nav-tabs nav-justified">
                    <li class="active pull-left" style="    width: 21%;cursor: pointer;margin-top: 6px;">
						<a href="<?php echo base_url()?>users" style="text-decoration:none;cursor:pointer;">
                       <table style="background: URL(images/blue1.png) no-repeat;background-size: 90px 90px;width: 90px;height: 90px;">
							<tbody>
								<tr><td cellpadding="0"><div style="text-align: center;"><i class="fa fa-home sub-menu-icon" style="font-size: 25px;color: white;"></i> </div></td>
								</tr>
								<tr><td cellpadding="0"><div style="text-align: center;margin-top: -15px;"><span class="texto_grande" style="color: white;font-size: 16px;">Home</span></div></td>
								</tr>
							</tbody>
						</table>
						</a>
                    </li>
					<li class="active pull-left" style="    width: 21%;cursor: pointer;margin-top: 6px;">
						<a href="<?php echo base_url()?>editProfile" style="text-decoration:none;cursor:pointer;">
                       <table style="background-size: 90px 90px;    width: 85px;height: 90px;">
							<tbody>
								<tr><td cellpadding="0"><div style="text-align: center;"><img src="<?php echo base_url();?>images/icons/profile.png" style="width:25px;"></div></td>
								</tr>
								<tr><td cellpadding="0"><div style="text-align: center;margin-top: -15px;"><span class="texto_grande" style="color: white;font-size: 16px;">Edit Profile</span></div></td>
								</tr>
							</tbody>
						</table>
						</a>
                    </li>
					
					<li class="active pull-left" style="    width: 21%;cursor: pointer;margin-top: 6px;">
						<a href="javascript://" style="text-decoration:none;cursor:pointer;"   class="changePassword active" >
                       <table style="background-size: 90px 90px;width: 90px;height: 90px;">
							<tbody>
								<tr><td cellpadding="0"><div style="text-align: center;"><i class="fa fa-key" aria-hidden="true" style="font-size: 25px;color: white;"></i></div></td>
								</tr>
								<tr><td cellpadding="0"><div style="text-align: center;margin-top: -15px;"><span class="texto_grande" style="color: white;font-size: 16px;">Change Password</span></div></td>
								</tr>
							</tbody>
						</table>
						</a>
                    </li>
					
					<li class="active pull-left" style="    width: 21%;cursor: pointer;margin-top: 6px;">
						<a href="<?php echo base_url()?>users/logout" style="text-decoration:none;cursor:pointer;"   class="changePassword active" >
                       <table style="background-size: 90px 90px;width: 90px;height: 90px;">
							<tbody>
								<tr><td cellpadding="0"><div style="text-align: center;"><i class="fa fa-power-off" aria-hidden="true" style="font-size: 25px;color: white;"></i></div></td>
								</tr>
								<tr><td cellpadding="0"><div style="text-align: center;margin-top: -15px;"><span class="texto_grande" style="color: white;font-size: 16px;">Logout</span></div></td>
								</tr>
							</tbody>
						</table>
						</a>
                    </li>
                   
                    
					<!--<li class="pull-left"  style="width: 25%;    margin-top: 36px;">
                        <a style="    cursor: pointer;text-decoration: none;background-size: 98px 94px;height: 100%;margin-top: 11px;width: 100px;margin-left: 0px;"  class="changePassword active" >
                            <i class="fa fa-key" aria-hidden="true" style="font-size: 40px;"></i><br>
                            <span class="texto_grande">Change Password</span>
                        </a>
                    </li>-->
					
                    
                </ul>
        <!-- /.col-lg-12 -->
        </div>
		
        <div class="container-full" style="background: white;" id="homeHeading">
            <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #054F72;height: 33px;">
                
            </div>
            <div  class="col-md-8 col-lg-8 col-sm-8 pull-left" style="width: 80%;background: #EB9B2C ;height: 33px;">
                <div style="float:left;"><h5 style="font-weight: bold;font-size: 16px;">Home</h5></div>
				<div style="    float: left;margin-top: 4px;margin-bottom: 4px;    margin-left: 27%;width: 43%;">
					
				
					<input type="text" name="jobCode" class="form-control" id="searchedJobCode" value="" placeholder="Apply with Job Code"  style="    font-weight: bold;height:26px;width: 84%;    float: left;">
					<button class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Please click here to apply for Job" onclick="getJobDetailspAge()" style="float: left;    margin-left: 4px;    padding: 2px;width: 14%;"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>
					<div style="clear:both"></div>
					<div class="alert alert-danger fade in" id="JobcodeValidation" style="display:none;    position: relative;margin-top: -15%;width: 98%;">
						<a href="#" class="close" data-dismiss="alert">&times;</a>
						<strong>Error!</strong> A problem has been occurred while submitting your data.
					</div>
				</div>
				<div style="clear:both"></div>
            </div>
            <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #054F72;height: 33px;">
                
            </div>
            
        </div>

        <div class="container-full" style="background: white;">
            <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #f8f8f8;height: 30px;">

                </div>
            <div  class="col-md-8 col-lg-8 col-sm-8 pull-left" id="mainBodyContent" style="    margin-bottom: 20px;padding-top: 10px;width: 80%;background: white ;">
                <div class="col-lg-12">
                    <!-- /.row -->
                <div class="col-lg-4">
				<div class="panel panel-default" style="height:133px;">
                        <div class="panel-heading">
                            Recent Messages
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-striped">
                                    <tbody>

                                        <?php if(count($messages) > 0) { ?>
                                            <?php if(count($messages) >= 2) { $messageCount = 2; } else { $messageCount = count($messages);  }
											for($i = 0; $i<$messageCount; $i++){
												if($i%2  == 0){
													
													$backGroundColor = "#fff";
												}else{
													$backGroundColor = "#E5E5E5";
												}
												?>
												 <tr style="background-color:<?php echo $backGroundColor; ?>">
                                                <td>
                                                <a href="javascript:" 
                                            data-toggle="modal" 
                                            data-target="#messageModal" 
                                            class="showmessages" onclick="getmessageDetailsById(<?php echo $messages[$i]->messageId; ?>);"><?php echo $messages[$i]->senderName; ?> </a></td>
                                            </a>

                                                </td>
                                                <td><?php echo $messages[$i]->messageSubject;?></td>
                                                <td>
                                                    <?php echo date('d M, Y', strtotime($messages[$i]->time)); ?>
                                                    <input type="hidden" name="">
                                                </td>
                                            </tr> 
												
											<?php }
											 ?>
                                        <?php } else { ?>
										<div style="text-align: center; padding: 23px"><span style="font-size: 15px">No Messages</span></div>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <div class="panel panel-default"  style="height:133px;">
                        <div class="panel-heading">
                            Open Job Invitations
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-striped">
                                    <tbody>
                                        <?php  if(count($jobInvitations) > 0) { 
										$i = 0;
										?>
                                            <?php foreach($jobInvitations as $jobInvitation) { ?>
											<?php if($i%2  == 0){
													
													$backGroundColor = "#fff";
												}else{
													$backGroundColor = "#E5E5E5";
												}
												$i++;
												?>
                                            <tr  style="background-color:<?php echo $backGroundColor; ?>">
                                                <td><?php echo $jobInvitation->company; ?></td>
                                                <td><a href="<?php echo base_url(); ?>job/invitation/<?php echo $jobInvitation->invitationId;?>"><?php echo $jobInvitation->jobName; ?></a></td>
                                                <td><?php echo date('d M, Y', strtotime($jobInvitation->invitationDate)); ?></td>
                                            </tr>
                                            <?php } ?>
                                        <?php } else { ?>
                                            <div style="text-align: center; padding: 23px"><span style="font-size: 15px">No Job Invitation</span></div>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->

                    
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-4 -->
                <div class="col-lg-8">
                    <div class="row1">
                        <div class="col-lg-12" style="background:#054F72;">
                            <div class="panel panel-default" style="    border: none;">

                                <!-- /.panel-heading -->
                                <div class="col-lg-7 panel-body" style="height: 265px;     padding-left: 0px;   padding-right: 0px;">
								<h2 style="margin-top: 15px;   font-size: 16px;color: white;">Jobs in 30 days</h2>
								<?php
								
								//if($showChart == 1){ 
								?>
                                    <div style="width:300px;height:200px;background-color: #054f72">
										<canvas id="myChart" width="300" height="200"></canvas>
										<?php if($showChart != 1){?>
											<div style="
    /* position: absolute; */
">
  <div style="
    position: relative;
    bottom: 117px;
    margin-left: 29%;
    width: 100%;
    color: white;
    font-size: 15px;
">
    No Jobs Applied
  </div>
</div>
										<?php } ?>
									</div>
								<?php // } else { ?>
									<!--<img src="<?php //echo base_url()?>images/icons/bar.png" style="max-height:200px;">-->
								<?php // } ?>
                                </div>
                                <div class="col-lg-5 panel-body" style="">

                                    <div class="panel-default" style="margin-top: 0px;">

                                        <!-- /.panel-heading -->
                                        <div class="panel-body">
                                            <div class="table-responsive" style="    font-size: 10px;">
                                                <table class="table table-bordered table-hover table-striped" style="    border: none;">
                                                        <tr style="    background: none;">
                                                            <th colspan="5" style="color:white;font-size: 14px;padding-left: 0px;    padding-top: 15px;">Profile Completeness</th>
                                                        </tr>
														<?php 
														$avgProfileCOmpleteness = "80";
														$avgAppliedJobs = "8";
														if(ceil($profile_Percent) == 100){
															$toolTipMessageProfile = "Your Profile is Complete. Congratulations.";
														}elseif(ceil($profile_Percent) >= $avgProfileCOmpleteness){
															$toolTipMessageProfile = "You are doing better than other candidates! Great";
														}else{
															$toolTipMessageProfile = "Please complete your profile so that companies can find you.";
														}
														
														
														if(count($appliedJobs) >= $avgAppliedJobs){
															$toolTipMessage = "You are doing better than other candidates! Great";
														}else{
															$toolTipMessage = "Please apply for more jobs each month.";
														}
														
														if($profile_Percent == 100){?>
                                                        <tr  data-toggle="tooltip" data-placement="top" title="<?php echo $toolTipMessageProfile ?>" >
                                                            <th colspan="5" style="    padding-right: 2px;    text-align: right;    background: #eed33a;"><?php echo $profile_Percent."%"; ?></th>
                                                           
                                                        </tr>
                                                        <tr style="    background: none;">
                                                            <th colspan="5" style="    font-size: 13px;color:white;text-align: right;padding: 0px;">
                                                                    <span class="glyphicon glyphicon-triangle-top" aria-hidden="true" style="color:#eed33a;"></span>
                                                            </th>

                                                        </tr>
														<?php
														}else{
														?>
														<tr  >
                                                            <th colspan="3" style="    background: #eed33a;    padding-right: 2px;width:<?php echo ceil($profile_Percent)."%"?>;    text-align: right;" title="Your profile is <?php echo ceil($profile_Percent)."%"; ?> complete."><?php echo ceil($profile_Percent)."%"; ?></th>
                                                            <th style="    background: #36a2eb;    padding-right: 2px;    text-align: right;" title="<?php echo $toolTipMessageProfile ?>">80%</th>
                                                            <th style="    background: #4f994c;" title="Job seekers with more than 80% profile completeness have higher chances of finding jobs"></th>
                                                        </tr>
                                                        <tr style="    background: none;">
                                                            <th colspan="3" style="    font-size: 12px;color:white;text-align: right;padding: 0px;" title="">
                                                                    <span class="glyphicon glyphicon-triangle-top" aria-hidden="true" style="color:#eed33a;"></span>
                                                            </th> 
                                                            <th style="   font-size: 12px;color:white;text-align: right;padding: 0px;" >
                                                                <span class="glyphicon glyphicon-triangle-top" aria-hidden="true" style="color:#36a2eb"></span>
                                                            </th>
                                                            <th style="color:white;text-align: right;padding: 0px;"></th>
                                                        </tr>
														
														<?php } ?>
												</table>
												
												
												
												
												
												
												
												
												
												<table class="table table-bordered table-hover table-striped" style="    border: none;">
                                                        <tr style="    background: none;">
                                                            <th colspan="5" style="color:white;font-size: 14px;padding-left: 0px;    padding-top: 35px;">Applied Jobs</th>
                                                        </tr>
                                                        <tr>
                                                            <th colspan="3" style="    background: #eed33a;    padding-right: 2px;width:<?php if(count($appliedJobs) > 0){echo count($appliedJobs)*10;}else{ echo "0.001"; } ?>%;    text-align: right;" title="In last 30 days, you applied for <?php echo count($appliedJobs) ?> jobs"><?php echo count($appliedJobs) ?></th>
                                                            <th style="    background:#36a2eb;    padding-right: 2px;    text-align: right;"  data-toggle="tooltip" data-placement="top" title="<?php echo $toolTipMessage?>" >8</th>
                                                            <th style="    background:  #4f994c;width:10%" title="Job seekers who apply 8 or more jobs in 30 days have higher chances of finding jobs."></th>
                                                        </tr>
                                                        <tr style="    background: none;">
                                                            <th colspan="3" style="   font-size: 12px;color:white;text-align: right;padding: 0px;">
                                                                    <span class="glyphicon glyphicon-triangle-top" aria-hidden="true"  style="color:#eed33a;"></span>
                                                            </th>
                                                            <th style="   font-size: 12px;	color:white;text-align: right;padding: 0px;">
                                                                <span class="glyphicon glyphicon-triangle-top" aria-hidden="true" style="color:#36a2eb"></span>
                                                            </th>
                                                            <th style="color:white;text-align: right;padding: 0px;"></th>
                                                        </tr>
                                                        
                                                </table>
                                            </div>
											<div style="    margin-top: 16px;">
											
												<div style="color:white;"><span class="glyphicon glyphicon-triangle-top" aria-hidden="true"  style="color:#eed33a;"></span>&nbsp;You are here</div>
												<div style="color:white;"><span class="glyphicon glyphicon-triangle-top" aria-hidden="true" style="color:#36a2eb"></span>&nbsp;Category Average</div>
											</div>
                                            <!-- /.table-responsive -->
                                        </div>
                                        <!-- /.panel-body -->
                                    </div>
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-6 (nested) -->

                        <div class="col-lg-5">
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.col-lg-8 -->
                </div>   
            <!-- /.row -->
                <div class="col-lg-12" style="margin-top:10px;">
            <!-- /.row -->
			
            <div class="col-lg-4" style="height:120px">
                <!-- /.panel -->
                <div class="panel panel-default" style="height:133px;">
                    <div class="panel-heading">
                        Saved Jobs
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="row1">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-striped">
                                    <tbody>
                                        <?php if(count($savedJobs) > 0) { ?>
                                            <?php if(count($savedJobs) >= 2) { $saveLimit = 2; } else { $saveLimit = count($savedJobs);  } for($k=0; $k<$saveLimit; $k++) { ?>
											<?php if($k%2  == 0){
													
													$backGroundColor = "#fff";
												}else{
													$backGroundColor = "#E5E5E5";
												}
												?>
                                            <tr  style="background-color:<?php echo $backGroundColor; ?>">
                                                <td><a href="<?php echo base_url(); ?>job/details/<?php echo $savedJobs[$k]->jobCode;?>"><?php echo $savedJobs[$k]->title;?></a></td>
                                                <td><?php echo $savedJobs[$k]->companyName;?></td>
                                                <td><?php echo date('d M, Y', strtotime($savedJobs[$k]->openTill)); ?></td>
                                            </tr>
                                            <?php } ?>
                                        <?php } else { ?>
                                            <div style="text-align: center; padding: 12px"><span style="font-size: 15px">No Saved Job</span></div>
                                        <?php } ?>
                                    
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.panel-body -->
                </div>
            </div>

            <!-- /.col-lg-8 -->
            <div class="col-lg-8" style="height:120px">
                <!-- /.panel -->
                <div class="panel panel-default" style="height:133px;">
                    <div class="panel-heading">
                        Recently Applied Jobs
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped">
                                <tbody>
                                    <?php if(count($appliedJobs) > 0) { ?> 
                                        <?php if(count($appliedJobs) >= 2) { $appLimit = 2; } else { $appLimit = count($appliedJobs);  } 
										for($j=0; $j<$appLimit; $j++) { ?>
										<?php if($j%2  == 0){
													
													$backGroundColor = "#fff";
												}else{
													$backGroundColor = "#E5E5E5";
												}
												?>
                                        <tr  style="background-color:<?php echo $backGroundColor; ?>">
                                            <td><?php echo $appliedJobs[$j]->companyName;?></td>
                                            <td><a href="<?php echo base_url(); ?>job/details/<?php echo $appliedJobs[$j]->jobCode;?>"><?php echo $appliedJobs[$j]->title;?></a></td>
                                            <td><?php echo date('d M, Y', strtotime($appliedJobs[$j]->appliedAt)); ?></td>
                                            <td>
                                                <?php
                                                    if($appliedJobs[$j]->appliedStatus == 'Shortlisted' || $appliedJobs[$j]->appliedStatus == 'Rejected') {
                                                        echo $appliedJobs[$j]->appliedStatus;
                                                    } elseif ($appliedJobs[$j]->appliedStatus == 'Applied' && $appliedJobs[$j]->viewedStatus == 1) {
                                                       echo 'Viewed';
                                                    } else {
                                                        echo $appliedJobs[$j]->appliedStatus;
                                                    }
                                                ?>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    <?php } else { ?>
                                        <div style="text-align: center; padding: 12px"><span style="font-size: 15px">No Applied Job</span></div>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-8 -->

                
                </div>
            <!-- /.row -->

                </div>

            <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #f8f8f8;height: 30px;">

                </div>

        </div>
        
        <?php
        
        ?>
        
    <!-- /#page-wrapper -->
    </div>
</div>
<!-- /#wrapper -->
<div class="col-md-12 col-lg-12 col-sm-12" style="margin-top:10px;position:fixed;bottom:0px;">
                    <div id="NewsTicker"  style="position:relative;bottom:15px;border: 1px solid grey;color: #2590e6;padding: 5px;font-weight: 600;cursor:pointer;">
					<div>
						<i class="fa fa-newspaper-o" aria-hidden="true"></i><a href="javascript://"   data-toggle="modal" data-target="#NewsDetails"><u>Campus Compensatioin: Companies prefer to link pay to performance</u></a></div>
								<div id="newsTickerHover" style="">Latest Job News</div>
					</div>
					
            </div>

<div class="modal fade" id="NewsDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header" style="border-bottom:none;">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h2 id="myModalLabel" class="newsHeader" style="color:#08688F;font-size: 20px;"></h2>
      </div>
        <div class="modal-body newsImage">
        </div>
        <div class="modal-body newsBody">
        </div>
        
    </div>
  </div>
</div>

<div class="modal fade" id="EducationNews" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header" style="border-bottom:none;">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h2 id="myModalLabel" style="color:#08688F;font-size: 20px;">
                <img src="<?php base_url();?>images/logo1.png" style="width:150px;">
            </h2>
      </div>
        <div class="modal-body">
            <iframe src="" id="EducationNewsBody" style="width:100%; height: 500px;"></iframe>
        </div>
        
    </div>
  </div>
</div>

<div class="modal fade" id="messageModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 class="modal-title" style="float:left;"><div id="message_company_name"></div></h3><div id="message_date_time" style="float:right; margin-right: 11px;   font-size: 13px;"></div>
            </div>
            <div class="modal-body" style="    padding-top: 0px;">
			<h3><div id="message_title" style="    font-size: 19px;"></div></h3>
                <div id="message_message" style="    font-size: 13px;"></div>
            </div>
            <div class="modal-footer">
			
                <button type="button" class="btn btn-default " data-dismiss="modal">Close</button>
            </div>      
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

</div>
<script src="<?php echo base_url(); ?>theme/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url(); ?>theme/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>theme/js/bootbox.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo base_url(); ?>theme/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="<?php echo base_url(); ?>theme/vendor/raphael/raphael.min.js"></script>
    <script src="<?php echo base_url(); ?>theme/vendor/morrisjs/morris.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url(); ?>theme/dist/js/sb-admin-2.js"></script>
    <script src="<?php echo base_url() ?>theme/js/formValidation.min.js"></script>
<script src="<?php echo base_url() ?>theme/js/bootstrap.min.js"></script>

	

<script type="text/javascript">
    function getmessageDetailsById(id) {
        var tokenUser = '<?php echo $token ?>';
        var js_obj = {token: 'ecbcd7eaee29848978134beeecdfbc7c',methodname: 'getMessageDetails',userId:tokenUser, messageId:id};
        var encoded = JSON.stringify( js_obj );
        var data= encoded;
        $.ajax({
              url : "<?php echo base_url()?>Fjapi",
              data: data,
              type: "POST",
              success: function(response){
                    item = $.parseJSON(response);
                    if(item.code == "200"){
                        $("#message_title").text(item.data.messageSubject);
                        $("#message_company_name").text(item.data.senderName);
                        $("#message_date_time").text(item.data.time);
						var messageContent = item.data.message;
						
                        $("#message_message").html(messageContent);
                    }else{
                        alert(item.message);
                    }
                }
        });
    }
	
	function checkUserMessageStatus(){
		
	}
</script>
<?php
if($userName == ""){?>
<script>

$(document).on('click', '.showmessages', function () {
    document.addEventListener('contextmenu', event => event.preventDefault());
    var messageSubject    = $(this).data('options').messageSubject;

        alert(messageSubject);
     });


 $(document).ready(function() {
	 
	 $("svg").css("width","100%");
	 $("svg").css("height","200px");
	 
        var methodname = 'getBasicInformation';
        var tokenUser = '<?php echo $token ?>';
        var js_obj = {token: 'ecbcd7eaee29848978134beeecdfbc7c',methodname: methodname,userId:tokenUser};

        var encoded = JSON.stringify( js_obj );

        var data= encoded;
        
        $.ajax({
              url : "<?php echo base_url()?>Fjapi",
              data: data,
              type: "POST",
              success: function(response){
                    item = $.parseJSON(response);
                    if(item.code == "200"){
                        var obj = JSON.parse(response);
                        var fullname = JSON.stringify(obj.basicInfo[0].fullname);
                        var email = JSON.stringify(obj.basicInfo[0].email);
                        var mobile = JSON.stringify(obj.basicInfo[0].mobile);
                        var dob = JSON.stringify(obj.basicInfo[0].dob);
                        var pincode = JSON.stringify(obj.basicInfo[0].pincode);
                        var gender = JSON.stringify(obj.basicInfo[0].gender);
                        var profilePercent = JSON.stringify(obj.basicInfo[0].profilePercent);
                        
                        $("#name").val(fullname.replace(/\"/g, ""));
                        $("#mobile").val(mobile.replace(/\"/g, ""));
                        $("#email").val(email.replace(/\"/g, ""));
                        $("#pincode").val(pincode.replace(/\"/g, ""));
                        $("#datepicker-13").val(dob.replace(/\"/g, ""));
                        $("#gender").val(gender.replace(/\"/g, ""));
                        //window.location="<?php echo base_url()?>users/home";
                    }else{
                        //alert(item.message);
                    }
              }
        });
		
		answerNowTimer = window.setTimeout(setheightChart, 5000);
    });
       
	   function setheightChart(){
		   $("svg").attr("height","200");
		   
	   }
function saveBasicInfo(){
        //var forData = $('#createSubuserForm').serialize();
        //var data= JSON.stringify($('#createSubuserForm').serializeObject());
        var userId = $("#userId").val();
        var token = $("#token").val();
        var methodname = $("#methodname").val();
        var name = $("#name").val();
        var email = $("#email").val();
        var mobile = $("#mobile").val();
        var gender = $("#gender").val();
        var dob = $("#datepicker-13").val();
        var pincode = $("#pincode").val();
        
        
        var js_obj = {userId: userId, token: token,methodname: methodname, basicInformation: [{fullname: name, email: email, mobile: mobile, gender: gender, dob: dob, pincode: pincode}] };

		var encoded = JSON.stringify( js_obj );

		var data= encoded;

        
        
        
        $.ajax({
              url : "<?php echo base_url()?>Fjapi",
              data: data,
              type: "POST",
              success: function(response){
                    item = $.parseJSON(response);
                    if(item.code == "200"){
                        var obj = JSON.parse(response);
                        var fullname = JSON.stringify(obj.basicInfo[0].fullname);
                        var email = JSON.stringify(obj.basicInfo[0].email);
                        var mobile = JSON.stringify(obj.basicInfo[0].mobile);
                        var dob = JSON.stringify(obj.basicInfo[0].dob);
                        var pincode = JSON.stringify(obj.basicInfo[0].pincode);
                        var gender = JSON.stringify(obj.basicInfo[0].gender);
                        var profilePercent = JSON.stringify(obj.basicInfo[0].profilePercent);
                        
                        $("#name").val(fullname.replace(/\"/g, ""));
                        $("#mobile").val(mobile.replace(/\"/g, ""));
                        $("#email").val(email.replace(/\"/g, ""));
                        $("#pincode").val(pincode.replace(/\"/g, ""));
                        $("#datepicker-13").val(dob.replace(/\"/g, ""));
                        $("#gender").val(gender.replace(/\"/g, ""));
                        //window.location="<?php echo base_url()?>users/home";
                    }else{
                        alert(item.message);
                    }
              }
        });
    
    }
</script>
<?php }
?>


<script>

$(document).ready(function(){
    $.fn.serializeObject = function()
    {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
    

});

$(document).ready(function(){
	var WindowHeight = $(window).height();
	
	/*$("#navheight").css("height",(WindowHeight*6.5)/100);
	$("#tabs").css("height",(WindowHeight*13)/100);
	$("#homeHeading").css("height",(WindowHeight*3.5)/100);
	$("#mainBodyContent").css("height",(WindowHeight*72)/100);*/
    getTickers();
});
var count = 0;

function getEducationalTImes(url){
	var x = screen.width/2 - 700/2;
    var y = screen.height/2 - 450/2;
	var width = $( window ).width();
	var height = $( window ).height();
    myWindow = window.open(url, "", "width="+width/2+",height="+height/2+",left="+x+",top="+y);
    //$('#EducationNewsBody').attr('src', 'http://35.154.53.72/jobseeker/TestChart.php');

}

function getTickers(){
    
    var methodname = 'getTickerList';//getTickerList getTopTickers
    var token = 'ecbcd7eaee29848978134beeecdfbc7c';
    var userId = '<?php echo $token; ?>';

    var js_obj = {userId: userId, token: token, methodname: methodname};

    var encoded = JSON.stringify( js_obj );

    var data= encoded;
   $.ajax({
            url : "<?php echo base_url(); ?>Fjapi",
            data: data,
            type: "POST",
            success: function(response){
                
                var obj = JSON.parse(response);
                
                if(obj.data[count].title == 'undefined'){
                    count = 0;
                }
                var Header = JSON.stringify(obj.data[count].title);
                Header = Header.replace(/\"/g, "");
                var Image = JSON.stringify(obj.data[count].thumbnail);
                Image = Image.replace(/\"/g, "");
                
                var URLWithQuotes = JSON.stringify(obj.data[count].link);
                URL = URLWithQuotes.replace(/\"/g, "");
                var Description = JSON.stringify(obj.data[count].description);
                var Description = Description.replace(/\"/g, "");
                var Description = Description.replace(/(?:\\[rn])+/g, "</br>");
                $(".newsHeader").html("<a href='javascript://' style='text-decoration:none;color:#08688F;'>"+Header+"</a>");
                URL = "'"+URL+"'";
				
                $(".newsImage").html("<img src='"+Image+"' style='width:100%;'>");
                $("#NewsTicker").html('<div><a href="javascript://"  onclick="getEducationalTImes('+URL+');" data-toggle="modal" style="color:#2590e6;"><i class="fa fa-newspaper-o" aria-hidden="true"></i>&nbsp;&nbsp;<u>'+Header+'</u></a></div><div id="newsTickerHover" style="">Latest Job News</div>');
                $(".newsBody").html(Description);
                count++;

            },
            complete: function(){
                setTimeout(getTickers, 15000);
            }
    });
 
}
 function getJobDetailspAge(){
	var SearchedJobCode = $("#searchedJobCode").val();
		if(SearchedJobCode != ""){
			window.location="<?php echo base_url()?>job/details/"+SearchedJobCode;
		}else{
			$("#JobcodeValidation").html('<a href="#" class="close" data-dismiss="alert">&times;</a><strong>Error!</strong> Please Enter the Job Code.')
			$("#JobcodeValidation").show(1000);
		}
		
		setTimeout(function() {
        $("#JobcodeValidation").hide()
		}, 4000);
	 
	 
 }

</script>
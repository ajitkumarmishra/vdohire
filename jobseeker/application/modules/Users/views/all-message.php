<style>
.dataTables_filter{
	    margin-top: 6px;
    margin-right: 6px;
}
.dataTables_info{
	display:none;
}
</style>
    <!-- Navigation -->
 
        <div class="container-full" style="background: white;">
            <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #054F72;height: 33px;">
                
            </div>
            <div  class="col-md-8 col-lg-8 col-sm-8 pull-left" style="width: 80%;background: #EB9B2C ;height: 33px;">
                <h5 style="font-weight: bold;font-size: 18px;">Recent Messages <?php echo "(".$totalMessages.")"; ?><input type="hidden" name="TotalMessageCounter" id="TotalMessageCounter" value="<?php echo $totalMessages ?>"></h5>
            </div>
            <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #054F72;height: 33px;">
                
            </div>
            
        </div>

        <div class="container-full" style="background: white;">
            <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #f8f8f8;height: 30px;">
            </div>

            <div  class="col-md-8 col-lg-8 col-sm-8 pull-left" style="    margin-bottom: 20px;padding-top: 20px;width: 80%;background: white ;">
                
                <div class="col-lg-12">
                <!-- /.row -->

                    <!-- /.panel -->
                    <div class="panel panel-default">
                        
                        <!-- /.panel-heading -->
                        <div class="panel-body">

                                <table width="100%" class="table table-striped  table-hover" id="dataTables-example">
                                    <thead style="background:#69686D;color:white;">
                                        <th style="width:35%;border-bottom-width: 0px;">Company</th>
                                        <th style="width:35%;border-bottom-width: 0px;">Subject</th>
                                        <th style="width:20%;border-bottom-width: 0px;">Created Date</th>
                                        <th style="width:5%;border-bottom-width: 0px;">View</th>
                                        <th style="width:5%;border-bottom-width: 0px;">Reply</th>
                                    </thead>
                                    <tbody>
										<?php //if(count($content) >= 15) { $contentLimit = 15; } else { $contentLimit = count($content);  } ?>
                                        <?php for($j = 0; $j<count($content); $j++) { ?>
										<?php if($j%2  == 0){
													$backGroundColor = "#E5E5E5";
												}else{
													$backGroundColor = "#fff";
													
												}
												?>
                                        <tr style="background-color:<?php echo $backGroundColor; ?>">
											<td><?php echo $content[$j]->senderName; ?></td>
                                            <td><?php if($content[$j]->messageSubject == ""){ echo "Message from ".$content[$j]->senderName; }else{echo $content[$j]->messageSubject;} ?></td>
                                            <td><?php echo $content[$j]->time; ?></td>
                                            <td><a href="javascript:" data-toggle="modal" data-target="#messageModal" class="showmessages" id="welcomeToFirstJob"   onclick="getmessageDetailsById(<?php echo $content[$j]->messageId; ?>);">View</a></td>
                                            <td><a href="javascript:" data-toggle="modal" data-target="#messageReplyModal" class="showmessagesreply" id="welcomeToFirstJobreply"   onclick="ReplyTomessage(<?php echo $content[$j]->messageId; ?>);">Reply</a></td>
                                            
                                        </tr>
                                        <?php } ?>
										
                                    </tbody>
									<tbody id="infinteScrollContent1">
									
									</tbody>
                                </table>

                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.col-lg-8 -->
                </div>
                <!-- /.row -->
            </div>

            <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #f8f8f8;height: 30px;">
            </div>
        </div>
    <!-- /#page-wrapper -->
    </div>
</div>
<div class="modal fade" id="messageModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 class="modal-title" style="float:left;"><div id="message_company_name"></div></h3><div id="message_date_time" style="float:right; margin-right: 11px;   font-size: 13px;"></div>
            </div>
            <div class="modal-body" style="    padding-top: 0px;">
			<h3><div id="message_title" style="    font-size: 19px;"></div></h3>
                <div id="message_message" style="    font-size: 13px;"></div>
            </div>
            <div class="modal-footer">
			
                <button type="button" class="btn btn-default " data-dismiss="modal">Close</button>
            </div>      
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="messageReplyModal">
    <div class="modal-dialog">
        <div class="modal-content">
		<form id="messageReplyBox">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <div class="modal-title" style="float:left;"><b>Reply To:&nbsp;&nbsp;</b></div><div id="message_company_nameReply" class="pull-left"></div>
				<div style="clear:both"></div>
            </div>
            <div class="modal-body" style="    padding-top: 0px;">
			<h3><div id="message_titleReply" style="    font-size: 19px;"></div></h3>
                
				<input type="hidden" name="message_id" id="message_id" value="">
				<input type="hidden" name="senderId" id="senderId" value="">
				<textarea name="replyBox" cols="10" rows="6" style="width:100%" id="replyBox"></textarea>
            </div>
            <div class="modal-footer">
				
                <button type="button" onclick="replyTo()" class="btn btn-default pull-right" style="background: #054F72; color: white;">Reply</button>
            </div>      
			</form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script src="<?php echo base_url(); ?>theme/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url(); ?>theme/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>theme/js/bootbox.min.js"></script>

    <!-- Metis Menu Plugin JavaScript 
    <script src="<?php echo base_url(); ?>theme/vendor/metisMenu/metisMenu.min.js"></script>-->

    <!-- DataTables JavaScript -->
    <script src="<?php echo base_url(); ?>theme/vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>theme/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>theme/vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Morris Charts JavaScript
    <script src="<?php echo base_url(); ?>theme/vendor/raphael/raphael.min.js"></script>
    <script src="<?php echo base_url(); ?>theme/vendor/morrisjs/morris.min.js"></script>
    <script src="<?php echo base_url(); ?>theme/data/morris-data.js"></script>-->

    <!-- Custom Theme JavaScript 
    <script src="<?php //echo base_url(); ?>theme/dist/js/sb-admin-2.js"></script>
    <script src="<?php //echo base_url(); ?>theme/js/formValidation.min.js"></script>-->
    <!--<script src="<?php //echo base_url(); ?>theme/js/bootstrap.min.js"></script>-->
    <script src="<?php echo base_url(); ?>js/all-message.js"></script>
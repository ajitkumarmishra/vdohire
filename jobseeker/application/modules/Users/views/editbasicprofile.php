
<style>
.form-group.required .control-label:after { 
   content:"*";
   color:red;
}
.form-group.required .control-label:after{
	float:left;
	
}

.fileinput-button{
	display:none;
	
}
.edit_user_img:hover .fileinput-button{
	display:block;
}

.datepicker-years {
    display: block !important;
}

</style>
<?php
//Get Notification unread count
$getNotificationUnreadCount = 0;

//print_r($allNotificationList);exit;
foreach ($allNotificationList as $notiCount) {
    if($notiCount->status == 0) {
        $getNotificationUnreadCount++;
    }
}
//End of get Notification unread count
?>

        
        <div style="clear:both"></div>
        <div class="container-full" style="background: white;">
            <div  class="col-md-3 col-lg-3 col-sm-3 pull-left" style="background: #f8f8f8;height: 30px;">
			<ul class="nav nav-tabs editProfileNav" style="background-color: #f8f8f8;">
                            <li style="    width: 100%;" class="active"><a style="    width: 100%;background:none;color: black; text-decoration: none; font-size: 16px; font-weight: bold;" data-toggle="tab" href="#menu1">Personal Information</a></li>
                            <li style="    width: 100%;" ><a style="    width: 100%;background:none;color: black; text-decoration: none; font-size: 16px; font-weight: bold;" data-toggle="tab" href="#menu2">Educational Information</a></li>
                            <li style="    width: 100%;" ><a style="    width: 100%;background:none;color: black; text-decoration: none; font-size: 16px; font-weight: bold;" data-toggle="tab" href="#menu3">Employment History</a></li>
                            <li style="    width: 100%;" ><a style="    width: 100%;background:none;color: black; text-decoration: none; font-size: 16px; font-weight: bold;" data-toggle="tab" href="#menu4">Extra Information</a></li>
                            <li style="    width: 100%;" ><a style="    width: 100%;background:none;color: black; text-decoration: none; font-size: 16px; font-weight: bold;" data-toggle="tab" href="#menu5">Social Media Information</a></li>
                        </ul>
			</div>
            <div  class="col-md-9 col-lg-9 col-sm-9 pull-left" style="border:1px solid lightgrey;padding-bottom: 23px;margin-bottom: 20px;padding-top: 20px;background: white ;">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="tab-content">
                            <div id="menu1" class="tab-pane fade in active">
                                <form method="post" action="" name="createUser" enctype="multipart/form-data" id="createSubuserForm" class="has-validation-callback" style="margin-top: 24px;">
                                    <div class="col-md-12 content_para">
                                        <?php
                                            if($basic->image != ""){
                                                $image = base_url()."uploads/userImages/".$basic->image;
                                            }else{
                                                $image = "https://firstjob.co.in/admin/theme/firstJob/image/default.png";
                                            }
                                        ?>
                                        <div class="col-md-2 ed_gry_bg" style="margin-left: 39%;">
                                            <div class="edit_user_img"><a href="javascript:" class="uploadImage">
                                                    <img src="<?php echo $image;?>" alt="" id="uploadIcon" title="" class="img-responsive"></a>
                                                    <span class="btn btn-success fileinput-button" style="position: relative;overflow: hidden;display: inline-block;    margin-top: 5px;    width: 100%;">
                                                        
                                                        <span>Select Photo</span>
                                                        <!-- The file input field used as target for the file upload widget -->
                                                        <input type="file" name="uploadProfile"  id="profileImage" value="" style="    cursor: pointer;    top: 0;    right: 0;    margin: 0;    opacity: 0; position: absolute;bottom: 0px;color: white;">
                                                        
                                                    </span>
                                                    
                                                    <div style="">
                                                        <input type="button" name="Upload" value="Upload" id="showUploadButton"  style="display:none;    width: 100%;    margin-top: 7px;" class="btn btn-primary"  onclick="uploadProfilePic()">
                                                    </div>
                                                <div id="fimageName"> </div> 
                                            </div>
                                        </div>
                                    </div>


                <div class="clearfix"></div>

                <div class="col-md-12 content_para">


<div class="clearfix"></div>

                        <div class="form-group required" style="margin-top:20px;">
                            <label for="inputEmail" class="control-label col-xs-4 required-field required" required="true">Name : </label>
                            <div class="col-xs-8">
                                <input type="text" value="<?php echo $basic->fullname; ?>" name="param[basicInformation][]['fullname']"  placeholder="Name"  id="name" class="form-control" data-validation="required" required="required">
								<div class="alert-error" id="nameError" style="display:none;padding-top:10px;color:red;font-size: 12px;">
									<strong>Please enter name.</strong>
								</div>
                            </div>
                        </div>
<div class="clearfix"></div>

                                        <div class="clearfix"></div>
                                        <div class="form-group required" style="margin-top:20px;">
                                            <label for="inputEmail" class="control-label col-xs-4 required-field">Email : </label>
                                            <div class="col-xs-8">
                                                <input type="text" value=" <?php echo $basic->email; ?>" name="param[basicInformation][]['email']" id="email" placeholder="Email"  class="form-control" data-validation="required">
                                                <div class="alert-error" id="emailError" style="display:none;padding-top:10px;color:red;font-size: 12px;">
                                                    <strong>Please enter a valid Email.</strong>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>
                                        <div class="form-group required" style="margin-top:20px;">
                                            <label for="inputEmail" class="control-label col-xs-4 required-field">Mobile : </label>
                                            <div class="col-xs-8">
                                                <input type="text" value="<?php echo $basic->mobile; ?>" name="param[basicInformation][]['mobile']" id="mobile" placeholder="Mobile"  class="form-control" maxlength="10" data-validation="required"  pattern="[789][0-9]{9}">
                                                <div class="alert-error" id="mobileError" style="display:none;padding-top:10px;color:red;font-size: 12px;">
                                                    <strong>Please enter Mobile No.</strong>
                                                </div>
                                            </div>
                                        </div>

                        <div class="clearfix"></div>

                        <div class="form-group" style="margin-top:20px;">
                            <label for="inputEmail" class="control-label col-xs-4">Gender : </label>
                            <div class="col-xs-8">
							<?php if($basic->gender == 1){ ?>
                                <select class="selectpicker form-control" name="param[basicInformation][]['gender']" id="gender" data-validation="required">
                                    <option value="1" selected="selected">Male</option>
                                    <option value="2">Female</option>
                                </select>
							<?php }else{ ?>
								<select class="selectpicker form-control" name="param[basicInformation][]['gender']" id="gender" data-validation="required">
                                    <option value="1" selected="selected">Male</option>
                                    <option value="2" selected="selected">Female</option>
                                </select>
							
							<?php }  ?>
                            </div>
                        </div>

                                        <div class="clearfix"></div>
                                        <div class="form-group" style="margin-top:20px;">
                                            <label for="inputEmail" class="control-label col-xs-4">Year Of Birth : </label>
                                            <div class="col-xs-8">
                                                <input type="text" value="<?php echo $basic->dob; ?>" name="param[basicInformation][]['dob']" id="datepicker-13"  placeholder="YYYY" class="form-control pick-date hasDatepicker">
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>
                                        <div class="form-group" style="margin-top:20px;">
                                            <label for="inputEmail" class="control-label col-xs-4">Pin Code : </label>
                                            <div class="col-xs-8">
                                                <input type="text" value="<?php echo $basic->pincode; ?>" name="param[basicInformation][]['pincode']" placeholder="Pincode"  id="pincode" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>


                                    <div class="form-group" style="margin-top:20px;">
                                        <input type="hidden" name="param[accessToken]" value="f1r$tj0b098">
                                        <input type="hidden" id="methodname" name="param[methodname]" value="updateDetailsOfJobsSeeker">
                                        <input type="hidden" id ="userId" name="param[userId]" value="<?php echo $token ?>">
                                        <input type="hidden" id="token" name="param[token]" value="ecbcd7eaee29848978134beeecdfbc7c">
                                        <button class="btn btn-lg btn-primary btn-block"  name="Submit" value="Save" type="button" onclick="saveBasicInfo()" style="    width: 15%;margin-left: 35%;">SAVE</button>             
                                    </div>
                                    <div style="float:right;">
                                        <span style="color:red;font-size:19px;">*</span><span style="font-size:12px">&nbsp;&nbsp;Required Fields.</span>
                                    </div>
                                </form>
                            </div>

                            <div id="menu2" class="tab-pane fade">
                                <div  style="margin: 14px; float: right; padding: 10px;">
                                    <span><button type="button" data-toggle="modal" data-target="#addEducationModal"><i class="fa fa-plus" aria-hidden="true"></i></button></span>
                                </div>
                                
                                <div class="">
                                    <table class="table">
                                        <thead>
                                          <tr>
                                            <th>Course</th>
                                            <th>University</th>
                                            <th>Institute</th>
                                            <th>Year Of Completion</th>
                                            <th>Marks Percentage</th>
                                            <th>Action</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach($educationalInfo as $educationDetail) { ?>
                                            <tr>
                                                <td><?php echo $educationDetail->courseName;?></td>
                                                <td><?php echo $educationDetail->universityName;?></td>
                                                <td><?php echo $educationDetail->institute;?></td>
                                                <td><?php echo $educationDetail->completionYear;?></td>
                                                <td><?php echo $educationDetail->percent.'%';?></td>
                                                <td>
                                                    <button type="button" title="Please click here to delete this education information." onclick="editQualification(<?php echo $educationDetail->id;?>)"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                                                    <button type="button" title="Please click here to delete this education information." onclick="deleteQualification(<?php echo $educationDetail->id;?>)"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                                </td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div id="menu3" class="tab-pane fade">
                                <div  style="margin: 14px; float: right; padding: 10px;">
                                    <span><button type="button" data-toggle="modal" data-target="#addEmploymentHistoryModal"><i class="fa fa-plus" aria-hidden="true"></i></button></span>
                                </div>
                                <div class="">
                                    <table class="table">
                                        <thead>
                                          <tr>
                                            <th>Company</th>
                                            <th>Role</th>
                                            <th>Responsibilities</th>
                                            <th>Period</th>
                                            <th>Action</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach($employmentInfo as $employmentDetail) { ?>
                                            <tr>
                                                <td><?php echo $employmentDetail->companyName;?></td>
                                                <td><?php echo $employmentDetail->roleOrPosition;?></td>
                                                <td><?php echo $employmentDetail->responsibilities;?></td>
                                                <td><?php echo $employmentDetail->dateFrom.' - ';?> 
                                                <?php echo !empty($employmentDetail->dateTo) ? $employmentDetail->dateTo : 'Present' ;?>
                                                </td>
                                                <td>
                                                    <button type="button" title="Please click here to edit this employment information." onclick="editEmployment(<?php echo $employmentDetail->id;?>)"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                                                    <button type="button" title="Please click here to delete this employment information." onclick="deleteEmployment(<?php echo $employmentDetail->id;?>)"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                                </td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div id="menu4" class="tab-pane fade">
                                <form role="form" method="post" id="otherInformationForm" style="margin-top: 24px;">
                                    <div class="row">
                                        <div class="col-md-4 col-md-offset-1">
                                            <label class="control-label">Location : </label>
                                        </div>
                                        <div class="col-md-6 col-md-offset-right-1">
                                            <?php
                                                $locationArray = array();
                                                foreach ($otherInfo->preferredLocation as $preferredLocationRow) {
                                                    $locationArray[] = $preferredLocationRow->name;
                                                }
                                            ?>
                                            <div class="form-group">
                                                <select data-placeholder="Enter the cities you want to work in" style="display: none;" multiple="" class="chosen-select location form-control" tabindex="-1" name="preferredLocation" id="preferredLocation" data-validation="alphanumeric required">
                                                    <option value=""></option>
                                                    <?php foreach ($city as $item) : ?>
                                                        <option value="<?php echo $item['city']; ?>" <?php echo in_array($item['city'], $locationArray) ? 'selected' :'' ;?>><?php echo $item['city']; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4 col-md-offset-1">
                                            <label class="control-label">Expected CTC(in lacs p.s) : </label>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <select class="selectpicker form-control" name="expectedCtcFrom" id="expectedCtcFrom" data-validation="required">
                                                    <option value="">CTC-From</option>
                                                    <?php for ($i = 0; $i <= 10; $i++) : ?>
                                                        <option value="<?php echo $i; ?>" <?php echo $i==$otherInfo->expectedCtcFrom ? 'selected' :'' ;?>><?php echo $i; ?></option>
                                                    <?php endfor; ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <select class="selectpicker form-control" name="expectedCtcTo" id="expectedCtcTo" data-validation="required">
                                                    <option value="">CTC-To</option>
                                                    <?php for ($t = 0; $t <= 9; $t++) : ?>
                                                        <option value="<?php echo $t; ?>" <?php echo $t==$otherInfo->expectedCtcTo ? 'selected' :'' ;?>><?php echo $t; ?></option>
                                                    <?php endfor; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4 col-md-offset-1">
                                            <label class="control-label">Work Emperience (Years) : </label>
                                        </div>
                                        <div class="col-md-6 col-md-offset-right-1">
                                            <div class="form-group">
                                                <select class="selectpicker form-control" name="workExperience" id="workExperience" data-validation="required">
                                                    <option value="">Select</option>
                                                    <?php for ($w = 0; $w <= 10; $w++) : ?>
                                                        <option value="<?php echo $w; ?>" <?php echo $w==$otherInfo->workExperience ? 'selected' :'' ;?>><?php echo $w; ?></option>
                                                    <?php endfor; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4 col-md-offset-1">
                                            <label class="control-label">Aadhar Card Number </label>
                                        </div>
                                        <div class="col-md-6 col-md-offset-right-1">
                                            <div class="form-group">
                                                <input type="text" class="form-control" value="<?php echo $otherInfo->adharCard; ?>" id="adharCard" name="adharCard" placeholder="Enter Aadhar Card Number">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4 col-md-offset-1">
                                            <label class="control-label">Languages </label>
                                        </div>
                                        <div class="col-md-6 col-md-offset-right-1">
                                            <div class="form-group">
                                                <input type="text" class="form-control" value="<?php echo $otherInfo->languages; ?>" id="languages" name="languages" placeholder="Enter your languages with comma separated">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <input type="hidden" name="token" id="token" value="ecbcd7eaee29848978134beeecdfbc7c">
                                        <input type="hidden" name="methodname" value="editOtherInfo">
                                        <input type="hidden" name="userId" value="<?php echo $token ?>">

                                        <div class="col-md-2 col-md-offset-5">
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-warning btn-block"><span class=""></span> SAVE </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <div id="menu5" class="tab-pane fade">
                                <form role="form" method="post" id="socialInformationForm" style="margin-top: 24px;">

                                    <div class="row">
                                        <div class="col-md-4 col-md-offset-1">
                                            <label class="control-label">Facebook : </label>
                                        </div>
                                        <div class="col-md-6 col-md-offset-right-1">
                                            <div class="form-group">
                                                <input type="text" class="form-control" value="<?php echo $socialInfo->facebookLink; ?>" id="facebookLink" name="facebookLink" placeholder="Plese enter your facebook link">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4 col-md-offset-1">
                                            <label class="control-label">LinkdIn : </label>
                                        </div>
                                        <div class="col-md-6 col-md-offset-right-1">
                                            <div class="form-group">
                                                <input type="text" class="form-control" value="<?php echo $socialInfo->linkedInLink; ?>" id="linkedInLink" name="linkedInLink" placeholder="Please enter your linkdin link">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4 col-md-offset-1">
                                            <label class="control-label">Twitter : </label>
                                        </div>
                                        <div class="col-md-6 col-md-offset-right-1">
                                            <div class="form-group">
                                                <input type="text" class="form-control" value="<?php echo $socialInfo->twitterLink; ?>" id="twitterLink" name="twitterLink" placeholder="Enter your twitter link">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4 col-md-offset-1">
                                            <label class="control-label">Googel Plus : </label>
                                        </div>
                                        <div class="col-md-6 col-md-offset-right-1">
                                            <div class="form-group">
                                                <input type="text" class="form-control" value="<?php echo $socialInfo->googlePlusLink; ?>" id="googlePlusLink" name="googlePlusLink" placeholder="Enter your google plus link">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <input type="hidden" name="token" id="token" value="ecbcd7eaee29848978134beeecdfbc7c">
                                        <input type="hidden" name="methodname" value="editSocialInfo">
                                        <input type="hidden" name="userId" value="<?php echo $token ?>">

                                        <div class="col-md-2 col-md-offset-5">
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-warning btn-block"><span class=""></span> SAVE </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            
        </div>
    </div>
</div>
<!-- /#wrapper -->

<!-- edd education Modal -->
<div class="modal fade" id="addEducationModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content" style="background-color: #999 !important;">
        <div class="modal-header" style="padding:15px 50px;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="text-center"><span></span> Save Qualification</h4>
        </div>
        <div class="modal-body" style="padding:40px 50px;">
          <form role="form" id="addQualificationFrom" method="post">
            <div class="form-group">
                
                <select class="form-control selectpicker" id="courseId" name="courseId">
                    <option value="">Select Course Type*</option>
                    <?php foreach($courseData as $courseRow) { ?>
                    <option value="<?php echo $courseRow->id;?>"><?php echo $courseRow->name;?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <select data-placeholder="University*" style="display: none;" class="chosen-select form-control" tabindex="-1" name="universityId" id="universityId" data-validation="alphanumeric required">
                    <option value=""></option>
                    <?php foreach ($universityData as $universityRow) : ?>
                        <option value="<?php echo $universityRow->id; ?>"><?php echo $universityRow->name; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
              <input type="text" class="form-control" id="institute" name="institute" placeholder="Institute*">
            </div>
            <div class="form-group">
              <input type="text" class="form-control" id="completionYear" name="completionYear" placeholder="Year of Completion(e.g 2014)*">
            </div>
            <div class="form-group">
              <input type="text" class="form-control" id="percent" name="percent" placeholder="%(percentage e.g 58.48)*">
            </div>

            <input type="hidden" name="token" id="token" value="ecbcd7eaee29848978134beeecdfbc7c">
            <input type="hidden" name="methodname" value="editEducationalInfo">
            <input type="hidden" name="userId" value="<?php echo $token ?>">
            <input type="hidden" name="id" value="" id="educationId">

            <button type="submit" class="btn btn-warning btn-block"><span class=""></span> SAVE</button>
          </form>
        </div>
      </div>
    </div>
</div>
<!---add education modal end-->

<!-- edd education Modal -->
  <div class="modal fade" id="addEmploymentHistoryModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content" style="background-color: #999 !important;">
        <div class="modal-header" style="padding:15px 50px;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="text-center"><span></span> Save Employment</h4>
        </div>
        <div class="modal-body" style="padding:40px 50px;">
          <form role="form" id="addEmploymentFrom" method="post">
            <div class="form-group">
                <input type="text" class="form-control" id="companyName" name="companyName" placeholder="Company*">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" id="roleOrPosition" name="roleOrPosition" placeholder="Role/Position*">
            </div>
            
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        <input type="text" class="form-control" id="dateFrom" name="dateFrom" placeholder="From*">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <input type="text" class="form-control" id="dateTo" name="dateTo" placeholder="To">
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="form-group">
                        <label style="margin-top: 4px;" class="checkbox-inline"><input id="isPresent" name="isPresent" type="checkbox" value="1">Present</label>
                    </div>
                </div>
            </div>


            <div class="form-group">
              <textarea class="form-control" rows="3" id="responsibilities" name="responsibilities" placeholder="Responsibilities*"></textarea>
            </div>

            <input type="hidden" name="token" id="token" value="ecbcd7eaee29848978134beeecdfbc7c">
            <input type="hidden" name="methodname" value="editEmploymentInfo">
            <input type="hidden" name="userId" value="<?php echo $token ?>">
            <input type="hidden" name="id" value="" id="employmentId">

            <button type="submit" class="btn btn-warning btn-block"><span class=""></span>SAVE</button>
          </form>
        </div>
      </div>
    </div>
  </div>
<!---add education modal end-->

<script src="<?php echo base_url(); ?>theme/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url(); ?>theme/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>theme/js/bootbox.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->

    <!-- Custom Theme JavaScript -->
<script src="<?php echo base_url(); ?>theme/js/formValidation.min.js"></script>
<script src="<?php echo base_url(); ?>theme/js/bootstrap.min.js"></script>
<!-- Basic profile js JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url(); ?>js/min/editbasicprofile.min.js"></script>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Users Controller
 * Description : Used to handle all the jobseeker related data
 * @author Synergy
 * @createddate : April 3, 2016
 * @modificationlog : Initializing the controlelr
 * @change on Mar 24, 2017
 */
 
class Users extends MY_Controller {

    public $allNotificationList = array();
    /**
     * Responsable for inherit the parent connstructor
     * @return void
     */
    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('fj');
		$this->load->helper('jwt');
		
		$this->getEditProfileData();
		//Use to get Locations for search Job
        $this->searchLocationList = getLocationCities();
		
		
        $userRealData = $this->session->get_userdata();
        //print_r($userRealData);exit;

        if (isset($userRealData['userId'])) {
            /* Get Notification List for user */
            $userId = $userRealData['userId'];

            $statictoken = 'ecbcd7eaee29848978134beeecdfbc7c';
			if($this->config->item('HTTPSENABLED') == 1){
				$base_url = 'https://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
			}else{
				$base_url = 'http://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
			}
            
            $json = array("token" => $statictoken, "methodname" => 'getUserNotifications', 'userId' => $userId);                                                                    
            $data_string = json_encode($json);                                                                         
                                                                                                                                 
            $ch = curl_init($base_url);                                                                      
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
                'Content-Type: application/json',                                                                                
                'Content-Length: ' . strlen($data_string))                                                                       
            );                                                                                                                   
                                                                                                                                 
            $result = curl_exec($ch);
            $allResult = json_decode($result);
            $this->allNotificationList = $allResult->data;

            /* End of get notification list fo user */
        }
    }

    /**
     * Function Naem : setSessionfromApp
     * Discription : Used for mobile view in mobile App. Not used in jobseeker website
     * @author Synergy
     * @param none
     * @return render data into main view page
     */
    function  setSessionfromApp(){
		$this->load->model('coreapi_model','CORE_MODEL');
		
        $value = json_decode(file_get_contents('php://input'));
        $this->load->library('session');
        $session_data = array(
		    'userEmail' => 'sunil.rai@gmail.com', //isset($value->userEmail)?$value->userEmail:""
		    'jobCode' => 'FGLI231879', //$value->jobCode
		    'token' => "ecbcd7eaee29848978134beeecdfbc7c",
		);
        $statictoken = 'ecbcd7eaee29848978134beeecdfbc7c';
        $base_url = 'http://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
		
		//registrating the user using the email 
		$json = array("token" => $statictoken, "methodname" => 'virtualSignUp', 'email' => 'sunil.rai@gmail.com');                                                                    
        $data_string = json_encode($json);                
		
        $ch = curl_init($base_url);                                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data_string))                                                                       
        );                                                                                                                  
                                                                                                                             
        $result1 = curl_exec($ch);
		curl_close ($ch);
        $allResult1 = json_decode($result1);
		$_SESSION['userId'] = $allResult1->token;
		
		$statictoken = 'ecbcd7eaee29848978134beeecdfbc7c';
        $base_url = 'http://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';

        $this->load->library('session');
        $this->load->helper('cookie');
        $jobCode = 'FGLI231879';//$this->uri->segment('3');
        $session = $this->session->get_userdata();
		$session = $this->session->get_userdata();
		
        $data = array("token" => $statictoken, "methodname" => 'verifyJob', 'referrenceJobId' => $jobCode, 'searchBy' => 'job','deviceUniqueId' => '1111');                                                                    
        $data_string = json_encode($data);                                                                                   
                                                                                                                             
        $ch = curl_init($base_url);                                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data_string))                                                                       
        );                                                                                                                  
                                                                                                                             
        $result = curl_exec($ch);
        $allResult = json_decode($result);
        $allContent = $allResult->data;
		
		//check job expire
		$checkAppliedJobjson = array("token" => $statictoken, "methodname" => 'checkAppliedJob', 'userId' => $_SESSION['userId'], 'jobCode' => $jobCode);                                                                    
        $data_string = json_encode($checkAppliedJobjson);                
		
        $ch = curl_init($base_url);                                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data_string))                                                                       
        );                                                                                                                  
                                                                                                                             
        $result = curl_exec($ch);
		
		curl_close ($ch);
		$allResult = json_decode($result);
        $data['checkAppliedJob'] = $allResult->code;
		//check save Jobs
		$checkAppliedJobjson = array("token" => $statictoken, "methodname" => 'getSavedJobStatus', 'userId' => $_SESSION['userId'], 'jobCode' => $jobCode);                                                                    
        $data_string = json_encode($checkAppliedJobjson);                
		
        $ch = curl_init($base_url);                                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data_string))                                                                       
        );                                                                                                                  
                                                                                                                             
        $result = curl_exec($ch);
		
		curl_close ($ch);
		$allResult = json_decode($result);
        $data['checkSavedJobs'] = $allResult->code;
		
		
		$_SESSION['jobCode'] = $jobCode;
		
        $data['content'] = $allContent[0];
        $data['token'] = $userId;
        $data['userName'] = $userName;
        $data['email'] = $userEmail;
        $data['invitationId'] = $invitationId;
        $data['main_content'] = 'web_view_job_details';
        $this->load->view('fj-mainpage', $data);
    }
	
	/**
     * Function Name : index
     * Discription : Use to display dashboard of the user
     * @author Synergy
     * @param none
     * @return render data into main view page
     */
	function index() {
		$data['coreApi'] = $this->load->model('coreapi_model','CORE_MODEL');
        $this->load->helper('cookie');
        //$this->load->library('session');
        $_SESSION['invitationId'] = "";
        $session = $this->session->get_userdata();
        $token =  $session['userId'];
        $userEmail = isset($session['userEmail'])?$session['userEmail']:"";
        $userName = isset($session['userName'])?$session['userName']:"";
		
        $userId = isset($session['userId'])?$session['userId']:"";

        $statictoken = 'ecbcd7eaee29848978134beeecdfbc7c';
		if($this->config->item('HTTPSENABLED') == 1){
			$base_url = 'https://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
		}else{
			$base_url = 'http://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
		}
        
		$json = array("token" => $statictoken, "methodname" => 'checkProfile', 'email' => $userEmail);           
		                                                   
        $data_string = json_encode($json);                                                                            
                                                                                                                                
        $ch = curl_init($base_url);                                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data_string))                                                                       
        );                                                                                                                   
                                                                                                                             
        $result = curl_exec($ch);
		$allResult = json_decode($result);
		if($allResult->code == 200){
			redirect("https://".$_SERVER["HTTP_HOST"]."/jobseeker/complete-profile",'refresh');	
		}
		
        //Get jobseeker event
        $json = array("token" => $statictoken, "methodname" => 'getJobSeekerEvent', 'userId' => $userId);                                                                    
        $data_string = json_encode($json);                                                                                   
                                                                                                                              
        $ch = curl_init($base_url);                                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);		
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data_string))                                                                       
        );                                                                                                                   
                                                                                                                             
        $result = curl_exec($ch);
        $allResult = json_decode($result);
        $allContent = $allResult->eventList;
        $data['content'] = $allContent[0];
        //End of job seeker event

        //Start of getting job invitations
        $json_inv = array("token" => $statictoken, "methodname" => 'getInvites', 'userId' => $userId, 'email' => $userEmail);                                                                    
        $data_string_inv = json_encode($json_inv);                                                                                   
                                                                                                                             
        $ch_inv = curl_init($base_url);                                                                      
        curl_setopt($ch_inv, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch_inv, CURLOPT_POSTFIELDS, $data_string_inv);                                                                  
        curl_setopt($ch_inv, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch_inv, CURLOPT_SSL_VERIFYPEER, false);			
        curl_setopt($ch_inv, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data_string_inv))                                                                       
        );                                                                                                                   
                                                                                                                             
        $result_inv = curl_exec($ch_inv);
        $all_result_inv = json_decode($result_inv);
		if($all_result_inv->code == 200){
			$allContent_inv = $all_result_inv->data;
		}else{
			$allContent_inv = array();
		}
        
        $data['jobInvitations'] = $allContent_inv;
        //End of getting job invitations

        //Start of Recent Messages
        $json_message = array("token" => $statictoken, "methodname" => 'getMessageList', 'userId' => $userId);                                                                    
        $data_string_message = json_encode($json_message);                                                                                                                                                                    //echo $data_string_message;die;
        $ch_message = curl_init($base_url);                                                                      
        curl_setopt($ch_message, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch_message, CURLOPT_POSTFIELDS, $data_string_message);                                                    
        curl_setopt($ch_message, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch_message, CURLOPT_SSL_VERIFYPEER, false);		
        curl_setopt($ch_message, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data_string_message))                                                                       
        );                                                                                                                   
                                                                                                                             
        $result_message = curl_exec($ch_message);
		//$curl_errno= curl_errno($ch_message);
		//echo 'Request Error:' . curl_error($ch_message);die;
		
        $all_result_message = json_decode($result_message);
        $data['messages'] = $all_result_message->data;
		
        //End of Recent Messages

        //Start of applied jobs
        $json_appliedjobs = array("token" => $statictoken, "methodname" => 'getAppliedJob', 'userId' => $userId);                                                                    
        $data_string_appliedjobs = json_encode($json_appliedjobs);                                                                                                                                                                    
        $ch_appliedjobs = curl_init($base_url);                                                                      
        curl_setopt($ch_appliedjobs, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch_appliedjobs, CURLOPT_POSTFIELDS, $data_string_appliedjobs);
        curl_setopt($ch_appliedjobs, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch_appliedjobs, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch_appliedjobs, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data_string_appliedjobs))
        );                                                                                                                   
                                                                                                                             
        $result_appliedjobs = curl_exec($ch_appliedjobs);
		
        $all_result_appliedjobs = json_decode($result_appliedjobs);
        $data['appliedJobs'] = $all_result_appliedjobs->data;
        //print_r($data['appliedJobs']);exit;

        //End of applied jobs
		
		//profilePercent
		$json_ProfilePercent = array("token" => $statictoken, "methodname" => 'getProfilePercent', 'userId' => $userId);                                                                    
        $data_string_ProfilePercent = json_encode($json_ProfilePercent);                                                                                                                                                                    
        $ch_ProfilePercent = curl_init($base_url);                                                                      
        curl_setopt($ch_ProfilePercent, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch_ProfilePercent, CURLOPT_POSTFIELDS, $data_string_ProfilePercent);
        curl_setopt($ch_ProfilePercent, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch_ProfilePercent, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch_ProfilePercent, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data_string_ProfilePercent))
        );                                                                                                                   
                                                                                                                             
        $result_ProfilePercent = curl_exec($ch_ProfilePercent);
        $all_result_ProfilePercent = json_decode($result_ProfilePercent);
		$data['profile_Percent'] = $all_result_ProfilePercent->profilePercent;
        //Start of saved jobs
        $json_savedjobs = array("token" => $statictoken, "methodname" => 'getSavedJob', 'userId' => $userId);                                                                    
        $data_string_savedjobs = json_encode($json_savedjobs);                                                                                                                                                                    
        $ch_savedjobs = curl_init($base_url);                                                                      
        curl_setopt($ch_savedjobs, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch_savedjobs, CURLOPT_POSTFIELDS, $data_string_savedjobs);
        curl_setopt($ch_savedjobs, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch_savedjobs, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch_savedjobs, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data_string_savedjobs))
        );                                                                                                                   
                                                                                                                             
        $result_savedjobs = curl_exec($ch_savedjobs);
        $all_result_savedjobs = json_decode($result_savedjobs);
        //print_r($all_result_savedjobs);exit;
		if($all_result_savedjobs->code == 200){
			$data['savedJobs'] = $all_result_savedjobs->data;
		}else{
			$data['savedJobs'] = array();
		}
        //End of saved jobs
		
		//get basic profile
		$dataBasic = array("token" => $statictoken, "methodname" => 'getBasicInformation_web', 'userId' => $userId);                                                                    
        $data_string = json_encode($dataBasic);                                                                                   
                                                                                                                             
        $ch = curl_init($base_url);                                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data_string))                                                                       
        );                                                                                                                   
        
		
        $result = curl_exec($ch);
        $allResult = json_decode($result);
		$basic = $allResult->basicInfo;
		$data['basic'] = $basic[0];
		
		$_SESSION['userName'] = $data['basic']->fullname;
        $_SESSION['userImage'] = $data['basic']->image;
		
		
       
        if(isset($session['userId']) && $session['userId']!= ""){
            $data['token'] = $userId;
            $data['email'] = $userEmail;
            $data['userName'] = $userName;
			$data['city'] = $this->searchLocationList;
			
            $data['allNotificationList'] = $this->allNotificationList;
            $data['main_content'] = 'index';
            $this->load->view('fj-mainpage-withMenu', $data);
        }else{ 
            redirect("http://".$_SERVER["HTTP_HOST"]."",'refresh');
        }
        
    }
    
	/**
     * Function Name : setSession
     * Discription : Use to set session of the jobseeker when logged In
     * @author Synergy
     * @param none
     * @return render data into main view page
     */
    function  setSession(){
		
        $value = json_decode(file_get_contents('php://input'));
        $this->load->library('session');
        $session_data = array(
		    'userEmail' => isset($value->userEmail)?$value->userEmail:"",
		    'userId' => $value->token,
		    'userName' => $value->userName,
		    'token' => "ecbcd7eaee29848978134beeecdfbc7c",
		);
        
        $this->session->sess_expiration = '14400';// expires in 4 hours
        $this->session->set_userdata($session_data);
        echo "success";die;
    }
    
	/**
     * Function Name : setSessionSignUp
     * Discription : Use to set session of the jobseeker when signup
     * @author Synergy
     * @param none
     * @return render data into main view page
     */
    function setSessionSignUp(){
        $value = json_decode(file_get_contents('php://input'));
        
        $this->load->library('session');
        $data = $value->data;
        $basic = $data[0];
        
        $session_data = array(
		    'userEmail' => isset($basic->email)?$basic->email:"",
		    'userId' => $basic->token,
		    'userName' => $basic->fullname,
		    'token' => "ecbcd7eaee29848978134beeecdfbc7c",
		);
        
        $this->session->sess_expiration = '14400';// expires in 4 hours
        $this->session->set_userdata($session_data);
		
		//send message to the user
		$statictoken = 'ecbcd7eaee29848978134beeecdfbc7c';
        if($this->config->item('HTTPSENABLED') == 1){
			$base_url = 'https://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
		}else{
			$base_url = 'http://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
		}
		
		$messageText = "Dear ".$basic->fullname.",<br><br>  

Welcome to the VDOHire family. You have now joined thousands of jobseekers who have used VDOHire to find and interview for their dream job from the comfort of their home. At VDOHire we are committed to helping jobseekers by making the interview process convenient and comfortable. Now you can apply for jobs and submit your interviews, at Anytime, from Anywhere.<br><br>

To keep yourself updated with the latest jobs and career news we recommend that you download the VDOHire Android App. You can download the App from the Google playstore by clicking <a href='https://goo.gl/Le9H7b' target='_blank'>here</a>.<br><br>

Now that you have registered, we encourage you to complete your Profile, upload a recent Resume and start submitting interviews for open opportunities on the platform.<br><br>

If you face any problem while using any of our services, – do reach out to us at <b>help@VDOHire.com</b><br><br>

Wishing you luck in your job search!<br>

Team VDOHire<br>
Get a Job<br>
Anytime, Anywhere
";
		$subject = "Welcome to VDOHire";
		$parentMessageId = ""; 
		$adminSynergy = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpZCI6MzIzfQ.hMyoB8qNuk1yo7DR3jcN1NAsW2YVnwFuFtsTkRqIc9FilYzITzmL022Mxa8-45_wg2fNV12TUOVnFbM1-WiOxQ';
		$jsonMessage = array("token" => $statictoken, "methodname" => 'sendMessageToUsers', 'toId' => $_SESSION['userId'], "messageText" => $messageText, "messageSubject" => $subject, 'parentMessageId' => $parentMessageId, 'userId' =>  $adminSynergy);                                                                    
        $data_string = json_encode($jsonMessage);                
		
        $ch = curl_init($base_url);                                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data_string))                                                                       
        );                                                                                                                  
                                                                                                                             
        $result = curl_exec($ch);
		curl_close ($ch);
		
		echo "success";die;
    }
    
	/**
     * Function Name : LogOut
     * Discription : Use to logout the jobseeker from web portal
     * @author Synergy
     * @param none
     * @return render data into main view page
     */
	
    function LogOut(){
        $this->load->library('session');
        $this->load->helper('cookie');
        session_unset();
        unset($_SESSION['FBID']);
            //echo $_SESSION['FBID'];die;
        session_destroy();
        delete_cookie('token');
        redirect("http://".$_SERVER["HTTP_HOST"]."",'refresh');
    }
	
	/**
     * Function Name : showBasicProfile
     * Discription : Use to show basic profile of the jobseeker
     * @author Synergy
     * @param none
     * @return render data into main view page
     */
	 
	function showBasicProfile() {
        $statictoken = 'ecbcd7eaee29848978134beeecdfbc7c';
        if($this->config->item('HTTPSENABLED') == 1){
			$base_url = 'https://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
		}else{
			$base_url = 'http://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
		}

        //$this->load->library('session');
        $this->load->helper('cookie');
        $eventId = $this->uri->segment('3');
        $session = $this->session->get_userdata();
        $_SESSION['invitationId'] = $invitationId;
        $token =  $session['token'];
        $userEmail =  $session['userEmail'];
        $userName =  $session['userName'];
        $userId =  $session['userId'];

        $data = array("token" => $statictoken, "methodname" => 'getBasicInformation', 'userId' => $userId);                                                                    
        $data_string = json_encode($data);                                                                                   
                                                                                                                             
        $ch = curl_init($base_url);                                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data_string))                                                                       
        );                                                                                                                   
                                                                                                                             
        $result = curl_exec($ch);
        $allResult = json_decode($result);
		$basic = $allResult->basicInfo;
		$data['basic'] = $basic[0];
        
        if(isset($token) && $token!= ""){
            $data['token'] = $userId;
            $data['userName'] = $userName;
            $data['email'] = $userEmail;
            $data['allNotificationList'] = $this->allNotificationList;
            $data['main_content'] = 'basicprofile';
            $this->load->view('fj-mainpage', $data);
        }else{ 
            redirect("http://".$_SERVER["HTTP_HOST"]."",'refresh');
        }
        
    }
	
	/**
     * Function Name : editBasicProfile
     * Discription : To edit the basic profile of the job seeker
     * @author Synergy
     * @param none
     * @return render data into main view page
     */
	 
	function editBasicProfile() {
        $statictoken = 'ecbcd7eaee29848978134beeecdfbc7c';
        if($this->config->item('HTTPSENABLED') == 1){
			$base_url = 'https://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
		}else{
			$base_url = 'http://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
		}

        //$this->load->library('session');
        $session = $this->session->get_userdata();
        $_SESSION['invitationId'] = $invitationId;
		
        $token =  $session['userId'];
        $userEmail = isset($session['userEmail'])?$session['userEmail']:"";
        $userName = isset($session['userName'])?$session['userName']:"";
        $userId = isset($session['userId'])?$session['userId']:"";
        
        //Get Basic Information
        $data = array("token" => $statictoken, "methodname" => 'getBasicInformation', 'userId' => $userId);
        $data_string = json_encode($data);                                             
        $ch = curl_init($base_url);                                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data_string))                                                                       
        );

        $result = curl_exec($ch);
        $allResult = json_decode($result);
		$basic = $allResult->basicInfo;
		$data['basic'] = $basic[0];
        //End of Get Basic Information


        //Get Educational Information
        $educationDdata = array("token" => $statictoken, "methodname" => 'getEducationalInformation', 'userId' => $userId);
        $education_data_string = json_encode($educationDdata);                                             
        $education_ch = curl_init($base_url);                                                                      
        curl_setopt($education_ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($education_ch, CURLOPT_POSTFIELDS, $education_data_string);                                                                  
        curl_setopt($education_ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($education_ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($education_ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($education_data_string))                                                                       
        );

        $education_result = curl_exec($education_ch);
        $educationResultObject = json_decode($education_result);
        $educationalInfo = $educationResultObject->educationalInfo;
        $data['educationalInfo'] = $educationalInfo;
        //End of Get Educational Information

        //Get Employment Hisotry
        $employmentData = array("token" => $statictoken, "methodname" => 'getEmploymentInformation', 'userId' => $userId);
        $employment_data_string = json_encode($employmentData);                                             
        $employment_ch = curl_init($base_url);                                                                      
        curl_setopt($employment_ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($employment_ch, CURLOPT_POSTFIELDS, $employment_data_string);                                                                  
        curl_setopt($employment_ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($employment_ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($employment_ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($employment_data_string))                                                                       
        );

        $employment_result = curl_exec($employment_ch);
        $employmentResultObject = json_decode($employment_result);
        $employmentInfo = $employmentResultObject->employmentInfo;
        $data['employmentInfo'] = $employmentInfo;
        //print_r($data['employmentInfo']);exit;
        //End of Get Employment History

        //Get Course List
        $courseData = array("token" => $statictoken, "methodname" => 'getCourses');
        $course_data_string = json_encode($courseData);                                             
        $course_ch = curl_init($base_url);                                                                      
        curl_setopt($course_ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($course_ch, CURLOPT_POSTFIELDS, $course_data_string);                                                                  
        curl_setopt($course_ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($course_ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($course_ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($course_data_string))                                                                       
        );

        $course_result = curl_exec($course_ch);
        $courseResultObject = json_decode($course_result);
        $courseData = $courseResultObject->data;
        $data['courseData'] = $courseData;
        //End of Get Course List

        //Get Course List
        $universityData = array("token" => $statictoken, "methodname" => 'getUniversityList');
        $university_data_string = json_encode($universityData);                                             
        $university_ch = curl_init($base_url);                                                                      
        curl_setopt($university_ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($university_ch, CURLOPT_POSTFIELDS, $university_data_string);                                                                  
        curl_setopt($university_ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($university_ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($university_ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($university_data_string))                                                                       
        );

        $university_result = curl_exec($university_ch);
        $universityResultObject = json_decode($university_result);
        $universityData = $universityResultObject->data;
        $data['universityData'] = $universityData;
        //End of Get Course List


        //Get other information data
        $otherInfoData = array("token" => $statictoken, "methodname" => 'getOtherInformation', 'userId' => $userId);
        $otherinfo_data_string = json_encode($otherInfoData);                                             
        $otherinfo_ch = curl_init($base_url);                                                                      
        curl_setopt($otherinfo_ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($otherinfo_ch, CURLOPT_POSTFIELDS, $otherinfo_data_string);                                                                  
        curl_setopt($otherinfo_ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($otherinfo_ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($otherinfo_ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($otherinfo_data_string))                                                                       
        );

        $otehrinfo_result = curl_exec($otherinfo_ch);
        $otherInfoResultObject = json_decode($otehrinfo_result);
        $otherInfo = $otherInfoResultObject->otherInfo[0];
        $data['otherInfo'] = $otherInfo;
        //End of Get other information data

        //Get social information data
        $socialInfoData = array("token" => $statictoken, "methodname" => 'getSocialInformation', 'userId' => $userId);
        $social_data_string = json_encode($socialInfoData);                                             
        $social_ch = curl_init($base_url);                                                                      
        curl_setopt($social_ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($social_ch, CURLOPT_POSTFIELDS, $social_data_string);                                                                  
        curl_setopt($social_ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($social_ch, CURLOPT_SSL_VERIFYPEER, false);		
        curl_setopt($social_ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($social_data_string))                                                                       
        );

        $social_result = curl_exec($social_ch);
        $socialResultObject = json_decode($social_result);
        $socialInfo = $socialResultObject->socialInfo[0];
        $data['socialInfo'] = $socialInfo;
        //End of Get social information data

		
		$_SESSION['userName'] = $data['basic']->fullname;
        $_SESSION['userImage'] = $data['basic']->image;
		
        if(isset($session['userId']) && $session['userId']!= ""){
            $data['token'] = $userId;
            $data['userName'] = $_SESSION['userName'];
            $data['email'] = $userEmail;
            $data['city'] = $this->searchLocationList;
            $data['allNotificationList'] = $this->allNotificationList;
            $data['main_content'] = 'editbasicprofile';
            $this->load->view('fj-mainpage-withMenu', $data);
        }else{ 
            redirect("http://".$_SERVER["HTTP_HOST"]."",'refresh');
        }
    }

	
	/**
     * Function Name : resume
     * Discription : To show the manual upload resume page in edit profile
     * @author Synergy
     * @param none
     * @return render data into main view page
     */
	 
	function resume() {
        $statictoken = 'ecbcd7eaee29848978134beeecdfbc7c';
        if($this->config->item('HTTPSENABLED') == 1){
			$base_url = 'https://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
		}else{
			$base_url = 'http://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
		}

        //$this->load->library('session');

        $session = $this->session->get_userdata();
        
        if(isset($session['userId']) && $session['userId']!= ""){
            $userEmail =  $session['userEmail'];
            $userName =  $session['userName'];
            $userId =  $session['userId'];

            $data = array("token" => $statictoken, "methodname" => 'getUserResumes', 'userId' => $userId);                                                                    
            $data_string = json_encode($data);                                                                                   
                                                                                                                                 
            $ch = curl_init($base_url);                                                                      
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
                'Content-Type: application/json',                                                                                
                'Content-Length: ' . strlen($data_string))                                                                       
            );                                                                                                                   
                                                                                                                                 
            $result = curl_exec($ch);
            $allResult = json_decode($result);
            $resumeList = $allResult->resumeList;
            $data['resumelist'] = $resumeList;


            $data['token'] = $userId;
            $data['userName'] = $userName;
            $data['email'] = $userEmail;
            $data['allNotificationList'] = $this->allNotificationList;
            $data['main_content'] = 'resume';
            $this->load->view('fj-mainpage-withMenu', $data);
        }else{ 
            redirect("http://".$_SERVER["HTTP_HOST"]."",'refresh');
        }
    }
	
	
	/**
     * Function Name : editProfile
     * Discription : To show the edit profile page
     * @author Synergy
     * @param none
     * @return render data into main view page
     */
	 
    function editProfile() {
        $statictoken = 'ecbcd7eaee29848978134beeecdfbc7c';
        
        //$this->load->library('session');
        $session = $this->session->get_userdata();
        
        if(isset($session)){
            $data['token'] = $session['userId'];
            $data['userName'] = $session['userName'];
            $data['email'] = $session['userEmail'];
            $data['allNotificationList'] = $this->allNotificationList;
            $data['main_content'] = 'edit_profile';
            $this->load->view('fj-mainpage', $data);
			
        }else{ 
            redirect("http://".$_SERVER["HTTP_HOST"]."",'refresh');
        }
    }
	
	/**
     * Function Name : completeProfile
     * Discription : To ask user to complete his basic profile. This function is used when user directly comes from the invitation link to the job portal.
     * @author Synergy
     * @param none
     * @return render data into main view page
     */
	 
	function completeProfile() {
		$session = $this->session->get_userdata();
        $statictoken = 'ecbcd7eaee29848978134beeecdfbc7c';
        if($this->config->item('HTTPSENABLED') == 1){
			$base_url = 'https://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
		}else{
			$base_url = 'http://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
		}

        //$this->load->library('session');
        

        $data = array("token" => $statictoken, "methodname" => 'getBasicInformation', 'userId' => $session['userId']);                                                                    
        $data_string = json_encode($data);                                                                                   
                                                                                                                             
        $ch = curl_init($base_url);                                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data_string))                                                                       
        );                                                                                                                   
                                                                                                                             
        $result = curl_exec($ch);
        $allResult = json_decode($result);
		
		$basic = $allResult->basicInfo;
		$data['basic'] = $basic[0];
        
        if(isset($session['userId']) && $session['userId']!= ""){
            $data['token'] = $session['userId'];
            $data['userName'] = $session['userName'];
            $data['email'] = $session['userEmail'];
            $data['allNotificationList'] = $this->allNotificationList;
            $data['main_content'] = 'complete_profile';
            $this->load->view('fj-mainpage', $data);
        }else{ 
            redirect("http://".$_SERVER["HTTP_HOST"]."",'refresh');
        }
    }
	
	/**
     * Function Name : send_document_email
     * Discription : To show the send resume by email page.
     * @author Synergy
     * @param none
     * @return render data into main view page
     */
	 
    function send_document_email() {
        $statictoken = 'ecbcd7eaee29848978134beeecdfbc7c';
        
        //$this->load->library('session');
        $session = $this->session->get_userdata();
        
        if(isset($session['userId']) && $session['userId']!= ""){
            $data['token'] = $session['userId'];
            $data['userName'] = $session['userName'];
            $data['email'] = $session['userEmail'];
            $data['allNotificationList'] = $this->allNotificationList;
            $data['main_content'] = 'send_resume_document';
            $this->load->view('fj-mainpage-withMenu', $data);
        }else{ 
            redirect("http://".$_SERVER["HTTP_HOST"]."",'refresh');
        }
    }
	
	
	/**
     * Function Name : showAllMessages
     * Discription : To show all the messages which user has recived from admin
     * @author Synergy
     * @param none
     * @return render data into main view page
     */
	 
	function showAllMessages(){
		$statictoken = 'ecbcd7eaee29848978134beeecdfbc7c';
        if($this->config->item('HTTPSENABLED') == 1){
			$base_url = 'https://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
		}else{
			$base_url = 'http://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
		}
        //$this->load->library('session');
        $session = $this->session->get_userdata();
		
        if(isset($session['userId']) && $session['userId']!= ""){
            $userEmail =  $session['userEmail'];
            $userName =  $session['userName'];
            $userId =  $session['userId'];
			
        //Start of Recent Messages
			$json_message = array("token" => $statictoken, "methodname" => 'getMessageListWithPagination', 'userId' => $userId, 'limit' => 50, 'counter'=> 0);                                                                    
			$data_string_message = json_encode($json_message);                                                                                                                                                                    
			$ch_message = curl_init($base_url);                                                                      
			curl_setopt($ch_message, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
			curl_setopt($ch_message, CURLOPT_POSTFIELDS, $data_string_message);                                                    
			curl_setopt($ch_message, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch_message, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch_message, CURLOPT_HTTPHEADER, array(                                                                          
				'Content-Type: application/json',                                                                                
				'Content-Length: ' . strlen($data_string_message))                                                                       
			);                                                                                                                   
																																 
			$result_message = curl_exec($ch_message);
			$all_result_message = json_decode($result_message);
			$allContent = $all_result_message->data;
			
			$data['totalMessages'] = $all_result_message->message;
			$data['content'] = $allContent;
			$data['token'] = $userId;
			$data['userName'] = $userName;
			$data['email'] = $userEmail;
			//$data['invitationId'] = $invitationId;
            $data['allNotificationList'] = $this->allNotificationList;
			$data['main_content'] = 'all-message';
			$this->load->view('fj-mainpage-withMenu', $data);
		}else{ 
            redirect("http://".$_SERVER["HTTP_HOST"]."",'refresh');
        }
	}
	
	/**
	 * Function Name : getMessageNext
	 * Discription : To get the next 50 messages using API. This function does not have any view.
	 * @author Synergy
	 * @param none
	 * @return render data into main view page
	 */
	 
	function getMessageNext(){
		$page = $this->input->post('page');
		$statictoken = 'ecbcd7eaee29848978134beeecdfbc7c';
        if($this->config->item('HTTPSENABLED') == 1){
			$base_url = 'https://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
		}else{
			$base_url = 'http://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
		}
        //$this->load->library('session');
        $session = $this->session->get_userdata();
		
        if(isset($session['userId']) && $session['userId']!= ""){
            $userEmail =  $session['userEmail'];
            $userName =  $session['userName'];
            $userId =  $session['userId'];
			
		
			//Start of Recent Messages
			$json_message = array("token" => $statictoken, "methodname" => 'getMessageListWithPagination', 'userId' => $userId, 'limit' => 50, 'counter'=> $page);                                                                    
			$data_string_message = json_encode($json_message);                                                                                                                                                                    
			$ch_message = curl_init($base_url);                                                                      
			curl_setopt($ch_message, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
			curl_setopt($ch_message, CURLOPT_POSTFIELDS, $data_string_message);                                                    
			curl_setopt($ch_message, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch_message, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch_message, CURLOPT_HTTPHEADER, array(                                                                          
				'Content-Type: application/json',                                                                                
				'Content-Length: ' . strlen($data_string_message))                                                                       
			);                                                                                                                   
																																 
			$result_message = curl_exec($ch_message);
			$all_result_message = json_decode($result_message);
			$allContent = $all_result_message->data;
			$MessageContent = "";
			
			foreach($allContent as $row){
				$MessageContent .= "<tr><td>".$row->messageId.$row->senderName."</td>";
				$MessageContent .= "<td>".$row->messageSubject."</td>";
				$MessageContent .= "<td>".$row->time."</td>";
				$MessageContent .= '<a href="">View</a></tr>';
				
			};
			//$MessageContent .= '<tbody id="infinteScrollContent'.$page++.'"></tbody>';
			echo $MessageContent;die;
		}else{ 
            redirect("http://".$_SERVER["HTTP_HOST"]."",'refresh');
        }
	}
	
	/**
	 * Function Name : showAlljobInvitations
	 * Discription : To show all the job invitations page.
	 * @author Synergy
	 * @param none
	 * @return render data into main view page
	 */
	 
	function showAlljobInvitations(){
		$statictoken = 'ecbcd7eaee29848978134beeecdfbc7c';
        if($this->config->item('HTTPSENABLED') == 1){
			$base_url = 'https://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
		}else{
			$base_url = 'http://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
		}
        //$this->load->library('session');
        $session = $this->session->get_userdata();
		
        if(isset($session['userId']) && $session['userId']!= ""){
            $userEmail =  $session['userEmail'];
            $userName =  $session['userName'];
            $userId =  $session['userId'];
			
			//Start of getting job invitations
			$json_inv = array("token" => $statictoken, "methodname" => 'getInvites', 'userId' => $userId, 'email' => $userEmail);                                                                    
			$data_string_inv = json_encode($json_inv);                                                                                   
																																 
			$ch_inv = curl_init($base_url);                                                                      
			curl_setopt($ch_inv, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
			curl_setopt($ch_inv, CURLOPT_POSTFIELDS, $data_string_inv);                                                                  
			curl_setopt($ch_inv, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch_inv, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch_inv, CURLOPT_HTTPHEADER, array(                                                                          
				'Content-Type: application/json',                                                                                
				'Content-Length: ' . strlen($data_string_inv))                                                                       
			);                                                                                                                   
																																 
			$result_inv = curl_exec($ch_inv);
			$all_result_inv = json_decode($result_inv);
			$allContent_inv = $all_result_inv->data;
			$data['jobInvitations'] = $allContent_inv;
			
			$data['content'] = $allContent_inv;
			$data['token'] = $userId;
			$data['userName'] = $userName;
			$data['email'] = $userEmail;
			$data['invitationId'] = $invitationId;
            $data['allNotificationList'] = $this->allNotificationList;
			$data['main_content'] = 'all-invite-jobs';
			$this->load->view('fj-mainpage-withMenu', $data);
		}else{ 
            redirect("http://".$_SERVER["HTTP_HOST"]."",'refresh');
        }
	}
	
	/**
	 * Function Name : allClosedJobInvitations
	 * Discription : To show all the closed job invitations page.
	 * @author Synergy
	 * @param none
	 * @return render data into main view page
	 */
	 
	function allClosedJobInvitations(){
		$statictoken = 'ecbcd7eaee29848978134beeecdfbc7c';
        if($this->config->item('HTTPSENABLED') == 1){
			$base_url = 'https://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
		}else{
			$base_url = 'http://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
		}
        //$this->load->library('session');
        $session = $this->session->get_userdata();
		
        if(isset($session['userId']) && $session['userId']!= ""){
            $userEmail =  $session['userEmail'];
            $userName =  $session['userName'];
            $userId =  $session['userId'];
			
			//Start of getting job invitations
			$json_inv = array("token" => $statictoken, "methodname" => 'getClosedInvites', 'userId' => $userId, 'email' => $userEmail);                                                                    
			$data_string_inv = json_encode($json_inv);                                                                                   
																																 
			$ch_inv = curl_init($base_url);                                                                      
			curl_setopt($ch_inv, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
			curl_setopt($ch_inv, CURLOPT_POSTFIELDS, $data_string_inv);                                                                  
			curl_setopt($ch_inv, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch_inv, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch_inv, CURLOPT_HTTPHEADER, array(                                                                          
				'Content-Type: application/json',                                                                                
				'Content-Length: ' . strlen($data_string_inv))                                                                       
			);                                                                                                                   
																																 
			$result_inv = curl_exec($ch_inv);
			$all_result_inv = json_decode($result_inv);
			$allContent_inv = $all_result_inv->data;
			$data['jobInvitations'] = $allContent_inv;
			
			$data['content'] = $allContent_inv;
			$data['token'] = $userId;
			$data['userName'] = $userName;
			$data['email'] = $userEmail;
			$data['invitationId'] = $invitationId;
            $data['allNotificationList'] = $this->allNotificationList;
			$data['main_content'] = 'all-closedinvite-jobs';
			$this->load->view('fj-mainpage-withMenu', $data);
		}else{ 
            redirect("http://".$_SERVER["HTTP_HOST"]."",'refresh');
        }
	}
	
	/**
	 * Function Name : showAllsavedJobs
	 * Discription : To show all the saved job page.
	 * @author Synergy
	 * @param none
	 * @return render data into main view page
	 */
	 
	function showAllsavedJobs(){
		$statictoken = 'ecbcd7eaee29848978134beeecdfbc7c';
        if($this->config->item('HTTPSENABLED') == 1){
			$base_url = 'https://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
		}else{
			$base_url = 'http://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
		}
        //$this->load->library('session');
        $session = $this->session->get_userdata();
		
        if(isset($session['userId']) && $session['userId']!= ""){
            $userEmail =  $session['userEmail'];
            $userName =  $session['userName'];
            $userId =  $session['userId'];
			
		
			//Start of saved jobs
			$json_savedjobs = array("token" => $statictoken, "methodname" => 'getSavedJob', 'userId' => $userId);                                                                    
			$data_string_savedjobs = json_encode($json_savedjobs);                                                                                                                                                                    
			$ch_savedjobs = curl_init($base_url);                                                                      
			curl_setopt($ch_savedjobs, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch_savedjobs, CURLOPT_POSTFIELDS, $data_string_savedjobs);
			curl_setopt($ch_savedjobs, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch_savedjobs, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch_savedjobs, CURLOPT_HTTPHEADER, array(                                                                          
				'Content-Type: application/json',                                                                                
				'Content-Length: ' . strlen($data_string_savedjobs))
			);                                                                                                                   
																																 
			$result_savedjobs = curl_exec($ch_savedjobs);
			$all_result_savedjobs = json_decode($result_savedjobs);
			//print_r($all_result_savedjobs);exit;
			$data['savedJobs'] = $all_result_savedjobs->data;
			//End of saved jobs
			$data['token'] = $userId;
			$data['userName'] = $userName;
			$data['email'] = $userEmail;
			$data['invitationId'] = $invitationId;
            $data['allNotificationList'] = $this->allNotificationList;
			$data['main_content'] = 'all-saved-jobs';
			$this->load->view('fj-mainpage-withMenu', $data);
		}else{ 
            redirect("http://".$_SERVER["HTTP_HOST"]."",'refresh');
        }
	}
	
	
	/**
	 * Function Name : showAllappliedJobs
	 * Discription : To show all the applied job page.
	 * @author Synergy
	 * @param none
	 * @return render data into main view page
	 */
	 
	function showAllappliedJobs(){
		$statictoken = 'ecbcd7eaee29848978134beeecdfbc7c';
        if($this->config->item('HTTPSENABLED') == 1){
			$base_url = 'https://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
		}else{
			$base_url = 'http://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
		}
        //$this->load->library('session');
        $session = $this->session->get_userdata();
		
        if(isset($session['userId']) && $session['userId']!= ""){
            $userEmail =  $session['userEmail'];
            $userName =  $session['userName'];
            $userId =  $session['userId'];
			
		
			//Start of applied jobs
			$json_appliedjobs = array("token" => $statictoken, "methodname" => 'getAppliedJob', 'userId' => $userId);                                                                    
			$data_string_appliedjobs = json_encode($json_appliedjobs);                                                                                                                                                             
			$ch_appliedjobs = curl_init($base_url);                                                                      
			curl_setopt($ch_appliedjobs, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch_appliedjobs, CURLOPT_POSTFIELDS, $data_string_appliedjobs);
			curl_setopt($ch_appliedjobs, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch_appliedjobs, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch_appliedjobs, CURLOPT_HTTPHEADER, array(                                                                          
				'Content-Type: application/json',                                                                                
				'Content-Length: ' . strlen($data_string_appliedjobs))
			);                                                                                                                   
																																 
			$result_appliedjobs = curl_exec($ch_appliedjobs);
			$all_result_appliedjobs = json_decode($result_appliedjobs);
			
			$data['appliedJobs'] = $all_result_appliedjobs->data;
			
			//End of applied jobs
			$data['token'] = $userId;
			$data['userName'] = $userName;
			$data['email'] = $userEmail;
			//$data['invitationId'] = $invitationId;
            $data['allNotificationList'] = $this->allNotificationList;
			$data['main_content'] = 'all-applied-jobs';
			$this->load->view('fj-mainpage-withMenu', $data);
		}else{ 
            redirect("http://".$_SERVER["HTTP_HOST"]."",'refresh');
        }
	}

    function getAllNotifications(){
        $statictoken = 'ecbcd7eaee29848978134beeecdfbc7c';
        if($this->config->item('HTTPSENABLED') == 1){
			$base_url = 'https://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
		}else{
			$base_url = 'http://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
		}
        $this->load->library('session');
        $session = $this->session->get_userdata();
        
        if(isset($session['userId']) && $session['userId']!= ""){
            $data['city'] = $this->searchLocationList;
            $data['allNotificationList'] = $this->allNotificationList;
            $data['main_content'] = 'all_notifications';
            $this->load->view('fj-mainpage-withMenu', $data);
        }else{ 
            redirect("http://".$_SERVER["HTTP_HOST"]."",'refresh');
        }
    }

    function readAllNotifications(){
        $statictoken = 'ecbcd7eaee29848978134beeecdfbc7c';
        if($this->config->item('HTTPSENABLED') == 1){
			$base_url = 'https://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
		}else{
			$base_url = 'http://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
		}
        $session = $this->session->get_userdata();
        
        if(isset($session['userId']) && $session['userId']!= ""){
            $userId =  $session['userId'];
            
        
            //Start of applied jobs
            $jsonData = array("token" => $statictoken, "methodname" => 'readAllNotifications', 'userId' => $userId);                                                                    
            $data_string = json_encode($jsonData);                                                                                                                                                                    
            $ch = curl_init($base_url);                                                                      
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
                'Content-Type: application/json',                                                                                
                'Content-Length: ' . strlen($data_string))
            );                                                                                                                    
            $result = curl_exec($ch);
            $all_result = json_decode($result);
            redirect("https://".$_SERVER["HTTP_HOST"]."/jobseeker/users");
        }else{ 
            redirect("http://".$_SERVER["HTTP_HOST"]."",'refresh');
        }
    }
}
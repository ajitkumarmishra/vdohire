
    <link rel="stylesheet" href="https://cdn.webrtc-experiment.com/style.css">
	<link rel="stylesheet" href="<?php echo base_url();?>css/progress-circle.css">
	<link rel="stylesheet" href="<?php echo base_url();?>css/progress-circle-small.css">
    <style>
    audio {
        vertical-align: bottom;
        width: 10em;
    }
    video {
        max-width: 100%;
        vertical-align: top;
    }
    input {
        border: 1px solid #d9d9d9;
        border-radius: 1px;
        font-size: 2em;
        margin: .2em;
        width: 30%;
    }
    p,
    .inner {
        padding: 1em;
    }
    li {
        border-bottom: 1px solid rgb(189, 189, 189);
        border-left: 1px solid rgb(189, 189, 189);
        padding: .5em;
    }
    label {
        display: inline-block;
        width: 8em;
    }
    </style>

    <style>
        .recordrtc button {
            font-size: inherit;
        }

        .recordrtc button, .recordrtc select {
            vertical-align: middle;
            line-height: 1;
            padding: 2px 5px;
            height: auto;
            font-size: inherit;
            margin: 0;
        }

        .recordrtc, .recordrtc .header {
            display: block;
            text-align: center;
            padding-top: 0;
        }

        .recordrtc video {
            width: 70%;
        }

        .recordrtc option[disabled] {
            display: none;
        }
    </style>

    <script src="<?php echo base_url(); ?>js/min/webRtc/RecordRTC.min.js"></script>
    <script src="<?php echo base_url(); ?>js/min/webRtc/gif-recorder.min.js"></script>
    <script src="<?php echo base_url(); ?>js/min/webRtc/getScreenId.min.js"></script>

    <!-- for Edige/FF/Chrome/Opera/etc. getUserMedia support -->
    <script src="<?php echo base_url(); ?>js/min/webRtc/gumadapter.min.js"></script>

		
        <div class="container-full" style="background: white;">
            <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #054F72;height: 33px;">
                
            </div>
            <div  class="col-md-8 col-lg-8 col-sm-8 pull-left" style="    font-size: 17px;color: white;font-weight: bold;width: 80%;background: #EB9B2C ;">
				
						<center>
						<div class="large-12 column" id="myModal" style="position: static;display:block;top: 400px;left: 700px;z-index:1;">
						  <div id="progress-circle1" class="progress-circle-small progress-small-0" style="  margin-top:2px;margin-bottom:0px;   width: 49px;height: 49px;">
							<span id="percet_text1" style="font-size: 10px;    margin-top: -13px;margin-left: -13px;line-height: 27px;    width: 25px;height: 25px;">0</span>
						  </div>
						</div>
						</center>
						<center style="font-size:14px;">Video Resume</center>
				
				
                
				
				
            </div>
            <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #054F72;height: 33px;">
                
            </div>
            
        </div>
        <div style="clear:both"></div>
        <div class="container-full" style="background: white;">
            <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #f8f8f8;height: 30px;">
						
                </div>
            <div  class="col-md-8 col-lg-8 col-sm-8 pull-left" style="border:1px solid lightgrey;padding-bottom: 10px;padding-top: 20px;width: 80%;background: white ;">
                <div class="col-lg-12">
                    <!-- /.row -->
                    <center>
					
							<span style="color:black;font-weight:bold;">Question: </span><span  class="img-circle" style="background:#39a6e9;padding: 10px;    margin: 1rem;width: 56px;height: 56px;">
                                <span id="questionItemNo" style="font-weight:bold;">1</span>

							</span>
						<p style="font-size: 15px;font-weight: bold;" id="QuestionAsPerSynergy">
							Ensure your audio and webcam are working! Don't worry, you are not being recorder yet
						</p>
						<p id="demo" class="pull-right" style="padding:10px;border-top:6px solid #eb9b2c; font-size:18px;border-radius:5px 5px 5px 5px;background: #054f72;color: white;width:113px;"></p>
					</center>
                    
                    
                <!-- /.col-lg-4 -->
					<center>
        

						
						
						<section class="experiment recordrtc"  style="    border: none;display:none">
						<div style="margin-top: 10px;">
							<video id="recording-player" style=" width: 496px;height: 279px;"  controls muted></video>
						</div>
            <h2 class="header" style="margin-top:0px;border:none;">
                <select class="recording-media" style="display:none;">
                    <option value="record-audio-plus-video">Microphone+Camera</option>
                    <option value="record-audio">Microphone</option>
                    <option value="record-screen">Full Screen</option>
                    <option value="record-audio-plus-screen">Microphone+Screen</option>
                </select>


                <select class="media-container-format" style="display:none;">
                    <option>vp8</option>
                    <option>vp9</option>
                    <option>h264</option>
                    <option>opus</option>
                    <option>pcm</option>
                    <option>gif</option>
                    <option>whammy</option>
                </select>

                <input type="checkbox" id="chk-MultiStreamRecorder" style="margin:0;width:auto; display:none;">


                <button  onclick="IamDone()" id="btn-start-recording" style="    padding: 16px;font-size: 18px;background: #07B1ED;border: none;   margin-left: 46px;margin-top: 10px;margin-right: 50px;">Start Recording</button>
                <div id="btn-pause-recording" style="display: none; font-size: 15px;"></div>
                <select class="media-resolutions" style="display:none;">
                    <option value="default">Default resolutions</option>
                    <option value="1920x1080">1080p</option>
                    <option value="1280x720">720p</option>
                    <option value="3840x2160">4K Ultra HD (3840x2160)</option>
                </select>

                <select class="media-framerates" style="display:none;">
                    <option value="default">Default framerates</option>
                    <option value="5">5 fps</option>
                    <option value="15">15 fps</option>
                    <option value="24">24 fps</option>
                    <option value="30">30 fps</option>
                    <option value="60">60 fps</option>
                </select>

                <select class="media-bitrates" style="display:none;">
                    <option value="default">Default bitrates</option>
                    <option value="8000000000">1 GB bps</option>
                    <option value="800000000">100 MB bps</option>
                    <option value="8000000">1 MB bps</option>
                    <option value="800000">100 KB bps</option>
                    <option value="8000">1 KB bps</option>
                    <option value="800">100 Bytes bps</option>
                </select>
            </h2>

            <div style="text-align: center; display: none;">
                <button id="save-to-disk" style="display: none;">Save To Disk</button>
				<button id="upload-to-php" style="display: none;">Upload to PHP</button>
                <button id="open-new-tab" style="display: none;">Open New Tab</button>
				<button id="upload-to-youtube" style="vertical-align:top;display: none;" >Upload to YouTube</button>
                
            </div>

            
        </section>
							<!-- /.row -->
					</center>
					<section class="answerButton" style="    border: none;display:block;    margin-top: 9%;">
						<center>
								<button onclick="answerNow()" type="button" class="btn btn-primary" style="    padding: 16px;font-size: 18px;background: #07B1ED;border: none;   margin-left: 10px;margin-top: 10px;margin-right: 50px;">ANSWER NOW</button>
						</center>
					</section>
                <!-- /.col-lg-8 -->
                </div>   
            <!-- /.row -->
                
            <!-- /.row -->

                </div>

            <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #f8f8f8;height: 30px;">

                </div>

        </div>
        
        <?php
        
        ?>
        
    <!-- /#page-wrapper -->
    </div>
</div>
<!-- /#wrapper -->
<input type="hidden" value="<?php echo $isInvite; ?>" name="isInvite" id="isInvite">


<script src="<?php echo base_url(); ?>theme/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url(); ?>theme/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>theme/js/bootbox.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url(); ?>theme/js/formValidation.min.js"></script>
    <script src="<?php echo base_url(); ?>theme/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>js/min/audition_question_view.min.js"></script>
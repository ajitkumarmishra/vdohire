<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Users Controller
 * Description : Used to handle all the jobseeker related data
 * @author Synergy
 * @createddate : April 3, 2016
 * @modificationlog : Initializing the controlelr
 * @change on Mar 24, 2017
 */
class Audition extends MY_Controller {

    public $allNotificationList = array();
    /**
     * Responsable for inherit the parent connstructor
     * @return void
     */
    function __construct() {
        parent::__construct();

        $this->load->library('session');
        $this->load->helper('fj');
        $this->load->helper('jwt');
        
        $this->getEditProfileData();
        //Use to get Locations for search Job
        $this->searchLocationList = getLocationCities();

        $userRealData = $this->session->get_userdata();
        //print_r($userRealData);exit;

        if (isset($userRealData['userId'])) {
            /* Get Notification List for user */
            $userId = $userRealData['userId'];

            $statictoken = 'ecbcd7eaee29848978134beeecdfbc7c';

            if($this->config->item('HTTPSENABLED') == 1){
               $base_url = 'https://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
            }else{
               $base_url = 'http://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
            }

            $json = array("token" => $statictoken, "methodname" => 'getUserNotifications', 'userId' => $userId);                                                                    
            $data_string = json_encode($json);                                                                         
                                                                                                                                 
            $ch = curl_init($base_url);                                                                      
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);                                                                      
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
                'Content-Type: application/json',                                                                                
                'Content-Length: ' . strlen($data_string))                                                                       
            );                                                                                                                   
                                                                                                                                 
            $result = curl_exec($ch);
            $allResult = json_decode($result);
            $this->allNotificationList = $allResult->data;

            /* End of get notification list fo user */
        }
    }

    /**
     * Function Naem : index
     * Discription : Use to display login page for job seeker
     * @author Synergy
     * @param none
     * @return render data into main view page
     */
    
    function index() {
        $this->load->helper('cookie');

        $session = $this->session->get_userdata();
        $token = $session['token'];
        $userEmail = $session['userEmail'];
        $userName = $session['userName'];
        $userId = $session['userId'];
		
        $statictoken = 'ecbcd7eaee29848978134beeecdfbc7c';
		
        if($this->config->item('HTTPSENABLED') == 1){
           $base_url = 'https://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
        }else{
           $base_url = 'http://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
        }
	   
		//set visitor log
		$ip =  $_SERVER['REMOTE_ADDR'] ;
		$page = "videoResumeInstruction";
		$useragent = $_SERVER ['HTTP_USER_AGENT'];
		$cookie = $_COOKIE['visitor_cookie'];
		$userEmail = isset($_SESSION['userEmail'])?$_SESSION['userEmail']:"";
		
		if($cookie == ""){
			setcookie('visitor_cookie', $cookie_value, time() + (7200), "/"); // 86400 = 1 day
		}
		$jsonVisitor = array("token" => $statictoken, "methodname" => 'auditionHistory', 'userId' => $userId);
		
		$data_string_visitor = json_encode($jsonVisitor);                                                                                   
																															 
		$ch_visitor = curl_init($base_url);                                                                      
		curl_setopt($ch_visitor, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
		curl_setopt($ch_visitor, CURLOPT_POSTFIELDS, $data_string_visitor);                                                                  
		curl_setopt($ch_visitor, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch_visitor, CURLOPT_SSL_VERIFYPEER, false);                                                         
		curl_setopt($ch_visitor, CURLOPT_HTTPHEADER, array(                                                                          
			'Content-Type: application/json',                                                                                
			'Content-Length: ' . strlen($data_string_visitor))                                                                       
		);                                                                                                                   
		$curlResult = curl_exec($ch_visitor);
		$allResult = json_decode($curlResult);
		$resumeList = $allResult->data;
		$data['resumelist'] = $resumeList;
        if (isset($userId) && $userId != "") {
            $data['city'] = $this->searchLocationList;
            $data['allNotificationList'] = $this->allNotificationList;
            $data['token'] = $userId;
            $data['userName'] = $userName;
            $data['email'] = $userEmail;
            $data['main_content'] = 'videoResume';
            $this->load->view('fj-mainpage-withMenu', $data);
        } else {
            redirect("https://" . $_SERVER["HTTP_HOST"] . "", 'refresh');
        }

        //echo 'hello';exit;
    }

    function setup() {
        $this->load->library('session');
        $this->load->helper('cookie');
        
		$statictoken = 'ecbcd7eaee29848978134beeecdfbc7c';

        if($this->config->item('HTTPSENABLED') == 1){
           $base_url = 'https://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
        }else{
           $base_url = 'http://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
        }
				
		if(isset($_SESSION['userId']) && $_SESSION['userId'] != ""){
            $data['city'] = $this->searchLocationList;
            $data['allNotificationList'] = $this->allNotificationList;
			$data['main_content'] = 'videoResumeSetup';
			$this->load->view('fj-mainpage-withoutMenu', $data);
		}else{
			redirect(base_url(),'refresh');
		}

    }

    function instruction() {
        $this->load->helper('cookie');
        $this->load->library('session');
		
        $statictoken = 'ecbcd7eaee29848978134beeecdfbc7c';

        if($this->config->item('HTTPSENABLED') == 1){
           $base_url = 'https://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
        }else{
           $base_url = 'http://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
        }
		
	   
		//set visitor log
		$ip =  $_SERVER['REMOTE_ADDR'] ;
		$page = "videoResumeInstruction";
		$useragent = $_SERVER ['HTTP_USER_AGENT'];
		$cookie = $_COOKIE['visitor_cookie'];
		$userEmail = isset($_SESSION['userEmail'])?$_SESSION['userEmail']:"";
		
		if($cookie == ""){
			setcookie('visitor_cookie', $cookie_value, time() + (7200), "/"); // 86400 = 1 day
		}
		$jsonVisitor = array("token" => $statictoken, "methodname" => 'userVisitLog', "ip" => $ip, "useragent" => $useragent, "page" => $page,  "cookie" => $cookie, "userEmail" => $userEmail);
		
		$data_string_visitor = json_encode($jsonVisitor);                                                                                   
																															 
		$ch_visitor = curl_init($base_url);                                                                      
		curl_setopt($ch_visitor, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
		curl_setopt($ch_visitor, CURLOPT_POSTFIELDS, $data_string_visitor);                                                                  
		curl_setopt($ch_visitor, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch_visitor, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch_visitor, CURLOPT_HTTPHEADER, array(                                                                          
			'Content-Type: application/json',                                                                                
			'Content-Length: ' . strlen($data_string_visitor))                                                                       
		);                                                                                                                   
		$curlResult = curl_exec($ch_visitor);
				
        if(isset($_SESSION['userId']) && $_SESSION['userId'] != ""){
            $data['city'] = $this->searchLocationList;
            $data['allNotificationList'] = $this->allNotificationList;
			$data['token'] = $_SESSION['userId'];
			//$data['userName'] = $userName;
			$data['jobActivationCode'] = 200;
			$data['email'] = $userEmail;
			$data['main_content'] = 'videoResumeInstruction';
			$this->load->view('fj-mainpage-withoutMenu', $data);
		}else{
			redirect(base_url(),'refresh');
		}
        
    }

    function videoInterview() {
        //$this->load->helper('cookie');
        $this->load->library('session');

        $session = $this->session->get_userdata();
        $this->load->helper('cookie');

        $session = $this->session->get_userdata();
        $token = $session['token'];
        $userEmail = $session['userEmail'];
        $userName = $session['userName'];
        $userId = $session['userId'];
        if (isset($userId) && $userId != "") {
            $data['city'] = $this->searchLocationList;
            $data['allNotificationList'] = $this->allNotificationList;
            $data['token'] = $userId;
            $data['isInvite'] = 1;
            $data['userName'] = $userName;
            $data['email'] = $userEmail;
            $data['main_content'] = 'video';
            $this->load->view('fj-mainpage', $data);
        } else {
            redirect("https://" . $_SERVER["HTTP_HOST"] . "", 'refresh');
        }
    }

    function questionView() {
        //$this->load->helper('cookie');
        $this->load->library('session');

        $session = $this->session->get_userdata();
        $statictoken = 'ecbcd7eaee29848978134beeecdfbc7c';
        
        if($this->config->item('HTTPSENABLED') == 1){
           $base_url = 'https://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
        }else{
           $base_url = 'http://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
        }
		$session = $this->session->get_userdata();
        $token = $session['token'];
        $userEmail = $session['userEmail'];
        $userName = $session['userName'];
			
        if(isset($_SESSION['userId']) && $_SESSION['userId'] != ""){
            $data['city'] = $this->searchLocationList;
            $data['allNotificationList'] = $this->allNotificationList;
            $data['token'] = $_SESSION['userId'];
            $data['userName'] = $userName;
            $data['email'] = $userEmail;
			$data['isInvite'] = 1;
            $data['main_content'] = 'audition_question_view';
            $this->load->view('fj-mainpage-withoutMenu', $data);
        } else {
            redirect(base_url(), 'refresh');
        }
    }

    function SubmitAns() {
        $a = array('101' => 'ab', '122' => 'cd');
        $b = array('ab' => '11111', '1e2' => '222222222');
        array_push($a, $b);
        echo "<pre>";
        print_r($a);
        die;

        //$this->load->library('session');
        $SesssionArray = $_SESSION['AssessmentAns'];
        //echo "<pre>";print_r($SesssionArray);die;
        //$counter = count($SesssionArray);
        $inputSesssionArray = $SesssionArray . "$%$" . $_POST['question'] . ":" . $_POST['ans'] . "$%$";

        $_SESSION['AssessmentAns'] = $inputSesssionArray;
        /* foreach($this->session->get_userdata('AssessmentAns') as $key=>$data){
          $SesssionArray[$key] = $data;

          } */

        echo "<pre>";
        print_r($_SESSION);
        die;
    }

    function uploadResume() {
        //$this->load->helper('cookie');
        $this->load->library('session');

        $session = $this->session->get_userdata();
        $this->load->helper('cookie');
		
		$statictoken = 'ecbcd7eaee29848978134beeecdfbc7c';

        if($this->config->item('HTTPSENABLED') == 1){
           $base_url = 'https://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
        }else{
           $base_url = 'http://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
        }

		$data = array("token" => $statictoken, "methodname" => 'verifyJob', 'referrenceJobId' => $_SESSION['jobCode'], 'deviceUniqueId' => '1111111', 'searchBy' => 'job' );                                                                    
        $data_string = json_encode($data);                
		
        $ch = curl_init($base_url);                                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);                               
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data_string))                                                                       
        );                                                                                                                   
                                                                                                                             
        $result = curl_exec($ch);
		
        $allResult = json_decode($result);
        $invitaionDetails = $allResult->data;
		
		
		
		$data = array("token" => $statictoken, "methodname" => 'getUserResumes', 'userId' => $_SESSION['userId']);                                                                    
		$data_string = json_encode($data);                                                                                   
																															 
		$ch = curl_init($base_url);                                                                      
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);                                                                      
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
			'Content-Type: application/json',                                                                                
			'Content-Length: ' . strlen($data_string))                                                                       
		);                                                                                                                   
																															 
		$result = curl_exec($ch);
		$allResult = json_decode($result);
		$resumeList = $allResult->resumeList;
		$data['resumelist'] = $resumeList;
		$data['invitationDetails'] = $invitaionDetails[0];
		
		
         if(isset($_SESSION['userId']) && $_SESSION['userId'] != ""){
            $data['token'] = $userId;
            $data['userName'] = $userName;
            $data['email'] = $userEmail;
            $data['main_content'] = 'uploadResume';
            $this->load->view('fj-mainpage-withoutMenu', $data);
        } else {
            redirect("https://" . $_SERVER["HTTP_HOST"] . "", 'refresh');
        }
    }

    function quidelines() {
        //$this->load->helper('cookie');
        $this->load->library('session');
		
        $session = $this->session->get_userdata();
        $this->load->helper('cookie');
		$statictoken = 'ecbcd7eaee29848978134beeecdfbc7c';

        if($this->config->item('HTTPSENABLED') == 1){
           $base_url = 'https://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
        }else{
           $base_url = 'http://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
        }

		$data = array("token" => $statictoken, "methodname" => 'verifyJob', 'referrenceJobId' => $_SESSION['jobCode'], 'deviceUniqueId' => '1111111', 'searchBy' => 'job' );                                                                    
        $data_string = json_encode($data);                
		
        $ch = curl_init($base_url);                                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);                                                                      
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data_string))                                                                       
        );                                                                                                                   
                                                                                                                             
        $result = curl_exec($ch);
		
        $allResult = json_decode($result);
        $invitaionDetails = $allResult->data;
       
        
        
        $data['invitationDetails'] = $invitaionDetails[0];
        
		
		
		
        if(isset($_SESSION['userId']) && $_SESSION['userId'] != ""){
            $data['token'] = $userId;
            $data['userName'] = $userName;
            $data['email'] = $userEmail;
            $data['main_content'] = 'assessmentGuidliance';
            $this->load->view('fj-mainpage-withoutMenu', $data);
        } else {
            redirect("https://" . $_SERVER["HTTP_HOST"] . "", 'refresh');
        }
    }

    function thankyou() {
        //$this->load->helper('cookie');
        $this->load->library('session');
		$session = $this->session->get_userdata();
		
		$statictoken = 'ecbcd7eaee29848978134beeecdfbc7c';

        if($this->config->item('HTTPSENABLED') == 1){
           $base_url = 'https://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
        }else{
           $base_url = 'http://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
        }

		$data = array("token" => $statictoken, "methodname" => 'verifyJob', 'referrenceJobId' => $_SESSION['jobCode'], 'deviceUniqueId' => '1111111', 'searchBy' => 'job' );                                                                    
        $data_string = json_encode($data);                
		
        $ch = curl_init($base_url);                                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);                                                                      
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data_string))                                                                       
        );                                                                                                                   
                                                                                                                             
        $result = curl_exec($ch);
		
        $allResult = json_decode($result);
        $invitaionDetails = $allResult->data;
		$data['invitationDetails'] = $invitaionDetails[0];
		
		//set visitor log
		$ip =  $_SERVER['REMOTE_ADDR'] ;
		$page = "thankyou";
		$useragent = $_SERVER ['HTTP_USER_AGENT'];
		$cookie = $_COOKIE['visitor_cookie'];
		$userEmail = isset($_SESSION['userEmail'])?$_SESSION['userEmail']:"";
		
		if($cookie == ""){
			setcookie('visitor_cookie', $cookie_value, time() + (7200), "/"); // 86400 = 1 day
		}
		$jsonVisitor = array("token" => $statictoken, "methodname" => 'userVisitLog', "ip" => $ip, "useragent" => $useragent, "page" => $page,  "cookie" => $cookie, "userEmail" => $userEmail);
		
		$data_string_visitor = json_encode($jsonVisitor);                                                                                   
																															 
		$ch_visitor = curl_init($base_url);                                                                      
		curl_setopt($ch_visitor, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
		curl_setopt($ch_visitor, CURLOPT_POSTFIELDS, $data_string_visitor);                                                                  
		curl_setopt($ch_visitor, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch_visitor, CURLOPT_SSL_VERIFYPEER, false);                                                                      
		curl_setopt($ch_visitor, CURLOPT_HTTPHEADER, array(                                                                          
			'Content-Type: application/json',                                                                                
			'Content-Length: ' . strlen($data_string_visitor))                                                                       
		);                                                                                                                   
		$curlResult = curl_exec($ch_visitor);
		

        $session = $this->session->get_userdata();
        $token = $session['token'];
        $userEmail = $session['userEmail'];
        $userName = $session['userName'];
        $userId = $session['userId'];
        if (isset($userId) && $userId != "") {
            $data['token'] = $userId;
            $data['userName'] = $userName;
            $data['email'] = $userEmail;
            $data['main_content'] = 'thankyou';
            $this->load->view('fj-mainpage-withoutMenu', $data);
        } else {
            redirect("https://" . $_SERVER["HTTP_HOST"] . "", 'refresh');
        }
    }
}
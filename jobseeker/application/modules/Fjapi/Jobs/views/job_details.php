
<style>
video::-internal-media-controls-download-button {
    display:none;
}

video::-webkit-media-controls-enclosure {
    overflow:hidden;
}

video::-webkit-media-controls-panel {
    width: calc(100% + 30px); /* Adjust as needed */
}
#tabs > ul li > a > table:hover{
	background:none;
}
</style>


    <div class="container-full" style="background: white;color:white;">
        <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #054F72;height: 38px;">

        </div>
        <div  class="col-md-8 col-lg-8 col-sm-8 pull-left" style="font-size: 17px;color: white;font-weight: bold;width: 80%;background: #EB9B2C ;">
            <div class="btn-group btn-group-lg" style="padding: 10px 0px 10px 0px;">
                <div  class="pull-left" style="width: auto;margin-right:30px;">
                    <button onclick="showJobDescription()" id="showJobDescription"  type="button" class="btn btn-primary" style="font-weight: 600;padding: 13px;background: #054f72;font-size: 17px;border: none;">JOB DESCRIPTION</button>
                </div>
                <div  class=" pull-left" style="    width: auto;margin-right:30px;">
                    <button onclick="ShowAboutCompany()" id="ShowAboutCompany" type="button" class="btn btn-primary" style="font-weight: 600;padding: 13px;background: #d3d3d3;font-size: 17px;border: none;color:black;">ABOUT COMPANY</button>
                </div>

            </div>
            <div style="clear:both"></div>
        </div>
        <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #054F72;height: 38px;">
        </div>
    </div>
    <div style="clear:both"></div>
    <div class="container-full" style="background: white;">
        <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #f8f8f8;    height: 23px;">
        </div>
        <div  class="col-md-8 col-lg-8 col-sm-8 pull-left" style="border:1px solid lightgrey;padding-bottom: 10px;padding-top: 0px;width: 80%;background: white ;">
            <div>
                <div class="col-lg-12" style="    height: 350px;overflow-y: auto;">
                    <div class="col-lg-6" id="showAboutUs" style="display:none;">
                        <h3 style="margin-top:10px;"><?php echo $content->companyName; ?></h3>
                        <?php echo $content->aboutCompany; ?>
                    </div>
                    <div class="col-lg-6" id="showJobDescription1">
                        <h3 style="margin-top:10px;">Job Description</h3>
						<?php echo $content->description; ?>
                        <div style="clear:both"></div>
                        <!--<h3>Introduction Video</h3>
                        <video src="<?php //echo 'https://35.154.53.72/jobseeker/video/test.webm';  ?>" style="    height: 288px;" controls></video>-->
                    </div>
                    <div class="col-lg-6" style="" id="screeningProcess">
                        <h3 style="margin-top:10px;">Application Process</h3>
                        <p><b>The application process for this position consists of:</b><br>
                        <ul>
                            <li>An Interview, with "<?php if($content->interviewQuestion == 1){ echo $content->interviewQuestion.'" question'; }else{  echo $content->interviewQuestion.'" questions'; } ?> with a total answering time of "<?php 
							$mins = floor($content->interviewDuration/60); 
							$seconds = $content->interviewDuration - $mins*60;
							if($seconds == 0){
								$seconds = "";
							}else{
								$seconds = $seconds." seconds";
							}
							
							if($mins >1){
								$text = "mins";
							}else{
								$text = "min";
							}
							echo $mins." ".$text." ". $seconds;  ?>"</li>
							<?php
							if($content->assessmentDuration == ""){
							}else{
							?>
                            <li>An Assessment, with "<?php if($content->assessmentQuestion == 1){ echo $content->assessmentQuestion.'" question'; }else{  echo $content->assessmentQuestion.'" questions'; } ?> , with multiple-choice answer options, with a total provided time of "<?php 
							$mins = floor($content->assessmentDuration/60); 
							$seconds = $content->assessmentDuration - $mins*60; 
							if($seconds == 0){
								$seconds = "";
							}else{
								$seconds = $seconds." seconds";
							}
							
							if($mins >1){
								$text = "mins";
							}else{
								$text = "min";
							}
							echo $mins." ".$text." ". $seconds; ?>"</li>
							<?php } ?>
                        </ul>
                        </p>
                        <br>
                        <p>
                            Click on <b>"Apply"</b> to initiate the FirstJob Digital Application experience or <b>"Save for Later"</b> if you would like to complete the application process later. The saved job will be available on your Dashboard. If you click on "Apply", you will be taken through a set of guidelines to ensure you are ready for the Interview. This should not take more than 5 minutes. The Job Interview will start only when you click on the "Start Interview" button. Till that point, you can choose "Save for Later" at any stage. 
                        </p>
                        <!-- /.col-lg-8 -->
                    </div>

                    <!-- /.col-lg-8 -->
                </div>
            </div>



            <!-- /.row -->
            <div class="col-lg-12" style="">
                <!-- /.row -->
                <div class="col-lg-6">
                    <?php if ($checkAppliedJob == 200) {
								if($getUserJobProcessStatus > 0){?>
									<center>
						<input type="hidden" name="alreadyAppliedstatus" id="alreadyAppliedstatus" value="1">
						<button type="button" class="btn btn-primary" style="    padding: 16px;
                                    font-size: 18px;background: #11AAF0;border: none;   margin-left: 10px;margin-right: 50px;" disabled>ALREADY APPLIED</a></button>
									</center>
								<?php }else{
						?>
                        <center>
						<input type="hidden" name="alreadyAppliedstatus" id="alreadyAppliedstatus" value="0">
                        <?php 
						//$jobCodeArray = array('808069', '319138', '375988' , 'M17VMh', 'RBLM953715', '166624', '524845');
						//if(in_array($this->uri->segment(3), $jobCodeArray)) { ?>
    						<!--<a class="btn btn-primary"  href="javascript://" style=" padding: 16px; font-size: 18px;background: #11AAF0;border: none;   margin-left: 10px;margin-right: 50px;color:white;    text-decoration: none;    padding-left: 60px;padding-right: 63px;" disabled>APPLY
                            </a>-->
                        <?php //} else { ?>
    						<a class="btn btn-primary"  href="<?php echo base_url() ?>media/question" style="    padding: 16px; font-size: 18px;background: #11AAF0;border: none;   margin-left: 10px;margin-right: 50px;color:white;    text-decoration: none;    padding-left: 60px;padding-right: 63px;" >APPLY
                            </a>
                        <?php //} ?>
                        </center>
                <?php
								}
				} else { ?>
                        <center>
						<input type="hidden" name="alreadyAppliedstatus" id="alreadyAppliedstatus" value="1">
						<button type="button" class="btn btn-primary" style="    padding: 16px;
                                    font-size: 18px;background: #11AAF0;border: none;   margin-left: 10px;margin-right: 50px;" disabled>ALREADY APPLIED</a></button>
									</center>
                <?php } ?>
                </div>
                
                <!-- /.col-lg-8 -->
                <div class="col-lg-6">
                    <!-- /.panel -->
                    <?php if ($checkAppliedJob == 200 && $checkSavedJobs != 200) { ?>
                        <center><button type="button" onclick="saveForJob()" class="btn btn-primary" title="Save for later." style="padding: 16px;font-size: 18px;background: #7FCFB4;border: none;   margin-left:10px;margin-right: 50px;">SAVE FOR LATER</button>
                        </center>
<?php } else { 
if($checkSavedJobs == 200){
	$SaveButtontooltip = "Job Already Saved";	
}else{
	$SaveButtontooltip = "Job Already Applied";
}
?>
                  <center><button type="button" onclick="saveForJob()" class="btn btn-primary" style="padding: 16px;font-size: 18px;background: #7FCFB4;border: none;   margin-left:10px;margin-right: 50px;" title="Job Already Saved" disabled>SAVE FOR LATER</button>
                        </center>    
<?php } ?>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-8 -->

            </div>
            <!-- /.row -->
            <!-- /.row -->
        </div>
    </div>
    <!-- /#page-wrapper -->
</div>
</div>
<!-- /#wrapper -->



<script src="<?php echo base_url(); ?>theme/vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url(); ?>theme/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>theme/js/bootbox.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->

<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url(); ?>theme/js/formValidation.min.js"></script>
<script src="<?php echo base_url(); ?>theme/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>js/min/job_details.min.js"></script>


<style>
    .content-row {
        clear: both;
        position: relative;
        background-color: #FFF;
        margin-bottom: 5px;
        border-right: 9px solid #f9f9f9;
    }

    .content-row .content-anchor {
        display: block;
        padding: 25px 25px 5px;
        position: relative;
        text-decoration: none;
    }

    .content-row .desig {
        display: block;
        font-size: 14px;
        padding-bottom: 10px;
        color: #337ab7;
    }

    .content-row .org {
        display: block;
        padding-right: 110px;
        font-weight: 300;
        padding-bottom: 10px;
    }

    .content-row .exp {
        float: left;
        width: 18%;
        padding-right: 10px;
        font-weight: 300;
        color: #999;
    }

    .content-row .loc {
        float: left;
        width: 82%;
        padding-bottom: 20px;
        font-weight: 300;
        padding-bottom: 10px;
    }

    .content-row .loc span {
        float: left;
        width: 88%;
        word-wrap: break-word;
    }

    .content-row .more {
        overflow: hidden;
        clear: left;
        padding-bottom: 10px;
    }

    .content-row .label {
        float: left;
        clear: left;
        width: 18%;
        line-height: 15px;
        padding-right: 10px;
        font-weight: 300;
        font-size: 12px;
        color: #999;
        text-align: left;
        padding: 0;
    }

    .content-row .desc {
        float: left;
        width: 71%;
        font-size: 13px;
        color: #444;
        line-height: 16px;
        font-weight: 300;
        text-align: justify;
    }

    .content-row .fa {
        margin-right: 7px;
    }

    .content-row .other_details {
        background-color: #f9f9f9;
        overflow: hidden;
        clear: left;
        border-bottom: 2px solid #f9f9f9;
        font-size: 12px;
        color: #999;
        margin-left: 21px;
    }

    .content-row .salary {
        max-width: 35%;
        border-right: 1px solid #f4f4f4;
        padding: 5px 10px;
        float: left;
        height: 30px;
        line-height: 30px;
        min-width: 25px;
        font-weight: 300;
    }

    .content-row .rec_details {
        margin: 0 10px;
        padding: 5px 0;
        float: right;
        text-align: right;
        width: 48%;
        border-right: none;
        font-weight: normal;
    }

    .acord_head {
        cursor: pointer;
        padding: 12px 25px 10px 20px;
        margin: 3px 0 0;
        font-size: 14px;
        font-weight: 400;
        color: #747474;
        position: relative;
        border-top: 1px solid #f1f0f0;
    }

</style>
<?php //print '<pre>';print_r($_SESSION);exit;?>
    <!-- Navigation -->

    <div class="row" style="    background: #f8f8f8;margin-right: 0px;">
        <div class="container-full" style="background: white;">
            <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #054F72;height: 33px;">
                
            </div>
            <div  class="col-md-8 col-lg-8 col-sm-8 pull-left" style="width: 80%;background: #EB9B2C ;height: 33px;">
                <h5 style="font-weight: bold;font-size: 18px;">Search Job Results ( <?php echo ($endPoint > 0) ? $endPoint : 0 ?> )
                </h5>
                <input type="hidden" id="endPoint" value="<?php echo $endPoint ?>">
            </div>
            <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #054F72;height: 33px;">
                
            </div>
            
        </div>

        <div class="container-full" style="background: white;">
            <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #f8f8f8;height: 30px;">
            </div>

            <div  class="col-md-8 col-lg-8 col-sm-8 pull-left" style="min-height: 675px; margin-bottom: 20px;padding-top: 20px;width: 80%;background: white ;">
                
                <div class="col-lg-12">
                <!-- /.row -->

                    <!-- /.panel -->
                    <div class="panel panel-default" style="border: none;">
                        
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <!--<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead style="background:#69686D;color:white;">
                                    <th style="width:30%;border-bottom-width: 0px;">Job</th>
                                    <th style="width:30%;border-bottom-width: 0px;">Company</th>
                                    <th style="width:30%;border-bottom-width: 0px;">Job Code</th>
                                    <th style="width:30%;border-bottom-width: 0px;">Vacancies</th>
                                </thead>
                                <tbody>
                                <?php //print '<pre>';print_r($content);exit; ?>
                                    <?php foreach($content as $row) { ?>
                                    <tr>
                                        <td><a href="<?php //echo base_url()."job/details/".$row->jobCode ?>"><?php //echo $row->title; ?></a></td>
                                        <td><?php //echo $row->companyName; ?></td>
                                        <td><?php //echo $row->jobCode; ?></td>
                                        <td><?php //echo $row->vaccancies; ?></td>
                                        
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>-->
                            <!-- /.table-responsive -->
                            <h5> You have searched For:- 
                                <?php
                                    //print_r($_SESSION);exit;
                                    if(!empty($_SESSION['searchKeyword'])) {
                                        echo $_SESSION['searchKeyword'];
                                    }

                                    if(!empty($_SESSION['searchKeyword']) && !empty($_SESSION['locationName'])) {
                                        echo ', '.implode(',',$_SESSION['locationName']);
                                    } else if(!empty($_SESSION['locationName'])) {
                                        echo implode(',',$_SESSION['locationName']);
                                    }

                                    if((!empty($_SESSION['searchKeyword']) || !empty($_SESSION['locationName'])) && ($_SESSION['yearsFrom'] > 0)) {
                                        echo ', '.$_SESSION['yearsFrom'].' yr';
                                    } else if($_SESSION['yearsFrom'] >= 0 && $_SESSION['yearsFrom'] !='') {
                                        echo $_SESSION['yearsFrom'].' yr';
                                    } else {
                                        echo '';
                                    }

                                    if((!empty($_SESSION['searchKeyword']) || !empty($_SESSION['locationName']) || ($_SESSION['yearsFrom'] >= 0 && $_SESSION['yearsFrom'] != '')) && ($_SESSION['expectedSalary'] >= 0)) {

                                        if($_SESSION['expectedSalary'] > 0) {
                                            echo ', '.substr($_SESSION['expectedSalary'], 0, -5).' lakh';
                                        } else if($_SESSION['expectedSalary'] == 0 && $_SESSION['expectedSalary'] != '') {
                                            echo ', <1 lakh';
                                        } else {
                                            echo '';
                                        }
                                    } else if($_SESSION['expectedSalary'] >=0 ) {
                                        if($_SESSION['expectedSalary'] > 0) {
                                            echo substr($_SESSION['expectedSalary'], 0, -5).' lakh';
                                        } else if($_SESSION['expectedSalary'] == 0 && $_SESSION['expectedSalary'] != '') {
                                            echo '<1 lakh';
                                        } else {
                                            echo '';
                                        }
                                    }
                                ?>
                            </h5>

                            <div class="col-md-9">
                                <?php if(count($content) > 0) { ?>
                                    <?php foreach($content as $row) { ?>
                                        <div class="content-row">
                                            <a href="<?php echo base_url()."job/details/".$row->jobCode ?>" target="_blank" class="content-anchor">
                                                <span class="desig"><?php echo $row->title; ?></span>
                                                <span class="org"><?php echo $row->companyName; ?></span>
                                                <span class="exp"><i class="fa fa-briefcase" aria-hidden="true"></i>
                                                    <?php
                                                        if(!empty($row->experienceFrom) || !empty($row->experienceTo))
                                                            echo $row->experienceFrom .' - '.$row->experienceTo.' Years';
                                                        else
                                                            echo 'Not Disclosed'; 
                                                    ?>
                                                </span>
                                                <span class="loc"><i class="fa fa-map-marker" aria-hidden="true" style="float: left; margin-top: 3px;"></i><span>
                                                <?php
                                                    if($row->location) {
                                                        $jlocations = array();
                                                        foreach ($row->location as $jlocation) {
                                                            $jlocations[] = $jlocation->city;
                                                        }
                                                        echo implode(', ', $jlocations);
                                                    } else {
                                                        echo 'Not Specified';
                                                    }
                                                ?>
                                                </span></span>
                                                <div class="more">
                                                    <span class="label"><i class="fa fa-graduation-cap" aria-hidden="true"></i>Education </span>
                                                    <div class="desc">
                                                        <span class="skill">
                                                            <?php
                                                                if($row->qualification) {
                                                                    $jqualifications = array();
                                                                    foreach ($row->qualification as $jqualification) {
                                                                        $jqualifications[] = $jqualification->qualificationName;
                                                                    }
                                                                    echo implode(', ', $jqualifications);
                                                                } else {
                                                                    echo 'Not Specified';
                                                                }
                                                            ?>
                                                        <span>
                                                    </div>
                                                </div>

                                                <div class="more">
                                                    <span class="label"><i class="fa fa-file-text" aria-hidden="true"></i></i>Description </span>
                                                    <div class="desc">
                                                        <span class="skill">
                                                            <?php
                                                                if($row->description) {
                                                                    echo substr($row->description, 0, 200) . "...";
                                                                    //echo $row->description;
                                                                } else {
                                                                    echo 'Not Specified';
                                                                }
                                                            ?>
                                                        <span>
                                                    </div>
                                                </div>
                                            </a>
                                            <div class="other_details">
                                                <span itemprop="baseSalary" class="salary  "><i class="fa fa-inr" aria-hidden="true"></i>
                                                <?php
                                                    if(!empty($row->minimumSalary) || !empty($row->maximumSalary))
                                                        echo number_format($row->minimumSalary).' - '.number_format($row->maximumSalary).' P.A.';
                                                    else
                                                        echo 'Not Disclosed'; 
                                                ?>
                                                </span>
                                                <div class="rec_details"> Posted , <span class="date"><?php echo $row->createdAt; ?></span> </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <div id="infinteScrollContent1"></div>
                                <?php } else { echo 'No Job Found'; } ?>
                            </div>
                            <div class="col-md-3">
                                <h4>Refine Your Results</h4>
                                <form method="post" action="<?php echo base_url().'job/searched-jobs';?>">
                                    <div class="form-group">
                                        <label for="freshnessFilter" id="" class="acord_head">Freshness</label>
                                        <select class="form-control" id="freshnessFilter" name="freshnessFilter" style="margin-left: 27px; font-size: 13px;font-weight: 400;">
                                            <option value="" <?php echo $_SESSION['freshnessFilter'] == '' ? 'selected':'' ?>>-Select-</option>
                                            <option value="90" <?php echo $_SESSION['freshnessFilter'] == '90' ? 'selected':'' ?>>Last 3 Months</option>
                                            <option value="45" <?php echo $_SESSION['freshnessFilter'] == '45' ? 'selected':'' ?>>Last 45 Days</option>
                                            <option value="30" <?php echo $_SESSION['freshnessFilter'] == '30' ? 'selected':'' ?>>Last 30 Days</option>
                                            <option value="15" <?php echo $_SESSION['freshnessFilter'] == '15' ? 'selected':'' ?>>Last 15 Days</option>
                                            <option value="7" <?php echo $_SESSION['freshnessFilter'] == '7' ? 'selected':'' ?>>Last 7 Days</option>
                                            <option value="3" <?php echo $_SESSION['freshnessFilter'] == '3' ? 'selected':'' ?>>Last 3 Days</option>
                                            <option value="1" <?php echo $_SESSION['freshnessFilter'] == '1' ? 'selected':'' ?>>Last 1 Days</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label id="salaryFilterLabel" class="acord_head">Salary<i class="fa fa-caret-down" aria-hidden="true" style="margin-left: 7px;"></i></label>
                                        <div class="form-control" style="margin-left: 25px; height: 100px; overflow: auto;" id="salaryFilterCheckbox">
                                            <div class="checkbox">
                                              <label><input type="checkbox" name="salaryRangeFilter[]" value="0to3" <?php echo in_array('0to3', $_SESSION['salaryRangeFilter']) ? 'checked' :'' ;?>>0-3 Lakhs</label>
                                            </div>
                                            <div class="checkbox">
                                              <label><input type="checkbox" name="salaryRangeFilter[]" value="3to6" <?php echo in_array('3to6', $_SESSION['salaryRangeFilter']) ? 'checked' :'' ;?>>3-6 Lakhs</label>
                                            </div>
                                            <div class="checkbox disabled">
                                              <label><input type="checkbox" name="salaryRangeFilter[]" value="6to10" <?php echo in_array('6to10', $_SESSION['salaryRangeFilter']) ? 'checked' :'' ;?>>6-10 Lakhs</label>
                                            </div>
                                            <div class="checkbox disabled">
                                              <label><input type="checkbox" name="salaryRangeFilter[]" value="10to15" <?php echo in_array('10to15', $_SESSION['salaryRangeFilter']) ? 'checked' :'' ;?>>10-15 Lakhs</label>
                                            </div>
                                            <div class="checkbox disabled">
                                              <label><input type="checkbox" name="salaryRangeFilter[]" value="15to25" <?php echo in_array('15to25', $_SESSION['salaryRangeFilter']) ? 'checked' :'' ;?>>15-25 Lakhs</label>
                                            </div>
                                            <div class="checkbox disabled">
                                              <label><input type="checkbox" name="salaryRangeFilter[]" value="25to50" <?php echo in_array('25to50', $_SESSION['salaryRangeFilter']) ? 'checked' :'' ;?>>25-50 Lakhs</label>
                                            </div>
                                            <div class="checkbox disabled">
                                              <label><input type="checkbox" name="salaryRangeFilter[]" value="50to75" <?php echo in_array('50to75', $_SESSION['salaryRangeFilter']) ? 'checked' :'' ;?>>50-75 Lakhs</label>
                                            </div>
                                            <div class="checkbox disabled">
                                              <label><input type="checkbox" name="salaryRangeFilter[]" value="75to100" <?php echo in_array('75to100', $_SESSION['salaryRangeFilter']) ? 'checked' :'' ;?>>75-100 Lakhs</label>
                                            </div>
                                            <div class="checkbox disabled">
                                              <label><input type="checkbox" name="salaryRangeFilter[]" value="101" <?php echo in_array('101', $_SESSION['salaryRangeFilter']) ? 'checked' :'' ;?>>100+ Lakhs</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label id="qualificationFilterLabel" class="acord_head">Education<i class="fa fa-caret-down" aria-hidden="true" style="margin-left: 7px;"></i></label>
                                        <div class="form-control" style="margin-left: 25px; height: 100px; overflow: auto;" id="qualificationFilter">
                                            <?php foreach($qualificationArray as $qualificationRow) { ?>
                                            <div class="checkbox">
                                                <label><input type="checkbox" name="qualification[]" value="<?php echo $qualificationRow->id?>" <?php echo in_array($qualificationRow->id, $_SESSION['qualification']) ? 'checked' :'' ;?>><?php echo $qualificationRow->name?></label>
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="locationFilterName" id="locationFilter" class="acord_head">Location<i class="fa fa-caret-down" aria-hidden="true" style="margin-left: 7px;"></i></label>
                                        <select data-placeholder="Enter the cities you want to work in" multiple="" class="chosen-select location form-control" tabindex="-1" name="locationName[]" id="locationFilterName" data-validation="alphanumeric required">
                                            <option value=""></option>
                                            <?php foreach ($city as $item) : ?>
                                                <option value="<?php echo $item['city']; ?>" <?php echo in_array($item['city'], $_SESSION['locationName']) ? 'selected' :'' ;?>><?php echo $item['city']; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <input type="hidden" name="filterType" value="refine">
                                    <button type="submit" class="btn btn-default" style="margin-left: 22px; background-color: #054F72; color: #fff;" >Refine</button>
                                    </form>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.col-lg-8 -->
                </div>
                <!-- /.row -->
            </div>

            <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #f8f8f8;height: 30px;">
            </div>
        </div>
    <!-- /#page-wrapper -->
    </div>
</div>

<script src="<?php echo base_url(); ?>theme/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url(); ?>theme/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>theme/js/bootbox.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <!--<script src="<?php //echo base_url(); ?>theme/dist/js/sb-admin-2.js"></script>-->
    <script src="<?php echo base_url(); ?>theme/js/formValidation.min.js"></script>
    <script src="<?php echo base_url(); ?>js/min/searched_jobs.min.js"></script>
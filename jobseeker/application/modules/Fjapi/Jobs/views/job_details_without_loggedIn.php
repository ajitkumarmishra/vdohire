<?php

/*$uri_path = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_PATH);
$uri_segments = explode('/', $uri_path);
$event_jobs =  $uri_segments['3'];
//echo "<pre>";print_r($content);die;
$_SESSION['jobCode'] = $content->jobCode;*/
?>
<style>
video::-internal-media-controls-download-button {
    display:none;
}

video::-webkit-media-controls-enclosure {
    overflow:hidden;
}

video::-webkit-media-controls-panel {
    width: calc(100% + 30px); /* Adjust as needed */
}
#tabs > ul li > a > table:hover{
	background:none;
}
.nav > li > a{
	color:black;
}
</style>
<!-- Navigation -->



    
    <div class="container-full" style="background: white;color:white;">
        <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #054F72;height: 38px;">

        </div>
        <div  class="col-md-8 col-lg-8 col-sm-8 pull-left" style="font-size: 17px;color: white;font-weight: bold;width: 80%;background: #EB9B2C ;">
            <div class="btn-group btn-group-lg" style="padding: 10px 0px 10px 0px;">
                <div  class="pull-left" style="width: auto;margin-right:30px;">
                    <button onclick="showJobDescription()" id="showJobDescription"  type="button" class="btn btn-primary" style="font-weight: 600;padding: 13px;background: #054f72;font-size: 17px;border: none;">JOB DESCRIPTION</button>
                </div>
                <div  class=" pull-left" style="    width: auto;margin-right:30px;">
                    <button onclick="ShowAboutCompany()" id="ShowAboutCompany" type="button" class="btn btn-primary" style="font-weight: 600;padding: 13px;background: #d3d3d3;font-size: 17px;border: none;color:black;">ABOUT COMPANY</button>
                </div>

            </div>
            <div style="clear:both"></div>
        </div>
        <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #054F72;height: 38px;">
        </div>
    </div>
    <div style="clear:both"></div>
    <div class="container-full" style="background: white;">
        <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #f8f8f8;    height: 23px;">
        </div>
        <div  class="col-md-8 col-lg-8 col-sm-8 pull-left" style="border:1px solid lightgrey;padding-bottom: 10px;padding-top: 0px;width: 80%;background: white ;">
            <div>
                <div class="col-lg-12" style="    height: 350px;overflow-y: auto;">
                    <div class="col-lg-6" id="showAboutUs" style="display:none;">
                        <h3 style="margin-top:10px;"><?php echo $content->companyName; ?></h3>
                        <?php echo $content->aboutCompany; ?>
                    </div>
                    <div class="col-lg-6" id="showJobDescription1">
                        <h3 style="margin-top:10px;">Job Description</h3>
						<?php echo $content->description; ?>
                        <div style="clear:both"></div>
                        <!--<h3>Introduction Video</h3>
                        <video src="<?php //echo 'https://35.154.53.72/jobseeker/video/test.webm';  ?>" style="    height: 288px;" controls></video>-->
                    </div>
                    <div class="col-lg-6" style="" id="screeningProcess">
                        <h3 style="margin-top:10px;">Application Process</h3>
                        <p><b>The screening process for this position consists of:</b><br>
                        <ul>
                            <li>An Interview, with "<?php if($content->interviewQuestion == 1){ echo $content->interviewQuestion.'" question'; }else{  echo $content->interviewQuestion.'" questions'; } ?> with a total answering time of "<?php 
							$mins = floor($content->interviewDuration/60); 
							$seconds = $content->interviewDuration - $mins*60;
							if($seconds == 0){
								$seconds = "";
							}else{
								$seconds = $seconds." seconds";
							}
							if($mins >1){
								$text = "mins";
							}else{
								$text = "min";
							}
							echo $mins." ".$text." ". $seconds;  ?>"</li>
							
							<?php
							if($content->assessmentDuration == ""){
							}else{
							?>
                            <li>An Assessment, with "<?php if($content->assessmentQuestion == 1){ echo $content->assessmentQuestion.'" question'; }else{  echo $content->assessmentQuestion.'" questions'; } ?> , with multiple-choice answer options, with a total provided time of "<?php 
							$mins = floor($content->assessmentDuration/60); 
							$seconds = $content->assessmentDuration - $mins*60; 
							if($mins >1){
								$text = "mins";
							}else{
								$text = "min";
							}
							echo $mins." ".$text." ". $seconds; ?> seconds"</li>
							<?php } ?>
                        </ul>
                        </p>
                        <br>
                        <p>
                            Click on <b>"Apply"</b> to initiate the FirstJob Digital Screening experience or <b>"Save for Later"</b> if you would like to complete the screening process later. The saved job will be available on your Dashboard. If you click on "Apply", you will be taken through a set of guidelines to ensure you are ready for the Interview. This should not take more than 5 minutes. The Job Interview will start only when you click on the "Start Interview" button. Till that point, you can choose "Save for Later" at any stage. 
                        </p>
                        <!-- /.col-lg-8 -->
                    </div>

                    <!-- /.col-lg-8 -->
                </div>
            </div>



            <!-- /.row -->
            <div class="col-lg-12" style="">
                <!-- /.row -->
                <div class="col-lg-6">
                        <center>
						<input type="hidden" name="alreadyAppliedstatus" id="alreadyAppliedstatus" value="0">
                        <?php //if($this->uri->segment(3) == '524845') { ?>
						<!--<a class="btn btn-primary"  href="javascript://" data-toggle="modal" data-target="#registerModal" class="showmessages" style="    padding: 16px;
                                    font-size: 18px;background: #11AAF0;border: none;   margin-left: 10px;margin-right: 50px;color:white;    text-decoration: none;    padding-left: 60px;padding-right: 63px;">APPLY</a>-->
                        <?php //} else { ?>
						<a class="btn btn-primary"  href="javascript://" data-toggle="modal" data-target="#registerModal" class="showmessages" style="    padding: 16px;
                                    font-size: 18px;background: #11AAF0;border: none;   margin-left: 10px;margin-right: 50px;color:white;    text-decoration: none;    padding-left: 60px;padding-right: 63px;">APPLY</a>
                        <?php //} ?>
                        </center>

                </div>
                
                <!-- /.col-lg-8 -->
                <div class="col-lg-6">
                    <center><button type="button" class="btn btn-primary" style="padding: 16px;font-size: 18px;background: #7FCFB4;border: none;   margin-left:10px;margin-right: 50px;" title="Please LogIn or SignUp to save for this later" disabled>SAVE FOR LATER</button>
                    </center>   
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-8 -->

            </div>
            <!-- /.row -->
            <!-- /.row -->
        </div>
    </div>
    <!-- /#page-wrapper -->
</div>
<!-- Modal -->
  <div class="modal" id="registerModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
			<ul  class="nav nav-pills">
				<li class="active">
					<a  href="#1a" data-toggle="tab">Sign Up</a>
				</li>
				<li><a href="#2a" data-toggle="tab">Login</a>
				</li>
			</ul>
        </div>
        <div class="modal-body">
          <div class="tab-content clearfix">
			  <div class="tab-pane active" id="1a">
		  <form id="Register" method="post" class="form-horizontal fv-form fv-form-bootstrap" style="" novalidate="novalidate"><button type="submit" class="fv-hidden-submit" style="display: none; width: 0px; height: 0px;"></button>
			
          <div class="col-md-12 padd_tp_bt_20">
                    <div class="form-horizontal form_save">
            <div class="form-group has-feedback">
                <label for="inputEmail" class="control-label col-xs-3 required-field ">Email</label>
                <div class="col-xs-9">
                    <input type="text" class="form-control" placeholder="Email *" name="email" id="email" value="<?php echo isset($_SESSION['EMAIL'])?$_SESSION['EMAIL']:""; ?>" data-fv-field="email">
					<i class="form-control-feedback" data-fv-icon-for="email" style="display: none;"></i>
				</div>
            </div>
			
			<div class="form-group has-feedback">
                <label for="inputEmail" class="control-label col-xs-3 required-field ">Name</label>
                <div class="col-xs-9">
                    <input type="text" class="form-control" placeholder="Name*" name="name" data-validation="length" data-validation-length="min6" id="name" value="<?php echo isset($_SESSION['FULLNAME'])?$_SESSION['FULLNAME']:""; ?>" data-fv-field="name"><i class="form-control-feedback" data-fv-icon-for="name" style="display: none;"></i>
                </div>
            </div>
			<div class="form-group has-feedback">
                <label for="inputEmail" class="control-label col-xs-3 required-field ">Mobile</label>
                <div class="col-xs-9">
                    <input type="text" class="form-control" placeholder="Mobile*" name="phone" data-validation="length" data-validation-length="min6" id="phone" value="" data-fv-field="phone"><i class="form-control-feedback" data-fv-icon-for="phone" style="display: none;"></i>
                </div>
            </div>
			
            <div class="form-group has-feedback">
                <label for="inputEmail" class="control-label col-xs-3 required-field ">Password</label>
                <div class="col-xs-9">
                    <input type="password" class="form-control" placeholder="Password*" name="newPassword" data-validation="length" data-validation-length="min6" id="newPassword" value="" data-fv-field="newPassword"><i class="form-control-feedback" data-fv-icon-for="newPassword" style="display: none;"></i>
                </div>
            </div>
            <div class="form-group has-feedback">
                <label for="inputEmail" class="control-label col-xs-3 required-field ">Confirm Password</label>
                <div class="col-xs-9">
                    <input type="password" class="form-control" placeholder="New Password*" name="confirmNewPassword" id="confirmNewPassword" value="" data-fv-field="confirmNewPassword"><i class="form-control-feedback" data-fv-icon-for="confirmNewPassword" style="display: none;"></i>
                </div>
            </div>

            <div class="form-group">
                <div class="col-xs-5 col-xs-offset-3">
                    <input type="hidden" name="token" id="token" value="ecbcd7eaee29848978134beeecdfbc7c">
                    <input type="hidden" name="methodname" id="methodname" value="signUp_v2">
					<input type="hidden" name="fb_id" id="fb_id" value="<?php echo isset($_SESSION['FBID'])?$_SESSION['FBID']:""; ?>">
                    <input type="hidden" name="userId" id="userId" value="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpZCI6IjE5MCJ9.TfL2ffRt2Ik_m13LfWlBi85IWy3hcW98WaSPYDOAkloDAM7jdDT18vmCDPrMBCtulQV_n-1sW2CJEsZRI4SMgQ">
                    <button type="submit" id="registerButton" class="btn btn-default pull-right" onclick="signUp();" style="background: #054F72; color: white;">REGISTER</button>
                </div>
            </div>

        </div>
    </div>
	</form>
				</div>
				<div class="tab-pane" id="2a">
					<form id="login" method="post" class="form-horizontal fv-form fv-form-bootstrap" style="" novalidate="novalidate"><button type="submit" class="fv-hidden-submit" style="display: none; width: 0px; height: 0px;"></button>
			
          <div class="col-md-12 padd_tp_bt_20">
                    <div class="form-horizontal form_save">
            <div class="form-group has-feedback">
                <label for="inputEmail" class="control-label col-xs-3 required-field ">Email</label>
                <div class="col-xs-9">
                    <input type="text" class="form-control" placeholder="Email *" name="emailLogin" id="emailLogin" value="<?php echo isset($_SESSION['EMAIL'])?$_SESSION['EMAIL']:""; ?>" data-fv-field="emailLogin">
					<i class="form-control-feedback" data-fv-icon-for="email" style="display: none;"></i>
				</div>
            </div>
			
			
            <div class="form-group has-feedback">
                <label for="inputEmail" class="control-label col-xs-3 required-field ">Password</label>
                <div class="col-xs-9">
                    <input type="password" class="form-control" placeholder="Password*" name="newPasswordlogin" data-validation="length" data-validation-length="min6" id="newPasswordlogin" value="" data-fv-field="newPasswordlogin"><i class="form-control-feedback" data-fv-icon-for="newPasswordlogin" style="display: none;"></i>
                </div>
            </div>
            

            <div class="form-group">
                <div class="col-xs-5 col-xs-offset-3">
                    <input type="hidden" name="token" id="token" value="ecbcd7eaee29848978134beeecdfbc7c">
                    <input type="hidden" name="methodname" id="methodname" value="signUp_v2">
					<input type="hidden" name="fb_id" id="fb_id" value="<?php echo isset($_SESSION['FBID'])?$_SESSION['FBID']:""; ?>">
                    <input type="hidden" name="userId" id="userId" value="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpZCI6IjE5MCJ9.TfL2ffRt2Ik_m13LfWlBi85IWy3hcW98WaSPYDOAkloDAM7jdDT18vmCDPrMBCtulQV_n-1sW2CJEsZRI4SMgQ">
                    <button type="submit" class="btn btn-default pull-right" onclick="SignIn();" style="background: #054F72; color: white;">LOGIN</button>
                </div>
            </div>

        </div>
    </div>
	</form>
				</div>

        </div>
        <div class="modal-footer" style="    text-align: left;font-size: 12px;">
          <span><span style="color:red;">*</span>New User, Please SignUp</span><br>
          <span><span style="color:red;">*</span>Existing User, Please Login</span>
        </div>
      </div>
      
    </div>
  </div>
  
  
</div>
</div>
<!-- /#wrapper -->



<script src="<?php echo base_url(); ?>theme/vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url(); ?>theme/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>theme/js/bootbox.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->

<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url(); ?>theme/js/formValidation.min.js"></script>

<script src="<?php echo base_url(); ?>js/job_details_without_loggedIn.js"></script>
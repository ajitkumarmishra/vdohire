<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Users Controller
 * Description : Used to handle all the jobseeker related data
 * @author Synergy
 * @createddate : April 3, 2016
 * @modificationlog : Initializing the controlelr
 * @change on Mar 24, 2017
 */
class Jobs extends MY_Controller {

    public $allNotificationList = array();
    /**
     * Responsable for inherit the parent connstructor
     * @return void
     */
    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('cookie');
		$this->load->helper('jwt');
		$this->load->helper('fj');
		
        $this->getEditProfileData();
        //Use to get Locations for search Job
        $this->searchLocationList = getLocationCities();

        $userRealData = $this->session->get_userdata();
        //print_r($userRealData);exit;

        if (isset($userRealData['userId'])) {
            /* Get Notification List for user */
            $userId = $userRealData['userId'];

            $statictoken = 'ecbcd7eaee29848978134beeecdfbc7c';
            $base_url = 'https://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
            $json = array("token" => $statictoken, "methodname" => 'getUserNotifications', 'userId' => $userId);                                                                    
            $data_string = json_encode($json);                                                                         
                                                                                                                                 
            $ch = curl_init($base_url);                                                                      
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
                'Content-Type: application/json',                                                                                
                'Content-Length: ' . strlen($data_string))                                                                       
            );                                                                                                                   
                                                                                                                                 
            $result = curl_exec($ch);
            $allResult = json_decode($result);
            $this->allNotificationList = $allResult->data;

            /* End of get notification list fo user */
        }
		
    }

    /**
     * Function Naem : index
     * Discription : Use to display login page for job seeker
     * @author Synergy
     * @param none
     * @return render data into main view page
     */
    function invitation() {
        
        $invitationId = $this->uri->segment('3');
        $session = $this->session->get_userdata();
        $_SESSION['invitationId'] = $invitationId;
       
		$statictoken = 'ecbcd7eaee29848978134beeecdfbc7c';
        $base_url = 'https://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
        $AlreadyLoggedIn = 0;
        if(isset($_SESSION['userEmail']) && $_SESSION['userEmail'] != ""){
                $AlreadyLoggedIn = 1;
        }
		//get invitation details
		$json = array("token" => $statictoken, "methodname" => 'getWebInterviewInvites', 'invitationId' => $invitationId);                                                                    
        $data_string = json_encode($json);                
		
        $ch = curl_init($base_url);                                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data_string))                                                                       
        );                                                                                                                  
                                                                                                                             
        $result = curl_exec($ch);
        curl_close ($ch);
        $allResult = json_decode($result);
        $invitaionDetails = $allResult->data;
        $data['invitationDetails'] = $invitaionDetails[0];
        $_SESSION['userEmail'] = $data['invitationDetails']->email;
        $_SESSION['jobCode'] = $data['invitationDetails']->fjCode;
		
		//check job code 
        $data = array("token" => $statictoken, "methodname" => 'verifyJob', 'referrenceJobId' => $_SESSION['jobCode'], 'searchBy' => 'job','deviceUniqueId' => '1111');                                                                    
        $data_string = json_encode($data);                                                                                   
                                                                                                                             
        $ch = curl_init($base_url);                                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data_string))                                                                       
        );                                                                                                                  
                                                                                                                             
        $result = curl_exec($ch);
        $allResult = json_decode($result);
        $allContent = $allResult->data;
        $data['content'] = $allContent[0];
        
		//registrating the user using the email 
		$json = array("token" => $statictoken, "methodname" => 'virtualSignUp', 'email' => $_SESSION['userEmail']);                                                                    
        $data_string = json_encode($json);                
		
        $ch = curl_init($base_url);                                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data_string))                                                                       
        );                                                                                                                  
                                                                                                                             
        $result1 = curl_exec($ch);
		curl_close ($ch);
        $allResult1 = json_decode($result1);
		$userDetailArray = $allResult1->data;
		$_SESSION['userId'] = $userDetailArray[0]->token;
		
		//check job expire
		$checkAppliedJobjson = array("token" => $statictoken, "methodname" => 'checkAppliedJob', 'userId' => $_SESSION['userId'], 'jobCode' => $_SESSION['jobCode']);                                                                    
        $data_string = json_encode($checkAppliedJobjson);                
		
        $ch = curl_init($base_url);                                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data_string))                                                                       
        );                                                                                                                  
                                                                                                                             
        $result = curl_exec($ch);
        curl_close ($ch);
        $allResult = json_decode($result);
        $data['checkAppliedJob'] = $allResult->code;
		
		
		//send message to the user
        if($AlreadyLoggedIn == 0){
                $messageText = "Dear ".$basic->fullname.",  <br><br>

Welcome to FirstJob family. You have now joined thousands of jobseekers who have used FirstJob to find and interview for their dream job from the comfort of their home. At FirstJob we are committed to help jobseekers by making interview process convenient and comfortable. Now you can apply for jobs and submit your interviews at Anytime, from Anywhere. <br><br>

To keep yourself updated with latest jobs and career news we recommend you to download FirstJob android app. You can download the app from Google play store (hyperlink). <br><br>

Next you can attach your latest resume and start submitting interviews for open opportunities on the platform. In case if you are stuck or face any problem while going through screening process – do reach out to us at hello@firstjob.co.in 

Wishing you great luck in your job search!<br><br>

Thanks,<br>
Team FirstJob<br>
Get hired Anytime, Anywhere
";
                $subject = "Welcome";
                $parentMessageId = ""; 
                $adminSynergy = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpZCI6MzIzfQ.hMyoB8qNuk1yo7DR3jcN1NAsW2YVnwFuFtsTkRqIc9FilYzITzmL022Mxa8-45_wg2fNV12TUOVnFbM1-WiOxQ';
                $jsonMessage = array("token" => $statictoken, "methodname" => 'sendMessageToUsers', 'toId' => $_SESSION['userId'], "messageText" => $messageText, "messageSubject" => $subject, 'parentMessageId' => $parentMessageId, 'userId' =>  $adminSynergy);                                                                    
                $data_string = json_encode($jsonMessage);                

                $ch = curl_init($base_url);                                                                      
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
                curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
                        'Content-Type: application/json',                                                                                
                        'Content-Length: ' . strlen($data_string))                                                                       
                );                                                                                                                  

                $result = curl_exec($ch);
                curl_close ($ch);
        }
        $data['city'] = $this->searchLocationList;
        $data['allNotificationList'] = $this->allNotificationList;
        $data['invitationId'] = $invitationId;
        $data['main_content'] = 'invitation';
        $this->load->view('fj-mainpage', $data);
		
    }

    /**
     * Function Naem : index
     * Discription : Use to display login page for job seeker
     * @author Synergy
     * @param none
     * @return render data into main view page
     */
    function event_jobs() {
		
		$this->load->model('coreapi_model','CORE_MODEL');
		$eventId = $this->CORE_MODEL->getJwtValue($this->uri->segment('3'));
		
		
        $statictoken = 'ecbcd7eaee29848978134beeecdfbc7c';
        $base_url = 'https://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
		$session = $this->session->get_userdata();
		
        //if(isset($_SESSION['userId'])) {
            $this->load->library('session');
            $this->load->helper('cookie');
            //$eventId = $this->uri->segment('3');
            $session = $this->session->get_userdata();
            $_SESSION['invitationId'] = $invitationId;
            $token =  get_cookie('token');
            $userEmail =  get_cookie('userEmail');
            $userName =  get_cookie('userName');
            $userId =  get_cookie('userId');

            $data = array("token" => $statictoken, "methodname" => 'getJobsForEvent', 'eventId' => $eventId);                                                                    
            $data_string = json_encode($data);                                                                                   
                                                                                                                                 
            $ch = curl_init($base_url);                                                                      
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
                'Content-Type: application/json',                                                                                
                'Content-Length: ' . strlen($data_string))                                                                       
            );                                                                                                                   
                                                                                                                                 
            $result = curl_exec($ch);
            //print_r($result);exit;
            $allResult = json_decode($result);
            $allContent = $allResult->eventList;
            $totalVacancies = $allResult->totalVacancies;
            


            //Get jobseeker event
            $json = array("token" => $statictoken, "methodname" => 'getJobSeekerEvent');                                                                    
            $data_string_event = json_encode($json);                                                                                   
                                                                                                                                 
            $ch_event = curl_init($base_url);                                                                      
            curl_setopt($ch_event, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
            curl_setopt($ch_event, CURLOPT_POSTFIELDS, $data_string_event);                                                                  
            curl_setopt($ch_event, CURLOPT_RETURNTRANSFER, true);                                                                      
            curl_setopt($ch_event, CURLOPT_HTTPHEADER, array(                                                                          
                'Content-Type: application/json',                                                                                
                'Content-Length: ' . strlen($data_string_event))                                                                       
            );                                                                                                                   
                                                                                                                                 
            $eventResult = curl_exec($ch_event);
            $eventAllResult = json_decode($eventResult);
			
			//set visitor log
			$ip =  $_SERVER['REMOTE_ADDR'] ;
			$page = "jobDetail";
			$useragent = $_SERVER ['HTTP_USER_AGENT'];
			$cookie = $_COOKIE['visitor_cookie'];
			$userEmail = isset($_SESSION['userEmail'])?$_SESSION['userEmail']:"";
			
			if($cookie == ""){
				setcookie('visitor_cookie', $cookie_value, time() + (7200), "/"); // 86400 = 1 day
			}
			$jsonVisitor = array("token" => $statictoken, "methodname" => 'userVisitLog', "ip" => $ip, "useragent" => $useragent, "page" => $page,  "cookie" => $cookie, "userEmail" => $userEmail);
			
			$data_string_visitor = json_encode($jsonVisitor);                                                                                   
																																 
			$ch_visitor = curl_init($base_url);                                                                      
			curl_setopt($ch_visitor, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
			curl_setopt($ch_visitor, CURLOPT_POSTFIELDS, $data_string_visitor);                                                                  
			curl_setopt($ch_visitor, CURLOPT_RETURNTRANSFER, true);                                                                      
			curl_setopt($ch_visitor, CURLOPT_HTTPHEADER, array(                                                                          
				'Content-Type: application/json',                                                                                
				'Content-Length: ' . strlen($data_string_visitor))                                                                       
			);                                                                                                                   
			$curlResult = curl_exec($ch_visitor);
			
			
			
			////Get event application details
            $json = array("token" => $statictoken, "methodname" => 'getJobFairApplicationData', 'eventId' => $eventId);                                                                    
            $data_string_event = json_encode($json);                                                                                   
                                                                                                                                 
            $ch_app = curl_init($base_url);                                                                      
            curl_setopt($ch_app, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
            curl_setopt($ch_app, CURLOPT_POSTFIELDS, $data_string_event);                                                                  
            curl_setopt($ch_app, CURLOPT_RETURNTRANSFER, true);                                                                      
            curl_setopt($ch_app, CURLOPT_HTTPHEADER, array(                                                                          
                'Content-Type: application/json',                                                                                
                'Content-Length: ' . strlen($data_string_event))                                                                       
            );                                                                                                                   
                                                                                                                                 
            $eventAppResult = curl_exec($ch_app);
            $eventAppResult = json_decode($eventAppResult);
			$totalApplicationReceived = $eventAppResult->totalApplicationReceived;
            $totalShortlistedApplication = $eventAppResult->totalShortlistedApplication;
			
            $eventAllContent = $eventAllResult->eventList;
            $data['eventContent'] = $eventAllContent[0];
            //End of job seeker event
			$data['eventDate'] = $this->config->item('eventDate');
			$data['eventStarted'] = $this->config->item('eventStarted');
			$data['showShortlisted'] = $this->config->item('showShortlisted');
            $data['content'] = $allContent;
            $data['totalApplicationReceived'] = $totalApplicationReceived;
            $data['totalShortlistedApplication'] = $totalShortlistedApplication;
            $data['token'] = $userId;
            $data['totalVacancies'] = $totalVacancies;
            $data['eventId'] = $eventId;
            $data['userName'] = $userName;
            $data['email'] = $userEmail;
            $data['invitationId'] = $invitationId;
            $data['city'] = $this->searchLocationList;
            $data['allNotificationList'] = $this->allNotificationList;
            $data['main_content'] = 'event-jobs';
            $this->load->view('fj-mainpage', $data);
        /*} else {
            redirect(base_url());
        }*/
    }
	
	function hot_jobs() {
		
		$this->load->model('coreapi_model','CORE_MODEL');
		$eventId = 1;//$this->CORE_MODEL->getJwtValue($this->uri->segment('3'));
		
		
        $statictoken = 'ecbcd7eaee29848978134beeecdfbc7c';
        $base_url = 'https://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
		$session = $this->session->get_userdata();
		
        //if(isset($_SESSION['userId'])) {
            $this->load->library('session');
            $this->load->helper('cookie');
            //$eventId = $this->uri->segment('3');
            $session = $this->session->get_userdata();
            $_SESSION['invitationId'] = $invitationId;
            $token =  get_cookie('token');
            $userEmail =  get_cookie('userEmail');
            $userName =  get_cookie('userName');
            $userId =  get_cookie('userId');

            $data = array("token" => $statictoken, "methodname" => 'getJobsForEvent', 'eventId' => $eventId);                                                                    
            $data_string = json_encode($data);                                                                                   
                                                                                                                                 
            $ch = curl_init($base_url);                                                                      
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
                'Content-Type: application/json',                                                                                
                'Content-Length: ' . strlen($data_string))                                                                       
            );                                                                                                                   
                                                                                                                                 
            $result = curl_exec($ch);
            //print_r($result);exit;
            $allResult = json_decode($result);
            $allContent = $allResult->eventList;
            $totalVacancies = $allResult->totalVacancies;
            


            //Get jobseeker event
            $json = array("token" => $statictoken, "methodname" => 'getJobSeekerEvent');                                                                    
            $data_string_event = json_encode($json);                                                                                   
                                                                                                                                 
            $ch_event = curl_init($base_url);                                                                      
            curl_setopt($ch_event, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
            curl_setopt($ch_event, CURLOPT_POSTFIELDS, $data_string_event);                                                                  
            curl_setopt($ch_event, CURLOPT_RETURNTRANSFER, true);                                                                      
            curl_setopt($ch_event, CURLOPT_HTTPHEADER, array(                                                                          
                'Content-Type: application/json',                                                                                
                'Content-Length: ' . strlen($data_string_event))                                                                       
            );                                                                                                                   
                                                                                                                                 
            $eventResult = curl_exec($ch_event);
            $eventAllResult = json_decode($eventResult);
			
			//set visitor log
			$ip =  $_SERVER['REMOTE_ADDR'] ;
			$page = "jobDetail";
			$useragent = $_SERVER ['HTTP_USER_AGENT'];
			$cookie = $_COOKIE['visitor_cookie'];
			$userEmail = isset($_SESSION['userEmail'])?$_SESSION['userEmail']:"";
			
			if($cookie == ""){
				setcookie('visitor_cookie', $cookie_value, time() + (7200), "/"); // 86400 = 1 day
			}
			$jsonVisitor = array("token" => $statictoken, "methodname" => 'userVisitLog', "ip" => $ip, "useragent" => $useragent, "page" => $page,  "cookie" => $cookie, "userEmail" => $userEmail);
			
			$data_string_visitor = json_encode($jsonVisitor);                                                                                   
																																 
			$ch_visitor = curl_init($base_url);                                                                      
			curl_setopt($ch_visitor, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
			curl_setopt($ch_visitor, CURLOPT_POSTFIELDS, $data_string_visitor);                                                                  
			curl_setopt($ch_visitor, CURLOPT_RETURNTRANSFER, true);                                                                      
			curl_setopt($ch_visitor, CURLOPT_HTTPHEADER, array(                                                                          
				'Content-Type: application/json',                                                                                
				'Content-Length: ' . strlen($data_string_visitor))                                                                       
			);                                                                                                                   
			$curlResult = curl_exec($ch_visitor);
			
			
			
			////Get event application details
            $json = array("token" => $statictoken, "methodname" => 'getJobFairApplicationData', 'eventId' => $eventId);                                                                    
            $data_string_event = json_encode($json);                                                                                   
                                                                                                                                 
            $ch_app = curl_init($base_url);                                                                      
            curl_setopt($ch_app, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
            curl_setopt($ch_app, CURLOPT_POSTFIELDS, $data_string_event);                                                                  
            curl_setopt($ch_app, CURLOPT_RETURNTRANSFER, true);                                                                      
            curl_setopt($ch_app, CURLOPT_HTTPHEADER, array(                                                                          
                'Content-Type: application/json',                                                                                
                'Content-Length: ' . strlen($data_string_event))                                                                       
            );                                                                                                                   
                                                                                                                                 
            $eventAppResult = curl_exec($ch_app);
            $eventAppResult = json_decode($eventAppResult);
			$totalApplicationReceived = $eventAppResult->totalApplicationReceived;
            $totalShortlistedApplication = $eventAppResult->totalShortlistedApplication;
			
            $eventAllContent = $eventAllResult->eventList;
            $data['eventContent'] = $eventAllContent[0];
            //End of job seeker event
			$data['eventDate'] = $this->config->item('eventDate');
			$data['eventStarted'] = $this->config->item('eventStarted');
			$data['showShortlisted'] = $this->config->item('showShortlisted');
            $data['content'] = $allContent;
            $data['totalApplicationReceived'] = $totalApplicationReceived;
            $data['totalShortlistedApplication'] = $totalShortlistedApplication;
            $data['token'] = $userId;
            $data['totalVacancies'] = $totalVacancies;
            $data['eventId'] = $eventId;
            $data['userName'] = $userName;
            $data['email'] = $userEmail;
            $data['invitationId'] = $invitationId;
            $data['city'] = $this->searchLocationList;
            $data['allNotificationList'] = $this->allNotificationList;
            $data['main_content'] = 'hot-jobs';
            $this->load->view('fj-mainpage-withMenu', $data);
			
        /*} else {
            redirect(base_url());
        }*/
    }
	
	function job_details() {
		

		$_SESSION['invitationId'] = "";
        $statictoken = 'ecbcd7eaee29848978134beeecdfbc7c';
        $base_url = 'https://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';

        $this->load->library('session');
        $this->load->helper('cookie');
        $jobCode = $this->uri->segment('3');
        $session = $this->session->get_userdata();
		$session = $this->session->get_userdata();
		
        $arrayData = array("token" => $statictoken, "methodname" => 'verifyJob', 'referrenceJobId' => $jobCode, 'searchBy' => 'job','deviceUniqueId' => '1111');                                                                    
        $data_string = json_encode($arrayData);                                                                                   
                                                                                                                 
        $chverifyJob = curl_init($base_url);                                                                      
        curl_setopt($chverifyJob, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($chverifyJob, CURLOPT_POSTFIELDS, $data_string);                                                                  
        curl_setopt($chverifyJob, CURLOPT_RETURNTRANSFER, true);                                                                      
        curl_setopt($chverifyJob, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data_string))                                                                       
        );                                                                                                                  
                                                                                                                             
        $resultVerifyJob = curl_exec($chverifyJob);
        $allResult = json_decode($resultVerifyJob);
        $allContent = $allResult->data;
		$_SESSION['jobCode'] = $jobCode;
		
		//set visitor log
		$ip =  $_SERVER['REMOTE_ADDR'] ;
		$page = "jobDetail";
		$useragent = $_SERVER ['HTTP_USER_AGENT'];
		$cookie = $_COOKIE['visitor_cookie'];
		$userEmail = isset($_SESSION['userEmail'])?$_SESSION['userEmail']:"";
		
		if($cookie == ""){
			setcookie('visitor_cookie', $cookie_value, time() + (7200), "/"); // 86400 = 1 day
		}
		$jsonVisitor = array("token" => $statictoken, "methodname" => 'userVisitLog', "ip" => $ip, "useragent" => $useragent, "page" => $page,  "cookie" => $cookie, "userEmail" => $userEmail);
		
		$data_string_visitor = json_encode($jsonVisitor);                                                                                   
																															 
		$ch_visitor = curl_init($base_url);                                                                   
		curl_setopt($ch_visitor, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
		curl_setopt($ch_visitor, CURLOPT_POSTFIELDS, $data_string_visitor);                                                                  
		curl_setopt($ch_visitor, CURLOPT_RETURNTRANSFER, true);                                                                      
		curl_setopt($ch_visitor, CURLOPT_HTTPHEADER, array(                                                                          
			'Content-Type: application/json',                                                                                
			'Content-Length: ' . strlen($data_string_visitor))                                                                       
		);                                                                                                                   
		$curlResult = curl_exec($ch_visitor);
		curl_close ($ch_visitor);
		
		//check job expire
		$checkAppliedJobjson = array("token" => $statictoken, "methodname" => 'checkAppliedJob', 'userId' => $_SESSION['userId'], 'jobCode' => $jobCode);                                                                    
        $data_stringApp = json_encode($checkAppliedJobjson);                
		
        $chApp = curl_init($base_url);                                                                      
        curl_setopt($chApp, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($chApp, CURLOPT_POSTFIELDS, $data_stringApp);                                                                  
        curl_setopt($chApp, CURLOPT_RETURNTRANSFER, true);                                                                      
        curl_setopt ($chApp, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($chApp, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data_stringApp))                                                                       
        );                                                                                                                  
                                                                                                                             
        $result = curl_exec($chApp);
		
		curl_close ($chApp);
		$allResultApp = json_decode($result);
        $data['checkAppliedJob'] = $allResultApp->code;
		
		//check jobProcessstatus
		$checkAppliedJobStepjson = array("token" => $statictoken, "methodname" => 'getUserJobProcessStatus', 'userId' => $_SESSION['userId'], 'jobCode' => $jobCode);                                                                    
        $dataStepstring = json_encode($checkAppliedJobStepjson);                
		
        $chProcess = curl_init($base_url);                                                                      
        curl_setopt($chProcess, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($chProcess, CURLOPT_POSTFIELDS, $dataStepstring);                                                                  
        curl_setopt($chProcess, CURLOPT_RETURNTRANSFER, true);                                                                      
        curl_setopt ($chProcess, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($chProcess, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($dataStepstring))                                                                       
        );                                                                                                                  
                                                                                                                             
        $resultApplied = curl_exec($chProcess);
		
		curl_close ($chProcess);
		$allAppliedResult = json_decode($resultApplied);
        $data['getUserJobProcessStatus'] = $allAppliedResult->status;
		
		//check save Jobs
		$checkAppliedJobjsonSave = array("token" => $statictoken, "methodname" => 'getSavedJobStatus', 'userId' => $_SESSION['userId'], 'jobCode' => $jobCode);                                                                    
        $data_stringSave = json_encode($checkAppliedJobjsonSave);                
		
        $chSave = curl_init($base_url);                                                                      
        curl_setopt($chSave, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($chSave, CURLOPT_POSTFIELDS, $data_stringSave);                                                                  
        curl_setopt($chSave, CURLOPT_RETURNTRANSFER, true);                                                                      
        curl_setopt ($chSave, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($chSave, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data_stringSave))                                                                       
        );                                                                                                                  
                                                                                                                             
        $resultSave = curl_exec($chSave);
		
		curl_close ($chSave);
		$allResultSave = json_decode($resultSave);
        $data['checkSavedJobs'] = $allResultSave->code;
		
		
		
        $data['content'] = $allContent[0];
        $data['token'] = $userId;
        $data['userName'] = $userName;
        $data['email'] = $userEmail;
        $data['invitationId'] = $invitationId;
        $data['city'] = $this->searchLocationList;
        $data['allNotificationList'] = $this->allNotificationList;
        if(isset($_SESSION['userId']) && $_SESSION['userId'] != ""){
			
			$data['main_content'] = 'job_details';
		}else{
			$data['main_content'] = 'job_details_without_loggedIn';
		}
        $this->load->view('fj-mainpage-withoutMenu', $data);
    }
	
	
    function searched_jobs() {
        $qualicationArray = $this->db->query("SELECT DISTINCT(name), id FROM fj_courses ORDER BY name ASC")->result();

        $token = $_SESSION['token'];
        $methodname = 'searchJobs';

        if($_POST) {

            if(isset($_POST['filterType']) && ($_POST['filterType'] == 'refine')) {
                if(isset($_POST['freshnessFilter'])) {
                    $freshnessFilter = $_POST['freshnessFilter'];
                    $_SESSION['freshnessFilter'] = $freshnessFilter;
                } else {
                    $freshnessFilter = '';
                    $_SESSION['freshnessFilter'] = $freshnessFilter;
                }

                if(isset($_POST['salaryRangeFilter'])) {
                    $salaryRangeFilter = $_POST['salaryRangeFilter'];
                    $_SESSION['salaryRangeFilter'] = $salaryRangeFilter;
                } else {
                    $salaryRangeFilter = '';
                    $_SESSION['salaryRangeFilter'] = $salaryRangeFilter;
                }

                if(isset($_POST['qualification'])) {
                    $qualification = $_POST['qualification'];
                    $_SESSION['qualification'] = $qualification;
                } else {
                    $qualification = '';
                    $_SESSION['qualification'] = $qualification;
                }

                if(isset($_POST['locationName'])) {
                    $locationName = $_POST['locationName'];
                    $_SESSION['locationName'] = $locationName;
                } else {
                    $locationName = '';
                    $_SESSION['locationName'] = $locationName;
                }

                $searchKeyword = $_SESSION['searchKeyword'];
                $yearsFrom = $_SESSION['yearsFrom'];
                $expectedSalary = $_SESSION['expectedSalary'];

                $startPoint = $_SESSION['startPoint'];

            } else if(isset($_POST['filterType']) && ($_POST['filterType'] == 'ajax')) {
                $searchKeyword = $_SESSION['searchKeyword'];
                $locationName = $_SESSION['locationName'];
                $yearsFrom = $_SESSION['yearsFrom'];
                $expectedSalary = $_SESSION['expectedSalary'];
                $freshnessFilter = $_SESSION['freshnessFilter'];
                $salaryRangeFilter = $_SESSION['salaryRangeFilter'];
                $qualification = $_SESSION['qualification'];
                $startPoint = $_POST['startPoint']*25;

                $_SESSION['startPoint'] = $startPoint;
                //$jobsLimit = $_SESSION['jobsLimit'];
            } else {
                $searchKeyword = $_POST['searchKeyword'];
                $_SESSION['searchKeyword'] = $searchKeyword;

                $locationName = $_POST['locationName'];
                $_SESSION['locationName'] = $locationName;

                $yearsFrom = $_POST['yearsFrom'];
                $_SESSION['yearsFrom'] = $yearsFrom;

                $expectedSalary = $_POST['expectedSalary'];
                $_SESSION['expectedSalary'] = $expectedSalary;

                $freshnessFilter = '';
                $_SESSION['freshnessFilter'] = '';

                $salaryRangeFilter = '';
                $_SESSION['salaryRangeFilter'] = '';

                $qualification = '';
                $_SESSION['qualification'] = '';

                $startPoint = $_POST['startPoint'];
                $_SESSION['startPoint'] = $startPoint;

                //print '<pre>';print_r($_SESSION);exit;
                //$jobsLimit = $_POST['jobsLimit'];
                //$_SESSION['jobsLimit'] = $jobsLimit;
            }
        } else if($_GET) {
            $searchKeyword = $_GET['searchKeyword'];
            $_SESSION['searchKeyword'] = $searchKeyword;

            if(!empty($_GET['locationName'])) {
                $locationName = explode(',', $_GET['locationName']);
                $_SESSION['locationName'] = $locationName;
            } else {
                $locationName = '';
                $_SESSION['locationName'] = $locationName;
            }
            

            $yearsFrom = $_GET['yearsFrom'];
            $_SESSION['yearsFrom'] = $yearsFrom;

            $expectedSalary = $_GET['expectedSalary'];
            $_SESSION['expectedSalary'] = $expectedSalary;

            $freshnessFilter = '';
            $_SESSION['freshnessFilter'] = '';

            $salaryRangeFilter = '';
            $_SESSION['salaryRangeFilter'] = '';

            $qualification = '';
            $_SESSION['qualification'] = '';

            $startPoint = 0;
            $_SESSION['startPoint'] = $startPoint;
            //print_r($_SESSION);exit;
        } else {
            $searchKeyword = $_SESSION['searchKeyword'];
            $locationName = $_SESSION['locationName'];
            $yearsFrom = $_SESSION['yearsFrom'];
            $expectedSalary = $_SESSION['expectedSalary'];
            $freshnessFilter = $_SESSION['freshnessFilter'];
            $salaryRangeFilter = $_SESSION['salaryRangeFilter'];
            $qualification = $_SESSION['qualification'];
            $startPoint = $_SESSION['startPoint'];
            //$jobsLimit = $_SESSION['jobsLimit'];
        }


        if($_SESSION['userId']) {
            $userId = $_SESSION['userId'];
        } else {
            $userId = '';
        }
        $base_url = 'https://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';

        $searchedJobJson = array("token" => $token, "methodname" => $methodname, 'userId' => $userId, 'searchKeyword' => $searchKeyword, 'locationName' => $locationName, 'yearsFrom' => $yearsFrom, 'expectedSalary' => $expectedSalary, 'freshnessFilter' => $freshnessFilter, 'salaryRangeFilter' => $salaryRangeFilter, 'qualification' => $qualification, 'startPoint' => $startPoint, 'jobsLimit' => 25, 'userId' => $userId);

        //print '<pre>';print_r($searchedJobJson);exit;
        $data_string = json_encode($searchedJobJson);
        //print_r($data_string);exit;              

        $ch = curl_init($base_url);                                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data_string))                                                                    
        );                                                                                                                  
                                                                                                                             
        $result = curl_exec($ch);
       
        
        curl_close ($ch);
        //print_r($result);exit;
        $allResult = json_decode($result);
        $content = $allResult->data;
        //print_r($content);exit;
        
        if(isset($_POST['filterType']) && ($_POST['filterType'] == 'ajax')) {
            $ajaxDataContent = '';
            foreach ($content as $row) {
                if(!empty($row->experienceFrom) || !empty($row->experienceTo))
                    $actualExperiance = $row->experienceFrom .' - '.$row->experienceTo.' Years';
                else
                    $actualExperiance = 'Not Disclosed';

                if($row->location) {
                    $jlocations = array();
                    foreach ($row->location as $jlocation) {
                        $jlocations[] = $jlocation->city;
                    }
                    $actualLocation = implode(', ', $jlocations);
                } else {
                    $actualLocation = 'Not Specified';
                }

                if($row->qualification) {
                    $jqualifications = array();
                    foreach ($row->qualification as $jqualification) {
                        $jqualifications[] = $jqualification->qualificationName;
                    }
                    $actualQualification = implode(', ', $jqualifications);
                } else {
                    $actualQualification  = 'Not Specified';
                }

                if($row->description) {
                    $actualDescription = substr($row->description, 0, 200) . "...";
                } else {
                    $actualDescription = 'Not Specified';
                }

                if(!empty($row->minimumSalary) || !empty($row->maximumSalary))
                    $actualSalary = number_format($row->minimumSalary).' - '.number_format($row->maximumSalary).' P.A.';
                else
                    $actualSalary = 'Not Disclosed';

                $ajaxDataContent .= '<div class="content-row">
                    <a href="'.base_url().'job/details/'.$row->jobCode.'" target="_blank" class="content-anchor">
                        <span class="desig">'.$row->title.'</span>
                        <span class="org">'.$row->companyName.'</span>
                        <span class="exp"><i class="fa fa-briefcase" aria-hidden="true"></i>
                           '.$actualExperiance.'
                        </span>
                        <span class="loc"><i class="fa fa-map-marker" aria-hidden="true" style="float: left; margin-top: 3px;"></i><span>
                        '.$actualLocation.'
                        </span></span>
                        <div class="more">
                            <span class="label"><i class="fa fa-graduation-cap" aria-hidden="true"></i>Education </span>
                            <div class="desc">
                                <span class="skill">
                                    '.$actualQualification.'
                                <span>
                            </div>
                        </div>

                        <div class="more">
                            <span class="label"><i class="fa fa-file-text" aria-hidden="true"></i></i>Description </span>
                            <div class="desc">
                                <span class="skill">
                                    '.$actualDescription.'
                                <span>
                            </div>
                        </div>
                    </a>
                    <div class="other_details">
                        <span itemprop="baseSalary" class="salary  "><i class="fa fa-inr" aria-hidden="true"></i>
                            '.$actualSalary.'
                        </span>
                        <div class="rec_details"> Posted , <span class="date">'.$row->createdAt.'</span> </div>
                    </div>
                </div>';
            }
            echo $ajaxDataContent;exit;
        } else {
            $data['city'] = $this->searchLocationList;
            $data['qualificationArray'] = $qualicationArray;
            $data['content'] = $content;
            $data['endPoint'] = $allResult->endPoint;
            //print '<pre>';print_r($data['content']);exit;
            $data['city'] = $this->searchLocationList;
            $data['allNotificationList'] = $this->allNotificationList;
            $data['main_content'] = 'searched-jobs';
            $this->load->view('fj-mainpage-withMenu', $data);
        }
    }

}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Interviewapi Model
 * Description : Handle all the CRUD operation for interviews
 * @author Synergy
 * @createddate : Nov 28, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 16, 2017
 */

/*
#############################################################
    -------------------- Api Listing --------------------
    =========USER INTERVIEW RELATED=========
        01. userExists
        02. jobExists
        03. SETquestionExists
        04. questionExists
        05. answerExists
        06. findSETquestionList
        07. findQuestionDetail
        08. submitQuestionAnswer
        09. pendingQuestionAnswer
        10. interviewQuestionList
        11.     interviewQuestionDetail
        12.     interviewQuestionAnswer
        13. assessmentQuestionList
        14.     assessmentQuestionDetail
        15.     assessmentQuestionAnswer
        16. answerScreen
#############################################################
*/
class interviewapi_model extends CI_Model {

    /**
     * Defining static variables
     */
    static $interview                   = 'interview';
    static $assessment                  = 'assessment';
    static $interviewQuestionTable      = 'fj_interviewQuestions';
    static $assessmentQuestionTable     = 'fj_assessmentQuestions';
    static $interviewFieldId            = 'interviewId';
    static $assessmentFieldId           = 'assessmentId';
    
	
	function __construct() {
        parent::__construct();        
        $this->load->model('coreapi_model', 'coreModel');
    }
    //-------------------------------------------------------------------------//    
    //--------- Queries Used In Interview/AssessmentAPI Model Starts ----------//
    //-------------------------------------------------------------------------//

    /**
     * Description : Use to check applied Job Exists Or Not
     * Author : Synergy
     * @param int $id, int $fjJobId
     * @return array of data 
     */
    protected function appliedJobExists($id, $fjJobId) {
        $appliedJobRow    = $this->coreModel->queryRowArray("SELECT id FROM fj_userJob WHERE userId='$id' AND jobId='$fjJobId' LIMIT 0,1");
        return $appliedJobRow;
    }

    /**
     * Description : Use to check Interview Question Record Exists Or Not
     * Author : Synergy
     * @param int $interviewId, int $questionId, string $setType
     * @return array of data 
     */
    protected function SETquestionExists($interviewId, $questionId, $setType) {
        $tableName  = ($setType==self::$interview?self::$interviewQuestionTable:self::$assessmentQuestionTable);
        $fieldId    = ($setType==self::$interview?self::$interviewFieldId:self::$assessmentFieldId);
        $SETqusRow  = $this->coreModel->queryRowArray("SELECT id FROM ".$tableName." WHERE ".$fieldId."='$interviewId' AND questionId='$questionId' AND status='1' LIMIT 0,1 ");
        //return $SETqusRow;
        //Because of random questions
        return 1;
    }
    
    /**
     * Description : Use to check Question Exists Or Not
     * Author : Synergy
     * @param int $questionId
     * @return array of data 
     */
    protected function questionExists($questionId) {
        $questionRow  = $this->coreModel->queryRowArray("SELECT Q.id AS questionId FROM fj_question Q WHERE Q.id='$questionId' AND Q.status='1' LIMIT 0,1 ");
        return $questionRow;
    }

    /**
     * Description : Use to check User's Answer Exists Or Not
     * Author : Synergy
     * @param int $jobId, int $id, int $setId, int $questionId, string $setType
     * @return array of data 
     */
    protected function answerExists($jobId, $id, $setId, $questionId, $setType) {
        $fieldId    = ($setType==self::$interview?self::$interviewFieldId:self::$assessmentFieldId);
        $answerRow  = $this->coreModel->queryResultArray("SELECT id FROM fj_userAnswers WHERE jobId='$jobId' AND userId='$id' AND ".$fieldId."='$setId' AND questionId='$questionId' AND status='1' LIMIT 0,1 ");
        return $answerRow;
    }

    /**
     * Description : Find All Interview Question ID's For a Particular Interview Set
     * Author : Synergy
     * @param array $params, string $setType
     * @return array of data 
     */
    protected function findSETquestionList($params, $setType) {
        $tableName      = ($setType==self::$interview?self::$interviewQuestionTable:self::$assessmentQuestionTable);
        $fieldId        = ($setType==self::$interview?self::$interviewFieldId:self::$assessmentFieldId);
        $setNotFound    = ($setType==self::$interview?'interview_not_found':'assessment_not_found');
        
        $userId     = $this->coreModel->cleanString($params['userId']);
        $id         = $this->coreModel->getJwtValue($userId);
        $fjJobId    = $this->coreModel->cleanString($params['jobCode']);
        if($fjJobId!='' && $userId!='') {
            $userRow    = $this->coreModel->userExists($id);
            if(count($userRow)>0) {
                $jobRow = $this->coreModel->jobExists($fjJobId);
                if(count($jobRow)>0) {                                            
                    $jobAppliedRow = $this->appliedJobExists($id, $jobRow['id']);
                    if((count($jobAppliedRow)>0) && ($setType==self::$interview)) {
                        $this->coreModel->codeMessage('500', $this->lang->line('already_applied'));
                    }
                    else if((count($jobAppliedRow)>0) && ($setType==self::$assessment) && ($jobRow['interview']=='0' || $jobRow['interview']=='' || $jobRow['interview']==NULL)) {
                        $this->coreModel->codeMessage('500', $this->lang->line('already_applied'));
                    }
                    else {
                        if(($jobRow[$setType]!='') || ($jobRow[$setType]!='0')){
                            $query  = "SELECT IQ.id, IQ.".$fieldId.", IQ.questionId";
                            if($setType==self::$assessment) {
                                $query .= ", A.duration";
                            }
                            $query .= " FROM ".$tableName." IQ JOIN fj_question Q ";                            
                            if($setType==self::$assessment) {
                                $query .= ", fj_assessments A";
                            }
                            $query .= " WHERE IQ.".$fieldId."='".$jobRow[$setType]."' AND IQ.questionId=Q.id AND IQ.status='1' AND Q.status='1'";
                            if($setType==self::$assessment) {
                                $query .= "AND IQ.assessmentId = A.id";
                            }
                            
                            //print_r($query);exit;
                            $questionsResult = $this->coreModel->queryResultArray($query);
                            if(count($questionsResult)>0){
                                foreach($questionsResult as $question) {
                                    $rowData['setID']       = (string)$question[$fieldId];
                                    $rowData['questionID']  = (string)$question['questionId'];
                                    $setQuestions[]   = $rowData;
                                }
                                $this->coreModel->codeMessage('200', $this->lang->line('success'));                            
                                coreapi_model::$returnArray['userID']   = $id;
                                coreapi_model::$returnArray['jobID']    = $jobRow['id'];
                                if($setType==self::$assessment) {
                                    coreapi_model::$returnArray['duration']    = (string)$questionsResult[0]['duration'];
                                }
                                coreapi_model::$returnArray['data']     = $setQuestions;
                            }
                            else {
                                $this->coreModel->codeMessage('500', $this->lang->line('no_question_found'));
                                coreapi_model::$returnArray['data']     = array();
                            }
                        }
                        else {
                            $this->coreModel->codeMessage('500', $this->lang->line($setNotFound));
                            coreapi_model::$returnArray['data']     = array();
                        }
                    }
                }
                else {
                    $this->coreModel->codeMessage('500', $this->lang->line('no_job_found'));
                    coreapi_model::$returnArray['data']     = array();
                }
            }
            else {
                $this->coreModel->codeMessage('500', $this->lang->line('invalid_user'));
                coreapi_model::$returnArray['data']     = array();
            }
        }
        else {
            $this->coreModel->codeMessage('202', $this->lang->line('missing_required_fields'));
            coreapi_model::$returnArray['data']     = array();
        }
        return coreapi_model::$returnArray;
    }

    /**
     * Description : Find All Interview Question ID's For a Particular Interview Set
     * Author : Synergy
     * @param array $params, string $setType
     * @return array of data 
     */
    protected function findSETquestionList_v2($params, $setType) {
        $tableName      = ($setType==self::$interview?self::$interviewQuestionTable:self::$assessmentQuestionTable);
        $fieldId        = ($setType==self::$interview?self::$interviewFieldId:self::$assessmentFieldId);
        $setNotFound    = ($setType==self::$interview?'interview_not_found':'assessment_not_found');
        
        $userId     = $this->coreModel->cleanString($params['userId']);
        $id         = $this->coreModel->getJwtValue($userId);
        $fjJobId    = $this->coreModel->cleanString($params['jobCode']);
        if($fjJobId!='' && $userId!='') {
            $userRow    = $this->coreModel->userExists($id);
            if(count($userRow)>0) {
                $jobRow = $this->coreModel->jobExists($fjJobId);

                if(count($jobRow)>0) {                                            
                    $jobAppliedRow = $this->appliedJobExists($id, $jobRow['id']);
                    if((count($jobAppliedRow)>0) && ($setType==self::$interview)) {
                        $this->coreModel->codeMessage('500', $this->lang->line('already_applied'));
                    }
                    else if((count($jobAppliedRow)>0) && ($setType==self::$assessment) && ($jobRow['interview']=='0' || $jobRow['interview']=='' || $jobRow['interview']==NULL)) {
                        $this->coreModel->codeMessage('500', $this->lang->line('already_applied'));
                    }
                    else {
                        if(($jobRow[$setType]!='') || ($jobRow[$setType]!='0')){

                            //Provide  Random question for Users

                            if($setType==self::$assessment) {
                                $randomQuestions = array();

                                $questionnaires = $this->coreModel->queryResultArray("SELECT * FROM fj_assessmentRandomQuestionnaire WHERE assessmentId = ".$jobRow['assessment']."");

                                if($questionnaires) {
                                    $this->db->delete('fj_userJobAssessmentQuestions', array('jobId' => $jobRow['id'], 'userId' => $id));

                                    foreach ($questionnaires as $questionnaire) {

                                        $qbQuestions = getQuestionOfQuestionBank($questionnaire['questionBankId']);

                                        $arrayRandomQeustions = array_rand($qbQuestions, $questionnaire['noOfQuestions']);

                                        if (is_array($arrayRandomQeustions)) {
                                            foreach($arrayRandomQeustions as $kvalue) {
                                                $randomQuestionsData['setID'] = $questionnaire['assessmentId'];
                                                $randomQuestionsData['questionID'] = $qbQuestions[$kvalue]['id'];
                                                $randomQuestions[] = $randomQuestionsData;

                                                $insertArray['jobId'] = $jobRow['id'];
                                                $insertArray['questionId'] = $qbQuestions[$kvalue]['id'];
                                                $insertArray['title'] = $qbQuestions[$kvalue]['title'];
                                                $insertArray['file'] = $qbQuestions[$kvalue]['file'];
                                                $insertArray['correctAnswer'] = $qbQuestions[$kvalue]['answer'];
                                                $insertArray['userId'] = $id;

                                                if($qbQuestions[$kvalue]['IsMultipleChoice'] == 1) {
                                                    $multipleChoiceOptions = $this->coreModel->queryResultArray("SELECT * FROM fj_multipleChoiceAnswer WHERE questionId = ".$qbQuestions[$kvalue]['id']."");

                                                    if($multipleChoiceOptions) {
                                                        $insertArray['answer1'] = $multipleChoiceOptions[0]['option'];
                                                        $insertArray['answer2'] = $multipleChoiceOptions[1]['option'];
                                                        $insertArray['answer3'] = $multipleChoiceOptions[2]['option'];
                                                        $insertArray['answer4'] = $multipleChoiceOptions[3]['option'];
                                                    }
                                                }
                                                $this->db->insert('fj_userJobAssessmentQuestions', $insertArray);
                                            }
                                        } else {
                                            $randomQuestionsData['setID'] = $questionnaire['assessmentId'];
                                            $randomQuestionsData['questionID'] = $qbQuestions[$arrayRandomQeustions]['id'];
                                            $randomQuestions[] = $randomQuestionsData;

                                            $insertArray['jobId'] = $jobRow['id'];
                                            $insertArray['questionId'] = $qbQuestions[$arrayRandomQeustions]['id'];
                                            $insertArray['title'] = $qbQuestions[$arrayRandomQeustions]['title'];
                                            $insertArray['file'] = $qbQuestions[$arrayRandomQeustions]['file'];
                                            $insertArray['correctAnswer'] = $qbQuestions[$arrayRandomQeustions]['answer'];
                                            $insertArray['userId'] = $id;

                                            if($qbQuestions[$kvalue]['IsMultipleChoice'] == 1) {
                                                $multipleChoiceOptions = $this->coreModel->queryResultArray("SELECT * FROM fj_multipleChoiceAnswer WHERE questionId = ".$qbQuestions[$arrayRandomQeustions]['id']."");

                                                if($multipleChoiceOptions) {
                                                    $insertArray['answer1'] = $multipleChoiceOptions[0]['option'];
                                                    $insertArray['answer2'] = $multipleChoiceOptions[1]['option'];
                                                    $insertArray['answer3'] = $multipleChoiceOptions[2]['option'];
                                                    $insertArray['answer4'] = $multipleChoiceOptions[3]['option'];
                                                }
                                            }
                                            $this->db->insert('fj_userJobAssessmentQuestions', $insertArray);
                                        }
                                    }
                                }
                            }
                            //End of random question

                            $query  = "SELECT IQ.id, Q.IsMultipleChoice, Q.title questionTitle, Q.file questionFile, Q.answer questionQnswer, IQ.".$fieldId.", IQ.questionId";
                            if($setType==self::$assessment) {
                                $query .= ", A.duration";
                            }
                            $query .= " FROM ".$tableName." IQ JOIN fj_question Q ";                            
                            if($setType==self::$assessment) {
                                $query .= ", fj_assessments A";
                            }
                            $query .= " WHERE IQ.".$fieldId."='".$jobRow[$setType]."' AND IQ.questionId=Q.id AND IQ.status='1' AND Q.status='1'";
                            if($setType==self::$assessment) {
                                $query .= "AND IQ.assessmentId = A.id";
                            }
                            //echo $query;exit;
                            $questionsResult = $this->coreModel->queryResultArray($query);

                            if($setType==self::$assessment) {
                                if(count($questionsResult)>0){
                                    $setQuestions = array();
                                    foreach($questionsResult as $question) {
                                        $rowData['setID']       = (string)$question[$fieldId];
                                        $rowData['questionID']  = (string)$question['questionId'];
                                        $setQuestions[]   = $rowData;

                                        $insertArray1['jobId'] = $jobRow['id'];
                                        $insertArray1['questionId'] = $question['questionId'];
                                        $insertArray1['title'] = $question['questionTitle'];
                                        $insertArray1['file'] = $question['questionFile'];
                                        $insertArray1['correctAnswer'] = $question['questionQnswer'];
                                        $insertArray1['userId'] = $id;

                                        if($question['IsMultipleChoice'] == 1) {
                                            $multipleChoiceOptions1 = $this->coreModel->queryResultArray("SELECT * FROM fj_multipleChoiceAnswer WHERE questionId = ".$question['id']."");

                                            if($multipleChoiceOptions1) {
                                                $insertArray1['answer1'] = $multipleChoiceOptions1[0]['option'];
                                                $insertArray1['answer2'] = $multipleChoiceOptions1[1]['option'];
                                                $insertArray1['answer3'] = $multipleChoiceOptions1[2]['option'];
                                                $insertArray1['answer4'] = $multipleChoiceOptions1[3]['option'];
                                            }
                                        }
                                        $this->db->insert('fj_userJobAssessmentQuestions', $insertArray1);
                                    }

                                    if($randomQuestions)
                                        $outputQuestions = array_merge($randomQuestions, $setQuestions);
                                    else
                                        $outputQuestions = $setQuestions;
                            
                                    coreapi_model::$returnArray['userID']   = $id;
                                    coreapi_model::$returnArray['jobID']    = $jobRow['id'];
                                    if($setType==self::$assessment) {
                                        coreapi_model::$returnArray['duration']    = (string)$questionsResult[0]['duration'];
                                    }
                                    coreapi_model::$returnArray['data'] = $outputQuestions;

                                    if($outputQuestions) {
                                        $this->coreModel->codeMessage('200', $this->lang->line('success'));
                                    } else {
                                        $this->coreModel->codeMessage('500', $this->lang->line('no_question_found'));
                                    }
                                }
                                else {
                                    coreapi_model::$returnArray['data'] = $randomQuestions;
                                    if($randomQuestions) {
                                        $this->coreModel->codeMessage('200', $this->lang->line('success'));
                                        coreapi_model::$returnArray['userID']   = $id;
                                        coreapi_model::$returnArray['jobID']    = $jobRow['id'];

                                        $durationTextArray = $this->coreModel->queryRowArray("SELECT * FROM fj_assessments WHERE id=".$jobRow['assessment']);
                                        coreapi_model::$returnArray['duration'] = (string)$durationTextArray['duration'];
                                    } else {
                                        $this->coreModel->codeMessage('500', $this->lang->line('no_question_found'));
                                    }
                                }
                            } else {
                                if(count($questionsResult)>0){
                                    $setQuestions = array();
                                    foreach($questionsResult as $question) {
                                        $rowData['setID']       = (string)$question[$fieldId];
                                        $rowData['questionID']  = (string)$question['questionId'];
                                        $setQuestions[]   = $rowData;
                                    }

                                    $this->coreModel->codeMessage('200', $this->lang->line('success'));                            
                                    coreapi_model::$returnArray['userID']   = $id;
                                    coreapi_model::$returnArray['jobID']    = $jobRow['id'];
                                    if($setType==self::$assessment) {
                                        coreapi_model::$returnArray['duration']    = (string)$questionsResult[0]['duration'];
                                    }
                                    coreapi_model::$returnArray['data']     = $setQuestions;
                                }
                                else {
                                    $this->coreModel->codeMessage('500', $this->lang->line('no_question_found'));
                                }
                            }
                        }
                        else {
                            $this->coreModel->codeMessage('500', $this->lang->line($setNotFound));
                        }
                    }
                }
                else {
                    $this->coreModel->codeMessage('500', $this->lang->line('no_job_found'));
                }
            }
            else {
                $this->coreModel->codeMessage('500', $this->lang->line('invalid_user'));
            }
        }
        else {
            $this->coreModel->codeMessage('202', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }

    /**
     * Description : Find All Interview Question ID's For a Particular Interview Set
     * Author : Synergy
     * @param array $params, string $setType
     * @return array of data 
     */
    protected function findSETquestionList_v3($params, $setType) {
        $tableName      = ($setType==self::$interview?self::$interviewQuestionTable:self::$assessmentQuestionTable);
        $fieldId        = ($setType==self::$interview?self::$interviewFieldId:self::$assessmentFieldId);
        $setNotFound    = ($setType==self::$interview?'interview_not_found':'assessment_not_found');
        
        $userId     = $this->coreModel->cleanString($params['userId']);
        $id         = $this->coreModel->getJwtValue($userId);
        $fjJobId    = $this->coreModel->cleanString($params['jobCode']);
        if($fjJobId!='' && $userId!='') {
            $userRow    = $this->coreModel->userExists($id);
            if(count($userRow)>0) {
                $jobRow = $this->coreModel->jobExists($fjJobId);

                if(count($jobRow)>0) {                                            
                    $jobAppliedRow = $this->appliedJobExists($id, $jobRow['id']);
                    if((count($jobAppliedRow)>0) && ($setType==self::$interview)) {
                        $this->coreModel->codeMessage('500', $this->lang->line('already_applied'));
                    }
                    else if((count($jobAppliedRow)>0) && ($setType==self::$assessment) && ($jobRow['interview']=='0' || $jobRow['interview']=='' || $jobRow['interview']==NULL)) {
                        $this->coreModel->codeMessage('500', $this->lang->line('already_applied'));
                    }
                    else {
                        if(($jobRow[$setType]!='') || ($jobRow[$setType]!='0')){

                            //Provide  Random question for Users

                            if($setType==self::$assessment) {
                                $randomQuestions = array();

                                $questionnaires = $this->coreModel->queryResultArray("SELECT * FROM fj_assessmentRandomQuestionnaire WHERE assessmentId = ".$jobRow['assessment']."");

                                if($questionnaires) {
                                    foreach ($questionnaires as $questionnaire) {

                                        $qbQuestions = getQuestionOfQuestionBank($questionnaire['questionBankId']);

                                        foreach( array_rand($qbQuestions, $questionnaire['noOfQuestions']) as $k ) {
                                            $randomQuestionsData['setID'] = $questionnaire['assessmentId'];
                                            $randomQuestionsData['questionID'] = $qbQuestions[$k]['id'];
                                            $randomQuestions[] = $randomQuestionsData;
                                        }
                                    }
                                }
                            }
                            //End of random question

                            $query  = "SELECT IQ.id, Q.IsMultipleChoice, Q.title questionTitle, Q.file questionFile, Q.answer questionQnswer, IQ.".$fieldId.", IQ.questionId";
                            if($setType==self::$assessment) {
                                $query .= ", A.duration";
                            }
                            $query .= " FROM ".$tableName." IQ JOIN fj_question Q ";                            
                            if($setType==self::$assessment) {
                                $query .= ", fj_assessments A";
                            }
                            $query .= " WHERE IQ.".$fieldId."='".$jobRow[$setType]."' AND IQ.questionId=Q.id AND IQ.status='1' AND Q.status='1'";
                            if($setType==self::$assessment) {
                                $query .= "AND IQ.assessmentId = A.id";
                            }
                            //echo $query;exit;
                            $questionsResult = $this->coreModel->queryResultArray($query);

                            if($setType==self::$assessment) {
                                if(count($questionsResult)>0){
                                    $setQuestions = array();
                                    foreach($questionsResult as $question) {
                                        $rowData['setID']       = (string)$question[$fieldId];
                                        $rowData['questionID']  = (string)$question['questionId'];
                                        $setQuestions[]   = $rowData;
                                    }

                                    if($randomQuestions)
                                        $outputQuestions = array_merge($randomQuestions, $setQuestions);
                                    else
                                        $outputQuestions = $setQuestions;
                            
                                    coreapi_model::$returnArray['userID']   = $id;
                                    coreapi_model::$returnArray['jobID']    = $jobRow['id'];
                                    if($setType==self::$assessment) {
                                        coreapi_model::$returnArray['duration']    = (string)$questionsResult[0]['duration'];
                                    }
                                    coreapi_model::$returnArray['data'] = $outputQuestions;

                                    if($outputQuestions) {
                                        $this->coreModel->codeMessage('200', $this->lang->line('success'));
                                    } else {
                                        $this->coreModel->codeMessage('500', $this->lang->line('no_question_found'));
                                    }
                                }
                                else {
                                    coreapi_model::$returnArray['data'] = $randomQuestions;
                                    if($randomQuestions) {
                                        $this->coreModel->codeMessage('200', $this->lang->line('success'));
                                        coreapi_model::$returnArray['userID']   = $id;
                                        coreapi_model::$returnArray['jobID']    = $jobRow['id'];

                                        $durationTextArray = $this->coreModel->queryRowArray("SELECT * FROM fj_assessments WHERE id=".$jobRow['assessment']);
                                        coreapi_model::$returnArray['duration'] = (string)$durationTextArray['duration'];
                                    } else {
                                        $this->coreModel->codeMessage('500', $this->lang->line('no_question_found'));
                                    }
                                }
                            } else {
                                if(count($questionsResult)>0){
                                    $setQuestions = array();
                                    foreach($questionsResult as $question) {
                                        $rowData['setID']       = (string)$question[$fieldId];
                                        $rowData['questionID']  = (string)$question['questionId'];
                                        $setQuestions[]   = $rowData;
                                    }

                                    $this->coreModel->codeMessage('200', $this->lang->line('success'));                            
                                    coreapi_model::$returnArray['userID']   = $id;
                                    coreapi_model::$returnArray['jobID']    = $jobRow['id'];
                                    if($setType==self::$assessment) {
                                        coreapi_model::$returnArray['duration']    = (string)$questionsResult[0]['duration'];
                                    }
                                    coreapi_model::$returnArray['data']     = $setQuestions;
                                }
                                else {
                                    $this->coreModel->codeMessage('500', $this->lang->line('no_question_found'));
                                }
                            }
                        }
                        else {
                            $this->coreModel->codeMessage('500', $this->lang->line($setNotFound));
                        }
                    }
                }
                else {
                    $this->coreModel->codeMessage('500', $this->lang->line('no_job_found'));
                }
            }
            else {
                $this->coreModel->codeMessage('500', $this->lang->line('invalid_user'));
            }
        }
        else {
            $this->coreModel->codeMessage('202', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }
    
    /**
     * Description : Use to find question details of a particular job
     * Author : Synergy
     * @param array $params, string $setType
     * @return array of data 
     */
    protected function findQuestionDetail($params, $setType) {
        $tableName      = ($setType==self::$interview?self::$interviewQuestionTable:self::$assessmentQuestionTable);
        $fieldId        = ($setType==self::$interview?self::$interviewFieldId:self::$assessmentFieldId);
        $setNotFound    = ($setType==self::$interview?'interview_not_found':'assessment_not_found');
        
        $userId         = $this->coreModel->cleanString($params['userId']);
        $id             = $this->coreModel->getJwtValue($userId);
        $fjJobId        = $this->coreModel->cleanString($params['jobCode']);
        $setId          = $this->coreModel->cleanString($params[$setType.'Set']);
        $questionId     = $this->coreModel->cleanString($params['questionId']);
        
        if($setId!='' && $userId!='' && $questionId!='' && $fjJobId!='') {
            $userRow    = $this->coreModel->userExists($id);
            if(count($userRow)>0) {                                        
                $jobRow = $this->coreModel->jobExists($fjJobId);
                if(count($jobRow)>0) {
                    $setQusRow = $this->SETquestionExists($setId, $questionId, $setType);
                    if(count($setQusRow)>0){
                        if($setType==self::$assessment) {
                            $optionsResult  = $this->coreModel->queryResultArray("
                                                                                SELECT 
                                                                                    Q.id AS questionId, Q.title, Q.file, Q.duration, Q.type,
                                                                                    MC.id AS optionId, MC.option
                                                                                FROM 
                                                                                    fj_question Q
                                                                                JOIN
                                                                                    fj_multipleChoiceAnswer MC
                                                                                WHERE
                                                                                    Q.id=MC.questionId              AND
                                                                                    Q.id='".$questionId."'          AND 
                                                                                    MC.questionId='".$questionId."' AND 
                                                                                    Q.status='1'                    AND
                                                                                    MC.status='1'
                                                                                ");
                            if(count($optionsResult)>0) {
                                $folderName             = (($optionsResult[0]['type']=='1' || $optionsResult[0]['type']=='2')?($optionsResult[0]['type']=='1'?'video':'audio'):'');
                                $rowData['questionId']  = (string)$optionsResult[0]['questionId'];
                                $rowData['title']       = (string)$optionsResult[0]['title'];
                                $rowData['file']        = (($optionsResult[0]['type']=='1' || $optionsResult[0]['type']=='2')?base_url() . '/uploads/questions/'.$folderName.'/' . $optionsResult[0]['file']:'');                        
                                $rowData['type']        = (string)(($optionsResult[0]['type']=='1' || $optionsResult[0]['type']=='2')?($optionsResult[0]['type']=='1'?'video':'audio'):'text');
                                if($setType==self::$interview) {
                                    $rowData['duration']    = (string)$optionsResult[0]['duration'];
                                }

                                foreach($optionsResult as $option) {
                                    $optionData['optionId']    = (string)$option['optionId'];
                                    $optionData['optionValue'] = (string)$option['option'];
                                    $optionArray[]  = $optionData;
                                }
                                $rowData['options'] = $optionArray;
                                coreapi_model::$data[] = $rowData;
                                $this->coreModel->codeMessage('200', $this->lang->line('success'));
                                coreapi_model::$returnArray['data']     = coreapi_model::$data;
                            }
                            else {
                                $this->coreModel->codeMessage('500', $this->lang->line('no_question_found'));
                            }
                        }
                        if($setType==self::$interview){
                            $optionsResult  = $this->coreModel->queryResultArray("
                                                                                SELECT 
                                                                                    Q.id AS questionId, Q.title, Q.file, Q.duration, Q.type
                                                                                FROM 
                                                                                    fj_question Q
                                                                                WHERE
                                                                                    Q.id='".$questionId."'         AND 
                                                                                    Q.status='1'
                                                                                ");
                            if(count($optionsResult)>0) {
                                $folderName             = (($optionsResult[0]['type']=='1' || $optionsResult[0]['type']=='2')?($optionsResult[0]['type']=='1'?'video':'audio'):'');
                                $rowData['questionId']  = (string)$optionsResult[0]['questionId'];
                                $rowData['title']       = (string)$optionsResult[0]['title'];
                                $rowData['file']        = (($optionsResult[0]['type']=='1' || $optionsResult[0]['type']=='2')?base_url() . '/uploads/questions/'.$folderName.'/' . $optionsResult[0]['file']:'');
                                $rowData['type']        = (string)(($optionsResult[0]['type']=='1' || $optionsResult[0]['type']=='2')?($optionsResult[0]['type']=='1'?'video':'audio'):'text');
                                if($setType==self::$interview) {
                                    $rowData['duration']    = (string)$optionsResult[0]['duration'];
                                }
                                coreapi_model::$data[]  = $rowData;
                                $this->coreModel->codeMessage('200', $this->lang->line('success'));
                                coreapi_model::$returnArray['data']     = coreapi_model::$data;
                            }
                            else {
                                $this->coreModel->codeMessage('500', $this->lang->line('no_question_found'));
                            }
                        }
                    }
                    else {
                        $this->coreModel->codeMessage('500', $this->lang->line('invalid_question'));
                    }
                }             
                else {
                    $this->coreModel->codeMessage('500', $this->lang->line('no_job_found'));
                }
            }
            else {
                $this->coreModel->codeMessage('500', $this->lang->line('invalid_user'));
            }
        }
        else {
            $this->coreModel->codeMessage('202', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }

    /**
     * Description : Use to submit User's Answer
     * Author : Synergy
     * @param array $params, string $setType
     * @return array of data 
     */
    protected function submitQuestionAnswer($params, $setType){
        $tableName  = ($setType==self::$interview?self::$interviewQuestionTable:self::$assessmentQuestionTable);
        $fieldId    = ($setType==self::$interview?self::$interviewFieldId:self::$assessmentFieldId);
        
        $userId         = $this->coreModel->cleanString($params['userId']);
        $id             = $this->coreModel->getJwtValue($userId);
        $fjJobId        = $this->coreModel->cleanString($params['jobCode']);
        $setId          = $this->coreModel->cleanString($params[$setType.'Set']);
        $questionId     = $this->coreModel->cleanString($params['questionId']);
        $answer         = $this->coreModel->cleanString($params['answer']);
        if($setId!='' && $userId!='' && $questionId!='' && $fjJobId!='') {
            $userRow    = $this->coreModel->userExists($id);
            if(count($userRow)>0) {                
                $jobRow = $this->coreModel->jobExists($fjJobId);
                if(count($jobRow)>0) {
                    $setQusRow = $this->SETquestionExists($setId, $questionId, $setType);
                    if(count($setQusRow)>0){
                        $questionRow  = $this->questionExists($questionId);
                        if(count($questionRow)>0) {
                            $answerRow  = $this->answerExists($jobRow['id'], $id, $setId, $questionId, $setType);
                            if(count($answerRow)==0) {
                                if($setType==self::$assessment) {
                                    $filename   = $answer;
                                }
                                if($setType==self::$interview) {
                                    $filename   = '';
                                    if($answer!='') {
                                        $binary     =   base64_decode($answer);
                                        $filename   =   $id.uniqid().date('YmdHis');
                                        header('Content-Type:  video/mp4; charset=utf-8');
                                        $file = fopen('uploads/answers/'.$filename.'.mp4', 'wb');
                                        fwrite($file, $binary);
                                        fclose($file);
                                    }
                                }
                                if($this->applyJob($id, $fjJobId)==TRUE) {
                                    $answerData = array(
                                                        'jobId'         => (int)$jobRow['id'],
                                                        'userId'        => (int)$id,
                                                        $fieldId        => (int)$setId,
                                                        'questionId'    => (int)$questionId,
                                                        'answer'        => (string)$filename,
                                                        'createdAt'     => date('Y-m-d H:i:s')
                                                    );
                                    $this->db->insert('fj_userAnswers', $answerData);
                                    $insert_id      = $this->db->insert_id();
                                    if($insert_id) {
                                        $whereData  = array(
                                                        'jobId'         => (int)$jobRow['id'],
                                                        'userId'        => (int)$id,
                                                        'questionId'    => (int)$questionId,
                                                    );

                                        $answerData1 = array('userAnswer'    => (string)$filename);
                                        $this->db->where($whereData)->update('fj_userJobAssessmentQuestions', $answerData1);

                                        $this->coreModel->codeMessage('200', $this->lang->line('success'));
                                        coreapi_model::$returnArray['question'] = (string)$questionId;
                                    }
                                    else {
                                        $this->coreModel->codeMessage('500', $this->lang->line('something_wrong'));
                                    }
                                }
                                else {
                                    $this->coreModel->codeMessage('500', $this->lang->line('something_wrong'));                                        
                                }
                            }
                            else if(count($answerRow)>0) {
                                if($setType==self::$assessment) {
                                    $filename   = $answer;
                                    $whereData  = array(
                                                        'jobId'         => (int)$jobRow['id'],
                                                        'userId'        => (int)$id,
                                                        'questionId'    => (int)$questionId,
                                                    );
                                    $answerData = array('answer'    => (string)$filename);
                                    $this->db->where($whereData)->update('fj_userAnswers', $answerData);

                                    $answerData1 = array('userAnswer'    => (string)$filename);
                                    $this->db->where($whereData)->update('fj_userJobAssessmentQuestions', $answerData1);
                                    $this->coreModel->codeMessage('200', $this->lang->line('success'));                                    
                                    coreapi_model::$returnArray['question'] = (string)$questionId;
                                }
                                else {
                                    $this->coreModel->codeMessage('500', $this->lang->line('already_attempted'));
                                }
                            }
                            else {
                                $this->coreModel->codeMessage('500', $this->lang->line('invalid_attempt'));
                            }
                        }
                        else {
                            $this->coreModel->codeMessage('500', $this->lang->line('invalid_attempt'));
                        }
                    }
                    else {
                        $this->coreModel->codeMessage('500', $this->lang->line('invalid_question'));
                    }
                }                
                else {
                    $this->coreModel->codeMessage('500', $this->lang->line('no_job_found'));
                }
            }
            else {
                $this->coreModel->codeMessage('500', $this->lang->line('invalid_user'));
            }
        }
        else {
            $this->coreModel->codeMessage('202', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }
    
    /**
     * Description : Use to Submit User's Pending Questions Answer
     * Author : Synergy
     * @param int $setId, int $questionId, int $jobId, int $userId, string $setType
     * @return array of data 
     */
    protected function pendingQuestionAnswer($setId, $questionId, $jobId, $userId, $setType) {
        $fieldId    = ($setType==self::$interview?self::$interviewFieldId:self::$assessmentFieldId);
        $setQusRow  = $this->SETquestionExists($setId, $questionId, $setType);
        if(count($setQusRow)>0){
            $questionRow  = $this->questionExists($questionId);
            if(count($questionRow)>0) {
                $answerRow  = $this->answerExists($jobId, $userId, $setId, $questionId, $setType);
                if(count($answerRow)==0) {
                    $answerData = array(
                                        'jobId'         => (int)$jobId,
                                        'userId'        => (int)$userId,
                                        $fieldId        => (int)$setId,
                                        'questionId'    => (int)$questionId,
                                        'createdAt'     => date('Y-m-d H:i:s')
                                    );
                    $this->db->insert('fj_userAnswers', $answerData);
                }
            }
        }
    }
    
    /**
     * Description : Use to Submit User's Answer In REQUEST IN PARTS
     * Author : Synergy
     * @param array $posts, string $setType, file $files
     * @return array of data 
     */
    protected function submitQuestionAnswerParts($posts, $setType, $files=''){
        $tableName      = ($setType==self::$interview?self::$interviewQuestionTable:self::$assessmentQuestionTable);
        $fieldId        = ($setType==self::$interview?self::$interviewFieldId:self::$assessmentFieldId);
        $dir_path       = "uploads/answers/";
        
        $userId         = $this->coreModel->cleanString($posts['userId']);
        $id             = $this->coreModel->getJwtValue($userId);
        $fjJobId        = $this->coreModel->cleanString($posts['jobCode']);
        $setId          = $this->coreModel->cleanString($posts[$setType.'Set']);
        $questionId     = $this->coreModel->cleanString($posts['questionId']);
        $answer         = ($setType==self::$interview?$files['answer']:$posts['answer']);
                
        if($userId!='' && $fjJobId!='' && $setId!='' && $questionId!='') {
            $userRow    = $this->coreModel->userExists($id);
            if(count($userRow)>0) {                
                $jobRow = $this->coreModel->jobExists($fjJobId);
                if(count($jobRow)>0) {
                    $setQusRow = $this->SETquestionExists($setId, $questionId, $setType);
                    if(count($setQusRow)>0){
                        $questionRow    = $this->questionExists($questionId);
                        if(count($questionRow)>0) {
                            $answerRow  = $this->answerExists($jobRow['id'], $id, $setId, $questionId, $setType);
                            if(count($answerRow)==0) {
                                if($setType==self::$assessment) {
                                    $filename   = $answer;
                                }

                                if($setType==self::$interview) {
                                    if($answer['error']===0) {
                                        $answer['name'] = $id.uniqid().date('YmdHis').'.mp4';
                                        $filename       = $answer['name'];
                                        $target_path    = $dir_path . basename($filename);
                                        try {
                                            if(!move_uploaded_file($answer['tmp_name'], $target_path)) {
                                                $this->coreModel->codeMessage('500', $this->lang->line('something_wrong'));
                                            }
                                        } catch (Exception $e) {
                                                $this->coreModel->codeMessage('500', $e->getMessage());
                                        }
                                    }
                                    else {
                                        $this->coreModel->codeMessage('202', $this->lang->line('empty_answer'));
                                    }
                                }

                                if($this->applyJob($id, $fjJobId)==TRUE) {
                                    $answerData = array(
                                                        'jobId'         => (int)$jobRow['id'],
                                                        'userId'        => (int)$id,
                                                        $fieldId        => (int)$setId,
                                                        'questionId'    => (int)$questionId,
                                                        'answer'        => (string)$filename,
                                                        'createdAt'     => date('Y-m-d H:i:s')
                                                    );
                                    $this->db->insert('fj_userAnswers', $answerData);
                                    $insert_id      = $this->db->insert_id();
                                    if($insert_id) {
                                        $this->coreModel->codeMessage('200', $this->lang->line('success'));                                    
                                        coreapi_model::$returnArray['question'] = (string)$questionId;
                                    }
                                    else {
                                        $this->coreModel->codeMessage('500', $this->lang->line('something_wrong'));
                                    } 
                                }
                                else {
                                    $this->coreModel->codeMessage('500', $this->lang->line('something_wrong'));                                        
                                }
                            }
                            else if(count($answerRow)>0) {
                                if($setType==self::$assessment) {
                                    if(jobsapi_model::applyJob($id, $fjJobId)==TRUE) {
                                        $filename   = $answer;
                                        $whereData  = array(
                                                            'jobId'         => (int)$jobRow['id'],
                                                            'userId'        => (int)$id,
                                                            'questionId'    => (int)$questionId,
                                                        );
                                        $answerData = array('answer' => (string)$filename);
                                        $this->db->where($whereData)->update('fj_userAnswers', $answerData);
                                        $this->coreModel->codeMessage('200', $this->lang->line('success'));                                    
                                        coreapi_model::$returnArray['question'] = (string)$questionId;
                                    }
                                    else {
                                        $this->coreModel->codeMessage('500', $this->lang->line('something_wrong'));                                        
                                    }
                                }
                                else {
                                    $this->coreModel->codeMessage('500', $this->lang->line('already_attempted'));
                                }
                            }
                            else {
                                $this->coreModel->codeMessage('500', $this->lang->line('invalid_attempt'));
                            }
                        }
                        else {
                            $this->coreModel->codeMessage('500', $this->lang->line('invalid_attempt'));
                        }
                    }
                    else {
                        $this->coreModel->codeMessage('500', $this->lang->line('invalid_question'));
                    }
                }                
                else {
                    $this->coreModel->codeMessage('500', $this->lang->line('no_job_found'));
                }
            }
            else {
                $this->coreModel->codeMessage('500', $this->lang->line('invalid_user'));
            }
        }
        else {
            $this->coreModel->codeMessage('202', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }
    
    /**
     * Description : Use to Submit User's Answer(Initerview Video) In chunks of base64 code
     * Author : Synergy
     * @param array $posts, string $setType
     * @return array of data 
     */
     protected function submitQuestionAnswerParts_v2($posts, $setType){
        //error_reporting(E_ALL);
        //ini_set('display_errors', 1);
        $tableName      = ($setType==self::$interview?self::$interviewQuestionTable:self::$assessmentQuestionTable);
        $fieldId        = ($setType==self::$interview?self::$interviewFieldId:self::$assessmentFieldId);
        $dir_path       = "/var/www/html/jobseeker/uploads/answers/";
        
        $answerBucketName = 'fjinterviewanswers';
        $s3         = new S3('AKIAJY7WPI2VG2OQ4GGA', 'usHqZSFHZ8Cr20Ms0xjloEIi6iMS6YfEXE5MagUC');

        $userId         = $this->coreModel->cleanString($posts['userId']);
        $id             = $this->coreModel->getJwtValue($userId);
        $fjJobId        = $this->coreModel->cleanString($posts['jobCode']);
        $setId          = $this->coreModel->cleanString($posts[$setType.'Set']);
        $questionId     = $this->coreModel->cleanString($posts['questionId']);
        $answer         = $posts['answer'];
        $filename       = $posts['filename'];
        $fileFailChunk      = $this->coreModel->cleanString($posts['fileFailChunk']);
        $isLastChunk        = $this->coreModel->cleanString($posts['isLastChunk']);
                
        if($userId!='' && $fjJobId!='' && $setId!='' && $questionId!='') {
            $userRow    = $this->coreModel->userExists($id);
            if(count($userRow)>0) {                
                $jobRow = $this->coreModel->jobExists($fjJobId);
                if(count($jobRow)>0) {
                    $setQusRow = $this->SETquestionExists($setId, $questionId, $setType);
                    if(count($setQusRow)>0){
                        $questionRow    = $this->questionExists($questionId);
                        if(count($questionRow)>0) {
                            $answerRow  = $this->answerExists($jobRow['id'], $id, $setId, $questionId, $setType);
                            if(count($answerRow)==0) {
                                if($setType==self::$interview) {
                                    if($filename) {
                                        $target_path = $dir_path . $filename;

                                         //inserting for testing
                                        //echo "SELECT id FROM fj_userVideoUploadLog WHERE userId='$id' AND jobId='$jobRow[id]' AND questionid = '$questionId' ORDER BY starttime DESC LIMIT 0,1";exit;
                                        /*$userjobids = $this->coreModel->queryRowArray("SELECT id FROM fj_userVideoUploadLog WHERE userId='$id' AND jobId='$jobRow[id]' AND questionid = '$questionId' ORDER BY starttime DESC LIMIT 0,1");*/

                                        //if(count($userjobids)==0) {
                                            $this->db->insert('fj_userVideoUploadLog', array('userId' => $id, 'jobId' => $jobRow['id'], 'userjobid' => '', 'questionid' => $questionId, 'localFileName' => $target_path, 'starttime' => date('Y-m-d H:i:s'), 'endtime' => date('Y-m-d H:i:s')));
                                        //}
                                        //End inserting for testing

                                        $copy_target_path = $dir_path.'copy-'.$filename;
                                        $fileData = base64_decode($answer);
                                        
                                        if (file_exists($target_path)) {
                                            copy($target_path, $copy_target_path);
                                            if($fileFailChunk == 1) {
                                                unlink($copy_target_path);
                                                unlink($target_path);
                                                $fid = file_put_contents($target_path, $fileData);
                                                if($fid) {
                                                    if($isLastChunk == 1) {
                                                        $originalFilename = $id.uniqid().date('YmdHis').'.mp4';
                                                        $originalTargePath = $dir_path . $originalFilename;
                                                        //rename($target_path, $originalTargePath);
														//ffmpeg Code
														shell_exec('ffmpeg -i ' . escapeshellcmd($target_path) . ' -vcodec copy -acodec aac -strict -2 -b:a 32k '.$dir_path.$originalFilename);
															
															
                                                        if($this->applyJob($id, $fjJobId)==TRUE) {
                                                            $answerData = array(
                                                                                'jobId'         => (int)$jobRow['id'],
                                                                                'userId'        => (int)$id,
                                                                                $fieldId        => (int)$setId,
                                                                                'questionId'    => (int)$questionId,
                                                                                'answer'        => (string)$originalFilename,
                                                                                'createdAt'     => date('Y-m-d H:i:s')
                                                                            );
                                                            $this->db->insert('fj_userAnswers', $answerData);
                                                            $insert_id      = $this->db->insert_id();
															
															
															
															
                                                            if($insert_id) {
                                                                if ($s3->putBucket($answerBucketName, S3::ACL_PUBLIC_READ)) {
                                                                    if ($s3->putObjectFile($originalTargePath, $answerBucketName, baseName($originalTargePath), S3::ACL_PUBLIC_READ)) {
                                                                        //unlink($answerVideoFile);

                                                                        //inserting for testing
                                                                        $userjobid = $this->coreModel->queryRowArray("SELECT id FROM fj_temp_userjob WHERE userId='$id' AND jobId='$jobRow[id]' ORDER BY createdAt DESC LIMIT 0,1");

                                                                        $whereDataLog = array(
                                                                                                    'userId'    => $id,
                                                                                                    'jobId'     => (int)$jobRow['id'],
                                                                                                    'questionid' => (int)$questionId
                                                                                                );
                                                                        $this->db->where($whereDataLog)->update('fj_userVideoUploadLog', array('userjobid' => $userjobid['id']));

                                                                        //Second temp logo update
                                                                        $usertmpLog = $this->coreModel->queryRowArray("SELECT id FROM fj_userVideoUploadLog WHERE userId='$id' AND jobId='$jobRow[id]' AND questionid = '$questionId' ORDER BY id DESC LIMIT 0,1");

                                                                        $whereDataLog1 = array(
                                                                                                    'userId'    => $id,
                                                                                                    'jobId'     => (int)$jobRow['id'],
                                                                                                    'questionid' => (int)$questionId,
                                                                                                    'id' => $usertmpLog['id']
                                                                                                );
                                                                        $this->db->where($whereDataLog1)->update('fj_userVideoUploadLog', array('endtime' => date('Y-m-d H:i:s'), 'userjobid' => $userjobid['id']));
                                                                        //End inserting for testing

                                                                    }
                                                                }
                                                                $this->coreModel->codeMessage('200', $this->lang->line('success'));                                    
                                                                coreapi_model::$returnArray['question'] = (string)$questionId;
                                                            }
                                                            else {
                                                                $this->coreModel->codeMessage('500', $this->lang->line('something_wrong'));
                                                            } 
                                                        }
                                                        else {
                                                            $this->coreModel->codeMessage('500', $this->lang->line('something_wrong'));
                                                        }
                                                    } else {
                                                        $this->coreModel->codeMessage('200', $this->lang->line('success'));
                                                    }
                                                } else {
                                                    $this->coreModel->codeMessage('500', $this->lang->line('something_wrong'));
                                                }
                                            } else {
                                                if (!$file = fopen($target_path, 'a')) {
                                                    $this->coreModel->codeMessage('500', $this->lang->line('something_wrong'));
                                                } else {
                                                    if (fwrite($file, $fileData) === FALSE) {
                                                        unlink($target_path);
                                                        rename($copy_target_path, $target_path);
                                                        $this->coreModel->codeMessage('500', $this->lang->line('something_wrong'));
                                                    } else {
                                                        fclose($file);
                                                        unlink($copy_target_path);
                                                        if($isLastChunk == 1) {
                                                            $originalFilename = $id.uniqid().date('YmdHis').'.mp4';
                                                            $originalTargePath = $dir_path . $originalFilename;
                                                            //rename($target_path, $originalTargePath);
															
															shell_exec('ffmpeg -i ' . escapeshellcmd($target_path) . ' -vcodec copy -acodec aac -strict -2 -b:a 32k '.$dir_path.$originalFilename);
															
                                                            if($this->applyJob($id, $fjJobId)==TRUE) {
                                                                $answerData = array(
                                                                                    'jobId'         => (int)$jobRow['id'],
                                                                                    'userId'        => (int)$id,
                                                                                    $fieldId        => (int)$setId,
                                                                                    'questionId'    => (int)$questionId,
                                                                                    'answer'        => (string)$originalFilename,
                                                                                    'createdAt'     => date('Y-m-d H:i:s')
                                                                                );
                                                                $this->db->insert('fj_userAnswers', $answerData);
                                                                $insert_id      = $this->db->insert_id();
																
																
																

                                                                if($insert_id) {
                                                                    if ($s3->putBucket($answerBucketName, S3::ACL_PUBLIC_READ)) {
                                                                        if ($s3->putObjectFile($originalTargePath, $answerBucketName, baseName($originalTargePath), S3::ACL_PUBLIC_READ)) {
                                                                            //unlink($answerVideoFile);

                                                                            //inserting for testing
                                                                            $userjobid = $this->coreModel->queryRowArray("SELECT id FROM fj_temp_userjob WHERE userId='$id' AND jobId='$jobRow[id]' ORDER BY createdAt DESC LIMIT 0,1");

                                                                            $whereDataLog = array(
                                                                                                        'userId'    => $id,
                                                                                                        'jobId'     => (int)$jobRow['id'],
                                                                                                        'questionid' => (int)$questionId
                                                                                                    );
                                                                            $this->db->where($whereDataLog)->update('fj_userVideoUploadLog', array('userjobid' => $userjobid['id']));


                                                                            //Second temp logo update
                                                                        $usertmpLog = $this->coreModel->queryRowArray("SELECT id FROM fj_userVideoUploadLog WHERE userId='$id' AND jobId='$jobRow[id]' AND questionid = '$questionId' ORDER BY id DESC LIMIT 0,1");

                                                                        $whereDataLog1 = array(
                                                                                                    'userId'    => $id,
                                                                                                    'jobId'     => (int)$jobRow['id'],
                                                                                                    'questionid' => (int)$questionId,
                                                                                                    'id' => $usertmpLog['id']
                                                                                                );
                                                                        $this->db->where($whereDataLog1)->update('fj_userVideoUploadLog', array('endtime' => date('Y-m-d H:i:s'), 'userjobid' => $userjobid['id']));
                                                                            //End inserting for testing

                                                                        }
                                                                    }
                                                                    $this->coreModel->codeMessage('200', $this->lang->line('success'));                                    
                                                                    coreapi_model::$returnArray['question'] = (string)$questionId;
                                                                }
                                                                else {
                                                                    $this->coreModel->codeMessage('500', $this->lang->line('something_wrong'));
                                                                } 
                                                            }
                                                            else {
                                                                $this->coreModel->codeMessage('500', $this->lang->line('something_wrong'));
                                                            }
                                                        } else {
                                                            $this->coreModel->codeMessage('200', $this->lang->line('success'));
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            $fid = file_put_contents($target_path, $fileData);
                                            if($fid) {
                                                if($isLastChunk == 1) {
                                                    $originalFilename = $id.uniqid().date('YmdHis').'.mp4';
                                                    $originalTargePath = $dir_path . $originalFilename;
                                                    //rename($target_path, $originalTargePath);
													
													
													shell_exec('ffmpeg -i ' . escapeshellcmd($target_path) . ' -vcodec copy -acodec aac -strict -2 -b:a 32k '.$dir_path.$originalFilename);
													
															
													
                                                    if($this->applyJob($id, $fjJobId)==TRUE) {
                                                        $answerData = array(
                                                                            'jobId'         => (int)$jobRow['id'],
                                                                            'userId'        => (int)$id,
                                                                            $fieldId        => (int)$setId,
                                                                            'questionId'    => (int)$questionId,
                                                                            'answer'        => (string)$originalFilename,
                                                                            'createdAt'     => date('Y-m-d H:i:s')
                                                                        );
                                                        $this->db->insert('fj_userAnswers', $answerData);
                                                        $insert_id      = $this->db->insert_id();
														
														
														

                                                        if($insert_id) {
                                                            if ($s3->putBucket($answerBucketName, S3::ACL_PUBLIC_READ)) {
                                                                if ($s3->putObjectFile($originalTargePath, $answerBucketName, baseName($originalTargePath), S3::ACL_PUBLIC_READ)) {
                                                                    //unlink($answerVideoFile);

                                                                    //inserting for testing
                                                                    $userjobid = $this->coreModel->queryRowArray("SELECT id FROM fj_temp_userjob WHERE userId='$id' AND jobId='$jobRow[id]' ORDER BY createdAt DESC LIMIT 0,1");

                                                                    $whereDataLog = array(
                                                                                                'userId'    => $id,
                                                                                                'jobId'     => (int)$jobRow['id'],
                                                                                                'questionid' => (int)$questionId
                                                                                            );
                                                                    $this->db->where($whereDataLog)->update('fj_userVideoUploadLog', array('userjobid' => $userjobid['id']));


                                                                    //Second temp logo update
                                                                        $usertmpLog = $this->coreModel->queryRowArray("SELECT id FROM fj_userVideoUploadLog WHERE userId='$id' AND jobId='$jobRow[id]' AND questionid = '$questionId' ORDER BY id DESC LIMIT 0,1");

                                                                        $whereDataLog1 = array(
                                                                                                    'userId'    => $id,
                                                                                                    'jobId'     => (int)$jobRow['id'],
                                                                                                    'questionid' => (int)$questionId,
                                                                                                    'id' => $usertmpLog['id']
                                                                                                );
                                                                        $this->db->where($whereDataLog1)->update('fj_userVideoUploadLog', array('endtime' => date('Y-m-d H:i:s'), 'userjobid' => $userjobid['id']));
                                                                    //End inserting for testing
                                                        
                                                                }
                                                            }
                                                            $this->coreModel->codeMessage('200', $this->lang->line('success'));                                    
                                                            coreapi_model::$returnArray['question'] = (string)$questionId;
                                                        }
                                                        else {
                                                            $this->coreModel->codeMessage('500', $this->lang->line('something_wrong'));
                                                        } 
                                                    }
                                                    else {
                                                        $this->coreModel->codeMessage('500', $this->lang->line('something_wrong'));
                                                    }
                                                } else {
                                                    $this->coreModel->codeMessage('200', $this->lang->line('success'));
                                                }
                                            } else {
                                                $this->coreModel->codeMessage('500', $this->lang->line('something_wrong'));
                                            }
                                        }
                                    }
                                    else {
                                        $this->coreModel->codeMessage('202', $this->lang->line('empty_answer'));
                                    }
                                }
                            }
                            else if(count($answerRow)>0) {
                                if($setType==self::$assessment) {
                                    if(jobsapi_model::applyJob($id, $fjJobId)==TRUE) {
                                        $filename   = $answer;
                                        $whereData  = array(
                                                            'jobId'         => (int)$jobRow['id'],
                                                            'userId'        => (int)$id,
                                                            'questionId'    => (int)$questionId,
                                                        );
                                        $answerData = array('answer' => (string)$filename);
                                        $this->db->where($whereData)->update('fj_userAnswers', $answerData);
                                        $this->coreModel->codeMessage('200', $this->lang->line('success'));                                    
                                        coreapi_model::$returnArray['question'] = (string)$questionId;
                                    }
                                    else {
                                        $this->coreModel->codeMessage('500', $this->lang->line('something_wrong'));                                        
                                    }
                                }
                                else {
                                    $this->coreModel->codeMessage('500', $this->lang->line('already_attempted'));
                                }
                            }
                            else {
                                $this->coreModel->codeMessage('500', $this->lang->line('invalid_attempt'));
                            }
                        }
                        else {
                            $this->coreModel->codeMessage('500', $this->lang->line('invalid_attempt'));
                        }
                    }
                    else {
                        $this->coreModel->codeMessage('500', $this->lang->line('invalid_question'));
                    }
                }                
                else {
                    $this->coreModel->codeMessage('500', $this->lang->line('no_job_found'));
                }
            }
            else {
                $this->coreModel->codeMessage('500', $this->lang->line('invalid_user'));
            }
        }
        else {
            $this->coreModel->codeMessage('202', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }
    /**
     * Description : Use to return percentage of profile completion of a particular user
     * Author : Synergy
     * @param array $params
     * @return array of data 
     */
    protected function profileComplition($params){
        $userId         = $this->coreModel->cleanString($params['userId']);
        $id             = $this->coreModel->getJwtValue($userId);
        if($userId!='') {
            $userData   =   $this->coreModel->queryRowArray("SELECT 
                                                                id, fullname, email, mobile, dob, pinCode, gender,
                                                                preferredLocation, expectedCtcFrom, expectedCtcTo, workExperience,adharCard,languages,image,
                                                                facebookLink, linkedInLink, twitterLink, googlePlusLink
                                                          FROM fj_users WHERE id='$id' AND status='1'");
            $basicField         = 6;
            $educationalField   = 4;
            $otherField         = 2;
            $socialField        = 0;
            $totalField         = (int)$basicField+$educationalField+$otherField+$socialField;
            $filledData         = 0;
            if(count($userData)>0){
                ($userData['fullname']!=''?$filledData++:$filledData);
                ($userData['email']!=''?$filledData++:$filledData);
                ($userData['mobile']!=''?$filledData++:$filledData);
                (($userData['dob']!='') && ($userData['dob']!='0000-00-00')?$filledData++:$filledData);
                ($userData['pinCode']!=''?$filledData++:$filledData);
                ($userData['gender']!=''?$filledData++:$filledData);
                
                //($userData['facebookLink']!=''?$filledData++:$filledData);
                //($userData['linkedInLink']!=''?$filledData++:$filledData);
                //($userData['twitterLink']!=''?$filledData++:$filledData);
                //($userData['googlePlusLink']!=''?$filledData++:$filledData);
                
                ($userData['preferredLocation']!=''?$filledData++:$filledData);
                //($userData['expectedCtcFrom']!=''?$filledData++:$filledData);
                //($userData['expectedCtcTo']!=''?$filledData++:$filledData);
                ($userData['workExperience']!=''?$filledData++:$filledData);
                //($userData['adharCard']!=''?$filledData++:$filledData);
                //($userData['languages']!=''?$filledData++:$filledData);
                //($userData['image']!=''?$filledData++:$filledData);
                                
                $qualificationResults   = $this->coreModel->queryResultArray("SELECT Q . * , C.id as courseId, C.name as courseName, U.name as adminUniversityName FROM fj_userQualification Q JOIN fj_university U, fj_courses C WHERE Q.universityId = U.id AND Q.courseId=C.id AND Q.userId='$id' AND Q.status='1'");
                if(count($qualificationResults)>0) {
                    $filledData = $filledData+4;
                }
            }
            
            return ($totalField==$filledData?TRUE:FALSE);
        }
        return FALSE;
    }
    
    /**
     * Description : Use to apply for a job from a specific user
     * Author : Synergy
     * @param int $userId, string $jobCode
     * @return array of data 
     */
    public function applyJob($userId, $jobCode) {
        $userId     = $this->coreModel->cleanString($userId);
        $fjJobId    = $this->coreModel->cleanString($jobCode);
        if($fjJobId!='' && $userId!='') {
            $userRow    = $this->coreModel->userExists($userId);
            if(count($userRow)>0){
                $jobRow = $this->coreModel->jobExists($fjJobId);
                if(count($jobRow)>0){
                    $userJobRow = $this->coreModel->queryRowArray("SELECT id, status, createdAt FROM fj_temp_userjob WHERE userId='$userRow[id]' AND jobId='$jobRow[id]' ORDER BY createdAt DESC LIMIT 0,1");

                    $companyConfig = $this->coreModel->queryRowArray("SELECT id FROM fj_companyConfig WHERE mainUserId='$jobRow[createdBy]' AND companyConfigParamId='1' AND paramValue='1' LIMIT 0,1");

                    if(count($userJobRow)==0) {

                        //Notification for corporate user
                        $interviewJobId = $jobRow['id'];
                        $joBid = encryptURLparam($jobRow['id'], URLparamKey);
                        $useId = encryptURLparam($userRow['id'], URLparamKey);

                        $interviewLink = 'https://'.$_SERVER['HTTP_HOST'].'/admin/queue/list?JobId='.$joBid.'&userId='.$useId;

                        $jobLevels = $this->coreModel->queryResultArray("SELECT panel.userId, user.email, user.fullname, job.title FROM fj_jobPanel panel JOIN fj_users user ON user.id = panel.userId JOIN fj_jobs job ON job.id = panel.jobId WHERE panel.jobId='$interviewJobId' AND panel.level = '1'");
                        $dateTime = date('d M Y H:i:s');

                        if($jobLevels) {
                            foreach($jobLevels as $jobLevel) {
                                if(empty($jobLevel['fullname'])) {
                                    $companyUserName = $jobLevel['email'];
                                } else {
                                    $companyUserName = $jobLevel['fullname'];
                                }
                                
                                $this->email->from('noreply@vdohire.com', 'VDOHire');
                                $this->email->to($jobLevel['email']);
                                $this->email->subject('VDOHire : Interview Received');
                                $message = "
                                                Dear {$companyUserName},
                                                <br/><br/>
                                                There is a new candidate’s digital application received on VDOHire platform.
                                                <br/><br/>
                                                Candidate Name : {$userRow['fullname']}
                                                <br/>
                                                Date Time : {$dateTime}
                                                <br/>
                                                Interview Link : {$interviewLink}
                                                <br/><br/><br/>
                                                Thanks,<br/><br/>
                                                VDOHire Team";
                                $this->email->message($message);
                                $this->email->set_mailtype('html');
                                $this->email->send();

                                $notificationMessage = 'You have received an interview by '.$userRow['fullname'].' on the job '.$jobLevel['title'].'';
                                $this->db->insert('fj_notification', array('userId' => $jobLevel['userId'], 'userIdCreatedBy' => $userRow['id'], 'notifyMessage' => $notificationMessage, 'notifyMessageType' => 'Interview Received', 'messageReadStatus' => '0', 'createdAt' => date('Y-m-d H:i:s')));
                            }
                        }
                        //End of notification for corporate user

                        // Save Applied Job Information
                        $jobData    = array(
                                            'userId'    => (int)$userRow['id'],
                                            'jobId'     => (int)$jobRow['id'],
                                            'status'    => '1',
                                            'source'    => 1,
                                            'createdAt' => date('Y-m-d H:i:s'),
                                            'updatedAt' => date('Y-m-d H:i:s')
                                        );

                        $jobDatas    = array(
                                            'userId'    => (int)$userRow['id'],
                                            'jobId'     => (int)$jobRow['id'],
                                            'status'    => '2',
                                            'source'    => 1,
                                            'createdAt' => date('Y-m-d H:i:s'),
                                            'updatedAt' => date('Y-m-d H:i:s')
                                        );

                        if( count($companyConfig) >= 1 ) {
                            $insertJob  = $this->db->insert('fj_temp_userjob', $jobDatas);
                            $insertJob  = $this->db->insert('fj_userJob', $jobData);
                        } else {
                            $insertJob  = $this->db->insert('fj_temp_userjob', $jobData);
                            $insertJob  = $this->db->insert('fj_userJob', $jobData);
                        }

                        if($insertJob){
                            $updateDataSaved     = array('status'=>'0');
                            $whereDataSaved      = array(
                                                    'userId'    => (int)$userRow['id'],
                                                    'jobId'     => (int)$jobRow['id'],
                                                    'status'    => '1'
                                                );
                            $this->db->where($whereDataSaved)->update('fj_userSavedJobs',$updateDataSaved);
                            return TRUE;
                        }
                        else {                        
                            return FALSE;
                        }
                    }
                    else {
                        return TRUE;
                    }
                }
                else {
                    return FALSE;
                }
            }
            else {
                return FALSE;
            }
        }
        else {
            return FALSE;
        }
        return FALSE;
    }
    
    /*
     *  @Author : Wildnet
     *  @Params : array(user id, job code)
     *  @Short Description : All Question List For That Particular Job
     *  @return : array with code. 
     */
    //-------------- Interview Question List For a Job Code Starts ------------//
    public function interviewQuestionList($params) {
        $searchResult   = $this->findSETquestionList($params, self::$interview);
        unset($searchResult['jobID']);
        unset($searchResult['userID']);
        return $searchResult;
    }
    //-------------- Interview Question List For a Job Code Ends --------------//

    //-------------- Interview Question List For invite a Job Code Starts ------------//

    public function interviewQuestionListInvite($params) {
        $searchResult   = $this->findSETquestionList($params, self::$interview);
        unset($searchResult['jobID']);
        unset($searchResult['userID']);
        return $searchResult;
    }
    //-------------- Interview Question List For invite a Job Code Ends --------------//
    ############################################################################
    #
    #
    #
    ############################################################################
    /*
     *  @Author : Wildnet
     *  @Params : array(interview Set Id, question Id, user Id)
     *  @Short Description : Particular Question Detail For That Particular Job
     *  @return : array with code. 
     */
    //------------------- Interview Question Details Starts -------------------//
    public function interviewQuestionDetail($params) {
        $searchResult   = $this->findQuestionDetail($params, self::$interview);
        return $searchResult;
    }
    //------------------- Interview Question Details Ends ---------------------//

    //------------------- Interview Question Details invite Starts -------------------//
    public function interviewQuestionDetailInvite($params) {
        $searchResult   = $this->findQuestionDetail($params, self::$interview);
        return $searchResult;
    }
    //------------------- Interview Question Details invite Ends ---------------------//
    ############################################################################
    #
    #
    #
    ############################################################################
    /*
     *  @Author : Wildnet
     *  @Params : array(interview Set Id, question Id, user Id, answer)
     *  @Short Description : Store User Answer of a Particular Question
     *  @return : array with code. 
     */
    //------------ Submit User's Interview Answer Details Starts --------------//
    public function interviewQuestionAnswer($params) {
        $submitResult   = $this->submitQuestionAnswer($params, self::$interview);
        return $submitResult;
    }
    //------------ Submit User's Interview Answer Details Ends ----------------//
    ############################################################################
    #
    #
    #
    ############################################################################
    /*
     *  @Author : Wildnet
     *  @Params : array(interview Set Id, question Id, user Id, answer)
     *  @Short Description : Store User Answer of a Particular Interview Question
     *  @return : array with code. 
     */
    //--------- Submit User's Interview Answer Details (PARTS) Starts ---------//
    public function interviewQuestionAnswerParts($posts, $files) {
        $submitResult   = $this->submitQuestionAnswerParts($posts, self::$interview, $files);
        return $submitResult;
    }
    //---------- Submit User's Interview Answer Details (PARTS) Ends ----------//

    /*
     *  @Author : Synergy
     *  @Params : array(interview Set Id, question Id, user Id, answer)
     *  @Short Description : Store User Answer of a Particular Interview Question
     *  @return : array with code. 
     */
    //--------- Submit User's Interview Answer Details (PARTS) Starts ---------//
    public function interviewQuestionAnswer_v2($posts) {
        if($posts['isInvite'] == 1) {
            $submitResult   = $this->submitQuestionAnswerParts_v2($posts, self::$interview);
            return $submitResult;
        } else {
            if($this->profileComplition($posts)==TRUE) {
                $submitResult   = $this->submitQuestionAnswerParts_v2($posts, self::$interview);
                return $submitResult;
            }
            else {
                $submitResult   = $this->submitQuestionAnswerParts_v2($posts, self::$interview);
                return $submitResult;
            }
        }
    }
    //---------- Submit User's Interview Answer Details (PARTS) Ends ----------//

    ############################################################################
    //-------------------------------------------------------------------------//
    //--------------------- Interview Question/Answer -------------------------//
    //-------------------------------------------------------------------------//
    

    
    //-------------------------------------------------------------------------//
    //--------------------- Assessment Question/Answer ------------------------//
    //-------------------------------------------------------------------------//
    ############################################################################
    /*
     *  @Author : Wildnet
     *  @Params : array(user id, job code)
     *  @Short Description : All Assessment Question List For That Particular Job
     *  @return : array with code. 
     */
    //-------------- Assessment Question List For a Job Code Starts -----------//
    public function assessmentQuestionList($params) {
        $searchResult   = $this->findSETquestionList($params, self::$assessment);
        unset($searchResult['jobID']);
        unset($searchResult['userID']);
        return $searchResult;
    }

    /*
     *  @Author : Wildnet
     *  @Params : array(user id, job code)
     *  @Short Description : All Assessment Question List For That Particular Job
     *  @return : array with code. 
     */
    //-------------- Assessment Question List For a Job Code Starts -----------//
    public function assessmentQuestionList_v2($params) { 
        $searchResult   = $this->findSETquestionList($params, self::$assessment);
        unset($searchResult['jobID']);
        unset($searchResult['userID']);
        return $searchResult;
    }

    public function assessmentQuestionList_v3($params) { 
        $searchResult   = $this->findSETquestionList_v2($params, self::$assessment);
        unset($searchResult['jobID']);
        unset($searchResult['userID']);
        return $searchResult;
    }

    public function hasAssessmentQuestions($params) { 
        $searchResult   = $this->findSETquestionList_v3($params, self::$assessment);
        unset($searchResult['jobID']);
        unset($searchResult['userID']);
        return $searchResult;
    }

    //-------------- Assessment Question List For a Job Code Ends -------------//
    ############################################################################
    #
    #
    #
    ############################################################################
    /*
     *  @Author : Wildnet
     *  @Params : array(assessment Set Id, question Id, user Id)
     *  @Short Description : Particular Question Detail For That Particular Job
     *  @return : array with code. 
     */
    //------------------- Assessment Question Details Starts ------------------//
    public function assessmentQuestionDetail($params) { 
        $searchResult   = $this->findQuestionDetail($params, self::$assessment);
        return $searchResult;
    }

    /*
     *  @Author : Wildnet
     *  @Params : array(assessment Set Id, question Id, user Id)
     *  @Short Description : Particular Question Detail For That Particular Job
     *  @return : array with code. 
     */
    //------------------- Assessment Question Details Starts ------------------//
    public function assessmentQuestionDetail_v2($params) {
        $searchResult   = $this->findQuestionDetail($params, self::$assessment);
        return $searchResult;
    }
    //------------------- Assessment Question Details Ends --------------------//
    ############################################################################
    #
    #
    #
    ############################################################################
    /*
     *  @Author : Wildnet
     *  @Params : array(assessment Set Id, question Id, user Id, answer)
     *  @Short Description : Store User Answer of a Particular Assessment Question
     *  @return : array with code. 
     */
    //------------- Submit User's Assessment Answer Details Starts ------------//
    public function assessmentQuestionAnswer($params) {  
        $submitResult   = $this->submitQuestionAnswer($params, self::$assessment);
        return $submitResult;
    }

    ############################################################################
    /*
     *  @Author : Wildnet
     *  @Params : array(assessment Set Id, question Id, user Id, answer)
     *  @Short Description : Store User Answer of a Particular Assessment Question
     *  @return : array with code. 
     */
    //------------- Submit User's Assessment Answer Details Starts ------------//
    public function assessmentQuestionAnswer_v2($params) {         
        $submitResult   = $this->submitQuestionAnswer($params, self::$assessment);
        return $submitResult;
    }
    //------------- Submit User's Assessment Answer Details Ends --------------//
    ############################################################################
    #
    #
    #
    ############################################################################
    /*
     *  @Author : Wildnet
     *  @Params : array(assessment Set Id, question Id, user Id, answer)
     *  @Short Description : Store User Answer of a Particular Assessment Question
     *  @return : array with code. 
     */
    //-------- Submit User's Assessment Answer Details (PARTS) Starts ---------//
    public function assessmentQuestionAnswerParts($posts) {
        $submitResult   = $this->submitQuestionAnswerParts($posts, self::$assessment);
        return $submitResult;
    }
    //--------- Submit User's Assessment Answer Details (PARTS) Ends ----------//
    ############################################################################
    //-------------------------------------------------------------------------//
    //--------------------- Assessment Question/Answer ------------------------//
    //-------------------------------------------------------------------------//
    
    
    
    //-------------------------------------------------------------------------//
    //--------------------------- Answer Screen -------------------------------//
    //-------------------------------------------------------------------------//
    ############################################################################
    /*
     *  @Author : Wildnet
     *  @Params : array(user id, job code)
     *  @Short Description : All Question List For That Particular Job
     *  @return : array with code. 
     */
    //-------------- Interview Question List For a Job Code Starts ------------//    
    public function answerScreen($params) {     
        $userId         = $this->coreModel->cleanString($params['userId']);
        $id             = $this->coreModel->getJwtValue($userId);
        $fjJobId        = $this->coreModel->cleanString($params['jobCode']);
        
        if($fjJobId!='' && $userId!='') {
            $userRow    = $this->coreModel->userExists($id);
            if(count($userRow)>0){
                $jobRow = $this->coreModel->jobExists($fjJobId);
                if(count($jobRow)>0){
                    $companyData    = $this->coreModel->queryRowArray("SELECT * FROM fj_users WHERE status='1' AND id='".$jobRow['createdBy']."' ");
                    $rowData['companyVideo']    = ($jobRow['jd']!=''?base_url() . '/uploads/jd/'.$jobRow['jd']:'');
                    $rowData['companyName']     = (string)($companyData['company']!=''?$companyData['company']:'');
                    $rowData['companyLogo']     = ($companyData['companyLogo']!=''?base_url() . '/uploads/corporateLogo/'.$companyData['companyLogo']:'');
                    coreapi_model::$data[]      = $rowData;
                    $this->coreModel->codeMessage('200', $this->lang->line('success'));
                    coreapi_model::$returnArray['data'] = coreapi_model::$data;
                }              
                else {
                    $this->coreModel->codeMessage('500', $this->lang->line('no_job_found'));
                }
            }
            else {
                $this->coreModel->codeMessage('500', $this->lang->line('invalid_user'));
            }
        }
        else {
            $this->coreModel->codeMessage('202', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }

    /**
     * Description : Use to check User's Answer Exists Or Not
     * Author : Synergy
     * @param int $jobId, int $id, int $setId, int $questionId, string $setType
     * @return array of data 
     */

    public function getUnmergedVideoInterviews($params) {
        //SELECT ua.userId, fj.fjCode FROM fj_userAnswers ua JOIN `fj_temp_userjob` tu ON tu.userId = ua.userId AND tu.jobId = ua.jobId JOIN fj_jobs fj ON fj.id = ua.jobId WHERE (tu.mergedVideo IS NULL OR tu.mergedVideo = '') AND (DATEDIFF(now(),tu.createdAt) < 2) GROUP BY ua.jobId

        //$answerRow  = $this->coreModel->queryResultArray("SELECT uans.userId, fjob.fjCode FROM `fj_userAnswers` uans JOIN fj_jobs fjob ON fjob.id = uans.jobId LEFT JOIN fj_userJob ujob ON ujob.userId = uans.userId WHERE ((uans.userId NOT IN (SELECT userId FROM fj_userJob) AND uans.jobId NOT IN (SELECT jobId FROM fj_userJob)) OR (ujob.mergedVideo IS NULL OR ujob.mergedVideo = '')) AND (uans.createdAt >= (NOW() + INTERVAL 200 HOUR) OR ujob.createdAt >= (NOW() + INTERVAL 200 HOUR)) GROUP BY uans.userId, uans.jobId");
        /*$answerRow  = $this->coreModel->queryResultArray("SELECT 
                                                        uans.userId, 
                                                        fjob.fjCode 
                                                        FROM `fj_userAnswers` uans 
                                                        JOIN fj_jobs fjob ON fjob.id = uans.jobId 
                                                        JOIN fj_temp_userjob ujob ON ujob.userId = uans.userId 

                                                        WHERE 
                                                        (
                                                        (uans.userId NOT IN (SELECT userId FROM fj_userJob) AND 
                                                        uans.jobId NOT IN 
                                                        (SELECT jobId FROM fj_temp_userjob)) 
                                                        OR 
                                                        (ujob.mergedVideo IS NULL OR ujob.mergedVideo = '')) 

                                                        AND (DATEDIFF(now(),uans.CreatedAt) < 2) GROUP BY uans.userId, uans.jobId");*/

        
        $answerRow  = $this->coreModel->queryResultArray("SELECT ua.userId, fj.fjCode FROM fj_userAnswers ua JOIN `fj_temp_userjob` tu ON tu.userId = ua.userId AND tu.jobId = ua.jobId JOIN fj_jobs fj ON fj.id = ua.jobId WHERE (tu.mergedVideo IS NULL OR tu.mergedVideo = '') AND (DATEDIFF(now(),tu.createdAt) < 2) OR (ua.userId NOT IN (SELECT userId FROM fj_temp_userjob) AND ua.jobId NOT IN (SELECT jobId FROM fj_temp_userjob)) GROUP BY ua.jobId");

        if(count($answerRow) > 0) {
            $resultData = array();
            foreach ($answerRow as $value) {
                $jwtToken = coreapi_model::getJwtToken($value['userId']);
                $rowData['userToken'] = $jwtToken;
                $rowData['jobCode'] = $value['fjCode'];
                $resultData[] = $rowData;
            }
            
            $this->coreModel->codeMessage('200', $this->lang->line('success'));
            coreapi_model::$returnArray['data'] = $resultData;
        }
        else {
            $this->coreModel->codeMessage('500', 'No unmerged video interview found');
        }
        return coreapi_model::$returnArray;
    } 
}
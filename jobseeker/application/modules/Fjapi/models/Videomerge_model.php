<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : videomerge Model
 * Description : Handle Interview video merge data
 * @author Synergy
 * @createddate : Jan 5, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 17, 2017
 */
   
class videomerge_model extends CI_Model {

    /**
     * Initializing staric variables to be used in different functions
     * @return void
     */
    static $interview           = 'interview';
    static $assessment          = 'assessment';
    static $interviewTable      = 'fj_interviews';
    static $assessmentTable     = 'fj_assessments';
    static $interviewFieldId    = 'interviewId';
    static $assessmentFieldId   = 'assessmentId';
    static $mergeTextFile       = 'F.txt';
    
    /**
     * Description : Use to check if User's Answer Exists Or Not
     * Author : Synergy
     * @param int $jobId, int $userId, string $setType
     * @return array of data
     */
    protected function setAnswerExists($jobId, $userId, $setType) {
        $tableName      = ($setType==self::$interview?self::$interviewTable:self::$assessmentTable);
        $fieldId        = ($setType==self::$interview?self::$interviewFieldId:self::$assessmentFieldId);
        $SETqusResult   = coreapi_model::queryResultArray(" SELECT 
                                                                    A.*,
                                                                    J.title,
                                                                    Q.name,
                                                                    Q.type,
                                                                    QB.title,
                                                                    QB.file
                                                            FROM 
                                                                    fj_userAnswers  A
                                                            JOIN
                                                                    fj_jobs         J,
                                                                    ".$tableName."  Q,
                                                                    fj_question     QB
                                                            WHERE
                                                                    A.".$fieldId."=Q.id   AND
                                                                    A.jobId=J.id           AND
                                                                    A.questionId=QB.id     AND
                                                                    A.jobId=".$jobId."     AND
                                                                    A.userId=".$userId."
                                                           ");
        return $SETqusResult;
    }
    
    /**
     * Description : Use to check if User's Answer Exists Or Not
     * Author : Synergy
     * @param int $jobId, int $userId, string $setType
     * @return array of data
     */
    protected function setAnswerExists_v2($jobId, $userId, $setType) {

        $tableName      = ($setType==self::$interview?self::$interviewTable:self::$assessmentTable);
        $fieldId        = ($setType==self::$interview?self::$interviewFieldId:self::$assessmentFieldId);

        /*$SETqusResult   = coreapi_model::queryResultArray(" SELECT 
                                                                    A.*,
                                                                    J.title,
                                                                    Q.name,
                                                                    Q.type,
                                                                    QB.title,
                                                                    QB.file
                                                            FROM 
                                                                    fj_userAnswers  A
                                                            JOIN
                                                                    fj_jobs         J,
                                                                    ".$tableName."  Q,
                                                                    fj_question     QB
                                                            WHERE
                                                                    A.".$fieldId."=Q.id   AND
                                                                    A.jobId=J.id           AND
                                                                    A.questionId=QB.id     AND
                                                                    A.jobId=".$jobId."     AND
                                                                    A.userId=".$userId." AND A.id IN(SELECT id FROM `fj_userAnswers` WHERE jobId=341 AND userId=5659 AND questionId = 92)
                                                           ");*/

        $SETqusResult   = coreapi_model::queryResultArray(" SELECT 
                                                                    A.*,
                                                                    J.title,
                                                                    Q.name,
                                                                    Q.type,
                                                                    QB.title,
                                                                    QB.file
                                                            FROM 
                                                                    fj_userAnswers  A
                                                            JOIN
                                                                    fj_jobs         J,
                                                                    ".$tableName."  Q,
                                                                    fj_question     QB
                                                            WHERE
                                                                    A.".$fieldId."=Q.id   AND
                                                                    A.jobId=J.id           AND
                                                                    A.questionId=QB.id     AND
                                                                    A.jobId=".$jobId."     AND
                                                                    A.userId=".$userId."
                                                           ");
        
        
        return $SETqusResult;
    }
    
    /**
     * Description : Use to Create Temporary Video Name
     * Author : Synergy
     * @param string $fileNameCreate, string $tempVideoName
     * @return void
     */
    protected function addVideoName($fileNameCreate, $tempVideoName) {
        $qus    = "\r\nfile ".$tempVideoName;
        $fh     = fopen($fileNameCreate, 'a') or die("can't open file");
        fwrite($fh, $qus);
        fclose($fh);
    }
    
    /**
     * Description : Use to Create Temporary Image Name
     * Author : Synergy
     * @param string $questionText, string $tempQusImageName
     * @return void
     */
    protected function createImage($questionText, $tempQusImageName) {
        // Create Image
        header('Content-type: image/png');
        $text           = "Question :\n\n";
        $text          .= $questionText;
        $text_length    = 45;
        $wrap_text      = wordwrap($text, $text_length, "<br />", true);
        $fontsize       = 14;
        $fontfile       = 'OpenSans-Regular.ttf';
        $line_text = str_replace('<br />', "\n", $wrap_text);
        
        header ("Content-type: image/png");
        $string = $text;
        $im = $this->make_wrapped_txt($string, 0, 4, 4, 200);
        imagepng($im, $tempQusImageName);
        imagedestroy($im);
        
    }
    
    /**
     * Description : Use to Create wrapped text
     * Author : Synergy
     * @param string $txt, string $color, int $space, int $font, int $w
     * @return string
     */
    function make_wrapped_txt($txt, $color=000000, $space=4, $font=4, $w=300) {
        if (strlen($color) != 6) $color = 000000;
        $int = hexdec($color);
        $h = imagefontheight($font);
        $fw = imagefontwidth($font);
        $txt = explode("\n", wordwrap($txt, ($w / $fw), "\n"));
        $lines = count($txt);
        $im = imagecreate($w, (($h * $lines) + ($lines * $space)));
        $bg = imagecolorallocate($im, 255, 255, 255);
        $color = imagecolorallocate($im, 0xFF & ($int >> 0x10), 0xFF & ($int >> 0x8), 0xFF & $int);
        $y = 0;
        foreach ($txt as $text) {
          $x = (($w - ($fw * strlen($text))) / 2); 
          imagestring($im, $font, $x, $y, $text, $color);
          $y += ($h + $space);
        }
        return $im;
    }
    
    /*
     *  @Author : Wildnet
     *  @Params : array(user id, referrence job code, 'interview')
     *  @Short Description : Send Company details & Merge User Interview Answer's To Corresponding Questions
     *  @return : array with code. 
     */
    public function answerScreen_v2($params, $setType) {
        $field      = ($setType==self::$interview?self::$interview:self::$assessment);      
        $userId     = coreapi_model::cleanString($params['userId']);
        $id         = coreapi_model::getJwtValue($userId);
        $fjJobId    = coreapi_model::cleanString($params['jobCode']);


        // Check Params Empty Or Not
        if($fjJobId!='' && $userId!='') {
            // User Exists Or Not
            $userRow    = coreapi_model::userExists($id);
            if(count($userRow)>0) {
                // Job Exists Or Not
                $jobRow = coreapi_model::jobExists($fjJobId);
                if(count($jobRow)>0){
                    // User's Answers To corresponding Job Exists Or Not

                    //echo $jobRow['id'].' - '.$id.' - '.$field; die;

                    $answerResult  = $this->setAnswerExists_v2($jobRow['id'], $id, $field);
                    

                    if(count($answerResult)>0) {
                        // Set Final Merged Video Name
                        //$mergeVideoName = rand(11111, 99999).$id.$jobRow['id'].rand(11111, 99999);
                        $mergeVideoName = $id.$jobRow['id'];
                        $tempFile       = array();
                        $myFile         = self::$mergeTextFile;
                        $handle     = fopen ($myFile, "w+");
                        fclose($handle);
                        
                        $fileNameCreate = $mergeVideoName.".txt";
                        $fp = fopen($fileNameCreate,"wb");
                        fwrite($fp,"");
                        fclose($fp);
                        // Loop All User Answers For The Particular Job

                        $allAnswerVideo = array();
                        
                        foreach($answerResult as $key=>$answerRow) {
                            $allAnswerVideo[] = $answerRow['answer'];

                             $result1 = shell_exec('ffmpeg -i ' . escapeshellcmd('/var/www/html/jobseeker/uploads/answers/'.$answerRow['answer']) . ' 2>&1');
                                preg_match('/(?<=Duration: )(\d{2}:\d{2}:\d{2})\.\d{2}/', $result1, $match1);
                                $this->db->insert('fj_interview_time_check', array('duration' => $match1[1], 'file' => $answerRow['answer']));
                            // Question Is Video Format
                            if($answerRow['file']!='' && $answerRow['type']=='1'){
                                $tempQusVideoName = "qus".$answerRow['id'].$answerRow['jobId'].$answerRow['userId'].".flv";
                                $tempAnsVideoName = "ans".$answerRow['id'].$answerRow['jobId'].$answerRow['userId'].".flv";
                                

                                shell_exec("ffmpeg -y -i /var/www/html/jobseeker/uploads/questions/video/".$answerRow['file']." -vf scale=640:480  -vcodec libx264 -preset medium -crf 23 -acodec libmp3lame -ar 44100 -q:a 5 qus1".$tempQusVideoName);

                                shell_exec("ffmpeg -y -i /var/www/html/jobseeker/qus1".$tempQusVideoName." -vf pad='720:720:40:120:black'  -vcodec libx264 -preset medium -crf 23 -acodec libmp3lame -ar 44100 -q:a 5 ".$tempQusVideoName);
                                $this->addVideoName($fileNameCreate, $tempQusVideoName);


                                shell_exec("ffmpeg -y -i /var/www/html/jobseeker/uploads/answers/".$answerRow['answer']." -vf scale=640:480,transpose='2'  -vcodec libx264 -preset medium -crf 23 -acodec libmp3lame -ar 44100 -q:a 5 ".$tempAnsVideoName);
                                
                                $tempAnsVideoName1 = "ans1".$answerRow['id'].$answerRow['jobId'].$answerRow['userId'].".flv";
                                shell_exec("ffmpeg -y -i /var/www/html/jobseeker/".$tempAnsVideoName." -vf pad='720:720:120:40:black'  -vcodec libx264 -preset medium -crf 23 -acodec libmp3lame -ar 44100 -q:a 5 ".$tempAnsVideoName1);
                                $this->addVideoName($fileNameCreate, $tempAnsVideoName1);


                                $tempFile[] = "qus1".$tempQusVideoName;
                                $tempFile[] = $tempQusVideoName;
                                $tempFile[] = $tempAnsVideoName;
                                $tempFile[] = $tempAnsVideoName1;
                            }

                            // Question Is Audio & Text Format
                            else if($answerRow['file']!='' && $answerRow['type']=='2'){             
                                $tempQusImageName   = "temp".$answerRow['id'].$answerRow['jobId'].$answerRow['userId'].".png";
                                $tempQusVideoMP4    = "temp".$answerRow['id'].$answerRow['jobId'].$answerRow['userId'].".mp4";
                                $this->createImage($answerRow['title'], $tempQusImageName);

                                shell_exec("ffmpeg -y -i /var/www/html/jobseeker/uploads/questions/audio/".$answerRow['file']." -loop 1 -f image2 -i ".$tempQusImageName."  -vf scale=720:720 -codec:v mpeg4 -flags:v +qscale -global_quality:v 0 -codec:a libmp3lame -t 06 ".$tempQusVideoMP4);                               
                                $tempQusVideoName = "qus".$answerRow['id'].$answerRow['jobId'].$answerRow['userId'].".flv";
                                shell_exec("ffmpeg -y -i /var/www/html/jobseeker/".$tempQusVideoMP4." -vf scale=720:720  -vcodec libx264 -preset medium -crf 23 -acodec libmp3lame -ar 44100 -q:a 5 ".$tempQusVideoName);
                                $this->addVideoName($fileNameCreate, $tempQusVideoName);
                                

                                $tempAnsVideoName = "ans".$answerRow['id'].$answerRow['jobId'].$answerRow['userId'].".flv";
                                shell_exec("ffmpeg -y -i /var/www/html/jobseeker/uploads/answers/".$answerRow['answer']." -vf scale=640:480,transpose='2'  -vcodec libx264 -preset medium -crf 23 -acodec libmp3lame -ar 44100 -q:a 5 ".$tempAnsVideoName);
                                $tempAnsVideoName1 = "ans1".$answerRow['id'].$answerRow['jobId'].$answerRow['userId'].".flv";
                                shell_exec("ffmpeg -y -i /var/www/html/jobseeker/".$tempAnsVideoName." -vf pad='720:720:120:40:black'  -vcodec libx264 -preset medium -crf 23 -acodec libmp3lame -ar 44100 -q:a 5 ".$tempAnsVideoName1);
                                $this->addVideoName($fileNameCreate, $tempAnsVideoName1); 
                                
                                
                                $tempFile[] = $tempQusImageName;
                                $tempFile[] = $tempQusVideoMP4;
                                $tempFile[] = $tempQusVideoName;
                                $tempFile[] = $tempAnsVideoName;
                                $tempFile[] = $tempAnsVideoName1;
                                
                            }
                            // Question Is Text Format Only
                            else { 
                            
                                $tempQusImageName   = "temp".$answerRow['id'].$answerRow['jobId'].$answerRow['userId'].".png";
                                $tempQusVideoMP4    = "temp".$answerRow['id'].$answerRow['jobId'].$answerRow['userId'].".mp4";
                                $this->createImage($answerRow['title'], $tempQusImageName);

                                shell_exec("ffmpeg -y -i /var/www/html/jobseeker/uploads/testAudio.mp3 -loop 1 -f image2 -i ".$tempQusImageName."  -vf scale=720:720 -codec:v mpeg4 -flags:v +qscale -global_quality:v 0 -codec:a libmp3lame -t 06 ".$tempQusVideoMP4);                             
                                $tempQusVideoName = "qus".$answerRow['id'].$answerRow['jobId'].$answerRow['userId'].".flv";
                                shell_exec("ffmpeg -y -i /var/www/html/jobseeker/".$tempQusVideoMP4." -vf scale=720:720  -vcodec libx264 -preset medium -crf 23 -acodec libmp3lame -ar 44100 -q:a 5 ".$tempQusVideoName);
                                $this->addVideoName($fileNameCreate, $tempQusVideoName);
                                

                                $tempAnsVideoName = "ans".$answerRow['id'].$answerRow['jobId'].$answerRow['userId'].".flv";
                                shell_exec("ffmpeg -y -i /var/www/html/jobseeker/uploads/answers/".$answerRow['answer']." -vf scale=640:480 -vcodec libx264 -preset medium -crf 23 -acodec libmp3lame -ar 44100 -q:a 5 ".$tempAnsVideoName);
                                $tempAnsVideoName1 = "ans1".$answerRow['id'].$answerRow['jobId'].$answerRow['userId'].".flv";
                                shell_exec("ffmpeg -y -i /var/www/html/jobseeker/".$tempAnsVideoName." -vf scale=720:720 -vcodec libx264 -preset medium -crf 23 -acodec libmp3lame -ar 44100 -q:a 5 ".$tempAnsVideoName1);
                                $this->addVideoName($fileNameCreate, $tempAnsVideoName1); 
                                

                                $tempFile[] = $tempQusImageName;
                                $tempFile[] = $tempQusVideoMP4;
                                $tempFile[] = $tempQusVideoName;
                                $tempFile[] = $tempAnsVideoName;
                                $tempFile[] = $tempAnsVideoName1;
                            }
                        }
                        
                        // Combine Video
                        shell_exec("ffmpeg -y -f concat -i  /var/www/html/jobseeker/".$fileNameCreate." -c copy  -vf scale=720:720 -vcodec libx264 -preset medium -crf 23 -acodec libmp3lame -ar 44100 -q:a 5  /var/www/html/jobseeker/uploads/mergeVideo/".$mergeVideoName.".flv 2>&1");
                                
                        $uploadFile = '/var/www/html/jobseeker/uploads/mergeVideo/'.$mergeVideoName.'.flv';
                        $bucketName = 'videoMerge';
                        
                        $s3         = new S3('AKIAJY7WPI2VG2OQ4GGA', 'usHqZSFHZ8Cr20Ms0xjloEIi6iMS6YfEXE5MagUC');
                        
                        if ($s3->putBucket($bucketName, S3::ACL_PUBLIC_READ)) {

                            if ($s3->putObjectFile($uploadFile, $bucketName, baseName($uploadFile), S3::ACL_PUBLIC_READ)) {
                                $userRowJob = coreapi_model::queryRowArray("SELECT id, status, createdAt FROM fj_temp_userjob WHERE userId='$userRow[id]' AND jobId='$jobRow[id]' ORDER BY createdAt DESC LIMIT 0,1");
                                if(count($userRowJob)==0) {
                                    // Save Applied Job Information
                                    $insertData    = array(
                                                        'userId'    => (int)$id,
                                                        'jobId'     => (int)$jobRow['id'],
                                                        'status'    => '1',
                                                        'createdAt' => date('Y-m-d H:i:s'),
                                                        'mergedVideo' => (string)$mergeVideoName.".flv"
                                                    );
                                    $this->db->insert('fj_temp_userjob', $insertData);

                                    $result2 = shell_exec('ffmpeg -i ' . escapeshellcmd('/var/www/html/jobseeker/uploads/mergeVideo/'.$mergeVideoName) . ' 2>&1');

                                    preg_match('/(?<=Duration: )(\d{2}:\d{2}:\d{2})\.\d{2}/', $result2, $match2);
                                    $this->db->insert('fj_interview_time_check', array('duration' => $match2[1], 'file' => $mergeVideoName));

                                    /*unlink($fileNameCreate);
                                    unlink($uploadFile);
                                    foreach($tempFile as $tempFile) {
                                        unlink($tempFile);
                                    }*/

                                    //Removing answer part video
                                    foreach ($allAnswerVideo as $answerPart) {
                                        $answerVideoFile = '/var/www/html/jobseeker/uploads/answers/'.$answerPart;
                                        $answerBucketName = 'fjinterviewanswers';

                                        if ($s3->putBucket($answerBucketName, S3::ACL_PUBLIC_READ)) {
                                            if ($s3->putObjectFile($answerVideoFile, $answerBucketName, baseName($answerVideoFile), S3::ACL_PUBLIC_READ)) {
                                                //unlink($answerVideoFile);
                                            }
                                        }
                                    }
                                }
                                else {
                                    $whereData  = array('userId'=> (int)$id, 'jobId' => (int)$jobRow['id']);
                                    $updateData = array('mergedVideo' => (string)$mergeVideoName.".flv");
                                    $updateJob  = $this->db->where($whereData)->update('fj_temp_userjob', $updateData);

                                    $result2 = shell_exec('ffmpeg -i ' . escapeshellcmd('/var/www/html/jobseeker/uploads/mergeVideo/'.$mergeVideoName) . ' 2>&1');

                                    preg_match('/(?<=Duration: )(\d{2}:\d{2}:\d{2})\.\d{2}/', $result2, $match2);
                                    $this->db->insert('fj_interview_time_check', array('duration' => $match2[1], 'file' => $mergeVideoName));
                                    
                                    /*unlink($fileNameCreate);
                                    unlink($uploadFile);
                                    foreach($tempFile as $tempFile) {
                                        unlink($tempFile);
                                    }*/

                                    //Removing answer part video
                                    foreach ($allAnswerVideo as $answerPart) {
                                        $answerVideoFile = '/var/www/html/jobseeker/uploads/answers/'.$answerPart;
                                        $answerBucketName = 'fjinterviewanswers';

                                        if ($s3->putBucket($answerBucketName, S3::ACL_PUBLIC_READ)) {
                                            if ($s3->putObjectFile($answerVideoFile, $answerBucketName, baseName($answerVideoFile), S3::ACL_PUBLIC_READ)) {
                                                //unlink($answerVideoFile);
                                            }
                                        }
                                    }
                                }

                                $updateDataSaved     = array('status'=>'0');
                                $whereDataSaved      = array(
                                                        'userId'    => (int)$id,
                                                        'jobId'     => (int)$jobRow['id'],
                                                        'status'    => '1'
                                                    );
                                $this->db->where($whereDataSaved)->update('fj_userSavedJobs',$updateDataSaved);

                                coreapi_model::codeMessage('200', $this->lang->line('success'));
                            } 
                            else {
                                coreapi_model::codeMessage('503', 'S# error.');
                            }
                        } else {
                            coreapi_model::codeMessage('503', 'Something went wrong.');
                        }
                    }              
                    else {
                        coreapi_model::codeMessage('400', $this->lang->line('no_job_found'));
                    }
                }
                else {
                    coreapi_model::codeMessage('500', $this->lang->line('no_job_found'));
                }
            }
            else {
                coreapi_model::codeMessage('500', $this->lang->line('invalid_user'));
            }
        }
        else {
            coreapi_model::codeMessage('500', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }

    /*
     *  @Author : Wildnet
     *  @Params : array(user id, referrence job code, 'interview')
     *  @Short Description : Send Company details & Merge User Interview Answer's To Corresponding Questions
     *  @return : array with code. 
     */
    public function answerScreen($params, $setType) {
        $field      = ($setType==self::$interview?self::$interview:self::$assessment);      
        $userId     = coreapi_model::cleanString($params['userId']);
        $id         = coreapi_model::getJwtValue($userId);
        $fjJobId    = coreapi_model::cleanString($params['jobCode']);

        // Check Params Empty Or Not
        if($fjJobId!='' && $userId!='') {
            // User Exists Or Not
            $userRow    = coreapi_model::userExists($id);
            if(count($userRow)>0) {
                // Job Exists Or Not
                $jobRow = coreapi_model::jobExists($fjJobId);
                if(count($jobRow)>0){
                    // User's Answers To corresponding Job Exists Or Not

                    //echo $jobRow['id'].' - '.$id.' - '.$field; die;

                    $answerResult  = $this->setAnswerExists($jobRow['id'], $id, $field);
                    //print_r($answerResult);exit;
                    

                    if(count($answerResult)>0) {
                        // Set Final Merged Video Name
                        //$mergeVideoName = rand(11111, 99999).$id.$jobRow['id'].rand(11111, 99999);
                        $mergeVideoName = $id.$jobRow['id'];
                        $tempFile       = array();
                        $myFile         = self::$mergeTextFile;
                        $handle     = fopen ($myFile, "w+");
                        fclose($handle);
                        
                        $fileNameCreate = $mergeVideoName.".txt";
                        $fp = fopen($fileNameCreate,"wb");
                        fwrite($fp,"");
                        fclose($fp);
                        // Loop All User Answers For The Particular Job

                        $allAnswerVideo = array();
                        
                        foreach($answerResult as $key=>$answerRow) {
                            $allAnswerVideo[] = $answerRow['answer'];

                             $result1 = shell_exec('ffmpeg -i ' . escapeshellcmd('/var/www/html/jobseeker/uploads/answers/'.$answerRow['answer']) . ' 2>&1');
                                preg_match('/(?<=Duration: )(\d{2}:\d{2}:\d{2})\.\d{2}/', $result1, $match1);
                                $this->db->insert('fj_interview_time_check', array('duration' => $match1[1], 'file' => $answerRow['answer']));
                            // Question Is Video Format
                            if($answerRow['file']!='' && $answerRow['type']=='1'){
                                $tempQusVideoName = "qus".$answerRow['id'].$answerRow['jobId'].$answerRow['userId'].".flv";
                                $tempAnsVideoName = "ans".$answerRow['id'].$answerRow['jobId'].$answerRow['userId'].".flv";
                                

                                shell_exec("ffmpeg -y -i /var/www/html/jobseeker/uploads/questions/video/".$answerRow['file']." -vf scale=640:480  -vcodec libx264 -preset medium -crf 23 -acodec libmp3lame -ar 44100 -q:a 5 qus1".$tempQusVideoName);

                                shell_exec("ffmpeg -y -i /var/www/html/jobseeker/qus1".$tempQusVideoName." -vf pad='720:720:40:120:black'  -vcodec libx264 -preset medium -crf 23 -acodec libmp3lame -ar 44100 -q:a 5 ".$tempQusVideoName);
                                $this->addVideoName($fileNameCreate, $tempQusVideoName);


                                shell_exec("ffmpeg -y -i /var/www/html/jobseeker/uploads/answers/".$answerRow['answer']." -vf scale=640:480 -vcodec libx264 -preset medium -crf 23 -acodec libmp3lame -ar 44100 -q:a 5 ".$tempAnsVideoName);
                                
                                $tempAnsVideoName1 = "ans1".$answerRow['id'].$answerRow['jobId'].$answerRow['userId'].".flv";
                                shell_exec("ffmpeg -y -i /var/www/html/jobseeker/".$tempAnsVideoName." -vf scale=720:720  -vcodec libx264 -preset medium -crf 23 -acodec libmp3lame -ar 44100 -q:a 5 ".$tempAnsVideoName1);
                                $this->addVideoName($fileNameCreate, $tempAnsVideoName1);


                                $tempFile[] = "qus1".$tempQusVideoName;
                                $tempFile[] = $tempQusVideoName;
                                $tempFile[] = $tempAnsVideoName;
                                $tempFile[] = $tempAnsVideoName1;
                            }

                            // Question Is Audio & Text Format
                            else if($answerRow['file']!='' && $answerRow['type']=='2'){             
                                $tempQusImageName   = "temp".$answerRow['id'].$answerRow['jobId'].$answerRow['userId'].".png";
                                $tempQusVideoMP4    = "temp".$answerRow['id'].$answerRow['jobId'].$answerRow['userId'].".mp4";
                                $this->createImage($answerRow['title'], $tempQusImageName);

                                shell_exec("ffmpeg -y -i /var/www/html/jobseeker/uploads/questions/audio/".$answerRow['file']." -loop 1 -f image2 -i ".$tempQusImageName."  -vf scale=720:720 -codec:v mpeg4 -flags:v +qscale -global_quality:v 0 -codec:a libmp3lame -t 06 ".$tempQusVideoMP4);                               
                                $tempQusVideoName = "qus".$answerRow['id'].$answerRow['jobId'].$answerRow['userId'].".flv";
                                shell_exec("ffmpeg -y -i /var/www/html/jobseeker/".$tempQusVideoMP4." -vf scale=720:720  -vcodec libx264 -preset medium -crf 23 -acodec libmp3lame -ar 44100 -q:a 5 ".$tempQusVideoName);
                                $this->addVideoName($fileNameCreate, $tempQusVideoName);
                                

                                $tempAnsVideoName = "ans".$answerRow['id'].$answerRow['jobId'].$answerRow['userId'].".flv";
                                shell_exec("ffmpeg -y -i /var/www/html/jobseeker/uploads/answers/".$answerRow['answer']." -vf scale=640:480  -vcodec libx264 -preset medium -crf 23 -acodec libmp3lame -ar 44100 -q:a 5 ".$tempAnsVideoName);
                                $tempAnsVideoName1 = "ans1".$answerRow['id'].$answerRow['jobId'].$answerRow['userId'].".flv";
                                shell_exec("ffmpeg -y -i /var/www/html/jobseeker/".$tempAnsVideoName." -vf scale=720:720 -vcodec libx264 -preset medium -crf 23 -acodec libmp3lame -ar 44100 -q:a 5 ".$tempAnsVideoName1);
                                $this->addVideoName($fileNameCreate, $tempAnsVideoName1); 
                                
                                
                                $tempFile[] = $tempQusImageName;
                                $tempFile[] = $tempQusVideoMP4;
                                $tempFile[] = $tempQusVideoName;
                                $tempFile[] = $tempAnsVideoName;
                                $tempFile[] = $tempAnsVideoName1;
                                
                            }
                            // Question Is Text Format Only
                            else { 
							
                                $tempQusImageName   = "temp".$answerRow['id'].$answerRow['jobId'].$answerRow['userId'].".png";
                                $tempQusVideoMP4    = "temp".$answerRow['id'].$answerRow['jobId'].$answerRow['userId'].".mp4";
                                $this->createImage($answerRow['title'], $tempQusImageName);

                                shell_exec("ffmpeg -y -i /var/www/html/jobseeker/uploads/testAudio.mp3 -loop 1 -f image2 -i ".$tempQusImageName."  -vf scale=720:720 -codec:v mpeg4 -flags:v +qscale -global_quality:v 0 -codec:a libmp3lame -t 06 ".$tempQusVideoMP4);                             
                                $tempQusVideoName = "qus".$answerRow['id'].$answerRow['jobId'].$answerRow['userId'].".flv";
                                shell_exec("ffmpeg -y -i /var/www/html/jobseeker/".$tempQusVideoMP4." -vf scale=720:720  -vcodec libx264 -preset medium -crf 23 -acodec libmp3lame -ar 44100 -q:a 5 ".$tempQusVideoName);
                                $this->addVideoName($fileNameCreate, $tempQusVideoName);
                                

                                $tempAnsVideoName = "ans".$answerRow['id'].$answerRow['jobId'].$answerRow['userId'].".flv";
                                shell_exec("ffmpeg -y -i /var/www/html/jobseeker/uploads/answers/".$answerRow['answer']." -vf scale=640:480 -vcodec libx264 -preset medium -crf 23 -acodec libmp3lame -ar 44100 -q:a 5 ".$tempAnsVideoName);
                                $tempAnsVideoName1 = "ans1".$answerRow['id'].$answerRow['jobId'].$answerRow['userId'].".flv";
                                shell_exec("ffmpeg -y -i /var/www/html/jobseeker/".$tempAnsVideoName." -vf scale=720:720 -vcodec libx264 -preset medium -crf 23 -acodec libmp3lame -ar 44100 -q:a 5 ".$tempAnsVideoName1);
                                $this->addVideoName($fileNameCreate, $tempAnsVideoName1);
                                

                                $tempFile[] = $tempQusImageName;
                                $tempFile[] = $tempQusVideoMP4;
                                $tempFile[] = $tempQusVideoName;
                                $tempFile[] = $tempAnsVideoName;
                                $tempFile[] = $tempAnsVideoName1;
                            }
                        }
						
                        // Combine Video
                        shell_exec("ffmpeg -y -f concat -i  /var/www/html/jobseeker/".$fileNameCreate." -c copy  -vf scale=720:720 -vcodec libx264 -preset medium -crf 23 -acodec libmp3lame -ar 44100 -q:a 5  /var/www/html/jobseeker/uploads/mergeVideo/".$mergeVideoName.".flv 2>&1");
		                        
                        $uploadFile = '/var/www/html/jobseeker/uploads/mergeVideo/'.$mergeVideoName.'.flv';
                        $bucketName = 'videoMerge';
						
                        $s3         = new S3('AKIAJY7WPI2VG2OQ4GGA', 'usHqZSFHZ8Cr20Ms0xjloEIi6iMS6YfEXE5MagUC');
						
                        if ($s3->putBucket($bucketName, S3::ACL_PUBLIC_READ)) {

                            if ($s3->putObjectFile($uploadFile, $bucketName, baseName($uploadFile), S3::ACL_PUBLIC_READ)) {
                                $userRowJob = coreapi_model::queryRowArray("SELECT id, status, createdAt FROM fj_temp_userjob WHERE userId='$userRow[id]' AND jobId='$jobRow[id]' ORDER BY createdAt DESC LIMIT 0,1");
                                if(count($userRowJob)==0) {
                                    // Save Applied Job Information
                                    $insertData    = array(
                                                        'userId'    => (int)$id,
                                                        'jobId'     => (int)$jobRow['id'],
                                                        'status'    => '1',
                                                        'createdAt' => date('Y-m-d H:i:s'),
                                                        'mergedVideo' => (string)$mergeVideoName.".flv"
                                                    );
                                    $this->db->insert('fj_temp_userjob', $insertData);

                                    $result2 = shell_exec('ffmpeg -i ' . escapeshellcmd('/var/www/html/jobseeker/uploads/mergeVideo/'.$mergeVideoName) . ' 2>&1');

                                    preg_match('/(?<=Duration: )(\d{2}:\d{2}:\d{2})\.\d{2}/', $result2, $match2);
                                    $this->db->insert('fj_interview_time_check', array('duration' => $match2[1], 'file' => $mergeVideoName));

                                    unlink($fileNameCreate);
                                    unlink($uploadFile);
                                    foreach($tempFile as $tempFile) {
                                        unlink($tempFile);
                                    }

                                    //Removing answer part video
                                    foreach ($allAnswerVideo as $answerPart) {
                                        $answerVideoFile = '/var/www/html/jobseeker/uploads/answers/'.$answerPart;
                                        $answerBucketName = 'fjinterviewanswers';

                                        if ($s3->putBucket($answerBucketName, S3::ACL_PUBLIC_READ)) {
                                            if ($s3->putObjectFile($answerVideoFile, $answerBucketName, baseName($answerVideoFile), S3::ACL_PUBLIC_READ)) {
                                                unlink($answerVideoFile);
                                            }
                                        }
                                    }
                                }
                                else {
                                    $whereData  = array('userId'=> (int)$id, 'jobId' => (int)$jobRow['id']);
                                    $updateData = array('mergedVideo' => (string)$mergeVideoName.".flv");
                                    $updateJob  = $this->db->where($whereData)->update('fj_temp_userjob', $updateData);

                                    $result2 = shell_exec('ffmpeg -i ' . escapeshellcmd('/var/www/html/jobseeker/uploads/mergeVideo/'.$mergeVideoName) . ' 2>&1');

                                    preg_match('/(?<=Duration: )(\d{2}:\d{2}:\d{2})\.\d{2}/', $result2, $match2);
                                    $this->db->insert('fj_interview_time_check', array('duration' => $match2[1], 'file' => $mergeVideoName));
                                    
                                    unlink($fileNameCreate);
                                    unlink($uploadFile);
                                    foreach($tempFile as $tempFile) {
                                        unlink($tempFile);
                                    }

                                    //Removing answer part video
                                    foreach ($allAnswerVideo as $answerPart) {
                                        $answerVideoFile = '/var/www/html/jobseeker/uploads/answers/'.$answerPart;
                                        $answerBucketName = 'fjinterviewanswers';

                                        if ($s3->putBucket($answerBucketName, S3::ACL_PUBLIC_READ)) {
                                            if ($s3->putObjectFile($answerVideoFile, $answerBucketName, baseName($answerVideoFile), S3::ACL_PUBLIC_READ)) {
                                                unlink($answerVideoFile);
                                            }
                                        }
                                    }
                                }

                                $updateDataSaved     = array('status'=>'0');
                                $whereDataSaved      = array(
                                                        'userId'    => (int)$id,
                                                        'jobId'     => (int)$jobRow['id'],
                                                        'status'    => '1'
                                                    );
                                $this->db->where($whereDataSaved)->update('fj_userSavedJobs',$updateDataSaved);

                                coreapi_model::codeMessage('200', $this->lang->line('success'));
                            } 
                            else {
                                coreapi_model::codeMessage('503', 'S# error.');
                            }
                        } else {
                            coreapi_model::codeMessage('503', 'Something went wrong.');
                        }
                    }              
                    else {
                        coreapi_model::codeMessage('400', $this->lang->line('no_job_found'));
                    }
                }
                else {
                    coreapi_model::codeMessage('500', $this->lang->line('no_job_found'));
                }
            }
            else {
                coreapi_model::codeMessage('500', $this->lang->line('invalid_user'));
            }
        }
        else {
            coreapi_model::codeMessage('500', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }
    
	public function FfmpegTest(){
		$fileNameCreate = "testScript.txt";
		$mergeVideoName = "output";
		shell_exec("ffmpeg -y -f concat -i  /var/www/html/admin/".$fileNameCreate." -c copy  -vf scale=720:720 -vcodec libx264 -preset medium -crf 23 -acodec libmp3lame -ar 44100 -q:a 5  /var/www/html/admin/uploads/mergeVideo/".$mergeVideoName.".flv 2>&1");
	}
    /**
     * Description : After given interview on a job, display company details 
     * Author : Synergy
     * @param array $params(userId, jobCode), string $setType
     * @return string
     */
    public function thankyouScreen($params, $setType) {
        $field      = ($setType==self::$interview?self::$interview:self::$assessment);        
        $userId     = coreapi_model::cleanString($params['userId']);
        $id         = coreapi_model::getJwtValue($userId);
        $fjJobId    = coreapi_model::cleanString($params['jobCode']);
        // Check Params Empty Or Not
        if($fjJobId!='' && $userId!='') {
            // User Exists Or Not
            $userRow    = coreapi_model::userExists($id);
            if(count($userRow)>0) {
                // Job Exists Or Not
                $jobRow = coreapi_model::jobExists($fjJobId);
                if(count($jobRow)>0){
                    $companyData                = coreapi_model::queryRowArray("SELECT * FROM fj_users WHERE status='1' AND id='".$jobRow['createdBy']."' ");
                    $rowData['companyVideo']    = ($jobRow['jd']!='' && $jobRow['jd']!='0'?base_url() . '/uploads/jd/'.$jobRow['jd']:'');
                    $rowData['companyName']     = (string)($companyData['company']!=''?$companyData['company']:'');
                    $rowData['companyLogo']     = ($companyData['companyLogo']!=''?base_url() . '/uploads/corporateLogo/'.$companyData['companyLogo']:'');
                    coreapi_model::$data[]      = $rowData;
                    coreapi_model::codeMessage('200', $this->lang->line('success'));
                    coreapi_model::$returnArray['data'] = coreapi_model::$data;
                }
                else {
                    coreapi_model::codeMessage('500', $this->lang->line('no_job_found'));
                }
            }
            else {
                coreapi_model::codeMessage('500', $this->lang->line('invalid_user'));
            }
        }
        else {
            coreapi_model::codeMessage('500', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Userapi Model
 * Description : Handle CRUD operation for User
 * @author Synergy
 * @createddate : Jan 10, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 17, 2017
 */

/*
  #############################################################
  -------------------- Api Listing --------------------
  =========USER REGISTRATION, LOGIN, ACCOUNT RELATED=========
  00.    userExists
  01.    generateOTP
  02.    signUp
  03.         EmailExistCheck
  04.         MobileExistCheck
  05.         OtpExistCheck
  06.         getState
  07.         getCity
  08.         getOffice
  09.         getPincode
  10.         InsertUserInfo
  11.    recoverPassword
  12.    signIn
  13.         changePassword
  14.    getUserInformation
  15.         getBasicInformation
  16.         getEducationalInformation
  17.         getOtherInformation
  18.         getSocialInformation
  19.         profilePercent
  20.    editBasicinfo
  21.    editEducationalInfo
  22.         getCourses
  23.         searchUniversity
  24.         removeQualification
  25.    editOtherInfo
  26.    editSocialInfo
  27.    editBasicinfo
  28.    getTokenID
  #############################################################
 */

class Userapi_model extends CI_Model {

    //-------------------------------------------------------------------------//
    //------------------------- Registration API ------------------------------//
    //-------------------------------------------------------------------------//
    ############################################################################
    /*
     *  @Author : Wildnet
     *  @Params : array(user email).
     *  @Short Description : email already exists
     *  @return : array with code. 
     */
    //-------------- Check Email Id Already Exists or Not Starts --------------//
	function __construct() {
        parent::__construct();        
        $this->load->model('coreapi_model', 'coreModel');
    }
	
    public function EmailExistCheck($email_id) {
        $email_id = $this->coreModel->cleanString($email_id);
        $query = $this->db->query("SELECT id FROM fj_users WHERE email = '$email_id' ");
        $total_records = $query->num_rows();
        return (($total_records > 0) ? FALSE : TRUE);
    }
	
	public function EmailExistCheck_V1($email_id) {
        $email_id = $this->coreModel->cleanString($email_id);
        $query = $this->db->query("SELECT id, role FROM fj_users WHERE email = '$email_id' ");
        $total_records = $query->num_rows();
		return (($total_records > 0) ? TRUE : FALSE);
    }

    //--------------- Check Email Id Already Exists or Not Ends ---------------//
    ############################################################################
    #
    #
    #
    ############################################################################    
    /*
     *  @Author : Wildnet
     *  @Params : array(user mobile).
     *  @Short Description : mobile already exists
     *  @return : array with code. 
     */
    //--------------- Check Email Id Already Exists or Not Starts ---------------//
    public function MobileExistCheck($mobile) {
        $mobile = $this->coreModel->cleanString($mobile);
        $query = $this->db->query("SELECT id FROM fj_users WHERE mobile = '$mobile' ");
        $total_records = $query->num_rows();
        return (($total_records > 0) ? FALSE : TRUE);
    }

    //--------------- Check Email Id Already Exists or Not Ends ---------------//
    ############################################################################
    #
    #
    #
    ############################################################################
    /*
     *  @Author : Wildnet
     *  @Params : array(user mobile, otp).
     *  @Short Description : otp valid or not
     *  @return : array with code. 
     */
    //-------------- Check OTP Against User Exists or Not Starts --------------//
    public function OtpExistCheck($mobile, $otp) {
        $mobile = $this->coreModel->cleanString($mobile);
        $query = $this->db->query("SELECT otpCode FROM fj_otp WHERE mobileNumber = '$mobile' AND otpCode = '$otp' AND otpType='1' ORDER BY id DESC LIMIT 0,1 ");
        $total_records = $query->num_rows();
        return (($total_records > 0) ? TRUE : FALSE);
    }

    //-------------- Check OTP Against User Exists or Not Starts --------------//
    ############################################################################
    /*
     *  @Author : Wildnet
     *  @Params : array(field data).
     *  @Short Description : insert data to Db
     *  @return : insert id
     */
    //-------------------------- Insert Data Starts ---------------------------//    
    public function InsertUserInfo($data) {
        $this->db->set('createdAt', 'NOW()', FALSE);
        $this->db->insert('fj_users', $data);
        return $this->db->insert_id();
    }
    
    
    //-------------GET USER TOKEN-------------------------//
    ##############################################################
    
   
    //--------------------------- Insert Data Ends ----------------------------//
    ############################################################################
    #
    #
    #
    ############################################################################
    /*
     *  @Author : Wildnet
     *  @Params : mobile.
     *  @Short Description : get mobile number and genrate otp, Send otp to mobile number
     *  @return : array with code. 
     */
    //-------------------------- Generate OTP Start ---------------------------//
    public function generateOTP($params) {
        if ($params['mobile'] != '') {
            $mobile = substr($this->coreModel->cleanString($params['mobile']), -10);
            $email = $this->coreModel->cleanString($params['email']);
            $randomOTP = mt_rand(111111, 999999);

            $email_exist_check = $this->EmailExistCheck($email);
            $mobile_exist_check = $this->MobileExistCheck($mobile);

            if ($email_exist_check == FALSE || $mobile_exist_check == FALSE) {
                $message = '';
                $message .= ($email_exist_check == FALSE ? $this->lang->line('email_id') : '');
                $message .= ($email_exist_check == FALSE && $mobile_exist_check == FALSE ? $this->lang->line('and') : '');
                $message .= ($mobile_exist_check == FALSE ? $this->lang->line('mobile_number') : '');
                $message .= $this->lang->line('already_exists');
                $this->coreModel->codeMessage('202', $message);
            } else {
                $result = $this->coreModel->queryRowArray("SELECT id FROM fj_otp WHERE mobileNumber='$mobile' LIMIT 0,1");
                if (count($result) > 0) {
                    $updateData = array('otpCode' => $randomOTP, 'otpType' => '1');
                    try {
                        $queryExecute = $this->db->where('mobileNumber', $mobile)->update('fj_otp', $updateData);
                    } catch (Exception $e) {
                        $this->coreModel->codeMessage('500', $e->getMessage());
                    }
                    $this->OTP_MODEL->sendMessage($mobile, $randomOTP);
                    $this->coreModel->codeMessage('200', $this->lang->line('success'));
                    coreapi_model::$returnArray['OTP'] = $randomOTP;
                } else {
                    $insertData = array('otpCode' => $randomOTP, 'mobileNumber' => $mobile, 'otpType' => '1');
                    try {
                        $this->db->insert('fj_otp', $insertData);
                    } catch (Exception $e) {
                        $this->coreModel->codeMessage('500', $e->getMessage());
                    }
                    $this->OTP_MODEL->sendMessage($mobile, $randomOTP);
                    $this->coreModel->codeMessage('200', $this->lang->line('success'));
                    coreapi_model::$returnArray['OTP'] = $randomOTP;
                }
            }
        } else {
            $this->coreModel->codeMessage('300', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }

    //-------------------------- Generate OTP End -----------------------------//
    ############################################################################ 
    #
    #
    #
    ############################################################################
    /*
     *  @Author : Wildnet
     *  @Params : array(user email, pass etc).
     *  @Short Description : register a user
     *  @return : array with code. 
     */
    //---------------------- Sign Up/ Registration User Starts ----------------//
    public function virtualSignUp($params) {
        
        $email      = $this->coreModel->cleanString($params['email']);
        $candidateName = $this->coreModel->cleanString($params['userName']);
        
        if (
                $email      != ''
        ) {
            try {
                $email_exist_check  = $this->EmailExistCheck_V1($email);
                
                if ($email_exist_check == FALSE) {
                    coreapi_model::$data['email']           = $email;
                    coreapi_model::$data['fullname']        = $candidateName;
                    coreapi_model::$data['activationKey']   = md5(rand(0, 1000) . 'uniquefrasehere');
                    coreapi_model::$data['status']          = '1';
                    coreapi_model::$data['role']            = '3';
                    // Insert Registartion Data
                    $create_user = $this->InsertUserInfo(coreapi_model::$data);
                    if ($create_user) {
                        // Get ID
                        $insert_id = $this->db->insert_id();

                        $invitationData = $this->coreModel->queryRowArray("SELECT id FROM fj_jobInvitationForUser WHERE email='$email'");
                        if($invitationData) {
                            $this->db->where('id', $invitationData['id'])->update('fj_jobInvitationForUser', array('userId'=>$insert_id));
                        }

                        //Send Email To User
                        $this->email->from('noreply@vdohire.com', 'VDOHire');
                        $this->email->to(coreapi_model::$data['email']);
                        $this->email->subject('VDOHire Registration Confirmation');
                        $url = base_url() . 'users/validate/' . coreapi_model::$data['activationKey'];
                        $message = "
                                        Hi {$fullname}
                                        </br></br>
                                        You have been registered successfully with VDOHire.
                                        <br/></br>
                                        Your registered email id is {$email} and mobile number is {$mobile}.
                                        <br/></br>
                                        Enjoy browsing the VDOHire services.
                                        </br></br></br>
                                        Thanks,</br></br>
                                        Team VDOHire";
                        $this->email->message($message);
                        $this->email->set_mailtype('html');
                        $this->email->send();

                        coreapi_model::$data['token']   = $this->coreModel->getJwtToken($insert_id);
                        coreapi_model::$data['dob']     = '';
                        coreapi_model::$data['image']   = '';
                        unset(coreapi_model::$data['password']);
                        unset(coreapi_model::$data['status']);
                        unset(coreapi_model::$data['role']);
                        unset(coreapi_model::$data['activationKey']);
                        unset(coreapi_model::$data['deviceId']);
                        unset(coreapi_model::$data['deviceType']);
                        $userData[] = coreapi_model::$data;
                        $this->coreModel->codeMessage('200', $this->lang->line('success'));
                        coreapi_model::$returnArray['data'] = $userData;
                        
                        if (!empty($deviceId) && !empty($deviceType)) {
                            // remove device token from other user
                            $tokenduplicatedata = $this->coreModel->queryRowArray("SELECT GROUP_CONCAT(id) AS id FROM fj_users WHERE deviceId='" . $params['device_token'] . "'");
                            if (!empty($tokenduplicatedata['result']['id'])) {
                                $idlist             = $tokenduplicatedata['result']['id'];                    
                                $removetokendata    = $this->db->where_in('deviceId', $idlist)->update('fj_users', array('deviceId'=>''));
                            }

                            $formdata['device_type']    = trim($params['device_type']);
                            $formdata['device_token']   = trim($params['device_token']);
                            $this->db->where_in('id', $insert_id)->update('fj_users', array('deviceId'=>$device_token, 'deviceType'=>$device_type));
                        }
                    } else {
                        $this->coreModel->codeMessage('500', $this->lang->line('error_occured'));
                        coreapi_model::$returnArray['data'] = array();
                    }
                } else {
                    $row = $this->coreModel->queryRowArray("SELECT * FROM fj_users WHERE email='$email'");
                    if (count($row) > 0) {
                        if (($row['status'] == '1') && ($row['role'] == '3')) {
                                $invitationData = $this->coreModel->queryRowArray("SELECT id FROM fj_jobInvitationForUser WHERE email='$email'");
                                if($invitationData) {
                                    $this->db->where('id', $invitationData['id'])->update('fj_jobInvitationForUser', array('userId'=>$row['id']));
                                }

                                $auditionRow = $this->coreModel->queryRowArray("SELECT * FROM fj_userAudition WHERE (userId='$row[id]')");
                                $this->coreModel->codeMessage('200', $this->lang->line('success'));
                                coreapi_model::$data['token']            = $this->coreModel->getJwtToken($row['id']);
                                coreapi_model::$data['userName']         = (string) $row['fullname'];
                                coreapi_model::$data['userEmail']        = (string) $row['email'];
                                coreapi_model::$data['userMobile']       = (string) $row['mobile'];
                                coreapi_model::$data['userImage']        = ($row['image'] != '' ? base_url() . '/uploads/userImages/' . $row['image'] . ".PNG" : '');
                                coreapi_model::$data['userDob']          = (string) (($row['dob'] == '' || $row['dob'] == '0000-00-00') ? '' : date('Y', strtotime($row['dob'])));
                                coreapi_model::$data['userGender']       = (string) $row['gender'];
                                coreapi_model::$data['userPincode']      = (string) ($row['pinCode'] != 0 ? $row['pinCode'] : '');
                                coreapi_model::$data['profilePercent']   = Userapi_model::profilePercent($row['id']);
                                $userData[] = coreapi_model::$data;
								coreapi_model::$returnArray['data'] = $userData;
                                
								if (!empty($params['device_type']) && !empty($params['device_token'])) {
                                    // remove device token from other user
                                    $tokenduplicatedata = $this->coreModel->queryRowArray("SELECT GROUP_CONCAT(id) AS id FROM fj_users WHERE deviceId='" . $params['device_token'] . "'");
                                    if (!empty($tokenduplicatedata['result']['id'])) {
                                        $idlist             = $tokenduplicatedata['result']['id'];                    
                                        $removetokendata    = $this->db->where_in('deviceId', $idlist)->update('fj_users', array('deviceId'=>''));
                                    }

                                    $formdata['device_type']    = trim($params['device_type']);
                                    $formdata['device_token']   = trim($params['device_token']);
                                    $this->db->where_in('id', $row['id'])->update('fj_users', array('deviceId'=>$device_token, 'deviceType'=>$device_type));
                                }
                        } else {
                            $this->coreModel->codeMessage('500', $this->lang->line('invalid_user'));
                            coreapi_model::$returnArray['data'] = array();
                        }
                    } else {
                        $this->coreModel->codeMessage('500', $this->lang->line('invalid_user'));
                        coreapi_model::$returnArray['data'] = array();
                    }
                }
            } catch (Exception $e) {
                $this->coreModel->codeMessage('500', $e->getMessage());
                coreapi_model::$returnArray['data'] = array();
            }
        } else {
            $this->coreModel->codeMessage('300', $this->lang->line('missing_required_fields'));
            coreapi_model::$returnArray['data'] = array();
        }
        return coreapi_model::$returnArray;
    }
    
    public function signUp($params) {
        $fullname   = $this->coreModel->cleanString($params['fullname']);
        $mobile     = substr($this->coreModel->cleanString($params['mobile']), -10);
        $password   = $this->coreModel->cleanString($params['password']);
        $email      = $this->coreModel->cleanString($params['email']);
        $gender     = $this->coreModel->cleanString($params['gender']);
        $pincode    = $this->coreModel->cleanString($params['pincode']);
        $deviceId   = $this->coreModel->cleanString($params['deviceId']);
        $deviceType = $this->coreModel->cleanString($params['deviceType']);
        $otpCode    = $this->coreModel->cleanString($params['otpCode']);
        // Check Params Keys
        if (
                $fullname   != '' &&
                $mobile     != '' &&
                $password   != '' &&
                $email      != '' &&
                $gender     != '' &&
                $deviceType != '' &&
                $deviceType != '' &&
                $otpCode    != '' &&
                $pincode    != ''
        ) {
            try {
                $email_exist_check  = $this->EmailExistCheck($email);
                $mobile_exist_check = $this->MobileExistCheck($mobile);
                if ($email_exist_check == FALSE || $mobile_exist_check == FALSE) {
                    $message = '';
                    $message .= ($email_exist_check == FALSE ? $this->lang->line('email_id') : '');
                    $message .= ($email_exist_check == FALSE && $mobile_exist_check == FALSE ? $this->lang->line('and') : '');
                    $message .= ($mobile_exist_check == FALSE ? $this->lang->line('mobile_number') : '');
                    $message .= $this->lang->line('already_exists');
                    $this->coreModel->codeMessage('202', $message);
                } else {
                    $otp_exist_check = $this->OtpExistCheck($mobile, $otpCode);
                    if ($otp_exist_check == TRUE) {
                        coreapi_model::$data['fullname']        = $fullname;
                        coreapi_model::$data['mobile']          = $mobile;
                        coreapi_model::$data['password']        = password_hash($password, PASSWORD_BCRYPT);
                        coreapi_model::$data['email']           = $email;
                        coreapi_model::$data['gender']          = (($gender == 'Male' || $gender == 'male') ? 1 : 2);
                        coreapi_model::$data['pinCode']         = $pincode;
                        coreapi_model::$data['deviceType']      = $deviceType;
                        coreapi_model::$data['deviceId']        = $deviceId;
                        coreapi_model::$data['activationKey']   = md5(rand(0, 1000) . 'uniquefrasehere');
                        coreapi_model::$data['status']          = '1';
                        coreapi_model::$data['role']            = '3';
                        // Insert Registartion Data
                        $create_user = $this->InsertUserInfo(coreapi_model::$data);
                        if ($create_user) {
                            // Get ID
                            $insert_id = $this->db->insert_id();
                            //Send Email To User
                            $this->email->from('noreply@firstjob.com', 'First Job');
                            $this->email->to(coreapi_model::$data['email']);
                            $this->email->subject('FJ Registration Confirmation');
                            $url = base_url() . 'users/validate/' . coreapi_model::$data['activationKey'];
                            $message = "
                                            Hi {$fullname}
                                            </br></br>
                                            You have been registered successfully with FirstJob.
                                            <br/></br>
                                            Your registered email id is {$email} and mobile number is {$mobile}.
                                            <br/></br>
                                            Enjoy browsing the FirstJob services.
                                            </br></br></br>
                                            Thanks,</br></br>
                                            Team FirstJob";
                            $this->email->message($message);
                            $this->email->set_mailtype('html');
                            $this->email->send();

                            coreapi_model::$data['token']   = $this->coreModel->getJwtToken($insert_id);
                            coreapi_model::$data['dob']     = '';
                            coreapi_model::$data['image']   = '';
                            unset(coreapi_model::$data['password']);
                            unset(coreapi_model::$data['status']);
                            unset(coreapi_model::$data['role']);
                            unset(coreapi_model::$data['activationKey']);
                            unset(coreapi_model::$data['deviceId']);
                            unset(coreapi_model::$data['deviceType']);
                            $userData[] = coreapi_model::$data;
                            $this->coreModel->codeMessage('200', $this->lang->line('success'));
                            coreapi_model::$returnArray['data'] = $userData;
                            
                            if (!empty($deviceId) && !empty($deviceType)) {
                                // remove device token from other user
                                $tokenduplicatedata = $this->coreModel->queryRowArray("SELECT GROUP_CONCAT(id) AS id FROM fj_users WHERE deviceId='" . $params['device_token'] . "'");
                                if (!empty($tokenduplicatedata['result']['id'])) {
                                    $idlist             = $tokenduplicatedata['result']['id'];                    
                                    $removetokendata    = $this->db->where_in('deviceId', $idlist)->update('fj_users', array('deviceId'=>''));
                                }

                                $formdata['device_type']    = trim($params['device_type']);
                                $formdata['device_token']   = trim($params['device_token']);
                                $this->db->where_in('id', $insert_id)->update('fj_users', array('deviceId'=>$device_token, 'deviceType'=>$device_type));
                            }
                        } else {
                            $this->coreModel->codeMessage('500', $this->lang->line('error_occured'));
                        }
                    } else {
                        $this->coreModel->codeMessage('500', $this->lang->line('invalid_otp'));
                    }
                }
            } catch (Exception $e) {
                $this->coreModel->codeMessage('500', $e->getMessage());
            }
        } else {
            $this->coreModel->codeMessage('300', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }

    /*
     *  @Author : Synergy
     *  @Params : array(user email, pass etc).
     *  @Short Description : Created version 2 api for register a user 
     *  @return : array with code. 
     */
    public function signUp_v2($params) {
        $fullname   = $this->coreModel->cleanString($params['fullname']);
        $mobile     = substr($this->coreModel->cleanString($params['mobile']), -10);
        $password   = $this->coreModel->cleanString($params['password']);
        $email      = $this->coreModel->cleanString($params['email']);
        $gender     = $this->coreModel->cleanString($params['gender']);
        $pincode    = $this->coreModel->cleanString($params['pincode']);
        $deviceId   = $this->coreModel->cleanString($params['deviceId']);
        $deviceType = $this->coreModel->cleanString($params['deviceType']);
        // Check Params Keys
        if (
                $fullname   != '' &&
                $mobile     != '' &&
                $password   != '' &&
                $email      != '' &&
                $gender     != '' &&
                $deviceType != '' &&
                $deviceType != '' &&
                $pincode    != ''
        ) {
            try {
                $email_exist_check  = $this->EmailExistCheck($email);
                $mobile_exist_check = $this->MobileExistCheck($mobile);
                if ($email_exist_check == FALSE || $mobile_exist_check == FALSE) {
                    $message = '';
                    $message .= ($email_exist_check == FALSE ? $this->lang->line('email_id') : '');
                    $message .= ($email_exist_check == FALSE && $mobile_exist_check == FALSE ? $this->lang->line('and') : '');
                    $message .= ($mobile_exist_check == FALSE ? $this->lang->line('mobile_number') : '');
                    $message .= $this->lang->line('already_exists');
                    $this->coreModel->codeMessage('202', $message);
                } else {
                    coreapi_model::$data['fullname']        = $fullname;
                    coreapi_model::$data['mobile']          = $mobile;
                    coreapi_model::$data['password']        = password_hash($password, PASSWORD_BCRYPT);
                    coreapi_model::$data['email']           = $email;
                    coreapi_model::$data['gender']          = (($gender == 'Male' || $gender == 'male') ? 1 : 2);
                    coreapi_model::$data['pinCode']         = $pincode;
                    coreapi_model::$data['deviceType']      = $deviceType;
                    coreapi_model::$data['deviceId']        = $deviceId;
                    coreapi_model::$data['activationKey']   = md5(rand(0, 1000) . 'uniquefrasehere');
                    coreapi_model::$data['status']          = '1';
                    coreapi_model::$data['role']            = '3';
                    // Insert Registartion Data
                    $create_user = $this->InsertUserInfo(coreapi_model::$data);
                    if ($create_user) {
                        // Get ID
                        $insert_id = $this->db->insert_id();
                        //Send Email To User
                        $this->email->from('noreply@firstjob.com', 'First Job');
                        $this->email->to(coreapi_model::$data['email']);
                        $this->email->subject('FJ Registration Confirmation');
                        $url = base_url() . 'users/validate/' . coreapi_model::$data['activationKey'];
                        $message = "
                                        Hi {$fullname}
                                        </br></br>
                                        You have been registered successfully with FirstJob.
                                        <br/></br>
                                        Your registered email id is {$email} and mobile number is {$mobile}.
                                        <br/></br>
                                        Enjoy browsing the FirstJob services.
                                        </br></br></br>
                                        Thanks,</br></br>
                                        Team FirstJob";
                        $this->email->message($message);
                        $this->email->set_mailtype('html');
                        $this->email->send();

                        coreapi_model::$data['token']   = $this->coreModel->getJwtToken($insert_id);
                        coreapi_model::$data['dob']     = '';
                        coreapi_model::$data['image']   = '';
                        unset(coreapi_model::$data['password']);
                        unset(coreapi_model::$data['status']);
                        unset(coreapi_model::$data['role']);
                        unset(coreapi_model::$data['activationKey']);
                        unset(coreapi_model::$data['deviceId']);
                        unset(coreapi_model::$data['deviceType']);
                        $userData[] = coreapi_model::$data;
                        $this->coreModel->codeMessage('200', $this->lang->line('success'));
                        coreapi_model::$returnArray['data'] = $userData;
                        
                        if (!empty($deviceId) && !empty($deviceType)) {
                            // remove device token from other user
                            $tokenduplicatedata = $this->coreModel->queryRowArray("SELECT GROUP_CONCAT(id) AS id FROM fj_users WHERE deviceId='" . $params['device_token'] . "'");
                            if (!empty($tokenduplicatedata['result']['id'])) {
                                $idlist             = $tokenduplicatedata['result']['id'];                    
                                $removetokendata    = $this->db->where_in('deviceId', $idlist)->update('fj_users', array('deviceId'=>''));
                            }

                            $formdata['device_type']    = trim($params['device_type']);
                            $formdata['device_token']   = trim($params['device_token']);
                            $this->db->where_in('id', $insert_id)->update('fj_users', array('deviceId'=>$device_token, 'deviceType'=>$device_type));
                        }
                    } else {
                        $this->coreModel->codeMessage('500', $this->lang->line('error_occured'));
                    }
                }
            } catch (Exception $e) {
                $this->coreModel->codeMessage('500', $e->getMessage());
            }
        } else {
            $this->coreModel->codeMessage('300', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }

    /*
     *  @Author : Synergy
     *  @Params : array(user email, pass etc).
     *  @Short Description : Created version 3 api for register a user 
     *  @return : array with code. 
     */
	public function completeProfile($params) {
        $fullname   = $this->coreModel->cleanString($params['fullname']);
        $mobile     = substr($this->coreModel->cleanString($params['mobile']), -10);
        $password   = $this->coreModel->cleanString($params['password']);
        $confirm      = $this->coreModel->cleanString($params['confirm']);
		$userId = $this->coreModel->cleanString($params['userId']);
        $id = $this->coreModel->getJwtValue($userId);
        // Check Params Keys
        if (
                $id   != '' &&
				$fullname   != '' &&
                $mobile     != '' &&
                $password   != '' &&
				$confirm   != '' 
        ) {
            try {
                $row = $this->coreModel->queryRowArray("SELECT email FROM fj_users WHERE id='$id' AND status='1' LIMIT 0,1");
				if (count($row) > 0) {
					$updateData = array(
						'fullname' => $fullname,
						'email' => $row['email'],
						'mobile' => $mobile,
						'password' => password_hash($password, PASSWORD_BCRYPT)
					);
					$query = $this->db->where('id', $id)->update('fj_users', $updateData);
					$_SESSION['userName'] = $fullname;
					
                    $this->coreModel->codeMessage('200', $this->lang->line('success'));
                    
                } else {
					$message = "User doesn't Exists";
                    $this->coreModel->codeMessage('202', $message);
                }
            } catch (Exception $e) {
                $this->coreModel->codeMessage('500', $e->getMessage());
            }
        } else {
            $this->coreModel->codeMessage('300', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }
		
    public function signUp_v3($params) {
        $base_urls = "https://".$_SERVER["HTTP_HOST"];
        $fullname   = $this->coreModel->cleanString($params['fullname']);
        $mobile     = substr($this->coreModel->cleanString($params['mobile']), -10);
        $password   = $this->coreModel->cleanString($params['password']);
        $email      = $this->coreModel->cleanString($params['email']);
        $deviceId   = $this->coreModel->cleanString($params['deviceId']);
        $deviceType = $this->coreModel->cleanString($params['deviceType']);
        // Check Params Keys
        if (
                $fullname   != '' &&
                $mobile     != '' &&
                $password   != '' &&
                $email      != ''  
        ) {
            try {
                $email_exist_check  = $this->EmailExistCheck($email);
                $mobile_exist_check = $this->MobileExistCheck($mobile);
                if ($email_exist_check == FALSE || $mobile_exist_check == FALSE) {
                    $message = '';
                    $message .= ($email_exist_check == FALSE ? $this->lang->line('email_id') : '');
                    $message .= ($email_exist_check == FALSE && $mobile_exist_check == FALSE ? $this->lang->line('and') : '');
                    $message .= ($mobile_exist_check == FALSE ? $this->lang->line('mobile_number') : '');
                    $message .= $this->lang->line('already_exists');
                    $this->coreModel->codeMessage('202', $message);
                } else {
                    coreapi_model::$data['fullname']        = $fullname;
                    coreapi_model::$data['mobile']          = $mobile;
                    coreapi_model::$data['password']        = password_hash($password, PASSWORD_BCRYPT);
                    coreapi_model::$data['email']           = $email;
                    coreapi_model::$data['deviceType']      = $deviceType;
                    coreapi_model::$data['deviceId']        = $deviceId;
                    coreapi_model::$data['activationKey']   = md5(rand(0, 1000) . 'uniquefrasehere');
                    coreapi_model::$data['status']          = '1';
                    coreapi_model::$data['role']            = '3';
                    coreapi_model::$data['imageSource'] = 0;

                    $create_user = $this->InsertUserInfo(coreapi_model::$data);

                    if ($create_user) {
                        $insert_id = $this->db->insert_id();
                        //Send Email To User
                        $this->email->from('noreply@vdohire.com', 'VDOHire');
                        $this->email->to(coreapi_model::$data['email']);
                        $this->email->subject('Welcome to VDOHire');
                        $url = base_url() . 'users/validate/' . coreapi_model::$data['activationKey'];
                        $message = "
                                        Dear {$fullname}
                                        <br/><br/>
                                        Congratulations, you have successfully registered on VDOHire!
                                        <br/><br/>
                                        Your registration details are as follows:
                                        <br/><br/>
                                        Registered phone number: {$mobile}
                                        <br/> 
                                        Registered email id: {$email}
                                        <br/><br/>
                                        Log on to edit your profile, update your resume, complete job interview, and more. You can log on to FirstJob web console by clicking <a href='{$base_urls}' target='_blank'>here</a> or you can also browse our services through VDOHire android app. Download the app from <a href='https://goo.gl/Le9H7b' target='_blank'>here</a>, if you haven’t done it yet.
                                        <br/><br/></br>
                                        Wishing you all the best for your job search.
                                        <br/><br/>
                                        Team VDOHire
                                        <br/><br/>
                                        Get hired Anytime, Anywhere.";
                        $this->email->message($message);
                        $this->email->set_mailtype('html');
                        $this->email->send();

                        coreapi_model::$data['token']   = $this->coreModel->getJwtToken($insert_id);
                        coreapi_model::$data['dob']     = '';
                        coreapi_model::$data['image']   = '';
                        unset(coreapi_model::$data['password']);
                        unset(coreapi_model::$data['status']);
                        unset(coreapi_model::$data['role']);
                        unset(coreapi_model::$data['activationKey']);
                        unset(coreapi_model::$data['deviceId']);
                        unset(coreapi_model::$data['deviceType']);
                        $userData[] = coreapi_model::$data;
                        $this->coreModel->codeMessage('200', $this->lang->line('success'));
                        coreapi_model::$returnArray['data'] = $userData;
                        
                        if (!empty($deviceId) && !empty($deviceType)) {
                            // remove device token from other user
                            $tokenduplicatedata = $this->coreModel->queryRowArray("SELECT GROUP_CONCAT(id) AS id FROM fj_users WHERE deviceId='" . $deviceId . "' AND id != '" . $insert_id . "'");
                            if (!empty($tokenduplicatedata['id'])) {
                                $idlist             = trim($tokenduplicatedata['id'], "'");
                                $intarray = explode(",", $idlist);                   
                                $removetokendata    = $this->db->where_in('id', $intarray)->update('fj_users', array('deviceId'=>''));
                            } 
                        }
                    } else {
                        $this->coreModel->codeMessage('500', $this->lang->line('error_occured'));
                    }
                }
            } catch (Exception $e) {
                $this->coreModel->codeMessage('500', $e->getMessage());
            }
        } else {
            $this->coreModel->codeMessage('300', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }
    //--------------------- Sign Up/ Registration User Ends -------------------//


    ############################################################################
    /*
     *  @Author : Wildnet
     *  @Params : null.
     *  @Short Description : get all state names
     *  @return : array with code. 
     */
    //------------------------- Get State List Start --------------------------//
    public function getState($params) {
        $result = $this->coreModel->queryResultArray("SELECT DISTINCT(state) FROM fj_pinCode ORDER BY state ASC");
        if (count($result) > 0) {
            foreach ($result as $row) {
                coreapi_model::$data['stateName'] = (string) $row['state'];
                $stateData[] = coreapi_model::$data;
                unset(coreapi_model::$data['stateName']);
            }
            $this->coreModel->codeMessage('200', $this->lang->line('success'));
            coreapi_model::$returnArray['data'] = $stateData;
        } else {
            $this->coreModel->codeMessage('300', $this->lang->line('state_not_found'));
        }
        return coreapi_model::$returnArray;
    }

    //------------------------- Get State List End ----------------------------//
    ############################################################################
    #
    #
    #
    ############################################################################
    /*
     *  @Author : Wildnet
     *  @Params : state name.
     *  @Short Description : get all city names for a state
     *  @return : array with code. 
     */
    //--------------------------- Get City List Start -------------------------//
    public function getCity($params) {
        $state = $params['state'];
        if ($state != '') {
            $result = $this->coreModel->queryResultArray("SELECT DISTINCT(city) FROM fj_pinCode WHERE state='$state' ORDER BY city ASC");
            if (count($result) > 0) {
                foreach ($result as $row) {
                    coreapi_model::$data['cityName'] = (string) $row['city'];
                    $cityData[] = coreapi_model::$data;
                    unset(coreapi_model::$data['cityName']);
                }
                $this->coreModel->codeMessage('200', $this->lang->line('success'));
                coreapi_model::$returnArray['data'] = $cityData;
            } else {
                $this->coreModel->codeMessage('300', $this->lang->line('city_not_found'));
            }
        } else {
            $this->coreModel->codeMessage('500', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }

    //--------------------------- Get City List End ---------------------------//
    ############################################################################
    #
    #
    #
    ############################################################################
    /*
     *  @Author : Wildnet
     *  @Params : state, city.
     *  @Short Description : get all office names for a state-city
     *  @return : array with code. 
     */
    //-------------------------- Get Office List Start ------------------------//
    public function getOffice($params) {
        $state = $params['state'];
        $city = $params['city'];
        if ($state != '' && $city != '') {
            $result = $this->coreModel->queryResultArray("SELECT DISTINCT(office) FROM fj_pinCode WHERE state='$state' AND city='$city' ORDER BY office ASC");
            if (count($result) > 0) {
                foreach ($result as $row) {
                    coreapi_model::$data['officeName'] = (string) $row['office'];
                    $officeData[] = coreapi_model::$data;
                    unset(coreapi_model::$data['officeName']);
                }
                $this->coreModel->codeMessage('200', $this->lang->line('success'));
                coreapi_model::$returnArray['data'] = $officeData;
            } else {
                $this->coreModel->codeMessage('300', $this->lang->line('office_not_found'));
            }
        } else {
            $this->coreModel->codeMessage('500', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }

    //-------------------------- Get Office List End --------------------------//
    ############################################################################
    #
    #
    #
    ############################################################################
    //
    /*
     *  @Author : Wildnet
     *  @Params : state, city, office.
     *  @Short Description : get pincode for a state-city-office
     *  @return : array with code. 
     */
    //--------------------------- Get Office List Start -----------------------//
    public function getPincode($params) {
        $state = $params['state'];
        $city = $params['city'];
        $office = $params['office'];
        if ($state != '' && $city != '' && $office != '') {
            $result = $this->coreModel->queryResultArray("SELECT pincode FROM fj_pinCode WHERE state='$state' AND city='$city' AND office='$office' LIMIT 0,1");
            if (count($result) > 0) {
                foreach ($result as $row) {
                    coreapi_model::$data['pinCode'] = (int) $row['pincode'];
                    $pincodeData[] = coreapi_model::$data;
                    //unset(coreapi_model::$data['pinCode']);
                }
                $this->coreModel->codeMessage('200', $this->lang->line('success'));
                coreapi_model::$returnArray['data'] = $pincodeData;
            } else {
                $this->coreModel->codeMessage('300', $this->lang->line('pincode_not_found'));
            }
        } else {
            $this->coreModel->codeMessage('500', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }

    //--------------------------- Get Office List End -------------------------//
    ############################################################################
    #
    #
    #
    ############################################################################
    //-------------------------------------------------------------------------//
    //------------------------- Registration API ------------------------------//
    //-------------------------------------------------------------------------//
    //-------------------------------------------------------------------------//
    //----------------------------- Login API  --------------------------------//
    //-------------------------------------------------------------------------// 
    ############################################################################
    /*
     *  @Author : Wildnet
     *  @Params : array(user email, pass etc).
     *  @Short Description : login user
     *  @return : array with code. 
     */
    //------------------------ Sign In/ Login User Starts ---------------------//
    public function signIn($params) {
		$NameFull = isset($params['name'])?$params['name']:"";
		$Device = isset($params['device_token'])?$params['device_token']:"";
		$DeviceType = isset($params['device_type'])?$params['device_type']:"";
		
        $password               = $this->coreModel->cleanString($params['password']);
        $emailORmobile          = $this->coreModel->cleanString($params['email']);
        @$social_networking_id  = $this->coreModel->cleanString($params['networking_id']);
        $fullname               = $this->coreModel->cleanString($NameFull);
        $email                  = $this->coreModel->cleanString($params['email']);
        //$link_id = $this->coreModel->cleanString($params['link_id']);
        @$social_networking     = $this->coreModel->cleanString($params['social_networking_type']);        
        $device_token           = $this->coreModel->cleanString($Device);
        $device_type            = $this->coreModel->cleanString($DeviceType);
        
        // Check Params Keys
        if (@$social_networking != '') {
            //$social_networking =>1=fb,2=tw,3=linkdin,4=g+
            if (@$social_networking == '1') {
                $type = 'fb_id';
            }
            if (@$social_networking == '2') {
                $type = 'tw_id';
            }
            if (@$social_networking == '3') {
                $type = 'link_id';
            }
            if (@$social_networking == '4') {
                $type = 'google_id';
            }

            $row = $this->coreModel->queryRowArray("SELECT * FROM fj_users WHERE $type ='$social_networking_id' AND role='3'");
            if (count($row) > 0) {
                $auditionRow = $this->coreModel->queryRowArray("SELECT * FROM fj_userAudition WHERE (userId='$row[id]')");
                $this->coreModel->codeMessage('200', $this->lang->line('success'));
                coreapi_model::$returnArray['token']            = $this->coreModel->getJwtToken($row['id']);
                coreapi_model::$returnArray['userName']         = (string) $row['fullname'];
                coreapi_model::$returnArray['userEmail']        = (string) $row['email'];
                coreapi_model::$returnArray['userMobile']       = (string) $row['mobile'];
                coreapi_model::$returnArray['userImage']        = ($row['image'] != '' ? base_url() . '/uploads/userImages/' . $row['image'] . ".PNG" : '');
                coreapi_model::$returnArray['userDob']          = (string) (($row['dob'] == '' || $row['dob'] == '0000-00-00') ? '' : date('Y', strtotime($row['dob'])));
                coreapi_model::$returnArray['userGender']       = (string) $row['gender'];
                coreapi_model::$returnArray['userPincode']      = (string) ($row['pinCode'] != 0 ? $row['pinCode'] : '');
                coreapi_model::$returnArray['profilePercent']   = Userapi_model::profilePercent($row['id']);
                if (count($auditionRow) > 0) {
                    coreapi_model::$returnArray['auditionNumber']   = (string) $auditionRow['auditionNumber'];
                    coreapi_model::$returnArray['auditionId']       = (string) $auditionRow['id'];
                    coreapi_model::$returnArray['auditionURL']      = (string) (base_url() . '/page/getAudition/' . encryptURLparam($auditionRow['id'], URLparamKey));
                } else {
                    coreapi_model::$returnArray['auditionNumber']   = '';
                    coreapi_model::$returnArray['auditionId']       = '';
                    coreapi_model::$returnArray['auditionURL']      = '';
                }
                
                if (!empty($params['device_type']) && !empty($params['device_token'])) {
                    // remove device token from other user
                    $tokenduplicatedata = $this->coreModel->queryRowArray("SELECT GROUP_CONCAT(id) AS id FROM fj_users WHERE deviceId='" . $params['device_token'] . "'");
                    if (!empty($tokenduplicatedata['result']['id'])) {
                        $idlist             = $tokenduplicatedata['result']['id'];                    
                        $removetokendata    = $this->db->where_in('deviceId', $idlist)->update('fj_users', array('deviceId'=>''));
                    }
                    
                    $formdata['device_type']    = trim($params['device_type']);
                    $formdata['device_token']   = trim($params['device_token']);
                    $this->db->where_in('id', $row['id'])->update('fj_users', array('deviceId'=>$device_token, 'deviceType'=>$device_type));
                }
            } 
            else {
                if($emailORmobile !=''){
                    $check_status   = $this->coreModel->queryRowArray("SELECT * FROM fj_users WHERE (email='$emailORmobile' OR mobile='$emailORmobile') AND role='3'");
                    $count_test     = count($check_status);
                }else{
                    $count_test = 0;
                }
                
                if ($count_test > 0) {
                    if($check_status['role']=='3') {                      
                      $user_data_array = array(
                          $type => $social_networking_id
                      );
                      $query = $this->db->where('id', $check_status['id'])->update('fj_users', $user_data_array);

                      $row = $this->coreModel->queryRowArray("SELECT * FROM fj_users WHERE $type='$social_networking_id'");
                      if (count($row) > 0) {
                          $auditionRow = $this->coreModel->queryRowArray("SELECT * FROM fj_userAudition WHERE (userId='$row[id]')");
                          $this->coreModel->codeMessage('200', $this->lang->line('success'));
                          coreapi_model::$returnArray['token']          = $this->coreModel->getJwtToken($row['id']);
                          coreapi_model::$returnArray['userName']       = (string) $row['fullname'];
                          coreapi_model::$returnArray['userEmail']      = (string) $row['email'];
                          coreapi_model::$returnArray['userMobile']     = (string) $row['mobile'];
                          coreapi_model::$returnArray['userImage']      = ($row['image'] != '' ? base_url() . '/uploads/userImages/' . $row['image'] . ".PNG" : '');
                          coreapi_model::$returnArray['userDob']        = (string) (($row['dob'] == '' || $row['dob'] == '0000-00-00') ? '' : date('Y', strtotime($row['dob'])));
                          coreapi_model::$returnArray['userGender']     = (string) $row['gender'];
                          coreapi_model::$returnArray['userPincode']    = (string) ($row['pinCode'] != 0 ? $row['pinCode'] : '');
                          coreapi_model::$returnArray['profilePercent'] = Userapi_model::profilePercent($row['id']);
                          if (count($auditionRow) > 0) {
                              coreapi_model::$returnArray['auditionNumber'] = (string) $auditionRow['auditionNumber'];
                              coreapi_model::$returnArray['auditionId']     = (string) $auditionRow['id'];
                              coreapi_model::$returnArray['auditionURL']    = (string) (base_url() . '/page/getAudition/' . encryptURLparam($auditionRow['id'], URLparamKey));
                          } else {
                              coreapi_model::$returnArray['auditionNumber'] = '';
                              coreapi_model::$returnArray['auditionId']     = '';
                              coreapi_model::$returnArray['auditionURL']    = '';
                          }
                        
                        if (!empty($params['device_type']) && !empty($params['device_token'])) {
                            // remove device token from other user
                            $tokenduplicatedata = $this->coreModel->queryRowArray("SELECT GROUP_CONCAT(id) AS id FROM fj_users WHERE deviceId='" . $params['device_token'] . "'");
                            if (!empty($tokenduplicatedata['result']['id'])) {
                                $idlist             = $tokenduplicatedata['result']['id'];                    
                                $removetokendata    = $this->db->where_in('deviceId', $idlist)->update('fj_users', array('deviceId'=>''));
                            }

                            $formdata['device_type']    = trim($params['device_type']);
                            $formdata['device_token']   = trim($params['device_token']);
                            $this->db->where_in('id', $row['id'])->update('fj_users', array('deviceId'=>$device_token, 'deviceType'=>$device_type));
                        }
                      }
                    }
                    else {
                        $this->coreModel->codeMessage('500', $this->lang->line('invalid_user'));
                    }
                }
                else {
                    $user_data_array = array(
                                              $type         => $social_networking_id,
                                              'fullname'    => $fullname,
                                              'email'       => $email,
                                              'role'        => '3'
                                          );
                    $insert_id = $this->db->insert('fj_users', $user_data_array);

                    $row = $this->coreModel->queryRowArray("SELECT * FROM fj_users WHERE $type='$social_networking_id'");
                    if (count($row) > 0) {
                        $auditionRow = $this->coreModel->queryRowArray("SELECT * FROM fj_userAudition WHERE (userId='$row[id]')");
                        $this->coreModel->codeMessage('200', $this->lang->line('success'));
                        coreapi_model::$returnArray['token']            = $this->coreModel->getJwtToken($row['id']);
                        coreapi_model::$returnArray['userName']         = (string) $row['fullname'];
                        coreapi_model::$returnArray['userEmail']        = (string) $row['email'];
                        coreapi_model::$returnArray['userMobile']       = (string) $row['mobile'];
                        coreapi_model::$returnArray['userImage']        = ($row['image'] != '' ? base_url() . '/uploads/userImages/' . $row['image'] . ".PNG" : '');
                        coreapi_model::$returnArray['userDob']          = (string) (($row['dob'] == '' || $row['dob'] == '0000-00-00') ? '' : date('Y', strtotime($row['dob'])));
                        coreapi_model::$returnArray['userGender']       = (string) $row['gender'];
                        coreapi_model::$returnArray['userPincode']      = (string) ($row['pinCode'] != 0 ? $row['pinCode'] : '');
                        coreapi_model::$returnArray['profilePercent']   = Userapi_model::profilePercent($row['id']);
                        if (count($auditionRow) > 0) {
                            coreapi_model::$returnArray['auditionNumber']   = (string) $auditionRow['auditionNumber'];
                            coreapi_model::$returnArray['auditionId']       = (string) $auditionRow['id'];
                            coreapi_model::$returnArray['auditionURL']      = (string) (base_url() . '/page/getAudition/' . encryptURLparam($auditionRow['id'], URLparamKey));
                        } else {
                            coreapi_model::$returnArray['auditionNumber']   = '';
                            coreapi_model::$returnArray['auditionId']       = '';
                            coreapi_model::$returnArray['auditionURL']      = '';
                        }
                        
                        if (!empty($params['device_type']) && !empty($params['device_token'])) {
                            // remove device token from other user
                            $tokenduplicatedata = $this->coreModel->queryRowArray("SELECT GROUP_CONCAT(id) AS id FROM fj_users WHERE deviceId='" . $params['device_token'] . "'");
                            if (!empty($tokenduplicatedata['result']['id'])) {
                                $idlist             = $tokenduplicatedata['result']['id'];                    
                                $removetokendata    = $this->db->where_in('deviceId', $idlist)->update('fj_users', array('deviceId'=>''));
                            }

                            $formdata['device_type']    = trim($params['device_type']);
                            $formdata['device_token']   = trim($params['device_token']);
                            $this->db->where_in('id', $row['id'])->update('fj_users', array('deviceId'=>$device_token, 'deviceType'=>$device_type));
                        }
                    }
                }
            }
        }
        else if ($password != '' && $emailORmobile != '') {
            $row = $this->coreModel->queryRowArray("SELECT * FROM fj_users WHERE (email='$emailORmobile' OR mobile='$emailORmobile') AND role='3'");
            if (count($row) > 0) {
                if ($row['status'] == '1') {
                    if (password_verify($password, $row['password'])) {
                        $auditionRow = $this->coreModel->queryRowArray("SELECT * FROM fj_userAudition WHERE (userId='$row[id]')");
                        $this->coreModel->codeMessage('200', $this->lang->line('success'));
                        coreapi_model::$returnArray['token']            = $this->coreModel->getJwtToken($row['id']);
                        coreapi_model::$returnArray['userName']         = (string) $row['fullname'];
                        coreapi_model::$returnArray['userEmail']        = (string) $row['email'];
                        coreapi_model::$returnArray['userMobile']       = (string) $row['mobile'];
                        coreapi_model::$returnArray['userImage']        = ($row['image'] != '' ? base_url() . '/uploads/userImages/' . $row['image'] . ".PNG" : '');
                        coreapi_model::$returnArray['userDob']          = (string) (($row['dob'] == '' || $row['dob'] == '0000-00-00') ? '' : date('Y', strtotime($row['dob'])));
                        coreapi_model::$returnArray['userGender']       = (string) $row['gender'];
                        coreapi_model::$returnArray['userPincode']      = (string) ($row['pinCode'] != 0 ? $row['pinCode'] : '');
                        coreapi_model::$returnArray['profilePercent']   = Userapi_model::profilePercent($row['id']);
                        if (count($auditionRow) > 0) {
                            coreapi_model::$returnArray['auditionNumber']   = (string) $auditionRow['auditionNumber'];
                            coreapi_model::$returnArray['auditionId']       = (string) $auditionRow['id'];
                            coreapi_model::$returnArray['auditionURL']      = (string) (base_url() . '/page/getAudition/' . encryptURLparam($auditionRow['id'], URLparamKey));
                        } else {
                            coreapi_model::$returnArray['auditionNumber']   = '';
                            coreapi_model::$returnArray['auditionId']       = '';
                            coreapi_model::$returnArray['auditionURL']      = '';
                        }
                        
                        if (!empty($params['device_type']) && !empty($params['device_token'])) {
                            // remove device token from other user
                            $tokenduplicatedata = $this->coreModel->queryRowArray("SELECT GROUP_CONCAT(id) AS id FROM fj_users WHERE deviceId='" . $params['device_token'] . "'");
                            if (!empty($tokenduplicatedata['result']['id'])) {
                                $idlist             = $tokenduplicatedata['result']['id'];                    
                                $removetokendata    = $this->db->where_in('deviceId', $idlist)->update('fj_users', array('deviceId'=>''));
                            }

                            $formdata['device_type']    = trim($params['device_type']);
                            $formdata['device_token']   = trim($params['device_token']);
                            $this->db->where_in('id', $row['id'])->update('fj_users', array('deviceId'=>$device_token, 'deviceType'=>$device_type));
                        }
                    } else {
                        $this->coreModel->codeMessage('202', $this->lang->line('invalid_password'));
                    }
                } else {
                    $this->coreModel->codeMessage('202', $this->lang->line('account_not_active'));
                }
            } else {
                $this->coreModel->codeMessage('500', $this->lang->line('invalid_user'));
            }
        } else { if(@$social_networking !=''){
            $this->coreModel->codeMessage('200', $this->lang->line('success'));
        }else{
            $this->coreModel->codeMessage('300', $this->lang->line('missing_required_fields'));
        }
            
        }
        return coreapi_model::$returnArray;
    }

    //------------------------ Sign In/ Login User Ends -----------------------//
    ############################################################################
    //-------------------------------------------------------------------------//
    //----------------------------- Login API  --------------------------------//
    //-------------------------------------------------------------------------//
    //-------------------------------------------------------------------------//
    //------------------------- Forgot Password API  --------------------------//
    //-------------------------------------------------------------------------//   
    ############################################################################
    /*
     *  @Author : Wildnet
     *  @Params : array(user email, mobile etc).
     *  @Short Description : recover user password
     *  @return : array with code. 
     */
    //------------------ Forgot / Recover User Password Starts ----------------//
    public function recoverPassword($params) {
        $emailORmobile = $this->coreModel->cleanString($params['emailORmobile']);
        // Check Params Keys
        if ($emailORmobile != '') {
            $row = $this->coreModel->queryRowArray("SELECT  id, fullname, status, password, email FROM fj_users WHERE (email='$emailORmobile' OR mobile='$emailORmobile')");
            if (count($row) > 0) {
                if ($row['status'] == '1') {
                    //$password = substr(str_shuffle(md5(time())), 0, 10);
                    
                    $password           = substr(str_shuffle(md5(time())), 0, 6);
                    $newPassword        = password_hash($password, PASSWORD_BCRYPT);
                    $updateData         = array('password' => $newPassword);
                    $query              = $this->db->where('id', $row['id'])->update('fj_users', $updateData);
                    $config['mailtype'] = 'html';
                    /* Send Email To User */
                    $this->email->from('noreply@firstjob.co.in', 'First Job');
                    $this->email->to($row['email']);
                    $this->email->subject('FirstJob: Forgotten Password');
                    $temp['password']   = $password;
                    $temp['name']       = $row['fullname'];                    
                    $body               = $this->load->view('emails/recoverPasswordTemp.php', $temp, TRUE);
                    $this->email->message($body);
                    $this->email->set_mailtype('html');                    
                    $this->email->send();
                    $this->coreModel->codeMessage('200', $this->lang->line('pwd_sent'));
                } else {
                    $this->coreModel->codeMessage('202', $this->lang->line('account_not_active'));
                }
            } else {
                $this->coreModel->codeMessage('500', $this->lang->line('invalid_user'));
            }
        } else {
            $this->coreModel->codeMessage('300', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }

    //------------------ Forgot / Recover User Password Ends ------------------//
    ############################################################################
    //-------------------------------------------------------------------------//
    //------------------------- Forgot Password API  --------------------------//
    //-------------------------------------------------------------------------//
    //-------------------------------------------------------------------------//
    //-------------------------- Change Password ------------------------------//
    //-------------------------------------------------------------------------//
    ############################################################################
    /*
     *  @Author : Wildnet
     *  @Params : array(user id, old password, new password)
     *  @Short Description : Change User Password
     *  @return : array with code. 
     */
    //--------------------- Change User's Password Starts ---------------------//
    public function changePassword($params) {
        $userId = $this->coreModel->cleanString($params['userId']);
        $id = $this->coreModel->getJwtValue($userId);
        $oldPassword = $this->coreModel->cleanString($params['oldPassword']);
        $newPassword = $this->coreModel->cleanString($params['newPassword']);

        if ($oldPassword != '' && $newPassword != '' && $userId != '') {
            $row = $this->coreModel->queryRowArray("SELECT id, status, password FROM fj_users WHERE id='$id' LIMIT 0,1");
            if (count($row) > 0) {
                if ($row['status'] == '1') {
                    if (password_verify($oldPassword, $row['password'])) {
                        $password = password_hash($newPassword, PASSWORD_BCRYPT);
                        $updateData = array('password' => $password);
                        try {
                            $queryExecute = $this->db->where('id', $id)->where('status', '1')->update('fj_users', $updateData);
                        } catch (Exception $e) {
                            //--------------- Response Data ---------------//
                            $this->coreModel->codeMessage('500', $e->getMessage());
                        }
                        if ($queryExecute) {
                            $this->coreModel->codeMessage('200', $this->lang->line('success'));
                        } else {
                            $this->coreModel->codeMessage('500', $this->lang->line('something_wrong'));
                        }
                    } else {
                        $this->coreModel->codeMessage('500', $this->lang->line('invalid_old_password'));
                    }
                } else {
                    $this->coreModel->codeMessage('500', $this->lang->line('account_not_active'));
                }
            } else {
                $this->coreModel->codeMessage('500', $this->lang->line('no_record_found'));
            }
        } else {
            $this->coreModel->codeMessage('202', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }

    //--------------------- Change User's Password Starts ---------------------//
    ############################################################################
    //-------------------------------------------------------------------------//
    //-------------------------- Change Password ------------------------------//
    //-------------------------------------------------------------------------//
    //-------------------------------------------------------------------------//
    //----------------------- Educational Courses List ------------------------//
    //-------------------------------------------------------------------------//
    ############################################################################
    /*
     *  @Author : Wildnet
     *  @Params : null
     *  @Short Description : Send Educational Courses List
     *  @return : array with code. 
     */
    //------------------- Send Educational Courses List Starts ----------------//
    public function getCourses($params) {
        $result = $this->coreModel->queryResultArray("SELECT DISTINCT(name), id FROM fj_courses ORDER BY name ASC");
        if (count($result) > 0) {
            foreach ($result as $row) {
                $courseData['id'] = (int) $row['id'];
                $courseData['name'] = (string) $row['name'];
                coreapi_model::$data[] = $courseData;
            }
            $this->coreModel->codeMessage('200', $this->lang->line('success'));
            coreapi_model::$returnArray['data'] = coreapi_model::$data;
        } else {
            $this->coreModel->codeMessage('202', $this->lang->line('no_record_found'));
        }
        return coreapi_model::$returnArray;
    }

    //------------------- Send Educational Courses List Ends ------------------//
    ############################################################################
    //-------------------------------------------------------------------------//
    //----------------------- Educational Courses List ------------------------//
    //-------------------------------------------------------------------------//
    //-------------------------------------------------------------------------//
    //--------------------------- University List -----------------------------//
    //-------------------------------------------------------------------------//
    ############################################################################    
    /*
     *  @Author : Wildnet
     *  @Params : null
     *  @Short Description : Send University List
     *  @return : array with code. 
     */
    //----------------------- Search University List Starts -------------------//
    public function searchUniversity($params) {
        $universityName = $this->coreModel->cleanString($params['universityName']);
        $result = $this->coreModel->queryResultArray("SELECT name, id FROM fj_university WHERE name LIKE '" . $universityName . "%' ");
        if (count($result) > 0) {
            foreach ($result as $row) {
                $universityData['id'] = (int) $row['id'];
                $universityData['name'] = (string) $row['name'];
                coreapi_model::$data[] = $universityData;
            }
            $this->coreModel->codeMessage('200', $this->lang->line('success'));
            coreapi_model::$returnArray['data'] = coreapi_model::$data;
        } else {
            $this->coreModel->codeMessage('202', $this->lang->line('no_record_found'));
        }
        return coreapi_model::$returnArray;
    }

    //---------------------- Search University List Ends ----------------------//
    ############################################################################
    //-------------------------------------------------------------------------//
    //--------------------------- University List -----------------------------//
    //-------------------------------------------------------------------------//
    //-------------------------------------------------------------------------//
    //----------------------- Delete Qualification ----------------------------//
    //-------------------------------------------------------------------------//
    ############################################################################
    /*
     *  @Author : Wildnet
     *  @Params : null
     *  @Short Description : Remove User's Selected Qualification
     *  @return : array with code. 
     */
    //--------------- Remove User's Selected Qualification Starts -------------//
    public function removeQualification($params) {
        $userId = $this->coreModel->cleanString($params['userId']);
        $id = $this->coreModel->getJwtValue($userId);
        $qualificationID = $this->coreModel->cleanString($params['id']);
        if ($qualificationID != '' && $userId != '') {
            $row = $this->coreModel->queryRowArray("SELECT id FROM fj_userQualification WHERE id='$qualificationID' AND userID='$id' AND status='1'");
            if (count($row) == '1') {
                $dataEdu = array('status' => '0');
                $queryEdu = $this->db->where('id', $qualificationID)->where('userId', $id)->update('fj_userQualification', $dataEdu);
                //--------------- Response Data ---------------//
                $this->coreModel->codeMessage('200', $this->lang->line('success'));
            } else {
                $this->coreModel->codeMessage('202', $this->lang->line('no_record_found'));
            }
        } else {
            $this->coreModel->codeMessage('202', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }

    //--------------- Remove User's Selected Qualification Ends ---------------//
    ############################################################################
    //-------------------------------------------------------------------------//
    //----------------------- Delete Qualification ----------------------------//
    //-------------------------------------------------------------------------//
    //-------------------------------------------------------------------------//
    //------------------------- Edit Profile Starts ---------------------------//
    //-------------------------------------------------------------------------// 
    ############################################################################
    /*
     *  @Author : Wildnet
     *  @Params : array(basic information).
     *  @Short Description : Edit User Basic Information
     *  @return : array with code. 
     */
    //--------------- Edit User Profile Basic Information Starts --------------//
    public function editBasicinfo($params) {
        $userId = $this->coreModel->cleanString($params['userId']);
        $id = $this->coreModel->getJwtValue($userId);
        $basicInformation = ($params['basicInformation']);
        if (
                $userId != '' &&
                $basicInformation[0]['fullname'] != '' &&
                $basicInformation[0]['email'] != '' &&
                $basicInformation[0]['mobile'] != '' &&
                $basicInformation[0]['gender'] != '' &&
                $basicInformation[0]['dob'] != '' &&
                $basicInformation[0]['pincode'] != ''
        ) {
            $row = $this->coreModel->queryRowArray("SELECT id,fullname,email,mobile,gender,dob,pinCode FROM fj_users WHERE id='$id' AND status='1' LIMIT 0,1");
            if (count($row) > 0) {
                //--------------- Basic Information Data ---------------//
                $fullname = ($this->coreModel->cleanString($basicInformation[0]['fullname']) != '' ? $this->coreModel->cleanString($basicInformation[0]['fullname']) : $this->coreModel->cleanString($row['fullname']));
                $email = ($this->coreModel->cleanString($basicInformation[0]['email']) != '' ? $this->coreModel->cleanString($basicInformation[0]['email']) : $this->coreModel->cleanString($row['email']));
                $mobile = ($this->coreModel->cleanString($basicInformation[0]['mobile']) != '' ? $this->coreModel->cleanString($basicInformation[0]['mobile']) : $this->coreModel->cleanString($row['mobile']));
                $gender = ($this->coreModel->cleanString($basicInformation[0]['gender']) != '' ? ($this->coreModel->cleanString($basicInformation[0]['gender']) == 'Male' ? 1 : 2) : $this->coreModel->cleanString($row['gender']));
                $dob = ($this->coreModel->cleanString($basicInformation[0]['dob']) != '' ? date('Y-m-d', strtotime($this->coreModel->cleanString($basicInformation[0]['dob']) . '-01-01')) : $this->coreModel->cleanString($row['dob']));
                $pincode = ($this->coreModel->cleanString($basicInformation[0]['pincode']) != '' ? $this->coreModel->cleanString($basicInformation[0]['pincode']) : $this->coreModel->cleanString($row['pinCode']));
                // Data Array
                $updateData = array(
                    'fullname' => $fullname,
                    'email' => $email,
                    'mobile' => $mobile,
                    'dob' => $dob,
                    'pincode' => $pincode,
                    'gender' => $gender
                );

                if ($this->coreModel->cleanString($row['email']) != $this->coreModel->cleanString($basicInformation[0]['email']) || $this->coreModel->cleanString($row['mobile']) != $this->coreModel->cleanString($basicInformation[0]['mobile'])) {
                    // If New Email Id Entered
                    if ($this->coreModel->cleanString($row['email']) != $this->coreModel->cleanString($basicInformation[0]['email'])) {
                        $email_exist_check = $this->EmailExistCheck($basicInformation[0]['email']);
                        if ($email_exist_check == FALSE) {
                            $this->coreModel->codeMessage('202', $this->lang->line('email_exists'));
                        }
                    } else {
                        $email_exist_check = TRUE;
                    }
                    // If New Email Id Entered
                    // 
                    // If New Mobile Number Entered
                    if ($this->coreModel->cleanString($row['mobile']) != $this->coreModel->cleanString($basicInformation[0]['mobile'])) {
                        $mobile_exist_check = $this->MobileExistCheck($basicInformation[0]['mobile']);
                        if ($mobile_exist_check == FALSE) {
                            $this->coreModel->codeMessage('202', $this->lang->line('mobile_exists'));
                        }
                    } else {
                        $mobile_exist_check = TRUE;
                    }
                    // If New Mobile Number Entered

                    if ($mobile_exist_check == TRUE && $email_exist_check == TRUE) {
                        $query = $this->db->where('id', $id)->update('fj_users', $updateData);
                        if ($query) {
                            $this->coreModel->codeMessage('200', $this->lang->line('success'));
                        } else {
                            $this->coreModel->codeMessage('500', $this->lang->line('something_wrong'));
                        }
                    }
                } else {
                    $query = $this->db->where('id', $id)->update('fj_users', $updateData);
                    if ($query) {
                        //--------------- Response Data ---------------//
                        $this->coreModel->codeMessage('200', $this->lang->line('success'));
                    } else {
                        $this->coreModel->codeMessage('500', $this->lang->line('something_wrong'));
                    }
                }
            } else {
                $this->coreModel->codeMessage('202', $this->lang->line('invalid_user'));
            }
        } else {
            $this->coreModel->codeMessage('500', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }

    //---------------- Edit User Profile Basic Information Ends ---------------//
    ############################################################################
    #
    #
    #
    ############################################################################
    /*
     *  @Author : Wildnet
     *  @Params : array(educational information).
     *  @Short Description : Edit User Educational Information
     *  @return : array with code. 
     */
    //------------ Edit User Profile Educational Information Starts -----------//
    public function editEducationalInfo($params) {
        $userId = $this->coreModel->cleanString($params['userId']);
        $id = $this->coreModel->getJwtValue($userId);
        
        $courseId = $this->coreModel->cleanString($params['courseId']);
        $universityId = $this->coreModel->cleanString($params['universityId']);
        $institute = $this->coreModel->cleanString($params['institute']);
        $completionYear = $this->coreModel->cleanString($params['completionYear']);
        $percent = $this->coreModel->cleanString($params['percent']);
        $educationId = $this->coreModel->cleanString($params['id']);

        if ($userId != '') {
            $row = $this->coreModel->queryRowArray("SELECT id FROM fj_users WHERE id='$id' AND status='1' LIMIT 0,1");
            if (count($row) > 0) {
                //--------------- Educational Information Data ---------------//

                if ( $courseId != '' && $universityId != '' && $completionYear != '' && $percent != '') {
                    if ($educationId != '') {
                        $userUniversityID = ($universityId != 'Other' ? $universityId : '287');
                        $dataEdu = array(
                            'courseId' => $courseId,
                            'universityId' => $userUniversityID,
                            'institute' => $institute,
                            'completionYear' => $completionYear,
                            'percent' => $percent,
                            'status' => '1'
                        );
                        $queryEdu = $this->db->where('userId', $id)->where('id', $educationId)->update('fj_userQualification', $dataEdu);
                        unset($dataEdu);
                    } else {
                        $userUniversityID = ($universityId != 'Other' ? $universityId : '287');
                        $dataEdu = array(
                            'userId' => $id,
                            'courseId' => $courseId,
                            'universityId' => $userUniversityID,
                            'institute' => $institute,
                            'completionYear' => $completionYear,
                            'percent' => $percent,
                            'status' => '1'
                        );
                        $queryEdu = $this->db->insert('fj_userQualification', $dataEdu);
                        unset($dataEdu);
                    }
                    $this->coreModel->codeMessage('200', $this->lang->line('success'));
                } else {
                    $this->coreModel->codeMessage('300', 'fdgfdgfdg');
                }
                

            } else {
                $this->coreModel->codeMessage('202', $this->lang->line('invalid_user'));
            }
        } else {
            $this->coreModel->codeMessage('300', 'dsfsdfsdf');
        }
        return coreapi_model::$returnArray;
    }

    //------------ Edit User Profile Educational Information Ends -------------//
    ############################################################################
    #
    #
    #
    ############################################################################
    /*
     *  @Author : Wildnet
     *  @Params : array(other information).
     *  @Short Description : Edit User Profile Other Information
     *  @return : array with code. 
     */
    //-------------- Edit User Profile Other Information Starts ---------------//
    public function editOtherInfo($params) {
        //print_r($params);exit;
        $userId = $this->coreModel->cleanString($params['userId']);
        $id = $this->coreModel->getJwtValue($userId);
        if (
                $userId != '' &&
                $params['preferredLocation'] != '' &&
                $params['workExperience'] != ''
        ) {
            $row = $this->coreModel->queryRowArray("SELECT id,expectedCtcFrom,expectedCtcTo,workExperience,adharCard,languages,image FROM fj_users WHERE id='$id' AND status='1' LIMIT 0,1");
            if (count($row) > 0) {
                $ctcOne = ($params['expectedCtcFrom'] == '' ? '0' : $params['expectedCtcFrom']);
                $ctcTwo = ($params['expectedCtcTo'] == '' ? '0' : $params['expectedCtcTo']);
                if ($ctcOne > $ctcTwo) {
                    $ctcFrom = $ctcTwo;
                    $ctcTo = $ctcOne;
                } else if ($ctcOne < $ctcTwo) {
                    $ctcFrom = $ctcOne;
                    $ctcTo = $ctcTwo;
                } else {
                    $ctcFrom = $ctcOne;
                    $ctcTo = $ctcTwo;
                }

                //--------------- Other Information Data ---------------//

                $expectedCtcFrom = ($ctcFrom != '' ? $ctcFrom : $this->coreModel->cleanString($row['expectedCtcFrom']));
                $expectedCtcTo = ($ctcTo != '' ? $ctcTo : $this->coreModel->cleanString($row['expectedCtcTo']));
                $workExperience = ($this->coreModel->cleanString($params['workExperience']) != '' ? $this->coreModel->cleanString($params['workExperience']) : $this->coreModel->cleanString($row['workExperience']));
                $adharCard = ($this->coreModel->cleanString($params['adharCard']) != '' ? $this->coreModel->cleanString($params['adharCard']) : $this->coreModel->cleanString($row['adharCard']));
                $languages = ($this->coreModel->cleanString($params['languages']) != '' ? $this->coreModel->cleanString($params['languages']) : $this->coreModel->cleanString($row['languages']));

                // Data Array 
                $updateData = array(
                    'expectedCtcFrom' => $expectedCtcFrom,
                    'expectedCtcTo' => $expectedCtcTo,
                    'workExperience' => $workExperience,
                    'adharCard' => $adharCard,
                    'languages' => $languages
                );
                $query = $this->db->where('id', $id)->update('fj_users', $updateData);
                if ($query) {
                    //--------------- Response Data ---------------//
                    $this->coreModel->codeMessage('200', $this->lang->line('success'));
                } else {
                    $this->coreModel->codeMessage('500', $this->lang->line('something_wrong'));
                }


                $allLocationsss = $params['preferredLocation'];
                if(!is_array($allLocationsss)) {
                    $preferredLocation = array();
                    if($allLocationsss) {
                        $preferredLocation[] = $allLocationsss;
                    }
                } else {
                    $preferredLocation = $allLocationsss;
                }


                $locationList = array();
                $getAllPreferredLocation = $this->coreModel->queryResultArray("SELECT locationName FROM fj_userPreferLocations WHERE userId='$id'");
                if (count($getAllPreferredLocation) > 0) {
                    foreach ($getAllPreferredLocation as $locationVal) {
                        $locationList[] = $locationVal['locationName'];
                    }
                }

                $newLocations = $preferredLocation;
                $oldLocations = $locationList;
                $allLocations = array_values(array_unique(array_merge($oldLocations, $newLocations)));

                if(count($allLocations) > 0) {
                    foreach ($allLocations as $val) {
                        if (in_array($val, $newLocations)) {
                            $chkRecord = $this->coreModel->queryRowArray("SELECT id FROM fj_userPreferLocations WHERE userId='$id' AND locationName='$val'");
                            if (count($chkRecord) > 0) {
                                if ($chkRecord['status'] != '1') {
                                    $updateData = array('status' => '1', 'createdAt' => date('Y-m-d H:i:s'));
                                    $updateWhere = array('userId' => $id, 'locationName' => $val);
                                    $this->db->where($updateWhere)->update('fj_userPreferLocations', $updateData);
                                }
                            } else {
                                $insertData = array('userId' => $id, 'locationName' => $val, 'createdAt' => date('Y-m-d H:i:s'), 'status' => '1');
                                $this->db->insert('fj_userPreferLocations', $insertData);
                            }
                        } else {
                            $updateData = array('status' => '2', 'createdAt' => date('Y-m-d H:i:s'));
                            $updateWhere = array('userId' => $id, 'locationName' => $val);
                            $this->db->where($updateWhere)->update('fj_userPreferLocations', $updateData);
                        }
                    }
                }
                $this->coreModel->codeMessage('200', $this->lang->line('success'));
            } else {
                $this->coreModel->codeMessage('202', $this->lang->line('invalid_user'));
            }
        } else {
            $this->coreModel->codeMessage('300', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }

    //-------------- Edit User Profile Other Information Ends -----------------//
    ############################################################################
    #
    #
    #
    ############################################################################
    /*
     *  @Author : Wildnet
     *  @Params : array(basic information, educational information, other information, social information).
     *  @Short Description : Edit User Profile
     *  @return : array with code. 
     */
    //-------------- Edit User Profile Social Information Starts --------------//
    public function editSocialInfo($params) {
        $userId = $this->coreModel->cleanString($params['userId']);
        $id = $this->coreModel->getJwtValue($userId);

        if (
                $userId != ''
        ) {
            $row = $this->coreModel->queryRowArray("SELECT id,facebookLink,linkedInLink,twitterLink,googlePlusLink FROM fj_users WHERE id='$id' AND status='1' LIMIT 0,1");
            if (count($row) > 0) {
                //--------------- Social Information Data ---------------//
                /*$facebookLink = ($this->coreModel->cleanString($params['facebookLink']) != '' ? $this->coreModel->cleanString($params['facebookLink']) : $this->coreModel->cleanString($row['facebookLink']));
                $linkedInLink = ($this->coreModel->cleanString($params['linkedInLink']) != '' ? $this->coreModel->cleanString($params['linkedInLink']) : $this->coreModel->cleanString($row['linkedInLink']));
                $twitterLink = ($this->coreModel->cleanString($params['twitterLink']) != '' ? $this->coreModel->cleanString($params['twitterLink']) : $this->coreModel->cleanString($row['twitterLink']));
                $googlePlusLink = ($this->coreModel->cleanString($params['googlePlusLink']) != '' ? $this->coreModel->cleanString($params['googlePlusLink']) : $this->coreModel->cleanString($row['googlePlusLink']));*/

                $facebookLink = $params['facebookLink'];
                $linkedInLink = $params['linkedInLink'];
                $twitterLink = $params['twitterLink'];
                $googlePlusLink = $params['googlePlusLink'];

                // Data Array
                $updateData = array(
                    'facebookLink' => $facebookLink,
                    'linkedInLink' => $linkedInLink,
                    'twitterLink' => $twitterLink,
                    'googlePlusLink' => $googlePlusLink
                );
                $query = $this->db->where('id', $id)->update('fj_users', $updateData);
                if ($query) {
                    //--------------- Response Data ---------------//
                    $this->coreModel->codeMessage('200', $this->lang->line('success'));
                } else {
                    $this->coreModel->codeMessage('500', $this->lang->line('something_wrong'));
                }
            } else {
                $this->coreModel->codeMessage('202', $this->lang->line('invalid_user'));
            }
        } else {
            $this->coreModel->codeMessage('300', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }

    //-------------- Edit User Profile Social Information Ends ----------------//    
    ############################################################################
    #
    #
    #
    ############################################################################
    //-------------------------------------------------------------------------//
    //-------------------------- Edit Profile Ends ----------------------------//
    //-------------------------------------------------------------------------//
    //-------------------------------------------------------------------------//
    //------------------------- Send Profile Data -----------------------------//
    //-------------------------------------------------------------------------//
    ############################################################################
    /*
     *  @Author : Wildnet
     *  @Params : null
     *  @Short Description : get User's All Information
     *  @return : array with code. 
     */
    //--------------------- User's All Information Starts ---------------------//
    public function getUserInformation($params) {
        $userId = $this->coreModel->cleanString($params['userId']);
        $id = $this->coreModel->getJwtValue($userId);
        if ($userId != '') {
            $row = $this->coreModel->queryRowArray("SELECT 
                                                            id,fullname,email,mobile,dob,pinCode,gender,
                                                            preferredLocation,expectedCtcFrom, expectedCtcTo, workExperience,adharCard,languages,image,
                                                            facebookLink,linkedInLink,twitterLink,googlePlusLink
                                                      FROM fj_users WHERE id='$id' AND status='1'");
            if (count($row) > 0) {
                // Basic Information
                $basicInfo = array();
                $rowDataBasicInfo['fullname'] = $row['fullname'];
                $rowDataBasicInfo['email'] = $row['email'];
                $rowDataBasicInfo['mobile'] = $row['mobile'];
                $rowDataBasicInfo['dob'] = $row['dob'];
                $rowDataBasicInfo['pincode'] = $row['pinCode'];
                $rowDataBasicInfo['gender'] = $row['gender'];
                $basicInfo[] = $rowDataBasicInfo;
                // Other Information
                $locationList = array();
                $getAllPreferredLocation = $this->coreModel->queryResultArray("SELECT locationName FROM fj_userPreferLocations WHERE userId='$id' AND status='1'");
                if (count($getAllPreferredLocation) > 0) {
                    foreach ($getAllPreferredLocation as $locationVal) {
                        $tempArray['name'] = trim($locationVal['locationName']);
                        $locationList[] = $tempArray;
                    }
                }
                $otherInfo = array();
                $rowDataOtherInfo['preferredLocation'] = $locationList;
                $rowDataOtherInfo['expectedCtcFrom'] = $row['expectedCtcFrom'];
                $rowDataOtherInfo['expectedCtcTo'] = $row['expectedCtcTo'];
                $rowDataOtherInfo['workExperience'] = $row['workExperience'];
                $rowDataOtherInfo['adharCard'] = $row['adharCard'];
                $rowDataOtherInfo['languages'] = $row['languages'];
                $rowDataOtherInfo['image'] = ($row['image'] != '' ? base_url() . '/uploads/userImages/' . $row['image'] : '');
                $otherInfo[] = $rowDataOtherInfo;
                // Social Information
                $socialInfo = array();
                $rowDataSocialInfo['facebookLink'] = $row['facebookLink'];
                $rowDataSocialInfo['linkedInLink'] = $row['linkedInLink'];
                $rowDataSocialInfo['twitterLink'] = $row['twitterLink'];
                $rowDataSocialInfo['googlePlusLink'] = $row['googlePlusLink'];
                $socialInfo[] = $rowDataSocialInfo;
                // Educational Information
                $educationalInfo = array();
                $qualificationResults = $this->coreModel->queryResultArray("SELECT Q . * , C.id as courseId, C.name as courseName, U.name as adminUniversityName FROM fj_userQualification Q JOIN fj_university U, fj_courses C WHERE Q.universityId = U.id AND Q.courseId=C.id AND Q.userId='$id' AND Q.status='1'");
                foreach ($qualificationResults as $row) {
                    $rowDataEducationalInfo['id'] = $row['id'];
                    $rowDataEducationalInfo['courseId'] = $row['courseId'];
                    $rowDataEducationalInfo['courseName'] = $row['courseName'];
                    $rowDataEducationalInfo['universityId'] = $row['universityId'];
                    $rowDataEducationalInfo['universityId'] = $row['universityId'];
                    $rowDataEducationalInfo['universityName'] = ($row['universityId'] == '287' ? $row['universityName'] : $row['adminUniversityName']);
                    $rowDataEducationalInfo['institute'] = $row['institute'];
                    $rowDataEducationalInfo['completionYear'] = $row['completionYear'];
                    $rowDataEducationalInfo['percent'] = $row['percent'];
                    $educationalInfo[] = $rowDataEducationalInfo;
                }
                //--------------- Response Data ---------------//
                $this->coreModel->codeMessage('200', $this->lang->line('success'));
                coreapi_model::$returnArray['basicInfo'] = $basicInfo;
                coreapi_model::$returnArray['otherInfo'] = $otherInfo;
                coreapi_model::$returnArray['socialInfo'] = $socialInfo;
                coreapi_model::$returnArray['educationalInfo'] = $educationalInfo;
            } else {
                $this->coreModel->codeMessage('500', $this->lang->line('no_record_found'));
            }
        } else {
            $this->coreModel->codeMessage('300', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }

    //--------------------- User's All Information Ends -----------------------//
    ############################################################################
    #
    #
    #
    ############################################################################
    /*
     *  @Author : Wildnet
     *  @Params : null
     *  @Short Description : get User's Basic Information
     *  @return : array with code. 
     */
    //--------------------- User's Basic Information Starts -------------------//
    public function getBasicInformation($params) {
        $userId = $this->coreModel->cleanString($params['userId']);
        $id = $this->coreModel->getJwtValue($userId);
        if ($userId != '') {
            $row = $this->coreModel->queryRowArray("SELECT id,fullname,email,mobile,dob,pinCode,gender,resume_path FROM fj_users WHERE id='$id' AND status='1'");
            if (count($row) > 0) {
                // Basic Information
                $basicInfo = array();
                $rowDataBasicInfo['fullname'] = (string) $row['fullname'];
                $rowDataBasicInfo['email'] = (string) $row['email'];
                $rowDataBasicInfo['mobile'] = (string) $row['mobile'];
                $rowDataBasicInfo['dob'] = (string) (($row['dob'] == '' || $row['dob'] == '0000-00-00') ? '' : date('Y', strtotime($row['dob'])));
                $rowDataBasicInfo['pincode'] = (string) ($row['pinCode'] != 0 ? $row['pinCode'] : '');
                $rowDataBasicInfo['gender'] = (string) $row['gender'];

                if(is_null($row['resume_path']) || $row['resume_path'] == '')
                    $rowDataBasicInfo['resumePath'] = "";
                else
                    $rowDataBasicInfo['resumePath'] = "https://s3.amazonaws.com/fjauditions/".$row['resume_path'];

                $rowDataBasicInfo['profilePercent'] = Userapi_model::profilePercent($id);
                $basicInfo[] = $rowDataBasicInfo;
                //--------------- Response Data ---------------//
                $this->coreModel->codeMessage('200', $this->lang->line('success'));
                coreapi_model::$returnArray['basicInfo'] = $basicInfo;
            } else {
                $this->coreModel->codeMessage('500', $this->lang->line('no_record_found'));
                coreapi_model::$returnArray['basicInfo'] = array();
            }
        } else {
            $this->coreModel->codeMessage('300', $this->lang->line('missing_required_fields'));
            coreapi_model::$returnArray['basicInfo'] = array();
        }
        return coreapi_model::$returnArray;
    }

    //--------------------- User's Basic Information Ends ---------------------//
    ############################################################################
    #
    #
    #
    ############################################################################
    /*
     *  @Author : Wildnet
     *  @Params : null
     *  @Short Description : get User's Educational Information
     *  @return : array with code. 
     */
    //------------------ User's Educational Information Starts ----------------//
    public function getEducationalInformation($params) {
        $userId = $this->coreModel->cleanString($params['userId']);
        $id = $this->coreModel->getJwtValue($userId);
        if ($userId != '') {
            $row = $this->coreModel->queryRowArray("SELECT id FROM fj_users WHERE id='$id' AND status='1'");
            if (count($row) > 0) {
                // Educational Information
                $educationalInfo = array();
                $qualificationResults = $this->coreModel->queryResultArray("SELECT Q . * , C.id as courseId, C.name as courseName, U.name as adminUniversityName FROM fj_userQualification Q JOIN fj_university U, fj_courses C WHERE Q.universityId = U.id AND Q.courseId=C.id AND Q.userId='$id' AND Q.status='1'");
                foreach ($qualificationResults as $row) {
                    $rowDataEducationalInfo['id'] = (string) $row['id'];
                    $rowDataEducationalInfo['courseId'] = (string) $row['courseId'];
                    $rowDataEducationalInfo['courseName'] = (string) $row['courseName'];
                    $rowDataEducationalInfo['universityId'] = (string) $row['universityId'];
                    $rowDataEducationalInfo['universityId'] = (string) $row['universityId'];
                    $rowDataEducationalInfo['universityName'] = (string) ($row['universityId'] == '287' ? $row['universityName'] : $row['adminUniversityName']);
                    $rowDataEducationalInfo['institute'] = (string) $row['institute'];
                    $rowDataEducationalInfo['completionYear'] = (string) $row['completionYear'];
                    $rowDataEducationalInfo['percent'] = $row['percent'];
                    $educationalInfo[] = $rowDataEducationalInfo;
                }
                //--------------- Response Data ---------------//
                $this->coreModel->codeMessage('200', $this->lang->line('success'));
                coreapi_model::$returnArray['educationalInfo'] = $educationalInfo;
                coreapi_model::$returnArray['profilePercent'] = Userapi_model::profilePercent($id);
            } else {
                $this->coreModel->codeMessage('500', $this->lang->line('no_record_found'));
                coreapi_model::$returnArray['educationalInfo'] = array();
                coreapi_model::$returnArray['profilePercent'] = 0;
            }
        } else {
            $this->coreModel->codeMessage('300', $this->lang->line('missing_required_fields'));
            coreapi_model::$returnArray['educationalInfo'] = array();
            coreapi_model::$returnArray['profilePercent'] = 0;
        }
        return coreapi_model::$returnArray;
    }

    //------------------ User's Educational Information Ends ------------------//
    ############################################################################
    #
    #
    #
    ############################################################################
    /*
     *  @Author : Wildnet
     *  @Params : null
     *  @Short Description : get User's Other Information
     *  @return : array with code. 
     */
    //-------------------- User's Other Information Starts --------------------//
    public function getOtherInformation($params) {
        $userId = $this->coreModel->cleanString($params['userId']);
        $id = $this->coreModel->getJwtValue($userId);
        if ($userId != '') {
            $row = $this->coreModel->queryRowArray("SELECT 
                                                            id,preferredLocation,expectedCtcFrom, expectedCtcTo, workExperience,adharCard,languages,image
                                                      FROM fj_users WHERE id='$id' AND status='1'");
            if (count($row) > 0) {
                // Other Information
                $locationList = array();
                $getAllPreferredLocation = $this->coreModel->queryResultArray("SELECT locationName FROM fj_userPreferLocations WHERE userId='$id' AND status='1'");
                if (count($getAllPreferredLocation) > 0) {
                    foreach ($getAllPreferredLocation as $locationVal) {
                        $tempArray['name'] = (string) trim($locationVal['locationName']);
                        $locationList[] = $tempArray;
                    }
                }
                $otherInfo = array();
                $rowDataOtherInfo['preferredLocation'] = $locationList;
                $rowDataOtherInfo['expectedCtcFrom'] = (string) $row['expectedCtcFrom'];
                $rowDataOtherInfo['expectedCtcTo'] = (string) $row['expectedCtcTo'];
                $rowDataOtherInfo['workExperience'] = (string) $row['workExperience'];
                $rowDataOtherInfo['adharCard'] = (string) $row['adharCard'];
                $rowDataOtherInfo['languages'] = (string) $row['languages'];
                $rowDataOtherInfo['image'] = ($row['image'] != '' ? base_url() . '/uploads/userImages/' . $row['image'] : '');
                $rowDataOtherInfo['profilePercent'] = Userapi_model::profilePercent($id);
                $otherInfo[] = $rowDataOtherInfo;
                //--------------- Response Data ---------------//
                $this->coreModel->codeMessage('200', $this->lang->line('success'));
                coreapi_model::$returnArray['otherInfo'] = $otherInfo;
            } else {
                $this->coreModel->codeMessage('500', $this->lang->line('no_record_found'));
                coreapi_model::$returnArray['otherInfo'] = array();
            }
        } else {
            $this->coreModel->codeMessage('300', $this->lang->line('missing_required_fields'));
            coreapi_model::$returnArray['otherInfo'] = array();
        }
        return coreapi_model::$returnArray;
    }

    /*
     *  @Author : Synergy
     *  @Params : userId
     *  @Short Description : v2 of get User's Other Information
     *  @return : array with code. 
     */

    public function getOtherInformation_v2($params) {
        $userId = $this->coreModel->cleanString($params['userId']);
        $id = $this->coreModel->getJwtValue($userId);
        if ($userId != '') {
            $row = $this->coreModel->queryRowArray("SELECT id,preferredLocation,expectedCtcFrom, expectedCtcTo, workExperience,adharCard,languages,image, isAaadharActivated FROM fj_users WHERE id='$id' AND status='1'");
            if (count($row) > 0) {
                // Other Information
                $locationList = array();
                $getAllPreferredLocation = $this->coreModel->queryResultArray("SELECT locationName FROM fj_userPreferLocations WHERE userId='$id' AND status='1'");
                if (count($getAllPreferredLocation) > 0) {
                    foreach ($getAllPreferredLocation as $locationVal) {
                        $tempArray['name'] = (string) trim($locationVal['locationName']);
                        $locationList[] = $tempArray;
                    }
                }
                $otherInfo = array();
                $rowDataOtherInfo['preferredLocation'] = $locationList;
                $rowDataOtherInfo['expectedCtcFrom'] = (string) $row['expectedCtcFrom'];
                $rowDataOtherInfo['expectedCtcTo'] = (string) $row['expectedCtcTo'];
                $rowDataOtherInfo['workExperience'] = (string) $row['workExperience'];
                $rowDataOtherInfo['adharCard'] = (string) $row['adharCard'];
                $rowDataOtherInfo['languages'] = (string) $row['languages'];
                $rowDataOtherInfo['isAaadharActivated'] = (string) $row['isAaadharActivated'];
                $rowDataOtherInfo['image'] = ($row['image'] != '' ? base_url() . '/uploads/userImages/' . $row['image'] : '');
                $rowDataOtherInfo['profilePercent'] = Userapi_model::profilePercent($id);
                $otherInfo[] = $rowDataOtherInfo;
                //--------------- Response Data ---------------//
                $this->coreModel->codeMessage('200', $this->lang->line('success'));
                coreapi_model::$returnArray['otherInfo'] = $otherInfo;
            } else {
                $this->coreModel->codeMessage('500', $this->lang->line('no_record_found'));
            }
        } else {
            $this->coreModel->codeMessage('300', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }
	
    //-------------------- User's Other Information Ends ----------------------//
    ############################################################################
    #
    #
    #
    ############################################################################
    /*
     *  @Author : Wildnet
     *  @Params : null
     *  @Short Description : get User's All Information
     *  @return : array with code. 
     */
    //--------------------- User's Social Information Starts ------------------//
    public function getSocialInformation($params) {
        $userId = $this->coreModel->cleanString($params['userId']);
        $id = $this->coreModel->getJwtValue($userId);
        if ($userId != '') {
            $row = $this->coreModel->queryRowArray("SELECT 
                                                            id,facebookLink,linkedInLink,twitterLink,googlePlusLink
                                                      FROM fj_users WHERE id='$id' AND status='1'");
            if (count($row) > 0) {
                // Social Information
                $socialInfo = array();
                $rowDataSocialInfo['facebookLink'] = (string) $row['facebookLink'];
                $rowDataSocialInfo['linkedInLink'] = (string) $row['linkedInLink'];
                $rowDataSocialInfo['twitterLink'] = (string) $row['twitterLink'];
                $rowDataSocialInfo['googlePlusLink'] = (string) $row['googlePlusLink'];
                $rowDataSocialInfo['profilePercent'] = Userapi_model::profilePercent($id);
                $socialInfo[] = $rowDataSocialInfo;
                //--------------- Response Data ---------------//
                $this->coreModel->codeMessage('200', $this->lang->line('success'));
                coreapi_model::$returnArray['socialInfo'] = $socialInfo;
            } else {
                $this->coreModel->codeMessage('500', $this->lang->line('no_record_found'));
                coreapi_model::$returnArray['socialInfo'] = array();
            }
        } else {
            $this->coreModel->codeMessage('300', $this->lang->line('missing_required_fields'));
            coreapi_model::$returnArray['socialInfo'] = array();
        }
        return coreapi_model::$returnArray;
    }

    //--------------------- User's Social Information Ends --------------------//    
    ############################################################################
    #
    #
    #
    ############################################################################
    //-------------------------------------------------------------------------//
    //------------------------- Send Profile Data -----------------------------//
    //-------------------------------------------------------------------------//
    //-------------------------------------------------------------------------//
    //---------------------- Profile Complition Percent -----------------------//
    //-------------------------------------------------------------------------//    
    ############################################################################
    /*
     *  @Author : Wildnet
     *  @Params : user id
     *  @Short Description : get User's profile completion percent
     *  @return : array with code. 
     */
    //---------------------- User's Profile Percent Starts --------------------//
    protected function profilePercent($userId) {
        $userId = $this->coreModel->cleanString($userId);
        $percentFilled = 0;
        if ($userId != '') {
            $userData = $this->coreModel->queryRowArray("SELECT 
                                                                id, fullname, email, mobile, dob, pinCode, gender,
                                                                preferredLocation, expectedCtcFrom, expectedCtcTo, workExperience,adharCard,languages,image,
                                                                facebookLink, linkedInLink, twitterLink, googlePlusLink
                                                          FROM fj_users WHERE id='$userId' AND status='1'");
            $basicField = 6;
            $educationalField = 5;
            $otherField = 7;
            $socialField = 4;
            $employmentField = 5;
            $totalField = (int) $basicField + $educationalField + $otherField + $socialField;
            $filledData = 0;
            if (count($userData) > 0) {
                ($userData['fullname'] != '' ? $filledData++ : $filledData);
                ($userData['email'] != '' ? $filledData++ : $filledData);
                ($userData['mobile'] != '' ? $filledData++ : $filledData);
                (($userData['dob'] != '') && ($userData['dob'] != '0000-00-00') ? $filledData++ : $filledData);
                ($userData['pinCode'] != '' ? $filledData++ : $filledData);
                ($userData['gender'] != '' ? $filledData++ : $filledData);

                ($userData['facebookLink'] != '' ? $filledData++ : $filledData);
                ($userData['linkedInLink'] != '' ? $filledData++ : $filledData);
                ($userData['twitterLink'] != '' ? $filledData++ : $filledData);
                ($userData['googlePlusLink'] != '' ? $filledData++ : $filledData);

                ($userData['preferredLocation'] != '' ? $filledData++ : $filledData);
                ($userData['expectedCtcFrom'] != '' ? $filledData++ : $filledData);
                ($userData['expectedCtcTo'] != '' ? $filledData++ : $filledData);
                ($userData['workExperience'] != '' ? $filledData++ : $filledData);
                ($userData['adharCard'] != '' ? $filledData++ : $filledData);
                ($userData['languages'] != '' ? $filledData++ : $filledData);
                ($userData['image'] != '' ? $filledData++ : $filledData);

                $qualificationResults = $this->coreModel->queryResultArray("SELECT Q . * , C.id as courseId, C.name as courseName, U.name as adminUniversityName FROM fj_userQualification Q JOIN fj_university U, fj_courses C WHERE Q.universityId = U.id AND Q.courseId=C.id AND Q.userId='$userId' AND Q.status='1'");
                if (count($qualificationResults) > 0) {
                    $filledData = $filledData + 5;
                }

                $employmentResults = $this->coreModel->queryRowArray("
                                                                    SELECT 
                                                                        fe.* , 
                                                                        u.id    AS uId, 
                                                                        u.fullname  AS userName
                                                                    FROM 
                                                                        fj_user_employment fe 
                                                                    JOIN 
                                                                        fj_users u
                                                                    ON u.id = fe.userId 
                                                                    WHERE 
                                                                        fe.userId='$userId'      AND
                                                                        u.status='1'             AND 
                                                                        fe.status='1'
                                                                    ");

                if (count($employmentResults) > 0) {
                    $filledData = $filledData + 5;
                }
            }
            $percentFilled = round(($filledData / $totalField) * 100, 2);
            if($percentFilled > 100) {
              $percentFilled = 100;
            }
        }
        return $percentFilled;
    }
    
    /*
     *  @Author : Wildnet
     *  @Params : user id
     *  @Short Description : get User's profile completion percent
     *  @return : array with code. 
     */
    //---------------------- User's Profile Percent Starts --------------------//
    public function getProfilePercent($params) {
        $id = $this->coreModel->cleanString($params['userId']);
        $userId = $this->coreModel->getJwtValue($id);
        $percentFilled = 0;
        if ($userId != '') {
            $userData = $this->coreModel->queryRowArray("SELECT 
                                                                id, fullname, email, mobile, dob, pinCode, gender,
                                                                preferredLocation, expectedCtcFrom, expectedCtcTo, workExperience,adharCard,languages,image,
                                                                facebookLink, linkedInLink, twitterLink, googlePlusLink
                                                          FROM fj_users WHERE id='$userId' AND status='1'");
            $basicField = 6;
            $educationalField = 5;
            $otherField = 7;
            $socialField = 4;
            $employmentField = 5;
            $totalField = (int) $basicField + $educationalField + $otherField + $socialField;
            $filledData = 0;
            if (count($userData) > 0) {
                ($userData['fullname'] != '' ? $filledData++ : $filledData);
                ($userData['email'] != '' ? $filledData++ : $filledData);
                ($userData['mobile'] != '' ? $filledData++ : $filledData);
                (($userData['dob'] != '') && ($userData['dob'] != '0000-00-00') ? $filledData++ : $filledData);
                ($userData['pinCode'] != '' ? $filledData++ : $filledData);
                ($userData['gender'] != '' ? $filledData++ : $filledData);

                ($userData['facebookLink'] != '' ? $filledData++ : $filledData);
                ($userData['linkedInLink'] != '' ? $filledData++ : $filledData);
                ($userData['twitterLink'] != '' ? $filledData++ : $filledData);
                ($userData['googlePlusLink'] != '' ? $filledData++ : $filledData);

                ($userData['preferredLocation'] != '' ? $filledData++ : $filledData);
                ($userData['expectedCtcFrom'] != '' ? $filledData++ : $filledData);
                ($userData['expectedCtcTo'] != '' ? $filledData++ : $filledData);
                ($userData['workExperience'] != '' ? $filledData++ : $filledData);
                ($userData['adharCard'] != '' ? $filledData++ : $filledData);
                ($userData['languages'] != '' ? $filledData++ : $filledData);
                ($userData['image'] != '' ? $filledData++ : $filledData);

                $qualificationResults = $this->coreModel->queryResultArray("SELECT Q . * , C.id as courseId, C.name as courseName, U.name as adminUniversityName FROM fj_userQualification Q JOIN fj_university U, fj_courses C WHERE Q.universityId = U.id AND Q.courseId=C.id AND Q.userId='$userId' AND Q.status='1'");
                if (count($qualificationResults) > 0) {
                    $filledData = $filledData + 5;
                }

                $employmentResults = $this->coreModel->queryRowArray("
                                                                    SELECT 
                                                                        fe.* , 
                                                                        u.id    AS uId, 
                                                                        u.fullname  AS userName
                                                                    FROM 
                                                                        fj_user_employment fe 
                                                                    JOIN 
                                                                        fj_users u
                                                                    ON u.id = fe.userId 
                                                                    WHERE 
                                                                        fe.userId='$userId'      AND
                                                                        u.status='1'             AND 
                                                                        fe.status='1'
                                                                    ");

                if (count($employmentResults) > 0) {
                    $filledData = $filledData + 5;
                }
            }
            $percentFilled = round(($filledData / $totalField) * 100, 2);
            if($percentFilled > 100) {
              $percentFilled = 100;
            }

            $this->coreModel->codeMessage('200', $this->lang->line('success'));
            coreapi_model::$returnArray['profilePercent'] = $percentFilled;
        }
        return coreapi_model::$returnArray;
    }

    //---------------------- User's Profile Percent Ends ----------------------//
    ############################################################################
    //-------------------------------------------------------------------------//
    //---------------------- End of Profile Complition Percent -----------------------//
    //-------------------------------------------------------------------------//


    //-------------------------------------------------------------------------//
    //--------------------- Start of Gets User ID From User's Token --------------------//
    //-------------------------------------------------------------------------//
    ############################################################################
    function getTokenID($params) {
        $userId = $this->coreModel->cleanString($params['userId']);
        $id = $this->coreModel->getJwtToken($userId);
        $token = array();
        $token['id'] = $userId;
        $jwtKey = $this->config->item('jwtKey');
        $secretKey = base64_decode($jwtKey);
        $signatureAlgo = $this->config->item('signatureAlgorithm');
        return JWT::encode($token, $secretKey, $signatureAlgo);
    }

    ############################################################################
    //-------------------------------------------------------------------------//
    //---------------------End of Gets User ID From User's Token --------------------//
    //-------------------------------------------------------------------------//
    

    /*
     *  @Author : Wildnet
     *  @Params : array $params(locationName)
     *  @Short Description : get all matching locations which are in search location text
     *  @return : array with code. 
     */
    public function getSearchCriteriaLocation($params) {
        $locationName   = $this->coreModel->cleanString($params['locationName']);        
        if($locationName!='' ) {
            $locationRow    = $this->coreModel->queryResultArray("
                                                            SELECT 
                                                                * 
                                                            FROM 
                                                                fj_cityState
                                                            WHERE
                                                                city LIKE '$locationName%'  OR 
                                                                state LIKE '$locationName%' OR
                                                                country LIKE '$locationName%'
                                                            ORDER BY 
                                                                (CASE WHEN city LIKE '$locationName%' THEN 1 WHEN state LIKE '$locationName%' THEN 2 WHEN country LIKE '$locationName%' THEN 3 ELSE 4 END)
                                                        ");
            if(count($locationRow)>0){
                $locations = array();
                foreach($locationRow as $rowData){
                    $locationData['locationId'] = (string)$rowData['id'];
                    $locationData['name']       = (string)$rowData['city'].', '.$rowData['state'];
                    coreapi_model::$data[]      = $locationData;
                }
                $this->coreModel->codeMessage('200', $this->lang->line('success'));
                coreapi_model::$returnArray['data']    = coreapi_model::$data;
            }
            else {
                $this->coreModel->codeMessage('300', $this->lang->line('no_location_found'));
            }
        }
        else {
            $this->coreModel->codeMessage('300', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }
    
    /*
     *  @Author : Synergy
     *  @Params : array $params(locatonId, qualificationId, userId)
     *  @Short Description : Inserts into databse whatever locations or qualifications user searchs. So that we keep tracks
     *  @return : array with code. 
     */
    public function setUserSearchCriteria($params) {
        $locatonId          = $this->coreModel->cleanString($params['locatonId']);
        $qualificationId    = $this->coreModel->cleanString($params['qualificationId']);
        $userId             = $this->coreModel->cleanString($params['userId']);
        $id                 = $this->coreModel->getJwtValue($userId);
        if($qualificationId!='' && $locatonId!='' && $userId!='') {
            $userRow    = $this->coreModel->userExists($id);
            if(count($userRow)>0) {
                $notificationResult =   $this->coreModel->queryRowArray("
                                                            SELECT 
                                                                id
                                                            FROM 
                                                                fj_userNotification
                                                            WHERE
                                                                userId=$id
                                                        ");
                if (count($notificationResult) > 0) {
                    //$this->db->where('userId', $id)->update('fj_userNotification', array('notificationStatus'=>'1'));
                }
                else {
                    $this->db->insert('fj_userNotification', array('userId'=>$id, 'notificationStatus'=>'1'));   
                }                
                
                $this->db->where('userId', $id)->update('fj_userSearchLocationCriteria', array('status'=>'2'));
                $this->db->where('userId', $id)->update('fj_userSearchQualificationCriteria', array('status'=>'2'));
                $locations = explode(',', $locatonId);
                foreach($locations as $location) {
                    $insertData = array('locationId' => $location, 'userId' => $id, 'status' => '1');
                    $this->db->insert('fj_userSearchLocationCriteria', $insertData);
                }
                $qualifications = explode(',', $qualificationId);
                foreach($qualifications as $qualification) {
                    $insertData = array('qualificationId' => $qualification, 'userId' => $id, 'status' => '1');
                    $this->db->insert('fj_userSearchQualificationCriteria', $insertData);
                }                
                $this->coreModel->codeMessage('200', $this->lang->line('success'));
            }
            else {
                $this->coreModel->codeMessage('500', $this->lang->line('invalid_user'));
            }
        } else {
            $this->coreModel->codeMessage('300', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }
    
    /*
     *  @Author : Synergy
     *  @Params : array $params(userId)
     *  @Short Description : Get locations or qualifications from databse whatever locations or qualifications user searched.
     *  @return : array of data with response code. 
     */
    public function getUserSearchCriteria($params) {
        $userId             = $this->coreModel->cleanString($params['userId']);
        $id                 = $this->coreModel->getJwtValue($userId);
        if($userId!='') {
            $userRow    = $this->coreModel->userExists($id);
            if(count($userRow)>0) {
                $locationResult         =   $this->coreModel->queryResultArray("
                                                            SELECT 
                                                                USLC.locationId,
                                                                L.city,
                                                                L.state
                                                            FROM 
                                                                fj_userSearchLocationCriteria   USLC
                                                            LEFT JOIN
                                                                fj_cityState            L
                                                            ON
                                                                USLC.locationId=L.id
                                                            WHERE
                                                                USLC.status=1    AND
                                                                USLC.userId=$id
                                                        ");
                
                $qualificationResult    =   $this->coreModel->queryResultArray("
                                                            SELECT 
                                                                USQC.qualificationId AS courseId,
                                                                C.name AS courseName
                                                            FROM 
                                                                fj_userSearchQualificationCriteria   USQC
                                                            LEFT JOIN
                                                                fj_courses              C
                                                            ON
                                                                USQC.qualificationId=C.id
                                                            WHERE
                                                                USQC.status=1    AND
                                                                USQC.userId=$id
                                                        ");
                $locationData       = array();
                $qualificationData  = array();
                if (count($locationResult) > 0) {
                    foreach ($locationResult as $locationRow) {
                        $locationRowData['locationId']  = (string) $locationRow['locationId'];
                        $locationRowData['cityName']    = (string) $locationRow['city'];
                        $locationRowData['stateName']   = (string) $locationRow['state'];
                        $locationData[] = $locationRowData;
                    }
                } 
                if (count($qualificationResult) > 0) {
                    foreach ($qualificationResult as $qualificationRow) {
                        $qualificationRowData['courseId']    = (string) $qualificationRow['courseId'];
                        $qualificationRowData['courseName']  = (string) $qualificationRow['courseName'];
                        $qualificationData[]    = $qualificationRowData;
                    }
                }                
                $this->coreModel->codeMessage('200', $this->lang->line('success'));
                coreapi_model::$returnArray['location']         = $locationData;
                coreapi_model::$returnArray['qualification']    = $qualificationData;
            }
            else {
                $this->coreModel->codeMessage('500', $this->lang->line('invalid_user'));
            }
        } else {
            $this->coreModel->codeMessage('300', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }
    

    /*
     *  @Author : Synergy
     *  @Params : array $params(status, userId)
     *  @Short Description : Use to set notification ON or Off by a specific user. 
     *  @return : array of data with response code. 
     */
    public function setUserNotification($params) {
        $status = $this->coreModel->cleanString($params['status']);
        $userId = $this->coreModel->cleanString($params['userId']);
        $id     = $this->coreModel->getJwtValue($userId);
        if($status!='' && $userId!='') {
            $userRow    = $this->coreModel->userExists($id);
            if(count($userRow)>0) {
                $notiStatus = ($status=='ON'?'1':'2');
                $this->db->where('userId', $id)->update('fj_userNotification', array('notificationStatus'=>$notiStatus));
                $this->coreModel->codeMessage('200', $this->lang->line('success'));
            }
            else {
                $this->coreModel->codeMessage('500', $this->lang->line('invalid_user'));
            }
        } else {
            $this->coreModel->codeMessage('300', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }

    /*
     *  @Author : Synergy
     *  @Params : array $params(status, userId)
     *  @Short Description : Use to get notification status ON or Off done by a specific User. 
     *  @return : array of data with response code. 
     */
    public function getUserNotification($params) {
        $userId = $this->coreModel->cleanString($params['userId']);
        $id     = $this->coreModel->getJwtValue($userId);
        if($userId!='') {
            $userRow    = $this->coreModel->userExists($id);
            if(count($userRow)>0) {
                $notiResult    =   $this->coreModel->queryRowArray("
                                                            SELECT 
                                                                *
                                                            FROM 
                                                                fj_userNotification 
                                                            WHERE
                                                                userId=$id
                                                        ");
                if (count($notiResult) > 0) {
                    $notiStatus = ($notiResult['notificationStatus']=='1'?'ON':'OFF');
                    $this->coreModel->codeMessage('200', $this->lang->line('success'));
                    coreapi_model::$returnArray['status']   = $notiStatus;
                }
                else {
                    $this->coreModel->codeMessage('501', $this->lang->line('invalid_user'));
                }                
            }
            else {
                $this->coreModel->codeMessage('500', $this->lang->line('invalid_user'));
            }
        } else {
            $this->coreModel->codeMessage('300', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }
	
	/*
     *  @Author : Synergy
     *  @Params : array $params(status, userId)
     *  @Short Description : Use to get notification status ON or Off done by a specific User. 
     *  @return : array of data with response code. 
     */
    public function getUserNotifications($params) {
        $userId = $this->coreModel->cleanString($params['userId']);
        $id     = $this->coreModel->getJwtValue($userId);
        if($userId!='') {
            $userRow    = $this->coreModel->userExists($id);
            if(count($userRow)>0) {
                $notiResult    =   $this->coreModel->queryResultArray("
                                                            SELECT 
                                                                *
                                                            FROM 
                                                                fj_userNotifications 
                                                            WHERE
                                                                userId=$id ORDER BY createdAt DESC
                                                        ");
                if (count($notiResult) > 0) {
                    foreach ($notiResult as $row) {
                        $resultData['id'] = (int) $row['id'];
                        $resultData['userId'] = (int) $row['userId'];
                        $resultData['message'] = (string) $row['message'];
                        $resultData['sender'] = (string) $row['sender'];
                        $resultData['status'] = (int) $row['status'];
                        $resultData['createdAt'] = (string) $row['createdAt'];
                        coreapi_model::$data[] = $resultData;
                    }
                    $this->coreModel->codeMessage('200', $this->lang->line('success'));
                    coreapi_model::$returnArray['data'] = coreapi_model::$data;
                }
                else {
                    $this->coreModel->codeMessage('501', $this->lang->line('invalid_user'));
                }                
            }
            else {
                $this->coreModel->codeMessage('500', $this->lang->line('invalid_user'));
            }
        } else {
            $this->coreModel->codeMessage('300', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }

    /*
     *  @Author : Synergy
     *  @Params : array $params(status, userId)
     *  @Short Description : Use to get notification status ON or Off done by a specific User. 
     *  @return : array of data with response code. 
     */
    public function readAllNotifications($params) {
        $userId = $this->coreModel->cleanString($params['userId']);
        $id     = $this->coreModel->getJwtValue($userId);
        if($userId!='') {
            $userRow    = $this->coreModel->userExists($id);
            if(count($userRow)>0) {
                $notiResult    =   $this->coreModel->queryResultArray("
                                                            SELECT 
                                                                *
                                                            FROM 
                                                                fj_userNotifications 
                                                            WHERE
                                                                userId=$id ORDER BY createdAt DESC
                                                        ");
                if (count($notiResult) > 0) {
                    $updateNotificationStatus = array(
                        'status' => 1
                    );
                    $this->db->where(array('userId'=>$id))->update('fj_userNotifications', $updateNotificationStatus);
                    $this->coreModel->codeMessage('200', $this->lang->line('success'));
                }
                else {
                    $this->coreModel->codeMessage('501', $this->lang->line('invalid_user'));
                }                
            }
            else {
                $this->coreModel->codeMessage('500', $this->lang->line('invalid_user'));
            }
        } else {
            $this->coreModel->codeMessage('300', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }

    /**
     * Author : Synergy
     * Function Name : sendMessage function
     * Description : send message to specific user.
     * @params array(userId).
     * @return message list.
     * 
     */
    public function deleteNotification($params) {
        $notificationId  = $params['notificationsId'];
        //echo $notificationId;exit;

        //$this->db->delete('fj_userNotifications', array('id' => $notificationId));

        $this->db->where_in('id', $notificationId);
        $this->db->delete('fj_userNotifications');

        $this->coreModel->codeMessage('200', $this->lang->line('success'));
        return coreapi_model::$returnArray;
    }

    /*
     *  @Author : Synergy
     *  @Params : array $params(userId, email, mobile)
     *  @Short Description : Get all invites to user on jobs to give interview. Invititation done through corporate portal or superamdin portal
     *  @return : array of data with response code. 
     */
	 
    public function getInvites($params) {
        $userId = $this->coreModel->cleanString($params['userId']);
        $id     = $this->coreModel->getJwtValue($userId);
        $where = "";
        if(!empty($params['email'])) {
          $email = $this->coreModel->cleanString($params['email']);
          $where .= " OR iu.email = '".$email."'";
        }

        if(!empty($params['mobile'])) {
          $mobile = $this->coreModel->cleanString($params['mobile']);
          $where .= " OR iu.userMobile = '".$mobile."'";
        }
        
        $cudDate = date('Y-m-d');
        $result = $this->coreModel->queryResultArray("SELECT DISTINCT iu.jobId, fju.company, iu.userId, job.fjCode, iu.createdAt invitationDate,iu.id id, job.title jobName FROM fj_jobInvitationForUser iu JOIN fj_jobs job ON job.id = iu.jobId JOIN fj_users fju ON fju.id = job.createdBy WHERE ((iu.userId = '".$id."' $where) AND iu.invitationStatus = 0) AND ((job.openTill >= '".$cudDate."') OR (job.openTill = '0000-00-00 00:00:00'))  AND iu.jobId NOT IN(SELECT jobId FROM fj_userAnswers WHERE userId = $id) ORDER BY iu.createdAt DESC LIMIT 3");

        if (count($result) > 0) {
            foreach ($result as $row) {
				$inviteData['invitationId'] = getHashEID($row['id']);
                $inviteData['jobId'] = (int) $row['jobId'];
                $inviteData['fjCode'] = (string) $row['fjCode'];
                $inviteData['company'] = (string) $row['company'];
                $inviteData['jobName'] = (string) $row['jobName'];
                $inviteData['invitationDate'] = (string) $row['invitationDate'];
                coreapi_model::$data[] = $inviteData;
            }
            $this->coreModel->codeMessage('200', $this->lang->line('success'));
            coreapi_model::$returnArray['data'] = coreapi_model::$data;
        } else {
            $this->coreModel->codeMessage('202', $this->lang->line('no_record_found'));
        }
        return coreapi_model::$returnArray;
    }

    /*
     *  @Author : Synergy
     *  @Params : array $params(invitationId)
     *  @Short Description : Get all invites to user on jobs to give interview. Invititation done through corporate portal or superamdin portal
     *  @return : array of data with response code. 
     */
    public function getWebInterviewInvites($params) {
        $invitationId = $this->coreModel->cleanString($params['invitationId']);
        $intId = getEID($invitationId);

        $cudDate = date('Y-m-d');
		//echo "SELECT fju.company, iu.jobId, iu.userId, job.fjCode FROM fj_jobInvitationForUser iu JOIN fj_jobs job ON job.id = iu.jobId JOIN fj_users fju ON fju.id = job.createdBy WHERE (iu.id = '".$intId."' AND iu.invitationStatus = 0) AND job.openTill >= '".$cudDate."' ORDER BY iu.createdAt DESC LIMIT 2";die;
        $result = $this->coreModel->queryResultArray("SELECT fju.company, iu.userName, iu.jobId,iu.email,iu.invitationStatus, iu.userId, job.fjCode,job.title FROM fj_jobInvitationForUser iu JOIN fj_jobs job ON job.id = iu.jobId JOIN fj_users fju ON fju.id = job.createdBy WHERE (iu.id = '".$intId."' AND iu.invitationStatus = 0) ORDER BY iu.createdAt DESC LIMIT 2");

        if (count($result) > 0) {
            foreach ($result as $row) {
                $inviteData['jobId'] = (int) $row['jobId'];
                $inviteData['fjCode'] = (string) $row['fjCode'];
                $inviteData['company'] = (string) $row['company'];
                $inviteData['title'] = (string) $row['title'];
                $inviteData['email'] = (string) $row['email'];
                $inviteData['userName'] = (string) $row['userName'];
                $inviteData['invitationStatus'] = (string) $row['invitationStatus'];
                coreapi_model::$data[] = $inviteData;
            }
            $this->coreModel->codeMessage('200', $this->lang->line('success'));
            coreapi_model::$returnArray['data'] = coreapi_model::$data;
        } else {
            $this->coreModel->codeMessage('202', $this->lang->line('no_record_found'));
        }
        return coreapi_model::$returnArray;
    }

    /*
     *  @Author : Synergy
     *  @Params : array $params(userId, jobId)
     *  @Short Description : Use to delete an invitation on a specific Job by a specic user. Invititation done through corporate portal or superamdin portal
     *  @return : array of data with response code. 
     */
    public function deleteInvite($params) {
        $userId = $this->coreModel->cleanString($params['userId']);
        $userId = $this->coreModel->cleanString($params['jobId']);
        $jobId     = $this->coreModel->getJwtValue($userId);
        $email = '';
        $mobile = '';

        if(!empty($params['email'])) {
          $email = $this->coreModel->cleanString($params['email']);
        }

        if(!empty($params['mobile'])) {
          $mobile = $this->coreModel->cleanString($params['mobile']);
        }
        

        $updateData = array(
                'invitationStatus' => 1
        );

        $this->db->where('userId', $id);
        if($email)
          $this->db->or_where('email', $email);
        if($mobile)
          $this->db->or_where('userMobile', $mobile);
        $this->db->where('jobId', $jobId);
        $this->db->update('fj_jobInvitationForUser', $updateData);

        if ($this->db->affected_rows() > 0) {
            $this->coreModel->codeMessage('200', $this->lang->line('success'));
        } else {
            $this->coreModel->codeMessage('202', $this->lang->line('no_record_found'));
        }
        return coreapi_model::$returnArray;
    }

    /*
     *  @Author : Synergy
     *  @Params : array(employment information).
     *  @Short Description : add/edit employment information
     *  @return : array with code. 
     */
    public function editEmploymentInfo($params) {
      $userId = $this->coreModel->cleanString($params['userId']);
      $id = $this->coreModel->getJwtValue($userId);

      $companyName = $this->coreModel->cleanString($params['companyName']);
      $dateFrom = $this->coreModel->cleanString($params['dateFrom']);
      $dateTo = $this->coreModel->cleanString($params['dateTo']);
      $roleOrPosition = $this->coreModel->cleanString($params['roleOrPosition']);
      $responsibilities = $this->coreModel->cleanString($params['responsibilities']);
      $isPresent = $this->coreModel->cleanString($params['isPresent']);
      $employmentId = $this->coreModel->cleanString($params['id']);

      if($userId != '') {
        $row = $this->coreModel->queryRowArray("SELECT id FROM fj_users WHERE id='$id' AND status='1' LIMIT 0,1");
        if (count($row) > 0){
            if($employmentId != '') {
              $dataEmployment = array(
                'companyName' => $companyName,
                'dateFrom' => $dateFrom,
                'dateTo' => $dateTo,
                'roleOrPosition' => $roleOrPosition,
                'responsibilites' => $responsibilities,
                'isPresent' => $isPresent
              );
              $queryEmp = $this->db->where('userId', $id)->where('id', $employmentId)->update('fj_user_employment', $dataEmployment);
            } else {
              $dataEmployment = array(
                'userId' => $id,
                'companyName' => $companyName,
                'dateFrom' => $dateFrom,
                'dateTo' => $dateTo,
                'roleOrPosition' => $roleOrPosition,
                'responsibilites' => $responsibilities,
                'isPresent' => $isPresent
              );
              $queryEmp = $this->db->insert('fj_user_employment', $dataEmployment);
            }
            $this->coreModel->codeMessage('200', $this->lang->line('success'));
        }else{
          $this->coreModel->codeMessage('202', $this->lang->line('invalid_user'));
        }
      } else {
        $this->coreModel->codeMessage('300', $this->lang->line('missing_required_fields'));
      }
      return coreapi_model::$returnArray;
    }

    /*
     *  @Author : Synergy
     *  @Params : array(userId, token, resumeFile, resumeName).
     *  @Short Description : add/edit resume for user
     *  @return : array with code. 
     */
    function editResume($posts){
        if($this->coreModel->array_keys_exist( $posts, 'userId', 'token', 'resumeFile', 'resumeName')  &&  ($posts['userId']!='') && ($posts['token']!='')) {
            $userId             = $this->coreModel->cleanString($posts['userId']);
            $id                 = $this->coreModel->getJwtValue($userId);

            $fileFailChunk      = $this->coreModel->cleanString($posts['fileFailPart']);
            $isLastChunk        = $this->coreModel->cleanString($posts['isLastPart']);
            $resumeFile       = $posts['resumeFile'];

            $resumeName   = $this->coreModel->cleanString($posts['resumeName']);

            $dir_path_temp_resume    = "uploads/";

            if($posts['token']=='ecbcd7eaee29848978134beeecdfbc7c') {
                $userRow    = $this->coreModel->userExists($id);

                if(count($userRow)>0) {  
                    if ($resumeFile) {
                        $target_path = $dir_path_temp_resume . $resumeName;
                        $copy_target_path = $dir_path_temp_resume.'copy-'.$resumeName;
                        $fileData = base64_decode($resumeFile);

                        $resumeUpdateData = array(
                            'resume_path' => $resumeName,
                            'updatedAt'     => date('Y-m-d H:i:s'),
                        );


                        if (file_exists($target_path)) {
                            copy($target_path, $copy_target_path);

                            if($fileFailChunk == 1) {
                                unlink($target_path);
                                $this->db->where(array('id'=>$id))->update('fj_users', $resumeUpdateData);
                                if($this->db->affected_rows() > 0){
                                    unlink($copy_target_path);
                                    $fid = file_put_contents($target_path, $fileData);
                                    if($fid) {
                                        $this->coreModel->codeMessage('200', $this->lang->line('success'));
                                    } else {
                                        $this->coreModel->codeMessage('503', $this->lang->line('something_wrong'));
                                    }
                                }else{
                                    rename($copy_target_path, $target_path);
                                    $this->coreModel->codeMessage('503', $this->lang->line('something_wrong'));
                                }
                            } else {
                                if (!$file = fopen($target_path, 'a')) {
                                    $this->coreModel->codeMessage('503', $this->lang->line('something_wrong'));
                                } else {
                                    if (fwrite($file, $fileData) === FALSE) {
                                        $this->coreModel->codeMessage('503', $this->lang->line('something_wrong'));
                                    } else {
                                        fclose($file);
                                        $this->db->where(array('id'=>$id))->update('fj_users', $resumeUpdateData);
                                        if($this->db->affected_rows() > 0){
                                            unlink($copy_target_path);
                                            $this->coreModel->codeMessage('200', $this->lang->line('success'));
                                        }else{
                                            unlink($target_path);
                                            rename($copy_target_path, $target_path);
                                            $this->coreModel->codeMessage('503', $this->lang->line('something_wrong'));
                                        }
                                    }
                                }
                            }
                        } else {
                            $fid = file_put_contents($target_path, $fileData);
                            if($fid) {
                                $this->db->where(array('id'=>$id))->update('fj_users', $resumeUpdateData);
 
                                if($this->db->affected_rows() > 0){
                                    unlink($copy_target_path);
                                    $this->coreModel->codeMessage('200', $this->lang->line('success'));
                                }else{
                                    unlink($target_path);
                                    $this->coreModel->codeMessage('503', $this->lang->line('something_wrong'));
                                }
                            } else {
                                $this->coreModel->codeMessage('503', $this->lang->line('something_wrong'));
                            }
                        }

                        /*if($isLastChunk == 1) {

                            $s3 = new S3(awsAccessKey, awsSecretKey);
                            $s3->putBucket($bucket, S3::ACL_PUBLIC_READ);
                            //Rename image name.
                            $tmp                = $target_path;
                            $actual_image_name  = $resumeName;
                            $bucket             = "fjauditions";
                            if($s3->putObjectFile($tmp, $bucket, $actual_image_name, S3::ACL_PUBLIC_READ)) {
                                unlink($target_path);
                            }
                        }*/
                    }
                    else {
                        $this->coreModel->codeMessage('202', $this->lang->line('invalid_audition_file'));
                    }                               
                }
                else {
                    $this->coreModel->codeMessage('500', $this->lang->line('invalid_user'));
                }
            }
            else {
                $this->coreModel->codeMessage('300', $this->lang->line('invalid_token'));
            }
        }
        else {
            $this->coreModel->codeMessage('202', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }

    /*
     *  @Author : Synergy
     *  @Params : array(userId, token, resumeFile, resumeName).
     *  @Short Description : add/edit resume for user
     *  @return : array with code. 
     */
    function editResume_v2($posts){
		
        if($this->coreModel->array_keys_exist( $posts, 'userId', 'token', 'resumeFile', 'resumeName')  &&  ($posts['userId']!='') && ($posts['token']!='')) {
            $userId             = $this->coreModel->cleanString($posts['userId']);
            $id                 = $this->coreModel->getJwtValue($userId);

            $fileFailChunk      = $this->coreModel->cleanString($posts['fileFailChunk']);
            $isLastChunk        = $this->coreModel->cleanString($posts['isLastChunk']);

            if(isset($posts['fjCode'])) {
                $jcode             = $this->coreModel->cleanString($posts['fjCode']);
            } else {
                $jcode = '';
            }

            $resumeFile       = $posts['resumeFile'];

            $resumeName   = $this->coreModel->cleanString($posts['resumeName']);


            $dir_path_temp_resume    = "uploads/resumes/";

            if($posts['token']=='ecbcd7eaee29848978134beeecdfbc7c') {
                $userRow    = $this->coreModel->userExists($id);

                if(count($userRow)>0) {
                    if ($resumeFile) {
                        $target_path = $dir_path_temp_resume . $resumeName;
                        $copy_target_path = $dir_path_temp_resume.'copy-'.$resumeName;
                        $fileData = base64_decode($resumeFile);

                        $resumeUpdateData = array(
                            'resume_path' => $resumeName,
                            'updatedAt'     => date('Y-m-d H:i:s'),
                        );


                        if (file_exists($target_path)) {
                            copy($target_path, $copy_target_path);

                            if($fileFailChunk == 1) {
                                unlink($target_path);
                                unlink($copy_target_path);
                                $fid = file_put_contents($target_path, $fileData);
                                if($fid) {
                                    $this->coreModel->codeMessage('200', $this->lang->line('success'));
                                } else {
                                    $this->coreModel->codeMessage('503', $this->lang->line('something_wrong'));
                                }
                            } else {
                                if (!$file = fopen($target_path, 'a')) {
                                    $this->coreModel->codeMessage('503', $this->lang->line('something_wrong'));
                                } else {
                                    if (fwrite($file, $fileData) === FALSE) {
                                        $this->coreModel->codeMessage('503', $this->lang->line('something_wrong'));
                                    } else {
                                        fclose($file);
                                        unlink($copy_target_path);
                                        $this->coreModel->codeMessage('200', $this->lang->line('success'));
                                    }
                                }
                            }
                        } else {
                            $fid = file_put_contents($target_path, $fileData);
                            if($fid) {
                                //unlink($copy_target_path);
                                $this->coreModel->codeMessage('200', $this->lang->line('success'));
                            } else {
                                $this->coreModel->codeMessage('503', $this->lang->line('something_wrong'));
                            }
                        }
                        if($isLastChunk == 1) {
                            $filesize = filesize($target_path);
                            $urlofResume = 'https://s3.amazonaws.com/resumefirstjob/'.$resumeName;
                            /*$urlofResume = base_url().$target_path;
                            $resumeInsertData = array(
                                                    'userId' => $id,
                                                    'resumeName' => $resumeName,
                                                    'fileSize' => $filesize,
                                                    'url' => $urlofResume,
                                                    'active' => 0,
                                                    'uploadSource' => 1,
                                                    'createdBy' => $id,
                                                    'createdAt'     => date('Y-m-d H:i:s'),
                                                    );
                            $this->db->insert('fj_user_resumes', $resumeInsertData);*/

                            //Uploading resume in firstjobresume
                            $s3 = new S3('AKIAJY7WPI2VG2OQ4GGA', 'usHqZSFHZ8Cr20Ms0xjloEIi6iMS6YfEXE5MagUC');
                            $bucket             = "resumefirstjob";
                            $s3->putBucket($bucket, S3::ACL_PUBLIC_READ);
                            //Rename image name.
                            $tmp                = $target_path;
							
                            $actual_resume_name  = $resumeName;
                            
                            if($s3->putObjectFile($tmp, $bucket, $actual_resume_name, S3::ACL_PUBLIC_READ)) {
                                $resumeInsertData = array(
                                                    'userId' => $id,
                                                    'resumeName' => $resumeName,
                                                    'fileSize' => $filesize,
                                                    'url' => $urlofResume,
                                                    'active' => 0,
                                                    'uploadSource' => 1,
                                                    'createdBy' => $id,
                                                    'createdAt'     => date('Y-m-d H:i:s'),
                                                    );
                                $this->db->insert('fj_user_resumes', $resumeInsertData);

                                if($jcode != '') {
                                    $jobData = $this->coreModel->queryRowArray("SELECT id FROM fj_jobs WHERE fjCode='$jcode'");
                                    $jobUpdateDataResumePath = array(
                                        'jobResume' => $resumeName,
                                        'updatedAt'     => date('Y-m-d H:i:s'),
                                    );
                                    $this->db->where(array('jobId'=>$jobData['id'], 'userId'=>$id))->update('fj_temp_userjob', $jobUpdateDataResumePath);
									$this->db->where(array('jobId'=>$jobData['id'], 'userId'=>$id))->update('fj_userJob', $jobUpdateDataResumePath);
                                }
                                unlink($target_path);
                            }
                        }
                    }
                    else {
                        $this->coreModel->codeMessage('202', $this->lang->line('invalid_audition_file'));
                    }                               
                }
                else {
                    $this->coreModel->codeMessage('500', $this->lang->line('invalid_user'));
                }
            }
            else {
                $this->coreModel->codeMessage('300', $this->lang->line('invalid_token'));
            }
        }
        else {
            $this->coreModel->codeMessage('202', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }

    /*
     *  @Author : Synergy
     *  @Params : userId
     *  @Short Description : get User's resumes
     *  @return : array of resumes with code. 
     */
    public function getUserResumes($params) {
        $userId = $this->coreModel->cleanString($params['userId']);
        $id = $this->coreModel->getJwtValue($userId);
        if ($userId != '') {
            $isUserExist = $this->coreModel->queryRowArray("SELECT id FROM fj_users WHERE id='$id' AND status='1'");
            if (count($isUserExist) > 0) {
                // Resume Information
                $resumeInfo = array();
                $resumeInfoResults = $this->coreModel->queryResultArray("SELECT *  FROM fj_user_resumes WHERE userId='$id'");
                foreach ($resumeInfoResults as $row) {
                    $rowDataResumeInfo['resumeId'] = (string) $row['id'];
                    $rowDataResumeInfo['resumeName'] = (string) $row['resumeName'];
                    $rowDataResumeInfo['url'] = (string) $row['url'];
                    $rowDataResumeInfo['active'] = (string) $row['active'];
                    $rowDataResumeInfo['createdAt'] = date('M d, Y', strtotime($row['createdAt']));
                    $resumeInfo[] = $rowDataResumeInfo;
                }
                //--------------- Response Data ---------------//
                $this->coreModel->codeMessage('200', $this->lang->line('success'));
                coreapi_model::$returnArray['resumeList'] = $resumeInfo;
            } else {
                $this->coreModel->codeMessage('500', $this->lang->line('no_record_found'));
                coreapi_model::$returnArray['resumeList'] = array();
            }
        } else {
            $this->coreModel->codeMessage('300', $this->lang->line('missing_required_fields'));
            coreapi_model::$returnArray['resumeList'] = array();
        }
        return coreapi_model::$returnArray;
    }

    /*
     *  @Author : Synergy
     *  @Params : userId
     *  @Short Description : get User's resumes
     *  @return : array of resumes with code. 
     */
    public function setJobResume($params) {
        $userId = $this->coreModel->cleanString($params['userId']);
        $resumeId = $this->coreModel->cleanString($params['resumeId']);
        $jobCode = $this->coreModel->cleanString($params['jobCode']);
        $id = $this->coreModel->getJwtValue($userId);
        if ($userId != '') {
            $isUserExist = $this->coreModel->queryRowArray("SELECT id FROM fj_users WHERE id='$id' AND status='1'");
            if (count($isUserExist) > 0) {
                // Resume Information
                $resumeInfo = array();
                $resumeInfoResults = $this->coreModel->queryRowArray("SELECT *  FROM fj_user_resumes WHERE id='$resumeId'");

                if($resumeInfoResults) {
                    $jobData = $this->coreModel->queryRowArray("SELECT id FROM fj_jobs WHERE fjCode='$jobCode'");
                    $jobUpdateDataResumePath = array(
                        'jobResume' => $resumeInfoResults['resumeName'],
                        'updatedAt'     => date('Y-m-d H:i:s'),
                    );
                    $this->db->where(array('jobId'=>$jobData['id'], 'userId'=>$id))->update('fj_temp_userjob', $jobUpdateDataResumePath);
					$this->db->where(array('jobId'=>$jobData['id'], 'userId'=>$id))->update('fj_userJob', $jobUpdateDataResumePath);
					
                    $this->coreModel->codeMessage('200', $this->lang->line('success'));
                } else {
                    $this->coreModel->codeMessage('500', $this->lang->line('no_record_found'));
                }
                
            } else {
                $this->coreModel->codeMessage('500', $this->lang->line('no_record_found'));
            }
        } else {
            $this->coreModel->codeMessage('300', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }

    /*
     *  @Author : Synergy
     *  @Params : array(userId, resumeId).
     *  @Short Description : Use to set user active resume
     *  @return : array with code. 
     */
    public function setUserActiveResume($params) {
        $userId = $this->coreModel->cleanString($params['userId']);
        $resumeId = $this->coreModel->cleanString($params['resumeId']);
        $id = $this->coreModel->getJwtValue($userId);

        if($userId != '' && $resumeId != '') {
            $isUserExist = $this->coreModel->queryRowArray("SELECT id FROM fj_users WHERE id='$id' AND status='1'");
            if (count($isUserExist) > 0) {
                $resumeData = $this->coreModel->queryRowArray("SELECT * FROM fj_user_resumes WHERE id='$resumeId'");
                if (count($resumeData) > 0){
                    //Updating user row with active resume
                    $resumeUpdateData = array(
                            'resume_path' => $resumeData['resumeName'],
                            'updatedAt'     => date('Y-m-d H:i:s'),
                        );
                    $this->db->where(array('id'=>$id))->update('fj_users', $resumeUpdateData);

                    //Updating resume table with active resume
                    $resumeTableData = array(
                            'active' => 1
                        );
                    $this->db->where(array('id'=>$resumeId))->update('fj_user_resumes', $resumeTableData);

                    //Updating resume table with inactive resume
                    $resumeTableData = array(
                            'active' => 0
                        );
                    $this->db->where(array('id !=' => $resumeId))->update('fj_user_resumes', $resumeTableData);

                    $this->coreModel->codeMessage('200', $this->lang->line('success'));
                }else{
                    $this->coreModel->codeMessage('202', $this->lang->line('no_record_found'));
                }
            } else {
                $this->coreModel->codeMessage('500', $this->lang->line('no_record_found'));
            }
        } else {
            $this->coreModel->codeMessage('300', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }

    /*
     *  @Author : Synergy
     *  @Params : array(userId, resumeId).
     *  @Short Description : Use to set user active resume
     *  @return : array with code. 
     */
    public function deleteUserResume($params) {
        $userId = $this->coreModel->cleanString($params['userId']);
        $resumeId = $this->coreModel->cleanString($params['resumeId']);
        $id = $this->coreModel->getJwtValue($userId);

        if($userId != '' && $resumeId != '') {
            $isUserExist = $this->coreModel->queryRowArray("SELECT id, resume_path FROM fj_users WHERE id='$id' AND status='1'");
            if (count($isUserExist) > 0) {
                $resumeData = $this->coreModel->queryRowArray("SELECT * FROM fj_user_resumes WHERE id='$resumeId'");
                if (count($resumeData) > 0){
                    //Updating user row with active resume
                    if($isUserExist['resume_path'] == $resumeData['resumeName'] && $resumeData['active'] == 1) {
                        $resumeUpdateData = array(
                                'resume_path' => '',
                                'updatedAt'     => date('Y-m-d H:i:s'),
                            );
                        $this->db->where(array('id'=>$id))->update('fj_users', $resumeUpdateData);
                    }

                    //Unlink the specified file
                    $bucket = "resumefirstjob";
                    $s3 = new S3(awsAccessKey, awsSecretKey);
                    $s3->putBucket($bucket, S3::ACL_PUBLIC_READ);
                    $actual_resume_name  = $resumeData['resumeName'];
                    $s3->deleteObject($bucket, $actual_resume_name, S3::ACL_PUBLIC_READ);

                    //Updating resume table with inactive resume
                    $this->db->delete('fj_user_resumes', array('id' => $resumeId)); 

                    $this->coreModel->codeMessage('200', $this->lang->line('success'));
                }else{
                    $this->coreModel->codeMessage('202', $this->lang->line('no_record_found'));
                }
            } else {
                $this->coreModel->codeMessage('500', $this->lang->line('no_record_found'));
            }
        } else {
            $this->coreModel->codeMessage('300', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }

    /*
     *  @Author : Synergy
     *  @Params : array(email, token).
     *  @Short Description : check for user exists or not
     *  @return : array with code. 
     */
    public function isUserExists($params) {
        $email = $this->coreModel->cleanString($params['email']);

        if($email != '') {
            $row = $this->coreModel->queryRowArray("SELECT id FROM fj_users WHERE email='$email' AND status='1'");
            if (count($row) > 0){
                $this->coreModel->codeMessage('200', $this->lang->line('success'));
            }else{
                $this->coreModel->codeMessage('202', $this->lang->line('invalid_user'));
            }
        } else {
            $this->coreModel->codeMessage('300', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }
	/*
     *  @Author : Synergy
     *  @Params : array(email, token).
     *  @Short Description : check for user exists or not
     *  @return : array with code. 
     */
    public function checkProfile($params) {
        $email = $this->coreModel->cleanString($params['email']);

        if($email != '') {
            $row = $this->coreModel->queryRowArray("SELECT id,password,fb_id,google_id FROM fj_users WHERE email='$email' AND status='1'");
            if (count($row) > 0){
				
				if(empty($row['password']) && empty($row['fb_id']) && empty($row['google_id'])){
					$this->coreModel->codeMessage('200', $this->lang->line('success'));
				}else{
					$this->coreModel->codeMessage('202', "valid user");
				}
            }else{
                $this->coreModel->codeMessage('202', $this->lang->line('invalid_user'));
            }
        } else {
            $this->coreModel->codeMessage('300', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }

    /*
     *  @Author : Synergy
     *  @Params : array(email, token).
     *  @Short Description : upload resume on s3 bucket
     *  @return : array with code. 
     */
    public function uploadResumeWithDocumentPathOld($params) {
        $email = $this->coreModel->cleanString($params['email']);


        if($email != '') {
            $row = $this->coreModel->queryRowArray("SELECT id, resume_path FROM fj_users WHERE email='$email' AND status='1' LIMIT 0,1");
            if (count($row) > 0){
                $dir_path_temp    = "uploads/";
                $target_path = $dir_path_temp . $row['resume_path'];
                //$target_path = $row['resume_path'];

                $resumeName = $row['resume_path'];

                //move file on s3 bucket
                $s3 = new S3(awsAccessKey, awsSecretKey);
                $s3->putBucket($bucket, S3::ACL_PUBLIC_READ);
                //Rename image name.
                $tmp                = $target_path;
                $actual_image_name  = $resumeName;
                $bucket             = "fjauditions";
                if($s3->putObjectFile($tmp, $bucket, $actual_image_name, S3::ACL_PUBLIC_READ)) {
                    unlink($target_path);
                }
                $this->coreModel->codeMessage('200', $this->lang->line('success'));
            }else{
                $this->coreModel->codeMessage('202', $this->lang->line('invalid_user'));
            }
        } else {
            $this->coreModel->codeMessage('300', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }

    /*
     *  @Author : Synergy
     *  @Params : array(email, token).
     *  @Short Description : upload resume on s3 bucket
     *  @return : array with code. 
     */
    public function uploadResumeWithDocumentPath($params) {
        $email = $this->coreModel->cleanString($params['email']);
        $resume_path = $this->coreModel->cleanString($params['resume_path']);

        if($email != '' || $resume_path != '') {
            $row = $this->coreModel->queryRowArray("SELECT id FROM fj_users WHERE email='$email' AND status='1' LIMIT 0,1");
            if (count($row) > 0){

                $updateData = array(
                    'resume_path' => $resume_path
                );
                $query = $this->db->where('email', $email)->update('fj_users', $updateData);

                if ($query) {
                  $this->coreModel->codeMessage('200', $this->lang->line('success'));
                } else {
                  $this->coreModel->codeMessage('500', $this->lang->line('something_wrong'));
                }
            }else{
                $this->coreModel->codeMessage('202', $this->lang->line('invalid_user'));
            }
        } else {
            $this->coreModel->codeMessage('300', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }

    /*
     *  @Author : Synergy
     *  @Params : array(email, guid).
     *  @Short Description : update guid for specific email
     *  @return : array with code. 
     */
    public function updateProcessStatusForEmail($params) {
        $email = $this->coreModel->cleanString($params['email']);
        $guid = $this->coreModel->cleanString($params['guid']);

        if($email != '' || $guid != '') {
            $row = $this->coreModel->queryRowArray("SELECT id FROM fj_users WHERE email='$email' AND status='1'");
            if (count($row) > 0){
                $guidData = $this->coreModel->queryRowArray("SELECT id FROM fj_users WHERE resume_guid='$guid'");
                if(count($guidData) > 0) {
                  $this->coreModel->codeMessage('503', $this->lang->line('something_wrong'));
                  coreapi_model::$returnArray['status'] = '0';
                } else {
                  $updateData = array(
                    'resume_guid' => $guid
                  );
                  $query = $this->db->where('email', $email)->update('fj_users', $updateData);

                  if ($query) {
                      $this->coreModel->codeMessage('200', $this->lang->line('success'));
                      coreapi_model::$returnArray['status'] = '1';
                  } else {
                      $this->coreModel->codeMessage('500', $this->lang->line('something_wrong'));
                  }
                }
            }else{
                $this->coreModel->codeMessage('202', $this->lang->line('invalid_user'));
            }
        } else {
            $this->coreModel->codeMessage('300', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }
    
    /*
     *  @Author : Synergy
     *  @Params : array(userId, email, aadharNumber, aadharReferenceNumber).
     *  @Short Description : Update aadhar number
     *  @return : array with code. 
     */
    //-------------- Edit User Profile Social Information Starts --------------//
    public function updateAadharActivationStatus($params) {
        $userId = $this->coreModel->cleanString($params['userId']);
        $email = $this->coreModel->cleanString($params['email']);
        $id = $this->coreModel->getJwtValue($userId);
        $aadharNumber = $this->coreModel->cleanString($params['aadharNumber']);
        $aadharReferenceNumber = $this->coreModel->cleanString($params['aadharReferenceNumber']);

        if ($userId != '') {
            $row = $this->coreModel->queryRowArray("SELECT id FROM fj_users WHERE id='$id' AND email='$email' AND status='1' LIMIT 0,1");
            if (count($row) > 0) {
                
                $updateData = array(
                    'adharCard' => $aadharNumber,
                    'aadharReferenceNumber' => $aadharReferenceNumber,
                    'isAaadharActivated' => 1
                );
                $query = $this->db->where('id', $id)->update('fj_users', $updateData);
                if ($query) {
                    $this->coreModel->codeMessage('200', $this->lang->line('success'));
                } else {
                    $this->coreModel->codeMessage('500', $this->lang->line('something_wrong'));
                }
            } else {
                $this->coreModel->codeMessage('202', $this->lang->line('invalid_user'));
            }
        } else {
            $this->coreModel->codeMessage('300', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }

    /*
     *  @Author : Synergy
     *  @Params : array(userId, deviceId).
     *  @Short Description : Update device token
     *  @return : array with code. 
     */
    public function updateDeviceToken($params) {
        $userId = $this->coreModel->cleanString($params['userId']);
        $id = $this->coreModel->getJwtValue($userId);
        $deviceId = $this->coreModel->cleanString($params['deviceId']);
        $deviceType = $this->coreModel->cleanString($params['deviceType']);


        if ($userId != '') {
            $row = $this->coreModel->queryRowArray("SELECT id FROM fj_users WHERE id='$id' AND status='1' LIMIT 0,1");
            if (count($row) > 0) {
                
                $updateData = array(
                    'deviceId' => $deviceId,
                    'deviceType' => $deviceType
                );
                $query = $this->db->where('id', $id)->update('fj_users', $updateData);
                if ($query) {
                    $this->coreModel->codeMessage('200', $this->lang->line('success'));
                } else {
                    $this->coreModel->codeMessage('500', $this->lang->line('something_wrong'));
                }
            } else {
                $this->coreModel->codeMessage('202', $this->lang->line('invalid_user'));
            }
        } else {
            $this->coreModel->codeMessage('300', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }

    /*
     *  @Author : Synergy
     *  @Params : userId
     *  @Short Description : get User's employment information
     *  @return : array with code. 
     */
    public function getEmploymentInformation($params) {
        $userId = $this->coreModel->cleanString($params['userId']);
        $id = $this->coreModel->getJwtValue($userId);
        if ($userId != '') {
            $row = $this->coreModel->queryRowArray("SELECT id FROM fj_users WHERE id='$id' AND status='1'");
            if (count($row) > 0) {
                // Employment Information
                $employmentInfo = array();
                $employmentResults = $this->coreModel->queryResultArray("SELECT ue . *  FROM fj_user_employment ue JOIN fj_users u ON u.id = ue.userId WHERE ue.userId='$id' AND ue.status='1'");
                foreach ($employmentResults as $row) {
                    $rowDataEmploymentInfo['id'] = (string) $row['id'];
                    $rowDataEmploymentInfo['userId'] = (string) $row['userId'];
                    $rowDataEmploymentInfo['companyName'] = (string) $row['companyName'];
                    $rowDataEmploymentInfo['dateFrom'] = (string) $row['dateFrom'];
                    $rowDataEmploymentInfo['dateTo'] = (string) $row['dateTo'];
                    $rowDataEmploymentInfo['roleOrPosition'] = (string) $row['roleOrPosition'];
                    $rowDataEmploymentInfo['responsibilities'] = (string) $row['responsibilites'];
                    $rowDataEmploymentInfo['isPresent'] = (string) $row['isPresent'];
                    $employmentInfo[] = $rowDataEmploymentInfo;
                }
                //--------------- Response Data ---------------//
                $this->coreModel->codeMessage('200', $this->lang->line('success'));
                coreapi_model::$returnArray['employmentInfo'] = $employmentInfo;
                coreapi_model::$returnArray['profilePercent'] = Userapi_model::profilePercent($id);
            } else {
                $this->coreModel->codeMessage('500', $this->lang->line('no_record_found'));
                coreapi_model::$returnArray['employmentInfo'] = array();
                coreapi_model::$returnArray['profilePercent'] = 0;
            }
        } else {
            $this->coreModel->codeMessage('300', $this->lang->line('missing_required_fields'));
            coreapi_model::$returnArray['employmentInfo'] = array();
            coreapi_model::$returnArray['profilePercent'] = 0;
        }
        return coreapi_model::$returnArray;
    }

    /*
     *  @Author : Synergy
     *  @Params : userId, id(employmentId)
     *  @Short Description : Remove User's Selected employment information
     *  @return : array with code. 
     */
    public function removeEmployment($params) {
        $userId = $this->coreModel->cleanString($params['userId']);
        $id = $this->coreModel->getJwtValue($userId);
        $employmentId = $this->coreModel->cleanString($params['id']);
        if ($employmentId != '' && $userId != '') {
            $row = $this->coreModel->queryRowArray("SELECT id FROM fj_user_employment WHERE id='$employmentId' AND userId='$id' AND status='1'");
            if (count($row) == '1') {
                $updateData = array('status' => '0');
                $queryEmp = $this->db->where('id', $employmentId)->where('userId', $id)->update('fj_user_employment', $updateData);
                //--------------- Response Data ---------------//
                $this->coreModel->codeMessage('200', $this->lang->line('success'));
            } else {
                $this->coreModel->codeMessage('202', $this->lang->line('no_record_found'));
            }
        } else {
            $this->coreModel->codeMessage('202', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }

    /*
     *  @Author : Synergy
     *  @Params : userId
     *  @Short Description : Use to get candidate profile average percent
     *  @return : array with code. 
     */
    public function getCandidateProfileAverage($params) {
        $userId = $this->coreModel->cleanString($params['userId']);
        $id = $this->coreModel->getJwtValue($userId);
        if ($userId != '') {
            $this->coreModel->codeMessage('200', $this->lang->line('success'));
            coreapi_model::$returnArray['profileAveragePercent'] = '80';
        } else {
            $this->coreModel->codeMessage('202', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }

    /*
     *  @Author : Synergy
     *  @Params : userId
     *  @Short Description : Use to get candidate profile average percent
     *  @return : array with code. 
     */
    public function getCandidateAverageAppliedJobs($params) {
        $userId = $this->coreModel->cleanString($params['userId']);
        $id = $this->coreModel->getJwtValue($userId);
        if ($userId != '') {
            $this->coreModel->codeMessage('200', $this->lang->line('success'));
            coreapi_model::$returnArray['averageAppliedJobs'] = '8';
        } else {
            $this->coreModel->codeMessage('202', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }


    /*
     *  @Author : Synergy
     *  @Params : array(user email, pass etc).
     *  @Short Description : Created version 3 api for register a user 
     *  @return : array with code. 
     */
    public function updateDetailsOfJobsSeeker($params) {
        //$gender = ($this->coreModel->cleanString($basicInformation[0]['gender']) != '' ? ($this->coreModel->cleanString($basicInformation[0]['gender']) == 'Male' ? 1 : 2) : $this->coreModel->cleanString($row['gender']));
        //$dob = ($this->coreModel->cleanString($basicInformation[0]['dob']) != '' ? date('Y-m-d', strtotime($this->coreModel->cleanString($basicInformation[0]['dob']) . '-01-01')) : $this->coreModel->cleanString($row['dob']));

        $fullname   = $this->coreModel->cleanString($params['fullname']);
        $mobile     = substr($this->coreModel->cleanString($params['mobile']), -10);
        $email   = $this->coreModel->cleanString($params['email']);
        $gender   = $this->coreModel->cleanString($params['gender']);
        $dob   = $this->coreModel->cleanString($params['dob']) . '-01-01';
        $pincode   = $this->coreModel->cleanString($params['pincode']);

        $userId = $this->coreModel->cleanString($params['userId']);
        $id = $this->coreModel->getJwtValue($userId);
        // Check Params Keys
        if (
                $id   != '' &&
                $fullname   != '' &&
                $mobile     != '' &&
                $email   != ''  
        ) {
            try {
                $row = $this->coreModel->queryRowArray("SELECT email FROM fj_users WHERE id='$id' AND status='1' LIMIT 0,1");
                if (count($row) > 0) {
                    $emailExistsCheck = $this->coreModel->queryRowArray("SELECT id FROM fj_users WHERE id != '$id' AND status='1' AND email = '$email'");

                    $mobileExistsCheck = $this->coreModel->queryRowArray("SELECT id FROM fj_users WHERE id != '$id' AND status='1' AND mobile = '$mobile'");
                    if(count($emailExistsCheck) > 0) {
                        $this->coreModel->codeMessage('202', 'Email already exists. Please user another one!');
                    } elseif (count($mobileExistsCheck) > 0) {
                        $this->coreModel->codeMessage('202', 'Mobile number already exists. Please user another one!');
                    }
                    else {
                        $updateData = array(
                            'fullname' => $fullname,
                            'email' => $email,
                            'mobile' => $mobile,
                            'gender' => $gender,
                            'dob' => $dob,
                            'pinCode' => $pincode,
                        );
                        $query = $this->db->where('id', $id)->update('fj_users', $updateData);

                        $this->coreModel->codeMessage('200', $this->lang->line('success'));
                    }
                    
                } else {
                    $message = "User doesn't Exists";
                    $this->coreModel->codeMessage('202', $message);
                }
            } catch (Exception $e) {
                $this->coreModel->codeMessage('500', $e->getMessage());
            }
        } else {
            $this->coreModel->codeMessage('300', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }

    /*
     *  @Author : Synergy
     *  @Params : userId, id(employmentId)
     *  @Short Description : Remove User's Selected employment information
     *  @return : array with code. 
     */
    public function getJobSeekerEvent($params) {
        $eventDate = strtotime($this->config->item('eventDatePHP'));
        $curDate = strtotime(date('Y-m-d 00:00:00'));

		$row = $this->coreModel->queryResultArray("SELECT event.id id, event.eventName eventName, event.description description, event.startDate startDate, event.endDate FROM fj_events event  ORDER BY event.createdAt DESC LIMIT 1");

       
        $arr = array();

        if($eventDate <= $curDate) {
            $rowArray['description'] = $row[0]['description'];
        } else {
            $rowArray['description'] = $row[0]['description'];
        }
        $rowArray['id'] = $row[0]['id'];
        $rowArray['eventName'] = $row[0]['eventName'];
        $rowArray['startDate'] = $row[0]['startDate'];
        $rowArray['endDate'] = $row[0]['endDate'];
        $arr[] = $rowArray;
		
        if (count($row) > 0) {
            $this->coreModel->codeMessage('200', $this->lang->line('success'));
            coreapi_model::$returnArray['eventList'] = $arr;
        } else {
            $this->coreModel->codeMessage('202', $this->lang->line('no_record_found'));
        }
        
        return coreapi_model::$returnArray;
    }

    /*
     *  @Author : Synergy
     *  @Params : userId, eventId
     *  @Short Description : Get all jobs for a specific event
     *  @return : array with code. 
     */
    public function getJobsForEvent($params) {
        
        $eventId = $this->coreModel->cleanString($params['eventId']);
        $jobArray = array();
            $rows = $this->coreModel->queryResultArray("SELECT job.id jobId, job.fjCode fjCode, job.noOfVacancies noOfVacancies, job.title title, job.description description, IF(job.sourceCompanyName != 'NULL', job.sourceCompanyName, user.company) company  FROM fj_jobs job JOIN fj_events event ON event.id = job.eventId JOIN fj_users user ON user.id = job.createdBy WHERE job.eventId = '$eventId' ORDER BY job.createdAt DESC");

            $totalJobsCount = $this->coreModel->queryRowArray("SELECT count(tu.id) as totalJobs  FROM `fj_temp_userjob` tu JOIN fj_jobs fj ON fj.id = tu.jobId WHERE fj.eventId = '$eventId'");

            $totalShortlistedCount = $this->coreModel->queryRowArray("SELECT *  FROM `fj_eventConfigData`");

            $noOfVacancies = 0;
            if (count($rows) > 0) {
                foreach ($rows as $row) {
                    $jobRow['jobId'] = $row['jobId'];
                    $jobRow['fjCode'] = $row['fjCode'];
                    $jobRow['title'] = $row['title'];
                    $jobRow['description'] = $row['description'];
                    $jobRow['company'] = $row['company'];
                    $jobRow['vacancies'] = $row['noOfVacancies'];
                    
                    $locs = $this->jobLocations($row['jobId']);
                    $locArray = array();
                    foreach ($locs as $loc) {
                        $locArray[] = $loc['city'];
                    }
                    $jobRow['locations'] = implode(',', $locArray);
                    $jobArray[] = $jobRow;

                    $noOfVacancies += $row['noOfVacancies'];
                }
                $this->coreModel->codeMessage('200', $this->lang->line('success'));
                coreapi_model::$returnArray['eventList'] = $jobArray;
                coreapi_model::$returnArray['totalApplicationReceived'] = $totalJobsCount['totalJobs'];
                coreapi_model::$returnArray['totalShortlistedApplication'] = $totalShortlistedCount['parameterValue'];
                coreapi_model::$returnArray['totalVacancies'] = $noOfVacancies;
            } else {
                $this->coreModel->codeMessage('202', $this->lang->line('no_record_found'));
            }
        
        return coreapi_model::$returnArray;
    }

    /*
     *  @Author : Synergy
     *  @Params : userId, eventId
     *  @Short Description : Get all jobs for a specific event
     *  @return : array with code. 
     */
    public function getJobFairApplicationData($params) {
        $eventId = $this->coreModel->cleanString($params['eventId']);

        $totalJobsCount = $this->coreModel->queryRowArray("SELECT count(tu.id) as totalJobs  FROM `fj_temp_userjob` tu JOIN fj_jobs fj ON fj.id = tu.jobId WHERE fj.eventId = '$eventId'  and tu.createdAt > '2017-06-02 00:00:00'");
		$actualCount = $totalJobsCount['totalJobs'];
		
		$eventDatePHP = $this->config->item('eventDatePHP');
		
        $actualTotalJobsCount = $this->coreModel->queryRowArray("select Floor(TIME_TO_SEC(timediff(now(),'2017-06-05 12:00:00')) / 60) as a");
		$actualSecondsCount = $actualTotalJobsCount['a'];
		$calCount = 12;
		$actualSecondsCount = $actualSecondsCount-120;
		$calCount = $calCount+ $actualSecondsCount/8;
		
		//$actualCount = $actualCount + $calCount+ 58;

		//$actualCount = $actualCount + 110;
//        if ($calCount > 0)
//            $actualCount = $actualCount + $calCount+ 147;
//        else
//            $actualCount = $actualCount + 147;
        //$actualCount = $actualCount + 147;
        $actualCount = $actualCount + 185;
		
		/*(if($actualSecondsCount>0){
			if($actualSecondsCount%2 == 0){
				
				$actualCount = $actualCount+floor($Counter)*$even+ceil($Counter)*$odd;
			}else{
				$actualCount = $actualCount+ceil($Counter)*$odd+floor($Counter)*$even;
			}
		}*/
        $totalShortlistedCount = $this->coreModel->queryRowArray("SELECT *  FROM `fj_eventConfigData`");

            if ($eventId) {
                $this->coreModel->codeMessage('200', $this->lang->line('success'));
                coreapi_model::$returnArray['totalApplicationReceived'] = floor($actualCount);
                coreapi_model::$returnArray['totalShortlistedApplication'] = $totalShortlistedCount['parameterValue'];
                //coreapi_model::$returnArray['totalShortlistedApplication'] = 5;
            } else {
                $this->coreModel->codeMessage('202', $this->lang->line('no_record_found'));
            }
        return coreapi_model::$returnArray;
    }
	
	public function getJobFairApplicationData1($params) {

        $eventId = $this->coreModel->cleanString($params['eventId']);

        $totalJobsCount = $this->coreModel->queryRowArray("SELECT count(tu.id) as totalJobs  FROM `fj_temp_userjob` tu JOIN fj_jobs fj ON fj.id = tu.jobId WHERE fj.eventId = '$eventId'  and tu.createdAt > '2017-06-02 00:00:00'");
        $actualCount = $totalJobsCount['totalJobs'];

        $eventDatePHP = $this->config->item('eventDatePHP');

        $actualTotalJobsCount = $this->coreModel->queryRowArray("select Floor(TIME_TO_SEC(timediff(now(),'2017-06-04 10:30:00')) / 60) as a");
        $actualSecondsCount = $actualTotalJobsCount['a'];
        $calCount = 12;
        $actualSecondsCount = $actualSecondsCount-120;
        $calCount = $calCount+ $actualSecondsCount/8;

        //$actualCount = $actualCount + $calCount+ 58;

        //$actualCount = $actualCount + 110;
        if ($calCount > 0)
            $actualCount = $actualCount + $calCount+ 110;
        else
            $actualCount = $actualCount + 110;

        /*(if($actualSecondsCount>0){
            if($actualSecondsCount%2 == 0){

                $actualCount = $actualCount+floor($Counter)*$even+ceil($Counter)*$odd;
            }else{
                $actualCount = $actualCount+ceil($Counter)*$odd+floor($Counter)*$even;
            }
        }*/
        $totalShortlistedCount = $this->coreModel->queryRowArray("SELECT *  FROM `fj_eventConfigData`");

        if ($eventId) {
            $this->coreModel->codeMessage('200', $this->lang->line('success'));
            coreapi_model::$returnArray['totalApplicationReceived'] = floor($actualCount);
            coreapi_model::$returnArray['totalShortlistedApplication'] = $totalShortlistedCount['parameterValue'];
            //coreapi_model::$returnArray['totalShortlistedApplication'] = 5;
        } else {
            $this->coreModel->codeMessage('202', $this->lang->line('no_record_found'));
        }
        return coreapi_model::$returnArray;
    }
	

    /**
     * Description : Use to get all location for a particular job
     * Author : Synergy
     * @param int $jobId
     * @return array of data 
     */ 
    protected function jobLocations($jobId){        
        $jobLocation = array();
        $jobLocationRow = $this->jobLocationQuery($jobId);
        if(count($jobLocationRow)>0) {
            foreach($jobLocationRow as $locationData) {
                $location['city']       = (string)$locationData['city'];
                $location['state']      = (string)$locationData['state'];
                $location['country']    = (string)$locationData['country'];
                $jobLocation[]  = $location;
            }
        }
        return $jobLocation;
    }

    /**
     * Description : Use to Get All Location For A Job
     * Author : Synergy
     * @param int $jobRowID
     * @return array of data 
     */
    protected function jobLocationQuery($jobRowID){ 
        $jobLocationResult = $this->coreModel->queryResultArray("
                                                            SELECT 
                                                                    JL.*, 
                                                                    CS.city, 
                                                                    CS.state, 
                                                                    CS.country 
                                                            FROM 
                                                                    fj_jobLocations JL 
                                                            JOIN 
                                                                    fj_cityState CS 
                                                            WHERE 
                                                                    JL.location=CS.id       AND 
                                                                    JL.jobId='$jobRowID'    AND
                                                                    status='1'
                                                        ");
        return $jobLocationResult;
    }

    /*
     *  @Author : Synergy
     *  @Params : userId, eventId, searchText
     *  @Short Description : Get all jobs for a specific event 
     *  @return : array with code. 
     */
    public function getFilteredJobs($params) {
        $userId = $this->coreModel->cleanString($params['userId']);
        $eventId = $this->coreModel->cleanString($params['eventId']);
        $searchText = $this->coreModel->cleanString($params['searchText']);
        $id = $this->coreModel->getJwtValue($userId);

        if ($id != '' || $eventId != '' || $searchText != '') {
            $row = $this->coreModel->queryResultArray("SELECT job.id jobId, job.title title, job.description description, user.company company  FROM fj_jobs job JOIN fj_events event ON event.id = job.eventId JOIN fj_users user ON user.id = job.createdBy WHERE job.eventId = '$eventId' AND (job.title LIKE '%$searchText%' OR job.description LIKE '%$searchText%' OR user.company LIKE '%$searchText%') ORDER BY job.createdAt DESC");
            if (count($row) > 0) {
                $this->coreModel->codeMessage('200', $this->lang->line('success'));
                coreapi_model::$returnArray['eventList'] = $row;
            } else {
                $this->coreModel->codeMessage('202', $this->lang->line('no_record_found'));
            }
        } else {
            $this->coreModel->codeMessage('202', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }

    /*
     *  @Author : Synergy
     *  @Params : userId, eventId
     *  @Short Description : Get all jobs for a specific event
     *  @return : array with code. 
     */
    public function getInterviewStatusForEvent($params) {
        $userId = $this->coreModel->cleanString($params['userId']);
        $eventId = $this->coreModel->cleanString($params['eventId']);
        $id = $this->coreModel->getJwtValue($userId);

        if ($id != '') {
            $row = $this->coreModel->queryResultArray("SELECT job.id jobId, job.title title, job.description description, user.company company, (select count(ujj.id) FROM fj_userJob ujj where ujj.jobId = job.id AND ujj.status=1) AS appliedInterviews, (select count(ujjj.id) FROM fj_userJob ujjj where ujjj.jobId = job.id AND ujjj.status IN(2,4)) AS shortlistedInterviews, sum(uj.viewed) viewed  FROM fj_jobs job JOIN fj_events event ON event.id = job.eventId JOIN fj_users user ON user.id = job.createdBy LEFT JOIN fj_userJob uj ON uj.jobId = job.id WHERE job.eventId = '$eventId' GROUP BY job.eventId ORDER BY job.createdAt DESC");
            if (count($row) > 0) {
                $this->coreModel->codeMessage('200', $this->lang->line('success'));
                coreapi_model::$returnArray['eventList'] = $row;
            } else {
                $this->coreModel->codeMessage('202', $this->lang->line('no_record_found'));
            }
        } else {
            $this->coreModel->codeMessage('202', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }

    /*
     *  @Author : Wildnet
     *  @Params : null
     *  @Short Description : get User's Basic Information
     *  @return : array with code. 
     */
    //--------------------- User's Basic Information Starts -------------------//
    public function getBasicInformation_web($params) {
        $userId = $this->coreModel->cleanString($params['userId']);
        $id = $this->coreModel->getJwtValue($userId);
        if ($userId != '') {
            $row = $this->coreModel->queryRowArray("SELECT u.id,u.fullname,u.email,u.image, u.mobile,u.dob,u.pinCode,u.gender,u.resume_path,ue.companyName,uq.universityId,uq.universityName FROM fj_users u LEFT JOIN fj_user_employment ue ON ue.userId = u.id LEFT JOIN fj_userQualification uq on uq.userId = u.id WHERE u.id='$id' AND u.status='1'");
            if (count($row) > 0) {
                // Basic Information
                $basicInfo = array();
                $rowDataBasicInfo['fullname'] = (string) $row['fullname'];
                $rowDataBasicInfo['email'] = (string) $row['email'];
				$rowDataBasicInfo['image'] = (string) $row['image'];
                $rowDataBasicInfo['mobile'] = (string) $row['mobile'];
                $rowDataBasicInfo['dob'] = (string) (($row['dob'] == '' || $row['dob'] == '0000-00-00') ? '' : date('Y', strtotime($row['dob'])));
                $rowDataBasicInfo['pincode'] = (string) ($row['pinCode'] != 0 ? $row['pinCode'] : '');
                $rowDataBasicInfo['gender'] = (string) $row['gender'];
                $rowDataBasicInfo['companyName'] = (string) $row['companyName'];
                $rowDataBasicInfo['universityId'] = (string) $row['universityId'];
                $rowDataBasicInfo['universityName'] = (string) $row['universityName'];

                if(is_null($row['resume_path']) || $row['resume_path'] == '')
                    $rowDataBasicInfo['resumePath'] = "";
                else
                    $rowDataBasicInfo['resumePath'] = "https://s3.amazonaws.com/fjauditions/".$row['resume_path'];

                $rowDataBasicInfo['profilePercent'] = Userapi_model::profilePercent($id);
                $basicInfo[] = $rowDataBasicInfo;
                //--------------- Response Data ---------------//
                $this->coreModel->codeMessage('200', $this->lang->line('success'));
                coreapi_model::$returnArray['basicInfo'] = $basicInfo;
            } else {
                $this->coreModel->codeMessage('500', $this->lang->line('no_record_found'));
            }
        } else {
            $this->coreModel->codeMessage('300', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }
	
	/*
     *  @Author : Synergy
     *  @Params : userId
     *  @Short Description : v2 of get User's Other Information
     *  @return : array with code. 
     */

    public function uploadUserImage($params) {
        $userId = $this->coreModel->cleanString($params['userId']);
        $id = $this->coreModel->getJwtValue($userId);
		$image = $this->coreModel->cleanString($params['image']);
        if ($userId != '' && $image != '') {
            $row = $this->coreModel->queryRowArray("SELECT * FROM fj_users WHERE id='$id' AND status='1'");
            if (count($row) > 0) {
                // Other Information
                $binary = base64_decode($image);
                $filename = ($this->coreModel->cleanString($params['imageName']) != '' ? $params['imageName'] : $this->coreModel->cleanString($row['image']));

                if ($image != '') {
                    header('Content-Type: bitmap; charset=utf-8');
                    $file = fopen('uploads/userImages/' . $filename, 'wb');
                    fwrite($file, $binary);
                    fclose($file);
                }
				$updateData = array(
                        'image' => $filename,
                        'imageSource' => 1
                    );
                    $query = $this->db->where('id', $id)->update('fj_users', $updateData);
					$this->coreModel->codeMessage('200', $this->lang->line('success'));
            } else {
                $this->coreModel->codeMessage('500', $this->lang->line('no_record_found'));
            }
        } else {
            $this->coreModel->codeMessage('300', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }

    /*
     *  @Author : Synergy
     *  @Params : messageFirst, messageSecond.
     *  @Short Description : It is use for testing purpose
     *  @return : array with code. 
     */
    public function testApi($params) {
        $messageFirst = $this->coreModel->cleanString($params['messageFirst']);
        $messageSecond = $this->coreModel->cleanString($params['messageSecond']);

        $insertData = array('field1' => $messageFirst, 'field2' => $messageSecond);
        $result = $this->db->insert('fj_test', $insertData);

        if($result) {
          $this->coreModel->codeMessage('200', $this->lang->line('success'));
        } else {
          $this->coreModel->codeMessage('500', $this->lang->line('something_wrong'));
        }
      return coreapi_model::$returnArray;
    }

    /*
     *  @Author : Synergy
     *  @Params : messageFirst, messageSecond.
     *  @Short Description : It is use for testing purpose
     *  @return : array with code. 
     */
    public function addCronJobInfo($params) {
        $cronJobType = $this->coreModel->cleanString($params['cronJobType']);

        $insertData = array('cronJobType' => $cronJobType);
        $result = $this->db->insert('fj_cronJobInfo', $insertData);

        if($result) {
            $lastInsertedId = $this->db->insert_id();
            $this->coreModel->codeMessage('200', $this->lang->line('success'));
            coreapi_model::$returnArray['batchNo'] = $lastInsertedId;
        } else {
          $this->coreModel->codeMessage('500', $this->lang->line('something_wrong'));
        }
      return coreapi_model::$returnArray;
    }

    /*
     *  @Author : Synergy
     *  @Params : messageFirst, messageSecond.
     *  @Short Description : It is use for testing purpose
     *  @return : array with code. 
     */
    public function addCronJobDetailInfo($params) {
        $batchNo = $this->coreModel->cleanString($params['batchNo']);
        $uniqueId = $this->coreModel->cleanString($params['uniqueId']);
        $status = $this->coreModel->cleanString($params['status']);

        $insertData = array('batchNo' => $batchNo, 'uniqueId' => $uniqueId, 'status' => $status);
        $result = $this->db->insert('fj_cronJobInfoDetail', $insertData);

        if($result) {
          $this->coreModel->codeMessage('200', $this->lang->line('success'));
        } else {
          $this->coreModel->codeMessage('500', $this->lang->line('something_wrong'));
        }
      return coreapi_model::$returnArray;
    }

    /*
     *  @Author : Synergy
     *  @Params : userId, id(employmentId)
     *  @Short Description : Remove User's Selected employment information
     *  @return : array with code. 
     */
    public function checkWelcomeMessageDisplayed($params) {
        $userId = $this->coreModel->cleanString($params['userId']);
        $id = $this->coreModel->getJwtValue($userId);
        if ($userId != '') {
            $row = $this->coreModel->queryRowArray("SELECT welcomeMessageStatus FROM fj_users WHERE id='$id' AND status='1'");
            if (count($row) == '1') {
                if($row['welcomeMessageStatus'] == 0) {
                    $updateData = array('welcomeMessageStatus' => 1);
                    $this->db->where('id', $id)->update('fj_users', $updateData);
                }
                $this->coreModel->codeMessage('200', $this->lang->line('success'));
                coreapi_model::$returnArray['status'] = $row['welcomeMessageStatus'];
            } else {
                $this->coreModel->codeMessage('202', $this->lang->line('no_record_found'));
            }
        } else {
            $this->coreModel->codeMessage('202', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }
	
	
	public function isUserExists_V1($params) {
        if(isset($params['email']) && !empty($params['email'])) {
            $email = $this->coreModel->cleanString($params['email']);
        } else {
            $email = '';
        }
        //$link_id = $this->coreModel->cleanString($params['link_id']);
        @$social_networking     = $this->coreModel->cleanString($params['social_networking_type']);
        @$social_networking_id  = $this->coreModel->cleanString($params['networking_id']);
        // Check Params Keys
        if (@$social_networking != '') {
            //$social_networking =>1=fb,2=tw,3=linkdin,4=g+
            if (@$social_networking == '1') {
                $type = 'fb_id';
            }
            if (@$social_networking == '2') {
                $type = 'tw_id';
            }
            if (@$social_networking == '3') {
                $type = 'link_id';
            }
            if (@$social_networking == '4') {
                $type = 'google_id';
            }

            //echo "SELECT * FROM fj_users WHERE $type ='$social_networking_id' AND role='3'";exit;
            $row = $this->coreModel->queryRowArray("SELECT * FROM fj_users WHERE $type ='$social_networking_id' AND role='3'");

            if (count($row) > 0) {
                
                $auditionRow = $this->coreModel->queryRowArray("SELECT * FROM fj_users WHERE (id='$row[id]')");
                $this->coreModel->codeMessage('200', $this->lang->line('success'));
                coreapi_model::$returnArray['token']            = $this->coreModel->getJwtToken($row['id']);
                coreapi_model::$returnArray['userName']         = (string) $row['fullname'];
                coreapi_model::$returnArray['userEmail']        = (string) $row['email'];
                coreapi_model::$returnArray['userMobile']       = (string) $row['mobile'];
                coreapi_model::$returnArray['userImage']        = ($row['image'] != '' ? base_url() . '/uploads/userImages/' . $row['image'] . ".PNG" : '');
                coreapi_model::$returnArray['userDob']          = (string) (($row['dob'] == '' || $row['dob'] == '0000-00-00') ? '' : date('Y', strtotime($row['dob'])));
                coreapi_model::$returnArray['userGender']       = (string) $row['gender'];
                coreapi_model::$returnArray['userPincode']      = (string) ($row['pinCode'] != 0 ? $row['pinCode'] : '');
                coreapi_model::$returnArray['profilePercent']   = Userapi_model::profilePercent($row['id']);
                
            }else if($email != '') {
				$row = $this->coreModel->queryRowArray("SELECT * FROM fj_users WHERE email='$email' AND status='1' AND role='3'");
				if (count($row) > 0){
					$this->coreModel->codeMessage('200', $this->lang->line('success'));
					coreapi_model::$returnArray['token']            = $this->coreModel->getJwtToken($row['id']);
					coreapi_model::$returnArray['userName']         = (string) $row['fullname'];
					coreapi_model::$returnArray['userEmail']        = (string) $row['email'];
					coreapi_model::$returnArray['userMobile']       = (string) $row['mobile'];
					coreapi_model::$returnArray['userImage']        = ($row['image'] != '' ? base_url() . '/uploads/userImages/' . $row['image'] . ".PNG" : '');
					coreapi_model::$returnArray['userDob']          = (string) (($row['dob'] == '' || $row['dob'] == '0000-00-00') ? '' : date('Y', strtotime($row['dob'])));
					coreapi_model::$returnArray['userGender']       = (string) $row['gender'];
					coreapi_model::$returnArray['userPincode']      = (string) ($row['pinCode'] != 0 ? $row['pinCode'] : '');
					coreapi_model::$returnArray['profilePercent']   = Userapi_model::profilePercent($row['id']);
					if($type != ""){
                        coreapi_model::$data[$type] = isset($params['networking_id'])?$params['networking_id']:"";
                    }
					
                    $this->db->where_in('id', $row['id'])->update('fj_users', array($type=>$params['networking_id']));
				}else{
					$this->coreModel->codeMessage('202', $this->lang->line('invalid_user'));
				}
			}else{
                $this->coreModel->codeMessage('202', $this->lang->line('invalid_user'));
            }
            return coreapi_model::$returnArray;
            
        }else if($email != '') {
            $row = $this->coreModel->queryRowArray("SELECT id FROM fj_users WHERE email='$email' AND status='1'");
            if (count($row) > 0){
                $this->coreModel->codeMessage('200', $this->lang->line('success'));
            }else{
                $this->coreModel->codeMessage('202', $this->lang->line('invalid_user'));
            }
        } else {
            $this->coreModel->codeMessage('300', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }
	
	/*
	*
	This function Signup the user with social login and normal login
	
	
	*/
	
	public function signUp_v4($params) {
        $fullname   = $this->coreModel->cleanString($params['fullname']);
        $mobile     = substr($this->coreModel->cleanString($params['mobile']), -10);
        $password   = $this->coreModel->cleanString($params['password']);
        $email      = $this->coreModel->cleanString($params['email']);
        $deviceId   = $this->coreModel->cleanString($params['deviceId']);
        $deviceType = $this->coreModel->cleanString($params['deviceType']);
        // Check Params Keys
        $social_networking = $this->coreModel->cleanString($params['social_networking_type']);
        // Check Params Keys
        $type = "";
        if (@$social_networking != '') {
            //$social_networking =>1=fb,2=tw,3=linkdin,4=g+
            if (@$social_networking == '1') {
                $type = 'fb_id';
            }
            if (@$social_networking == '2') {
                $type = 'tw_id';
            }
            if (@$social_networking == '3') {
                $type = 'link_id';
            }
            if (@$social_networking == '4') {
                $type = 'google_id';
            }
        }
        if (
                $fullname   != '' &&
                $mobile     != '' &&
                $password   != '' &&
                $email      != ''  
        ) {
            try {
                $email_exist_check  = $this->EmailExistCheck_V1($email);
                $mobile_exist_check = $this->MobileExistCheck($mobile);
                if ($email_exist_check == TRUE) {
					$row = $this->coreModel->queryRowArray("SELECT * FROM fj_users WHERE email='$email' AND status='1'");
                    
                    $this->db->where_in('id', $row['id'])->update('fj_users', array($type=>$params['networking_id']));
					
					coreapi_model::$data['token']            = $this->coreModel->getJwtToken($row['id']);
					coreapi_model::$data['userName']         = (string) $row['fullname'];
					coreapi_model::$data['fullname']         = (string) $row['fullname'];
					coreapi_model::$data['userEmail']        = (string) $row['email'];
					coreapi_model::$data['email']            = (string) $row['email'];
					coreapi_model::$data['userMobile']       = (string) $row['mobile'];
					coreapi_model::$data['userImage']        = ($row['image'] != '' ? base_url() . '/uploads/userImages/' . $row['image'] . ".PNG" : '');
					coreapi_model::$data['userDob']          = (string) (($row['dob'] == '' || $row['dob'] == '0000-00-00') ? '' : date('Y', strtotime($row['dob'])));
					coreapi_model::$data['userGender']       = (string) $row['gender'];
					coreapi_model::$data['userPincode']      = (string) ($row['pinCode'] != 0 ? $row['pinCode'] : '');
					coreapi_model::$data['profilePercent']   = Userapi_model::profilePercent($row['id']);
					$this->coreModel->codeMessage('200', $this->lang->line('success'));
					$userData[] = coreapi_model::$data;
					coreapi_model::$returnArray['data'] = $userData;
                } else {
                    coreapi_model::$data['fullname']        = $fullname;
                    coreapi_model::$data['mobile']          = $mobile;
                    coreapi_model::$data['password']        = password_hash($password, PASSWORD_BCRYPT);
                    coreapi_model::$data['email']           = $email;
                    coreapi_model::$data['deviceType']      = $deviceType;
                    coreapi_model::$data['deviceId']        = $deviceId;
                    coreapi_model::$data['activationKey']   = md5(rand(0, 1000) . 'uniquefrasehere');
                    coreapi_model::$data['status']          = '1';
                    coreapi_model::$data['role']            = '3';
                    if($type != ""){
                        coreapi_model::$data[$type] = isset($params['networking_id'])?$params['networking_id']:"";
                    }
					
                    $create_user = $this->InsertUserInfo(coreapi_model::$data);
					
                    if ($create_user) {
                        $insert_id = $this->db->insert_id();
                        //Send Email To User
                        $this->email->from('noreply@firstjob.com', 'First Job');
                        $this->email->to(coreapi_model::$data['email']);
                        $this->email->subject('Welcome to FirstJob');
                        $url = base_url() . 'users/validate/' . coreapi_model::$data['activationKey'];
                        $message = "
                                        Dear {$fullname}
                                        <br/><br/>
                                        Congratulations, you have successfully registered on FirstJob!
                                        <br/><br/>
                                        Your registration details are as follows:
                                        <br/><br/>
                                        Registered phone number: {$mobile}
                                        <br/> 
                                        Registered email id: {$email}
                                        <br/><br/>
                                        Log on to edit your profile, update your resume, complete job interview, and more. You can log on to FirstJob web console by clicking <a href='{$base_urls}' target='_blank'>here</a> or you can also browse our services through FirstJob android app. Download the app from <a href='https://goo.gl/Le9H7b' target='_blank'>here</a>, if you haven’t done it yet.
                                        <br/><br/></br>
                                        Wishing you all the best for your job search.
                                        <br/><br/>
                                        Team FirstJob
                                        <br/><br/>
                                        Get hired Anytime, Anywhere.";
                        $this->email->message($message);
                        $this->email->set_mailtype('html');
                        $this->email->send();

                        coreapi_model::$data['token']   = $this->coreModel->getJwtToken($insert_id);
                        coreapi_model::$data['dob']     = '';
                        coreapi_model::$data['image']   = '';
                        unset(coreapi_model::$data['password']);
                        unset(coreapi_model::$data['status']);
                        unset(coreapi_model::$data['role']);
                        unset(coreapi_model::$data['activationKey']);
                        unset(coreapi_model::$data['deviceId']);
                        unset(coreapi_model::$data['deviceType']);
                        $userData[] = coreapi_model::$data;
                        $this->coreModel->codeMessage('200', $this->lang->line('success'));
                        coreapi_model::$returnArray['data'] = $userData;
                        
                        if (!empty($deviceId) && !empty($deviceType)) {
                            // remove device token from other user
                            $tokenduplicatedata = $this->coreModel->queryRowArray("SELECT GROUP_CONCAT(id) AS id FROM fj_users WHERE deviceId='" . $deviceId . "' AND id != '" . $insert_id . "'");
                            if (!empty($tokenduplicatedata['id'])) {
                                $idlist             = trim($tokenduplicatedata['id'], "'");
                                $intarray = explode(",", $idlist);                   
                                $removetokendata    = $this->db->where_in('id', $intarray)->update('fj_users', array('deviceId'=>''));
                            } 
                        }
                    } else {
                        $this->coreModel->codeMessage('500', $this->lang->line('error_occured'));
                    }
                }
            } catch (Exception $e) {
                $this->coreModel->codeMessage('500', $e->getMessage());
            }
        } else {
            $this->coreModel->codeMessage('300', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }
	
	/*
     *  @Author : Synergy
     *  @Params : array $params(userId, email, mobile)
     *  @Short Description : Get all closed invites to user on jobs to give interview. Invititation done through corporate portal or superamdin portal
     *  @return : array of data with response code. 
     */
    public function getClosedInvites($params) {
        $userId = $this->coreModel->cleanString($params['userId']);
        $id     = $this->coreModel->getJwtValue($userId);
        $where = "";
        if(!empty($params['email'])) {
          $email = $this->coreModel->cleanString($params['email']);
          $where .= " OR iu.email = '".$email."'";
        }

        if(!empty($params['mobile'])) {
          $mobile = $this->coreModel->cleanString($params['mobile']);
          $where .= " OR iu.userMobile = '".$mobile."'";
        }
        
        $cudDate = date('Y-m-d');
        $result = $this->coreModel->queryResultArray("SELECT DISTINCT iu.jobId, fju.company, iu.userId, job.fjCode, iu.createdAt invitationDate,iu.id id, job.title jobName FROM fj_jobInvitationForUser iu JOIN fj_jobs job ON job.id = iu.jobId JOIN fj_users fju ON fju.id = job.createdBy WHERE (((iu.userId = '".$id."' $where) AND iu.invitationStatus = 1) OR job.openTill <= '".$cudDate."' ) AND iu.jobId NOT IN(SELECT jobId FROM fj_userAnswers WHERE userId = $id) ORDER BY iu.createdAt DESC ");
	
        if (count($result) > 0) {
            foreach ($result as $row) {
				$inviteData['invitationId'] = getHashEID($row['id']);
                $inviteData['jobId'] = (int) $row['jobId'];
                $inviteData['fjCode'] = (string) $row['fjCode'];
                $inviteData['company'] = (string) $row['company'];
                $inviteData['jobName'] = (string) $row['jobName'];
                $inviteData['invitationDate'] = (string) $row['invitationDate'];
                coreapi_model::$data[] = $inviteData;
            }
            $this->coreModel->codeMessage('200', $this->lang->line('success'));
            coreapi_model::$returnArray['data'] = coreapi_model::$data;
        } else {
            $this->coreModel->codeMessage('202', $this->lang->line('no_record_found'));
        }
        return coreapi_model::$returnArray;
    }
	
	
	public function userVisitLog($params) {
        $ip = $this->coreModel->cleanString($params['ip']);
        $useragent = $this->coreModel->cleanString($params['useragent']);
        $page = $this->coreModel->cleanString($params['page']);
        $cookie = $this->coreModel->cleanString($params['cookie']);
        $userEmail = $this->coreModel->cleanString($params['userEmail']);
        //$row = $this->coreModel->queryRowArray("SELECT * FROM fj_uservisitlog WHERE cookie ='".$cookie."'");
		
        $data['ip']        = $ip;
		$data['useragent'] = $useragent;
		$data['page']      = $page;
		$data['cookievalue']    = $cookie;
        $data['email']    = $userEmail;
        $data['createdAt']    = date('Y-m-d H:i:s');
		
        $this->db->insert('fj_uservisitlog', $data);
		
		$this->coreModel->codeMessage('200', $this->lang->line('success'));
        return coreapi_model::$returnArray;
    }
	
	/*
     *  @Author : Synergy
     *  @Params : userId
     *  @Short Description : get User's employment information
     *  @return : array with code. 
     */
    public function getEmploymentDetails($params) {
        $userId = $this->coreModel->cleanString($params['userId']);
        $employmentId = $this->coreModel->cleanString($params['id']);
        $id = $this->coreModel->getJwtValue($userId);
        if ($userId != '') {
            $row = $this->coreModel->queryRowArray("SELECT id FROM fj_users WHERE id='$id' AND status='1'");
            if (count($row) > 0) {
                // Employment Information
                $employmentResults = $this->coreModel->queryRowArray("SELECT ue . *  FROM fj_user_employment ue JOIN fj_users u ON u.id = ue.userId WHERE ue.userId='$id' AND ue.id = $employmentId AND ue.status='1'");
                $rowDataEmploymentInfo['id'] = (string) $employmentResults['id'];
                $rowDataEmploymentInfo['userId'] = (string) $row['userId'];
                $rowDataEmploymentInfo['companyName'] = (string) $employmentResults['companyName'];
                $rowDataEmploymentInfo['dateFrom'] = (string) $employmentResults['dateFrom'];
                $rowDataEmploymentInfo['dateTo'] = (string) $employmentResults['dateTo'];
                $rowDataEmploymentInfo['roleOrPosition'] = (string) $employmentResults['roleOrPosition'];
                $rowDataEmploymentInfo['responsibilities'] = (string) $employmentResults['responsibilites'];
                $rowDataEmploymentInfo['isPresent'] = (string) $employmentResults['isPresent'];
                //--------------- Response Data ---------------//
                $this->coreModel->codeMessage('200', $this->lang->line('success'));
                coreapi_model::$returnArray['employmentInfo'] = $rowDataEmploymentInfo;
                coreapi_model::$returnArray['profilePercent'] = Userapi_model::profilePercent($id);
            } else {
                $this->coreModel->codeMessage('500', $this->lang->line('no_record_found'));
            }
        } else {
            $this->coreModel->codeMessage('300', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }
	
	public function getEducationDetails($params) {
        $userId = $this->coreModel->cleanString($params['userId']);
        $educationId = $this->coreModel->cleanString($params['id']);
        $id = $this->coreModel->getJwtValue($userId);
        if ($userId != '') {
            $row = $this->coreModel->queryRowArray("SELECT id FROM fj_users WHERE id='$id' AND status='1'");
            if (count($row) > 0) {
                // Educational Information
                $educationalInfo = array();
                $row = $this->coreModel->queryRowArray("SELECT Q . * , C.id as courseId, C.name as courseName, U.name as adminUniversityName FROM fj_userQualification Q JOIN fj_university U, fj_courses C WHERE Q.universityId = U.id AND Q.courseId=C.id AND Q.userId='$id' AND Q.id='$educationId' AND Q.status='1'");

                $rowDataEducationalInfo['id'] = (string) $row['id'];
                $rowDataEducationalInfo['courseId'] = (string) $row['courseId'];
                $rowDataEducationalInfo['courseName'] = (string) $row['courseName'];
                $rowDataEducationalInfo['universityId'] = (string) $row['universityId'];
                $rowDataEducationalInfo['universityName'] = (string) ($row['universityId'] == '287' ? $row['universityName'] : $row['adminUniversityName']);
                $rowDataEducationalInfo['institute'] = (string) $row['institute'];
                $rowDataEducationalInfo['completionYear'] = (string) $row['completionYear'];
                $rowDataEducationalInfo['percent'] = $row['percent'];

                //--------------- Response Data ---------------//
                $this->coreModel->codeMessage('200', $this->lang->line('success'));
                coreapi_model::$returnArray['educationalInfo'] = $rowDataEducationalInfo;
                coreapi_model::$returnArray['profilePercent'] = Userapi_model::profilePercent($id);
            } else {
                $this->coreModel->codeMessage('500', $this->lang->line('no_record_found'));
            }
        } else {
            $this->coreModel->codeMessage('300', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }
	
	public function getUniversityList($params) {
        //$universityName = $this->coreModel->cleanString($params['universityName']);
        $result = $this->coreModel->queryResultArray("SELECT name, id FROM fj_university");
        if (count($result) > 0) {
            foreach ($result as $row) {
                $universityData['id'] = (int) $row['id'];
                $universityData['name'] = (string) $row['name'];
                coreapi_model::$data[] = $universityData;
            }
            $this->coreModel->codeMessage('200', $this->lang->line('success'));
            coreapi_model::$returnArray['data'] = coreapi_model::$data;
        } else {
            $this->coreModel->codeMessage('202', $this->lang->line('no_record_found'));
        }
        return coreapi_model::$returnArray;
    }
	
	
	
}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Coreapi Model
 * Description : Keeps all the methods which are common and being used in all api models
 * @author Synergy
 * @createddate : Nov 1, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 15, 2017
 */
class discussionapi_model extends CI_Model {
	
	function __construct() {
        parent::__construct();        
        $this->load->model('coreapi_model', 'coreModel');
    }
    /**
     * Description : Use to check if a forum exists or not
     * Author : Synergy
     * @param int $id
     * @return array of data
     */
    function forumExists($id) {
        $forumRow    = $this->coreModel->queryRowArray("SELECT id FROM fj_discussionForum WHERE id='$id' AND status='1' LIMIT 0,1");
        return $forumRow;
    }
    
    /**
     * Description : Use to created a forum
     * Author : Synergy
     * @param array $params
     * @return array of data
     */
    function createForum($params){
        $userId = $this->coreModel->cleanString($params['userId']);
        $id     = $this->coreModel->getJwtValue($userId);
        $topic  = $this->coreModel->cleanString($params['topic']);
        if($userId!='' && $topic!='') {
            $userRow    = $this->coreModel->userExists($id);
            if(count($userRow)>0){
                $insertData = array(                        
                                    'userId'        => $id,
                                    'description'   => $topic,
                                    'type'          => '1'
                                );
                $this->db->insert('fj_discussionForum', $insertData);
                $insert_id      = $this->db->insert_id();
                if($insert_id) {
                    $this->coreModel->codeMessage('200', $this->lang->line('success'));
                }
                else {
                    $this->coreModel->codeMessage('500', $this->lang->line('something_wrong'));
                }
            }
            else {
                $this->coreModel->codeMessage('500', $this->lang->line('invalid_user'));
            }
        }
        else {
            $this->coreModel->codeMessage('500', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }
    
    /**
     * Description : Use to get all the forum list
     * Author : Synergy
     * @param array $params
     * @return array of data
     */
    function getForumList($params){
        // pagination pending
        $forumResult   = $this->coreModel->queryResultArray("
                                                            SELECT 
                                                                F.*,
                                                                U.image,
                                                                U.fullname
                                                            FROM 
                                                                fj_discussionForum F
                                                            JOIN
                                                                fj_users   U
                                                            WHERE
                                                                F.userId=U.id   AND  
                                                                F.status='1'    AND 
                                                                F.type='1' 
                                                            ORDER BY
                                                                createdAt DESC
                                                            ");
        if(count($forumResult)>0) {
            foreach($forumResult as $forumRow) {
				//
                // Forum Details
				$getCountForum = $this->coreModel->queryResultArray("SELECT *  FROM `fj_discussionForum` WHERE type='2' and status='1' and `discussionReplyId` = ".$forumRow['id']." order by createdAt Desc");
				
                $rowData['forumId']     = (string)$forumRow['id'];
                $rowData['topic']       = (string)ucwords($this->coreModel->limit_text($forumRow['description'], 20));
                $rowData['username']    = (string)ucwords($forumRow['fullname']);
                $rowData['reply']    	=  $getCountForum;
                $rowData['createdAt']   = $forumRow['createdAt'];
                $rowData['thumbnail']   = (string)($forumRow['image']!=''?base_url() . '/uploads/userImages/' . $forumRow['image'].".PNG":'');
                coreapi_model::$data[]  = $rowData;
            }
            $this->coreModel->codeMessage('200', $this->lang->line('success'));
            coreapi_model::$returnArray['data']    = coreapi_model::$data;                
        }
        else {
            $this->coreModel->codeMessage('500', $this->lang->line('no_forum_found'));
        }
        return coreapi_model::$returnArray;
    }
    
    /**
     * Description : Use to get details of a particular forum
     * Author : Synergy
     * @param array $params
     * @return array of data
     */
    function getForumDetail($params){
        $forumId    = $this->coreModel->cleanString($params['forumId']);
        $sortby    = $this->coreModel->cleanString($params['sortby']);
		
		$forumResultCreater   = $this->coreModel->queryResultArray("
                                                            SELECT 
                                                                F.*,
                                                                U.image,
                                                                U.fullname
                                                            FROM 
                                                                fj_discussionForum F
                                                            JOIN
                                                                fj_users   U
                                                            WHERE
                                                                F.userId=U.id   AND  
                                                                F.status='1'    AND 
                                                                F.type='1'  AND 
																F.id='$forumId'
                                                            ORDER BY
                                                                createdAt DESC
                                                            ");
		if(count($forumResultCreater) > 0){
			foreach($forumResultCreater as $forumRowUser) {
				// Forum Details
				$rowData['createdBy']     = (string)ucwords($forumRowUser['fullname']);
				$rowData['heading']       = (string)ucwords($this->coreModel->limit_text($forumRowUser['description'], 20));
				
			}
		}
		
        if($forumId!='') {
            $forumResult   = $this->coreModel->queryResultArray("
                                                        SELECT 
                                                            F.*,
                                                            U.image,
                                                            U.fullname
                                                        FROM 
                                                            fj_discussionForum F
                                                        JOIN
                                                            fj_users   U
                                                        WHERE
                                                            F.userId=U.id   AND  
                                                            F.status='1'    AND 
                                                            (F.id='$forumId'       OR  F.discussionReplyId='$forumId')
                                                        ORDER BY
                                                            F.id ".$sortby."
                                                    ");
            if(count($forumResult)>0) {
                foreach($forumResult as $forumRow) {
                    // Forum Details
                    $rowData['forumId']     = (string)$forumRow['id'];
                    $rowData['topic']       = (string)ucwords($forumRow['description']);
                    $rowData['username']    = (string)ucwords($forumRow['fullname']);
                    $rowData['createdAt']    = (string)ucwords($forumRow['createdAt']);
                    $rowData['thumbnail']   = (string)($forumRow['image']!=''?base_url() . '/uploads/userImages/' . $forumRow['image'].".PNG":'');
                    coreapi_model::$data[]  = $rowData;
                }         
                $this->coreModel->codeMessage('200', $this->lang->line('success'));
                coreapi_model::$returnArray['data']    = coreapi_model::$data;                
            }
            else {
                $this->coreModel->codeMessage('500', $this->lang->line('invalid_forum'));
            }
        }
        else {
            $this->coreModel->codeMessage('500', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }
    
    /**
     * Description : Use to reply on a specific forum
     * Author : Synergy
     * @param array $params
     * @return array of data
     */
    function replyForum($params){
        $userId     = $this->coreModel->cleanString($params['userId']);
        $id         = $this->coreModel->getJwtValue($userId);
        $forumId    = $this->coreModel->cleanString($params['forumId']);
        $message    = $this->coreModel->cleanString($params['message']);
        if($userId!='' && $forumId!='' && $message!='') {
            $userRow    = $this->coreModel->userExists($id);
            if(count($userRow)>0){
                $forumRow    = $this->forumExists($forumId);
                if(count($forumRow)>0){
                    $insertData = array(                        
                                        'userId'            => $id,
                                        'description'       => $message,
                                        'type'              => '2',
                                        'discussionReplyId' => $forumId
                                    );
                    $this->db->insert('fj_discussionForum', $insertData);
                    $insert_id      = $this->db->insert_id();
                    if($insert_id) {
                        $this->coreModel->codeMessage('200', $this->lang->line('success'));
                    }
                    else {
                        $this->coreModel->codeMessage('500', $this->lang->line('something_wrong'));
                    }
                }
                else {
                    $this->coreModel->codeMessage('500', $this->lang->line('something_wrong'));
                }
            }
            else {
                $this->coreModel->codeMessage('500', $this->lang->line('invalid_user'));
            }
        }
        else {
            $this->coreModel->codeMessage('500', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Usertracker Model
 * Description : Handle the tracking of users
 * @author Synergy
 * @createddate : Dec 10, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 17, 2017
 */
class usertracker_model extends CI_Model {
    /**
     * Description : Use to tracking details
     * Author : Synergy
     * @param int $userID, string $lat, string $long
     * @return boolean
     */
    function getTrackingDetails($userID='', $lat='', $long=''){
        $insertData =   array(                        
                            $userId                 => $userID,
                            $eventPage              => $_SERVER['REQUEST_URI'],
                            $latitude               => $lat,
                            $longitude              => $long,
                            $ipAddress              => $_SERVER['REMOTE_ADDR'],
                            $userBrowsingPlatform   => $_SERVER['HTTP_USER_AGENT']
                        );
        $this->db->insert('fj_userTracking', $insertData);
    }
}
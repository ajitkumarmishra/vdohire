<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Auditionapi Model
 * Description : Handle all the CRUD operation for Audition
 * @author Synergy
 * @createddate : July 25, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 16, 2017
 */
class auditionapi_model extends CI_Model {
    
    /**
     * Description : Return total auditions given by app user
     * Author : Synergy
     * @param array $params
     * @return array of data 
     */
	 
	function __construct() {
        parent::__construct();        
        $this->load->model('coreapi_model', 'coreModel');
    }
	
    function auditionHistory($params){
        $userId = $this->coreModel->cleanString($params['userId']);
        $id     = $this->coreModel->getJwtValue($userId);
        if($userId!='') {
            $auditionResult = $this->coreModel->queryResultArray("SELECT id, status, audition_file,auditionFiles FROM fj_userAudition WHERE userId='$id' AND status IN (1,2) ORDER BY status ASC");
            if(count($auditionResult)>0){
                foreach($auditionResult as $auditionRow){
                    $rowData['id']              = (string)$auditionRow['id'];
                    $rowData['auditionAnsFiles']   = (string)$auditionRow['auditionFiles'];
                    $rowData['source']   = 1;
                    $rowData['status']          = (string)($auditionRow['status']=='1'?'Active':'Inactive');
                    $rowData['audition_file']   = (string)(base_url() . '/uploads/audition/video/'.substr_replace($auditionRow['audition_file'] , 'mp4', strrpos($auditionRow['audition_file'] , '.') +1));
                    $rowData['auditionUrl']     = (string)(base_url() . '/page/getAudition/'.encryptURLparam($auditionRow['id'], URLparamKey));
                    coreapi_model::$data[]  = $rowData;
                }
                $this->coreModel->codeMessage('200', $this->lang->line('success'));
                coreapi_model::$returnArray['data']    = coreapi_model::$data;
            }
            else {
                $this->coreModel->codeMessage('500', $this->lang->line('no_audition_found'));
                coreapi_model::$returnArray['data']    = array();
            }
        }
        else {
            $this->coreModel->codeMessage('500', $this->lang->line('missing_required_fields'));
            coreapi_model::$returnArray['data']    = array();
        }
        return coreapi_model::$returnArray;
    }

    /**
     * Description : Use to update all audition status 2 i.e inactive of $params['userId'] and do active $params['auditionId'] audition
     * Author : Synergy
     * @param array $params
     * @return array of data 
     */
    function auditionUpdate($params){
        $userId     = $this->coreModel->cleanString($params['userId']);
        $id         = $this->coreModel->getJwtValue($userId);
        $auditionId = $this->coreModel->cleanString($params['auditionId']);
        if($userId!='') {
            $auditionResult = $this->coreModel->queryResultArray("SELECT id, status, audition_file FROM fj_userAudition WHERE userId='$id' AND id='$auditionId' ");
            if(count($auditionResult)>0){
                $this->db->where(array('userId'=>$id))->update('fj_userAudition', array('status'=>'2'));
                $this->db->where(array('userId'=>$id, 'id'=>$auditionId))->update('fj_userAudition', array('status'=>'1'));            
                $this->coreModel->codeMessage('200', $this->lang->line('success'));
            }
            else {
                $this->coreModel->codeMessage('500', $this->lang->line('no_audition_found'));
            }
        }
        else {
            $this->coreModel->codeMessage('500', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }

    /**
     * Description : Use to delete an audition of specific user
     * Author : Synergy
     * @param array $params
     * @return array of data 
     */
    function auditionDelete($params){
        $userId     = $this->coreModel->cleanString($params['userId']);
        $id         = $this->coreModel->getJwtValue($userId);
        $auditionId = $this->coreModel->cleanString($params['auditionId']);
        if($userId!='') {
            $this->db->where(array('userId'=>$id, 'id'=>$auditionId))->update('fj_userAudition', array('status'=>'3'));            
            $this->coreModel->codeMessage('200', $this->lang->line('success'));
        }
        else {
            $this->coreModel->codeMessage('500', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }

    /**
     * Description : Use to get audition sets
     * Author : Synergy
     * @param array $params
     * @return array of data 
     */
    function auditionSet($params){
        $userId     = $this->coreModel->cleanString($params['userId']);
        $id         = $this->coreModel->getJwtValue($userId);
        $industryId = $this->coreModel->cleanString($params['industryId']);
        $functionId = $this->coreModel->cleanString($params['functionId']);
        if($userId!='' && $industryId!='' && $functionId!='') {
            $query = "
                    SELECT 
                        aSet.name, aQ.title, aQ.duration, aQ.id, aSet.id AS setId
                    FROM 
                        fj_auditionSet          aSet
                    JOIN 
                        fj_auditionSetQuestions aSetQ,
                        fj_auditionQuestions    aQ
                    WHERE 
                        aSet.status='1'                 AND
                        aSet.id=aSetQ.auditionId        AND
                        aSetQ.questionId=aQ.id          AND
                        aSet.industryId='$industryId'   AND
                        aSet.functionId='$functionId' ";
            $auditionResult = $this->coreModel->queryResultArray($query);
            if(count($auditionResult)>0){
                foreach($auditionResult as $auditionRow){
                    $rowData['auditionSetId']   = (string)$auditionRow['setId'];
                    $rowData['auditionSet']     = (string)$auditionRow['name'];
                    coreapi_model::$data[]  = $rowData;
                }
                $this->coreModel->codeMessage('200', $this->lang->line('success'));
                coreapi_model::$returnArray['data']    = coreapi_model::$data;
            }
            else {
                $this->coreModel->codeMessage('500', $this->lang->line('no_audition_found'));
            }
        }
        else {
            $this->coreModel->codeMessage('500', $this->lang->line('login_required'));
        }
        return coreapi_model::$returnArray;
    }
	
	function auditionSetQuestions($params){
        $userId     = $this->coreModel->cleanString($params['userId']);
        $id         = $this->coreModel->getJwtValue($userId);
        $industryId = $this->coreModel->cleanString($params['industryId']);
        if($userId!='' && $industryId!='') {
            $query = "
                    SELECT * FROM `fj_auditionSetQuestions` WHERE `auditionId` = '$industryId'   AND
                        status='1'";
            $auditionResult = $this->coreModel->queryResultArray($query);
			//echo "<pre>";print_r($auditionResult);die;
            if(count($auditionResult)>0){
                foreach($auditionResult as $auditionRow){
					//echo "<pre>";print_r();die;
                    $rowData['questionID']   = $auditionRow['questionId'];
                    $rowData['setID']     = $industryId;
                    coreapi_model::$data[]  = $rowData;
                }
                $this->coreModel->codeMessage('200', $this->lang->line('success'));
                coreapi_model::$returnArray['data']    = coreapi_model::$data;
            }
            else {
                $this->coreModel->codeMessage('500', $this->lang->line('no_audition_found'));
            }
        }
        else {
            $this->coreModel->codeMessage('500', $this->lang->line('login_required'));
        }
        return coreapi_model::$returnArray;
    }

    /**
     * Description : Use to get audition sets inside a function of industry
     * Author : Synergy
     * @param array $params
     * @return array of data 
     */
	function auditionQuestion($params){
        $userId     = $this->coreModel->cleanString($params['userId']);
        $id         = $this->coreModel->getJwtValue($userId);
        $questionId = $this->coreModel->cleanString($params['questionId']);
        if($userId!='' && $questionId!='') {
            $query = "
                    SELECT * FROM `fj_auditionQuestions` WHERE `id` = '$questionId'   AND
                        status='1'";
            $auditionResult = $this->coreModel->queryResultArray($query);
			//echo "<pre>";print_r($auditionResult);die;
            if(count($auditionResult)>0){
                foreach($auditionResult as $auditionRow){
					//echo "<pre>";print_r();die;
                    $rowData['questionID']   = $auditionRow['id'];
                    $rowData['title']   = $auditionRow['title'];
                    $rowData['duration']   = 30;
                    //$rowData['setID']     = $industryId;
                    coreapi_model::$data[]  = $rowData;
                }
                $this->coreModel->codeMessage('200', $this->lang->line('success'));
                coreapi_model::$returnArray['data']    = coreapi_model::$data;
            }
            else {
                $this->coreModel->codeMessage('500', $this->lang->line('no_audition_found'));
            }
        }
        else {
            $this->coreModel->codeMessage('500', $this->lang->line('login_required'));
        }
        return coreapi_model::$returnArray;
    }
	
    function auditionSetData($params){
        $userId         = $this->coreModel->cleanString($params['userId']);
        $id             = $this->coreModel->getJwtValue($userId);
        $industryId     = $this->coreModel->cleanString($params['industryId']);
        $functionId     = $this->coreModel->cleanString($params['functionId']);
        $auditionSetId  = $this->coreModel->cleanString($params['setId']);
        if($userId!='') {
            $query = "
                    SELECT 
                        aSet.name, aQ.title, aQ.duration, aQ.id, aSet.id AS setId
                    FROM 
                        fj_auditionSet          aSet
                    JOIN 
                        fj_auditionSetQuestions aSetQ,
                        fj_auditionQuestions    aQ
                    WHERE 
                        aSet.status='1'                 AND
                        aSet.id=aSetQ.auditionId        AND
                        aSetQ.questionId=aQ.id          AND
                        aSet.industryId='$industryId'  AND
                        aSet.functionId='$functionId'  AND
                        aSet.id='$auditionSetId' ";
            $auditionResult = $this->coreModel->queryResultArray($query);
            if(count($auditionResult)>0){
                foreach($auditionResult as $auditionRow){
                    $rowData['auditionSetId']   = (string)$auditionRow['setId'];
                    $rowData['auditionSet']     = (string)$auditionRow['name'];
                    $rowData['questionId']      = (string)$auditionRow['id'];
                    $rowData['question']        = (string)$auditionRow['title'];
                    $rowData['duration']        = (string)$auditionRow['duration'];
                    coreapi_model::$data[]  = $rowData;
                }
                $this->coreModel->codeMessage('200', $this->lang->line('success'));
                coreapi_model::$returnArray['data']    = coreapi_model::$data;
            }
            else {
                $this->coreModel->codeMessage('500', $this->lang->line('no_audition_found'));
            }
        }
        else {
            $this->coreModel->codeMessage('500', $this->lang->line('login_required'));
        }
        return coreapi_model::$returnArray;
    }

    /**
     * Description : Use to get all auditions with its details like question, duration, audition type 
     * Author : Synergy
     * @param array $params
     * @return array of data 
     */
    function auditionData($params){
        $userId         = $this->coreModel->cleanString($params['userId']);
        $id             = $this->coreModel->getJwtValue($userId);
        if($userId!='') {
            $query = "
                    SELECT 
                        aSet.name, aQ.title, aQ.duration, aQ.id, aSet.id AS setId
                    FROM 
                        fj_auditionSet          aSet
                    JOIN 
                        fj_auditionSetQuestions aSetQ,
                        fj_auditionQuestions    aQ
                    WHERE 
                        aSet.status='1'                 AND
                        aSet.id=aSetQ.auditionId        AND
                        aSetQ.questionId=aQ.id          ";
            $auditionResult = $this->coreModel->queryResultArray($query);
            if(count($auditionResult)>0){
                foreach($auditionResult as $auditionRow){
                    $rowData['auditionSetId']   = (string)$auditionRow['setId'];
                    $rowData['auditionSet']     = (string)str_replace(array(' ',',','!','~','`','@','#','$','%','^','&','*','(',')','_','-','+','=','{','[',']','}','|','\\','"','\'',';',':','<','>','?','/','.',','),'',$auditionRow['name']);
                    $rowData['questionId']      = (string)$auditionRow['id'];
                    $rowData['question']        = (string)$auditionRow['title'];
                    $rowData['duration']        = (string)$auditionRow['duration'];
                    $rowData['auditionSetType'] = 'text';
                    coreapi_model::$data[]  = $rowData;
                }
                $this->coreModel->codeMessage('200', $this->lang->line('success'));
                coreapi_model::$returnArray['data']    = coreapi_model::$data;
            }
            else {
                $this->coreModel->codeMessage('500', $this->lang->line('no_audition_found'));
            }
        }
        else {
            $this->coreModel->codeMessage('500', $this->lang->line('login_required'));
        }
        return coreapi_model::$returnArray;
    }

    /**
     * Description : Use to upload entire audition file in one attempt given by specific user
     * Author : Wildnet
     * @param array $params
     * @return array of data 
     */
    function addAuditionFile($posts, $files){
        if(coreapi_model::array_keys_exist( $posts, 'userId', 'token', 'auditionFileArray', 'auditionFileName')  &&  ($posts['userId']!='') && ($posts['token']!='')) {            
            $userId             = $this->coreModel->cleanString($posts['userId']);
            $auditionNumber     = $this->coreModel->cleanString($posts['auditionNumber']);
            $id                 = $this->coreModel->getJwtValue($userId);
            $auditionFileArray  = $this->coreModel->cleanString($posts['auditionFileArray']);
            $auditionFile       = $files['auditionFile'];
            $auditionFileName   = $this->coreModel->cleanString($posts['auditionFileName']);
            
            $dir_path_video         = "uploads/audition/video/";
            $dir_path_temp_video    = "uploads/audition/tempVideo/";
            
            if($posts['token']=='ecbcd7eaee29848978134beeecdfbc7c') {
                $userRow    = $this->coreModel->userExists($id);
                if(count($userRow)>0) {   
                    
                    if($auditionFile['error']===0) {
                        $target_path    = $dir_path_temp_video . $auditionFileName;
                        try {
                            $s3 = new S3(awsAccessKey, awsSecretKey);
                            $s3->putBucket($bucket, S3::ACL_PUBLIC_READ);
                            //Rename image name.
                            $tmp                = $auditionFile['tmp_name'];
                            $actual_image_name  = $auditionFileName;
                            $bucket             = "fjauditions";

                            if ($s3->putObjectFile($tmp, $bucket, $actual_image_name, S3::ACL_PUBLIC_READ)) {
                                $auditionData = array(
                                                        'userId'        => (int)$id,
                                                        'auditionFiles' => (string)$auditionFileArray,
                                                        'auditionNumber'=> (string)$auditionNumber,
                                                        'createdAt'     => date('Y-m-d H:i:s'),
                                                        'status'        => '1'
                                                    );
                                
                                $auditionResult = $this->coreModel->queryResultArray("SELECT id FROM fj_userAudition WHERE userId='$id' ORDER BY status ASC LIMIT 0,1");
                                if(count($auditionResult)>0){ 
                                    $this->db->where(array('userId'=>$id))->update('fj_userAudition', $auditionData);
                                    $this->coreModel->codeMessage('200', $this->lang->line('success'));
                                }
                                else {
                                    $this->db->insert('fj_userAudition', $auditionData);
                                    $insert_id  = $this->db->insert_id();
                                    if($insert_id) {
                                        $this->coreModel->codeMessage('200', $this->lang->line('success'));
                                    }
                                    else {
                                        $this->coreModel->codeMessage('500', $this->lang->line('something_wrong'));
                                    } 
                                } 
                            }
                            else {
                                $this->coreModel->codeMessage('501', $this->lang->line('something_wrong'));
                            }
                        } 
                        catch (Exception $e) {
                                $this->coreModel->codeMessage('502', $e->getMessage());
                        }
                    }
                    else {
                        $this->coreModel->codeMessage('503', $this->lang->line('invalid_audition_file'));
                    }                                
                }       
                else {
                    $this->coreModel->codeMessage('504', $this->lang->line('invalid_user'));
                }
            }
            else {
                $this->coreModel->codeMessage('505', $this->lang->line('invalid_token'));
            }
        }
        else {
            $this->coreModel->codeMessage('202', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }

    /**
     * Description : Use to provide all auditions given by a specifc user
     * Author : Wildnet
     * @param array $params
     * @return array of data 
     */
    function createAuditionFile($posts){
        if(coreapi_model::array_keys_exist( $posts, 'userId', 'token')  &&  ($posts['userId']!='') && ($posts['token']!='')) {  
            $userId = $this->coreModel->cleanString($posts['userId']);
            $id     = $this->coreModel->getJwtValue($userId);            
            if($posts['token']=='ecbcd7eaee29848978134beeecdfbc7c') {
                $userRow    = $this->coreModel->userExists($id);
                if(count($userRow)>0) {
                    $auditionData   = array('audition_file' => (string)$auditionFileArray);                    
                    $auditionResult = coreapi_model::queryRowArray("SELECT * FROM fj_userAudition WHERE userId='$id' ORDER BY status ASC LIMIT 0,1");
                    if(count($auditionResult)>0){
                        $rowData['id']          = (string)$auditionResult['id'];
                        $rowData['auditionUrl'] = (string)(base_url() . '/page/getAudition/'.encryptURLparam($auditionResult['id'], URLparamKey));
                        coreapi_model::$data[]  = $rowData;
                        $this->coreModel->codeMessage('200', $this->lang->line('success'));
                        coreapi_model::$returnArray['data'] = coreapi_model::$data;
                    }
                    else {
                        $this->coreModel->codeMessage('500', $this->lang->line('something_wrong'));
                    }                                     
                }       
                else {
                    $this->coreModel->codeMessage('500', $this->lang->line('invalid_user'));
                }
            }
            else {
                $this->coreModel->codeMessage('300', $this->lang->line('invalid_token'));
            }
        }
        else {
            $this->coreModel->codeMessage('202', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }

    /**
     * Description : Use to upload audition file in chunks (i.e in base64 and move on s3 bucket) given by specific user
     * Author : Synergy
     * @param array $params
     * @return array of data 
     */
    function addAuditionFile_v2($posts){
		
        if($this->coreModel->array_keys_exist( $posts, 'userId', 'token', 'auditionFileArray', 'auditionFileName')  &&  ($posts['userId']!='') && ($posts['token']!='')) {

            $userId             = $this->coreModel->cleanString($posts['userId']);
            $auditionNumber     = $this->coreModel->cleanString($posts['auditionNumber']);
            $id                 = $this->coreModel->getJwtValue($userId);
            $auditionFileArray  = $this->coreModel->cleanString($posts['auditionFileArray']);
            $fileFailChunk      = $this->coreModel->cleanString($posts['fileFailChunk']);
            $isLastChunk        = $this->coreModel->cleanString($posts['isLastChunk']);
            $auditionFile       = $posts['auditionFile'];
            $auditionFileName   = $this->coreModel->cleanString($posts['auditionFileName']);
			$bucket = 'fjauditions';
            $dir_path_temp_video    = "uploads/";

            if($posts['token']=='ecbcd7eaee29848978134beeecdfbc7c') {
                $userRow    = $this->coreModel->userExists($id);
                if(count($userRow)>0) {
                    if ($auditionFile) {
                        $target_path = $dir_path_temp_video . $auditionFileName;
                        $copy_target_path = $dir_path_temp_video.'copy-'.$auditionFileName;
                        $fileData = base64_decode($auditionFile);

                        $auditionData = array(
                            'userId'        => (int)$id,
                            'auditionFiles' => (string)$auditionFileArray,
                            'auditionNumber'=> (string)$auditionNumber,
                            'createdAt'     => date('Y-m-d H:i:s'),
                            'updatedAt'     => date('Y-m-d H:i:s'),
                            'createdBy'     => $id,
                            'updatedBy'     => $id,
                            'status'        => '1'
                        );
                        $auditionUpdateData = array(
                            'userId'        => (int)$id,
                            'auditionFiles' => (string)$auditionFileArray,
                            'auditionNumber'=> (string)$auditionNumber,
                            'updatedBy'     => $id,
                            'updatedAt'     => date('Y-m-d H:i:s'),
                            'status'        => '1'
                        );

                        $auditionResult = $this->coreModel->queryResultArray("SELECT id FROM fj_userAudition WHERE userId='$id' ORDER BY status ASC LIMIT 0,1");

                        if (file_exists($target_path)) {
                            copy($target_path, $copy_target_path);

                            if($fileFailChunk == 1) {
                                //unlink($target_path);
                                if(count($auditionResult)>0){
                                    $this->db->where(array('userId'=>$id))->update('fj_userAudition', $auditionUpdateData);
                                    if($this->db->affected_rows() > 0){
                                        //unlink($copy_target_path);
                                        $fid = file_put_contents($target_path, $fileData);
                                        if($fid) {
                                            $this->coreModel->codeMessage('200', $this->lang->line('success'));
                                        } else {
                                            $this->coreModel->codeMessage('503', $this->lang->line('something_wrong'));
                                        }
                                    }else{
                                        rename($copy_target_path, $target_path);
                                        $this->coreModel->codeMessage('503', $this->lang->line('something_wrong'));
                                    }
                                }
                                else {
                                    $this->db->insert('fj_userAudition', $auditionData);
                                    $insert_id  = $this->db->insert_id();
                                    if($insert_id) {
                                        //unlink($copy_target_path);
                                        $this->coreModel->codeMessage('200', $this->lang->line('success'));
                                    }
                                    else {
                                        //unlink($target_path);
                                        rename($copy_target_path, $target_path);
                                        $this->coreModel->codeMessage('503', $this->lang->line('something_wrong'));
                                    }
                                }
                            } else {
                                if (!$file = fopen($target_path, 'a')) {
                                    $this->coreModel->codeMessage('503', $this->lang->line('something_wrong'));
                                } else {
                                    if (fwrite($file, $fileData) === FALSE) {
                                        $this->coreModel->codeMessage('503', $this->lang->line('something_wrong'));
                                    } else {
                                        fclose($file);
                                        if(count($auditionResult)>0){
                                            $this->db->where(array('userId'=>$id))->update('fj_userAudition', $auditionUpdateData);
                                            if($this->db->affected_rows() > 0){
                                                //unlink($copy_target_path);
                                                $this->coreModel->codeMessage('200', $this->lang->line('success'));
                                            }else{
                                                //unlink($target_path);
                                                rename($copy_target_path, $target_path);
                                                $this->coreModel->codeMessage('503', $this->lang->line('something_wrong'));
                                            }
                                        }
                                        else {
                                            $this->db->insert('fj_userAudition', $auditionData);
                                            $insert_id  = $this->db->insert_id();
                                            if($insert_id) {
                                                //unlink($copy_target_path);
                                                $this->coreModel->codeMessage('200', $this->lang->line('success'));
                                            }
                                            else {
                                                //unlink($target_path);
                                                rename($copy_target_path, $target_path);
                                                $this->coreModel->codeMessage('503', $this->lang->line('something_wrong'));
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
							
                            $fid = file_put_contents($target_path, $fileData);
							
                            if($fid) {
                                if(count($auditionResult)>0){
                                    $this->db->where(array('userId'=>$id))->update('fj_userAudition', $auditionUpdateData);
 
                                    if($this->db->affected_rows() > 0){
                                        //unlink($copy_target_path);
                                        $this->coreModel->codeMessage('200', $this->lang->line('success'));
                                    }else{
                                        //unlink($target_path);
                                        $this->coreModel->codeMessage('503', $this->lang->line('something_wrong'));
                                    }
                                }
                                else {
                                    $this->db->insert('fj_userAudition', $auditionData);
                                    $insert_id  = $this->db->insert_id();
                                    if($insert_id) {
                                        //unlink($copy_target_path);
                                        $this->coreModel->codeMessage('200', $this->lang->line('success'));
                                    }
                                    else {
                                        //unlink($target_path);
                                        rename($copy_target_path, $target_path);
                                        $this->coreModel->codeMessage('503', $this->lang->line('something_wrong'));
                                    }
                                }
                            } else {
                                $this->coreModel->codeMessage('503', $this->lang->line('something_wrong'));
                            }
                        }
						
                        if($isLastChunk == 1) {

                            $s3 = new S3('AKIAJY7WPI2VG2OQ4GGA', 'usHqZSFHZ8Cr20Ms0xjloEIi6iMS6YfEXE5MagUC');
							$tmp                = $target_path;
                            $actual_image_name  = $auditionFileName;
                            $bucket             = "fjauditions";
							
                            $s3->putBucket($bucket, S3::ACL_PUBLIC_READ);
							
                            //Rename image name.
                            
							
                            if($s3->putObjectFile($tmp, $bucket, baseName($actual_image_name), S3::ACL_PUBLIC_READ)) {
                                //unlink($target_path);
                            }
							
                        }
                    }
                    else {
                        $this->coreModel->codeMessage('202', $this->lang->line('invalid_audition_file'));
                    }                               
                }
                else {
                    $this->coreModel->codeMessage('500', $this->lang->line('invalid_user'));
                }
            }
            else {
                $this->coreModel->codeMessage('300', $this->lang->line('invalid_token'));
            }
        }
        else {
            $this->coreModel->codeMessage('202', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }

    /**
     * Description : Use to insert a row whenever user open the app or close the app
     * Author : Synergy
     * @param array $params
     * @return array of data 
     */
    function LogAppEvent($posts){
        if(coreapi_model::array_keys_exist( $posts, 'userId', 'token')  &&  ($posts['userId']!='') && ($posts['token']!='')) {
            $userId     = $this->coreModel->cleanString($posts['userId']);
            $id         = $this->coreModel->getJwtValue($userId);
            $deviceId         = $this->coreModel->cleanString($posts['deviceId']);
            $appStart         = $this->coreModel->cleanString($posts['appStart']);

            if($posts['token']=='ecbcd7eaee29848978134beeecdfbc7c') {
                $userRow    = $this->coreModel->userExists($id);
                if(count($userRow)>0) {
                    $insertData = array(
                        'userId'        => (int)$id,
                        'deviceId' => (string)$deviceId,
                        'appStart'=> (string)$appStart
                    );             
                    $this->db->insert('fj_logappevent', $insertData);
                    $insert_id  = $this->db->insert_id();
                    if($insert_id){
                        $this->coreModel->codeMessage('200', $this->lang->line('success'));
                    }
                    else {
                        $this->coreModel->codeMessage('500', $this->lang->line('something_wrong'));
                    }                                     
                }      
                else {
                    $this->coreModel->codeMessage('500', $this->lang->line('invalid_user'));
                }
            }
            else {
                $this->coreModel->codeMessage('300', $this->lang->line('invalid_token'));
            }
        }
        else {
            $this->coreModel->codeMessage('202', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }
}
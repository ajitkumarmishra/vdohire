<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Otp Model
 * Description : Handle all the opt related functions
 * @author Synergy
 * @createddate : Nov 25, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 17, 2017
 */
class Otp_model extends CI_Model {
    
    /**
     * Declaring private variabls
     */
    private $host ;
    private $feedId;
    private $username;
    private $password;
    private $url;
    private $message;
    
    /**
     * Responsable for initializing host credentials of message third party api
     * @return void
     */
    public function __construct() {
        parent::__construct();        
        $this->host     = 'http://bulkpush.mytoday.com/BulkSms/SingleMsgApi';
        $this->feedId   = '358805';
        $this->username = '9958412749';
        $this->password = 'atmaa';
    }
    
    /**
     * Description : Use to send message on specific mobile number
     * Author : Synergy
     * @param string $to, string $otpcode
     * @return boolean or error messge or curl json data
     */
    public function sendMessage($to, $otpcode){
        $this->message  = urlencode($otpcode.' is your FirstJob verification code.');
        $this->url      = $this->host.'?feedid='.$this->feedId.'&username='.$this->username.'&password='.$this->password.'&To=91'.$to.'&Text='.$this->message.'&senderid=91'.$to;
        
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $this->url);
        curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.73 Safari/537.36"); 
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 300); 
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 300);
        // Execute Curl
        $response = curl_exec($curl);
                        
        if (curl_errno($curl) ) {
            $result = 'ERROR -> ' . curl_errno($curl) . ': ' . curl_error($curl);
            echo $result;
            curl_close($curl); // close cURL handler
            exit;
        }
        else {
            if (empty($response)) {
                $result = 'ERROR -> ' . curl_errno($curl) . ': ' . curl_error($curl);
                echo $result;
                curl_close($curl); exit;
            } 
            else {
                $returnCode = (int)curl_getinfo($curl, CURLINFO_HTTP_CODE);
                if (empty($returnCode)) {
                     die("No HTTP code was returned");
                } 
                else {    
                    // echo results
                    if($returnCode==200) {
                        $xml = simplexml_load_string($response);
                        foreach ($xml as $key) {
                            if($key->ERROR){
                                //foreach ($key->ERROR as $errorVal){
                                    //echo $errorVal->CODE[0];
                                //}
                                return FALSE;
                            }
                            else {
                                //$key['SUBMITDATE'];
                                //$key['TID'];
                                return TRUE;
                            }
                        }
                    }

                }
            }
        }
        //Close the handle
        curl_close($curl);
    }
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Tickerapi Model
 * Description : Handle all the CRUD operation for Ticker API
 * @author Synergy
 * @createddate : Dec 25, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 17, 2017
 */
class tickerapi_model extends CI_Model {
	
	
	function __construct() {
        parent::__construct();        
        $this->load->model('coreapi_model', 'coreModel');
    }
	
    /**
     * Description : Use to get top one ticker details randmonly
     * Author : Synergy
     * @param array $params
     * @return array of data 
     */
    function getTopTickers($params){
        //$tickerResult   = $this->coreModel->queryResultArray("SELECT * FROM fj_ticker WHERE  tickerEndDate >= NOW() AND  tickerStartDate <= NOW() ORDER BY RAND() LIMIT 0,1");
        $tickerResult   = $this->coreModel->queryResultArray("SELECT * FROM fj_ticker ORDER BY createdAt DESC, RAND() LIMIT 0,1");
        if(count($tickerResult)>0) {
            foreach($tickerResult as $tickerRow) {
                // Ticker Details
                $rowData['tickerId']    = (string)$tickerRow['tickerId'];
                $rowData['title']       = (string)$tickerRow['tickerTitle'];
                $rowData['description']       = strip_tags($tickerRow['tickerDesc']);
                $rowData['link']       = (string)$tickerRow['link'];
                coreapi_model::$data[]  = $rowData;
            }            
            $this->coreModel->codeMessage('200', $this->lang->line('success'));
            coreapi_model::$returnArray['data']    = coreapi_model::$data;                
        }
        else {
            $this->coreModel->codeMessage('500', $this->lang->line('no_ticker_found'));
        }
        return coreapi_model::$returnArray;
    }

    /**
     * Description : Use to get list of all tickers
     * Author : Synergy
     * @param array $params
     * @return array of data 
     */
    function getTickerList($params){
        // pagination pending
        //$tickerResult   = $this->coreModel->queryResultArray("SELECT * FROM fj_ticker WHERE  tickerEndDate >= NOW() AND  tickerStartDate <= NOW()  GROUP BY tickerTitle ORDER BY createdAt DESC");
        $tickerResult   = $this->coreModel->queryResultArray("SELECT * FROM fj_ticker ORDER BY createdAt DESC LIMIT 0,15");
        if(count($tickerResult)>0) {
            foreach($tickerResult as $tickerRow) {
                $rowData['title']       = (string)$tickerRow['tickerTitle'];
                $rowData['thumbnail']   = (string)($tickerRow['tickerThumbImage']!=''?base_url() . '/uploads/ticker/thumb/' . $tickerRow['tickerThumbImage']:'');
                $rowData['url']         = (string)base_url() . '/page/getTicker/'.encryptURLparam($tickerRow['tickerId'], URLparamKey);
                $rowData['description']       = strip_tags($tickerRow['tickerDesc']);
                $rowData['link']       = (string)$tickerRow['link'];
                coreapi_model::$data[]  = $rowData;
            }            
            $this->coreModel->codeMessage('200', $this->lang->line('success'));
            coreapi_model::$returnArray['data']    = coreapi_model::$data;                
        }
        else {
            $this->coreModel->codeMessage('500', $this->lang->line('no_ticker_found'));
        }
        return coreapi_model::$returnArray;
    }

    /**
     * Description : Use to get details of a ticker and insert the userId and deviceId into databse who has clicked on specific ticker
     * Author : Synergy
     * @param array $params(tickerId, userId, deviceId)
     * @return array of data 
     */
    function getTickerDetail($params){
        $tickerID   = $this->coreModel->cleanString($params['tickerId']);
        $userId     = $this->coreModel->cleanString($params['userId']);
        $id         = $this->coreModel->getJwtValue($userId);
        $deviceId   = $this->coreModel->cleanString($params['deviceId']);
        $userRow    = $this->coreModel->userExists($id);
        if($tickerID!='') {
            $tickerRow   = $this->coreModel->queryRowArray("SELECT * FROM fj_ticker WHERE tickerId='$tickerID' Limit 0,1");
            if(count($tickerRow)>0) {
                $trackData = array(
                                    'userId'    => (count($userRow)>0?$id:NULL),
                                    'event'     => 'tickerClicked',
                                    'tickerId'  => (int)$tickerID,
                                    'deviceId'  => $deviceId
                                );
                $this->db->insert('fj_userEventTracker', $trackData);
                $insert_id      = $this->db->insert_id();
                if($insert_id) {                    
                    $rowData['title']       = (string)$tickerRow['tickerTitle'];
                    $rowData['file']        = (string)($tickerRow['tickerImage']!=''?base_url() . '/uploads/ticker/' . $tickerRow['tickerImage']:'');
                    $rowData['url']         = (string)base_url() . '/page/getTicker/'.encryptURLparam($tickerRow['tickerId'], URLparamKey);
                    $rowData['description']       = strip_tags($tickerRow['tickerDesc']);
                    $rowData['link']       = (string)$tickerRow['link'];
                    $rowData['image']       = (string)$tickerRow['tickerThumbImage'];
                    coreapi_model::$data[]  = $rowData;           
                    $this->coreModel->codeMessage('200', $this->lang->line('success'));
                    coreapi_model::$returnArray['data']    = coreapi_model::$data;    
                }
                else {
                    $this->coreModel->codeMessage('500', $this->lang->line('something_wrong'));
                }            
            }
            else {
                $this->coreModel->codeMessage('500', $this->lang->line('invalid_ticker'));
            }
        }
        else {
            $this->coreModel->codeMessage('500', $this->lang->line('missing_required_fields'));
        }
        return coreapi_model::$returnArray;
    }
}
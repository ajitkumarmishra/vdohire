<div class="row">
    <div class="col-sm-12 col-md-12 offset3">
        <h1>Account settings</h1>

        <?php if (@$message): ?>
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <?php echo $message; ?>
            </div>
        <?php endif; ?>

        <div class="well">
            <form class="form-horizontal" method="post" action="">
                <div class="control-group">
                    <label for="user_name" class="control-label">Name</label>
                    <div class="controls">
                        <input type="text" class="form-control" id="user_name" name="user_name" value="<?php echo $user->user_name; ?>" />
                    </div>
                </div>
                <div class="control-group">
                    <label for="inputNickname" class="control-label">Nickname</label>
                    <div class="controls">
                        <input type="text" class="form-control" id="inputNickname" name="nickname" value="<?php echo $user->user_nicename; ?>" />
                    </div>
                </div>
                <div class="control-group">
                    <label for="user_mobile" class="control-label">Phone</label>
                    <div class="controls">
                        <input type="text" class="form-control" id="user_mobile" name="user_mobile" value="<?php echo $user->user_mobile; ?>" />
                    </div>
                </div>
                <div class="control-group">
                    <label for="user_dob" class="control-label">Date of Birth</label>
                    <div class="controls">
                        <input type="text" class="form-control" id="user_dob" name="user_dob" value="<?php echo $user->user_dob; ?>" />
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="inputEmail">Email</label>
                    <div class="controls">
                        <input type="text" class="form-control" id="inputEmail" placeholder="Email" name="email" value="<?php echo $user->user_email; ?>" />
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="inputPassword">Password</label>
                    <div class="controls">
                        <input type="password" class="form-control" id="inputPassword" placeholder="Password" name="password" value="" />
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <button type="submit" class="btn form-control btn-primary">Update</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


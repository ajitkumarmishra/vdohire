<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Fjapi Controller
 * Description : Used to handle all APIs
 * @author Synergy
 * @createddate : Jun 10, 2016
 * @modificationlog : Adding comments and cleaning the code
 * @change on Mar 16, 2017
 */
class Fjapi extends MY_Controller {
    
    /**
     * Initializing variable
     */
    protected $token;
    
    /**
     * Responsable for auto load the the models
     * Responsable for auto load helpers
     * Responsable for auto load libraries
     * Defining the timezone
     * @return void
     */
    function __construct() {
        parent::__construct();
        // Load API Model
        $this->load->model('coreapi_model',             'CORE_MODEL');
        $this->load->model('otp_model',                 'OTP_MODEL');
        $this->load->model('userapi_model',             'USER_MODEL');
        $this->load->model('jobsapi_model',             'JOBS_MODEL');
        $this->load->model('interviewapi_model',        'INTERVIEW_MODEL');        
        $this->load->model('videomerge_model',          'MERGEVIDEO_MODEL');      
        $this->load->model('usertracker_model',         'TRAKER_MODEL');      
        $this->load->model('tickerapi_model',           'TICKER_MODEL');     
        $this->load->model('discussionapi_model',       'FORUM_MODEL');     
        $this->load->model('practiseapi_model',         'PRACTISE_MODEL'); 
        $this->load->model('knowledgecenterapi_model',  'KNOWLEDGE_MODEL');
        $this->load->model('auditionapi_model',         'AUDITION_MODEL');
        $this->load->model('messageapi_model',          'MESSAGE_MODEL');
        
        $this->load->model('visiterapi_model',          'VISITOR_MODEL');
        
        $this->load->model('practiseassessmentapi_model','PRACTICEASSESSMENT_MODEL');
        // Load API Message
        $this->lang->load('fjapi_message',  'english');
        // Set Token Value
        $this->token    = 'ecbcd7eaee29848978134beeecdfbc7c';
        // Other Requirements
        $this->layout   = false; 
        $this->load->helper('fj');
        $this->load->helper('jwt');
        $this->load->helper('s3');
        
        $this->load->helper('url');
        $this->load->library('email');
        $this->load->library('session');    
        $this->load->library('form_validation');
        
        date_default_timezone_set("Asia/Calcutta");
    }

    /**
     * Description : Mapping all the APIs
     * Author : Synergy
     * @param json data
     * @return json data
     */
    function index() {
		
        // Extract JSON Data
        $json       = file_get_contents("php://input");
        //echo $json;exit;    
        // Convert The String Of Data To An Array
        $data       = json_decode($json, true);
        //print_r($data);exit;
        $response   = array();
        $result     = array();
        
        //--------------- Validate API Token Request Starts ---------------//
        if ($data['token'] != $this->token) {
            $response['code']       = '300';
            $response['message']    = $this->lang->line('invalid_token');
        }
        //--------------- Validate API Token Request Ends ---------------//
        //
        //--------------- API Method List Starts ---------------//
        else {
            $methodName = !empty($data['methodname']) ? $data['methodname'] : '';
            switch ($methodName) {
                
                case 'generateOTP':
                    $result = $this->USER_MODEL->$methodName($data);
                    break;
                case 'signUp':
                    $result = $this->USER_MODEL->$methodName($data);
                    break;
                case 'getState':
                    $result = $this->USER_MODEL->$methodName($data);
                    break;
                case 'getCity':
                    $result = $this->USER_MODEL->$methodName($data);
                    break;
                case 'getOffice':
                    $result = $this->USER_MODEL->$methodName($data);
                    break;
                case 'getPincode':
                    $result = $this->USER_MODEL->$methodName($data);
                    break;               
                case 'signIn':
                    $result = $this->USER_MODEL->$methodName($data);
                    break;                
                case 'changePassword':
                    $result = $this->USER_MODEL->$methodName($data);
                    break;
                case 'recoverPassword':
                    $result = $this->USER_MODEL->$methodName($data);
                    break; 
                case 'getCourses':
                    $result = $this->USER_MODEL->$methodName($data);
                    break;               
                case 'searchUniversity':
                    $result = $this->USER_MODEL->$methodName($data);
                    break;
                case 'editBasicinfo':
                    $result = $this->USER_MODEL->$methodName($data);
                    break;
                case 'editEducationalInfo':
                    $result = $this->USER_MODEL->$methodName($data);
                    break;
                case 'editOtherInfo':
                    $result = $this->USER_MODEL->$methodName($data);
                    break;
                case 'editSocialInfo':
                    $result = $this->USER_MODEL->$methodName($data);
                    break;  
                case 'removeQualification':
                    $result = $this->USER_MODEL->$methodName($data);
                    break;               
                case 'getUserInformation':
                    $result = $this->USER_MODEL->$methodName($data);
                    break;               
                case 'getBasicInformation':
                    $result = $this->USER_MODEL->$methodName($data);
                    break;               
                case 'getEducationalInformation':
                    $result = $this->USER_MODEL->$methodName($data);
                    break;               
                case 'getOtherInformation':
                    $result = $this->USER_MODEL->$methodName($data);
                    break;               
                case 'getSocialInformation':
                    $result = $this->USER_MODEL->$methodName($data);
                    break; 
                case 'getTokenID':
                    $result = $this->USER_MODEL->$methodName($data);
                    break;  
                
                case 'getSearchCriteriaLocation':
                    $result = $this->USER_MODEL->$methodName($data);
                    break;
                case 'setUserSearchCriteria':
                    $result = $this->USER_MODEL->$methodName($data);
                    break;
                case 'getUserSearchCriteria':
                    $result = $this->USER_MODEL->$methodName($data);
                    break;  
                case 'setUserNotification':
                    $result = $this->USER_MODEL->$methodName($data);
                    break;   
                case 'getUserNotification':
                    $result = $this->USER_MODEL->$methodName($data);
                    break;
                case 'verifyJob':
                    $result = $this->JOBS_MODEL->$methodName($data);
                    break;
                case 'getJobLocation':
                    $result = $this->JOBS_MODEL->$methodName($data);
                    break;
                case 'searchJobs':
                    $result = $this->JOBS_MODEL->$methodName($data);
                    break;
                case 'getJobDetail':
                    $result = $this->JOBS_MODEL->$methodName($data);
                    break;
                case 'getSavedJob':
                    $result = $this->JOBS_MODEL->$methodName($data);
                    break;
                case 'saveJob':
                    $result = $this->JOBS_MODEL->$methodName($data);
                    break;
                case 'removeSavedJob':
                    $result = $this->JOBS_MODEL->$methodName($data);
                    break;
                case 'applyJob':
                    $result = $this->JOBS_MODEL->$methodName($data);
                    break; 
                case 'getAppliedJob':
                    $result = $this->JOBS_MODEL->$methodName($data);
                    break;            
                case 'interviewQuestionList':
                    $result = $this->INTERVIEW_MODEL->$methodName($data);
                    break;
                case 'interviewQuestionListInvite':
                    $result = $this->INTERVIEW_MODEL->$methodName($data);
                    break;
                case 'interviewQuestionDetail':
                    $result = $this->INTERVIEW_MODEL->$methodName($data);
                    break;
                case 'interviewQuestionDetailInvite':
                    $result = $this->INTERVIEW_MODEL->$methodName($data);
                    break;
                case 'interviewQuestionAnswer':
                    $result = $this->INTERVIEW_MODEL->$methodName($data);
                    break;          
                case 'assessmentQuestionList':
                    $result = $this->INTERVIEW_MODEL->$methodName($data);
                    break;
                case 'assessmentQuestionDetail':
                    $result = $this->INTERVIEW_MODEL->$methodName($data);
                    break;
                case 'assessmentQuestionAnswer':
                    $result = $this->INTERVIEW_MODEL->$methodName($data);
                    break;
                case 'answerScreen':
                    $result = $this->MERGEVIDEO_MODEL->$methodName($data, 'interview');
                    break;
					
				case 'FfmpegTest':
                    $result = $this->MERGEVIDEO_MODEL->$methodName($data, 'interview');
                    break;
					
                case 'thankyouScreen':
                    $result = $this->MERGEVIDEO_MODEL->$methodName($data, 'interview');
                    break;

                case 'getTopTickers':
                    $result = $this->TICKER_MODEL->$methodName($data);
                    break;
                case 'getTickerList':
                    $result = $this->TICKER_MODEL->$methodName($data);
                    break;
                case 'getTickerDetail':
                    $result = $this->TICKER_MODEL->$methodName($data);
                    break;
                
                case 'createForum':
                    $result = $this->FORUM_MODEL->$methodName($data);
                    break;
                case 'getForumList':
                    $result = $this->FORUM_MODEL->$methodName($data);
                    break;
                case 'getForumDetail':
                    $result = $this->FORUM_MODEL->$methodName($data);
                    break;
                case 'replyForum':
                    $result = $this->FORUM_MODEL->$methodName($data);
                    break;
                
                case 'getIndustryList':
                    $result = $this->PRACTISE_MODEL->$methodName($data);
                    break;
                case 'getFunctionList':
                    $result = $this->PRACTISE_MODEL->$methodName($data);
                    break;
                /*
                case 'getInterviewSetList':
                    $result = $this->PRACTISE_MODEL->$methodName($data);
                    break;
                case 'downloadSet':
                    $result = $this->PRACTISE_MODEL->$methodName($data);
                    break;
                * 
                */                
                case 'getPractiseSetFunctions':
                    $result = $this->PRACTISE_MODEL->$methodName($data);
                    break;
                case 'getPractiseSets':
                    $result = $this->PRACTISE_MODEL->$methodName($data);
                    break;
                case 'downloadPractiseSet':
                    $result = $this->PRACTISE_MODEL->$methodName($data);
                    break;
                case 'downloadReport':
                    $result = $this->PRACTISE_MODEL->$methodName($data);
                    break;
                                        
                case 'getPractiseSet':
                    $result = $this->PRACTISE_MODEL->$methodName($data);
                    break;
                case 'getPractiseIndustryList':
                    $result = $this->PRACTISE_MODEL->$methodName($data);
                    break;                                        
                case 'getPractiseFunctionList':
                    $result = $this->PRACTISE_MODEL->$methodName($data);
                    break;                                        
                case 'getPractiseSetList':
                    $result = $this->PRACTISE_MODEL->$methodName($data);
                    break;
                
                case 'getKnowledgeCenterCategory':
                    $result = $this->KNOWLEDGE_MODEL->$methodName($data);
                    break;
                case 'getKnowledgeCenterTopics':
                    $result = $this->KNOWLEDGE_MODEL->$methodName($data);
                    break;
                case 'getKnowledgeCenterDetail':
                    $result = $this->KNOWLEDGE_MODEL->$methodName($data);
                    break; 
                				
                case 'auditionHistory':
                    $result = $this->AUDITION_MODEL->$methodName($data);
                    break;
                case 'auditionUpdate':
                    $result = $this->AUDITION_MODEL->$methodName($data);
                    break;
                case 'auditionDelete':
                    $result = $this->AUDITION_MODEL->$methodName($data);
                    break;
                case 'auditionData':
                    $result = $this->AUDITION_MODEL->$methodName($data);
                    break;
                case 'createAuditionFile':
                    $result = $this->AUDITION_MODEL->$methodName($data);
                    break;

                case 'practiceAssessmentSet':
                    $result = $this->PRACTICEASSESSMENT_MODEL->$methodName($data, 'interview');
                    break;
                
                case 'practiceAssessmentSetDetail':
                    $result = $this->PRACTICEASSESSMENT_MODEL->$methodName($data, 'interview');
                    break;
                
                case 'mergeVideo':
                    $result = $this->MERGEVIDEO_MODEL->$methodName($data, 'interview');
                    break;
                
                case 'visitor':
                    $result = $this->VISITOR_MODEL->$methodName($data);
                    break;
                
                case 'messageList':
                    $result = $this->MESSAGE_MODEL->$methodName($data);
                    break;                
                case 'messageDetail':
                    $result = $this->MESSAGE_MODEL->$methodName($data);
                    break;                
                case 'messageReply':
                    $result = $this->MESSAGE_MODEL->$methodName($data);
                    break;
                
                //Changes Done by Synergy
                case 'addAuditionFile_v2':
                    $result = $this->AUDITION_MODEL->$methodName($data);
                break;

                case 'LogAppEvent':
                    $result = $this->AUDITION_MODEL->$methodName($data);
                break;

                case 'interviewQuestionAnswer_v2':
                    $result = $this->INTERVIEW_MODEL->$methodName($data);
                break;

                case 'signUp_v2':
                    $result = $this->USER_MODEL->$methodName($data);
                break;

                case 'getInvites':
                    $result = $this->USER_MODEL->$methodName($data);
                break;

                case 'deleteInvite':
                    $result = $this->USER_MODEL->$methodName($data);
                break;

                case 'editEmploymentInfo':
                    $result = $this->USER_MODEL->$methodName($data);
                break;

                case 'editResume':
                    $result = $this->USER_MODEL->$methodName($data);
                break;

                case 'editResume_v2':
                    $result = $this->USER_MODEL->$methodName($data);
                break;

                case 'getUserResumes':
                    $result = $this->USER_MODEL->$methodName($data);
                break;

                case 'setUserActiveResume':
                    $result = $this->USER_MODEL->$methodName($data);
                break;

                case 'deleteUserResume':
                    $result = $this->USER_MODEL->$methodName($data);
                break;

                case 'isUserExists':
                    $result = $this->USER_MODEL->$methodName($data);
                break;

                case 'uploadResumeWithDocumentPath':
                    $result = $this->USER_MODEL->$methodName($data);
                break;

                case 'uploadResumeWithDocumentPathOld':
                    $result = $this->USER_MODEL->$methodName($data);
                break;

                case 'updateProcessStatusForEmail':
                    $result = $this->USER_MODEL->$methodName($data);
                break;

                case 'sendMessage':
                    $result = $this->MESSAGE_MODEL->$methodName($data);
                break;

                case 'getMessageList':
                    $result = $this->MESSAGE_MODEL->$methodName($data);
                break;

                case 'getSentMessageList':
                    $result = $this->MESSAGE_MODEL->$methodName($data);
                break;

                case 'getMessageDetails':
                    $result = $this->MESSAGE_MODEL->$methodName($data);
                break;

                case 'deleteMessage':
                    $result = $this->MESSAGE_MODEL->$methodName($data);
                break;

                case 'getEmploymentInformation':
                    $result = $this->USER_MODEL->$methodName($data);
                break;

                case 'removeEmployment':
                    $result = $this->USER_MODEL->$methodName($data);
                break;

                case 'signUp_v3':
                    $result = $this->USER_MODEL->$methodName($data);
                break;

                case 'updateDeviceToken':
                    $result = $this->USER_MODEL->$methodName($data);
                break;

                case 'updateAadharActivationStatus':
                    $result = $this->USER_MODEL->$methodName($data);
                break;

                case 'getOtherInformation_v2':
                    $result = $this->USER_MODEL->$methodName($data);
                break;

                case 'getCandidateProfileAverage':
                    $result = $this->USER_MODEL->$methodName($data);
                break;

                case 'getCandidateAverageAppliedJobs':
                    $result = $this->USER_MODEL->$methodName($data);
                break;

                case 'getWebInterviewInvites':
                    $result = $this->USER_MODEL->$methodName($data);
                break;

                case 'testApi':
                    $result = $this->USER_MODEL->$methodName($data);
                break;
				case 'checkProfile':
                    $result = $this->USER_MODEL->$methodName($data);
                    break;
				case 'virtualSignUp':
                    $result = $this->USER_MODEL->$methodName($data);
                    break;
				case 'completeProfile':
                    $result = $this->USER_MODEL->$methodName($data);
                    break;

                case 'getJobSeekerEvent':
                    $result = $this->USER_MODEL->$methodName($data);
                    break;

                case 'updateDetailsOfJobsSeeker':
                    $result = $this->USER_MODEL->$methodName($data);
                    break;

                case 'getProfilePercent':
                    $result = $this->USER_MODEL->$methodName($data);
                    break;
                case 'getBasicInformation_web':
                    $result = $this->USER_MODEL->$methodName($data);
                    break;
				case 'sendMessageToUsers':
                    $result = $this->MESSAGE_MODEL->$methodName($data);
                    break;
				case 'uploadUserImage':
                    $result = $this->USER_MODEL->$methodName($data);
                    break;
                case 'assessmentQuestionList_v2':
                    $result = $this->INTERVIEW_MODEL->$methodName($data);
                    break;

                case 'assessmentQuestionDetail_v2':
                    $result = $this->INTERVIEW_MODEL->$methodName($data);
                    break;
                case 'assessmentQuestionAnswer_v2':
                    $result = $this->INTERVIEW_MODEL->$methodName($data);
                    break;
                case 'checkAppliedJob':
                    $result = $this->JOBS_MODEL->$methodName($data);
                    break;
				case 'getJobsForEvent':
                    $result = $this->USER_MODEL->$methodName($data);
                    break;

                case 'getJobFairApplicationData':
                    $result = $this->USER_MODEL->$methodName($data);
                    break;
                    
                case 'getUnmergedVideoInterviews':
                    $result = $this->INTERVIEW_MODEL->$methodName($data);
                    break;

                case 'answerScreen_v2':
                    $result = $this->MERGEVIDEO_MODEL->$methodName($data, 'interview');
                    break;

                case 'setJobResume':
                    $result = $this->USER_MODEL->$methodName($data);
                    break;

                case 'getSavedJobStatus':
                    $result = $this->JOBS_MODEL->$methodName($data);
                    break;

                case 'addCronJobInfo':
                    $result = $this->USER_MODEL->$methodName($data);
                    break;

                case 'addCronJobDetailInfo':
                    $result = $this->USER_MODEL->$methodName($data);
                    break;

                case 'checkWelcomeMessageDisplayed':
                    $result = $this->USER_MODEL->$methodName($data);
                    break;

                case 'getUserJobProcessStatus':
                    $result = $this->JOBS_MODEL->$methodName($data);
                    break;

                case 'setUserJobProcessStatus':
                    $result = $this->JOBS_MODEL->$methodName($data);
                    break;
					
				case 'isUserExists_V1':
                    $result = $this->USER_MODEL->$methodName($data);
                    break;
					
				case 'signUp_v4':
                    $result = $this->USER_MODEL->$methodName($data);
                    break;
					
				case 'getMessageListWithPagination':
                    $result = $this->MESSAGE_MODEL->$methodName($data);
                    break;

				case 'getClosedInvites':
                    $result = $this->USER_MODEL->$methodName($data);
                    break;

				case 'getJobFairApplicationData1':
                    $result = $this->USER_MODEL->$methodName($data);
                    break;

				case 'userVisitLog':
                    $result = $this->USER_MODEL->$methodName($data);
                    break;

                case 'getUserNotifications':
                    $result = $this->USER_MODEL->$methodName($data);
                    break;
                case 'readAllNotifications':
                    $result = $this->USER_MODEL->$methodName($data);
                    break;

                case 'deleteNotification':
                    $result = $this->USER_MODEL->$methodName($data);
                    break;
				
				case 'auditionSetQuestions':
                    $result = $this->AUDITION_MODEL->$methodName($data);
                    break;
				
				case 'auditionQuestion':
                    $result = $this->AUDITION_MODEL->$methodName($data);
                    break;
					
				case 'getUniversityList':
                    $result = $this->USER_MODEL->$methodName($data);
                    break;

                case 'getEducationDetails':
                    $result = $this->USER_MODEL->$methodName($data);
                    break;

                case 'getEmploymentDetails':
                    $result = $this->USER_MODEL->$methodName($data);
                    break;

                case 'getRecentJobSearchList':
                    $result = $this->JOBS_MODEL->$methodName($data);
                    break;

                case 'assessmentQuestionList_v3':
                    $result = $this->INTERVIEW_MODEL->$methodName($data);
                break;

                case 'hasAssessmentQuestions':
                    $result = $this->INTERVIEW_MODEL->$methodName($data);
                break;

                default:
                    $response['code']       = '500';
                    $response['message']    = $this->lang->line('default_invalid');
            }
        }
        //--------------- API Method List Ends ---------------//
        //
        //--------------- Populate Response Data ---------------//
        if (!empty($result)) {            
            $response = $result;
        }
        echo json_encode($response); exit;
        //--------------- Populate Response Data ---------------//
    }
    //--------------- Index Method Ends ---------------//
    
    
    /**
     * Description : This API is used to submit answer of interview questions
     * Author : Synergy
     * @param $_POST
     * @return json encoded data 
     */
    function interviewQuestionAnswer() {
        $result = $this->INTERVIEW_MODEL->interviewQuestionAnswerParts($_POST, $_FILES);
        if (!empty($result)) {            
            $response = $result;
        }
        echo json_encode($response); exit;
    }
    
    /**
     * Description : This function is use to submit answer of assessment questions
     * Author : Synergy
     * @param array $_POST
     * @return json encoded data 
     */
    function assessmentQuestionAnswer() {
        $result = $this->INTERVIEW_MODEL->assessmentQuestionAnswerParts($_POST);
        if (!empty($result)) {            
            $response = $result;
        }
        echo json_encode($response); exit;
    }

    /**
     * Description : This Function is used to upload Audition Video
     * Author : Synergy
     * @param array $_POST
     * @return json encoded data 
     */
    function addAuditionData() {

        $result = $this->AUDITION_MODEL->addAudition($_POST, $_FILES);
        if (!empty($result)) {            
            $response = $result;
        }
        echo json_encode($response); exit;
    }

    /**
     * Description : This Function is used to merge audition files
     * Author : Synergy
     * @param array $_POST
     * @return json encoded data 
     */
    function addAuditionFile() {
        $result = $this->AUDITION_MODEL->addAuditionFile($_POST, $_FILES);
        if (!empty($result)) {            
            $response = $result;
        }
        echo json_encode($response); exit;
    }
    
    /**
     * Description : This Function is used to for testing purpose of upload audition
     * Author : Synergy
     * @param array $_POST
     * @return json encoded data 
     */
    function addAuditionFileTest() {
        $result = $this->AUDITION_MODEL->addAuditionFileTest($_POST, $_FILES);
        if (!empty($result)) {            
            $response = $result;
        }
        echo json_encode($response); exit;
    }

    /**
     * Description : This Function is used for converting video
     * Author : Synergy
     * @param void
     * @return void
     */
    function videoConvert() {
        shell_exec("ffmpeg -y  -noautorotate -f concat -i file2.txt -c copy video2.mp4 ");
    }
}
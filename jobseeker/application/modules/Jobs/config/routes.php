<?php
$route['job/invitation/(:any)'] = 'Jobs/invitation/$1';
$route['job/event-jobs/(:any)'] = 'Jobs/event_jobs/$1';
$route['job/hot_jobs'] = 'Jobs/hot_jobs';
$route['job/details/(:any)'] = 'Jobs/job_details/$1';
$route['job/searched-jobs'] = 'Jobs/searched_jobs';
$route['job/searched-jobs-result'] = 'Jobs/searched_jobs_result';

?>
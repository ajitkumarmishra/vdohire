<style>
    #tabs > ul li > a > table:hover{
        background: none;
    }
</style>

    <?php
        $createdBy = $content->createdBy;
        $CI =& get_instance();
        //echo "SELECT id, role, createdBy FROM fj_users WHERE id='$createdBy'";exit;
        $companyQuery = $CI->db->query("SELECT id, role, createdBy FROM fj_users WHERE id='$createdBy'")->row_array();
        $actualCompanyId = $companyQuery['createdBy'];
        if($companyQuery['role'] == 2) {
            $companyQuery = $CI->db->query("SELECT mediaUrl FROM fj_account WHERE userId='$createdBy'")->row_array();
        } else {
            $companyQuery = $CI->db->query("SELECT mediaUrl FROM fj_account WHERE userId='$actualCompanyId'")->row_array();
        }

        if($companyQuery) {
            $companyMediaUrl = $companyQuery['mediaUrl'];
        } else {
            $companyMediaUrl = '';
        }
    ?>
    
    <!-- Navigation -->
    <nav class="navbar navbar-inverse">
      <div class="container-fluid">
        <div class="navbar-header pull-left">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>                        
          </button>
          <a class="navbar-brand" href="<?php echo base_url()?>users"><img src="<?php echo base_url(); ?>images/logo.png" rel="stylesheet"></a>
        </div>
        <div class="collapse navbar-collapse pull-left" id="myNavbar" style="padding-top:20px; width: 76%;">
          <span style="padding-left: 27px; font-size: 16px; color: white;">Welcome to digital interview process with VDOHire. You are singed in as <?php echo $_SESSION['userEmail'];?>.</span>
        </div>
        <div class="clearfix"></div>
      </div>
    </nav>
    

    <div class="row" style="    background: #f8f8f8;margin-right: 0px;">
        <div id="tabs" style="background: #054F72;    height: 97px;">
            <div id="tabs" style="background: #054F72;    height: 97px;">
        <ul class="nav nav-tabs nav-justified">
            <li class="active pull-left" style="    width: 18%;cursor: pointer;margin-top: 6px;">
                <a href="javascript://" style="text-decoration:none;cursor:pointer;width:100%;">
                    <table style="width:100%;">
                        <tbody>
                            <tr>
                                <td>
                                    <div style="text-align: center;    padding: 13px;padding-right: 0px;padding-left: 0px;"><b>Position</b><br>
									<span   id="positionDivContainer"  style="font-size:15px;    display: inline-grid;
    width: 228px;
    white-space: nowrap;
    overflow: hidden !important;
    text-overflow: ellipsis;"><?php echo $content->title; ?></span></div></td>
                            </tr>

                        </tbody>
                    </table>
                </a>
            </li>
            <li class="active pull-left" style="    width: 18%;cursor: pointer;margin-top: 6px;">
                <a href="javascript://" style="text-decoration:none;cursor:pointer;width:100%;">
                    <table style="width:100%;">
                        <tbody>
                            <tr>
                                <td cellpadding="0">
                                    <div style="text-align: center;    padding: 13px;padding-right: 0px;padding-left: 0px;"><b>Company</b><br>
									<span style="font-size:15px;display: inline-grid;
    width: 228px;
    white-space: nowrap;
    overflow: hidden !important;
    text-overflow: ellipsis;"  id="CompanyNameContainer"><?php echo $content->companyName; ?></span></div></td>
                            </tr>
                        </tbody>
                    </table>
                </a>
            </li>

            <li class="active pull-left" style="    width: 12%;cursor: pointer;margin-top: 6px;">
                <a href="javascript://" style="text-decoration:none;cursor:pointer;width:100%;"   class="active" >
                    <table style="width:100%;">
                        <tbody>
                            <tr><td cellpadding="0">
                                    <div style="text-align: center; padding: 13px;padding-right: 0px;padding-left: 0px;">
                                        <b>Location</b><br>
                                        <span style="font-size:15px;display: inline-grid;width: 228px;white-space: nowrap;overflow: hidden !important;text-overflow: ellipsis;">
                                            <?php
                                                $locations = array();
                                                foreach ($content->location as $location) {
                                                    $locations[] = $location->city;
                                                }
                                                echo implode(', ', $locations);
                                            ?>
                                        </span>
                                    </div>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                </a>
            </li>

            <li class="active pull-left" style="    width: 12%;cursor: pointer;margin-top: 6px;">
                <a href="javascript://" style="text-decoration:none;cursor:pointer;width:100%;"   class="active" >
                    <table style="width:100%;">
                        <tbody>
                            <tr>
                                <td>
                                    <div style="text-align: center;    padding: 13px;padding-right: 0px;padding-left: 0px;">
                                        <span class="texto_grande" style="color: white;font-size: 16px;"><b>Vacancies</b><br>
										<span style="font-size:15px"><?php echo $content->vaccancies; ?></span></span></div></td>
                            </tr>
                        </tbody>
                    </table>
                </a>
            </li>

            <li class="active pull-left" style="    width: 20%;cursor: pointer;margin-top: 6px;">
                <a href="javascript://" style="text-decoration:none;cursor:pointer;width:100%;"   class="active" >
                    <table style="width:100%;">
                        <tbody>
                            <tr>
                                <td cellpadding="0">
                                    <div style="text-align: center;    padding: 13px;padding-right: 0px;padding-left: 0px;">
                                        <span class="texto_grande" style="color: white;font-size: 16px;"><b>Salary&nbsp;(INR)</b><br>
										<span style="font-size:15px"><?php echo $content->minSalary; ?> - <?php echo $content->maxSalary; ?></span></span></div></td>
                            </tr>
                            
                        </tbody>
                    </table>
                </a>
            </li>

            <li class="active pull-left" style="    width: 18%;cursor: pointer;margin-top: 6px;">
                <a href="javascript://" style="text-decoration:none;cursor:pointer;width:100%;"   class="active" >
                    <table style="width:100%;">
                        <tbody>
                            <tr>
                                <td cellpadding="0">
                                    <div style="text-align: center;   padding: 13px;padding-right: 0px;padding-left: 0px;"><span class="texto_grande" style="color: white;font-size: 16px;">
                                            <b>Last date to Apply</b><br>
                                            <span style="font-size:15px">
                                            <?php
                                                if(($content->openTill != '0000-00-00 00:00:00') && !empty($content->openTill)) {
                                                    echo date('d M, Y', strtotime($content->openTill)); 
                                                } else {
                                                    echo '-';
                                                }
                                            ?>
                                            </span>
                                        </span></div></td>
                            </tr>
                        </tbody>
                    </table>
                </a>
            </li>

        </ul>
        <!-- /.col-lg-12 -->
    </div>
        </div>
		
        <div class="container-full" style="background: white;">
            <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #054F72;height: 33px;">
                
            </div>
            <div  class="col-md-8 col-lg-8 col-sm-8 pull-left" style="width: 80%;background: #EB9B2C ;">
                <div class="btn-group btn-group-lg" style="padding: 10px 0px 10px 0px;">
<!--                    <div  class="pull-left" style="width: auto;margin-right:30px;">
                        <button onclick="showRequirements()" id="showRequirements"  type="button" class="btn btn-primary" style="font-weight: 600;padding: 13px;background: #29ABC1;font-size: 17px;border: none;">VIDEO INTERVIEW</button>
                    </div>-->
                    <div  class="pull-left" style="    width: auto;margin-right:30px;">
                        <button onclick="showJobDescription()" id="showJobDescription1" type="button" class="btn btn-primary" style="font-weight: 600;padding: 13px;background: #29ABC1;font-size: 17px;border: none;color:black;">JOB DESCRIPTION</button>
                    </div>
                    
                    <div  class=" pull-left" style="    width: auto;margin-right:30px;">
                        <button onclick="ShowAboutCompany()" id="ShowAboutCompany" type="button" class="btn btn-primary" style="font-weight: 600;padding: 13px;background: #d3d3d3;font-size: 17px;border: none;color:black;">ABOUT COMPANY</button>
                    </div>
                    
                </div>
            </div>
            <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #054F72;height: 33px;">
                
            </div>
            
        </div>
        <div style="clear:both"></div>
        <div class="container-full" style="background: white;">
            <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #f8f8f8;height: 30px;">

                </div>
            <div  class="col-md-8 col-lg-8 col-sm-8 pull-left" style="border:1px solid lightgrey;padding-top: 10px;padding-bottom: 10px;width: 80%;background: white ;">
                <div class="col-lg-12">
                    <!-- /.row -->
                <div class="col-lg-6" id="Requirements" style="display:none;">
                    <h3 style="margin-top:10px;">Requirements</h3>
                    
                    <ul style="    padding-left: 15px;">
                        <li style="padding: 5px;font-size: 15px;font-weight: 600;">Working webcam and Microphone</li>                        
                        <li style="padding: 5px;font-size: 15px;font-weight: 600;">Strong Internet Connectivity</li>
                        <li style="padding: 5px;font-size: 15px;font-weight: 600;">Room with minimal background noise</li>
                    </ul>
                    <!-- /.panel -->

                    <p style="    font-size: 14px;font-weight: 600;padding-top: 0px;">This interview is powered by VDOHire, an asynchronous web based video interviewing platform. 
                        The employer has pre-recorded
                        questions that you will see in the next few steps. You will be expected to record your responses using video.</p>
                    
                    <p style="    font-size: 14px;font-weight: 600;padding-top: 10px;">VDOHire simulates a real interview. Once you start you cannot leave the page.
                    If you leave during an in-person interview, your application would not be considered. Likewise your interview will be terminated if you leave the page once you start.</p>
                    
                    <p style="    font-size: 14px;font-weight: 600;padding-top: 10px;">
                        Ensure you have a strong internet connection and are situated in a quite room.
                    </p>
                    
                </div>
				<div id="showAboutUs"  class="col-lg-6" style="display:none;">
					<h3 style="margin-top:10px;">About <?php echo $content->companyName; ?></h3>
                    <?php echo $content->aboutCompany; ?>
				</div>

                <div class="col-lg-6" id="companyVideoDiv" style="display:none;">
                    <h3 style="margin-top:10px;">Company Video</h3>
                    <?php if(!empty($companyMediaUrl)) { 
                        if( strpos( $companyMediaUrl, 'youtube.com' ) !== false ) {
                            $youtubeFileName = substr($companyMediaUrl, strrpos($companyMediaUrl, '?v=') + 3);
                            ?>
                            <iframe width="625" height="265" src="https://www.youtube.com/embed/<?php echo $youtubeFileName ?>" frameborder="0" allowfullscreen></iframe>
                        <?php } else { ?>
                            <video style="width: 625px;height: 265px;" controls="">
                                <source src="<?php echo $companyMediaUrl ?>">
                                </source>
                              Your browser does not support the video tag.
                            </video>
                        <?php } ?>
                    <?php } else { ?>
                        <span>Video Not Available</span>
                    <?php } ?>
                </div>
				
				<div id="showJobDescription"  class="col-lg-6" style="text-align: justify;">
                    <h3 style="margin-top:10px;">Job Description</h3>
                    <?php echo htmlspecialchars_decode($content->description); ?>
                    <div style="clear:both"></div>
				</div>
                <!-- /.col-lg-4 -->
                <div id="screeningProcess" class="col-lg-6">
                    <h3 style="margin-top:10px;">Application Process</h3>
                        <p><b>The application process for this position consists of:</b><br>
                        <ul>
                            <li>An Interview, with "<?php if($content->interviewQuestion == 1){ echo $content->interviewQuestion.'" question'; }else{  echo $content->interviewQuestion.'" questions'; } ?> with a total answering time of "<?php 
							$mins = floor($content->interviewDuration/60); 
							$seconds = $content->interviewDuration - $mins*60;
							if($seconds == 0){
								$seconds = "";
							}else{
								$seconds = $seconds." seconds";
							}
							
							if($mins >1){
								$text = "mins";
							}else{
								$text = "min";
							}
							echo $mins." ".$text." ". $seconds;  ?>"</li>
							<?php
							if($content->assessmentDuration == ""){
							}else{
							?>
                            <li>An Assessment, with "<?php if($content->assessmentQuestion == 1){ echo $content->assessmentQuestion.'" question'; }else{  echo $content->assessmentQuestion.'" questions'; } ?> , with multiple-choice answer options, with a total provided time of "<?php 
							$mins = floor($content->assessmentDuration/60); 
							$seconds = $content->assessmentDuration - $mins*60;
							if($seconds == 0){
								$seconds = "";
							}else{
								$seconds = $seconds." seconds";
							}
							
							if($mins >1){
								$text = "mins";
							}else{
								$text = "min";
							}
							echo $mins." ".$text." ". $seconds; ?>"</li>
							<?php } ?>
                        </ul>
                        </p>
                        <br>
                        <p>
                            Click on <b>"TAKE INTERVIEW"</b> to initiate the VDOHire Digital Application experience. If you click on "TAKE INTERVIEW", you will be taken through a set of guidelines to ensure you are ready for the Interview. This should not take more than 5 minutes. The Job Interview will start only when you click on the "START INTERVIEW" button. 
                        </p>
                    <!-- /.row -->
                </div>
                <!-- /.col-lg-8 -->
                </div>   
            <!-- /.row -->
                <div class="col-lg-12">
            <!-- /.row -->
            <div class="col-lg-4">
			<?php if($checkAppliedJob == 200) { ?>
                <center>
				<a class="btn btn-primary" href="<?php echo base_url()?>media/question" style="margin-right: 50px;font-size: 18px;background: #11AAF0;color:white; padding: 16px; text-decoration: none;">TAKE INTERVIEW</a>
            </center>
			<?php } else {  ?>
					<button type="button" class="btn btn-primary" style=" padding: 16px;
                            font-size: 18px;background: #11AAF0;border: none;   margin-left: 10px;margin-right: 50px;" disabled>ALREADY APPLIED</a></button>
			<?php }  ?>
            </div>
                <div class="col-lg-4">
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-8 -->
                <div class="col-lg-4" style="    display: none;">
                    <!-- /.panel -->
					<?php if($checkAppliedJob == 200){?>
                    <center><button type="button" onclick="saveForJob()" class="btn btn-primary" style="padding: 16px;font-size: 18px;background: #7FCFB4;border: none;   margin-left:10px;margin-top: 10px;margin-right: 50px;">SAVE FOR LATER</button>
                    </center>
					<?php }else{  ?>
                    <!-- /.panel -->
					<?php }  ?>
                </div>
                <!-- /.col-lg-8 -->
                
                </div>
            <!-- /.row -->

                </div>

            <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #f8f8f8;height: 30px;">

                </div>

        </div>
        
        <?php
        
        ?>
        
    <!-- /#page-wrapper -->
    </div>
</div>
<!-- /#wrapper -->
<form id="changepassword" method="post" class="form-horizontal" style="display: none;">
    <div id="page-content-wrapper">
        <div class="container-fluid whitebg">
            <div class="row">
                <div class="col-md-12 padd_tp_bt_20">
                    <div class="form-horizontal form_save">
            <div class="form-group">
                <label for="inputEmail" class="control-label col-xs-3 required-field ">Old Password</label>
                <div class="col-xs-9">
                    <input type="text" class="form-control" placeholder="Old Password" name="oldPassword" id="oldPassword" value="">
                </div>
            </div>

            <div class="form-group">
                <label for="inputEmail" class="control-label col-xs-3 required-field ">New Password</label>
                <div class="col-xs-9">
                    <input type="text" class="form-control" placeholder="New Password" name="newPassword" data-validation="length" data-validation-length="min6" id="newPassword"  value="">
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail" class="control-label col-xs-3 required-field ">Confirm New Password</label>
                <div class="col-xs-9">
                    <input type="text" class="form-control" placeholder="Confirm New Password" name="confirmNewPassword"  id="confirmNewPassword" value="">
                </div>
            </div>

            <div class="form-group">
                <div class="col-xs-5 col-xs-offset-3">
                    <input type="hidden" name="token" id="token" value="ecbcd7eaee29848978134beeecdfbc7c">
                    <input type="hidden" name="methodname" id="methodname" value="changePassword">
                    <input type="hidden" name="userId" id="userId" value="<?php echo $token ?>">
                    <button type="submit" class="btn btn-default pull-left" style="background: #054F72; color: white;">Save</button>
                </div>
            </div>

        </div>
    </div>
        </div>
    </div>
    </div>
</form>
<div class="modal fade" id="NewsDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header" style="border-bottom:none;">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h2 id="myModalLabel" class="newsHeader" style="color:#08688F;font-size: 20px;"></h2>
      </div>
        <div class="modal-body newsImage">
        </div>
        <div class="modal-body newsBody">
        </div>
        
    </div>
  </div>
</div>


<script src="<?php echo base_url(); ?>theme/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url(); ?>theme/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>theme/js/bootbox.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->

    <!-- Custom Theme JavaScript -->
<script src="<?php echo base_url(); ?>theme/js/formValidation.min.js"></script>
<script src="<?php echo base_url(); ?>theme/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>js/invitation.js"></script>

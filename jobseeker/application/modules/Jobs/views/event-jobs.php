
<script type="text/javascript" src="<?php echo base_url() ?>js/jquery.countdown.js"></script>
<style>
#dataTables-example_filter{
	display:none;
}
.odd{
	    background: white;
}
.even{
	    background: #f7f5f4;
}
.hasCountdown {
}
.countdown_rtl {
	direction: rtl;
}
.countdown_holding span {
	color: #888;
}
.countdown_row {
	clear: both;
	width: 100%;
	padding: 0px 2px;
	text-align: center;
}
.countdown_show1 .countdown_section {
	width: 98%;
}
.countdown_show2 .countdown_section {
	width: 48%;
}
.countdown_show3 .countdown_section {
	width: 32.5%;
}
.countdown_show4 .countdown_section {
	width: 24.5%;
}
.countdown_show5 .countdown_section {
	width: 19.5%;
}
.countdown_show6 .countdown_section {
	width: 16.25%;
}
.countdown_show7 .countdown_section {
	width: 14%;
}
.countdown_section {
	display: block;
	float: left;
	font-size: 75%;
	text-align: center;
}
.countdown_amount {
	font-size: 200%;
}
.countdown_descr {
	display: block;
	width: 100%;
}
</style>
<script type="text/javascript">



$(function () {
	
	var austDay = new Date("<?php echo $eventDate; ?>");
	//austDay = new Date(austDay.getFullYear() + 1, 5, 2);alert(austDay);
	$('#defaultCountdown').countdown({until: austDay});
	$('#year').text(austDay.getFullYear());
});
</script>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse" id="<?php echo $eventDate; ?>">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="<?php echo base_url();?>users"><img src="<?php echo base_url(); ?>images/logo.png" rel="stylesheet"></a>
    </div>
	<!--<div style="text-align: center;margin-right: 36%;margin-top: 20px;"></div>-->
	<?php //if(isset($_SESSION['userId']) && $_SESSION['userId'] != ""){ ?>
    <div class="collapse navbar-collapse pull-right" id="myNavbar" style="margin-right:112px;">
        <ul class="nav navbar-nav" style="margin-bottom: 7px;">
		<li style="margin-right: 445px;">
			<h5 style="font-weight: bold;font-size: 22px;">Virtual Job Fair July 2017</h5>
		</li>
            <?php if($eventStarted == 1){ ?>
			<!--<li style="    background: #108BF5;border: 1px solid #108BF5;    margin-right: 21px;">
                <a href="#" style="color:white;text-decoration: none;padding-left: 7px;cursor: default;">
                    <center>
					<span class="dashboard-invite-friends" style="font-weight:600;">Applications Received<br></span>
                    <span class="dashboard-invite-friends" id="applicationReceived" style="font-weight:600;"><?php //echo $totalApplicationReceived ?></span>
					</center>
                </a>
            </li>-->
			<?php if($showShortlisted == 1){ ?>
            <li  style="   background: #4F994C;border: 1px solid #4F994C;    margin-right: 21px;">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#" style="color:white;text-decoration: none;padding-left: 7px;cursor: default;">
                    <center>
					<span class="dashboard-video-profile" style="font-weight:600;">Applications Viewed<br></span>
                    <span class="dashboard-video-profile"  id="applicationShortlisted"  style="font-weight:600;"><?php echo $totalShortlistedApplication ?></span>
					</center>
                </a>
				
            </li>
			<?php } ?>
			<?php } ?>
			<?php if(isset($_SESSION['userId']) && $_SESSION['userId'] != ""){ ?>
            <li  style="border: 1px solid black;background:white;    margin-right: 21px;">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#" style="color:black;text-decoration: none;padding-left: 7px;">
                    <img src="<?php echo base_url();?>images/profile/profile-img.jpg" style="    width: 19px;"><span  style="font-weight:600;">&nbsp;<?php echo $_SESSION['userName']?></span>&nbsp;<span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li style="background:white;"><a href="javascript://"  data-toggle="modal" data-target="#EditprofileModal" class="EditprofileModalPopup" id="EditprofileModalPopup"  style="color:black;text-decoration: none;"><i class="fa fa-user fa-fw"></i>Edit Profile</a></li>
                    <li style="background:white;"><a href="#" style="color:black;text-decoration: none;" class="changePassword" ><i class="fa fa-key" aria-hidden="true"></i>&nbsp;Change Password</a></li>
                    <li style="background:white;"><a href="<?php echo base_url();?>users/logout" style="color:black;text-decoration: none;"><i class="fa fa-sign-out fa-fw"></i>Logout</a></li>
                </ul>
            </li>
			<?php } ?>
        </ul>
    </div>
	<?php //} ?>
  </div>
</nav>
    

    <div class="row" style="    background: #f8f8f8;margin-right: 0px;">
        <div id="tabs" style="background: #054F72;    height: 30px;padding: 0 169px;">
				
                <!--<ul class="nav nav-tabs nav-justified">
                    <li class="active pull-left" style="    width: 25%;cursor: pointer;margin-top: 36px;">
						<a href="<?php echo base_url()?>users" style="text-decoration:none;cursor:pointer;">
                       <table style="background: URL(images/blue1.png) no-repeat;background-size: 90px 90px;width: 90px;height: 90px;">
							<tbody>
								<tr><td cellpadding="0"><div style="text-align: center;"><i class="fa fa-home sub-menu-icon" style="font-size: 25px;color: white;"></i> </div></td>
								</tr>
								<tr><td cellpadding="0"><div style="text-align: center;margin-top: -15px;"><span class="texto_grande" style="color: white;font-size: 16px;">Home</span></div></td>
								</tr>
							</tbody>
						</table>
						</a>
                    </li>
					<li class="active pull-left" style="    width: 25%;cursor: pointer;margin-top: 36px;">
						<a href="<?php echo base_url()?>editProfile" style="text-decoration:none;cursor:pointer;">
                       <table style="background-size: 90px 90px;    width: 85px;;height: 90px;">
							<tbody>
								<tr><td cellpadding="0"><div style="text-align: center;"><img src="<?php echo base_url();?>images/icons/profile.png" style="width:25px;"></div></td>
								</tr>
								<tr><td cellpadding="0"><div style="text-align: center;margin-top: -15px;"><span class="texto_grande" style="color: white;font-size: 16px;">Edit Profile</span></div></td>
								</tr>
							</tbody>
						</table>
						</a>
                    </li>
					
					<li class="active pull-left" style="    width: 25%;cursor: pointer;margin-top: 36px;">
						<a href="javascript://" style="text-decoration:none;cursor:pointer;"   class="changePassword active" >
                       <table style="background-size: 90px 90px;width: 90px;height: 90px;">
							<tbody>
								<tr><td cellpadding="0"><div style="text-align: center;"><i class="fa fa-key" aria-hidden="true" style="font-size: 25px;color: white;"></i></div></td>
								</tr>
								<tr><td cellpadding="0"><div style="text-align: center;margin-top: -15px;"><span class="texto_grande" style="color: white;font-size: 16px;">Change Password</span></div></td>
								</tr>
							</tbody>
						</table>
						</a>
                    </li>
					
					<li class="active pull-left" style="    width: 25%;cursor: pointer;margin-top: 36px;">
						<a href="<?php echo base_url()?>users/logout" style="text-decoration:none;cursor:pointer;"   class="changePassword active" >
                       <table style="background-size: 90px 90px;width: 90px;height: 90px;">
							<tbody>
								<tr><td cellpadding="0"><div style="text-align: center;"><i class="fa fa-power-off" aria-hidden="true" style="font-size: 25px;color: white;"></i></div></td>
								</tr>
								<tr><td cellpadding="0"><div style="text-align: center;margin-top: -15px;"><span class="texto_grande" style="color: white;font-size: 16px;">Logout</span></div></td>
								</tr>
							</tbody>
						</table>
						</a>
                    </li>
                   
                    
					<!--<li class="pull-left"  style="width: 25%;    margin-top: 36px;">
                        <a style="    cursor: pointer;text-decoration: none;background-size: 98px 94px;height: 100%;margin-top: 11px;width: 100px;margin-left: 0px;"  class="changePassword active" >
                            <i class="fa fa-key" aria-hidden="true" style="font-size: 40px;"></i><br>
                            <span class="texto_grande">Change Password</span>
                        </a>
                    </li>-->
					
                    
                <!--</ul>-->
        <!-- /.col-lg-12 -->
        </div>
        <div class="container-full" style="background: white;">
            <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #054F72;height: 33px;">
                
            </div>
            <div  class="col-md-8 col-lg-8 col-sm-8 pull-left" style="width: 80%;background: #EB9B2C ;height: 33px;">
                <h5 style="font-weight: bold;font-size: 18px;">Welcome to <?php echo $eventContent->eventName;?></h5>
				
            </div>
            <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #054F72;height: 33px;">
                
            </div>
            
        </div>

        <div class="container-full" style="background: white;">
            <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #f8f8f8;height: 30px;">
            </div>

            <div  class="col-md-8 col-lg-8 col-sm-8 pull-left" style="    margin-bottom: 20px;padding-top: 20px;width: 80%;background: white ;">
                
                <div class="col-lg-12">
                <!-- /.row -->

                    <!-- /.panel -->
                    <div class="panel panel-default" style="border-color: ivory">
                        
                        <!-- /.panel-heading -->
                        <div class="panel-body" style="font-size: 14px !important;">
                            <div class="col-lg-7" style="    width: 70%;">
                                <?php echo $eventContent->description;?>
								<center style="    font-size: 16px;font-weight: bolder;margin-top: 37px;">Total Vacancies:&nbsp;<?php echo $totalVacancies ?></center>
								<?php if($eventStarted == 0){ ?>
							<div class="col-lg-12" style="margin-top:20px;">
							<center style="    padding-top: 7px;">
								<div style="width:100%;background:#108bf5;color: white;    box-shadow: 2px 2px 2px 1px black;">
									<div style="font-size:16px;width:50%;font-weight:bold;padding:10px;">Job Fair Starts In</div>
									<div id="defaultCountdown" style=" width: 50%;   height: 77px;"></div>
								</div>
							</center>
							</div>
							<?php }else{ ?>
							<?php if(!empty($content)){ ?>
							
                            <div class="col-lg-12" style="margin-top:20px;">
                                <table width="100%" class="table table-striped table-hover" id="dataTables-example">
                                    <thead style="    background: #69686D;color: white;">
                                        <th style="width:23%;">Company</th>
                                        <th style="width:23%;">Job</th>
                                        <th style="width:34%;">Location</th>
                                    </thead>
                                    <tbody>
                                        <?php 
										foreach($content as $row) {
																				
										?>
                                        <tr>
											<td><?php echo $row->company; ?></td>
                                            <td><a href="<?php echo base_url()."job/details/".$row->fjCode ?>"><?php echo $row->title; ?></a></td>
                                            <td><?php echo $row->locations; ?></td>
                                            
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
							<?php } 
							} ?>
                            </div>
							<div class="col-lg-3" style="float: right;width: 30%;">
							<div style="background: #f7f5f4;">
                                <div style="font-size:16px;text-align: center;">Participating Companies</div>
                                <div style="    margin-top: 25px;">
								<ul style="    list-style: none;    padding-left: 10px;">
								<li style="padding-bottom: 10px;text-align: center;">
									<img src="https://VDOHire.com/images/hdfc-life-logo.png" style="width:27%;">
								</li>
								<li style="padding-bottom: 10px;text-align: center;"><img src="https://VDOHire.com/images/future-generali-logo.png" style="width:27%"></li>
								<li style="padding-bottom: 10px;text-align: center;"><img src="https://VDOHire.com/images/Just-Dial-Logo.png" style="width:27%"></li>
								<li style="padding-bottom: 10px;text-align: center;"><img src="https://VDOHire.com/images/club-mahindra-new-logo.png" style="width:27%"></li>
								<li style="padding-bottom: 10px;text-align: center;    padding-top: 20px;"><img src="https://VDOHire.com/images/ratnakar_bank.png" style="width:27%"></li>
                                    <li style="padding-bottom: 10px;text-align: center;    padding-top: 20px;"><img src="https://VDOHire.com/images/peoplestrong.png" style="height:18px;"></li>
								<li style="padding-bottom: 10px;text-align: center;    padding-top: 20px;"><img src="https://VDOHire.com/images/kwench-logo.png" style="width:27%"></li>
								<li style="padding-bottom: 10px;text-align: center;    padding-top: 20px;"><img src="https://VDOHire.com/images/investors-clinic.png" style="width:27%"></li>
								<li style="padding-bottom: 10px;text-align: center;    padding-top: 20px;"><img src="https://VDOHire.com/images/smartStudentLogo.png" style="width:27%"></li>
								
								<li style="padding-bottom: 10px;text-align: center;    padding-top: 20px;"><img src="https://VDOHire.com/images/pitcrew.png" style="width:39%"></li>
								
								
								<li style="padding-bottom: 10px;text-align: center;    padding-top: 27px;"><img src="https://VDOHire.com/images/Quest_logo.png" style="width:27%"></li>
								<li style="padding-bottom: 10px;text-align: center;    padding-top: 24px;"><img src="https://VDOHire.com/images/synergy-logo.png" style="width:27%"></li>
								
								
								
								</ul>
								</div>
                            </div>
                            </div>
                            
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.col-lg-8 -->
                </div>
                <!-- /.row -->
            </div>

            <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #f8f8f8;height: 30px;">
            </div>
        </div>
    <!-- /#page-wrapper -->
    </div>
</div>


    <!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url(); ?>theme/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>theme/js/bootbox.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_url(); ?>theme/vendor/metisMenu/metisMenu.min.js"></script>

<!-- DataTables JavaScript -->
<script src="<?php echo base_url(); ?>theme/vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>theme/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>theme/vendor/datatables-responsive/dataTables.responsive.js"></script>



<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url(); ?>theme/dist/js/sb-admin-2.js"></script>
<script src="<?php echo base_url(); ?>theme/js/formValidation.min.js"></script>
<script src="<?php echo base_url(); ?>theme/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>js/min/event-jobs.min.js"></script>
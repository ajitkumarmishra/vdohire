<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Name : Users Controller
 * Description : Used to handle all the jobseeker related data
 * @author Synergy
 * @createddate : April 3, 2016
 * @modificationlog : Initializing the controlelr
 * @change on Mar 24, 2017
 */
class Discussion extends MY_Controller {

    public $allNotificationList = array();
    /**
     * Responsable for inherit the parent connstructor
     * @return void
     */
    function __construct() {
        parent::__construct();

        $this->load->library('session');
        $this->load->helper('fj');
        $this->load->helper('jwt');
        
        $this->getEditProfileData();
        //Use to get Locations for search Job
        $this->searchLocationList = getLocationCities();

        $userRealData = $this->session->get_userdata();
        //print_r($userRealData);exit;

        if (isset($userRealData['userId'])) {
            /* Get Notification List for user */
            $userId = $userRealData['userId'];

            $statictoken = 'ecbcd7eaee29848978134beeecdfbc7c';

            if($this->config->item('HTTPSENABLED') == 1){
               $base_url = 'https://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
            }else{
               $base_url = 'http://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
            }

            $json = array("token" => $statictoken, "methodname" => 'getUserNotifications', 'userId' => $userId);                                                                    
            $data_string = json_encode($json);                                                                         
                                                                                                                                 
            $ch = curl_init($base_url);                                                                      
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);                                                                      
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
                'Content-Type: application/json',                                                                                
                'Content-Length: ' . strlen($data_string))                                                                       
            );                                                                                                                   
                                                                                                                                 
            $result = curl_exec($ch);
            $allResult = json_decode($result);
            $this->allNotificationList = $allResult->data;

            /* End of get notification list fo user */
        }
    }

    /**
     * Function Naem : index
     * Discription : Use to display login page for job seeker
     * @author Synergy
     * @param none
     * @return render data into main view page
     */

    function index() {
        $session = $this->session->get_userdata();
        $token = $session['token'];
        $userEmail = $session['userEmail'];
        $userName = $session['userName'];
        $userId = $session['userId'];
		
		$session = $this->session->get_userdata();
        $statictoken = 'ecbcd7eaee29848978134beeecdfbc7c';

        if($this->config->item('HTTPSENABLED') == 1){
           $base_url = 'https://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
        }else{
           $base_url = 'http://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
        }

		$data = array("token" => $statictoken, "methodname" => 'getForumList', "userId" => $userId);                                                                    
        $data_string = json_encode($data);                
		
        $ch = curl_init($base_url);                                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);                                                                      
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data_string))                                                                       
        );                                                                                                                   
                                                                                                                             
        $result = curl_exec($ch);
		
        $discussionForumListArray = json_decode($result);
        $discussionForumList = $discussionForumListArray->data;
		
		$data['discussionForumList'] = $discussionForumList;
		
        if (isset($userId) && $userId != "") {
            $data['city'] = $this->searchLocationList;
            $data['allNotificationList'] = $this->allNotificationList;
            $data['token'] = $userId;
            $data['userName'] = $userName;
            $data['email'] = $userEmail;
            $data['main_content'] = 'discussion';
            $this->load->view('fj-mainpage-withMenu', $data);
        } else {
            redirect("https://" . $_SERVER["HTTP_HOST"] . "", 'refresh');
        }

        //echo 'hello';exit;
    }
	
	function details() {
        $session = $this->session->get_userdata();
        $token = $session['token'];
        $userEmail = $session['userEmail'];
        $userName = $session['userName'];
        $userId = $session['userId'];
		if(isset($_POST['sortBy']) && $_POST['sortBy'] != ""){
			$sortBy = $_POST['sortBy'];
		}else{
			$sortBy = 'DESC';
		}
		
		$forumId = $this->uri->segment(3); // 1stsegment
		
		$session = $this->session->get_userdata();
        $statictoken = 'ecbcd7eaee29848978134beeecdfbc7c';

        if($this->config->item('HTTPSENABLED') == 1){
           $base_url = 'https://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
        }else{
           $base_url = 'http://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
        }

		$data = array("token" => $statictoken, "methodname" => 'getForumDetail',"forumId" => $forumId, "userId" => $userId, "sortby" => $sortBy );                                                                    
        $data_string = json_encode($data);           
		
        $ch = curl_init($base_url);                                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);                                                                      
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data_string))                                                                       
        );                                                                                                                   
                                                                                                                             
        $result = curl_exec($ch);
		
        $discussionForumDetailsArray = json_decode($result);
        $discussionForumDetails = $discussionForumDetailsArray->data;
		
		$data['discussionForumDetails'] = $discussionForumDetails;
		$data['forumId'] = $forumId;
		
        if (isset($userId) && $userId != "") {
            $data['city'] = $this->searchLocationList;
            $data['allNotificationList'] = $this->allNotificationList;
            $data['token'] = $userId;
            $data['userName'] = $userName;
            $data['email'] = $userEmail;
            $data['sortBy'] = $sortBy;
            $data['main_content'] = 'forum-details';
            $this->load->view('fj-mainpage-withMenu', $data);
        } else {
            redirect("https://" . $_SERVER["HTTP_HOST"] . "", 'refresh');
        }

        //echo 'hello';exit;
    }
	
	/*function answer() {
        $this->load->library('session');
        $this->load->helper('cookie');

        $session = $this->session->get_userdata();
        $token = $session['token'];
        $userEmail = $session['userEmail'];
        $userName = $session['userName'];
        $userId = $session['userId'];
		
		$topicId = $this->uri->segment(3); // 1stsegment
		
		$session = $this->session->get_userdata();
        $statictoken = 'ecbcd7eaee29848978134beeecdfbc7c';
        $base_url = 'https://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';
		$data = array("token" => $statictoken, "methodname" => 'getKnowledgeCenterDetail',"topicId" => $topicId);                                                                    
        $data_string = json_encode($data);                
		
        $ch = curl_init($base_url);                                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data_string))                                                                       
        );                                                                                                                   
                                                                                                                             
        $result = curl_exec($ch);
		
        $knowledgeDataArray = json_decode($result);
        $knowledgeData = $knowledgeDataArray->data;
		
		$data['knowledgeData'] = $knowledgeData;
		
        if (isset($userId) && $userId != "") {
            $data['token'] = $userId;
            $data['userName'] = $userName;
            $data['email'] = $userEmail;
            $data['main_content'] = 'knowledgeDataForTopic';
            $this->load->view('fj-mainpage-withMenu', $data);
        } else {
            redirect("https://" . $_SERVER["HTTP_HOST"] . "", 'refresh');
        }

        //echo 'hello';exit;
    }*/

}
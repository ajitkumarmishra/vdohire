
<?php
//Get Notification unread count
$getNotificationUnreadCount = 0;

//print_r($allNotificationList);exit;
foreach ($allNotificationList as $notiCount) {
    if($notiCount->status == 0) {
        $getNotificationUnreadCount++;
    }
}
//End of get Notification unread count
?>

    <div class="container-full" style="background: white;color:white;">
        <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #054F72;height: 33px;">
            
        </div>
        <div  class="col-md-8 col-lg-8 col-sm-8 pull-left" style="font-size: 17px;color: white;font-weight: bold;width: 80%;background: #EB9B2C ;">
            <div><h2 style="font-size:16px;color:black;font-weight:bold;">Knowledge Center</h2></div>
        </div>
        <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #054F72;height: 33px;">
        </div>
    </div>
    <div style="clear:both"></div>
    <div class="container-full" style="background: white;">
        <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #f8f8f8;height: 30px;">
        </div>
        <div  class="col-md-8 col-lg-8 col-sm-8 pull-left" style="border:1px solid lightgrey;padding-bottom: 23px;margin-bottom: 20px;padding-top: 20px;width: 80%;background: white ;">
            <div class="col-lg-12">
                <div class="row">
                    <form class="form-horizontal">
                        <fieldset>
                            <div class="form-group">
                                <label class="col-xs-3"></label>
                                
                            </div>
                            <div class="form-group">
                                <label class="col-xs-3"></label>
                                <div class="col-xs-12">
								<?php
								foreach($knowledgeData as $data){ ?>
									<div class="col-xs-4">
                                        <label style="    width: 100%;">
                                            <a class="btn btn-primary" href="<?php echo base_url()?>knowledge-center/category/<?php echo $data->id ?>" id="manuallyEditProfile" name="editProfile" value="manuallyEditProfile" type="radio" style="    width: 100%;     height: 100px;   padding-top: 38px;text-decoration: none;"><?php echo $data->category ?></a>
                                        </label>
                                    </div>
								<?php }
								?>
                                    
                                </div>
                            </div>
                            
                        </fieldset>
                    </form>
                </div>
                <!-- /.row -->
            </div>
        </div>
    <!-- /#page-wrapper -->
    </div>
</div>
<!-- /#wrapper -->



<script src="<?php echo base_url(); ?>theme/vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url(); ?>theme/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>theme/js/bootbox.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->

<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url(); ?>theme/js/formValidation.min.js"></script>
<script src="<?php echo base_url(); ?>theme/js/bootstrap.min.js"></script>

<!-- Edit profile js JavaScript -->
<script src="<?php echo base_url(); ?>js/min/editprofile.min.js"></script>
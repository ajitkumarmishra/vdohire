<style>
.scrollbar
{
	float: left;
	background: #F5F5F5;
	overflow-y: scroll;
	margin-bottom: 25px;
}

#style-2::-webkit-scrollbar-track
{
	-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
	border-radius: 10px;
	background-color: #FFF;
}

#style-2::-webkit-scrollbar
{
	width: 8px;
	background-color: #FFF;
}

#style-2::-webkit-scrollbar-thumb
{
	border-radius: 10px;
	-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
	background-color: #000;
}
.speech{
	background: URL(<?php echo base_url()?>images/icons/comment.png) no-repeat;
	    width: 100%;
    height: 96px;
    background-size: 100% 80px;
    text-align: center;
    padding-top: 10px;
	padding-left: 20px;
}
</style>

    <div class="container-full" style="background: white;color:white;">
        <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #054F72;height: 33px;">
            
        </div>
        <div  class="col-md-8 col-lg-8 col-sm-8 pull-left" style="font-size: 17px;color: white;font-weight: bold;width: 80%;background: #EB9B2C ;">
            <div class="pull-left"><h2 style="font-size:16px;color:black;font-weight:bold;"><a href="<?php echo base_url()."discussion-forum"; ?>" >Discussion Forum</a>&nbsp;&nbsp;->&nbsp;&nbsp;<?php echo $discussionForumDetails[0]->heading  ?>&nbsp;&nbsp;<?php echo "(".count($discussionForumDetails).")"; ?></h2></div>
			
			<div class="pull-left" style="    color: black;padding: 12px;margin-left: 14%;">Discussion started by: <?php echo $discussionForumDetails[0]->createdBy  ?></div>
			
			<div class="pull-right" style="    color: black;padding: 12px;">
			<form id="sortByForumForm" action="<?php echo base_url()."discussion-forum/details/".$forumId ?>" method="post">
			<select onchange="postThisForm();" name="sortBy" style="    font-size: 15px;border-radius: 13px;padding: 1px;">
			<option value="ASC" <?php echo ($sortBy == "ASC")?"selected":""; ?>>Date Asc</option>
			<option value="DESC" <?php echo ($sortBy == "DESC")?"selected":""; ?>>Date Desc</option>
			</select>
			
			</form>
			</div>
			<div style="clear:both;"></div>
        </div>
        <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #054F72;height: 33px;">
        </div>
    </div>
    <div style="clear:both"></div>
    <div class="container-full" style="background: white;">
        <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #f8f8f8;height: 30px;">
        </div>
        <div  class="col-md-8 col-lg-8 col-sm-8 pull-left" style="border:1px solid lightgrey;padding-bottom: 23px;margin-bottom: 20px;width: 80%;background: white ;">
            <div class="col-lg-12">
                <div class="row">
                            
                                <div class="col-xs-12" style="    padding-top: 20px;">
								<input type="hidden" name="forumId" value="<?php echo $forumId?>" id="forumId">
								<div class="col-xs-9 scrollbar" id="style-2" style="border-right: 1px solid silver;    height: 500px;
    overflow: auto;">
								<?php
								foreach($discussionForumDetails as $data){ ?>
									<div class="col-xs-12" style="    padding-bottom: 23px;padding-top: 23px;">
									
									<div class="col-xs-3">
											<center>
											<div><img class="img-circle" style="    width: 26%;" src="<?php echo base_url()."images/profile/profile-img.jpg"//echo $data->thumbnail ?>"></div>
											
											<div style="    color: black;font-weight: bold;    padding-top: 10px;"><?php echo $data->username ?></div>
											<div style="    color: grey;padding-top: 10px;font-size: 12px;"><?php echo date('d M, Y', strtotime($data->createdAt)); ?></div>
											</center>
											
											
									
									</div>
									<div class="col-xs-9">
                                            <div class=" speech"  id="manuallyEditProfile" name="editProfile" value="manuallyEditProfile" type="radio">
											
											<?php echo ucfirst($data->topic) ?></div>
                                    </div>
									<div style="clear:both"></div>
									</div>
								<?php }
								?>
								</div>
								<div class="col-xs-3">
								<div class="col-xs-12"  style="    padding-bottom: 23px;padding-top: 23px;">
									<div><textarea  name="replyForum" id="replyForum" type="text" placeholder="Write your comment!"  rows="4" cols="150" style="padding: 20px;border: 1px solid grey;height: 232px;width: 90%;border-radius: 14px;" /></textarea>
									</div>
									<div><button class="btn" onclick="replyToForum()" style="background:#11AAF0;    float: right;margin-right: 10%;    width: 88%;    color: white;    margin-top: 4px;"><b>POST</b></button></div>
									<div style="clear:both"></div>
                                </div>
								</div>
								<div style="clear:both"></div>
								
								
                                </div>
                            
                </div>
                <!-- /.row -->
            </div>
        </div>
    <!-- /#page-wrapper -->
    </div>
</div>
<!-- /#wrapper -->



<script src="<?php echo base_url(); ?>theme/vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url(); ?>theme/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>theme/js/bootbox.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->

<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url(); ?>theme/js/formValidation.min.js"></script>
<script src="<?php echo base_url(); ?>theme/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>js/min/forum-details.min.js"></script>
<script>

</script>
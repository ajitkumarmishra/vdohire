
<?php

//End of get Notification unread count
?>


    <div class="container-full" style="background: white;color:white;">
        <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #054F72;height: 33px;">
            
        </div>
        <div  class="col-md-8 col-lg-8 col-sm-8 pull-left" style="font-size: 17px;color: white;font-weight: bold;width: 80%;background: #EB9B2C ;">
            <div class="pull-left"><h2 style="font-size:16px;color:black;font-weight:bold;margin-top:10px;">Discussion Forum</h2></div>
            <div class="pull-right">
					<a title="Add Forum" class="btn btn-primary" href="javascript://" data-toggle="modal" data-target="#createForum" style="    padding:5px; font-size: 13px;margin-top: 4px;background: #11AAF0;border: none;   margin-left: 10px;color:white;    text-decoration: none;    padding-left:5px;padding-right:5px;"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp;Add Forum</a>
			</div>
        </div>
        <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #054F72;height: 33px;">
        </div>
    </div>
    <div style="clear:both"></div>
    <div class="container-full" style="background: white;">
        <div  class="col-md-2 col-lg-2 col-sm-2 pull-left" style="width: 10%;background: #f8f8f8;height: 30px;">
        </div>
        <div  class="col-md-8 col-lg-8 col-sm-8 pull-left" style="border:1px solid lightgrey;padding-bottom: 23px;margin-bottom: 20px;padding-top: 20px;width: 80%;background: white ;">
            <div class="col-lg-12">
                <div class="row">
                    <form class="form-horizontal">
                        <fieldset>
                            <div class="form-group">
                                <div class="col-xs-12">
								<table width="100%" class="table table-striped table-hover" id="dataTables-example">
                                    <thead style="    background: #69686D;color: white;">
                                        <th style="width:13%;">Moderator</th>
                                        <th style="width:23%;">Topic</th>
                                        <th style="width:34%;">Counts</th>
                                        <th style="width:34%;">Last Reply</th>
                                    </thead>
									<tbody>
								<?php
								$i = 0;
								foreach($discussionForumList as $data){ ?>
									<?php 
									if($i%2  == 0){
													
										$backGroundColor = "#fff";
									}else{
										$backGroundColor = "#E5E5E5";
									}
									$i++;
									?>
									
									
									<tr  style="background-color:<?php echo $backGroundColor; ?>">
									<td >
										
											
                                        <div>
											<div>
											<img class="img-circle" style="    width: 26%;" src="<?php echo base_url()."images/profile/profile-img.jpg"//echo $data->thumbnail ?>">
											
											</div>
											
											<div style="    color: grey;font-weight: bold;    padding-top: 10px;"><?php echo $data->username ?></div>
											
										</div>
									</td>
									<td>
										<div style="    padding-top: 15px;"><?php echo ucfirst($data->topic) ?></div>
									</td>
									<td>									
											<div style="    padding-top: 15px;">
											Posts: <?php echo count($data->reply) ?>
											</div>
									</td>		
                                   
									<td>
										<div class="pull-left" style="    width: 70%;    padding-top: 15px;">
											<div><?php echo ucfirst(isset($data->reply[0]->description)?$data->reply[0]->description:"") ?></div>
											<div style="    color: grey;padding-top: 10px;font-size: 12px;"><?php echo date('d M, Y', strtotime($data->createdAt)); ?></div>
										</div>
										
										
										<div class="pull-right" style="    width: 30%;    padding-top: 15px;">
										<a title="View Forum" class="btn btn-primary" href="<?php echo base_url()."discussion-forum/details/".$data->forumId; ?>" style="float:right;	padding:7px; font-size: 13px;background: #11AAF0;border: none;color:white;    text-decoration: none;"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>&nbsp;&nbsp;VIEW</a>
										</div>
										<div style="clear:both"></div>
									</td>
									</tr>
									
									
								<?php }
								?>
								</tbody>
                                    </table>
                                    
                                </div>
                            </div>
                            
                        </fieldset>
                    </form>
                </div>
                <!-- /.row -->
            </div>
        </div>
    <!-- /#page-wrapper -->
    </div>
</div>
<!-- /#wrapper -->
<div class="modal fade" id="createForum">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
			<b>Forum Topic</b>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                
            </div>
            <div class="modal-body" style="    padding-top: 0px;">
				<input type="text" class="form-control" name="heading" id="heading" value="">
            </div>
            <div class="modal-footer">
				
                <button type="button" onclick="createForum()" class="btn btn-default pull-right" style="background: #054F72; color: white;">SUBMIT</button>
            </div>  
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<script src="<?php echo base_url(); ?>theme/vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url(); ?>theme/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>theme/js/bootbox.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->

<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url(); ?>theme/js/formValidation.min.js"></script>
<script src="<?php echo base_url(); ?>theme/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>js/min/discussion.min.js"></script>

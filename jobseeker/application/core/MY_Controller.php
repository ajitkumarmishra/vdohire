<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/* load the MX_Controller class */
require APPPATH."third_party/MX/Controller.php";

class MY_Controller extends MX_Controller {
	function __construct() {
        parent::__construct();
	}
	
	public function getEditProfileData(){
		$statictoken = 'ecbcd7eaee29848978134beeecdfbc7c';
        $base_url = 'https://'.$_SERVER['HTTP_HOST'].'/jobseeker/Fjapi';

        //$this->load->library('session');
        $session = $this->session->get_userdata();
		
        if(isset($session['userId']) && !empty($session['userId'])) {
            $token =  isset($session['userId'])?$session['userId']:"";
            $userEmail = isset($session['userEmail'])?$session['userEmail']:"";
            $userName = isset($session['userName'])?$session['userName']:"";
            $userId = isset($session['userId'])?$session['userId']:"";
            
            //Get Basic Information
            $data = array("token" => $statictoken, "methodname" => 'getBasicInformation', 'userId' => $userId);
            $data_string = json_encode($data);                                             
            $ch = curl_init($base_url);                                                                      
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
                'Content-Type: application/json',                                                                                
                'Content-Length: ' . strlen($data_string))                                                                       
            );

            $result = curl_exec($ch);
            $allResult = json_decode($result);
    		$basic = $allResult->basicInfo;
            if(!empty($basic)) {
                $data['basic'] = $basic[0];
            }
            else {
                $data['basic'] = array();
            }
        } else {
            $data['basic'] = array();
        }
        //End of Get Basic Information


        //Get Educational Information
        if(isset($session['userId']) && !empty($session['userId'])) {
            $educationDdata = array("token" => $statictoken, "methodname" => 'getEducationalInformation', 'userId' => $userId);
            $education_data_string = json_encode($educationDdata);                                             
            $education_ch = curl_init($base_url);                                                                      
            curl_setopt($education_ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
            curl_setopt($education_ch, CURLOPT_POSTFIELDS, $education_data_string);                                                                  
            curl_setopt($education_ch, CURLOPT_RETURNTRANSFER, true);                                                                      
            curl_setopt($education_ch, CURLOPT_HTTPHEADER, array(                                                                          
                'Content-Type: application/json',                                                                                
                'Content-Length: ' . strlen($education_data_string))                                                                       
            );

            $education_result = curl_exec($education_ch);
            $educationResultObject = json_decode($education_result);
            $educationalInfo = $educationResultObject->educationalInfo;
            $data['educationalInfo'] = $educationalInfo;
        } else {
            $data['educationalInfo'] = array();
        }
        //End of Get Educational Information

        //Get Employment Hisotry
        if(isset($session['userId']) && !empty($session['userId'])) {
            $employmentData = array("token" => $statictoken, "methodname" => 'getEmploymentInformation', 'userId' => $userId);
            $employment_data_string = json_encode($employmentData);                                             
            $employment_ch = curl_init($base_url);                                                                      
            curl_setopt($employment_ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
            curl_setopt($employment_ch, CURLOPT_POSTFIELDS, $employment_data_string);                                                                  
            curl_setopt($employment_ch, CURLOPT_RETURNTRANSFER, true);                                                                      
            curl_setopt($employment_ch, CURLOPT_HTTPHEADER, array(                                                                          
                'Content-Type: application/json',                                                                                
                'Content-Length: ' . strlen($employment_data_string))                                                                       
            );

            $employment_result = curl_exec($employment_ch);
            $employmentResultObject = json_decode($employment_result);
            $employmentInfo = $employmentResultObject->employmentInfo;
            $data['employmentInfo'] = $employmentInfo;
        } else {
            $data['employmentInfo'] = array();
        }
        //print_r($data['employmentInfo']);exit;
        //End of Get Employment History

        //Get Course List
        $courseData = array("token" => $statictoken, "methodname" => 'getCourses');
        $course_data_string = json_encode($courseData);                                             
        $course_ch = curl_init($base_url);                                                                      
        curl_setopt($course_ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($course_ch, CURLOPT_POSTFIELDS, $course_data_string);                                                                  
        curl_setopt($course_ch, CURLOPT_RETURNTRANSFER, true);                                                                      
        curl_setopt($course_ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($course_data_string))                                                                       
        );

        $course_result = curl_exec($course_ch);
        $courseResultObject = json_decode($course_result);
        $courseData = $courseResultObject->data;
        $data['courseData'] = $courseData;
        //End of Get Course List

        //Get Course List
        $universityData = array("token" => $statictoken, "methodname" => 'getUniversityList');
        $university_data_string = json_encode($universityData);                                             
        $university_ch = curl_init($base_url);                                                                      
        curl_setopt($university_ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($university_ch, CURLOPT_POSTFIELDS, $university_data_string);                                                                  
        curl_setopt($university_ch, CURLOPT_RETURNTRANSFER, true);                                                                      
        curl_setopt($university_ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($university_data_string))                                                                       
        );

        $university_result = curl_exec($university_ch);
        $universityResultObject = json_decode($university_result);
        $universityData = $universityResultObject->data;
        $data['universityData'] = $universityData;
        //End of Get Course List


        //Get other information data
        if(isset($session['userId']) && !empty($session['userId'])) {
            $otherInfoData = array("token" => $statictoken, "methodname" => 'getOtherInformation', 'userId' => $userId);
            $otherinfo_data_string = json_encode($otherInfoData);                                             
            $otherinfo_ch = curl_init($base_url);                                                                      
            curl_setopt($otherinfo_ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
            curl_setopt($otherinfo_ch, CURLOPT_POSTFIELDS, $otherinfo_data_string);                                                                  
            curl_setopt($otherinfo_ch, CURLOPT_RETURNTRANSFER, true);                                                                      
            curl_setopt($otherinfo_ch, CURLOPT_HTTPHEADER, array(                                                                          
                'Content-Type: application/json',                                                                                
                'Content-Length: ' . strlen($otherinfo_data_string))                                                                       
            );

            $otehrinfo_result = curl_exec($otherinfo_ch);
            $otherInfoResultObject = json_decode($otehrinfo_result);
            $otherInfo = $otherInfoResultObject->otherInfo[0];
            $data['otherInfo'] = $otherInfo;
        } else {
            $data['otherInfo'] = array();
        }
        //End of Get other information data

        //Get social information data

        if(isset($session['userId']) && !empty($session['userId'])) {
            $socialInfoData = array("token" => $statictoken, "methodname" => 'getSocialInformation', 'userId' => $userId);
            $social_data_string = json_encode($socialInfoData);                                             
            $social_ch = curl_init($base_url);                                                                      
            curl_setopt($social_ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
            curl_setopt($social_ch, CURLOPT_POSTFIELDS, $social_data_string);                                                                  
            curl_setopt($social_ch, CURLOPT_RETURNTRANSFER, true);                                                                      
            curl_setopt($social_ch, CURLOPT_HTTPHEADER, array(                                                                          
                'Content-Type: application/json',                                                                                
                'Content-Length: ' . strlen($social_data_string))                                                                       
            );

            $social_result = curl_exec($social_ch);
            $socialResultObject = json_decode($social_result);
            $socialInfo = $socialResultObject->socialInfo[0];
            $data['socialInfo'] = $socialInfo;
        } else {
            $data['socialInfo'] = array();
        }
        //End of Get social information data

		//get Resume list
        if(isset($session['userId']) && !empty($session['userId'])) {
    		$dataResume = array("token" => $statictoken, "methodname" => 'getUserResumes', 'userId' => $userId);                                                                    
    		$dataResumestring = json_encode($dataResume);                                                                                   
    																															 
    		$ch = curl_init($base_url);                                                                      
    		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
    		curl_setopt($ch, CURLOPT_POSTFIELDS, $dataResumestring);                                                                  
    		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
    		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
    			'Content-Type: application/json',                                                                                
    			'Content-Length: ' . strlen($dataResumestring))                                                                       
    		);                                                                                                                   
    																															 
    		$Resumeresult = curl_exec($ch);
    		$allResumeResult = json_decode($Resumeresult);
    		$resumeList = $allResumeResult->resumeList;
    		$data['resumelist'] = $resumeList;
        } else {
            $data['resumelist'] = array();
        }
		//end of resume list
		
		//video Resume list
        if(isset($session['userId']) && !empty($session['userId'])) {
    		$jsonVideo = array("token" => $statictoken, "methodname" => 'auditionHistory', 'userId' => $userId);
    		
    		$data_string_video = json_encode($jsonVideo);                                                                                   
    																															 
    		$ch_visitor = curl_init($base_url);                                                                      
    		curl_setopt($ch_visitor, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
    		curl_setopt($ch_visitor, CURLOPT_POSTFIELDS, $data_string_video);                                                                  
    		curl_setopt($ch_visitor, CURLOPT_RETURNTRANSFER, true);                                                                      
    		curl_setopt($ch_visitor, CURLOPT_HTTPHEADER, array(                                                                          
    			'Content-Type: application/json',                                                                                
    			'Content-Length: ' . strlen($data_string_video))                                                                       
    		);                                                                                                                   
    		$curlVideoResult = curl_exec($ch_visitor);
    		$VideoallResult = json_decode($curlVideoResult);
    		$VideoresumeList = $VideoallResult->data;
    		$data['Videoresumelist'] = $VideoresumeList;
        } else {
            $data['Videoresumelist'] = array();
        }
		//echo "<pre>";print_r($data['resumelist']);die;
			
		
		$data = array(
			'basic' => $data['basic'],
			'educationalInfo' => $data['educationalInfo'],
			'employmentInfo' => $data['employmentInfo'],
			'courseData' => $data['courseData'],
			'universityData' => $data['universityData'],
			'otherInfo' => $data['otherInfo'],
			'socialInfo' => $data['socialInfo'],
			'resumelist' => $data['resumelist'],
			'Videoresumelist' => $data['Videoresumelist'],
		
		);
		$this->load->vars($data);
	}
}

?>
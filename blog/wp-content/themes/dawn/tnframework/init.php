<?php
/**
 * TN wordpress Framework
 */
define('TNFRAMEWORK_CORE' , get_template_directory() . '/tnframework/core');
define('TNFRAMEWORK_CLASS' , get_template_directory() . '/tnframework/class');
require_once(TNFRAMEWORK_CORE  . '/core.func.php');
require_once(TNFRAMEWORK_CORE . '/core.backend.php');
require_once(TNFRAMEWORK_CLASS  . '/class.init.php');
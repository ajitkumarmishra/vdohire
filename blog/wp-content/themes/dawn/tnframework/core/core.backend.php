<?php

/*------------------------------------------------------------
    Theme Backend Functions
--------------------------------------------------------------*/


/*------------------------------------------------------------
  Get Pages By Page Template

  * this function will return an array of pages
--------------------------------------------------------------*/
function tnframework_list_pages($template){
    if($template != '')
    {
        $return = array();
        $pages = get_posts(array(
            'post_type' => 'page',
            'meta_key' => '_wp_page_template',
            'meta_value' => $template . '.php'
          ));
        foreach ($pages as $page) {
            $return[$page->ID] = $page->post_title;
        }

        return $return;
    }
}


/* -------------------------------------------------------------*/
/* Print Custom Widget Fields */
/* -------------------------------------------------------------*/
if(!function_exists('tnframework_prepare_widget_fields'))
{
     function tnframework_prepare_widget_fields($fields , $instance , $object){

        if(!empty($fields) && is_array($fields))
        {

                $return = '';

                foreach($fields as $field)
                {

                    if(isset($field['id'])){

                            // Field Value
                            $fieldValue = isset($instance[$field['id']]) ? $instance[$field['id']] : '';
                            $name = $object->get_field_name($field['id']);
                            $id = $object->get_field_id($field['id']);

                            // Return
                            $return .= '<p>';
                            $return .= '<label for="'.esc_attr($id).'"><b>'.$field['title'].'</b></label><br />';

                            switch($field['type'])
                            {
                                        case 'text' :
                                            $return .= '<input type="text" id="'.esc_attr($id).'" class="widefat" name="'.esc_attr($name).'" value="'.esc_attr($fieldValue).'">';
                                        break;

                                        case 'textarea' :
                                            $return .= '<textarea class="widefat" name="'.esc_attr($name).'" id="'.esc_attr($id).'">'.esc_attr($fieldValue).'</textarea>';
                                        break;

                                        case 'check' :

                                        break;

                                        case 'select' :


                                        break;
                            }

                            // Print Backend
                            $return .= '<br /><span class="widefat">'.$field['description'].'</span>';
                            $return .= '</p>';
                    }
                }

                echo $return;
        }

     }
}
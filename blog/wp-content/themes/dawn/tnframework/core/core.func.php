<?php
$dawn_theme_options = dawn_theme_settings();

/* --------------------------------------------------------------
Fix for some common WP errors
And additional functions , i.e like button , social icons ..
-------------------------------------------------------------- */

/* - Update Data - */
if (!get_option('tnframework_data') || get_option('tnframework_data') == '') {
	update_option('tnframework_data', array());
}

// /* --------------------------------------------------------------
//   Serialize Options
//  '_tnframework_' main prefix for all tnf options ,
//  i.e _tnframework_vc_settings
// -------------------------------------------------------------- */
function tnframework_update_data($section, $config) {

	$data_param = '_tnframework_' . $section;
	$data = get_option($data_param);
	update_option($data_param, array_merge($data, $config));

}

/* --------------------------------------------------------------
Return Shortcode Attributes

 * this function will check the content for the given shortcode
 * will return an array of shortcode properties and values
 * attribute in this format : 'propertyvalue' => 'Property Value'
-------------------------------------------------------------- */

function tnframework_get_shortcode_atts($shortcode, $attributes, $content) {

	// pattren
	$pattern = '\[(\[?)(' . $shortcode . ')(?![\w-])([^\]\/]*(?:\/(?!\])[^\]\/]*)*?)(?:(\/)\]|\](?:([^\[]*+(?:\[(?!\/\2\])[^\[]*+)*+)\[\/\2\])?)(\]?)';

	// final output
	$return = array();

	// collect rows
	$rows = array();

	if ($content != '') {
		if (preg_match_all('/' . $pattern . '/s', $content, $matches)
			&& array_key_exists(2, $matches)
			&& in_array($shortcode, $matches[2])) {
			// check the content
			if (is_array($matches) && isset($matches[3])) {
				// group shortcode rows
				foreach ($matches[3] as $key => $value) {

					$string = explode(' ', $value);
					$rows[] = $string;

				} // end foreach

				// format each shortcode row and append it to $return[]
				foreach ($rows as $row) {

					$single_row = array();

					foreach ($row as $key => $value) {
						$string = explode('=', $value);
						if (isset($string[0]) && in_array($string[0], $attributes)) {
							$single_row[] = array($string[0] => $string[1]);
						}
					}

					if(is_array($single_row[1]))
					{
						$return[] = array_merge($single_row[0], $single_row[1]);
					}
				}

			}
		}

		return $return;

	}
}

/* --------------------------------------------------------------
Prepare Next And Prev Buttons

 * this function will prepare next and prev pagination buttons
-------------------------------------------------------------- */
function tnframework_pagination($max, $paged = false, $fontawesome = true, $class = array('prev', 'next')) {

	// Get Current Page Number
	$current_page = get_query_var('page') ? get_query_var('page') : get_query_var('paged');

	if($current_page == '') $current_page = 1;

	/* $return html content */
	$return = '';
	$numeric_pagination = '';

	/* if $paged != false and $max pages > 1, will add numeric pagination list */
	if ($paged != false && $max > 1) {

		$numeric_pagination .= '<div class="pagination">';

		// TF
		$pag_args = array(
			'before' => '<li>',
			'after' => '</li>',
			'link_before' => '',
			'link_after' => '',
			'next_or_number' => 'number',
			'nextpagelink' => '',
			'previouspagelink' => '',
			'pagelink' => '%',
			'echo' => 0,
		);

		wp_link_pages($pag_args);

		// Prev Button
		if($current_page > 1)
		{
			$page = get_pagenum_link($current_page - 1);

			$numeric_pagination .= '<a href="'.esc_url($page).'" class="has_icon"><i class="fa fa-chevron-left"></i><span>'.esc_html__('Previous' , 'dawn').'</span></a>';
		}

		$pagination_args = array(
		    'base'            => get_pagenum_link(1) . '%_%',
		    'total'           => $max,
		    'current'         => $current_page,
		    'show_all'        => False,
		    'end_size'        => 1,
		    'mid_size'        => 1,
		    'prev_next'       => false,
		    'type'            => 'plain',
		    'add_args'        => false,
		    'add_fragment'    => ''
		  );

		$numeric_pagination .= paginate_links($pagination_args);

		// Next Page
		if($current_page < $max && $max > 1)
		{
			$page = get_pagenum_link($current_page + 1);

			$numeric_pagination .= '<a href="'.esc_url($page).'" class="has_icon"><i class="fa fa-chevron-right"></i><span>'.esc_html__('Next' , 'dawn').'</span></a>';
		}

		$numeric_pagination .= '</div><!-- end Pagination -->';
	}

	$return .= $numeric_pagination;

	return $return;
}

/* --------------------------------------------------------------
Date Format

 * this function will return user-set date format (defined in
 * theme options page)
-------------------------------------------------------------- */
function tnframework_post_date() {

	$dawn_theme_options = dawn_theme_settings();
	if (isset($dawn_theme_options['edit_date_format']) && $dawn_theme_options['edit_date_format'] != '') {
		return get_the_date($dawn_theme_options['edit_date_format']);
	} else {
		return get_the_date('d M , Y');
	}
}

/* --------------------------------------------------------------
Font Awesome List

 * this function will return a list of font-awesome icons
 * in this format :
 * 1 - <li><i class="fa fa-icon-name"></i></li>
 * 2 - <option name="icon-name"><i class="fa fa-icon-name"></i></option>
-------------------------------------------------------------- */
function tnframework_get_fontawesome_list($action = 'select' , $depth = false) {

	$list = str_replace(' ', '', 'adjust , adn , align-center , align-justify , align-left , align-right , ambulance , anchor , android , angle-double-down , angle-double-left , angle-double-right , angle-double-up , angle-down , angle-left , angle-right , angle-up , apple , archive , arrow-circle-down , arrow-circle-left , arrow-circle-o-down , arrow-circle-o-left , arrow-circle-o-right , arrow-circle-o-up , arrow-circle-right , arrow-circle-up , arrow-down , arrow-left , arrow-right , arrow-up , arrows , arrows-alt , arrows-h , arrows-v , asterisk , automobile , backward , ban , bank , bar-chart-o , barcode , bars , beer , behance , behance-square , bell , bell-o , bitbucket , bitbucket-square , bitcoin , bold , bolt , bomb , book , bookmark , bookmark-o , briefcase , btc , bug , building , building-o , bullhorn , bullseye , cab , calendar , calendar-o , camera , camera-retro , car , caret-down , caret-left , caret-right , caret-square-o-down , caret-square-o-left , caret-square-o-right , caret-square-o-up , caret-up , certificate , chain , chain-broken , check , check-circle , check-circle-o , check-square , check-square-o , chevron-circle-down , chevron-circle-left , chevron-circle-right , chevron-circle-up , chevron-down , chevron-left , chevron-right , chevron-up , child , circle , circle-o , circle-o-notch , circle-thin , clipboard , clock-o , cloud , cloud-download , cloud-upload , cny , code , code-fork , codepen , coffee , cog , cogs , columns , comment , comment-o , comments , comments-o , compass , compress , copy , credit-card , crop , crosshairs , css3 , cube , cubes , cut , cutlery , dashboard , database , dedent , delicious , desktop , deviantart , digg , dollar , dot-circle-o , download , dribbble , dropbox , drupal , edit , eject , ellipsis-h , ellipsis-v , empire , envelope , envelope-o , envelope-square , eraser , eur , euro , exchange , exclamation , exclamation-circle , exclamation-triangle , expand , external-link , external-link-square , eye , eye-slash , facebook , facebook-square , fast-backward , fast-forward , fax , female , fighter-jet , file , file-archive-o , file-audio-o , file-code-o , file-excel-o , file-image-o , file-movie-o , file-o , file-pdf-o , file-photo-o , file-picture-o , file-powerpoint-o , file-sound-o , file-text , file-text-o , file-video-o , file-word-o , file-zip-o , files-o , film , filter , fire , fire-extinguisher , flag , flag-checkered , flag-o , flash , flask , flickr , floppy-o , folder , folder-o , folder-open , folder-open-o , font , forward , foursquare , frown-o , gamepad , gavel , gbp , ge , gear , gears , gift , git , git-square , github , github-alt , github-square , gittip , glass , globe , google , google-plus , google-plus-square , graduation-cap , group , h-square , hacker-news , hand-o-down , hand-o-left , hand-o-right , hand-o-up , hdd-o , header , headphones , heart , heart-o , history , home , hospital-o , html5 , image , inbox , indent , info , info-circle , inr , instagram , institution , italic , joomla , jpy , jsfiddle , key , keyboard-o , krw , language , laptop , leaf , legal , lemon-o , level-down , level-up , life-bouy , life-ring , life-saver , lightbulb-o , link , linkedin , linkedin-square , linux , list , list-alt , list-ol , list-ul , location-arrow , lock , long-arrow-down , long-arrow-left , long-arrow-right , long-arrow-up , magic , magnet , mail-forward , mail-reply , mail-reply-all , male , map-marker , maxcdn , medkit , meh-o , microphone , microphone-slash , minus , minus-circle , minus-square , minus-square-o , mobile , mobile-phone , money , moon-o , mortar-board , music , navicon , openid , outdent , pagelines , paper-plane , paper-plane-o , paperclip , paragraph , paste , pause , paw , pencil , pencil-square , pencil-square-o , phone , phone-square , photo , picture-o , pied-piper , pied-piper-alt , pied-piper-square , pinterest , pinterest-square , plane , play , play-circle , play-circle-o , plus , plus-circle , plus-square , plus-square-o , power-off , print , puzzle-piece , qq , qrcode , question , question-circle , quote-left , quote-right , ra , random , rebel , recycle , reddit , reddit-square , refresh , renren , reorder , repeat , reply , reply-all , retweet , rmb , road , rocket , rotate-left , rotate-right , rouble , rss , rss-square , rub , ruble , rupee , save , scissors , search , search-minus , search-plus , send , send-o , share , share-alt , share-alt-square , share-square , share-square-o , shield , shopping-cart , sign-in , sign-out , signal , sitemap , skype , slack , sliders , smile-o , sort , sort-alpha-asc , sort-alpha-desc , sort-amount-asc , sort-amount-desc , sort-asc , sort-desc , sort-down , sort-numeric-asc , sort-numeric-desc , sort-up , soundcloud , space-shuttle , spinner , spoon , spotify , square , square-o , stack-exchange , stack-overflow , star , star-half , star-half-empty , star-half-full , star-half-o , star-o , steam , steam-square , step-backward , step-forward , stethoscope , stop , strikethrough , stumbleupon , stumbleupon-circle , subscript , suitcase , sun-o , superscript , support , table , tablet , tachometer , tag , tags , tasks , taxi , tencent-weibo , terminal , text-height , text-width , th , th-large , th-list , thumb-tack , thumbs-down , thumbs-o-down , thumbs-o-up , thumbs-up , ticket , times , times-circle , times-circle-o , tint , toggle-down , toggle-left , toggle-right , toggle-up , trash-o , tree , trello , trophy , truck , try , tumblr , tumblr-square , turkish-lira , twitter , twitter-square , umbrella , underline , undo , university , unlink , unlock , unlock-alt , unsorted , upload , usd , user , user-md , users , video-camera , vimeo-square , vine , vk , volume-down , volume-off , volume-up , warning , wechat , weibo , weixin , wheelchair , windows , won , wordpress , wrench , xing , xing-square , yahoo , yen , youtube , youtube-play , youtube-square');


	if($depth != false)
	{
			$return = array();
			$list = explode(',' , $list);

			foreach ($list as $key) {
				$return[$key] = $key;
			}

			return $return;
	}
	elseif($action == 'select') {
		return array_merge(array(' '), explode(',', $list));
	}

}

/* --------------------------------------------------------------
  Check if file exists , This function will check if URL exists
  or not
-------------------------------------------------------------- */
function tnframework_file_exists($file) {
	$header = @get_header($file);
	if ($header[0] == 'HTTP/1.1 404 Not Found') {
		return false;
	} else {
		return true;
	}

}

/**
 * Fix Widget Empty Title , this will fix empty title for all widgets
 * and will add cutom 'before_widget' parameter for search widget
 */
//
add_filter( 'widget_title', 'tnframework_empty_widget_title', 10, 3 );
function tnframework_empty_widget_title( $output = '' ) {

    if($output == '') {
		echo '<div class="widget-content">';
		return false;
	}else {
		return $output;
	}
}


/**
 * Comments pagination
 **/
function tnframework_comments_pagination($id) {

	$count = 0;
	$return = '';
	$pagination = get_option('comments_per_page');
	if (is_numeric($id) && $pagination) {
		$get_comments = get_comments(array(
			'post_id' => $id,

		));

		foreach ($get_comments as $comment) {
			if ($comment->comment_parent == 0) {
				$count = $count + 1;
			}

		}
	}

	// return comments pagination
	if ($count >= $pagination) {

		$return .= '<span class="prev">' . previous_comments_link(esc_html__('Older Comments', 'dawn')) . '</span>';
		$return .= '<span style="float: right;" class="next">' . next_comments_link(esc_html__('Newer Comments', 'dawn')) . '</span>';
		echo $return;

	}
}

/**
 * Next and prev links
 **/
function tnframework_posts_link_prev_class($format) {
	$format = str_replace('href=', 'class="next" href=', $format);
	return $format;
}
add_filter('next_posts_link', 'tnframework_posts_link_prev_class');

/**
 * Default WP posts limit
 **/
$blog_limit = isset($dawn_theme_options['limit_posts']) ? $dawn_theme_options['limit_posts'] : 5;
update_option('posts_per_page', $blog_limit);

/*
--------------------------
Custom Date Format
--------------------------
 */
function tnframework_date() {
	$dawn_theme_options = dawn_theme_settings();

	if (isset($dawn_theme_options['edit_date_format']) && $dawn_theme_options['edit_date_format'] != '') {
		return get_the_date($dawn_theme_options['edit_date_format']);
	} else {
		return get_the_date('d M , Y');
	}
}

/*
--------------------------
Get post by format
--------------------------
 */
function tnframework_get_post($filepath = '', $post_format = '') {

	if ($post_format != '') {

		switch ($post_format) {

			case 'standard' :
				require $filepath . 'standard.php';
				break;

			case 'image':
				require $filepath . 'image.php';
				break;

			case 'video':
				require $filepath . 'video.php';
				break;

			case 'audio':
				require $filepath . 'audio.php';
				break;

			case 'image':
				require $filepath . 'standard.php';
				break;

			case 'gallery':
				require $filepath . 'gallery.php';
				break;

			case 'link':
				require $filepath . 'link.php';
				break;

			case 'quote':
				require $filepath . 'quote.php';
				break;
		}
	} else {
		require $filepath . 'standard.php';
	}
}

/* --------------------------------------------------------------
Social Icons

 * this function will return social icons with links
 * $icons , icons array
 * $fontawesome enable font awesome
 * $options , theme options array
-------------------------------------------------------------- */
function tnframework_social_icons($icons = 'facebook,twitter,dribbble,google,pinterest', $options, $wrap = false, $return = false) {

	$output = '';
	$return_url = '';
	$icons = str_replace(' ', '', $icons);
	$icons = explode(',', $icons);
	if ($wrap != false) {
		$output .= '<div class="' . esc_attr($wrap) . '">';
	}

	foreach ($icons as $icon) {
		if (isset($options[$icon]) && $options[$icon] != '') {
			if ($icon == 'facebook') {

				$return_url = 'http://facebook.com/' . $options['facebook'];
				$output .=  '<a href="'.esc_url($return_url).'" class="facebook"><i class="fa fa-facebook"></i></a>';

			} elseif ($icon == 'google') {

				$output .=  '<a href="' . esc_url($options['google']) . '" class="google"><i class="fa fa-google-plus"></i></a>';

			} elseif ($icon == 'twitter') {

				$return_url = 'http://twitter.com/' . $options['twitter'];
				$output .=  '<a href="'.esc_url($return_url).'" class="twitter"><i class="fa fa-twitter"></i></a>';
			}
			 else {

				$output .=  '<a href="'.esc_url(' . $options[$icon] . ').'" class="' . esc_attr($icon) . '"><i class="fa fa-' . esc_attr($icon) . '"></i></a>';
			}

		}
	}
	if ($wrap != false) {
		$output .= '</div>';
	}

	if($return === true) return $output; else echo $output;
}

/* --------------------------------------------------------------
/Clean CF7 Output
-------------------------------------------------------------- */
function tnframework_wpcf7_clean_callback($content) {

	if (!defined('WPCF7_AUTOP')) {
		define('WPCF7_AUTOP', false);
	}

	return $content;

}
add_filter('tnframework_wpcf7_clean', 'tnframework_wpcf7_clean_callback', 10, 3);

/* --------------------------------------------------------------
Theme Editing

 * clear read more link
-------------------------------------------------------------- */
if (!function_exists('tnframework_clear_more_link')) {
	function tnframework_clear_more_link($more) {
		return '';
	}
	add_filter('the_content_more_link', 'tnframework_clear_more_link');
}

/* -------------------------------------------------------------*/
/* Check if comments is allowed for a single post */
/* -------------------------------------------------------------*/

if (!function_exists('tnframework_comments_open')) {
	function tnframework_comments_open() {

		$dawn_theme_options = dawn_theme_settings();
		if ((isset($dawn_theme_options['enable_comments']) && $dawn_theme_options['enable_comments'] != '0') && comments_open()) {
			return true;
		} else {
			return false;
		}

	}
}

/* -------------------------------------------------------------*/
/* List Categories  */
/* -------------------------------------------------------------*/
if (!function_exists('tnframework_list_categories')) {
	function tnframework_list_categories($postID, $links = false, $separator = ',', $prefix = false , $class = false) {

		// Category Item Class
		$class = ($class == false) ? 'category-item' : $class;

		// Get Categories
		$categories = get_the_category($postID);

		// Reeturn
		$return = '';
		if (!empty($categories)) {

			if ($prefix != false) {
				$return .= '&nbsp;' . $separator . ' ';
			}

			foreach ($categories as $category) {
				if ($category->cat_name != 'Uncategorized') {
					if ($links != false) {
						$return .= '<a class="'.esc_attr($class).'" href="' . esc_url(get_category_link($category->term_id)) . '">' . $category->cat_name . '</a>' . $separator;
					} else {
						$return .= $category->cat_name . $separator;
					}

				}
			}
			return trim($return, $separator);

		}

	}
}


/* -------------------------------------------------------------*/
/* List Post Tags  */
/* -------------------------------------------------------------*/
if (!function_exists('tnframework_list_tags')) {
	function tnframework_list_tags($class){

		$return = '';

		// Tags Array
		$tags = get_the_tags();

		if(is_array($tags)){
			foreach ($tags as $tag) {
				$return .= '<a href="'.esc_url(get_tag_link($tag->term_id)).'" class="'.esc_attr($class).'">'.$tag->name.'</a>';
			}
		}

		return $return;
	}
}

/* -------------------------------------------------------------*/
/* Prepare WP_Query Loop Config  */
/* -------------------------------------------------------------*/
if (!function_exists('tnframework_query_config')) {
	function tnframework_query_config($query_name, $limit, $order, $pagination = false) {
		$config = array();
		$query_name = $query_name ? $query_name : 'blog';
		$limit = ($limit != '') ? $limit : 3;
		$order = $order ? $order : 'date';

		// prepare config array
		$config['posts_per_page'] = $limit;
		$config['post_type'] = ($query_name == 'blog') ? 'post' : $query_name;
		$config['orderby'] = ($order == 1) ? 'date' : 'comment_count'; // 1= date , 2 = comments count
		$config['post_status'] = 'publish';
		if ($pagination != false) {
			$config['paged'] = $paged;
		}

		return $config;
	}
}


/* -------------------------------------------------------------*/
/* FileSystem Init  */
/* -------------------------------------------------------------*/


function tnframework_filesystem_init($url , $type , $content = null , $nonce = null){

	$stat = '';
	$return_error = '';

	// Require File APi
	require_once(ABSPATH . 'wp-admin/includes/file.php');

	// Request Credentials
	if (false === ($creds = request_filesystem_credentials($url, '') ) ) {
		$stat = 'false';
	}
	else {
		if ( ! WP_Filesystem($creds) ) {
			request_filesystem_credentials($url, '');
		}
	}

	// Globals
	global $wp_filesystem;

	// Write To File
	if($stat != 'false' && $type == 'write')
	{
		if(method_exists($wp_filesystem , 'put_contents'))
		{
			$wp_filesystem->put_contents($url , $content , FS_CHMOD_FILE);
		}
		else {
			$return_error = esc_html__('put_contents() method does not exist' , 'dawn');
		}
	}


	// Read From File
	if($stat != 'false' && $type == 'read')
	{
		if(method_exists($wp_filesystem , 'get_contents'))
		{
			return $wp_filesystem->get_contents($url);
		}
		else {
			$return_error = esc_html__('get_contents() method does not exist' , 'dawn');
		}
	}

	if($return_error != '')
	{
		return new WP_Error( 'broke', $return_error );
	}
}
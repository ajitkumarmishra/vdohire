<?php
// link Post Type
$link = get_post_meta(get_the_ID() , 'post_link' , true);
$link_description = get_post_meta(get_the_ID() , 'post_link_description' , true);
?>
<div class="single-post post-type-quote">
  <div class="post-content-outer container-box">
    <div class="quote-wrapper"><i class="fa fa-link"></i><span class="sep"></span>
      <h2 class="quote-title"><a href="<?php echo esc_url($link); ?>"><?php echo $link; ?></a></h2><span class="sep wide"></span><span class="author"><?php echo $link_description; ?></span>
    </div>
  </div>
</div>
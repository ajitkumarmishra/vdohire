<?php
// Quote Post Type
$quote = get_post_meta(get_the_ID() , 'post_quote' , true);
$quote_author = get_post_meta(get_the_ID() , 'post_quote_author' , true);
?>
<div class="single-post post-type-quote">
  <div class="post-content-outer container-box">
    <div class="quote-wrapper"><i class="fa fa-quote-left"></i><span class="sep"></span>
      <h2 class="quote-title"><?php echo $quote; ?></h2><span class="sep wide"></span><span class="author"><?php echo $quote_author; ?></span>
    </div>
  </div>
</div>
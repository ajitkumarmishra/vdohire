<?php
// Gallery Post
$dawn_theme_options = dawn_theme_settings();

// Get Attached Gallery ( IDs of the attached images ) .
$gallery = get_post_meta(get_the_ID() , 'post_gallery' , true);
$gallery_type = get_post_meta(get_the_ID() , 'gallery_type' , true);
$gallery_images = array();

// Push Gallery Images
if($gallery != '') {

    $gallery = explode(',' , $gallery);

    foreach ($gallery as $id) {

        // Get each attached image by ID
        $gallery_images[] = wp_get_attachment_image_src($id, 'dawn-post');
    }
}

// Post Class
$post_class = 'single-post post-type-' . ( ($gallery_type == 'grid') ? 'inline-gallery' : 'gallery' );
?>
<!-- Post Type <?php echo get_post_format(); ?> -->
<div <?php post_class( esc_attr($post_class . (is_sticky() ? 'sticky-post' : '')) ); ?>>

    <?php if($gallery_type == 'grid' && $gallery != '' ) : ?>
        <div class="post-media">
          <div class="inline-gallery">
              <?php
              foreach ($gallery_images as $image) {
                  if(isset($image[0]))
                  {
                      echo '<a href="'.esc_url($image[0]).'"><img src="'.esc_attr($image[0]).'" alt="'.esc_attr(esc_html__('Slider Image' ,'dawn')).'"></a>';
                  }

              }
              ?>
          </div>
        </div>
    <?php elseif($gallery != '') : ?>
        <!-- Gallery -->
        <div class="post-media">
            <div class="flexslider">
              <ul class="slides">
                <?php
                foreach ($gallery_images as $image) {
                    if(isset($image[0]))
                    {
                        echo '<li> <a><img src="'.esc_attr($image[0]).'" alt="'.esc_attr(esc_html__('Slider Image' ,'dawn')).'"></a></li>';
                    }

                }
                ?>
              </ul>
            </div>
        </div>
    <?php endif; ?>

    <div class="post-content-outer container-box">
        <div class="post-meta-top">
            <?php if(is_sticky()) { echo '<p>'.esc_html__('Sticky' , 'dawn').'</p>'; } ?><p><?php echo tnframework_list_categories(get_the_ID() ,true , ' '); ?>
            <a href="<?php echo esc_url(get_permalink()); ?>"><?php echo get_the_date('M d , Y'); ?></a></p>
        </div><!-- end meta top -->
        <div class="post-title">
            <h2> <a class="uppercase" href="<?php echo esc_url(get_permalink()); ?>"><?php echo get_the_title(); ?></a></h2>
        </div>
      <div class="post-content-inner">
        <?php

            the_content();

            if(is_single()){
                wp_link_pages( array(
    				'before'      => '<div class="post-pagination-link-container"><div class="post-pagination-title">' . esc_html__('Pages : ' , 'dawn') . '</div>',
    				'after'       => '</div>',
    				'link_before' => '<div class="post-pagination-link">',
    				'link_after'  => '</div>',
    				'pagelink'    => '%',
    				'separator'   => '',
    			) );
            }

        ?>
      </div>
       <div class="post-meta-bottom">

         <?php if(!is_page()) : ?>
             <div class="tagcloud">
                 <?php echo tnframework_list_tags('button button-with-background tag'); ?>
             </div>
         <?php else :  ?>
             <div class="read-more">
                 <a href="<?php echo esc_url(get_permalink()); ?>" class="button button-with-background button-large"><?php esc_html_e('read more' , 'dawn'); ?></a>
             </div><!-- end read more -->
         <?php endif; ?>

             <div class="share"><?php tnframework_share_buttons(array('facebook' , 'twitter' , 'pinterest' , 'google')); ?>
             </div>
             <!-- end share -->
       </div>
    </div>
</div>
<?php
// Image Post
$dawn_theme_options = dawn_theme_settings();

// Get Attached Image
$image = '';
if(has_post_thumbnail()) {
    $imagesrc = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'dawn-post');
    $image = $imagesrc[0];
}
?>
<!-- Post Type <?php echo get_post_format(); ?> -->
<div <?php post_class( esc_attr('single-post post-type-image '. (is_sticky() ? 'sticky-post' : '')) ); ?>>

    <?php if($image != '') :  ?>
    <div class="post-media post-media-image"><a href="<?php echo esc_url(get_permalink()); ?>"> <img src="<?php echo esc_attr($image); ?>" alt="<?php esc_attr(get_the_title()); ?>"></a></div><!-- end post media -->
    <?php endif; ?>

    <div class="post-content-outer container-box">
        <div class="post-meta-top">
            <?php if(is_sticky()) { echo '<p>'.esc_html__('Sticky' , 'dawn').'</p>'; } ?><p><?php echo tnframework_list_categories(get_the_ID() ,true , ' '); ?>
            <a href="<?php echo esc_url(get_permalink()); ?>"><?php echo get_the_date('M d , Y'); ?></a></p>
        </div><!-- end meta top -->
        <div class="post-title">
            <h2> <a class="uppercase" href="<?php echo esc_url(get_permalink()); ?>"><?php echo get_the_title(); ?></a></h2>
        </div>
      <div class="post-content-inner">
        <?php
            the_content();
            if(is_single()){
                wp_link_pages( array(
    				'before'      => '<div class="post-pagination-link-container"><div class="post-pagination-title">' . esc_html__('Pages : ' , 'dawn') . '</div>',
    				'after'       => '</div>',
    				'link_before' => '<div class="post-pagination-link">',
    				'link_after'  => '</div>',
    				'pagelink'    => '%',
    				'separator'   => '',
    			) );
            }
        ?>
      </div>
       <div class="post-meta-bottom">

         <?php if(!is_page()) : ?>
             <div class="tagcloud">
                 <?php echo tnframework_list_tags('button button-with-background tag'); ?>
             </div>
         <?php else :  ?>
             <div class="read-more">
                 <a href="<?php echo esc_url(get_permalink()); ?>" class="button button-with-background button-large"><?php esc_html_e('read more' , 'dawn'); ?></a>
             </div><!-- end read more -->
         <?php endif; ?>

             <div class="share"><?php tnframework_share_buttons(array('facebook' , 'twitter' , 'pinterest' , 'google')); ?>
             </div>
             <!-- end share -->
       </div>
    </div>
</div>
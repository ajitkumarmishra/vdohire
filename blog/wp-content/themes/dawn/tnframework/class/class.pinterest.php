<?php

$dawn_theme_options = dawn_theme_settings();
/* --------------------------------------------------------------
 	Pinterest Share Button

 	* this function will print pinterest share button for
 	* current post .
-------------------------------------------------------------- */

function tnframework_pinterest_button($id)
{
  $url  = 'http://pinterest.com/pin/create/button/?source_url=' . get_permalink() . '&media=';
  if($id != '')
  {
          if(get_post_format() != '')
          {
                switch(get_post_format($id))
                {
                        case 'image' :
                          $post_thumbnail_id = get_post_thumbnail_id($id);
                          $post_thumbnail_url = wp_get_attachment_url( $post_thumbnail_id );
                          $url .= $post_thumbnail_url;
                          break;




                        case 'gallery' :
                          $images = explode(',' , str_replace(',,' , ',' , get_post_meta($id  , 'buzz_media_gallery' , true)));
                          if(is_array($images)) $url .= $images[0];
                          break;


                        default:
                            if(has_post_thumbnail($id))
                            {
                              $post_thumbnail_id = get_post_thumbnail_id($id);
                              $post_thumbnail_url = wp_get_attachment_url( $post_thumbnail_id );
                              $url .= $post_thumbnail_url;
                            }

                }
          }


  }
  $url .= '&description=' . get_the_title();
  return '<a target="_blank" href="'.$url.'" class="pinterest"></a>';
}
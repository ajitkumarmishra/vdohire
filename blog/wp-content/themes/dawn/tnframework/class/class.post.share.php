<?php

/* --------------------------------------------------------------
 	Post Share Helper

 	* this function will prepare post share buttons
 	* default buttons is : facebook , twitter , pinterest ,plus

 	** NOTE : this function should be used inside the loop
-------------------------------------------------------------- */

function tnframework_share_buttons($buttons = array('facebook' , 'twitter' , 'google' , 'pinterest')){


		/*
		* $return return content
		* $p post permalink
		* $t post title
		 */
		$return = '';
		$p = get_permalink();
		$t = rawurlencode(get_the_title());
		$id = get_the_ID();


		// facebook
		if(in_array('facebook', $buttons)) {
			$facebook_url = esc_url('https://www.facebook.com/sharer.php?u='.$p.'&amp;t='.$t);
			$return .= '<a target="_blank" name="fb_share" class="facebook" href="'.$facebook_url.'"><i class="fa fa-facebook"></i></a>';
		}

		// twitter
		if(in_array('twitter' , $buttons)) {
			$twitter_url = esc_url('https://twitter.com/intent/tweet?original_referer='.$p.'&amp;shortened_url='.$t.'&amp;text='.$t.'&amp;url='.$p);
			$return .= '<a target="_blank" href="'.$twitter_url.'" class="twitter"><i class="fa fa-twitter"></i></a>';
		}

		// google plus
		if(in_array('google' , $buttons)) {
			$google_url = esc_url('https://plus.google.com/share?url='.$p);
			$return .= '<a target="_blank" href="'.$google_url.'" class="google"><i class="fa fa-google-plus"></i></a>';
		}

		// pinterest
		if(in_array('pinterest' , $buttons))
		{
				  $url  = 'http://pinterest.com/pin/create/button/?source_url=' . $p . '&media=';
		          if($id != '')
		          {
		                  if(get_post_format($id) != '')
		                  {
		                        switch(get_post_format($id))
		                        {
		                                case 'image' :
										  $imagesrc = wp_get_attachment_image_src(get_post_thumbnail_id($id), 'full');
									      $image = $imagesrc[0];
		                                  $url .= $image;
		                                  break;




		                                case 'gallery' :
										  $gallery = '';
		                                  $images = get_post_meta($id , 'post_gallery' , true);
										  if($images != ''){
											  $gallery = explode(',' , $images);
											  if(isset($gallery[0])){
												  $returned_image = wp_get_attachment_image_src($gallery[0], 'dawn-post');
												  $url .= isset($returned_image[0]) ? $returned_image[0] : '';
											  }
										  }

		                                  break;


		                                default:
		                                    if(has_post_thumbnail($id))
		                                    {
		                                      $post_thumbnail_id = get_post_thumbnail_id($id);
		                                      $post_thumbnail_url = wp_get_attachment_url( $post_thumbnail_id );
		                                      $url .= $post_thumbnail_url;
		                                    }

		                        }
		                  }


		          }
		          $url .= '&description=' . $t;
		          $return .= '<a target="_blank" href="'.esc_url($url).'" class="pinterest"><i class="fa fa-pinterest"></i></a>';
		}


		// tumblr
		if(in_array('tumblr' , $buttons) ) {
			$tumblr_url = esc_url('http://www.tumblr.com/share/link?url='.urlencode($p).'&name='.$t);
			$return .= '<a target="_blank" class="tumblr" href="'.$tumblr_url.'"><i class="fa fa-tumblr"></i></a>';
		}

		// Linkedin
		if(in_array('linkedin' , $buttons) ) {
			$linkedin_url = esc_url('https://www.linkedin.com/shareArticle?mini=true&url='.$p.'&title='.$t.'&summary=&source=');
			$return .= '<a target="_blank" class="linkedin" href="'.$linkedin_url.'"><i class="fa fa-linkedin"></i></a>';
		}

		// Stumbleupon

		if(in_array('stumbleupon' , $buttons)) {
			$stumbleupon_url = esc_url('http://www.stumbleupon.com/submit?url='.$p.'&title='.$t);
			$return .= '<a target="_blank" class="stumbleupon" href="'.$stumbleupon_url.'"><i class="fa fa-stumbleupon"></i></a>';
		}

		// Digg
		if(in_array('digg' , $buttons)) {
			 $digg_url = esc_url('http://digg.com/submit?url='.$p.'&title='.$t);
			 $return .= '<a class="digg" target="_blank" href="'.$stumbleupon_url.'"><i class="fa fa-digg"></i></a>';
		}

		// email
		if(in_array('email' , $buttons)) {
			$mail_url = esc_url('mailto:?subject='.rawurlencode(esc_html__('Shared Link' , 'dawn')).'&amp;body='.rawurlencode(esc_html__('Check out this post' , 'dawn')).' '.$p);
			$return .= '<a class="envelope" target="_blank" href="'.$mail_url.'" title="'.esc_html__('Share' , 'dawn').'"><i class="fa fa-envelope"></i></a>';
		}
		echo $return;
}
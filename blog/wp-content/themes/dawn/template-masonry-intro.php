<?php
/* - Template Masonry Intro - */
$dawn_theme_options = dawn_theme_settings();

/* - Settings - */
$intro_settings = array(
    'posts_per_page' => 6 ,
    'post_type' => 'dawn_slides' ,
    'orderby' => 'date' ,
    'post_status' => 'publish'
);

$items = new WP_Query($intro_settings);
?>
<section id="slider-grid" class="section-boxed content-slider">
  <div class="container">
    <div class="row">
      <?php

      // Second And Third Grid
      $second_grid = array();
      $third_grid = array();


      if($items->have_posts()) : while($items->have_posts()) : $items->the_post();

      // Get Attached Image
      $image = '';
      if(has_post_thumbnail()) {
          $imagesrc = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'dawn-intro');
          $image_thumbnail_src = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'dawn-intro-thumbnail');

          $image = $imagesrc[0];
          $image_thumbnail = $image_thumbnail_src[0];
      }

      ?>

          <?php if($items->current_post == 0) :  ?>

            <!-- Intro Post -->
            <div class="col-md-4 col-xs-6">
                <div class="grid-item full-height">
                  <div class="item-image">
                      <a href="<?php echo esc_url(get_permalink()); ?>"> <img style="max-width: none;" src="<?php echo esc_attr($image); ?>" alt="<?php echo esc_attr(get_the_title()); ?>"></a>
                  </div>
                  <div class="item-content">
                    <h4 class="item-title uppercase"><?php echo get_the_title(); ?></h4>
                    <?php echo tnframework_list_categories(get_the_ID() , true , '' , false, 'button-with-white-border button button-small'); ?>
                  </div>
                </div>
            </div><!-- end intro post -->

        <?php elseif($items->current_post > 0 && $items->current_post <= 3) :
                    $second_grid[] = array(
                        'title' => get_the_title() ,
                        'link' => get_permalink() ,
                        'image' => $image_thumbnail ,
                        'categories' => tnframework_list_categories(get_the_ID() , true , '' , false, 'button-with-white-border button button-small')
                    );
            ?>


          <?php elseif($items->current_post > 3) :
                    $third_grid[] = array(
                        'title' => get_the_title() ,
                        'link' => get_permalink() ,
                        'image' => $image_thumbnail ,
                        'categories' => tnframework_list_categories(get_the_ID() , true , '' , false, 'button-with-white-border button button-small')
                    );
          ?>


          <?php endif; // End first post check ?>

          <?php endwhile; endif; wp_reset_postdata(); ?>


        <!-- Middle Column Posts -->
        <div class="col-md-4 col-xs-6">
        <?php
        if(!empty($second_grid)) :
            foreach ($second_grid as $post) :
                ?>
                <div class="grid-item third-height">
                  <div class="item-image">
                       <a href="<?php echo esc_url($post['link']); ?>"> <img style="max-width: none;" src="<?php echo esc_attr($post['image']); ?>" alt="<?php echo esc_attr($post['title']); ?>"></a>
                  </div>
                  <div class="item-content">
                    <h5 class="item-title uppercase"><?php echo $post['title']; ?></h5>
                    <?php echo $post['categories']; ?>
                  </div>
                </div>
        <?php
        endforeach;
        endif; ?>
        </div><!-- end middle column post -->

        <!-- Third Column Posts -->
        <div class="col-md-4 col-xs-6">
        <?php
        if(!empty($third_grid)) :
            foreach ($third_grid as $post) :
                ?>
                <div class="grid-item half-height">
                  <div class="item-image">
                       <a href="<?php echo esc_url($post['link']); ?>"> <img style="max-width: none;" src="<?php echo esc_attr($post['image']); ?>" alt="<?php echo esc_attr($post['title']); ?>"></a>
                  </div>
                  <div class="item-content">
                    <h5 class="item-title uppercase"><?php echo $post['title']; ?></h5>
                    <?php echo $post['categories']; ?>
                  </div>
                </div>
        <?php
        endforeach;
        endif; ?>
        </div><!-- end third column post -->
    </div>
  </div>
</section>
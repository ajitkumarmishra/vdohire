<?php
/* - Template Classic Slider - */

/* - Theme Settings - */
$dawn_theme_options = dawn_theme_settings();

/* - Slider Settings - */
$theme_slider_options = array(
    'posts_per_page' => isset($dawn_theme_options['slider_posts_limit']) ? $dawn_theme_options['slider_posts_limit'] : 3 ,
    'post_type' => 'dawn_slides' ,
    'orderby' => 'date' ,
    'post_status' => 'publish'
);

$slides = new WP_Query($theme_slider_options);

if($slides->have_posts()) :
?>
<section id="slider-classic" class="content-slider section-wide">
  <div class="slides-container">
    <div class="flexslider-custom">
      <ul class="slides">
        <?php while($slides->have_posts()) : $slides->the_post(); ?>
        <li>
            <?php if(has_post_thumbnail()) :  ?>
                <?php
                $imagesrc = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'full');
        		$image = $imagesrc[0];  ?>
                <div class="image"><img src="<?php echo esc_attr($image); ?>" alt="<?php echo esc_attr(get_the_title()); ?>"></div>
            <?php endif; ?>

          <div class="content">
            <h3 class="slider-title"><?php the_title(); ?></h3>
            <!-- Slide Link One -->
            <?php if(get_post_meta(get_the_ID() , 'slide_link_one' , true) != '') : ?>
                <a href="<?php echo esc_url(get_post_meta(get_the_ID() , 'slide_link_one' , true)); ?>" class="read-more button button-with-white-border"><?php echo esc_html__('Read More' , 'dawn'); ?></a>
            <?php endif; ?>

            <!-- Slide Link Two -->
            <?php if(get_post_meta(get_the_ID() , 'slide_link_two' , true) != '') : ?>
                <a href="<?php echo esc_url(get_post_meta(get_the_ID() , 'slide_link_two' , true)); ?>" class="read-more button button-with-background"><?php echo esc_html__('Read More' , 'dawn'); ?></a>
            <?php endif; ?>
          </div>
        </li>
        <?php endwhile; ?>
      </ul>
    </div>
  </div><!-- end slider container -->
</section>
<?php  endif; wp_reset_postdata(); ?>
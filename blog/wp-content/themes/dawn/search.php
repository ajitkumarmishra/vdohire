<?php
/* - Search Page - */
$dawn_theme_options = dawn_theme_settings();

/* - Header - */
get_header();


// Current Page
$paged = get_query_var('page') ? get_query_var('page') : get_query_var('paged');

// Blog Settings
$blog_query_settings = array(
    'posts_per_page' => isset($dawn_theme_options['blog_posts_limit']) ? $dawn_theme_options['blog_posts_limit'] : 3 ,
    'post_type' => 'post' ,
    'orderby' => 'date' ,
    'post_status' => 'publish' ,
    'paged' => $paged ,
    's' => get_search_query()
);

$blog_query = new WP_Query($blog_query_settings);
?>
<!-- Blog section -->
<section id="blog-classic" class="content-wrapper"><!-- Left Side -->
  <section class="container">
    <div class="row">
      <div class="col-md-8 posts-container">
        <?php if($blog_query->have_posts()) : while($blog_query->have_posts()) : $blog_query->the_post(); ?>
            <?php
            // Post format
            $post_format = (get_post_format() != '') ? get_post_format() : 'standard';

            // Get post by post format
            tnframework_get_post(DAWN_TNFRAMEWORK_POSTS_ROOT . '/post/' , $post_format);
            ?>
        <?php endwhile; ?>

        <?php echo tnframework_pagination($blog_query->max_num_pages , true); ?>

        <?php else : ?>

            <div class="single-post post-type-image">
              <div class="post-content-outer container-box">
                <div class="post-content-inner">
                    <?php echo (isset($dawn_theme_options['search_error'])) ? $dawn_theme_options['search_error'] : esc_html__('No Posts Found' , 'dawn'); ?>
                </div><!-- end post content inner -->
              </div>
            </div><!-- end page content -->

        <?php endif; wp_reset_postdata(); ?>
      </div><!-- end Left Side Posts Container -->

      <!-- Right Side (Sidebar) -->
      <div class="col-md-4">
        <div id="sidebar-widgets-container" class="widgets-container">
            <?php get_sidebar(); ?>
        </div><!-- end widgets container -->
      </div><!-- End Right Side (Sidebar) -->
  </div><!-- end row -->
  </section>
</section>
<!-- End Blog Section -->

<?php
/* - Footer - */
get_footer();
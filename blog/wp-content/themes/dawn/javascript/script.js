/*global jQuery:false , global dawn_settings:false*/
(function($) {

    $(document).ready(function() {
        // Default Settings
        var dawn_settings = {
            flexAnimationMethod: 'linear',
            animationMethod: 'linear',
            sliderAnimationSpeed: 500,
            animationSpeed: 300,
            animationEasing: 'easeInSine'
        };

        // Toggle Search Form
        $('.search-icon a').on('click', function() {

            // Parent Section
            var parent = $(this).closest('section');

            // Show Search Form
            parent.find('.searchform').fadeToggle(
                300).find(
                'input').focus();

            // Toggle Navigation (for Desktop Navigation)
            if (parent.attr('id') == 'navigation') {
                parent.find('.navigation-menu').fadeToggle(
                    dawn_settings.animationSpeed,
                    dawn_settings.animationEasing);
            }
            // Toggle Search Icon Class
            $(this).find('i').toggleClass(
                'fa-search fa-times');

        });


        // Toggle Navigation Menu
        $('#navigation-responsive  a.toggle ').on(
            'click',
            function() {

                $('#navigation-responsive .right-panel').fadeToggle(
                    dawn_settings.animationSpeed);

            });

        // Submenu
        $('#navigation .navigation-menu ul li ').hover(function() {
            $(this).find('>ul').stop().fadeIn(dawn_settings
                .animationSpeed, dawn_settings.animationEasing
            );
        }, function() {
            $(this).find('>ul').stop().fadeOut(
                dawn_settings.animationSpeed,
                dawn_settings.animationEasing);
        });
        // Flexslider
        $('.flexslider , .flexslider-custom').flexslider({
            animation: dawn_settings.flexAnimationMethod,
            touch: true,
            easing: dawn_settings.animationMethod,
            animationSpeed: dawn_settings.sliderAnimationSpeed,
            keyboard: true,
            nextText: '<span class="overlay"></span><i class="fa fa-chevron-right"></i>',
            prevText: '<span class="overlay"></span><i class="fa fa-chevron-left"></i>',
            slideshow: true,
            slideshowSpeed: 10000,
            controlNav: false,
            directionNav: true,
            direction: "horizontal"
        });

        // Instagram Widget Animaiton
        $('.widget-content.instagram a').hover(function() {
            $(this).parent().find('a').addClass('faded');
            $(this).removeClass('faded');
        }, function() {
            $(this).parent().find('a').removeClass('faded');
        });

        // Masonry blog posts
        $('.posts-container .row .grid-posts-wrapper').masonry({
            itemSelector: '.grid-item',
            columnWidth: 1,
            percentPosition: true
        });
    });

})(jQuery);

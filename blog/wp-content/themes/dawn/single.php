<?php
$dawn_theme_options = dawn_theme_settings();
get_header();
?>

<section id="blog-classic" class="content-wrapper single-post-page"><!-- Left Side -->
  <section class="container">
    <div class="row">
    <?php if(have_posts()) : while(have_posts()) : the_post(); ?>
        <div class="col-md-8 posts-container"><!-- Image Post -->
            <?php
                // Post format
                $post_format = get_post_format();

                // Get post by post format
                tnframework_get_post(DAWN_TNFRAMEWORK_POSTS_ROOT . '/post/' , $post_format);
            ?>


            <?php if(get_post_type() != 'dawn_slides') : ?>
            <!-- Author -->
            <div id="post-author" class="post-content-box post-author">
                  <div class="author-image"><?php echo get_avatar( get_the_author_meta( 'ID' ), 200 ); ?></div>
                  <div class="content">

                    <h4 class="widget-title"><?php echo get_the_author_meta('display_name'); ?></h4>
                    <p><?php echo get_the_author_meta('description'); ?></p>

                    <div class="social-icons-colored">
                        <?php if(get_the_author_meta('facebook-url') != '') echo ' <a href="'.esc_url(get_the_author_meta('facebook-url')).'" class="facebook"><i class="fa fa-facebook"></i></a>';

                        if(get_the_author_meta('twitter-url') != '') echo ' <a href="'.esc_url(get_the_author_meta('twitter-url')).'" class="twitter"><i class="fa fa-twitter"></i></a>';

                        if(get_the_author_meta('pinterest-url') != '') echo ' <a href="'.esc_url(get_the_author_meta('pinterest-url')).'" class="pinterest"><i class="fa fa-pinterest"></i></a>';

                        if(get_the_author_meta('google-plus-url') != '') echo ' <a href="'.esc_url(get_the_author_meta('google-plus-url')).'" class="google"><i class="fa fa-google"></i></a>';

                        if(get_the_author_meta('dribbble-url') != '') echo ' <a href="'.esc_url(get_the_author_meta('dribbble-url')).'" class="dribbble"><i class="fa fa-dribbble"></i></a>'; ?>
                    </div><!-- end author social icons -->
                </div><!-- end content -->
            </div><!-- end post author -->

            <!-- Comments -->
            <?php comments_template(); ?>


            <?php endif; // end check post type ?>

        </div><!-- end Left Side Posts Container -->
    <?php endwhile; endif; wp_reset_postdata(); ?>

        <!-- Right Side (Sidebar) -->
        <div class="col-md-4">
        <div id="sidebar-widgets-container" class="widgets-container">
            <?php get_sidebar(); ?>
        </div><!-- end widgets container -->
        </div><!-- End Right Side (Sidebar) -->
    </div>
  </section>
</section>

<?php
/* - Footer - */
get_footer();
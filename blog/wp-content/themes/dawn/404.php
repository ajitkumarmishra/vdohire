<?php
$dawn_theme_options = dawn_theme_settings();
get_header();
?>

<section id="blog-classic" class="content-wrapper single-post-page"><!-- Left Side -->
  <section class="container">
    <div class="row">
        <div class="col-md-8 posts-container">

            <div class="single-post post-type-image">
              <div class="post-content-outer container-box">
                <div class="post-content-inner">
                    <?php
                    echo (isset($dawn_theme_options['404_message']) && $dawn_theme_options['404_message'] != '') ? $dawn_theme_options['404_message'] : esc_html__('404 Post Or Page Not Found' , 'dawn'); ?>
                </div><!-- end post content inner -->
              </div>
            </div><!-- end page content -->
        </div><!-- end Left Side Posts Container -->

        <!-- Right Side (Sidebar) -->
        <div class="col-md-4">
        <div id="sidebar-widgets-container" class="widgets-container">
            <?php get_sidebar(); ?>
        </div><!-- end widgets container -->
        </div><!-- End Right Side (Sidebar) -->
    </div>
  </section>
</section>

<?php
/* - Footer - */
get_footer();
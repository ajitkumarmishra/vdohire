<?php $dawn_theme_options = dawn_theme_settings(); ?>
<footer>
  <div class="container">
    <div class="row">
      <div class="widgets-container">
            <?php
            if(is_active_sidebar('footer')) {
                dynamic_sidebar('footer');
            } ?>
      </div><!-- end widgets container -->
  </div><!-- end row -->
  </div><!-- end container -->
</footer>
<?php wp_footer(); ?>
</body>
</html>
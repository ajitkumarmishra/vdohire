<?php
$dawn_theme_options = dawn_theme_settings();

if(!is_page_template()){

    $sidebar_setting = (isset($dawn_theme_options['default_sidebar']) && $dawn_theme_options['default_sidebar'] != '') ? $dawn_theme_options['default_sidebar'] : 'ONE';

    if(is_active_sidebar(ucwords($sidebar_setting))) {
        dynamic_sidebar(ucwords($sidebar_setting));
    }
}
else{
    if(is_active_sidebar(get_post_meta(get_the_ID() , 'sidebar' , true))){
        dynamic_sidebar(get_post_meta(get_the_ID() , 'sidebar' , true));
    }

}
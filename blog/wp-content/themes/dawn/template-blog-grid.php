<?php
/* - Template Blog Grid - */
$dawn_theme_options = dawn_theme_settings();


// Blog Settings
$blog_query_settings = array(
    'posts_per_page' => isset($dawn_theme_options['blog_posts_limit']) ? $dawn_theme_options['blog_posts_limit'] : 3 ,
    'post_type' => 'post' ,
    'orderby' => 'date' ,
    'post_status' => 'publish' ,
    'paged' => $paged
);

$blog_query = new WP_Query($blog_query_settings);
?>
<!-- Blog section -->
<section id="blog-classic" class="content-wrapper grid-blog"><!-- Left Side -->
  <section class="container">
    <div class="row">
      <div class="col-md-8 posts-container">
          <!-- row -->
          <div class="row">

              <!-- grid posts wrapper -->
              <div class="grid-posts-wrapper">
                  <?php if($blog_query->have_posts()) : while($blog_query->have_posts()) : $blog_query->the_post(); ?>
                      <div class="col-md-6 col-sm-6 col-xs-12 grid-item">
                          <?php
                          // Post format
                          $post_format = (get_post_format() != '') ? get_post_format() : 'standard';
                          // Get post by post format
                          tnframework_get_post(DAWN_TNFRAMEWORK_POSTS_ROOT . '/post/' , $post_format);
                          ?>
                      </div><!-- end col -->
                  <?php endwhile; ?>
              </div><!-- end grid posts wrapper -->

              <div class="margin" style="height: 20px;"></div>
              <?php echo tnframework_pagination($blog_query->max_num_pages , true); ?>
              <?php endif; wp_reset_postdata(); ?>
          </div><!-- end row -->
      </div><!-- end Left Side Posts Container -->

      <!-- Right Side (Sidebar) -->
      <div class="col-md-4">
        <div id="sidebar-widgets-container" class="widgets-container">
            <?php get_sidebar(); ?>
        </div><!-- end widgets container -->
      </div><!-- End Right Side (Sidebar) -->
  </div><!-- end row -->
  </section>
</section>
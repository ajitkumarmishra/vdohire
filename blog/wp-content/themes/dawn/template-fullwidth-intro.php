<?php
/* - Template Fullwidth Intro - */

/* - Theme Settings - */
$dawn_theme_options = dawn_theme_settings();

/* - Settings - */
$intro_settings = array(
    'posts_per_page' => 4 ,
    'post_type' => 'dawn_slides' ,
    'orderby' => 'date' ,
    'post_status' => 'publish'
);

$items = new WP_Query($intro_settings);
?>
<section id="intro-grid-fullwidth">
    <?php

    if($items->have_posts()) : while($items->have_posts()) : $items->the_post();

    // Get Attached Image
    $image = '';
    if(has_post_thumbnail()) {
        $imagesrc = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'full');
        $image_thumbnail_src = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'dawn-intro-thumbnail');

        $image = $imagesrc[0];
        $image_thumbnail = $image_thumbnail_src[0];
    }

    ?>

        <?php if($items->current_post == 0) :  ?>

            <div class="intro-item large">
              <div class="item-image"><img src="<?php echo esc_attr($image); ?>" alt="<?php echo esc_attr(get_the_title()); ?>"></div>
              <div class="item-content">
                  <?php echo tnframework_list_categories(get_the_ID() , true , '' , false, 'button button-with-background button-small'); ?>

                <h3 class="item-title">
                     <a class="uppercase" href="<?php echo esc_url(get_permalink()); ?>"><?php echo get_the_title(); ?></a></h3>
              </div>
            </div>

        <?php else : ?>

            <div class="intro-item third">
              <div class="item-image">
                  <a href="<?php echo esc_url(get_permalink()); ?>">
                      <img src="<?php echo esc_attr($image_thumbnail); ?>" alt="<?php echo esc_attr(get_the_title()); ?>">
                  </a>
              </div><!-- end item image -->
              <div class="item-content">
                <h6 class="item-title">
                    <a href="<?php echo esc_url(get_permalink()); ?>"><?php echo get_the_title(); ?>
                    </a>
                </h6>
              </div><!-- end item content -->
            </div>

        <?php endif; // End first post check ?>

        <?php endwhile; endif; wp_reset_postdata(); ?>

</section>
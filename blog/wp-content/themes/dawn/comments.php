<?php
$dawn_theme_options = dawn_theme_settings();


if(post_password_required())
{
    return;
}
/* - Comments Template - */
function dawn_comments_template($comment , $args , $depth){
    $GLOBALS['comment'] = $comment;
    extract($args);
    $comment_depth_class = implode(' ' , get_comment_class('Depth'));

    ?>

    <div id="<?php echo esc_attr(get_comment_ID()); ?>" class="single-comment  <?php if($depth > 1) echo 'sub-comments'; ?>
    <?php echo esc_attr($comment_depth_class); ?>">

      	<div class="avatar">
      	  <div class="img"><?php echo get_avatar( $comment->comment_author_email, 150 ); ?></div>
      	</div>
      	<div class="comment-content">
      	  <h5> <a href="#"><?php echo $comment->comment_author; ?></a></h5><span class="meta"><?php echo get_comment_date(); ?></span>
      	  <div class="content">

      		<?php if($comment->comment_approved == 0) : ?>
            <p><?php echo esc_html__('Your comment is awaiting moderation' , 'dawn'); ?></p>
            <?php else : ?>
            <p><?php echo $comment->comment_content; ?></p>
            <?php endif; ?>

      	  </div>
          <?php
            $comments_reply_text = esc_html__('reply' , 'dawn');
            comment_reply_link( array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth'] , 'reply_text' => $comments_reply_text ) ) );
          ?>
        </div><!-- end comment content -->

    <?php
}

/* - Commente Reply Link - */
add_filter('comment_reply_link', 'replace_reply_link_class');
function replace_reply_link_class($class){
    $class = str_replace("class='comment-reply-link", "class='button button-with-grey-background button-small", $class);
    return $class;
}

?>
<!-- Comments -->
<div id="comments" class="post-content-box post-comments">
  <div class="box-title">
	<h4 class="widget-title comments-title">
        <?php
            $comments_n = esc_html__('No Comments', 'dawn');
            $comments_o = esc_html__('1 Comment', 'dawn');
            $comments_r = esc_html__('(%) Comments', 'dawn');
            comments_number($comments_n, $comments_o, $comments_r );
        ?></h4>
  </div><!-- end box title -->

  <?php wp_list_comments(array('type' => 'comment' , 'style' => 'div' , 'callback' => 'dawn_comments_template')); ?>

  <?php tnframework_comments_pagination(get_the_ID()); ?>
</div><!-- end comments -->


<?php if( comments_open() && post_type_supports( get_post_type(), 'comments' ) ) : ?>
<!-- Comments Form -->
<div id="reply-form" class="post-content-box comments-form">
  <div class="box-title">
    <h4 class="widget-title comments-title"><?php echo esc_html__('Leave a reply' , 'dawn'); ?></h4>
  </div>
  <div class="form">
    <?php

    // Check if comments registration is required
    if(get_option('comment_registration') == 1 && !is_user_logged_in()) :
        echo '<p>'.esc_html__('Only registerd members can post a comment , ' , 'dawn').'<a href="'.wp_login_url(get_permalink()).'">'.esc_html__('Login / Register' , 'dawn').'</a></p>';

    else :
        $reqs = '';
        if($req) $reqs = '('.esc_html__('required' , 'dawn').')'; else $reqs = '';
        $commenter = wp_get_current_commenter();
        $comment_form_args = array(
            'id_form' => 'comments-form',
            'comment_notes_before' => '' ,
            'comment_notes_after' => '',
            'id_submit' => 'submit',
            'class_submit' => 'button button-large button-with-background',
            'title_reply' => '' ,
              'title_reply_to' => wp_kses( __('Leave a Reply to %s or' , 'dawn') , wp_kses_allowed_html('data') ),
              'cancel_reply_link' => esc_html__( 'Cancel Reply' , 'dawn' ),
              'label_submit' => esc_html__( 'Submit' , 'dawn' ),
              'comment_field' => '<textarea name="comment" id="comment-text" placeholder="'.esc_html__('Write Message' , 'dawn').'" class="comment-text"></textarea>' ,
              'fields' => apply_filters( 'comment_form_default_fields', array(

                            'author' => '<br /><input type="text" value="'.esc_attr( $commenter['comment_author'] ).'" name="author" class="comment-name textfield"  id="comment-name" placeholder="'.esc_html__('Your Name *' , 'dawn').'" />' ,


                            'email' => '<input type="text" value="'. esc_attr(  $commenter['comment_author_email'] ).'" name="email" class="comment-email textfield"  id="comment-email" placeholder="'.esc_html__('Your Email *' , 'dawn').'" />' ,

                            'website' => '<input type="text" value="'. esc_attr(  $commenter['comment_author_url'] ).'" name="url" class="comment-website textfield"  id="comment-website" placeholder="'.esc_html__('Your Website ' , 'dawn').'" />'

                          ) )
          );

        comment_form($comment_form_args);

    endif; // end comments registration check. ?>
  </div>
</div><!-- end form -->
<?php endif; // end if comments open ?>
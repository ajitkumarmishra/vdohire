<?php $dawn_theme_options = dawn_theme_settings(); ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <!--Added by Synergy-->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <!--End Synergy content-->
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!--Added by Synergy-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="author" content="" />
    <meta name="description" content="Guidance, Articles, Stories, Case Studies & News for fresher’s on how to get recruited fast. Subscribe now." />
    <meta name="keywords"  content="first job, entry level jobs, first time jobs, first job alert, first time job seeker, fresher job" />
    <!--End Synergy content-->
    <?php
    if( (function_exists('wp_site_icon') && !has_site_icon()) || (!function_exists('wp_site_icon'))) {

        if (isset($dawn_theme_options['favicon']['url']) && $dawn_theme_options['favicon']['url'] != '' && !has_site_icon()) :
        ?>
        <!-- FavIcon -->
        <link rel="shortcut icon" href="<?php echo esc_url($dawn_theme_options['favicon']['url']); ?>">
        <?php
        endif;

    } ?>
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <!--Added by Synergy-->
    <title>VDOHire Blog – 8 Reasons Why You Should Be Using Video Interviews In Your Hiring Process</title>
    <?php wp_head(); ?>
    <!--End Synergy content-->
	<link rel="stylesheet" href="http://firstjob.co.in/blog/wp-content/themes/dawn/css/menual_css.css" type="text/css" />
</head>
 <body <?php body_class(); ?>>
     <section id="navigation-responsive">
       <div class="container">
         <div class="row">
           <div class="col-md-12">
             <div class="searchform wide-form disabled">
               <?php echo get_search_form(); ?>
             </div>
             <div class="logo-wrapper">
               <div class="logo">
                    <?php echo dawn_theme_logo(); ?>
               </div><!-- end logo -->
               <div class="toggleMenu"><a href="#" class="toggle"><i class="fa fa-navicon"> </i></a></div>
               <div class="search-icon"><a href="#" class="search"><i class="fa fa-search"> </i></a></div>
             </div>
             <div class="right-panel">
               <div class="navigation-menu">
                 <?php
                     wp_nav_menu(array(
                     'theme_location' => 'primary',
                     'items_wrap' => '<ul>%3$s</ul>',
                     'container' => false
                     ));
                 ?>
               </div>
             </div>
           </div>
         </div>
       </div>
     </section><!-- navigation -->
     <section id="navigation">
       <div class="container">
         <div class="row">
           <div class="col-md-12">
             <div class="searchform wide-form disabled">
                <?php echo get_search_form(); ?>
             </div>
             <div class="logo">
                <?php echo dawn_theme_logo(); ?>
             </div>
             <div class="right-panel">
               <div class="navigation-menu">
                 <?php
                     wp_nav_menu(array(
                     'theme_location' => 'primary',
                     'items_wrap' => '<ul>%3$s</ul>',
                     'container' => false
                     ));
                 ?>
               </div>
               <div class="search-icon"><a href="#" class="search"><i class="fa fa-search"></i></a></div>
             </div>
           </div>
         </div>
       </div>
   </section><!-- end Navigation -->
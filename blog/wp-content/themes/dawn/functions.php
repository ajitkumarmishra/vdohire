<?php
/**
 * Dawn Theme
 */
define('DAWN_TNFRAMEWORK_MODULES', get_template_directory() . '/tnframework');
define('DAWN_TNFRAMEWORK_PLUGINS', DAWN_TNFRAMEWORK_MODULES . '/plugins/tgm/tgm_plugins');
define('DAWN_TNFRAMEWORK_POSTS_ROOT', DAWN_TNFRAMEWORK_MODULES . '/posts');

/* -------------------------------------------------------------*/
/* Theme Settings  */
/* -------------------------------------------------------------*/
function dawn_theme_settings(){
	// Redux Framework Settings
	return get_option('dawn_redux');
}

// SCSSPHP
require_once DAWN_TNFRAMEWORK_MODULES . '/scssphp/scss.inc.php';
use Leafo\ScssPhp\Compiler;



/* -------------------------------------------------------------*/
/* Plugins  */
/* -------------------------------------------------------------*/

require_once(DAWN_TNFRAMEWORK_MODULES . '/plugins/tgm/class-tgm-plugin-activation.php');

add_action('tgmpa_register' , 'dawn_register_plugin');
function dawn_register_plugin(){

	$plugins = array(

		array(
            'name'			=> esc_html__('Dawn Post Settings ' , 'dawn'), // The plugin name
            'slug'			=> 'dawn_post_settings', // The plugin slug (typically the folder name)
            'source'			=> DAWN_TNFRAMEWORK_PLUGINS . '/dawn_post_settings/dawn_post_settings.zip', // The plugin source
            'required'			=> true, // If false, the plugin is only 'recommended' instead of required
            'version'			=> '1.0', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
            'force_activation'		=> false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
            'force_deactivation'	=> false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
            'external_url'		=> '' // If set, overrides default API URL and points to an external URL
        ) ,
        array(
            'name'			=> esc_html__('Dawn Shortcodes' , 'dawn'), // The plugin name
            'slug'			=> 'dawn_shortcodes', // The plugin slug (typically the folder name)
            'source'			=> DAWN_TNFRAMEWORK_PLUGINS . '/dawn_shortcodes/dawn_shortcodes.zip', // The plugin source
            'required'			=> true, // If false, the plugin is only 'recommended' instead of required
            'version'			=> '1.0', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
            'force_activation'		=> false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
            'force_deactivation'	=> false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
            'external_url'		=> '' // If set, overrides default API URL and points to an external URL
        ) ,

		array(
            'name'			=> esc_html__('Dawn ReduxFramework' , 'dawn'), // The plugin name
            'slug'			=> 'dawn_redux_framework', // The plugin slug (typically the folder name)
            'source'			=> DAWN_TNFRAMEWORK_PLUGINS . '/dawn_redux_framework/dawn_redux_framework.zip', // The plugin source
            'required'			=> true, // If false, the plugin is only 'recommended' instead of required
            'version'			=> '1.0', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
            'force_activation'		=> false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
            'force_deactivation'	=> false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
            'external_url'		=> '' // If set, overrides default API URL and points to an external URL
        ) ,
		array(
            'name'			=> esc_html__('Dawn Slides' , 'dawn'), // The plugin name
            'slug'			=> 'dawn_slides', // The plugin slug (typically the folder name)
            'source'			=> DAWN_TNFRAMEWORK_PLUGINS . '/dawn_slides/dawn_slides.zip', // The plugin source
            'required'			=> true, // If false, the plugin is only 'recommended' instead of required
            'version'			=> '1.0', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
            'force_activation'		=> false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
            'force_deactivation'	=> false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
            'external_url'		=> '' // If set, overrides default API URL and points to an external URL
        ) ,
		array(
            'name'			=> esc_html__('Dawn Widgets' , 'dawn'), // The plugin name
            'slug'			=> 'dawn_widgets', // The plugin slug (typically the folder name)
            'source'			=> DAWN_TNFRAMEWORK_PLUGINS . '/dawn_widgets/dawn_widgets.zip', // The plugin source
            'required'			=> true, // If false, the plugin is only 'recommended' instead of required
            'version'			=> '1.0', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
            'force_activation'		=> false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
            'force_deactivation'	=> false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
            'external_url'		=> '' // If set, overrides default API URL and points to an external URL
        ) ,

		array(
            'name'			=> esc_html__('WP Instagram Widget' , 'dawn'), // The plugin name
            'slug'			=> 'wp-instagram-widget', // The plugin slug (typically the folder name)
            'required'			=> false, // If false, the plugin is only 'recommended' instead of required
        ) ,
    );


    /**
     * Array of configuration settings. Amend each line as needed.
     * If you want the default strings to be available under your own theme domain,
     * leave the strings uncommented.
     * Some of the strings are added into a sprintf, so see the comments at the
     * end of each line for what each argument will be.
     */
    $config = array(
        'domain'		=> 'dawn', // Text domain - likely want to be the same as your theme.
        'default_path'		=> '', // Default absolute path to pre-packaged plugins
        'parent_slug'	=> 'themes.php', // Default parent URL slug
        'menu'			=> esc_html__('install-required-plugins' , 'dawn'), // Menu slug
		'capability'   => 'edit_theme_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
        'has_notices'		=> true, // Show admin notices or not
        'is_automatic'		=> true, // Automatically activate plugins after installation or not
        'message'		=> '', // Message to output right before the plugins table

    );
	tgmpa( $plugins, $config );
}


/* --------------------------------------------------------------
Load Theme Framework
-------------------------------------------------------------- */
require_once DAWN_TNFRAMEWORK_MODULES . '/init.php';


/* --------------------------------------------------------------
Theme Translation
-------------------------------------------------------------- */
function dawn_theme_translation() {
	load_theme_textdomain('dawn', get_template_directory() . '/lang');
}
add_action('after_setup_theme', 'dawn_theme_translation');

/* --------------------------------------------------------------
Theme Support
-------------------------------------------------------------- */
function dawn_theme_support(){
	add_theme_support('automatic-feed-links');
	add_theme_support( 'title-tag' );
	add_theme_support('post-thumbnails');
	add_theme_support('post-formats' , array('video' , 'audio' , 'image' , 'standard' , 'gallery' , 'quote' , 'link'));
	register_nav_menu('primary', esc_html__('Main Menu', 'dawn'));
}
add_action('after_setup_theme' , 'dawn_theme_support');

/* Menu attributes */
function dawn_menu_items_attrs($atts, $item, $args) {
	$atts['data-id'] = $item->ID;

	return $atts;
}
add_filter('nav_menu_link_attributes', 'dawn_menu_items_attrs', 10, 3);

/* Image Size */
if(function_exists('add_image_size'))
{
	add_image_size('dawn-post' , 800 , 420 , true);
	add_image_size('dawn-intro' , 550 , 500 , true);
	add_image_size('dawn-intro-thumbnail' , 320 , 200 , true);
	add_image_size('dawn-grid-post' , 550 , 320 , true);
	add_image_size('dawn-widget-thumbnail' , 120 , 80 , true);
}


/* -------------------------------------------------------------*/
/* Theme Logo  */
/* -------------------------------------------------------------*/
function dawn_theme_logo(){

	$dawn_theme_options = dawn_theme_settings();

	if(	$dawn_theme_options != '' && isset(	$dawn_theme_options['logo']	)	){

		$dawn_logo = $dawn_theme_options['logo'];

		if(	isset($dawn_logo['url']) && $dawn_logo['url'] != ''	)
		{
			return '<a href="'.esc_url(get_site_url()).'" class="logo"><img src="'.esc_attr($dawn_logo['url']).'" alt="'.get_bloginfo('description').'"></a>';
		}
	}
	else {
		return '<a href="'.esc_url(get_site_url()).'" class="logo"><img src="'.esc_attr(get_template_directory_uri() . '/images/logo.png').'" alt="'.esc_attr(get_bloginfo('description')).'"></a>';
	}

}
/* -------------------------------------------------------------*/
/* SCSSPHP  */
/* -------------------------------------------------------------*/
/* -
Theme SCSS compiler will compile user custom style settings into CSS ,
And it will update "dawn_theme_setting_sass" option with the new CSS data
- */
if(class_exists('ReduxFramework'))
{
	function dawn_theme_settings_compiler($options, $css) {

		$scss = new Compiler();
		$scss->setImportPaths(get_template_directory() . '/sass/');
		$scss->setFormatter('Leafo\ScssPhp\Formatter\Crunched');


		// Style Settings
		$settings = array(
			'background-color' => (isset($options['background_color']) ) ? $options['background_color'] : '#f4f4f4',

			'accent-color' => (isset($options['accent_color']) ) ? $options['accent_color'] : '#37A8E0',

			'headers-font-family' => (isset ($options['headers_font_family']['font-family']) ) ? $options['headers_font_family']['font-family'] : 'Raleway',

			'headers-font-color' => (isset($options['headers_font_family']['color']) ) ? $options['headers_font_family']['color'] : '#4d4d4d',

			'headers-font-weight' => (isset($options['h1h2_font_weight']['font-weight']) ) ? $options['h1h2_font_weight']['font-weight'] : '900',

			'sub-headers-font-weight' => (isset($options['h3h4_font_weight']['font-weight']) ) ? $options['h3h4_font_weight']['font-weight'] : '800',

			'items-headers-font-weight' => (isset($options['h5h6_font_weight']['font-weight']) ) ? $options['h5h6_font_weight']['font-weight'] : '600',

			'body-font-family' => (isset($options['body_font_family']['font-family']) ) ? $options['body_font_family']['font-family'] : 'Lato',

			'body-font-weight' => (isset($options['body_font_family']['font-weight']) ) ? $options['body_font_family']['font-weight'] : 'normal',

			'body-font-color' => (isset($options['body_font_family']['color']) ) ? $options['body_font_family']['color'] : '#898989',

			'body-font-size' => (isset($options['body_font_family']['font-size']) ) ? $options['body_font_family']['font-size'] : '16px',

			'body-line-height' => (isset($options['body_font_family']['line-height']) ) ? $options['body_font_family']['line-height'] : '28px',

			'sidebar-font-size' => (isset($options['sidebar_font_size']['font-size']) ) ? $options['sidebar_font_size']['font-size'] : '14px',

			'nav-font-family' => (isset($options['navigation_font']['font-family']) ) ? $options['navigation_font']['font-family'] : 'Raleway',

			'nav-font-weight' => (isset($options['navigation_font']['font-weight']) ) ? $options['navigation_font']['font-weight'] : '700',

			'nav-font-size' => (isset($options['navigation_font']['font-size']) ) ? $options['navigation_font']['font-size'] : '14px',

			'nav-font-color' => (isset($options['navigation_font']['color']) ) ? $options['navigation_font']['color'] : '#7a7a7a'

		);

		// Prepare Values
		$scss->setVariables($settings);

		// SCSS
		$return = '@import "style.scss";';

		// Compile
		$compiled =  $scss->compile($return);

		update_option('dawn_theme_setting_sass' , $compiled);
	}
	add_filter('redux/options/dawn_redux/compiler', 'dawn_theme_settings_compiler', 10, 2);

}


/* -------------------------------------------------------------*/
/* Theme Scripts And CSS  */
/* -------------------------------------------------------------*/
/* - Register Default Theme Fonts - */
function dawn_default_fonts(){

	$fonts_url = '';
    if ( 'off' !== _x( 'on', 'Google font: on or off', 'dawn' ) ) {

	    $fonts_url = add_query_arg( 'family', urlencode( 'Lato:400,300,700,700italic,900|Raleway:400,300,500,600,700,800,900&subset=latin,latin-ext' ), "//fonts.googleapis.com/css" );
	}

	return $fonts_url;
}

/* - Theme CSS and Javascript - */
function dawn_theme_scripts() {

	// Theme Options
	$dawn_theme_options = dawn_theme_settings();

	// Comments JS
	if (is_singular()) {
		wp_enqueue_script('comment-reply');
	}
	// MediaElements JS
	wp_enqueue_script('wp-mediaelement' ,false , array('jquery') , NULL , false);

	// Other Javascript libs
	$theme_javascript_libs = array( 'jquery-ui-custom-min.js' ,
									'jquery.flexslider-min.js' ,
									'jquery.masonry.min.js');
	foreach ($theme_javascript_libs as $lib) {
		wp_enqueue_script($lib, get_template_directory_uri() . '/javascript/' . $lib , array('jquery'), false, true);
	}

	// Custom Javascript Settings
	wp_enqueue_script('dawn-custom-javascript' ,  get_template_directory_uri() . '/javascript/script.js' , array('jquery'), false, true);



	// Theme CSS
	wp_enqueue_style('dawn-theme-fonts', dawn_default_fonts() , array() , '1.0.0');
	wp_enqueue_style('dawn-theme-css-plugins' , get_stylesheet_directory_uri() . '/css/plugins.css');

	if(get_option('dawn_redux') == '' || get_option('dawn_theme_setting_sass') == '') {

		wp_enqueue_style('dawn-theme-css-style' , get_stylesheet_directory_uri() . '/css/style.css');
	}

	// Inline CSS If Redux Framework Is Installed
	if(get_option('dawn_redux') != '')
	{
		if(wp_style_is('redux-google-fonts-dawn_redux' , 'done')){

			wp_add_inline_style('redux-google-fonts-dawn_redux', get_option('dawn_theme_setting_sass')	);
		}
		else{

			wp_add_inline_style('dawn-theme-css-plugins', get_option('dawn_theme_setting_sass')	);
		}
	}

}
add_action('wp_enqueue_scripts', 'dawn_theme_scripts' );

/* -------------------------------------------------------------*/
/* Content  */
/* -------------------------------------------------------------*/

/* - Content Width - */
if (!isset($content_width)) {
	$content_width = 940;
}

/* -------------------------------------------------------------*/
/* Sidebars  */
/* -------------------------------------------------------------*/

// Theme Sidebars
$dawn_theme_sidebars = array(
	esc_html__('one' , 'dawn') ,
	esc_html__('two' , 'dawn') ,
	esc_html__('three' , 'dawn') ,
	esc_html__('four' , 'dawn') ,
	esc_html__('five' , 'dawn'));

if($dawn_theme_sidebars != get_option('dawn_theme_sidebars'))
{
	update_option('dawn_theme_sidebars' , $dawn_theme_sidebars);
}

// Register Sidebars
function dawn_sidebars()
{
	global $dawn_theme_sidebars;

	// Sidebars
	foreach (get_option('dawn_theme_sidebars') as $sidebar) {
			register_sidebar(array(
	           'name' => esc_html__('Sidebar ' , 'dawn') . ucfirst($sidebar),
	           'id' => $sidebar,
	           'description' => esc_html__('Sidebar ' , 'dawn').ucfirst($sidebar).esc_html__(' Widgets' , 'dawn'),
	           'before_widget' => '<div class="widget">',
	           'after_widget' => '</div></div>',
	           'before_title' => '<h4 class="widget-title">',
	           'after_title' => '</h4><div class="widget-content">'
	        ));
	}


	// Footer
	register_sidebar(array(
	   'name' => esc_html__('Footer' , 'dawn'),
	   'id' => 'footer',
	   'description' => esc_html__('Footer Widgets' , 'dawn'),
	   'before_widget' => '<div class="widget col-md-4">',
	   'after_widget' => '</div></div>',
	   'before_title' => '<h4 class="widget-title">',
	   'after_title' => '</h4><div class="widget-content">'
	));
}
add_action('widgets_init' , 'dawn_sidebars');



/* -------------------------------------------------------------*/
/* Post Content Limitation  */
/* -------------------------------------------------------------*/
function dawn_limit_post_content($content){

	$dawn_theme_options = dawn_theme_settings();

	// Check Content Limitation
	$content_limitation = (isset($dawn_theme_options['content_limitation']) && $dawn_theme_options['content_limitation'] != '') ? $dawn_theme_options['content_limitation'] : 11;

	if((is_archive() || is_page_template() || is_search()) && (isset($dawn_theme_options['enable_content_limitation']) && $dawn_theme_options['enable_content_limitation'] == 1) ){
		$return = explode(' ' , $content);
		$return = array_slice($return , 0 , intval($content_limitation));
		$return = implode(' ' , $return);
		return wp_strip_all_tags($return);
	}
	else{
		return $content;
	}
}
add_action('the_content' , 'dawn_limit_post_content' , 20);


/* -------------------------------------------------------------*/
/* Custom Search Form  */
/* -------------------------------------------------------------*/
function dawn_custom_search_form($form){

	$form  = '<form method="get" action="'.	home_url( '/' )	.'">';
	$form .= '	<input name="s" type="text" value="' . esc_attr(get_search_query()) . '"';
	$form .= '	class="query" placeholder="'.	esc_attr(esc_html__('Type And Hit Enter' , 'dawn')).	'"  />';
	$form .= '	</form>';
	return $form;
}
add_action('get_search_form' , 'dawn_custom_search_form');

/* -------------------------------------------------------------*/
/* Commen  */
/* -------------------------------------------------------------*/


// User Profile Fields
add_action( 'show_user_profile', 'dawn_author_fields' );
add_action( 'edit_user_profile', 'dawn_author_fields' );
$dawn_profile_fields = array(
				'facebook-url' => esc_html__('Facebook URL' , 'dawn') ,
				'twitter-url' => esc_html__('Twitter URL' , 'dawn') ,
				'pinterest-url' => esc_html__('Pinterest URL' , 'dawn'),
				'google-plus-url' => esc_html__('Google Plus URL' , 'dawn'),
				'dribbble-url'=> esc_html__('Dribbble URL' , 'dawn')
				);
function dawn_author_fields( $user ) {

			global $dawn_profile_fields;

			$return = '<h3>'.esc_html__('Social Media Links' , 'dawn').'</h3>';
			$return .= '<table class="form-table">';
			foreach ($dawn_profile_fields as $field) {
				$fieldID = strtolower(str_replace(' ', '-', $field));
				$return .= '<tr>';
				$return .= '<th><label for="'.esc_attr($fieldID).'">'.$field.'</label></th>';
				$return .= '<td><input type="text" id="'.esc_attr($fieldID).'" value="'.esc_attr( get_the_author_meta( $fieldID, $user->ID ) ).'" name="'.esc_attr($fieldID).'" class="'.$fieldID.'"></td>';
				$return .= '</tr>';
			}
			$return .= '</table>';
			echo $return;
}

// update fields
add_action( 'personal_options_update', 'dawn_save_portfolio_fields' );
add_action( 'edit_user_profile_update', 'dawn_save_portfolio_fields' );

function dawn_save_portfolio_fields( $user_id ) {

	global $dawn_profile_fields;

	if ( !current_user_can( 'edit_user', $user_id ) )
		return false;

		foreach ($dawn_profile_fields as $field) {
			 $fieldID = strtolower(str_replace(' ', '-', $field));
			 update_user_meta( $user_id,  $fieldID, $_POST[ $fieldID] );
		}

}



/** Remove redux menu under the tools **/
add_action( 'admin_menu', 'dawn_remove_redux_submenu',12 );
function dawn_remove_redux_submenu() {
    remove_submenu_page('tools.php','redux-about');
}
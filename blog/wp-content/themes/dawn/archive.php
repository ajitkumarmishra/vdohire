<?php
/* - Archive Page - */
$dawn_theme_options = dawn_theme_settings();

/* - Header - */
get_header();

// Current Page
$paged = get_query_var('page') ? get_query_var('page') : get_query_var('paged');


?>
<!-- Blog section -->
<section id="blog-classic" class="content-wrapper"><!-- Left Side -->
  <section class="container">
    <div class="row">
      <div class="col-md-8 posts-container">
        <?php if(have_posts()) : ?>

        <!-- Archive Title -->
        <div class="single-post post-type-image">
          <div class="post-content-outer container-box">
              <div class="post-title">
                  <h2 style="font-weight: normal;"><?php

                  echo esc_html__('Search Results For : ' , 'dawn');

                  if(is_author()) :
                      $author = get_userdata( get_query_var('author') );
                      $query_name =  $author->display_name;
                      elseif(is_day()) :
                      $query_name =  get_the_date();
                      elseif(is_month()) :
                      $query_name =  single_month_title(' ');
                      elseif(is_year()) :
                      $query_name =  get_the_date( _x( 'Y', '', 'dawn' ) );
                      elseif(is_category()) :
                      $query_name =  single_cat_title('' , false);
                      elseif(is_tag()) :
                      $query_name =  single_tag_title('' , false);
                  endif;

                  echo '<span class="uppercase">'.$query_name.'</span>';

                  ?></h2>
              </div><!-- end post title -->
          </div>
        </div><!-- end Archive Title -->

        <?php

        while(have_posts()) : the_post();

        // Post format
        $post_format = (get_post_format() != '') ? get_post_format() : 'standard';

        // Get post by post format
        tnframework_get_post(DAWN_TNFRAMEWORK_POSTS_ROOT . '/post/' , $post_format);

        endwhile;

        // Pagination
        echo tnframework_pagination($wp_query->max_num_pages , true);

        endif; wp_reset_postdata(); ?>

      </div><!-- end Left Side Posts Container -->

      <!-- Right Side (Sidebar) -->
      <div class="col-md-4">
        <div id="sidebar-widgets-container" class="widgets-container">
            <?php get_sidebar(); ?>
        </div><!-- end widgets container -->
      </div><!-- End Right Side (Sidebar) -->
  </div><!-- end row -->
  </section>
</section>
<!-- End Blog Section -->

<?php
/* - Footer - */
get_footer();
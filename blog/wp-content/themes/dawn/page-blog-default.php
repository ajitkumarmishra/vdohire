<?php
/*
    Template Name: Blog With Classic Slider
*/
$dawn_theme_options = dawn_theme_settings();

// Header
get_header();

// Templates
get_template_part('template' , 'slider');
get_template_part('template' , 'blog-classic');

// Footer
get_footer();
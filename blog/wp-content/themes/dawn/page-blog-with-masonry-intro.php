<?php
/*
    Template Name: Blog With Masonry Intro
*/
$dawn_theme_options = dawn_theme_settings();

// Header
get_header();

// Templates
get_template_part('template' , 'masonry-intro');
get_template_part('template' , 'blog-classic');

// Footer
get_footer();
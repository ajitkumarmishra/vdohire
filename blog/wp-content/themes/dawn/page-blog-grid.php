<?php
/*
    Template Name: Blog Grid
*/
$dawn_theme_options = dawn_theme_settings();

// Header
get_header();

// Templates
get_template_part('template' , 'slider');
get_template_part('template' , 'blog-grid');

// Footer
get_footer();
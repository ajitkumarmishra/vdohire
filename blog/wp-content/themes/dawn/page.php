<?php
$dawn_theme_options = dawn_theme_settings();
get_header();
?>

<section id="blog-classic" class="content-wrapper single-post-page"><!-- Left Side -->
  <section class="container">
    <div class="row">
    <?php if(have_posts()) : while(have_posts()) : the_post(); ?>
        <div class="col-md-8 posts-container">

            <div class="single-post post-type-image">
              <div class="post-content-outer container-box">
                <div class="post-meta-top">
                    <?php if(is_sticky()) { echo '<p>'.esc_html__('Sticky' , 'dawn').'</p>'; } ?><p><?php echo tnframework_list_categories(get_the_ID() ,true , ' '); ?>
                    <a href="<?php echo esc_url(get_permalink()); ?>"><?php echo get_the_date('M d , Y'); ?></a></p>
                </div><!-- end meta top -->
                <div class="post-title">
                    <h2> <a class="uppercase" href="<?php echo esc_url(get_permalink()); ?>"><?php echo get_the_title(); ?></a></h2>
                </div>
                <div class="post-content-inner">
                        <?php the_content(); ?>
                </div><!-- end post content inner -->
              </div>
            </div><!-- end page content -->

            <?php comments_template(); ?>
        </div><!-- end Left Side Posts Container -->
    <?php endwhile; endif; wp_reset_postdata(); ?>

        <!-- Right Side (Sidebar) -->
        <div class="col-md-4">
        <div id="sidebar-widgets-container" class="widgets-container">
            <?php get_sidebar(); ?>
        </div><!-- end widgets container -->
        </div><!-- End Right Side (Sidebar) -->
    </div>
  </section>
</section>

<?php
/* - Footer - */
get_footer();
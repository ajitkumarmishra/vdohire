<?php
/*
 Plugin Name: Dawn ReduxFramework
 Plugin URI: http://themeforest.net/user/montuthemes
 Description: Settings Panel For "Dawn" Theme
 Version: 1.0
 Author: Montuthemes
 Author URI: http://themeforest.net/user/montuthemes
 Text Domain: dawn
*/


/* --------------------------------------------------------------
Include The Framework
-------------------------------------------------------------- */
if (!class_exists('ReduxFramework')) {

	require_once trailingslashit(plugin_dir_path(__FILE__)) . '/admin/admin-init.php';
}

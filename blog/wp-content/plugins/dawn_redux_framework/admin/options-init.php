<?php

    /* - Get sidebars list  - */
    $dawn_config_get_sidebars = get_option('dawn_theme_sidebars');
    $dawn_theme_sidebars = array();
    if(is_array($dawn_config_get_sidebars) && !empty($dawn_config_get_sidebars))
    {
    	foreach ($dawn_config_get_sidebars as $key => $value) {
    		$dawn_theme_sidebars[$value] = $value;
    	}

    }

    /**
     * For full documentation, please visit: http://docs.reduxframework.com/
     * For a more extensive sample-config file, you may look at:
     * https://github.com/reduxframework/redux-framework/blob/master/sample/sample-config.php
     */

    if ( ! class_exists( 'Redux' ) ) {
        return;
    }

    // This is your option name where all the Redux data is stored.
    $opt_name = "dawn_redux";

    /**
     * ---> SET ARGUMENTS
     * All the possible arguments for Redux.
     * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
     * */

    $theme = wp_get_theme(); // For use with some settings. Not necessary.

    $args = array(
        'opt_name' => 'dawn_redux',
        'use_cdn' => TRUE,
        'display_name' => __('Theme Settings' , 'dawn'),
        'display_version' => '1.0.0',
        'page_slug' => 'dawn-theme-options',
        'page_title' => __('Redux Settings' , 'dawn'),
        'dev_mode' => FALSE ,
        'update_notice' => TRUE,
        'intro_text' => __('For Online Documentations : <a href="://montuthemes.com/docs/dawn/">Click Here</a>' , 'dawn'),
        'footer_text' => __( 'Dawn Theme Settings , MontuThemes 2016.' , 'dawn'),
        'admin_bar' => TRUE,
        'menu_type' => 'menu',
        'menu_title' => __('Theme Settings' , 'dawn'),
        'allow_sub_menu' => TRUE,
        'page_parent_post_type' => 'your_post_type',
        'customizer' => TRUE,
        'default_mark' => '*',
        'hints' => array(
            'icon_position' => 'right',
            'icon_color' => 'lightgray',
            'icon_size' => 'normal',
            'tip_style' => array(
                'color' => 'light',
            ),
            'tip_position' => array(
                'my' => 'top left',
                'at' => 'bottom right',
            ),
            'tip_effect' => array(
                'show' => array(
                    'duration' => '500',
                    'event' => 'mouseover',
                ),
                'hide' => array(
                    'duration' => '500',
                    'event' => 'mouseleave unfocus',
                ),
            ),
        ),
        'output' => TRUE,
        'output_tag' => TRUE,
        'settings_api' => TRUE,
        'cdn_check_time' => '1440',
        'compiler' => TRUE,
        'page_permissions' => 'manage_options',
        'save_defaults' => TRUE,
        'show_import_export' => TRUE,
        'database' => 'options',
        'transient_time' => '3600',
        'network_sites' => TRUE,
    );

    Redux::setArgs( $opt_name, $args );

    /*
     * ---> END ARGUMENTS
     */

    /*
     * ---> START HELP TABS
     */



    // Set the help sidebar
    $content = wp_kses( __('<p>This is the sidebar content, HTML is allowed.</p>', 'dawn') , wp_kses_allowed_html('data') );
    Redux::setHelpSidebar( $opt_name, $content );


    /*
     * <--- END HELP TABS
     */


    /*
     *
     * ---> START SECTIONS
     *
     */





    // Home Page Settings
    Redux::setSection( $opt_name , array(
        'title' => __('Website Settings', 'dawn'),
        'id' => 'dawn-website-settings' ,
        'desc' => __('You can upload your own website logo , fav icon , tracking code ..', 'dawn'),
        'icon' => 'el-icon-home',
        'fields' => array(

            array(
                'id' => 'logo',
                'type' => 'media',
                'title' => __('Logo', 'dawn'),
                'mode' => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc' => __('Upload your website logo .', 'dawn'),
            ),
            array(
                'id' => 'favicon',
                'type' => 'media',
                'title' => wp_kses( __('Website Icon <br />,<strong>(For Older WordPress Sites)</strong>', 'dawn') , wp_kses_allowed_html('data')),
                'mode' => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc' => wp_kses(__('<strong>Note</strong> : Site icon is provided via customizer in WordPress 4.3+ , This option is for older versions of WordPress .', 'dawn') , wp_kses_allowed_html('data')),
            )

        ),
    ));

    // social settings
    Redux::setSection( $opt_name , array(
        'title' => __('Social Media Settings', 'dawn'),
        'id' => 'dawn-socialmedia-settings' ,
        'desc' => __('Edit social icons link , add twitter secret keys .. etc', 'dawn'),
        'icon' => 'el-icon-twitter',
        'fields' => array(
                array(
                'id'=>'facebook',
                'type' => 'text',
                'title' => __('Facebook Page Name', 'dawn'),
                'desc'=> __('You can add your Facebook page name here .', 'dawn'),
                'default' => 'facebook'

                ),
                array(
                'id'=>'twitter',
                'type' => 'text',
                'title' => __('Twitter', 'dawn'),
                'desc'=> __('You can add your Twitter page name here .', 'dawn'),
                'default' => 'twitter'

                ),
                array(
                'id'=>'twitter_consumer_key',
                'type' => 'text',
                'title' => __('Twitter Consumer Key', 'dawn'),
                'desc'=> __('You can add your Twitter consumer key here .', 'dawn')

                ),
                array(
                'id'=>'twitter_consumer_secret',
                'type' => 'text',
                'title' => __('Twitter Consumer Secret', 'dawn'),
                'desc'=> __('You can add your Twitter consumer secret here.', 'dawn')

                ),
                array(
                'id'=>'twitter_access_token',
                'type' => 'text',
                'title' => __('Twitter Access Token', 'dawn'),
                'desc'=> __('You can add your Twitter access token here..', 'dawn')

                ),
                array(
                'id'=>'twitter_access_token_secret',
                'type' => 'text',
                'title' => __('Twitter Access Token Secret', 'dawn'),
                'desc'=> __('You can add your Twitter access token secret here .', 'dawn')

                ),
                array(
                'id'=>'dribbble',
                'type' => 'text',
                'title' => __('Dribbble', 'dawn'),
                'desc'=> __('You can add url .', 'dawn'),
                'default' => '#'

                ),
                array(
                'id'=>'flickr',
                'type' => 'text',
                'title' => __('Flickr', 'dawn'),
                'desc'=> __('You can add your Flickr url .', 'dawn'),
                'default' => '#'

                ),
                array(
                'id'=>'youtube',
                'type' => 'text',
                'title' => __('Youtube', 'dawn'),
                'desc'=> __('You can add your Youtube channel url', 'dawn'),
                'default' => '#'

                ),
                array(
                'id'=>'pinterest',
                'type' => 'text',
                'title' => __('Pinterest', 'dawn'),
                'desc'=> __('You can add your Pinterest  url', 'dawn'),
                'default' => '#'

                ),
                array(
                'id'=>'linkedin',
                'type' => 'text',
                'title' => __('Linkedin', 'dawn'),
                'desc'=> __('You can add your Linkedin  url', 'dawn'),
                'default' => '#'

                ),
                array(
                'id'=>'rss',
                'type' => 'text',
                'title' => __('RSS', 'dawn'),
                'desc'=> __('You can add your RSS url.', 'dawn'),
                'default' => '#'

                ),
                array(
                'id'=>'tumblr',
                'type' => 'text',
                'title' => __('Tumblr', 'dawn'),
                'desc'=> __('You can add your tumblr url', 'dawn'),
                'default' => '#'

                ),
                array(
                'id'=>'vimeo',
                'type' => 'text',
                'title' => __('Vimeo', 'dawn'),
                'desc'=> __('You can add your vimeo url', 'dawn'),
                'default' => '#'

                ),
                array(
                'id'=>'soundcloud',
                'type' => 'text',
                'title' => __('Soundcloud', 'dawn'),
                'desc'=> __('You can add your soundcloud url', 'dawn'),
                'default' => '#'

                ),
                array(
                'id'=>'instagram',
                'type' => 'text',
                'title' => __('Instagram', 'dawn'),
                'desc'=> __('You can add your instagram url', 'dawn'),
                'default' => '#'

                ),
                array(
                'id'=>'skype',
                'type' => 'text',
                'title' => __('Skype', 'dawn'),
                'desc'=> __('You can add your skype url', 'dawn'),
                'default' => '#'

                ),
                array(
                'id'=>'google',
                'type' => 'text',
                'title' => __('Google Plus', 'dawn'),
                'desc'=> __('You can add your googleplus url', 'dawn'),
                'default' => '#'

                )
                ,
                array(
                'id'=>'github',
                'type' => 'text',
                'title' => __('Github', 'dawn'),
                'desc'=> __('You can add your Github url', 'dawn'),
                'default' => '#'

                )



        )
    ));


    // Error Messages
    Redux::setSection( $opt_name , array(
        'title' => __('Alerts And Messages', 'dawn'),
        'id' => 'dawn-messages-settings' ,
        'desc' => __('Edit search messages , archive titles .. etc', 'dawn'),
        'icon' => 'el-icon-exclamation-sign',
        'fields' => array(
                array(
                'id'=>'search_error',
                'type' => 'textarea',
                'default' => __('No Search Results Found.' , 'dawn') ,
                'title' => __('No Search Results Message', 'dawn'),
                'desc'=> __('If there is no search results found , this message will appear .. ', 'dawn')
                ),
                array(
                'id'=>'404_message',
                'type' => 'textarea',
                'title' => __('Error 404 Message', 'dawn'),
                'default' => __('Error 404 , Page Not Found .' , 'dawn') ,
                'desc'=> __('You can add your own 404 page message here .. ', 'dawn')

                )
        )
    ));

    /* - Blog Settings - */
    Redux::setSection( $opt_name , array(
        'title' => __('Blog Settings', 'dawn'),
        'desc' => __('Edit blog settings .. ', 'dawn'),
        'id' => 'dawn-blog-settings' ,
        'icon' => 'el-icon-pencil-alt',
        'fields' => array(

            array(
            'id'=>'slider_posts_limit',
            'type' => 'slider',
            'title' => __('Limit Slider Posts', 'dawn'),
            'desc'=> __('Limit slider posts number , click and hold mouse button to increase / decrease value .. ', 'dawn'),
            'default' => '5' ,
            "min" 		=> "1",
            "step"		=> "1",
            "max" 		=> "500"
            ),
            array(
            'id'=>'blog_posts_limit',
            'type' => 'slider',
            'title' => __('Limit Blog Posts', 'dawn'),
            'desc'=> __('Limit blog posts number , click and hold mouse button to increase / decrease value .. ', 'dawn'),
            'default' => '5' ,
            "min" 		=> "1",
            "step"		=> "1",
            "max" 		=> "500"
            ),
            array(
            'id'=>'enable_content_limitation',
            'type' => 'switch',
            'title' => __('Limit Post Content In Blog Pages', 'dawn'),
            'desc'=> __('You can enable / disable post content limitation in blog pages , You can leave this option disabled if you are using [more] tag in posts .', 'dawn'),
            'default' => 2,
            'on' => 'Enabled',
            'off' => 'Disabled'
            ),
            array(
            'id'=>'content_limitation',
            'type' => 'slider',
            'title' => __('Content Limitation', 'dawn'),
            'desc'=> __('Limit the post content words in blog pages .', 'dawn'),
            'default' => '30' ,
            "min" 		=> "1",
            "step"		=> "1",
            "max" 		=> "10000"
            ),
            array(
            'id'=>'enable_comments',
            'type' => 'switch',
            'title' => __('Enable Comments Section', 'dawn'),
            'desc'=> __('You can enable / disable comments section entirely , including approved comments and comments form .', 'dawn'),
            'default' => 1,
            'on' => 'Enabled',
            'off' => 'Disabled'
            ),
            array(
                'id'=>'default_sidebar',
                'type' => 'select',
                'title' => __('Default Sidebar', 'dawn'),
                'desc' => __('Select the default sidebar for posts and pages .', 'dawn'),
                'options' => $dawn_theme_sidebars,
                'default' => '1'

            )

        )
    ));


    // theme style settings
    Redux::setSection( $opt_name , array(
        'title' => __('Style And Typography Settings', 'dawn'),
        'id' => 'dawn-typography-settings' ,
        'desc' => __('Edit theme style and typography settings .. ', 'dawn'),
        'icon' => 'el-icon-brush',
        'fields' => array(

                array(
                'id'=>'background_color',
                'type' => 'color',
                'compiler' => true,
                'title' => __('Background Color', 'dawn'),
                'desc' => __('Pick a background color .', 'dawn'),
                'default' => '#f5f5f5',
                'validate' => 'color',
                'transparent' => false
                ),
                array(
                'id'=>'accent_color',
                'type' => 'color',
                'title' => __('Accent Color', 'dawn'),
                'desc' => __('accent color for main theme elements , e.g button background color , links hover color .. ', 'dawn'),
                'default' => '#37A8E0',
                'validate' => 'color',
                'transparent' => false
                ),
                array(
                    'id'=>'headers_font_family',
                    'type' => 'typography',
                    'title' => __('Headers Font Family', 'dawn'),
                    //'compiler'=>true, // Use if you want to hook in your own CSS compiler
                    'google'=>true, // Disable google fonts. Won't work if you haven't defined your google api key
                    'font-backup'=>false, // Select a backup non-google font in addition to a google font
                    'font-style'=>false, // Includes font-style and weight. Can use font-style or font-weight to declare
                    'subsets'=>true, // Only appears if google is true and subsets not set to false
                    'font-size'=>false,
                    'line-height'=>false,
                    'font-weight' => false,
                    'font-style' => false,
                    'text-align' => false ,
                    'word-spacing'=>false, // Defaults to false
                    'letter-spacing'=>false, // Defaults to false
                    'color'=>true,
                    'preview'=>false,
                    'all_styles' => false,
                    'units'=>'px', // Defaults to px
                    'desc'=> __('Customize headers color and font family .', 'dawn'),
                    'default'=> array(
                        'color'=>"#4d4d4d",
                        'font-family'=>'Raleway'
                        ),
                    ),
                    array(
                        'id'=>'h1h2_font_weight',
                        'type' => 'typography',
                        'title' => __('Headers H1 And H2 Font Weight', 'dawn'),
                        'google'=>false,
                        'font-family' => false,
                        'font-backup'=>false,
                        'font-style'=>false,
                        'subsets'=>false,
                        'font-size'=>false,
                        'line-height'=>false,
                        'font-weight' => true,
                        'font-style' => false,
                        'text-align' => false ,
                        'word-spacing'=>false,
                        'letter-spacing'=>false,
                        'color'=>false,
                        'preview'=>false,
                        'all_styles' => false,
                        'units'=>'px', // Defaults to px
                        'desc'=> __('Customize headers font weight .', 'dawn'),
                        'default'=> array(
                            'font-weight'=> '900'
                        )
                    ),
                    array(
                            'id'=>'h3h4_font_weight',
                            'type' => 'typography',
                            'title' => __('Headers H3 And H4 Font Weight', 'dawn'),
                            'google'=>false,
                            'font-family' => false,
                            'font-backup'=>false,
                            'font-style'=>false,
                            'subsets'=>false,
                            'font-size'=>false,
                            'line-height'=>false,
                            'font-weight' => true,
                            'font-style' => false,
                            'text-align' => false ,
                            'word-spacing'=>false,
                            'letter-spacing'=>false,
                            'color'=>false,
                            'preview'=>false,
                            'all_styles' => false,
                            'units'=>'px', // Defaults to px
                            'desc'=> __('Customize headers font weight .', 'dawn'),
                            'default'=> array(
                                'font-weight'=> '800'
                            ),
                ),
                array(
                        'id'=>'h5h6_font_weight',
                        'type' => 'typography',
                        'title' => __('Headers H5 And H6 Font Weight', 'dawn'),
                        'google'=>true,
                        'font-family' => false,
                        'font-backup'=>false,
                        'font-style'=>true,
                        'subsets'=>false,
                        'font-size'=>false,
                        'line-height'=>false,
                        'font-weight' => true,
                        'font-style' => false,
                        'text-align' => false ,
                        'word-spacing'=>false,
                        'letter-spacing'=>false,
                        'color'=>false,
                        'preview'=>false,
                        'all_styles' => false,
                        'units'=>'px', // Defaults to px
                        'desc'=> __('Customize headers font weight .', 'dawn'),
                        'default'=> array(
                            'font-weight'=> '600',
                            'font-family' => 'Raleway'
                        ),
                ),
                array(
                    'id'=>'body_font_family',
                    'type' => 'typography',
                    'title' => __('Content Font Settings', 'dawn'),
                    'google'=>true,
                    'font-family' => true,
                    'font-backup'=>false,
                    'font-style'=>false,
                    'subsets'=>false,
                    'font-size'=>true,
                    'line-height'=>true,
                    'font-weight' => true,
                    'font-style' => true,
                    'text-align' => false ,
                    'word-spacing'=>false,
                    'letter-spacing'=>false,
                    'color'=>true,
                    'preview'=>false,
                    'all_styles' => false,
                    'units'=>'px', // Defaults to px
                    'desc'=> __('', 'dawn'),
                    'default'=> array(
                        'font-weight'=> '400',
                        'font-family' => 'Lato' ,
                        'color' => '#898989' ,
                        'font-size' => '16px' ,
                        'line-height' => '28px'
                    ),
                ),
                array(
                        'id'=>'sidebar_font_size',
                        'type' => 'typography',
                        'title' => __('Sidebar Fon Size and ', 'dawn'),
                        'google'=>false,
                        'font-family' => false,
                        'font-backup'=>false,
                        'font-style'=>false,
                        'subsets'=>false,
                        'font-size'=>true,
                        'line-height'=>false,
                        'font-weight' => false,
                        'font-style' => false,
                        'text-align' => false ,
                        'word-spacing'=>false,
                        'letter-spacing'=>false,
                        'color'=>false,
                        'preview'=>false,
                        'all_styles' => false,
                        'units'=>'px', // Defaults to px
                        'desc'=> __('', 'dawn'),
                        'default'=> array(
                            'font-size'=> '14px'
                        ),
                ),
                array(
                    'id'=>'navigation_font',
                    'type' => 'typography',
                    'title' => __('Navigation Font Settings', 'dawn'),
                    'google'=>true,
                    'font-family' => true,
                    'font-backup'=>false,
                    'font-style'=>false,
                    'subsets'=>false,
                    'font-size'=>true,
                    'line-height'=>false,
                    'font-weight' => true,
                    'font-style' => true,
                    'text-align' => false ,
                    'word-spacing'=>false,
                    'letter-spacing'=>false,
                    'color'=>true,
                    'preview'=>false,
                    'all_styles' => false,
                    'units'=>'px', // Defaults to px
                    'desc'=> __('Navigation font settings', 'dawn'),
                    'default'=> array(
                        'font-weight'=> '700',
                        'font-family' => 'Raleway' ,
                        'color' => '#7a7a7a' ,
                        'font-size' => '14px'
                    ),
                ),
                array(
                'id'       => 'css_editor',
                'type'     => 'ace_editor',
                'title'    => __('Custom CSS', 'dawn'),
                'subtitle' => __('Insert your CSS code here.', 'dawn'),
                'mode'     => 'css',
                'theme'    => 'monokai',
                'desc'     => '',
                'default'  => ""
                )

            )
    ));


    /*
     * <--- END SECTIONS
     */

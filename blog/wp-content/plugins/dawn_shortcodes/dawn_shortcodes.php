<?php
/*
 Plugin Name: Dawn Theme Shortcodes
 Plugin URI: http://themeforest.net/user/montuthemes
 Description: Additional Shortcodes
 Version: 1.0
 Author: Montuthemes
 Author URI: http://themeforest.net/user/montuthemes
 Text Domain: tnf
*/


/* - Blockquote - */
add_shortcode('dawn_blockquote' , function($attrs, $content){

    extract(shortcode_atts(array('author' => '') , $attrs));

    return '<div class="blockquote">
            <div class="icon"><i class="fa fa-quote-left"></i></div>
            <div class="content">
              <p>'.$content.'</p><span class="author">'.$author.'</span>
            </div>
          </div>';
});

/* - Margin - */
add_shortcode('dawn_margin' , function($attrs, $content){

    extract(shortcode_atts(array('margin' => '15') , $attrs));

    return '<div style="height: '.$margin.'px" class="margin"></div>';
});

/* - Grid - */
add_shortcode('dawn_grid' , function($attrs , $content){

    extract(shortcode_atts(array('width' => 'half') , $attrs));

    return '<div class="grid-'.$width.'">'.do_shortcode($content).'</div>';
});

/* - Grid Container - */
add_shortcode('dawn_grid_container' , function($attrs, $content){

    return '<div class="images-gallery custom-grid-section gutter1">'.do_shortcode($content).'</div><!-- end grid container -->';
});

/* - Social Icons - */
add_shortcode('dawn_social_icons' , function($attrs, $content){

    $dawn_theme_options = dawn_theme_settings();

    extract(shortcode_atts(array(
        'icons' => 'facebook,twitter,instagram,linkedin'
    ) , $attrs));

    $output = '<div class="social_icons">';
    $output .= tnframework_social_icons($icons ,$dawn_theme_options, false , true);
    $output .= '</div>';

    return $output;
});


// Plugin Translation
function dawn_theme_shortcodes_translations(){
		load_plugin_textdomain('dawn', false, get_template_directory() . '/lang/' );
}
add_action('plugins_loaded' , 'dawn_theme_shortcodes_translations' , 9);
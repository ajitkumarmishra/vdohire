<?php
/*
 Plugin Name: Dawn Theme Slidess Plugin
 Plugin URI: http://themeforest.net/user/montuthemes
 Description: Dawn Theme Slides Plugins
 Version: 1.0
 Author: Montuthemes
 Author URI: http://themeforest.net/user/montuthemes
 Text Domain: dawn
*/


/* - Register Slides Post Type - */
add_action('init' , 'dawn_slides_posttype');
function dawn_slides_posttype(){

    $labels = array( 'name' => __( 'Slides' , 'dawn') , 'singular_name' => __( 'Slide' , 'dawn') );

    $args = array(
        'label'                 => __( 'Slides', 'dawn' ),
		'description'           => __( 'Add A New Slide', 'dawn' ),
		'labels'                => $labels,
		'supports'              => array('title' , 'editor' , 'thumbnail'),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
        'taxonomies' => array('category'),
		'capability_type'       => 'page');

    register_post_type( 'dawn_slides', $args);
}

// Plugin Translation
function dawn_theme_slides_translation(){
		load_plugin_textdomain('dawn', false, plugin_dir_path(__FILE__) . '/lang/' );
}
add_action('plugins_loaded' , 'dawn_theme_slides_translation' , 9);
<?php

/* - Twitter Widget - */

$dawn_theme_options = function_exists('dawn_theme_settings') ? dawn_theme_settings() : '';

require_once(DAWN_PLUGIN_DIR . '/dawn_twitter_widget/inc/twitter_oauth/twitteroauth/twitteroauth.php');


/* --------------------------------------------------------------
  Twitter Date Handler

  * this handler will use twitter ouath framework to grab
  * lastest user tweets using API keys
-------------------------------------------------------------- */
function tnframework_twitter_time($date){

    $output = '';

    //get current timestampt
    $b = strtotime("now");
    //get timestamp when tweet created
    $c = strtotime($date);
    //get difference
    $d = $b - $c;
    //calculate different time values
    $minute = 60;
    $hour = $minute * 60;
    $day = $hour * 24;
    $week = $day * 7;

    if(is_numeric($d) && $d > 0) {
        //if less then 3 seconds
        if($d < 3) return "right now";
        //if less then minute
        if($d < $minute) return floor($d) . " seconds ago";
        //if less then 2 minutes
        if($d < $minute * 2) return "about 1 minute ago";
        //if less then hour
        if($d < $hour) return floor($d / $minute) . " minutes ago";
        //if less then 2 hours
        if($d < $hour * 2) return "about 1 hour ago";
        //if less then day
        if($d < $day) return floor($d / $hour) . " hours ago";
        //if more then day, but less then 2 days
        if($d > $day && $d < $day * 2) return "yesterday";
        //if less then year
        if($d < $day * 365) return floor($d / $day) . " days ago";
        //else return more than a year
        return "over a year ago";
    }

    return $output;

}
/* -------------------------------------------------------------*/
/* Twitter Function*/
/* -------------------------------------------------------------*/
function tnframework_twitter($args)
{

    $dawn_theme_options = function_exists('dawn_theme_settings') ? dawn_theme_settings() : '';

    $limit = 1 ;
    $twitter_id = '';
    $enable_id = true;
    $widget = false;
    $alternative_style = false;
    $array = false;

    extract($args);

    $consumer_key = isset($dawn_theme_options['twitter_consumer_key']) ? $dawn_theme_options['twitter_consumer_key'] : '';

    $consumer_secret = isset($dawn_theme_options['twitter_consumer_secret']) ? $dawn_theme_options['twitter_consumer_secret'] : '';

    $access_token = isset($dawn_theme_options['twitter_access_token']) ? $dawn_theme_options['twitter_access_token'] : '';

    $access_token_secret = isset($dawn_theme_options['twitter_access_token_secret']) ? $dawn_theme_options['twitter_access_token_secret'] : '';

    $settings = array(
        'oauth_access_token' => $access_token,
        'oauth_access_token_secret' => $access_token_secret,
        'consumer_key' => $consumer_key,
        'consumer_secret' => $consumer_secret
    );
    $return = '';
    $returnArray = array();
    $twitterconn = new TwitterOAuth($consumer_key, $consumer_secret, $access_token, $access_token_secret);
    $latesttweets = $twitterconn->get("https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=".$twitter_id."&count=".($limit));
    if($consumer_key != '' && $consumer_secret != '' && $access_token != '' && $access_token_secret != '')
    {


            foreach($latesttweets as $tweet ){

                  if($array == false)
                  {
                            if($alternative_style == true) {


                            // Create the tweet
                            $id_str = ( isset($tweet->id_str) ) ? $tweet->id_str : '';
                            $tweet_url = 'https://twitter.com/twitter/status/' . $id_str;
                            $time = new DateTime($tweet->created_at);
                            $time = $time->format('d');

                            // Return
                            $return .= '<div class="tweet">';

                            $return .= '<p class="carousel-single">';

                            $output =  preg_replace('@(https?://([-\w\.]+[-\w])+(:\d+)?(/([\w/_\.#-]*(\?\S+)?[^\.\s])?)?)@', '<a class="special-link" href="$1" target="_blank">$1</a>', $tweet->text);
                            $return .= preg_replace('/(^|\s)@([a-z0-9_]+)/i',
                                            '$1<a href="http://www.twitter.com/$2">@$2</a>',
                                             $output);



                            $return .= '</p>';

                            $return .= '<a target="_blank" class="tweet-date" href="'.$tweet_url.'">' .   $time  .   __(' days ago' , 'dawn') . '</a>';

                            $return .= '</div><!-- end tweet -->';

                          }else {

                              $return .= '<div class="tweet">';

                              if($widget != false) $return .= '<i class="fa fa-quote-left"></i>';

                              $return .= '<p class="carousel-single">';

                              $output =  preg_replace('@(https?://([-\w\.]+[-\w])+(:\d+)?(/([\w/_\.#-]*(\?\S+)?[^\.\s])?)?)@', '<a class="custom" href="$1" target="_blank">$1</a>', $tweet->text);
                              $return .= preg_replace('/(^|\s)@([a-z0-9_]+)/i',
                                              '$1<a href="http://www.twitter.com/$2">@$2</a>',
                                               $output);
                              $return .= '</p></div>';
                              }



                          if($enable_id === 'true') {
                              if($widget == false)
                                $return .= '<a target="_blank" href="http://twitter.com/'.$twitter_id.'" class="user"><span class="bg"></span>'.__('Follow Us!' , 'dawn').'</a>';
                              else
                                $return .= '<a target="_blank" href="http://twitter.com/'.$twitter_id.'" href="#" class="follow-us-btn btn btn-white btn-small">'.__('Follow Us!' , 'dawn').'</a>';
                          }

                  }
                  else {

                      $tweet_time = tnframework_twitter_time( $tweet->created_at);
                      $returnArray[] = array(
                          'text' => preg_replace('@(https?://([-\w\.]+[-\w])+(:\d+)?(/([\w/_\.#-]*(\?\S+)?[^\.\s])?)?)@', '<a class="custom" href="$1" target="_blank">$1</a>', $tweet->text),
                          'url' => 'https://twitter.com/twitter/status/' . $tweet->id_str ,
                          'time' => $tweet_time
                      );


                  }


            } // end foreach

            if($return != '') return $return; else return $returnArray;

      } // end if

}


/* -------------------------------------------------------------*/
/* About Us Widget */
/* -------------------------------------------------------------*/

class TNFramework_Widget_Twitter extends WP_Widget
{


    public $widget_fields;

    public function __construct(){


        parent::__construct(
          'tnframework_widget_twitter', // Base ID
          __( 'Twitter Widget', 'dawn' ), // Name
          array( 'description' => __( 'this widget will show your recent tweets .', 'dawn' ), ) // Args
        );

        $this->widget_fields = array(

            array(
                'id' => 'title' ,
                'title' => __('Title' , 'dawn') ,
                'type' => 'text' ,
                'description' => ''
              ),
            array(
                'id' => 'username' ,
                'title' => __('Twitter Username' , 'dawn') ,
                'type' => 'text' ,
                'description' => wp_kses( __('Add your twitter username , <strong>Note :</strong> you need to add your twitter consumer keys in Theme Options .' ,'dawn') , wp_kses_allowed_html('data'))
              ),
            array(
                'id' => 'limit' ,
                'title' => __('Limit Tweets' , 'dawn') ,
                'type' => 'text' ,
                'description' => __('Limit how many tweets should be printed .' , 'dawn')
              )

        );
    }

    public function widget($args , $instance){


        // widget args
        extract($args);

        // fields
        foreach ($this->widget_fields as $field) {

            $instance[$field['id']] = $instance[$field['id']] ? $instance[$field['id']] : '';
        }


        // Widget
        echo $before_widget;
        echo $before_title;
        echo $instance['title'];
        echo $after_title;

        // Output

        ?>

        <div class="twitter-feed-container">
            <?php

            $args  = array(
                'limit' => $instance['limit'] ,
                'twitter_id' => $instance['username'] ,
                'enable_id' => false,
                'widget' => false,
                'alternative_style' => false ,
                'array' => true

             );
             if(function_exists('tnframework_twitter')){
                $tweets =  tnframework_twitter($args);
                if(is_array($tweets))
                {
                 foreach ($tweets as $tweet) {

                     $return = '<div class="tweet">';
                     $return .= '<a href="'.esc_url($tweet['url']).'" class="special-link">@'.$instance['username'].'</a>';
                     $return .= '<p>'.$tweet['text'].'</p>';
                     $return .= '<div class="meta"><i class="fa fa-twitter"></i><span class="date">'.$tweet['time'].'</span></div>';
                     $return .= '</div>';

                     echo $return;

                 }
                }
             }

            ?>
        </div>
        <!-- end Twitter widget container -->


        <?php


        // Widget End
        echo $after_widget;



    }

    public function form($instance){

        if(function_exists('tnframework_prepare_widget_fields')) tnframework_prepare_widget_fields($this->widget_fields , $instance , $this);
    }


    public function update( $new_instance, $old_instance ) {
      $instance = array();
      foreach ($this->widget_fields as $field) {
        $instance[$field['id']] = ( ! empty( $new_instance[$field['id']] ) ) ? strip_tags( $new_instance[$field['id']] ) : '';
      }

      return $instance;
    }


}


function tnframework_register_widget_twitter()
{
  register_widget('TNFramework_Widget_Twitter');
}
add_action('widgets_init' , 'tnframework_register_widget_twitter');
<?php

$dawn_theme_options = function_exists('dawn_theme_settings') ? dawn_theme_settings() : '';

/* -------------------------------------------------------------*/
/* About Us Widget */
/* -------------------------------------------------------------*/

class TNFramework_Widget_Aboutme extends WP_Widget
{


		public $widget_fields;

		public function __construct(){


				parent::__construct(
					'tnframework_widget_aboutme', // Base ID
					__( 'About Me Widget', 'dawn' ), // Name
					array( 'description' => __( 'about Me widget', 'dawn' ) ) // Args
				);

				$this->widget_fields = array(

						array(
								'id' => 'title' ,
								'title' => esc_html__('Title' , 'dawn') ,
								'type' => 'text' ,
								'description' => ''
							),
						array(
								'id' => 'image' ,
								'title' => esc_html__('Add Image' , 'dawn') ,
								'type' => 'text' ,
								'description' => esc_html__('Add your image ID here .' , 'dawn')
							),
						array(
								'id' => 'desc' ,
								'title' => esc_html__('Description' , 'dawn') ,
								'type' => 'textarea' ,
								'description' => esc_html__('Add widget content here' , 'dawn')
							),
						array(
								'id' => 'social' ,
								'title' => esc_html__('Social Icons' , 'dawn') ,
								'type' => 'text' ,
								'description' => esc_html__('Add social icons, you can use this pattern "facebook,twitter,pinterest" .. etc , Edit your social settings from Theme Options > Social Media Settings' , 'dawn')
							)

				);
		}

		public function widget($args , $instance){

				// theme options
				$dawn_theme_options = dawn_theme_settings();

				// widget args
				extract($args);

				// fields
				foreach ($this->widget_fields as $field) {

						$instance[$field['id']] = $instance[$field['id']] ? $instance[$field['id']] : '';
				}


				// Widget
				echo $before_widget;
				echo $before_title;
				echo $instance['title'];
				echo $after_title;

				// Output
				?>
				 <div class="about-widget">


                    <!-- image -->
                    <?php

                    if(isset($instance['image'])) {
                		$imagesrc = wp_get_attachment_image_src( $instance['image'], 'full');
						if(isset($imagesrc[0])) echo '<img src="'.esc_attr($imagesrc[0]).'" alt="">';
                    }

                    ?>

                    <!-- content -->
                    <p class="user-content">
                        <?php echo isset($instance['desc']) ? $instance['desc'] : ''; ?>
                    </p>

                    <!-- social -->
                    <div class="social-icons-colored">

                        <?php if(isset($instance['social'])) tnframework_social_icons($instance['social'] , $dawn_theme_options);  ?>

                    </div>
                    <!-- end social icons solid -->

                </div>
                <!-- end about widget -->



				<?php


				// Widget End
				echo $after_widget;



		}

		public function form($instance){

			if(function_exists('tnframework_prepare_widget_fields')) tnframework_prepare_widget_fields($this->widget_fields , $instance , $this); else echo esc_html__('Function Does Not Exist' , 'dawn');
		}


		public function update( $new_instance, $old_instance ) {
			$instance = array();
			foreach ($this->widget_fields as $field) {
				$instance[$field['id']] = ( ! empty( $new_instance[$field['id']] ) ) ? strip_tags( $new_instance[$field['id']] ) : '';
			}

			return $instance;
		}


}


function tnframework_register_widget_aboutme()
{
	register_widget('TNFramework_Widget_Aboutme');
}
add_action('widgets_init' , 'tnframework_register_widget_aboutme');


<?php
$dawn_theme_options = function_exists('dawn_theme_settings') ? dawn_theme_settings() : '';

/* -------------------------------------------------------------*/
/* About Us Widget */
/* -------------------------------------------------------------*/

class TNFramework_Widget_Instagram extends WP_Widget
{


    public $widget_fields;

    public function __construct(){


        parent::__construct(
          'tnframework_widget_instagram', // Base ID
          __( 'Instagram Widget', 'dawn' ), // Name
          array( 'description' => __( 'this widget will show your recent instagram photos .', 'dawn' ), ) // Args
        );

        $this->widget_fields = array(

            array(
                'id' => 'title' ,
                'title' => esc_html__('Title' , 'dawn') ,
                'type' => 'text' ,
                'description' => ''
              ),
            array(
                'id' => 'user_id' ,
                'title' => esc_html__('User ID' , 'dawn') ,
                'type' => 'text' ,
                'description' => wp_kses( __('You can get any user id <a href="//jelled.com/instagram/lookup-user-id"> here </a>' , 'dawn') , wp_kses_allowed_html('data'))
              ),
             array(
                'id' => 'client_id' ,
                'title' => esc_html__('Client ID' , 'dawn') ,
                'type' => 'text' ,
                'description' => wp_kses( __('<strong>Note :</strong> Check the documenations for more information about how to setup your API key .' , 'dawn') , wp_kses_allowed_html('data'))
              ),
            array(
                'id' => 'limit' ,
                'title' => esc_html__('Limit' , 'dawn') ,
                'type' => 'text' ,
                'description' => esc_html__('Limit how many photos you want to show .' , 'dawn')
              )

        );
    }

    public function widget($args , $instance){

        // theme options
        $dawn_theme_options = dawn_theme_settings();

        // widget args
        extract($args);

        // fields
        foreach ($this->widget_fields as $field) {

            $instance[$field['id']] = $instance[$field['id']] ? $instance[$field['id']] : '';
        }


        // Widget
        echo $before_widget;
        echo $before_title;
        echo $instance['title'];
        echo $after_title;

        // Output

        ?>

        <div class="instagram">

            <?php
            $return = '';
            $content = 'https://api.instagram.com/v1/users/'.$instance['user_id'].'/media/recent?client_id='.$instance['client_id'].'&count='.$instance['limit'];

            $get_content = wp_remote_get($content);


            // check for errors
            if(is_wp_error($get_content))
            {
            $error = $get_content->get_error_message;
            $return .= $error;
            }else{

            $output = json_decode( $get_content['body']);
            if(!empty($output) && is_array($output->data))
            {
            foreach ($output->data as $key => $value) {
                $return .=  '<a target="_blank" href="'.esc_url($value->link).'"><img src="'.esc_attr($value->images->thumbnail->url).'" alt="'.esc_attr($value->caption->text).'" /></a>';
            }

            }

            }
            echo $return;
            ?>

        </div>
        <!-- end instagram widget container -->


        <?php


        // Widget End
        echo $after_widget;



    }

    public function form($instance){

        if(function_exists('tnframework_prepare_widget_fields')) tnframework_prepare_widget_fields($this->widget_fields , $instance , $this);
    }


    public function update( $new_instance, $old_instance ) {
      $instance = array();
      foreach ($this->widget_fields as $field) {
        $instance[$field['id']] = ( ! empty( $new_instance[$field['id']] ) ) ? strip_tags( $new_instance[$field['id']] ) : '';
      }

      return $instance;
    }


}


function tnframework_register_widget_instagram()
{
  register_widget('TNFramework_Widget_Instagram');
}
add_action('widgets_init' , 'tnframework_register_widget_instagram');
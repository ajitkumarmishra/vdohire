<?php
$dawn_theme_options = function_exists('dawn_theme_settings') ? dawn_theme_settings() : '';

/* -------------------------------------------------------------*/
/* About Us Widget */
/* -------------------------------------------------------------*/

class TNFramework_Widget_Popularposts extends WP_Widget
{


    public $widget_fields;

    public function __construct(){


        parent::__construct(
          'tnframework_widget_popularposts', // Base ID
          __( 'Popular Posts', 'dawn' ), // Name
          array( 'description' => __( 'this widget will show posts sorted by comments count .', 'dawn' ), ) // Args
        );

        $this->widget_fields = array(

            array(
                'id' => 'title' ,
                'title' => esc_html__('Title' , 'dawn') ,
                'type' => 'text' ,
                'description' => ''
              ),
            array(
                'id' => 'limit' ,
                'title' => esc_html__('Limit' , 'dawn') ,
                'type' => 'text' ,
                'description' => esc_html__('Limit how many posts  you want to show .' , 'dawn')
              )

        );
    }

    public function widget($args , $instance){

        // theme options
        $dawn_theme_options = dawn_theme_settings();

        // widget args
        extract($args);

        // fields
        foreach ($this->widget_fields as $field) {

            $instance[$field['id']] = $instance[$field['id']] ? $instance[$field['id']] : '';
        }


        // Widget
        echo $before_widget;
        echo $before_title;
        echo $instance['title'];
        echo $after_title;

        // Output

        ?>

        <!-- popular posts -->
        <div class="widget-content popular-posts">


        <?php


        $featured_args = array(
                'posts_per_page' => $instance['limit'] ,
                'orderby' => 'comment_count',
                'order' => 'DESC',
                'ignore_sticky_posts' => 1
        );

        $featured_query = new WP_Query($featured_args);


        if($featured_query->have_posts()) : while($featured_query->have_posts()) : $featured_query->the_post();

        ?>

            <!-- single post -->
            <div class="post">

                <?php

                $image = '';

                // Get Featured Image
                $imagesrc = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'dawn-widget-thumbnail');

                if(isset($imagesrc[0])) $image = $imagesrc[0];

                ?>

                <!-- Post Image -->
                <a href="<?php echo esc_url(get_permalink()); ?>" class="post-img">
                    <?php if($image != '') :  ?>
                        <img src="<?php echo esc_attr($image); ?>" alt="<?php echo esc_attr(get_the_title()); ?>">
                    <?php endif; ?>
                </a><!-- end Image -->

                <!-- Post Content -->
                <a href="<?php echo esc_url(get_permalink()); ?>">

                    <p><?php echo get_the_title(); ?></p>

                    <div class="popular-post-meta">
                        <span class="date"><?php echo get_the_date('M d , Y'); ?></span><span class="comments"><?php echo $featured_query->comment_count . esc_html__(' Comments' , 'dawn'); ?></span>
                    </div><!-- end popular post meta -->

                </a><!-- end Post Content-->

            </div>
            <!-- end single post -->

        <?php endwhile; endif; wp_reset_postdata(); ?>


        </div>
        <!-- end popular posts -->

        <?php


        // Widget End
        echo $after_widget;



    }

    public function form($instance){

        if(function_exists('tnframework_prepare_widget_fields')) tnframework_prepare_widget_fields($this->widget_fields , $instance , $this);
    }


    public function update( $new_instance, $old_instance ) {
      $instance = array();
      foreach ($this->widget_fields as $field) {
        $instance[$field['id']] = ( ! empty( $new_instance[$field['id']] ) ) ? strip_tags( $new_instance[$field['id']] ) : '';
      }

      return $instance;
    }


}


function tnframework_register_widget_popularposts()
{
  register_widget('TNFramework_Widget_Popularposts');
}
add_action('widgets_init' , 'tnframework_register_widget_popularposts');
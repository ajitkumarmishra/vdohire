<?php
$dawn_theme_options = function_exists('dawn_theme_settings') ? dawn_theme_settings() : '';

/* -------------------------------------------------------------*/
/* About Us Widget */
/* -------------------------------------------------------------*/

class TNFramework_Widget_Categories extends WP_Widget
{


    public $widget_fields;

    public function __construct(){


        parent::__construct(
          'tnframework_widget_categories', // Base ID
          __( 'Categories Widget', 'dawn' ), // Name
          array( 'description' => __( 'Categories widget', 'dawn' ) ) // Args
        );

        $this->widget_fields = array(

            array(
                'id' => 'title' ,
                'title' => esc_html__('Title' , 'dawn') ,
                'type' => 'text' ,
                'description' => ''
              ),
            array(
                'id' => 'limit' ,
                'title' => esc_html__('Limit Categories' , 'dawn') ,
                'type' => 'text' ,
                'description' => esc_html__('Limit categories.' , 'dawn')
              )

        );

    }

    public function widget($args , $instance){

        // theme options
        $dawn_theme_options = dawn_theme_settings();

        // widget args
        extract($args);

        // fields
        foreach ($this->widget_fields as $field) {

            $instance[$field['id']] = $instance[$field['id']] ? $instance[$field['id']] : '';
        }


        // Widget
        echo $before_widget;
        echo $before_title;
        echo $instance['title'];
        echo $after_title;

        // Output
        ?>

        <!-- custom list -->
        <div class="categories">

        <ul class="custom-list">
        <?php

        $args = array (
          'echo' => 0,
          'show_count' => 1,
          'title_li' => '',
          'depth' => 1 ,
          'orderby' => 'count' ,
          'order' => 'DESC' ,
          'number' => $instance['limit']
          );
        $variable = wp_list_categories($args);
        $variable = str_replace ( "(" , '<span class="count">', $variable );
        $variable = str_replace ( ")" , "</span>", $variable );
        echo $variable;

        ?>
        </ul>
        </div><!-- end categories -->


        <?php


        // Widget End
        echo $after_widget;



    }

    public function form($instance){

        if(function_exists('tnframework_prepare_widget_fields')) tnframework_prepare_widget_fields($this->widget_fields , $instance , $this);
    }


    public function update( $new_instance, $old_instance ) {
      $instance = array();
      foreach ($this->widget_fields as $field) {
        $instance[$field['id']] = ( ! empty( $new_instance[$field['id']] ) ) ? strip_tags( $new_instance[$field['id']] ) : '';
      }

      return $instance;
    }


}


function tnframework_register_widget_categories()
{
  register_widget('TNFramework_Widget_Categories');
}
add_action('widgets_init' , 'tnframework_register_widget_categories');
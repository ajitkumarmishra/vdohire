<?php
$dawn_theme_options = function_exists('dawn_theme_settings') ? dawn_theme_settings() : '';

/* -------------------------------------------------------------*/
/* Social Icons Widget */
/* -------------------------------------------------------------*/

class TNFramework_Widget_Social extends WP_Widget
{


		public $widget_fields;

		public function __construct(){


				parent::__construct(
					'tnframework_widget_social', // Base ID
					__( 'Social Icons Widget', 'dawn' ), // Name
					array( 'description' => __( 'Social Icons Widget', 'dawn' ) ) // Args
				);

				$this->widget_fields = array(

						array(
								'id' => 'title' ,
								'title' => esc_html__('Title' , 'dawn') ,
								'type' => 'text' ,
								'description' => ''
							),
						array(
								'id' => 'social' ,
								'title' => esc_html__('Social Icons' , 'dawn') ,
								'type' => 'text' ,
								'description' => esc_html__('Add social icons, you can use this pattern "facebook,twitter,pinterest" .. etc , Edit your social settings from Theme Options > Social Media Settings' , 'dawn')
							)

				);
		}

		public function widget($args , $instance){

				// theme options
				$dawn_theme_options = dawn_theme_settings();

				// widget args
				extract($args);

				// fields
				foreach ($this->widget_fields as $field) {

						$instance[$field['id']] = $instance[$field['id']] ? $instance[$field['id']] : '';
				}


				// Widget
				echo $before_widget;
				echo $before_title;
				echo $instance['title'];
				echo $after_title;

				// Output
				?>
				 <div class="social-icons-colored-large">

                    <!-- social -->
                    <div class="social-icons-colored">


                        <?php if(isset($instance['social'])) tnframework_social_icons($instance['social'] , $dawn_theme_options);  ?>

                    </div>
                    <!-- end social icons solid -->

                </div>
                <!-- end social icons -->



				<?php


				// Widget End
				echo $after_widget;



		}

		public function form($instance){

			if(function_exists('tnframework_prepare_widget_fields')) tnframework_prepare_widget_fields($this->widget_fields , $instance , $this); else echo 'Function Does Not Exist';
		}


		public function update( $new_instance, $old_instance ) {
			$instance = array();
			foreach ($this->widget_fields as $field) {
				$instance[$field['id']] = ( ! empty( $new_instance[$field['id']] ) ) ? strip_tags( $new_instance[$field['id']] ) : '';
			}

			return $instance;
		}


}


function tnframework_register_widget_social()
{
	register_widget('TNFramework_Widget_Social');
}
add_action('widgets_init' , 'tnframework_register_widget_social');


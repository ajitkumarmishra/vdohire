<?php

function tnframework_prepare_widget_fields($fields , $instance , $object){

   if(!empty($fields) && is_array($fields))
   {

           $return = '';

           foreach($fields as $field)
           {

               if(isset($field['id'])){

                       // Field Value
                       $fieldValue = isset($instance[$field['id']]) ? $instance[$field['id']] : '';
                       $name = $object->get_field_name($field['id']);
                       $id = $object->get_field_id($field['id']);

                       // Return
                       $return .= '<p>';
                       $return .= '<label for="'.esc_attr($id).'"><b>'.$field['title'].'</b></label><br />';

                       switch($field['type'])
                       {
                                   case 'text' :
                                       $return .= '<input type="text" id="'.esc_attr($id).'" class="widefat" name="'.esc_attr($name).'" value="'.esc_attr($fieldValue).'">';
                                   break;

                                   case 'textarea' :
                                       $return .= '<textarea class="widefat" name="'.esc_attr($name).'" id="'.esc_attr($id).'">'.esc_attr($fieldValue).'</textarea>';
                                   break;

                                   case 'check' :

                                   break;

                                   case 'select' :


                                   break;
                       }

                       // Print Backend
                       $return .= '<br /><span class="widefat">'.$field['description'].'</span>';
                       $return .= '</p>';
               }
           }

           echo $return;
   }

}
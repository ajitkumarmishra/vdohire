<?php

function tnframework_social_icons($icons = 'facebook,twitter,dribbble,google,pinterest', $options, $wrap = false, $return = false) {

	$output = '';
	$return_url = '';
	$icons = str_replace(' ', '', $icons);
	$icons = explode(',', $icons);
	if ($wrap != false) {
		$output .= '<div class="' . esc_attr($wrap) . '">';
	}

	foreach ($icons as $icon) {
		if (isset($options[$icon]) && $options[$icon] != '') {
			if ($icon == 'facebook') {

				$return_url = 'http://facebook.com/' . $options['facebook'];
				$output .=  '<a href="'.esc_url($return_url).'" class="facebook"><i class="fa fa-facebook"></i></a>';

			} elseif ($icon == 'google') {

				$output .=  '<a href="' . esc_url($options['google']) . '" class="google"><i class="fa fa-google-plus"></i></a>';

			} elseif ($icon == 'twitter') {

				$return_url = 'http://twitter.com/' . $options['twitter'];
				$output .=  '<a href="'.esc_url($return_url).'" class="twitter"><i class="fa fa-twitter"></i></a>';
			}
			 else {

				$output .=  '<a href="'.esc_url(' . $options[$icon] . ').'" class="' . esc_attr($icon) . '"><i class="fa fa-' . esc_attr($icon) . '"></i></a>';
			}

		}
	}
	if ($wrap != false) {
		$output .= '</div>';
	}

	if($return === true) return $output; else echo $output;
}
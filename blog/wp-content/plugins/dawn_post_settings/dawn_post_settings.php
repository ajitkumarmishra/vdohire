<?php
/*
 Plugin Name: Dawn Post Settings
 Plugin URI: http://themeforest.net/user/montuthemes
 Description: Settings Panel Posts And Pages , "Dawn" Theme
 Version: 1.0
 Author: Montuthemes
 Author URI: http://themeforest.net/user/montuthemes
 Text Domain: dawn
*/

define('DAWN_OPTIONTREE_PLUGIN_PATH' , trailingslashit(plugin_dir_path(__FILE__) . '/option-tree') );

/* - Options Tree Loader - */
add_filter( 'ot_theme_mode', '__return_false' );
require_once( DAWN_OPTIONTREE_PLUGIN_PATH . '/ot-loader.php' );
require_once(DAWN_OPTIONTREE_PLUGIN_PATH . '/dawn-metaboxes.php');
add_filter( 'ot_show_pages', '__return_false' );
add_filter( 'ot_use_theme_options', '__return_false' );



// Plugin Translation
function dawn_optiontree_translation(){
		load_plugin_textdomain('dawn', false, get_template_directory() . '/lang/' );
}
add_action('plugins_loaded' , 'dawn_optiontree_translation' , 9);


<?php
/**
 * Initialize the custom Meta Boxes.
 */

/* Register page templates */
add_action('admin_init', 'dawn_register_page_meta_boxes');
function dawn_register_page_meta_boxes() {

	$dawn_theme_sidebars = get_option('dawn_theme_sidebars');

	// Slides Meta Boxes
	$slide_meta_boxes = array(
		'id' => 'dawn_slide_metaboxes',
		'title' => esc_html__('Slide Settings', 'dawn'),
		'desc' => '',
		'pages' => array('dawn_slides'),
		'context' => 'normal',
		'priority' => 'high',
		'fields' => array(
			array(
				'label' => esc_html__('Links', 'dawn'),
				'id' => 'main_settings_tab',
				'type' => 'tab',
			),
			array(
				'label' => esc_html__('Link One', 'dawn'),
				'id' => 'slide_link_one',
				'type' => 'text',
				'desc' => esc_html__('Add a link to this slide .', 'dawn'),
			),
			array(
				'label' => esc_html__('Link Two', 'dawn'),
				'id' => 'slide_link_two',
				'type' => 'text',
				'desc' => esc_html__('Add a second link to this slide .', 'dawn'),
			),

		),
	);

	// Sidebars
	$sidebars = array();
	foreach ($dawn_theme_sidebars as $sidebar) {
		array_push($sidebars , array(
		  'value' => $sidebar,
		  'label' => __( 'Sidebar '.strtoupper($sidebar), 'dawn' )
		));
	}

	// Page Meta Boxes
	$page_meta_boxes = array(
		'id' => 'dawn_page_meta_boxes',
		'title' => esc_html__('Slide Settings', 'dawn'),
		'desc' => '',
		'pages' => array('page'),
		'context' => 'normal',
		'priority' => 'high',
		'fields' => array(
			array(
				'label' => esc_html__('Links', 'dawn'),
				'id' => 'main_settings_tab',
				'type' => 'tab',
			),
			array(
             'label'       => __( 'Select Sidebar', 'dawn' ),
             'id'          => 'sidebar',
             'desc' => esc_html__('Select sidebar for this page , you can edit sidebars widgets in Widgets page.' , 'dawn') ,
             'type'        => 'select' ,
             'choices' => $sidebars
           ),

		),
	);


	// Post Meta Settings
	$post_meta_boxes = array(
	'id'          => 'dawn_post_meta_boxes',
	'title'       => __( 'Post Settings', 'dawn' ),
	'desc'        => '',
	'pages'       => array( 'post' ),
	'context'     => 'normal',
	'priority'    => 'high',
	'fields'      => array(

	  array(
	    'label'       => __( 'Video Settings', 'dawn' ),
	    'id'          => 'video_tab',
	    'type'        => 'tab'
	  ),
	  array(
	    'label'       => __( 'Embed Video', 'dawn' ),
	    'id'          => 'post_embed_video',
	    'type'        => 'textarea',
	    'desc'        => __( 'Insert video code here for video post format .. i.e youtube embed code , please select Video post format.', 'dawn' )
	  ),
	  array(
	    'label'       => __( 'Video Height', 'dawn' ),
	    'id'          => 'post_video_height',
	    'type'        => 'text',
	    'desc'        => __( 'Add video height in pixels .. ', 'dawn' )
	  ),
	  array(
	    'label'       => __( 'Audio Settings', 'dawn' ),
	    'id'          => 'audio_tab',
	    'type'        => 'tab'
	  ),
	  array(
	    'label'       => __( 'Embed Audio', 'dawn' ),
	    'id'          => 'post_embed_audio',
	    'type'        => 'textarea',
	    'desc'        => __( 'Insert audio code here for audio post format .. i.e soundcloud embed code  , please select Audio post format.', 'dawn' )
	  ),

	  array(
	    'label'       => __( 'Gallery Settings', 'dawn' ),
	    'id'          => 'gallery_tab',
	    'type'        => 'tab'
	  ),
	  array(
	   'label'       => __( 'Gallery Type', 'dawn' ),
	   'id'          => 'gallery_type',
	   'desc' => esc_html__('Select gallery type , By default gallery images will be represented as a Slider , You can choose Slider Or Grid.' , 'dawn') ,
	   'type'        => 'select' ,
	   'choices' => array(
		   array(
			   'value' => 'slider' ,
			   'label' => esc_html__('Slider' , 'dawn')
		   ),
		   array(
			   'value' => 'grid' ,
			   'label' => esc_html__('Grid' , 'dawn')
		   )
	   )
	  ),
	  array(
	    'label'       => __( 'Gallery', 'dawn' ),
	    'id'          => 'post_gallery',
	    'type'        => 'gallery',
	    'desc'        => esc_html__('Create a gallery for this post , Make sure Gallery post type is selected.' , 'dawn'),
	  ),
	   array(
	    'label'       => __( 'Link Settings', 'dawn' ),
	    'id'          => 'link_tab',
	    'type'        => 'tab'
	  ),
	    array(
	    'label'       => __( 'Link URL', 'dawn' ),
	    'id'          => 'post_link',
	    'type'        => 'text',
	    'desc'        => __( 'Add your link here , Make sure Link post format is selected.', 'dawn' )
	  ),
	    array(
	    'label'       => __( 'Link Description', 'dawn' ),
	    'id'          => 'post_link_description',
	    'type'        => 'textarea',
	    'desc'        => __( 'Add your link description , Make sure Link post format is selected .', 'dawn' )
	  ),
	  array(
	    'label'       => __( 'Quote Settings', 'dawn' ),
	    'id'          => 'quote_tab',
	    'type'        => 'tab'
	  ),

	    array(
	    'label'       => __( 'Quote', 'dawn' ),
	    'id'          => 'post_quote',
	    'type'        => 'textarea',
	    'desc'        => wp_kses(__( 'Add quote content here , Make sure <strong>Quote</strong> post format is selected .', 'dawn' ) , wp_kses_allowed_html('data'))
	  ),
	     array(
	    'label'       => __( 'Quote Author', 'dawn' ),
	    'id'          => 'post_quote_author',
	    'type'        => 'text',
	    'desc'        => wp_kses(__( 'Add quote author here , Make sure <strong>Quote</strong> post format is selected .', 'dawn' ) , wp_kses_allowed_html('data'))
	  ),

	)

	);

	/**
	 * Register meta boxes using the
	 * ot_register_meta_box() function.
	 */

	if (function_exists('ot_register_meta_box')) {
		ot_register_meta_box($slide_meta_boxes);
		ot_register_meta_box($page_meta_boxes);
		ot_register_meta_box($post_meta_boxes);
	}

}
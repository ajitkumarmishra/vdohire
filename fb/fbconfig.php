<?php
session_start();

// added in v4.0.0
require_once 'autoload.php';
use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\FacebookSDKException;
use Facebook\FacebookRequestException;
use Facebook\FacebookAuthorizationException;
use Facebook\GraphObject;
use Facebook\Entities\AccessToken;
use Facebook\HttpClients\FacebookCurlHttpClient;
use Facebook\HttpClients\FacebookHttpable;
// init app with app id and secret
FacebookSession::setDefaultApplication( '1916423951933088','c8bc5df02c559f33c81a6ee2b218473e' );
// login helper with redirect_uri
    $helper = new FacebookRedirectLoginHelper('https://vdohire.com/fb/fbconfig.php' );
try {
  $session = $helper->getSessionFromRedirect();
} catch( FacebookRequestException $ex ) {
  // When Facebook returns an error
} catch( Exception $ex ) {
  // When validation fails or other local issues
}
// see if we have a session
if ( isset( $session ) ) {
  // graph api request for user data
  $request = new FacebookRequest( $session, 'GET', '/me?fields=id,name,email' );
  $response = $request->execute();
  // get response
  $graphObject = $response->getGraphObject();
     	$fbid = $graphObject->getProperty('id');              // To Get Facebook ID
 	    $fbfullname = $graphObject->getProperty('name'); // To Get Facebook full name
	    $femail = $graphObject->getProperty('email');    // To Get Facebook email ID
	/* ---- Session Variables -----*/
	    $_SESSION['FBID'] = $fbid;           
        $_SESSION['FULLNAME'] = $fbfullname;
	    $_SESSION['EMAIL'] =  $femail;
		//echo $_SESSION['FBID'];die;
    /* ---- header location after session ----*/
  //header("Location: http://localhost/firstjob/");
} else {
  $loginUrl = $helper->getLoginUrl();
 header("Location: ".$loginUrl);
}
?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<body>

<div class="container" style="width:100%;">
<nav class="navbar navbar-inverse" id="navheight" style="    margin-bottom: 0px;    height: 76px;    background: #EB9B2C;border: none;">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="https://vdohire.com/jobseeker/users"><img src="https://vdohire.com/jobseeker/images/logo.png" rel="stylesheet"></a>
    </div>
    
    
    
  </div>
</nav>
<div style="background:#054F72;height: 158px;font-size: 33px;color: white;text-align: center;    padding: 52px;">
You have successfully logged in using Facebook. Please provide the information.
</div>
  <!-- Modal -->
  <div class="modal" id="myModal" role="dialog">
    <div class="modal-dialog" style="    width: 50%;">
    
      <!-- Modal content-->
      <div class="modal-content" style="margin-top: 30%;">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Basic Details</h4>
        </div>
        <div class="modal-body">
          <form id="Register" method="post" class="form-horizontal fv-form fv-form-bootstrap" style="" novalidate="novalidate"><button type="submit" class="fv-hidden-submit" style="display: none; width: 0px; height: 0px;"></button>
    <div id="page-content-wrapper">
        <div class="container-fluid whitebg">
            <div class="row">
                <div class="col-md-12 padd_tp_bt_20">
                    <div class="form-horizontal form_save">
            <div class="form-group has-feedback">
                <label for="inputEmail" class="control-label col-xs-3 required-field "><span style="color:red;">*</span>Email</label>
                <div class="col-xs-9">
                    <input type="text" class="form-control" placeholder="Email" name="email" id="email" value="<?php echo isset($_SESSION['EMAIL'])?$_SESSION['EMAIL']:""; ?>" data-fv-field="email" <?php echo isset($_SESSION['EMAIL'])? "readonly" : ""; ?>>
					<i class="form-control-feedback" data-fv-icon-for="email" style="display: none;"></i>
				</div>
            </div>
			
			<div class="form-group has-feedback">
                <label for="inputEmail" class="control-label col-xs-3 required-field "><span style="color:red;">*</span>Name</label>
                <div class="col-xs-9">
                    <input type="text" class="form-control" placeholder="Name" name="name" data-validation="length" data-validation-length="min6" id="name" value="<?php echo isset($_SESSION['FULLNAME'])?$_SESSION['FULLNAME']:""; ?>" data-fv-field="name"><i class="form-control-feedback" data-fv-icon-for="name" style="display: none;"></i>
                </div>
            </div>
			<div class="form-group has-feedback">
                <label for="inputEmail" class="control-label col-xs-3 required-field "><span style="color:red;">*</span>Phone</label>
                <div class="col-xs-9">
                    <input type="text" class="form-control" placeholder="Phone" name="phone" data-validation="length" data-validation-length="min6" id="phone" value="" data-fv-field="phone"><i class="form-control-feedback" data-fv-icon-for="phone" style="display: none;"></i>
                </div>
            </div>
			
            <div class="form-group has-feedback">
                <label for="inputEmail" class="control-label col-xs-3 required-field "><span style="color:red;">*</span>New Password</label>
                <div class="col-xs-9">
                    <input type="text" class="form-control" placeholder="New Password" name="newPassword" data-validation="length" data-validation-length="min6" id="newPassword" value="" data-fv-field="newPassword"><i class="form-control-feedback" data-fv-icon-for="newPassword" style="display: none;"></i>
                </div>
            </div>
            <div class="form-group has-feedback">
                <label for="inputEmail" class="control-label col-xs-3 required-field "><span style="color:red;">*</span>Confirm New Password</label>
                <div class="col-xs-9">
                    <input type="text" class="form-control" placeholder="Confirm New Password" name="confirmNewPassword" id="confirmNewPassword" value="" data-fv-field="confirmNewPassword"><i class="form-control-feedback" data-fv-icon-for="confirmNewPassword" style="display: none;"></i>
                </div>
            </div>

            <div class="form-group">
                <div class="col-xs-5 col-xs-offset-3">
                    <input type="hidden" name="token" id="token" value="ecbcd7eaee29848978134beeecdfbc7c">
                    <input type="hidden" name="methodname" id="methodname" value="signUp_v2">
					<input type="hidden" name="fb_id" id="fb_id" value="<?php echo isset($_SESSION['FBID'])?$_SESSION['FBID']:""; ?>">
                    <input type="hidden" name="userId" id="userId" value="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpZCI6IjE5MCJ9.TfL2ffRt2Ik_m13LfWlBi85IWy3hcW98WaSPYDOAkloDAM7jdDT18vmCDPrMBCtulQV_n-1sW2CJEsZRI4SMgQ">
                    <button type="submit" class="btn btn-default pull-right" onclick="signUp();" style="background: #054F72; color: white;">SAVE</button>
                </div>
            </div>

        </div>
    </div>
        </div>
    </div>
    </div>
</form>
        </div>
        <div class="modal-footer">
          <div class=""><span style="color:red;">*</span>Required Field.</div>
        </div>
      </div>
      
    </div>
  </div>
  
</div>
  </body>
    <script src="https://vdohire.com/js/formValidation.min.js"></script>
<script src="https://vdohire.com/js/min/bootstrap.min.js"></script>
<?php
if(isset($_SESSION['FBID']) && $_SESSION['FBID'] != ""){
?>
<script type="text/javascript">
$(document).ready(function() {
    fb_login();
});
</script>
<?php
}
?>
<script type="text/javascript">
/*for fb login*/
function signUp(){
	$('#Register').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                email: {
                    validators: {
                        notEmpty: {
                            message: 'The username is required'
                        },
                        emailAddress: {
                            message: 'The value is not a valid email address'
                        }
                    }
                },
                name: {
                    validators: {
                        notEmpty: {
                            message: 'Enter Your Full Name'
                        }
                    }
                },
				phone: {
                    validators: {
                        notEmpty: {
                            message: 'Enter Phone Number'
                        }
                    }
                },
				newPassword: {
                    validators: {
                        notEmpty: {
                            message: 'The password is required'
                        },
                        stringLength: {
                            min: 6,
                            max: 30,
                            message: 'The password must be more than 6 characters long'
                        }
                    }
                },
				confirmNewPassword: {
                    validators: {
                        notEmpty: {
                            message: 'The password is required'
                        },
                        stringLength: {
                            min: 6,
                            max: 30,
                            message: 'The password must be more than 6 characters long'
                        }
                    }
                }
				
            }
        })
		.on('success.form.fv', function(e) {
            // Prevent form submission
            e.preventDefault();

            var fullname = $("#name").val();
            var username = $("#name").val();
            var inputEmail = $("#email").val();
            var mobile = $("#phone").val();
            var email = $("#email").val();
            var inputPassword = $("#newPassword").val();
            var methodname = 'signUp_v4';
            var token = 'ecbcd7eaee29848978134beeecdfbc7c';
            var gender = 'male';
            var pincode = '110075';
            var deviceId = '1234';
            var deviceType = 'android';
            var social_networking_type = '1';
			var networking_id = '<?php echo $_SESSION['FBID'];?>';
            var js_obj = {social_networking_type:social_networking_type,networking_id:networking_id, fullname: fullname, username: username, email: inputEmail, password: inputPassword,token: token,methodname: methodname,
    mobile:mobile,gender:gender,pincode:pincode,deviceId:deviceId,deviceType:deviceType,};

            var encoded = JSON.stringify( js_obj );

            var data= encoded;

            $.ajax({
                    url : "https://"+window.location.host+"/jobseeker/Fjapi",
                    data: data,
                    type: "POST",
                    success: function(response){
                        console.log(response);
                        item = $.parseJSON(response);
                        if(item.code == "200"){
							
                            $.ajax({
                                url : "https://"+window.location.host+"/jobseeker/users/setSessionSignUp",
                                data: response,
                                type: "POST",
                                success: function(response){
                                    
                                    if(response == "success"){
                                        window.location="https://"+window.location.host+"/jobseeker/users";
                                    }else{
                                        alert(response);
                                    }
                                }
                            });
                            
                        }else{
                            alert(item.message);
                        }
                    }
            });
        });
        
        
}
function fb_login(){
	
    var methodname = 'isUserExists_V1'; //isUserExists     signIn
	var token = 'ecbcd7eaee29848978134beeecdfbc7c';
	var email = '<?php echo $_SESSION['EMAIL']; ?>';
	
	var social_networking_type = '1';
	var networking_id = '<?php echo $_SESSION['FBID'];?>';
	
	var js_obj = {social_networking_type: social_networking_type,networking_id:networking_id, token: token, methodname: methodname};
	var encoded = JSON.stringify( js_obj );
	var data= encoded;
	$.ajax({
		url : "https://"+window.location.host+"/jobseeker/Fjapi",
		data: data,
		type: "POST",
		success: function(response){
			console.log(response);
			item = $.parseJSON(response);
			if(item.code == "200"){
				var tokenUser = item.token;
				$.ajax({
					url : "https://"+window.location.host+"/jobseeker/users/setSession",
					data: response,
					type: "POST",
					success: function(response){
						
						if(response == "success"){
							window.location = "https://"+window.location.host+"/jobseeker/users";
						}else{
							alert(response);
						}
					}
				});
				
			}else if(item.code == "202"){
				//show popup 
				$("#myModal").show();
			}
		}
	});
}
</script>